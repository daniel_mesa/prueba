VERSION 5.00
Object = "{00025600-0000-0000-C000-000000000046}#5.2#0"; "Crystl32.OCX"
Object = "{C932BA88-4374-101B-A56C-00AA003668DC}#1.1#0"; "msmask32.ocx"
Object = "{0BA686C6-F7D3-101A-993E-0000C0EF6F5E}#1.0#0"; "THREED32.OCX"
Begin VB.Form FrmKgrup 
   BorderStyle     =   1  'Fixed Single
   ClientHeight    =   5820
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   4095
   Icon            =   "FrmKgrup.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   5820
   ScaleWidth      =   4095
   Begin VB.Frame FraOpciones 
      Caption         =   "Mostrar K�rdex ..."
      Height          =   735
      Left            =   0
      TabIndex        =   0
      Top             =   0
      Width           =   4095
      Begin VB.OptionButton OptOpcion 
         Caption         =   "Consolidado"
         Height          =   255
         Index           =   2
         Left            =   2760
         TabIndex        =   3
         Top             =   360
         Width           =   1215
      End
      Begin VB.OptionButton OptOpcion 
         Caption         =   "Dependencias"
         Height          =   255
         Index           =   1
         Left            =   1200
         TabIndex        =   2
         Top             =   360
         Width           =   1335
      End
      Begin VB.OptionButton OptOpcion 
         Caption         =   "Almac�n"
         Height          =   255
         Index           =   0
         Left            =   120
         TabIndex        =   1
         Top             =   360
         Value           =   -1  'True
         Width           =   975
      End
   End
   Begin VB.Frame FraGrupos 
      Caption         =   "Rango de Grupos de Art�culos"
      Height          =   1335
      Left            =   0
      TabIndex        =   4
      Top             =   840
      Width           =   4095
      Begin VB.TextBox TxtRango 
         Height          =   285
         Index           =   0
         Left            =   1560
         MaxLength       =   9
         TabIndex        =   5
         Text            =   "0"
         Top             =   360
         Width           =   1095
      End
      Begin VB.TextBox TxtRango 
         Height          =   285
         Index           =   1
         Left            =   1560
         MaxLength       =   9
         TabIndex        =   7
         Text            =   "ZZZZZZZZZ"
         Top             =   840
         Width           =   1095
      End
      Begin Threed.SSCommand CmdSelec 
         Height          =   375
         Index           =   0
         Left            =   2640
         TabIndex        =   6
         ToolTipText     =   "Selecci�n de Grupos de Art�culos"
         Top             =   360
         Width           =   615
         _Version        =   65536
         _ExtentX        =   1085
         _ExtentY        =   661
         _StockProps     =   78
         BevelWidth      =   0
         RoundedCorners  =   0   'False
         Outline         =   0   'False
         Picture         =   "FrmKgrup.frx":0442
      End
      Begin Threed.SSCommand CmdSelec 
         Height          =   375
         Index           =   1
         Left            =   2640
         TabIndex        =   8
         ToolTipText     =   "Selecci�n de Grupos de Art�culos"
         Top             =   840
         Width           =   615
         _Version        =   65536
         _ExtentX        =   1085
         _ExtentY        =   661
         _StockProps     =   78
         BevelWidth      =   0
         RoundedCorners  =   0   'False
         Outline         =   0   'False
         Picture         =   "FrmKgrup.frx":0894
      End
      Begin VB.Label LblHasta 
         AutoSize        =   -1  'True
         Caption         =   "Final : "
         Height          =   195
         Left            =   960
         TabIndex        =   22
         Top             =   840
         Width           =   465
      End
      Begin VB.Label LblInicio 
         AutoSize        =   -1  'True
         Caption         =   "Inicial : "
         Height          =   195
         Left            =   960
         TabIndex        =   21
         Top             =   360
         Width           =   540
      End
   End
   Begin VB.Frame FraFecha 
      Caption         =   "Fecha    (dd/mm/aaaa)"
      Height          =   855
      Left            =   0
      TabIndex        =   14
      Top             =   3720
      Width           =   4095
      Begin MSMask.MaskEdBox MskFecha 
         Height          =   375
         Index           =   0
         Left            =   840
         TabIndex        =   15
         Top             =   360
         Width           =   1095
         _ExtentX        =   1931
         _ExtentY        =   661
         _Version        =   393216
         MaxLength       =   10
         Mask            =   "##/##/####"
         PromptChar      =   "_"
      End
      Begin MSMask.MaskEdBox MskFecha 
         Height          =   375
         Index           =   1
         Left            =   2760
         TabIndex        =   16
         Top             =   360
         Width           =   1095
         _ExtentX        =   1931
         _ExtentY        =   661
         _Version        =   393216
         MaxLength       =   10
         Mask            =   "##/##/####"
         PromptChar      =   "_"
      End
      Begin VB.Label LblFeIni 
         Caption         =   "Inicial :"
         Height          =   255
         Left            =   240
         TabIndex        =   20
         Top             =   360
         Width           =   495
      End
      Begin VB.Label LblFeFinal 
         Caption         =   "Final :"
         Height          =   255
         Left            =   2280
         TabIndex        =   19
         Top             =   360
         Width           =   495
      End
   End
   Begin VB.Frame FraDependencias 
      Caption         =   "Rango de Dependencias"
      Enabled         =   0   'False
      Height          =   1335
      Left            =   0
      TabIndex        =   9
      Top             =   2280
      Width           =   4095
      Begin VB.TextBox TxtRango 
         Height          =   285
         Index           =   3
         Left            =   1440
         MaxLength       =   11
         TabIndex        =   12
         Text            =   "ZZZZZZZZZZZ"
         Top             =   840
         Width           =   1335
      End
      Begin VB.TextBox TxtRango 
         Height          =   285
         Index           =   2
         Left            =   1440
         MaxLength       =   11
         TabIndex        =   10
         Text            =   "0"
         Top             =   360
         Width           =   1335
      End
      Begin Threed.SSCommand CmdSelec 
         Height          =   375
         Index           =   3
         Left            =   2760
         TabIndex        =   13
         ToolTipText     =   "Selecci�n de Dependencias"
         Top             =   840
         Width           =   615
         _Version        =   65536
         _ExtentX        =   1085
         _ExtentY        =   661
         _StockProps     =   78
         BevelWidth      =   0
         RoundedCorners  =   0   'False
         Outline         =   0   'False
         Picture         =   "FrmKgrup.frx":0CE6
      End
      Begin Threed.SSCommand CmdSelec 
         Height          =   375
         Index           =   2
         Left            =   2760
         TabIndex        =   11
         ToolTipText     =   "Selecci�n de Dependencias"
         Top             =   360
         Width           =   615
         _Version        =   65536
         _ExtentX        =   1085
         _ExtentY        =   661
         _StockProps     =   78
         BevelWidth      =   0
         RoundedCorners  =   0   'False
         Outline         =   0   'False
         Picture         =   "FrmKgrup.frx":1138
      End
      Begin VB.Label Label1 
         AutoSize        =   -1  'True
         Caption         =   "Inicial : "
         Height          =   195
         Left            =   840
         TabIndex        =   18
         Top             =   360
         Width           =   540
      End
      Begin VB.Label Label2 
         AutoSize        =   -1  'True
         Caption         =   "Final : "
         Height          =   195
         Left            =   840
         TabIndex        =   17
         Top             =   840
         Width           =   465
      End
   End
   Begin Crystal.CrystalReport Crys_Listar 
      Left            =   0
      Top             =   1920
      _ExtentX        =   741
      _ExtentY        =   741
      _Version        =   348160
      WindowLeft      =   20
      WindowWidth     =   750
      WindowHeight    =   450
      WindowControlBox=   -1  'True
      WindowMaxButton =   -1  'True
      WindowMinButton =   -1  'True
      WindowControls  =   -1  'True
      PrintFileLinesPerPage=   60
      WindowShowCloseBtn=   -1  'True
      WindowShowSearchBtn=   -1  'True
      WindowShowPrintSetupBtn=   -1  'True
      WindowShowRefreshBtn=   -1  'True
   End
   Begin Threed.SSCommand SCmd_Options 
      Height          =   735
      Index           =   1
      Left            =   1560
      TabIndex        =   23
      Top             =   5040
      Width           =   855
      _Version        =   65536
      _ExtentX        =   1508
      _ExtentY        =   1296
      _StockProps     =   78
      Caption         =   "&Imprimir"
      Picture         =   "FrmKgrup.frx":158A
   End
   Begin Threed.SSCommand SCmd_Options 
      Height          =   735
      Index           =   2
      Left            =   2640
      TabIndex        =   24
      Top             =   5040
      Width           =   855
      _Version        =   65536
      _ExtentX        =   1508
      _ExtentY        =   1296
      _StockProps     =   78
      Caption         =   "&Salir"
      Picture         =   "FrmKgrup.frx":1ACC
   End
   Begin Threed.SSCommand SCmd_Options 
      Height          =   735
      Index           =   0
      Left            =   480
      TabIndex        =   25
      Top             =   5040
      Width           =   855
      _Version        =   65536
      _ExtentX        =   1508
      _ExtentY        =   1296
      _StockProps     =   78
      Caption         =   "&Pantalla"
      Picture         =   "FrmKgrup.frx":21DA
   End
   Begin Threed.SSCommand SCmd_Options 
      Height          =   375
      Index           =   3
      Left            =   0
      TabIndex        =   26
      Top             =   0
      Visible         =   0   'False
      Width           =   975
      _Version        =   65536
      _ExtentX        =   1720
      _ExtentY        =   661
      _StockProps     =   78
   End
   Begin Threed.SSCommand SCmd_Options 
      Height          =   375
      Index           =   4
      Left            =   1080
      TabIndex        =   27
      Top             =   0
      Visible         =   0   'False
      Width           =   975
      _Version        =   65536
      _ExtentX        =   1720
      _ExtentY        =   661
      _StockProps     =   78
   End
   Begin VB.Label LblReg 
      Height          =   255
      Left            =   0
      TabIndex        =   28
      Top             =   4680
      Width           =   3975
   End
End
Attribute VB_Name = "FrmKgrup"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Dim Cantidad As Double
Dim CantiK As Double
Dim Costo As Double
Dim CostoK As Double

Dim CantDepe As Double
Dim CantDK As Double
Dim CostoD As Double
Dim CostoDK As Double

Dim Rstdatos As ADODB.Recordset
Dim Articulo As String

Private Sub Form_Load()
    Dim ElClnt As New ElTercero
    ElClnt.IniXNit
 Call CenterForm(MDI_Inventarios, Me)
' Call Leer_Permisos("03010", SCmd_Options(3), SCmd_Options(3), SCmd_Options(0))
 Crys_Listar.Formulas(0) = "ENTIDAD='" & ElClnt.Nombre & Comi
 Crys_Listar.Formulas(1) = "NIT= 'NIT. " & ElClnt.Nit & Comi
 Crys_Listar.Formulas(2) = "HORA= 'Hora: " & Format(Now, "hh:mm") & Comi
 Crys_Listar.Formulas(3) = "USUARIO = 'Usuario:" & UserId & Comi
' Crys_Listar.Connect = conCrys
 SCmd_Options(1).Enabled = SCmd_Options(0).Enabled
 MskFecha(0) = Hoy
 MskFecha(1) = Hoy
End Sub
Private Sub OptOpcion_Click(Index As Integer)
  TxtRango(2) = "0"
  TxtRango(3) = "ZZZZZZZZZ"
  If OptOpcion(1).Value = True Then
     FraDependencias.Enabled = True
  Else
     FraDependencias.Enabled = False
  End If
End Sub
Private Sub OptOpcion_KeyPress(Index As Integer, KeyAscii As Integer)
  If KeyAscii = 13 Then
     Call Cambiar_Enter(KeyAscii)
  End If
End Sub
Private Sub TxtRango_GotFocus(Index As Integer)
    TxtRango(Index).SelStart = 0
    TxtRango(Index).SelLength = Len(TxtRango(Index).Text)
End Sub
Private Sub TxtRango_KeyPress(Index As Integer, KeyAscii As Integer)
    Call ValKeyAlfaNum(KeyAscii)
    If KeyAscii = 13 Then
     If Index = 3 Then
      MskFecha(0).SetFocus
     Else
      If TxtRango(Index + 1).Enabled = True Then
       TxtRango(Index + 1).SetFocus
      Else
       MskFecha(0).SetFocus
      End If
     End If
    End If
End Sub
Private Sub TxtRango_KeyDown(Index As Integer, KeyCode As Integer, Shift As Integer)
 Select Case Index
    Case 0
           If KeyCode = 38 Then
              If OptOpcion(0).Value = True Then
                 OptOpcion(0).SetFocus
              End If
              If OptOpcion(1).Value = True Then
                 OptOpcion(1).SetFocus
              End If
              If OptOpcion(2).Value = True Then
                 OptOpcion(2).SetFocus
              End If
           End If
           If KeyCode = 40 Then
              TxtRango(1).SetFocus
           End If
    Case 1, 2
           If KeyCode = 38 Then
              TxtRango(Index - 1).SetFocus
           End If
           If KeyCode = 40 Then
             If TxtRango(Index + 1).Enabled = True Then
               TxtRango(Index + 1).SetFocus
             Else
               MskFecha(0).SetFocus
             End If
           End If
    Case 3
           If KeyCode = 38 Then
              TxtRango(Index - 1).SetFocus
           End If
           If KeyCode = 40 Then
              MskFecha(0).SetFocus
           End If
 End Select
End Sub
Private Sub MskFecha_GotFocus(Index As Integer)
    MskFecha(Index).SelStart = 0
    MskFecha(Index).SelLength = Len(MskFecha(Index).Text)
End Sub
Private Sub MskFecha_KeyPress(Index As Integer, KeyAscii As Integer)
    Call ValKeyFecha(KeyAscii, MskFecha(Index))
    Call Cambiar_Enter(KeyAscii)
End Sub
Private Sub MskFecha_LostFocus(Index As Integer)
   Call ValFecha(MskFecha(Index), 0)
End Sub
Private Sub MskFecha_KeyDown(Index As Integer, KeyCode As Integer, Shift As Integer)
 If Index = 0 Then
   If KeyCode = 38 Then
    If TxtRango(3).Enabled = True Then
      TxtRango(3).SetFocus
    Else
      TxtRango(1).SetFocus
    End If
   End If
   If KeyCode = 40 Then
      MskFecha(1).SetFocus
   End If
 Else
   If KeyCode = 38 Then
      MskFecha(0).SetFocus
   End If
   If KeyCode = 40 Then
      SCmd_Options(0).SetFocus
   End If
 End If
End Sub
Private Sub CmdSelec_Click(Index As Integer)
    Codigo = NUL$
    Select Case Index
        Case 0, 1: Codigo = Seleccion("GRUP_ARTICULO", "DE_DESC_GRUP", "CD_CODI_GRUP,DE_DESC_GRUP", "GRUPOS DE ARTICULOS", NUL$)
        Case 2, 3: Codigo = Seleccion("CENTRO_COSTO", "NO_NOMB_CECO", "CD_CODI_CECO,NO_NOMB_CECO", "DEPENDENCIAS", NUL$)
    End Select
    If Codigo <> NUL$ Then TxtRango(Index) = Codigo
    TxtRango(Index).SetFocus
End Sub
Private Sub SCmd_Options_Click(Index As Integer)
    Screen.MousePointer = 11
    Select Case Index
        '''True = pantalla
        '''False = impresora
        Case 0: Call Informe(True)
        Case 1: Call Informe(False)
        Case 2: Unload Me
    End Select
    Screen.MousePointer = 0
End Sub
Private Sub Informe(ByVal pantalla As Boolean)
Dim ArrTemp() As Variant
  Dim algo
  If TxtRango(0) > TxtRango(1) Or TxtRango(1) = "" Then
     Call Mensaje1("Revise rango de grupos", 3)
     Exit Sub
  End If
  If TxtRango(2) > TxtRango(3) Or TxtRango(3) = "" Then
     Call Mensaje1("Revise rango de dependencias", 3)
     Exit Sub
  End If
  algo = DateDiff("d", MskFecha(0), MskFecha(1))
  If CLng(algo) < 0 Then
     Call Mensaje1("Fecha Inicial Mayor que Fecha Final", 3)
     Exit Sub
  End If
  'validar que el mes de donde se van a tomar los saldos iniciales este cerrado
  ReDim ArrTemp(0)
  Result = LoadData("PARAMETROS_INVE", "FE_CIER_APLI", NUL$, ArrTemp())
  If Result <> FAIL And Encontro Then
     If (CLng(Format(MskFecha(0), "yyyymm")) - 1) > CLng(Format(ArrTemp(0), "yyyymm")) Then
        If CLng(Format(MskFecha(0), "mm")) - 1 > 0 Then
           Call Mensaje1("No se puede generar el listado porque " & MonthName(CLng(Format(MskFecha(0), "mm")) - 1) & " de " & Format(MskFecha(0), "yyyy") & " no se ha cerrado", 1)
        Else
           Call Mensaje1("No se puede generar el listado porque Diciembre " & " de " & CLng(Format(MskFecha(0), "yyyy")) - 1 & " no se ha cerrado", 1)
        End If
        Call MouseNorm
        Exit Sub
     End If
  Else
     Call Mensaje1("No se pudo obtener la fecha de cierre", 1)
     Call MouseNorm
     Exit Sub
  End If
  ''Env�a rango de fechas al informe
  Crys_Listar.Formulas(4) = "FECHAI= '" & MskFecha(0) & Comi
  Crys_Listar.Formulas(5) = "FECHAF= '" & MskFecha(1) & Comi
  ''''''''''''''''''''''''''''''''''
  If OptOpcion(0).Value = True Then
    Crys_Listar.WindowTitle = "K�rdex Almac�n por Grupos"
    Crys_Listar.ReportFileName = DirTrab + "kgalma.RPT"
  End If
  If OptOpcion(1).Value = True Then
    Crys_Listar.WindowTitle = "K�rdex Dependencias por Grupos"
    Crys_Listar.ReportFileName = DirTrab + "kgdepe.RPT"
  End If
  If OptOpcion(2).Value = True Then
    Crys_Listar.WindowTitle = "K�rdex Consolidado por Grupos"
    Crys_Listar.ReportFileName = DirTrab + "kgconso.RPT"
  End If
  If FraDependencias.Enabled = True Then
    Crys_Listar.SelectionFormula = "{GRUP_ARTICULO.CD_CODI_GRUP} in '" & TxtRango(0) & "' to '" & TxtRango(1) & Comi & _
    " and {KARDEX_ARTI.CD_DEPE_KARD} in '" & TxtRango(2) & "' to '" & TxtRango(3) & Comi
  Else
    Crys_Listar.SelectionFormula = "{GRUP_ARTICULO.CD_CODI_GRUP} in '" & TxtRango(0) & "' to '" & TxtRango(1) & Comi
  End If
  If SCmd_Options(4).Enabled = False Then
     Call Mensaje1("Permiso Denegado", 3)
  Else
     Crea_Temporales ''Crea temporal para impresi�n
     If pantalla = True Then
        Crys_Listar.Destination = crptToWindow
     Else
        Crys_Listar.Destination = crptToPrinter
     End If
     Crys_Listar.Action = 1
  End If
End Sub
''Crea temporal para informaci�n del k�rdex x Grupos
Private Sub Crea_Temporales()
ReDim Arr(2) 'Para manejar la tabla de saldos almac�n
ReDim AD(0)
ReDim Arr2(3) 'Para manejar la tabla de saldos dependencias
Dim Fuc As String
  
  Valores = " CT_CANT_SALD=0, VL_COST_SALD=0"
  Result = DoUpdate("SAL_ANT", Valores, NUL$)
  Result = DoUpdate("SAL_ANT_DEP", Valores, NUL$)
  
  Fuc = "01/" & Format(MskFecha(0), "mm") & "/" & Format(MskFecha(0), "yyyy")
  Fuc = DateAdd("d", -1, Fuc)
  
  If OptOpcion(2).Value = False Then
     Condicion = "ID_ESTA_KARD <> " & "2"
'     condicion = condicion & " AND FE_FECH_KARD < " & fecha(MskFecha(0))
'     condicion = condicion & " AND FE_FECH_KARD >= " & fecha(MskFecha(0))
'     condicion = condicion & " AND FE_FECH_KARD <= " & fecha(MskFecha(1))
     If OptOpcion(0).Value = True Then
        Condicion = Condicion & " AND CD_ORDO_KARD <> " & "2"
        Condicion = Condicion & " AND CD_ORDO_KARD <> " & "4"
        Condicion = Condicion & " AND CD_ORDO_KARD <> " & "5"
     End If
     If OptOpcion(1).Value = True Then
        Condicion = Condicion & " AND CD_ORDO_KARD <> " & "1"
        Condicion = Condicion & " AND CD_ORDO_KARD <> " & "7"
        Condicion = Condicion & " AND CD_DEPE_KARD >= " & Comi & TxtRango(2) & Comi
        Condicion = Condicion & " AND CD_DEPE_KARD <= " & Comi & TxtRango(3) & Comi
     End If
     Condicion = Condicion & " AND CD_ARTI_KARD = CD_CODI_ARTI"
     Condicion = Condicion & " AND CD_GRUP_ARTI >= " & Comi & TxtRango(0) & Comi
     Condicion = Condicion & " AND CD_GRUP_ARTI <= " & Comi & TxtRango(1) & Comi
     Set Rstdatos = New ADODB.Recordset
     If OptOpcion(0).Value = True Then Campos = "DISTINCT CD_ARTI_KARD": Condicion = Condicion & " ORDER BY CD_ARTI_KARD"
     If OptOpcion(1).Value = True Then Campos = "DISTINCT CD_ARTI_KARD, CD_DEPE_KARD": Condicion = Condicion & " ORDER BY CD_ARTI_KARD, CD_DEPE_KARD"
     
     Call SelectRST("KARDEX_ARTI, ARTICULO", Campos, Condicion, Rstdatos)
     If (Result <> False) Then
       If Not Rstdatos.EOF Then
          Rstdatos.MoveFirst
          Do While Not Rstdatos.EOF
             'SALDO ANTERIOR ALMACEN
             If OptOpcion(0).Value = True Then
                LblReg = "Leyendo: " & Rstdatos.Fields(0)
                DoEvents
                Cantidad = Saldo_Inicial(Rstdatos.Fields(0), Fuc, Costo)
                CantiK = Saldo_Kardex(Rstdatos.Fields(0), MskFecha(0), CostoK)
                        
                Cantidad = Cantidad + CantiK
                Costo = Costo + CostoK
                
                If Cantidad = 0 Then Costo = 0
            
                Valores = " CT_CANT_SALD=" & Cantidad & Coma
                Valores = Valores & " VL_COST_SALD=" & Format(Costo, "############0.##00")
                
                Condicion = "CD_ARTI_SALD = '" & Rstdatos.Fields(0) & Comi
                
                Result = DoUpdate("SAL_ANT", Valores, Condicion)
             End If
             'SALDO ANTERIOR DEPENDENCIAS
             If OptOpcion(1).Value = True Then
                LblReg = "Leyendo: " & Rstdatos.Fields(0) & " Depend. " & Rstdatos.Fields(1)
                DoEvents

                Cantidad = Saldo_Inicial_Dep(Rstdatos.Fields(0), Rstdatos.Fields(1), Fuc, Costo)
                CantiK = Saldo_Kardex_Dep(Rstdatos.Fields(0), Rstdatos.Fields(1), MskFecha(0), CostoK)
                  
                Cantidad = Cantidad + CantiK
                Costo = Costo + CostoK
                
                Valores = " CT_CANT_SALD=" & Cantidad & Coma
                Valores = Valores & " VL_COST_SALD=" & Format(Costo, "############0.##00")
                
                Condicion = "CD_ARTI_SALD = '" & Rstdatos.Fields(0) & Comi
                Condicion = Condicion & " AND CD_DEPE_SALD = '" & Rstdatos.Fields(1) & Comi
                Result = LoadData("SAL_ANT_DEP", "CD_DEPE_SALD", Condicion, AD())
                If AD(0) = NUL$ Then
                   Valores = "CD_DEPE_SALD=" & Comi & Rstdatos.Fields(1) & Comi & Coma & _
                             "CD_ARTI_SALD=" & Comi & Rstdatos.Fields(0) & Comi & Coma & _
                             "CT_CANT_SALD=" & Cantidad & Coma & _
                             "VL_COST_SALD=" & Format(Costo, "###0.##00")
                   Result = DoInsertSQL("SAL_ANT_DEP", Valores)
                Else
                   Result = DoUpdate("SAL_ANT_DEP", Valores, Condicion)
                End If
                       
             End If
             Rstdatos.MoveNext
        Loop
      End If
    End If
    Rstdatos.Close
  Else
    'Saldo almac�n
    Condicion = "ID_ESTA_KARD <> " & "2"
'    condicion = condicion & " And FE_FECH_KARD < " & fecha(MskFecha(0))
'    condicion = condicion & " AND FE_FECH_KARD >= " & fecha(MskFecha(0))
'     condicion = condicion & " AND FE_FECH_KARD <= " & fecha(MskFecha(1))
   ' condicion = condicion & " And CD_ORDO_KARD <> " & "2"
   ' condicion = condicion & " And CD_ORDO_KARD <> " & "3"
   ' condicion = condicion & " And CD_ORDO_KARD <> " & "5"
   ' condicion = condicion & " And CD_ORDO_KARD <> " & "6"
    Condicion = Condicion & " AND CD_ARTI_KARD = CD_CODI_ARTI"
    Condicion = Condicion & " AND CD_GRUP_ARTI >= " & Comi & TxtRango(0) & Comi
    Condicion = Condicion & " AND CD_GRUP_ARTI <= " & Comi & TxtRango(1) & Comi
    Condicion = Condicion & " ORDER BY CD_ARTI_KARD, CD_DEPE_KARD"

    Set Rstdatos = New ADODB.Recordset
    Call SelectRST("KARDEX_ARTI, ARTICULO", "DISTINCT CD_ARTI_KARD, CD_DEPE_KARD", Condicion, Rstdatos)
    If (Result <> False) Then
      If Not Rstdatos.EOF Then
        Articulo = NUL$
        Rstdatos.MoveFirst
        Do While Not Rstdatos.EOF
           LblReg = "Leyendo: " & Rstdatos.Fields(0)
           DoEvents
           If Articulo <> Rstdatos.Fields(0) Then
              'leer saldo en almacen
              If Articulo <> NUL$ Then
                 Condicion = "CD_ARTI_SALD = '" & Articulo & Comi
                 Valores = " CT_CANT_SALD=" & Cantidad & Coma
                 Valores = Valores & " VL_COST_SALD=" & Format(Costo, "############0.##00")
           
                 Result = DoUpdate("SAL_ANT", Valores, Condicion)
              End If
              
              Cantidad = Saldo_Inicial(Rstdatos.Fields(0), Fuc, Costo)
              CantiK = Saldo_Kardex(Rstdatos.Fields(0), MskFecha(0), CostoK)
                                    
              Cantidad = Cantidad + CantiK
              Costo = Costo + CostoK
              
              Articulo = Rstdatos.Fields(0)
           End If
           
           CantDepe = Saldo_Inicial_Dep(Rstdatos.Fields(0), Rstdatos.Fields(1), Fuc, CostoD)
           CantDK = Saldo_Kardex_Dep(Rstdatos.Fields(0), Rstdatos.Fields(1), MskFecha(0), CostoDK)
                  
           Cantidad = Cantidad + CantDepe + CantDK
           Costo = Costo + CostoD + CostoDK
           
           Rstdatos.MoveNext
        Loop
        Condicion = "CD_ARTI_SALD = '" & Articulo & Comi
        Valores = " CT_CANT_SALD=" & Cantidad & Coma
        Valores = Valores & " VL_COST_SALD=" & Format(Costo, "############0.##00")
        
        Result = DoUpdate("SAL_ANT", Valores, Condicion)
      End If
    End If
    Rstdatos.Close
  End If
End Sub
