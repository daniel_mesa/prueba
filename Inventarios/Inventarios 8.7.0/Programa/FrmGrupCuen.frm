VERSION 5.00
Object = "{0BA686C6-F7D3-101A-993E-0000C0EF6F5E}#1.0#0"; "THREED32.OCX"
Begin VB.Form FrmGrupCuen 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Cuentas Contables por Grupo y Dependencia"
   ClientHeight    =   5625
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   6480
   Icon            =   "FrmGrupCuen.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   ScaleHeight     =   5625
   ScaleWidth      =   6480
   Begin VB.ComboBox CboTipoEmp 
      Height          =   315
      Left            =   1320
      TabIndex        =   5
      Top             =   1080
      Width           =   3495
   End
   Begin VB.TextBox Txt_Cuenta 
      Height          =   285
      Index           =   5
      Left            =   1200
      MaxLength       =   15
      TabIndex        =   30
      Top             =   3960
      Width           =   1455
   End
   Begin VB.TextBox Txt_Cuenta 
      Height          =   285
      Index           =   4
      Left            =   1200
      MaxLength       =   15
      TabIndex        =   26
      Top             =   3480
      Width           =   1455
   End
   Begin VB.TextBox Txt_Cuenta 
      Height          =   285
      Index           =   3
      Left            =   1200
      MaxLength       =   15
      TabIndex        =   12
      Top             =   3000
      Width           =   1455
   End
   Begin VB.Frame Frame2 
      Height          =   1095
      Left            =   1320
      TabIndex        =   19
      Top             =   4440
      Width           =   3975
      Begin Threed.SSCommand SCmd_Options 
         Height          =   735
         Index           =   0
         Left            =   120
         TabIndex        =   20
         Top             =   240
         Width           =   855
         _Version        =   65536
         _ExtentX        =   1508
         _ExtentY        =   1296
         _StockProps     =   78
         Caption         =   "&GUARDAR"
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   6
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Picture         =   "FrmGrupCuen.frx":058A
      End
      Begin Threed.SSCommand SCmd_Options 
         Height          =   735
         Index           =   1
         Left            =   1080
         TabIndex        =   21
         Top             =   240
         Width           =   855
         _Version        =   65536
         _ExtentX        =   1508
         _ExtentY        =   1296
         _StockProps     =   78
         Caption         =   "&BORRAR"
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   6
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Picture         =   "FrmGrupCuen.frx":0C54
      End
      Begin Threed.SSCommand SCmd_Options 
         Height          =   735
         Index           =   3
         Left            =   2040
         TabIndex        =   22
         Top             =   240
         Width           =   855
         _Version        =   65536
         _ExtentX        =   1508
         _ExtentY        =   1296
         _StockProps     =   78
         Caption         =   "&LISTAR"
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   6
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Picture         =   "FrmGrupCuen.frx":131E
      End
      Begin Threed.SSCommand SCmd_Options 
         Height          =   735
         Index           =   4
         Left            =   3000
         TabIndex        =   23
         Top             =   240
         Width           =   855
         _Version        =   65536
         _ExtentX        =   1508
         _ExtentY        =   1296
         _StockProps     =   78
         Caption         =   "&SALIR"
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   6
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Picture         =   "FrmGrupCuen.frx":19E8
      End
   End
   Begin VB.ComboBox Cbo_Ceco 
      Height          =   315
      Left            =   1320
      TabIndex        =   3
      Top             =   600
      Width           =   5055
   End
   Begin VB.ComboBox Cbo_Grupo 
      Height          =   315
      Left            =   1320
      TabIndex        =   1
      Top             =   120
      Width           =   5055
   End
   Begin VB.TextBox Txt_Cuenta 
      Height          =   285
      Index           =   0
      Left            =   1200
      MaxLength       =   15
      TabIndex        =   6
      Top             =   1560
      Width           =   1455
   End
   Begin VB.TextBox Txt_Cuenta 
      Height          =   285
      Index           =   1
      Left            =   1200
      MaxLength       =   15
      TabIndex        =   8
      Top             =   2040
      Width           =   1455
   End
   Begin VB.TextBox Txt_Cuenta 
      Height          =   285
      Index           =   2
      Left            =   1200
      MaxLength       =   15
      TabIndex        =   10
      Top             =   2520
      Width           =   1455
   End
   Begin Threed.SSCommand CmdSelec 
      Height          =   375
      Index           =   0
      Left            =   2640
      TabIndex        =   13
      ToolTipText     =   "Selecci�n de Terceros"
      Top             =   1560
      Width           =   615
      _Version        =   65536
      _ExtentX        =   1085
      _ExtentY        =   661
      _StockProps     =   78
      BevelWidth      =   0
      RoundedCorners  =   0   'False
      Outline         =   0   'False
      Picture         =   "FrmGrupCuen.frx":20B2
   End
   Begin Threed.SSCommand CmdSelec 
      Height          =   375
      Index           =   1
      Left            =   2640
      TabIndex        =   14
      ToolTipText     =   "Selecci�n de Terceros"
      Top             =   2040
      Width           =   615
      _Version        =   65536
      _ExtentX        =   1085
      _ExtentY        =   661
      _StockProps     =   78
      BevelWidth      =   0
      RoundedCorners  =   0   'False
      Outline         =   0   'False
      Picture         =   "FrmGrupCuen.frx":2A64
   End
   Begin Threed.SSCommand CmdSelec 
      Height          =   375
      Index           =   2
      Left            =   2640
      TabIndex        =   15
      ToolTipText     =   "Selecci�n de Terceros"
      Top             =   2520
      Width           =   615
      _Version        =   65536
      _ExtentX        =   1085
      _ExtentY        =   661
      _StockProps     =   78
      BevelWidth      =   0
      RoundedCorners  =   0   'False
      Outline         =   0   'False
      Picture         =   "FrmGrupCuen.frx":3416
   End
   Begin Threed.SSCommand CmdSelec 
      Height          =   375
      Index           =   3
      Left            =   2640
      TabIndex        =   24
      ToolTipText     =   "Selecci�n de Terceros"
      Top             =   3000
      Width           =   615
      _Version        =   65536
      _ExtentX        =   1085
      _ExtentY        =   661
      _StockProps     =   78
      BevelWidth      =   0
      RoundedCorners  =   0   'False
      Outline         =   0   'False
      Picture         =   "FrmGrupCuen.frx":3DC8
   End
   Begin Threed.SSCommand CmdSelec 
      Height          =   375
      Index           =   4
      Left            =   2640
      TabIndex        =   27
      ToolTipText     =   "Selecci�n de Terceros"
      Top             =   3480
      Width           =   615
      _Version        =   65536
      _ExtentX        =   1085
      _ExtentY        =   661
      _StockProps     =   78
      BevelWidth      =   0
      RoundedCorners  =   0   'False
      Outline         =   0   'False
      Picture         =   "FrmGrupCuen.frx":477A
   End
   Begin Threed.SSCommand CmdSelec 
      Height          =   375
      Index           =   5
      Left            =   2640
      TabIndex        =   31
      ToolTipText     =   "Selecci�n de Terceros"
      Top             =   3960
      Width           =   615
      _Version        =   65536
      _ExtentX        =   1085
      _ExtentY        =   661
      _StockProps     =   78
      BevelWidth      =   0
      RoundedCorners  =   0   'False
      Outline         =   0   'False
      Picture         =   "FrmGrupCuen.frx":512C
   End
   Begin VB.Label Label7 
      Caption         =   "Tipo Empresa"
      Height          =   255
      Left            =   120
      TabIndex        =   34
      Top             =   1080
      Width           =   975
   End
   Begin VB.Label Label1 
      Alignment       =   1  'Right Justify
      Caption         =   "Ajuste X Inflaci�n CR"
      Height          =   375
      Index           =   5
      Left            =   0
      TabIndex        =   33
      Top             =   3960
      Width           =   1095
   End
   Begin VB.Label lbl_cuenta 
      BackColor       =   &H8000000E&
      BorderStyle     =   1  'Fixed Single
      Height          =   285
      Index           =   5
      Left            =   3240
      TabIndex        =   32
      Top             =   3960
      Width           =   3135
   End
   Begin VB.Label Label1 
      Alignment       =   1  'Right Justify
      Caption         =   "Ajuste X Inflaci�n DB"
      Height          =   375
      Index           =   4
      Left            =   0
      TabIndex        =   29
      Top             =   3480
      Width           =   1095
   End
   Begin VB.Label lbl_cuenta 
      BackColor       =   &H8000000E&
      BorderStyle     =   1  'Fixed Single
      Height          =   285
      Index           =   4
      Left            =   3240
      TabIndex        =   28
      Top             =   3480
      Width           =   3135
   End
   Begin VB.Label lbl_cuenta 
      BackColor       =   &H8000000E&
      BorderStyle     =   1  'Fixed Single
      Height          =   285
      Index           =   3
      Left            =   3240
      TabIndex        =   25
      Top             =   3000
      Width           =   3135
   End
   Begin VB.Label Label1 
      Alignment       =   1  'Right Justify
      Caption         =   "Cuenta de Salida"
      Height          =   375
      Index           =   3
      Left            =   240
      TabIndex        =   11
      Top             =   3000
      Width           =   855
   End
   Begin VB.Label LblCodigo 
      Alignment       =   1  'Right Justify
      Caption         =   "&Depedencia:"
      Height          =   255
      Index           =   1
      Left            =   120
      TabIndex        =   2
      Top             =   600
      Width           =   975
   End
   Begin VB.Label Label1 
      Alignment       =   1  'Right Justify
      Caption         =   "Cuenta de ingreso"
      Height          =   375
      Index           =   0
      Left            =   240
      TabIndex        =   4
      Top             =   1560
      Width           =   855
   End
   Begin VB.Label lbl_cuenta 
      BackColor       =   &H8000000E&
      BorderStyle     =   1  'Fixed Single
      Height          =   285
      Index           =   0
      Left            =   3240
      TabIndex        =   18
      Top             =   1560
      Width           =   3135
   End
   Begin VB.Label Label1 
      Alignment       =   1  'Right Justify
      Caption         =   "Cuenta de Gasto"
      Height          =   375
      Index           =   1
      Left            =   240
      TabIndex        =   7
      Top             =   2040
      Width           =   855
   End
   Begin VB.Label lbl_cuenta 
      BackColor       =   &H8000000E&
      BorderStyle     =   1  'Fixed Single
      Height          =   285
      Index           =   1
      Left            =   3240
      TabIndex        =   17
      Top             =   2040
      Width           =   3135
   End
   Begin VB.Label Label1 
      Alignment       =   1  'Right Justify
      Caption         =   "Cuenta de Costo"
      Height          =   375
      Index           =   2
      Left            =   240
      TabIndex        =   9
      Top             =   2520
      Width           =   855
   End
   Begin VB.Label lbl_cuenta 
      BackColor       =   &H8000000E&
      BorderStyle     =   1  'Fixed Single
      Height          =   285
      Index           =   2
      Left            =   3240
      TabIndex        =   16
      Top             =   2520
      Width           =   3135
   End
   Begin VB.Label LblCodigo 
      Alignment       =   1  'Right Justify
      Caption         =   "&Grupo:"
      Height          =   255
      Index           =   0
      Left            =   120
      TabIndex        =   0
      Top             =   120
      Width           =   615
   End
End
Attribute VB_Name = "FrmGrupCuen"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Dim Cod_Ceco(), Cod_Grupo() As Variant
Dim EncontroT As Boolean
Dim I As Byte
Dim Cod_TipoEmp() As Variant
Dim lblitfzpac(3) As String 'nombre de las cuentas con interfaz pacientes

Private Sub Cbo_Ceco_KeyPress(KeyAscii As Integer)
    Call Cambiar_Enter(KeyAscii)
End Sub

Private Sub Cbo_Ceco_LostFocus()
'    Call Limpiar
'    Call Leer_Grupo_Ceco
End Sub

Private Sub Cbo_Grupo_KeyPress(KeyAscii As Integer)
    Call Cambiar_Enter(KeyAscii)
End Sub

Private Sub Cbo_Grupo_LostFocus()
    Call Limpiar
    Cbo_Ceco.ListIndex = 0
End Sub

Private Sub CboTipoEmp_KeyPress(KeyAscii As Integer)
    Call Cambiar_Enter(KeyAscii)
End Sub

Private Sub CboTipoEmp_LostFocus()
    Call Limpiar
    Call Leer_Grupo_Ceco
End Sub

Private Sub CmdSelec_Click(Index As Integer)
   Codigo = NUL$
   'JLPB T29905/R28918
   'Codigo = Seleccion("CUENTAS", "NO_NOMB_CUEN", "CD_CODI_CUEN,NO_NOMB_CUEN", "CUENTAS CONTABLES", NUL$)
   Condicion = "TX_PUCNIIF_CUEN<>'1'"
   Codigo = Seleccion("CUENTAS", "NO_NOMB_CUEN", "CD_CODI_CUEN,NO_NOMB_CUEN", "CUENTAS CONTABLES", Condicion)
   'JLPB T29905/R28918
   If Codigo <> NUL$ Then Txt_Cuenta(Index) = Codigo
   Txt_Cuenta_LostFocus (Index)
   Txt_Cuenta(Index).SetFocus
End Sub

Private Sub Form_Load()
    
    Call CenterForm(MDI_Inventarios, Me)
    
    Result = loadctrl("GRUP_ARTICULO ORDER BY DE_DESC_GRUP", "DE_DESC_GRUP", "CD_CODI_GRUP", Cbo_Grupo, Cod_Grupo(), NUL$)
    If Result = FAIL Then
       Call Mensaje1("Error cargando tabla [GRUP_ARTICULO]", 2)
    End If
    If Cbo_Grupo.ListCount > 0 Then Cbo_Grupo.ListIndex = 0
    
    Result = loadctrl("CENTRO_COSTO ORDER BY NO_NOMB_CECO", "NO_NOMB_CECO", "CD_CODI_CECO", Cbo_Ceco, Cod_Ceco(), NUL$)
    If Result = FAIL Then
       Call Mensaje1("Error cargando tabla [CENTRO_COSTO]", 2)
    End If
    If Cbo_Ceco.ListCount > 0 Then Cbo_Ceco.ListIndex = 0
    
    Result = loadctrl("TIPO_EMPRESA ORDER BY DE_DESC_TIEMP", "DE_DESC_TIEMP", "CD_CODI_TIEMP", CboTipoEmp, Cod_TipoEmp(), NUL$)
    If Result = FAIL Then Call Mensaje1("Error cargando tabla [TIPO_EMPRESA]", 2)
    If CboTipoEmp.ListCount > 0 Then CboTipoEmp.ListIndex = 0
    
End Sub

Private Sub SCmd_Options_Click(Index As Integer)
    Select Case Index
        Case 0: Call Grabar
        Case 1: Call Borrar
        Case 3: Call Listar
        Case 4: Unload Me
    End Select
End Sub

Private Sub Txt_Cuenta_GotFocus(Index As Integer)
   Txt_Cuenta(Index).SelStart = 0
   Txt_Cuenta(Index).SelLength = Len(Txt_Cuenta(Index))
End Sub

Private Sub Txt_Cuenta_KeyPress(Index As Integer, KeyAscii As Integer)
    Call Cambiar_Enter(KeyAscii)
End Sub

Private Sub Txt_Cuenta_LostFocus(Index As Integer)
   Txt_Cuenta(Index) = Trim(Txt_Cuenta(Index))
   If Txt_Cuenta(Index) = NUL$ Then
      lbl_cuenta(Index) = NUL$
   Else
      Call Buscar_Cuenta(Index)
   End If
End Sub

'Private Sub Buscar_Cuenta(Index As Integer)
Private Function Buscar_Cuenta(Index As Integer) As Boolean 'JACC M5464
   ReDim arr(0)
   If Txt_Cuenta(Index) = NUL$ Then lbl_cuenta(Index) = NUL$: Exit Function
   'JLPB T29905/R28918 INICIO Se deja en comentario la siguiente linea
   'Result = LoadData("CUENTAS", "NO_NOMB_CUEN", "CD_CODI_CUEN=" & Comi & Txt_Cuenta(Index) & Comi, arr())
   Condicion = "CD_CODI_CUEN=" & Comi & Txt_Cuenta(Index) & Comi
   Condicion = Condicion & " AND TX_PUCNIIF_CUEN<>'1'"
   Result = LoadData("CUENTAS", "NO_NOMB_CUEN", Condicion, arr())
   'JLPB T29905/R28918 fin
   lbl_cuenta(Index) = arr(0)
   If arr(0) <> NUL$ Then
      If Valida_Nivel_Cuentas(Txt_Cuenta(Index)) Then
         Call Mensaje1("Verifique que la cuenta contable sea de �ltimo nivel!", 2)
         Txt_Cuenta(Index).SetFocus
      End If
   Else
      Buscar_Cuenta = True 'JACC M5464
      lbl_cuenta(Index).Caption = "No existe la cuenta"   ''JACC M5464
      Call Mensaje1("Cuenta contable no registrada !", 2)
      'Txt_Cuenta(Index).SetFocus 'JACC M5464 Se coloca en comentario
   End If
End Function

Private Sub Limpiar()
Dim I As Byte
    For I = 0 To 5
        Txt_Cuenta(I) = NUL$
        lbl_cuenta(I) = NUL$
    Next
End Sub

Private Sub Leer_Grupo_Ceco()
Dim arr(7)
    If Cbo_Grupo.ListIndex = -1 Then Exit Sub
    If Cbo_Ceco.ListIndex = -1 Then Exit Sub
    If CboTipoEmp.ListIndex = -1 Then Exit Sub  'JACC R2353
    
    Condicion = "CD_GRUP_RGD=" & Comi & Cod_Grupo(Cbo_Grupo.ListIndex) & Comi
    Condicion = Condicion & " AND CD_DEPE_RGD=" & Comi & Cod_Ceco(Cbo_Ceco.ListIndex) & Comi
    Condicion = Condicion & " AND CD_CODI_TIEMP_RGD=" & Comi & Cod_TipoEmp(Me.CboTipoEmp.ListIndex) & Comi  'Req 1337
        
    Result = LoadData("R_GRUP_DEPE", Asterisco, Condicion, arr())
    If arr(0) = NUL$ Then
       Call Limpiar
       EncontroT = False
    Else
        For I = 2 To 7
            Txt_Cuenta(I - 2) = arr(I)
            Call Buscar_Cuenta(I - 2)
        Next
        EncontroT = True
    End If
    lblitfzpac(0) = lbl_cuenta(0)
    lblitfzpac(1) = lbl_cuenta(2)
    lblitfzpac(2) = lbl_cuenta(3)
End Sub

Private Sub Grabar()

Dim GrupoArt As String 'HRR Issues869 grupo de articulo
Dim CentroCosto As String 'HRR Issues869 dependencia
Dim TipoEmp As String 'HRR Issues869 tipo de empresa

   ''JACC R2353
   If CboTipoEmp.ListIndex = -1 Then
        Call Mensaje1("Debe seleccionar el tipo de empresa", 2)
        Exit Sub
    End If
   'JACC R2353

    If Cbo_Grupo.ListIndex = -1 Then
        Call Mensaje1("Debe seleccionar un grupo de Art�culos", 2)
        Exit Sub
    End If
    If Cbo_Ceco.ListIndex = -1 Then
        Call Mensaje1("Debe seleccionar una Dependencia", 2)
        Exit Sub
    End If
    If lbl_cuenta(0) = NUL$ And lbl_cuenta(1) = NUL$ And lbl_cuenta(2) = NUL$ Then
        Call Mensaje1("No se especificaron Cuentas Contables", 2)
        Exit Sub
    End If
    Call MouseClock
    
       'HRR Issues869
        'If lbl_cuenta(0) <> lblitfzpac(0) Or lbl_cuenta(2) <> lblitfzpac(1) Or lbl_cuenta(3) <> lblitfzpac(2) Then HRR M982
        If (lbl_cuenta(0) = NUL$ And lblitfzpac(0) <> NUL$) Or (lbl_cuenta(2) = NUL$ And lblitfzpac(1) <> NUL$) Or (lbl_cuenta(3) = NUL$ And lblitfzpac(2) <> NUL$) Then 'HRR M982
           GrupoArt = CStr(Cod_Grupo(Cbo_Grupo.ListIndex))
           CentroCosto = CStr(Cod_Ceco(Cbo_Ceco.ListIndex))
           TipoEmp = CStr(Cod_TipoEmp(CboTipoEmp.ListIndex))
            
           If BuscarCargosSinFacCredito(GrupoArt, CentroCosto, TipoEmp) Then
              Call Mensaje1("No se puede borrar la cuenta porque tiene cargos sin factura credito en Pacientes", 2)
              Call MouseNorm
              Exit Sub
           End If
    
        End If
       'Issues869
    'JACC M5464
    For I = 0 To 5
      If Txt_Cuenta(I) <> NUL$ Then
         If Buscar_Cuenta(CInt(I)) Then
           Txt_Cuenta(I).SetFocus
           Call MouseNorm: Exit Sub
         End If
      End If
    Next
    'JACC M5464
    
    Msglin "Grabando Cuentas por Grupo y Dependencia"
    Encontro = EncontroT
    If (BeginTran(STranIUp & "R_GRUP_DEPE") <> FAIL) Then
       If (Not Encontro) Then
          Valores = "CD_GRUP_RGD=" & Comi & Cod_Grupo(Cbo_Grupo.ListIndex) & Comi & Coma
          Valores = Valores & "CD_DEPE_RGD=" & Comi & Cod_Ceco(Cbo_Ceco.ListIndex) & Comi & Coma
          Valores = Valores & "CD_INGR_RGD=" & Comi & Cambiar_Comas_Comillas(Txt_Cuenta(0)) & Comi & Coma
          Valores = Valores & "CD_GAST_RGD=" & Comi & Cambiar_Comas_Comillas(Txt_Cuenta(1)) & Comi & Coma
          Valores = Valores & "CD_COST_RGD=" & Comi & Cambiar_Comas_Comillas(Txt_Cuenta(2)) & Comi & Coma
          Valores = Valores & "CD_SALI_RGD=" & Comi & Cambiar_Comas_Comillas(Txt_Cuenta(3)) & Comi & Coma
          Valores = Valores & "CD_AJDB_RGD=" & Comi & Cambiar_Comas_Comillas(Txt_Cuenta(4)) & Comi & Coma
          Valores = Valores & "CD_AJCR_RGD=" & Comi & Cambiar_Comas_Comillas(Txt_Cuenta(5)) & Comi
          Valores = Valores & Coma & "CD_CODI_TIEMP_RGD=" & Comi & Cod_TipoEmp(CboTipoEmp.ListIndex) & Comi    'Req 1337
          
          Result = DoInsertSQL("R_GRUP_DEPE", Valores)
          If (Result <> FAIL) Then Result = Auditor("R_GRUP_DEPE", TranIns, LastCmd)
       Else
         If WarnMsg("Desea Modificar la Informaci�n de las cuentas ", 256) Then
            Valores = "CD_INGR_RGD=" & Comi & Cambiar_Comas_Comillas(Txt_Cuenta(0)) & Comi & Coma
            Valores = Valores & "CD_GAST_RGD=" & Comi & Cambiar_Comas_Comillas(Txt_Cuenta(1)) & Comi & Coma
            Valores = Valores & "CD_COST_RGD=" & Comi & Cambiar_Comas_Comillas(Txt_Cuenta(2)) & Comi & Coma
            Valores = Valores & "CD_SALI_RGD=" & Comi & Cambiar_Comas_Comillas(Txt_Cuenta(3)) & Comi & Coma
            Valores = Valores & "CD_AJDB_RGD=" & Comi & Cambiar_Comas_Comillas(Txt_Cuenta(4)) & Comi & Coma
            Valores = Valores & "CD_AJCR_RGD=" & Comi & Cambiar_Comas_Comillas(Txt_Cuenta(5)) & Comi
            Valores = Valores & Coma & "CD_CODI_TIEMP_RGD=" & Comi & Cod_TipoEmp(CboTipoEmp.ListIndex) & Comi    'Req 1337
          
            Condicion = "CD_GRUP_RGD=" & Comi & Cod_Grupo(Cbo_Grupo.ListIndex) & Comi & " AND "
            Condicion = Condicion & "CD_DEPE_RGD=" & Comi & Cod_Ceco(Cbo_Ceco.ListIndex) & Comi
            Condicion = Condicion & " AND CD_CODI_TIEMP_RGD=" & Comi & Cod_TipoEmp(CboTipoEmp.ListIndex) & Comi     'Req 1337
            
            Result = DoUpdate("R_GRUP_DEPE", Valores, Condicion)
            If (Result <> FAIL) Then Result = Auditor("R_GRUP_DEPE", TranUpd, LastCmd)
         End If
       End If
   End If
   If (Result <> FAIL) Then
      If (CommitTran() <> FAIL) Then
          Limpiar
          Cbo_Grupo.SetFocus
      Else
         Call RollBackTran
      End If
   Else
      Call RollBackTran
   End If
   Call MouseNorm
   Msglin NUL$
End Sub

Private Sub Borrar()

Dim GrupoArt As String 'HRR Issues869 grupo de articulo
Dim CentroCosto As String 'HRR Issues869 dependencia
Dim TipoEmp As String 'HRR Issues869 tipo de empresa

    If Cbo_Grupo.ListIndex = -1 Then
        Call Mensaje1("Debe seleccionar un grupo de Art�culos", 2)
        Exit Sub
    End If
    If Cbo_Ceco.ListIndex = -1 Then
        Call Mensaje1("Debe seleccionar una Dependencia", 2)
        Exit Sub
    End If
    Call MouseClock
    Msglin "Borrando Cuentas por Grupo y Dependencia"
    Encontro = EncontroT
    
    If (Encontro <> False) Then
       If (Not WarnDel()) Then Call MouseNorm: Msglin NUL$: Exit Sub
       
       'HRR Issues869
     'If lbl_cuenta(0) <> lblitfzpac(0) Or lbl_cuenta(2) <> lblitfzpac(1) Or lbl_cuenta(3) <> lblitfzpac(2) Then HRR M982
           GrupoArt = CStr(Cod_Grupo(Cbo_Grupo.ListIndex))
           CentroCosto = CStr(Cod_Ceco(Cbo_Ceco.ListIndex))
           TipoEmp = CStr(Cod_TipoEmp(CboTipoEmp.ListIndex))
            
           If BuscarCargosSinFacCredito(GrupoArt, CentroCosto, TipoEmp) Then
              Call Mensaje1("No se puede borrar la cuenta porque tiene cargos sin factura credito en Pacientes", 2)
              Call MouseNorm
              Exit Sub
           End If
    
     ' End If
     'Issues869
       
       Condicion = "CD_GRUP_RGD=" & Comi & Cod_Grupo(Cbo_Grupo.ListIndex) & Comi & " AND "
       Condicion = Condicion & "CD_DEPE_RGD=" & Comi & Cod_Ceco(Cbo_Ceco.ListIndex) & Comi
   
       If (BeginTran(STranDel & "R_GRUP_DEPE") <> FAIL) Then
           Result = DoDelete("R_GRUP_DEPE", Condicion)
           If (Result <> FAIL) Then Result = Auditor("R_GRUP_DEPE", TranDel, LastCmd)
       End If
   Else
       Call Mensaje1("No se pudo borrar el registro de la base de datos, debido a que no existe !!!...", 1)
       Call MouseNorm: Msglin NUL$: Exit Sub
   End If
   If (Result <> FAIL) Then
      If (CommitTran() <> FAIL) Then
          Limpiar
          Cbo_Grupo.SetFocus
      Else
         Call RollBackTran
      End If
   Else
      Call RollBackTran
   End If
   Call MouseNorm
   Msglin NUL$
End Sub

Private Sub Listar()

    Dim ElCltn As New ElTercero
    On Error GoTo control
    ElCltn.IniXNit
    With MDI_Inventarios
'        .Crys_Listar.Connect = conCrys
        .Crys_Listar.Formulas(0) = "ENTIDAD='" & ElCltn.Nombre & Comi
        .Crys_Listar.Formulas(1) = "NIT= 'NIT. " & ElCltn.Nombre & Comi
        .Crys_Listar.Formulas(2) = "HORA= 'Hora: " & Format(Now, "hh:mm") & Comi
        .Crys_Listar.Formulas(3) = "USUARIO = 'Usuario:" & UserId & Comi
        .Crys_Listar.ReportFileName = DirRpt & "InfCuentasContXGrDep.rpt"
        .Crys_Listar.Destination = crptToWindow
        .Crys_Listar.WindowTitle = "Informe de Cuentas Contables por Grupo y Dependencia."
        .Crys_Listar.Action = 1
    End With
    Exit Sub
    
control:
    Call Mensaje1(ERR.Number & ": " & ERR.Description & "  Nombre del Reporte : InfCuentasContXGrDep.rpt", 1)
End Sub
