VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.1#0"; "Mscomctl.ocx"
Object = "{C932BA88-4374-101B-A56C-00AA003668DC}#1.1#0"; "MSMASK32.OCX"
Object = "{0BA686C6-F7D3-101A-993E-0000C0EF6F5E}#1.0#0"; "THREED32.OCX"
Begin VB.Form frmPrmREPOSAL 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Parametros para SALDOS"
   ClientHeight    =   7095
   ClientLeft      =   45
   ClientTop       =   435
   ClientWidth     =   11475
   Icon            =   "frmPrmREPOSAL.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   7095
   ScaleWidth      =   11475
   Begin VB.CheckBox ChkDetalle 
      Caption         =   "Detallado por Art�culo"
      Height          =   375
      Left            =   3240
      TabIndex        =   17
      Top             =   600
      Width           =   2895
   End
   Begin VB.CheckBox chkARTICULOS 
      Caption         =   "Todos Los Art�culos"
      Height          =   375
      Left            =   600
      TabIndex        =   15
      Top             =   600
      Width           =   2895
   End
   Begin VB.Frame fraRANGO 
      Height          =   2175
      Left            =   6360
      TabIndex        =   5
      Top             =   4800
      Width           =   4815
      Begin VB.CheckBox ChkExistencia 
         Caption         =   "Mostrar articulo con existencia cero"
         Height          =   255
         Left            =   360
         TabIndex        =   16
         Top             =   1800
         Width           =   3015
      End
      Begin VB.Frame Frame1 
         Height          =   615
         Left            =   240
         TabIndex        =   12
         Top             =   1080
         Width           =   3225
         Begin VB.OptionButton opcSIN 
            Caption         =   "Sin Costear"
            Height          =   315
            Left            =   1800
            TabIndex        =   11
            Top             =   200
            Width           =   1300
         End
         Begin VB.OptionButton opcCON 
            Caption         =   "Costeado"
            Height          =   315
            Left            =   240
            TabIndex        =   10
            Top             =   200
            Value           =   -1  'True
            Width           =   1300
         End
      End
      Begin VB.CheckBox chkFCRANGO 
         Caption         =   "Condicionar las fechas del reporte"
         Height          =   315
         Left            =   360
         TabIndex        =   7
         Top             =   240
         Width           =   4095
      End
      Begin MSMask.MaskEdBox txtFCHASTA 
         Height          =   315
         Left            =   3240
         TabIndex        =   9
         Top             =   600
         Width           =   1215
         _ExtentX        =   2143
         _ExtentY        =   556
         _Version        =   393216
         PromptChar      =   "_"
      End
      Begin Threed.SSCommand cmdIMPRIMIR 
         Height          =   735
         Index           =   2
         Left            =   3600
         TabIndex        =   13
         Top             =   1200
         Width           =   855
         _Version        =   65536
         _ExtentX        =   1508
         _ExtentY        =   1296
         _StockProps     =   78
         Caption         =   "&IMPRIMIR"
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   6
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         RoundedCorners  =   0   'False
         Picture         =   "frmPrmREPOSAL.frx":058A
      End
      Begin MSMask.MaskEdBox txtFCDESDE 
         Height          =   315
         Left            =   960
         TabIndex        =   8
         Top             =   600
         Visible         =   0   'False
         Width           =   1215
         _ExtentX        =   2143
         _ExtentY        =   556
         _Version        =   393216
         PromptChar      =   "_"
      End
      Begin VB.Label lblDESDE 
         Alignment       =   1  'Right Justify
         Caption         =   "Desde:"
         Height          =   255
         Left            =   120
         TabIndex        =   14
         Top             =   600
         Visible         =   0   'False
         Width           =   735
      End
      Begin VB.Label Label1 
         Alignment       =   1  'Right Justify
         Caption         =   "Hasta:"
         Height          =   255
         Left            =   2400
         TabIndex        =   6
         Top             =   600
         Width           =   735
      End
   End
   Begin MSComctlLib.ProgressBar pgbLLEVO 
      Height          =   255
      Left            =   6480
      TabIndex        =   3
      Top             =   240
      Visible         =   0   'False
      Width           =   4455
      _ExtentX        =   7858
      _ExtentY        =   450
      _Version        =   393216
      Appearance      =   1
      Max             =   1
   End
   Begin VB.ComboBox cboTIPO 
      Height          =   315
      Left            =   3240
      TabIndex        =   0
      Text            =   "Combo1"
      Top             =   240
      Width           =   2655
   End
   Begin MSComctlLib.ImageList imgBotones 
      Left            =   10920
      Top             =   3960
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   20
      ImageHeight     =   14
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   7
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmPrmREPOSAL.frx":0C54
            Key             =   ""
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmPrmREPOSAL.frx":0FA6
            Key             =   "ASG"
         EndProperty
         BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmPrmREPOSAL.frx":1260
            Key             =   "CAN"
         EndProperty
         BeginProperty ListImage4 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmPrmREPOSAL.frx":15B2
            Key             =   "SAV"
         EndProperty
         BeginProperty ListImage5 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmPrmREPOSAL.frx":18CC
            Key             =   "ADD"
         EndProperty
         BeginProperty ListImage6 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmPrmREPOSAL.frx":1B86
            Key             =   "DEL"
         EndProperty
         BeginProperty ListImage7 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmPrmREPOSAL.frx":1FC8
            Key             =   "CHA"
         EndProperty
      EndProperty
   End
   Begin MSComctlLib.ListView lstBODEGAS 
      Height          =   2055
      Left            =   480
      TabIndex        =   2
      Top             =   4800
      Width           =   5655
      _ExtentX        =   9975
      _ExtentY        =   3625
      View            =   3
      LabelEdit       =   1
      MultiSelect     =   -1  'True
      LabelWrap       =   -1  'True
      HideSelection   =   -1  'True
      FullRowSelect   =   -1  'True
      GridLines       =   -1  'True
      _Version        =   393217
      ForeColor       =   -2147483640
      BackColor       =   -2147483643
      BorderStyle     =   1
      Appearance      =   1
      NumItems        =   0
   End
   Begin MSComctlLib.ListView lstARTICULOS 
      Height          =   3375
      Left            =   480
      TabIndex        =   1
      Top             =   1080
      Width           =   10500
      _ExtentX        =   18521
      _ExtentY        =   5953
      View            =   3
      LabelEdit       =   1
      MultiSelect     =   -1  'True
      LabelWrap       =   -1  'True
      HideSelection   =   0   'False
      AllowReorder    =   -1  'True
      Checkboxes      =   -1  'True
      FullRowSelect   =   -1  'True
      GridLines       =   -1  'True
      _Version        =   393217
      ForeColor       =   -2147483640
      BackColor       =   -2147483643
      BorderStyle     =   1
      Appearance      =   1
      NumItems        =   2
      BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Text            =   "Nombre del Art�culo"
         Object.Width           =   7761
      EndProperty
      BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   1
         Text            =   "C�digo"
         Object.Width           =   3881
      EndProperty
   End
   Begin VB.Label txtTIPO 
      Alignment       =   1  'Right Justify
      Caption         =   "Tipo de Art�culos:"
      Height          =   315
      Left            =   600
      TabIndex        =   4
      Top             =   240
      Width           =   2415
   End
End
Attribute VB_Name = "frmPrmREPOSAL"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Dim ArrUXB() As Variant
Dim Articulo As UnArticulo
Dim Dueno As New ElTercero
'Dim LaBodega As String, LaAccion As String
Dim LaAccion As String      'DEPURACION DE CODIGO
'JAUM T29577 Inicio se deja linea en comentario
'Dim CnTdr As Integer, NumEnCombo As Integer
Dim CnTdr As Double 'Contador
Dim NumEnCombo As Integer
'JAUM T29577 Fin
Public OpcCod        As String   'Opci�n de seguridad
'NU_AUTO_ARTI, CD_CODI_ARTI, NO_NOMB_ARTI, NU_AUTO_UNVE_ARTI, TX_VENCE_ARTI, TX_PARA_ARTI,
'TX_ENTRA_ARTI, CD_GRUP_ARTI, CD_USOS_ARTI, VL_ULCO_ARTI, VL_COPR_ARTI, CT_EXIS_ARTI, DE_OBSE_ARTI, ID_TIPO_ARTIID_TIPO_ARTI, CD_RIPS_ARTI, NU_INDPYP_ARTI

Private Sub cboTIPO_GotFocus()
    NumEnCombo = Me.CboTipo.ListIndex
End Sub

'---------------------------------------------------------------------------------------
' Procedure : ChkDetalle_Click
' DateTime  : 14/03/2019 09:51
' Author    : daniel_mesa
' Purpose   : DRMG T45889-R43750 habilita o deshabilita el lstBODEGAS
'---------------------------------------------------------------------------------------
'
Private Sub ChkDetalle_Click()
   Dim Renglon As MSComctlLib.ListItem 'Almacena los items seleccionados en el lstBODEGAS
   
   If ChkDetalle.value = 1 Then
      lstBODEGAS.Enabled = False
      For Each Renglon In Me.lstBODEGAS.ListItems
         Renglon.Selected = False
      Next
   Else
      lstBODEGAS.Enabled = True
   End If
   
End Sub

Private Sub chkfcRANGO_Click()
    Me.txtFCDESDE.Enabled = False
    Me.txtFCHASTA.Enabled = False
    If Not CBool(Me.chkFCRANGO.value) = True Then Exit Sub
    Me.txtFCDESDE.Enabled = True
    Me.txtFCHASTA.Enabled = True
End Sub

Private Sub CmdImprimir_Click(Index As Integer)
   Dim RstSALD As ADODB.Recordset
   Set RstSALD = New ADODB.Recordset
   Dim Renglon As MSComctlLib.ListItem
   Dim NroRegs As Long
   Dim strARTICULOS As String, strBODEGAS As String
   Dim strTITRANGO As String, strTITFECHA As String, strFECORTE As String
   Dim stCmd As String 'NMSR M3287
   Dim StSql As String 'HRR M4559
   Dim InDec As Integer 'HRR M4559
       
   If Me.CboTipo.ListIndex < 0 Then Exit Sub 'APGR M2933
    
   Call Limpiar_CrysListar
    
   'HRR M4559
   'If Aplicacion.VerDecimales_Valores(InDec) Then 'HRR M4559 Nota 0024085
   If Aplicacion.VerDecimales_Cantidades(InDec) Then 'HRR M4559 Nota 0024085
   Else
      InDec = 0
   End If
   'HRR M4559
    
   If Me.chkARTICULOS.value = 0 Then
      For Each Renglon In Me.lstARTICULOS.ListItems
         'If Renglon.Selected Then strARTICULOS = strARTICULOS & IIf(Len(strARTICULOS) > 0, " OR ", "") & "CD_CODI_ARTI='" & Renglon.ListSubItems("COAR").Text & "'"
         If Renglon.Selected Then strARTICULOS = strARTICULOS & IIf(Len(strARTICULOS) > 0, " OR ", "") & "NU_AUTO_ARTI_KARD=" & Renglon.Tag
      Next
   End If
   If Me.ChkDetalle.value = 0 Then 'DRMG T45889-R43750
      For Each Renglon In Me.lstBODEGAS.ListItems
         'If Renglon.Selected Then strBODEGAS = strBODEGAS & IIf(Len(strBODEGAS) > 0, " OR ", "") & "TX_CODI_BODE='" & Renglon.ListSubItems("COD").Text & "'"
         If Renglon.Selected Then strBODEGAS = strBODEGAS & IIf(Len(strBODEGAS) > 0, " OR ", "") & "NU_AUTO_BODE_KARD=" & Renglon.Tag
      Next
   End If 'DRMG T45889-R43750
   If Len(strARTICULOS) > 0 Then strARTICULOS = "(" & strARTICULOS & ")"
   If Len(strBODEGAS) > 0 Then strBODEGAS = "(" & strBODEGAS & ")"
   If Len(strARTICULOS) > 0 And Len(strBODEGAS) > 0 Then
      strARTICULOS = strARTICULOS & " AND " & strBODEGAS
   Else
      strARTICULOS = strARTICULOS & strBODEGAS
   End If

   strTITRANGO = "TODOS LOS COMPROBANTES"
   strTITFECHA = "A LA FECHA"
   If CBool(Me.chkFCRANGO.value) Then
      'strARTICULOS = strARTICULOS & IIf(Len(strARTICULOS) > 0, " AND ", "") & _
      '"(FE_FECH_KARD <= " & FFechaCon(CDate(Me.txtFCHASTA.Text)) & ")"
      strFECORTE = "(FE_FECH_KARD <= " & FFechaCon(CDate(Me.txtFCHASTA.Text)) & ")"
      strTITFECHA = "Fecha de Corte: " & Me.txtFCHASTA.Text
   End If
   MDI_Inventarios.Crys_Listar.Formulas(8) = "RAGFecha='" & strTITFECHA & Comi

   strFECORTE = strFECORTE & IIf(Len(strFECORTE) = 0, "", " AND ") & "(NU_AUTO_KARD>=" & PriRegKARD & ")"
   'strARTICULOS = strARTICULOS & IIf(Len(strARTICULOS) = 0, "", " AND ") & "(NU_AUTO_KARD>=" & PriRegKARD & ")"
   If Len(strARTICULOS) > 0 Then
      strARTICULOS = strARTICULOS & " AND " & strFECORTE
   Else
      strARTICULOS = strARTICULOS & strFECORTE
   End If
   Debug.Print strARTICULOS
    
   If ExisteTABLA("IN_SALDOREPORTE") Then Result = EliminarTabla("IN_SALDOREPORTE")
   'strARTICULOS = "SELECT TX_NOMB_BODE, CD_CODI_ARTI, NO_NOMB_ARTI, TX_NOMB_UNVE, NU_ACTUNMR_KARD, NU_ACTUDNM_KARD, VL_COPR_ARTI INTO IN_SALDOREPORTE" & _
   " FROM IN_KARDEX, ARTICULO, IN_BODEGA, IN_UNDVENTA" & _
   " WHERE NU_AUTO_KARD IN (SELECT MAX(NU_AUTO_KARD)" & _
   " FROM IN_KARDEX" & IIf(Len(strARTICULOS) > 0, " WHERE ", "") & strARTICULOS & _
   " GROUP BY NU_AUTO_BODE_KARD, NU_AUTO_ARTI_KARD)" & _
   " AND NU_ACTUNMR_KARD<>0" & _
   " AND NU_AUTO_ARTI_KARD=NU_AUTO_ARTI" & _
   " AND NU_AUTO_BODE_KARD=NU_AUTO_BODE" & _
   " AND NU_AUTO_UNVE_ARTI=NU_AUTO_UNVE" & _
   " ORDER BY TX_NOMB_BODE, NO_NOMB_ARTI"
    
   'PedroJ Def 4427                                                                        'REOL M1312 [VL_COPR_ARTI->NU_COSTO_KARD]
   'strARTICULOS = "SELECT TX_NOMB_BODE, CD_CODI_ARTI, NO_NOMB_ARTI, TX_NOMB_UNVE, NU_ACTUNMR_KARD, NU_ACTUDNM_KARD, NU_COSTO_KARD INTO IN_SALDOREPORTE" & _
   '" FROM IN_KARDEX, ARTICULO, IN_BODEGA, IN_UNDVENTA" & _
   '" WHERE NU_AUTO_KARD IN (SELECT MAX(NU_AUTO_KARD)" & _
   '" FROM IN_KARDEX" & IIf(Len(strARTICULOS) > 0, " WHERE ", "") & strARTICULOS & _
   '" GROUP BY NU_AUTO_BODE_KARD, NU_AUTO_ARTI_KARD)" & _
   '" AND NU_AUTO_ARTI_KARD=NU_AUTO_ARTI" & _
   '" AND NU_AUTO_BODE_KARD=NU_AUTO_BODE" & _
   '" AND NU_AUTO_UNVE_ARTI=NU_AUTO_UNVE"
   'If CBool(Me.ChkExistencia.Value) = False Then strARTICULOS = strARTICULOS & " AND NU_ACTUNMR_KARD<>0"
   'strARTICULOS = strARTICULOS & " ORDER BY TX_NOMB_BODE, NO_NOMB_ARTI"
   'PedroJ Def 4427
   'NYCM M2791-----------------------------
   'strARTICULOS = "SELECT CD_CODI_ARTI,TX_NOMB_BODE, NO_NOMB_ARTI,TX_NOMB_UNVE,SUM(NU_ENTRAD_KARD)AS ENTRADAS,SUM(NU_SALIDA_KARD)AS SALIDAS," & _
   '" sum(((NU_ENTRAD_KARD)*(NU_COSTO_KARD)*(NU_MULT_KARD))/(NU_DIVI_KARD))AS COSTO_ENTRADAS," & _
   '" SUM(((NU_SALIDA_KARD)*(NU_COSTO_KARD)*(NU_MULT_KARD))/(NU_DIVI_KARD))AS COSTO_SALIDAS INTO IN_SALDOREPORTE " & _
   '" FROM IN_KARDEX, ARTICULO, IN_BODEGA, IN_UNDVENTA " & _
   '" WHERE " & strARTICULOS & _
   '" AND NU_AUTO_ARTI_KARD=NU_AUTO_ARTI AND NU_AUTO_BODE_KARD=NU_AUTO_BODE " & _
   '" AND NU_AUTO_UNVE_ARTI=NU_AUTO_UNVE " & _
   '" GROUP BY NO_NOMB_ARTI,TX_NOMB_BODE,TX_NOMB_UNVE,CD_CODI_ARTI"
   'If CBool(Me.ChkExistencia.Value) = False Then strARTICULOS = strARTICULOS & " HAVING (SUM(NU_ENTRAD_KARD) - SUM(NU_SALIDA_KARD)) <> 0"
   '/--------------------------------------
   'Debug.Print strARTICULOS

   'BD(BDCurCon).Execute strARTICULOS, NroRegs

   'HRR M4559 Inicio
   'NMSR M3287
   'stCmd = "SELECT CD_CODI_ARTI,TX_NOMB_BODE, NO_NOMB_ARTI,TX_NOMB_UNVE,"
   ''NMSR M3675 /***
   'stCmd = "SELECT T2.CD_CODI_ARTI,T2.TX_NOMB_BODE, T2.NO_NOMB_ARTI,T2.TX_NOMB_UNVE,"
   'stCmd = stCmd & "SUM(T2.ENTRADAS) AS ENTRADAS,"
   'stCmd = stCmd & "SUM(T2.SALIDAS) AS SALIDAS,"
   'stCmd = stCmd & "SUM(T2.COSTO_PROM) AS COSTO_PROMEDIO"
   'stCmd = stCmd & " INTO IN_SALDOREPORTE "
   'stCmd = stCmd & " FROM ("
   'stCmd = stCmd & "SELECT CD_CODI_ARTI,TX_NOMB_BODE, NO_NOMB_ARTI,TX_NOMB_UNVE,"
   ''NMSR M3675 ***/
   'stCmd = stCmd & "SUM(NU_ENTRAD_KARD*NU_MULT_KARD/NU_DIVI_KARD) AS ENTRADAS,"
   'stCmd = stCmd & "SUM(NU_SALIDA_KARD*NU_MULT_KARD/NU_DIVI_KARD) AS SALIDAS,"
   '''NMSR M3675 antes /****
   ''stCmd = stCmd & " SUM(NU_COSTO_KARD*(NU_ENTRAD_KARD*NU_MULT_KARD/NU_DIVI_KARD))AS COSTO_ENTRADAS,"
   ''stCmd = stCmd & " SUM(NU_COSTO_KARD*(NU_SALIDA_KARD*NU_MULT_KARD/NU_DIVI_KARD))AS COSTO_SALIDAS"
   '''NMSR M3675 ***/
   'stCmd = stCmd & " 0 AS COSTO_PROM" 'NMSR M3675
   ''stCmd = stCmd & " INTO IN_SALDOREPORTE " 'NMSR M3675
   'stCmd = stCmd & " FROM IN_KARDEX, ARTICULO, IN_BODEGA, IN_UNDVENTA "
   'stCmd = stCmd & " WHERE " & strARTICULOS
   'stCmd = stCmd & " AND NU_AUTO_ARTI_KARD=NU_AUTO_ARTI"
   'stCmd = stCmd & " AND NU_AUTO_BODE_KARD=NU_AUTO_BODE "
   'stCmd = stCmd & " AND NU_AUTO_UNVE_ARTI=NU_AUTO_UNVE "
   'stCmd = stCmd & " GROUP BY NO_NOMB_ARTI,TX_NOMB_BODE,TX_NOMB_UNVE,CD_CODI_ARTI"
   ''NMSR M3675
   'stCmd = stCmd & " UNION "
   'stCmd = stCmd & " SELECT CD_CODI_ARTI,TX_NOMB_BODE, NO_NOMB_ARTI,TX_NOMB_UNVE"
   'stCmd = stCmd & " ,0 AS ENTRADAS,0 AS SALIDAS,NU_COSTO_KARD AS COSTO_PROM"     'LJSA 4559  - Se pone en comentario -
   ''LJSA M4559 -- Costo Promedio ---
   ''stCmd = stCmd & " ,0 AS ENTRADAS,0 AS SALIDAS"
   ''stCmd = stCmd & ", (Select Sum((NU_ENTRAD_KARD - NU_SALIDA_KARD) * NU_COSTO_KARD)"
   ''stCmd = stCmd & "/(Sum(NU_ENTRAD_KARD)-Sum(NU_SALIDA_KARD))"
   ''stCmd = stCmd & " From IN_KARDEX Where "
   ''stCmd = stCmd & strARTICULOS & ") AS COSTO_PROM"
   ''LJSA M4559 -- Costo Promedio ---
   'stCmd = stCmd & " FROM IN_KARDEX, ARTICULO, IN_BODEGA, IN_UNDVENTA,"
   'stCmd = stCmd & " (SELECT MAX(NU_AUTO_KARD) AS AUTONUM,NU_AUTO_ARTI_KARD AS ARTI,"
   'stCmd = stCmd & " NU_AUTO_BODE_KARD AS BODE, NU_AUTO_UNVE_ARTI AS UNIVEN"
   'stCmd = stCmd & " FROM IN_KARDEX, Articulo, IN_BODEGA, IN_UNDVENTA"
   'stCmd = stCmd & " WHERE " & strARTICULOS
   'stCmd = stCmd & " AND NU_AUTO_ARTI_KARD=NU_AUTO_ARTI"
   'stCmd = stCmd & " AND NU_AUTO_BODE_KARD=NU_AUTO_BODE"
   'stCmd = stCmd & " AND NU_AUTO_UNVE_ARTI=NU_AUTO_UNVE"
   'stCmd = stCmd & " GROUP BY NU_AUTO_ARTI_KARD,NU_AUTO_BODE_KARD, NU_AUTO_UNVE_ARTI) AS T1"
   'stCmd = stCmd & " WHERE (NU_AUTO_ARTI_KARD = T1.ARTI) "
   'stCmd = stCmd & " AND (NU_AUTO_KARD=T1.AUTONUM)"
   'stCmd = stCmd & " AND NU_AUTO_ARTI=NU_AUTO_ARTI_KARD"
   'stCmd = stCmd & " AND NU_AUTO_BODE_KARD=NU_AUTO_BODE"
   'stCmd = stCmd & " AND NU_AUTO_UNVE_ARTI=NU_AUTO_UNVE"
   'stCmd = stCmd & " AND NU_AUTO_BODE = T1.BODE"
   'stCmd = stCmd & " AND NU_AUTO_UNVE = T1.UNIVEN ) AS T2"
   'stCmd = stCmd & " GROUP BY T2.NO_NOMB_ARTI,T2.TX_NOMB_BODE,T2.TX_NOMB_UNVE,T2.CD_CODI_ARTI"
   ''NMSR M3675
   ''If CBool(Me.ChkExistencia.Value) = False Then stCmd = stCmd & " HAVING (SUM(NU_ENTRAD_KARD) - SUM(NU_SALIDA_KARD)) <> 0"
   ''If CBool(Me.ChkExistencia.Value) = False Then stCmd = stCmd & " HAVING (SUM(NU_ENTRAD_KARD*NU_MULT_KARD/NU_DIVI_KARD) - SUM(NU_SALIDA_KARD*NU_MULT_KARD/NU_DIVI_KARD)) <> 0" 'NMSR M3668'NMSR M3675
   'If CBool(Me.ChkExistencia.Value) = False Then stCmd = stCmd & " HAVING SUM(T2.ENTRADAS) - SUM(T2.SALIDAS) <> 0" 'NMSR M3675
   '
   'Debug.Print stCmd
   '
   'BD(BDCurCon).Execute stCmd, NroRegs
   'NMSR M3287
   'HRR M4559 Fin
   
   'HRR M4559 Inicio Nota 0024085 Se quita el formateo a la consulta.
   'JAUM T39545 Inicio se deja bloque en comentario
   'StSql = "SELECT TX_NOMB_BODE, CD_CODI_ARTI, NO_NOMB_ARTI, TX_NOMB_UNVE,SUM(ENTRADAS) AS ENTRADAS, 0 AS SALIDAS, SUM(COSTO_PROMEDIO) AS COSTO_PROMEDIO "
   'StSql = StSql & "  INTO IN_SALDOREPORTE FROM ("
   'StSql = StSql & " SELECT TX_NOMB_BODE, CD_CODI_ARTI, NO_NOMB_ARTI, TX_NOMB_UNVE, 0 AS ENTRADAS, 0 AS SALIDAS,"
   'StSql = StSql & "SUM(CAST((NU_ENTRAD_KARD+(NU_SALIDA_KARD*-1)) AS FLOAT)*CAST(NU_MULT_KARD AS FLOAT)"
   '' DAHV T6796 - INICIO
   ''StSql = StSql & " /CAST(NU_DIVI_KARD AS FLOAT)*CAST(NU_COSTO_KARD AS FLOAT)) AS COSTO_PROMEDIO"
   'StSql = StSql & " /CAST(NU_DIVI_KARD AS FLOAT)*(CASE WHEN TX_IVADED_KARD = 'N' THEN CAST(NU_COSTO_KARD AS FLOAT) ELSE (CAST(NU_COSTO_KARD AS FLOAT) * 100)/(100+ CAST(NU_IMPU_KARD AS FLOAT)) END)) AS COSTO_PROMEDIO"
   '' DAHV T6796 - FIN
   'StSql = StSql & " FROM IN_KARDEX, ARTICULO, IN_BODEGA, IN_UNDVENTA "
   'StSql = StSql & " WHERE NU_AUTO_ARTI_KARD=NU_AUTO_ARTI"
   'StSql = StSql & " AND NU_AUTO_BODE_KARD=NU_AUTO_BODE"
   'StSql = StSql & " AND NU_AUTO_UNVE_ARTI=NU_AUTO_UNVE"
   'StSql = StSql & "  AND " & strARTICULOS
   'StSql = StSql & " GROUP BY TX_NOMB_BODE, CD_CODI_ARTI, NO_NOMB_ARTI, TX_NOMB_UNVE"
   'StSql = StSql & " UNION "
   ''DAHV R2329 - INICIO
   ''Depuracion de c�digo devisi�n por cero
   ''StSql = StSql & "SELECT TX_NOMB_BODE, CD_CODI_ARTI, NO_NOMB_ARTI, TX_NOMB_UNVE, ROUND(CAST(NU_ACTUNMR_KARD AS FLOAT)/CAST(NU_ACTUDNM_KARD AS FLOAT)," & InDec & " ) AS ENTRADAS, 0 AS SALIDAS, 0 AS COSTO_PROMEDIO"
   'StSql = StSql & " SELECT TX_NOMB_BODE, CD_CODI_ARTI, NO_NOMB_ARTI, TX_NOMB_UNVE, "
   'StSql = StSql & " CASE NU_ACTUDNM_KARD WHEN 0 THEN 0 ELSE ROUND(CAST(NU_ACTUNMR_KARD AS FLOAT)/CAST(NU_ACTUDNM_KARD AS FLOAT)," & InDec & " ) END AS ENTRADAS,"
   'StSql = StSql & " 0 AS SALIDAS, 0 AS COSTO_PROMEDIO"
   ''DAHV R2329 - FIN
   'StSql = StSql & " FROM IN_KARDEX, ARTICULO, IN_BODEGA, IN_UNDVENTA"
   'StSql = StSql & " WHERE NU_AUTO_KARD IN (SELECT MAX(NU_AUTO_KARD)"
   'StSql = StSql & " FROM IN_KARDEX " & IIf(Len(strARTICULOS) > 0, "WHERE ", "") & strARTICULOS
   'StSql = StSql & " GROUP BY NU_AUTO_BODE_KARD, NU_AUTO_ARTI_KARD)"
   'StSql = StSql & " AND NU_AUTO_ARTI_KARD=NU_AUTO_ARTI"
   'StSql = StSql & " AND NU_AUTO_BODE_KARD=NU_AUTO_BODE"
   'StSql = StSql & " AND NU_AUTO_UNVE_ARTI=NU_AUTO_UNVE"
   'If CBool(Me.ChkExistencia.Value) = False Then StSql = StSql & " AND NU_ACTUNMR_KARD<>0"
   'StSql = StSql & " ) AS T1"
   'StSql = StSql & " GROUP BY TX_NOMB_BODE, CD_CODI_ARTI, NO_NOMB_ARTI, TX_NOMB_UNVE"
   'If CBool(Me.ChkExistencia.Value) = False Then StSql = StSql & " HAVING SUM(ENTRADAS)<>0"
   StSql = ";WITH T1 AS (SELECT NU_AUTO_BODE_KARD,NU_AUTO_ARTI_KARD,TX_NOMB_BODE, CD_CODI_ARTI, NO_NOMB_ARTI,"
   StSql = StSql & " TX_NOMB_UNVE, 0 AS ENTRADAS, 0 AS SALIDAS,"
   StSql = StSql & " SUM(CAST((NU_ENTRAD_KARD+(NU_SALIDA_KARD*-1)) AS FLOAT)*CAST(NU_MULT_KARD AS FLOAT)"
   StSql = StSql & " /CAST(NU_DIVI_KARD AS FLOAT)*(CASE WHEN TX_IVADED_KARD = 'N' THEN CAST(NU_COSTO_KARD AS FLOAT)"
   StSql = StSql & " ELSE (CAST(NU_COSTO_KARD AS FLOAT) * 100)/(100+ CAST(NU_IMPU_KARD AS FLOAT)) END)) AS COSTO_PROMEDIO"
   StSql = StSql & " FROM IN_KARDEX INNER JOIN ARTICULO ON NU_AUTO_ARTI_KARD = NU_AUTO_ARTI"
   StSql = StSql & " INNER JOIN IN_BODEGA ON NU_AUTO_BODE_KARD = NU_AUTO_BODE"
   StSql = StSql & " INNER JOIN IN_UNDVENTA ON NU_AUTO_UNVE_ARTI = NU_AUTO_UNVE"
   StSql = StSql & " WHERE " & strARTICULOS
   StSql = StSql & " GROUP BY NU_AUTO_BODE_KARD,NU_AUTO_ARTI_KARD,TX_NOMB_BODE, CD_CODI_ARTI, NO_NOMB_ARTI, TX_NOMB_UNVE)"
   StSql = StSql & ", T2 AS (SELECT IK2.TX_NOMB_BODE, IK2.CD_CODI_ARTI, IK2.NO_NOMB_ARTI, IK2.TX_NOMB_UNVE,"
   StSql = StSql & " CASE IK.NU_ACTUDNM_KARD WHEN 0 THEN 0 ELSE ROUND(CAST(IK.NU_ACTUNMR_KARD AS FLOAT)/CAST(IK.NU_ACTUDNM_KARD AS FLOAT),0 ) END AS ENTRADAS,"
   StSql = StSql & " 0 AS SALIDAS, IK2.COSTO_PROMEDIO AS COSTO_PROMEDIO"
   If Me.ChkDetalle.value = 1 Then StSql = StSql & " ,COSUL.ULTIMO_COSTO" 'DRMG T45889-R43750
   If Me.ChkDetalle.value = 1 Then StSql = StSql & " ,COSPRO.COS_PROMEDIO" 'DRMG T46179
   StSql = StSql & " FROM IN_KARDEX IK INNER JOIN T1 IK2 ON IK.NU_AUTO_BODE_KARD = IK2.NU_AUTO_BODE_KARD"
   StSql = StSql & " AND IK.NU_AUTO_ARTI_KARD = IK2.NU_AUTO_ARTI_KARD"
   StSql = StSql & " INNER JOIN ARTICULO ON IK.NU_AUTO_ARTI_KARD=NU_AUTO_ARTI"
   StSql = StSql & " INNER JOIN IN_UNDVENTA ON NU_AUTO_UNVE_ARTI=NU_AUTO_UNVE"
   'DRMG T45889-R43750 INICIO
   If Me.ChkDetalle.value = 1 Then
      StSql = StSql & " INNER JOIN ( SELECT * FROM (SELECT"
      'StSql = StSql & " ROW_NUMBER()OVER(PARTITION BY CD_CODI_ARTI, NU_AUTO_BODE ORDER BY FE_FECH_KARD DESC) Particion," 'DRMG T46179 SE DEJA EN COMENTARIO
      StSql = StSql & " ROW_NUMBER()OVER(PARTITION BY CD_CODI_ARTI, NU_AUTO_BODE ORDER BY FE_FECH_KARD DESC,NU_AUTO_KARD DESC) Particion," 'DRMG T46179
      StSql = StSql & " NU_AUTO_ARTI,NU_AUTO_BODE,(NU_COSTO_KARD) AS ULTIMO_COSTO,FE_FECH_KARD"
      StSql = StSql & " FROM IN_ENCABEZADO "
      StSql = StSql & " INNER JOIN IN_KARDEX ON NU_AUTO_ORGCABE_KARD=NU_AUTO_ENCA AND (NU_ENTRAD_KARD * (Cast(NU_MULT_KARD As Float)/Cast(NU_DIVI_KARD As Float))) >0"
      StSql = StSql & " INNER JOIN IN_BODEGA ON  NU_AUTO_BODE_KARD=NU_AUTO_BODE"
      StSql = StSql & " INNER JOIN ARTICULO ON NU_AUTO_ARTI_KARD=NU_AUTO_ARTI"
      'StSql = StSql & " WHERE NU_AUTO_DOCU_ENCA IN (3,6,10) AND " & strARTICULOS 'DRMG T46179 SE DJE AEN COMENTARIO
      StSql = StSql & " WHERE NU_AUTO_DOCU_ENCA IN (3,9,12,14,15,22,24,26) AND " & strARTICULOS 'DRMG T46179
      StSql = StSql & " )AS P WHERE PARTICION = 1 ) AS COSUL"
      StSql = StSql & " ON COSUL.NU_AUTO_ARTI =IK.NU_AUTO_ARTI_KARD AND COSUL.NU_AUTO_BODE=IK.NU_AUTO_BODE_KARD"
      'DRMG T46179 INICIO CONSULTA EN CUAL TRAERA EL ULTCOMO COSTO PROMEDIO DE CUAQUIER TIPO DE MOVIMIENTO
      StSql = StSql & " INNER JOIN ( SELECT * FROM (SELECT"
      StSql = StSql & " ROW_NUMBER()OVER(PARTITION BY CD_CODI_ARTI, NU_AUTO_BODE ORDER BY FE_FECH_KARD DESC,NU_AUTO_KARD DESC) Particion,"
      StSql = StSql & " NU_AUTO_ARTI,NU_AUTO_BODE,FE_FECH_KARD"
      StSql = StSql & Coma & " IIF(NU_AUTO_DOCU_ENCA IN (3,9,12,14,15,22,24,26),ISNULL(NU_COSPRO_KARD,0),NU_COSTO_KARD) AS COS_PROMEDIO"
      StSql = StSql & " FROM IN_ENCABEZADO "
      StSql = StSql & " INNER JOIN IN_KARDEX ON NU_AUTO_ORGCABE_KARD=NU_AUTO_ENCA AND (NU_ENTRAD_KARD * (Cast(NU_MULT_KARD As Float)/Cast(NU_DIVI_KARD As Float))) >0"
      StSql = StSql & " INNER JOIN IN_BODEGA ON  NU_AUTO_BODE_KARD=NU_AUTO_BODE"
      StSql = StSql & " INNER JOIN ARTICULO ON NU_AUTO_ARTI_KARD=NU_AUTO_ARTI"
      StSql = StSql & " WHERE " & strARTICULOS
      StSql = StSql & " )AS COSPRO WHERE PARTICION = 1 ) AS COSPRO"
      StSql = StSql & " ON COSPRO.NU_AUTO_ARTI =IK.NU_AUTO_ARTI_KARD AND COSPRO.NU_AUTO_BODE=IK.NU_AUTO_BODE_KARD"
      'DRMG T46179 FIN
   End If
   'DRMG T45889-R43750 FIN
   StSql = StSql & " WHERE NU_AUTO_KARD IN (SELECT MAX(NU_AUTO_KARD) FROM IN_KARDEX"
   StSql = StSql & " WHERE " & strARTICULOS
   StSql = StSql & " GROUP BY NU_AUTO_BODE_KARD, NU_AUTO_ARTI_KARD)"
   If CBool(Me.ChkExistencia.value) = False Then StSql = StSql & " AND NU_ACTUNMR_KARD<>0"
   StSql = StSql & ") SELECT T2.TX_NOMB_BODE,T2.CD_CODI_ARTI,T2.NO_NOMB_ARTI,T2.TX_NOMB_UNVE,"
   'DRMG T46179 INICIO SE DEJA EN COMENTARI LA PRIMERA LINEA
   'StSql = StSql & " SUM(T2.ENTRADAS) AS ENTRADAS, 0 AS SALIDAS, SUM(T2.COSTO_PROMEDIO) AS COSTO_PROMEDIO"
   StSql = StSql & " SUM(T2.ENTRADAS) AS ENTRADAS, 0 AS SALIDAS"
   If Not Me.ChkDetalle.value = 1 Then
      StSql = StSql & ",SUM(T2.COSTO_PROMEDIO) AS COSTO_PROMEDIO"
   Else
      StSql = StSql & ",T2.COS_PROMEDIO AS COSTO_PROMEDIO"
   End If
   'DRMG T46179 FIN
   If Me.ChkDetalle.value = 1 Then StSql = StSql & " ,T2.ULTIMO_COSTO" 'DRMG T45889-R43750
   StSql = StSql & " INTO IN_SALDOREPORTE FROM T2"
   StSql = StSql & " GROUP BY T2.TX_NOMB_BODE,T2.CD_CODI_ARTI,T2.NO_NOMB_ARTI,T2.TX_NOMB_UNVE"
   If Me.ChkDetalle.value = 1 Then StSql = StSql & " ,T2.ULTIMO_COSTO" 'DRMG T45889-R43750
   If Me.ChkDetalle.value = 1 Then StSql = StSql & " ,T2.COS_PROMEDIO" 'DRMG T46179
   If CBool(Me.ChkExistencia.value) = False Then StSql = StSql & " HAVING SUM(ENTRADAS)<>0"
   'JAUM T39545 Fin
   strARTICULOS = StSql
   Debug.Print strARTICULOS
   BD(BDCurCon).Execute strARTICULOS, NroRegs

   'HRR M4559 Fin

   MDI_Inventarios.Crys_Listar.Formulas(10) = "ENTIDAD=" & Comi & Trim(Dueno.Nombre) & ", NIT:" & Trim(Dueno.Nit) & Comi
   'DRMG T45889-R43750 INICIO
   If Me.ChkDetalle.value = 1 Then
      If Me.opcCON Then
         MDI_Inventarios.Crys_Listar.Formulas(11) = "COS=1"
      Else
         MDI_Inventarios.Crys_Listar.Formulas(11) = "COS=0"
      End If
      Call ListarD("InfExistDeta.rpt", "", crptToWindow, "SALDOS DE INVENTARIO", MDI_Inventarios, True)
   Else
   'DRMG T45889-R43750 FIN
      Call ListarD("infSALDOS" & IIf(Me.opcCON, "CON", "SIN") & ".rpt", "", crptToWindow, "SALDOS DE INVENTARIO", MDI_Inventarios, True)
   End If 'DRMG T45889-R43750
   MDI_Inventarios.Crys_Listar.Formulas(10) = ""
End Sub

Private Sub Form_Load()
'    Call Main
    Call CenterForm(MDI_Inventarios, Me)
    Me.CboTipo.Clear
    Dueno.IniXNit (Dueno.Nit)
    Me.CboTipo.AddItem "Para la Venta"
    Me.CboTipo.AddItem "Para El Consumo"
    NumEnCombo = Me.CboTipo.ListIndex
    If Len(FormatDateTime(Date, vbShortDate)) = 8 Then
        Me.txtFCDESDE.Mask = "##/##/##"
        Me.txtFCHASTA.Mask = "##/##/##"
    Else
        Me.txtFCDESDE.Mask = "##/##/####"
        Me.txtFCHASTA.Mask = "##/##/####"
    End If
    Me.txtFCDESDE.Text = FormatDateTime(DateAdd("d", -(Day(Date) - 1), Date), vbShortDate)
    Me.txtFCHASTA.Text = FormatDateTime(DateAdd("d", -1, DateAdd("M", 1, DateAdd("d", -(Day(Date) - 1), Date))), vbShortDate)
    Me.chkFCRANGO.value = False
    Me.txtFCDESDE.Enabled = False
    Me.txtFCHASTA.Enabled = False
'NU_AUTO_BODE, NU_AUTO_BODE_BODE, TX_CODI_BODE, TX_NOMB_BODE, CD_CODI_CECO_BODE, TX_VENTA_BODE
'FROM IN_BODEGA;
    Me.lstBODEGAS.ListItems.Clear
    Me.lstBODEGAS.Checkboxes = False
    Me.lstBODEGAS.MultiSelect = True
    Me.lstBODEGAS.HideSelection = False
    Me.lstBODEGAS.ColumnHeaders.Clear
    'Me.lstBODEGAS.ColumnHeaders.Add , "NOM", "Noombre de la Bodega", 0.66 * Me.lstBODEGAS.Width
    Me.lstBODEGAS.ColumnHeaders.Add , "NOM", "Nombre de la Bodega", 0.66 * Me.lstBODEGAS.Width 'NMSR M2797
    Me.lstBODEGAS.ColumnHeaders.Add , "COD", "C�digo", 0.34 * Me.lstBODEGAS.Width
    
    Me.lstARTICULOS.ListItems.Clear
    Me.lstARTICULOS.Checkboxes = False
    Me.lstARTICULOS.MultiSelect = True
    Me.lstARTICULOS.HideSelection = False
    Me.lstARTICULOS.Width = 10500
    Me.lstARTICULOS.ColumnHeaders.Clear
'lstArticulos, NOAR, COAR, NOCO, NOUS, NOGR, AUCO, COGR, COUS
    Me.lstARTICULOS.ColumnHeaders.Add , "NOAR", "Nombre del art�culo", 0.4 * Me.lstARTICULOS.Width
    Me.lstARTICULOS.ColumnHeaders.Add , "COAR", "C�digo", 0.15 * Me.lstARTICULOS.Width
    Me.lstARTICULOS.ColumnHeaders.Add , "NOCO", "Unidad de conteo", 0.15 * Me.lstARTICULOS.Width
    Me.lstARTICULOS.ColumnHeaders.Add , "NOUS", "Uso", 0.15 * Me.lstARTICULOS.Width
    Me.lstARTICULOS.ColumnHeaders.Add , "NOGR", "Grupo", 0.15 * Me.lstARTICULOS.Width
    Me.lstARTICULOS.ColumnHeaders.Add , "AUCO", "AutoUNC", 0
    Me.lstARTICULOS.ColumnHeaders.Add , "COGR", "CodGRU", 0
    Me.lstARTICULOS.ColumnHeaders.Add , "COUS", "CodUSO", 0
FALLO:
End Sub

Private Sub cboTIPO_LostFocus()
    If NumEnCombo = Me.CboTipo.ListIndex Then Exit Sub
    Dim tipo As String * 1
    Me.lstBODEGAS.ListItems.Clear
    Me.lstARTICULOS.ListItems.Clear
    Me.pgbLLEVO.value = 0
    Me.pgbLLEVO.Visible = True
    Select Case Me.CboTipo.ListIndex
    Case Is = -1
        tipo = "0"
    Case Is = 0
        tipo = "V"
    Case Is = 1
        tipo = "C"
    End Select
    Campos = "NU_AUTO_BODE, NU_AUTO_BODE_BODE, TX_CODI_BODE, TX_NOMB_BODE, " & _
        "CD_CODI_CECO_BODE, TX_VENTA_BODE"
    Desde = "IN_BODEGA"
    Condi = "TX_VENTA_BODE='" & tipo & "' ORDER BY TX_NOMB_BODE"
    ReDim ArrUXB(5, 0)
    Result = LoadMulData(Desde, Campos, Condi, ArrUXB())
    If Result = FAIL Then GoTo FALLO
    On Error GoTo SIGUI
    For CnTdr = 0 To UBound(ArrUXB, 2)
        Me.lstBODEGAS.ListItems.Add , "B" & ArrUXB(2, CnTdr), ArrUXB(3, CnTdr)
        Me.lstBODEGAS.ListItems("B" & ArrUXB(2, CnTdr)).ListSubItems.Add , "COD", ArrUXB(2, CnTdr)
        Me.lstBODEGAS.ListItems("B" & ArrUXB(2, CnTdr)).Tag = ArrUXB(0, CnTdr)
SIGUI:
    Next
    On Error GoTo 0
    Campos = "DISTINCT NU_AUTO_ARTI, CD_CODI_ARTI, NO_NOMB_ARTI, NU_AUTO_UNVE_ARTI, TX_PARA_ARTI, " & _
        "DE_DESC_GRUP, DE_DESC_USOS, TX_NOMB_UNVE, CD_GRUP_ARTI, CD_USOS_ARTI, " & _
        "NU_MULT_UNVE, NU_DIVI_UNVE"
    
    Desde = "ARTICULO, USOS, GRUP_ARTICULO, IN_UNDVENTA, IN_DETALLE"
    Condi = "CD_CODI_GRUP = CD_GRUP_ARTI AND CD_CODI_USOS = CD_USOS_ARTI"
    Condi = Condi & " AND NU_AUTO_UNVE = NU_AUTO_UNVE_ARTI"
    Condi = Condi & " AND NU_AUTO_ARTI = NU_AUTO_ARTI_DETA"
    Condi = Condi & " AND TX_PARA_ARTI='" & tipo & "' ORDER BY NO_NOMB_ARTI"
    ReDim ArrUXB(11, 0)
    Result = LoadMulData(Desde, Campos, Condi, ArrUXB())
    If Result = FAIL Then GoTo FALLO
    If Not Encontro Then GoTo NOENC
    Me.pgbLLEVO.Min = 0
    Me.pgbLLEVO.value = 0
    Me.pgbLLEVO.Max = UBound(ArrUXB, 2) + 1
    Dim Articulo As New UnArticulo
    For CnTdr = 0 To UBound(ArrUXB, 2)
        Me.pgbLLEVO.value = CnTdr
        Articulo.AutoNumero = ArrUXB(0, CnTdr)
        Articulo.Codigo = ArrUXB(1, CnTdr)
        Articulo.Nombre = ArrUXB(2, CnTdr)
        Articulo.AutoUDConteo = ArrUXB(3, CnTdr)
        Articulo.NombreGrupo = ArrUXB(5, CnTdr)
        Articulo.NombreUso = ArrUXB(6, CnTdr)
        Articulo.NombreUndConteo = ArrUXB(7, CnTdr)
        Articulo.ParaQueEs = InStr(1, "VC", ArrUXB(4, CnTdr)) - 1
        Articulo.CodigoGrupo = ArrUXB(8, CnTdr)
        Articulo.CodigoUso = ArrUXB(9, CnTdr)
        Me.lstARTICULOS.ListItems.Add , "A" & Articulo.AutoNumero, Trim(Articulo.Nombre)
        Me.lstARTICULOS.ListItems("A" & Articulo.AutoNumero).Tag = CStr(Articulo.AutoNumero)
'lstArticulos, NOAR, COAR, NOCO, NOUS, NOGR, AUCO, COGR, COUS, MUCO, DICO
        Me.lstARTICULOS.ListItems("A" & Articulo.AutoNumero).ListSubItems.Add , "COAR", Trim(Articulo.Codigo)
        Me.lstARTICULOS.ListItems("A" & Articulo.AutoNumero).ListSubItems.Add , "NOCO", Trim(Articulo.NombreUndConteo)
        Me.lstARTICULOS.ListItems("A" & Articulo.AutoNumero).ListSubItems.Add , "NOUS", Trim(Articulo.NombreUso)
        Me.lstARTICULOS.ListItems("A" & Articulo.AutoNumero).ListSubItems.Add , "NOGR", Trim(Articulo.NombreGrupo)
        Me.lstARTICULOS.ListItems("A" & Articulo.AutoNumero).ListSubItems.Add , "AUCO", Trim(Articulo.AutoUDConteo)
        Me.lstARTICULOS.ListItems("A" & Articulo.AutoNumero).ListSubItems.Add , "COGR", Trim(Articulo.CodigoGrupo)
        Me.lstARTICULOS.ListItems("A" & Articulo.AutoNumero).ListSubItems.Add , "COUS", Trim(Articulo.CodigoUso)
    Next
    Set Articulo = Nothing
NOENC:
FALLO:
    Me.pgbLLEVO.Visible = False
End Sub

Private Sub lstARTICULOS_ColumnClick(ByVal ColumnHeader As MSComctlLib.ColumnHeader)
    Me.lstARTICULOS.SortKey = ColumnHeader.Index - 1
    Me.lstARTICULOS.Sorted = True
End Sub

'DEPURACION DE CODIGO
'Private Sub txtIDEN_KeyPress(Tecla As Integer)
'    Call txtNOMB_KeyPress(Tecla)
'End Sub

'DEPURACION DE CODIGO
'Private Sub txtNOMB_KeyPress(Tecla As Integer)
'    Dim Cara As String * 1
'    Cara = Chr(Tecla)
'    If Cara = vbBack Then Exit Sub
'    If Asc(Cara) > 96 And Asc(Tecla) < 113 Then Tecla = Asc(UCase(Chr(Tecla)))
'    If InStr(" 01234567890ABCDEFGHIJKLMNOPQRSTUVWXYZ", Chr(Tecla)) = 0 Then Tecla = 0
'End Sub

Private Sub txtFCDESDE_LostFocus()
    If IsDate(Me.txtFCDESDE.Text) Then Exit Sub
    Me.txtFCDESDE.Text = FormatDateTime(DateAdd("d", -(Day(Date) - 1), Date), vbShortDate)
    Me.txtFCDESDE.SetFocus
End Sub

Private Sub txtFCHASTA_LostFocus()
    If IsDate(Me.txtFCHASTA.Text) Then Exit Sub
    Me.txtFCHASTA.Text = FormatDateTime(DateAdd("d", -1, DateAdd("M", 1, DateAdd("d", -(Day(Date) - 1), Date))), vbShortDate)
    Me.txtFCHASTA.SetFocus
End Sub

Private Sub Form_KeyPress(KeyAscii As Integer)
    If KeyAscii = vbKeyReturn Then KeyAscii = vbKeyTab
End Sub

