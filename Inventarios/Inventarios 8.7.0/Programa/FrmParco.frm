VERSION 5.00
Object = "{0BA686C6-F7D3-101A-993E-0000C0EF6F5E}#1.0#0"; "THREED32.OCX"
Begin VB.Form FrmParco 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Parametrizaci�n con Contabilidad "
   ClientHeight    =   3210
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   8085
   Icon            =   "FrmParco.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   ScaleHeight     =   3210
   ScaleWidth      =   8085
   Begin VB.Frame FraDatos 
      Height          =   3135
      Left            =   120
      TabIndex        =   14
      Top             =   0
      Width           =   7095
      Begin VB.ComboBox CboMovimiento 
         Height          =   315
         ItemData        =   "FrmParco.frx":058A
         Left            =   1800
         List            =   "FrmParco.frx":058C
         Style           =   2  'Dropdown List
         TabIndex        =   1
         Top             =   240
         Width           =   4335
      End
      Begin VB.TextBox Txt_Cuenta 
         Height          =   285
         Index           =   0
         Left            =   1800
         MaxLength       =   15
         TabIndex        =   5
         Top             =   1200
         Width           =   1455
      End
      Begin VB.TextBox Txt_Cuenta 
         Height          =   285
         Index           =   1
         Left            =   1800
         MaxLength       =   15
         TabIndex        =   7
         Top             =   1680
         Width           =   1455
      End
      Begin VB.TextBox Txt_Cuenta 
         Height          =   285
         Index           =   2
         Left            =   1800
         MaxLength       =   15
         TabIndex        =   9
         Top             =   2160
         Width           =   1455
      End
      Begin VB.TextBox Txt_Cuenta 
         Height          =   285
         Index           =   3
         Left            =   1800
         MaxLength       =   15
         TabIndex        =   11
         Top             =   2640
         Width           =   1455
      End
      Begin VB.TextBox TxtConta 
         Enabled         =   0   'False
         Height          =   285
         Index           =   0
         Left            =   1800
         MaxLength       =   3
         TabIndex        =   3
         Tag             =   "0"
         Top             =   720
         Width           =   495
      End
      Begin Threed.SSCommand CmdSelec 
         Height          =   375
         Index           =   0
         Left            =   2280
         TabIndex        =   15
         ToolTipText     =   "Selecci�n de Comprobantes Contables"
         Top             =   600
         Width           =   615
         _Version        =   65536
         _ExtentX        =   1085
         _ExtentY        =   661
         _StockProps     =   78
         Enabled         =   0   'False
         BevelWidth      =   0
         RoundedCorners  =   0   'False
         Outline         =   0   'False
         Picture         =   "FrmParco.frx":058E
      End
      Begin Threed.SSCommand CmdSelec 
         Height          =   375
         Index           =   1
         Left            =   3240
         TabIndex        =   17
         ToolTipText     =   "Selecci�n de Cuenta"
         Top             =   1080
         Width           =   615
         _Version        =   65536
         _ExtentX        =   1085
         _ExtentY        =   661
         _StockProps     =   78
         BevelWidth      =   0
         RoundedCorners  =   0   'False
         Outline         =   0   'False
         Picture         =   "FrmParco.frx":0F40
      End
      Begin Threed.SSCommand CmdSelec 
         Height          =   375
         Index           =   2
         Left            =   3240
         TabIndex        =   18
         ToolTipText     =   "Selecci�n de Cuenta"
         Top             =   1560
         Width           =   615
         _Version        =   65536
         _ExtentX        =   1085
         _ExtentY        =   661
         _StockProps     =   78
         BevelWidth      =   0
         RoundedCorners  =   0   'False
         Outline         =   0   'False
         Picture         =   "FrmParco.frx":18F2
      End
      Begin Threed.SSCommand CmdSelec 
         Height          =   375
         Index           =   3
         Left            =   3240
         TabIndex        =   19
         ToolTipText     =   "Selecci�n de Cuenta"
         Top             =   2040
         Width           =   615
         _Version        =   65536
         _ExtentX        =   1085
         _ExtentY        =   661
         _StockProps     =   78
         BevelWidth      =   0
         RoundedCorners  =   0   'False
         Outline         =   0   'False
         Picture         =   "FrmParco.frx":22A4
      End
      Begin Threed.SSCommand CmdSelec 
         Height          =   375
         Index           =   4
         Left            =   3240
         TabIndex        =   20
         ToolTipText     =   "Selecci�n de Cuenta"
         Top             =   2520
         Width           =   615
         _Version        =   65536
         _ExtentX        =   1085
         _ExtentY        =   661
         _StockProps     =   78
         BevelWidth      =   0
         RoundedCorners  =   0   'False
         Outline         =   0   'False
         Picture         =   "FrmParco.frx":2C56
      End
      Begin VB.Label Label1 
         Caption         =   "Cuenta de IVA"
         Height          =   255
         Index           =   2
         Left            =   120
         TabIndex        =   4
         Top             =   1200
         Width           =   1455
      End
      Begin VB.Label lbl_cuenta 
         BackColor       =   &H8000000E&
         BorderStyle     =   1  'Fixed Single
         Height          =   285
         Index           =   0
         Left            =   3840
         TabIndex        =   24
         Top             =   1200
         Width           =   3135
      End
      Begin VB.Label Label1 
         Caption         =   "Cuenta de Rete IVA"
         Height          =   255
         Index           =   1
         Left            =   120
         TabIndex        =   6
         Top             =   1680
         Width           =   1455
      End
      Begin VB.Label lbl_cuenta 
         BackColor       =   &H8000000E&
         BorderStyle     =   1  'Fixed Single
         Height          =   285
         Index           =   1
         Left            =   3840
         TabIndex        =   23
         Top             =   1680
         Width           =   3135
      End
      Begin VB.Label lbl_cuenta 
         BackColor       =   &H8000000E&
         BorderStyle     =   1  'Fixed Single
         Height          =   285
         Index           =   2
         Left            =   3840
         TabIndex        =   22
         Top             =   2160
         Width           =   3135
      End
      Begin VB.Label lbl_cuenta 
         BackColor       =   &H8000000E&
         BorderStyle     =   1  'Fixed Single
         Height          =   285
         Index           =   3
         Left            =   3840
         TabIndex        =   21
         Top             =   2640
         Width           =   3135
      End
      Begin VB.Label Label1 
         Caption         =   "Cuenta de ICA"
         Height          =   255
         Index           =   5
         Left            =   120
         TabIndex        =   8
         Top             =   2160
         Width           =   1455
      End
      Begin VB.Label Label1 
         AutoSize        =   -1  'True
         Caption         =   "Cuenta de Retenci�n"
         Height          =   195
         Index           =   6
         Left            =   120
         TabIndex        =   10
         Top             =   2640
         Width           =   1515
      End
      Begin VB.Label Label1 
         Caption         =   "Movimiento:"
         Height          =   255
         Index           =   0
         Left            =   120
         TabIndex        =   0
         Top             =   240
         Width           =   1095
      End
      Begin VB.Label Lbl_compro 
         BackColor       =   &H80000009&
         BorderStyle     =   1  'Fixed Single
         Enabled         =   0   'False
         Height          =   285
         Index           =   0
         Left            =   2880
         TabIndex        =   16
         Top             =   720
         Width           =   3135
      End
      Begin VB.Label LblCompro 
         AutoSize        =   -1  'True
         Caption         =   "Com&probante :"
         Height          =   195
         Index           =   0
         Left            =   120
         TabIndex        =   2
         Top             =   720
         Width           =   1035
      End
   End
   Begin Threed.SSCommand Cmd_Botones 
      Height          =   705
      HelpContextID   =   120
      Index           =   1
      Left            =   7320
      TabIndex        =   13
      ToolTipText     =   "Salir"
      Top             =   960
      Width           =   735
      _Version        =   65536
      _ExtentX        =   1296
      _ExtentY        =   1244
      _StockProps     =   78
      Caption         =   "&SALIR"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   6
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Picture         =   "FrmParco.frx":3608
   End
   Begin Threed.SSCommand Cmd_Botones 
      Height          =   705
      HelpContextID   =   120
      Index           =   0
      Left            =   7320
      TabIndex        =   12
      ToolTipText     =   "Guardar"
      Top             =   120
      Width           =   735
      _Version        =   65536
      _ExtentX        =   1296
      _ExtentY        =   1244
      _StockProps     =   78
      Caption         =   "&GUARDAR"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   6
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Picture         =   "FrmParco.frx":3CD2
   End
End
Attribute VB_Name = "FrmParco"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Dim Tabla As String
Dim Mensaje As String
Dim TipoM As Byte
'Dim i As Integer   'DEPURACION DE CODIGO

Private Sub CboMovimiento_KeyPress(KeyAscii As Integer)
    Call Cambiar_Enter(KeyAscii)
End Sub

Private Sub CboMovimiento_LostFocus()
    Call Leer_Movimiento
End Sub

Private Sub CmdSelec_Click(Index As Integer)
   'COMPROBANTE ENTRADA
   If Index = 0 Then
      Codigo = Seleccion("TC_COMPROBANTE", "NO_NOMB_COMP", "CD_CODI_COMP,NO_NOMB_COMP", "COMPROBANTES CONTABLES", NUL$)
      If Codigo <> NUL$ Then TxtConta(Index) = Codigo
      TxtConta_LostFocus (Index)
      TxtConta(Index).SetFocus
   Else
      Codigo = NUL$
      'JLPB T29905/R28918 INICIO Se deja en comentario el siguiente bloque
      'Codigo = Seleccion("CUENTAS", "NO_NOMB_CUEN", "CD_CODI_CUEN,NO_NOMB_CUEN", "CUENTAS CONTABLES", NUL$)
      Condicion = "TX_PUCNIIF_CUEN<>'1'"
      Codigo = Seleccion("CUENTAS", "NO_NOMB_CUEN", "CD_CODI_CUEN,NO_NOMB_CUEN", "CUENTAS CONTABLES", Condicion)
      'JLPB T29905/R28918 FIN
      If Codigo <> NUL$ Then Txt_Cuenta(Index - 1) = Codigo
      Txt_Cuenta_LostFocus (Index - 1)
      Txt_Cuenta(Index - 1).SetFocus
   End If
End Sub

Private Sub Form_Load()
    Me.CboMovimiento.Clear
    Me.CboMovimiento.AddItem "ENTRADA DE ALMACEN"
    Me.CboMovimiento.ItemData(Me.CboMovimiento.NewIndex) = Entrada
    Me.CboMovimiento.AddItem "SALIDA DE BODEGA" '1
    Me.CboMovimiento.ItemData(Me.CboMovimiento.NewIndex) = Salida
    Me.CboMovimiento.AddItem "VENTA DE MERCANCIA" '2
    Me.CboMovimiento.ItemData(Me.CboMovimiento.NewIndex) = FacturaVenta
    Me.CboMovimiento.AddItem "BAJA DE PACIENTES" '3
    Me.CboMovimiento.ItemData(Me.CboMovimiento.NewIndex) = BajaConsumo
    Me.CboMovimiento.AddItem "DEVOLUCION DE CLIENTE" '4
    Me.CboMovimiento.ItemData(Me.CboMovimiento.NewIndex) = DevolucionFacturaVenta
    Me.CboMovimiento.AddItem "DEVOLUCION A PROVEEDOR" '5
    Me.CboMovimiento.ItemData(Me.CboMovimiento.NewIndex) = DevolucionEntrada
    Me.CboMovimiento.AddItem "APROVECHAMIENTOS/DONACIONES" '6
    Me.CboMovimiento.ItemData(Me.CboMovimiento.NewIndex) = AprovechaDonacion
    Me.CboMovimiento.AddItem "AJUSTES X INFLACION" '7
    Me.CboMovimiento.ItemData(Me.CboMovimiento.NewIndex) = AjusteXInflacion
    Me.CboMovimiento.AddItem "DEVOLUCION BAJA CONSUMO" '8
    Me.CboMovimiento.ItemData(Me.CboMovimiento.NewIndex) = DevolucionBaja
    Me.CboMovimiento.AddItem "COMPRA DE INVENTARIOS" '9
    Me.CboMovimiento.ItemData(Me.CboMovimiento.NewIndex) = CompraDElementos
    
    Call CenterForm(MDI_Inventarios, Me)
    Tabla = "CONCEPTOT"
    Mensaje = "Parametrizaci�n con Contabilidad"
    Call Cargar_Datos
    CboMovimiento.ListIndex = 0
End Sub

Private Sub Buscar_Comprobante(Index As Integer)
   ReDim arr(0)
   Condicion = "CD_CODI_COMP=" & Comi & Cambiar_Comas_Comillas(TxtConta(Index)) & Comi
   Result = LoadData("TC_COMPROBANTE", "NO_NOMB_COMP", Condicion, arr())
   If (Result <> False) Then
     If Encontro Then
        Lbl_compro(Index) = arr(0)
     Else
        TxtConta(Index) = NUL$
        Lbl_compro(Index) = NUL$
     End If
   End If
End Sub

Private Sub Cmd_Botones_Click(Index As Integer)
    Select Case Index
        Case 0: Call Grabar
        Case 1: Unload Me
    End Select
End Sub

Private Sub Grabar()
Dim arr(0)
   DoEvents
   If Lbl_compro(0) = NUL$ Then
      Call Mensaje1("No se especifico ningun comprobante!", 2)
      Exit Sub
   End If
   Call MouseClock
   Msglin "Ingresando informaci�n de la " & Mensaje
   If (BeginTran(STranIUp & Tabla) <> FAIL) Then
        If WarnMsg("Desea Guardar la " & Mensaje, 0) Then
            Result = LoadData("CONCEPTOT", "NU_MOVI_CONC", "NU_MOVI_CONC=" & TipoM, arr())
            If arr(0) = NUL$ Then
               Valores = "NU_MOVI_CONC=" & TipoM & Coma
               Valores = Valores & "CD_COMP_CONC=" & Comi & TxtConta(0) & Comi & Coma
               Valores = Valores & "CD_IVA_CONC=" & Comi & Txt_Cuenta(0) & Comi & Coma
               Valores = Valores & "CD_RIVA_CONC=" & Comi & Txt_Cuenta(1) & Comi & Coma
               Valores = Valores & "CD_ICA_CONC=" & Comi & Txt_Cuenta(2) & Comi & Coma
               Valores = Valores & "CD_RETE_CONC=" & Comi & Txt_Cuenta(3) & Comi
               Result = DoInsertSQL(Tabla, Valores)
               If (Result <> FAIL) Then Result = Auditor(Tabla, TranIns, LastCmd)
            Else
               Condicion = "NU_MOVI_CONC=" & TipoM
               Valores = "CD_COMP_CONC=" & Comi & TxtConta(0) & Comi & Coma
               Valores = Valores & "CD_IVA_CONC=" & Comi & Txt_Cuenta(0) & Comi & Coma
               Valores = Valores & "CD_RIVA_CONC=" & Comi & Txt_Cuenta(1) & Comi & Coma
               Valores = Valores & "CD_ICA_CONC=" & Comi & Txt_Cuenta(2) & Comi & Coma
               Valores = Valores & "CD_RETE_CONC=" & Comi & Txt_Cuenta(3) & Comi
               Result = DoUpdate(Tabla, Valores, Condicion)
               If (Result <> FAIL) Then Result = Auditor(Tabla, TranUpd, LastCmd)
            End If
        End If
   End If
   If (Result <> FAIL) Then
      If (CommitTran() <> FAIL) Then
           Call Limpiar
           CboMovimiento.SetFocus
      Else
         Call RollBackTran
      End If
   Else
      Call RollBackTran
   End If
   Call MouseNorm
   Msglin NUL$
End Sub
'Private Sub Graba_Cuentas()
'  Dim I As Integer
'  For I = 1 To GrdCuentas.Rows - 1 Step 1
'       '''Crea la relaci�n entre el concepto y las cuentas
'       Valores = Comi & CmbMovi.ListIndex & Comi & Coma
'       Valores = Valores & Comi & GrdCuentas.TextMatrix(I, 0) & Comi & Coma
'       Valores = Valores & Comi & GrdCuentas.TextMatrix(I, 1) & Comi & Coma
'       Valores = Valores & Comi & GrdCuentas.TextMatrix(I, 2) & Comi
'       Result = DoInsert("R_CUEN_MOVI", Valores)
'  Next
'End Sub

Private Sub Txt_Cuenta_GotFocus(Index As Integer)
   Txt_Cuenta(Index).SelStart = 0
   Txt_Cuenta(Index).SelLength = Len(Txt_Cuenta(Index))
End Sub

Private Sub Txt_Cuenta_KeyPress(Index As Integer, KeyAscii As Integer)
    Call Cambiar_Enter(KeyAscii)
End Sub

Private Sub Txt_Cuenta_LostFocus(Index As Integer)
   Txt_Cuenta(Index) = Trim(Txt_Cuenta(Index))
   If Txt_Cuenta(Index) = NUL$ Then
      lbl_cuenta(Index) = NUL$
   Else
      Call Buscar_Cuenta(Index)
   End If
End Sub

Private Sub TxtConta_GotFocus(Index As Integer)
   TxtConta(Index).SelStart = 0
   TxtConta(Index).SelLength = Len(TxtConta(Index).Text)
   Msglin "Digite el C�digo del Comprobante Contable asociado al Movimiento"
End Sub

Private Sub TxtConta_KeyPress(Index As Integer, KeyAscii As Integer)
  Call Cambiar_Enter(KeyAscii)
  Call ValKeyAlfaNum(KeyAscii)
End Sub

Private Sub TxtConta_LostFocus(Index As Integer)
  If TxtConta(Index) <> NUL$ Then
     Call Buscar_Comprobante(Index)
  Else
     Lbl_compro(Index) = NUL$
  End If
End Sub

Private Sub Cargar_Datos()
'Dim Arr(1)
'    condicion = "CD_CODI_COMP = CD_COMP_CONC AND NU_MOVI_CONC =0"
'    Result = LoadData("CONCEPTOT,TC_COMPROBANTE", "CD_COMP_CONC, NO_NOMB_COMP", condicion, Arr())
'    TxtConta(0) = Arr(0): Lbl_compro(0) = Arr(1)
'    Lbl_compro(0).Tag = Arr(1)
'
'    condicion = "CD_CODI_COMP = CD_COMP_CONC AND NU_MOVI_CONC =5"
'    Result = LoadData("CONCEPTOT,TC_COMPROBANTE", "CD_COMP_CONC, NO_NOMB_COMP", condicion, Arr())
'    TxtConta(1) = Arr(0): Lbl_compro(1) = Arr(1)
'    Lbl_compro(1).Tag = Arr(1)
'
'    condicion = "CD_CODI_COMP = CD_COMP_CONC AND NU_MOVI_CONC =2"
'    Result = LoadData("CONCEPTOT,TC_COMPROBANTE", "CD_COMP_CONC, NO_NOMB_COMP", condicion, Arr())
'    TxtConta(2) = Arr(0): Lbl_compro(2) = Arr(1)
'    Lbl_compro(2).Tag = Arr(1)
'
'    condicion = "CD_CODI_COMP = CD_COMP_CONC AND NU_MOVI_CONC =7"
'    Result = LoadData("CONCEPTOT,TC_COMPROBANTE", "CD_COMP_CONC, NO_NOMB_COMP", condicion, Arr())
'    TxtConta(3) = Arr(0): Lbl_compro(3) = Arr(1)
'    Lbl_compro(3).Tag = Arr(1)
End Sub

Private Sub Leer_Movimiento()
Dim arr(5)
'  Select Case CboMovimiento.ListIndex
'    Case 0: TipoM = 0
'    Case 1: TipoM = 5
'    Case 2: TipoM = 2
'    Case 3: TipoM = 7
'    Case 4: TipoM = 3
'    Case 5: TipoM = 6
'    Case 6: TipoM = 8
'  End Select
    If Not Me.CboMovimiento.ListIndex >= 0 Then Exit Sub
    TipoM = Me.CboMovimiento.ItemData(Me.CboMovimiento.ListIndex)
    Condicion = "CD_CODI_COMP = CD_COMP_CONC AND NU_MOVI_CONC =" & TipoM
    Result = LoadData("CONCEPTOT,TC_COMPROBANTE", "CD_COMP_CONC, NO_NOMB_COMP, CD_IVA_CONC, CD_RIVA_CONC, CD_ICA_CONC, CD_RETE_CONC", Condicion, arr())
    TxtConta(0) = arr(0): Lbl_compro(0) = arr(1)
    Lbl_compro(0).Tag = arr(1)
    Txt_Cuenta(0) = arr(2): Call Buscar_Cuenta(0)
    Txt_Cuenta(1) = arr(3): Call Buscar_Cuenta(1)
    Txt_Cuenta(2) = arr(4): Call Buscar_Cuenta(2)
    Txt_Cuenta(3) = arr(5): Call Buscar_Cuenta(3)
End Sub

Private Sub Buscar_Cuenta(Index As Integer)
   ReDim arr(0)
   If Txt_Cuenta(Index) = NUL$ Then lbl_cuenta(Index) = NUL$: Exit Sub
   'JLPB T29905/R28918 INICIO Se deja en comentario la siguiente linea
   'Result = LoadData("CUENTAS", "NO_NOMB_CUEN", "CD_CODI_CUEN=" & Comi & Txt_Cuenta(Index) & Comi, arr())
   Condicion = "CD_CODI_CUEN=" & Comi & Txt_Cuenta(Index) & Comi
   Condicion = Condicion & " AND TX_PUCNIIF_CUEN<>'1'"
   Result = LoadData("CUENTAS", "NO_NOMB_CUEN", Condicion, arr())
   'JLPB T29905/R28918 FIN
   lbl_cuenta(Index) = arr(0)
   If arr(0) <> NUL$ Then
      If Valida_Nivel_Cuentas(Txt_Cuenta(Index)) Then
         Call Mensaje1("Verifique que la cuenta contable sea de �ltimo nivel!", 2)
         Txt_Cuenta(Index).SetFocus
      End If
   Else
      'Call Mensaje1("Cuenta contable no registrada !", 2)
      Txt_Cuenta(Index) = NUL$
      'Txt_Cuenta(Index).SetFocus
   End If
End Sub

Private Sub Limpiar()
    TxtConta(0) = NUL$: Lbl_compro(0) = NUL$
    Txt_Cuenta(0) = NUL$: lbl_cuenta(0) = NUL$
    Txt_Cuenta(1) = NUL$: lbl_cuenta(1) = NUL$
    Txt_Cuenta(2) = NUL$: lbl_cuenta(2) = NUL$
    Txt_Cuenta(3) = NUL$: lbl_cuenta(3) = NUL$
End Sub
