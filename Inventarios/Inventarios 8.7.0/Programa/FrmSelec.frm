VERSION 5.00
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "MSFLXGRD.OCX"
Object = "{0BA686C6-F7D3-101A-993E-0000C0EF6F5E}#1.0#0"; "THREED32.OCX"
Begin VB.Form FrmSeleccion 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Selecci�n"
   ClientHeight    =   4740
   ClientLeft      =   2595
   ClientTop       =   2445
   ClientWidth     =   9150
   ClipControls    =   0   'False
   Icon            =   "FrmSelec.frx":0000
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   4740
   ScaleWidth      =   9150
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin VB.Frame Frame1 
      Height          =   4695
      Left            =   0
      TabIndex        =   6
      Top             =   0
      Width           =   9135
      Begin VB.OptionButton OptCriterio 
         Caption         =   "Nombre"
         Height          =   255
         Index           =   0
         Left            =   1800
         TabIndex        =   14
         Top             =   240
         Value           =   -1  'True
         Width           =   1095
      End
      Begin VB.OptionButton OptCriterio 
         Caption         =   "C�digo"
         Height          =   255
         Index           =   1
         Left            =   2880
         TabIndex        =   15
         Top             =   240
         Width           =   855
      End
      Begin VB.ComboBox Txt_Seleccion 
         Height          =   315
         ItemData        =   "FrmSelec.frx":058A
         Left            =   4680
         List            =   "FrmSelec.frx":05A0
         TabIndex        =   1
         Top             =   240
         Width           =   3375
      End
      Begin VB.CommandButton Cmd_Cancelar 
         Caption         =   "&CANCELAR"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   6
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   435
         Left            =   4800
         MouseIcon       =   "FrmSelec.frx":05CC
         Picture         =   "FrmSelec.frx":08D6
         Style           =   1  'Graphical
         TabIndex        =   4
         Top             =   4080
         Width           =   1335
      End
      Begin VB.CommandButton Cmd_Seleccion 
         Caption         =   "&SELECCIONAR"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   6
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   435
         Left            =   2760
         Picture         =   "FrmSelec.frx":0C18
         Style           =   1  'Graphical
         TabIndex        =   3
         Top             =   4080
         Width           =   1335
      End
      Begin Threed.SSPanel SSPanel2 
         Height          =   3375
         Left            =   15
         TabIndex        =   7
         Top             =   600
         Width           =   8985
         _Version        =   65536
         _ExtentX        =   15849
         _ExtentY        =   5953
         _StockProps     =   15
         BackColor       =   12632256
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Begin VB.PictureBox Picture1 
            BorderStyle     =   0  'None
            Height          =   360
            Left            =   8205
            Picture         =   "FrmSelec.frx":116A
            ScaleHeight     =   360
            ScaleWidth      =   600
            TabIndex        =   5
            Top             =   600
            Width           =   600
         End
         Begin MSFlexGridLib.MSFlexGrid Grd_Selecciones 
            Height          =   2745
            Left            =   120
            TabIndex        =   2
            Top             =   480
            Width           =   8775
            _ExtentX        =   15478
            _ExtentY        =   4842
            _Version        =   393216
            Rows            =   1
            FixedRows       =   0
            FixedCols       =   0
            BackColorBkg    =   16777215
            Redraw          =   -1  'True
            SelectionMode   =   1
            MouseIcon       =   "FrmSelec.frx":1B0C
         End
         Begin VB.Label Lbl_Condicion 
            BackColor       =   &H80000009&
            BorderStyle     =   1  'Fixed Single
            Caption         =   "Lbl_Condicion"
            Height          =   255
            Left            =   6120
            TabIndex        =   12
            Top             =   120
            Visible         =   0   'False
            Width           =   975
         End
         Begin VB.Image Image1 
            Height          =   615
            Left            =   3000
            Top             =   1200
            Width           =   1095
         End
         Begin VB.Label Lbl_Tabla 
            BackColor       =   &H80000009&
            BorderStyle     =   1  'Fixed Single
            Caption         =   "Lbl_Tabla"
            Height          =   255
            Left            =   1320
            TabIndex        =   10
            Top             =   120
            Visible         =   0   'False
            Width           =   855
         End
         Begin VB.Label Lbl_Campos 
            BackColor       =   &H80000009&
            BorderStyle     =   1  'Fixed Single
            Caption         =   "Lbl_Campos"
            Height          =   255
            Left            =   7080
            TabIndex        =   11
            Top             =   120
            Visible         =   0   'False
            Width           =   975
         End
         Begin VB.Label Lbl_OrderBY 
            BackColor       =   &H80000009&
            BorderStyle     =   1  'Fixed Single
            Caption         =   "Lbl_OrderBY"
            Height          =   255
            Left            =   120
            TabIndex        =   9
            Top             =   120
            Visible         =   0   'False
            Width           =   975
         End
         Begin VB.Label Lbl_Titulo 
            Alignment       =   2  'Center
            Caption         =   "Lbl_Titulo"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   12
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Left            =   120
            TabIndex        =   8
            Top             =   120
            Width           =   8475
         End
      End
      Begin VB.Label Label1 
         Caption         =   "Iniciales"
         Height          =   255
         Left            =   4080
         TabIndex        =   13
         Top             =   240
         Width           =   735
      End
      Begin VB.Label Label3 
         Caption         =   "Cri&terios de Selecci�n"
         Height          =   255
         Left            =   120
         TabIndex        =   0
         Top             =   240
         Width           =   1935
      End
   End
End
Attribute VB_Name = "FrmSeleccion"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Dim BAND As Integer
'Dim SQL2 As String     'DEPURACION DE CODIGO

Private Sub Cmd_Cancelar_Click()
    Codigo = NUL$
    If BAND = 1 Then ' cargando el grid
      CancelTrans = 1
    End If
    If BAND = 0 Then
      Unload Me
      CancelTrans = 0
    End If
    
End Sub

Private Sub Cmd_Seleccion_Click()

Grd_Selecciones.Col = 0
Codigo = Grd_Selecciones.Text
Unload Me
End Sub
Private Sub Form_Unload(Cancel As Integer)
   If BAND = 1 Then
      CancelTrans = 1
      Cancel = True
   Else
      CancelTrans = 0
   End If
'   If Grd_Selecciones.Rows > 0 Then
'     'If Grd_Selecciones.TextMatrix(Grd_Selecciones.Rows - 1, 0) = NUL$ Then Codigo = NUL$
'   Else
'     Codigo = NUL$
'   End If
'INICIO 'GAVL T5264
    If Grd_Selecciones.Rows = 1 Then
        Codigo = NUL$
    End If
   'FIN 'GAVL T5264
End Sub

Private Sub Grd_selecciones_DblClick()
If BAND = 0 Then Call Cmd_Seleccion_Click
End Sub

Private Sub Grd_selecciones_KeyPress(KeyAscii As Integer)
If BAND = 0 Then
   If KeyAscii = 13 Then Call Cmd_Seleccion_Click
End If
End Sub

Private Sub Form_Load()
    Grd_Selecciones.ColWidth(0) = 1500
    Grd_Selecciones.ColWidth(1) = 10000
    Txt_Seleccion.ListIndex = 0 'NYCM M2227
    CancelTrans = 0
End Sub

Private Sub Grd_Selecciones_RowColChange()
Dim A As Long
A = Grd_Selecciones.RowPos(Grd_Selecciones.Row)
If (A < 2640 And A >= 0) Then
 Picture1.Top = 480 + A
End If
End Sub

Private Sub OptCriterio_KeyPress(Index As Integer, KeyAscii As Integer)
Call Cambiar_Enter(KeyAscii) 'NYCM M2227
End Sub



Private Sub Txt_Seleccion_GotFocus()
Txt_Seleccion.SelStart = 0
Txt_Seleccion.SelLength = Len(Txt_Seleccion.Text)
End Sub

Private Sub Txt_Seleccion_KeyDown(KeyCode As Integer, Shift As Integer)

Call Cambiar_Enter(KeyCode)

End Sub

Private Sub Txt_Seleccion_KeyPress(KeyAscii As Integer)
Call Cambiar_Enter(KeyAscii)
End Sub

Private Sub Txt_Seleccion_LostFocus()
Dim Pos As Integer

DoEvents

Grd_Selecciones.Clear
Grd_Selecciones.Rows = 0

'If InStr(Lbl_Campos, " ") <> 0 Then Lbl_Campos = Mid(Lbl_Campos, InStr(Lbl_Campos, " "), Len(Lbl_Campos)) 'REOL M1395
Lbl_Campos = Replace(Lbl_Campos, "DISTINCT", "") 'REOL 1533
Pos = InStr(Lbl_Campos, ",")
If Pos = 0 Then Exit Sub
Codigo = Mid(Lbl_Campos, 1, Pos - 1)

If Trim(Txt_Seleccion) = "*" Or Trim(Txt_Seleccion) = "%" Then Txt_Seleccion = CaractLike

If OptCriterio(0).Value = True Then
   Condicion = Lbl_OrderBY & " Like '" & Txt_Seleccion & CaractLike & Comi & IIf(Lbl_Condicion = NUL$, NUL$, " AND " & Lbl_Condicion) & " ORDER BY " & Lbl_OrderBY
Else
   Condicion = Codigo & " Like '" & Txt_Seleccion & CaractLike & Comi & IIf(Lbl_Condicion = NUL$, NUL$, " AND " & Lbl_Condicion) & " ORDER BY " & Codigo
End If

If Txt_Seleccion.Text <> "" Then
    Call desabilitar
    Result = LoadfGrid(Grd_Selecciones, Lbl_Tabla, Lbl_Campos, Condicion)
    Call habilitar
    Cmd_Seleccion.Enabled = True
    If Not Encontro And Grd_Selecciones.TextMatrix(0, 0) = NUL$ Then
      Call Mensaje1("Ningun elemento encontrado", 3)
    End If
    Grd_Selecciones.Col = 0
    'DoEvents  'ronald se coloco el do events para quitar un error 5
    'en tiempo de ejecucion debido a que la grilla no alcanzaba
    'a estar habilitada cuando se le enviaba el setfocus
    If Grd_Selecciones.Visible = True Then Grd_Selecciones.SetFocus
        
    If Result = FAIL Then
        Call Mensaje1("Error al cargar la tabla " & Lbl_Tabla, 1)
        Unload Me
    End If
    
End If
CancelTrans = 0
End Sub

Sub desabilitar()
    BAND = 1
    Cmd_Seleccion.Enabled = False
    Txt_Seleccion.Enabled = False
    'SSPanel2.Enabled = False
End Sub

Sub habilitar()
    BAND = 0
    Cmd_Seleccion.Enabled = True
    Txt_Seleccion.Enabled = True
    SSPanel2.Enabled = True

End Sub
