VERSION 5.00
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "Comdlg32.ocx"
Object = "{00025600-0000-0000-C000-000000000046}#5.2#0"; "Crystl32.OCX"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Object = "{0BA686C6-F7D3-101A-993E-0000C0EF6F5E}#1.0#0"; "THREED32.OCX"
Begin VB.MDIForm MDI_Inventarios 
   BackColor       =   &H00FFFFFF&
   Caption         =   "Cnt-Inventarios"
   ClientHeight    =   7200
   ClientLeft      =   1530
   ClientTop       =   1695
   ClientWidth     =   11310
   Icon            =   "MdiInventarios.frx":0000
   LinkTopic       =   "MDIForm1"
   Picture         =   "MdiInventarios.frx":058A
   WindowState     =   2  'Maximized
   Begin MSComctlLib.ImageList ImageList1 
      Left            =   2880
      Top             =   1560
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   16
      ImageHeight     =   16
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   2
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MdiInventarios.frx":15FECC
            Key             =   ""
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MdiInventarios.frx":16021E
            Key             =   ""
         EndProperty
      EndProperty
   End
   Begin VB.PictureBox picRes 
      Align           =   1  'Align Top
      Height          =   135
      Left            =   0
      ScaleHeight     =   75
      ScaleWidth      =   11250
      TabIndex        =   4
      Top             =   75
      Visible         =   0   'False
      Width           =   11310
      Begin VB.Image imgHelp 
         Height          =   180
         Left            =   480
         Picture         =   "MdiInventarios.frx":160570
         Top             =   120
         Visible         =   0   'False
         Width           =   180
      End
      Begin VB.Image imgH 
         Height          =   120
         Left            =   240
         Picture         =   "MdiInventarios.frx":160A42
         Top             =   120
         Visible         =   0   'False
         Width           =   120
      End
   End
   Begin MSComctlLib.ImageList ImgLstInventar 
      Left            =   1920
      Top             =   1500
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   20
      ImageHeight     =   14
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   1
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MdiInventarios.frx":160B44
            Key             =   ""
         EndProperty
      EndProperty
   End
   Begin VB.Timer RelojMDI 
      Interval        =   61111
      Left            =   120
      Top             =   480
   End
   Begin VB.PictureBox VBSQL1 
      Align           =   1  'Align Top
      Height          =   0
      Left            =   0
      ScaleHeight     =   0
      ScaleWidth      =   11310
      TabIndex        =   1
      Top             =   210
      Width           =   11310
   End
   Begin VB.PictureBox Errores 
      Align           =   1  'Align Top
      Height          =   75
      Left            =   0
      ScaleHeight     =   15
      ScaleWidth      =   11250
      TabIndex        =   0
      Top             =   0
      Visible         =   0   'False
      Width           =   11310
      Begin MSComctlLib.ListView Lstw_Formas 
         Height          =   2775
         Left            =   0
         TabIndex        =   2
         Top             =   0
         Width           =   4095
         _ExtentX        =   7223
         _ExtentY        =   4895
         View            =   2
         LabelWrap       =   -1  'True
         HideSelection   =   -1  'True
         _Version        =   393217
         ForeColor       =   -2147483640
         BackColor       =   -2147483643
         BorderStyle     =   1
         Appearance      =   1
         NumItems        =   5
         BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Text            =   "opcion"
            Object.Width           =   2540
         EndProperty
         BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   1
            Text            =   "permiso 1"
            Object.Width           =   2540
         EndProperty
         BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   2
            Text            =   "permiso 2"
            Object.Width           =   2540
         EndProperty
         BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   3
            Text            =   "permiso 3"
            Object.Width           =   2540
         EndProperty
         BeginProperty ColumnHeader(5) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   4
            Text            =   "permiso 4"
            Object.Width           =   2540
         EndProperty
      End
   End
   Begin MSComDlg.CommonDialog CMDialog1 
      Left            =   600
      Top             =   960
      _ExtentX        =   847
      _ExtentY        =   847
      _Version        =   393216
   End
   Begin Crystal.CrystalReport Crys_Listar 
      Left            =   120
      Top             =   960
      _ExtentX        =   741
      _ExtentY        =   741
      _Version        =   348160
      WindowLeft      =   20
      WindowWidth     =   750
      WindowHeight    =   450
      WindowControlBox=   -1  'True
      WindowMaxButton =   -1  'True
      WindowMinButton =   -1  'True
      WindowControls  =   -1  'True
      PrintFileLinesPerPage=   60
      WindowShowGroupTree=   -1  'True
      WindowAllowDrillDown=   -1  'True
      WindowShowCloseBtn=   -1  'True
      WindowShowSearchBtn=   -1  'True
      WindowShowPrintSetupBtn=   -1  'True
      WindowShowRefreshBtn=   -1  'True
   End
   Begin MSComctlLib.ImageList Img_List 
      Left            =   120
      Top             =   1440
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   20
      ImageHeight     =   20
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   20
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MdiInventarios.frx":160E96
            Key             =   ""
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MdiInventarios.frx":161560
            Key             =   ""
         EndProperty
         BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MdiInventarios.frx":161C2A
            Key             =   ""
         EndProperty
         BeginProperty ListImage4 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MdiInventarios.frx":1622F4
            Key             =   ""
         EndProperty
         BeginProperty ListImage5 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MdiInventarios.frx":163296
            Key             =   ""
         EndProperty
         BeginProperty ListImage6 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MdiInventarios.frx":163918
            Key             =   ""
         EndProperty
         BeginProperty ListImage7 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MdiInventarios.frx":163D5A
            Key             =   ""
         EndProperty
         BeginProperty ListImage8 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MdiInventarios.frx":164B2C
            Key             =   ""
         EndProperty
         BeginProperty ListImage9 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MdiInventarios.frx":1651F6
            Key             =   ""
         EndProperty
         BeginProperty ListImage10 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MdiInventarios.frx":1658C0
            Key             =   ""
         EndProperty
         BeginProperty ListImage11 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MdiInventarios.frx":165F8A
            Key             =   ""
         EndProperty
         BeginProperty ListImage12 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MdiInventarios.frx":166654
            Key             =   ""
         EndProperty
         BeginProperty ListImage13 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MdiInventarios.frx":166D1E
            Key             =   ""
         EndProperty
         BeginProperty ListImage14 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MdiInventarios.frx":1673E8
            Key             =   ""
         EndProperty
         BeginProperty ListImage15 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MdiInventarios.frx":167C3A
            Key             =   ""
         EndProperty
         BeginProperty ListImage16 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MdiInventarios.frx":168304
            Key             =   ""
         EndProperty
         BeginProperty ListImage17 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MdiInventarios.frx":1689CE
            Key             =   ""
         EndProperty
         BeginProperty ListImage18 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MdiInventarios.frx":169622
            Key             =   ""
         EndProperty
         BeginProperty ListImage19 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MdiInventarios.frx":169CEC
            Key             =   ""
         EndProperty
         BeginProperty ListImage20 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MdiInventarios.frx":16A2DE
            Key             =   ""
         EndProperty
      EndProperty
   End
   Begin MSComctlLib.ImageList Img_Dis 
      Left            =   720
      Top             =   1440
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   20
      ImageHeight     =   20
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   18
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MdiInventarios.frx":16A838
            Key             =   ""
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MdiInventarios.frx":16AD92
            Key             =   ""
         EndProperty
         BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MdiInventarios.frx":16B2EC
            Key             =   ""
         EndProperty
         BeginProperty ListImage4 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MdiInventarios.frx":16B846
            Key             =   ""
         EndProperty
         BeginProperty ListImage5 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MdiInventarios.frx":16BDA0
            Key             =   ""
         EndProperty
         BeginProperty ListImage6 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MdiInventarios.frx":16C2FA
            Key             =   ""
         EndProperty
         BeginProperty ListImage7 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MdiInventarios.frx":16C854
            Key             =   ""
         EndProperty
         BeginProperty ListImage8 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MdiInventarios.frx":16D52E
            Key             =   ""
         EndProperty
         BeginProperty ListImage9 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MdiInventarios.frx":16E208
            Key             =   ""
         EndProperty
         BeginProperty ListImage10 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MdiInventarios.frx":16E762
            Key             =   ""
         EndProperty
         BeginProperty ListImage11 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MdiInventarios.frx":16ECBC
            Key             =   ""
         EndProperty
         BeginProperty ListImage12 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MdiInventarios.frx":16F216
            Key             =   ""
         EndProperty
         BeginProperty ListImage13 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MdiInventarios.frx":16F770
            Key             =   ""
         EndProperty
         BeginProperty ListImage14 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MdiInventarios.frx":16FCCA
            Key             =   ""
         EndProperty
         BeginProperty ListImage15 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MdiInventarios.frx":17051C
            Key             =   ""
         EndProperty
         BeginProperty ListImage16 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MdiInventarios.frx":17062E
            Key             =   ""
         EndProperty
         BeginProperty ListImage17 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MdiInventarios.frx":170B88
            Key             =   ""
         EndProperty
         BeginProperty ListImage18 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "MdiInventarios.frx":1710E2
            Key             =   ""
         EndProperty
      EndProperty
   End
   Begin Threed.SSPanel Pnl_Barra_Estado 
      Align           =   2  'Align Bottom
      Height          =   390
      Left            =   0
      Negotiate       =   -1  'True
      TabIndex        =   3
      Top             =   6810
      Width           =   11310
      _Version        =   65536
      _ExtentX        =   19950
      _ExtentY        =   688
      _StockProps     =   15
      BackColor       =   12632256
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BevelOuter      =   0
      Begin VB.Timer Timer1 
         Enabled         =   0   'False
         Interval        =   61111
         Left            =   1440
         Top             =   -6240
      End
      Begin Threed.SSPanel Pnl_Usuario 
         Height          =   360
         Left            =   5080
         TabIndex        =   5
         Top             =   0
         Width           =   2940
         _Version        =   65536
         _ExtentX        =   5177
         _ExtentY        =   635
         _StockProps     =   15
         Caption         =   " Usuario: "
         BackColor       =   12632256
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Alignment       =   1
         Autosize        =   3
      End
      Begin Threed.SSPanel Pnl_Fecha 
         Height          =   360
         Left            =   8010
         TabIndex        =   6
         Top             =   0
         Width           =   2940
         _Version        =   65536
         _ExtentX        =   5177
         _ExtentY        =   635
         _StockProps     =   15
         BackColor       =   12632256
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Alignment       =   1
         Autosize        =   3
      End
      Begin Threed.SSPanel Pnl_Version 
         Height          =   360
         Left            =   10930
         TabIndex        =   7
         Top             =   0
         Width           =   6000
         _Version        =   65536
         _ExtentX        =   10583
         _ExtentY        =   635
         _StockProps     =   15
         BackColor       =   12632256
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Alignment       =   1
         Autosize        =   3
      End
      Begin Threed.SSPanel pnl_conexion 
         Height          =   360
         Left            =   16920
         TabIndex        =   8
         Top             =   0
         Width           =   2205
         _Version        =   65536
         _ExtentX        =   3889
         _ExtentY        =   635
         _StockProps     =   15
         BackColor       =   12632256
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Alignment       =   1
      End
   End
   Begin VB.Menu Mnu_Archivo 
      Caption         =   "&Archivo"
      Begin VB.Menu Mnu_Desconexion 
         Caption         =   "&Desconexi�n de la Base de Datos"
         Enabled         =   0   'False
         Visible         =   0   'False
      End
      Begin VB.Menu Mnu_Conexion 
         Caption         =   "&Conexi�n a la Base de Datos"
      End
      Begin VB.Menu Mnu_Salir 
         Caption         =   "&Salir                                    Alt-F4"
      End
   End
   Begin VB.Menu MnuAdministracion 
      Caption         =   "A&dministraci�n"
      HelpContextID   =   1000
      Visible         =   0   'False
      Begin VB.Menu MnuMenu 
         Caption         =   "Menu "
      End
      Begin VB.Menu MnuRAYA 
         Caption         =   "-"
      End
      Begin VB.Menu MnuACTUALIZA 
         Caption         =   "Actualizar"
      End
   End
   Begin VB.Menu MnuMovi 
      Caption         =   "&Movimiento"
      HelpContextID   =   2000
      Visible         =   0   'False
   End
   Begin VB.Menu MnuVentanas 
      Caption         =   "&Ventanas"
      WindowList      =   -1  'True
   End
   Begin VB.Menu MnuAyuda 
      Caption         =   "A&yuda"
      HelpContextID   =   8000
      Begin VB.Menu MnuTemas 
         Caption         =   "&Temas de Ayuda"
         HelpContextID   =   800001
      End
      Begin VB.Menu mnuraya9 
         Caption         =   "-"
      End
      Begin VB.Menu MnuAcerca 
         Caption         =   "&Acerca de...."
         HelpContextID   =   800002
      End
   End
End
Attribute VB_Name = "MDI_Inventarios"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Public accesoc As Integer
Dim EjecutarInicio As Integer
Dim FrmManualTarifario As New FrmTabGen
Dim vDifEnPesos As Single
Dim inX As Integer, inY As Integer '|.DR.|

Private Sub MDIForm_Initialize()
    
Dim stCadena As String ' almacena el comando para cambiar el ejecutable
Dim StInstApp As String  'Instacia de la aplicacion (fuentes o ejecutable)
Dim StServName As String 'almacena la ruta a la base de datos o el tipo de aplicacion
Dim LnCadena As Long  'longitud de la cadena

'Call InitFormat 'HRR R1187
 'HRR R1187
   StInstApp = String(255, 0)
   LnCadena = GetModuleFileName(App.hInstance, StInstApp, 255)
   StInstApp = Left(StInstApp, LnCadena)
        
   If Not (UCase(Right(StInstApp, 7)) = "VB6.EXE") Then
        If Control_Version(StServName) Then
                         
             stCadena = "copy /Y " & Chr$(34) & StServName & "\CNTSISTE.CNT\PROGRAMA\CNT-INVENTARIOS\CNT-INVENTARIOS.EXE" & Chr$(34) & " " & Chr$(34) & "%cd%\" & Chr$(34)
             'StCadena = StCadena & vbCrLf & "call " & Chr$(34) & "%cd%\CNT-CARTERA.EXE" & Chr$(34)
             Open App.Path & "\CopyExe.bat" For Output As #1
             Print #1, stCadena
             Close #1
             
             Call Shell(App.Path & "\CopyExe.bat", 1)
             End
        Else
            Call InitFormat
        End If
   Else
      Call InitFormat
   End If
'HRR R1187
   BoSeleccion = False 'AASV M3694
End Sub

Private Sub MDIForm_MouseDown(Button As Integer, Shift As Integer, x As Single, Y As Single)
        
        inX = x: inY = Y
        
End Sub

Private Sub MDIForm_MouseUp(Button As Integer, Shift As Integer, x As Single, Y As Single)
        
        Static btCrack As Byte
        
        '|.DR.|, Activar depuraci�n SQL: (Dibuje una X sobre el formulario MDI)
        If inX - x < -2000 And inY - Y < -2000 And btCrack = 0 Then
            btCrack = 1
        ElseIf x - inX < -2000 And inY - Y < -2000 And btCrack = 1 Then
            boDebug = True
            MsgBox ("Depuraci�n SQL activada."), vbInformation, "Ventana de depuraci�n SQL"
        Else
            btCrack = 0
        End If
        
End Sub

Private Sub Mnu_Conexion_Click()
   Call Connection(Connect)
End Sub

Public Sub EstadoConexion()
   Dim I As Integer
   If (Not Connect) Then
      Call LogOff
      'Me.Caption = "Cnt-Medicina Prepagada" 'JACC M3768 Se coloca en comentario
      Me.Mnu_Conexion.Caption = "Conexi�n a la Base de Datos"
      Me.MnuAdministracion.Enabled = False
      Me.MnuAdministracion.Visible = False
   End If
'   If Not (Status) Then
   
'   End If
   'Mnu_Mantenimiento.Visible = Connect
   'Mnu_Conexion.Caption = IIf(Connect, "Des&conexi�n de", "&Conexi�n a") & " la base de datos"
   Pnl_Usuario.Caption = " Usuario: " & IIf(Connect, UserId, NUL$) 'AASV R2240 SE QUITA EL COMENTARIO
   If (Connect) Then
      Me.Mnu_Conexion.Caption = "Desconexi�n de la Base de Datos"
      
      'Antes de R1249
      Me.MnuAdministracion.Enabled = True
      Me.MnuAdministracion.Visible = True
      'R1249
      
    'LJSA M4703 ---- Se pone en comentario y se habilita el c�digo anterior --
    '  'HRR R1249
    '  If NumStation = 0 Or ValVersion = False Then
    '     Me.MnuAdministracion.Enabled = True
    '     Me.MnuAdministracion.Visible = True
    '  End If
    '  'R1249
    'LJSA M4703 ---- Se pone en comentario y se habilita el c�digo anterior --
      
      If ValVersion = False Then Me.MnuMenu.Enabled = False
      Call MDIForm_Activate
      If ValVersion = True Then FrmMenu.Show
   End If
End Sub

Private Sub MDIForm_Activate()
   Dim Result As Integer
   Dim I As Integer
   
   ProgName = App.Title
   
   Call RelojMDI_Timer
   If (EjecutarInicio) Then
      EjecutarInicio = False
      Call MouseClock
      'FrmInicio.Show 1
      Call MouseNorm
      Call Connection(Connect)
   Else
      If (Not ConnActive()) Then
         Connect = False
         Call EstadoConexion
      End If
   End If
   If (Not Connect) Then Exit Sub
   Call MouseNorm
   'Mnu_Conexion.Caption = BDNameStr smdl
   
    ' APGR IMAGENES M2894 - INICIO
    Call ImagenesMenu(MDI_Inventarios, 0, 1, MDI_Inventarios.ImageList1.ListImages(1).Picture)
    Call ImagenesMenu(MDI_Inventarios, 0, 0, MDI_Inventarios.ImageList1.ListImages(2).Picture)
    ' APGR IMAGENES M2894 - FIN
   
 End Sub

Private Sub MDIForm_Load()
    'HRR M1466
    Static Process, objWMIService, colProcesses
    Dim StProc As String
    Dim ArProcEje() As String
    Dim InI As Integer
    Dim BoFlag As Boolean
    ReDim ArProcEje(0)
    Dim Hora As String
   Hora = "000000"
    'HRR M1466
    Me.Left = -15
    Me.Top = -15
    Call MouseClock
    ProgName = "InveAdonew"
    ININame = "InveAdonew"
    OrigFecha = "Servidor"
    EjecutarInicio = True
    Set h = frmAyuda '|.DR.|
    Call InitApp
'    vDifEnPesos = CSng(GetINIString("Parametros", "DifEnPesos", "0.01"))
    'Call SetBDParams(BDNameStr, BDTypeCur, BDDSNODBCName)
    '/****************** Natalia
    Dim Strfilename As String
    Dim lngCount As Long
    Dim fecha As String

    Strfilename = String(255, 0)
    lngCount = GetModuleFileName(App.hInstance, Strfilename, 255)
    Strfilename = Left(Strfilename, lngCount)
    If Not (UCase(Right(Strfilename, 7)) = "VB6.EXE") Then
       'fecha = Format(FileDateTime(App.Path & "\" & App.EXEName & ".vbp"), "dd/mm/yyyy hh:mm:ss AMPM")
    'Else
       fecha = Format(FileDateTime(App.Path & "\" & App.EXEName & ".exe"), "dd/mm/yyyy hh:mm:ss AMPM")
    End If
     MDI_Inventarios.Pnl_Version = "Versi�n Software: " & App.Major & "." & App.Minor & "." & App.Revision & "  -  Fecha Ejecutable: " & fecha
     MDI_Inventarios.pnl_conexion = NombBD 'SMDL M1512
   '******************/
   
   'HRR M1466
   If NumStation <> 0 Then
        StProc = GetSetting("INVENTARIO", "CONEXIONES", "Procesos", "")
               
        ' Accedemos a los Objetos necesarios ( Servicio WMI )
        '         WMI = WINDOWS MANAGEMENT INSTRUMENTATION
        'Set objWMIService = GetObject("winmgmts:" _
        '    & "{impersonationLevel=impersonate}!\\" & Punto & "\root\cimv2")
        Set objWMIService = GetObject("winmgmts:")
        ' Y dentro del Objeto WMI accedemos a la columna de procesos
        'Set colProcesses = objWMIService.ExecQuery("select * from win32_process where name='cnt-inventarios.exe'")
        Set colProcesses = objWMIService.ExecQuery("select name,processid,creationdate from win32_process where name='cnt-inventarios.exe'")
                    
        For Each Process In colProcesses
           If Hora < Mid(Process.creationdate, 1, 14) Then
              Hora = Mid(Process.creationdate, 1, 14)
              StIdProcAct = CStr(Process.ProcessId)
           End If
           If UCase(Process.Name) = "CNT-INVENTARIOS.EXE" Then
              ArProcEje(UBound(ArProcEje)) = CStr(Process.ProcessId)
              ReDim Preserve ArProcEje(UBound(ArProcEje) + 1)
           End If
            
        Next
        
        ' Y Descargamos los objetos
        Set objWMIService = Nothing
        Set colProcesses = Nothing
        Set Process = Nothing
    
'        InI = 0
'        BoFlag = True
'        If StProc <> NUL$ Then
'           For InI = 0 To UBound(ArProcEje)
'              BoFlag = True
'              If InStr(StProc, ArProcEje(InI)) Then BoFlag = False
'              If BoFlag Then Exit For
'           Next
'        End If
'
'        If BoFlag Then StIdProcAct = ArProcEje(InI)
        
        StProc = Join(ArProcEje, ",")
        If InStrRev(StProc, ",") = Len(StProc) And Len(StProc) <> 0 Then StProc = Mid(StProc, 1, Len(StProc) - 1)
        
       ' SaveSetting "INVENTARIO", "CONEXIONES", "Procesos", StProc 'JACC R2307 Se coloca en comentario
        
    End If
   'HRR M1466
   
End Sub

Private Sub MDIForm_Unload(Cancel As Integer)

   Dim Arr(1) 'HRR R1249
   Dim numconact As Long 'HRR R1249
   
   Call MouseNorm
   If (DeseaSalir() = False) Then
      Cancel = True
      Exit Sub
   End If
   
   'HRR R1249
   If IdContConn Then
         
      If (BeginTran("Control de licencia") <> FAIL) Then
         'JACC R2307 Se coloca en comentario
'         Result = DoUpdate("PARAMETROS_INVE", "NU_NUMESTA_PINV=NU_NUMESTA_PINV", NUL$)
'         'Result = LoadData("PARAMETROS_INVE", "NU_NUMESTA_PINV,FE_LICE_PINV", NUL$, Arr())
'         Result = LoadData("PARAMETROS_INVE", "NU_NUMESTA_PINV,NU_NUMELIC_PINV", NUL$, Arr()) 'AASV RLICENCIA
'
'         If Result <> FAIL And Encontro Then
'            'If CDate(Format(Fechlic, "dd/mm/yyyy")) = CDate(Format(Arr(1), "dd/mm/yyyy")) Then
'            If DbNumLic = Arr(1) Then 'AASV RLICENCIA
'               numconact = CLng(Encriptar(ConvertHexaCadena(UCase(CStr(Arr(0)))), UCase(Trim(IdEntidad))))
'               numconact = numconact - 1
'               'HRR M1466
'               If numconact >= 0 Then
'                  Valores = "NU_NUMESTA_PINV=" & Comi & ConvertCadenaHexa(Encriptar(UCase(Trim(numconact)), UCase(Trim(IdEntidad)))) & Comi
'                  Result = DoUpdate("PARAMETROS_INVE", Valores, NUL$)
'               End If
'               'HRR M1466
'            End If
'         End If
         'JACC R2307 Fin Comentario
         ''JACC R2307 BORRAR CONEXION
            If NumStation <> 0 Then
                    Call Conection_On(True, True)
            End If
         'JACC R2307
         
         
         
         'HRR M1466
         If Result <> FAIL Then
            If CommitTran() = FAIL Then
               Call RollBackTran
            End If
         Else
            Call RollBackTran
         End If
         'HRR M1466
         
         'HRR M1466
         If MotorBD = "ACCESS" Then
            Arr(0) = GetSetting("INVENTARIO", "CONEXIONES", DirDB & NombBD & "/Procesos", "")
            Arr(0) = Replace(Arr(0), StIdProcAct, "")
            Arr(0) = Replace(Arr(0), ",,", ",")
            Arr(0) = IIf(InStr(Arr(0), ",") = 1, Mid(Arr(0), 2), Arr(0))
            If InStrRev(Arr(0), ",") = Len(Arr(0)) And Len(Arr(0)) <> 0 Then Arr(0) = Mid(Arr(0), 1, Len(Arr(0)) - 1)
            SaveSetting "INVENTARIO", "CONEXIONES", DirDB & NombBD & "/Procesos", Arr(0)
         Else
             'JACC R2307 Se coloca en comentario
'            Arr(0) = GetSetting("INVENTARIO", "CONEXIONES", ServName & "/" & NombBD & "/Procesos", "")
'            Arr(0) = Replace(Arr(0), StIdProcAct, "")
'            Arr(0) = Replace(Arr(0), ",,", ",")
'            Arr(0) = IIf(InStr(Arr(0), ",") = 1, Mid(Arr(0), 2), Arr(0))
'            If InStrRev(Arr(0), ",") = Len(Arr(0)) And Len(Arr(0)) <> 0 Then Arr(0) = Mid(Arr(0), 1, Len(Arr(0)) - 1)
'            SaveSetting "INVENTARIO", "CONEXIONES", ServName & "/" & NombBD & "/Procesos", Arr(0)
            'JACC R2307 Se coloca en comentario
         End If
         'HRR M1466
         
      End If
         
   End If
   'R1249
  'JACC R2307 Se coloca en comentario
   'HRR M1466
'   If NumStation <> 0 And StIdProcAct <> NUL$ Then
'        Arr(0) = GetSetting("INVENTARIO", "CONEXIONES", "Procesos", "")
'        Arr(0) = Replace(Arr(0), StIdProcAct, "")
'        Arr(0) = Replace(Arr(0), ",,", ",")
'        Arr(0) = IIf(InStr(Arr(0), ",") = 1, Mid(Arr(0), 2), Arr(0))
'        If InStrRev(Arr(0), ",") = Len(Arr(0)) And Len(Arr(0)) <> 0 Then Arr(0) = Mid(Arr(0), 1, Len(Arr(0)) - 1)
'        SaveSetting "INVENTARIO", "CONEXIONES", "Procesos", Arr(0)
'   End If
   'HRR M1466
   'JACC R2307 Fim Comentario
   
'   Result = DoUpdate("CONEXIONES", "FECHA_DESCONEXION=" & FFECHAHORA(Nowserver), "NU_NUME_CONE=" & NumConex)
   Call ExitApp
End Sub

Private Sub Mnu_Salir_Click()
  Call MouseClock
  Unload Me
End Sub

Private Sub MnuAcerca_Click()
   FrmAcercaDe.Show
'    frmPrmUNIDADES.Show
End Sub

Private Sub MnuACTUALIZA_Click()
    Call ActualizarBDSQL
End Sub

Private Sub MnuMenu_Click()
   FrmMenu.Show
End Sub

Private Sub MnuTemas_Click()
''   FrmCuadroComision.Show
'   SendKeys "{F1}", True
'    frmPrmDOCINTERNOS.Show
End Sub

Private Sub RelojMDI_Timer()
   Pnl_Fecha.Caption = ESP$ & Format(FechaServer, "dd/mm/yyyy") & ESP$ & ESP$ & Format(Nowserver, "hh:mm AM/PM") 'AASV R2240 SE QUITA COMENTARIO
End Sub

