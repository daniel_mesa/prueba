VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "MSFLXGRD.OCX"
Begin VB.Form FrmDesImp 
   Caption         =   "Impuestos - Descuentos"
   ClientHeight    =   4905
   ClientLeft      =   60
   ClientTop       =   450
   ClientWidth     =   10050
   Icon            =   "FrmDesImp.frx":0000
   LinkTopic       =   "Form1"
   ScaleHeight     =   4905
   ScaleWidth      =   10050
   StartUpPosition =   3  'Windows Default
   Begin VB.Frame FraFondo 
      Height          =   4335
      Left            =   3600
      TabIndex        =   8
      Top             =   480
      Width           =   6375
      Begin VB.CommandButton CmdTerminar 
         Caption         =   "&TERMINAR"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   6
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   495
         Left            =   5040
         Picture         =   "FrmDesImp.frx":058A
         Style           =   1  'Graphical
         TabIndex        =   10
         Top             =   3720
         Width           =   1215
      End
      Begin VB.TextBox TxtVlBase 
         Height          =   285
         Left            =   1320
         Locked          =   -1  'True
         TabIndex        =   9
         Top             =   3780
         Visible         =   0   'False
         Width           =   1455
      End
      Begin MSFlexGridLib.MSFlexGrid GrdDeIm 
         Height          =   3375
         Left            =   120
         TabIndex        =   11
         Top             =   240
         Width           =   6135
         _ExtentX        =   10821
         _ExtentY        =   5953
         _Version        =   393216
         Rows            =   1
         Cols            =   12
         FixedCols       =   0
         AllowUserResizing=   1
      End
      Begin VB.Label LbBase 
         Caption         =   "Valor Base"
         Height          =   255
         Left            =   360
         TabIndex        =   12
         Top             =   3795
         Visible         =   0   'False
         Width           =   855
      End
   End
   Begin VB.Frame Frame1 
      Caption         =   "Lista"
      Height          =   4335
      Left            =   120
      TabIndex        =   6
      Top             =   480
      Width           =   2655
      Begin MSComctlLib.TreeView TrvLista 
         Height          =   3975
         Left            =   120
         TabIndex        =   7
         Top             =   240
         Width           =   2415
         _ExtentX        =   4260
         _ExtentY        =   7011
         _Version        =   393217
         Indentation     =   176
         LineStyle       =   1
         Style           =   7
         HotTracking     =   -1  'True
         Appearance      =   1
      End
   End
   Begin VB.CommandButton Cmd_agregar 
      Height          =   255
      Left            =   2880
      Picture         =   "FrmDesImp.frx":08CC
      Style           =   1  'Graphical
      TabIndex        =   5
      Top             =   2160
      Width           =   615
   End
   Begin VB.CommandButton Cmd_Quitar 
      Height          =   255
      Left            =   2880
      Picture         =   "FrmDesImp.frx":0C1E
      Style           =   1  'Graphical
      TabIndex        =   4
      Top             =   2640
      Width           =   615
   End
   Begin VB.PictureBox picHelp 
      Appearance      =   0  'Flat
      AutoSize        =   -1  'True
      BackColor       =   &H80000005&
      BorderStyle     =   0  'None
      ForeColor       =   &H80000008&
      Height          =   120
      Index           =   0
      Left            =   7035
      ScaleHeight     =   120
      ScaleWidth      =   120
      TabIndex        =   2
      Tag             =   $"FrmDesImp.frx":0F70
      ToolTipText     =   "Ayuda"
      Top             =   120
      Width           =   120
   End
   Begin VB.ComboBox cmbConcepto 
      Height          =   315
      Left            =   2880
      Style           =   2  'Dropdown List
      TabIndex        =   1
      Top             =   120
      Width           =   4095
   End
   Begin VB.ListBox lstConcepto 
      Height          =   255
      ItemData        =   "FrmDesImp.frx":106E
      Left            =   7560
      List            =   "FrmDesImp.frx":1070
      TabIndex        =   0
      Top             =   120
      Visible         =   0   'False
      Width           =   1335
   End
   Begin VB.Label lblEtiq 
      Alignment       =   1  'Right Justify
      AutoSize        =   -1  'True
      Caption         =   "Filtrar por concepto"
      Height          =   195
      Index           =   2
      Left            =   1440
      TabIndex        =   3
      Top             =   120
      Width           =   1365
   End
End
Attribute VB_Name = "FrmDesImp"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'HRR R1853
Option Explicit
Public DbBase As Double
Public LblQue  As Variant
Public AutoRet As String, AutRetIva As String, AutRetIca As String
Public TipoRegimen As String, GranContrib As String
Public ExentoIva As String, ExentoIca As String, ExentoFuente As String
Public TipoPers As String
Dim BoLoad   As Boolean
Dim ArDesc() As Variant
Dim ArImp() As Variant
Dim StConcepto As String
Public BoArbol As Boolean
Dim BoIvaOtrosImp As Boolean
Dim DbOtrosDesc As Double
Dim DbOtrosImp As Double
Public DbIVA As Double
Public ClssImpDes As ImpuDesc
Dim VrValBase() As Variant 'JAUM T28644-R27752 Almacena en un vector el c�digo del impuesto y el valor base
Dim InCanEntra As Integer 'JAUM T28644-R27752 Almacena la cantidad de entradas que tiene la compra

Private Sub cmbConcepto_KeyPress(KeyAscii As Integer)
   Call Cambiar_Enter(KeyAscii)
   
End Sub

Private Sub cmbConcepto_LostFocus()
   
   lstConcepto.ListIndex = cmbConcepto.ListIndex
   If StConcepto <> lstConcepto.Text Then
      If MsgBox("�Esta seguro de cambiar el concepto ?", vbQuestion + vbYesNo) = vbYes Then
         TrvLista.Nodes.Clear
         StConcepto = lstConcepto.Text
         ClssImpDes.concepto = StConcepto
         GrdDeIm.Rows = 2
         Call Cmd_Quitar_Click
         Call Buscar_Descuentos
         Call Buscar_Impuestos
         Call Cambiar_Descuentos
         Call Cambiar_Impuestos
         Call Copiar_ImpDesc_Grid
         
         
      End If
   End If
   
End Sub

'Agrega un elemento de la lista al grid
Private Sub Cmd_agregar_Click() 'SI SE UTILIZA TIENE QUE REVIZAR EL CODIGO
Dim texto      As String
Dim cadena     As String

Dim Cadena2   As String
Dim aux As String

   With GrdDeIm
      Call MouseClock

      If Instanciado(TrvLista.SelectedItem) = False Then MouseNorm: Exit Sub
      If Instanciado(TrvLista.SelectedItem.Parent) Then
         texto = TrvLista.SelectedItem.Key
         cadena = Left(texto, IIf(InStr(1, texto, "|") - 1 < 1, 0, InStr(1, texto, "|") - 1))
         If TrvLista.SelectedItem.Parent = "Descuentos" Then
            If BuscarItem(cadena, 0) = False Then
               cadena = Right(texto, Len(texto) - InStr(1, texto, "|"))
               If Mid(TrvLista.SelectedItem.Tag, 1, 1) = "P" Then
                  aux = Replace(TrvLista.SelectedItem.Tag, "P|", "")
                  aux = Mid(aux, 1, IIf(InStr(1, aux, "|") - 1 < 0, Len(aux), InStr(1, aux, "|") - 1))
                  Cadena2 = Left(TrvLista.SelectedItem.Key, InStr(1, TrvLista.SelectedItem.Key, "|") - 1) 'codigo
                  Cadena2 = Cadena2 & vbTab
                  Cadena2 = Cadena2 & vbTab & cadena 'Descripcion
                  Cadena2 = Cadena2 & vbTab & aux 'porcentaje
                  Cadena2 = Cadena2 & vbTab & Round(((DbBase * aux) / 100), 0) 'valor
                  Cadena2 = Cadena2 & vbTab & "R" 'Suma - Resta
                  Cadena2 = Cadena2 & vbTab & "P" 'Tipo
                  aux = Replace(TrvLista.SelectedItem.Tag, "P|", "")
                  If InStr(1, aux, "|") = 0 Then
                    aux = 0
                  Else
                    aux = Mid(aux, InStr(1, aux, "|") + 1, Len(aux))
                  End If
                  Cadena2 = Cadena2 & vbTab & aux 'Tope
                  Cadena2 = Cadena2 & vbTab & "D"
                  .AddItem Cadena2
                  .TextMatrix(.Rows - 1, 4) = Format(GrdDeIm.TextMatrix(GrdDeIm.Rows - 1, 4), "#,###.#0")
               Else
                  aux = Replace(TrvLista.SelectedItem.Tag, "V|", "")
                  aux = Mid(aux, 1, IIf(InStr(1, aux, "|") - 1 < 0, Len(aux), InStr(1, aux, "|") - 1))
                  Cadena2 = Left(TrvLista.SelectedItem.Key, InStr(1, TrvLista.SelectedItem.Key, "|") - 1) 'codigo
                  Cadena2 = Cadena2 & vbTab
                  Cadena2 = Cadena2 & vbTab & cadena 'Descripcion
                  Cadena2 = Cadena2 & vbTab & "0" 'porcentaje
                  Cadena2 = Cadena2 & vbTab & aux 'valor
                  Cadena2 = Cadena2 & vbTab & "R" 'Suma - Resta
                  Cadena2 = Cadena2 & vbTab & "V" 'Tipo
                  aux = Replace(TrvLista.SelectedItem.Tag, "V|", "")
                  If InStr(1, aux, "|") = 0 Then
                    aux = 0
                  Else
                    aux = Mid(aux, InStr(1, aux, "|") + 1, Len(aux))
                  End If
                  Cadena2 = Cadena2 & vbTab & ConDoble(aux) 'Tope
                  Cadena2 = Cadena2 & vbTab & "D"
                  .AddItem Cadena2
                  .TextMatrix(.Rows - 1, 4) = Format(GrdDeIm.TextMatrix(GrdDeIm.Rows - 1, 4), "#,###.#0")
               End If
            End If
         Else
            If BuscarItem(cadena, 1) = False Then
               aux = Mid(TrvLista.SelectedItem.Tag, 1, InStr(1, TrvLista.SelectedItem.Tag, "|") - 1)
               cadena = Right(texto, Len(texto) - InStr(1, texto, "|"))
               Cadena2 = vbTab & Left(TrvLista.SelectedItem.Key, InStr(1, TrvLista.SelectedItem.Key, "|") - 1) 'codigo
               Cadena2 = Cadena2 & vbTab & cadena 'descripcion
               Cadena2 = Cadena2 & vbTab & aux 'Porcentaje
               'Cadena2 = Cadena2 & vbTab & Round(((DbBase * aux) / 100), 0) 'valor
               ''JACC R2345
               If TipoPers = "0" Then
                  ReDim ArrDeImp(0)
                  Condicion = "CD_CODI_IMPU =" & Comi & Mid(Left(TrvLista.SelectedItem.Key, InStr(1, TrvLista.SelectedItem.Key, "|") - 1), 2, Len(Left(TrvLista.SelectedItem.Key, InStr(1, TrvLista.SelectedItem.Key, "|") - 1))) & Comi
                  Result = LoadData("TC_IMPUESTOS", "TX_CODI_DESC_IMPU", Condicion, ArrDeImp)
                  If Result <> FAIL And Encontro And ArrDeImp(0) <> NUL$ Then
                     Cadena2 = Cadena2 & vbTab & Round((((DbBase - Valor_Descuento(ArrDeImp(0), DbBase)) * aux) / 100), 0) 'valor
                  Else
                     Cadena2 = Cadena2 & vbTab & Round(((DbBase * aux) / 100), 0) 'valor
                  End If
               Else
                  Cadena2 = Cadena2 & vbTab & Round(((DbBase * aux) / 100), 0) 'valor
               End If
               'JACC R2345
               
               
               
               Cadena2 = Cadena2 & vbTab & "R" 'Suma - Resta
               Cadena2 = Cadena2 & vbTab & "P" 'Tipo
               aux = Mid(TrvLista.SelectedItem.Tag, InStr(1, TrvLista.SelectedItem.Tag, "|") + 1, Len(TrvLista.SelectedItem.Tag))
               Cadena2 = Cadena2 & vbTab & ConDoble(aux) 'tope
               Cadena2 = Cadena2 & vbTab & "I"
               ''JACC R2345 agregar valor base
               If ArrDeImp(0) <> NUL$ And TipoPers = "0" Then
                  Cadena2 = Cadena2 & vbTab & vbTab & DbBase - Valor_Descuento(ArrDeImp(0), DbBase) 'valor
               Else
                  Cadena2 = Cadena2 & vbTab & vbTab & DbBase  'valor
               End If
               'JACC R2345
               
               
               .AddItem Cadena2
               .TextMatrix(.Rows - 1, 4) = Format(GrdDeIm.TextMatrix(GrdDeIm.Rows - 1, 4), "#,###.#0")
            End If
         End If
      End If
      Call MouseNorm
   End With
   
  Dim I As Variant
  If GrdDeIm.Rows > 1 Then
      For I = 1 To GrdDeIm.Rows - 1
         GrdDeIm.Row = I
         GrdDeIm.Col = 3 ' Para que revise el valor base
         GrdDeIm.Col = 4
         GrdDeIm.Col = 3 ' Para que revise el valor base
      Next
  End If
End Sub

'Verifica si un elemento que se vaya a agregar ya exite en la lista
Private Function BuscarItem(ByVal concepto As String, ByVal columna As Integer) As Integer
Dim I As Integer
   BuscarItem = False
   With GrdDeIm
      For I = 1 To .Rows - 1
         If .TextMatrix(I, columna) = concepto Then
            .Row = I
            .Col = 1
            .SetFocus
            BuscarItem = True
            Call Mensaje1("Este concepto ya esta seleccionado", 3)
            Exit For
         End If
      Next I
   End With
End Function

'Elimina un registro del grid
Private Sub Cmd_Quitar_Click()
Dim I
   If GrdDeIm.Rows = 2 Then
      For I = 0 To GrdDeIm.Cols - 1
         GrdDeIm.TextMatrix(1, I) = ""
      Next I
      GrdDeIm.Rows = 1
   ElseIf GrdDeIm.Row > 1 Then
      GrdDeIm.RemoveItem (GrdDeIm.Row)
   End If
End Sub

Private Sub Form_Load()
   
   Call CenterForm(MDI_Inventarios, Me)
   Call GrdFDef(GrdDeIm, 0, 0, ",1,,1,Descripcion,2100,%,735,Valor,1300,Suma(S) - Resta(R),1600,,1000,,1,,1,,1000,Valor Base,950,,1") 'JAUM T28644-R27752
   Dim ArrI() As Variant
   ReDim arr(4, 0)
   
   Call Cargar_Concepto
   StConcepto = ClssImpDes.ConceptoRef
     
   cmbConcepto.ListIndex = FindStrInCtrl(lstConcepto, StConcepto)
      
   If BoArbol = False Then
      
     TrvLista.Enabled = False
     Cmd_agregar.Enabled = False
     Cmd_Quitar.Enabled = False
         
   End If
   
    'JLPB T23820 INICIO SE DEJA EN COMENTARIO EL SIGUIENTE BLOQUE
'     'GAVL T6265 SENTENCIA PARA SABER SI LA OPCION ESTA ACTIVA EN CXP
'    If boICxP Then
'        If ExisteCAMPO("PARAMETROS_CXP", "NU_REDIMDE_PCXP") Then '
'            frmFaseCompra.BoAproxCent = CBool(fnDevDato("PARAMETROS_CXP", "NU_REDIMDE_PCXP", NUL$))
'        End If
'    End If
'    'FIN GAVL T6265
    'JLPB T23820 FIN
   
   Call Buscar_Descuentos
   Call Buscar_Impuestos
   
   Call Copiar_ImpDesc_Grid
   TxtVlBase.Text = DbBase
   
    
   
End Sub

Private Sub GrdDeIm_KeyDown(KeyCode As Integer, Shift As Integer)
   'JAUM T31312 Inicio
   If GrdDeIm.TextMatrix(GrdDeIm.Row, 8) = "D" Then
      If GrdDeIm.TextMatrix(GrdDeIm.Row, 6) = "V" Then
         If InCanEntra = 1 Then
            If KeyCode = 46 Then GrdDeIm.Text = ""
         ElseIf KeyCode = 46 Then
            Call Mensaje1("El valor no se puede modificar porque se ha seleccionado m�s de una entrada", 1)
         End If
      Else
         If KeyCode = 46 Then GrdDeIm.Text = ""
      End If
   Else
   'JAUM T31312 Fin
      If KeyCode = 46 Then GrdDeIm.Text = ""
   End If 'JAUM T31312
End Sub

Private Sub GrdDeIm_KeyPress(KeyAscii As Integer)
   'JAUM T28644-R27752 Inicio
   If GrdDeIm.TextMatrix(GrdDeIm.Row, 11) = NUL$ Then GoTo Siguiente
   If GrdDeIm.TextMatrix(GrdDeIm.Row, 11) = False Then
Siguiente:
   'JAUM T28644-R27752 Fin
      Select Case GrdDeIm.Col
         Case 3:
            If GrdDeIm.TextMatrix(GrdDeIm.Row, 8) = "D" Then
               If GrdDeIm.TextMatrix(GrdDeIm.Row, 6) = "P" Then Call ValidarPuntoFGrd(GrdDeIm, GrdDeIm.Col, KeyAscii, 6) 'PORCENTAJE
               If KeyAscii = 13 Then
                  GrdDeIm.Col = 4
                  GrdDeIm.Col = 3
               End If
            ElseIf GrdDeIm.TextMatrix(GrdDeIm.Row, 8) = "I" Then
               If GrdDeIm.TextMatrix(GrdDeIm.Row, 6) = "V" Or GrdDeIm.TextMatrix(GrdDeIm.Row, 6) = "P" Then Call ValidarPuntoFGrd(GrdDeIm, GrdDeIm.Col, KeyAscii, 10) 'VALOR O PORCENTAJE
               If KeyAscii = 13 Then
                  GrdDeIm.Col = 4
                  GrdDeIm.Col = 3
               End If
            End If
         Case 4:
            If GrdDeIm.TextMatrix(GrdDeIm.Row, 8) = "D" Then
               'JAUM T31312 Inicio Se deja linea en comentario
               'If GrdDeIm.TextMatrix(GrdDeIm.Row, 6) = "V" Or GrdDeIm.TextMatrix(GrdDeIm.Row, 6) = "P" Then Call ValidarPuntoFGrd(GrdDeIm, GrdDeIm.Col, KeyAscii, 10) 'VALOR O PORCENTAJE
               If GrdDeIm.TextMatrix(GrdDeIm.Row, 6) = "V" Then
                  If InCanEntra = 1 Then
                     Call ValidarPuntoFGrd(GrdDeIm, GrdDeIm.Col, KeyAscii, 10) 'VALOR O PORCENTAJE
                  ElseIf (En(KeyAscii, Asc(SCero), Asc("9"))) Or (KeyAscii = Asc(",")) Or (KeyAscii = Asc(".")) Or (KeyAscii = 8) Then
                     Call Mensaje1("El valor no se puede modificar porque se ha seleccionado m�s de una entrada", 3)
                  End If
               ElseIf GrdDeIm.TextMatrix(GrdDeIm.Row, 6) = "P" Then
                  Call ValidarPuntoFGrd(GrdDeIm, GrdDeIm.Col, KeyAscii, 10) 'VALOR O PORCENTAJE
               End If
               'JAUM T31312 Fin
               If KeyAscii = 13 Then
                  GrdDeIm.Col = 4
                  GrdDeIm.Col = 3
               End If
            ElseIf GrdDeIm.TextMatrix(GrdDeIm.Row, 8) = "I" Then
               If GrdDeIm.TextMatrix(GrdDeIm.Row, 0) = "I" Or GrdDeIm.TextMatrix(GrdDeIm.Row, 0) = "O" Then Exit Sub 'Si el impuesto es tipo iva o impuesto otros impuestos
               If GrdDeIm.TextMatrix(GrdDeIm.Row, 6) = "V" Or GrdDeIm.TextMatrix(GrdDeIm.Row, 6) = "P" Then Call ValidarPuntoFGrd(GrdDeIm, GrdDeIm.Col, KeyAscii, 10) 'VALOR O PORCENTAJE
               If KeyAscii = 13 Then
                  GrdDeIm.Col = 4
                  GrdDeIm.Col = 3
               End If
            End If
         Case 5:
            'HRR R1853
            'If GrdDeIm.TextMatrix(GrdDeIm.Row, 8) = "I" Then
            'If UCase(Chr(KeyAscii)) = "R" Or UCase(Chr(KeyAscii)) = "S" Then
            'GrdDeIm.Text = UCase(Chr(KeyAscii))
            'End If
            'End If
            'HRR R1853
         End Select
   'JAUM T28644-R27752 Inicio
   Else
      Select Case GrdDeIm.Col
         Case 4:
            Call ValidarPuntoFGrd(GrdDeIm, GrdDeIm.Col, KeyAscii, 10)
            If IsNumeric(GrdDeIm.TextMatrix(GrdDeIm.Row, 4)) Then
               If GrdDeIm.TextMatrix(GrdDeIm.Row, 4) > DbBase Then
                  Call Mensaje1("El valor ingresado no puede ser mayor al valor final", 3)
                  Call ValidarPuntoFGrd(GrdDeIm, GrdDeIm.Col, 8, 10)
               End If
            End If
         Case 10:
            Call ValidarPuntoFGrd(GrdDeIm, GrdDeIm.Col, KeyAscii, 10)
            If IsNumeric(GrdDeIm.TextMatrix(GrdDeIm.Row, 10)) Then
               If GrdDeIm.TextMatrix(GrdDeIm.Row, 10) > DbBase Then
                  Call Mensaje1("El valor ingresado no puede ser mayor al valor final", 3)
                  Call ValidarPuntoFGrd(GrdDeIm, GrdDeIm.Col, 8, 10)
               End If
            End If
      End Select
   End If
   'JAUM T28644-R27752 Fin
End Sub
Private Sub GrdDeIm_LeaveCell()
   If BoLoad = True Then Exit Sub
   Dim dbValor As Variant
   
   Select Case GrdDeIm.Col
      Case 3:
         If GrdDeIm.TextMatrix(GrdDeIm.Row, 8) = "D" Then
                 
            If GrdDeIm.TextMatrix(GrdDeIm.Row, 6) = "V" Then
               Call calcular_cxp
                   
               Exit Sub
            End If
                
            If GrdDeIm.Text = NUL$ Then GrdDeIm.Text = 0
            If Not IsNumeric(GrdDeIm.Text) Then GrdDeIm.Text = 0
            If DbBase = 0 Then
               dbValor = 0
            Else
               dbValor = ((GrdDeIm.TextMatrix(GrdDeIm.Row, 3) * DbBase) / 100)
            End If
            GrdDeIm.TextMatrix(GrdDeIm.Row, 4) = dbValor
            GrdDeIm.TextMatrix(GrdDeIm.Row, 4) = Format(GrdDeIm.TextMatrix(GrdDeIm.Row, 4), "#,###.#0")
            Call calcular_cxp
            ElseIf GrdDeIm.TextMatrix(GrdDeIm.Row, 8) = "I" Then
               If GrdDeIm.Text = NUL$ Then GrdDeIm.Text = 0
               If Not IsNumeric(GrdDeIm.Text) Then GrdDeIm.Text = 0
               If DbBase = 0 Then
                  dbValor = 0
               Else
                  Call calcular_cxp
                  Exit Sub
                   
               End If
               GrdDeIm.TextMatrix(GrdDeIm.Row, 4) = dbValor
               GrdDeIm.TextMatrix(GrdDeIm.Row, 4) = Format(GrdDeIm.TextMatrix(GrdDeIm.Row, 4), "#,###.#0")
            End If
      Case 4:
         If GrdDeIm.TextMatrix(GrdDeIm.Row, 8) = "D" Then
            If GrdDeIm.TextMatrix(GrdDeIm.Row, 6) = "P" Then
               If GrdDeIm.Text = NUL$ Then GrdDeIm.Text = 0
               If Not IsNumeric(GrdDeIm.Text) Then GrdDeIm.Text = 0
               If DbBase = 0 Then
                  dbValor = 0
               Else
                  dbValor = ((GrdDeIm.TextMatrix(GrdDeIm.Row, 4) * 100) / DbBase)
               End If
                   
               GrdDeIm.TextMatrix(GrdDeIm.Row, 3) = Format(dbValor, "#0.####")
                   
            Else
               If GrdDeIm.Text = NUL$ Then GrdDeIm.Text = 0
               If Not IsNumeric(GrdDeIm.Text) Then GrdDeIm.Text = 0
                   
            End If
            GrdDeIm.TextMatrix(GrdDeIm.Row, 4) = Format(GrdDeIm.TextMatrix(GrdDeIm.Row, 4), "#,###.#0")
            Call calcular_cxp
         ElseIf GrdDeIm.TextMatrix(GrdDeIm.Row, 8) = "I" Then
            If GrdDeIm.Text = NUL$ Then GrdDeIm.Text = 0
            If Not IsNumeric(GrdDeIm.Text) Then GrdDeIm.Text = 0
            If DbBase = 0 Then
               dbValor = 0
            Else
               If GrdDeIm.TextMatrix(GrdDeIm.Row, 11) = False Then 'JAUM T28644-R27752
                  Call calcular_PorcImp
                  Exit Sub
               End If 'JAUM T28644-R27752
                      
            End If
            If GrdDeIm.TextMatrix(GrdDeIm.Row, 11) = False Then 'JAUM T28644-R27752
               GrdDeIm.TextMatrix(GrdDeIm.Row, 3) = Format(dbValor, "#0.####")
            End If 'JAUM T28644-R27752
            GrdDeIm.TextMatrix(GrdDeIm.Row, 4) = Format(GrdDeIm.TextMatrix(GrdDeIm.Row, 4), "#,###.#0")
         End If
      'JAUM T28644-R27752 Inicio
      Case 10:
         If GrdDeIm.TextMatrix(GrdDeIm.Row, 11) = NUL$ Then GoTo Siguiente
         If GrdDeIm.TextMatrix(GrdDeIm.Row, 11) = True Then
            If GrdDeIm.TextMatrix(GrdDeIm.Row, 10) = NUL$ Or GrdDeIm.TextMatrix(GrdDeIm.Row, 10) = "." Then GrdDeIm.TextMatrix(GrdDeIm.Row, 10) = "0"
            If CDbl(GrdDeIm.TextMatrix(GrdDeIm.Row, 4)) > GrdDeIm.TextMatrix(GrdDeIm.Row, 10) Then
               GrdDeIm.TextMatrix(GrdDeIm.Row, 10) = CDbl(GrdDeIm.TextMatrix(GrdDeIm.Row, 4))
            End If
            GrdDeIm.TextMatrix(GrdDeIm.Row, 10) = Format(GrdDeIm.TextMatrix(GrdDeIm.Row, 10), "###.#0")
         End If
Siguiente:
      'JAUM T28644-R27752 Fin
   End Select

End Sub
Private Sub CmdTerminar_Click()
   Unload Me
End Sub

Private Sub Form_Unload(Cancel As Integer)
Dim I As Integer
Dim J As Integer
'Dim Pos As Integer DEPURACION DE CODIGO
'Dim cadena As String DEPURACION DE CODIGO
  
   If GrdDeIm.Rows = 1 Then
      ReDim ArImp(6, 0)
      ReDim ArDesc(6, 0)
      Call ClssImpDes.ImpuestosRef(ArImp)
      Call ClssImpDes.DescuentosRef(ArDesc)
      Call ClssImpDes.SetTieneDatosRef(False)
      Call ClssImpDes.SetConceptoRef(StConcepto)
      Set ClssImpDes = Nothing
      Exit Sub
   End If
   

   ReDim ArDesc(6, 0)
   For I = 1 To GrdDeIm.Rows - 1
          
      If GrdDeIm.TextMatrix(I, 8) = "D" Then
         ReDim Preserve ArDesc(6, I - 1)
         For J = 1 To 4
            If J < 4 Then
               ArDesc(J - 1, I - 1) = GrdDeIm.TextMatrix(I, J)
            ElseIf J = 4 Then
               ArDesc(J, I - 1) = GrdDeIm.TextMatrix(I, J)
            End If
         Next J
         ArDesc(3, I - 1) = GrdDeIm.TextMatrix(I, 7)
         ArDesc(5, I - 1) = GrdDeIm.TextMatrix(I, 9)
         ArDesc(6, I - 1) = GrdDeIm.TextMatrix(I, 0)
      End If
   Next I
   DbOtrosDesc = sumar_items2(GrdDeIm, 4, "S", 5, 7, DbBase, "D") ' Suma todos los que digan sumando
   DbOtrosDesc = DbOtrosDesc - sumar_items2(GrdDeIm, 4, "R", 5, 7, DbBase, "D") ' Son todos lo que digan sumando menos los que digan restando

   ReDim ArImp(6, 0)
   ReDim VrValBase(2, 0) 'JAUM T28644-R27752
   For I = 1 To GrdDeIm.Rows - 1
             
      If GrdDeIm.TextMatrix(I, 8) = "I" Then
         ReDim Preserve ArImp(6, I - 1)
         ReDim Preserve VrValBase(2, I - 1) 'JAUM T28644-R27752
            For J = 1 To 4
               If J < 4 Then ArImp(J - 1, I - 1) = GrdDeIm.TextMatrix(I, J)
               If J = 4 Then ArImp(J, I - 1) = GrdDeIm.TextMatrix(I, J)
                   
            Next J
            ArImp(3, I - 1) = GrdDeIm.TextMatrix(I, 7)
            ArImp(5, I - 1) = GrdDeIm.TextMatrix(I, 9)
            ArImp(6, I - 1) = GrdDeIm.TextMatrix(I, 0)
            'JAUM T28644-R27752 Inicio
            VrValBase(0, I - 1) = ArImp(0, I - 1) 'C�digo
            VrValBase(1, I - 1) = ArImp(4, I - 1) 'Valor
            VrValBase(2, I - 1) = GrdDeIm.TextMatrix(I, 10) 'Valor Base
            'JAUM T28644-R27752 Fin
      End If
   Next I
   frmFaseCompra.VrVBase = VrValBase 'JAUM T28644-R27752
   DbOtrosImp = sumar_items2(GrdDeIm, 4, "S", 5, 7, DbBase, "I") ' Suma todos los que digan sumando
   DbOtrosImp = DbOtrosImp - sumar_items2(GrdDeIm, 4, "R", 5, 7, DbBase, "I") ' Son todos lo que digan sumando menos los que digan restando
         
   Call ClssImpDes.ImpuestosRef(ArImp)
   Call ClssImpDes.DescuentosRef(ArDesc)
   Call ClssImpDes.SetTieneDatosRef(True)
   Call ClssImpDes.SetConceptoRef(StConcepto)
   Set ClssImpDes = Nothing
   
   Call MouseNorm
End Sub


Private Function Instanciado(objeto As Object) As Boolean
Dim S As String
On Error GoTo Error
   S = objeto.Tag
   Instanciado = True
Exit Function
Error:
      If ERR.Number = 91 Then
         Instanciado = False
      Else
         Resume Next
      End If
End Function

Private Function sumar_items2(ByVal Grd As MSFlexGrid, ByVal columna As Long, ByVal tipo As String, ByVal col2 As Long, ByVal ColTope As Long, ByVal Base As Double, DescImp As String) As Double
Dim TOTAL As Double
  TOTAL = 0
  Grd.Col = columna
  Grd.Row = 1
  While Grd.Row < Grd.Rows - 1
   If Grd.Text <> "" And IsNumeric(Grd.Text) = True And _
    Grd.TextMatrix(Grd.Row, col2) = tipo And _
    Grd.TextMatrix(Grd.Row, ColTope) <= Base And Grd.TextMatrix(Grd.Row, 8) = DescImp Then
     TOTAL = TOTAL + Grd.Text
   End If
   Grd.Row = Grd.Row + 1
  Wend
  If Grd.Text <> "" And IsNumeric(Grd.Text) = True And _
    Grd.TextMatrix(Grd.Row, col2) = tipo And _
    ConDoble(Grd.TextMatrix(Grd.Row, ColTope)) <= Base And Grd.TextMatrix(Grd.Row, 8) = DescImp Then
     TOTAL = TOTAL + Grd.Text
  End If
  sumar_items2 = TOTAL
End Function


Private Sub calcular_cxp()
  Dim tdesb As Double 'total descuento al valor bruto
  Dim porce As Double
  
  Dim tdesbcond As Double 'total Descuento bruto condicionado para calcular impuesto retencion
  Dim I As Double 'HRR R1391
      
  tdesb = 0
  tdesbcond = 0
    
  For I = 1 To GrdDeIm.Rows - 1
     'descuentos
     If GrdDeIm.TextMatrix(I, 8) = "D" Then
     
        If CDbl(GrdDeIm.TextMatrix(I, 7)) <= DbBase Then
               
           If GrdDeIm.TextMatrix(I, 6) = "P" Then
                
              porce = CDbl(GrdDeIm.TextMatrix(I, 3))
              'GrdDeIm.TextMatrix(I, 4) = Format(((porce * DbBase) / 100), "#,###.#0")
              
              'INICIO GAVL T6265
              'If frmFaseCompra.BoAproxCent = True Then 'JLPB T23820 SE DEJA EN COMENTARIO
              If BoAproxCent Then 'JLPB T23820
                GrdDeIm.TextMatrix(I, 4) = AproxCentena(Round(Format(((porce * DbBase) / 100), "#,###.#0")))
              Else
                GrdDeIm.TextMatrix(I, 4) = Format(((porce * DbBase) / 100), "#,###.#0")
              End If
              'FIN  GAVL T6265
              
              tdesb = tdesb + GrdDeIm.TextMatrix(I, 4)
              'verifica si el descuento es condicionado
              If GrdDeIm.TextMatrix(I, 0) Then tdesbcond = tdesbcond + ((porce * DbBase) / 100)
                        
           ElseIf GrdDeIm.TextMatrix(I, 6) = "V" Then
              
              If CDbl(GrdDeIm.TextMatrix(I, 4)) <= DbBase Then
                 
                 'GrdDeIm.TextMatrix(I, 4) = Format(CDbl(GrdDeIm.TextMatrix(I, 4)), "#,###.#0")
                 
                 'INICIO GAVL T6265
                'If frmFaseCompra.BoAproxCent = True Then 'JLPB T23820 SE DEJA EN COMENTARIO
                If BoAproxCent Then 'JLPB T23820
                  GrdDeIm.TextMatrix(I, 4) = AproxCentena(Round(Format(CDbl(GrdDeIm.TextMatrix(I, 4)), "#,###.#0")))
                Else
                  GrdDeIm.TextMatrix(I, 4) = Format(CDbl(GrdDeIm.TextMatrix(I, 4)), "#,###.#0")
                End If
                'FIN  GAVL T6265
                 
                 tdesb = tdesb + GrdDeIm.TextMatrix(I, 4)
                 'verifica si el descuento es condicionado
                 If GrdDeIm.TextMatrix(I, 0) Then tdesbcond = tdesbcond + GrdDeIm.TextMatrix(I, 4)
                     
              End If
                   
           End If
        Else
               
           'GrdDeIm.TextMatrix(I, 4) = Format(0, "#,###.#0")
           
            'INICIO GAVL T6265
             'If frmFaseCompra.BoAproxCent = True Then 'JLPB T23820 SE DEJA EN COMENTARIO
             If BoAproxCent Then 'JLPB T23820
              GrdDeIm.TextMatrix(I, 4) = AproxCentena(Round(Format(0, "#,###.#0")))
             Else
              GrdDeIm.TextMatrix(I, 4) = Format(0, "#,###.#0")
             End If
             'FIN  GAVL T6265
        End If
        
     End If
     
  Next
      
     
  For I = 1 To GrdDeIm.Rows - 1
     
     If GrdDeIm.TextMatrix(I, 8) = "I" Then
      
        If GrdDeIm.TextMatrix(I, 3) <> "" And IsNumeric(GrdDeIm.TextMatrix(I, 3)) = True Then
           
           If GrdDeIm.TextMatrix(I, 4) = NUL$ Then GrdDeIm.TextMatrix(I, 4) = 0
                 
           'impuesto retencion
           If GrdDeIm.TextMatrix(I, 0) = "R" Then
                 
              If CDbl(GrdDeIm.TextMatrix(I, 7)) <= DbBase Then
                  
                 porce = CDbl(GrdDeIm.TextMatrix(I, 3))
                 'GrdDeIm.TextMatrix(i, 4) = Format((porce * (DbBase - tdesb + tdesbcond) / 100), "#,###.#0")
                  ''JACC R2345
                  If TipoPers = "0" Then
                     ReDim ArrDeImp(0)
                     Result = LoadData("TC_IMPUESTOS", "TX_CODI_DESC_IMPU", "CD_CODI_IMPU =" & Comi & GrdDeIm.TextMatrix(I, 1) & Comi, ArrDeImp)
                     If Result <> FAIL And Encontro And ArrDeImp(0) <> NUL$ Then
                          'GrdDeIm.TextMatrix(I, 4) = Format((porce * ((DbBase - Valor_Descuento(ArrDeImp(0), DbBase)) - tdesb + tdesbcond) / 100), "#,###.#0")
                          
                           'INICIO GAVL T6265
                            'If frmFaseCompra.BoAproxCent = True Then 'JLPB T23820 SE DEJA EN COMENTARIO
                            If BoAproxCent Then 'JLPB T23820
                             GrdDeIm.TextMatrix(I, 4) = AproxCentena(Round(Format((porce * ((DbBase - Valor_Descuento(ArrDeImp(0), DbBase)) - tdesb + tdesbcond) / 100), "#,###.#0")))
                            Else
                             GrdDeIm.TextMatrix(I, 4) = Format((porce * ((DbBase - Valor_Descuento(ArrDeImp(0), DbBase)) - tdesb + tdesbcond) / 100), "#,###.#0")
                            End If
                            'FIN  GAVL T6265
                          
                          
                     Else
                          'GrdDeIm.TextMatrix(I, 4) = Format((porce * (DbBase - tdesb + tdesbcond) / 100), "#,###.#0")
                          
                          'INICIO GAVL T6265
                            'If frmFaseCompra.BoAproxCent = True Then 'JLPB T23820 SE DEJA EN COMENTARIO
                            If BoAproxCent Then 'JLPB T23820
                               If GrdDeIm.TextMatrix(I, 11) = False Then 'JAUM T28644-R27752
                                  GrdDeIm.TextMatrix(I, 4) = AproxCentena(Round(Format((porce * (DbBase - tdesb + tdesbcond) / 100), "#,###.#0")))
                               End If 'JAUM T28644-R27752
                            Else
                               If GrdDeIm.TextMatrix(I, 11) = False Then 'JAUM T28644-R27752
                                  GrdDeIm.TextMatrix(I, 4) = Format((porce * (DbBase - tdesb + tdesbcond) / 100), "#,###.#0")
                               End If
                            End If
                            'FIN  GAVL T6265
                          
                      End If
                  Else
'                     GrdDeIm.TextMatrix(I, 4) = Format((porce * (DbBase - tdesb + tdesbcond) / 100), "#,###.#0")
                     
                     'INICIO GAVL T6265
                    'If frmFaseCompra.BoAproxCent = True Then 'JLPB T23820 SE DEJA EN COMENTARIO
                    If BoAproxCent Then 'JLPB T23820
                       If GrdDeIm.TextMatrix(I, 11) = False Then 'JAUM T30157
                          GrdDeIm.TextMatrix(I, 4) = AproxCentena(Round(Format((porce * (DbBase - tdesb + tdesbcond) / 100), "#,###.#0")))
                       End If 'JAUM T30157
                    Else
                       If GrdDeIm.TextMatrix(I, 11) = False Then 'JAUM T30157
                          GrdDeIm.TextMatrix(I, 4) = Format((porce * (DbBase - tdesb + tdesbcond) / 100), "#,###.#0")
                       End If 'JAUM T30157
                    End If
                    'FIN  GAVL T6265
                     
                  End If
                  'JACC R2345
              Else
                    
                 GrdDeIm.TextMatrix(I, 4) = "0"
                  
              End If
                 
           End If
              
           'impuesto ica
           If GrdDeIm.TextMatrix(I, 0) = "C" Then
                 
              If CDbl(GrdDeIm.TextMatrix(I, 7)) <= DbBase Then
                    
                 porce = CDbl(GrdDeIm.TextMatrix(I, 3))
'                 GrdDeIm.TextMatrix(I, 4) = Format((porce * (DbBase - tdesb + tdesbcond) / 100), "#,###.#0")
                 
                  'INICIO GAVL T6265
                  'If frmFaseCompra.BoAproxCent = True Then 'JLPB T23820 SE DEJA EN COMENTARIO
                  If BoAproxCent Then 'JLPB T23820
                    GrdDeIm.TextMatrix(I, 4) = AproxCentena(Round(Format((porce * (DbBase - tdesb + tdesbcond) / 100), "#,###.#0")))
                  Else
                    GrdDeIm.TextMatrix(I, 4) = Format((porce * (DbBase - tdesb + tdesbcond) / 100), "#,###.#0")
                  End If
                 'FIN  GAVL T6265
                 
                 
                   
              Else
                    
                 GrdDeIm.TextMatrix(I, 4) = "0"
                   
              End If
                 
           End If
           
           'Impuesto retenido, sobre el iva
           If GrdDeIm.TextMatrix(I, 0) = "M" Then
              
              If CDbl(GrdDeIm.TextMatrix(I, 7)) <= DbIVA Then
                    
                 porce = CDbl(GrdDeIm.TextMatrix(I, 3))
                 'GrdDeIm.TextMatrix(I, 4) = Format((porce * DbIVA / 100), "#,###.#0")
                 
                   'INICIO GAVL T6265
                  'If frmFaseCompra.BoAproxCent = True Then 'JLPB T23820 SE DEJA EN COMENTARIO
                  If BoAproxCent Then 'JLPB T23820
                     GrdDeIm.TextMatrix(I, 4) = AproxCentena(Round(Format((porce * DbIVA / 100), "#,###.#0")))
                  Else
                     GrdDeIm.TextMatrix(I, 4) = Format((porce * DbIVA / 100), "#,###.#0")
                  End If
                 'FIN  GAVL T6265
   
                    
              Else
                    
                 GrdDeIm.TextMatrix(I, 4) = "0"
                     
              End If
               
           End If
              
        Else
              
           GrdDeIm.TextMatrix(I, 4) = "0"
             
        End If
        
     End If
   
  Next
   
End Sub

'calcula el porcentaje de los impuestos si se modifica el valor de impuesto
Private Sub calcular_PorcImp()
  Dim tdesb As Double 'total descuento al valor bruto
  Dim porce As Double
  Dim tdesbcond As Double 'total Descuento bruto condicionado para calcular impuesto retencion
  Dim InI As Double
  Dim Fila As Double
      
  tdesb = 0
  tdesbcond = 0
  Fila = GrdDeIm.Row
    
  For InI = 1 To GrdDeIm.Rows - 1
     'descuentos
     If GrdDeIm.TextMatrix(InI, 8) = "D" Then
     
        If CDbl(GrdDeIm.TextMatrix(InI, 7)) <= DbBase Then
               
           If GrdDeIm.TextMatrix(InI, 6) = "P" Then
                
              porce = CDbl(GrdDeIm.TextMatrix(InI, 3))
              GrdDeIm.TextMatrix(InI, 4) = Format(((porce * DbBase) / 100), "#,###.#0")
              tdesb = tdesb + GrdDeIm.TextMatrix(InI, 4)
              'verifica si el descuento es condicionado
              If GrdDeIm.TextMatrix(InI, 0) Then tdesbcond = tdesbcond + ((porce * DbBase) / 100)
                        
           ElseIf GrdDeIm.TextMatrix(InI, 6) = "V" Then
              
              If CDbl(GrdDeIm.TextMatrix(InI, 4)) <= DbBase Then
                 
                 GrdDeIm.TextMatrix(InI, 4) = Format(CDbl(GrdDeIm.TextMatrix(InI, 4)), "#,###.#0")
                 tdesb = tdesb + GrdDeIm.TextMatrix(InI, 4)
                 'verifica si el descuento es condicionado
                 If GrdDeIm.TextMatrix(InI, 0) Then tdesbcond = tdesbcond + GrdDeIm.TextMatrix(InI, 4)
                     
              End If
                   
           End If
        Else
               
           GrdDeIm.TextMatrix(InI, 4) = Format(0, "#,###.#0")
               
        End If
        
     End If
     'If frmFaseCompra.BoAproxCent = True Then GrdDeIm.TextMatrix(InI, 4) = AproxCentena(Round(GrdDeIm.TextMatrix(InI, 4))) 'GAVL T6421 'JLPB T23820 SE DEJA EN COMENTARIO
     If BoAproxCent Then GrdDeIm.TextMatrix(InI, 4) = AproxCentena(Round(GrdDeIm.TextMatrix(InI, 4))) 'JLPB T23820
  Next
      
  
     
  For InI = 1 To GrdDeIm.Rows - 1
      If Fila = InI Then
     
        If GrdDeIm.TextMatrix(InI, 8) = "I" Then
         
           If GrdDeIm.TextMatrix(InI, 4) <> "" And IsNumeric(GrdDeIm.TextMatrix(InI, 4)) = True Then
              
              If GrdDeIm.TextMatrix(InI, 3) = NUL$ Then GrdDeIm.TextMatrix(InI, 3) = 0
                    
              'impuesto retencion
              If GrdDeIm.TextMatrix(InI, 0) = "R" Then
                    
                 If (DbBase - tdesb + tdesbcond) <> 0 Then
                    If CDbl(GrdDeIm.TextMatrix(InI, 7)) <= DbBase Then
                     'HRR M5360 Inicio
                     'porce = CDbl(GrdDeIm.TextMatrix(InI, 4) * 100 / (DbBase - tdesb + tdesbcond))
                     'GrdDeIm.TextMatrix(InI, 3) = Format(porce, "#0.####")
                     'HRR M5360 Fin
                     'HRR M5360 Inicio
                     If (DbBase - tdesb + tdesbcond) <> 0 Then
                     
                        'porce = CDbl(GrdDeIm.TextMatrix(Ini, 4) * 100 / (DbBase - tdesb + tdesbcond))
                        ''JACC R2345
                        If TipoPers = "0" Then
                  ReDim ArrDeImp(0)
                           Result = LoadData("TC_IMPUESTOS", "TX_CODI_DESC_IMPU", "CD_CODI_IMPU =" & Comi & GrdDeIm.TextMatrix(InI, 1) & Comi, ArrDeImp)
                           If Result <> FAIL And Encontro And ArrDeImp(0) <> NUL$ Then
                               porce = CDbl(GrdDeIm.TextMatrix(InI, 4) * 100 / ((DbBase - Valor_Descuento(ArrDeImp(0), DbBase)) - tdesb + tdesbcond))
                              
                           Else
                              porce = CDbl(GrdDeIm.TextMatrix(InI, 4) * 100 / (DbBase - tdesb + tdesbcond))
                           End If
                        Else
                           porce = CDbl(GrdDeIm.TextMatrix(InI, 4) * 100 / (DbBase - tdesb + tdesbcond))
                        End If
                        'JACC R2345
                        GrdDeIm.TextMatrix(InI, 3) = Format(porce, "#0.####")
                     Else
                        GrdDeIm.TextMatrix(InI, 3) = "0"
                     End If
                     'HRR M5360 Fin
                    Else
                       GrdDeIm.TextMatrix(InI, 3) = "0"
                    End If
                 End If
                'If frmFaseCompra.BoAproxCent = True Then porce = (Round(porce)) 'GAVL T6421 'JLPB T23820 SE DEJA EN COMENTARIO
                If BoAproxCent Then porce = (Round(porce)) 'JLPB T23820
              End If
                 
              'impuesto ica
              If GrdDeIm.TextMatrix(InI, 0) = "C" Then
                    
                 If (DbBase - tdesb + tdesbcond) <> 0 Then
                    
                    If CDbl(GrdDeIm.TextMatrix(InI, 7)) <= DbBase Then
                       'HRR M5360 Inicio
                       'porce = CDbl(GrdDeIm.TextMatrix(InI, 4) * 100 / (DbBase - tdesb + tdesbcond))
                       'GrdDeIm.TextMatrix(InI, 3) = Format(porce, "#0.####")
                       'HRR M5360 Fin
                       'HRR M5360 Inicio
                       If (DbBase - tdesb + tdesbcond) <> 0 Then
                           porce = CDbl(GrdDeIm.TextMatrix(InI, 4) * 100 / (DbBase - tdesb + tdesbcond))
                           GrdDeIm.TextMatrix(InI, 3) = Format(porce, "#0.####")
                        Else
                           GrdDeIm.TextMatrix(InI, 3) = "0"
                        End If
                       'HRR M5360 Fin
                    Else
                       GrdDeIm.TextMatrix(InI, 3) = "0"
                    End If
                                      
                 End If
                    
              End If
              
              'impuesto retenido
              If GrdDeIm.TextMatrix(InI, 0) = "M" Then
                      
                 If CDbl(GrdDeIm.TextMatrix(InI, 7)) <= DbIVA Then
                    If DbIVA <> 0 Then 'AASV M5360
                        porce = CDbl(GrdDeIm.TextMatrix(InI, 4) * 100 / DbIVA)
                        GrdDeIm.TextMatrix(InI, 3) = Format(porce, "#0.####")
                    Else
                        GrdDeIm.TextMatrix(InI, 3) = "0"
                    End If 'AASV M5360
                 Else
                    GrdDeIm.TextMatrix(InI, 3) = "0"
                 End If
                     
              End If
                 
           Else
                 
              GrdDeIm.TextMatrix(InI, 3) = "0"
                
           End If
           
        End If
        
     End If
   
  Next
   
End Sub

Private Sub Cargar_Concepto()
    
    Dim InI As Integer
    ReDim arr(0)
    
    cmbConcepto.Clear
    lstConcepto.Clear
    
    Condicion = "ID_TIPO_CONC=7 AND CD_CODI_CONC='CINV'"
    Result = LoadData("Concepto", "DE_DESC_CONC", Condicion, arr)
    If Result <> FAIL And Encontro And arr(0) <> NUL$ Then
       cmbConcepto.AddItem arr(0)
       lstConcepto.AddItem "CINV"
    End If
    
    ReDim Arconc(1, 0)
    Condicion = "ID_TIPO_CONC=12"
    Result = LoadMulData("Concepto", "CD_CODI_CONC,DE_DESC_CONC", Condicion, Arconc)
    
    For InI = 0 To UBound(Arconc, 2)
        lstConcepto.AddItem Arconc(0, InI)
        cmbConcepto.AddItem Arconc(1, InI)
    Next

End Sub


Private Sub Buscar_Descuentos()
   
   Dim InI As Integer
   
   ReDim ArrDesc(4, 0)
   
   Desde = "DESCUENTO,R_CONC_DESC"
   Campos = "CD_CODI_DESC,DE_NOMB_DESC,VL_VALO_DESC,PR_PORC_DESC,VL_TOPE_DESC"
   Condicion = "CD_CODI_DESC=CD_DESC_CODE"
   Condicion = Condicion & " AND CD_CONC_CODE='" & lstConcepto.List(cmbConcepto.ListIndex) & "'"
   'GAPM M6403 INICIO
   Condicion = Condicion & " AND (TX_TIPDESC_DESC='2' OR TX_TIPDESC_DESC='3')"
   'GAPM M6403 FIN
   Result = LoadMulData(Desde, Campos, Condicion, ArrDesc)

   With TrvLista
        'Carga los descuentos
        '--------------------
        .Nodes.Add , , "Des", "Descuentos"
                    
         For InI = 0 To UBound(ArrDesc, 2)
            .Nodes.Add "Des", tvwChild, ArrDesc(0, InI) & "|" & ArrDesc(1, InI), ArrDesc(0, InI) & " " & ArrDesc(1, InI)
            If ArrDesc(3, InI) = NUL$ Then ArrDesc(3, InI) = 0
            If ArrDesc(2, InI) = NUL$ Then ArrDesc(2, InI) = 0
            If ConDoble(ArrDesc(3, InI)) <> 0 Then
               .Nodes.Item(.Nodes.Count).Tag = "P" & "|" & ConDoble(ArrDesc(3, InI)) & "|" & ConDoble(ArrDesc(4, InI))
            Else
               .Nodes.Item(.Nodes.Count).Tag = "V" & "|" & ConDoble(ArrDesc(2, InI)) & "|" & ConDoble(ArrDesc(4, InI))
            End If
         Next
          
   End With
   
End Sub

Private Sub Cambiar_Descuentos()

   ReDim ArDesc(8, 0)

   Desde = "DESCUENTO,R_CONC_DESC"
   Campos = "TX_TIPO_DESC,CD_CODI_DESC,DE_NOMB_DESC,VL_VALO_DESC,'R',PR_PORC_DESC,'D',VL_TOPE_DESC,CD_CUEN_DESC"
   Condicion = "CD_CODI_DESC=CD_DESC_CODE AND VL_TOPE_DESC<=" & DbBase
   Condicion = Condicion & " AND CD_CONC_CODE='" & lstConcepto.List(cmbConcepto.ListIndex) & "'"
   Result = LoadMulData(Desde, Campos, Condicion, ArDesc)

End Sub

Private Sub Buscar_Impuestos()

Dim InI As Integer
Dim ArrImp() As Variant


   ReDim arr(4)
          
   BoIvaOtrosImp = False
    
   Campos = "CD_IMPU_COIM"
   Desde = "R_CONC_IMPU, TC_IMPUESTOS"
   Condicion = "CD_CODI_IMPU=CD_IMPU_COIM"
   Condicion = Condicion & " AND CD_CONC_COIM='" & lstConcepto.List(cmbConcepto.ListIndex) & "'"
      
   If AutoRet = "1" Or ExentoFuente = "1" Then Condicion = Condicion & " AND ID_TIPO_IMPU <> 'R'"
   If AutRetIva = "1" Or ExentoIva = "1" Then Condicion = Condicion & " AND ID_TIPO_IMPU <> 'M'"
   If AutRetIca = "1" Or ExentoIca = "1" Then Condicion = Condicion & " AND ID_TIPO_IMPU <> 'C'"
    
   Result = FiltrarImpuestos(Campos, Desde, Condicion, TipoPers, TipoRegimen, GranContrib, ArrImp())
   If Result <> FAIL Then
   
      With TrvLista
         .Nodes.Add , , "Imp", "Impuestos"
   
         For InI = 0 To UBound(ArrImp())
            Campos = "CD_CODI_IMPU,DE_NOMB_IMPU,PR_PORC_IMPU,VL_TOPE_IMPU,ID_TIPO_IMPU"
            Condicion = "CD_CODI_IMPU=" & Comi & ArrImp(InI) & Comi
            Result = LoadData("TC_IMPUESTOS", Campos, Condicion, arr)
            If Result <> FAIL And Encontro Then
               If arr(4) = "O" Or arr(4) = "I" Then BoIvaOtrosImp = True
               .Nodes.Add "Imp", tvwChild, "I" & arr(0) & "|" & arr(1), arr(0) & " " & arr(1)
               If arr(2) = NUL$ Then arr(2) = 0
               .Nodes.Item(.Nodes.Count).Tag = arr(2) & "|" & ConDoble(arr(3))
               
            End If
         
         Next
         
      End With
          
   End If
   
   If BoIvaOtrosImp Then
      Call Mensaje1("El concepto " & ClssImpDes.concepto & "  tiene impuestos tipo iva, otros impuestos que no se aplican en la compra.", 3)
   End If
                       
End Sub


Private Sub Cambiar_Impuestos()

   Dim InI As Integer
   Dim InJ As Integer
   Dim ArrImp() As Variant
   Dim BoContinue As Boolean
   
   'JAUM T28644-R27752 Inicio se deja bloque en comentario
   'ReDim arr(5)
   'ReDim ArImp(9, 0)
   ReDim arr(6)
   ReDim ArImp(10, 0)
   'JAUM T28644-R27752 Fin
   InJ = 0
       
   Campos = "CD_IMPU_COIM"
   Desde = "R_CONC_IMPU, TC_IMPUESTOS"
   Condicion = "CD_CODI_IMPU=CD_IMPU_COIM"
   Condicion = Condicion & " AND CD_CONC_COIM='" & lstConcepto.List(cmbConcepto.ListIndex) & "'"
      
   If AutoRet = "1" Or ExentoFuente = "1" Then Condicion = Condicion & " AND ID_TIPO_IMPU <> 'R'"
   If AutRetIva = "1" Or ExentoIva = "1" Then Condicion = Condicion & " AND ID_TIPO_IMPU <> 'M'"
   If AutRetIca = "1" Or ExentoIca = "1" Then Condicion = Condicion & " AND ID_TIPO_IMPU <> 'C'"
   Condicion = Condicion & " AND ID_TIPO_IMPU<>'I' AND ID_TIPO_IMPU<>'O'"
    
   Result = FiltrarImpuestos(Campos, Desde, Condicion, TipoPers, TipoRegimen, GranContrib, ArrImp())
   If Result <> FAIL Then
   
      For InI = 0 To UBound(ArrImp())
         'Campos = "CD_CODI_IMPU,DE_NOMB_IMPU,PR_PORC_IMPU,VL_TOPE_IMPU,CD_CUEN_IMPU,ID_TIPO_IMPU" JAUM T28644-R27752 Se deja linea en comentario
         Campos = "CD_CODI_IMPU,DE_NOMB_IMPU,PR_PORC_IMPU,VL_TOPE_IMPU,CD_CUEN_IMPU,ID_TIPO_IMPU,TX_RETEFU_IMPU" 'JAUM T28644-R27752
         Condicion = "CD_CODI_IMPU=" & Comi & ArrImp(InI) & Comi & " AND VL_TOPE_IMPU<=" & DbBase
         Result = LoadData("TC_IMPUESTOS", Campos, Condicion, arr)
         If Result <> FAIL And Encontro Then
         
            
               BoContinue = False
               If arr(5) = "M" Then
                  If arr(3) <= DbIVA Then BoContinue = True
               Else
                  BoContinue = True
               End If
               If BoContinue Then
                   'HRR M2642
'                  If UBound(ArImp, 2) <> 0 Then
'                     ReDim Preserve ArImp(UBound(ArImp, 2) + 1)
'                     InJ = InJ + 1
'                  End If
                  'HRR M2642
                  'HRR M2642
                  If InJ <> 0 Then
                     'ReDim Preserve ArImp(9, UBound(ArImp, 2) + 1) JAUM T28644-R27752 Se deja linea en comentario
                     ReDim Preserve ArImp(10, UBound(ArImp, 2) + 1) 'JAUM T28644-R27752
                  End If
                  'HRR M2642
                  
                  ArImp(0, InJ) = NUL$
                  ArImp(1, InJ) = arr(0) 'CODIGO
                  ArImp(2, InJ) = arr(1) 'NOMBRE
                  ArImp(3, InJ) = 0 'VALOR
                  ArImp(4, InJ) = "R"
                  ArImp(5, InJ) = arr(2) '%
                  ArImp(6, InJ) = "I"
                  ArImp(7, InJ) = arr(3) 'tope
                  ArImp(8, InJ) = arr(4) 'cuenta
                  ArImp(9, InJ) = arr(5) 'tipo
                  ArImp(10, InJ) = arr(6) 'JAUM T28644-R27752
                  InJ = InJ + 1
                              
               End If
               
            
            
         End If
      
      Next
         
   End If


End Sub


Private Sub Copiar_ImpDesc_Grid()
   
   Dim InI As Integer
   Dim InX As Integer 'JAUM T28644-R27752 Contador
   TrvLista.Enabled = True
   'Dim dbValor As Double

   BoLoad = True
   
   With GrdDeIm
      .ColWidth(0) = 0
      .ColWidth(1) = 0
      .ColWidth(6) = 500
         For InI = 0 To UBound(ArImp, 2)
            If ArImp(1, InI) <> NUL$ Then
               
               'HRR DEP
'               If DbBase = 0 Then
'                  dbValor = 0
'               Else
'                  dbValor = Format(((ArImp(3, InI) * 100) / DbBase), "###0.0###")
'               End If
               '.AddItem vbTab & ArImp(1, InI) & vbTab & ArImp(2, InI) & vbTab & dbValor & vbTab & ArImp(3, InI) & vbTab & ArImp(4, InI) & vbTab & "P"
               'HRR DEP
               If InCanEntra > 1 And ArImp(10, InI) = True Then GoTo SEGUIR 'JAUM T28644-R27752
               .AddItem ArImp(9, InI) & vbTab & ArImp(1, InI) & vbTab & ArImp(2, InI) & vbTab & ArImp(5, InI) & vbTab & ArImp(3, InI) & vbTab & ArImp(4, InI) & vbTab & "P"
               .TextMatrix(GrdDeIm.Rows - 1, 4) = Format(GrdDeIm.TextMatrix(GrdDeIm.Rows - 1, 4), "#,###.#0")
               .TextMatrix(GrdDeIm.Rows - 1, 7) = ArImp(7, InI)
               .TextMatrix(GrdDeIm.Rows - 1, 8) = "I"
               .TextMatrix(GrdDeIm.Rows - 1, 9) = ArImp(8, InI)
               If TipoPers = "0" Then
                  ReDim ArrDeImp(0)
                  Result = LoadData("TC_IMPUESTOS", "TX_CODI_DESC_IMPU", "CD_CODI_IMPU =" & Comi & ArImp(1, InI) & Comi, ArrDeImp)
                  If Result <> FAIL And Encontro And ArrDeImp(0) <> NUL$ Then
                      .TextMatrix(GrdDeIm.Rows - 1, 10) = DbBase - Valor_Descuento(ArrDeImp(0), DbBase) 'JACC R2345
                  Else
                     'JAUM T28644-R27752 Inicio
                     If ArImp(10, InI) = True Then
                        For InX = 0 To UBound(VrValBase, 2)
                           If VrValBase(0, InX) <> NUL$ Then
                              If ArImp(1, InI) = VrValBase(0, InX) Then
                                 .TextMatrix(GrdDeIm.Rows - 1, 4) = VrValBase(1, InX)
                                 .TextMatrix(GrdDeIm.Rows - 1, 10) = VrValBase(2, InX)
                                 Exit For
                              End If
                           Else
                              .TextMatrix(GrdDeIm.Rows - 1, 10) = "0"
                           End If
                        Next
                     Else
                     'JAUM T28644-R27752 Fin
                        .TextMatrix(GrdDeIm.Rows - 1, 10) = DbBase  'JACC R2345
                     End If 'JAUM T28644-R27752
                  End If
               Else
                  'JAUM T30157 Inicio
                  If ArImp(10, InI) = True Then
                     For InX = 0 To UBound(VrValBase, 2)
                        If VrValBase(0, InX) <> NUL$ Then
                           If ArImp(1, InI) = VrValBase(0, InX) Then
                              .TextMatrix(GrdDeIm.Rows - 1, 4) = VrValBase(1, InX)
                              .TextMatrix(GrdDeIm.Rows - 1, 10) = VrValBase(2, InX)
                              Exit For
                           End If
                        Else
                           .TextMatrix(GrdDeIm.Rows - 1, 10) = "0"
                        End If
                     Next
                  Else
                  'JAUM T30157 Fin
                     .TextMatrix(GrdDeIm.Rows - 1, 10) = DbBase  'JACC R2345
                  End If 'JAUM T30157
               End If
               .TextMatrix(GrdDeIm.Rows - 1, 11) = ArImp(10, InI) 'JAUM T28644-R27752
            End If
SEGUIR: 'JAUM T28644-R27752
         Next
         
            
         For InI = 0 To UBound(ArDesc, 2)
            
            If ArDesc(0, InI) <> NUL$ Then
             
              If ConDoble(ArDesc(5, InI)) <> 0 Then
                  'HRR DEP
'                 If DbBase = 0 Then
'                    dbValor = 0
'                 Else
'                    dbValor = Format(((ArDesc(3, InI) * 100) / DbBase), "###0.0###")
'                 End If
'                 .AddItem ArDesc(0, InI) & vbTab & ArDesc(1, InI) & vbTab & ArDesc(2, InI) & vbTab & dbValor & vbTab & ArDesc(3, InI) & vbTab & ArDesc(4, InI)
                  'HRR DEP
            
                  .AddItem ArDesc(0, InI) & vbTab & ArDesc(1, InI) & vbTab & ArDesc(2, InI) & vbTab & ArDesc(5, InI) & vbTab & ArDesc(3, InI) & vbTab & ArDesc(4, InI)
                 
                 .TextMatrix(GrdDeIm.Rows - 1, 6) = "P"
                 .TextMatrix(GrdDeIm.Rows - 1, 7) = ArDesc(7, InI)
                 .TextMatrix(GrdDeIm.Rows - 1, 8) = "D"
                 .TextMatrix(GrdDeIm.Rows - 1, 9) = ArDesc(8, InI)
                 .TextMatrix(GrdDeIm.Rows - 1, 10) = DbBase 'JACC R2345
              
              ElseIf ConDoble(ArDesc(3, InI)) <> 0 Then
                 
                 .AddItem ArDesc(0, InI) & vbTab & ArDesc(1, InI) & vbTab & ArDesc(2, InI) & vbTab & "0" & vbTab & ArDesc(3, InI) & vbTab & ArDesc(4, InI)
                 
                 .TextMatrix(GrdDeIm.Rows - 1, 6) = "V"
                 .TextMatrix(GrdDeIm.Rows - 1, 7) = ArDesc(7, InI)
                 .TextMatrix(GrdDeIm.Rows - 1, 8) = "D"
                 .TextMatrix(GrdDeIm.Rows - 1, 9) = ArDesc(8, InI)
                 .TextMatrix(GrdDeIm.Rows - 1, 10) = DbBase 'JACC R2345
              End If
              .TextMatrix(GrdDeIm.Rows - 1, 4) = Format(GrdDeIm.TextMatrix(GrdDeIm.Rows - 1, 4), "#,###.#0")
              
            End If
         Next
         
            For InI = 1 To .Rows - 1
               .Row = InI
               Call calcular_cxp
            Next
         
   End With
   
   BoLoad = False

   If BoArbol = False Then TrvLista.Enabled = False
     
   GrdDeIm.ColWidth(7) = 0
   GrdDeIm.ColWidth(8) = 0


End Sub

'Public Sub Cargar_ImpDesc(ByRef ArrDesc() As Variant, ByRef ArrImp() As Variant, ByRef ClssID As ImpuDesc) 'JAUM T28644-R27752 Se deja linea en comentario
Public Sub Cargar_ImpDesc(ByRef ArrDesc() As Variant, ByRef ArrImp() As Variant, ByRef ClssID As ImpuDesc, ByRef VrValorBase As Variant, ByRef InCantEntra As Variant)  'JAUM T28644-R27752
   ArDesc = ArrDesc
   ArImp = ArrImp
   DbBase = Format(DbBase, "#,###,###,###,##0.#0")
   Set ClssImpDes = New ImpuDesc
   Set ClssImpDes.RefImpuDesc = ClssID
   ClssImpDes.concepto = ClssImpDes.ConceptoRef
   VrValBase = VrValorBase 'JAUM T28644-R27752
   InCanEntra = InCantEntra 'JAUM T28644-R27752
End Sub
'HRR R1853



