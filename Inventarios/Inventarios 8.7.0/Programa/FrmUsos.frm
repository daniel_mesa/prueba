VERSION 5.00
Object = "{0BA686C6-F7D3-101A-993E-0000C0EF6F5E}#1.0#0"; "THREED32.OCX"
Begin VB.Form FrmUsos 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Usos"
   ClientHeight    =   2625
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   5955
   Icon            =   "FrmUsos.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   ScaleHeight     =   2625
   ScaleWidth      =   5955
   Begin VB.Frame Frame2 
      Height          =   1095
      Left            =   480
      TabIndex        =   3
      Top             =   1440
      Width           =   4935
      Begin Threed.SSCommand SCmd_Options 
         Height          =   735
         Index           =   0
         Left            =   120
         TabIndex        =   4
         Top             =   240
         Width           =   855
         _Version        =   65536
         _ExtentX        =   1508
         _ExtentY        =   1296
         _StockProps     =   78
         Caption         =   "&GUARDAR"
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   6
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Picture         =   "FrmUsos.frx":058A
      End
      Begin Threed.SSCommand SCmd_Options 
         Height          =   735
         Index           =   1
         Left            =   1080
         TabIndex        =   5
         Top             =   240
         Width           =   855
         _Version        =   65536
         _ExtentX        =   1508
         _ExtentY        =   1296
         _StockProps     =   78
         Caption         =   "&BORRAR"
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   6
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Picture         =   "FrmUsos.frx":0C54
      End
      Begin Threed.SSCommand SCmd_Options 
         Height          =   735
         Index           =   3
         Left            =   3000
         TabIndex        =   7
         Top             =   240
         Width           =   855
         _Version        =   65536
         _ExtentX        =   1508
         _ExtentY        =   1296
         _StockProps     =   78
         Caption         =   "&LISTAR"
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   6
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Picture         =   "FrmUsos.frx":131E
      End
      Begin Threed.SSCommand SCmd_Options 
         Height          =   735
         Index           =   4
         Left            =   3960
         TabIndex        =   8
         Top             =   240
         Width           =   855
         _Version        =   65536
         _ExtentX        =   1508
         _ExtentY        =   1296
         _StockProps     =   78
         Caption         =   "&SALIR"
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   6
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Picture         =   "FrmUsos.frx":19E8
      End
      Begin Threed.SSCommand SCmd_Options 
         Height          =   735
         Index           =   2
         Left            =   2040
         TabIndex        =   6
         Top             =   240
         Width           =   855
         _Version        =   65536
         _ExtentX        =   1508
         _ExtentY        =   1296
         _StockProps     =   78
         Caption         =   "S&ELECCION"
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   6
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Picture         =   "FrmUsos.frx":20B2
      End
   End
   Begin VB.Frame Frame1 
      Height          =   1335
      Left            =   0
      TabIndex        =   0
      Top             =   0
      Width           =   5895
      Begin VB.TextBox TxtUso 
         Height          =   285
         Index           =   1
         Left            =   1200
         MaxLength       =   40
         TabIndex        =   2
         Top             =   840
         Width           =   4455
      End
      Begin VB.TextBox TxtUso 
         Height          =   285
         Index           =   0
         Left            =   1200
         MaxLength       =   4
         TabIndex        =   1
         Top             =   240
         Width           =   615
      End
      Begin Threed.SSCommand CmdLimpia 
         Height          =   375
         Left            =   5280
         TabIndex        =   9
         ToolTipText     =   "Limpia Pantalla"
         Top             =   240
         Width           =   375
         _Version        =   65536
         _ExtentX        =   661
         _ExtentY        =   661
         _StockProps     =   78
         Picture         =   "FrmUsos.frx":2A64
      End
      Begin VB.Label LblDescripcion 
         Caption         =   "&Descripci�n :"
         Height          =   255
         Left            =   120
         TabIndex        =   11
         Top             =   840
         Width           =   975
      End
      Begin VB.Label LblCodigo 
         Caption         =   "&C�digo :"
         Height          =   255
         Left            =   120
         TabIndex        =   10
         Top             =   240
         Width           =   615
      End
   End
End
Attribute VB_Name = "FrmUsos"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Dim Tabla As String
Dim Mensaje As String
Dim EncontroT As Boolean
Public OpcCod        As String   'Opci�n de seguridad

Private Sub CmdLimpia_Click()
   Call Limpiar
End Sub

Private Sub Form_Load()
    Call CenterForm(MDI_Inventarios, Me)
    Tabla = "USOS"
    Mensaje = "Uso"
'    Call Leer_Permisos("01003", SCmd_Options(0), SCmd_Options(1), SCmd_Options(3))
End Sub

Private Sub SCmd_Options_Click(Index As Integer)
Dim uso As String
    Select Case Index
        Case 0: Encontro = EncontroT
                Grabar
        Case 1: Borrar
        Case 2: uso = Seleccion(Tabla, "DE_DESC_USOS", "CD_CODI_USOS,DE_DESC_USOS", "USOS", NUL$)
                If uso <> NUL$ Then TxtUso(0) = uso
                TxtUso(0).SetFocus
                SCmd_Options(2).SetFocus
        Case 3: Call Listar("USOS.rpt", Tabla, "CD_CODI_USOS", "DE_DESC_USOS", "Usos", NUL$, NUL$, NUL$, NUL$, NUL$)
                Me.SetFocus
        Case 4: Unload Me
    End Select
End Sub
Private Sub SCmd_Options_GotFocus(Index As Integer)
 Select Case Index
    Case 0: Msglin "Pulse este icono para grabar"
    Case 1: Msglin "Pulse este icono para borrar"
    Case 2: Msglin "Pulse este icono para selecci�n"
    Case 3: Msglin "Pulse este icono para generar un listado"
    Case 4: Msglin "Pulse este icono para cerrar la ventana "
  End Select
End Sub
Private Sub Buscar_Uso()
ReDim Arr(1)
Dim I As Byte
   Condicion = "CD_CODI_USOS=" & Comi & Cambiar_Comas_Comillas(TxtUso(0)) & Comi
               
   Result = LoadData(Tabla, Asterisco, Condicion, Arr())
   If (Result <> False) Then
     If Encontro Then
        TxtUso(1) = Arr(1)
        EncontroT = True
     Else
        TxtUso(1) = NUL$
        EncontroT = False
     End If
   End If
End Sub
Private Sub Grabar()
   DoEvents
   
   Call MouseClock
   Msglin "Ingresando informaci�n del " & Mensaje

   If (TxtUso(0) = NUL$ Or TxtUso(1) = NUL$) Then
      Call Mensaje1("Se deben especificar los datos completos del " & Mensaje, 3)
      Call MouseNorm: Exit Sub
   End If
   
   If (BeginTran(STranIUp & Tabla) <> FAIL) Then
       If (Not Encontro) Then
          'Silvia 13_09_03
          'Valores = "CD_CODI_USOS=" & Cambiar_Comas_Comillas(TxtUso(0)) & Comi & Coma
          'Valores = Valores & "DE_DESC_USOS=" & Cambiar_Comas_Comillas(TxtUso(1)) & Comi
          Valores = "CD_CODI_USOS=" & Comi & Cambiar_Comas_Comillas(TxtUso(0)) & Comi & Coma
          Valores = Valores & "DE_DESC_USOS=" & Comi & Cambiar_Comas_Comillas(TxtUso(1)) & Comi
          
          Result = DoInsertSQL(Tabla, Valores)
          If (Result <> FAIL) Then Result = Auditor(Tabla, TranIns, LastCmd)
       Else
         If WarnMsg("Desea Modificar la Informaci�n del " & Mensaje, 256) Then
            Valores = " DE_DESC_USOS=" & Comi & Cambiar_Comas_Comillas(TxtUso(1)) & Comi
            
            Condicion = "CD_CODI_USOS=" & Comi & Cambiar_Comas_Comillas(TxtUso(0)) & Comi
            
            Result = DoUpdate(Tabla, Valores, Condicion)
            If (Result <> FAIL) Then Result = Auditor(Tabla, TranUpd, LastCmd)
         End If
       End If
   End If
   If (Result <> FAIL) Then
      If (CommitTran() <> FAIL) Then
          Limpiar
      Else
         Call RollBackTran
      End If
   Else
      Call RollBackTran
   End If
   Call MouseNorm
   Msglin NUL$
End Sub
Private Sub Borrar()
Dim StMens As String 'DAHV M5798
'DAHV M5798 Inicio
StMens = "El uso " & Cambiar_Comas_Comillas(TxtUso(1)) & ", no se puede borrar"
StMens = StMens & vbCrLf & "debido a que se encuentra relacionado con diferentes art�culos"
'DAHV M5798 Fin
   DoEvents
   
   Call MouseClock
   
   Msglin "Borrando un " & Mensaje
   If (TxtUso(0) = NUL$) Then
       Call Mensaje1("Ingrese el c�digo del " & Mensaje, 1)
       Call MouseNorm: Msglin NUL$: Exit Sub
   End If
   If (Encontro <> False) Then
       If (Not WarnDel()) Then Call MouseNorm: Msglin NUL$: Exit Sub
       
       'DAHV M5798 - INICIO
       ReDim Arr(0)
       Campos = " TOP 1 CD_USOS_ARTI"
       Desde = "ARTICULO"
       Condicion = "CD_USOS_ARTI=" & Comi & Cambiar_Comas_Comillas(TxtUso(0).Text) & Comi
       Result = LoadData(Desde, Campos, Condicion, Arr())
       If Result <> FAIL Then
            If Encontro Then
                Call Mensaje1(StMens, 1)
                Call MouseNorm
                Msglin NUL$
                Exit Sub
            End If
       End If
       'DAHV M5798 - FIN
       
       
       Condicion = "CD_CODI_USOS=" & Comi & Cambiar_Comas_Comillas(TxtUso(0)) & Comi
   
        If (BeginTran(STranDel & Tabla) <> FAIL) Then
            Result = DoDelete(Tabla, Condicion)
            If (Result <> FAIL) Then Result = Auditor(Tabla, TranDel, LastCmd)
        End If
   Else
       Call Mensaje1("No se pudo borrar el " & Mensaje & " de la base de datos, debido a que no existe !!!...", 1)
       Call MouseNorm: Msglin NUL$: Exit Sub
   End If
   If (Result <> FAIL) Then
       If (CommitTran() <> FAIL) Then
          Limpiar
       Else
          Call RollBackTran
       End If
   Else
       Call RollBackTran
   End If
   Call MouseNorm
   Msglin NUL$
End Sub
Private Sub Limpiar()
Dim I As Byte
    For I = 0 To 1
        TxtUso(I) = NUL$
    Next I
    TxtUso(0).SetFocus
End Sub
Private Sub TxtUso_GotFocus(Index As Integer)
TxtUso(Index).SelStart = 0
TxtUso(Index).SelLength = Len(TxtUso(Index).Text)
Select Case Index
    Case 0: Msglin "Digite el c�digo del " & Mensaje
    Case 1: Msglin "Digite la descripci�n del " & Mensaje
            If TxtUso(0) = NUL$ Then TxtUso(0).SetFocus: Exit Sub
End Select
End Sub
Private Sub TxtUso_KeyDown(Index As Integer, KeyCode As Integer, Shift As Integer)
    Select Case Index
        Case 0: If KeyCode = 40 Then TxtUso(Index + 1).SetFocus: Exit Sub
        Case 1: If KeyCode = 38 Then TxtUso(Index - 1).SetFocus: Exit Sub
    End Select
End Sub
Private Sub TxtUso_KeyPress(Index As Integer, KeyAscii As Integer)
  Call ValKeyAlfaNum(KeyAscii)
  Call Cambiar_Enter(KeyAscii)
End Sub
Private Sub TxtUso_LostFocus(Index As Integer)
     Msglin NUL$
     Select Case Index
        Case 0: If TxtUso(0) <> NUL$ Then Buscar_Uso
     End Select
End Sub


