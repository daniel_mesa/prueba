VERSION 5.00
Object = "{00025600-0000-0000-C000-000000000046}#5.2#0"; "Crystl32.OCX"
Object = "{C932BA88-4374-101B-A56C-00AA003668DC}#1.1#0"; "msmask32.ocx"
Object = "{0BA686C6-F7D3-101A-993E-0000C0EF6F5E}#1.0#0"; "THREED32.OCX"
Begin VB.Form FrmLfavv 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Facturas de Venta x Vendedor"
   ClientHeight    =   4170
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   3855
   Icon            =   "FrmLfavv.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   ScaleHeight     =   4170
   ScaleWidth      =   3855
   Begin VB.Frame FraOrden 
      Caption         =   "Ordenar por"
      Height          =   735
      Left            =   0
      TabIndex        =   12
      Top             =   2520
      Width           =   3855
      Begin VB.OptionButton OptOrden 
         Caption         =   "Fecha"
         Height          =   255
         Index           =   0
         Left            =   720
         TabIndex        =   13
         Top             =   360
         Value           =   -1  'True
         Width           =   855
      End
      Begin VB.OptionButton OptOrden 
         Caption         =   "Nit"
         Height          =   255
         Index           =   1
         Left            =   2400
         TabIndex        =   14
         Top             =   360
         Width           =   615
      End
   End
   Begin VB.Frame FraEsta 
      Caption         =   "Mostrar"
      Height          =   735
      Left            =   0
      TabIndex        =   8
      Top             =   1920
      Width           =   3855
      Begin VB.OptionButton OptEstado 
         Caption         =   "Todos"
         Height          =   255
         Index           =   2
         Left            =   2880
         TabIndex        =   11
         Top             =   360
         Width           =   855
      End
      Begin VB.OptionButton OptEstado 
         Caption         =   "No Anulados"
         Height          =   255
         Index           =   0
         Left            =   120
         TabIndex        =   9
         Top             =   360
         Value           =   -1  'True
         Width           =   1335
      End
      Begin VB.OptionButton OptEstado 
         Caption         =   "Anulados"
         Height          =   255
         Index           =   1
         Left            =   1560
         TabIndex        =   10
         Top             =   360
         Width           =   975
      End
   End
   Begin VB.Frame FraFecha 
      Caption         =   "Fecha    (dd/mm/aaaa)"
      Height          =   855
      Left            =   0
      TabIndex        =   5
      Top             =   1200
      Width           =   3855
      Begin MSMask.MaskEdBox MskFecha 
         Height          =   375
         Index           =   0
         Left            =   720
         TabIndex        =   6
         Top             =   360
         Width           =   1095
         _ExtentX        =   1931
         _ExtentY        =   661
         _Version        =   393216
         MaxLength       =   10
         Mask            =   "##/##/####"
         PromptChar      =   "_"
      End
      Begin MSMask.MaskEdBox MskFecha 
         Height          =   375
         Index           =   1
         Left            =   2520
         TabIndex        =   7
         Top             =   360
         Width           =   1095
         _ExtentX        =   1931
         _ExtentY        =   661
         _Version        =   393216
         MaxLength       =   10
         Mask            =   "##/##/####"
         PromptChar      =   "_"
      End
      Begin VB.Label LblFeFinal 
         Caption         =   "Final :"
         Height          =   255
         Left            =   2040
         TabIndex        =   16
         Top             =   360
         Width           =   495
      End
      Begin VB.Label LblFeIni 
         Caption         =   "Inicial :"
         Height          =   255
         Left            =   120
         TabIndex        =   15
         Top             =   360
         Width           =   495
      End
   End
   Begin VB.Frame FraVendedores 
      Caption         =   "Rango de Vendedores"
      Height          =   1335
      Left            =   0
      TabIndex        =   0
      Top             =   0
      Width           =   3855
      Begin VB.TextBox TxtRango 
         Height          =   285
         Index           =   1
         Left            =   1440
         MaxLength       =   11
         TabIndex        =   3
         Text            =   "ZZZZZZZZZZZ"
         Top             =   840
         Width           =   1335
      End
      Begin VB.TextBox TxtRango 
         Height          =   285
         Index           =   0
         Left            =   1440
         MaxLength       =   11
         TabIndex        =   1
         Text            =   "0"
         Top             =   360
         Width           =   1335
      End
      Begin Threed.SSCommand CmdSelec 
         Height          =   375
         Index           =   1
         Left            =   2760
         TabIndex        =   4
         ToolTipText     =   "Selecci�n de Vendedores"
         Top             =   840
         Width           =   615
         _Version        =   65536
         _ExtentX        =   1085
         _ExtentY        =   661
         _StockProps     =   78
         BevelWidth      =   0
         RoundedCorners  =   0   'False
         Outline         =   0   'False
         Picture         =   "FrmLfavv.frx":0442
      End
      Begin Threed.SSCommand CmdSelec 
         Height          =   375
         Index           =   0
         Left            =   2760
         TabIndex        =   2
         ToolTipText     =   "Selecci�n de Vendedores"
         Top             =   360
         Width           =   615
         _Version        =   65536
         _ExtentX        =   1085
         _ExtentY        =   661
         _StockProps     =   78
         BevelWidth      =   0
         RoundedCorners  =   0   'False
         Outline         =   0   'False
         Picture         =   "FrmLfavv.frx":0894
      End
      Begin VB.Label Label1 
         AutoSize        =   -1  'True
         Caption         =   "Inicial : "
         Height          =   195
         Left            =   600
         TabIndex        =   20
         Top             =   360
         Width           =   540
      End
      Begin VB.Label Label2 
         AutoSize        =   -1  'True
         Caption         =   "Final : "
         Height          =   195
         Left            =   600
         TabIndex        =   19
         Top             =   840
         Width           =   465
      End
   End
   Begin Crystal.CrystalReport Crys_Listar 
      Left            =   0
      Top             =   2400
      _ExtentX        =   741
      _ExtentY        =   741
      _Version        =   348160
      WindowLeft      =   20
      WindowWidth     =   750
      WindowHeight    =   450
      WindowTitle     =   "Facturas de Venta por Vendedor"
      WindowControlBox=   -1  'True
      WindowMaxButton =   -1  'True
      WindowMinButton =   -1  'True
      WindowControls  =   -1  'True
      PrintFileLinesPerPage=   60
      WindowShowCloseBtn=   -1  'True
      WindowShowSearchBtn=   -1  'True
      WindowShowPrintSetupBtn=   -1  'True
      WindowShowRefreshBtn=   -1  'True
   End
   Begin Threed.SSCommand SCmd_Options 
      Height          =   375
      Index           =   3
      Left            =   120
      TabIndex        =   17
      Top             =   4800
      Visible         =   0   'False
      Width           =   975
      _Version        =   65536
      _ExtentX        =   1720
      _ExtentY        =   661
      _StockProps     =   78
   End
   Begin Threed.SSCommand SCmd_Options 
      Height          =   375
      Index           =   4
      Left            =   1440
      TabIndex        =   18
      Top             =   4800
      Visible         =   0   'False
      Width           =   975
      _Version        =   65536
      _ExtentX        =   1720
      _ExtentY        =   661
      _StockProps     =   78
   End
   Begin Threed.SSCommand SCmd_Options 
      Height          =   735
      Index           =   1
      Left            =   1560
      TabIndex        =   21
      Top             =   3360
      Width           =   855
      _Version        =   65536
      _ExtentX        =   1508
      _ExtentY        =   1296
      _StockProps     =   78
      Caption         =   "&Imprimir"
      Picture         =   "FrmLfavv.frx":0CE6
   End
   Begin Threed.SSCommand SCmd_Options 
      Height          =   735
      Index           =   2
      Left            =   2640
      TabIndex        =   22
      Top             =   3360
      Width           =   855
      _Version        =   65536
      _ExtentX        =   1508
      _ExtentY        =   1296
      _StockProps     =   78
      Caption         =   "&Salir"
      Picture         =   "FrmLfavv.frx":1228
   End
   Begin Threed.SSCommand SCmd_Options 
      Height          =   735
      Index           =   0
      Left            =   480
      TabIndex        =   23
      Top             =   3360
      Width           =   855
      _Version        =   65536
      _ExtentX        =   1508
      _ExtentY        =   1296
      _StockProps     =   78
      Caption         =   "&Pantalla"
      Picture         =   "FrmLfavv.frx":1936
   End
End
Attribute VB_Name = "FrmLfavv"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub Form_Load()
 Call CenterForm(MDI_Inventarios, Me)
 Call Leer_Permisos("03017", SCmd_Options(3), SCmd_Options(3), SCmd_Options(4))
 Crys_Listar.Formulas(0) = "ENTIDAD='" & Entidad & Comi
 Crys_Listar.Formulas(1) = "NIT= 'NIT. " & Nit & Comi
 Crys_Listar.Formulas(2) = "HORA= 'Hora: " & Format(Now, "hh:mm") & Comi
 Crys_Listar.Formulas(3) = "USUARIO = 'Usuario:" & UserId & Comi
 Crys_Listar.Connect = ReportConnect
 MskFecha(0) = Hoy
 MskFecha(1) = Hoy
End Sub
Private Sub TxtRango_GotFocus(Index As Integer)
    TxtRango(Index).SelStart = 0
    TxtRango(Index).SelLength = Len(TxtRango(Index).Text)
End Sub
Private Sub TxtRango_KeyPress(Index As Integer, KeyAscii As Integer)
    Dim I As Integer
    Call ValKeyAlfaNum(KeyAscii)
    If KeyAscii = 13 Then
     If Index = 0 Then
      TxtRango(Index + 1).SetFocus
     Else
      MskFecha(0).SetFocus
     End If
    End If
End Sub
Private Sub TxtRango_KeyDown(Index As Integer, KeyCode As Integer, Shift As Integer)
 If Index = 0 Then
   If KeyCode = 40 Then
      TxtRango(1).SetFocus
   End If
 Else
   If KeyCode = 38 Then
      TxtRango(0).SetFocus
   End If
   If KeyCode = 40 Then
      MskFecha(0).SetFocus
   End If
 End If
End Sub
Private Sub MskFecha_GotFocus(Index As Integer)
    MskFecha(Index).SelStart = 0
    MskFecha(Index).SelLength = Len(MskFecha(Index).Text)
End Sub
Private Sub MskFecha_KeyPress(Index As Integer, KeyAscii As Integer)
    Call ValKeyFecha(KeyAscii, MskFecha(Index))
    Call Cambiar_Enter(KeyAscii)
End Sub
Private Sub MskFecha_LostFocus(Index As Integer)
   Call ValFecha(MskFecha(Index), 0)
End Sub
Private Sub MskFecha_KeyDown(Index As Integer, KeyCode As Integer, Shift As Integer)
 If Index = 0 Then
   If KeyCode = 38 Then
      TxtRango(1).SetFocus
   End If
   If KeyCode = 40 Then
      MskFecha(1).SetFocus
   End If
 Else
   If KeyCode = 38 Then
      MskFecha(0).SetFocus
   End If
   If KeyCode = 40 Then
      If OptEstado(0).Value = True Then
         OptEstado(0).SetFocus
      End If
      If OptEstado(1).Value = True Then
         OptEstado(1).SetFocus
      End If
      If OptEstado(2).Value = True Then
         OptEstado(2).SetFocus
      End If
   End If
 End If
End Sub
Private Sub OptEstado_KeyPress(Index As Integer, KeyAscii As Integer)
  Call Cambiar_Enter(KeyAscii)
End Sub
Private Sub OptOrden_KeyPress(Index As Integer, KeyAscii As Integer)
  Call Cambiar_Enter(KeyAscii)
End Sub
Private Sub CmdSelec_Click(Index As Integer)
    Codigo = NUL$
    Codigo = Seleccion("TERCERO", "NO_NOMB_TERC", "CD_CODI_TERC,NO_NOMB_TERC", "TERCEROS", "CD_CODI_TERC <> 'ZZZZZZZZZZZ'")
    If Codigo <> NUL$ Then TxtRango(Index) = Codigo
    TxtRango(Index).SetFocus
End Sub
Private Sub SCmd_Options_Click(Index As Integer)
    Screen.MousePointer = 11
    Select Case Index
        '''True = pantalla
        '''False = impresora
        Case 0: Call Informe(True)
        Case 1: Call Informe(False)
        Case 2: Unload Me
    End Select
    Screen.MousePointer = 0
End Sub
Private Sub Informe(ByVal pantalla As Boolean)
On Error GoTo ERROR
  Dim algo
  If TxtRango(0) > TxtRango(1) Or TxtRango(1) = "" Then
     Call Mensaje1("Revise rango de vendedores", 3)
     Exit Sub
  End If
  algo = DateDiff("d", MskFecha(0), MskFecha(1))
  If CLng(algo) < 0 Then
     Call Mensaje1("Fecha Inicial Mayor que Fecha Final", 3)
     Exit Sub
  End If
  ''Env�a rango de fechas al informe
  Crys_Listar.Formulas(4) = "FECHAI= '" & MskFecha(0) & Comi
  Crys_Listar.Formulas(5) = "FECHAF= '" & MskFecha(1) & Comi
  ''''''''''''''''''''''''''''''''''
  Crys_Listar.SelectionFormula = ""
  Crys_Listar.ReportFileName = DirTrab + "ldfavv.RPT"
  If OptOrden(0).Value = True Then
    Crys_Listar.SortFields(0) = "+{venta.fe_fech_vent}"
  Else
    Crys_Listar.SortFields(0) = "+{venta.cd_terc_vent}"
  End If
  If OptEstado(0).Value = True Then
   Crys_Listar.SelectionFormula = "{venta.id_esta_vent} <> " & "2" & _
   " and {venta.cd_vend_vent} in '" & TxtRango(0) & "' to '" & TxtRango(1) & Comi
  End If
  If OptEstado(1).Value = True Then
   Crys_Listar.SelectionFormula = "{venta.id_esta_vent} = " & "2" & _
   " and {venta.cd_vend_vent} in '" & TxtRango(0) & "' to '" & TxtRango(1) & Comi
  End If
  If OptEstado(2).Value = True Then
   Crys_Listar.SelectionFormula = "{venta.cd_vend_vent} in '" & TxtRango(0) & "' to '" & TxtRango(1) & Comi
  End If
  If SCmd_Options(4).Enabled = False Then
     Call Mensaje1("Permiso Denegado", 3)
  Else
     If pantalla = True Then
        Crys_Listar.Destination = crptToWindow
     Else
        Crys_Listar.Destination = crptToPrinter
     End If
     Crys_Listar.Action = 1
  End If
Exit Sub
ERROR:
      ConvertErr
End Sub



