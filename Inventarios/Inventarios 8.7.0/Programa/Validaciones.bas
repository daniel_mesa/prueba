Attribute VB_Name = "Validaciones"
Option Explicit

Function Periodo_Reclamos(ByVal Fe_Primer As Date, ByVal Fe_Ultimo As Date, ByVal Fe_Presentacion As Date, ByVal Cod_Contrato As Integer, ByVal Cod_Plan As Integer) As Integer
'Valida el periodo de presentacion de reclamos

'Se deben ingresar los siguientes parametros:
'fecha primer gasto Fe_Primer
'fecha ultimo gasto Fe_Ultimo
'fecha presentacion Fe_Presentacion
'codigo contrato Cod_Contrato (INGRESAR EL AUTONUMERICO)
'codigo plan Cod_Plan (INGRESAR EL AUTONUMERICO)

ReDim arr(1)
condi = "NU_AUTO_CONT = " & Cod_Contrato
Result = LoadData("CONTRATO", "TX_PERREC_CONT,NU_PERACU_CONT", condi, arr)

ReDim arr2(0)
condi = "NU_AUTO_PLAN = " & Cod_Plan
Result = LoadData("PLAN", "NU_PERECL_PLAN", condi, arr2)


If arr(0) = "P" Then
   If Val(DateDiff("d", Fe_Primer, Fe_Presentacion)) <= Val(arr2(0)) Then
      Periodo_Reclamos = 1
   Else
      Call Mensaje1("El usuario ha superado el tiempo especificado para pedir un reembolso", 2)
      Periodo_Reclamos = -1: Exit Function
   End If
   
ElseIf arr(0) = "U" Then
   If Val(DateDiff("d", Fe_Ultimo, Fe_Presentacion)) <= Val(arr2(0)) Then
      Periodo_Reclamos = 1
   Else
      Call Mensaje1("El usuario ha superado el tiempo especificado para pedir un reembolso", 2)
      Periodo_Reclamos = -1: Exit Function
   End If
   If Val(DateDiff("d", Fe_Primer, Fe_Ultimo)) <= Val(arr(1)) Then
      Periodo_Reclamos = 1
   Else
      Call Mensaje1("La diferencia de tiempo entre el primer gasto y el segundo gasto ha superado el tope.", 2)
      Periodo_Reclamos = -1: Exit Function
   End If
   
Else
   Call Mensaje1("Este contrato no tiene definido un tipo de periodo. Remitase a la parametrizacion.", 2)
End If

End Function

Function Validar_Factura(ByVal Fe_Primer As Date, ByVal Fe_Ultimo As Date, ByVal Fe_Factura As Date) As Integer
'Permite establecer que la fecha de una factura se encuentra efectivamente
'dentro del rango de facturas (fe_primer a fe_ultimo)

'Se deben ingresar los siguientes parametros:
'fecha primer gasto Fe_Primer
'fecha ultimo gasto Fe_Ultimo
'fecha presentacion Fe_Factura

   If Val(DateDiff("d", Fe_Primer, Fe_Factura)) < 0 Then
      Call Mensaje1("La fecha de la factura es de un tiempo anterior al de la primer factura", 2)
      Validar_Factura = -1: Exit Function
   Else
      Validar_Factura = 1
   End If
   
   If Val(DateDiff("d", Fe_Factura, Fe_Ultimo)) < 0 Then
      Call Mensaje1("La fecha de la factura es de un tiempo posterior al de la �ltima factura", 2)
      Validar_Factura = -1: Exit Function
   Else
      Validar_Factura = 1
   End If
   
End Function
