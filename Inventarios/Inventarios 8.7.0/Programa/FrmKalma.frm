VERSION 5.00
Object = "{00025600-0000-0000-C000-000000000046}#5.2#0"; "CRYSTL32.OCX"
Object = "{C932BA88-4374-101B-A56C-00AA003668DC}#1.1#0"; "MSMASK32.OCX"
Object = "{0BA686C6-F7D3-101A-993E-0000C0EF6F5E}#1.0#0"; "THREED32.OCX"
Begin VB.Form FrmKalma 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "K�rdex Almac�n"
   ClientHeight    =   4575
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   3630
   Icon            =   "FrmKalma.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   ScaleHeight     =   4575
   ScaleWidth      =   3630
   Begin VB.Frame FraOpciones 
      Caption         =   "Opciones"
      Height          =   855
      Left            =   240
      TabIndex        =   8
      Top             =   2400
      Width           =   3135
      Begin VB.CheckBox ChkOpcion 
         Caption         =   "Valorizado"
         Height          =   255
         Index           =   1
         Left            =   1800
         TabIndex        =   10
         Top             =   360
         Width           =   1095
      End
      Begin VB.CheckBox ChkOpcion 
         Caption         =   "Detallado"
         Height          =   255
         Index           =   0
         Left            =   360
         TabIndex        =   9
         Top             =   360
         Width           =   1095
      End
   End
   Begin VB.Frame FraFecha 
      Caption         =   "Fecha    (dd/mm/aaaa)"
      Height          =   1335
      Left            =   240
      TabIndex        =   5
      Top             =   1200
      Width           =   3135
      Begin MSMask.MaskEdBox MskFecha 
         Height          =   375
         Index           =   0
         Left            =   1320
         TabIndex        =   6
         Top             =   360
         Width           =   1095
         _ExtentX        =   1931
         _ExtentY        =   661
         _Version        =   393216
         MaxLength       =   10
         Mask            =   "##/##/####"
         PromptChar      =   "_"
      End
      Begin MSMask.MaskEdBox MskFecha 
         Height          =   375
         Index           =   1
         Left            =   1320
         TabIndex        =   7
         Top             =   840
         Width           =   1095
         _ExtentX        =   1931
         _ExtentY        =   661
         _Version        =   393216
         MaxLength       =   10
         Mask            =   "##/##/####"
         PromptChar      =   "_"
      End
      Begin VB.Label LblFeIni 
         Caption         =   "Inicial :"
         Height          =   255
         Left            =   600
         TabIndex        =   12
         Top             =   360
         Width           =   495
      End
      Begin VB.Label LblFeFinal 
         Caption         =   "Final :"
         Height          =   255
         Left            =   600
         TabIndex        =   11
         Top             =   840
         Width           =   495
      End
   End
   Begin VB.Frame FraArticulos 
      Caption         =   "Rango de Art�culos"
      Height          =   1335
      Left            =   240
      TabIndex        =   0
      Top             =   0
      Width           =   3135
      Begin VB.TextBox TxtArticulo 
         Height          =   285
         Index           =   1
         Left            =   600
         MaxLength       =   16
         TabIndex        =   3
         Text            =   "ZZZZZZZZZZZZZZZZ"
         Top             =   840
         Width           =   1815
      End
      Begin VB.TextBox TxtArticulo 
         Height          =   285
         Index           =   0
         Left            =   600
         MaxLength       =   16
         TabIndex        =   1
         Text            =   "0"
         Top             =   360
         Width           =   1815
      End
      Begin Threed.SSCommand CmdSelec 
         Height          =   375
         Index           =   1
         Left            =   2400
         TabIndex        =   4
         ToolTipText     =   "Selecci�n de Art�culos"
         Top             =   840
         Width           =   615
         _Version        =   65536
         _ExtentX        =   1085
         _ExtentY        =   661
         _StockProps     =   78
         BevelWidth      =   0
         RoundedCorners  =   0   'False
         Outline         =   0   'False
         Picture         =   "FrmKalma.frx":058A
      End
      Begin Threed.SSCommand CmdSelec 
         Height          =   375
         Index           =   0
         Left            =   2400
         TabIndex        =   2
         ToolTipText     =   "Selecci�n de Art�culos"
         Top             =   360
         Width           =   615
         _Version        =   65536
         _ExtentX        =   1085
         _ExtentY        =   661
         _StockProps     =   78
         BevelWidth      =   0
         RoundedCorners  =   0   'False
         Outline         =   0   'False
         Picture         =   "FrmKalma.frx":0F3C
      End
      Begin VB.Label LblHasta 
         AutoSize        =   -1  'True
         Caption         =   "Final : "
         Height          =   195
         Left            =   120
         TabIndex        =   14
         Top             =   840
         Width           =   465
      End
      Begin VB.Label LblInicio 
         AutoSize        =   -1  'True
         Caption         =   "Inicial : "
         Height          =   195
         Left            =   120
         TabIndex        =   13
         Top             =   360
         Width           =   540
      End
   End
   Begin Crystal.CrystalReport Crys_Listar 
      Left            =   0
      Top             =   360
      _ExtentX        =   741
      _ExtentY        =   741
      _Version        =   348160
      WindowLeft      =   20
      WindowWidth     =   750
      WindowHeight    =   450
      WindowControlBox=   -1  'True
      WindowMaxButton =   -1  'True
      WindowMinButton =   -1  'True
      WindowControls  =   -1  'True
      PrintFileLinesPerPage=   60
      WindowShowCloseBtn=   -1  'True
      WindowShowSearchBtn=   -1  'True
      WindowShowPrintSetupBtn=   -1  'True
      WindowShowRefreshBtn=   -1  'True
   End
   Begin Threed.SSCommand SCmd_Options 
      Height          =   735
      Index           =   1
      Left            =   1320
      TabIndex        =   15
      Top             =   3720
      Width           =   855
      _Version        =   65536
      _ExtentX        =   1508
      _ExtentY        =   1296
      _StockProps     =   78
      Caption         =   "&IMPRIMIR"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   6
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Picture         =   "FrmKalma.frx":18EE
   End
   Begin Threed.SSCommand SCmd_Options 
      Height          =   735
      Index           =   2
      Left            =   2400
      TabIndex        =   16
      Top             =   3720
      Width           =   855
      _Version        =   65536
      _ExtentX        =   1508
      _ExtentY        =   1296
      _StockProps     =   78
      Caption         =   "&SALIR"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   6
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Picture         =   "FrmKalma.frx":1FB8
   End
   Begin Threed.SSCommand SCmd_Options 
      Height          =   735
      Index           =   0
      Left            =   240
      TabIndex        =   17
      Top             =   3720
      Width           =   855
      _Version        =   65536
      _ExtentX        =   1508
      _ExtentY        =   1296
      _StockProps     =   78
      Caption         =   "&PANTALLA"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   6
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Picture         =   "FrmKalma.frx":2682
   End
   Begin Threed.SSCommand SCmd_Options 
      Height          =   375
      Index           =   3
      Left            =   0
      TabIndex        =   18
      Top             =   0
      Visible         =   0   'False
      Width           =   975
      _Version        =   65536
      _ExtentX        =   1720
      _ExtentY        =   661
      _StockProps     =   78
   End
   Begin Threed.SSCommand SCmd_Options 
      Height          =   375
      Index           =   4
      Left            =   1080
      TabIndex        =   19
      Top             =   0
      Visible         =   0   'False
      Width           =   975
      _Version        =   65536
      _ExtentX        =   1720
      _ExtentY        =   661
      _StockProps     =   78
   End
   Begin VB.Label LblReg 
      Height          =   255
      Left            =   240
      TabIndex        =   20
      Top             =   3360
      Width           =   3135
   End
End
Attribute VB_Name = "FrmKalma"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Public OpcCod        As String   'Opci�n de seguridad

Private Sub Form_Load()
    Dim ElClnt As New ElTercero
    ElClnt.IniXNit
 Call CenterForm(MDI_Inventarios, Me)
' Call Leer_Permisos("03005", SCmd_Options(3), SCmd_Options(3), SCmd_Options(0))
 Crys_Listar.Formulas(0) = "ENTIDAD='" & ElClnt.Nombre & Comi
 Crys_Listar.Formulas(1) = "NIT= 'NIT. " & ElClnt.Nit & Comi
 Crys_Listar.Formulas(2) = "HORA= 'Hora: " & Format(Now, "hh:mm") & Comi
 Crys_Listar.Formulas(3) = "USUARIO = 'Usuario:" & UserId & Comi
' Crys_Listar.Connect = conCrys
 SCmd_Options(1).Enabled = SCmd_Options(0).Enabled
 MskFecha(0) = Hoy
 MskFecha(1) = Hoy
End Sub
Private Sub TxtArticulo_GotFocus(Index As Integer)
    TxtArticulo(Index).SelStart = 0
    TxtArticulo(Index).SelLength = Len(TxtArticulo(Index).Text)
End Sub
Private Sub TxtArticulo_KeyPress(Index As Integer, KeyAscii As Integer)
'    Dim i As Integer       'DEPURACION DE CODIGO
    Call ValKeyAlfaNum(KeyAscii)
    If KeyAscii = 13 Then
     If Index = 0 Then
      TxtArticulo(Index + 1).SetFocus
     Else
      MskFecha(0).SetFocus
     End If
    End If
End Sub
Private Sub TxtArticulo_KeyDown(Index As Integer, KeyCode As Integer, Shift As Integer)
 If Index = 0 Then
   If KeyCode = 40 Then
      TxtArticulo(1).SetFocus
   End If
 Else
   If KeyCode = 38 Then
      TxtArticulo(0).SetFocus
   End If
   If KeyCode = 40 Then
      MskFecha(0).SetFocus
   End If
 End If
End Sub
Private Sub MskFecha_GotFocus(Index As Integer)
    MskFecha(Index).SelStart = 0
    MskFecha(Index).SelLength = Len(MskFecha(Index).Text)
End Sub
Private Sub MskFecha_KeyPress(Index As Integer, KeyAscii As Integer)
    Call ValKeyFecha(KeyAscii, MskFecha(Index))
    Call Cambiar_Enter(KeyAscii)
End Sub
Private Sub MskFecha_LostFocus(Index As Integer)
   Call ValFecha(MskFecha(Index), 0)
End Sub
Private Sub MskFecha_KeyDown(Index As Integer, KeyCode As Integer, Shift As Integer)
 If Index = 0 Then
   If KeyCode = 38 Then
      TxtArticulo(1).SetFocus
   End If
   If KeyCode = 40 Then
      MskFecha(1).SetFocus
   End If
 Else
   If KeyCode = 38 Then
      MskFecha(0).SetFocus
   End If
   If KeyCode = 40 Then
      ChkOpcion(0).SetFocus
   End If
 End If
End Sub
Private Sub ChkOpcion_KeyPress(Index As Integer, KeyAscii As Integer)
    Call Cambiar_Enter(KeyAscii)
End Sub
Private Sub CmdSelec_Click(Index As Integer)
    Select Case Index
        Case 0, 1: Codigo = NUL$
                   Codigo = Seleccion("ARTICULO", "NO_NOMB_ARTI", "CD_CODI_ARTI,NO_NOMB_ARTI", "ARTICULOS", NUL$)
                   If Codigo <> NUL$ Then TxtArticulo(Index) = Codigo
                   TxtArticulo(Index).SetFocus
    End Select
End Sub
Private Sub SCmd_Options_Click(Index As Integer)
    Screen.MousePointer = 11
    Select Case Index
        '''True = pantalla
        '''False = impresora
        Case 0: Call Informe(True)
        Case 1: Call Informe(False)
        Case 2: Unload Me
    End Select
    Screen.MousePointer = 0
End Sub
Private Sub Informe(ByVal pantalla As Boolean)
Dim ArrTemp() As Variant
Dim nomrep As String
  Dim algo
  If TxtArticulo(0) > TxtArticulo(1) Or TxtArticulo(1) = "" Then
     Call Mensaje1("Revise rango de art�culos", 3)
     Exit Sub
  End If
  algo = DateDiff("d", MskFecha(0), MskFecha(1))
  If CLng(algo) < 0 Then
     Call Mensaje1("Fecha Inicial Mayor que Fecha Final", 3)
     Exit Sub
  End If
  'validar que el mes de donde se van a tomar los saldos iniciales este cerrado
  ReDim ArrTemp(0)
  Result = LoadData("PARAMETROS_INVE", "FE_CIER_APLI", NUL$, ArrTemp())
  If Result <> FAIL And Encontro Then
     If (CLng(Format(MskFecha(0), "yyyymm")) - 1) > CLng(Format(ArrTemp(0), "yyyymm")) Then
        If CLng(Format(MskFecha(0), "mm")) - 1 > 0 Then
           Call Mensaje1("No se puede generar el listado porque " & MonthName(CLng(Format(MskFecha(0), "mm")) - 1) & " de " & Format(MskFecha(0), "yyyy") & " no se ha cerrado", 1)
        Else
           Call Mensaje1("No se puede generar el listado porque Diciembre " & " de " & CLng(Format(MskFecha(0), "yyyy")) - 1 & " no se ha cerrado", 1)
        End If
        Call MouseNorm
        Exit Sub
     End If
  Else
     Call Mensaje1("No se pudo obtener la fecha de cierre", 1)
     Call MouseNorm
     Exit Sub
  End If
  ''Env�a rango de fechas al informe
  Crys_Listar.Formulas(4) = "FECHAI= '" & MskFecha(0) & Comi
  Crys_Listar.Formulas(5) = "FECHAF= '" & MskFecha(1) & Comi
  ''''''''''''''''''''''''''''''''''
  If ChkOpcion(0).Value = 1 Then 'detallado
    If ChkOpcion(1).Value = 1 Then 'valorizado
      Crys_Listar.WindowTitle = "K�rdex Almac�n Detallado Valorizado"
      Crys_Listar.ReportFileName = DirTrab + "kalmaV.RPT"
      nomrep = "kalmaV.RPT"
    Else 'no Valorizado
      Crys_Listar.WindowTitle = "K�rdex Almac�n Detallado"
      Crys_Listar.ReportFileName = DirTrab + "kalma.RPT"
      nomrep = "kalma.RPT"
    End If
  Else 'no detallado
    If ChkOpcion(1).Value = 1 Then 'valorizado
      Crys_Listar.WindowTitle = "K�rdex Almac�n Valorizado"
      Crys_Listar.ReportFileName = DirTrab + "kalmaV2.RPT"
      nomrep = "kalmaV2.RPT"
    Else 'no Valorizado
      Crys_Listar.WindowTitle = "K�rdex Almac�n"
      Crys_Listar.ReportFileName = DirTrab + "kalma2.RPT"
      nomrep = "kalma2.RPT"
    End If
  End If
  Crys_Listar.SelectionFormula = "{KARDEX_ARTI.CD_ARTI_KARD} in '" & TxtArticulo(0) & "' to '" & TxtArticulo(1) & Comi
  On Error GoTo control
  Debug.Print Crys_Listar.SelectionFormula
  If SCmd_Options(4).Enabled = False Then
     Call Mensaje1("Permiso Denegado", 3)
  Else
     Crear_Temporales ''Crea temporal para impresi�n
     If pantalla = True Then
        Crys_Listar.Destination = crptToWindow
     Else
        Crys_Listar.Destination = crptToPrinter
     End If
     
     Crys_Listar.Action = 1
  End If
  Exit Sub
control:
  Call Mensaje1(ERR.Number & ": " & ERR.Description & "  Nombre del Reporte : " & nomrep, 1)
End Sub

Private Sub Crear_Temporales()
Dim Rstdatos As ADODB.Recordset
Dim Costo As Double
Dim Cantidad As Double
Dim CostoK As Double
Dim CantK As Double
Dim Fuc As String

    Valores = " CT_CANT_SALD=0,VL_COST_SALD=0"
    Result = DoUpdate("SAL_ANT", Valores, NUL$)
    
    Fuc = "01/" & Format(MskFecha(0), "mm") & "/" & Format(MskFecha(0), "yyyy")
    Fuc = DateAdd("d", -1, Fuc)
    
    Condicion = "CD_ARTI_KARD >= '" & TxtArticulo(0) & Comi
    Condicion = Condicion & " and CD_ARTI_KARD <= '" & TxtArticulo(1) & Comi
    Condicion = Condicion & " and ID_ESTA_KARD <> " & "2"
    Condicion = Condicion & " ORDER BY CD_ARTI_KARD"

    Set Rstdatos = New ADODB.Recordset
    Call SelectRST("KARDEX_ARTI", "DISTINCT CD_ARTI_KARD", Condicion, Rstdatos)
    If Not Rstdatos.EOF Then
       LblReg = NUL$
       Rstdatos.MoveFirst
       Do While Not Rstdatos.EOF
          LblReg = "Leyendo Art�culo: " & Rstdatos.Fields(0)
          DoEvents
          Cantidad = Saldo_Inicial(Rstdatos.Fields(0), Fuc, Costo)
          CantK = Saldo_Kardex(Rstdatos.Fields(0), MskFecha(0), CostoK)
          Cantidad = Cantidad + CantK
          Costo = Costo + CostoK
          Condicion = "CD_ARTI_SALD=" & Comi & Rstdatos.Fields(0) & Comi
          
          If Cantidad = 0 Then Costo = 0
          
          Valores = " CT_CANT_SALD=" & Cantidad & Coma
          Valores = Valores & " VL_COST_SALD=" & Costo
          Result = DoUpdate("SAL_ANT", Valores, Condicion)
        
          Rstdatos.MoveNext
       Loop
    End If
    Rstdatos.Close
End Sub

'DEPURACION DE CODIGO
'Private Sub Recorre_Kardex()
'  ReDim MArr(3, 0) 'Para cargar registros (tabla k�rdex)
'  Dim c As Long  'Para recorrer registros (tabla k�rdex)
'  ReDim Arr(2) 'Para manejar la tabla de saldos
'  Dim Mes As Byte
'  Dim AuxF As String
'
'  AuxF = "01/" & Format(MskFecha(0), "mm") & "/" & Format(MskFecha(0), "yyyy")
'  AuxF = DateAdd("d", -1, AuxF)
'
'  Condicion = "CD_ARTI_KARD >= '" & TxtArticulo(0) & Comi
'  Condicion = Condicion & " and CD_ARTI_KARD <= '" & TxtArticulo(1) & Comi
'  Condicion = Condicion & " and ID_ESTA_KARD <> " & "2"
'  Condicion = Condicion & " and FE_FECH_KARD >" & FFechaCon(AuxF)
'  Condicion = Condicion & " and FE_FECH_KARD < " & FFechaCon(MskFecha(0))
'  Condicion = Condicion & " and CD_ORDO_KARD <> " & "2"
'  Condicion = Condicion & " and CD_ORDO_KARD <> " & "4"
'  Condicion = Condicion & " and CD_ORDO_KARD <> " & "5"
'  Condicion = Condicion & " GROUP BY CD_ARTI_KARD, CD_ORDO_KARD"
'  Result = LoadMulData("KARDEX_ARTI", "CD_ARTI_KARD, CD_ORDO_KARD, SUM (CT_CANT_KARD), SUM (VL_VALO_KARD)", Condicion, MArr())
'  If (Result <> False) Then
'    If Encontro Then
'       c = 0
'       Do While c <= UBound(MArr, 2)
'         Condicion = "CD_ARTI_SALD = '" & MArr(0, c) & Comi
'         Result = LoadData("SAL_ANT", Asterisco, Condicion, Arr())
'         If (Result <> False) Then
'           If Encontro Then
'             If Arr(1) = NUL$ Then Arr(1) = 0
'             If Arr(2) = NUL$ Then Arr(2) = 0
'             If MArr(1, c) = NUL$ Then MArr(1, c) = 0
'             If MArr(2, c) = NUL$ Then MArr(2, c) = 0
'             If MArr(3, c) = NUL$ Then MArr(3, c) = 0
'             If CDbl(MArr(1, c)) = 1 Or CDbl(MArr(1, c)) = 3 Then
'               Arr(1) = CDbl(Arr(1)) + CDbl(MArr(2, c))
'               Arr(2) = CDbl(Arr(2)) + CDbl(MArr(3, c))
'             End If
'             If CDbl(MArr(1, c)) = 6 Or CDbl(MArr(1, c)) = 7 Then
'               Arr(1) = CDbl(Arr(1)) - CDbl(MArr(2, c))
'               Arr(2) = CDbl(Arr(2)) - CDbl(MArr(3, c))
'             End If
'             If Arr(1) = 0 Then Arr(2) = 0
'             Valores = " CT_CANT_SALD= " & Arr(1) & Coma
'             Valores = Valores & " VL_COST_SALD= " & Format(Arr(2), "############0.##00")
'
'             Result = DoUpdate("SAL_ANT", Valores, Condicion)
'           End If
'         End If
'         c = c + 1
'       Loop
'    End If
'  End If
'End Sub

