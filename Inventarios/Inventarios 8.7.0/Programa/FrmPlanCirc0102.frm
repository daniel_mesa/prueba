VERSION 5.00
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "COMDLG32.OCX"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Object = "{C932BA88-4374-101B-A56C-00AA003668DC}#1.1#0"; "MSMASK32.OCX"
Object = "{0BA686C6-F7D3-101A-993E-0000C0EF6F5E}#1.0#0"; "THREED32.OCX"
Object = "{6B7E6392-850A-101B-AFC0-4210102A8DA7}#1.3#0"; "COMCTL32.OCX"
Begin VB.Form FrmPlanCirc0102 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Plano Compra de Medicamentos"
   ClientHeight    =   2865
   ClientLeft      =   6420
   ClientTop       =   5235
   ClientWidth     =   4920
   Icon            =   "FrmPlanCirc0102.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   2865
   ScaleWidth      =   4920
   Begin VB.Timer Timer1 
      Interval        =   950
      Left            =   0
      Top             =   0
   End
   Begin VB.Frame Frame1 
      Height          =   2415
      Left            =   120
      TabIndex        =   0
      Top             =   0
      Width           =   4695
      Begin VB.ComboBox CmbTipoCodigo 
         Height          =   315
         ItemData        =   "FrmPlanCirc0102.frx":058A
         Left            =   1200
         List            =   "FrmPlanCirc0102.frx":0597
         Style           =   2  'Dropdown List
         TabIndex        =   6
         Top             =   600
         Width           =   2175
      End
      Begin MSMask.MaskEdBox MskFechaIni 
         Height          =   285
         Left            =   1200
         TabIndex        =   3
         Top             =   240
         Width           =   1095
         _ExtentX        =   1931
         _ExtentY        =   503
         _Version        =   393216
         AutoTab         =   -1  'True
         MaxLength       =   10
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Format          =   "dd/mm/yyyy"
         Mask            =   "##/##/####"
         PromptChar      =   " "
      End
      Begin MSMask.MaskEdBox MskFechaFin 
         Height          =   285
         Left            =   3480
         TabIndex        =   5
         Top             =   240
         Width           =   1095
         _ExtentX        =   1931
         _ExtentY        =   503
         _Version        =   393216
         AutoTab         =   -1  'True
         MaxLength       =   10
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Format          =   "dd/mm/yyyy"
         Mask            =   "##/##/####"
         PromptChar      =   " "
      End
      Begin ComctlLib.ProgressBar PrgBarra 
         Height          =   255
         Left            =   120
         TabIndex        =   7
         Top             =   1080
         Width           =   4455
         _ExtentX        =   7858
         _ExtentY        =   450
         _Version        =   327682
         Appearance      =   1
      End
      Begin Threed.SSCommand SCmd_Options 
         Height          =   735
         Index           =   0
         Left            =   1320
         TabIndex        =   8
         Top             =   1440
         Width           =   855
         _Version        =   65536
         _ExtentX        =   1508
         _ExtentY        =   1296
         _StockProps     =   78
         Caption         =   "&GENERAR"
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   6
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Picture         =   "FrmPlanCirc0102.frx":05C5
      End
      Begin Threed.SSCommand SCmd_Options 
         Height          =   735
         HelpContextID   =   120
         Index           =   1
         Left            =   2640
         TabIndex        =   9
         ToolTipText     =   "Salir"
         Top             =   1440
         Width           =   855
         _Version        =   65536
         _ExtentX        =   1508
         _ExtentY        =   1296
         _StockProps     =   78
         Caption         =   "&SALIR"
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   6
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Picture         =   "FrmPlanCirc0102.frx":0F47
      End
      Begin VB.Label Label1 
         Caption         =   "Tipo Codigo:"
         Height          =   255
         Left            =   120
         TabIndex        =   11
         Top             =   600
         Width           =   975
      End
      Begin VB.Label Label3 
         Caption         =   "Fecha &Inicial:"
         Height          =   255
         Left            =   120
         TabIndex        =   2
         Top             =   240
         Width           =   1095
      End
      Begin VB.Label Label2 
         Caption         =   "Fecha &Final:"
         Height          =   255
         Left            =   2520
         TabIndex        =   4
         Top             =   240
         Width           =   975
      End
   End
   Begin Threed.SSPanel Panel_Info 
      Height          =   375
      Left            =   120
      TabIndex        =   1
      Top             =   2400
      Width           =   4695
      _Version        =   65536
      _ExtentX        =   8281
      _ExtentY        =   661
      _StockProps     =   15
      BackColor       =   12632256
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Begin MSComctlLib.StatusBar StatusBar 
         Height          =   255
         Left            =   60
         TabIndex        =   10
         Top             =   60
         Width           =   4575
         _ExtentX        =   8070
         _ExtentY        =   450
         _Version        =   393216
         BeginProperty Panels {8E3867A5-8586-11D1-B16A-00C0F0283628} 
            NumPanels       =   2
            BeginProperty Panel1 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
               AutoSize        =   1
               Object.Width           =   6227
               MinWidth        =   2999
            EndProperty
            BeginProperty Panel2 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
               Object.Width           =   1766
               MinWidth        =   1766
               Object.ToolTipText     =   "Tiempo Transcurrido"
            EndProperty
         EndProperty
      End
   End
   Begin MSComDlg.CommonDialog CMDialog 
      Left            =   0
      Top             =   0
      _ExtentX        =   847
      _ExtentY        =   847
      _Version        =   393216
      Filter          =   "Documento de Texto (*.txt)|*.txt| (*.dat)| (*.xls)"
   End
End
Attribute VB_Name = "FrmPlanCirc0102"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'NMSR R1754-1839
Option Explicit
Dim BoClock As Boolean 'True inicia el reloj
Dim ArPlan() As Variant
Dim StNombreArchivo As String
Dim FS
Dim A
Dim ArTipo() As Variant 'SKRV T15941

Private Sub Cbo_ListaPrecios_Change()

End Sub

Private Sub CmbTipoCodigo_GotFocus()
    SendKeys "{F4}" 'AASV M3279 DESPLIEGA EL COMBO CUANDO RECIBE EL FOCO
End Sub

Private Sub CmbTipoCodigo_KeyPress(KeyAscii As Integer)
Call Cambiar_Enter(KeyAscii) 'AASV M3279 CAMBIA EL ENTER POR TAB
End Sub

Private Sub Form_Load()
Call CenterForm(MDI_Inventarios, Me)
MskFechaIni = Hoy
MskFechaFin = Hoy
End Sub

Private Sub MskFechaFin_GotFocus()
    Call MarkFld(MskFechaFin)
    Msglin "Digite la fecha final del rango debe ser mayor a la inicial"
End Sub

Private Sub MskFechaFin_KeyPress(KeyAscii As Integer)
    Call Cambiar_Enter(KeyAscii)
    Call ValKeyFecha(KeyAscii, MskFechaFin)
End Sub


Private Sub MskFechaFin_LostFocus()
    If ValFecha(MskFechaFin, 1) = FAIL Then Exit Sub
    'NMSR M2509
    If CDate(MskFechaIni) > CDate(MskFechaFin) Then
        MsgBox "La fecha inicial debe ser menor o igual" & _
        " que la final, por favor corr�jalas", vbInformation
        'DAHV T4200 - INICIO
        'MskFechaFin.SetFocus: Exit Sub
        Exit Sub
        'DAHV T4200 - FIN
    End If 'NMSR M2509
End Sub

Private Sub MskFechaIni_GotFocus()
    Call MarkFld(MskFechaIni)
    Msglin "Digite la fecha inicial del rango que desea para el informe"
End Sub

Private Sub MskFechaIni_KeyPress(KeyAscii As Integer)
 Call Cambiar_Enter(KeyAscii)
 Call ValKeyFecha(KeyAscii, MskFechaIni)
End Sub

Private Sub MskFechaIni_LostFocus()
    If ValFecha(MskFechaIni, 1) = FAIL Then Exit Sub
    'NMSR M2509
    If CDate(MskFechaIni) > CDate(MskFechaFin) Then
        MsgBox "La fecha inicial debe ser menor o igual" & _
        " que la final, por favor corr�jalas", vbInformation
        ' DAHV T4200
         'MskFechaIni.SetFocus: Exit Sub
         Exit Sub
        ' DAHV T4200 - FIN
    End If 'NMSR M2509
End Sub

Private Sub SCmd_Options_Click(Index As Integer)
Select Case Index
    Case 0:
                 ' DAHV T4200 - INICIO
                  'NMSR M2509
                  If CDate(MskFechaIni) > CDate(MskFechaFin) Then
                    MsgBox "La fecha inicial debe ser menor o igual" & _
                    " que la final, por favor corr�jalas", vbInformation
'                    MskFechaIni.SetFocus: Exit Sub
                    Exit Sub
                  End If
                  'SKRV T14180 inicio
                  If Format(MskFechaIni, "YYYY") <> Format(MskFechaFin, "YYYY") Then
                     MsgBox "La fecha inicial y la fecha final deben pertenecer al mismo periodo fiscal.", vbInformation
                     Exit Sub
                  End If
                  'SKRV T14180 fin
                  'NMSR M2509
                  ' DAHV T4200 - INICIO
                  PrgBarra.Value = 0
                  If BoClock = False Then StatusBar.Panels(2) = "00:00:00"
                  BoClock = True
                  If CmbTipoCodigo.ListIndex = -1 Then Call Mensaje1("Seleccione un Tipo de codigo", 3): Exit Sub 'AASV M3279
                  Call RegControl
                  CMDialog.filename = NUL$
                  BoClock = False
    Case 1: Unload Me
End Select
End Sub

'---------------------------------------------------------------------------------------
' Procedure : BarraProg
' DateTime  : 28/09/2007 15:08
' Author    : natalia_silva
' Purpose   :Inicializa la barra de estado del formulario de liquidacion
'---------------------------------------------------------------------------------------
'
Sub BarraProg(Maximo As Long)
   DoEvents
   PrgBarra.Min = 0
   PrgBarra.Max = Maximo + 1
   PrgBarra.Value = 0
End Sub

'---------------------------------------------------------------------------------------
' Procedure : BarraInfo
' DateTime  : 28/09/2007 15:08
' Author    : natalia_silva
' Purpose   : Coloca un mensaje en la barra de estado del formulario de liquidacion
'---------------------------------------------------------------------------------------
'
Sub BarraInfo(StInfo As String)
    StatusBar.Panels(1).Text = StInfo
    DoEvents
End Sub

'---------------------------------------------------------------------------------------
' Procedure : BarraInc
' DateTime  : 28/09/2007 15:09
' Author    : natalia_silva
' Purpose   : Incrementa la barra de estado del formulario de liquidaci�n
'---------------------------------------------------------------------------------------
'
Sub BarraInc(DbIncremento As Double)
    If DbIncremento < PrgBarra.Max Then
       PrgBarra.Value = DbIncremento
    Else
       PrgBarra.Value = PrgBarra.Max
    End If
End Sub

'---------------------------------------------------------------------------------------
' Procedure : RegControl
' DateTime  : 28/09/2007 15:09
' Author    : natalia_silva
' Purpose   : Genera el Registro de Control para el archivo plano de la Circ 01-02 de 2007
'---------------------------------------------------------------------------------------
'
Function RegControl()
'Dim StCad As String
Dim StCad As Variant
Dim Incont As Integer
Dim InNit As String
Dim feultimodia As Date
Dim intultimodia As Integer
    
    Call BarraInfo("Generando archivo ...")
    
    ReDim ArPlan(0)
    Result = LoadData("ENTIDAD", "CD_NIT_ENTI", NUL$, ArPlan())
    If Result <> FAIL And Encontro Then
        InNit = ArPlan(0)
        If Dir("c:\cntsiste.cnt", vbDirectory) = "" Then Call Mensaje1("La carptea C:\CNTSISTE.CNT no existe", 1):  Exit Function
        
       For Incont = 0 To 1 'LDCR T5987
        'StNombreArchivo = "MEDCO" & Format(FechaServer, "YYYY") & Format(FechaServer, "MM") & Format(FechaServer, "DD") & "NI" & UCase(Trim(ArPlan(0)))'LDCR T5987 SE COLOCA DENTRO DE LA CONDICION
        'INICIO LDCR T5987
        If Incont = 0 Then
            'JJRG T14338
            feultimodia = DateSerial(Year(MskFechaFin), Month(MskFechaFin) + 1, 0) 'ultimo dia del mes
            intultimodia = Mid(feultimodia, 1, 2)
            
            'StNombreArchivo = "MEDCO" & Format(FechaServer, "YYYY") & Format(FechaServer, "MM") & Format(FechaServer, "DD") & "NI" & UCase(Trim(ArPlan(0))) 'LDCR T5987 SE COLOCA EN COMENTARIO
            StNombreArchivo = "MED114" & "MCOM" & Format(MskFechaFin, "YYYY") & Format(MskFechaFin, "MM") & intultimodia & "NI" & UCase(Trim(InNit))
            'JJRG T14338
        Else
            ReDim ArPlan(0)
            'JJRG T14338
            feultimodia = DateSerial(Year(MskFechaFin), Month(MskFechaFin) + 1, 0) 'ultimo dia del mes
            intultimodia = Mid(feultimodia, 1, 2)
            
            'StNombreArchivo = "MEDVN" & Format(FechaServer, "YYYY") & Format(FechaServer, "MM") & Format(FechaServer, "DD") & "NI" & UCase(Trim(ArPlan(0)))'LDCR T5987 SE COLOCA EN COMENTARIO
            'StNombreArchivo = "MEDVN" & Format(FechaServer, "YYYY") & Format(FechaServer, "MM") & Format(FechaServer, "DD") & "NI" & UCase(Trim(InNit))
            StNombreArchivo = "MED113" & "MVEN" & Format(MskFechaFin, "YYYY") & Format(MskFechaFin, "MM") & intultimodia & "NI" & UCase(Trim(InNit)) 'JJRG 12002
            'JJRG T14338
        End If
        
        'FIN LDCR T5987
        Set FS = CreateObject("Scripting.FileSystemObject")
        If Dir("c:\cntsiste.cnt\" & StNombreArchivo & ".txt", vbDirectory) <> "" Then Call FS.DeleteFile("c:\cntsiste.cnt\" & StNombreArchivo & ".txt", True)  'NMSR M2682
        Set A = FS.CreateTextFile("c:\cntsiste.cnt\" & StNombreArchivo & "1.txt", True)
        
        '2.2.1 REGISTRO DE CONTROL (REGISTRO TIPO 1)
        StCad = "1" & Coma 'Tipo Registro
        
        'JJRG 14338
        If Incont = 0 Then
           StCad = StCad & "2" & Coma     'Tipo Registro
        Else
           StCad = StCad & "1" & Coma     'Tipo Registro
        End If
        'JJRG 14338
        
        StCad = StCad & "NI" & Coma 'Tipo Ident de la Entidad
        StCad = StCad & UCase(Trim(InNit)) & Coma 'Nit Entidad
        StCad = StCad & DigitoNIT(CStr(Trim(InNit))) & Coma 'Digito de Verificaci�n
        StCad = StCad & Coma 'Sucursal de la Entidad
        If Incont = 1 Then
        StCad = StCad & Coma 'Tipo de Identificaci�n de la entidad principal a la que pertenece la entidad que reporta
        StCad = StCad & Coma 'N�mero de identificaci�n de la entidad principal a la que pertenece la entidad que reporta
        StCad = StCad & Coma 'Digito de verificaci�n del Nit de la entidad principal a la que pertenece la entidad que reporta
        End If
        StCad = StCad & Format(FechaServer, "YYYY") & Coma 'A�o (AAA) Inform Reportada
        StCad = StCad & Format(MskFechaIni, "MM") & Coma 'Mes Inicial (MM) Inform Reportada
        StCad = StCad & Format(MskFechaFin, "MM") & Coma  'Mes Final (MM) Inform Reportada
         
        If Result = FAIL Then
            Call Mensaje1("Proceso Cancelado", 3)
            Call BarraInfo("Proceso Terminado..")
            Exit Function
        End If
        
        'Call RegDetCompras("c:\cntsiste.cnt\" & StNombreArchivo, StCad) 'LDCR T5987 SE COLOCA DENTRO DE LA CONDICION
        'INICIO LDCR T5987
        If Incont = 0 Then
            Call RegDetCompras("c:\cntsiste.cnt\" & StNombreArchivo, StCad)
        Else
            Call RegDetVentas("c:\cntsiste.cnt\" & StNombreArchivo, StCad)
        End If
       Next Incont
       'FIN LDCR T5987
        Call BarraInfo("Proceso Terminado..")
        If Result <> FAIL Then Call Mensaje1("Archivo plano generado con �xito", 3)
    End If
End Function

'---------------------------------------------------------------------------------------
' Procedure : RegDetCompras
' DateTime  : 02/10/2007 14:06
' Author    : natalia_silva
' Purpose   :Genera el Registro de Detalle de Compras para el archivo plano de la Circ 01-02 de 2007
'---------------------------------------------------------------------------------------
'
Sub RegDetCompras(ByVal stArchivo As String, ByVal StCadRC As String)
   ReDim ArPlan(0)
   Dim StSql As String
   Dim ArArtic() As Variant
   Dim ArFac() As Variant
   Dim Lni As Long
   Dim StFechaIn As String
   Dim StFechaFi As String
   Dim StFechaLim As String
   Dim StCad As String
   'Dim DbTotReg As Double 'JAUM T40880 Se deja linea en comentario
   Dim DbTotComp As Double
   Dim StDato As String
   Dim LnCons As Long
   Dim InContador  As Integer 'LDCR T5871 'LDCR 5987 SE COLOCA EN COMENTARIOS
   
   Dim VaFecha As Variant 'SKRV T14180

    
   'INICIO LDCR NPC T6089
   'StFechaIn = "01/" & Format(MskFechaIni, "MM/YYYY")
   'StFechaLim = "01/" & Format(MskFechaFin, "MM/YYYY")
   'StFechaLim = DateAdd("M", 1, StFechaLim)
   'StFechaFi = DateAdd("M", 1, StFechaIn)
    
   StFechaIn = MskFechaIni
   StFechaLim = MskFechaFin
   'FIN LDCR NPC T6089
    
   'DbTotReg = 0 'JAUM T40880 Se deja linea en comentario
   DbTotComp = 0
   LnCons = 0
   'APGR T4200 - INICIO
   'While StFechaFi <= StFechaLim
   'INICIO LDCR NPC T6089
   'While DateDiff("d", StFechaFi, StFechaLim) >= 0
   If ExisteTABLA("PLANOC01") Then Result = ExecSQLCommand("DROP TABLE PLANOC01") 'LDCR T6562
     
   'While DateDiff("d", StFechaIn, StFechaLim) >= 0 'LDCR NPC T6089 SE COLOCA EN COMENTARIO
   'FIN LDCR NPC T6089
   'APGR T4200 - FIN
    
   'StSql = "SELECT * INTO PLANOC01 FROM VW_PLANOCIR01" 'SE COLOCA EN COMENTARIOS LDCR T5871
   'StSql = StSql & " WHERE FE_FECH_COMP>=" & FFechaCon(StFechaIn & " 00:00:00") 'Falta partir las fechas
   'StSql = StSql & " AND FE_FECH_COMP<" & FFechaCon(StFechaFi & " 23:59:59")

       
   'Dim Contador  As Integer 'LDCR T5871
   'For Contador = 0 To 1 'LDCR T5871
   'For InContador = 0 To 1 'LDCR T5871 'LDCR 5987 SE COLOCA EN COMENTARIOS
    
   'If Contador = 0 Then 'LDCR T5871
   'If InContador = 0 Then 'LDCR T5871 'LDCR 5987 SE COLOCA EN COMENTARIOS
   StSql = "SELECT * INTO PLANOC01 FROM VW_PLANOCIR01"
   'INICIO LDCR T6089
   'StSql = StSql & " WHERE FE_FECH_COMP>=" & FFechaCon(StFechaIn & " 00:00:00")
            
   'INICIO LDCR 0
   'StSql = StSql & " WHERE FE_FECH_COMP>=" & Comi & Format(StFechaIn & " 00:00:00", "YYYY/MM/DD HH:MM:SS") & Comi
   StSql = StSql & " WHERE FE_FECH_COMP>=" & FHFECHA(CDate(StFechaIn) & " 00:00:00")
   'FINICIO LDCR 0
            
   'StSql = StSql & " AND FE_FECH_COMP<" & FFechaCon(StFechaFi & " 23:59:59")
            
   'INICIO LDCR 0
   'StSql = StSql & " AND FE_FECH_COMP<" & Comi & Format(StFechaLim & " 23:59:59", "YYYY/MM/DD HH:MM:SS") & Comi
   StSql = StSql & " AND FE_FECH_COMP<" & FHFECHA(CDate(StFechaLim) & " 23:59:59")
   'FIN LDCR  0
            
   'FIN LDCR T6089
   'StSql = StSql & " AND CD_CODI_ARTI  not in (select CD_CODI_ARTI  from ARTICULO" 'LDCR 5987 SE COLOCA EN COMENTARIO
   'StSql = StSql & " WHERE  ID_TIPO_ARTI = 2 or ID_TIPO_ARTI = 3 or ID_TIPO_ARTI = 4)"
   'INICIO LDCR 5987 SE COLOCA EN COMENTARIOS
   'Else
   'StSql = "SELECT * INTO PLANOC01 FROM VW_PLANOCIR02"  'LDCR T5865
   'StSql = StSql & " WHERE FECHA>=" & FFechaCon(StFechaIn & " 00:00:00")
   'StSql = StSql & " AND FECHA<" & FFechaCon(StFechaFi & " 23:59:59")
   'End If
   ''FIN  LDCR T5871
   'FIN LDCR 5987 SE COLOCA EN COMENTARIOS
   Result = ExecSQLCommand(StSql)
     
        
   If Result <> FAIL Then
            
      Dim i As Integer 'SKRV T14180
      For i = Format(StFechaIn, "MM") To Format(StFechaLim, "MM") 'SKRV T14180
         'ReDim ArArtic(0, 0)
         'ReDim ArArtic(2, 0) 'AASV M3279
         ReDim ArArtic(3, 0) 'SKRV T14180
         'Result = LoadMulData("PLANOC01", "DISTINCT CD_CODI_ARTI", NUL$, ArArtic)
         'Result = LoadMulData("PLANOC01", "DISTINCT CD_CODI_ARTI,CD_RIPS_ARTI,NU_CODCUM_ARTI", NUL$, ArArtic) 'AASV M3279
                  
         'SKRV T14180: Se modifica consulta agregando el campo FE_FECH_COMP obteniendo solo el mes y condicionando a que el mes sea = al del indice
         Result = LoadMulData("PLANOC01", "DISTINCT CD_CODI_ARTI,CD_RIPS_ARTI,NU_CODCUM_ARTI, MONTH(FE_FECH_COMP)as Mes", " MONTH(FE_FECH_COMP)='" & i & "'", ArArtic) 'SKRV T14180
                                    
         If Result <> FAIL And Encontro Then
            'select ID_TIPO_ARTI from articulo where cd_codi_arti = 'A02AA018231'
            'DbTotReg = DbTotReg + UBound(ArArtic, 2) + 1 'JAUM T40880 Se deja linea en comentario
            For Lni = 0 To UBound(ArArtic, 2)
                      
               'SKRV T15941 INICIO
               ReDim ArTipo(0)
               Result = LoadData("ARTICULO", "ID_TIPO_ARTI", "CD_CODI_ARTI='" & ArArtic(0, Lni) & "'", ArTipo)
               If Result <> FAIL And Encontro Then
                  If ArTipo(0) = 0 Or ArTipo(0) = 1 Then ' {0=No Pos}{1=Pos} si se cumple condicion hace lo que hacia antes
                     LnCons = LnCons + 1
                     '2.2.2 REGISTRO DE DETALLE DE COMPRAS (REGISTRO TIPO 2)
                     Call BarraProg(UBound(ArArtic, 2))
                     StCad = "2" & Coma     'Tipo Registro
                     StCad = StCad & LnCons & Coma  'Consecutivo
                     'SKRV T14180 INICIO
                     'StCad = StCad & Format(StFechaIn, "MM") & Coma 'Mes Inicial (MM) Inform Reportada
                     If i > 9 Then
                        StCad = StCad & i & Coma 'Mes
                     Else
                        StCad = StCad & "0" & i & Coma 'Mes
                     End If
                     'SKRV T14180 FIN
                     'StCad = StCad & ArArtic(0, Lni) & Coma ' Codigo Articulo
                     'AASV M3279
                     Select Case CmbTipoCodigo.ListIndex
                        Case 0
                           StCad = StCad & ArArtic(0, Lni) & Coma 'CODIGO DEL ARTICULO
                        Case 1
                           StCad = StCad & ArArtic(1, Lni) & Coma 'CODIGO RIPS DEL ARTICULO
                        Case 2
                           StCad = StCad & ArArtic(2, Lni) & Coma 'CODIGO CUM DEL ARTICULO
                     End Select
                     'AASV M3279
                     Campos = "MIN(MINIMO)AS MI"
                     Campos = Campos & ",MAX(MAXIMO) AS MA"
                     Campos = Campos & ",SUM(COMP_NETA) AS COM_NET"
                     Campos = Campos & ",SUM(UNID_COMPR) AS UNI_COMP"
                                   
                     ReDim ArPlan(3)
                     'JAUM T34493 Inicio se deja linea en comentario
                     'Result = LoadData("PLANOC01", Campos, "CD_CODI_ARTI ='" & ArArtic(0, Lni) & "' ORDER BY 1", ArPlan)
                     Condicion = "CD_CODI_ARTI ='" & ArArtic(0, Lni) & "' AND MONTH(FE_FECH_COMP) = '"
                     Condicion = Condicion & i & "' ORDER BY 1"
                     Result = LoadData("PLANOC01", Campos, Condicion, ArPlan)
                     'JAUM T34493 Fin
                     If Result <> FAIL And Encontro Then
                        StCad = StCad & ArPlan(0) & Coma 'Precio Unitario M�nimo de Compra
                        StCad = StCad & ArPlan(1) & Coma 'Precio Unitario M�ximo de Compra
                        StCad = StCad & ArPlan(2) & Coma 'Valor Total de Compras Netas
                        DbTotComp = DbTotComp + CDbl(ArPlan(2))
                        StCad = StCad & ArPlan(3) & Coma 'Total Unidades Compradas
                     End If
                                   
                     Condicion = "MINIMO = " & ArPlan(0)
                     Condicion = Condicion & " AND CD_CODI_ARTI ='" & ArArtic(0, Lni) & "'"
                     Condicion = Condicion & " AND MONTH(FE_FECH_COMP) = '" & i & Comi 'JAUM T34493
                     Condicion = Condicion & " ORDER BY 1"
                     ReDim ArFac(0)
                     'Result = LoadData("PLANOC01", "TX_FAPR_COMP", Condicion, ArFac)
                     'INICIO  LDCR T5871
                     'If Contador = 0 Then LDCR T5871
                     'If InContador = 0 Then 'LDCR 5987 SE COLOCA EN COMENTARIOS
                     Result = LoadData("PLANOC01", "TX_FAPR_COMP", Condicion, ArFac)
                     'Else
                     'Result = LoadData("PLANOC01", "CODIGO", Condicion, ArFac)
                     'End If
                     ''FIN LDCR T5871
                     'FIN LDCR 5987 SE COLOCA EN COMENTARIOS
                     If Result <> FAIL And Encontro Then
                        StCad = StCad & ArFac(0) & Coma 'N� Fac del Precio Unitario M�nimo de Compra
                     End If
                                   
                     Condicion = "MAXIMO = " & ArPlan(1)
                     Condicion = Condicion & " AND CD_CODI_ARTI ='" & ArArtic(0, Lni) & "'"
                     Condicion = Condicion & " AND MONTH(FE_FECH_COMP) = '" & i & Comi 'JAUM T34493
                     Condicion = Condicion & " ORDER BY 1"
                     ReDim ArFac(0)
                     'Result = LoadData("PLANOC01", "TX_FAPR_COMP", Condicion, ArFac)
                     'INICIO  LDCR T5871
                     'If Contador = 0 Then LDCR T5871
                     'If InContador = 0 Then 'LDCR 5987 SE COLOCA EN COMENTARIOS
                     Result = LoadData("PLANOC01", "TX_FAPR_COMP", Condicion, ArFac)
                     'INICIO LDCR 5987 SE COLOCA EN COMENTARIOS
                     'Else
                     'Result = LoadData("PLANOC01", "CODIGO", Condicion, ArFac)
                     'End If
                     ''FIN LDCR T5871
                     'FIN LDCR 5987 SE COLOCA EN COMENTARIOS
                     If Result <> FAIL And Encontro Then
                        StCad = StCad & ArFac(0)  'N� Fac del Precio Unitario M�ximo de Compra
                     End If
                     If Result = FAIL Then
                        Call Mensaje1("Proceso Cancelado", 3)
                        Call BarraInfo("Proceso Terminado..")
                        Exit Sub
                     End If
                     A.WriteLine (StCad)
                     Call BarraInc(Lni + 1)
                  End If
               End If
               'SKRV T15941 FIN
            Next
         End If
      Next
      Result = ExecSQLCommand("DROP TABLE PLANOC01")
   End If
   'Next 'LDCR T5871 '5987 SE COLOCA EN COMENTARIOS
        
   'If ExisteTABLA("PLANOC01") Then Result = ExecSQLCommand("DROP TABLE PLANOC01") 'LDCR T6562 SE COLOCA EN  COMENTARIO  LDCR NPC T6562
   'INICIO LDCR T6089
   'StFechaIn = StFechaFi
   'StFechaFi = DateAdd("M", 1, StFechaIn)
   StFechaIn = DateAdd("D", 1, StFechaIn)
   'FIN LDCR T6089
       
   'Wend 'LDCR NPC T6089 SE COLOCA EN COMENTARIO
   A.Close
    
   'StCadRC = StCadRC & DbTotReg & Coma 'N� Total Reg Detall 'JAUM T40880 Se deja linea en comentario
   StCadRC = StCadRC & LnCons & Coma 'N� Total Reg Detall 'JAUM T40880
   StCadRC = StCadRC & DbTotComp & Coma 'Sum Total Compras
   StCadRC = StCadRC & "0" 'Sum Total Recobros

    
   Open stArchivo & ".txt" For Append As #1
      Print #1, StCadRC
      Open stArchivo & "1.txt" For Input As #2
         Do While Not EOF(2)
            Line Input #2, StDato
            Print #1, StDato
         Loop
      Close #2
   Close #1
   Kill (stArchivo & "1.txt")
End Sub
'---------------------------------------------------------------------------------------
' Procedure : Timer1_Timer
' DateTime  : 02/10/2007 12:09
' Author    : natalia_silva
' Purpose   : Coloca el tiempo que va transcurriendo en el StatusBar
'---------------------------------------------------------------------------------------
'
Private Sub Timer1_Timer()
Dim S, m, Y As Integer
   
   DoEvents
   If BoClock = True Then
      Y = CInt(Trim(Mid(StatusBar.Panels(2), 1, 2)))
      m = CInt(Trim(Mid(StatusBar.Panels(2), 4, 2)))
      S = CInt(Trim(Mid(StatusBar.Panels(2), 7, 2)))
      If S = 59 Then
         S = 0
         If m = 59 Then
            m = 0
            Y = Y + 1
         Else
            m = m + 1
         End If
      Else
         S = S + 1
      End If
  
      If Len(S) < 2 Then S = "0" & S
      If Len(m) < 2 Then m = "0" & m
      If CStr(Len(Y)) < 2 Then Y = "0" & Y
      
      StatusBar.Panels(2) = "0" & Y & ":" & m & ":" & S
      DoEvents
   End If
End Sub
'NMSR R1754-1839

'---------------------------------------------------------------------------------------
' Procedure : RegDetVentas
' DateTime  : 02/05/2011 12:26
' Author    : Luis David Colmenares Rincon
' Purpose   : Genera el Registro de Detalle de Compras para el archivo plano de la Circ 01-02 de 2007
'---------------------------------------------------------------------------------------
'INICIO LDCR T5987
Sub RegDetVentas(ByVal stArchivo As String, ByVal StCadRC As String)
ReDim ArPlan(0)
Dim StSql As String
Dim ArArtic() As Variant
Dim ArFac() As Variant
Dim Lni As Long
Dim StFechaIn As String
Dim StFechaFi As String
Dim StFechaLim As String
Dim StCad As String
Dim DbTotReg As Double
Dim DbTotComp As Double
Dim StDato As String
Dim LnCons As Long
'INICIO LDCR T5871
Dim InContador  As Integer
Dim Incont As Integer
'Dim InCant As Integer 'LDCR T7319 SE COLOCA EN COMENTARIO DADO Q NO SCE ESTA USANDO
'FIN LDCR T5871

    'INICIO LDCR T6089
'    StFechaIn = "01/" & Format(MskFechaIni, "MM/YYYY")
'    StFechaLim = "01/" & Format(MskFechaFin, "MM/YYYY")
'    StFechaLim = DateAdd("M", 1, StFechaLim)
'    StFechaFi = DateAdd("M", 1, StFechaIn)
    
    StFechaIn = MskFechaIni
    StFechaLim = MskFechaFin
    'FIN LDCR T6089
    
    DbTotReg = 0
    DbTotComp = 0
    LnCons = 0
    If ExisteTABLA("PLANOC01") Then Result = ExecSQLCommand("DROP TABLE PLANOC01") 'LDCR T6562
    'While DateDiff("d", StFechaFi, StFechaLim) >= 0
    'While DateDiff("d", StFechaIn, StFechaLim) >= 0 'LDCR NPC T6089 SE COLOCA EN COMENTARIO
    
            StSql = "SELECT * INTO PLANOC01 FROM VW_PLANOCIR02"
            'INICIO LDCR T6089
            'StSql = StSql & " WHERE FECHA>=" & FFechaCon(StFechaIn & " 00:00:00")
            'INICIO LDCR T6089
            'StSql = StSql & " WHERE FECHA>=" & Comi & Format(StFechaIn & " 00:00:00", "YYYY/MM/DD HH:MM:SS") & Comi
            StSql = StSql & " WHERE FECHA>=" & FHFECHA(StFechaIn & " 00:00:00")
            'FIN LDCR T6089
            'StSql = StSql & " AND FECHA<" & FFechaCon(StFechaFi & " 23:59:59")
            
            'INICIO LDCR T6089
            'StSql = StSql & " AND FECHA<" & Comi & Format(StFechaLim & " 23:59:59", "YYYY/MM/DD HH:MM:SS") & Comi
            StSql = StSql & " AND FECHA<" & FHFECHA(StFechaLim & " 23:59:59")
            'FIN LDCR T6089
            'FIN LDCR T6089
            
       Result = ExecSQLCommand(StSql)
     
        
        If Result <> FAIL Then
            
            Dim i As Integer 'SKRV T14180
            For i = Format(StFechaIn, "MM") To Format(StFechaLim, "MM") 'SKRV T14180
            
            'ReDim ArArtic(2, 0)
            ReDim ArArtic(3, 0) 'SKRV T14180
            'Result = LoadMulData("PLANOC01", "DISTINCT CD_CODI_ARTI,CD_RIPS_ARTI,NU_CODCUM_ARTI", NUL$, ArArtic) 'AASV M3279
            'SKRV T14180: Se modifica consulta agregando el campo FE_FECH_COMP obteniendo solo el mes y condicionando a que el mes sea = al del indice
            Result = LoadMulData("PLANOC01", "DISTINCT CD_CODI_ARTI,CD_RIPS_ARTI,NU_CODCUM_ARTI, MONTH(FECHA)as Mes", " MONTH(FECHA)='" & i & "'", ArArtic)

            If Result <> FAIL And Encontro Then
                DbTotReg = DbTotReg + UBound(ArArtic, 2) + 1
                For Lni = 0 To UBound(ArArtic, 2)
                
                  'SKRV T15941 INICIO
                   ReDim ArTipo(0)
                   Result = LoadData("ARTICULO", "ID_TIPO_ARTI", "CD_CODI_ARTI='" & ArArtic(0, Lni) & "'", ArTipo)
                   If Result <> FAIL And Encontro Then
                      If ArTipo(0) = 0 Or ArTipo(0) = 1 Then ' {0=No Pos}{1=Pos} si se cumple condicion hace lo que hacia antes
                              LnCons = LnCons + 1
                          Call BarraProg(UBound(ArArtic, 2))
                          StCad = "2" & Coma
                          StCad = StCad & LnCons & Coma
                          'SKRV T14180 INICIO
                          'StCad = StCad & Format(StFechaIn, "MM") & Coma
                          If i > 9 Then
                            StCad = StCad & i & Coma 'Mes
                          Else
                            StCad = StCad & "0" & i & Coma 'Mes
                          End If
                          'SKRV T14180 FIN
                          StCad = StCad & "COM" & Coma
                          'INICIO LDCR T5987 SE  ABILITA LA OPCION
      '                    Select Case CmbTipoCodigo.ListIndex
      '                      Case 0
      '                            StCad = StCad & ArArtic(0, Lni) & Coma 'CODIGO DEL ARTICULO
      '                      Case 1
      '                            StCad = StCad & ArArtic(1, Lni) & Coma 'CODIGO RIPS DEL ARTICULO
      '                      Case 2
                             '     StCad = StCad & ArArtic(2, Lni) & Coma 'CODIGO CUM DEL ARTICULO
      '                    End Select
      
                          Select Case CmbTipoCodigo.ListIndex
                            Case 0
                                  StCad = StCad & ArArtic(0, Lni) & Coma 'CODIGO DEL ARTICULO
                            Case 1
                                  StCad = StCad & ArArtic(1, Lni) & Coma 'CODIGO RIPS DEL ARTICULO
                            Case 2
                                  StCad = StCad & ArArtic(2, Lni) & Coma 'CODIGO CUM DEL ARTICULO
                          End Select
                          'FIN LDCR T5987
                          StCad = StCad & "SI" & Coma
                          Campos = "MIN(MINIMO)AS MI"
                          Campos = Campos & ",MAX(MAXIMO) AS MA"
                          Campos = Campos & ",SUM(COMP_NETA) AS COM_NET"
                          Campos = Campos & ",SUM(UNID_COMPR) AS UNI_COMP"
                          
                          ReDim ArPlan(3)
                          'Result = LoadData("PLANOC01", Campos, "CD_CODI_ARTI ='" & ArArtic(0, Lni) & "' ORDER BY 1", ArPlan) 'JLPB T24287 SE DEJA EN COMENTARIO
                          Result = LoadData("PLANOC01", Campos, "CD_CODI_ARTI ='" & ArArtic(0, Lni) & Comi & " AND MONTH(FECHA)='" & i & Comi & " ORDER BY 1", ArPlan) 'JLPB T24287
                          If Result <> FAIL And Encontro Then
                              StCad = StCad & ArPlan(0) & Coma 'Precio Unitario M�nimo de Compra
                              StCad = StCad & ArPlan(1) & Coma 'Precio Unitario M�ximo de Compra
                              StCad = StCad & ArPlan(2) & Coma 'Valor Total de Compras Netas
                              DbTotComp = DbTotComp + CDbl(ArPlan(2))
                              'InCant = InCant + ArPlan(3) 'LDCR T5987  'LDCR T7319  SE COLOCA EN COMENTARIO DADO Q NO SCE ESTA USANDO
                              StCad = StCad & ArPlan(3) & Coma 'Total Unidades Compradas
                          End If
                          
                          Condicion = "MINIMO = " & ArPlan(0)
                          Condicion = Condicion & " AND CD_CODI_ARTI ='" & ArArtic(0, Lni) & "'"
                          Condicion = Condicion & " AND MONTH(FECHA)='" & i & Comi 'JLPB T24287
                          'Condicion = Condicion & " ORDER BY 1" 'LDCR T6070
                          'Condicion = Condicion & " ORDER BY 1 DESC" 'LDCR T6070  'LDCR NPC T6070
                          ReDim ArFac(0)
                          
                          'Result = LoadData("PLANOC01", "CODIGO", Condicion, ArFac)
                          'Result = LoadData("PLANOC01", "MAX(CODIGO)", Condicion, ArFac)
                          Result = LoadData("PLANOC01", "MIN(CODIGO)", Condicion, ArFac) 'JLPB T24287
                          
                          If Result <> FAIL And Encontro Then
                              StCad = StCad & ArFac(0) & Coma 'N� Fac del Precio Unitario M�nimo de Compra
                          End If
                          
                          Condicion = "MAXIMO = " & ArPlan(1)
                          Condicion = Condicion & " AND CD_CODI_ARTI ='" & ArArtic(0, Lni) & "'"
                          Condicion = Condicion & " AND MONTH(FECHA)='" & i & Comi 'JLPB T24287
                          'Condicion = Condicion & " ORDER BY 1" 'LDCR NPC T6070
                          ReDim ArFac(0)
                         
                          'Result = LoadData("PLANOC01", "CODIGO", Condicion, ArFac)
                          Result = LoadData("PLANOC01", "MAX(CODIGO)", Condicion, ArFac)
                          If Result <> FAIL And Encontro Then
                              StCad = StCad & ArFac(0)  'N� Fac del Precio Unitario M�ximo de Compra
                          End If
                          If Result = FAIL Then
                              Call Mensaje1("Proceso Cancelado", 3)
                              Call BarraInfo("Proceso Terminado..")
                              Exit Sub
                          End If
                          A.WriteLine (StCad)
                          Call BarraInc(Lni + 1)
                      End If
                   End If
                   'SKRV T15941 FIN
                Next
            End If
            Next
            Result = ExecSQLCommand("DROP TABLE PLANOC01")
          End If
        'If ExisteTABLA("PLANOC01") Then Result = ExecSQLCommand("DROP TABLE PLANOC01")  'LDCR T6562SE COLOCA EN  COMENTARIO  LDCR NPC T6562
        'INICIO LDCR T6089
        'StFechaIn = StFechaFi
        'StFechaFi = DateAdd("M", 1, StFechaIn)
         StFechaIn = DateAdd("D", 1, StFechaIn)
        'FIN LDCR T6089
    'Wend  'LDCR NPC T6089 SE COLOCA EN COMENTARIO
    A.Close
    
    'INICIO LDCR T5987
    Incont = 1
    For InContador = 0 To 4
       Incont = InStr(Incont, StCadRC, ",")
       If Incont > 1 Then Incont = Incont + 1
    Next InContador
    
    
    'StCadRC = Left(StCadRC, Incont - 2) & Replace(StCadRC, ",", "," & inCant, Incont - 1, 1)
     StCadRC = Left(StCadRC, Incont - 2) & Replace(StCadRC, ",", "," & DbTotReg, Incont - 1, 1)
    'FIN LDCR T5987
    StCadRC = StCadRC & DbTotReg & Coma 'N� Total Reg Detall
    'StCadRC = StCadRC & DbTotComp & Coma 'Sum Total Compras 'LDCR T5987
    StCadRC = StCadRC & DbTotComp  'Sum Total Compras
    
    'StCadRC = StCadRC & "0" 'Sum Total Recobros 'LDCR T5987

    
    Open stArchivo & ".txt" For Append As #1
        Print #1, StCadRC
        Open stArchivo & "1.txt" For Input As #2
            Do While Not EOF(2)
                Line Input #2, StDato
                Print #1, StDato
            Loop
        Close #2
    Close #1
    Kill (stArchivo & "1.txt")
'FIN LDCR T5987
End Sub

