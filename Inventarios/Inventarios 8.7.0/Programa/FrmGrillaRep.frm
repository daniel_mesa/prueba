VERSION 5.00
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "MSFLXGRD.OCX"
Begin VB.Form FrmGrillaRep 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Form1"
   ClientHeight    =   6045
   ClientLeft      =   45
   ClientTop       =   435
   ClientWidth     =   12390
   Icon            =   "FrmGrillaRep.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   6045
   ScaleWidth      =   12390
   ShowInTaskbar   =   0   'False
   StartUpPosition =   3  'Windows Default
   Begin VB.CommandButton Cmd_Aceptar 
      Caption         =   "&Aceptar"
      Height          =   350
      Left            =   9900
      TabIndex        =   3
      Top             =   5580
      Width           =   1095
   End
   Begin VB.CommandButton Cmd_Cancelar 
      Caption         =   "&Cancelar"
      Height          =   350
      Left            =   11160
      TabIndex        =   2
      Top             =   5580
      Width           =   1095
   End
   Begin VB.Frame Fram_Datos 
      Caption         =   "Frame1"
      Height          =   5415
      Left            =   120
      TabIndex        =   0
      Top             =   120
      Width           =   12135
      Begin MSFlexGridLib.MSFlexGrid Msf_Datos 
         Height          =   4935
         Left            =   180
         TabIndex        =   1
         Top             =   300
         Width           =   11775
         _ExtentX        =   20770
         _ExtentY        =   8705
         _Version        =   393216
         AllowUserResizing=   1
      End
   End
End
Attribute VB_Name = "FrmGrillaRep"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Public Grupo As Integer
Public Rep   As Integer
Public FD    As String
Public FH    As String
Public Neg   As String
Public cont  As String
Public Brk   As String

Private Sub Cmd_Aceptar_Click()
   'Inserta los registros en la tabla temporal
   If (BeginTran(STranIns & "TMP_GENERICA") <> FAIL) Then
      For i = 1 To Msf_Datos.Rows - 1
         DoEvents
         With Msf_Datos
            Valores = "NU_AUTO_DATO = " & i & Coma
            Valores = Valores & "NU_AUTO_CONE = " & NumConex & Coma
            Valores = Valores & "TX_TEXT_2 = '" & .TextMatrix(i, 0) & "/" & .TextMatrix(i, 1) & Comi & Coma
            Valores = Valores & "NU_VALOR_2 = " & .TextMatrix(i, 5) & Coma
            Valores = Valores & "NU_VALOR_3 = " & .TextMatrix(i, 6) & Coma
            Valores = Valores & "NU_VALOR_4 = " & .TextMatrix(i, 7) & Coma
            Valores = Valores & "NU_VALOR_5 = " & .TextMatrix(i, 8) & Coma
            Valores = Valores & "NU_VALOR_6 = " & .TextMatrix(i, 9) & Coma
            Valores = Valores & "TX_TEXT_3 = '" & .TextMatrix(i, 10) & Comi & Coma
            Valores = Valores & "NU_VALOR_7 = " & .TextMatrix(i, 11) & Coma
            Valores = Valores & "NU_VALOR_8 = " & .TextMatrix(i, 12) & Coma
            Valores = Valores & "TX_TEXT_4 = '" & .TextMatrix(i, 13) & Comi & Coma
            Valores = Valores & "NU_VALOR_9 = " & .TextMatrix(i, 14) & Coma
            If Me.Msf_Datos.TextMatrix(i, 15) = NUL$ Then
               Call Mensaje1("Los datos estan incompletos", 3)
               Call RollBackTran
               Exit Sub
            End If
            Valores = Valores & "NU_VALOR_10 = " & .TextMatrix(i, 15) & Coma
            Valores = Valores & "NU_VALOR_11 = " & .TextMatrix(i, 16) & Coma
            Valores = Valores & "TX_TEXT_5 = '" & .TextMatrix(i, 17) & Comi & Coma
            Valores = Valores & "TX_TEXT_6 = '" & .TextMatrix(i, 18) & Comi & Coma
            Valores = Valores & "NU_AUTO_1 = " & .TextMatrix(i, 19) & Coma
            
            If Valores <> NUL$ Then Result = DoInsertSQL("TMP_GENERICA", Valores, "N")
            If Result = FAIL Then Exit For
         End With
      Next i
   End If
   
   If (Result <> FAIL) Then
      If (CommitTran() <> FAIL) Then
      Else
         Call RollBackTran
      End If
   Else
      Call RollBackTran
   End If
   Unload Me
End Sub

Private Sub Cmd_Cancelar_Click()
   Unload Me
End Sub

Private Sub Form_Load()
   Dim arrtmp() As Variant
   
   Call CenterForm(MDI_Inventarios, Me)
   If Grupo = 6 Then
      Select Case Rep
         Case 0
         With Msf_Datos
            .Cols = 21
            .Rows = 1
            .ColWidth(0) = 450: .TextMatrix(0, 0) = "A�o"
            .ColWidth(1) = 400: .TextMatrix(0, 1) = "Mes"
            
            'Criterio de consulta
            .ColWidth(2) = 0: .TextMatrix(0, 2) = "Contrato"
            .ColWidth(3) = IIf(Neg <> NUL$, 2000, 0): .TextMatrix(0, 3) = "Negocio"
            .ColWidth(4) = 0: .TextMatrix(0, 4) = "Broker"
            
            'Datos del reporte
            .ColWidth(5) = 1200: .TextMatrix(0, 5) = "Facturado"
            .ColWidth(6) = 1200: .TextMatrix(0, 6) = "Deducible"
            .ColWidth(7) = 1200: .TextMatrix(0, 7) = "Presentado"
            .ColWidth(8) = 1200: .TextMatrix(0, 8) = "Reembolsado"
            .ColWidth(9) = 1500: .TextMatrix(0, 9) = "No. Liquidaciones"
            .ColWidth(10) = 1500: .TextMatrix(0, 10) = "% Siniestralidad"
            .ColWidth(11) = 1500: .TextMatrix(0, 11) = "Facturaci�n Acum"
            .ColWidth(12) = 1400: .TextMatrix(0, 12) = "Reembolsos Acum"
            .ColWidth(13) = 1600: .TextMatrix(0, 13) = "Siniestralidad Acum"
            .ColWidth(14) = 1300: .TextMatrix(0, 14) = "Gastos Cuenta"
            .ColWidth(15) = 1200: .TextMatrix(0, 15) = "Gasto Total"
            .ColWidth(16) = 1500: .TextMatrix(0, 16) = "Gasto Total Acum"
            .ColWidth(17) = 2000: .TextMatrix(0, 17) = "Gasto Total/ Facturado"
            .ColWidth(18) = 1500: .TextMatrix(0, 18) = "Siniestralidad Acum"
            .ColWidth(19) = 0: .TextMatrix(0, 19) = "Autonumerico Negocio"
            .ColWidth(20) = 0: .TextMatrix(0, 20) = "Autonumerico Contrato"
         End With
      End Select
   End If
      
   'Carga datos en la Grilla - Facturado
   If Neg <> NUL$ And cont = NUL$ And Brk = NUL$ Then
      ReDim arr(3, 0)
   ElseIf Neg = NUL$ And cont <> NUL$ And Brk = NUL$ Then
      ReDim arr(3, 0)
   ElseIf Neg = NUL$ And cont = NUL$ And Brk <> NUL$ Then
      ReDim arr(3, 0)
   ElseIf Neg = NUL$ And cont <> NUL$ And Brk <> NUL$ Then
      ReDim arr(4, 0)
   ElseIf Neg <> NUL$ And cont <> NUL$ And Brk = NUL$ Then
      ReDim arr(4, 0)
   ElseIf Neg <> NUL$ And cont = NUL$ And Brk <> NUL$ Then
      ReDim arr(4, 0)
   ElseIf Neg <> NUL$ And cont <> NUL$ And Brk <> NUL$ Then
      ReDim arr(5, 0)
   Else
      ReDim arr(2, 0)
   End If
   Campos = "TO_CHAR(TO_DATE(TIPO_MOVIMIENTO.FE_GENE_TIMO), 'YYYY') AS ANNO, "
   Campos = Campos & "TO_CHAR(TO_DATE(TIPO_MOVIMIENTO.FE_GENE_TIMO), 'MM') AS MES, "
   Campos = Campos & "SUM(TIPO_MOVIMIENTO.NU_VALO_TIMO) "
   'Negocio
   If Neg <> NUL$ Then Campos = Campos & ", TIPO_MOVIMIENTO.NU_AUTO_NEGO_TIMO"
   'Contrato
   If cont <> NUL$ Then Campos = Campos & ", TIPO_MOVIMIENTO.NU_AUTO_CONT"
   Desde = "TIPO_MOVIMIENTO "
   Condicion = "TIPO_MOVIMIENTO.TX_TIPO_TIMO IN ('FA', 'FM') "
   Condicion = Condicion & "AND TIPO_MOVIMIENTO.TX_ESTA_TIMO <> 'A' "
   'Fecha
   If FD <> NUL$ And FH <> NUL$ Then
      Condicion = Condicion & " AND TO_DATE(TIPO_MOVIMIENTO.FE_GENE_TIMO,'DD/MM/YYYY HH24:MI:SS') >= " & FFechaCon(FD & " 00:00:00")
      Condicion = Condicion & " AND TO_DATE(TIPO_MOVIMIENTO.FE_GENE_TIMO,'DD/MM/YYYY HH24:MI:SS') <= " & FFechaCon(FH & " 23:59:59")
   End If
   'Contrato
   If cont <> NUL$ Then
      If cont <> "Todos" Then Condicion = Condicion & " AND TIPO_MOVIMIENTO.NU_AUTO_CONT_TIMO = " & cont
   End If
   If Neg <> NUL$ And Neg <> "Todos" Then Condicion = Condicion & " AND TIPO_MOVIMIENTO.NU_AUTO_NEGO_TIMO In (" & Neg & ") "
   Condicion = Condicion & "GROUP BY TO_CHAR(TO_DATE(TIPO_MOVIMIENTO.FE_GENE_TIMO), 'YYYY'), "
   Condicion = Condicion & "TO_CHAR(TO_DATE(TIPO_MOVIMIENTO.FE_GENE_TIMO), 'MM') "
   If Neg <> NUL$ Then Condicion = Condicion & ", TIPO_MOVIMIENTO.NU_AUTO_NEGO_TIMO "
   If cont <> NUL$ Then Condicion = Condicion & ", TIPO_MOVIMIENTO.NU_AUTO_CONT"
   Condicion = Condicion & "ORDER BY 1, 2 "
   Result = LoadMulData(Desde, Campos, Condicion, arr)

   If Result <> FAIL And Encontro Then
      For i = 0 To UBound(arr, 2)
         With Msf_Datos
            If Neg <> NUL$ Then
               ReDim arrtmp(0)
               Result = LoadData("NEGOCIO", "TX_DESC_NEGO", "NU_AUTO_NEGO = " & arr(3, i), arrtmp)
            End If
            .AddItem arr(0, i) & vbTab & arr(1, i)
            If Neg <> NUL$ Then .TextMatrix(i + 1, 3) = arrtmp(0)
            .TextMatrix(i + 1, 5) = arr(2, i)
            If Neg <> NUL$ Then
               .TextMatrix(i + 1, 19) = arr(3, i)
               .TextMatrix(i + 1, 20) = arr(4, i)
            End If
         End With
      Next i
   End If
   
   'Carga datos en la Grilla - Deducible
   If Neg <> NUL$ And cont = NUL$ And Brk = NUL$ Then
      ReDim arr(6, 0)
   ElseIf Neg = NUL$ And cont <> NUL$ And Brk = NUL$ Then
      ReDim arr(6, 0)
   ElseIf Neg = NUL$ And cont = NUL$ And Brk <> NUL$ Then
      ReDim arr(6, 0)
   ElseIf Neg = NUL$ And cont <> NUL$ And Brk <> NUL$ Then
      ReDim arr(7, 0)
   ElseIf Neg <> NUL$ And cont <> NUL$ And Brk = NUL$ Then
      ReDim arr(7, 0)
   ElseIf Neg <> NUL$ And cont = NUL$ And Brk <> NUL$ Then
      ReDim arr(7, 0)
   ElseIf Neg <> NUL$ And cont <> NUL$ And Brk <> NUL$ Then
      ReDim arr(8, 0)
   Else
      ReDim arr(5, 0)
   End If
   Campos = "TO_CHAR(TO_DATE(ESTADO_TRANSITO.FE_INGR_ESTR), 'YYYY') AS ANNO, "
   Campos = Campos & "TO_CHAR(TO_DATE(ESTADO_TRANSITO.FE_INGR_ESTR), 'MM') AS MES, SUM(REEMBOLSO.NU_VLDED_REEM), "
   Campos = Campos & "SUM(REEMBOLSO.NU_VLPRE_REEM), SUM(REEMBOLSO.NU_VLXPAG_REEM), COUNT(REEMBOLSO.NU_NUME_TRAN_REEM) "
   If Neg <> NUL$ Then Campos = Campos & ", CONTRATO.NU_AUTO_NEGO_CONT "
   Desde = "TRANSITO, ESTADO_TRANSITO, REEMBOLSO "
   If Neg <> NUL$ Then Desde = Desde & ", CONTRATO "
   Condicion = "TRANSITO.NU_NUME_TRAN = ESTADO_TRANSITO.NU_NUME_TRAN_ESTR "
   Condicion = Condicion & "AND TRANSITO.NU_NUME_TRAN = REEMBOLSO.NU_NUME_TRAN_REEM "
   Condicion = Condicion & "AND ESTADO_TRANSITO.TX_ESTA_ESTR = 'AR' "
   Condicion = Condicion & "AND ESTADO_TRANSITO.TX_ACTU_ESTR = 'S' "
   If FD <> NUL$ And FH <> NUL$ Then
      Condicion = Condicion & " AND TO_DATE(ESTADO_TRANSITO.FE_INGR_ESTR,'DD/MM/YYYY HH24:MI:SS') >= " & FFechaCon(FD & " 00:00:00")
      Condicion = Condicion & " AND TO_DATE(ESTADO_TRANSITO.FE_INGR_ESTR,'DD/MM/YYYY HH24:MI:SS') <= " & FFechaCon(FH & " 23:59:59")
   End If
   If Neg <> NUL$ Then
      Condicion = Condicion & " AND TRANSITO.NU_AUTO_CONT_TRAN = CONTRATO.NU_AUTO_CONT "
      If Neg <> "Todos" Then Condicion = Condicion & " AND CONTRATO.NU_AUTO_NEGO_CONT IN (" & Neg & ") "
   End If
   Condicion = Condicion & "GROUP BY TO_CHAR(TO_DATE(ESTADO_TRANSITO.FE_INGR_ESTR), 'YYYY'), TO_CHAR(TO_DATE(ESTADO_TRANSITO.FE_INGR_ESTR), 'MM') "
   If Neg <> NUL$ Then Condicion = Condicion & ", CONTRATO.NU_AUTO_NEGO_CONT "
   Condicion = Condicion & "ORDER BY 1, 2"
   Result = LoadMulData(Desde, Campos, Condicion, arr)
   
   If Result <> FAIL And Encontro Then
      For i = 0 To UBound(arr, 2)
         With Me.Msf_Datos
            'Busca datos en grilla para ingresar la informaci�n de reembolsos
            For j = 1 To .Rows - 1
               If .TextMatrix(j, 0) = arr(0, i) And .TextMatrix(j, 1) = arr(1, i) And .TextMatrix(j, 19) = arr(6, i) Then
                  .TextMatrix(j, 6) = CDbl(arr(2, i)) 'Deducible
                  .TextMatrix(j, 7) = CDbl(arr(3, i)) 'Presentado
                  .TextMatrix(j, 8) = CDbl(arr(4, i)) 'Reembolsado
                  .TextMatrix(j, 9) = CDbl(arr(5, i)) 'Total Liquidaciones
                  If .TextMatrix(j, 5) = NUL$ Then
                     .TextMatrix(j, 10) = Format$(0, "##0.00%") '% Siniestralidad
                  Else
                     .TextMatrix(j, 10) = Format$(.TextMatrix(j, 8) / .TextMatrix(j, 5), "##0.00%") '% Siniestralidad
                  End If
               Else
                  .TextMatrix(j, 6) = IIf(.TextMatrix(j, 6) <> NUL$, .TextMatrix(j, 6), 0)  'Deducible
                  .TextMatrix(j, 7) = IIf(.TextMatrix(j, 7) <> NUL$, .TextMatrix(j, 7), 0)  'Presentado
                  .TextMatrix(j, 8) = IIf(.TextMatrix(j, 8) <> NUL$, .TextMatrix(j, 8), 0)  'Reembolsado
                  .TextMatrix(j, 9) = IIf(.TextMatrix(j, 9) <> NUL$, .TextMatrix(j, 9), 0)  'Total Liquidaciones
                  .TextMatrix(j, 10) = Format$(IIf(.TextMatrix(j, 10) <> NUL$, .TextMatrix(j, 10), 0), "##0.00%")  '% Siniestralidad
               End If
               If j = 1 Then
                  .TextMatrix(j, 11) = .TextMatrix(j, 5) 'Facturaci�n Acumulada
                  .TextMatrix(j, 12) = .TextMatrix(j, 8) 'Reembolsos Acumulados
               Else
                  .TextMatrix(j, 11) = CDbl(IIf(.TextMatrix(j - 1, 11) = NUL$, 0, .TextMatrix(j - 1, 11))) + CDbl(.TextMatrix(j, 5))
                  .TextMatrix(j, 12) = CDbl(IIf(.TextMatrix(j - 1, 12) = NUL$, 0, .TextMatrix(j - 1, 12))) + CDbl(.TextMatrix(j, 8))
               End If
               If .TextMatrix(j, 11) = NUL$ Then
                  .TextMatrix(j, 13) = Format$(0, "##0.00%") '% Siniestralidad Acumulada
               Else
                  .TextMatrix(j, 13) = Format$(.TextMatrix(j, 12) / .TextMatrix(j, 11), "##0.00%") '% Siniestralidad Acumulada
               End If
            Next j
            
         End With
      Next i
   End If
   'Cambia el color de las columnas de la grilla
'''   Call MsfChangeBackColor(Msf_Datos, 1, 1, &HC0FFFF, Me.Msf_Datos.Rows - 1, 10)
   Call MsfChangeBackColor(Msf_Datos, 1, 14, &HFFFFC0, Me.Msf_Datos.Rows - 1, 14)
'''   Call MsfChangeBackColor(Msf_Datos, 1, 15, &HC0FFFF, Me.Msf_Datos.Rows - 1)
End Sub

Private Sub Form_Unload(Cancel As Integer)
   Call MouseClock
End Sub

Private Sub Msf_Datos_KeyDown(KeyCode As Integer, Shift As Integer)
   Call Escribir_Otros_Fgrid(Msf_Datos, KeyCode, True)
End Sub

Private Sub Msf_Datos_KeyPress(KeyAscii As Integer)
   Dim Fila As Integer
   Dim Col  As Integer
   
   With Msf_Datos
      Fila = .Row
      Col = .Col
      If .Col = 14 Then
         Call Escribir_Fgrid(Msf_Datos, 1, KeyAscii, 15, 4, True, Me)
         If .TextMatrix(Fila, Col) = NUL$ Then Exit Sub
         .TextMatrix(Fila, 15) = CDbl(.TextMatrix(Fila, 8)) + CDbl(.TextMatrix(Fila, Col))
         If Fila = 1 Then
            .TextMatrix(Fila, 16) = .TextMatrix(Fila, 15)
         Else
            If .TextMatrix(Fila - 1, 16) = NUL$ Then
               .TextMatrix(Fila, 16) = CDbl(.TextMatrix(Fila, 15))
            Else
               .TextMatrix(Fila, 16) = CDbl(.TextMatrix(Fila - 1, 16)) + CDbl(.TextMatrix(Fila, 15))
            End If
         End If
         
         .TextMatrix(Fila, 17) = Format$(.TextMatrix(Fila, 15) / .TextMatrix(Fila, 5), "##0.00%")
         .TextMatrix(Fila, 18) = Format$(.TextMatrix(Fila, 16) / .TextMatrix(Fila, 11), "##0.00%")
         
         For j = 1 To .Rows - 1
            If j = 1 Then
               .TextMatrix(j, 16) = .TextMatrix(j, 15)
            Else
               If .TextMatrix(j, 15) = NUL$ Then Exit Sub
               .TextMatrix(j, 16) = CDbl(.TextMatrix(j - 1, 16)) + CDbl(.TextMatrix(j, 15))
            End If
            .TextMatrix(j, 17) = Format$(.TextMatrix(j, 15) / .TextMatrix(j, 5), "##0.00%")
            .TextMatrix(j, 18) = Format$(.TextMatrix(j, 16) / .TextMatrix(j, 11), "##0.00%")
         Next j
      End If
   End With

End Sub
