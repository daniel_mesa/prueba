VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "MSFLXGRD.OCX"
Object = "{0BA686C6-F7D3-101A-993E-0000C0EF6F5E}#1.0#0"; "THREED32.OCX"
Begin VB.Form FrmUsuario 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Creaci�n de Usuarios"
   ClientHeight    =   5475
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   7350
   Icon            =   "frmUsuario.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   5475
   ScaleWidth      =   7350
   Tag             =   "10000204"
   Begin VB.Frame Frame1 
      Caption         =   "Usuarios"
      Height          =   1935
      Index           =   1
      Left            =   120
      TabIndex        =   0
      Top             =   480
      Width           =   7095
      Begin VB.CheckBox ChkInactivo 
         Caption         =   "Inactivo"
         Height          =   255
         Left            =   120
         TabIndex        =   12
         Top             =   1440
         Visible         =   0   'False
         Width           =   1575
      End
      Begin VB.ComboBox CboPerfil 
         Height          =   315
         Left            =   5280
         TabIndex        =   4
         Top             =   360
         Width           =   1695
      End
      Begin VB.TextBox TxtClave 
         Height          =   285
         IMEMode         =   3  'DISABLE
         Left            =   720
         PasswordChar    =   "*"
         TabIndex        =   8
         Top             =   1080
         Width           =   2715
      End
      Begin VB.TextBox TxtConfirmacion 
         Height          =   285
         IMEMode         =   3  'DISABLE
         Left            =   4440
         PasswordChar    =   "*"
         TabIndex        =   10
         Top             =   1080
         Width           =   2475
      End
      Begin VB.TextBox TxtUsuario 
         Height          =   285
         Left            =   720
         MaxLength       =   30
         TabIndex        =   2
         Top             =   360
         Width           =   3495
      End
      Begin VB.TextBox TxtNombre 
         Height          =   285
         Left            =   720
         TabIndex        =   6
         Top             =   720
         Width           =   6255
      End
      Begin Threed.SSCommand CmdSelecUsuario 
         Height          =   345
         Left            =   4260
         TabIndex        =   15
         ToolTipText     =   "Selecci�n de Usuarios"
         Top             =   240
         Width           =   495
         _Version        =   65536
         _ExtentX        =   873
         _ExtentY        =   609
         _StockProps     =   78
         BevelWidth      =   0
         RoundedCorners  =   0   'False
         Outline         =   0   'False
         Picture         =   "frmUsuario.frx":058A
      End
      Begin VB.Label Label7 
         Caption         =   "Perfil"
         Height          =   255
         Left            =   4860
         TabIndex        =   3
         Top             =   390
         Width           =   735
      End
      Begin VB.Label Label6 
         Caption         =   "Confirmacion"
         Height          =   255
         Left            =   3480
         TabIndex        =   9
         Top             =   1080
         Width           =   975
      End
      Begin VB.Label Label5 
         Caption         =   "Clave"
         Height          =   255
         Left            =   120
         TabIndex        =   7
         Top             =   1080
         Width           =   975
      End
      Begin VB.Label Label1 
         Caption         =   "Usuario"
         Height          =   255
         Left            =   120
         TabIndex        =   1
         Top             =   360
         Width           =   735
      End
      Begin VB.Label Label2 
         Caption         =   "Nombre"
         Height          =   255
         Left            =   120
         TabIndex        =   5
         Top             =   720
         Width           =   735
      End
   End
   Begin VB.Frame Frame2 
      Caption         =   "Lista de Usuarios"
      Height          =   2655
      Left            =   120
      TabIndex        =   11
      Top             =   2760
      Width           =   7095
      Begin MSFlexGridLib.MSFlexGrid FGrdUsuarios 
         Height          =   2235
         Left            =   120
         TabIndex        =   14
         Top             =   240
         Width           =   6855
         _ExtentX        =   12091
         _ExtentY        =   3942
         _Version        =   393216
         Cols            =   7
         FixedCols       =   0
         BackColorBkg    =   16777215
         AllowUserResizing=   3
      End
   End
   Begin MSComctlLib.Toolbar Tlb_Opciones 
      Align           =   1  'Align Top
      Height          =   420
      Left            =   0
      TabIndex        =   13
      Top             =   0
      Width           =   7350
      _ExtentX        =   12965
      _ExtentY        =   741
      ButtonWidth     =   609
      ButtonHeight    =   582
      Appearance      =   1
      _Version        =   393216
      BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
         NumButtons      =   7
         BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Style           =   3
         EndProperty
         BeginProperty Button2 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "G"
            Object.ToolTipText     =   "Guardar"
         EndProperty
         BeginProperty Button3 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "B"
            Object.ToolTipText     =   "Borrar"
         EndProperty
         BeginProperty Button4 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "I"
            Object.ToolTipText     =   "Imprimir"
         EndProperty
         BeginProperty Button5 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Style           =   3
         EndProperty
         BeginProperty Button6 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Enabled         =   0   'False
            Key             =   "C"
            Object.ToolTipText     =   "Cambiar C�digo"
         EndProperty
         BeginProperty Button7 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "A"
            Object.ToolTipText     =   "Ayuda"
         EndProperty
      EndProperty
   End
End
Attribute VB_Name = "FrmUsuario"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Dim CodPerfil() As Variant
Dim Clave As String
Dim EncontroUsua As Boolean
Dim Mensaje, Tabla As String
Dim arrEventos() As C_Eventos
Public OpcCod     As String   'Opci�n de seguridad

Private Sub CboPerfil_KeyPress(KeyAscii As Integer)
Call Cambiar_Enter(KeyAscii)
End Sub

'JAGS R7123
Private Sub ChkInactivo_KeyPress(KeyAscii As Integer)
   Call Cambiar_Enter(KeyAscii)
End Sub
'JAGS R7123

Private Sub CmdSelecUsuario_Click()
'NYCM R1737--------------------------
 Codigo = NUL$
    'TxtUsuario = Seleccion("USUARIO", "TX_IDEN_USUA", "TX_IDEN_USUA,TX_DESC_USUA", "USUARIOS", NUL$)
    TxtUsuario = Seleccion("USUARIO", "TX_DESC_USUA", "TX_IDEN_USUA,TX_DESC_USUA", "USUARIOS", NUL$) 'NYCM M2227
     Call TxtUsuario_LostFocus
'NYCM R1737--------------------------
End Sub

Private Sub FGrdUsuarios_DblClick()
With FGrdUsuarios
   If .Row > 0 Then
      TxtUsuario = .TextMatrix(.Row, 0)
      Clave = .TextMatrix(.Row, 5)
      Call TxtUsuario_LostFocus
   End If
End With
End Sub

Private Sub Form_Activate()
   FrmMenu.CodOpc = OpcCod
End Sub

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
Select Case KeyCode
      Case vbKeyF1:
      Case vbKeyF2:
      Case vbKeyF3:
      Case vbKeyF11:
      Case vbKeyF5:
      Case vbKeyF6:
      Case vbKeyF12: Unload Me
   End Select
End Sub

Private Sub Form_Load()
Dim i As Integer
Dim ValorGrid As String

Call Estado_Botones_Forma(Tlb_Opciones, OpcCod)
Call CenterForm(MDI_Inventarios, Me)
Call LoadIconos(Tlb_Opciones)
Call EventosControles(Me, arrEventos)
Mensaje = " usuario"
Tabla = "USUARIO"
Call GrdFDef(FGrdUsuarios, 0, 0, "Usuario,1300,Nombre,2400,Doc,1,Numero,1,Perfil,1300,Grupo,1,Clave,1")
FGrdUsuarios.Rows = 1
'CAMPOS = "ID_IDEN_USUA,NO_NOMB_USUA,"
'CAMPOS = CAMPOS & "iif(NU_TIPD_USUA=0,'CC ',iif(NU_TIPD_USUA=1,'TI ',iif(NU_TIPD_USUA=2,'RC ',iif(NU_TIPD_USUA=3,'CE ',iif(NU_TIPD_USUA=4,'PA ',iif(NU_TIPD_USUA=5,'ASI','MSI')))))) ,"
'CAMPOS = CAMPOS & "NU_DOCU_USUA,NO_NOMB_PERF,"
'CAMPOS = CAMPOS & "iif(CD_CODI_GRUP_USUA=0,'MEDICO','OTRO'),PS_PASS_USUA"
'
'Desde = "USUARIOS INNER JOIN PERFILES ON USUARIOS.CD_CODI_PERF_USUA=PERFILES.CD_CODI_PERF"
'Desde = Desde & " ORDER BY ID_IDEN_USUA"
'Result = LoadfGrid(FGrdUsuarios, Desde, CAMPOS, NUL$)
   Desde = "USUARIO,PERFIL"
   Campos = "TX_IDEN_USUA,TX_DESC_USUA,"
   Campos = Campos & "'',"
   Campos = Campos & "'',TX_DESC_PERF,"
   Campos = Campos & "'',TX_PASS_USUA"
   Condicion = "USUARIO.NU_AUTO_PERF_USUA=PERFIL.NU_AUTO_PERF ORDER BY TX_IDEN_USUA"
   FGrdUsuarios.Rows = 2
   i = 1
   If (DoSelect(Desde, Campos, Condicion) <> FAIL) Then
          If (ResultQuery()) Then
              Call MouseClock
              Do While (NextRowQuery())
                  ValorGrid = DataQuery(1) & vbTab
                  ValorGrid = ValorGrid & DataQuery(2) & vbTab
                  ValorGrid = ValorGrid & IIf(DataQuery(3) = 0, "CC ", IIf(DataQuery(3) = 1, "TI ", IIf(DataQuery(3) = 2, "RC ", IIf(DataQuery(3) = 3, "CE ", IIf(DataQuery(3) = 4, "PA ", IIf(DataQuery(3) = 5, "ASI", "MSI")))))) & vbTab
                  ValorGrid = ValorGrid & DataQuery(4) & vbTab
                  ValorGrid = ValorGrid & DataQuery(5) & vbTab
                  ValorGrid = ValorGrid & IIf(DataQuery(6) = 0, "MEDICO", "OTRO") & vbTab
                  ValorGrid = ValorGrid & DataQuery(7)
                  FGrdUsuarios.AddItem ValorGrid, i
              Loop
          End If
   End If
Result = loadctrl("PERFIL ORDER BY TX_DESC_PERF ", "TX_DESC_PERF", "NU_AUTO_PERF", CboPerfil, CodPerfil(), NUL$)
 'JAGS R7123
   ChkInactivo.Visible = True
 'JAGS R7123
Call MouseNorm
Call FreeBufs(Result)
End Sub

Private Sub Form_Unload(Cancel As Integer)
Call DestruirControles(arrEventos())
End Sub

Private Sub Tlb_Opciones_ButtonClick(ByVal Button As MSComctlLib.Button)
Select Case Button.Key
   Case "G": Call Grabar
   Case "B": Call Borrar
   Case "I": 'DAHV M0003306
                
         ReDim Arr(1)
         Result = LoadData("ENTIDAD", "CD_NIT_ENTI, NO_NOMB_ENTI", NUL$, Arr)
         If Result <> FAIL Then
            Entidad = Arr(1)
            Nit = Arr(0)
         End If
         Call Listar1("InfUsuarios.rpt", NUL$, NUL$, 0, "LISTADO DE USUARIOS", MDI_Inventarios)

End Select
End Sub

'DEPURACION DE CODIGO
'Private Sub Txt_VlMaxAut_KeyPress(KeyAscii As Integer)
'Call ValKeyNum(KeyAscii)
'End Sub

Private Sub TxtClave_KeyPress(KeyAscii As Integer)
Call Cambiar_Enter(KeyAscii)
End Sub

Private Sub TxtClave_LostFocus()
If TxtClave <> NUL$ Then
'   If EncontroUsua Then
'      If vbYes = MsgBox("Desea cambiar la clave de este usuario", vbYesNo + vbQuestion + vbDefaultButton2) Then
'         Codigo = "n"
'         'While Codigo <> Clave And Codigo <> NUL$
'         While ConvertCadenaHexa(Encriptar(UCase(Trim(Codigo)), UCase(Trim(TxtUsuario)))) <> Clave And Codigo <> NUL$
'            FrmAcceso.Caption = "Clave anterior"
'            FrmAcceso.Show 1
'            'If Codigo <> Clave And Codigo <> NUL$ Then Call Mensaje1("Clave errada", 1)
'            If ConvertCadenaHexa(Encriptar(UCase(Trim(Codigo)), UCase(Trim(TxtUsuario)))) <> Clave And Codigo <> NUL$ Then Call Mensaje1("Clave errada", 1)
'         Wend
'         'If Codigo = Clave Then Codigo = "SI"
'         If ConvertCadenaHexa(Encriptar(UCase(Trim(Codigo)), UCase(Trim(TxtUsuario)))) = Clave Then Codigo = "SI"
'         If Codigo = NUL$ Then TxtClave = NUL$
'      Else
'         TxtClave = NUL$
'      End If
'   End If
End If
End Sub

Private Sub TxtConfirmacion_KeyPress(KeyAscii As Integer)
Call Cambiar_Enter(KeyAscii)
End Sub

Private Sub TxtNombre_KeyPress(KeyAscii As Integer)
Call Cambiar_Enter(KeyAscii)
End Sub

Private Sub TxtUsuario_KeyPress(KeyAscii As Integer)
Call Cambiar_Enter(KeyAscii)
End Sub

Private Sub TxtUsuario_LostFocus()
ReDim Arr(6)

If TxtUsuario <> NUL$ Then
   Campos = "TX_PASS_USUA,TX_DESC_USUA,'','',NU_AUTO_PERF_USUA,'',NU_ESTA_USUA"
   Condicion = "TX_IDEN_USUA='" & TxtUsuario & Comi
   Result = LoadData("USUARIO", Campos, Condicion, Arr)
   If Encontro Then
      EncontroUsua = True
      Clave = Arr(0)
      txtNombre = Arr(1)
      CboPerfil.ListIndex = FindInArr(CodPerfil, Arr(4))
      TxtClave = NUL$
      ChkInactivo.Value = Arr(6) 'JAGS T8193
      TxtConfirmacion = NUL$
   Else
      EncontroUsua = False
      Call LimpiarUsua
   End If
End If
End Sub

Sub LimpiarUsua()
Dim i As Integer
Dim ValorGrid As String
txtNombre = NUL$
CboPerfil.ListIndex = -1
TxtClave = NUL$
TxtConfirmacion = NUL$
ChkInactivo.Value = 0 'JAGS T8193
   Desde = "USUARIO,PERFIL"
   Campos = "TX_IDEN_USUA,TX_DESC_USUA,"
   Campos = Campos & "'',"
   Campos = Campos & "'',TX_DESC_PERF,"
   Campos = Campos & "'',TX_PASS_USUA"
   Condicion = "USUARIO.NU_AUTO_PERF_USUA=PERFIL.NU_AUTO_PERF ORDER BY TX_IDEN_USUA"

   FGrdUsuarios.Rows = 2
   i = 1
   If (DoSelect(Desde, Campos, Condicion) <> FAIL) Then
          If (ResultQuery()) Then
              Call MouseClock
              Do While (NextRowQuery())
                  ValorGrid = DataQuery(1) & vbTab
                  ValorGrid = ValorGrid & DataQuery(2) & vbTab
                  ValorGrid = ValorGrid & IIf(DataQuery(3) = 0, "CC ", IIf(DataQuery(3) = 1, "TI ", IIf(DataQuery(3) = 2, "RC ", IIf(DataQuery(3) = 3, "CE ", IIf(DataQuery(3) = 4, "PA ", IIf(DataQuery(3) = 5, "ASI", "MSI")))))) & vbTab
                  ValorGrid = ValorGrid & DataQuery(4) & vbTab
                  ValorGrid = ValorGrid & DataQuery(5) & vbTab
                  ValorGrid = ValorGrid & IIf(DataQuery(6) = 0, "MEDICO", "OTRO") & vbTab
                  ValorGrid = ValorGrid & DataQuery(7)
                  FGrdUsuarios.AddItem ValorGrid, i
              Loop
          End If
   End If
   Call MouseNorm
   Call FreeBufs(Result)
End Sub


Private Sub Grabar()
'   Dim Arr(0) As Variant   'DEPURACION DE CODIGO
   
   DoEvents
   
   Call MouseClock
   Msglin "Ingresando informaci�n del " & Mensaje
      
   If TxtUsuario = NUL$ Then
      Call Mensaje1("Se debe especificar el usuario o cuenta", 3)
      Call MouseNorm: Exit Sub
   End If
   
   If txtNombre = NUL$ Then
      Call Mensaje1("Se debe especificar el nombre del usuario", 3)
      Call MouseNorm: Exit Sub
   End If
   
   If CboPerfil.ListIndex = -1 Then
      Call Mensaje1("Se debe especificar el perfil del usuario", 3)
      Call MouseNorm: Exit Sub
   End If
   If Not EncontroUsua And TxtClave <> TxtConfirmacion Then
      Call Mensaje1("La confirmaci�n de la clave esta errada. Vuelva a intentarlo", 3)
      Call MouseNorm: Exit Sub
   End If
   
   If Not EncontroUsua And TxtClave = NUL$ Then
      Call Mensaje1("Debe asignar una clave a este usuario. Vuelva a intentarlo", 3)
      Call MouseNorm: Exit Sub
   End If
   If Not WarnMsg("Desea guardar la informaci�n?") Then Call MouseNorm: Exit Sub 'CARV M5085
   If (BeginTran(STranIUp & Tabla) <> FAIL) Then

       If (Not EncontroUsua) Then
         Valores = "TX_IDEN_USUA='" & TxtUsuario & Comi & Coma
         'Valores = Valores & "TX_PASS_USUA='" & TxtClave & Comi & Coma
         Valores = Valores & "TX_PASS_USUA=" & Comi & ConvertCadenaHexa(Encriptar(UCase(Trim(TxtClave)), UCase(Trim(TxtUsuario)))) & Comi & Coma
         Valores = Valores & "TX_DESC_USUA='" & txtNombre & Comi & Coma
         Valores = Valores & "NU_AUTO_PERF_USUA=" & CodPerfil(CboPerfil.ListIndex)
         'JAGS R7123
         If ChkInactivo.Visible Then
            Valores = Valores & Coma & "NU_ESTA_USUA=" & Comi & ChkInactivo.Value & Comi
         End If
         'JAGS R7123
         Result = DoInsertSQL(Tabla, Valores)
       Else
         If WarnMsg("Desea Modificar la Informaci�n del " & Mensaje) Then
            Valores = "TX_IDEN_USUA='" & TxtUsuario & Comi & Coma
            Valores = Valores & "TX_DESC_USUA='" & txtNombre & Comi
            If UCase(TxtUsuario) <> "ADMINISTRADOR" Then
               Valores = Valores & Coma & "NU_AUTO_PERF_USUA='" & CodPerfil(CboPerfil.ListIndex) & Comi
               'JAGS R7123
         If ChkInactivo.Visible Then
            Valores = Valores & Coma & "NU_ESTA_USUA=" & Comi & ChkInactivo.Value & Comi
         End If
         'JAGS R7123
            End If
            
            Result = SUCCEED
            If TxtClave <> Clave And TxtClave <> NUL$ Then
               If TxtClave <> TxtConfirmacion Then
                  Call Mensaje1("La confrmaci�n de la clave esta errada. Vuelva a intentarlo", 3)
                  Call MouseNorm
                  Result = FAIL
               End If
               
               'Valores = Valores & Coma & "TX_PASS_USUA='" & TxtClave & Comi
                Valores = Valores & Coma & "TX_PASS_USUA='" & ConvertCadenaHexa(Encriptar(UCase(Trim(TxtClave)), UCase(Trim(TxtUsuario)))) & Comi
         
            End If
            If Result <> FAIL Then
               Condicion = "TX_IDEN_USUA='" & TxtUsuario & Comi
               Result = DoUpdate(Tabla, Valores, Condicion)
            End If
         End If
       End If
   End If
   If (Result <> FAIL) Then
      If (CommitTran() <> FAIL) Then
         TxtUsuario = NUL$
         
         Call LimpiarUsua
         On Error Resume Next
         TxtUsuario.SetFocus
      Else
         Call RollBackTran
      End If
   Else
      Call RollBackTran
   End If
   Call MouseNorm
   Msglin NUL$

End Sub

Private Sub Borrar()
'   Dim Arr(0) As Variant   'DEPURACION DE CODIGO
   
   DoEvents
   
   Call MouseClock
   Msglin "Ingresando informaci�n del " & Mensaje
      
   If TxtUsuario = NUL$ Then
      Call Mensaje1("Se debe especificar el usuario o cuenta", 3)
      Call MouseNorm: Exit Sub
   End If
   'AASV M4572 Inicio
   If UCase(TxtUsuario) = "ADMINISTRADOR" Then
      Call Mensaje1("No se puede eliminar el usuario Administrador", 3)
      Call MouseNorm: Exit Sub
   End If
   'AASV M4572 Fin
   'JACC M5802
   If UCase(Tabla) = "USUARIO" Then
      ReDim ArrFork(0)
      Condicion = "NU_AUTO_USUA_CONE = NU_AUTO_USUA AND TX_IDEN_USUA= '" & TxtUsuario & Comi
      Result = LoadData("USUARIO,CONEXION", "TOP 1 NU_AUTO_USUA", Condicion, ArrFork)
      If Result <> FAIL And Encontro Then
          Call Mensaje1("No se puede eliminar porque tiene movimientos relacionados", 3)
         Call MouseNorm: Exit Sub
      End If
   End If
   'JACC M5802
   If (BeginTran(STranDel & Tabla) <> FAIL) Then
      If WarnDel() Then
         Condicion = "TX_IDEN_USUA='" & TxtUsuario & Comi
         If UCase(TxtUsuario) <> "ADMINISTRADOR" Then
            Result = DoDelete(Tabla, Condicion)
         End If
      End If
   End If
   If (Result <> FAIL) Then
      If (CommitTran() <> FAIL) Then
          TxtUsuario = NUL$
          Call LimpiarUsua
          On Error Resume Next
          TxtUsuario.SetFocus
      Else
         Call RollBackTran
      End If
   Else
      Call RollBackTran
   End If
   Call MouseNorm
   Msglin NUL$

End Sub


