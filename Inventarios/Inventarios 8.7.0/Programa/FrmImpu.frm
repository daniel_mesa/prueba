VERSION 5.00
Object = "{0BA686C6-F7D3-101A-993E-0000C0EF6F5E}#1.0#0"; "THREED32.OCX"
Begin VB.Form FrmImpuestos 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Impuestos y Otros Cargos"
   ClientHeight    =   4650
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   5895
   Icon            =   "FrmImpu.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   ScaleHeight     =   4650
   ScaleWidth      =   5895
   Begin VB.Frame Frame2 
      Height          =   1095
      Left            =   480
      TabIndex        =   13
      Top             =   3480
      Width           =   4935
      Begin Threed.SSCommand SCmd_Options 
         Height          =   735
         Index           =   0
         Left            =   120
         TabIndex        =   14
         Top             =   240
         Width           =   855
         _Version        =   65536
         _ExtentX        =   1508
         _ExtentY        =   1296
         _StockProps     =   78
         Caption         =   "&GUARDAR"
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   6
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Picture         =   "FrmImpu.frx":058A
      End
      Begin Threed.SSCommand SCmd_Options 
         Height          =   735
         Index           =   1
         Left            =   1080
         TabIndex        =   15
         Top             =   240
         Visible         =   0   'False
         Width           =   855
         _Version        =   65536
         _ExtentX        =   1508
         _ExtentY        =   1296
         _StockProps     =   78
         Caption         =   "&BORRAR"
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   6
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Picture         =   "FrmImpu.frx":0C54
      End
      Begin Threed.SSCommand SCmd_Options 
         Height          =   735
         Index           =   3
         Left            =   3000
         TabIndex        =   17
         Top             =   240
         Width           =   855
         _Version        =   65536
         _ExtentX        =   1508
         _ExtentY        =   1296
         _StockProps     =   78
         Caption         =   "&LISTAR"
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   6
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Picture         =   "FrmImpu.frx":131E
      End
      Begin Threed.SSCommand SCmd_Options 
         Height          =   735
         Index           =   4
         Left            =   3960
         TabIndex        =   18
         Top             =   240
         Width           =   855
         _Version        =   65536
         _ExtentX        =   1508
         _ExtentY        =   1296
         _StockProps     =   78
         Caption         =   "&SALIR"
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   6
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Picture         =   "FrmImpu.frx":19E8
      End
      Begin Threed.SSCommand SCmd_Options 
         Height          =   735
         Index           =   2
         Left            =   2040
         TabIndex        =   16
         Top             =   240
         Width           =   855
         _Version        =   65536
         _ExtentX        =   1508
         _ExtentY        =   1296
         _StockProps     =   78
         Caption         =   "S&ELECCION"
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   6
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Picture         =   "FrmImpu.frx":20B2
      End
   End
   Begin VB.Frame Frame1 
      Height          =   3375
      Left            =   120
      TabIndex        =   0
      Top             =   0
      Width           =   5655
      Begin VB.TextBox TxtImpu 
         Height          =   285
         Index           =   0
         Left            =   1440
         MaxLength       =   3
         TabIndex        =   1
         Top             =   240
         Width           =   495
      End
      Begin VB.TextBox TxtImpu 
         Height          =   285
         Index           =   1
         Left            =   1440
         MaxLength       =   30
         TabIndex        =   2
         Top             =   840
         Width           =   4095
      End
      Begin VB.TextBox TxtImpu 
         Height          =   285
         Index           =   2
         Left            =   1440
         MaxLength       =   7
         TabIndex        =   3
         Top             =   1440
         Width           =   735
      End
      Begin VB.TextBox TxtImpu 
         Height          =   285
         Index           =   3
         Left            =   1440
         MaxLength       =   18
         TabIndex        =   4
         Top             =   2040
         Width           =   1815
      End
      Begin VB.Frame FraSuma 
         Caption         =   "Tipo"
         Height          =   2055
         Left            =   3600
         TabIndex        =   5
         Top             =   1200
         Width           =   1935
         Begin VB.OptionButton OptOim 
            Caption         =   "Otros Impuestos"
            Height          =   195
            Left            =   120
            TabIndex        =   10
            Top             =   1680
            Width           =   1695
         End
         Begin VB.OptionButton OptIca 
            Caption         =   "Ica"
            Enabled         =   0   'False
            Height          =   195
            Left            =   120
            TabIndex        =   7
            Top             =   600
            Visible         =   0   'False
            Width           =   1455
         End
         Begin VB.OptionButton OptIva 
            Caption         =   "Iva"
            Height          =   195
            Left            =   120
            TabIndex        =   6
            Top             =   240
            Value           =   -1  'True
            Width           =   975
         End
         Begin VB.OptionButton OptRete 
            Caption         =   "Retenci�n"
            Enabled         =   0   'False
            Height          =   195
            Left            =   120
            TabIndex        =   8
            Top             =   960
            Visible         =   0   'False
            Width           =   1095
         End
         Begin VB.OptionButton OptImRe 
            Caption         =   "Impuesto Retenido"
            Enabled         =   0   'False
            Height          =   195
            Left            =   120
            TabIndex        =   9
            Top             =   1320
            Visible         =   0   'False
            Width           =   1695
         End
      End
      Begin VB.TextBox TxtImpu 
         Height          =   285
         Index           =   4
         Left            =   1440
         MaxLength       =   15
         TabIndex        =   11
         Top             =   2640
         Width           =   1575
      End
      Begin Threed.SSCommand CmdSelec 
         Height          =   375
         Left            =   3000
         TabIndex        =   12
         ToolTipText     =   "Selecci�n de Cuentas Contables"
         Top             =   2640
         Width           =   615
         _Version        =   65536
         _ExtentX        =   1085
         _ExtentY        =   661
         _StockProps     =   78
         BevelWidth      =   0
         RoundedCorners  =   0   'False
         Outline         =   0   'False
         Picture         =   "FrmImpu.frx":2A64
      End
      Begin Threed.SSCommand CmdLimpia 
         Height          =   375
         Left            =   5160
         TabIndex        =   19
         ToolTipText     =   "Limpia Pantalla"
         Top             =   240
         Width           =   375
         _Version        =   65536
         _ExtentX        =   661
         _ExtentY        =   661
         _StockProps     =   78
         Picture         =   "FrmImpu.frx":3416
      End
      Begin VB.Label LblCodigo 
         Caption         =   "&C�digo :"
         Height          =   255
         Left            =   120
         TabIndex        =   24
         Top             =   240
         Width           =   735
      End
      Begin VB.Label LblDescripcion 
         Caption         =   "&Descripci�n :"
         Height          =   255
         Left            =   120
         TabIndex        =   23
         Top             =   840
         Width           =   975
      End
      Begin VB.Label LblPorcentaje 
         Caption         =   "&Porcentaje (%) :"
         Height          =   255
         Left            =   120
         TabIndex        =   22
         Top             =   1440
         Width           =   1215
      End
      Begin VB.Label LblTope 
         Caption         =   "&A partir de :"
         Height          =   255
         Left            =   120
         TabIndex        =   21
         Top             =   2040
         Width           =   1095
      End
      Begin VB.Label LblCuenta 
         Caption         =   "Cuenta C&ontable :"
         Height          =   255
         Left            =   120
         TabIndex        =   20
         Top             =   2640
         Width           =   1335
      End
   End
End
Attribute VB_Name = "FrmImpuestos"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Public Tabla As String
Public Mensaje As String
Public EncontroT As Boolean
Public OpcCod        As String   'Opci�n de seguridad

Private Sub CmdLimpia_Click()
    Call Limpiar
End Sub

Private Sub CmdSelec_Click()
   Dim CodigoI As String
   Codigo = NUL$
   'JLPB T29905/R28918 INICIO Se deja en comentario la siguiente linea
   'CodigoI = Seleccion("CUENTAS", "NO_NOMB_CUEN", "CD_CODI_CUEN,NO_NOMB_CUEN", "CUENTAS CONTABLES", NUL$)
   Condicion = "TX_PUCNIIF_CUEN<>'1'"
   CodigoI = Seleccion("CUENTAS", "NO_NOMB_CUEN", "CD_CODI_CUEN,NO_NOMB_CUEN", "CUENTAS CONTABLES", Condicion)
   'JLPB T29905/R28918 FIN
   If CodigoI <> NUL$ Then TxtImpu(4) = CodigoI
   TxtImpu(4).SetFocus
End Sub
Private Sub Form_Load()
    Call CenterForm(MDI_Inventarios, Me)
    Tabla = "TC_IMPUESTOS"
    Mensaje = "Impuesto y/o Retenci�n"
'    Call Leer_Permisos("01001", SCmd_Options(0), SCmd_Options(1), SCmd_Options(3))
End Sub

'---------------------------------------------------------------------------------------
' Procedure : OptIca_Click
' DateTime  : 26/08/2015 12:08
' Author    : juan_urrego
' Purpose   : T29614
'---------------------------------------------------------------------------------------
'
Private Sub OptIca_Click()
   If OptIca.Value = True Then
      TxtImpu(2).Enabled = True
      TxtImpu(3).Enabled = True
   End If
End Sub

'---------------------------------------------------------------------------------------
' Procedure : OptImRe_Click
' DateTime  : 26/08/2015 12:09
' Author    : juan_urrego
' Purpose   : T29614
'---------------------------------------------------------------------------------------
'
Private Sub OptImRe_Click()
   If OptImRe.Value = True Then
      TxtImpu(2).Enabled = True
      TxtImpu(3).Enabled = True
   End If
End Sub

'---------------------------------------------------------------------------------------
' Procedure : OptIva_Click
' DateTime  : 26/08/2015 12:07
' Author    : juan_urrego
' Purpose   : T29614
'---------------------------------------------------------------------------------------
'
Private Sub OptIva_Click()
   If OptIva.Value = True Then
      TxtImpu(2).Enabled = True
      TxtImpu(3).Enabled = True
   End If
End Sub

Private Sub OptIva_KeyPress(KeyAscii As Integer)
  Call Cambiar_Enter(KeyAscii)
End Sub
Private Sub OptIca_KeyPress(KeyAscii As Integer)
  Call Cambiar_Enter(KeyAscii)
End Sub

'---------------------------------------------------------------------------------------
' Procedure : OptOim_Click
' DateTime  : 26/08/2015 12:10
' Author    : juan_urrego
' Purpose   : T29614
'---------------------------------------------------------------------------------------
'
Private Sub OptOim_Click()
   If OptOim.Value = True Then
      TxtImpu(2).Enabled = True
      TxtImpu(3).Enabled = True
   End If
End Sub


Private Sub OptRete_KeyPress(KeyAscii As Integer)
  Call Cambiar_Enter(KeyAscii)
End Sub
Private Sub OptImRe_KeyPress(KeyAscii As Integer)
  Call Cambiar_Enter(KeyAscii)
End Sub
Private Sub OptOim_KeyPress(KeyAscii As Integer)
  Call Cambiar_Enter(KeyAscii)
End Sub
Private Sub SCmd_Options_Click(Index As Integer)
Dim impu As String
    Select Case Index
        Case 0: Encontro = EncontroT
                Grabar
        Case 1: Borrar
        Case 2: impu = Seleccion(Tabla, "DE_NOMB_IMPU", "CD_CODI_IMPU,DE_NOMB_IMPU", "IMPUESTOS Y RETENCIONES", NUL$)
                If impu <> NUL$ Then TxtImpu(0) = impu
                TxtImpu(0).SetFocus
                SCmd_Options(2).SetFocus
        Case 3: Call Listar("IMPUESTO.rpt", Tabla, "CD_CODI_IMPU", "DE_NOMB_IMPU", "Impuestos y Retenciones", NUL$, NUL$, NUL$, NUL$, NUL$)
                Me.SetFocus
        Case 4: Unload Me
    End Select
End Sub
Private Sub SCmd_Options_GotFocus(Index As Integer)
 Select Case Index
    Case 0: Msglin "Pulse este icono para grabar"
    Case 1: Msglin "Pulse este icono para borrar"
    Case 2: Msglin "Pulse este icono para selecci�n"
    Case 3: Msglin "Pulse este icono para generar un listado"
    Case 4: Msglin "Pulse este icono para cerrar la ventana "
  End Select
End Sub
Private Sub TxtImpu_GotFocus(Index As Integer)
   Seltext TxtImpu(Index)
   Select Case Index
      Case 0: Msglin "Digite el C�digo del " & Mensaje
      Case 1: Msglin "Digite la Descripci�n del " & Mensaje
         If TxtImpu(0) = NUL$ Then TxtImpu(0).SetFocus: Exit Sub
      Case 2: Msglin "Digite el Porcentaje del " & Mensaje
      Case 3: Msglin "Digite el Valor Base del " & Mensaje
      Case 4: Msglin "Digite la Cuenta Contable del " & Mensaje
   End Select
End Sub
Private Sub TxtImpu_KeyDown(Index As Integer, KeyCode As Integer, Shift As Integer)
'Dim Terc As String     'DEPURACION DE CODIGO
    Select Case Index
        Case 0: If KeyCode = 40 Then TxtImpu(Index + 1).SetFocus: Exit Sub
        Case 4: If KeyCode = 38 Then TxtImpu(Index - 1).SetFocus: Exit Sub
        Case Else:
                    If KeyCode = 38 Then
                       TxtImpu(Index - 1).SetFocus
                    Else
                     If KeyCode = 40 Then
                       TxtImpu(Index + 1).SetFocus
                     End If
                    End If
    End Select
End Sub
Private Sub TxtImpu_KeyPress(Index As Integer, KeyAscii As Integer)
 If Index = 2 Then
  Call ValKeyReal(TxtImpu(2), KeyAscii)
 Else
  If Index = 3 Then
    Call ValKeyNum(KeyAscii)
  Else
    Call ValKeyAlfaNum(KeyAscii)
  End If
 End If
 If Index = 4 Then
  If KeyAscii = 13 Then
   SCmd_Options(0).SetFocus
  End If
 Else
   Call Cambiar_Enter(KeyAscii)
 End If
End Sub
Private Sub TxtImpu_LostFocus(Index As Integer)
   If Val(TxtImpu(Index).Text) = 0 And Index = 2 Then TxtImpu(Index).Text = Format(0, "0.0#") 'LJSA M4199
   If Not IsNumeric(TxtImpu(Index).Text) And Index = 3 Then TxtImpu(Index).Text = Format(0, "0.0#")  'LJSA M4199
   Msglin NUL$
   Select Case Index
      Case 0: If TxtImpu(0) <> NUL$ Then Buscar_Impuesto
      Case 2:
         If TxtImpu(Index) = NUL$ Then
            TxtImpu(Index) = "0"
         Else
            If TxtImpu(Index) <= 99.9999 Then
               TxtImpu(Index) = Format(TxtImpu(Index), "#0.####")
            Else
               Call Mensaje1("Revise el porcentaje del " & Mensaje, 3)
               TxtImpu(Index) = "0"
               TxtImpu(Index).SetFocus: Exit Sub
            End If
         End If
      Case 3:
         If TxtImpu(Index) = NUL$ Then
            TxtImpu(Index) = "0"
         Else
            TxtImpu(Index) = Format(TxtImpu(Index), "#,###,###,###,##0.#0")
         End If
      Case 4: If TxtImpu(4) <> NUL$ Then Buscar_Cuenta (4)
   End Select
End Sub
Private Sub Buscar_Impuesto()
'JAUM T29614 Inicio se deja bloque en comentario
'ReDim arr(7)
'Dim i As Byte
   'Condicion = "CD_CODI_IMPU=" & Comi & Cambiar_Comas_Comillas(TxtImpu(0)) & Comi
               
   'Result = LoadData(Tabla, Asterisco, Condicion, arr())
   'If (Result <> False) Then
     'If Encontro Then
        'TxtImpu(1) = arr(1)
        'TxtImpu(2) = Format(arr(2), "#0.####")
        'TxtImpu(3) = Format(arr(3), "#,###,###,###,##0.#0")
        'TxtImpu(4) = arr(5)
        'If arr(6) = "I" Then
         'OptIva.Value = True
        'End If
        'If arr(6) = "C" Then
         'OptIca.Value = True
        'End If
        'If arr(6) = "R" Then
         'OptRete.Value = True
        'End If
        'If arr(6) = "M" Then
         'OptImRe.Value = True
        'End If
        'If arr(6) = "O" Then
         'OptOim.Value = True
        'End If
        'EncontroT = True
     'Else
        'For i = 1 To 4
            'If i = 2 Or i = 3 Then
              'TxtImpu(i) = "0"
            'Else
              'TxtImpu(i) = NUL$
            'End If
        'Next i
   ReDim arr(5)
   Dim ByI As Byte
   Condicion = "CD_CODI_IMPU=" & Comi & Cambiar_Comas_Comillas(TxtImpu(0)) & Comi
   Campos = "DE_NOMB_IMPU, PR_PORC_IMPU, VL_TOPE_IMPU, CD_CUEN_IMPU, ID_TIPO_IMPU, TX_RETEFU_IMPU"
   Result = LoadData(Tabla, Campos, Condicion, arr())
   If Result <> FAIL And Encontro Then
      TxtImpu(1) = arr(0)
      TxtImpu(2) = Format(arr(1), "#0.####")
      TxtImpu(3) = Format(arr(2), "#,###,###,###,##0.#0")
      If arr(5) = True Then
         TxtImpu(2).Enabled = False
         TxtImpu(3).Enabled = False
      Else
         TxtImpu(2).Enabled = True
         TxtImpu(3).Enabled = True
      End If
      TxtImpu(4) = arr(3)
      If arr(4) = "I" Then
         OptIva.Value = True
      End If
      If arr(4) = "C" Then
         OptIca.Value = True
      End If
      If arr(4) = "R" Then
         OptRete.Value = True
      End If
      If arr(4) = "M" Then
         OptImRe.Value = True
      End If
      If arr(4) = "O" Then
         OptOim.Value = True
      End If
      EncontroT = True
   Else
      For ByI = 1 To 4
         If ByI = 2 Or ByI = 3 Then
            TxtImpu(ByI) = "0"
         Else
            TxtImpu(ByI) = NUL$
         End If
      Next
'JAUM T29614 Fin
      OptIva.Value = True
      EncontroT = False
   End If
End Sub
Private Sub Buscar_Cuenta(ByVal indice As Integer)
   ReDim arr(0)
   Condicion = "CD_CODI_CUEN=" & Comi & Cambiar_Comas_Comillas(TxtImpu(indice)) & Comi
   Condicion = Condicion & " AND TX_PUCNIIF_CUEN<>'1'" 'JLPB T29905/R28918
   Result = LoadData("CUENTAS", "CD_CODI_CUEN", Condicion, arr())
   If (Result <> False) Then
      If Encontro Then
         TxtImpu(indice) = arr(0)
         If Valida_Nivel_Cuentas(TxtImpu(indice)) = True Then
            Call Mensaje1("La cuenta no es de �ltimo nivel", 3)
            TxtImpu(indice) = NUL$
            TxtImpu(indice).SetFocus
         End If
      Else
         TxtImpu(indice) = NUL$
         'TxtImpu(Indice).SetFocus   'LJSA M4199 Depuraci�n de c�digo
      End If
   End If
End Sub
Private Sub Grabar()
   Dim VrRetefuente() As Variant 'JAUM T30157 Almacena el tipo de impuesto y si tiene marcada la opci�n de retencion en la fuente
   ReDim VrRetefuente(1) 'JAUM T30157
   DoEvents
   
   Call MouseClock
   Msglin "Ingresando informaci�n del " & Mensaje

   If (TxtImpu(0) = NUL$ Or TxtImpu(1) = NUL$ Or TxtImpu(2) = NUL$) Then
      Call Mensaje1("Se deben especificar los datos completos del " & Mensaje, 3)
      Call MouseNorm: Exit Sub
   End If
   If OptIva.Value = True Or OptIca.Value = True Or OptImRe.Value = True Or OptOim.Value = True Then 'JAUM T29614
      If CDbl(TxtImpu(2)) = 0 Then
         Call Mensaje1("Se debe especificar el porcentaje del " & Mensaje, 3)
         Call MouseNorm: Exit Sub
      End If
   'JAUM T29614 Inicio
   Else
      If fnDevDato("TC_IMPUESTOS", "TX_RETEFU_IMPU", "CD_CODI_IMPU ='" & TxtImpu(0) & Comi, True) = False Then
         If CDbl(TxtImpu(2)) = 0 Then
            Call Mensaje1("Se debe especificar el porcentaje del " & Mensaje, 3)
            Call MouseNorm: Exit Sub
         End If
      End If
   End If 'JAUM T29614 Fin
   'JAUM T30157 Inicio
   Result = LoadData("TC_IMPUESTOS", "TX_RETEFU_IMPU, ID_TIPO_IMPU", "CD_CODI_IMPU = '" & TxtImpu(0) & Comi, VrRetefuente)
   If VrRetefuente(0) = True And VrRetefuente(1) = "R" Then
      Call Mensaje1("El impuesto seleccionado es de tipo retenci�n en la fuente, solo se puede modificar desde Cuentas X Pagar", 1)
      Call MouseNorm
      Exit Sub
   End If
   'JAUM T30157 Fin
   If (BeginTran(STranIUp & Tabla) <> FAIL) Then
      If (Not Encontro) Then
         Valores = "CD_CODI_IMPU = " & Comi & Cambiar_Comas_Comillas(TxtImpu(0)) & Comi
         Valores = Valores & Coma & "DE_NOMB_IMPU = " & Comi & Cambiar_Comas_Comillas(TxtImpu(1)) & Comi
         Valores = Valores & Coma & "PR_PORC_IMPU = " & CDbl(TxtImpu(2))
         Valores = Valores & Coma & "VL_TOPE_IMPU = " & CDbl(TxtImpu(3))
         Valores = Valores & Coma & "ID_SUMA_IMPU = " & Comi & NUL$ & Comi
         Valores = Valores & Coma & "CD_CUEN_IMPU = " & Comi & Cambiar_Comas_Comillas(TxtImpu(4)) & Comi
         If OptIva.Value = True Then
            Valores = Valores & Coma & "ID_TIPO_IMPU = " & Comi & "I" & Comi
         End If
         If OptIca.Value = True Then
            Valores = Valores & Coma & "ID_TIPO_IMPU = " & Comi & "C" & Comi
         End If
         If OptRete.Value = True Then
            Valores = Valores & Coma & "ID_TIPO_IMPU = " & Comi & "R" & Comi
         End If
         If OptImRe.Value = True Then
            Valores = Valores & Coma & "ID_TIPO_IMPU = " & Comi & "M" & Comi
         End If
         If OptOim.Value = True Then
            Valores = Valores & Coma & "ID_TIPO_IMPU = " & Comi & "O" & Comi
         End If
         Valores = Valores & Coma & "CD_SUBC_IMPU = " & Comi & NUL$ & Comi
         Result = DoInsertSQL(Tabla, Valores)
         If (Result <> FAIL) Then Result = Auditor(Tabla, TranIns, LastCmd)
      Else
         If WarnMsg("Desea Modificar la Informaci�n del " & Mensaje, 256) Then
            Valores = "DE_NOMB_IMPU=" & Comi & Cambiar_Comas_Comillas(TxtImpu(1)) & Comi & Coma
            Valores = Valores & " PR_PORC_IMPU=" & CDbl(TxtImpu(2)) & Coma
            Valores = Valores & " VL_TOPE_IMPU=" & CDbl(TxtImpu(3)) & Coma
            Valores = Valores & " CD_CUEN_IMPU=" & Comi & Cambiar_Comas_Comillas(TxtImpu(4)) & Comi & Coma
            
            If OptIva.Value = True Then
               Valores = Valores & " ID_TIPO_IMPU=" & Comi & "I" & Comi
            End If
            If OptIca.Value = True Then
               Valores = Valores & " ID_TIPO_IMPU=" & Comi & "C" & Comi
            End If
            If OptRete.Value = True Then
               Valores = Valores & " ID_TIPO_IMPU=" & Comi & "R" & Comi
            End If
            If OptImRe.Value = True Then
               Valores = Valores & " ID_TIPO_IMPU=" & Comi & "M" & Comi
            End If
            If OptOim.Value = True Then
               Valores = Valores & " ID_TIPO_IMPU=" & Comi & "O" & Comi
            End If
            'JAUM T29614 Inicio
            If OptIva.Value = True Or OptIca.Value = True Or OptImRe.Value = True Or OptOim.Value = True Then
               Valores = Valores & ", TX_RETEFU_IMPU = 0"
            End If
            'JAUM T29614 Fin
            Condicion = "CD_CODI_IMPU=" & Comi & Cambiar_Comas_Comillas(TxtImpu(0)) & Comi
            Result = DoUpdate(Tabla, Valores, Condicion)
            If (Result <> FAIL) Then Result = Auditor(Tabla, TranUpd, LastCmd)
         End If
      End If
   End If
   If (Result <> FAIL) Then
      If (CommitTran() <> FAIL) Then
          Limpiar
      Else
         Call RollBackTran
      End If
   Else
      Call RollBackTran
   End If
   Call MouseNorm
   Msglin NUL$
End Sub
Private Sub Borrar()
   DoEvents
   
   Call MouseClock
   
   Msglin "Borrando un " & Mensaje
   If (TxtImpu(0) = NUL$) Then
       Call Mensaje1("Ingrese el c�digo del " & Mensaje, 1)
       Call MouseNorm: Msglin NUL$: Exit Sub
   End If
   'PedroJ
      ReDim arr(0)
      Condicion = "CD_IMPU_COIM=" & Comi & Cambiar_Comas_Comillas(TxtImpu(0)) & Comi
      Result = LoadData("R_CONC_IMPU", "CD_CONC_COIM", Condicion, arr)
      If Result <> FAIL And Encontro Then
          Call Mensaje1("Este Impuesto esta asociado a un Concepto de Movimiento", 1)
          Exit Sub
      End If
   'PedroJ
   
   If (EncontroT <> False) Then
       If (Not WarnDel()) Then Call MouseNorm: Msglin NUL$: Exit Sub
        Condicion = "CD_CODI_IMPU=" & Comi & Cambiar_Comas_Comillas(TxtImpu(0)) & Comi
        If (BeginTran(STranDel & Tabla) <> FAIL) Then
            Result = DoDelete(Tabla, Condicion)
            If (Result <> FAIL) Then Result = Auditor(Tabla, TranDel, LastCmd)
        End If
   Else
       Call Mensaje1("No se pudo borrar el " & Mensaje & " de la base de datos, debido a que no existe !!!...", 1)
       Call MouseNorm: Msglin NUL$: Exit Sub
   End If
   If (Result <> FAIL) Then
       If (CommitTran() <> FAIL) Then
          Limpiar
       Else
          Call RollBackTran
       End If
   Else
       Call RollBackTran
   End If
   Call MouseNorm
   Msglin NUL$
End Sub
Private Sub Limpiar()
Dim I As Byte
   For I = 0 To 4
      If I = 2 Or I = 3 Then
         TxtImpu(I) = "0.00"
      Else
         TxtImpu(I) = NUL$
      End If
   Next I
   TxtImpu(2).Text = "0.0000"
   OptIva.Value = True
   TxtImpu(0).SetFocus
   TxtImpu(2).Enabled = True 'JAUM T29614
   TxtImpu(3).Enabled = True 'JAUM T29614
End Sub

