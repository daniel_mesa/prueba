VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "MSFLXGRD.OCX"
Object = "{0BA686C6-F7D3-101A-993E-0000C0EF6F5E}#1.0#0"; "THREED32.OCX"
Begin VB.Form FrmSeleccion3 
   BorderStyle     =   1  'Fixed Single
   ClientHeight    =   6705
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   11445
   ClipControls    =   0   'False
   Icon            =   "FrmSele3.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   6705
   ScaleWidth      =   11445
   StartUpPosition =   2  'CenterScreen
   Begin VB.OptionButton OptSeleccion 
      Caption         =   "C�digo"
      Height          =   255
      Index           =   1
      Left            =   1560
      TabIndex        =   13
      Top             =   120
      Width           =   1155
   End
   Begin VB.OptionButton OptSeleccion 
      Caption         =   "Nombre"
      Height          =   255
      Index           =   0
      Left            =   240
      TabIndex        =   12
      Top             =   120
      Value           =   -1  'True
      Width           =   1155
   End
   Begin VB.Frame frmCUAL 
      Height          =   735
      Left            =   9480
      TabIndex        =   8
      Top             =   4080
      Visible         =   0   'False
      Width           =   1815
      Begin VB.OptionButton opcPROVE 
         Caption         =   "Opciones"
         Height          =   255
         Left            =   120
         TabIndex        =   10
         Top             =   420
         Width           =   1575
      End
      Begin VB.OptionButton opcSALDOS 
         Caption         =   "Saldos"
         Height          =   255
         Left            =   120
         TabIndex        =   9
         Top             =   160
         Value           =   -1  'True
         Width           =   1575
      End
   End
   Begin MSComctlLib.ListView lstPROVEEDORES 
      Height          =   2595
      Left            =   480
      TabIndex        =   7
      Top             =   3960
      Visible         =   0   'False
      Width           =   8600
      _ExtentX        =   15161
      _ExtentY        =   4577
      View            =   3
      LabelEdit       =   1
      LabelWrap       =   -1  'True
      HideSelection   =   0   'False
      FullRowSelect   =   -1  'True
      GridLines       =   -1  'True
      _Version        =   393217
      ForeColor       =   -2147483640
      BackColor       =   -2147483643
      BorderStyle     =   1
      Appearance      =   1
      NumItems        =   0
   End
   Begin VB.ComboBox TxtDesde 
      Height          =   315
      ItemData        =   "FrmSele3.frx":058A
      Left            =   3600
      List            =   "FrmSele3.frx":05A0
      TabIndex        =   0
      Text            =   "Iniciales"
      Top             =   120
      Width           =   4095
   End
   Begin Threed.SSCommand SscMarcar 
      Height          =   495
      Left            =   10680
      TabIndex        =   1
      ToolTipText     =   "Marcar o Desmarcar todo"
      Top             =   0
      Width           =   615
      _Version        =   65536
      _ExtentX        =   1085
      _ExtentY        =   873
      _StockProps     =   78
      Caption         =   "TODO"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   6
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Picture         =   "FrmSele3.frx":05CC
   End
   Begin MSFlexGridLib.MSFlexGrid Grd_Selecciones 
      Height          =   3255
      Left            =   0
      TabIndex        =   2
      Top             =   600
      Width           =   11415
      _ExtentX        =   20135
      _ExtentY        =   5741
      _Version        =   393216
      Cols            =   8
      FixedCols       =   0
      ForeColorSel    =   -2147483635
      BackColorBkg    =   12632256
      FocusRect       =   2
      MergeCells      =   2
   End
   Begin Threed.SSCommand SscAceptar 
      Height          =   495
      Left            =   9960
      TabIndex        =   3
      Top             =   4920
      Width           =   1215
      _Version        =   65536
      _ExtentX        =   2143
      _ExtentY        =   873
      _StockProps     =   78
      Caption         =   "&ACEPTAR"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   6
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Picture         =   "FrmSele3.frx":08EE
   End
   Begin Threed.SSCommand SscCancelar 
      Height          =   495
      Left            =   9960
      TabIndex        =   4
      Top             =   5640
      Width           =   1215
      _Version        =   65536
      _ExtentX        =   2143
      _ExtentY        =   873
      _StockProps     =   78
      Caption         =   "&CANCELAR"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   6
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Picture         =   "FrmSele3.frx":0C40
   End
   Begin MSComctlLib.ListView lstSALXBOD 
      Height          =   2595
      Left            =   480
      TabIndex        =   6
      Top             =   3960
      Width           =   8600
      _ExtentX        =   15161
      _ExtentY        =   4577
      View            =   3
      LabelEdit       =   1
      LabelWrap       =   -1  'True
      HideSelection   =   0   'False
      Checkboxes      =   -1  'True
      FullRowSelect   =   -1  'True
      GridLines       =   -1  'True
      _Version        =   393217
      ForeColor       =   -2147483640
      BackColor       =   -2147483643
      BorderStyle     =   1
      Appearance      =   1
      NumItems        =   0
   End
   Begin MSFlexGridLib.MSFlexGrid grdPROYECTO 
      Height          =   4455
      Left            =   2040
      TabIndex        =   11
      Top             =   2040
      Visible         =   0   'False
      Width           =   8295
      _ExtentX        =   14631
      _ExtentY        =   7858
      _Version        =   393216
      AllowUserResizing=   1
   End
   Begin VB.Label LblRango 
      AutoSize        =   -1  'True
      Caption         =   "Desde : "
      Height          =   195
      Left            =   2880
      TabIndex        =   5
      Top             =   120
      Width           =   600
   End
End
Attribute VB_Name = "FrmSeleccion3"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Dim todo As Boolean
Private vLblTabla As String
Private vLblCampos As String
Private vLblCondicion As String
Private vLblTitulo As String
'Private vFilaGrilla As Integer     'DEPURACION DE CODIGO
'Private VcolMarca As Integer   'DEPURACION DE CODIGO
Private vColGrdCodigo As Integer
Private vStrCodigo As String
Private vEsTodo As Boolean
Private vBodegas As New Collection
Private CllCDBXArt As New Collection
Private vTipoBodega As String
Private vEsOrdenDeCompra As Boolean
Private vNOMostrarSaldos As Boolean       'PJCA

Private vTercero As String
'Private vArticulo As String        'DEPURACION DE CODIGO
Private vColAutoArticulo As Byte
Private vColCodigoArticulo As Byte
Private vColNombreArticulo As Byte
Private vColCDBarra As Byte
Private vColNombreComercial As Byte
Private vColNitTercero As Byte
Private vColNombreTercero As Byte
Private vColCosto As Byte
Private vColImpuesto As Byte
Private vColCalifica As Byte
Private vColOpcion As Byte
Private vColOrden As Byte

Private vColGrdCosto As Integer, vColGrdImpuesto As Integer
Private vColumnaGrdProyecto As Integer, vFilaGrdProyecto As Integer
Private vColumnaGrd_Selecciones As Integer, vFilaGrd_Selecciones As Integer

Private UnArticulo As New ElArticulo
Private UnTercero As New ElTercero
Private UnCDBarra As New CdgBarra
'Private vContar As Integer, vFila As Integer
Private vFila As Integer    'DEPURACION DE CODIGO
Private vclave As String, vAsignado As Long
Private ArrPRYC() As Variant

Property Let NOMostrarSaldos(Log As Boolean)      'PJCA: Si es True no muestra los saldos (Ejm: cuando sea entrada o Requisici�n u OC)
    vNOMostrarSaldos = Log
End Property

Property Let EsOrdenDeCompra(Log As Boolean)
    vEsOrdenDeCompra = Log
End Property

Property Let TipoDeBodega(Cue As String)
    vTipoBodega = Cue
End Property

Property Let Tercero(Cue As String)
    vTercero = Cue
End Property

Public Property Let Lbl_Tabla(Cue As String)
    vLblTabla = Cue
End Property

Public Property Let Lbl_Campos(Cue As String)
    vLblCampos = Cue
End Property

Public Property Let Lbl_Condicion(Cue As String)
    vLblCondicion = Cue
End Property

Public Property Let Lbl_Titulo(Cue As String)
    vLblTitulo = Cue
End Property

Private Sub grd_selecciones_DblClick()
    Grd_Selecciones.Col = 7
    If Grd_Selecciones.TextMatrix(Grd_Selecciones.Row, 1) <> "" And Grd_Selecciones.Row <> 0 Then
        If Grd_Selecciones.TextMatrix(Grd_Selecciones.Row, 0) = "0" Then
            Grd_Selecciones.TextMatrix(Grd_Selecciones.Row, 0) = "1"
            Grd_Selecciones.CellPictureAlignment = 3
            Set Grd_Selecciones.CellPicture = MDI_Inventarios.ImgLstInventar.ListImages(1).Picture
        Else
            Grd_Selecciones.TextMatrix(Grd_Selecciones.Row, 0) = "0"
            Grd_Selecciones.CellPictureAlignment = 3
            Set Grd_Selecciones.CellPicture = LoadPicture()
        End If
    End If
End Sub

Private Sub Form_Load()
'\\\\\\\\\
    Me.grdPROYECTO.Rows = 2
    Me.grdPROYECTO.FixedRows = 1
    
    Call GrdFDef(grdPROYECTO, 0, 0, "Auto,+0," & _
        "Codigo,0," & _
        "Descripci�n,2300," & _
        "CD Barra,1500," & _
        "Nom C/ial,1800," & _
        "Nit,0," & _
        "Nombre,2800," & _
        "Costo,+1200," & _
        "Califica,+900," & _
        "Opci�n,+900," & _
        "Impuesto,+0," & _
        "Orden,+0")
    
    vColAutoArticulo = 0: vColCodigoArticulo = 1: vColNombreArticulo = 2: vColCDBarra = 3: vColNombreComercial = 4
    vColNitTercero = 5: vColNombreTercero = 6: vColCosto = 7: vColCalifica = 8: vColOpcion = 9: vColImpuesto = 10: vColOrden = 11
    
    Me.grdPROYECTO.FixedCols = 3
    Me.grdPROYECTO.MergeCells = flexMergeRestrictColumns
    Me.grdPROYECTO.MergeCol(0) = True
    Me.grdPROYECTO.MergeCol(1) = True
    Me.grdPROYECTO.MergeCol(2) = True
    Me.grdPROYECTO.MergeCol(3) = True
    Me.grdPROYECTO.Left = Me.lstSALXBOD.Left
    Me.grdPROYECTO.Height = Me.lstSALXBOD.Height
    Me.grdPROYECTO.Top = Me.lstSALXBOD.Top
    Me.grdPROYECTO.Width = Me.lstSALXBOD.Width
'////////
    Titulos_Grid
    limpia_matriz Mselec2, 1000, 6
    Me.lstSALXBOD.ListItems.Clear
    Me.lstSALXBOD.ColumnHeaders.Clear
    Me.lstSALXBOD.Width = 2 * Me.lstSALXBOD.Width / 3
    Me.lstSALXBOD.ColumnHeaders.Add , "COD", "C�digo", 0.2 * Me.lstSALXBOD.Width
    Me.lstSALXBOD.ColumnHeaders.Add , "NOM", "Nombre", 0.5 * Me.lstSALXBOD.Width
    Me.lstSALXBOD.ColumnHeaders.Add , "SLD", "Saldo", 0.16 * Me.lstSALXBOD.Width
    Me.lstSALXBOD.ColumnHeaders("SLD").Alignment = lvwColumnRight
    Me.lstSALXBOD.ColumnHeaders.Add , "MIN", "M�nimo", 0.16 * Me.lstSALXBOD.Width
    Me.lstSALXBOD.ColumnHeaders("MIN").Alignment = lvwColumnRight
    Me.lstSALXBOD.ColumnHeaders.Add , "REP", "Reposi", 0.16 * Me.lstSALXBOD.Width
    Me.lstSALXBOD.ColumnHeaders("REP").Alignment = lvwColumnRight
    Me.lstSALXBOD.ColumnHeaders.Add , "MAX", "M�ximo", 0.16 * Me.lstSALXBOD.Width
    Me.lstSALXBOD.ColumnHeaders("MAX").Alignment = lvwColumnRight
    Me.lstSALXBOD.ColumnHeaders.Add , "DES", "MxDesp", 0.16 * Me.lstSALXBOD.Width
    Me.lstSALXBOD.ColumnHeaders("DES").Alignment = lvwColumnRight
    Me.lstSALXBOD.ColumnHeaders.Add , "ENT", "Entradas", 0.16 * Me.lstSALXBOD.Width
    Me.lstSALXBOD.ColumnHeaders("ENT").Alignment = lvwColumnRight
    Me.lstSALXBOD.ColumnHeaders.Add , "SAL", "Salidas", 0.16 * Me.lstSALXBOD.Width
    Me.lstSALXBOD.ColumnHeaders("SAL").Alignment = lvwColumnRight
    Me.lstSALXBOD.Width = 3 * Me.lstSALXBOD.Width / 2
'    Me.lstPROVEEDORES.ColumnHeaders.Clear
'    Me.lstPROVEEDORES.ColumnHeaders.Add , "COD", "C�digo", 0.15 * Me.lstPROVEEDORES.Width
'    Me.lstPROVEEDORES.ColumnHeaders.Add , "DES", "Nombre", 0.2 * Me.lstPROVEEDORES.Width
'    Me.lstPROVEEDORES.ColumnHeaders.Add , "TER", "Proveedor", 0 * Me.lstPROVEEDORES.Width
'    Me.lstPROVEEDORES.ColumnHeaders.Add , "NOM", "Proveedor", 0.35 * Me.lstPROVEEDORES.Width
'    Me.lstPROVEEDORES.ColumnHeaders.Add , "COS", "Costo", 0.12 * Me.lstPROVEEDORES.Width
'    Me.lstPROVEEDORES.ColumnHeaders.Item("COS").Alignment = lvwColumnRight
'    Me.lstPROVEEDORES.ColumnHeaders.Add , "IMP", "Impuesto", 0.12 * Me.lstPROVEEDORES.Width
'    Me.lstPROVEEDORES.ColumnHeaders.Item("IMP").Alignment = lvwColumnRight
End Sub

Private Sub Grd_Selecciones_EnterCell()
'    Dim ElSa As New InfXBodega, LasSalidas As New InfXBodega, LasEntradas As New InfXBodega     'DEPURACION DE CODIGO
'    Dim Bode As LaBodega, SaldoActiva As Single, EntTtl As Single, SalTtl As Single                        'DEPURACION DE CODIGO
'    Dim SldTtl As Single, MaxTtl As Long, MinTtl As Long, RepTtl As Long, DesTtl As Long                 'DEPURACION DE CODIGO
'    Dim Lumn As Byte, CnTr As Byte, UnBdg As ListItem, LaSlctd As String                                    'DEPURACION DE CODIGO
'    Dim ParcialEntradas As Single, ParcialSalidas As Single
    
    If vNOMostrarSaldos = True Then Exit Sub
    
    vColumnaGrd_Selecciones = Me.Grd_Selecciones.Col
    vFilaGrd_Selecciones = Me.Grd_Selecciones.Row
    If vEsTodo Then Exit Sub
    If Me.Grd_Selecciones.Row < 1 Then Exit Sub
    If Me.opcSALDOS.Value Then
        With Me.Grd_Selecciones
            If .TextMatrix(.Row, vColGrdCodigo) = vStrCodigo Then GoTo SEGUIR
            'UnArticulo.IniciaXCodigo vStrCodigo
            UnArticulo.IniciaXCodigo .TextMatrix(.Row, vColGrdCodigo)       'PedroJ Def 4297
            Call InfoArticuloXBodega(Me.lstSALXBOD, UnArticulo, vBodegas, Me.lstSALXBOD.Visible)
        End With
    End If
SEGUIR:
    If Me.opcPROVE.Value Then
'\\\\\\\\
        Me.grdPROYECTO.Rows = 1
        vStrCodigo = Me.Grd_Selecciones.TextMatrix(Me.Grd_Selecciones.Row, vColGrdCodigo)
        UnArticulo.IniciaXCodigo vStrCodigo
        UnArticulo.INTCllCodigosDBarra
        Set CllCDBXArt = UnArticulo.GetCllDeCDBarra
        vclave = ""
        For Each UnCDBarra In CllCDBXArt
            vclave = vclave & IIf(Len(vclave) = 0, "", ",") & "'" & Trim(UnCDBarra.Codigo) & "'"
        Next
        If Len(vclave) = 0 Then Exit Sub
        Campos = "MAX(NU_AUTO_PROY_CDBSE)"
        Desde = "IN_CDBARSELE"
        Condi = "TX_COBA_COBA_CDBSE IN (" & vclave & ")"
        ReDim ArrPRYC(0)
        Result = LoadData(Desde, Campos, Condi, ArrPRYC())
        If Not Encontro Then Exit Sub
        If Not IsNumeric(ArrPRYC(0)) Then Exit Sub
        vAsignado = CLng(ArrPRYC(0))
        Campos = "NU_AUTO_PROY_CDBSE,TX_COBA_COBA_CDBSE,CD_CODI_TERC_CDBSE,NU_OPCI_CDBSE,NU_COST_CDBSE,NU_CALI_CDBSE,NU_ORDE_CDBSE,NU_IMPU_CDBSE"
        Campos = Campos & ",NU_AUTO_ARTI_COBA,TX_COBA_COBA,TX_REGI_COBA,TX_CIAL_COBA,TX_LABO_COBA"
        Condi = "TX_COBA_COBA_CDBSE=TX_COBA_COBA"
        Condi = Condi & " AND NU_AUTO_PROY_CDBSE=" & vAsignado
        Condi = Condi & " AND NU_AUTO_ARTI_COBA=" & UnArticulo.Numero
        Condi = Condi & " ORDER BY NU_ORDE_CDBSE"
        ReDim ArrPRYC(12, 0)
        Result = LoadMulData("IN_CDBARSELE,IN_CODIGOBAR", Campos, Condi, ArrPRYC())
        Me.grdPROYECTO.Rows = 1
        If Result = FAIL Then Exit Sub
        If Encontro Then
            vAsignado = 0
            With Me.grdPROYECTO
            For vFila = 0 To UBound(ArrPRYC, 2)
                .Rows = .Rows + 1
                .Row = .Rows - 1
                If Len(ArrPRYC(3, vFila)) = 0 Then ArrPRYC(3, vFila) = "0"
                If Len(ArrPRYC(4, vFila)) = 0 Then ArrPRYC(4, vFila) = "0"
                If Len(ArrPRYC(5, vFila)) = 0 Then ArrPRYC(5, vFila) = "0"
                If Len(ArrPRYC(6, vFila)) = 0 Then ArrPRYC(6, vFila) = "0"
                If Len(ArrPRYC(7, vFila)) = 0 Then ArrPRYC(7, vFila) = "0"
                If Not vAsignado = CLng(ArrPRYC(8, vFila)) Then
                    vAsignado = CLng(ArrPRYC(8, vFila))
                    UnArticulo.IniciaXNumero vAsignado
                End If
                UnTercero.IniXNit CStr(ArrPRYC(2, vFila))
'                Set vUno = Me.lstARTICULO.FindItem(CStr(ArrPRYC(1, vFila)), lvwTag)
'                If vUno Is Nothing Then
'                    UnCDBarra.IniciaXCodigo CStr(ArrPRYC(1, vFila))
'                    vClave = "B" & vFila
'                    Me.lstARTICULO.ListItems.Add , vClave, Trim(UnArticulo.Codigo)
'                    Me.lstARTICULO.ListItems.Item(vClave).Tag = Trim(UnCDBarra.Codigo)
'                    Me.lstARTICULO.ListItems.Item(vClave).ListSubItems.Add , "BAR", Trim(UnCDBarra.Codigo)
'                    Me.lstARTICULO.ListItems.Item(vClave).ListSubItems.Add , "NOM", Trim(UnArticulo.Nombre)
'                    Me.lstARTICULO.ListItems.Item(vClave).ListSubItems.Add , "CIA", Trim(UnCDBarra.Comercial)
'                    Me.lstARTICULO.ListItems.Item(vClave).ListSubItems.Add , "UND", Trim(UnArticulo.UnidadConteo)
'                    Me.lstARTICULO.ListItems.Item(vClave).ListSubItems.Add , "AUT", Trim(UnArticulo.Numero)
'                End If
                .TextMatrix(.Row, vColAutoArticulo) = CStr(vAsignado)
                .TextMatrix(.Row, vColCodigoArticulo) = Trim(UnArticulo.Codigo)
                .TextMatrix(.Row, vColNombreArticulo) = Trim(UnArticulo.Nombre)
                .TextMatrix(.Row, vColCDBarra) = ArrPRYC(1, vFila)
                .TextMatrix(.Row, vColNombreComercial) = ArrPRYC(11, vFila)
                .TextMatrix(.Row, vColNitTercero) = ArrPRYC(2, vFila)
                .TextMatrix(.Row, vColNombreTercero) = Trim(UnTercero.Nombre)
                .TextMatrix(.Row, vColCosto) = Format(CSng(ArrPRYC(4, vFila)), "#,###.00")
                .TextMatrix(.Row, vColCalifica) = Format(CLng(ArrPRYC(5, vFila)), "#,##0")
                .TextMatrix(.Row, vColImpuesto) = Format(CSng(ArrPRYC(7, vFila)), "#0.00")
                .TextMatrix(.Row, vColOrden) = Format(CInt(ArrPRYC(6, vFila)), "#####")
                .TextMatrix(.Row, vColOpcion) = ArrPRYC(3, vFila)
            Next
            End With
        End If
        Me.grdPROYECTO.Refresh
'////////
'        With Me.Grd_Selecciones
'            If .Row < 1 Then Exit Sub
'            If .TextMatrix(.Row, vColGrdCodigo) = vStrCodigo Then Exit Sub
'            vStrCodigo = .TextMatrix(.Row, vColGrdCodigo)
'            vArticulo.IniciaXCodigo vStrCodigo
'            vArticulo.INTCllProveedoresXArticulo
'            Call InfoProveedoresXArticulo(Me.lstPROVEEDORES, vArticulo)
'        End With
    End If
    vStrCodigo = Me.Grd_Selecciones.TextMatrix(Me.Grd_Selecciones.Row, vColGrdCodigo)
End Sub

Private Sub grdPROYECTO_DblClick()
    If vColumnaGrdProyecto = vColCosto Then 'vColGrdCosto, vColGrdImpuesto
    If Not Me.grdPROYECTO.TextMatrix(vFilaGrdProyecto, vColNitTercero) = vTercero Then Call MsgBox("El proveedor no es el mismo de la Orden de Compra", vbInformation): Exit Sub
        If CSng(Me.Grd_Selecciones.TextMatrix(vFilaGrd_Selecciones, 3)) = CSng(Me.Grd_Selecciones.TextMatrix(vFilaGrd_Selecciones, 9)) Then
            Me.Grd_Selecciones.TextMatrix(vFilaGrd_Selecciones, 3) = Format(Me.grdPROYECTO.TextMatrix(vFilaGrdProyecto, vColCosto), "#,##0.00")
            Me.Grd_Selecciones.TextMatrix(vFilaGrd_Selecciones, 8) = Me.grdPROYECTO.TextMatrix(vFilaGrdProyecto, vColImpuesto)
        Else
            Me.Grd_Selecciones.TextMatrix(vFilaGrd_Selecciones, 3) = Format(Me.Grd_Selecciones.TextMatrix(vFilaGrd_Selecciones, 9), "#,##0.00")
            Me.Grd_Selecciones.TextMatrix(vFilaGrd_Selecciones, 8) = Me.Grd_Selecciones.TextMatrix(vFilaGrd_Selecciones, 10)
        End If
    End If
End Sub

Private Sub grdPROYECTO_EnterCell()
    vColumnaGrdProyecto = Me.grdPROYECTO.Col
    vFilaGrdProyecto = Me.grdPROYECTO.Row
End Sub

Private Sub opcPROVE_Click()
    If Me.opcSALDOS.Value Then
        Me.grdPROYECTO.Visible = False
        Me.lstSALXBOD.Visible = True
    Else
        Me.lstSALXBOD.Visible = False
        Me.grdPROYECTO.Visible = True
    End If
    vStrCodigo = ""
    Call Grd_Selecciones_EnterCell
End Sub

Private Sub opcSALDOS_Click()
    If Me.opcSALDOS.Value Then
        Me.grdPROYECTO.Visible = False
        Me.lstSALXBOD.Visible = True
    Else
        Me.lstSALXBOD.Visible = False
        Me.grdPROYECTO.Visible = True
    End If
    vStrCodigo = ""
    Call Grd_Selecciones_EnterCell
End Sub

Private Sub OptSeleccion_KeyPress(Index As Integer, KeyAscii As Integer)
    Call Cambiar_Enter(KeyAscii) 'CARV R2351
End Sub

Private Sub SscAceptar_Click()
    Dim aux As Integer
    Dim aux2 As Integer
    aux = 1
    aux2 = 1
'    Grd_Selecciones.Row = 0
    While aux <= Grd_Selecciones.Rows - 1
'        Grd_Selecciones.Row = Grd_Selecciones.Row + 1
'        Grd_Selecciones.Col = 0
'        If Grd_Selecciones.Text = "1" Then
        If Grd_Selecciones.TextMatrix(aux, 0) = "1" Then
            If Grd_Selecciones.TextMatrix(aux, 1) <> "" Then
                Mselec2(aux2, 1) = Grd_Selecciones.TextMatrix(aux, 6)
                Mselec2(aux2, 2) = Grd_Selecciones.TextMatrix(aux, 1)
                Mselec2(aux2, 3) = Grd_Selecciones.TextMatrix(aux, 2)
                Mselec2(aux2, 4) = Grd_Selecciones.TextMatrix(aux, 3)
                Mselec2(aux2, 5) = Grd_Selecciones.TextMatrix(aux, 4)
                Mselec2(aux2, 6) = Grd_Selecciones.TextMatrix(aux, 5)
                If Not IsNumeric(Grd_Selecciones.TextMatrix(aux, 3)) Then Grd_Selecciones.TextMatrix(aux, 3) = "0"
                Mselec2(aux2, 7) = Grd_Selecciones.TextMatrix(aux, 3)
                If Not IsNumeric(Grd_Selecciones.TextMatrix(aux, 8)) Then Grd_Selecciones.TextMatrix(aux, 8) = "0"
'                Mselec2(aux2, 8) = Grd_Selecciones.TextMatrix(aux, 8)
                Mselec2(aux2, 8) = IIf(Grd_Selecciones.TextMatrix(aux, 10) = "S", Grd_Selecciones.TextMatrix(aux, 8), "0")  'REOL M2519
                aux2 = aux2 + 1
            End If
        End If
        aux = aux + 1
    Wend
    Unload Me
End Sub

Private Sub SscCancelar_Click()
    Unload Me
End Sub

Private Sub SscMarcar_Click()
    Dim aux As Long
    vEsTodo = True
    aux = 1
    While aux <= Grd_Selecciones.Rows - 1
        Grd_Selecciones.Col = 7
        Grd_Selecciones.Row = aux
        If Grd_Selecciones.TextMatrix(aux, 1) <> "" Then
            If todo = False Then
                Grd_Selecciones.TextMatrix(aux, 0) = "1"
                Grd_Selecciones.CellPictureAlignment = 3
                Set Grd_Selecciones.CellPicture = MDI_Inventarios.ImgLstInventar.ListImages(1).Picture
            Else
                Grd_Selecciones.TextMatrix(aux, 0) = "0"
                Grd_Selecciones.CellPictureAlignment = 3
                Set Grd_Selecciones.CellPicture = LoadPicture()
            End If
        End If
        aux = aux + 1
    Wend
    If todo = False Then
        todo = True
    Else
        todo = False
    End If
    vEsTodo = False
End Sub

Private Sub TxtDesde_KeyPress(KeyAscii As Integer)
  Call Cambiar_Enter(KeyAscii)
End Sub

Private Sub TxtDesde_LostFocus()
    Dim aux As Integer
    Dim pos As Long
    Grd_Selecciones.Clear
    Grd_Selecciones.Rows = 2
    Titulos_Grid
'    DoEvents
    
    ''''''
'    pos = InStr(vLblCampos, ",") + 1
'    Codigo = Mid(vLblCampos, pos, Len(vLblCampos))
'    Codigo = Mid(Codigo, 1, InStr(Codigo, ",") - 1)
    ''''''
     pos = InStr(vLblCampos, ",") + 1
     Codigo = Mid(vLblCampos, pos, Len(vLblCampos))
     ' CARV R2351 - INICIO
     ' Codigo = Mid(Codigo, 1, InStr(Codigo, ",") - 1)
     If OptSeleccion(0).Value = True Then
        Codigo = Mid(Codigo, 1, InStr(Codigo, ",") - 1)
     ElseIf OptSeleccion(1).Value = True Then
        Codigo = Mid(Codigo, InStr(Codigo, ",") + 1, InStr(Codigo, ",") - 1)
     End If
     ' CARV R2351 - FIN
        
    
    If Trim(TxtDesde) = "*" Or Trim(TxtDesde) = "%" Then TxtDesde = CaractLike
    Condicion = Codigo & " Like '" & TxtDesde & CaractLike & Comi & IIf(vLblCondicion = NUL$, NUL$, " AND " & vLblCondicion) & " ORDER BY " & Codigo
    
    If TxtDesde.Text <> "" Then
        Result = LoadfGrid(Grd_Selecciones, vLblTabla, vLblCampos, Condicion)
        For aux = 1 To Grd_Selecciones.Rows - 1 Step 1
            Me.Grd_Selecciones.TextMatrix(aux, 3) = Format(Grd_Selecciones.TextMatrix(aux, 3), "#,##0.00")
            Me.Grd_Selecciones.TextMatrix(aux, 5) = Format(Grd_Selecciones.TextMatrix(aux, 5), "#,##0.0")
        Next
    End If
salto:
    aux = 1
    While aux <= Grd_Selecciones.Rows - 1
'        Me.Grd_Selecciones.Row = Grd_Selecciones.Row + 1
'        Me.Grd_Selecciones.Col = 0
'        Me.Grd_Selecciones.Text = "0"
        Me.Grd_Selecciones.TextMatrix(aux, 0) = "0"
        aux = aux + 1
    Wend
    vStrCodigo = ""
    If Me.Grd_Selecciones.Rows > 1 Then
        Me.Grd_Selecciones.Row = 1
'        Call Grd_Selecciones_EnterCell
    Else
        Me.Grd_Selecciones.Row = 0
    End If
End Sub

Private Sub Form_Activate()
'    Me.opcSALDOS.Value = True
    If Len(vTipoBodega) = 0 Then Me.lstSALXBOD.Visible = False
    If Len(vTipoBodega) = 0 Then Me.lstPROVEEDORES.Visible = False
    If Len(vTipoBodega) = 0 Then Me.frmCUAL.Visible = False
    If Len(vTipoBodega) > 0 Then Me.lstSALXBOD.Visible = True
    If Len(vTipoBodega) > 0 And vEsOrdenDeCompra Then Me.frmCUAL.Visible = True
    If Len(vTipoBodega) > 0 Then Call INTCllBodegas(vBodegas, vTipoBodega)
    If todo = False Then
        TxtDesde.SetFocus
        'OptSeleccion(0).SetFocus 'CARV R2351
    Else
        TxtDesde.Text = CaractLike
        TxtDesde_LostFocus
    End If
'    If vEsOrdenDeCompra Then Me.lstSALXBOD.Visible = False
'    If vEsOrdenDeCompra Then Me.lstPROVEEDORES.Visible = True
    If Me.frmCUAL.Visible Then Me.opcSALDOS.Value = True
End Sub

Private Sub TxtDesde_GotFocus()
    TxtDesde.SelStart = 0
    TxtDesde.SelLength = Len(TxtDesde.Text)
End Sub

Private Sub Titulos_Grid()
    Me.Grd_Selecciones.Cols = 11
    Grd_Selecciones.ColWidth(0) = 0
    Grd_Selecciones.ColWidth(1) = 3800
    Grd_Selecciones.ColWidth(2) = 1800
    Grd_Selecciones.ColWidth(3) = 1400
    Grd_Selecciones.ColWidth(4) = 1600
    Grd_Selecciones.ColWidth(5) = 1200
    Grd_Selecciones.ColWidth(6) = 0
    Grd_Selecciones.ColWidth(7) = 600
    Grd_Selecciones.ColWidth(8) = 0
    Grd_Selecciones.ColWidth(9) = 0
    Grd_Selecciones.ColWidth(10) = 0
    Grd_Selecciones.Row = 0
    vColGrdCodigo = 2
    vColGrdCosto = 3
    vColGrdImpuesto = 8
    Grd_Selecciones.Col = 2
    Grd_Selecciones.CellAlignment = 4
    Grd_Selecciones.Text = "C�digo"
    Grd_Selecciones.Col = 1
    Grd_Selecciones.CellAlignment = 4
    Grd_Selecciones.Text = "Descripci�n"
    Grd_Selecciones.Col = 3
    Grd_Selecciones.CellAlignment = 4
    Grd_Selecciones.Text = "Costo Promedio"
    Grd_Selecciones.Col = 4
    Grd_Selecciones.CellAlignment = 4
    Grd_Selecciones.Text = "Unidad de Medida"
    Grd_Selecciones.Col = 5
    Grd_Selecciones.CellAlignment = 4
'    Grd_Selecciones.Text = "Concentraci�n"
    Grd_Selecciones.Text = "Consumo Promedio"
    Grd_Selecciones.Col = 7
    Grd_Selecciones.CellAlignment = 4
    Grd_Selecciones.Text = "Marcar"
    Grd_Selecciones.Row = 1
    todo = False
End Sub

