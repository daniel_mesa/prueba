VERSION 5.00
Object = "{0D452EE1-E08F-101A-852E-02608C4D0BB4}#2.0#0"; "FM20.DLL"
Begin VB.Form FrmListar 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Listar"
   ClientHeight    =   6810
   ClientLeft      =   45
   ClientTop       =   225
   ClientWidth     =   5745
   Icon            =   "FrmListar.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   6810
   ScaleWidth      =   5745
   Begin VB.Frame Frame5 
      Height          =   2655
      Left            =   120
      TabIndex        =   19
      Top             =   120
      Width           =   4815
      Begin VB.ComboBox Combo2 
         Height          =   315
         ItemData        =   "FrmListar.frx":000C
         Left            =   120
         List            =   "FrmListar.frx":0016
         TabIndex        =   25
         Text            =   "Combo1"
         Top             =   1680
         Width           =   2775
      End
      Begin VB.ComboBox Combo1 
         Height          =   315
         ItemData        =   "FrmListar.frx":002F
         Left            =   120
         List            =   "FrmListar.frx":0039
         TabIndex        =   23
         Text            =   "Combo1"
         Top             =   1080
         Width           =   2775
      End
      Begin VB.ComboBox Cbo_Listar 
         Height          =   315
         Left            =   120
         TabIndex        =   21
         Text            =   "Combo1"
         Top             =   480
         Width           =   2775
      End
      Begin MSForms.ComboBox ComboBox1 
         Height          =   255
         Left            =   720
         TabIndex        =   26
         Top             =   2160
         Width           =   3015
         VariousPropertyBits=   746604571
         DisplayStyle    =   3
         Size            =   "5318;450"
         BoundColumn     =   3
         ColumnCount     =   3
         MatchEntry      =   1
         ShowDropButtonWhen=   2
         FontHeight      =   165
         FontCharSet     =   0
         FontPitchAndFamily=   2
      End
      Begin VB.Label Lbl_Opciones 
         Caption         =   "Opciones"
         Height          =   255
         Left            =   120
         TabIndex        =   24
         Top             =   1440
         Width           =   2655
      End
      Begin VB.Label Lbl_Enviar 
         Caption         =   "Enviar a"
         Height          =   255
         Left            =   120
         TabIndex        =   22
         Top             =   840
         Width           =   2775
      End
      Begin VB.Label Lbl_Listar 
         Caption         =   "&Seleccionar por"
         Height          =   255
         Left            =   120
         TabIndex        =   20
         Top             =   240
         Width           =   2775
      End
   End
   Begin VB.Frame Frame4 
      Height          =   3375
      Left            =   120
      TabIndex        =   0
      Top             =   3120
      Width           =   4575
      Begin VB.Frame Frame2 
         Caption         =   "&Rangos"
         Height          =   1095
         Left            =   1920
         TabIndex        =   10
         Top             =   240
         Width           =   2535
         Begin VB.TextBox Txt_Final 
            Height          =   285
            Left            =   600
            TabIndex        =   12
            Text            =   "Z"
            Top             =   720
            Width           =   1815
         End
         Begin VB.TextBox Txt_Inicio 
            Height          =   285
            Left            =   600
            TabIndex        =   11
            Text            =   "0"
            Top             =   360
            Width           =   1815
         End
         Begin VB.Label Label2 
            Caption         =   "&Final"
            Height          =   255
            Left            =   120
            TabIndex        =   14
            Top             =   720
            Width           =   615
         End
         Begin VB.Label Label1 
            Caption         =   "&Inicio"
            Height          =   255
            Left            =   120
            TabIndex        =   13
            Top             =   360
            Width           =   735
         End
      End
      Begin VB.Frame Frame3 
         Caption         =   "&Listar por"
         Height          =   1095
         Left            =   120
         TabIndex        =   7
         Top             =   240
         Width           =   1695
         Begin VB.OptionButton Opt_Por 
            Caption         =   "&Nombre"
            Height          =   195
            Index           =   1
            Left            =   120
            TabIndex        =   9
            Top             =   720
            Width           =   1335
         End
         Begin VB.OptionButton Opt_Por 
            Caption         =   "C�&digo"
            Height          =   255
            Index           =   0
            Left            =   120
            TabIndex        =   8
            Top             =   360
            Value           =   -1  'True
            Width           =   1455
         End
      End
      Begin VB.Frame Frame1 
         Caption         =   "Enviar a"
         Height          =   615
         Left            =   120
         TabIndex        =   4
         Top             =   1440
         Width           =   4335
         Begin VB.OptionButton Opt_Enviar 
            Caption         =   "&Pantalla"
            Height          =   255
            Index           =   0
            Left            =   720
            TabIndex        =   6
            Top             =   240
            Value           =   -1  'True
            Width           =   1455
         End
         Begin VB.OptionButton Opt_Enviar 
            Caption         =   "&Impresora"
            Height          =   195
            Index           =   1
            Left            =   2280
            TabIndex        =   5
            Top             =   240
            Width           =   1455
         End
      End
      Begin VB.ComboBox Cbo_Opciones 
         Height          =   315
         Left            =   120
         Style           =   2  'Dropdown List
         TabIndex        =   3
         Top             =   2400
         Width           =   4335
      End
      Begin VB.CommandButton Cmd_Aceptar 
         Caption         =   "&Aceptar"
         Height          =   375
         Left            =   840
         TabIndex        =   2
         Top             =   2880
         Width           =   1215
      End
      Begin VB.CommandButton Cmd_Cancel 
         Caption         =   "&Cancelar"
         Height          =   375
         Left            =   2400
         TabIndex        =   1
         Top             =   2880
         Width           =   1335
      End
      Begin VB.Label Lbl_Codigo 
         Caption         =   "Lbl_Codigo"
         Height          =   255
         Left            =   3360
         TabIndex        =   18
         Top             =   2400
         Visible         =   0   'False
         Width           =   975
      End
      Begin VB.Label Lbl_Nombre 
         Caption         =   "Lbl_Nombre"
         Height          =   255
         Left            =   1680
         TabIndex        =   17
         Top             =   2400
         Visible         =   0   'False
         Width           =   1095
      End
      Begin VB.Label Lbl_Tabla 
         Caption         =   "Lbl_Tabla"
         Height          =   255
         Left            =   240
         TabIndex        =   16
         Top             =   2400
         Visible         =   0   'False
         Width           =   855
      End
      Begin VB.Label Lbl_Opcioness 
         Caption         =   "&Opciones"
         Height          =   255
         Left            =   120
         TabIndex        =   15
         Top             =   2160
         Width           =   3375
      End
   End
End
Attribute VB_Name = "FrmListar"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Dim Cod_Opciones() As Variant
Dim Tabla As String
Dim Codigo_Campo As String
Dim Nombre_Campo As String
Dim seccion As String
Private Sub Cmd_Aceptar_Click()
Dim Formula, Ordenar As String

Call Limpiar_CrysListar
Debug.Print MDI_Inventarios.Crys_Listar.Connect

'            .Connect = "ODBC;DSN=PACIENTE;"'            .Connect = .Connect & "UID=ADMINISTRADOR" & ";"
'MDI_Inventarios.Crys_Listar.Connect = MDI_Inventarios.Crys_Listar.Connect

Ordenar = NUL$
Ordenar = "+" & CorA & Lbl_Tabla & Punto & IIf(Opt_Por(0).Value = True, Lbl_Codigo, Lbl_Nombre) & CorB

Formula = CorA & Lbl_Tabla & Punto & IIf(Opt_Por(0).Value = True, Lbl_Codigo, Lbl_Nombre) & CorB
Formula = Formula & " >= '" & Txt_Inicio & "'"
Formula = Formula & " AND " & CorA & Lbl_Tabla & Punto & IIf(Opt_Por(0).Value = True, Lbl_Codigo, Lbl_Nombre) & CorB
Formula = Formula & " <= '" & Txt_Final & "Z" & Comi

If Cbo_Opciones.Visible = True And Cbo_Opciones <> NUL$ Then
    If Cod_Opciones(Cbo_Opciones.ListIndex) <> "TODOS" Then
        Formula = Formula & " AND " & CorA & Tabla & Punto & Codigo_Campo & CorB
        Formula = Formula & "='" & Cod_Opciones(Cbo_Opciones.ListIndex) & Comi
    End If
End If

Formula = Formula & IIf(seccion = NUL$, NUL$, " and " & seccion)
Debug.Print Formula
MDI_Inventarios.Crys_Listar.SelectionFormula = Formula
MDI_Inventarios.Crys_Listar.GroupSelectionFormula = NUL$
MDI_Inventarios.Crys_Listar.Destination = IIf(Opt_Enviar(0).Value = True, 0, 1)
On Error GoTo Control_Listar

MDI_Inventarios.Crys_Listar.SortFields(0) = Ordenar
MDI_Inventarios.Crys_Listar.Action = 1

Exit Sub

Control_Listar:
    Call Mensaje1(err.Number & ESP$ & err.Description, 1)
Resume Next
End Sub

Private Sub cmd_cancel_Click()
Unload Me
End Sub

Private Sub Form_Load()
   Me.Height = 3765
   Me.Width = 4665
   Call CenterForm(MDI_Inventarios, FrmListar)
End Sub
Private Sub Form_Activate()
   If Cbo_Opciones.Visible = False Then
      Me.Height = 3135
      Cmd_Aceptar.Top = 2280
      Cmd_Cancel.Top = 2280
      Lbl_Opciones.Visible = False
   End If
End Sub


Sub Opciones(ByVal TablaOpcion As String, ByVal NombreOpcion As String, ByVal CodigoOpcion As String, ByVal CondiOpcion As String, ByVal OcultarSeccion As String)
Result = loadctrl(TablaOpcion, NombreOpcion, CodigoOpcion, Cbo_Opciones, Cod_Opciones(), CondiOpcion)
If Result <> FAIL Then
    Tabla = TablaOpcion
    Nombre_Campo = NombreOpcion
    Codigo_Campo = CodigoOpcion
    ReDim Preserve Cod_Opciones(UBound(Cod_Opciones()) + 1)
    Cod_Opciones(UBound(Cod_Opciones())) = "TODOS"
    Cbo_Opciones.AddItem "TODOS"
    Cbo_Opciones.ListIndex = 0
    seccion = OcultarSeccion
    
End If
End Sub

Private Sub Opt_Enviar_KeyPress(Index As Integer, KeyAscii As Integer)
Call Cambiar_Enter(KeyAscii)
End Sub

Private Sub Opt_Por_Click(Index As Integer)
    If Opt_Por(0).Value = True Then
       Txt_Inicio.Text = "0"
       Txt_Final.Text = "Z"
    Else
       Txt_Inicio.Text = "A"
       Txt_Final.Text = "Z"
    End If
End Sub

Private Sub Opt_Por_KeyPress(Index As Integer, KeyAscii As Integer)
   Call Cambiar_Enter(KeyAscii)
End Sub

Private Sub Txt_Final_KeyPress(KeyAscii As Integer)
   Call Cambiar_Enter(KeyAscii)
End Sub

Private Sub Txt_Inicio_KeyPress(KeyAscii As Integer)
   Call Cambiar_Enter(KeyAscii)
End Sub

