VERSION 5.00
Object = "{6B7E6392-850A-101B-AFC0-4210102A8DA7}#1.3#0"; "COMCTL32.OCX"
Begin VB.Form FrmIniMov 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Inicializar Movimiento."
   ClientHeight    =   1590
   ClientLeft      =   3840
   ClientTop       =   3975
   ClientWidth     =   4065
   Icon            =   "FrmIniMov.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   1590
   ScaleWidth      =   4065
   Begin VB.CommandButton Com 
      Caption         =   "&INICIAR PROCESO"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   6
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   615
      Left            =   360
      Picture         =   "FrmIniMov.frx":058A
      Style           =   1  'Graphical
      TabIndex        =   1
      Top             =   840
      Width           =   1455
   End
   Begin VB.CommandButton Com2 
      Caption         =   "&SALIR"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   6
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   615
      Left            =   2160
      Picture         =   "FrmIniMov.frx":0C44
      Style           =   1  'Graphical
      TabIndex        =   0
      Top             =   840
      Width           =   1455
   End
   Begin ComctlLib.ProgressBar Prog 
      Height          =   255
      Left            =   120
      TabIndex        =   2
      Top             =   480
      Width           =   3735
      _ExtentX        =   6588
      _ExtentY        =   450
      _Version        =   327682
      Appearance      =   1
   End
   Begin VB.Label Label1 
      Caption         =   "Inicializa movimientos y consecutivos de Inventarios."
      Height          =   255
      Left            =   120
      TabIndex        =   3
      Top             =   120
      Width           =   3855
   End
End
Attribute VB_Name = "FrmIniMov"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Dim Final As Boolean
Dim OPC As Integer
Dim Iprog As Integer

Private Sub Com_Click()
OPC = MsgBox("Este proceso borra informaci�n que puede ser importante," _
    & " se recomienda hacer antes una copia de seguridad de la base de " _
    & "datos de Inventarios. �Esta seguro de realizar el proceso?", vbInformation + vbYesNo, "ADVERTENCIA")
If OPC = vbYes Then
   Final = False: Iprog = 0: Result = 0
   Call MouseClock
   Com.Enabled = False
   Com2.Caption = "&Cancelar"
   Prog.Min = 0: Prog.Max = 36: Prog.value = 0: Prog.Visible = True
   Call Proceso
   If (Result <> FAIL) Then
      If (CommitTran() <> FAIL) Then
         'exitos termino el proceso
         Call Beep: Call Beep
         Call MsgBox("�El Proceso Fue Exitoso!", vbInformation)
      Else
         Call RollBackTran
      End If
   Else
    If Final = True Then
      Call MsgBox("�El Proceso Fue Cancelado!", vbInformation)
    End If
   End If
   Com.Enabled = True
   Com2.Caption = "&Salir"
   Prog.value = 0
   Msglin NUL$
   Call MouseNorm
End If
End Sub

Private Sub Com2_Click()
   On Error Resume Next
   If Com.Enabled = True Then
       Unload Me
   Else
       Final = True
   End If
End Sub

Private Sub Form_Load()
   Call CenterForm(MDI_Inventarios, Me)
End Sub
  

Private Sub Form_Unload(Cancel As Integer)
    Call MouseNorm
    Final = True
End Sub

Sub Proceso()
    If (BeginTran(STranIUp & "INICIALIZAR") <> FAIL) Then
        BD(BDCurCon).CommandTimeout = 0
        Result = DoDelete("IN_KARDEXLOTE", NUL$)
        If Result <> FAIL Then Result = DoDelete("IN_DETALLE", NUL$)
        If Result <> FAIL Then Result = DoDelete("IN_KARDEX", NUL$)
        If Result <> FAIL Then Result = DoDelete("IN_PARTIPARTI", NUL$)
        If Result <> FAIL Then Result = DoDelete("IN_R_ENCA_PPTO", NUL$)
        If Result <> FAIL Then Result = DoDelete("IN_VAL_IMDE", NUL$) 'JAUM T38541
        If Result <> FAIL Then Result = DoDelete("IN_ENCABEZADO", NUL$)
        If Result <> FAIL Then Result = DoDelete("IN_DETA_DOCUCOSTO", NUL$) 'JAUM T38541
        If Result <> FAIL Then Result = DoDelete("IN_DETA_AJUSTECOSTO", NUL$) 'JAUM T38541
        If Result <> FAIL Then Result = DoDelete("IN_AJUSTECOSTOPROM", NUL$) 'GMS R2074
        If Result <> FAIL Then Result = DoDelete("IN_DETA_AJUSTECOSTO", NUL$) 'GMS R2074
        Condi = "CD_CONC_MOVC IN ('DVEN','ENTR','APDN','SALI','VENT','DEVC','DSP','TRS','DDP')"
        If Result <> FAIL Then Result = DoDelete("MOV_CONTABLE", Condi)
        If Result <> FAIL Then Result = DoDelete("IN_AUDMOVCONT", NUL$) 'JLPB T41459-R37519
        'Campos = "VL_ULCO_ARTI = 0, VL_COPR_ARTI = 0, TX_ENTRA_ARTI='N',TX_VENCE_ARTI='N'"
        Campos = "VL_ULCO_ARTI = 0, VL_COPR_ARTI = 0" 'NYCM M2746
        Result = DoUpdate("ARTICULO", Campos, "")
        If Result <> FAIL Then Result = DoDelete("AUDITOR_IN", NUL$)
        If Result <> FAIL Then Result = DoDelete("CONEXION", NUL$)
        If Result <> FAIL Then Result = DoUpdate("IN_COMPROBANTE", "NU_COMP_COMP = 0", "") 'NYCM M2790
        If Result <> FAIL Then Result = DoDelete("R_DESCFV_ENDE", NUL$) 'DEGP T43448
        BD(BDCurCon).CommandTimeout = 30
    End If
    If (Result <> FAIL) Then
       If (CommitTran() <> FAIL) Then
          Call MsgBox("�El Proceso Fue Exitoso!", vbInformation)
       Else
          Call Mensaje1("No se Inicializ� el Movimiento!!", 1)
          Call RollBackTran
       End If
    Else
       Call Mensaje1("No se Inicializ� el Movimiento!!", 1)
       Call RollBackTran
    End If
    Call MouseNorm
    Msglin NUL$

End Sub


