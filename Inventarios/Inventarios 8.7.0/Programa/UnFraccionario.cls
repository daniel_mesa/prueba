VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "UnFraccionario"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Private vNumerador As Long
Private vDenominador As Long

Public Property Let Numerador(num As Long)
    vNumerador = num
End Property

Public Property Let Denominador(num As Long)
    vDenominador = num
End Property

Public Property Get Numerador() As Long
    Numerador = vNumerador
End Property

Public Property Get Denominador() As Long
    Denominador = vDenominador
End Property

Private Sub Class_Initialize()
    vDenominador = 1
End Sub
