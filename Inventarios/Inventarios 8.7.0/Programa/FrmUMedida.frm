VERSION 5.00
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "MSFLXGRD.OCX"
Object = "{0BA686C6-F7D3-101A-993E-0000C0EF6F5E}#1.0#0"; "THREED32.OCX"
Begin VB.Form FrmUMedida 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "UNIDAD DE MEDIDA"
   ClientHeight    =   4140
   ClientLeft      =   45
   ClientTop       =   375
   ClientWidth     =   5580
   Icon            =   "FrmUMedida.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   ScaleHeight     =   4140
   ScaleWidth      =   5580
   Begin VB.Frame FrmBotones 
      Height          =   4095
      Left            =   4440
      TabIndex        =   11
      Top             =   0
      Width           =   1095
      Begin Threed.SSCommand SCmdOpcion 
         Height          =   855
         Index           =   0
         Left            =   120
         TabIndex        =   7
         ToolTipText     =   "Guardar"
         Top             =   240
         Width           =   855
         _Version        =   65536
         _ExtentX        =   1508
         _ExtentY        =   1508
         _StockProps     =   78
         Caption         =   "&GUARDAR"
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   6
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         AutoSize        =   1
         Picture         =   "FrmUMedida.frx":058A
      End
      Begin Threed.SSCommand SCmdOpcion 
         Height          =   855
         Index           =   1
         Left            =   120
         TabIndex        =   8
         ToolTipText     =   "Borrar"
         Top             =   1200
         Width           =   855
         _Version        =   65536
         _ExtentX        =   1508
         _ExtentY        =   1508
         _StockProps     =   78
         Caption         =   "&BORRAR"
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   6
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         AutoSize        =   1
         Picture         =   "FrmUMedida.frx":11DE
      End
      Begin Threed.SSCommand SCmdOpcion 
         Height          =   855
         Index           =   2
         Left            =   120
         TabIndex        =   9
         ToolTipText     =   "Listar"
         Top             =   2160
         Width           =   855
         _Version        =   65536
         _ExtentX        =   1508
         _ExtentY        =   1508
         _StockProps     =   78
         Caption         =   "&LISTAR"
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   6
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         AutoSize        =   1
         Picture         =   "FrmUMedida.frx":1E32
      End
      Begin Threed.SSCommand SCmdOpcion 
         Height          =   855
         Index           =   3
         Left            =   120
         TabIndex        =   10
         ToolTipText     =   "Salir"
         Top             =   3120
         Width           =   855
         _Version        =   65536
         _ExtentX        =   1508
         _ExtentY        =   1508
         _StockProps     =   78
         Caption         =   "&SALIR"
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   6
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         AutoSize        =   1
         Picture         =   "FrmUMedida.frx":2A86
      End
   End
   Begin VB.Frame FrmLista 
      Caption         =   "Lista"
      Height          =   2895
      Left            =   120
      TabIndex        =   4
      Top             =   1200
      Width           =   4215
      Begin MSFlexGridLib.MSFlexGrid MsflexLista 
         Height          =   2535
         Left            =   120
         TabIndex        =   6
         Top             =   240
         Width           =   3975
         _ExtentX        =   7011
         _ExtentY        =   4471
         _Version        =   393216
         Rows            =   1
         FixedCols       =   0
         SelectionMode   =   1
         AllowUserResizing=   1
      End
   End
   Begin VB.TextBox TxtDescripcion 
      Height          =   645
      Left            =   1080
      MaxLength       =   50
      MultiLine       =   -1  'True
      TabIndex        =   5
      Top             =   480
      Width           =   3255
   End
   Begin VB.TextBox TxtCodigo 
      Height          =   285
      Left            =   1080
      MaxLength       =   3
      TabIndex        =   1
      Top             =   120
      Width           =   1335
   End
   Begin Threed.SSCommand SCmdOpcion 
      Height          =   390
      Index           =   4
      Left            =   2400
      TabIndex        =   2
      Top             =   45
      Width           =   555
      _Version        =   65536
      _ExtentX        =   979
      _ExtentY        =   688
      _StockProps     =   78
      BevelWidth      =   0
      Outline         =   0   'False
      AutoSize        =   1
      Picture         =   "FrmUMedida.frx":36DA
   End
   Begin VB.Label LblDescripcion 
      Caption         =   "Descripci�n:"
      Height          =   255
      Left            =   120
      TabIndex        =   3
      Top             =   480
      Width           =   975
   End
   Begin VB.Label LblCodigo 
      Caption         =   "C�digo:"
      Height          =   255
      Left            =   120
      TabIndex        =   0
      Top             =   120
      Width           =   975
   End
End
Attribute VB_Name = "FrmUMedida"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'---------------------------------------------------------------------------------------
' Module    : FrmUMedida
' DateTime  : 30/09/2016 07:46
' Author    : jairo_parra
' Purpose   : JLPB T36875-R35965 'Formulario para almacenar y consultar las unidades de medida
'---------------------------------------------------------------------------------------

Option Explicit

Dim VrArr() As Variant
Private Const StTabla As String = "UNIDAD_MEDIDA"
Dim BoEncontro As Boolean

'---------------------------------------------------------------------------------------
' Procedure : Form_Load
' DateTime  : 30/09/2016 07:53
' Author    : jairo_parra
' Purpose   : JLPB T36875-R35965 Carga los valores iniciales del formulario
'---------------------------------------------------------------------------------------
'
Private Sub Form_Load()
   Call CenterForm(MDI_Inventarios, Me)
   Call Leer_Permisos(Me.Name, SCmdOpcion(0), SCmdOpcion(1), SCmdOpcion(2), FrmMenu.StNomForm)
   Call GrdFDef(MsflexLista, 0, 0, "C�digo,700,                         Descripci�n,3100")
   Call Cargar_Datos
End Sub

'---------------------------------------------------------------------------------------
' Procedure : MsflexLista_DblClick
' DateTime  : 30/09/2016 08:59
' Author    : jairo_parra
' Purpose   : JLPB T36875-R35965 Carga la informaci�n del grid a los controles
'---------------------------------------------------------------------------------------
'
Private Sub MsflexLista_DblClick()
   If MsflexLista.Rows > 1 Then
      MsflexLista.Col = 0
      TxtCodigo = MsflexLista.Text
      TxtCodigo = MsflexLista.Text
      MsflexLista.Col = 1
      TxtDescripcion = MsflexLista.Text
      If TxtCodigo.Enabled Then 'JLPB T37126
         TxtCodigo.SetFocus
      End If 'JLPB T37126
   End If
End Sub

'---------------------------------------------------------------------------------------
' Procedure : MsflexLista_KeyPress
' DateTime  : 30/09/2016 08:59
' Author    : jairo_parra
' Purpose   : JLPB T36875-R35965 Cambia el enter por el tab
'---------------------------------------------------------------------------------------
'
Private Sub MsflexLista_KeyPress(KeyAscii As Integer)
   Call Cambiar_Enter(KeyAscii)
End Sub

'---------------------------------------------------------------------------------------
' Procedure : SCmdOpcion_Click
' DateTime  : 30/09/2016 08:59
' Author    : jairo_parra
' Purpose   : JLPB T36875-R35965 Ejecuta el metodo segun el boton seleccionado
'---------------------------------------------------------------------------------------
'
Private Sub SCmdOpcion_Click(Index As Integer)
   Dim StCodigo As String
   Select Case Index
      Case 0: Call Grabar
      Case 1: Call Borrar
      Case 2: Call Listar
      Case 3: Unload Me
      Case 4:
         StCodigo = Seleccion(StTabla, "TX_DESC_UMED", "TX_CODIGO_UMED,TX_DESC_UMED", "UNIDAD_MEDIDA", NUL$)
         If StCodigo <> NUL$ Then
            TxtCodigo = StCodigo
            Call TxtCodigo_LostFocus
         Else
            TxtCodigo = NUL$
         End If
   End Select
End Sub

'---------------------------------------------------------------------------------------
' Procedure : SCmdOpcion_KeyPress
' DateTime  : 30/09/2016 08:59
' Author    : jairo_parra
' Purpose   : JLPB T36875-R35965 Cambia el enter por el tab
'---------------------------------------------------------------------------------------
'
Private Sub SCmdOpcion_KeyPress(Index As Integer, KeyAscii As Integer)
   Call Cambiar_Enter(KeyAscii)
End Sub

'---------------------------------------------------------------------------------------
' Procedure : TxtCodigo_Change
' DateTime  : 30/09/2016 07:48
' Author    : jairo_parra
' Purpose   : JLPB T36875-R35965 'Evento que valida los caracteres permitidos a ingresar
'---------------------------------------------------------------------------------------
'
Private Sub TxtCodigo_Change()
   Validar_Caracteres TxtCodigo, "0\9�a\z�A\Z�ѯ�"
End Sub

'---------------------------------------------------------------------------------------
' Procedure : Cargar_Datos
' DateTime  : 30/09/2016 08:59
' Author    : jairo_parra
' Purpose   : JLPB T36875-R35965 Carga los datos almacenados en el grid
'---------------------------------------------------------------------------------------
'
Private Function Cargar_Datos()
   Desde = StTabla
   Campos = "TX_CODIGO_UMED,TX_DESC_UMED"
   Result = DoSelect(Desde, Campos, NUL$)
   MsflexLista.Rows = 1
   If Result <> FAIL Then
      If ResultQuery() Then
         While NextRowQuery()
            MsflexLista.AddItem DataQuery(1) & vbTab & DataQuery(2)
         Wend
      End If
   End If
End Function

'---------------------------------------------------------------------------------------
' Procedure : TxtCodigo_GotFocus
' DateTime  : 30/09/2016 08:59
' Author    : jairo_parra
' Purpose   : JLPB T36875-R35965 Carga informaci�n en la barra de estado
'---------------------------------------------------------------------------------------
'
Private Sub TxtCodigo_GotFocus()
   TxtCodigo.Tag = TxtCodigo
   Msglin "Digite el c�digo de la unidad de medida"
End Sub

'---------------------------------------------------------------------------------------
' Procedure : TxtCodigo_KeyPress
' DateTime  : 30/09/2016 09:00
' Author    : jairo_parra
' Purpose   : JLPB T36875-R35965 Cambia el enter por el tab
'---------------------------------------------------------------------------------------
'
Private Sub TxtCodigo_KeyPress(KeyAscii As Integer)
   Call Cambiar_Enter(KeyAscii)
End Sub

'---------------------------------------------------------------------------------------
' Procedure : TxtCodigo_LostFocus
' DateTime  : 30/09/2016 09:00
' Author    : jairo_parra
' Purpose   : JLPB T36875-R35965 Valida si existe el registro y lo carga en los controles
'---------------------------------------------------------------------------------------
'
Private Sub TxtCodigo_LostFocus()
   ReDim VrArr(1)
   Condicion = "TX_CODIGO_UMED=" & Comi & TxtCodigo & Comi
   Result = LoadData(StTabla, "TX_CODIGO_UMED,TX_DESC_UMED", Condicion, VrArr())
   If Result <> FAIL And Encontro Then
      TxtCodigo = VrArr(0)
      TxtDescripcion = Restaurar_Comas_Comillas(CStr(VrArr(1)))
      BoEncontro = True
      If TxtDescripcion.Enabled Then 'JLPB T37126
         TxtDescripcion.SetFocus
      End If 'JLPB T37126
   Else
      TxtDescripcion = NUL$
      BoEncontro = False
   End If
End Sub

'---------------------------------------------------------------------------------------
' Procedure : Limpiar
' DateTime  : 30/09/2016 09:00
' Author    : jairo_parra
' Purpose   : JLPB T36875-R35965 Limpia los controles
'---------------------------------------------------------------------------------------
'
Private Sub Limpiar()
   TxtCodigo = NUL$
   TxtDescripcion = NUL$
   Call Cargar_Datos
   If TxtCodigo.Enabled Then 'JLPB T37126
      TxtCodigo.SetFocus
   End If 'JLPB T37126
   Msglin NUL$
End Sub

'---------------------------------------------------------------------------------------
' Procedure : TxtDescripcion_Change
' DateTime  : 10/10/2016 16:09
' Author    : jairo_parra
' Purpose   : JLPB T36875-R35965 'Evento que valida los caracteres permitidos a ingresar
'---------------------------------------------------------------------------------------
'
Private Sub TxtDescripcion_Change()
   Validar_Caracteres TxtDescripcion, "0\9�a\z�A\Z� \+�-\/�:\@�[\_�{\}�ѯ�"
End Sub

'---------------------------------------------------------------------------------------
' Procedure : TxtDescripcion_GotFocus
' DateTime  : 30/09/2016 09:00
' Author    : jairo_parra
' Purpose   : JLPB T36875-R35965 Carga informaci�n en la barra de estado
'---------------------------------------------------------------------------------------
'
Private Sub TxtDescripcion_GotFocus()
   Msglin "Digite el c�digo de " & Me.Caption
End Sub

'---------------------------------------------------------------------------------------
' Procedure : TxtDescripcion_KeyPress
' DateTime  : 30/09/2016 09:00
' Author    : jairo_parra
' Purpose   : JLPB T36875-R35965 Cambia el enter por el tab
'---------------------------------------------------------------------------------------
'
Private Sub TxtDescripcion_KeyPress(KeyAscii As Integer)
   Call Cambiar_Enter(KeyAscii)
End Sub

'---------------------------------------------------------------------------------------
' Procedure : Borrar
' DateTime  : 30/09/2016 09:00
' Author    : jairo_parra
' Purpose   : JLPB T36875-R35965 Borra el registro de la BD si no se encuentra ralacionado a un articulo
'---------------------------------------------------------------------------------------
'
Private Sub Borrar()
   Call MouseClock
   If TxtCodigo = NUL$ Then
      Call MsgBox("Ingrese el c�digo de la unidad de medida.", vbCritical, App.Title)
      Call MouseNorm: Exit Sub
   End If
   If TxtDescripcion = NUL$ Then
      Call MsgBox("No se puede borrar la unidad de medida por que no tiene un c�digo seleccionado.", vbCritical, App.Title)
      Call MouseNorm: Exit Sub
   End If
   If (BoEncontro <> False) Then
      If (Not WarnDel()) Then Call MouseNorm:  Exit Sub
      Msglin "Eliminando registro"
      Condicion = "TX_CODIGO_UMED_ARTI='" & TxtCodigo & Comi
      If existedato("ARTICULO", "TX_CODIGO_UMED_ARTI", Condicion) = True Then
         Call MouseNorm: Msglin NUL$: Exit Sub
      End If
      Condicion = "TX_CODIGO_UMED='" & TxtCodigo & Comi
      If (BeginTran(STranDel & StTabla) <> FAIL) Then
         Result = DoDelete(StTabla, Condicion)
         If (Result <> FAIL) Then Result = Auditor(StTabla, TranDel, LastCmd)
      End If
   Else
      Call MsgBox("No se pudo borrar la unidad de medida de la base de datos, debido a que no existe !!!...", vbExclamation, App.Title)
      Call MouseNorm
      Msglin NUL$
      Exit Sub
   End If
   If (Result <> FAIL) Then
      If (CommitTran() <> FAIL) Then
         Call MsgBox("El registro se borr� exitosamente.", vbInformation, App.Title)
         Call Limpiar
      Else
         Call RollBackTran
      End If
   Else
      Call RollBackTran
   End If
   Call MouseNorm
End Sub

'---------------------------------------------------------------------------------------
' Procedure : Grabar
' DateTime  : 30/09/2016 09:00
' Author    : jairo_parra
' Purpose   : JLPB T36875-R35965 Guarda la informaci�n en la BD
'---------------------------------------------------------------------------------------
'
Private Sub Grabar()
   Call MouseClock
   If (TxtCodigo = NUL$ Or TxtDescripcion = NUL$) Then
      Call MsgBox("Se debe especificar los datos completos de la unidad de medida!!!...", vbCritical, App.Title)
      Call MouseNorm: Exit Sub
   End If
   'JLPB T37259 INICIO
   If BoEncontro Then
      If MsgBox("Desea modificar la informaci�n?", vbYesNo, "Inventarios - Confirmaci�n") = vbNo Then Call MouseNorm: Exit Sub
   End If
   'JLPB T37259 FIN
   Valores = "TX_CODIGO_UMED=" & Comi & UCase(TxtCodigo) & Comi & Coma
   Valores = Valores & "TX_DESC_UMED=" & Comi & Cambiar_Comas_Comillas(TxtDescripcion) & Comi
   If (BeginTran(STranDel & StTabla) <> FAIL) Then
      Msglin "Guardando registro"
      If (Not BoEncontro) Then
         Result = DoInsertSQL(StTabla, Valores)
         If (Result <> FAIL) Then Result = Auditor(StTabla, TranIns, LastCmd)
      Else
         'If MsgBox("Desea modificar la informaci�n?", vbYesNo, "Inventarios - Confirmaci�n") = vbNo Then Call MouseNorm: Msglin NUL$: Exit Sub 'JLPB T37259 Se deja en comentario
         Condicion = "TX_CODIGO_UMED=" & Comi & TxtCodigo & Comi
         Result = DoUpdate(StTabla, Valores, Condicion)
         If (Result <> FAIL) Then Result = Auditor(StTabla, TranUpd, LastCmd)
      End If
   End If
   If (Result <> FAIL) Then
      If (CommitTran() <> FAIL) Then
         Call MsgBox("El registro se guard� exitosamente.", vbInformation, App.Title)
         Call Limpiar
      Else
         Call RollBackTran
      End If
   Else
      Call RollBackTran
   End If
   Call MouseNorm
End Sub

'---------------------------------------------------------------------------------------
' Procedure : Listar
' DateTime  : 30/09/2016 12:41
' Author    : jairo_parra
' Purpose   : JLPB T36875-R35965 Genera el informe de los datos
'---------------------------------------------------------------------------------------
'
Private Sub Listar()
   Call Limpiar_CrysListar
   With MDI_Inventarios
      .Crys_Listar.Formulas(0) = "ENTIDAD=" & Comi & Entidad & Comi
      .Crys_Listar.Formulas(1) = "NIT='Nit. " & Nit & Comi
      .Crys_Listar.Formulas(2) = "USUARIO='USUARIO: " & UserId & Comi
      .Crys_Listar.Formulas(3) = "TITULO='UNIDAD DE MEDIDA'"
   End With
   Call ListarD("InfUMedida.rpt", NUL$, crptToWindow, "UNIDAD DE MEDIDA", MDI_Inventarios, True)
End Sub
