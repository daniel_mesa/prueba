VERSION 5.00
Object = "{0BA686C6-F7D3-101A-993E-0000C0EF6F5E}#1.0#0"; "THREED32.OCX"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "Mscomctl.ocx"
Begin VB.Form FrmPlanosTarifa 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Plano tarifas"
   ClientHeight    =   2445
   ClientLeft      =   4185
   ClientTop       =   2970
   ClientWidth     =   4770
   Icon            =   "FrmPlTar.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   ScaleHeight     =   2445
   ScaleWidth      =   4770
   Begin MsComctlLib.ProgressBar PrgPlano 
      Height          =   255
      Left            =   120
      TabIndex        =   2
      Top             =   2160
      Width           =   4575
      _ExtentX        =   8070
      _ExtentY        =   450
      _Version        =   327682
      BorderStyle     =   1
      Appearance      =   1
   End
   Begin VB.Frame Frame2 
      Height          =   1095
      Left            =   120
      TabIndex        =   3
      Top             =   720
      Width           =   4575
      Begin Threed.SSCommand SCmd_Options 
         Height          =   735
         Index           =   0
         Left            =   240
         TabIndex        =   4
         Top             =   240
         Width           =   735
         _Version        =   65536
         _ExtentX        =   1296
         _ExtentY        =   1296
         _StockProps     =   78
         Caption         =   "&Descargar"
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Serif"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Picture         =   "FrmPlTar.frx":0442
      End
      Begin Threed.SSCommand SCmd_Options 
         Height          =   735
         Index           =   1
         Left            =   1320
         TabIndex        =   5
         Top             =   240
         Width           =   735
         _Version        =   65536
         _ExtentX        =   1296
         _ExtentY        =   1296
         _StockProps     =   78
         Caption         =   "&Cargar"
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Serif"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Picture         =   "FrmPlTar.frx":075C
      End
      Begin Threed.SSCommand SCmd_Options 
         Height          =   735
         Index           =   2
         Left            =   3480
         TabIndex        =   6
         Top             =   240
         Width           =   735
         _Version        =   65536
         _ExtentX        =   1296
         _ExtentY        =   1296
         _StockProps     =   78
         Caption         =   "&S                             "
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Serif"
            Size            =   6
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Picture         =   "FrmPlTar.frx":0A76
      End
      Begin Threed.SSCommand SCmd_Detener 
         Height          =   735
         Left            =   2400
         TabIndex        =   10
         Top             =   240
         Width           =   735
         _Version        =   65536
         _ExtentX        =   1296
         _ExtentY        =   1296
         _StockProps     =   78
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Serif"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Picture         =   "FrmPlTar.frx":157C
      End
   End
   Begin VB.ComboBox CboTarifaBase 
      Height          =   315
      Left            =   120
      TabIndex        =   0
      Top             =   360
      Width           =   4575
   End
   Begin VB.Frame Frame1 
      Caption         =   "Actualizar por"
      Height          =   495
      Left            =   1080
      TabIndex        =   7
      Top             =   1200
      Width           =   1815
      Begin VB.OptionButton OptActualizar 
         Caption         =   "C�digo alterno, c�digo principal"
         Height          =   375
         Index           =   1
         Left            =   120
         TabIndex        =   9
         Top             =   240
         Width           =   1575
      End
      Begin VB.OptionButton OptActualizar 
         Caption         =   "C�digo principal"
         Height          =   255
         Index           =   0
         Left            =   120
         TabIndex        =   8
         Top             =   240
         Value           =   -1  'True
         Width           =   1575
      End
   End
   Begin VB.Label Lbl_Errores 
      Caption         =   "0"
      Height          =   255
      Left            =   3720
      TabIndex        =   14
      Top             =   1920
      Width           =   855
   End
   Begin VB.Label Lbl_Procesados 
      Caption         =   "0"
      Height          =   255
      Left            =   1080
      TabIndex        =   13
      Top             =   1920
      Width           =   1815
   End
   Begin VB.Label Label2 
      Caption         =   "Errores:"
      Height          =   255
      Left            =   3120
      TabIndex        =   12
      Top             =   1920
      Width           =   615
   End
   Begin VB.Label Label1 
      Caption         =   "Procesados:"
      Height          =   255
      Left            =   120
      TabIndex        =   11
      Top             =   1920
      Width           =   975
   End
   Begin VB.Label lbl 
      Caption         =   "Tarifa base"
      Height          =   255
      Left            =   120
      TabIndex        =   1
      Top             =   120
      Width           =   855
   End
End
Attribute VB_Name = "FrmPlanosTarifa"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Dim texto As String
Dim NumReg As Long
Dim BandErr, BandStop As Boolean
Dim ArrTarifa(), CodTarifaBase() As Variant
Dim Bandera As String
Dim erroresCargue As Integer

Private Sub CboTarifaBase_Change()
   BandProcLote = 0
   BandStop = False
End Sub

Private Sub Form_Load()
Call CenterForm(MDI_Pacientes, Me)
Result = LoadCtrl("TARIFA_BASE order by NO_NOMB_TARB", "NO_NOMB_TARB", "CD_CODI_TARB", CboTarifaBase, CodTarifaBase(), NUL$)
BandProcLote = 0
BandStop = False
End Sub

Private Sub SCmd_Detener_Click()
If BandProcLote Then
   If WarnMsg("�Esta seguro que desea detener el proceso?") Then
      BandStop = True
   End If
End If
End Sub

Private Sub SCmd_Options_Click(Index As Integer)
If BandProcLote = 1 Then Exit Sub
Select Case Index
   Case 0: Call DescargarTarifa
   Case 1: Call CargarTarifas
   Case 2: Unload Me
   Case 3: Bandera = 1
End Select
End Sub

Sub CargarTarifas()
Dim limsup As Long
Dim liminf As Long
Dim Procesados As Integer
Dim Errores
   On Error GoTo control
   PrgPlano.Value = 0
   If CboTarifaBase.ListIndex = -1 Then
      Call Mensaje1("Debe seleccionar una tarifa", 3)
      Exit Sub
   End If
   If MDI_Pacientes.CMDialog1.InitDir = NUL$ Then MDI_Pacientes.CMDialog1.InitDir = DirDB
      MDI_Pacientes.CMDialog1.Filter = "(TODOS)|*"
      MDI_Pacientes.CMDialog1.Action = 1
      BandErr = False
      If MDI_Pacientes.CMDialog1.filename <> "" Then
         Result = DoDelete("ERRORES", NUL$)
         PrgPlano.Max = FileLen(MDI_Pacientes.CMDialog1.filename)
         Open MDI_Pacientes.CMDialog1.filename For Input As #1
         NumReg = 0
         BandProcLote = 1
         PrgPlano.Value = 57
         DoEvents
         Errores = 0
         BandStop = False
         Do While Not EOF(1)
            DoEvents
            If BandStop = True Then
               Exit Do
            End If
            PrgPlano.Value = PrgPlano.Value + 57
            Line Input #1, texto
            If Not ValidarTarifa Then
               Call IngresarTarifa
            Else
               Errores = Errores + 1
            End If
            On Error Resume Next
         Lbl_Procesados.Caption = Mid(texto, 1, 6) & " de " & CInt(PrgPlano.Max / 59)
         Lbl_Procesados.Refresh
         Lbl_Errores.Caption = Errores
         Lbl_Errores.Refresh
         Loop
         Close (1)
         BandProcLote = 0
         ReDim ARR(0)
         ARR(0) = "PACIENTE.MDB"
         Call Listar1("errores.rpt", NUL$, NUL$, ARR, 0, "Errores del archivo")
      End If
      PrgPlano.Value = 0
Exit Sub
control:
If err.Number <> 0 Then
   Call ConvertErr
   Close (1)
End If
Call MouseNorm
PrgPlano.Value = 0
End Sub

Sub DescargarTarifa()
Dim Linea As String
Dim Procesados As Integer
Dim Errores As Integer
Dim Total As Integer

   Bandera = 0
   PrgPlano.Value = 0
   If CboTarifaBase.ListIndex = -1 Then
      Call Mensaje1("Debe seleccionar una tarifa", 3)
      Exit Sub
   End If
   If MDI_Pacientes.CMDialog1.InitDir = NUL$ Then MDI_Pacientes.CMDialog1.InitDir = DirDB
   MDI_Pacientes.CMDialog1.Filter = "(TODOS)|*"
   MDI_Pacientes.CMDialog1.Action = 2
   BandErr = False
   If MDI_Pacientes.CMDialog1.filename <> "" Then
      On Error GoTo control
      Open MDI_Pacientes.CMDialog1.filename For Output As #1
      BandProcLote = 1
      Desde = "R_SER_TARB"
      Campos = "CD_CODI_SER_RST, NU_TILI_RST, VL_VALO_RST, CD_ALTE_RST"
      Condicion = "CD_CODI_TARB_RST='" & CodTarifaBase(CboTarifaBase.ListIndex) & Comi
      ReDim ARR(0)
      Result = LoadData(Desde, "COUNT(CD_CODI_SER_RST)", Condicion, ARR)
      If encontro Then Total = ARR(0)
      If (DoSelect(Desde, Campos, Condicion) <> FAIL) Then
         On Error GoTo control
         If (ResultQuery()) Then
            PrgPlano.Max = CLng(ARR(0))
            PrgPlano.Value = 0
            PrgPlano.min = 0
            BandStop = False
            Do While (NextRowQuery())
               Debug.Print BandProcLote
               DoEvents
               If BandStop = True Then
                  Exit Do
               End If
               Linea = CStr(PrgPlano.Value + 1) & String(6 - Len(CStr(PrgPlano.Value + 1)), " ")
               Linea = Linea & DataQuery(1) & String(13 - Len(DataQuery(1)), " ")
               Linea = Linea & DataQuery(3) & String(20 - Len(DataQuery(3)), " ")
               Linea = Linea & DataQuery(4) & String(16 - Len(DataQuery(4)), " ")
               Linea = Linea & ESP$ & DataQuery(2)
               Debug.Print Linea
               Print #1, Linea
               'If Bandera = 1 Then Close (1): Exit Sub
               Lbl_Procesados.Caption = PrgPlano.Value + 1 & " de " & Total
               Lbl_Procesados.Refresh
               PrgPlano.Value = PrgPlano.Value + 1
            Loop
            Close (1)
            BandProcLote = 0
            PrgPlano.Value = 0
         End If
      End If
   End If
Exit Sub
control:
If err.Number <> 0 Then
   Call ConvertErr
   Close (1)
End If
Call MouseNorm
PrgPlano.Value = 0

End Sub

Function ValidarTarifa() As Boolean
BandErr = False
Call CargarArr
If Len(texto) = 57 Then
   ReDim ARR(0)
   If OptActualizar(0) Then
      Result = LoadData("SERVICIOS", "CD_cODI_SER", "CD_cODI_SER = '" & ArrTarifa(1) & Comi, ARR)
      If Not encontro Then
         Call Error(CLng(NumReg), "Este codigo de procedimiento no esta registrado en la base de datos")
      End If
   End If
   If Not (ArrTarifa(2) = "0" Or ArrTarifa(2) = "1" Or ArrTarifa(2) = "2" Or ArrTarifa(2) = "3") Then
      Call Error(CLng(NumReg), "Este tipo de liquidaci�n no existe")
   End If
   If Not IsNumeric(ArrTarifa(3)) Then Call Error(CLng(NumReg), "El valor debe ser numerico")
Else
   Call Error(CLng(NumReg), "El tama�o del registro es incorrecto( deben ser 57 caracteres)")
End If
ValidarTarifa = BandErr
End Function

Sub CargarArr()
ReDim ArrTarifa(5)
   NumReg = NumReg + 1
   ArrTarifa(1) = Trim(Mid(texto, 7, 13))  'Codigo del procedimiento
   ArrTarifa(2) = Trim(Mid(texto, 57, 1)) 'Tipo de liquidacion
   ArrTarifa(3) = Trim(Mid(texto, 20, 20)) 'Valor
   ArrTarifa(4) = Trim(Mid(texto, 40, 16)) 'Codigo alterno
End Sub

Sub Error(Linea As Long, Txt As String)
   '"EL tama�o del registro es diferente a 214"
   Valores = "NU_LINE_ERRO=" & Linea & Coma
   Valores = Valores & "DE_DESC_ERRO='" & Txt & Comi
   Result = DoInsertSQL("ERRORES", Valores)
   BandErr = True
End Sub

Sub IngresarTarifa()
   On Error GoTo control
   Valores = "CD_CODI_TARB_RST='" & CodTarifaBase(CboTarifaBase.ListIndex) & Comi & Coma
   Valores = Valores & "CD_CODI_SER_RST='" & ArrTarifa(1) & Comi & Coma
   Valores = Valores & "NU_TILI_RST=" & ArrTarifa(2) & Coma
   Valores = Valores & "VL_VALO_RST=" & DecDouble(CStr(ArrTarifa(3))) & Coma
   Valores = Valores & "CD_ALTE_RST='" & ArrTarifa(4) & Comi
   
   ReDim ARR(0)
   If OptActualizar(0) Then
      Condicion = "CD_CODI_TARB_RST='" & CodTarifaBase(CboTarifaBase.ListIndex) & Comi
      Condicion = Condicion & " AND CD_CODI_SER_RST = '" & ArrTarifa(1) & Comi
      
      Result = LoadData("R_SER_TARB", "CD_CODI_SER_RST", Condicion, ARR)
      If encontro Then
         Result = DoUpdate("R_SER_TARB", Valores, Condicion)
      Else
         Result = DoInsertSQL("R_SER_TARB", Valores)
      End If
   Else
      If ArrTarifa(4) <> NUL$ Then
         Condicion = "CD_CODI_TARB_RST='" & CodTarifaBase(CboTarifaBase.ListIndex) & Comi
         Condicion = Condicion & " AND CD_ALTE_RST = '" & ArrTarifa(4) & Comi
         ReDim ARR(0)
         Result = LoadData("R_SER_TARB", "CD_CODI_SER_RST", Condicion, ARR)
         If encontro Then
            Valores = "CD_CODI_TARB_RST='" & CodTarifaBase(CboTarifaBase.ListIndex) & Comi & Coma
            Valores = Valores & "NU_TILI_RST=" & ArrTarifa(2) & Coma
            Valores = Valores & "VL_VALO_RST=" & DecDouble(CStr(ArrTarifa(3))) & Coma
            Valores = Valores & "CD_ALTE_RST='" & ArrTarifa(4) & Comi
            
            Result = DoUpdate("R_SER_TARB", Valores, Condicion)
         Else
            Condicion = "CD_CODI_TARB_RST='" & CodTarifaBase(CboTarifaBase.ListIndex) & Comi
            Condicion = Condicion & " AND CD_CODI_SER_RST = '" & ArrTarifa(1) & Comi
            
            Result = LoadData("R_SER_TARB", "CD_CODI_SER_RST", Condicion, ARR)
            If encontro Then
               Result = DoUpdate("R_SER_TARB", Valores, Condicion)
            Else
               Result = LoadData("SERVICIOS", "CD_CODI_SER", "CD_CODI_SER = '" & ArrTarifa(1) & Comi, ARR)
               If Not encontro Then
                  Call Error(CLng(NumReg), "Este codigo de procedimiento no esta registrado en la base de datos")
               Else
                  Result = DoInsertSQL("R_SER_TARB", Valores)
               End If
            End If
         End If
      End If
   End If
   If Result = FAIL Then
      Call Error(CLng(NumReg), ErrorProc)
   End If
Exit Sub
control:
Call Error(CLng(NumReg), ErrorProc)
End Sub
