Attribute VB_Name = "Contable"
Global arrMcuen() As String

Sub sbResumenMov() '|.DR.|, Genera un "documento de Excel" con el resumen de los movimientos.

'    Dim lgFilas As Long, dbResult As Double, stFile As String, xlsObject As Object
    Dim lgFilas As Long, xlsObject As Object    'DEPURACION DE CODIGO
'    Dim stOut As String    'DEPURACION DE CODIGO
    

    Set xlsObject = CreateObject("Excel.Application")
    xlsObject.Visible = True
    Set xlsObject = xlsObject.Workbooks.Add
    Set xlsObject = xlsObject.ActiveSheet

    With xlsObject
        .Range("A1:D1").Font.Bold = True
        .Range("A1") = "Descripci�n de la cuenta": .Range("B1") = "Cuenta": .Range("C1") = "D�bitos": .Range("D1") = "Cr�ditos"
        lgFilas = 1
        Do Until arrMcuen(0, lgFilas) = NUL$
                .Range("A" & lgFilas + 1) = arrMcuen(0, lgFilas)
                .Range("B" & lgFilas + 1) = arrMcuen(1, lgFilas)
                If arrMcuen(3, lgFilas) = "D" Then .Range("C" & lgFilas + 1) = Val(arrMcuen(2, lgFilas)) Else .Range("D" & lgFilas + 1) = Val(arrMcuen(2, lgFilas))
                lgFilas = lgFilas + 1
        Loop
        
                .Range("C" & lgFilas + 1).Formula = "=SUMA(C2:C" & lgFilas & ")": .Range("C" & lgFilas + 1).Font.Bold = True
                .Range("D" & lgFilas + 1).Formula = "=SUMA(D2:D" & lgFilas & ")": .Range("D" & lgFilas + 1).Font.Bold = True
                
                .Range("C2:D" & lgFilas + 1).NumberFormat = "$#,##0.00"
    End With
    
    
    

End Sub

'VALIDA QUE NO EXISTAN NIVELES SUPERIORES; TRUE, FALSO EN LAS CUENTAS CONTABLES
Function Valida_Nivel_Cuentas(cuenta As String) As Boolean
ReDim Arr(0, 0)
   
   'modificado ARA 05-01-2001 **************
   If Trim(cuenta) = NUL$ Then Valida_Nivel_Cuentas = False: Exit Function
   'se agrego esta linea****************
   
   If Len(cuenta) >= 6 Then
     Condicion = "CD_CODI_CUEN LIKE '" & Cambiar_Comas_Comillas(cuenta) & CaractLike & Comi
     Result = LoadMulData("Cuentas", "CD_CODI_CUEN", Condicion, Arr())
     If Result <> FAIL Then
        If UBound(Arr(), 2) > 0 Then
           Valida_Nivel_Cuentas = True
        Else
        '   If Arr(0, 0) = Cuenta Or Arr(0, 0) = NUL$ Then Valida_Nivel_Cuentas = False Else Valida_Nivel_Cuentas = True
           Valida_Nivel_Cuentas = False
        End If
     Else
        Valida_Nivel_Cuentas = True
     End If
   Else
     Valida_Nivel_Cuentas = True
   End If
End Function

Function fnBalance() As Double '|.DR.|
'Realiza un balance de prueba sobre el "arreglo" arrMcuen,
'  que es usado por Guardar_Movimiento_Contable, se usa el mismo m�todo de barrido.
'Si el valor retornado es diferente de 0, hay desbalance, un n�mero negativo indica
'  m�s cr�ditos que d�bitos y viceversa.

    Dim lgFilas As Long, dbResult As Double
    'DAHV M4133 INICIO ----------------------------------
    Dim InDec As Integer
       
    If Aplicacion.VerDecimales_Valores(InDec) Then
    Else
       InDec = 0
    End If
    'DAHV M4133 FIN -------------------------------------

    lgFilas = 1
'    Do Until arrMcuen(1, lgFilas) = NUL$
    Do Until arrMcuen(2, lgFilas) = NUL$    'REOL M1709
'        dbResult = Round(dbResult, 4) + (val(arrMcuen(2, lgFilas)) * IIf(arrMcuen(3, lgFilas) = "C", -1, 1))
        'dbResult = Aplicacion.Formatear_Valor(dbResult) + (Aplicacion.Formatear_Valor(Val(arrMcuen(2, lgFilas))) * IIf(arrMcuen(3, lgFilas) = "C", -1, 1))    'REOL M1709
        'dbResult = Aplicacion.Formatear_Valor(dbResult) + (Aplicacion.Formatear_Valor(arrMcuen(2, lgFilas)) * IIf(arrMcuen(3, lgFilas) = "C", -1, 1))    'NYCM M2769
        dbResult = Round(Round(dbResult, InDec) + ((Round(arrMcuen(2, lgFilas), InDec)) * IIf(arrMcuen(3, lgFilas) = "C", -1, 1)), InDec) 'DAHV 4133
        lgFilas = lgFilas + 1
    Loop

    fnBalance = dbResult

End Function



'00 Fin.
Function BuscarCargosSinFacCredito(GrupoArt As String, CentroCosto As String, TipoEmp As String) As Boolean
Desde = "MOVI_CARGOS, CONVENIOS, FORMULAS, ARTICULO"

Campos = "NU_NUME_MOVI"

Condicion = "NU_NUME_CONV_MOVI=NU_NUME_CONV AND NU_NUME_MOVI=NU_NUME_MOVI_FORM AND "
Condicion = Condicion & "CD_CODI_ELE_FORM=CD_CODI_ARTI AND CD_CODI_CECO_MOVI='" & CentroCosto & "' AND "
Condicion = Condicion & "NU_ESTA_MOVI<>2 AND NU_TIPO_MOVI=1 AND NU_NUME_FAC_MOVI=0 AND "
Condicion = Condicion & "CD_CODI_TIEMP_CONV='" & TipoEmp & "' AND "
Condicion = Condicion & "CD_CODI_UNF_FORM='" & CentroCosto & "' AND CD_GRUP_ARTI='" & GrupoArt & "'"

If (DoSelect(Desde, Campos, Condicion) <> FAIL) Then
   If (ResultQuery()) Then
      BuscarCargosSinFacCredito = True
   Else
      BuscarCargosSinFacCredito = False
   End If
Else
   BuscarCargosSinFacCredito = False
End If


End Function


Function Valida_Cierre_Contable(Mes As Double) As Boolean
ReDim Arr(0)

   'Condi = "CD_CUEN_SALD='ZZZZZZZZZZZZZZ' AND NU_MES_SALD=" & Mes
   Condi = "CD_CUEN_SALD='ZZZZZZZZZZZZZZZ' AND NU_MES_SALD=" & Mes 'natalia Req 1460
   ReDim ArrTemp(0)
   Result = LoadData("SALDOS", "VL_SAIN_SALD", Condi, Arr())
'   If Result <> FAIL And Encontro Then
   If Result <> FAIL Then
      If Encontro Then
         If Arr(0) = NUL$ Then Arr(0) = 0
         If CByte(Arr(0)) = 1 Then
             Valida_Cierre_Contable = True
             Call Mensaje1("El mes se encuentra cerrado en Contabilidad", 2)
         Else
             Valida_Cierre_Contable = False
         End If
      Else
            Valores = "'ZZZZZZZZZZZZZZZ', " & Mes & Coma & SCero & Coma & SCero & Coma & SCero
            Result = DoInsert("SALDOS", Valores)
            Valida_Cierre_Contable = False
      End If
      'Valida_Cierre_Contable = False
      'Call Mensaje1("Error validando el cierre en Contabilidad", 3)
   End If
End Function

' Crear el comprobante contable
' Orden de Argumentos :
' cuenta, tercero, fecha, comprobante, consecutivo, imputacion, concepto, valor, naturaleza, centro costo (dependencia), cheque, valor base, mes
Function Crear_Movimiento_Contable(Datos() As Variant) As Boolean
'ReDim aux(0)
'    Campos = "CD_CENT_MOVI=" & Comi & CentroC_Default & Comi
'    Campos = Campos & Coma & "CD_CUEN_MOVI=" & Comi & Datos(0) & Comi
'    Campos = Campos & Coma & "CD_TERC_MOVI=" & Comi & Datos(1) & Comi
'    'Campos = Campos & Coma & "FE_FECH_MOVI=" & "'" & CDate(CStr(Datos(2))) & "'"
'    Campos = Campos & Coma & "FE_FECH_MOVI=" & FFechaIns(CDate(CStr(Datos(2)))) 'NYCM M2709
'    Campos = Campos & Coma & "NU_COMP_MOVI=" & Comi & Datos(3) & Comi
'    Campos = Campos & Coma & "NU_CONS_MOVI=" & CLng(Datos(4))
'    Campos = Campos & Coma & "NU_IMPU_MOVI=" & CInt(Datos(5))
'    If Datos(7) = NUL$ Then Datos(7) = 0
'    Campos = Campos & Coma & "DE_DESC_MOVI=" & Comi & Cambiar_Comas_Comillas(CStr(Datos(6))) & Comi
'    Campos = Campos & Coma & "VL_VALO_MOVI=" & CDbl(Datos(7))
'    Campos = Campos & Coma & "ID_NATU_MOVI=" & Comi & Datos(8) & Comi
'    Campos = Campos & Coma & "CD_CECO_MOVI=" & Comi & Datos(9) & Comi
'    'SILVIA 21_08_03 SE AGREGO UNA VALIDACION IF PARA CUANDO LA VARIABLE DATOS(11)=NUL$
'    If Datos(11) = NUL$ Then Datos(11) = 0
'    Campos = Campos & Coma & "NU_CHEQ_MOVI=" & Comi & Datos(10) & Comi
'    Campos = Campos & Coma & "VL_REAL_MOVI=" & CDbl(Datos(11))
'    Campos = Campos & Coma & "NU_MES_MOVI=" & CByte(Datos(12))
'    Campos = Campos & Coma & "ID_GETR_MOVI=" & Comi & Datos(13) & Comi
'    'MARZO 22 - CAAP
'    If Datos(5) > 1 Then
'       aux(0) = NUL$
'    Else
'        Condicion = "NU_COMP_MOVI = " & Comi & Datos(3) & Comi
'        Condicion = Condicion & " AND NU_CONS_MOVI=" & Datos(4)
'        Condicion = Condicion & " AND NU_IMPU_MOVI=" & Datos(5)
'        Result = LoadData("MOVIMIENTOS", "CD_CUEN_MOVI", Condicion, aux())
'    End If
'    If aux(0) = NUL$ Then
'        Result = DoInsertSQL("MOVIMIENTOS", Campos)
'        If Result <> FAIL Then
'            Result = Auditor("MOVIMIENTOS", TranIns, LastCmd)
'            'ACUMULAR SALDOS
'            If (Result <> FAIL) Then Call Saldos(CStr(Datos(0)), CByte(Datos(12)), CDbl(Datos(7)), CStr(Datos(8)))
'            'CUENTA - COMPROBANTE
'            Campos = Mid(Datos(0), 1, 4) 'NIVEL 3
'            If (Result <> FAIL) Then Call Cuenta_Comprobante(Campos, CStr(Datos(3)), CByte(Datos(12)), CDbl(Datos(7)), CStr(Datos(8)))
'            Campos = Mid(Datos(0), 1, 6) 'NIVEL 4
'            If (Result <> FAIL) Then Call Cuenta_Comprobante(Campos, CStr(Datos(3)), CByte(Datos(12)), CDbl(Datos(7)), CStr(Datos(8)))
'            'CUENTA - TERCERO
'            If (Result <> FAIL) Then If Datos(1) <> NUL$ Then Call Cuenta_Tercero(CStr(Datos(0)), CStr(Datos(1)), CByte(Datos(12)), CDbl(Datos(7)), CStr(Datos(8)), CDbl(Datos(11)))
'            'CUENTA - CENTRO DE COSTO
'            If (Result <> FAIL) Then If Datos(9) <> NUL$ Then Call Cuenta_CentroCosto(CStr(Datos(0)), CStr(Datos(9)), CByte(Datos(12)), CDbl(Datos(7)), CStr(Datos(8)))
'            'CUENTA - CENTRO DE COSTO - TERCERO
'            If (Result <> FAIL) Then
'               If Datos(1) <> NUL$ And Datos(9) <> NUL$ Then Call Cuenta_CentroTercero(CStr(Datos(0)), CStr(Datos(9)), CStr(Datos(1)), CByte(Datos(12)), CDbl(Datos(7)), CStr(Datos(8)), CDbl(Datos(11)))
'            End If
'        End If
'        Crear_Movimiento_Contable = Result
'    Else
'        'imputacion 1
'        If Datos(5) = 1 Then Call Mensaje1("Ya se encuentra registrado en contabilidad", 2)
'        Crear_Movimiento_Contable = FAIL
'        Result = FAIL 'HRR M1468
'    End If
'NMSR R1934  Uso de SP para el movimiento contable
Dim ArrCta() As Variant 'NMSR R1934
Dim StNombreUsuario As String 'AASV R2171
'AASV R2171
ReDim Arr(0)
  'If Result <> FAIL And Encontro Then
  If Result <> FAIL Then 'AASV M4287
      'Result = LoadData(" USUARIO_IN", "NO_NOMB_USUA", "CD_CODI_USUA = '" & UserId & Comi, Arr)
      Result = LoadData(" USUARIO", "TX_DESC_USUA", "TX_IDEN_USUA = '" & UserId & Comi, Arr) 'AASV M4206
        If Encontro Then StNombreUsuario = Arr(0) 'Nombre Usuario
  End If
'AASV R2171
If MotorBD <> "SQL" Then
ReDim aux(0)

    'NMSR R1934
    ReDim ArrCta(0)
    Result = LoadData("CUENTAS", "CD_CODI_CUEN", "CD_CODI_CUEN=" & Comi & Datos(0) & Comi, ArrCta)
    If Result <> FAIL And Not Encontro Then
        Result = FAIL
        Crear_Movimiento_Contable = FAIL
        Call Mensaje1("La cuenta: " & Datos(0) & " no existe en la tabla Cuentas", 1)
        Exit Function
    End If
    'NMSR R1934
    Campos = "CD_CENT_MOVI=" & Comi & CentroC_Default & Comi
    Campos = Campos & Coma & "CD_CUEN_MOVI=" & Comi & Datos(0) & Comi
    Campos = Campos & Coma & "CD_TERC_MOVI=" & Comi & Datos(1) & Comi
    'Campos = Campos & Coma & "FE_FECH_MOVI=" & "'" & CDate(CStr(Datos(2))) & "'"
    Campos = Campos & Coma & "FE_FECH_MOVI=" & FFechaIns(CDate(CStr(Datos(2)))) 'NYCM M2709
    Campos = Campos & Coma & "NU_COMP_MOVI=" & Comi & Datos(3) & Comi
    Campos = Campos & Coma & "NU_CONS_MOVI=" & CLng(Datos(4))
    Campos = Campos & Coma & "NU_IMPU_MOVI=" & CInt(Datos(5))
    If Datos(7) = NUL$ Then Datos(7) = 0
    Campos = Campos & Coma & "DE_DESC_MOVI=" & Comi & Cambiar_Comas_Comillas(CStr(Datos(6))) & Comi
    Campos = Campos & Coma & "VL_VALO_MOVI=" & CDbl(Datos(7))
    Campos = Campos & Coma & "ID_NATU_MOVI=" & Comi & Datos(8) & Comi
    Campos = Campos & Coma & "CD_CECO_MOVI=" & Comi & Datos(9) & Comi
    'SILVIA 21_08_03 SE AGREGO UNA VALIDACION IF PARA CUANDO LA VARIABLE DATOS(11)=NUL$
    If Datos(11) = NUL$ Then Datos(11) = 0
    Campos = Campos & Coma & "NU_CHEQ_MOVI=" & Comi & Datos(10) & Comi
    Campos = Campos & Coma & "VL_REAL_MOVI=" & CDbl(Datos(11))
    Campos = Campos & Coma & "NU_MES_MOVI=" & CByte(Datos(12))
    Campos = Campos & Coma & "ID_GETR_MOVI=" & Comi & Datos(13) & Comi
    Campos = Campos & Coma & "TX_USER_MOVI=" & Comi & Cambiar_Comas_Comillas(StNombreUsuario) & Comi 'AASV R2171
    'MARZO 22 - CAAP
    If Datos(5) > 1 Then
       aux(0) = NUL$
    Else
        Condicion = "NU_COMP_MOVI = " & Comi & Datos(3) & Comi
        Condicion = Condicion & " AND NU_CONS_MOVI=" & Datos(4)
        Condicion = Condicion & " AND NU_IMPU_MOVI=" & Datos(5)
        Result = LoadData("MOVIMIENTOS", "CD_CUEN_MOVI", Condicion, aux())
    End If
    If aux(0) = NUL$ Then
        Result = DoInsertSQL("MOVIMIENTOS", Campos)
        If Result <> FAIL Then
            Result = Auditor("MOVIMIENTOS", TranIns, LastCmd)
            'ACUMULAR SALDOS
            If (Result <> FAIL) Then Call Saldos(CStr(Datos(0)), CByte(Datos(12)), CDbl(Datos(7)), CStr(Datos(8)))
            'CUENTA - COMPROBANTE
            Campos = Mid(Datos(0), 1, 4) 'NIVEL 3
            If (Result <> FAIL) Then Call Cuenta_Comprobante(Campos, CStr(Datos(3)), CByte(Datos(12)), CDbl(Datos(7)), CStr(Datos(8)))
            Campos = Mid(Datos(0), 1, 6) 'NIVEL 4
            If (Result <> FAIL) Then Call Cuenta_Comprobante(Campos, CStr(Datos(3)), CByte(Datos(12)), CDbl(Datos(7)), CStr(Datos(8)))
            'CUENTA - TERCERO
            If (Result <> FAIL) Then If Datos(1) <> NUL$ Then Call Cuenta_Tercero(CStr(Datos(0)), CStr(Datos(1)), CByte(Datos(12)), CDbl(Datos(7)), CStr(Datos(8)), CDbl(Datos(11)))
            'CUENTA - CENTRO DE COSTO
            If (Result <> FAIL) Then If Datos(9) <> NUL$ Then Call Cuenta_CentroCosto(CStr(Datos(0)), CStr(Datos(9)), CByte(Datos(12)), CDbl(Datos(7)), CStr(Datos(8)))
            'CUENTA - CENTRO DE COSTO - TERCERO
            If (Result <> FAIL) Then
               If Datos(1) <> NUL$ And Datos(9) <> NUL$ Then Call Cuenta_CentroTercero(CStr(Datos(0)), CStr(Datos(9)), CStr(Datos(1)), CByte(Datos(12)), CDbl(Datos(7)), CStr(Datos(8)), CDbl(Datos(11)))
            End If
        End If
        Crear_Movimiento_Contable = Result
    Else
        'imputacion 1
        If Datos(5) = 1 Then Call Mensaje1("Ya se encuentra registrado en contabilidad", 2)
        Crear_Movimiento_Contable = FAIL
        Result = FAIL 'HRR M1468
    End If
 ElseIf MotorBD = "SQL" Then
        
      Dim Cmd As New ADODB.Command
      Call CreaParametrosSP(Cmd, "CD_CENT_MOVI", adVarChar, "ZZZZZZZZZZZ", adParamInput, 11)
      Call CreaParametrosSP(Cmd, "CD_CUEN_MOVI", adVarChar, Datos(0), adParamInput, 15)
      Call CreaParametrosSP(Cmd, "CD_TERC_MOVI", adVarChar, Datos(1), adParamInput, 20)  'tercero
      'Call CreaParametrosSP(Cmd, "FE_FECH_MOVI", adDate, Format(Nowserver, "dd/mm/yyyy hh:mm:ss"), adParamInput, 19) 'HRR M3193
      Call CreaParametrosSP(Cmd, "FE_FECH_MOVI", adDate, Format(CStr(Datos(2)), "dd/mm/yyyy hh:mm:ss"), adParamInput, 19) 'HRR M3193
      Call CreaParametrosSP(Cmd, "NU_COMP_MOVI", adVarChar, Datos(3), adParamInput, 3)
      Call CreaParametrosSP(Cmd, "NU_CONS_MOVI", adNumeric, Datos(4), adParamInput, 20, 11)
      Call CreaParametrosSP(Cmd, "NU_IMPU_MOVI", adNumeric, Datos(5), adParamInput, 20, 5)
      Call CreaParametrosSP(Cmd, "DE_DESC_MOVI", adVarChar, Cambiar_Comas_Comillas(CStr(Datos(6))), adParamInput, 250)
      Call CreaParametrosSP(Cmd, "VL_VALO_MOVI", adNumeric, Datos(7), adParamInput, 20, 28, 2)
      Call CreaParametrosSP(Cmd, "ID_NATU_MOVI", adVarChar, Datos(8), adParamInput, 1)
      Call CreaParametrosSP(Cmd, "CD_CECO_MOVI", adVarChar, Datos(9), adParamInput, 11)
      Call CreaParametrosSP(Cmd, "NU_CHEQ_MOVI", adVarChar, Datos(10), adParamInput, 10)
      If Datos(11) = NUL$ Then Datos(11) = 0
      Call CreaParametrosSP(Cmd, "VL_REAL_MOVI", adNumeric, Datos(11), adParamInput, 20, 28, 2)
      Call CreaParametrosSP(Cmd, "NU_MES_MOVI", adNumeric, Datos(12), adParamInput, 2, 3)
      Call CreaParametrosSP(Cmd, "ID_GETR_MOVI", adVarChar, Datos(13), adParamInput, 1)
      Call CreaParametrosSP(Cmd, "TX_USER_MOVI", adVarChar, StNombreUsuario, adParamInput, 50) 'AASV R2171
      'INICIO LDCR 2da PARTE R9033/T9930
      Call CreaParametrosSP(Cmd, "TIPO_IVA", adInteger, Datos(14), adParamInput)
      Call CreaParametrosSP(Cmd, "VL_VALIVA", adNumeric, Datos(15), adParamInput, 20, 28, 2)
      Call CreaParametrosSP(Cmd, "VL_VALBIVA", adNumeric, Datos(16), adParamInput, 20, 28, 2)
      'FIN LDCR 2da PARTE R9033/T9930
      Call CreaParametrosSP(Cmd, "Resultado", adInteger, 0, adParamOutput)
      Call CreaParametrosSP(Cmd, "TIPO_EMPRE", adInteger, IIf(BoTipoEmpre = True, 1, 0), adParamInput) 'JACC R2176 OE
      'SKRV T24261/R22366 INICIO
      Call CreaParametrosSP(Cmd, "NU_TIPMOV_MOVI", adVarChar, Datos(17), adParamInput, 10)
      Call CreaParametrosSP(Cmd, "NU_MOV_MOVI", adInteger, Datos(18), adParamInput)
      'SKRV T24261/R22366 FIN
        
      Cmd.ActiveConnection = BD(BDCurCon)
      Cmd.CommandType = adCmdStoredProc
      Cmd.CommandText = "PA_Crear_Movimiento_Contable "
      On Error GoTo Error
      Cmd.Execute
      Result = Cmd.Parameters.Item("@Resultado").Value

      Set Cmd = Nothing
      Crear_Movimiento_Contable = Result
      Exit Function
Error:
    Call ConvertErr
    FrmCmd.TxtCmd.Text = "Para ver la ultima Sentencia Remitase al Servidor : Visor de Sucesos De Windows" 'AASV M3366
    Set Cmd = Nothing
    Result = FAIL
    Crear_Movimiento_Contable = FAIL
  End If
'NMSR R1934
End Function
'todos los niveles
Sub Saldos(ByVal cuenta As String, Mes As Byte, valor As Double, Naturaleza As String)
Dim I As Byte
Dim ArrCta() As Variant 'NMSR R1934

    I = Nivel_Cuenta(cuenta)
    Do While I > 0 And cuenta <> NUL$
       If BoTipoEmpre Then ''JACC R2176 OE
            Select Case I
                Case 1: cuenta = Mid(cuenta, 1, 1)
                Case 2: cuenta = Mid(cuenta, 1, 2)
                Case 3: cuenta = Mid(cuenta, 1, 4)
                Case 4: cuenta = Mid(cuenta, 1, 6)
                Case 5: cuenta = Mid(cuenta, 1, 8)
                Case 6: cuenta = Mid(cuenta, 1, 10)
                Case 7: cuenta = Mid(cuenta, 1, 12)
            End Select
       Else 'JACC R2176 OE
            Select Case I
                Case 1: cuenta = Mid(cuenta, 1, 1)
                Case 2: cuenta = Mid(cuenta, 1, 2)
                Case 3: cuenta = Mid(cuenta, 1, 4)
                Case 4: cuenta = Mid(cuenta, 1, 6)
                Case 5: cuenta = Mid(cuenta, 1, 9)
                Case 6: cuenta = Mid(cuenta, 1, 12)
                'Case 7: cuenta = Mid(cuenta, 1, 14)
                Case 7: cuenta = Mid(cuenta, 1, 15) 'natalia Req 1460
            End Select
        End If
        'NMSR R1934
        ReDim ArrCta(0)
        Result = LoadData("CUENTAS", "CD_CODI_CUEN", "CD_CODI_CUEN=" & Comi & cuenta & Comi, ArrCta)
        If Result <> FAIL And Not Encontro Then
            Result = FAIL
            Call Mensaje1("La cuenta: " & cuenta & " no existe en la tabla Cuentas", 1)
            Exit Sub
        End If
        'NMSR R1934
       Call Actualiza_Cuentas(cuenta, Mes, valor, Naturaleza)
       I = I - 1
    Loop
End Sub
Sub Actualiza_Cuentas(ByVal cuenta As String, Mes As Byte, valor As Double, Naturaleza As String)
Dim Arr(0)
    If valor <> 0 Then valor = Format(valor, "####0.#0")  'JACC DEPURACION NOV 2008
    Condicion = "CD_CUEN_SALD = '" & cuenta & Comi & " AND NU_MES_SALD = " & Mes
    Result = LoadData("SALDOS", "CD_CUEN_SALD", Condicion, Arr())
    If Arr(0) = NUL$ Then
       Campos = "CD_CUEN_SALD=" & Comi & cuenta & Comi & Coma & "NU_MES_SALD=" & Mes & Coma & "VL_SAIN_SALD=" & SCero & Coma & _
                "VL_CRED_SALD=" & IIf(Naturaleza = "C", valor, 0) & Coma & "VL_DEBI_SALD=" & IIf(Naturaleza = "D", valor, 0)
       Result = DoInsertSQL("SALDOS", Campos)
    Else
       If Naturaleza = "D" Then
          Campos = "VL_DEBI_SALD = VL_DEBI_SALD + " & valor
       Else
          Campos = "VL_CRED_SALD = VL_CRED_SALD + " & valor
       End If
       Result = DoUpdate("SALDOS", Campos, Condicion)
    End If
End Sub
Sub Cuenta_Comprobante(cuenta As String, Comprobante As String, Mes As Byte, valor As Double, Naturaleza As String)
Dim Arr(0)
Dim ArrCta() As Variant 'NMSR R1934
    If valor <> 0 Then valor = Format(valor, "#####0.#0") 'JACC Depuracion Nov 2008

    'NMSR R1934
    ReDim ArrCta(0)
    Result = LoadData("CUENTAS", "CD_CODI_CUEN", "CD_CODI_CUEN=" & Comi & cuenta & Comi, ArrCta)
    If Result <> FAIL And Not Encontro Then
        Result = FAIL
        Call Mensaje1("La cuenta: " & cuenta & " no existe en la tabla Cuentas", 1)
        Exit Sub
    End If
    ReDim ArrCta(0)
    Result = LoadData("TC_COMPROBANTE", "CD_CODI_COMP", "CD_CODI_COMP=" & Comi & Comprobante & Comi, ArrCta)
    If Result <> FAIL And Not Encontro Then
        Result = FAIL
        Call Mensaje1("El Comprobante: " & Comprobante & " no existe en la tabla TC_COMPROBANTE", 1)
        Exit Sub
    End If 'NMSR R1934
    Condicion = "CD_CUEN_RCOC = '" & cuenta & Comi & " AND CD_COMP_RCOC = '" & Comprobante & Comi & " AND NU_MES_RCOC = " & Mes
    Result = LoadData("CUENTA_COMPROB", "CD_CUEN_RCOC", Condicion, Arr())
    If Arr(0) = NUL$ Then
       Campos = "CD_CUEN_RCOC=" & Comi & cuenta & Comi & Coma & "CD_COMP_RCOC=" & Comi & Comprobante & Comi & Coma & "NU_MES_RCOC=" & Mes & Coma & _
                "VL_DEBI_RCOC=" & IIf(Naturaleza = "D", valor, 0) & Coma & "VL_CRED_RCOC=" & IIf(Naturaleza = "C", valor, 0)
       Result = DoInsertSQL("CUENTA_COMPROB", Campos)
    Else
       If Naturaleza = "D" Then
          Campos = "VL_DEBI_RCOC = VL_DEBI_RCOC + " & valor
       Else
          Campos = "VL_CRED_RCOC = VL_CRED_RCOC + " & valor
       End If
       Result = DoUpdate("CUENTA_COMPROB", Campos, Condicion)
    End If
End Sub
Sub Cuenta_Tercero(cuenta As String, Tercero As String, Mes As Byte, valor As Double, Naturaleza As String, ByVal Base As Double)
Dim Arr(0)
Dim ArrCta() As Variant 'NMSR R1934
    If valor <> 0 Then valor = Format(valor, "####0.#0") 'JACC Depuracion Nov 2008
    If Base <> 0 Then Base = Format(Base, "####0.#0")    'JACC Depuracion Nov 2008
    
    'NMSR R1934
    ReDim ArrCta(0)
    Result = LoadData("CUENTAS", "CD_CODI_CUEN", "CD_CODI_CUEN=" & Comi & cuenta & Comi, ArrCta)
    If Result <> FAIL And Not Encontro Then
        Result = FAIL
        Call Mensaje1("La cuenta: " & cuenta & " no existe en la tabla Cuentas", 1)
        Exit Sub
    End If
    ReDim ArrCta(0)
    Result = LoadData("TERCERO", "CD_CODI_TERC", "CD_CODI_TERC=" & Comi & Tercero & Comi, ArrCta)
    If Result <> FAIL And Not Encontro Then
        Result = FAIL
        Call Mensaje1("El Tercero: " & cuenta & " no existe en la tabla TERCERO", 1)
        Exit Sub
    End If
    'NMSR R1934
    Condicion = "CD_CUEN_RCT = '" & cuenta & Comi & " AND CD_TERC_RCT = '" & Tercero & Comi & " AND NU_MES_RCT = " & Mes
    Result = LoadData("CUENTA_TERCERO", "CD_CUEN_RCT", Condicion, Arr())
    If Arr(0) = NUL$ Then
       Campos = "CD_CUEN_RCT = " & Comi & cuenta & Comi & Coma & "CD_TERC_RCT= " & Comi & Tercero & Comi & Coma & "NU_MES_RCT= " & Mes & Coma & _
                "VL_DEBI_RCT = " & IIf(Naturaleza = "D", valor, 0) & Coma & "VL_CRED_RCT = " & IIf(Naturaleza = "C", valor, 0) & Coma & _
                "VL_BADB_RCT = " & IIf(Naturaleza = "D", Base, 0) & Coma & "VL_BACR_RCT = " & IIf(Naturaleza = "C", Base, 0)
       'JLPB T23495 INICIO LA SIGUIENTE LINEA SE DEJA EN COMENTARIO
       'Campos = Campos & ",0,0,0,0,0,0,0,0" 'LDCR T10675
       Campos = Campos & ",VL_CREDIVA_RCT=0,VL_DEBIIVA_RCT=0,VL_CREDNIVA_RCT=0,VL_DEBINIVA_RCT=0"
       Campos = Campos & ",VL_CREDBASE_RCT=0,VL_DEBIBASE_RCT=0,VL_CREDNBASE_RCT=0,VL_DEBINBASE_RCT=0"
       'JLPB T23495 FIN
       Result = DoInsertSQL("CUENTA_TERCERO", Campos)
    Else
        If Naturaleza = "D" Then
          Campos = "VL_DEBI_RCT = VL_DEBI_RCT + " & valor & Coma & _
                   "VL_BADB_RCT = VL_BADB_RCT + " & Base
       Else
          Campos = "VL_CRED_RCT = VL_CRED_RCT + " & valor & Coma & _
                   "VL_BACR_RCT = VL_BACR_RCT + " & Base
       End If
       Result = DoUpdate("CUENTA_TERCERO", Campos, Condicion)
    End If
End Sub
Sub Cuenta_CentroCosto(cuenta As String, Centro As String, Mes As Byte, valor As Double, Naturaleza As String)
Dim Arr(0)
Dim ArrCta() As Variant 'NMSR R1934
    If valor <> 0 Then valor = Format(valor, "####0.#0") 'JACC Depuracion Nov  2008
    'NMSR R1934
    ReDim ArrCta(0)
    Result = LoadData("CUENTAS", "CD_CODI_CUEN", "CD_CODI_CUEN=" & Comi & cuenta & Comi, ArrCta)
    If Result <> FAIL And Not Encontro Then
        Result = FAIL
        Call Mensaje1("La cuenta: " & cuenta & " no existe en la tabla Cuentas", 1)
        Exit Sub
    End If
    ReDim ArrCta(0)
    Result = LoadData("CENTRO_COSTO", "CD_CODI_CECO", "CD_CODI_CECO=" & Comi & Centro & Comi, ArrCta)
    If Result <> FAIL And Not Encontro Then
        Result = FAIL
        Call Mensaje1("El Centro de Costos: " & cuenta & " no existe en la tabla CENTRO_COSTO", 1)
        Exit Sub
    End If 'NMSR R1934
    Condicion = "CD_CUEN_RCC = '" & cuenta & Comi & " AND CD_CECO_RCC = '" & Centro & Comi & " AND NU_MES_RCC = " & Mes
    Result = LoadData("CUENTA_CECO", "CD_CUEN_RCC", Condicion, Arr())
    If Arr(0) = NUL$ Then
       Campos = "CD_CUEN_RCC=" & Comi & cuenta & Comi & Coma & "CD_CECO_RCC=" & Comi & Centro & Comi & Coma & "NU_MES_RCC=" & Mes & Coma & "VL_SAIN_RCC=" & SCero & Coma & _
                "VL_CRED_RCC=" & IIf(Naturaleza = "C", valor, 0) & Coma & "VL_DEBI_RCC=" & IIf(Naturaleza = "D", valor, 0)
       Result = DoInsertSQL("CUENTA_CECO", Campos)
    Else
        If Naturaleza = "D" Then
          Campos = "VL_DEBI_RCC = VL_DEBI_RCC + " & valor
       Else
          Campos = "VL_CRED_RCC = VL_CRED_RCC + " & valor
       End If
       Result = DoUpdate("CUENTA_CECO", Campos, Condicion)
    End If
End Sub
Sub Cuenta_CentroTercero(cuenta As String, Centro As String, Tercero As String, Mes As Byte, valor As Double, Naturaleza As String, ByVal Base As Double)
Dim Arr(0)
Dim ArrCta() As Variant 'NMSR R1934
    If valor <> 0 Then valor = Format(valor, "######0.#0") 'JACC Depuracion Nov 2008
    'NMSR R1934
    ReDim ArrCta(0)
    Result = LoadData("CUENTAS", "CD_CODI_CUEN", "CD_CODI_CUEN=" & Comi & cuenta & Comi, ArrCta)
    If Result <> FAIL And Not Encontro Then
        Result = FAIL
        Call Mensaje1("La cuenta: " & cuenta & " no existe en la tabla Cuentas", 1)
        Exit Sub
    End If
    ReDim ArrCta(0)
    Result = LoadData("CENTRO_COSTO", "CD_CODI_CECO", "CD_CODI_CECO=" & Comi & Centro & Comi, ArrCta)
    If Result <> FAIL And Not Encontro Then
        Result = FAIL
        Call Mensaje1("El Centro de Costos: " & cuenta & " no existe en la tabla CENTRO_COSTO", 1)
        Exit Sub
    End If
    ReDim ArrCta(0)
    Result = LoadData("TERCERO", "CD_CODI_TERC", "CD_CODI_TERC=" & Comi & Tercero & Comi, ArrCta)
    If Result <> FAIL And Not Encontro Then
        Result = FAIL
        Call Mensaje1("El Tercero: " & cuenta & " no existe en la tabla TERCERO", 1)
        Exit Sub
    End If
    'NMSR R1934
    Condicion = " CD_CUEN_RCCT = '" & cuenta & Comi & " AND CD_CECO_RCCT = '" & Centro & Comi & _
                " AND CD_TERC_RCCT = '" & Tercero & Comi & " AND NU_MES_RCCT = " & Mes
    Result = LoadData("CUENTA_CETE", "CD_CUEN_RCCT", Condicion, Arr())
    If Arr(0) = NUL$ Then
       Campos = "CD_CUEN_RCCT = " & Comi & cuenta & Comi & Coma & "CD_CECO_RCCT= " & Comi & Centro & Comi & Coma & _
                "CD_TERC_RCCT= " & Comi & Tercero & Comi & Coma & "NU_MES_RCCT = " & Mes & Coma & _
                "VL_DEBI_RCCT= " & IIf(Naturaleza = "D", valor, 0) & Coma & "VL_CRED_RCCT= " & IIf(Naturaleza = "C", valor, 0) & Coma & _
                "VL_BADB_RCCT= " & IIf(Naturaleza = "D", Base, 0) & Coma & "VL_BACR_RCCT= " & IIf(Naturaleza = "C", Base, 0)
       Result = DoInsertSQL("CUENTA_CETE", Campos)
    Else
        If Naturaleza = "D" Then
          Campos = "VL_DEBI_RCCT = VL_DEBI_RCCT + " & valor & Coma & _
                   "VL_BADB_RCCT = VL_BADB_RCCT + " & Base
       Else
          Campos = "VL_CRED_RCCT = VL_CRED_RCCT + " & valor & Coma & _
                   "VL_BACR_RCCT = VL_BACR_RCCT + " & Base
       End If
       Result = DoUpdate("CUENTA_CETE", Campos, Condicion)
    End If
End Sub
'funciones para cargar los saldos iniciales
Function Saldo_Cuenta(ByVal cuenta As String, valor As Double) As Integer
Dim I As Byte

    I = Nivel_Cuenta(cuenta)
    Do While I > 0 And cuenta <> NUL$
       If BoTipoEmpre Then ''JACC R2176 OE
            Select Case I
                Case 1: cuenta = Mid(cuenta, 1, 1)
                Case 2: cuenta = Mid(cuenta, 1, 2)
                Case 3: cuenta = Mid(cuenta, 1, 4)
                Case 4: cuenta = Mid(cuenta, 1, 6)
                Case 5: cuenta = Mid(cuenta, 1, 8)
                Case 6: cuenta = Mid(cuenta, 1, 10)
                Case 7: cuenta = Mid(cuenta, 1, 12)
            End Select
       Else 'JACC R2176 OE
            Select Case I
                Case 1: cuenta = Mid(cuenta, 1, 1)
                Case 2: cuenta = Mid(cuenta, 1, 2)
                Case 3: cuenta = Mid(cuenta, 1, 4)
                Case 4: cuenta = Mid(cuenta, 1, 6)
                Case 5: cuenta = Mid(cuenta, 1, 9)
                Case 6: cuenta = Mid(cuenta, 1, 12)
                'Case 7: cuenta = Mid(cuenta, 1, 14)
                Case 7: cuenta = Mid(cuenta, 1, 15) 'natalia Req 1460
            End Select
       End If
       Result = Actualiza_Saldo_Cuenta(cuenta, valor)
       If Result = FAIL Then Exit Do
       I = I - 1
    Loop
    Saldo_Cuenta = Result
    
End Function
Function Actualiza_Saldo_Cuenta(ByVal cuenta As String, valor As Double) As Integer

    Condicion = "CD_CODI_CUEN = '" & cuenta & Comi
    Campos = "VL_SALD_CUEN = VL_SALD_CUEN + " & valor
    Result = DoUpdate("CUENTAS", Campos, Condicion)
    Actualiza_Saldo_Cuenta = Result
    
End Function
Function Saldo_CentroC(ByVal cuenta As String, Centro As String, valor As Double) As Integer
Dim I As Byte

    I = Nivel_Cuenta(cuenta)
    Do While I > 0 And cuenta <> NUL$
       If BoTipoEmpre Then ''JACC R2176 OE
            Select Case I
                Case 1: cuenta = Mid(cuenta, 1, 1)
                Case 2: cuenta = Mid(cuenta, 1, 2)
                Case 3: cuenta = Mid(cuenta, 1, 4)
                Case 4: cuenta = Mid(cuenta, 1, 6)
                Case 5: cuenta = Mid(cuenta, 1, 8)
                Case 6: cuenta = Mid(cuenta, 1, 10)
                Case 7: cuenta = Mid(cuenta, 1, 12)
            End Select
       Else 'JACC R2176 OE
            Select Case I
                Case 1: cuenta = Mid(cuenta, 1, 1)
                Case 2: cuenta = Mid(cuenta, 1, 2)
                Case 3: cuenta = Mid(cuenta, 1, 4)
                Case 4: cuenta = Mid(cuenta, 1, 6)
                Case 5: cuenta = Mid(cuenta, 1, 9)
                Case 6: cuenta = Mid(cuenta, 1, 12)
                'Case 7: cuenta = Mid(cuenta, 1, 14)
                Case 7: cuenta = Mid(cuenta, 1, 15) 'natalia Req 1460
            End Select
        End If
       Result = Actualiza_Saldo_Ceco(cuenta, Centro, valor)
       If Result = FAIL Then Exit Do
       I = I - 1
    Loop
    Saldo_CentroC = Result
    
End Function
Function Actualiza_Saldo_Ceco(ByVal cuenta As String, Centro As String, valor As Double) As Integer
Dim Arr(0)
    If valor <> 0 Then valor = Format(valor, "####0.#0")  'JACC DEPURACION NOV 2008
    Condicion = " CD_CUEN_SALC = '" & cuenta & Comi & _
                " AND CD_CECO_SALC = '" & Centro & Comi
    Result = LoadData("SALDO_CECO", "CD_CUEN_SALC", Condicion, Arr())
    
    If Arr(0) = NUL$ Then
       Campos = "CD_CUEN_SALC=" & Comi & cuenta & Comi & Coma & "CD_CECO_SALC =" & Comi & Centro & Comi & Coma & "VL_SALD_SALC=" & valor
       Result = DoInsertSQL("SALDO_CECO", Campos)
    Else
        Campos = "VL_SALD_SALC = VL_SALD_SALC + " & valor
        Result = DoUpdate("SALDO_CECO", Campos, Condicion)
    End If

    Actualiza_Saldo_Ceco = Result
    
End Function
Function Actualiza_Saldo_Tercero(ByVal cuenta As String, Tercero As String, valor As Double) As Integer
Dim Arr(0)
    If valor <> 0 Then valor = Format(valor, "####0.#0")  'JACC DEPURACION NOV 2008
    Condicion = " CD_CUEN_SALT = '" & cuenta & Comi & _
                " AND CD_TERC_SALT = '" & Tercero & Comi
    Result = LoadData("SALDO_TERCERO", "CD_CUEN_SALT", Condicion, Arr())
    
    If Arr(0) = NUL$ Then
       Campos = Comi & cuenta & Comi & Coma & Comi & Tercero & Comi & Coma & valor
       Result = DoInsert("SALDO_TERCERO", Campos)
    Else
        Campos = "VL_SALD_SALT = VL_SALD_SALT + " & valor
        Result = DoUpdate("SALDO_TERCERO", Campos, Condicion)
    End If

    Actualiza_Saldo_Tercero = Result

End Function
Function Actualiza_Saldo_CeTe(ByVal cuenta As String, Centro As String, Tercero As String, valor As Double) As Integer
Dim Arr(0)
    If valor <> 0 Then valor = Format(valor, "####0.#0") 'JACC DEPURACION NOV 2008
    Condicion = " CD_CUEN_SACT = '" & cuenta & Comi & _
                " AND CD_CECO_SACT = '" & Centro & Comi & _
                " AND CD_TERC_SACT = '" & Tercero & Comi
    Result = LoadData("SALDO_CETE", "CD_CUEN_SACT", Condicion, Arr())
    
    If Arr(0) = NUL$ Then
       Campos = Comi & cuenta & Comi & Coma & Comi & Centro & Comi & Coma & _
                Comi & Tercero & Comi & Coma & valor
       Result = DoInsert("SALDO_CETE", Campos)
    Else
        Campos = "VL_SALD_SACT = VL_SALD_SACT + " & valor
        Result = DoUpdate("SALDO_CETE", Campos, Condicion)
    End If

    Actualiza_Saldo_CeTe = Result

End Function
'Nivel de Cuentas
Function Nivel_Cuenta(cuenta As String) As Byte

    If BoTipoEmpre Then ''JACC R2176 OE
        Select Case Len(cuenta)
            Case 1: Nivel_Cuenta = 1
            Case 2: Nivel_Cuenta = 2
            Case 3 To 4: Nivel_Cuenta = 3
            Case 5 To 6: Nivel_Cuenta = 4
            Case 7 To 8: Nivel_Cuenta = 5
            Case 9 To 10: Nivel_Cuenta = 6
            Case 11 To 12: Nivel_Cuenta = 7
        End Select
    Else 'JACC R2176
        Select Case Len(cuenta)
            Case 1: Nivel_Cuenta = 1
            Case 2: Nivel_Cuenta = 2
            Case 3 To 4: Nivel_Cuenta = 3
            Case 5 To 6: Nivel_Cuenta = 4
            Case 7 To 9: Nivel_Cuenta = 5
            Case 10 To 12: Nivel_Cuenta = 6
            'Case 13 To 14: Nivel_Cuenta = 7
            Case 13 To 15: Nivel_Cuenta = 7 'natalia Req 1460
        End Select
    End If
End Function

'Function Eliminar_Comprobante(ByVal Comprobante As String, ByVal Numero As Long) As Integer 'SKRV T25681 COMENTARIO
Function Eliminar_Comprobante(ByVal Comprobante As String, ByVal Numero As Long, StOrigen As String, StConsec As String) As Integer 'SKRV T25681
Dim I As Long
'INICIO LDCR T10191
'Dim RegCont(13)
'ReDim Datos(13, 0)
'Dim RegCont(16)'SKRV T25681 COMENTARIO
Dim RegCont(18) 'SKRV T25681
'ReDim Datos(16, 0)
ReDim Datos(17, 0) 'APGR T10760
'FIN LDCR T10191
    Msglin "Eliminando Comprobante"
    Condicion = " NU_COMP_MOVI='" & Comprobante & Comi & _
                " AND NU_CONS_MOVI = " & Numero & _
                " ORDER BY NU_IMPU_MOVI "
    'Result = LoadMulData("MOVIMIENTOS", Asterisco, Condicion, Datos())
    'AASV R2171
    Campos = " CD_CENT_MOVI,CD_CUEN_MOVI,CD_TERC_MOVI,FE_FECH_MOVI,"
    Campos = Campos & "NU_COMP_MOVI,NU_CONS_MOVI,NU_IMPU_MOVI,DE_DESC_MOVI,"
    Campos = Campos & "VL_VALO_MOVI,ID_NATU_MOVI,CD_CECO_MOVI,NU_CHEQ_MOVI,"
    Campos = Campos & "VL_REAL_MOVI,NU_MES_MOVI "
    'Campos = Campos & ",TX_IVADED_MOVI,TX_IVA_MOVI,VL_REAL_MOVI" 'LDCR PNC T10191
    Campos = Campos & ",'',TX_IVADED_MOVI,TX_IVA_MOVI,VL_REAL_MOVI" 'APGR T10760
    Result = LoadMulData("MOVIMIENTOS", Campos, Condicion, Datos())
   'AASV R2171
    If Result <> FAIL Then
       If Encontro Then
         For I = 0 To UBound(Datos(), 2)
             'ACUMULAR SALDOS
             If Datos(8, I) = NUL$ Then Datos(8, I) = 0
             If Datos(12, I) = NUL$ Then Datos(12, I) = 0
             Call Saldos(CStr(Datos(1, I)), CByte(Datos(13, I)), CDbl(Datos(8, I)) * -1, CStr(Datos(9, I)))
             'CUENTA - COMPROBANTE
             Campos = Mid(Datos(1, I), 1, 4) 'NIVEL 3
             If (Result <> FAIL) Then Call Cuenta_Comprobante(Campos, CStr(Datos(4, I)), CByte(Datos(13, I)), CDbl(Datos(8, I)) * -1, CStr(Datos(9, I)))
             Campos = Mid(Datos(1, I), 1, 6) 'NIVEL 4
             If (Result <> FAIL) Then Call Cuenta_Comprobante(Campos, CStr(Datos(4, I)), CByte(Datos(13, I)), CDbl(Datos(8, I)) * -1, CStr(Datos(9, I)))
             'CUENTA - TERCERO
             If (Result <> FAIL) Then If Datos(2, I) <> NUL$ Then Call Cuenta_Tercero(CStr(Datos(1, I)), CStr(Datos(2, I)), CByte(Datos(13, I)), CDbl(Datos(8, I)) * -1, CStr(Datos(9, I)), CDbl(Datos(12, I)) * -1)
             'CUENTA - CENTRO DE COSTO
             If (Result <> FAIL) Then If Datos(10, I) <> NUL$ Then Call Cuenta_CentroCosto(CStr(Datos(1, I)), CStr(Datos(10, I)), CByte(Datos(13, I)), CDbl(Datos(8, I)) * -1, CStr(Datos(9, I)))
             'CUENTA - CENTRO DE COSTO - TERCERO
             If (Result <> FAIL) Then
                If Datos(2, I) <> NUL$ And Datos(10, I) <> NUL$ Then Call Cuenta_CentroTercero(CStr(Datos(1, I)), CStr(Datos(10, I)), CStr(Datos(2, I)), CByte(Datos(13, I)), CDbl(Datos(8, I)) * -1, CStr(Datos(9, I)), CDbl(Datos(12, I)) * -1)
             End If
             If Result = FAIL Then Exit For
        Next
        If Result <> FAIL Then
           Condicion = " NU_COMP_MOVI='" & Comprobante & Comi & _
                       " AND NU_CONS_MOVI = " & Numero
           Result = DoDelete("MOVIMIENTOS", Condicion)
           'INICIO LDCR T10191
           'For I = 1 To 13
           'For I = 1 To 16
           For I = 1 To 17 'APGR T10760
           'FIN LDCR T10191
               RegCont(I - 1) = Datos(I, 0)
           Next
           RegCont(13) = "V"                 'Indica el modulo que graba el movimiento
' cuenta, tercero, fecha, comprobante, consecutivo, imputacion,
'concepto, valor, naturaleza, centro costo (dependencia), cheque,
'Valor Base, Mes
           RegCont(6) = "COMPROBANTE ANULADO"
           RegCont(7) = 0
           'SKRV T25681 INICIO
           RegCont(17) = StOrigen
           RegCont(18) = StConsec
           'SKRV T25681 FIN
           If Result <> FAIL Then Result = Crear_Movimiento_Contable(RegCont())
        End If
      End If
    End If
    Eliminar_Comprobante = Result
    Msglin ""
End Function


