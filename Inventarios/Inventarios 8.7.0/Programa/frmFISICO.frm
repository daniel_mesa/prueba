VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Begin VB.Form frmFISICO 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "A/dmon Inventario FISICO"
   ClientHeight    =   7095
   ClientLeft      =   45
   ClientTop       =   435
   ClientWidth     =   11475
   Icon            =   "frmFISICO.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   7095
   ScaleWidth      =   11475
   Begin VB.Frame frmGENERA 
      Height          =   855
      Left            =   480
      TabIndex        =   8
      Top             =   4800
      Visible         =   0   'False
      Width           =   10575
      Begin VB.Frame Frame2 
         Caption         =   "Archivo plano para"
         Height          =   615
         Left            =   1440
         TabIndex        =   10
         Top             =   120
         Width           =   5535
         Begin VB.OptionButton opcSALIDA 
            Caption         =   "Faltantes (Salida)"
            Height          =   255
            Left            =   3000
            TabIndex        =   12
            Top             =   240
            Width           =   2415
         End
         Begin VB.OptionButton opcENTRA 
            Caption         =   "Sobrantes (Entrada)"
            Height          =   255
            Left            =   120
            TabIndex        =   11
            Top             =   240
            Value           =   -1  'True
            Width           =   2535
         End
      End
      Begin VB.CommandButton cmdGENERAR 
         Caption         =   "&GENERAR"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   6
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   495
         Left            =   8880
         Picture         =   "frmFISICO.frx":058A
         Style           =   1  'Graphical
         TabIndex        =   9
         Top             =   240
         Width           =   975
      End
   End
   Begin MSComctlLib.ProgressBar pgbREVISA 
      Height          =   375
      Left            =   2880
      TabIndex        =   7
      Top             =   6240
      Visible         =   0   'False
      Width           =   5895
      _ExtentX        =   10398
      _ExtentY        =   661
      _Version        =   393216
      Appearance      =   1
   End
   Begin VB.Frame frmCONSUMO 
      Height          =   855
      Left            =   480
      TabIndex        =   3
      Top             =   5880
      Visible         =   0   'False
      Width           =   10575
      Begin VB.CommandButton cmdCALCULAR 
         Caption         =   "&CALCULAR"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   6
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   495
         Left            =   8880
         Picture         =   "frmFISICO.frx":0C8C
         Style           =   1  'Graphical
         TabIndex        =   5
         Top             =   240
         Width           =   975
      End
      Begin MSComCtl2.DTPicker dtpDESDE 
         Height          =   375
         Left            =   5040
         TabIndex        =   4
         Top             =   360
         Width           =   1695
         _ExtentX        =   2990
         _ExtentY        =   661
         _Version        =   393216
         Format          =   96534529
         CurrentDate     =   37987
      End
      Begin VB.Label Label4 
         Alignment       =   1  'Right Justify
         Caption         =   "El c�lculo de saldo con fecha de corte:"
         Height          =   375
         Left            =   600
         TabIndex        =   6
         Top             =   360
         Width           =   4155
      End
   End
   Begin MSComctlLib.ListView lstArticulos 
      Height          =   5700
      Left            =   480
      TabIndex        =   1
      Top             =   1080
      Width           =   10425
      _ExtentX        =   18389
      _ExtentY        =   10054
      View            =   3
      LabelEdit       =   1
      MultiSelect     =   -1  'True
      LabelWrap       =   -1  'True
      HideSelection   =   0   'False
      AllowReorder    =   -1  'True
      FullRowSelect   =   -1  'True
      GridLines       =   -1  'True
      _Version        =   393217
      ForeColor       =   -2147483640
      BackColor       =   -2147483643
      BorderStyle     =   1
      Appearance      =   1
      NumItems        =   2
      BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Text            =   "Nombre del Art�culo"
         Object.Width           =   7761
      EndProperty
      BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   1
         Text            =   "C�digo"
         Object.Width           =   3881
      EndProperty
   End
   Begin MSComctlLib.Toolbar tlbACCIONES 
      Align           =   1  'Align Top
      Height          =   630
      Left            =   0
      TabIndex        =   2
      Top             =   0
      Width           =   11475
      _ExtentX        =   20241
      _ExtentY        =   1111
      ButtonWidth     =   1535
      ButtonHeight    =   953
      AllowCustomize  =   0   'False
      Appearance      =   1
      _Version        =   393216
      BorderStyle     =   1
   End
   Begin MSComctlLib.ImageList imgBotones 
      Left            =   10800
      Top             =   3960
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   20
      ImageHeight     =   14
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   11
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmFISICO.frx":0FFE
            Key             =   ""
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmFISICO.frx":16C8
            Key             =   "ASG"
         EndProperty
         BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmFISICO.frx":271A
            Key             =   "CAN"
         EndProperty
         BeginProperty ListImage4 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmFISICO.frx":2CAC
            Key             =   "SAV"
         EndProperty
         BeginProperty ListImage5 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmFISICO.frx":2FFE
            Key             =   "ADD"
         EndProperty
         BeginProperty ListImage6 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmFISICO.frx":3C50
            Key             =   "DEL"
         EndProperty
         BeginProperty ListImage7 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmFISICO.frx":431A
            Key             =   "CHA"
         EndProperty
         BeginProperty ListImage8 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmFISICO.frx":499C
            Key             =   "PRN"
            Object.Tag             =   "PRN"
         EndProperty
         BeginProperty ListImage9 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmFISICO.frx":55F0
            Key             =   "GNR"
            Object.Tag             =   "GNR"
         EndProperty
         BeginProperty ListImage10 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmFISICO.frx":5D02
            Key             =   "CRG"
            Object.Tag             =   "CRG"
         EndProperty
         BeginProperty ListImage11 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmFISICO.frx":6084
            Key             =   "CMP"
            Object.Tag             =   "CMP"
         EndProperty
      EndProperty
   End
   Begin VB.Label lblInforma 
      Caption         =   "Informaci�n"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   320
      Left            =   480
      TabIndex        =   0
      Top             =   740
      Visible         =   0   'False
      Width           =   10335
   End
End
Attribute VB_Name = "frmFISICO"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Dim ArrUXB() As Variant
'Dim Articulo As UnArticulo     'DEPURACION DE CODIGO
Dim CualBodega As New LaBodega
Dim LaBodega As Long, LaAccion As String
Private vSeCargaron As Boolean
Public OpcCod As String   'Opci�n de seguridad
'NU_AUTO_ARTI, CD_CODI_ARTI, NO_NOMB_ARTI, NU_AUTO_UNVE_ARTI, TX_VENCE_ARTI, TX_PARA_ARTI,
'TX_ENTRA_ARTI, CD_GRUP_ARTI, CD_USOS_ARTI, VL_ULCO_ARTI, VL_COPR_ARTI, CT_EXIS_ARTI, DE_OBSE_ARTI, ID_TIPO_ARTIID_TIPO_ARTI, CD_RIPS_ARTI, NU_INDPYP_ARTI

Private Sub cmdCALCULAR_Click()
    Dim vArticulo As New ElArticulo
    Dim Uno As MSComctlLib.ListItem, CnTdr As Integer
    Me.frmCONSUMO.Visible = False
    Desde = "IN_KARDEX"
    Campos = "NU_AUTO_ARTI_KARD, MAX(NU_AUTO_KARD)"
    Condi = "FE_FECH_KARD<=" & FFechaCon(Me.dtpDESDE.Value)
    Condi = Condi & " AND NU_AUTO_BODE_KARD=" & LaBodega
    Condi = Condi & " GROUP BY NU_AUTO_ARTI_KARD"
    ReDim ArrUXB(1, 0)
    Result = LoadMulData(Desde, Campos, Condi, ArrUXB())
    If Result = FAIL Then Exit Sub
    If Not Encontro Then Exit Sub
    Me.tlbACCIONES.Buttons("GENERAR").Enabled = True
    Me.pgbREVISA.Min = 0
    Me.pgbREVISA.Max = UBound(ArrUXB, 2) + 1
    Me.pgbREVISA.Value = 0
    Me.pgbREVISA.Visible = True
    For Each Uno In Me.lstArticulos.ListItems
        Uno.Selected = False
    Next
    For CnTdr = 0 To UBound(ArrUXB, 2)
        ReDim ArrVLR(1)
        Desde = "IN_KARDEX"
        Campos = "NU_ACTUNMR_KARD, NU_ACTUDNM_KARD"
        Condi = "NU_AUTO_KARD=" & CStr(ArrUXB(1, CnTdr))
        Result = LoadData(Desde, Campos, Condi, ArrVLR())
        Set Uno = Me.lstArticulos.FindItem(Trim(CStr(ArrUXB(0, CnTdr))), lvwTag)
        If Uno Is Nothing Then
            If CLng(ArrVLR(0)) = 0 Then GoTo SLDCERO
            vArticulo.IniciaXNumero (CLng(ArrUXB(0, CnTdr)))
            If Encontro = False Then GoTo SLDCERO 'REOL M998
            Me.lstArticulos.ListItems.Add , "A" & vArticulo.Numero, vArticulo.Nombre
            Me.lstArticulos.ListItems("A" & vArticulo.Numero).Tag = vArticulo.Numero
            Me.lstArticulos.ListItems("A" & vArticulo.Numero).ListSubItems.Add , "COAR", Trim(vArticulo.Codigo)
            Me.lstArticulos.ListItems("A" & vArticulo.Numero).ListSubItems.Add , "CANT", "0"
            Me.lstArticulos.ListItems("A" & vArticulo.Numero).ListSubItems.Add , "UNDD", Format(vArticulo.CostoPromedio, "#########.#0")
            Me.lstArticulos.ListItems("A" & vArticulo.Numero).ListSubItems.Add , "CSTO", FormatNumber(CLng(ArrVLR(0)) * vArticulo.CostoPromedio / CLng(ArrVLR(1)), 0, vbFalse, vbTrue)
            Me.lstArticulos.ListItems("A" & vArticulo.Numero).ListSubItems.Add , "FALT", "0"
            Me.lstArticulos.ListItems("A" & vArticulo.Numero).ListSubItems.Add , "SOBR", "0"
            Me.lstArticulos.ListItems("A" & vArticulo.Numero).ListSubItems.Add , "VRFC", "0"
            Set Uno = Me.lstArticulos.FindItem(Trim(CStr(ArrUXB(0, CnTdr))), lvwTag)
            Uno.Selected = True
        End If
        If CLng(Uno.ListSubItems("CANT").Text) > (CLng(ArrVLR(0)) / CLng(ArrVLR(1))) Then
            Uno.ListSubItems("SOBR") = CStr(CLng(Uno.ListSubItems("CANT").Text) - (CLng(ArrVLR(0)) / CLng(ArrVLR(1))))
            Uno.ListSubItems("FALT") = "0"
        Else
            Uno.ListSubItems("FALT") = (CLng(ArrVLR(0)) / CLng(ArrVLR(1))) - CStr(CLng(Uno.ListSubItems("CANT").Text))
            Uno.ListSubItems("SOBR") = "0"
        End If
        Uno.ListSubItems("VRFC") = "1"
SLDCERO:
         Me.pgbREVISA.Value = CnTdr
    Next
    Me.pgbREVISA.Visible = False
    Me.lstArticulos.Height = 5700
    For Each Uno In Me.lstArticulos.ListItems
        If Not CLng(Uno.ListSubItems("VRFC").Text) > 0 Then
            Uno.ListSubItems("SOBR") = CStr(CLng(Uno.ListSubItems("CANT").Text))
            Uno.ListSubItems("FALT") = "0"
            Uno.ListSubItems("VRFC") = "1"
        End If
    Next
End Sub

Private Sub cmdGENERAR_Click()
    Set FrmFISICOIMPO.ListaArticulos = Me.lstArticulos
    FrmFISICOIMPO.CualBodega = LaBodega
    FrmFISICOIMPO.AccionExportar = True
    FrmFISICOIMPO.TipoSobrantes = Me.opcENTRA.Value
    FrmFISICOIMPO.Show 1
End Sub

Private Sub Form_Load()
'    Dim CnTdr As Integer, Punto As MSComctlLib.Node
    Dim CnTdr As Integer    'DEPURACION DE CODIGO
    Call CenterForm(MDI_Inventarios, Me)
    Me.tlbACCIONES.ImageList = Me.imgBotones
    Me.frmGENERA.Top = Me.frmCONSUMO.Top
    Me.tlbACCIONES.Buttons.Clear
    Me.tlbACCIONES.Buttons.Add , "CARGAR", "&Cargar"
    Me.tlbACCIONES.Buttons("CARGAR").Image = "CRG"
    Me.tlbACCIONES.Buttons("CARGAR").Style = tbrDropdown
    Me.tlbACCIONES.Buttons.Add , "LIMPIAR", "&Limpiar"
    Me.tlbACCIONES.Buttons("LIMPIAR").Image = "DEL"
    Me.tlbACCIONES.Buttons("LIMPIAR").Style = tbrDefault
    Me.tlbACCIONES.Buttons.Add , "COMPARAR", "C&omparar"
    Me.tlbACCIONES.Buttons("COMPARAR").Image = "CMP"
    Me.tlbACCIONES.Buttons("COMPARAR").Style = tbrDefault
    Me.tlbACCIONES.Buttons.Add , "GENERAR", "&Generar"
    Me.tlbACCIONES.Buttons("GENERAR").Image = "GNR"
    Me.tlbACCIONES.Buttons("GENERAR").Style = tbrDefault
    Me.tlbACCIONES.Buttons("GENERAR").Enabled = False
    
    Campos = "NU_AUTO_BODE, NU_AUTO_BODE_BODE, TX_CODI_BODE, TX_NOMB_BODE, " & _
        "CD_CODI_CECO_BODE, TX_VENTA_BODE"
    Desde = "IN_BODEGA ORDER BY TX_NOMB_BODE"
    Condi = ""
    ReDim ArrUXB(5, 0)
    Result = LoadMulData(Desde, Campos, Condi, ArrUXB())
    If Result = FAIL Then Exit Sub
    If Not Encontro Then Exit Sub
    For CnTdr = 0 To UBound(ArrUXB, 2)
        Me.tlbACCIONES.Buttons("CARGAR").ButtonMenus.Add , "B" & ArrUXB(0, CnTdr), ArrUXB(3, CnTdr)
        Me.tlbACCIONES.Buttons("CARGAR").ButtonMenus("B" & ArrUXB(0, CnTdr)).Tag = CStr(ArrUXB(0, CnTdr))
    Next
    Me.dtpDESDE.Value = DateAdd("d", 1 - Day(Nowserver), Nowserver)
End Sub

Private Sub lstArticulos_ColumnClick(ByVal ColumnHeader As MSComctlLib.ColumnHeader)
    Me.lstArticulos.SortKey = ColumnHeader.Index - 1
    Me.lstArticulos.Sorted = True
End Sub

Private Sub tlbACCIONES_ButtonClick(ByVal Boton As MSComctlLib.Button)
'    Dim Uno As MSComctlLib.ListItem    'DEPURACION DE CODIGO
    Select Case Boton.Key
    Case Is = "LIMPIAR"
        Me.tlbACCIONES.Buttons("GENERAR").Enabled = False
        Me.lblInforma.Visible = False
        Me.frmCONSUMO.Visible = False
        Call PrepararLista
        Me.lstArticulos.Height = 5700
    Case Is = "COMPARAR"
        If Me.frmCONSUMO.Visible Then
            Me.lstArticulos.Height = 5700
            Me.frmCONSUMO.Visible = False
        Else
            Me.lstArticulos.Height = 4700
            Me.frmCONSUMO.Visible = True
        End If
    Case Is = "GENERAR"
        If Me.frmGENERA.Visible Then
            Me.lstArticulos.Height = 5700
            Me.frmGENERA.Visible = False
        Else
            Me.lstArticulos.Height = 4700
            Me.frmGENERA.Visible = True
        End If
    End Select
End Sub

Private Sub tlbACCIONES_ButtonMenuClick(ByVal ButtonMenu As MSComctlLib.ButtonMenu)
'    Dim CnTdr As Integer       'DEPURACION DE CODIGO
'    Dim Uno As MSComctlLib.ListItem        'DEPURACION DE CODIGO
'    Dim ClaSelect As String, ClaBorrar As String, AutBuscar As String      'DEPURACION DE CODIGO
    LaBodega = CLng(ButtonMenu.Tag)
    LaAccion = ButtonMenu.Parent.Key
    Select Case LaAccion
    Case Is = "CARGAR"
        Me.frmCONSUMO.Visible = False
        CualBodega.IniciaXAuto (LaBodega)
        vSeCargaron = False
        Me.lblInforma.Caption = Trim(CualBodega.Nombre)
        Me.lblInforma.Visible = True
        Call PrepararLista
        Me.lstArticulos.MultiSelect = True
        Set FrmFISICOIMPO.ListaArticulos = Me.lstArticulos
        FrmFISICOIMPO.CualBodega = LaBodega
        FrmFISICOIMPO.AccionExportar = False
        FrmFISICOIMPO.Show 1
NOENC:
Fallo:
    End Select
End Sub

Private Sub Form_KeyPress(KeyAscii As Integer)
    If KeyAscii = vbKeyReturn Then KeyAscii = vbKeyTab
End Sub

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyReturn Then KeyCode = vbKeyTab
End Sub

Private Sub PrepararLista()
    Me.lstArticulos.ListItems.Clear
    Me.lstArticulos.Width = 10500
    Me.lstArticulos.ColumnHeaders.Clear
'lstArticulos, NOAR, COAR, NOCO, NOUS, NOGR, AUCO, COGR, COUS
    Me.lstArticulos.ColumnHeaders.Add , "NOAR", "Nombre del art�culo", 0.4 * Me.lstArticulos.Width
    Me.lstArticulos.ColumnHeaders.Add , "COAR", "C�digo", 0.15 * Me.lstArticulos.Width
    Me.lstArticulos.ColumnHeaders.Add , "NOCO", "Cantidad", 0.1 * Me.lstArticulos.Width
    Me.lstArticulos.ColumnHeaders("NOCO").Alignment = lvwColumnRight
    Me.lstArticulos.ColumnHeaders.Add , "UNDD", "Costo U/dad", 0
    Me.lstArticulos.ColumnHeaders("UNDD").Alignment = lvwColumnRight
    Me.lstArticulos.ColumnHeaders.Add , "CSTO", "Costo", 0.1 * Me.lstArticulos.Width
    Me.lstArticulos.ColumnHeaders("CSTO").Alignment = lvwColumnRight
    Me.lstArticulos.ColumnHeaders.Add , "FALT", "Faltan", 0.1 * Me.lstArticulos.Width
    Me.lstArticulos.ColumnHeaders("FALT").Alignment = lvwColumnRight
    Me.lstArticulos.ColumnHeaders.Add , "SOBR", "Sobran", 0.1 * Me.lstArticulos.Width
    Me.lstArticulos.ColumnHeaders("SOBR").Alignment = lvwColumnRight
    Me.lstArticulos.ColumnHeaders.Add , "VRFC", "Verifica", 0
    Me.lstArticulos.ColumnHeaders("VRFC").Alignment = lvwColumnRight
End Sub

