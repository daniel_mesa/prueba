VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "C_Eventos"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Private WithEvents mTextBox As VB.TextBox
Attribute mTextBox.VB_VarHelpID = -1
Private WithEvents mCboBox  As VB.ComboBox
Attribute mCboBox.VB_VarHelpID = -1
Private WithEvents mCbof    As MSForms.ComboBox
Attribute mCbof.VB_VarHelpID = -1

Private Sub Class_Terminate()
   Set mTextBox = Nothing
   Set mCboBox = Nothing
   Set mCbof = Nothing
End Sub

Private Sub mTextBox_GotFocus()
   mTextBox.BackColor = &HD6FEFE
   mTextBox.SelStart = 0
   mTextBox.SelLength = Len(mTextBox)
End Sub
Private Sub mTextBox_LostFocus()
   mTextBox.BackColor = vbWindowBackground
End Sub
Private Sub mTextBox_KeyPress(KeyAscii As Integer)
   Call Cambiar_Enter(KeyAscii)
End Sub
Private Sub mCboBox_GotFocus()
  
   If mCboBox.Style = 2 Then Exit Sub
   mCboBox.BackColor = &HD6FEFE
   mCboBox.SelStart = 0
   mCboBox.SelLength = Len(mCboBox)
   SendKeys "{F4}"
End Sub
Private Sub mCboBox_LostFocus()
   mCboBox.BackColor = vbWindowBackground
End Sub
Private Sub mCboBox_KeyPress(KeyAscii As Integer)
   Call Cambiar_Enter(KeyAscii)
End Sub
Private Sub mCbof_GotFocus()
   On Error Resume Next
   SendKeys "{F4}"
   mCbof.BackColor = &HD6FEFE
   mCbof.SelStart = 0
   mCbof.SelLength = Len(mCbof)
End Sub
Private Sub mCbof_LostFocus()
   mCbof.BackColor = vbWindowBackground
End Sub
Public Property Set VTextBox(ByVal ctlNewControl As VB.TextBox)
   Set mTextBox = ctlNewControl
End Property

Public Property Set VCboBox(ByVal ctlNewControl As VB.ComboBox)
   Set mCboBox = ctlNewControl
End Property

Public Property Set VCbof(ByVal ctlNewControl As MSForms.ComboBox)
   On Error Resume Next
   Set mCbof = ctlNewControl
End Property
