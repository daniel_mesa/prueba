VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "LaUnidad"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit

Private aut As Long 'NU_AUTO_UNVE
Private cod As String * 10 'TX_CODI_UNVE
Private nom As String * 50 'TX_NOMB_UNVE
Private mul As Integer 'NU_MULT_UNVE
Private div As Integer 'NU_DIVI_UNVE

Public Property Let AutoNumero(num As Long)
    aut = num
End Property

Public Property Get AutoNumero() As Long
    AutoNumero = aut
End Property

Public Property Let Codigo(Cue As String)
    cod = Cue
End Property

Public Property Get Codigo() As String
    Codigo = cod
End Property

Public Property Let Nombre(Cue As String)
    nom = Cue
End Property

Public Property Get Nombre() As String
    Nombre = nom
End Property

Public Property Let Multiplica(num As Integer)
    mul = num
End Property

Public Property Get Multiplica() As Integer
    Multiplica = mul
End Property

Public Property Let Divide(num As Integer)
    div = num
End Property

Public Property Get Divide() As Integer
    Divide = div
End Property

Private Sub Class_Initialize()
    mul = 1
    div = 1
End Sub

Public Sub INIXAutoNumerico()
    Dim Dsd As String, Cdc As String, Cmp As String
    Dim ArrCLS() As Variant
    Dsd = "IN_UNDVENTA"
    Cdc = "NU_AUTO_UNVE=" & aut
    Cmp = "NU_AUTO_UNVE, TX_CODI_UNVE, TX_NOMB_UNVE, NU_MULT_UNVE, NU_DIVI_UNVE"
    ReDim ArrCLS(4)
    Result = LoadData(Dsd, Cmp, Cdc, ArrCLS())
    If Not (Result <> FAIL And encontro) Then aut = 0: Exit Sub
    aut = ArrCLS(0)
    cod = ArrCLS(1)
    nom = ArrCLS(2)
    mul = ArrCLS(3)
    div = ArrCLS(4)
End Sub
