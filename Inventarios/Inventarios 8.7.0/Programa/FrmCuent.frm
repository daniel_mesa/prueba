VERSION 5.00
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "MSFLXGRD.OCX"
Object = "{0BA686C6-F7D3-101A-993E-0000C0EF6F5E}#1.0#0"; "THREED32.OCX"
Object = "{6B7E6392-850A-101B-AFC0-4210102A8DA7}#1.3#0"; "COMCTL32.OCX"
Begin VB.Form FrmCuentas 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Movimiento Contable"
   ClientHeight    =   4140
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   8700
   Icon            =   "FrmCuent.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   4140
   ScaleWidth      =   8700
   StartUpPosition =   2  'CenterScreen
   Begin VB.Frame FraGeneral 
      Height          =   4095
      Left            =   0
      TabIndex        =   0
      Top             =   0
      Width           =   8655
      Begin VB.ComboBox CboTipoIVA 
         Height          =   315
         ItemData        =   "FrmCuent.frx":058A
         Left            =   5160
         List            =   "FrmCuent.frx":0597
         Style           =   2  'Dropdown List
         TabIndex        =   2
         Top             =   2520
         Width           =   1455
      End
      Begin Threed.SSCommand SCmd_Options 
         Height          =   615
         Index           =   4
         Left            =   3960
         TabIndex        =   8
         ToolTipText     =   "Carga los datos por defecto"
         Top             =   3360
         Width           =   1095
         _Version        =   65536
         _ExtentX        =   1931
         _ExtentY        =   1085
         _StockProps     =   78
         Caption         =   "CARGAR DATOS"
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   6
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Picture         =   "FrmCuent.frx":05C3
      End
      Begin Threed.SSCommand SCmd_Options 
         Height          =   615
         Index           =   3
         Left            =   3000
         TabIndex        =   7
         ToolTipText     =   "Quita la l�nea seleccionada"
         Top             =   3360
         Width           =   975
         _Version        =   65536
         _ExtentX        =   1720
         _ExtentY        =   1085
         _StockProps     =   78
         Caption         =   "QUITAR LINEA"
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   6
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Picture         =   "FrmCuent.frx":1217
      End
      Begin Threed.SSCommand SCmd_Options 
         Height          =   615
         Index           =   2
         Left            =   2040
         TabIndex        =   6
         ToolTipText     =   "Selecci�n de Centros de Costo"
         Top             =   3360
         Width           =   975
         _Version        =   65536
         _ExtentX        =   1720
         _ExtentY        =   1085
         _StockProps     =   78
         Caption         =   "C. COSTOS"
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   6
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Picture         =   "FrmCuent.frx":1569
      End
      Begin Threed.SSCommand SCmd_Options 
         Height          =   615
         Index           =   0
         Left            =   120
         TabIndex        =   4
         ToolTipText     =   "Selecci�n de Cuentas Contables"
         Top             =   3360
         Width           =   975
         _Version        =   65536
         _ExtentX        =   1720
         _ExtentY        =   1085
         _StockProps     =   78
         Caption         =   "C.CONTABLES"
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   6
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Picture         =   "FrmCuent.frx":1C33
      End
      Begin Threed.SSCommand SCmd_Options 
         Height          =   615
         Index           =   5
         Left            =   5040
         TabIndex        =   9
         ToolTipText     =   "Cancelar y Salir"
         Top             =   3360
         Width           =   975
         _Version        =   65536
         _ExtentX        =   1720
         _ExtentY        =   1085
         _StockProps     =   78
         Caption         =   "CANCELAR"
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   6
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Picture         =   "FrmCuent.frx":22FD
      End
      Begin MSFlexGridLib.MSFlexGrid GrdCuentas 
         Height          =   2655
         Left            =   120
         TabIndex        =   1
         Top             =   240
         Width           =   8415
         _ExtentX        =   14843
         _ExtentY        =   4683
         _Version        =   393216
         Cols            =   8
         FixedCols       =   0
      End
      Begin Threed.SSCommand SCmd_Options 
         Height          =   615
         Index           =   1
         Left            =   1080
         TabIndex        =   5
         ToolTipText     =   "Selecci�n de Terceros"
         Top             =   3360
         Width           =   975
         _Version        =   65536
         _ExtentX        =   1720
         _ExtentY        =   1085
         _StockProps     =   78
         Caption         =   "TERCERO"
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   6
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Picture         =   "FrmCuent.frx":288F
      End
      Begin Threed.SSCommand SCmd_Options 
         Height          =   615
         Index           =   6
         Left            =   6000
         TabIndex        =   10
         ToolTipText     =   "Aceptar y Salir"
         Top             =   3360
         Width           =   975
         _Version        =   65536
         _ExtentX        =   1720
         _ExtentY        =   1085
         _StockProps     =   78
         Caption         =   "ACEPTAR"
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   6
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Picture         =   "FrmCuent.frx":2F59
      End
      Begin Threed.SSCommand SCmd_Options 
         Height          =   615
         Index           =   8
         Left            =   7920
         TabIndex        =   12
         Tag             =   "Pegar"
         ToolTipText     =   "Aceptar y Salir"
         Top             =   3360
         Width           =   615
         _Version        =   65536
         _ExtentX        =   1085
         _ExtentY        =   1085
         _StockProps     =   78
         Caption         =   "PEGAR"
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   6
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Picture         =   "FrmCuent.frx":3623
      End
      Begin Threed.SSCommand SCmd_Options 
         Height          =   615
         Index           =   7
         Left            =   7320
         TabIndex        =   11
         Tag             =   "Copiar"
         ToolTipText     =   "Aceptar y Salir"
         Top             =   3360
         Width           =   615
         _Version        =   65536
         _ExtentX        =   1085
         _ExtentY        =   1085
         _StockProps     =   78
         Caption         =   "COPIAR"
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   6
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Picture         =   "FrmCuent.frx":4405
      End
      Begin ComctlLib.StatusBar STBCuenta 
         Height          =   375
         Left            =   120
         TabIndex        =   3
         Top             =   2880
         Width           =   8415
         _ExtentX        =   14843
         _ExtentY        =   661
         SimpleText      =   ""
         _Version        =   327682
         BeginProperty Panels {0713E89E-850A-101B-AFC0-4210102A8DA7} 
            NumPanels       =   4
            BeginProperty Panel1 {0713E89F-850A-101B-AFC0-4210102A8DA7} 
               Object.Width           =   6880
               MinWidth        =   6880
               Key             =   ""
               Object.Tag             =   ""
            EndProperty
            BeginProperty Panel2 {0713E89F-850A-101B-AFC0-4210102A8DA7} 
               Object.Width           =   2646
               MinWidth        =   2646
               Key             =   ""
               Object.Tag             =   ""
            EndProperty
            BeginProperty Panel3 {0713E89F-850A-101B-AFC0-4210102A8DA7} 
               Object.Width           =   2646
               MinWidth        =   2646
               Key             =   ""
               Object.Tag             =   ""
            EndProperty
            BeginProperty Panel4 {0713E89F-850A-101B-AFC0-4210102A8DA7} 
               Object.Width           =   2822
               MinWidth        =   2822
               Key             =   ""
               Object.Tag             =   ""
            EndProperty
         EndProperty
      End
   End
End
Attribute VB_Name = "FrmCuentas"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Public Cue_Dep      As String
'Public Cue_Ter      As String 'CARV R2272 Se omite para declararla Global
Public LblCambiar   As String
Dim BandFill        As Integer
Dim Copiar          As String
Dim elifil          As Long
Private vDesdeOtro As Boolean
Private Frmllama    As VB.Form
'INICIO LDCR T10119
'Dim BoEntra As Boolean 'LDCR 2da PARTE R9033/T9930
'Dim BoIva As Boolean 'LDCR 2da PARTE R9033/T9930
'FIN LDCR T10119
Public Property Let DesdeOtroFormulario(bol As Boolean)
   vDesdeOtro = bol
End Property

Public Property Let Desde_Forma(vData As VB.Form)
   Set Frmllama = vData
End Property

Private Sub Form_Activate()
   Dim I As Integer
   If LblCambiar <> "S" Then
      For I = 0 To 8
         If I <> 5 And I <> 6 Then SCmd_Options(I).Enabled = False
      Next I
   End If
   
   'INICIO LDCR 2da PARTE R9033/T9930
'   If BoIva = True Then
'    CboTipoIVA.AddItem ("No deducible")
'    CboTipoIVA.AddItem ("Deducible")
'   End If
   'FIN LDCR 2da PARTE R9033/T9930
End Sub

'---------------------------------------------------------------------------------------
' Procedure : GrdCuentas_Click
' DateTime  : 12/05/2009 17:12
' Author    : carlos_roa
' Purpose   : Carga los Totales debito y credito de la cuenta
'---------------------------------------------------------------------------------------
'
Private Sub GrdCuentas_Click() '*** INICIO CARV R2246
   Dim CondiTemp As String
   Dim ArrTemp() As Variant
   Dim Fila As Long, I As Integer, Debitos As Double, Creditos As Double
   STBCuenta.Panels(1).Text = NUL$
   Fila = GrdCuentas.Row
   If Trim(GrdCuentas.TextMatrix(Fila, 0)) = NUL$ Then Exit Sub
   ReDim ArrTemp(0)
   STBCuenta.Panels(1).Text = GrdCuentas.TextMatrix(Fila, 0)
   CondiTemp = "CD_CODI_CUEN=" & Comi & GrdCuentas.TextMatrix(Fila, 0) & Comi
   CondiTemp = CondiTemp & "AND TX_PUCNIIF_CUEN<>'1'" 'JLPB T31054
   Result = LoadData("CUENTAS", "NO_NOMB_CUEN", CondiTemp, ArrTemp)
   If Result <> FAIL And Encontro Then
      STBCuenta.Panels(1).Text = GrdCuentas.TextMatrix(Fila, 0) & "  " & ArrTemp(0)
   'JLPB T31054 INICIO
   Else
      GrdCuentas.TextMatrix(Fila, 0) = ""
      STBCuenta.Panels(1).Text = ""
      Call Mensaje1("La cuenta no existe", 3)
   'JLPB T31054 FIN
   End If
   Debitos = 0
   Creditos = 0
   For I = 1 To GrdCuentas.Rows - 1
      If GrdCuentas.TextMatrix(I, 1) <> "" Then
         If GrdCuentas.TextMatrix(I, 2) = "D" Then
            Debitos = Debitos + CDbl(GrdCuentas.TextMatrix(I, 1))
         Else
            Creditos = Creditos + CDbl(GrdCuentas.TextMatrix(I, 1))
         End If
      End If
   Next
   STBCuenta.Panels(2).Text = "D. " & Format(Debitos, "#,###,###,###,##0.#0")
   STBCuenta.Panels(3).Text = "C. " & Format(Creditos, "#,###,###,###,##0.#0")
   STBCuenta.Panels(4).Text = Format((Debitos - Creditos), "#,###,###,###,##0.#0")
   
   'INICIO LDCR 2da PARTE R9033/T9930
   If GrdCuentas.Col = 7 Then
      If GrdCuentas.Col = 7 And GrdCuentas.Row > 0 Then
         CboTipoIVA.Left = GrdCuentas.CellLeft + 100
         CboTipoIVA.Top = GrdCuentas.CellTop + GrdCuentas.Top
         If Not CboTipoIVA.Visible And Me.Visible = True Then CboTipoIVA.Visible = True
         'If Me.Visible = True Then CboTipoIVA.SetFocus
      Else
         If CboTipoIVA.Visible = True Then CboTipoIVA.Visible = False
      End If
   End If
   'FIN LDCR 2da PARTE R9033/T9930
End Sub '*** FIN CARV R2246
'INICIO LDCR 2da PARTE R9033/T9930
Private Sub GrdCuentas_Scroll()
    CboTipoIva_Validate (False)
End Sub
'FIN LDCR 2da PARTE R9033/T9930
Private Sub GrdCuentas_SelChange() '*** INICIO CARV R2246
   'Call GrdCuentas_Click 'CARV M5114 no se requiere la ejecucion de la intruccion
   Call GrdCuentas_Click 'JACC M6542
End Sub '*** FIN CARV R2246

Private Sub Form_Load()
    CboTipoIVA.Visible = False 'LDCR 2da PARTE R9033/T9930
   Call Titulos_Grid
   matriz_a_grid Mcuen, GrdCuentas, 1500, 2, "0", NUL$
   If Not vDesdeOtro Then
        If Frmllama.cmdOpciones(0).Enabled = True Then
           SCmd_Options_Click 4
        End If
   End If
   Call CargarDatosAdc
   Call SumarRepetidos
   Call GrdCuentas_Click 'CARV M5114
   Call Audt_Movi(GrdCuentas, 0) 'JLPB T41459-R37519
End Sub

Private Sub Titulos_Grid()
  GrdCuentas.Clear
  GrdCuentas.ColWidth(0) = 1800
  GrdCuentas.ColWidth(1) = 1500
  GrdCuentas.ColWidth(2) = 500
  GrdCuentas.ColWidth(3) = 1500
  GrdCuentas.ColWidth(4) = 1500
  GrdCuentas.ColWidth(5) = 1500
  'INICIO LDCR T10119
  'LDCR 2da PARTE R9033/T9930
'  If BoEntra Then
    GrdCuentas.ColWidth(6) = 1500
    GrdCuentas.ColWidth(7) = 1450
'  Else
'    GrdCuentas.ColWidth(6) = 1
'    GrdCuentas.ColWidth(7) = 1
'  End If
  'LDCR 2da PARTE R9033/T9930
  'FIN LDCR T10119
  GrdCuentas.Row = 0
  GrdCuentas.Col = 0
  GrdCuentas.Text = "Cuenta"
  GrdCuentas.Col = 1
  GrdCuentas.Text = "Valor"
  GrdCuentas.Col = 2
  GrdCuentas.Text = "D/C"
  GrdCuentas.Col = 3
  GrdCuentas.Text = "Tercero"
  GrdCuentas.Col = 4
  GrdCuentas.Text = "Centro de Costo"
  GrdCuentas.Col = 5
  GrdCuentas.Text = "Valor Base"
  'LDCR 2da PARTE R9033/T9930
  GrdCuentas.Col = 6
  GrdCuentas.Text = "Valor IVA"
  GrdCuentas.Col = 7
  GrdCuentas.Text = "Tipo IVA"
  'LDCR 2da PARTE R9033/T9930
End Sub

Private Sub Form_Unload(Cancel As Integer)
   Set Frmllama = Nothing
End Sub

Private Sub GrdCuentas_KeyDown(KeyCode As Integer, Shift As Integer)
If KeyCode = 46 Then GrdCuentas.Text = ""
End Sub

Private Sub GrdCuentas_Validate(Cancel As Boolean)
Dim I As Long, Fila, columna, filsel, columsel
With GrdCuentas
   Fila = .Row: columna = .Col: filsel = .Rowsel: columsel = .ColSel
   For I = 1 To .Rows - 1
      .Row = I ' Dispara el evento de leave cell
   Next
   .Row = Fila: .Col = columna
   .Rowsel = filsel: .ColSel = columsel
End With
End Sub

Private Sub SCmd_Options_Click(Index As Integer)
   'Dim i As Long      'DEPURACION DE CODIGO
   ReDim Marcas(2)
   Select Case Index
      Case 0:
         If GrdCuentas.Col = 0 Then
            Codigo = NUL$
            'JLPB T31054 INICIO Se deja en Comentario la siguiente linea
            'Codigo = Seleccion("CUENTAS", "NO_NOMB_CUEN", "CD_CODI_CUEN,NO_NOMB_CUEN", "CUENTAS CONTABLES", NUL$)
            Condicion = " TX_PUCNIIF_CUEN <> '1' "
            Codigo = Seleccion("CUENTAS", "NO_NOMB_CUEN", "CD_CODI_CUEN,NO_NOMB_CUEN", "CUENTAS CONTABLES", Condicion)
            'JLPB T31054 FIN
            If Codigo <> NUL$ Then GrdCuentas.Text = Codigo
         End If
      Case 1:
         If GrdCuentas.Col = 3 Then
            If Valida_Marca_Cuenta(GrdCuentas.TextMatrix(GrdCuentas.Row, 0), Marcas()) <> FAIL Then
               Codigo = NUL$
               If Marcas(0) = "S" Then Codigo = Seleccion("TERCERO", "NO_NOMB_TERC", "CD_CODI_TERC,NO_NOMB_TERC", "TERCEROS", NUL$)
               If Codigo <> NUL$ Then GrdCuentas.Text = Codigo
            End If
         End If
      Case 2:
         If GrdCuentas.Col = 4 Then
            If Valida_Marca_Cuenta(GrdCuentas.TextMatrix(GrdCuentas.Row, 0), Marcas()) <> FAIL Then
               Codigo = NUL$
               'If Marcas(1) = "S" Then Codigo = Seleccion("CENTRO_COSTO", "NO_NOMB_CECO", "CD_CODI_CECO,NO_NOMB_CECO", "CENTROS DE COSTO", NUL$)
               If Marcas(1) = "S" Then Codigo = Seleccion("CENTRO_COSTO", "NO_NOMB_CECO", "CD_CODI_CECO,NO_NOMB_CECO", "CENTROS DE COSTO", "NU_ESTADO_CECO <> 1") 'APGR T1678
               If Codigo <> NUL$ Then GrdCuentas.Text = Codigo
            End If
         End If
      Case 3:
         If GrdCuentas.Rows - 1 < 2 Then
            GrdCuentas.TextMatrix(elifil, 0) = ""
            GrdCuentas.TextMatrix(elifil, 1) = ""
            GrdCuentas.TextMatrix(elifil, 2) = ""
            GrdCuentas.TextMatrix(elifil, 3) = ""
            GrdCuentas.TextMatrix(elifil, 4) = ""
            GrdCuentas.TextMatrix(elifil, 5) = ""
         Else
            GrdCuentas.RemoveItem elifil
         End If
         elifil = GrdCuentas.Rows - 1
      Case 4:
         GrdCuentas.Clear
         GrdCuentas.Rows = 2
         Call Titulos_Grid
         matriz_a_grid McuenD, GrdCuentas, 1500, 2, "0", NUL$
         Call SumarRepetidos
         Call CargarDatosAdc
      Case 5: continuar = False
         FrmCuentas.Hide
         Unload FrmCuentas
      Case 6:
         limpia_matriz Mcuen, 200, 6
         grid_a_matriz GrdCuentas, Mcuen
         GrdCuentas.Row = 1
         If LblCambiar = "S" Then
            'If GrdCuentas.TextMatrix(elifil, 0) <> NUL$ And GrdCuentas.TextMatrix(elifil, 1) <> NUL$ And GrdCuentas.TextMatrix(elifil, 2) <> NUL$ And GrdCuentas.TextMatrix(elifil, 3) <> NUL$ Then 'smdl m1598
            If GrdCuentas.TextMatrix(elifil, 0) <> NUL$ And GrdCuentas.TextMatrix(elifil, 1) <> NUL$ And GrdCuentas.TextMatrix(elifil, 2) <> NUL$ Then  'smdl m1710
            'If Valida_Datos_Conta(GrdCuentas) = True Then ' ronald: se coloco el parametro opcional para poder recorrer toda la grilla y validar de ese odo si se tienen terceros.
               If Valida_Datos_Conta() = True Then   'DEPURACION DE CODIGO
                  continuar = True
                  primera_entrada = False
                  FrmCuentas.Hide
                  Call Audt_Movi(GrdCuentas, 1) 'JLPB T41459-R37519
                  Unload FrmCuentas
               Else
                  limpia_matriz Mcuen, 200, 6
               End If
            Else
               limpia_matriz Mcuen, 200, 6
            End If
         Else
            FrmCuentas.Hide
            Call Audt_Movi(GrdCuentas, 1) 'JLPB T41459-R37519
            Unload FrmCuentas
         End If
      Case 7:
         Copiar = GrdCuentas.Text
      Case 8:
         With GrdCuentas
            If (1 >= .Col And 1 <= .ColSel) Or (5 >= .Col And 5 <= .ColSel) Then
               If Not IsNumeric(Copiar) Then
                  Call Mensaje1("El valor que se va a copiar debe ser n�merico", 3)
               Else
                  .FillStyle = flexFillRepeat
                  .Text = Copiar
               End If
            Else
               .FillStyle = flexFillRepeat
               .Text = Copiar
            End If
            .SetFocus
         End With
   End Select
End Sub

'
Private Sub GrdCuentas_KeyPress(KeyAscii As Integer)
   ReDim Marcas(2)
   Result = Valida_Marca_Cuenta(GrdCuentas.TextMatrix(GrdCuentas.Row, 0), Marcas())
   If LblCambiar = "S" Then
      Select Case GrdCuentas.Col
         Case 0
            '''Cuenta contable
            'ValidarAlfaFGrd GrdCuentas, GrdCuentas.Col, KeyAscii, 14
            ValidarAlfaFGrd GrdCuentas, GrdCuentas.Col, KeyAscii, 15 'natalia Req 1460
         Case 1
            '''Valores del item
            'ValidarPuntoFGrd GrdCuentas, GrdCuentas.Col, KeyAscii, 9 'JAUM T37261 Se deja linea en comentario
            'ValidarPuntoFGrd GrdCuentas, GrdCuentas.Col, KeyAscii, 10 'JAUM T37261 'JAUM T38421 Se deja linea en comentario
            ValidarPuntoFGrd GrdCuentas, GrdCuentas.Col, KeyAscii, 12 'JAUM T38421
         Case 2
            '''Tipo (d�bito o cr�dito)
            ValidarAlfaFGrd GrdCuentas, GrdCuentas.Col, KeyAscii, 1
         Case 3
            ''Tercero
            'If UCase(Frmllama.Name) = "FRMNCBAN" Or UCase(Frmllama.Name) = "FRMNDBAN" Or UCase(Frmllama.Name) = "FRMCONS" Then
            '  ValidarAlfaFGrd GrdCuentas, GrdCuentas.Col, KeyAscii, 11
            'End If
            If Marcas(0) = "S" Then 'JLPB T28787
               ValidarAlfaFGrd GrdCuentas, GrdCuentas.Col, KeyAscii, 11
            End If 'JLPB T28787
         Case 4
            ''Centro de Costo
            If Marcas(1) = "S" Then
               ValidarAlfaFGrd GrdCuentas, GrdCuentas.Col, KeyAscii, 11
            End If
         'Case 5 LDCR 2da PARTE R9033/T9930
         Case 5, 6
            ''Valor Base
            ValidarPuntoFGrd GrdCuentas, GrdCuentas.Col, KeyAscii, 9
      End Select
      If KeyAscii = 13 Then
         If GrdCuentas.Col = 5 Then 'si es la �ltima colunma, agrega rengl�n
            Agrega_LineaGrid GrdCuentas, True, 0
         Else
            Agrega_LineaGrid GrdCuentas, False, GrdCuentas.Col + 1 'avanza rengl�n
         End If
      End If
   End If
End Sub

Private Sub GrdCuentas_LeaveCell()
   ReDim Marcas(2)
   If BandFill = 1 Then Exit Sub
   If GrdCuentas.Row <> 0 Then
      'Call GrdCuentas_Click 'CARV M5114 'JACC M6542 SE COLOCA EN COMENTARIO
      Select Case GrdCuentas.Col
         Case 0
            If Valida_Marca_Cuenta(GrdCuentas.TextMatrix(GrdCuentas.Row, 0), Marcas()) <> FAIL Then
               If Marcas(0) = "N" Then
                  GrdCuentas.TextMatrix(GrdCuentas.Row, 3) = NUL$
               'ElseIf Marcas(0) = "S" then
               ElseIf Marcas(0) = "S" And GrdCuentas.TextMatrix(GrdCuentas.Row, 3) = NUL$ Then 'CARV R2272
                  'GrdCuentas.TextMatrix(GrdCuentas.Row, 3) = Cue_Ter
                  GrdCuentas.TextMatrix(GrdCuentas.Row, 3) = Cue_Ter 'CARV R2272
               End If
               If Marcas(1) = "N" Then
                  GrdCuentas.TextMatrix(GrdCuentas.Row, 4) = NUL$
               'ElseIf Marcas(1) = "S" And Cue_Dep <> NUL$ Then
               ElseIf Marcas(1) = "S" And GrdCuentas.TextMatrix(GrdCuentas.Row, 4) = NUL$ Then 'CARV R2272
                  'GrdCuentas.TextMatrix(GrdCuentas.Row, 4) = Cue_Dep
                  GrdCuentas.TextMatrix(GrdCuentas.Row, 4) = CentroC_Default 'CARV R2272
               End If
            End If
            'JLPB T31054 INICIO
            If GrdCuentas.TextMatrix(GrdCuentas.Row, 0) <> NUL$ Then
               If CVar(fnDevDato("CUENTAS", "TX_PUCNIIF_CUEN", " CD_CODI_CUEN=" & Comi & (GrdCuentas.TextMatrix(GrdCuentas.Row, 0)) & Comi, True)) = 1 Then
                  GrdCuentas.TextMatrix(GrdCuentas.Row, 0) = NUL$
                  Call Mensaje1("No existe Cuenta", 3)
               End If
            End If
            'JLPB T31054 FIN
         Case 1, 5
            If GrdCuentas.Text = NUL$ Then
               GrdCuentas.Text = "0"
            End If
            GrdCuentas.Text = Format(GrdCuentas.Text, "#,###,###,###,##0.#0")
         Case 2
            If GrdCuentas.Text <> "D" And GrdCuentas.Text <> "C" Then
               GrdCuentas.Text = "D"
            End If
         'INICIO LDCR 2da PARTE R9033/T9930
         Case 6
            If GrdCuentas.Text = NUL$ Then
               GrdCuentas.Text = "0"
            End If
            If CDbl(IIf(GrdCuentas.TextMatrix(GrdCuentas.Row, 6) = NUL$, 0, GrdCuentas.TextMatrix(GrdCuentas.Row, 6))) > CDbl(IIf(GrdCuentas.TextMatrix(GrdCuentas.Row, 1) = NUL$, 0, GrdCuentas.TextMatrix(GrdCuentas.Row, 1))) Then
               GrdCuentas.TextMatrix(GrdCuentas.Row, 6) = GrdCuentas.TextMatrix(GrdCuentas.Row, 1)
            End If
            GrdCuentas.Text = Format(GrdCuentas.Text, "#,###,###,###,##0.#0")
         'INICIO LDCR 2da PARTE R9033/T9930
      End Select
   End If
End Sub

Private Sub GrdCuentas_LostFocus()
   elifil = GrdCuentas.Row
End Sub

'Carga los datos por defecto del tercero y
'del centro de costo
Private Sub CargarDatosAdc()
'Dim bckcolor As String     'DEPURACION DE CODIGO
   'JLPB T28606 INICIO SE DEJA EN COMENTARIO EL SIGUIENTE BLOQUE
'   Dim banddep As String
'   Dim bandter As String
'   Dim I As Integer
'   'Si existen datos de dependencia ya, no la copia
'   'e igual para el tercero
'   banddep = "0"
'   bandter = "0"
'
'   If LblCambiar = "S" Then
'      For I = 1 To GrdCuentas.Rows - 1
'         If bandter = "0" And GrdCuentas.TextMatrix(I, 3) <> NUL$ Then bandter = "1":
'         If banddep = "0" And GrdCuentas.TextMatrix(I, 4) <> NUL$ Then banddep = "1":
'         If bandter = "1" And banddep = "1" Then Exit Sub
'      Next I
'
'   End If
'
'
'   With GrdCuentas
'      Call MouseClock
'      If .Rows > 1 And LblCambiar = "S" Then
'         BandFill = 1
'         .FillStyle = flexFillRepeat
'         'Coloca el codigo del tercero
'         .Row = 1
'         .Col = 3
'         .Rowsel = .Rows - 1
'         .ColSel = 3
'         If bandter = "0" Then .Text = Cue_Ter
'
'         'Coloca el codigo del centro de costo o depedencia
'         .Row = 1
'         .Col = 4
'         .Rowsel = .Rows - 1
'         .ColSel = 4
'         If banddep = "0" Then .Text = Cue_Dep
'         .Row = 1
'         .Col = 0
'         BandFill = 0
'      End If
'      Call MouseNorm
'   End With
   Dim InI As Integer 'Variable utilizada para realizar el recorrido de las filas del grid
   Call MouseClock
   ReDim Marcas(2)
   With GrdCuentas
      If .Rows > 1 And LblCambiar = "S" Then
         For InI = 1 To GrdCuentas.Rows - 1
            Marcas(0) = "N"
            Marcas(1) = "N"
            If .TextMatrix(InI, 0) <> NUL$ Then
               If Valida_Marca_Cuenta(.TextMatrix(InI, 0), Marcas()) <> FAIL Then
                  If Marcas(0) = "S" And .TextMatrix(InI, 3) = NUL$ Then .TextMatrix(InI, 3) = Cue_Ter
                  If Marcas(0) = "N" And .TextMatrix(InI, 3) <> NUL$ Then .TextMatrix(InI, 3) = NUL$
                  If Marcas(1) = "S" And .TextMatrix(InI, 4) = NUL$ Then .TextMatrix(InI, 4) = Cue_Dep
                  If Marcas(1) = "N" And .TextMatrix(InI, 4) <> NUL$ Then .TextMatrix(InI, 4) = NUL$
               End If
            Else
               .TextMatrix(InI, 3) = NUL$
               .TextMatrix(InI, 4) = NUL$
            End If
         Next
      End If
   End With
   Call MouseNorm
   'JLPB T28606 FIN
End Sub

Sub SumarRepetidos()
Dim filas As Variant
'Dim Info(5) As Variant
Dim Info(8) As Variant
Dim I, J, Contador As Integer
Dim fin As Variant
Dim A As Variant

If GrdCuentas.Rows = 2 Then Exit Sub

With FrmCuentas.GrdCuentas
   filas = .Rows
   I = 1
   
   
   Do While I <= filas - 1
      If .TextMatrix(I, 0) <> "" Then
            Contador = 0
            Info(0) = .TextMatrix(I, 0)
            Info(1) = CDbl(.TextMatrix(I, 1))
            Info(2) = .TextMatrix(I, 2)
            Info(3) = .TextMatrix(I, 3)
            Info(4) = .TextMatrix(I, 4)
            Info(5) = .TextMatrix(I, 5)
            'INICIO LDCR T10191
            Info(6) = .TextMatrix(I, 6)
            Info(7) = .TextMatrix(I, 7)
            'FIN LDCR T10191
            A = VerificarFilas
            If .Rows = 2 Then Exit Do 'NMSR M3778
            .RemoveItem (I)
            fin = filas - 1
            J = 1
            
            Contador = Contador + 1
            Do While J <= fin - 1 And A <> False
               If Info(0) = .TextMatrix(J, 0) And Info(2) = .TextMatrix(J, 2) And Info(3) = .TextMatrix(J, 3) Then
                  Info(1) = CDbl(Info(1)) + CDbl(.TextMatrix(J, 1)) ' Suma los valores si son de una misma cuenta, el resto de parametros siguen igual
                  '.RemoveItem (j)
                    If .Rows = 2 Then Exit Do 'NMSR M3778
                    .RemoveItem (J)
                    Contador = Contador + 1
                    J = 0
                    fin = fin - 1
               End If
               J = J + 1
            Loop
            'NMSR M3778
            If .Rows = 2 And Info(0) = .TextMatrix(1, 0) And Info(2) = .TextMatrix(1, 2) And Info(3) = .TextMatrix(1, 3) Then
                .TextMatrix(1, 1) = Info(1)
            Else
             'NMSR M3778
'            .AddItem Info(0) & vbTab & Format(Info(1), "#,###,###,###,##0.#000") & vbTab & Info(2) & vbTab & Info(3) & vbTab & Info(4) & vbTab & Info(5) '|.DR.|, M:1650
                'INICIO LDCR T10191
                '.AddItem Info(0) & vbTab & Format(Info(1), "#,###,###,###,##0.#0") & vbTab & Info(2) & vbTab & Info(3) & vbTab & Info(4) & vbTab & Info(5) 'REOL M1734
                .AddItem Info(0) & vbTab & Format(Info(1), "#,###,###,###,##0.#0") & vbTab & Info(2) & vbTab & Info(3) & vbTab & Info(4) & vbTab & Info(5) & vbTab & Info(6) & vbTab & Info(7)
                'FIN LDCR T10191
                I = 1
                filas = filas - Contador
            End If
      Else
         I = I + 1
      End If
   Loop
End With

End Sub

Function VerificarFilas() As Boolean
   If GrdCuentas.Rows = 3 Then
      VerificarFilas = False ' Ya que m�nimo debe haber una cuenta cr�dito y una d�bito
   Else
      VerificarFilas = True
   End If
End Function
'INICIO LDCR 2da PARTE R9033/T9930
'Function ValIva(BoEntrada As Boolean, Iva As Boolean) As Integer
'BoEntra = BoEntrada
'BoIva = Iva
'ValIva = Result
'End Function
Private Sub CboTipoIva_KeyPress(KeyAscii As Integer)
    Call Cambiar_Enter(KeyAscii)
End Sub
Private Sub CboTipoIva_LostFocus()
    SCmd_Options(6).SetFocus
End Sub
Private Sub CboTipoIva_Validate(Cancel As Boolean)
      If CboTipoIVA.Visible = True Then
        GrdCuentas.TextMatrix(GrdCuentas.Row, 7) = CboTipoIVA.Text
        If CboTipoIVA.Visible = True Then CboTipoIVA.Visible = False
      End If
End Sub
'FIN LDCR 2da PARTE R9033/T9930
Private Sub STBCuenta_PanelClick(ByVal Panel As ComctlLib.Panel)

End Sub

'---------------------------------------------------------------------------------------
' Procedure : Audt_Movi
' DateTime  : 02/03/2018 12:30
' Author    : jairo_parra
' Purpose   : JLPB T41459-R37519 Almacena los los registros del movimiento contable original y el que se va a guardar
'---------------------------------------------------------------------------------------
'
Private Sub Audt_Movi(ByRef MsflGrid As MSFlexGrid, ByTipo As Byte)
   Dim InI As Integer 'Utilizada para los recorridos del for
   Dim InA As Integer 'Utilizada para los recorridos del for
   
   With MsflGrid
      If ByTipo = 0 Then ReDim VrAudMov(8, 1, 0)
      For InA = ByTipo To ByTipo
         For InI = 1 To .Rows - 1
            If UBound(VrAudMov, 3) < InI - 1 Then ReDim Preserve VrAudMov(8, 1, InI - 1)
            VrAudMov(0, InA, InI - 1) = Trim(.TextMatrix(InI, 0))
            VrAudMov(1, InA, InI - 1) = Trim(.TextMatrix(InI, 3))
            VrAudMov(2, InA, InI - 1) = Trim(.TextMatrix(InI, 4))
            VrAudMov(3, InA, InI - 1) = Trim(.TextMatrix(InI, 2))
            VrAudMov(4, InA, InI - 1) = CDbl(IIf(.TextMatrix(InI, 1) = NUL$, 0, .TextMatrix(InI, 1)))
            VrAudMov(5, InA, InI - 1) = CDbl(IIf(.TextMatrix(InI, 5) = NUL$, 0, .TextMatrix(InI, 5)))
            VrAudMov(6, InA, InI - 1) = CDbl(IIf(.TextMatrix(InI, 6) = NUL$, 0, .TextMatrix(InI, 6)))
            VrAudMov(7, InA, InI - 1) = InI
            VrAudMov(8, InA, InI - 1) = ByTipo
         Next
      Next
   End With
End Sub
