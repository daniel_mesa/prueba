VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "MSFLXGRD.OCX"
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Object = "{0BA686C6-F7D3-101A-993E-0000C0EF6F5E}#1.0#0"; "THREED32.OCX"
Begin VB.Form FrmAjusteCostoProm 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Ajustar Costo Hist�rico"
   ClientHeight    =   7575
   ClientLeft      =   3075
   ClientTop       =   2805
   ClientWidth     =   10635
   Icon            =   "FrmAjusteCostoProm.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   7575
   ScaleWidth      =   10635
   ShowInTaskbar   =   0   'False
   Begin VB.PictureBox PictCmbTipo 
      Height          =   495
      Left            =   4950
      ScaleHeight     =   435
      ScaleWidth      =   1440
      TabIndex        =   17
      Top             =   1800
      Visible         =   0   'False
      Width           =   1500
      Begin VB.ComboBox CmbTipo 
         Height          =   315
         ItemData        =   "FrmAjusteCostoProm.frx":058A
         Left            =   -45
         List            =   "FrmAjusteCostoProm.frx":0597
         TabIndex        =   18
         Top             =   -30
         Width           =   1500
      End
   End
   Begin VB.TextBox TxtObserva 
      Height          =   975
      Left            =   5820
      MaxLength       =   150
      ScrollBars      =   3  'Both
      TabIndex        =   14
      Top             =   5010
      Width           =   4725
   End
   Begin VB.Frame FrmEnca 
      Height          =   1185
      Left            =   90
      TabIndex        =   0
      Top             =   60
      Width           =   10485
      Begin VB.TextBox Txt_Articulo 
         Height          =   285
         Left            =   720
         MaxLength       =   20
         TabIndex        =   6
         Top             =   690
         Width           =   1455
      End
      Begin Threed.SSCommand CmdPpto 
         Height          =   375
         Index           =   0
         Left            =   2190
         TabIndex        =   7
         ToolTipText     =   "Selecci�n de Cuenta"
         Top             =   690
         Width           =   615
         _Version        =   65536
         _ExtentX        =   1085
         _ExtentY        =   661
         _StockProps     =   78
         Picture         =   "FrmAjusteCostoProm.frx":05CC
      End
      Begin Threed.SSCommand CmdPpto 
         Height          =   375
         Index           =   1
         Left            =   9180
         TabIndex        =   9
         ToolTipText     =   "Agregar"
         Top             =   660
         Width           =   615
         _Version        =   65536
         _ExtentX        =   1085
         _ExtentY        =   661
         _StockProps     =   78
         Outline         =   0   'False
         Picture         =   "FrmAjusteCostoProm.frx":0F7E
      End
      Begin Threed.SSCommand CmdPpto 
         Height          =   375
         Index           =   2
         Left            =   9780
         TabIndex        =   10
         ToolTipText     =   "Quitar"
         Top             =   660
         Width           =   615
         _Version        =   65536
         _ExtentX        =   1085
         _ExtentY        =   661
         _StockProps     =   78
         Outline         =   0   'False
         Picture         =   "FrmAjusteCostoProm.frx":1648
      End
      Begin MSComCtl2.DTPicker DtFechaInicial 
         Height          =   285
         Left            =   1200
         TabIndex        =   2
         Top             =   270
         Width           =   1515
         _ExtentX        =   2672
         _ExtentY        =   503
         _Version        =   393216
         CheckBox        =   -1  'True
         DateIsNull      =   -1  'True
         Format          =   96534529
         CurrentDate     =   39701
         MinDate         =   29221
      End
      Begin MSComCtl2.DTPicker DtFechaFinal 
         Height          =   285
         Left            =   4440
         TabIndex        =   4
         Top             =   270
         Width           =   1515
         _ExtentX        =   2672
         _ExtentY        =   503
         _Version        =   393216
         CheckBox        =   -1  'True
         DateIsNull      =   -1  'True
         Format          =   96534529
         CurrentDate     =   39701
         MinDate         =   29221
      End
      Begin VB.Label LblFechaFinal 
         AutoSize        =   -1  'True
         Caption         =   "Fecha Final"
         Height          =   195
         Left            =   3360
         TabIndex        =   3
         Top             =   300
         Width           =   825
      End
      Begin VB.Label LblFechaInicial 
         AutoSize        =   -1  'True
         Caption         =   "Fecha Inicial"
         Height          =   195
         Left            =   90
         TabIndex        =   1
         Top             =   270
         Width           =   900
      End
      Begin VB.Label Lbl_articulo 
         BackColor       =   &H8000000E&
         BorderStyle     =   1  'Fixed Single
         Height          =   285
         Left            =   2820
         TabIndex        =   8
         Top             =   690
         Width           =   6165
      End
      Begin VB.Label LblArticulo 
         AutoSize        =   -1  'True
         Caption         =   "Art�culo"
         Height          =   195
         Left            =   90
         TabIndex        =   5
         Top             =   750
         Width           =   555
      End
   End
   Begin MSComctlLib.ListView lstBODEGAS 
      Height          =   2055
      Left            =   90
      TabIndex        =   12
      Top             =   4740
      Width           =   5655
      _ExtentX        =   9975
      _ExtentY        =   3625
      View            =   3
      LabelEdit       =   1
      MultiSelect     =   -1  'True
      LabelWrap       =   -1  'True
      HideSelection   =   -1  'True
      FullRowSelect   =   -1  'True
      GridLines       =   -1  'True
      _Version        =   393217
      ForeColor       =   -2147483640
      BackColor       =   -2147483643
      BorderStyle     =   1
      Appearance      =   1
      NumItems        =   0
   End
   Begin MSFlexGridLib.MSFlexGrid MsFlexArticulos 
      Height          =   3435
      Left            =   90
      TabIndex        =   11
      Top             =   1260
      Width           =   10500
      _ExtentX        =   18521
      _ExtentY        =   6059
      _Version        =   393216
      Cols            =   6
      FixedCols       =   0
      ForeColorSel    =   -2147483635
      WordWrap        =   -1  'True
      FocusRect       =   2
      MergeCells      =   1
   End
   Begin Threed.SSCommand CmdInicProceso 
      Height          =   735
      Left            =   9090
      TabIndex        =   15
      Top             =   6120
      Width           =   1485
      _Version        =   65536
      _ExtentX        =   2619
      _ExtentY        =   1296
      _StockProps     =   78
      Caption         =   "&INICIAR PROCESO"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   6
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Outline         =   0   'False
      Picture         =   "FrmAjusteCostoProm.frx":1D12
   End
   Begin VB.Label LblAviso 
      Caption         =   "Este proceso no tiene afectaci�n contable, ni tampoco hacia otras interfases."
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   630
      Left            =   90
      TabIndex        =   16
      Top             =   6870
      Width           =   5625
   End
   Begin VB.Label LblObser 
      AutoSize        =   -1  'True
      Caption         =   "Observaciones"
      Height          =   195
      Left            =   5850
      TabIndex        =   13
      Top             =   4770
      Width           =   1065
   End
End
Attribute VB_Name = "FrmAjusteCostoProm"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'---------------------------------------------------------------------------------------
' Module    : FrmAjusteCostoProm
' DateTime  : 10/09/2008 09:25
' Author    : Gerardo Mantilla Soto
' Purpose   : Actualiza los movimientos de inventarios que tengan un costo promedio incorrecto
'             GMS R2074
'---------------------------------------------------------------------------------------
Option Explicit
Dim ArrUXB() As Variant
Dim BoCambioFeIniOK As Boolean, BoCambioFeFinOK As Boolean  'Indica si se realizo Clic sobre alguno de los controles DTPicker
Dim Articulo As UnArticulo
Dim BoDatosOk As Boolean 'GMS M4031
Private Sub CmbTipo_KeyPress(KeyAscii As Integer)
    Call Cambiar_Enter(KeyAscii) 'GMS M4398
End Sub
Private Sub CmbTipo_LostFocus()
    'Guarda la selecci�n del rango a filtrar
    If CmbTipo.ListIndex <> -1 Then
        With MsFlexArticulos
            '.TextMatrix(.Row, 5) = CmbTipo.ListIndex
            .TextMatrix(.Row, 5) = CmbTipo.Text 'GMS M4398
        End With
    End If
    
    PictCmbTipo.Visible = False
    CmbTipo.ListIndex = -1
    'Coloca el foco al MsFlexGrid
    If MsFlexArticulos.Enabled Then MsFlexArticulos.SetFocus
End Sub
Private Sub CmdInicProceso_Click()
    Call IniciarProcesoAjusteCosPro
End Sub
Private Sub CmdPpto_Click(Index As Integer)
    Select Case Index
        Case 0: 'Art�culo
            Codigo = NUL$
            Codigo = Seleccion("ARTICULO", "NO_NOMB_ARTI", "CD_CODI_ARTI,NO_NOMB_ARTI", "ARTICULOS", NUL$)
            If Codigo <> NUL$ Then Txt_Articulo = Codigo
            Call Txt_Articulo_LostFocus
        Case 1: 'Adicionar un Art�culo
            If Not Articulo Is Nothing Then
                If Articulo.AutoNumero > 0 And Articulo.Codigo <> NUL$ And Articulo.Nombre <> NUL$ Then
                    With MsFlexArticulos
                        If Not Revisa_FGrd3(MsFlexArticulos, Articulo.AutoNumero, 0) Then
                            .Row = .Rows - 1
                            .TextMatrix(.Row, 0) = Articulo.AutoNumero       'AutoNumerico
                            .TextMatrix(.Row, 1) = Trim$(Articulo.Nombre)    'Nombre
                            .TextMatrix(.Row, 2) = Trim$(Articulo.Codigo)    'Codigo
                            Call Agrega_LineaGrid(MsFlexArticulos, True, 0)
                        Else
                            'Si el activo no existe limpia la fila
                            .Row = .Rows - 1
                            Call limpia_lin_grd(MsFlexArticulos, 0, 4)
                        End If
                        Txt_Articulo = NUL$: Lbl_articulo = NUL$
                    End With
                    Set Articulo = Nothing
                End If
            End If
        Case 2: 'Eliminar un Art�culo
            With MsFlexArticulos
                If .Row > 0 Then
                    If Val(.TextMatrix(.Row, 0)) > 0 Then .RemoveItem .Row
                End If
            End With
    End Select
End Sub
Private Sub DtFechaFinal_KeyDown(KeyCode As Integer, Shift As Integer)
    Call Cambiar_Enter(KeyCode)
End Sub
Private Sub DtFechaFinal_KeyPress(KeyAscii As Integer)
    If KeyAscii = vbKeySpace Then
        If Not BoCambioFeFinOK Then
            DtFechaFinal.Value = Hoy: BoCambioFeFinOK = True
        Else
            DtFechaFinal.Value = NUL$: BoCambioFeFinOK = False
        End If
    End If
End Sub
Private Sub DtFechaFinal_MouseUp(Button As Integer, Shift As Integer, x As Single, Y As Single)
    'GMS M4397. V�lida la posici�n donde fue hecho el Clic
    'If Button = vbLeftButton Then
    If Button = vbLeftButton And x < 200 Then
        If Not BoCambioFeFinOK Then
            DtFechaFinal.Value = Hoy: BoCambioFeFinOK = True
        Else
            DtFechaFinal.Value = NUL$: BoCambioFeFinOK = False
        End If
    End If
End Sub
Private Sub DtFechaFinal_Validate(Cancel As Boolean)
    If BoCambioFeIniOK And BoCambioFeFinOK Then
        If ValFecha(DtFechaFinal, 0) <> FAIL Then
           If DateDiff("d", DtFechaInicial, DtFechaFinal) < 0 Then
                Call Mensaje1("La fecha final es menor que la fecha inicial", 3)
                DtFechaFinal.Value = Hoy
                Cancel = True
           End If
        Else
            Cancel = True
        End If
    End If
End Sub
Private Sub DtFechaInicial_KeyDown(KeyCode As Integer, Shift As Integer)
    Call Cambiar_Enter(KeyCode)
End Sub
Private Sub DtFechaInicial_KeyPress(KeyAscii As Integer)
    If KeyAscii = vbKeySpace Then
        If Not BoCambioFeIniOK Then
            DtFechaInicial.Value = Hoy: BoCambioFeIniOK = True
        Else
            DtFechaInicial.Value = NUL$: BoCambioFeIniOK = False
        End If
    End If
End Sub

Private Sub DtFechaInicial_MouseUp(Button As Integer, Shift As Integer, x As Single, Y As Single)
    'GMS M4397. V�lida la posici�n donde fue hecho el Clic
    'If Button = vbLeftButton Then
    If Button = vbLeftButton And x < 200 Then
        If Not BoCambioFeIniOK Then
            DtFechaInicial.Value = Hoy: BoCambioFeIniOK = True
        Else
            DtFechaInicial.Value = NUL$: BoCambioFeIniOK = False
        End If
    End If
End Sub
Private Sub DtFechaInicial_Validate(Cancel As Boolean)
    If BoCambioFeIniOK Then
        If ValFecha(DtFechaInicial, 0) = FAIL Then
            Cancel = True
        End If
    End If
End Sub
Private Sub Form_Load()
    
    Call CenterForm(MDI_Inventarios, Me)
    'Call GrdFDef(MsFlexArticulos, 0, 0, "AutoNumeArti,1,Nombre del art�culo,2000,C�digo,1500,Costo Promedio,1500,%,1500")
    Call GrdFDef(MsFlexArticulos, 0, 0, "AutoNumeArti,1,Nombre del art�culo,2000,C�digo,1500,Costo Promedio,1500,%,1500,Tipo,1800") 'GMS 17-11-2008 Obs. Entrega
    'Definici�n del ListView de Bodegas
    lstBODEGAS.ListItems.Clear
    lstBODEGAS.Checkboxes = False
    lstBODEGAS.MultiSelect = True
    lstBODEGAS.HideSelection = False
    lstBODEGAS.ColumnHeaders.Clear
    lstBODEGAS.ColumnHeaders.Add , "NOM", "Nombre de la Bodega", 0.66 * Me.lstBODEGAS.Width
    lstBODEGAS.ColumnHeaders.Add , "COD", "C�digo", 0.34 * Me.lstBODEGAS.Width
    
    Call Buscar_Bodegas
    'Inicializa las variables
    BoCambioFeIniOK = False: BoCambioFeFinOK = False
End Sub
Private Sub MsFlexArticulos_KeyPress(KeyAscii As Integer)
    'Colocar el c�digo para este evento.
    Dim LnFilaActual As Long 'GMS M4398
    
    With MsFlexArticulos
        If .Row > 0 Then
            Select Case .Col
                Case 3: ' Costo Promedio
                    Call Escribir_Fgrid(MsFlexArticulos, .Col, KeyAscii, 9, 4, False, Me)
                Case 4: 'Porcentaje
                    If Val(.TextMatrix(.Row, .Col)) <= 100 Then
                        Call Escribir_Fgrid(MsFlexArticulos, .Col, KeyAscii, 3, 3, False, Me)
                    Else
                        .TextMatrix(.Row, .Col) = 100
                    End If
            End Select
        End If
        
        'GMS Rev. Tecnica M4031
        If KeyAscii = 13 Then
            LnFilaActual = .Row
            If .Col <> 5 Then 'GMS M4398
                Call Agrega_LineaGrid(MsFlexArticulos, False, .Col + 1)
            Else
                Call Agrega_LineaGrid(MsFlexArticulos, True, 1)
                If LnFilaActual < .Rows - 1 Then
                   .Row = LnFilaActual + 1
                Else
                   .Row = LnFilaActual
                End If
            End If
        End If
        
    End With
    
    'Call Cambiar_Enter(KeyAscii) 'GMS M4398
End Sub
Private Sub MsFlexArticulos_LeaveCell()
    PictCmbTipo.Visible = False
    CmbTipo.ListIndex = -1
End Sub

Private Sub MsFlexArticulos_RowColChange()
    'Evalua en que columna esta
    With MsFlexArticulos
        Select Case .Col
            Case 5: Call MostrarTipoSeleccion(MsFlexArticulos)
        End Select
    End With
End Sub

Private Sub MsFlexArticulos_Scroll()
    PictCmbTipo.Visible = False
    CmbTipo.ListIndex = -1
End Sub
Private Sub Txt_Articulo_KeyPress(KeyAscii As Integer)
    Call Cambiar_Enter(KeyAscii)
End Sub
Private Sub Txt_Articulo_LostFocus()
    If Txt_Articulo = NUL$ Then Exit Sub
    Call Buscar_Articulo
End Sub
Private Sub Buscar_Articulo()
    ReDim Arr(2)
    Campos = "NU_AUTO_ARTI, CD_CODI_ARTI, NO_NOMB_ARTI"
    Condicion = "CD_CODI_ARTI = " & Comi & Trim$(Txt_Articulo) & Comi
    Result = LoadData("ARTICULO", Campos, Condicion, Arr)
    If Result <> FAIL And Encontro Then
        Lbl_articulo = Arr(2)
        If Articulo Is Nothing Then
            Set Articulo = New UnArticulo
            Articulo.AutoNumero = Arr(0)
            Articulo.Codigo = Arr(1)
            Articulo.Nombre = Arr(2)
        End If
    Else
        Lbl_articulo = NUL$
    End If
End Sub
Private Sub Buscar_Bodegas()
    Dim LnFila As Long
    
    Campos = "NU_AUTO_BODE, NU_AUTO_BODE_BODE, TX_CODI_BODE, TX_NOMB_BODE, " & _
             "CD_CODI_CECO_BODE, TX_VENTA_BODE"
    Desde = "IN_BODEGA"
    Condi = " ORDER BY TX_NOMB_BODE"
    ReDim ArrUXB(5, 0)
    Result = LoadMulData(Desde & Condi, Campos, NUL$, ArrUXB())
    If Result <> FAIL And Encontro Then
        For LnFila = 0 To UBound(ArrUXB, 2)
            Me.lstBODEGAS.ListItems.Add , "B" & ArrUXB(2, LnFila), ArrUXB(3, LnFila)
            Me.lstBODEGAS.ListItems("B" & ArrUXB(2, LnFila)).ListSubItems.Add , "COD", ArrUXB(2, LnFila)
            Me.lstBODEGAS.ListItems("B" & ArrUXB(2, LnFila)).Tag = ArrUXB(0, LnFila)
        Next
    End If
End Sub
'---------------------------------------------------------------------------------------
' Procedure : IniciarProcesoAjusteCosPro
' DateTime  : 12/09/2008 16:11
' Author    : Gerardo Mantilla Soto
' Purpose   : Realiza el proceso de ajuste al costo promedio
'---------------------------------------------------------------------------------------
Private Sub IniciarProcesoAjusteCosPro()
    Dim InFila As Long, StBodegas As String
    Dim LstBod As MSComctlLib.ListItem
    
    
    BoDatosOk = False
    '---------------------------------------------------------------------------------
    'GMS Rev. Tecnica M4031
    'Realiza la validaci�n de los campos obligatorios: Fecha Inicial, Fecha Final
    'Articulos(Costo Promedio), Bodegas.
    'If DtFechaInicial.Value = NUL$ Then Call Mensaje1("Seleccione una Fecha Inicial", 2): Exit Sub
    'If DtFechaFinal.Value = NUL$ Then Call Mensaje1("Seleccione una Fecha Final", 2): Exit Sub
    'If MsFlexArticulos.Rows = 1 Then Call Mensaje1("Seleccione un Art�culo", 2): Exit Sub
    If IsNull(DtFechaInicial.Value) Then Call Mensaje1("Seleccione una Fecha Inicial", 2): Exit Sub
    If IsNull(DtFechaFinal.Value) Then Call Mensaje1("Seleccione una Fecha Final", 2): Exit Sub
    If MsFlexArticulos.Rows = 2 Then Call Mensaje1("Seleccione un Art�culo", 2): Exit Sub
    '---------------------------------------------------------------------------------
    
    'V�lida que por lo menos se haya seleccionado una bodega
    For Each LstBod In Me.lstBODEGAS.ListItems
        If LstBod.Selected Then StBodegas = StBodegas & IIf(Len(StBodegas) > 0, ",", NUL$) & LstBod.Tag
    Next
    If Len(StBodegas) = 0 Then Call Mensaje1("Seleccione al menos una bodega", 2): Exit Sub
    
    'Valida que el costo promedio este registrado
    With MsFlexArticulos
        For InFila = 1 To .Rows - 1
            If MsFlexArticulos.TextMatrix(InFila, 3) = NUL$ And MsFlexArticulos.TextMatrix(InFila, 0) <> NUL$ Then Call Mensaje1("Ingrese el costo promedio a ajustar", 2): Exit Sub
        Next InFila
    End With
    
    'Valida que se haya seleccionado un rango de selecci�n por cada articulo
    With MsFlexArticulos
        For InFila = 1 To .Rows - 1
            If MsFlexArticulos.TextMatrix(InFila, 5) = NUL$ And MsFlexArticulos.TextMatrix(InFila, 0) <> NUL$ Then Call Mensaje1("Ingrese un rango de b�squeda", 2): Exit Sub
        Next InFila
    End With
    
    
    
    'Pregunta por la confirmaci�n del Proceso
    If WarnMsg("�Desea realizar el proceso de ajuste al costo promedio?", 0) Then
        'Se inicia la transacci�n
        If BeginTran(TranUpd & "-AJUSTECOSPRO") <> FAIL Then
            If MotorBD = "SQL" Then
                'If Not ExisteStoreProcedure("PA_AJUSTE_COSPRO") Then
                If ExisteStoreProcedure("PA_AJUSTE_COSPRO") Then 'GMS Rev. Tecnica M4031
                    Dim SPCmd As adodb.Command
                    
                    'Recorre por cada art�culo para realizar el ajuste al costo promedios
                    With MsFlexArticulos
                        For InFila = 1 To .Rows - 1
                            If .TextMatrix(InFila, 0) <> NUL$ And .TextMatrix(InFila, 3) <> NUL$ Then 'GMS Rev. Tecnica M4031
                                Set SPCmd = New adodb.Command
                                '------------------------------------------------------
                                'Ejecuta un Store Procedure para inicializar el proceso
                                '------------------------------------------------------
                                'PA_AJUSTE_COSPRO
                                '------------------------------------------------------
                                'Parametros: @LISTA_NU_AUTO_BODEORG_ENCA: Lista de Bodegas
                                '            @FECHA_INICIAL: Fecha Inicial
                                '            @FECHA_FINAL: Fecha Final
                                '            @NU_AUTO_ARTI_DETA = Auto N�merico del Articulo
                                '            @COSTO_PROMEDIO = Costo promedio a ajustar
                                '            @PORCE = Porcentaje a aplicar
                                '            @NUMCONEX = N�mero de Conexi�n del Usuario
                                '            @TX_OBSER_ACPR = Observaciones
                                '            @Resultado OUTPUT
                                '------------------------------------------------------
                                'Call CreaParametrosSP(SPCmd, "LISTA_NU_AUTO_BODEORG_ENCA", adVarChar, StBodegas, adParamInput, 8000)
                                Call CreaParametrosSP(SPCmd, "LISTA_NU_AUTO_BODEORG_ENCA", adVarChar, StBodegas, adParamInput, 4000) 'GMS Rev. Tecnica M4031
                                Call CreaParametrosSP(SPCmd, "FECHA_INICIAL", adDate, DtFechaInicial, adParamInput)
                                Call CreaParametrosSP(SPCmd, "FECHA_FINAL", adDate, DtFechaFinal, adParamInput)
                                Call CreaParametrosSP(SPCmd, "NU_AUTO_ARTI_DETA", adInteger, CLng(.TextMatrix(InFila, 0)), adParamInput)
                                Call CreaParametrosSP(SPCmd, "COSTO_PROMEDIO", adDouble, CDbl(.TextMatrix(InFila, 3)), adParamInput)
                                Call CreaParametrosSP(SPCmd, "PORCE", adDouble, CDbl(IIf(.TextMatrix(InFila, 4) = NUL$, 0, .TextMatrix(InFila, 4))), adParamInput)
                                Call CreaParametrosSP(SPCmd, "NUMCONEX", adInteger, NumConex, adParamInput)
                                Call CreaParametrosSP(SPCmd, "TX_OBSER_ACPR", adVarChar, Cambiar_Comas_Comillas(TxtObserva), adParamInput, 150)
                                '------------------------------------------------------
                                'GMS M4398
                                'Call CreaParametrosSP(SPCmd, "Tipo_Sel", adInteger, CLng(.TextMatrix(InFila, 5)), adParamInput) 'GMS 17-11-2008 Obs. Entrega
                                If .TextMatrix(InFila, 5) = "Menor o igual" Then
                                    Call CreaParametrosSP(SPCmd, "Tipo_Sel", adInteger, 0, adParamInput)
                                ElseIf .TextMatrix(InFila, 5) = "Mayor o igual" Then
                                    Call CreaParametrosSP(SPCmd, "Tipo_Sel", adInteger, 1, adParamInput)
                                ElseIf .TextMatrix(InFila, 5) = "Menores y Mayores" Then
                                    Call CreaParametrosSP(SPCmd, "Tipo_Sel", adInteger, 2, adParamInput)
                                End If
                                '------------------------------------------------------
                                Call CreaParametrosSP(SPCmd, "Resultado", adBigInt, True, adParamOutput)
                                
                                SPCmd.CommandTimeout = 0 'GMS R4031
                                SPCmd.CommandType = adCmdStoredProc
                                SPCmd.CommandText = "PA_AJUSTE_COSPRO"
                                SPCmd.ActiveConnection = BD(BDCurCon)
                                SPCmd.Execute
                                SPCmd.CommandTimeout = 600 'GMS R4031
                                
                                
                                Result = IIf(SPCmd.Parameters.Item(9).Value, SUCCEED, FAIL): Set SPCmd = Nothing
                                If Result <> FAIL Then BoDatosOk = True
                            End If
                        Next InFila
                    End With
                Else
                    'Call Mensaje1("El procedimiento almacenado PA_AJUSTE_COSPRO, no existe", 2): Exit Sub
                    Call Mensaje1("El procedimiento almacenado PA_AJUSTE_COSPRO, no existe", 2) ' GMS 4031
                End If
            ElseIf MotorBD = "ACCESS" Then
                'Recorre por cada art�culo para realizar el ajuste al costo promedios
                With MsFlexArticulos
                    For InFila = 1 To .Rows - 1
                        If .TextMatrix(InFila, 0) <> NUL$ And .TextMatrix(InFila, 3) <> NUL$ Then 'GMS Rev. Tecnica M4031
                            'Call ProcesoAjusteProAccess(StBodegas, CLng(.TextMatrix(InFila, 0)), CDbl(.TextMatrix(InFila, 3)), CDbl(IIf(.TextMatrix(InFila, 4) = NUL$, 0, .TextMatrix(InFila, 4))))
                            Call ProcesoAjusteProAccess(StBodegas, CLng(.TextMatrix(InFila, 0)), CDbl(.TextMatrix(InFila, 3)), CDbl(IIf(.TextMatrix(InFila, 4) = NUL$, 0, .TextMatrix(InFila, 4))), InFila) 'GMS M4265
                        End If
                    Next InFila
                End With
            End If
        End If
        
        'If Result <> FAIL Then
        If BoDatosOk Then 'GMS M4031
            If CommitTran() <> FAIL Then
                Call Mensaje1("El proceso termino satisfactoriamente.", 3)
            Else
               RollBackTran
               Call Mensaje1("No se pudo ajustar el costo promedio", 2)
            End If
        Else
            RollBackTran
            Call Mensaje1("No se pudo ajustar el costo promedio", 2)
        End If
    End If
End Sub
'---------------------------------------------------------------------------------------
' Procedure : ProcesoAjusteProAccess
' DateTime  : 15/09/2008 10:18
' Author    : Gerardo Mantilla Soto
' Purpose   : Realiza el proceso de ajuste costo promedio para Access
'---------------------------------------------------------------------------------------
'Private Sub ProcesoAjusteProAccess(StListaBodegas As String, LnAutoArti As Long, DblCostoPro As Double, DblPorce As Double)
Private Sub ProcesoAjusteProAccess(StListaBodegas As String, LnAutoArti As Long, DblCostoPro As Double, DblPorce As Double, InFilaActual As Long) 'GMS M4265
    Dim InFila As Long, LnAutoACPR As Long
    Dim LnAutoACP As Long 'GMS M4031
    
    ReDim Arr(3, 0)
    Campos = "NU_AUTO_ENCA, NU_AUTO_BODEORG_ENCA, TX_NOMB_BODE"
    
    '-----------------------------------------------------------------------------------------
    'GMS Rev. Tecnica M4031
    'Desde = "IN_ENCABEZADO INNER JOIN IN_DETALLE ON (NU_AUTO_ORGCABE_DETA = NU_AUTO_ENCA AND NU_AUTO_ARTI_DETA = " & LnAutoArti & " )"
    'Desde = Desde & " INNER JOIN IN_BODEGA ON (NU_AUTO_BODE = NU_AUTO_BODEORG_ENCA)"
    Desde = "IN_ENCABEZADO, IN_DETALLE, IN_BODEGA"
    Condicion = "NU_AUTO_ENCA = NU_AUTO_ORGCABE_DETA"
    Condicion = Condicion & " AND NU_AUTO_BODEORG_ENCA = NU_AUTO_BODE_DETA"
    Condicion = Condicion & " AND NU_AUTO_BODEORG_ENCA = NU_AUTO_BODE"
    Condicion = Condicion & " AND NU_AUTO_ARTI_DETA = " & LnAutoArti & " AND "
    '-----------------------------------------------------------------------------------------
    Condicion = "NOT NU_AUTO_DOCU_ENCA IN (SELECT NU_AUTO_DOCU FROM IN_DOCUMENTO WHERE TX_CODI_DOCU IN ('ENTRA','ORDEN','ANLENT','ANLORD','DEVENT'))"
    Condicion = Condicion & " AND NU_AUTO_BODEORG_ENCA IN (" & StListaBodegas & " )"
    Condicion = Condicion & " AND FE_CREA_ENCA BETWEEN " & FFechaCon(DtFechaInicial) & " AND " & FFechaCon(DtFechaFinal)
    Condicion = Condicion & " GROUP BY NU_AUTO_ARTI_DETA,NU_AUTO_BODEORG_ENCA,NU_AUTO_ENCA,FE_CREA_ENCA,NU_COSTO_DETA,TX_NOMB_BODE"
    '-----------------------------------------------------------------------------------------
    With MsFlexArticulos
        '-----------------------------------------------------------------------------------------
        'GMS M4265. GMS M4398
        'If CLng(.TextMatrix(InFila, 5)) = 0 Then 'Menor
        If .TextMatrix(InFilaActual, 5) = "Menor o igual" Then 'Menor
            'Condicion = Condicion & " HAVING NU_COSTO_DETA < ROUND((" & DblCostoPro & " * (1+(" & DblPorce & "/100))),2)"
            Condicion = Condicion & " HAVING NU_COSTO_DETA < " & Round((DblCostoPro * (1 + (DblPorce / 100))), 2)
        'ElseIf CLng(.TextMatrix(InFila, 5)) = 1 Then 'Mayor
        ElseIf .TextMatrix(InFilaActual, 5) = "Mayor o igual" Then 'Mayor
            'Condicion = Condicion & " HAVING NU_COSTO_DETA > ROUND((" & DblCostoPro & " * (1+(" & DblPorce & "/100))),2)"
            Condicion = Condicion & " HAVING NU_COSTO_DETA > " & Round((DblCostoPro * (1 + (DblPorce / 100))), 2)
        'ElseIf CLng(.TextMatrix(InFila, 5)) = 2 Then 'Menores y Mayores
        ElseIf .TextMatrix(InFilaActual, 5) = "Menores y Mayores" Then 'Menores y Mayores
            'Condicion = Condicion & " HAVING NU_COSTO_DETA <> ROUND((" & DblCostoPro & " * (1+(" & DblPorce & "/100))),2)"
            Condicion = Condicion & " HAVING NU_COSTO_DETA <> " & Round((DblCostoPro * (1 + (DblPorce / 100))), 2)
        End If
        '-----------------------------------------------------------------------------------------
    End With
    '-----------------------------------------------------------------------------------------
    Condicion = Condicion & " ORDER BY NU_AUTO_ARTI_DETA,NU_AUTO_BODEORG_ENCA,NU_AUTO_ENCA,FE_CREA_ENCA"
    
    Result = LoadMulData(Desde, Campos, Condicion, Arr)
    If Result <> FAIL And Encontro Then
        '----------------------------------------------------------------------
        'GMS Rev. Tecnica M4031. Valida si existe un registro en el encabezado.
        Dim ArrTemp(0)
        Desde = "IN_AJUSTECOSTOPROM"
        Campos = "NU_AUTO_ACPR"
        Condicion = "FE_INICIA_ACPR = " & FFechaCon(DtFechaInicial)
        Condicion = Condicion & " AND FE_FINAL_ACPR = " & FFechaCon(DtFechaFinal)
        Condicion = Condicion & " AND NU_CONE_ACPR=" & NumConex
        Result = LoadData(Desde, Campos, Condicion, ArrTemp)
        If Result <> FAIL And Encontro Then LnAutoACPR = Val(ArrTemp(0)) Else LnAutoACPR = 0
        
        If LnAutoACPR = 0 Then
            Valores = "FE_INICIA_ACPR=" & FFechaIns(DtFechaInicial) & Coma
            Valores = Valores & "FE_FINAL_ACPR=" & FFechaIns(DtFechaFinal) & Coma
            Valores = Valores & "FE_RUNPRO_ACPR=" & FFechaIns(Nowserver) & Coma
            Valores = Valores & "NU_CONE_ACPR=" & NumConex & Coma
            Valores = Valores & "TX_OBSER_ACPR=" & Comi & Cambiar_Comas_Comillas(TxtObserva) & Comi & Coma
            Valores = Valores & "TX_BODE_ACPR=" & Comi & Trim(Cambiar_Comas_Comillas(StListaBodegas)) & Comi & Coma
            '----------------------------------------------------------------------
            'GMS M4398
            With MsFlexArticulos
                If .TextMatrix(InFilaActual, 5) = "Menor o igual" Then 'Menor
                    Valores = Valores & "NU_SELCONDI_ACPR=0"
                ElseIf .TextMatrix(InFilaActual, 5) = "Mayor o igual" Then 'Mayor
                    Valores = Valores & "NU_SELCONDI_ACPR=1"
                ElseIf .TextMatrix(InFilaActual, 5) = "Menores y Mayores" Then 'Menores y Mayores
                    Valores = Valores & "NU_SELCONDI_ACPR=2"
                End If
            End With
            '----------------------------------------------------------------------
            Result = DoInsertSQL("IN_AJUSTECOSTOPROM", Valores)
            
            If Result <> FAIL Then LnAutoACPR = GetMaxCod("IN_AJUSTECOSTOPROM", "NU_AUTO_ACPR", NUL$)
        End If
        '----------------------------------------------------------------------
        
        For InFila = 0 To UBound(Arr(), 2)
            'Se actualiza en IN_DETALLE
            'Valores = "NU_COSTO_DETA =" & Round((DblCostoPro * (1 + (DblPorce / 100))), 2)
            Valores = "NU_COSTO_DETA =" & Round(DblCostoPro, 2) 'GMS Rev. Tecnica M4031
            Condicion = "NU_AUTO_ORGCABE_DETA=" & CLng(Arr(0, InFila))
            Condicion = Condicion & " AND NU_AUTO_BODE_DETA=" & CLng(Arr(1, InFila))
            Condicion = Condicion & " AND NU_AUTO_ARTI_DETA=" & LnAutoArti
            Result = DoUpdate("IN_DETALLE", Valores, Condicion)
            If Result <> FAIL Then
            
                'Se actualiza en IN_KARDEX
                'Valores = "NU_COSTO_KARD =" & Round((DblCostoPro * (1 + (DblPorce / 100))), 2)
                'Valores = "NU_COSTO_KARD =" & DblCostoPro
                Valores = "NU_COSTO_KARD =" & Round(DblCostoPro, 2) 'JACC DEPURACION NOV 2008
                Condicion = "NU_AUTO_ORGCABE_KARD=" & CLng(Arr(0, InFila))
                Condicion = Condicion & " AND NU_AUTO_BODE_KARD=" & CLng(Arr(1, InFila))
                Condicion = Condicion & " AND NU_AUTO_ARTI_KARD=" & LnAutoArti
                Result = DoUpdate("IN_KARDEX", Valores, Condicion)
                If Result = FAIL Then Exit Sub
                    
                '----------------------------------------------------------
                'GMS Rev. Tecnica M4031
                'Evalua si ya hay registros previos en  IN_DETA_AJUSTECOSTO.
                'Desde = "IN_AJUSTECOSTOPROM, IN_DETA_AJUSTECOSTO"
                'Condicion = "NU_AUTO_ACPR = NU_AUTO_ACPR_DACP"
                'Condicion = Condicion & " AND NU_AUTO_ARTI_DACP =" & LnAutoArti
                'Condicion = Condicion & " AND AND FE_INICIA_ACPR=" & FFechaCon(DtFechaInicial)
                'Condicion = Condicion & " AND FE_FINAL_ACPR =" & FFechaCon(DtFechaFinal)
                'Condicion = Condicion & " AND NU_CONE_ACPR =" & NumConex
                'Result = LoadData(Desde, "NU_AUTO_ACPR", Condicion, ArrTemp)
                'If Result <> FAIL And Encontro Then
                '    Valores = "TX_BODE_ACPR = TX_BODE_ACPR + ',' + " & Arr(InFila, 2)
                '    Condicion = "NU_AUTO_ACPR =" & CLng(ArrTemp(0))
                '    Result = DoUpdate("IN_AJUSTECOSTOPROM", Valores, Condicion)
                '    If Result <> FAIL Then
                '        Valores = "TX_DCMNTS_DACP = TX_DCMNTS_DACP + ',' +" & CLng(Arr(InFila, 0))
                '        Result = DoUpdate("IN_DETA_AJUSTECOSTO", Valores, Condicion)
                '    End If
                'Else
                '    'Registra la informaci�n del Encabezado
                '    Valores = "FE_INICIA_ACPR=" & FFechaIns(DtFechaInicial) & Coma
                '    Valores = Valores & "FE_FINAL_ACPR=" & FFechaIns(DtFechaFinal) & Coma
                '    Valores = Valores & "FE_RUNPRO_ACPR=" & FFechaIns(Nowserver) & Coma
                '    Valores = Valores & "NU_CONE_ACPR=" & NumConex & Coma
                '    Valores = Valores & "TX_OBSER_ACPR=" & Comi & Cambiar_Comas_Comillas(TxtObserva) & Comi
                '    Valores = Valores & "TX_BODE_ACPR=" & Comi & Arr(InFila, 2)
                '    Result = DoInsertSQL("IN_AJUSTECOSTOPROM", Valores)
                '    If Result <> FAIL Then
                '        'Registra  la informaci�n del Detalle
                '        LnAutoACPR = GetMaxCod("IN_AJUSTECOSTOPROM", "NU_AUTO_ACPR", NUL$)
                '        Valores = "NU_AUTO_ACPR_DACP=" & LnAutoACPR & Coma
                '        Valores = Valores & "NU_AUTO_ARTI_DACP=" & LnAutoArti & Coma
                '        'Valores = Valores & "NU_COSTPR_DACP=" & Round((DblCostoPro * (1 + (DblPorce / 100))), 2)
                '        Valores = Valores & "NU_COSTPR_DACP=" & DblCostoPro 'GMS Rev. Tecnica M4031
                '        Valores = Valores & "NU_PORCEN_DACP=" & DblPorce
                '        Valores = Valores & "TX_DCMNTS_DACP=" & CLng(Arr(InFila, 0))
                '        Result = DoInsert("IN_DETA_AJUSTECOSTO", Valores)
                '    End If
                'End If
                
                '--------------------------------------------------------------------------
                'GMS M4031. Se crea la tabla IN_DETA_DOCUCOSTO
                'Para almacenar el detalle de los documentos modificados por cada art�culo.
                '--------------------------------------------------------------------------
                'Desde = "IN_DETA_AJUSTECOSTO"
                'Condicion = "NU_AUTO_ACPR_DACP = " & LnAutoACPR
                'Condicion = Condicion & " AND NU_AUTO_ARTI_DACP = " & LnAutoArti
                'Result = LoadData(Desde, "NU_AUTO_DACP", Condicion, ArrTemp)
                'If Result <> FAIL And Encontro Then
                    'Valores = "TX_DCMNTS_DACP = TX_DCMNTS_DACP + ',' +" & Trim(CStr(Arr(InFila, 0)))
                    'Result = DoUpdate("IN_DETA_AJUSTECOSTO", Valores, Condicion)
                If LnAutoACP <> 0 Then
                    Valores = "NU_AUTO_DACP_DCPR=" & LnAutoACP & Coma
                    Valores = Valores & "NU_AUTO_ENCA_DCPR=" & CLng(Arr(0, InFila))
                    Result = DoInsertSQL("IN_DETA_DOCUCOSTO", Valores)
                Else
                    Valores = "NU_AUTO_ACPR_DACP=" & LnAutoACPR & Coma
                    Valores = Valores & "NU_AUTO_ARTI_DACP=" & LnAutoArti & Coma
                    Valores = Valores & "NU_COSTPR_DACP=" & Round(DblCostoPro, 2) & Coma
                    Valores = Valores & "NU_PORCEN_DACP=" & DblPorce
                    'Valores = Valores & "TX_DCMNTS_DACP=" & Trim(CStr(Arr(InFila, 0)))'GMS M4031
                    Result = DoInsertSQL("IN_DETA_AJUSTECOSTO", Valores)
                    If Result <> FAIL Then
                        LnAutoACP = GetMaxCod("IN_DETA_AJUSTECOSTO", "NU_AUTO_DACP", NUL$)
                        
                        Valores = "NU_AUTO_DACP_DCPR=" & LnAutoACP & Coma
                        Valores = Valores & "NU_AUTO_ENCA_DCPR=" & CLng(Arr(0, InFila))
                        Result = DoInsertSQL("IN_DETA_DOCUCOSTO", Valores)
                    End If
                End If
                '----------------------------------------------------------
                If Result <> FAIL Then BoDatosOk = True
            Else
                Exit Sub
            End If
        Next InFila
    End If
End Sub
'---------------------------------------------------------------------------------------
' Procedure : MostrarTipoSeleccion
' DateTime  : 17/11/2008 11:30
' Author    : Gerardo Mantilla Soto
' Purpose   : Mostrar una lista para el tipo de selecci�n
'---------------------------------------------------------------------------------------
Sub MostrarTipoSeleccion(Grid As MSFlexGrid)
    With Grid
        If .Row > 0 And .Col = 5 Then
            If .TextMatrix(.Row, 0) <> NUL$ Then
                PictCmbTipo.Visible = False
                PictCmbTipo.Left = .CellLeft + .Left
                PictCmbTipo.Top = .CellTop + .Top
                PictCmbTipo.Height = .CellHeight + 10
                PictCmbTipo.Width = .CellWidth + 10
                CmbTipo.Width = PictCmbTipo.Width
                '-------------------------------------------------------------------
                'GMS M4398
                'cmbTIPO.ListIndex = IIf(.TextMatrix(.Row, 5) = NUL$, 0, .TextMatrix(.Row, 5))
                CmbTipo.ListIndex = IIf(.TextMatrix(.Row, 5) = "Menor o igual", 0, IIf(.TextMatrix(.Row, 5) = "Mayor o igual", 1, IIf(.TextMatrix(.Row, 5) = "Menores y Mayores", 2, -1)))
                '-------------------------------------------------------------------
                PictCmbTipo.ZOrder (0)
                PictCmbTipo.Visible = True
                CmbTipo.SetFocus
            End If
        End If
    End With
End Sub
