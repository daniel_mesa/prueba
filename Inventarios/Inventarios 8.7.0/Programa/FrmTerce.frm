VERSION 5.00
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Object = "{0BA686C6-F7D3-101A-993E-0000C0EF6F5E}#1.0#0"; "THREED32.OCX"
Begin VB.Form FrmTerceros 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Terceros"
   ClientHeight    =   7785
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   6690
   HelpContextID   =   10
   Icon            =   "FrmTerce.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   ScaleHeight     =   7785
   ScaleWidth      =   6690
   Begin TabDlg.SSTab SSTab1 
      Height          =   7695
      Left            =   0
      TabIndex        =   0
      Top             =   0
      Width           =   5445
      _ExtentX        =   9604
      _ExtentY        =   13573
      _Version        =   393216
      Tabs            =   2
      TabHeight       =   520
      TabCaption(0)   =   "Datos Generales"
      TabPicture(0)   =   "FrmTerce.frx":058A
      Tab(0).ControlEnabled=   -1  'True
      Tab(0).Control(0)=   "Frame1"
      Tab(0).Control(0).Enabled=   0   'False
      Tab(0).ControlCount=   1
      TabCaption(1)   =   "Datos Tributarios"
      TabPicture(1)   =   "FrmTerce.frx":05A6
      Tab(1).ControlEnabled=   0   'False
      Tab(1).Control(0)=   "FrmCREE"
      Tab(1).Control(1)=   "Frame7"
      Tab(1).Control(2)=   "CboRegimenIVA"
      Tab(1).Control(3)=   "Frame6"
      Tab(1).Control(4)=   "Frame5"
      Tab(1).Control(5)=   "Frame3"
      Tab(1).Control(6)=   "Frame4"
      Tab(1).Control(7)=   "Label5"
      Tab(1).ControlCount=   8
      Begin VB.Frame FrmCREE 
         Caption         =   "&Impuesto CREE"
         Height          =   975
         Left            =   -74880
         TabIndex        =   84
         Top             =   4080
         Width           =   5250
         Begin VB.CheckBox ChkImpCREE 
            Alignment       =   1  'Right Justify
            Caption         =   "&No sujeto pasivo CREE o Autorretenedores del impuesto CREE"
            Height          =   615
            Index           =   3
            Left            =   360
            TabIndex        =   74
            Top             =   240
            Width           =   3495
         End
      End
      Begin VB.Frame Frame7 
         Caption         =   "Exento "
         Height          =   1095
         Left            =   -74880
         TabIndex        =   60
         Top             =   2880
         Width           =   5250
         Begin VB.CheckBox ChkExcento 
            Alignment       =   1  'Right Justify
            Caption         =   "&Retenci�n de IVA"
            Height          =   255
            Index           =   2
            Left            =   2760
            TabIndex        =   62
            Top             =   240
            Width           =   1935
         End
         Begin VB.CheckBox ChkExcento 
            Alignment       =   1  'Right Justify
            Caption         =   "&Retenci�n de ICA"
            Height          =   255
            Index           =   1
            Left            =   360
            TabIndex        =   63
            Top             =   615
            Width           =   2175
         End
         Begin VB.CheckBox ChkExcento 
            Alignment       =   1  'Right Justify
            Caption         =   "&Retenci�n en la Fuente"
            Height          =   255
            Index           =   0
            Left            =   360
            TabIndex        =   61
            Top             =   250
            Width           =   2175
         End
      End
      Begin VB.ComboBox CboRegimenIVA 
         Height          =   315
         ItemData        =   "FrmTerce.frx":05C2
         Left            =   -73560
         List            =   "FrmTerce.frx":05CC
         Style           =   2  'Dropdown List
         TabIndex        =   46
         Top             =   480
         Width           =   1575
      End
      Begin VB.Frame Frame6 
         Height          =   2055
         Left            =   -74880
         TabIndex        =   47
         Top             =   800
         Width           =   5250
         Begin VB.OptionButton OptNo 
            Caption         =   "No"
            Height          =   255
            Left            =   2400
            TabIndex        =   50
            Top             =   240
            Width           =   615
         End
         Begin VB.OptionButton OptSi 
            Caption         =   "Si"
            Height          =   255
            Left            =   1800
            TabIndex        =   49
            Top             =   240
            Width           =   615
         End
         Begin VB.CheckBox ChkAutoRetIva 
            Alignment       =   1  'Right Justify
            Caption         =   "&AutoRetenedor de IVA"
            Height          =   375
            Left            =   120
            TabIndex        =   51
            Top             =   600
            Width           =   1815
         End
         Begin VB.CheckBox ChkAutoRetIca 
            Alignment       =   1  'Right Justify
            Caption         =   "&AutoRetenedor de ICA"
            Height          =   375
            Left            =   120
            TabIndex        =   54
            Top             =   1080
            Width           =   1815
         End
         Begin VB.CheckBox ChkAutoRetRenta 
            Alignment       =   1  'Right Justify
            Caption         =   "&AutoRetenedor de RENTA"
            Height          =   375
            Left            =   120
            TabIndex        =   57
            Top             =   1560
            Width           =   1815
         End
         Begin VB.TextBox TxtResIVA 
            Height          =   285
            Left            =   3705
            MaxLength       =   15
            TabIndex        =   53
            Top             =   580
            Width           =   1335
         End
         Begin VB.TextBox TxtResICA 
            Height          =   285
            Left            =   3705
            MaxLength       =   15
            TabIndex        =   56
            Top             =   1100
            Width           =   1335
         End
         Begin VB.TextBox TxtResRENTA 
            Height          =   285
            Left            =   3705
            MaxLength       =   15
            TabIndex        =   59
            Top             =   1600
            Width           =   1335
         End
         Begin VB.Label Label4 
            AutoSize        =   -1  'True
            Caption         =   "&Gran Contribuyente :"
            Height          =   195
            Index           =   4
            Left            =   120
            TabIndex        =   48
            Top             =   240
            Width           =   1455
         End
         Begin VB.Label Label4 
            Caption         =   "&Res. Autoretendor de IVA"
            Height          =   435
            Index           =   1
            Left            =   2280
            TabIndex        =   52
            Top             =   600
            Width           =   1335
         End
         Begin VB.Label Label4 
            Caption         =   "&Res. Autoretendor de ICA"
            Height          =   435
            Index           =   2
            Left            =   2280
            TabIndex        =   55
            Top             =   1080
            Width           =   1335
         End
         Begin VB.Label Label4 
            Caption         =   "&Res. Autoretendor de RENTA"
            Height          =   435
            Index           =   3
            Left            =   2280
            TabIndex        =   58
            Top             =   1560
            Width           =   1395
         End
      End
      Begin VB.Frame Frame5 
         Caption         =   "&Agente Retenci�n de IVA"
         Height          =   615
         Left            =   -74880
         TabIndex        =   64
         Top             =   5160
         Visible         =   0   'False
         Width           =   5250
         Begin VB.CheckBox ChkRegSimplif 
            Alignment       =   1  'Right Justify
            Caption         =   "&Regimen SIMPLIFICADO"
            Height          =   255
            Index           =   0
            Left            =   2640
            TabIndex        =   66
            Top             =   250
            Width           =   2175
         End
         Begin VB.CheckBox ChkRegComun 
            Alignment       =   1  'Right Justify
            Caption         =   "&Regimen COMUN"
            Height          =   255
            Index           =   0
            Left            =   360
            TabIndex        =   65
            Top             =   250
            Width           =   1815
         End
      End
      Begin VB.Frame Frame3 
         Caption         =   "&Agente Retenci�n de ICA"
         Height          =   615
         Left            =   -74880
         TabIndex        =   67
         Top             =   5655
         Visible         =   0   'False
         Width           =   5250
         Begin VB.CheckBox ChkRegComun 
            Alignment       =   1  'Right Justify
            Caption         =   "&Regimen COMUN"
            Height          =   255
            Index           =   1
            Left            =   360
            TabIndex        =   68
            Top             =   240
            Width           =   1815
         End
         Begin VB.CheckBox ChkRegSimplif 
            Alignment       =   1  'Right Justify
            Caption         =   "&Regimen SIMPLIFICADO"
            Height          =   255
            Index           =   1
            Left            =   2640
            TabIndex        =   69
            Top             =   250
            Width           =   2175
         End
      End
      Begin VB.Frame Frame4 
         Caption         =   "&Agente Retenci�n de RENTA"
         Height          =   615
         Left            =   -74880
         TabIndex        =   70
         Top             =   6165
         Visible         =   0   'False
         Width           =   5250
         Begin VB.CheckBox ChkRegComun 
            Alignment       =   1  'Right Justify
            Caption         =   "&Regimen COMUN"
            Height          =   255
            Index           =   2
            Left            =   360
            TabIndex        =   71
            Top             =   250
            Width           =   1815
         End
         Begin VB.CheckBox ChkRegSimplif 
            Alignment       =   1  'Right Justify
            Caption         =   "&Regimen SIMPLIFICADO"
            Height          =   255
            Index           =   2
            Left            =   2640
            TabIndex        =   72
            Top             =   250
            Width           =   2175
         End
      End
      Begin VB.Frame Frame1 
         Height          =   7215
         Left            =   0
         TabIndex        =   1
         Top             =   360
         Width           =   5295
         Begin VB.TextBox TxtValorAccion 
            Enabled         =   0   'False
            Height          =   285
            Left            =   1560
            MaxLength       =   15
            TabIndex        =   43
            Top             =   6840
            Width           =   1455
         End
         Begin VB.OptionButton OptDGNo 
            Caption         =   "No"
            Height          =   195
            Left            =   1320
            TabIndex        =   34
            Top             =   5760
            Visible         =   0   'False
            Width           =   735
         End
         Begin VB.OptionButton OptDGSi 
            Caption         =   "Si"
            Height          =   255
            Left            =   2040
            TabIndex        =   35
            Top             =   5760
            Visible         =   0   'False
            Width           =   615
         End
         Begin VB.TextBox TxtNom 
            Height          =   285
            Index           =   3
            Left            =   1440
            MaxLength       =   20
            TabIndex        =   15
            Top             =   2160
            Width           =   2535
         End
         Begin VB.TextBox TxtNom 
            Height          =   285
            Index           =   2
            Left            =   1440
            MaxLength       =   20
            TabIndex        =   13
            Top             =   1800
            Width           =   2535
         End
         Begin VB.TextBox TxtNom 
            Height          =   285
            Index           =   1
            Left            =   1440
            MaxLength       =   20
            TabIndex        =   11
            Top             =   1440
            Width           =   2535
         End
         Begin VB.TextBox TxtNom 
            Height          =   285
            Index           =   0
            Left            =   1440
            MaxLength       =   20
            TabIndex        =   9
            Top             =   1080
            Width           =   2535
         End
         Begin VB.TextBox TxtTercero 
            Height          =   315
            Index           =   0
            Left            =   1320
            MaxLength       =   20
            TabIndex        =   3
            Tag             =   " "
            Top             =   360
            Width           =   1335
         End
         Begin VB.TextBox TxtTercero 
            Height          =   525
            Index           =   3
            Left            =   1320
            MaxLength       =   200
            MultiLine       =   -1  'True
            ScrollBars      =   2  'Vertical
            TabIndex        =   19
            Tag             =   " "
            Top             =   3120
            Width           =   3855
         End
         Begin VB.TextBox TxtTercero 
            Height          =   285
            Index           =   4
            Left            =   1320
            MaxLength       =   15
            TabIndex        =   21
            Tag             =   " "
            Top             =   3720
            Width           =   1455
         End
         Begin VB.TextBox TxtTercero 
            Height          =   285
            Index           =   5
            Left            =   1320
            MaxLength       =   20
            TabIndex        =   27
            Tag             =   " "
            Top             =   5040
            Visible         =   0   'False
            Width           =   2175
         End
         Begin VB.TextBox TxtTercero 
            Height          =   285
            Index           =   6
            Left            =   1320
            MaxLength       =   50
            MultiLine       =   -1  'True
            TabIndex        =   30
            Tag             =   " "
            Top             =   5400
            Visible         =   0   'False
            Width           =   3495
         End
         Begin VB.TextBox TxtTercero 
            Height          =   315
            Index           =   1
            Left            =   2640
            Locked          =   -1  'True
            MaxLength       =   1
            TabIndex        =   4
            Tag             =   " "
            Top             =   360
            Width           =   255
         End
         Begin VB.TextBox TxtTercero 
            Height          =   525
            Index           =   2
            Left            =   1320
            MaxLength       =   250
            MultiLine       =   -1  'True
            ScrollBars      =   2  'Vertical
            TabIndex        =   17
            Tag             =   " "
            Top             =   2520
            Width           =   3855
         End
         Begin VB.ComboBox CmbTipoP 
            Height          =   315
            ItemData        =   "FrmTerce.frx":05E5
            Left            =   1320
            List            =   "FrmTerce.frx":05EF
            Style           =   2  'Dropdown List
            TabIndex        =   7
            Top             =   720
            Width           =   1575
         End
         Begin VB.ComboBox CmbTipoE 
            Height          =   315
            ItemData        =   "FrmTerce.frx":0606
            Left            =   1320
            List            =   "FrmTerce.frx":0619
            Style           =   2  'Dropdown List
            TabIndex        =   39
            Top             =   6120
            Width           =   1815
         End
         Begin VB.TextBox TxtTercero 
            Height          =   285
            Index           =   7
            Left            =   1320
            MaxLength       =   15
            TabIndex        =   42
            Tag             =   " "
            Top             =   6480
            Width           =   1455
         End
         Begin VB.ComboBox CmbTipoEm 
            Height          =   315
            ItemData        =   "FrmTerce.frx":0650
            Left            =   3120
            List            =   "FrmTerce.frx":065D
            Style           =   2  'Dropdown List
            TabIndex        =   40
            Top             =   6120
            Width           =   1695
         End
         Begin VB.CheckBox ChkSocio 
            Caption         =   "S&ocio"
            Height          =   195
            Left            =   3120
            TabIndex        =   36
            Top             =   5760
            Width           =   735
         End
         Begin VB.ComboBox CboTipoDoc 
            Height          =   315
            ItemData        =   "FrmTerce.frx":0687
            Left            =   3000
            List            =   "FrmTerce.frx":069D
            Style           =   2  'Dropdown List
            TabIndex        =   5
            Top             =   360
            Width           =   615
         End
         Begin VB.ComboBox CboActividad 
            Height          =   315
            ItemData        =   "FrmTerce.frx":06BB
            Left            =   1320
            List            =   "FrmTerce.frx":06BD
            Style           =   2  'Dropdown List
            TabIndex        =   31
            Top             =   5400
            Width           =   3855
         End
         Begin VB.ComboBox CboMuni 
            Height          =   315
            Left            =   1320
            TabIndex        =   28
            Top             =   5040
            Width           =   3855
         End
         Begin VB.ComboBox CboDpto 
            Height          =   315
            ItemData        =   "FrmTerce.frx":06BF
            Left            =   1320
            List            =   "FrmTerce.frx":06C1
            TabIndex        =   25
            Top             =   4680
            Width           =   3855
         End
         Begin VB.TextBox TxtTercero 
            Alignment       =   1  'Right Justify
            Enabled         =   0   'False
            Height          =   285
            Index           =   8
            Left            =   4080
            MaxLength       =   15
            TabIndex        =   37
            Tag             =   " "
            ToolTipText     =   "N�mero de Acciones del socio"
            Top             =   5760
            Width           =   1095
         End
         Begin VB.TextBox TxtTercero 
            Height          =   525
            Index           =   9
            Left            =   1320
            MaxLength       =   100
            MultiLine       =   -1  'True
            ScrollBars      =   2  'Vertical
            TabIndex        =   23
            Tag             =   " "
            Top             =   4080
            Width           =   3855
         End
         Begin VB.CheckBox ChkEstado 
            Caption         =   "&Inactivo"
            Height          =   195
            Left            =   3720
            TabIndex        =   44
            Top             =   6840
            Width           =   1335
         End
         Begin Threed.SSCommand CmdAtras 
            Height          =   375
            Left            =   3720
            TabIndex        =   80
            ToolTipText     =   "Retroceder Registro"
            Top             =   240
            Width           =   495
            _Version        =   65536
            _ExtentX        =   873
            _ExtentY        =   661
            _StockProps     =   78
            Picture         =   "FrmTerce.frx":06C3
         End
         Begin Threed.SSCommand CmdLimpia 
            Height          =   375
            Left            =   4800
            TabIndex        =   82
            ToolTipText     =   "Limpia Pantalla"
            Top             =   240
            Width           =   375
            _Version        =   65536
            _ExtentX        =   661
            _ExtentY        =   661
            _StockProps     =   78
            Picture         =   "FrmTerce.frx":0D8D
         End
         Begin Threed.SSCommand CmdAdelanta 
            Height          =   375
            Left            =   4200
            TabIndex        =   81
            ToolTipText     =   "Adelantar Registro"
            Top             =   240
            Width           =   495
            _Version        =   65536
            _ExtentX        =   873
            _ExtentY        =   661
            _StockProps     =   78
            Picture         =   "FrmTerce.frx":10DF
         End
         Begin Threed.SSCommand CmdSeleccion 
            Height          =   255
            Left            =   4680
            TabIndex        =   32
            ToolTipText     =   "Seleccion de Actividad Econ�mica"
            Top             =   5400
            Visible         =   0   'False
            Width           =   495
            _Version        =   65536
            _ExtentX        =   873
            _ExtentY        =   450
            _StockProps     =   78
            BevelWidth      =   0
            Outline         =   0   'False
            Picture         =   "FrmTerce.frx":17A9
         End
         Begin VB.Label Label6 
            Caption         =   "Valor Acciones"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Left            =   120
            TabIndex        =   83
            Top             =   6840
            Width           =   1455
         End
         Begin VB.Label LblNom 
            Caption         =   "Se&gundo Apellido"
            Height          =   255
            Index           =   3
            Left            =   120
            TabIndex        =   14
            Top             =   2160
            Width           =   1335
         End
         Begin VB.Label LblNom 
            Caption         =   "P&rimer Apellido"
            Height          =   255
            Index           =   2
            Left            =   120
            TabIndex        =   12
            Top             =   1800
            Width           =   1455
         End
         Begin VB.Label LblNom 
            Caption         =   "&Segundo Nombre"
            Height          =   255
            Index           =   1
            Left            =   120
            TabIndex        =   10
            Top             =   1440
            Width           =   1455
         End
         Begin VB.Label LblNom 
            Caption         =   "&Primer Nombre"
            Height          =   255
            Index           =   0
            Left            =   120
            TabIndex        =   8
            Top             =   1080
            Width           =   1335
         End
         Begin VB.Label LblAutoRete 
            AutoSize        =   -1  'True
            Caption         =   "A&utoretenedor "
            Height          =   195
            Left            =   120
            TabIndex        =   33
            Top             =   5760
            Visible         =   0   'False
            Width           =   1050
         End
         Begin VB.Label LblActividad 
            Caption         =   "&Actividad Ec�nomica"
            Height          =   435
            Left            =   120
            TabIndex        =   29
            Top             =   5280
            Width           =   900
         End
         Begin VB.Label Label1 
            AutoSize        =   -1  'True
            Caption         =   "&Ciudad"
            Height          =   255
            Left            =   120
            TabIndex        =   26
            Top             =   5040
            Width           =   495
         End
         Begin VB.Label LblTelefono 
            AutoSize        =   -1  'True
            Caption         =   "T&el�fono"
            Height          =   195
            Left            =   120
            TabIndex        =   20
            Top             =   3720
            Width           =   630
         End
         Begin VB.Label LblDireccion 
            AutoSize        =   -1  'True
            Caption         =   "&Direcci�n"
            Height          =   195
            Left            =   120
            TabIndex        =   18
            Top             =   3120
            Width           =   675
         End
         Begin VB.Label LblPersona 
            AutoSize        =   -1  'True
            Caption         =   "&Tipo Persona"
            Height          =   195
            Left            =   120
            TabIndex        =   6
            Top             =   720
            Width           =   945
         End
         Begin VB.Label LblNombre 
            AutoSize        =   -1  'True
            Caption         =   "&Razon Social"
            Height          =   195
            Left            =   120
            TabIndex        =   16
            Top             =   2640
            Width           =   945
         End
         Begin VB.Label LblNit 
            AutoSize        =   -1  'True
            Caption         =   "&Nit"
            Height          =   195
            Left            =   120
            TabIndex        =   2
            Top             =   360
            Width           =   195
         End
         Begin VB.Label Label2 
            AutoSize        =   -1  'True
            Caption         =   "T&ipo Empresa"
            Height          =   195
            Index           =   0
            Left            =   120
            TabIndex        =   38
            Top             =   6120
            Width           =   975
         End
         Begin VB.Label Label2 
            AutoSize        =   -1  'True
            Caption         =   "C�&d. Empresa"
            Height          =   195
            Index           =   1
            Left            =   120
            TabIndex        =   41
            Top             =   6480
            Width           =   990
         End
         Begin VB.Label Label3 
            AutoSize        =   -1  'True
            Caption         =   "De&partamento"
            Height          =   195
            Left            =   120
            TabIndex        =   24
            Top             =   4680
            Width           =   1005
         End
         Begin VB.Label Label4 
            AutoSize        =   -1  'True
            Caption         =   "&E-Mail"
            Height          =   195
            Index           =   0
            Left            =   120
            TabIndex        =   22
            Top             =   4200
            Width           =   435
         End
      End
      Begin VB.Label Label5 
         AutoSize        =   -1  'True
         Caption         =   "&Regimen de IVA"
         Height          =   195
         Left            =   -74880
         TabIndex        =   45
         Top             =   480
         Width           =   1155
      End
   End
   Begin VB.Frame Frame2 
      Height          =   5535
      Left            =   5520
      TabIndex        =   73
      Top             =   0
      Width           =   1095
      Begin Threed.SSCommand SCmd_Options 
         Height          =   735
         Index           =   0
         Left            =   120
         TabIndex        =   75
         Top             =   600
         Width           =   855
         _Version        =   65536
         _ExtentX        =   1508
         _ExtentY        =   1296
         _StockProps     =   78
         Caption         =   "&GUARDAR"
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   6
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Picture         =   "FrmTerce.frx":215B
      End
      Begin Threed.SSCommand SCmd_Options 
         Height          =   735
         Index           =   1
         Left            =   120
         TabIndex        =   76
         Top             =   1560
         Width           =   855
         _Version        =   65536
         _ExtentX        =   1508
         _ExtentY        =   1296
         _StockProps     =   78
         Caption         =   "&BORRAR"
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   6
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Picture         =   "FrmTerce.frx":2825
      End
      Begin Threed.SSCommand SCmd_Options 
         Height          =   735
         Index           =   3
         Left            =   120
         TabIndex        =   78
         Top             =   3480
         Width           =   855
         _Version        =   65536
         _ExtentX        =   1508
         _ExtentY        =   1296
         _StockProps     =   78
         Caption         =   "&LISTAR"
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   6
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Picture         =   "FrmTerce.frx":2EEF
      End
      Begin Threed.SSCommand SCmd_Options 
         Height          =   735
         Index           =   4
         Left            =   120
         TabIndex        =   79
         Top             =   4440
         Width           =   855
         _Version        =   65536
         _ExtentX        =   1508
         _ExtentY        =   1296
         _StockProps     =   78
         Caption         =   "&SALIR"
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   6
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Picture         =   "FrmTerce.frx":35B9
      End
      Begin Threed.SSCommand SCmd_Options 
         Height          =   735
         Index           =   2
         Left            =   120
         TabIndex        =   77
         Top             =   2520
         Width           =   855
         _Version        =   65536
         _ExtentX        =   1508
         _ExtentY        =   1296
         _StockProps     =   78
         Caption         =   "S&ELECCION"
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   6
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Picture         =   "FrmTerce.frx":3C83
      End
   End
End
Attribute VB_Name = "FrmTerceros"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Dim Tabla As String
Dim Mensaje As String
Dim EncontroT As Boolean
Dim Dpto, Muni, Activ As String
Dim Cod_Dpto(), Cod_Municipio(), Cod_ActEcon() As Variant
Dim I As Long

Private Sub CboActividad_Click()
    CboActividad.ToolTipText = CboActividad.List(CboActividad.ListIndex)
End Sub

Private Sub CboActividad_GotFocus()
   'NMSR M3288
   CboActividad.Width = 3355
   Me.CmdSeleccion.Visible = True
   'NMSR M3288
End Sub

Private Sub CboActividad_KeyPress(KeyAscii As Integer)
    Debug.Print KeyAscii
    If KeyAscii = 27 Then CboActividad.ListIndex = -1
    Call Cambiar_Enter(KeyAscii)
End Sub

Private Sub CboDpto_KeyPress(KeyAscii As Integer)
    Call Cambiar_Enter(KeyAscii)
End Sub

Private Sub CboDpto_LostFocus()
If CboDpto.ListIndex > -1 Then
    Condicion = "CD_CODI_DPTO_MUNI='" & Cod_Dpto(CboDpto.ListIndex) & Comi & " ORDER BY NO_NOMB_MUNI"
    Result = loadctrl("MUNICIPIOS", "NO_NOMB_MUNI", "CD_CODI_MUNI", CboMuni, Cod_Municipio(), Condicion)
    If (Result = FAIL) Then
       Call Mensaje1("Error cargando la tabla [MUNICIPIOS]", 1)
       Exit Sub
    End If
End If
End Sub

Private Sub CboMuni_KeyPress(KeyAscii As Integer)
  Call Cambiar_Enter(KeyAscii)
End Sub


'HRR R1717
Private Sub CboRegimenIVA_KeyPress(KeyAscii As Integer)
   Call Cambiar_Enter(KeyAscii)
End Sub
'HRR R1717

Private Sub CboTipoDoc_KeyPress(KeyAscii As Integer)
  Call Cambiar_Enter(KeyAscii)
End Sub

'HRR R1717
Private Sub ChkAutoRetIca_KeyPress(KeyAscii As Integer)
   Call Cambiar_Enter(KeyAscii)
End Sub
'HRR R1717

'HRR R1717
Private Sub ChkAutoRetIva_KeyPress(KeyAscii As Integer)
   Call Cambiar_Enter(KeyAscii)
End Sub
'HRR R1717

'HRR R1717
Private Sub ChkAutoRetRenta_KeyPress(KeyAscii As Integer)
   Call Cambiar_Enter(KeyAscii)
End Sub
'HRR R1717

'HRR R1717
Private Sub ChkEstado_KeyPress(KeyAscii As Integer)
   Call Cambiar_Enter(KeyAscii)
End Sub
'HRR R1717

'HRR R1717
Private Sub ChkEstado_LostFocus()
   'SCmd_Options(0).SetFocus 'HRR M2114
   'HRR M2114
   If SCmd_Options(0).Enabled Then
      SCmd_Options(0).SetFocus
   Else
      If SCmd_Options(1).Enabled Then
         SCmd_Options(1).SetFocus
      Else
         SCmd_Options(2).SetFocus
      End If
   End If
   'HRR M2114
End Sub
'HRR R1717

'HRR R1717
Private Sub ChkExcento_KeyPress(Index As Integer, KeyAscii As Integer)
   Call Cambiar_Enter(KeyAscii)
End Sub
'HRR R1717

Private Sub ChkImpCREE_KeyPress(Index As Integer, KeyAscii As Integer)
   Call Cambiar_Enter(KeyAscii) 'OMOG R16934/T19113
End Sub

'HRR R1717
Private Sub ChkRegComun_KeyPress(Index As Integer, KeyAscii As Integer)
   Call Cambiar_Enter(KeyAscii)
End Sub
'HRR R1717



'HRR R1717
Private Sub ChkRegSimplif_KeyPress(Index As Integer, KeyAscii As Integer)
   Call Cambiar_Enter(KeyAscii)
End Sub
'HRR R1717

Private Sub ChkSocio_Click()
   If ChkSocio Then
      txtTercero(8).Enabled = True
      TxtValorAccion.Enabled = True  'JACC M4486
      txtTercero(8) = txtTercero(8).Tag 'OATC T36782
      TxtValorAccion = TxtValorAccion.Tag 'OATC T36782
   Else
      txtTercero(8).Enabled = False
      TxtValorAccion.Enabled = False 'JACC M4486
      txtTercero(8) = "0" 'OATC T36782
      TxtValorAccion = "0" 'OATC T36782
   End If
End Sub

'HRR R1717
Private Sub ChkSocio_KeyPress(KeyAscii As Integer)
   Call Cambiar_Enter(KeyAscii)
End Sub
'HRR R1717

Private Sub CmbTipoE_GotFocus()
   Msglin "Seleccione el Tipo de Empresa "
End Sub


Private Sub CmbTipoE_KeyPress(KeyAscii As Integer)
  Call Cambiar_Enter(KeyAscii)
End Sub

Private Sub CmbTipoEm_KeyPress(KeyAscii As Integer)
  Call Cambiar_Enter(KeyAscii)
End Sub
Private Sub CmbTipoP_GotFocus()
    Msglin "Seleccione el tipo de persona"
End Sub

Private Sub CmbTipoP_KeyPress(KeyAscii As Integer)
  Call Cambiar_Enter(KeyAscii)
End Sub
Private Sub CmbTipoP_LostFocus()
    Call Enable_name 'HRR R1717
    If CmbTipoP.ListIndex = 0 Then
       CmbTipoE.ListIndex = 4
       CmbTipoEm.ListIndex = 0
       TxtNom(0).SetFocus 'HRR M2114
    ElseIf CmbTipoP.ListIndex = 1 Then
       CmbTipoE.ListIndex = 0
       txtTercero(2).SetFocus 'HRR M2114
    End If
    
End Sub


'HRR M2114
'---------------------------------------------------------------------------------------
' Procedure : Enable_name
' DateTime  : 12/09/2007 08:22
' Author    : hector_rodriguez
' Purpose   : habilitar o inhabilitar los campos de nombre
' dependiendo del tipo de persona natural  o juridica
'---------------------------------------------------------------------------------------
'
'HRR M2114
'HRR R1717
Private Sub Enable_name()
   If CmbTipoP.ListIndex = 0 Then
      For I = 0 To 3
          LblNom(I).Enabled = True
          TxtNom(I).Enabled = True
       Next
       Lblnombre.Enabled = False
       txtTercero(2).Enabled = False
       txtTercero(2).Text = ""
   ElseIf CmbTipoP.ListIndex = 1 Then
      For I = 0 To 3
          LblNom(I).Enabled = False
          TxtNom(I).Enabled = False
          TxtNom(I).Text = ""
       Next
       Lblnombre.Enabled = True
       txtTercero(2).Enabled = True
   Else
      For I = 0 To 3
          LblNom(I).Enabled = False
          TxtNom(I).Enabled = False
          TxtNom(I).Text = ""
       Next
       Lblnombre.Enabled = False
       txtTercero(2).Enabled = False
       txtTercero(2).Text = ""
   End If
End Sub
'HRR R1717

Private Sub CmdAdelanta_Click()
    Dim Terc As String
    Dim StDigNit As String 'HRR M2115
    Terc = Avanzar_Registro(Tabla, "CD_CODI_TERC", NUL$, txtTercero(0))
    'smdl m2000
    'If Terc <> NUL$ And Terc <> "ZZZZZZZZZZZ" Then TxtTercero(0) = Terc: Call Buscar_Tercero
    If Terc <> NUL$ And Terc <> "ZZZZZZZZZZZ" Then
       txtTercero(0) = Terc
       Call Buscar_Tercero
       StDigNit = DigitoNIT(CStr(txtTercero(0))) 'HRR M2115
       If txtTercero(1) <> StDigNit Then txtTercero(1) = StDigNit 'HRR M2115
       End If
    'smdl
End Sub
Private Sub CmdAtras_Click()
Dim Terc As String
    Terc = Retroceder_Registro(Tabla, "CD_CODI_TERC", NUL$, txtTercero(0))
    If Terc <> NUL$ And Terc <> "ZZZZZZZZZZZ" Then txtTercero(0) = Terc: Call Buscar_Tercero
End Sub

Private Sub CmdLimpia_Click()
    Call Limpiar(True) 'HRR M2115
    txtTercero(0).SetFocus 'HRR R1717
End Sub

Private Sub CmdSeleccion_Click()
    'NMSR 3288
    'Codigo = Seleccion("ACT_ECONOMICA", "DE_DESC_ACEC", "CD_CODI_ACEC, DE_DESC_ACEC", "ACTIVIDADES ECONOMICAS", NUL$)
    Codigo = Seleccion("ACT_ECONOMICA", "DE_DESC_ACEC", "CD_CODI_ACEC, DE_DESC_ACEC", "ACTIVIDADES ECONOMICAS", "NU_ESTADO=1") 'JJRG 15628
    If Codigo <> NUL$ Then
      I = FindInArr(Cod_ActEcon, Codigo)
      CboActividad.ListIndex = I
    End If
    ChkSocio.SetFocus
    CboActividad.Width = 3855
    Me.CmdSeleccion.Visible = False
    'NMSR 3288
End Sub

Private Sub Form_Activate()
'HRR R1717
'    Result = loadctrl("DEPARTAMENTOS ORDER BY NO_NOMB_DPTO", "NO_NOMB_DPTO", "CD_CODI_DPTO", CboDpto, Cod_Dpto(), NUL$)
'    If (Result = FAIL) Then
'       Call Mensaje1("Error cargando la tabla [DEPARTAMENTOS]", 1)
'       Exit Sub
'    End If
'    If Dpto <> NUL$ Then
'      I = FindInArr(Cod_Dpto, Dpto)
'      CboDpto.ListIndex = I
'    End If
'
'    Result = loadctrl("ACT_ECONOMICA", "CD_CODI_ACEC +' '+DE_DESC_ACEC", "CD_CODI_ACEC", CboActividad, Cod_ActEcon(), NUL$)
'    If (Result = FAIL) Then
'       Call Mensaje1("Error cargando la tabla [ACT_ECONOMICA]", 1)
'       Exit Sub
'    End If
'    If Activ <> NUL$ Then
'      I = FindInArr(Cod_ActEcon, Activ)
'      CboActividad.ListIndex = I
'    End If
'
'    CboActividad.AddItem "0 NINGUNA"
'    I = UBound(Cod_ActEcon())
'    ReDim Preserve Cod_ActEcon(I + 1)
'    Cod_ActEcon(I + 1) = 0
'HRR R1717
End Sub

'HRR R1717
'Private Sub Form_LinkOpen(Cancel As Integer)

'End Sub
'HRR R1717

Private Sub Form_Load()
    
    Call CenterForm(MDI_Inventarios, Me) 'HRR R1717
    Call Limpiar(True) 'HRR M2115
    Tabla = "Tercero"
    Mensaje = "Tercero"
    'Call Leer_Permisos("14", SCmd_Options(0), SCmd_Options(1), SCmd_Options(3)) 'HRR R1872
    Call Leer_Permisos(Me.Name, SCmd_Options(0), SCmd_Options(1), SCmd_Options(3)) 'HRR R1872
    ''JACC M4486
    If ExisteTABLA("TERCERO") Then
        If Not ExisteCAMPO("TERCERO", "NU_VLACC_TERC") Then
            Result = CrearCampo("TERCERO", "NU_VLACC_TERC", 7, 15, 1)
        End If
    End If
    'JACC M4486
    Result = loadctrl("DEPARTAMENTOS ORDER BY NO_NOMB_DPTO", "NO_NOMB_DPTO", "CD_CODI_DPTO", CboDpto, Cod_Dpto(), NUL$)
    If (Result = FAIL) Then
       Call Mensaje1("Error cargando la tabla [DEPARTAMENTOS]", 1)
       Exit Sub
    End If
    
      If Dpto <> NUL$ Then
      I = FindInArr(Cod_Dpto, Dpto)
      CboDpto.ListIndex = I
     End If
    
    CboTipoDoc.ListIndex = 0
    ReDim Cod_ActEcon(0) 'HRR M2598
    
    'Result = loadctrl("ACT_ECONOMICA", "CD_CODI_ACEC +' '+DE_DESC_ACEC", "CD_CODI_ACEC", CboActividad, Cod_ActEcon(), NUL$)
    Result = loadctrl("ACT_ECONOMICA", "CD_CODI_ACEC +' '+DE_DESC_ACEC", "CD_CODI_ACEC", CboActividad, Cod_ActEcon(), "NU_ESTADO=1") 'JJRG 15628
    If (Result = FAIL) Then
       Call Mensaje1("Error cargando la tabla [ACTIVIDAD ECONOMICA]", 1)
       Exit Sub
    End If
    
     If Activ <> NUL$ Then
      I = FindInArr(Cod_ActEcon, Activ)
      CboActividad.ListIndex = I
    End If
    
    CboActividad.AddItem "0 NINGUNA"
    'I = UBound(Cod_ActEcon()) 'HRR M2598
    'HRR M2598
    If Cod_ActEcon(0) = Empty Then
      I = -1
    Else
      I = UBound(Cod_ActEcon)
    End If
    'HRR M2598
    ReDim Preserve Cod_ActEcon(I + 1)
    Cod_ActEcon(I + 1) = 0
    Activ = NUL$ 'nat
    Dpto = NUL$ 'nat
    
End Sub

Private Sub OptNo_KeyPress(KeyAscii As Integer)
  Call Cambiar_Enter(KeyAscii)
End Sub
Private Sub OptSi_KeyPress(KeyAscii As Integer)
  Call Cambiar_Enter(KeyAscii)
End Sub
Private Sub SCmd_Options_Click(Index As Integer)
Dim Terc As String
Encontro = EncontroT
    Select Case Index
        Case 0: If txtTercero(0) <> "ZZZZZZZZZZZ" Then Grabar
        Case 1: If txtTercero(0) <> "ZZZZZZZZZZZ" Then Borrar
        Case 2: Terc = Seleccion(Tabla, "NO_NOMB_TERC", "CD_CODI_TERC,NO_NOMB_TERC", "TERCEROS", "CD_CODI_TERC <> 'ZZZZZZZZZZZ'")
                If Terc <> NUL$ Then txtTercero(0) = Terc
                SSTab1.Tab = 0 'OMOG T19707
                txtTercero(0).SetFocus
                SCmd_Options(2).SetFocus
        Case 3: Call Listar("TERCEROS.rpt", "TERCERO", "CD_CODI_TERC", "NO_NOMB_TERC", "Terceros", NUL$, NUL$, NUL$, NUL$, NUL$)
                Me.SetFocus
        Case 4: Unload Me
    End Select
End Sub

Private Sub SCmd_Options_GotFocus(Index As Integer)
 Select Case Index
    Case 0: Msglin "Pulse este icono para grabar"
    Case 1: Msglin "Pulse este icono para borrar"
    Case 2: Msglin "Pulse este icono para selecci�n"
    Case 3: Msglin "Pulse este icono para generar un listado"
    Case 4: Msglin "Pulse este icono para cerrar la ventana "
  End Select
End Sub

'HRR R1717
Private Sub SCmd_Options_LostFocus(Index As Integer)
   Select Case Index
   Case 4:
      If SSTab1.Tab = 0 Then
         txtTercero(0).SetFocus
      Else
         CboRegimenIVA.SetFocus
      End If
   Case Else:
      
   End Select
   
End Sub
'HRR R1717

'HRR R1717
Private Sub SSTab1_LostFocus()
   If SSTab1.Tab = 1 Then CboRegimenIVA.SetFocus
End Sub
'HRR R1717

'HRR R1717
Private Sub TxtNom_GotFocus(Index As Integer)
   Select Case Index:
      Case 0: Msglin "Digite el primer nombre del " & Mensaje
      Case 1: Msglin "Digite el segundo nombre del " & Mensaje
      Case 2: Msglin "Digite el primer apellido del " & Mensaje
      Case 3: Msglin "Digite el segundo apellido del " & Mensaje
      Case Else:
   End Select
End Sub
'HRR R1717

'HRR R1717
Private Sub TxtNom_KeyPress(Index As Integer, KeyAscii As Integer)
   Call ValKeyAlfaNum(KeyAscii)
   Call Cambiar_Enter(KeyAscii)
End Sub
'HRR R1717

'HRR R1717
Private Sub TxtResICA_KeyPress(KeyAscii As Integer)
   Call Cambiar_Enter(KeyAscii)
End Sub
'HRR R1717

'HRR R1717
Private Sub TxtResIVA_KeyPress(KeyAscii As Integer)
   Call Cambiar_Enter(KeyAscii)
End Sub
'HRR R1717

'HRR R1717
Private Sub TxtResRENTA_KeyPress(KeyAscii As Integer)
   Call Cambiar_Enter(KeyAscii)
End Sub
'HRR R1717

Private Sub TxtTercero_GotFocus(Index As Integer)
Select Case Index
    Case 0: Msglin "Digite el Nit del " & Mensaje
    Case 1: Msglin "Digite la digito de verificaci�n del " & Mensaje
            If txtTercero(0) = NUL$ Then txtTercero(0).SetFocus: Exit Sub
    Case 2: Msglin "Digite la nombre del " & Mensaje
    Case 3: Msglin "Digite la direcci�n del " & Mensaje
            If txtTercero(0) = NUL$ Then txtTercero(0).SetFocus: Exit Sub
    Case 4: Msglin "Digite el tel�fono del " & Mensaje
    Case 5: Msglin "Digite la ciudad del " & Mensaje
    Case 6: Msglin "Digite la actividad econ�mica del " & Mensaje
    Case 7: Msglin "Digite el c�digo de la empresa"
    Case 9: Msglin "Digite el e-mail del usuario"
End Select

End Sub
Private Sub TxtTercero_KeyDown(Index As Integer, KeyCode As Integer, Shift As Integer)
Dim Terc As String
    Select Case Index
        Case 0:
                If KeyCode = 40 Then
                   txtTercero(1).SetFocus
                ElseIf KeyCode = 34 Then
                        Terc = Avanzar_Registro(Tabla, "CD_CODI_TERC", NUL$, txtTercero(0))
                        If Terc <> NUL$ And Terc <> "ZZZZZZZZZZZ" Then txtTercero(0) = Terc: Call Buscar_Tercero
                    Else
                       If KeyCode = 33 Then
                          Terc = Retroceder_Registro(Tabla, "CD_CODI_TERC", NUL$, txtTercero(0))
                          If Terc <> NUL$ And Terc <> "ZZZZZZZZZZZ" Then txtTercero(0) = Terc: Call Buscar_Tercero
                    End If
                End If
        Case 2:
                If KeyCode = 34 Then
                   Terc = Avanzar_Registro(Tabla, "CD_CODI_TERC", "NO_NOMB_TERC", txtTercero(2))
                   If Terc <> NUL$ And Terc <> "ZZZZZZZZZZZ" Then txtTercero(0) = Terc: Call Buscar_Tercero
                ElseIf KeyCode = 33 Then
                       Terc = Retroceder_Registro(Tabla, "CD_CODI_TERC", "NO_NOMB_TERC", txtTercero(2))
                       If Terc <> NUL$ And Terc <> "ZZZZZZZZZZZ" Then txtTercero(0) = Terc: Call Buscar_Tercero
                End If
        Case 6:
        Case 7: If KeyCode = 38 Then CmbTipoEm.SetFocus
        Case Else:
'                    If KeyCode = 38 Then
'                       TxtTercero(Index - 1).SetFocus
'                    ElseIf KeyCode = 40 Then
'                       TxtTercero(Index + 1).SetFocus
'                    End If
    End Select
End Sub
Private Sub TxtTercero_KeyPress(Index As Integer, KeyAscii As Integer)
  'NMSR M3339
  If Index = 0 Or Index = 1 Then
        If KeyAscii = 39 Then KeyAscii = 0:   Exit Sub
  End If 'NMSR M3339
  Call ValKeyAlfaNum(KeyAscii)
  Call Cambiar_Enter(KeyAscii)
End Sub
Private Sub TxtTercero_LostFocus(Index As Integer)
     If Index = 0 Or Index = 1 Then txtTercero(Index) = Replace(txtTercero(Index), "'", "") 'NMSR M3339
     Dim StDigNit As String 'HRR M2115
     Msglin NUL$
     Select Case Index
        Case 0: 'Reol Req.1307.
            If txtTercero(0) <> NUL$ And SSTab1.Tab = 0 Then
                'TxtTercero(1) = DigitoNIT(CStr(TxtTercero(0))) SMDL M2000
                Buscar_Tercero
                StDigNit = DigitoNIT(CStr(txtTercero(0))) 'HRR M2115
                If txtTercero(1) <> StDigNit Then txtTercero(1) = StDigNit 'HRR M2115
                If EncontroT = False Then If txtTercero(1) = NUL$ Then txtTercero(Index) = 0
            End If
        Case 2:
                If EncontroT = False Then
                    If txtTercero(1) = NUL$ Then txtTercero(1) = 0 'Reol Req.1307./NYCM M2818
                    'If TxtTercero(1) = "0" Then CmbTipoP.ListIndex = 0 Else CmbTipoP.ListIndex = 1
                End If
     End Select
End Sub

'Private Sub Buscar_Tercero()
'ReDim Arr(17)
'Dim CC(0), Dpt(0), Mun(0), Actividad(0)
'Dim I As Double
'
'   Condicion = "CD_CODI_TERC=" & Comi & TxtTercero(0) & Comi
'   Result = LoadData(Tabla, Asterisco, Condicion, Arr())
'   If (Result <> False) Then
'     If Encontro Then
'        TxtTercero(1) = Arr(1)
'        TxtTercero(2) = Arr(2)
'        TxtTercero(3) = Arr(4)
'        TxtTercero(4) = Arr(5)
'        TxtTercero(5) = Arr(6)
'        TxtTercero(6) = Arr(9)
'        TxtTercero(7) = Arr(11)
'        TxtTercero(9) = Arr(16)
'        CmbTipoP.ListIndex = IIf(Arr(3) = "", -1, Arr(3))
'        CmbTipoE.ListIndex = IIf(Arr(7) = "", -1, Arr(3))
'        CmbTipoEm.ListIndex = IIf(Arr(8) = "", -1, Arr(3))
'        If Arr(10) = "S" Then OptSi.Value = True Else OptNo.Value = True
'        If Arr(12) = "1" Then ChkSocio.Value = 1 Else ChkSocio.Value = 0
'        CboTipoDoc.ListIndex = IIf(Arr(13) <> NUL$, Arr(13), 0)
'
'       If IsNumeric(Arr(14)) Then
'           Dpto = Arr(14)
'           Result = LoadData("DEPARTAMENTOS", "NO_NOMB_DPTO", "CD_CODI_DPTO='" & Dpto & Comi, Dpt())
'           If Result = FAIL Or Dpt(0) = NUL$ Then
'           Else
'            If Arr(14) <> NUL$ Then
'              I = FindInArr(Cod_Dpto, Arr(14))
'              CboDpto.ListIndex = I
'              Condicion = "CD_CODI_DPTO_MUNI='" & Cod_Dpto(CboDpto.ListIndex) & Comi & " ORDER BY NO_NOMB_MUNI"
'              Result = LoadCtrl("MUNICIPIOS", "NO_NOMB_MUNI", "CD_CODI_MUNI", CboMuni, Cod_Municipio(), Condicion)
'              If (Result = FAIL) Then
'                 Call Mensaje1("Error cargando la tabla [MUNICIPIOS]", 1)
'                 Exit Sub
'              End If
'            End If
'           End If
'
'           If IsNumeric(Arr(6)) Then
'              Muni = Arr(6)
'              Condicion = "CD_CODI_MUNI='" & Muni & Comi
'              Condicion = Condicion & " AND CD_CODI_DPTO_MUNI='" & Cod_Dpto(CboDpto.ListIndex) & Comi
'              Result = LoadData("MUNICIPIOS", "NO_NOMB_MUNI", Condicion, Mun())
'              If Result = FAIL Or Mun(0) = NUL$ Then
'              Else
'               If Arr(6) <> NUL$ Then
'                 I = FindInArr(Cod_Municipio, Arr(6))
'                 CboMuni.ListIndex = I
'                End If
'              End If
'           End If
'       Else
'          CboDpto.ListIndex = -1: CboMuni.ListIndex = -1
'       End If
'       If IsNumeric(Arr(9)) Then
'          Activ = Arr(9)
'          Result = LoadData("ACT_ECONOMICA", "DE_DESC_ACEC", "CD_CODI_ACEC='" & Activ & Comi, Actividad())
'          If Result = FAIL Or Actividad(0) = NUL$ Then
'            If Arr(9) <> NUL$ Then CboActividad.ListIndex = UBound(Cod_ActEcon)
'          Else
'            If Arr(9) <> NUL$ Then
'              I = FindInArr(Cod_ActEcon, Arr(9))
'              CboActividad.ListIndex = I
'            End If
'          End If
'       Else
'          CboActividad.ListIndex = -1
'       End If
'       TxtTercero(8) = Arr(15)
'       TxtTercero(9) = Arr(16)
'       If Arr(17) = True Then ChkEstado.Value = 1 Else ChkEstado.Value = 0        'AGD
'       EncontroT = True
'     Else
'        For I = 1 To 9
'            TxtTercero(I) = NUL$
'        Next I
'        CmbTipoP.ListIndex = -1
'        CmbTipoE.ListIndex = -1
'        CmbTipoEm.ListIndex = -1
'        CboTipoDoc.ListIndex = -1
'        CboActividad.ListIndex = -1
'        CboDpto.ListIndex = -1
'        CboMuni.ListIndex = -1
'        ChkEstado.Value = 0
'        EncontroT = False
'     End If
'   End If
'End Sub

'OATC T36782 INICIO Se deja bloque de codigo en comentario
'Private Sub Buscar_Tercero()
'   Dim StTerc As String 'HRR R1717
''ReDim Arr(14) 'HRR R1717
''ReDim Arr(18) 'HRR R1717
'   ReDim arr(19)
'   Dim CC(0), Dpt(0), Mun(0), Actividad(0)
'   Dim I As Double
'   Campos = "NU_VERI_TERC,NO_NOMB_TERC,ID_TIPO_TERC,DE_DIRE_TERC,NU_TELE_TERC,"
'   Campos = Campos & "DE_CIUD_TERC,ID_ENTI_TERC,DE_ACTI_TERC,CD_ENTI_TERC,TX_MAIL_TERC,NU_TIPD_TERC,"
'   Campos = Campos & "DE_DPTO_TERC,ID_TIEM_TERC,NU_ACCI_TERC,NU_ESTADO_TERC"
'   Campos = Campos & ",TX_PNOM_TERC,TX_SNOM_TERC,TX_PAPE_TERC,TX_SAPE_TERC"
'   Campos = Campos & ",NU_VLACC_TERC"
'   Condicion = "CD_CODI_TERC=" & Comi & txtTercero(0) & Comi
'
'   Result = LoadData(Tabla, Campos, Condicion, arr())
'   StTerc = txtTercero(0) 'HRR R1717
'   If (Result <> False) Then
'      If Encontro Then
''         TxtTercero(1) = Arr(0) 'Reol Req.1307.
'         Call Limpiar(False) 'HRR M2115
'         txtTercero(0).Text = StTerc 'HRR R1717
'         If arr(0) <> "" Then txtTercero(1) = arr(0) 'M1514
'         txtTercero(2) = arr(1)
'         txtTercero(3) = arr(3)
'         txtTercero(4) = arr(4)
'         txtTercero(5) = arr(5)
'         txtTercero(6) = arr(7)
'         txtTercero(7) = arr(8)
'         txtTercero(9) = arr(9)
'         CmbTipoP.ListIndex = IIf(arr(2) = "", -1, arr(2))
'         CmbTipoE.ListIndex = IIf(arr(12) = "", -1, arr(12)) 'nat
'         CmbTipoEm.ListIndex = IIf(arr(6) = "", -1, arr(6)) 'nat
'         If arr(10) = "S" Then OptSi.Value = True Else OptNo.Value = True
'         If arr(12) = "1" Then ChkSocio.Value = 1 Else ChkSocio.Value = 0
'         CboTipoDoc.ListIndex = IIf(arr(10) <> NUL$, arr(10), 0)
'        'HRR R1717
'         Call Enable_name
'         For I = 15 To 18
'            TxtNom(I - 15) = arr(I)
'         Next
'        'HRR R1717
'         TxtValorAccion = IIf(arr(19) = NUL$, 0, arr(19)) 'JACC M4486
'        'AASV M3501
'         If CmbTipoP.ListIndex = 0 Then
'            txtTercero(2) = arr(1)
'         End If
'        'AASV M3501
'
'         If IsNumeric(arr(11)) Then
'            Dpto = arr(11)
'            Result = LoadData("DEPARTAMENTOS", "NO_NOMB_DPTO", "CD_CODI_DPTO='" & Dpto & Comi, Dpt())
'            If Result = FAIL Or Dpt(0) = NUL$ Then
'            Else
'               If arr(11) <> NUL$ Then
'                  I = FindInArr(Cod_Dpto, arr(11))
'                  CboDpto.ListIndex = I
'                  Condicion = "CD_CODI_DPTO_MUNI='" & Cod_Dpto(CboDpto.ListIndex) & Comi & " ORDER BY NO_NOMB_MUNI"
'                  Result = loadctrl("MUNICIPIOS", "NO_NOMB_MUNI", "CD_CODI_MUNI", CboMuni, Cod_Municipio(), Condicion)
'                  If (Result = FAIL) Then
'                     Call Mensaje1("Error cargando la tabla [MUNICIPIOS]", 1)
'                     Exit Sub
'                  End If
'               End If
'            End If
'
'            If IsNumeric(arr(5)) And CboDpto.ListIndex > -1 Then
'               Muni = arr(5)
'               Condicion = "CD_CODI_MUNI='" & Muni & Comi
'               Condicion = Condicion & " AND CD_CODI_DPTO_MUNI='" & Cod_Dpto(CboDpto.ListIndex) & Comi
'               Result = LoadData("MUNICIPIOS", "NO_NOMB_MUNI", Condicion, Mun())
'               If Result = FAIL Or Mun(0) = NUL$ Then
'            Else
'               If arr(5) <> NUL$ Then
'                  I = FindInArr(Cod_Municipio, arr(5))
'                  CboMuni.ListIndex = I
'               End If
'            End If
'         End If
'      Else
'         CboDpto.ListIndex = -1: CboMuni.ListIndex = -1
'      End If
'      If IsNumeric(arr(7)) Then
'         Activ = arr(7)
'         Result = LoadData("ACT_ECONOMICA", "DE_DESC_ACEC", "CD_CODI_ACEC='" & Activ & Comi, Actividad())
'         If Result = FAIL Or Actividad(0) = NUL$ Then
'            If arr(7) <> NUL$ Then CboActividad.ListIndex = UBound(Cod_ActEcon) 'nat
'         Else
'            If arr(7) <> NUL$ Then
'               I = FindInArr(Cod_ActEcon, arr(7))
'               CboActividad.ListIndex = I
'            End If
'         End If
'      Else
'         CboActividad.ListIndex = -1
'      End If
'      txtTercero(8) = arr(13)
'      txtTercero(9) = arr(9)
'      If arr(14) = True Then ChkEstado.Value = 1 Else ChkEstado.Value = 0
'
'      Call Cargar_Datos_Tributarios
'      EncontroT = True
'   Else
'        'HRR R1717
''        For I = 2 To 9 'Reol Req.1307. Comienza I desde 2.
''            TxtTercero(I) = NUL$
''        Next I
''        CmbTipoP.ListIndex = -1
''        CmbTipoE.ListIndex = -1
''        CmbTipoEm.ListIndex = -1
''        CboTipoDoc.ListIndex = -1
''        CboActividad.ListIndex = -1
''        CboDpto.ListIndex = -1
''        CboMuni.ListIndex = -1
''        ChkEstado.Value = 0
'        'HRR R1717
'        'HRR R1717
'      Call Limpiar(True) 'HRR M2115
'      txtTercero(0).Text = StTerc
'        'HRR R1717
'      EncontroT = False
'      End If
'   End If
'---------------------------------------------------------------------------------------
' Procedure : Buscar_Tercero
' DateTime  : 10/10/2016 03:48
' Author    : ozle_torres
' Purpose   : OATC T36782 Consulta los datos del tercero ingresado
'---------------------------------------------------------------------------------------
'
Private Sub Buscar_Tercero()
   Dim StTerc As String 'Almacena el codigo del tercero ingresado por si no existe volver a cargarlo en el control
   Dim VrArr() As Variant 'Almacena los datos retornados por la consulta
   
   ReDim VrArr(22)
   Campos = "CD_CODI_TERC,NU_VERI_TERC,NU_TIPD_TERC,ID_TIPO_TERC,TX_PNOM_TERC,"
   Campos = Campos & "TX_SNOM_TERC,TX_PAPE_TERC,TX_SAPE_TERC,NO_NOMB_TERC,DE_DIRE_TERC,"
   Campos = Campos & "NU_TELE_TERC,TX_MAIL_TERC,DE_DPTO_TERC,DE_CIUD_TERC,DE_ACTI_TERC,"
   Campos = Campos & "ID_SOCI_TERC,NU_ACCI_TERC,ID_TIEM_TERC,ID_ENTI_TERC,CD_ENTI_TERC,"
   Campos = Campos & "NU_VLACC_TERC,NU_ESTADO_TERC,ID_RETE_TERC"
   Condicion = "CD_CODI_TERC=" & Comi & Trim(txtTercero(0)) & Comi
   Result = LoadData(Tabla, Campos, Condicion, VrArr)
   StTerc = txtTercero(0)
   If Result <> False And Encontro Then
      Call Limpiar(False)
      txtTercero(0) = VrArr(0)
      txtTercero(1) = VrArr(1)
      'JLPB T37994 INICIO Se deja en comentario las siguientes lineas
      'CboTipoDoc.ListIndex = VrArr(2)
      'CmbTipoP.ListIndex = VrArr(3)
      CboTipoDoc.ListIndex = Val(VrArr(2))
      CmbTipoP.ListIndex = IIf(VrArr(3) = NUL$, -1, VrArr(3))
      'JLPB T37994 FIN
      TxtNom(0) = VrArr(4)
      TxtNom(1) = VrArr(5)
      TxtNom(2) = VrArr(6)
      TxtNom(3) = VrArr(7)
      txtTercero(2) = VrArr(8)
      txtTercero(3) = VrArr(9)
      txtTercero(4) = VrArr(10)
      txtTercero(9) = VrArr(11)
      CboDpto.ListIndex = FindInArr(Cod_Dpto, VrArr(12))
      If CboDpto.ListIndex > -1 Then 'JLPB T37994
         Call CboDpto_LostFocus
         CboMuni.ListIndex = FindInArr(Cod_Municipio, VrArr(13))
      End If 'JLPB T37994
      CboActividad.ListIndex = FindInArr(Cod_ActEcon, VrArr(14))
      ChkSocio.Value = Val(VrArr(15))
      txtTercero(8) = IIf(Val(VrArr(15)), VrArr(16), 0)
      txtTercero(8).Tag = VrArr(16)
      'JLPB T37994 INICIO Se deja en comentario las siguientes lineas
      'CmbTipoE.ListIndex = VrArr(17)
      'CmbTipoEm.ListIndex = VrArr(18)
      CmbTipoE.ListIndex = IIf(VrArr(17) = NUL$, -1, VrArr(17))
      CmbTipoEm.ListIndex = IIf(VrArr(18) = NUL$, -1, VrArr(18))
      'JLPB T37994 FIN
      txtTercero(7) = VrArr(19)
      TxtValorAccion = IIf(Val(VrArr(15)), VrArr(20), 0)
      TxtValorAccion.Tag = VrArr(20)
      ChkEstado.Value = Val(VrArr(21))
      If VrArr(22) = "S" Then OptSi.Value = True Else OptNo.Value = False
      Call Cargar_Datos_Tributarios
      EncontroT = True
   Else
      Call Limpiar(True)
      txtTercero(0) = StTerc
      EncontroT = False
   End If
'OATC T36782 FIN
End Sub

Private Sub Grabar()
   DoEvents
   
   Call MouseClock
   Msglin "Ingresando informaci�n del " & Mensaje

   'HRR R1717
'   If (TxtTercero(0) = NUL$ Or TxtTercero(2) = NUL$) Then
'      Call Mensaje1("Se debe especificar los datos completos del " & Mensaje, 3)
'      Call MouseNorm: Exit Sub
'   End If
   'HRR R1717
      
   'HRR R1717
   If (txtTercero(0) = NUL$) Then
      Call Mensaje1("Se debe especificar el nit del " & Mensaje, 3)
      Call MouseNorm: Exit Sub
   End If
   'HRR R1717
   
   
   If CmbTipoP.ListIndex = -1 Then
      Call Mensaje1("Debe seleccionar el tipo de persona", 3)
      CmbTipoP.SetFocus
      Call MouseNorm: Exit Sub
   End If
   
   'HRR R1717
   If CmbTipoP.ListIndex = 0 Then
      If TxtNom(0) = NUL$ Then
         Call Mensaje1("Se debe especificar el primer nombre del " & Mensaje, 3)
         TxtNom(0).SetFocus
         Call MouseNorm: Exit Sub
      End If
      If TxtNom(2) = NUL$ Then
         Call Mensaje1("Se debe especificar el primer apellido del " & Mensaje, 3)
         TxtNom(2).SetFocus
         Call MouseNorm: Exit Sub
      End If
      txtTercero(2).Text = TxtNom(2).Text & " " & TxtNom(3) & " " & TxtNom(0).Text & " " & TxtNom(1).Text
   End If
   'HRR R1717
   
   'HRR R1717
   If (txtTercero(2) = NUL$) Then
      Call Mensaje1("Se debe especificar razon social del " & Mensaje, 3)
      Call MouseNorm: Exit Sub
   End If
   'HRR R1717
   
   If CmbTipoE.ListIndex = -1 Or CmbTipoEm.ListIndex = -1 Then
      Call Mensaje1("Debe seleccionar el tipo de entidad", 3)
      CmbTipoE.SetFocus
      Call MouseNorm: Exit Sub
   End If

   If CboTipoDoc.ListIndex = -1 Then
      Call Mensaje1("Debe seleccionar el Tipo de Documento", 3)
      CboTipoDoc.SetFocus
      Call MouseNorm: Exit Sub
   End If
   If CboDpto.ListIndex = -1 Then
       Call Mensaje1("Seleccione el Departamento!!!", 3)
       CboDpto.SetFocus
       Call MouseNorm: Exit Sub
   End If
   If CboMuni.ListIndex = -1 Then
       Call Mensaje1("Seleccione el Municipio!!!", 3)
       CboMuni.SetFocus
       Call MouseNorm: Exit Sub
   End If
   
   'Si es persona natural no obliga a seleccionar Act economica
     If CboActividad.ListIndex = -1 Then
       Call Mensaje1("Debe seleccionar la Actividad Economica", 3)
       CboActividad.SetFocus
       Call MouseNorm: Exit Sub
     End If
   
   If ChkSocio And txtTercero(8) = NUL$ Then
      Call Mensaje1("Debe especificar el n�mero de acciones", 3)
      txtTercero(8).SetFocus
      Call MouseNorm: Exit Sub
   End If
   
   If CboRegimenIVA.ListIndex = -1 Then
     Call Mensaje1("Seleccione el Tipo de Regimen de IVA.", 3)
     Call MouseNorm: Exit Sub
   End If
   
   If (BeginTran(STranIUp & Tabla) <> FAIL) Then
       If (Not Encontro) Then
          Valores = "CD_CODI_TERC = " & Comi & Cambiar_Comas_Comillas(txtTercero(0)) & Comi & Coma
          Valores = Valores & "NU_VERI_TERC = " & Comi & Cambiar_Comas_Comillas(txtTercero(1)) & Comi & Coma
          Valores = Valores & "NO_NOMB_TERC = " & Comi & Cambiar_Comas_Comillas(txtTercero(2)) & Comi & Coma
          Valores = Valores & "ID_TIPO_TERC = " & CmbTipoP.ListIndex & Coma
          Valores = Valores & "DE_DIRE_TERC = " & Comi & Cambiar_Comas_Comillas(txtTercero(3)) & Comi & Coma
          Valores = Valores & "NU_TELE_TERC = " & Comi & Cambiar_Comas_Comillas(txtTercero(4)) & Comi & Coma
          Valores = Valores & "DE_CIUD_TERC = " & Comi & Cod_Municipio(CboMuni.ListIndex) & Comi & Coma
          Valores = Valores & "ID_TIEM_TERC = " & CmbTipoE.ListIndex & Coma
          Valores = Valores & "ID_ENTI_TERC = " & CmbTipoEm.ListIndex & Coma
          Valores = Valores & "DE_ACTI_TERC = " & Comi & Cod_ActEcon(CboActividad.ListIndex) & Comi & Coma
          Valores = Valores & "ID_RETE_TERC = " & Comi & IIf(OptSi.Value = True, "S", "N") & Comi & Coma
          Valores = Valores & "CD_ENTI_TERC = " & Comi & Cambiar_Comas_Comillas(txtTercero(7)) & Comi & Coma
          Valores = Valores & "ID_SOCI_TERC = " & Comi & ChkSocio.Value & Comi & Coma
          Valores = Valores & "NU_TIPD_TERC = " & Comi & CboTipoDoc.ListIndex & Comi & Coma
          Valores = Valores & "DE_DPTO_TERC = " & Comi & Cod_Dpto(CboDpto.ListIndex) & Comi
          Valores = Valores & Coma & "NU_ACCI_TERC=" & Val(txtTercero(8)) & Coma & "TX_MAIL_TERC="                         'PedroJ->DIAN
          Valores = Valores & Comi & Cambiar_Comas_Comillas(txtTercero(9)) & Comi               'AGD
          Valores = Valores & Coma & "NU_ESTADO_TERC=" & ChkEstado.Value          'AGD
          'HRR R1717
          Valores = Valores & Coma & "TX_PNOM_TERC=" & Comi & Cambiar_Comas_Comillas(TxtNom(0).Text) & Comi
          Valores = Valores & Coma & "TX_SNOM_TERC=" & Comi & Cambiar_Comas_Comillas(TxtNom(1).Text) & Comi
          Valores = Valores & Coma & "TX_PAPE_TERC=" & Comi & Cambiar_Comas_Comillas(TxtNom(2).Text) & Comi
          Valores = Valores & Coma & "TX_SAPE_TERC=" & Comi & Cambiar_Comas_Comillas(TxtNom(3).Text) & Comi
          'HRR R1717
          Valores = Valores & Coma & "NU_VLACC_TERC = " & IIf(TxtValorAccion = NUL$, 0, TxtValorAccion) 'JACC M4486
          Result = DoInsertSQL(Tabla, Valores)
          If (Result <> FAIL) Then Result = Auditor(Tabla, TranIns, LastCmd)
          If (Result <> FAIL) Then
             Valores = "CD_CODI_TERC_PAIM =" & Comi & Cambiar_Comas_Comillas(txtTercero(0)) & Comi & Coma
             Valores = Valores & "ID_TIPO_TERC_PAIM =" & Comi & CmbTipoP.ListIndex & Comi & Coma
             Valores = Valores & "ID_TIPO_REGI_PAIM =" & Comi & CboRegimenIVA.ListIndex & Comi & Coma
             Valores = Valores & "ID_TIPO_CONTR_PAIM =" & Comi & IIf(OptSi.Value = True, "1", "0") & Comi & Coma
             Valores = Valores & "ID_AUTO_RETIVA_PAIM =" & Comi & ChkAutoRetIva.Value & Comi & Coma
             Valores = Valores & "ID_AUTO_RETICA_PAIM =" & Comi & ChkAutoRetIca.Value & Comi & Coma
             Valores = Valores & "ID_AUTO_RETREN_PAIM =" & Comi & ChkAutoRetRenta.Value & Comi & Coma
             Valores = Valores & "TX_RESO_ARETIVA_PAIM =" & Comi & Me.TxtResIVA & Comi & Coma
             Valores = Valores & "TX_RESO_ARETICA_PAIM =" & Comi & TxtResICA & Comi & Coma
             Valores = Valores & "TX_RESO_ARETREN_PAIM =" & Comi & TxtResRENTA & Comi & Coma
             Valores = Valores & "ID_AREIVA_REGCOM_PAIM =" & Comi & ChkRegComun(0).Value & Comi & Coma
             Valores = Valores & "ID_AREIVA_REGSIM_PAIM =" & Comi & ChkRegSimplif(0).Value & Comi & Coma
             Valores = Valores & "ID_AREICA_REGCOM_PAIM =" & Comi & ChkRegComun(1).Value & Comi & Coma
             Valores = Valores & "ID_AREICA_REGSIM_PAIM =" & Comi & ChkRegSimplif(1).Value & Comi & Coma
             Valores = Valores & "ID_AREREN_REGCOM_PAIM =" & Comi & ChkRegComun(2).Value & Comi & Coma
             Valores = Valores & "ID_AREREN_REGSIM_PAIM =" & Comi & ChkRegSimplif(2).Value & Comi & Coma
             Valores = Valores & "ID_EXERET_FUEN_PAIM=" & Comi & ChkExcento(0).Value & Comi & Coma
             Valores = Valores & "ID_EXERET_ICA_PAIM=" & Comi & ChkExcento(1).Value & Comi & Coma
             Valores = Valores & "ID_EXERET_IVA_PAIM=" & Comi & ChkExcento(2).Value & Comi & Coma
             Valores = Valores & "TX_RESO_CREE_PAIM=" & Comi & ChkImpCREE(3).Value & Comi 'OMOG R16934/T19113
             Result = DoInsertSQL("PARAMETROS_IMPUESTOS", Valores)
          End If
       Else
         If WarnMsg("Desea Modificar la Informaci�n del " & Mensaje, 256) Then
            Valores = "NU_VERI_TERC=" & Comi & Cambiar_Comas_Comillas(txtTercero(1)) & Comi & Coma
            Valores = Valores & " NO_NOMB_TERC=" & Comi & Cambiar_Comas_Comillas(txtTercero(2)) & Comi & Coma
            Valores = Valores & " ID_TIPO_TERC=" & CmbTipoP.ListIndex & Coma
            Valores = Valores & " DE_DIRE_TERC=" & Comi & Cambiar_Comas_Comillas(txtTercero(3)) & Comi & Coma
            Valores = Valores & " NU_TELE_TERC=" & Comi & Cambiar_Comas_Comillas(txtTercero(4)) & Comi & Coma
            Valores = Valores & " DE_CIUD_TERC=" & Comi & Cod_Municipio(CboMuni.ListIndex) & Comi & Coma
            Valores = Valores & " ID_TIEM_TERC=" & CmbTipoE.ListIndex & Coma
            Valores = Valores & " ID_ENTI_TERC=" & CmbTipoEm.ListIndex & Coma
            Valores = Valores & " DE_ACTI_TERC=" & Comi & Cod_ActEcon(CboActividad.ListIndex) & Comi & Coma
            Valores = Valores & " ID_RETE_TERC=" & Comi & IIf(OptSi.Value = True, "S", "N") & Comi & Coma
            Valores = Valores & " CD_ENTI_TERC=" & Comi & Cambiar_Comas_Comillas(txtTercero(7)) & Comi
            Valores = Valores & Coma & "ID_SOCI_TERC=" & Comi & ChkSocio.Value & Comi
            Valores = Valores & Coma & "NU_TIPD_TERC=" & Comi & CboTipoDoc.ListIndex & Comi
            Valores = Valores & Coma & "DE_DPTO_TERC=" & Comi & Cod_Dpto(CboDpto.ListIndex) & Comi
            Valores = Valores & Coma & "NU_ACCI_TERC=" & Val(txtTercero(8))                            'PedroJ->DIAN
            Valores = Valores & Coma & "TX_MAIL_TERC=" & Comi & Cambiar_Comas_Comillas(txtTercero(9)) & Comi   'AGD
            Valores = Valores & Coma & "NU_ESTADO_TERC=" & ChkEstado.Value                   'AGD
            'HRR R1717
            Valores = Valores & Coma & "TX_PNOM_TERC=" & Comi & Cambiar_Comas_Comillas(TxtNom(0).Text) & Comi
            Valores = Valores & Coma & "TX_SNOM_TERC=" & Comi & Cambiar_Comas_Comillas(TxtNom(1).Text) & Comi
            Valores = Valores & Coma & "TX_PAPE_TERC=" & Comi & Cambiar_Comas_Comillas(TxtNom(2).Text) & Comi
            Valores = Valores & Coma & "TX_SAPE_TERC=" & Comi & Cambiar_Comas_Comillas(TxtNom(3).Text) & Comi
            'HRR R1717
            Valores = Valores & Coma & "NU_VLACC_TERC = " & IIf(TxtValorAccion = NUL$, 0, TxtValorAccion) 'JACC M4486
            Condicion = "CD_CODI_TERC=" & Comi & txtTercero(0) & Comi
            
            Result = DoUpdate(Tabla, Valores, Condicion)
            If (Result <> FAIL) Then Result = Auditor(Tabla, TranUpd, LastCmd)
            If (Result <> FAIL) Then
                Condicion = "CD_CODI_TERC_PAIM =" & Comi & Cambiar_Comas_Comillas(txtTercero(0)) & Comi
                Valores = "ID_TIPO_TERC_PAIM =" & Comi & CmbTipoP.ListIndex & Comi & Coma
                Valores = Valores & "ID_TIPO_REGI_PAIM =" & Comi & CboRegimenIVA.ListIndex & Comi & Coma
                Valores = Valores & "ID_TIPO_CONTR_PAIM =" & Comi & IIf(OptSi.Value = True, "1", "0") & Comi & Coma
                Valores = Valores & "ID_AUTO_RETIVA_PAIM =" & Comi & ChkAutoRetIva.Value & Comi & Coma
                Valores = Valores & "ID_AUTO_RETICA_PAIM =" & Comi & ChkAutoRetIca.Value & Comi & Coma
                Valores = Valores & "ID_AUTO_RETREN_PAIM =" & Comi & ChkAutoRetRenta.Value & Comi & Coma
                Valores = Valores & "TX_RESO_ARETIVA_PAIM =" & Comi & Me.TxtResIVA & Comi & Coma
                Valores = Valores & "TX_RESO_ARETICA_PAIM =" & Comi & TxtResICA & Comi & Coma
                Valores = Valores & "TX_RESO_ARETREN_PAIM =" & Comi & TxtResRENTA & Comi & Coma
                Valores = Valores & "ID_AREIVA_REGCOM_PAIM =" & Comi & ChkRegComun(0).Value & Comi & Coma
                Valores = Valores & "ID_AREIVA_REGSIM_PAIM =" & Comi & ChkRegSimplif(0).Value & Comi & Coma
                Valores = Valores & "ID_AREICA_REGCOM_PAIM =" & Comi & ChkRegComun(1).Value & Comi & Coma
                Valores = Valores & "ID_AREICA_REGSIM_PAIM =" & Comi & ChkRegSimplif(1).Value & Comi & Coma
                Valores = Valores & "ID_AREREN_REGCOM_PAIM =" & Comi & ChkRegComun(2).Value & Comi & Coma
                Valores = Valores & "ID_AREREN_REGSIM_PAIM =" & Comi & ChkRegSimplif(2).Value & Comi & Coma
                Valores = Valores & "ID_EXERET_FUEN_PAIM=" & Comi & ChkExcento(0).Value & Comi & Coma
                Valores = Valores & "ID_EXERET_ICA_PAIM=" & Comi & ChkExcento(1).Value & Comi & Coma
                Valores = Valores & "ID_EXERET_IVA_PAIM=" & Comi & ChkExcento(2).Value & Comi
                Valores = Valores & ",TX_RESO_CREE_PAIM=" & Comi & ChkImpCREE(3).Value & Comi 'OMOG R16934/T19113
                'REOL M2303
                ReDim Arr(0)
                Result = LoadData("PARAMETROS_IMPUESTOS", "CD_CODI_TERC_PAIM", Condicion, Arr)
                If Result <> FAIL And Not Encontro Then
                   Result = DoInsertSQL("PARAMETROS_IMPUESTOS", Condicion & ", " & Valores)
                Else
                   Result = DoUpdate("PARAMETROS_IMPUESTOS", Valores, Condicion)
                End If
                'REOL M2303
            End If
         End If
       End If
   End If
   If (Result <> FAIL) Then
      If (CommitTran() <> FAIL) Then
          Limpiar (True) 'HRR M2115
          txtTercero(0).SetFocus 'HRR R1717
      Else
         Call RollBackTran
      End If
   Else
      Call RollBackTran
   End If
   Call MouseNorm
   Msglin NUL$
End Sub

Private Sub Borrar()
   DoEvents
   
   Call MouseClock
   
   Msglin "Borrando un " & Mensaje
   If (txtTercero(0) = NUL$) Then
       Call Mensaje1("Ingrese el c�digo del " & Mensaje, 1)
       Call MouseNorm: Msglin NUL$: Exit Sub
   End If
   If (Encontro <> False) Then
       If fnValidarCierreAnual Then Result = FAIL: Exit Sub  'JACC M3739
       If (Not WarnDel()) Then Call MouseNorm: Msglin NUL$: Exit Sub
       'REOL M2425
       ReDim Arr(0)
       Condicion = "CD_TERC_RCT=" & Comi & Cambiar_Comas_Comillas(txtTercero(0)) & Comi
       Result = LoadData("CUENTA_TERCERO", "CD_TERC_RCT", Condicion, Arr)
       If Result <> FAIL And Encontro Then Call Mensaje1("El Tercero tiene cuentas relacionadas y no se puede borrar", 3): Call MouseNorm: Exit Sub
       'REOL M2425
       Condicion = "CD_CODI_TERC=" & Comi & Cambiar_Comas_Comillas(txtTercero(0)) & Comi
   
        If (BeginTran(STranDel & Tabla) <> FAIL) Then
            Result = DoDelete(Tabla, Condicion)
            Condicion = "CD_CODI_TERC_PAIM=" & Comi & Cambiar_Comas_Comillas(txtTercero(0)) & Comi
            If (Result <> FAIL) Then Result = DoDelete("PARAMETROS_IMPUESTOS", Condicion)
            If (Result <> FAIL) Then Result = Auditor(Tabla, TranDel, LastCmd)
        End If
   Else
       Call Mensaje1("No se pudo borrar el " & Mensaje & " de la base de datos, debido a que no existe !!!...", 1)
       Call MouseNorm: Msglin NUL$: Exit Sub
   End If
   If (Result <> FAIL) Then
       If (CommitTran() <> FAIL) Then
          Limpiar (True) 'HRR M2115
          txtTercero(0).SetFocus 'HRR R1717
       Else
          Call RollBackTran
       End If
   Else
       Call RollBackTran
   End If
   Call MouseNorm
   Msglin NUL$
End Sub
Private Sub Limpiar(ByVal BoBand As Boolean)
Dim I As Byte
    For I = 0 To 9
        txtTercero(I) = NUL$
    Next I
        
    'HRR R1717
    'CmbTipoP.ListIndex = -1
    'CmbTipoE.ListIndex = -1
    'CmbTipoEm.ListIndex = -1
    'CboTipoDoc.ListIndex = -1
    'HRR R1717
    CboActividad.ListIndex = -1
    CboDpto.ListIndex = -1
    CboMuni.ListIndex = -1
    ChkSocio.Value = 0
    ChkEstado.Value = 0                 'AGD
    
    'CboRegimenIVA.ListIndex = -1
    ChkAutoRetIva.Value = 0: ChkAutoRetIca.Value = 0: ChkAutoRetRenta.Value = 0:
    ChkExcento(0).Value = 0: ChkExcento(1).Value = 0: ChkExcento(2).Value = 0
    ChkRegComun(0).Value = 0: ChkRegComun(1).Value = 0: ChkRegComun(2).Value = 0
    ChkRegSimplif(0).Value = 0: ChkRegSimplif(1).Value = 0: ChkRegSimplif(2).Value = 0
    TxtResICA = NUL$: TxtResIVA = NUL$: TxtResRENTA = NUL$
    SSTab1.Tab = 0
    'TxtTercero(0).SetFocus 'HRR R1717
    TxtValorAccion = NUL$   'JACC M4486
    'HRR R1717
    For I = 0 To 3
       TxtNom(I) = NUL$
    Next
    CmbTipoP.ListIndex = 0
    CboTipoDoc.ListIndex = 0
    CmbTipoE.ListIndex = 4
    CmbTipoEm.ListIndex = 0
    CboRegimenIVA.ListIndex = 0
    ChkImpCREE(3).Value = 0 'OMOG T19706
    If BoBand Then Call Enable_name
    'HRR R1717
    
End Sub

Private Sub Cargar_Datos_Tributarios()
'ReDim Arr(17) 'OMOG R16934/T19113 SE ESTABLECE EN COMENTARIO
ReDim Arr(18)  'OMOG R16934/T19113
    Campos = "ID_TIPO_TERC_PAIM,ID_TIPO_REGI_PAIM,ID_TIPO_CONTR_PAIM,ID_AUTO_RETIVA_PAIM,"
    Campos = Campos & "ID_AUTO_RETICA_PAIM,ID_AUTO_RETREN_PAIM,TX_RESO_ARETIVA_PAIM,TX_RESO_ARETICA_PAIM,"
    Campos = Campos & "TX_RESO_ARETREN_PAIM,ID_AREIVA_REGCOM_PAIM,ID_AREIVA_REGSIM_PAIM,ID_AREICA_REGCOM_PAIM,"
    Campos = Campos & "ID_AREICA_REGSIM_PAIM,ID_AREREN_REGCOM_PAIM,ID_AREREN_REGSIM_PAIM,"
    Campos = Campos & "ID_EXERET_FUEN_PAIM,ID_EXERET_ICA_PAIM,ID_EXERET_IVA_PAIM"
    Campos = Campos & ", TX_RESO_CREE_PAIM" 'OMOG R16934/T19113
    Condicion = "CD_CODI_TERC_PAIM=" & Comi & Cambiar_Comas_Comillas(txtTercero(0)) & Comi
    Result = LoadData("PARAMETROS_IMPUESTOS", Campos, Condicion, Arr)
    If Result <> FAIL Then
        'If Arr(0) <> NUL$ Then CmbTipoP.ListIndex = Arr(0) 'HRR M2664 el tipo de persona se define en la tabla tercero, no en esta tabla
        If Arr(1) <> NUL$ Then CboRegimenIVA.ListIndex = Arr(1)
        If Arr(2) = "1" Then OptSi.Value = True Else OptNo.Value = True
        ChkAutoRetIva.Value = IIf(Arr(3) <> NUL$, Arr(3), 0)
        ChkAutoRetIca.Value = IIf(Arr(4) <> NUL$, Arr(4), 0)
        ChkAutoRetRenta.Value = IIf(Arr(5) <> NUL$, Arr(5), 0)
        TxtResIVA = Arr(6)
        TxtResICA = Arr(7)
        TxtResRENTA = Arr(8)
        ChkRegComun(0).Value = IIf(Arr(9) <> NUL$, Arr(9), 0)
        ChkRegSimplif(0).Value = IIf(Arr(10) <> NUL$, Arr(10), 0)
        ChkRegComun(1).Value = IIf(Arr(11) <> NUL$, Arr(11), 0)
        ChkRegSimplif(1).Value = IIf(Arr(12) <> NUL$, Arr(12), 0)
        ChkRegComun(2).Value = IIf(Arr(13) <> NUL$, Arr(13), 0)
        ChkRegSimplif(2).Value = IIf(Arr(14) <> NUL$, Arr(14), 0)
        ChkExcento(0).Value = IIf(Arr(15) <> NUL$, Arr(15), 0)
        ChkExcento(1).Value = IIf(Arr(16) <> NUL$, Arr(16), 0)
        ChkExcento(2).Value = IIf(Arr(17) <> NUL$, Arr(17), 0)
        ChkImpCREE(3).Value = IIf(Arr(18) <> NUL$, Arr(18), 0) 'OMOG R16934/T19113
    End If
End Sub


Private Sub TxtValorAccion_KeyPress(KeyAscii As Integer) 'JACC M4486
   Call ValKeyNum(KeyAscii)
    If KeyAscii = 46 Then KeyAscii = 0
    Call Cambiar_Enter(KeyAscii)
End Sub
