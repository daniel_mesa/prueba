VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "MSFLXGRD.OCX"
Object = "{0BA686C6-F7D3-101A-993E-0000C0EF6F5E}#1.0#0"; "THREED32.OCX"
Object = "{6B7E6392-850A-101B-AFC0-4210102A8DA7}#1.3#0"; "COMCTL32.OCX"
Object = "{65E121D4-0C60-11D2-A9FC-0000F8754DA1}#2.0#0"; "mschrt20.ocx"
Begin VB.Form FrmGENERAL 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "FrmGENERAL"
   ClientHeight    =   8460
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   10590
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   ScaleHeight     =   8460
   ScaleWidth      =   10590
   Begin VB.Frame frmBAJA 
      Caption         =   "Baja"
      Height          =   2175
      Left            =   1920
      TabIndex        =   22
      Top             =   3960
      Visible         =   0   'False
      Width           =   4455
      Begin VB.CommandButton cmdPROCEDER 
         Caption         =   "Proceder"
         Height          =   375
         Index           =   8
         Left            =   1440
         TabIndex        =   158
         Top             =   1560
         Width           =   1695
      End
      Begin VB.TextBox TxtBajaD 
         Height          =   285
         Index           =   4
         Left            =   1680
         MaxLength       =   11
         TabIndex        =   78
         Top             =   960
         Width           =   1335
      End
      Begin VB.OptionButton OptSalida 
         Caption         =   "Baja"
         Height          =   255
         Index           =   0
         Left            =   960
         TabIndex        =   29
         Top             =   240
         Value           =   -1  'True
         Width           =   735
      End
      Begin VB.OptionButton OptSalida 
         Caption         =   "Venta"
         Height          =   255
         Index           =   1
         Left            =   1920
         TabIndex        =   27
         Top             =   240
         Width           =   735
      End
      Begin VB.TextBox TxtBajaD 
         Enabled         =   0   'False
         Height          =   285
         Index           =   9
         Left            =   1680
         MaxLength       =   16
         TabIndex        =   23
         Top             =   600
         Width           =   1695
      End
      Begin Threed.SSCommand CmdSelec 
         Height          =   375
         Index           =   0
         Left            =   3240
         TabIndex        =   168
         ToolTipText     =   "Selecci�n de Dependencias"
         Top             =   960
         Width           =   615
         _Version        =   65536
         _ExtentX        =   1085
         _ExtentY        =   661
         _StockProps     =   78
         BevelWidth      =   0
         RoundedCorners  =   0   'False
         Outline         =   0   'False
         Picture         =   "windoc.frx":0000
      End
      Begin VB.Label LblDependencia 
         Caption         =   "Dependencia :"
         Height          =   255
         Left            =   480
         TabIndex        =   80
         Top             =   960
         Width           =   1095
      End
      Begin VB.Label Label16 
         AutoSize        =   -1  'True
         Caption         =   "Valor:"
         Height          =   195
         Left            =   1080
         TabIndex        =   25
         Top             =   600
         Width           =   405
      End
   End
   Begin VB.Frame frmINVENTARIOS 
      Height          =   8415
      Left            =   0
      TabIndex        =   88
      Top             =   0
      Width           =   10575
      Begin MSComctlLib.ListView lstSALXBOD 
         Height          =   2805
         Left            =   120
         TabIndex        =   108
         Top             =   5175
         Visible         =   0   'False
         Width           =   6135
         _ExtentX        =   10821
         _ExtentY        =   4948
         View            =   3
         LabelEdit       =   1
         LabelWrap       =   -1  'True
         HideSelection   =   0   'False
         Checkboxes      =   -1  'True
         FullRowSelect   =   -1  'True
         GridLines       =   -1  'True
         _Version        =   393217
         ForeColor       =   -2147483640
         BackColor       =   -2147483643
         BorderStyle     =   1
         Appearance      =   1
         NumItems        =   0
      End
      Begin VB.Frame FraRequi 
         BackColor       =   &H8000000A&
         Caption         =   "Documentos"
         Height          =   3495
         Left            =   9120
         TabIndex        =   151
         Top             =   1680
         Visible         =   0   'False
         Width           =   1335
         Begin VB.ListBox lstDocsPadre 
            Height          =   2400
            Left            =   120
            MultiSelect     =   2  'Extended
            TabIndex        =   152
            Top             =   240
            Width           =   1095
         End
         Begin Threed.SSCommand cmdDocsPadre 
            Height          =   465
            HelpContextID   =   40
            Index           =   0
            Left            =   120
            TabIndex        =   153
            ToolTipText     =   "Agregar"
            Top             =   2880
            Width           =   495
            _Version        =   65536
            _ExtentX        =   873
            _ExtentY        =   820
            _StockProps     =   78
            ForeColor       =   -2147483640
            Picture         =   "windoc.frx":0452
         End
         Begin Threed.SSCommand cmdDocsPadre 
            Height          =   465
            HelpContextID   =   40
            Index           =   1
            Left            =   720
            TabIndex        =   154
            ToolTipText     =   "Quitar"
            Top             =   2880
            Width           =   495
            _Version        =   65536
            _ExtentX        =   873
            _ExtentY        =   820
            _StockProps     =   78
            ForeColor       =   -2147483640
            Picture         =   "windoc.frx":08AC
         End
      End
      Begin VB.Frame FraGeneral 
         Height          =   1455
         Left            =   120
         TabIndex        =   128
         Top             =   240
         Width           =   10335
         Begin VB.Frame frmCUAL 
            Height          =   600
            Left            =   4560
            TabIndex        =   135
            Top             =   0
            Width           =   1440
            Begin VB.OptionButton opcORG 
               Caption         =   "Original"
               Height          =   240
               Left            =   120
               TabIndex        =   139
               Top             =   120
               Value           =   -1  'True
               Width           =   1000
            End
            Begin VB.OptionButton opcACT 
               Caption         =   "Actual"
               Height          =   240
               Left            =   120
               TabIndex        =   137
               Top             =   330
               Width           =   1000
            End
         End
         Begin VB.TextBox txtDocum 
            BackColor       =   &H00D6FEFE&
            CausesValidation=   0   'False
            Enabled         =   0   'False
            ForeColor       =   &H00000000&
            Height          =   285
            Index           =   1
            Left            =   8940
            MaxLength       =   2
            TabIndex        =   131
            Top             =   240
            Width           =   375
         End
         Begin VB.TextBox txtDocum 
            BackColor       =   &H00D6FEFE&
            Enabled         =   0   'False
            ForeColor       =   &H00000000&
            Height          =   285
            Index           =   2
            Left            =   9300
            MaxLength       =   2
            TabIndex        =   132
            Top             =   220
            Width           =   375
         End
         Begin VB.TextBox txtDocum 
            BackColor       =   &H00D6FEFE&
            Enabled         =   0   'False
            ForeColor       =   &H00000000&
            Height          =   285
            Index           =   3
            Left            =   9660
            MaxLength       =   4
            TabIndex        =   133
            Top             =   220
            Width           =   495
         End
         Begin VB.TextBox txtNumero 
            BackColor       =   &H00D6FEFE&
            CausesValidation=   0   'False
            ForeColor       =   &H00000000&
            Height          =   285
            Left            =   6840
            MaxLength       =   8
            TabIndex        =   130
            Top             =   240
            Width           =   855
         End
         Begin VB.ComboBox cboConsecutivo 
            BackColor       =   &H00D6FEFE&
            CausesValidation=   0   'False
            ForeColor       =   &H00000000&
            Height          =   315
            Left            =   1140
            Style           =   2  'Dropdown List
            TabIndex        =   129
            Top             =   220
            Width           =   3135
         End
         Begin VB.ComboBox cboBodega 
            BackColor       =   &H00D6FEFE&
            ForeColor       =   &H00000000&
            Height          =   315
            Left            =   1140
            Style           =   2  'Dropdown List
            TabIndex        =   140
            Top             =   960
            Width           =   2900
         End
         Begin VB.ComboBox cboBodDestino 
            BackColor       =   &H00D6FEFE&
            CausesValidation=   0   'False
            ForeColor       =   &H00000000&
            Height          =   315
            Left            =   5160
            Style           =   2  'Dropdown List
            TabIndex        =   142
            Top             =   960
            Width           =   2900
         End
         Begin VB.TextBox txtDocum 
            BackColor       =   &H00D6FEFE&
            CausesValidation=   0   'False
            ForeColor       =   &H00000000&
            Height          =   285
            Index           =   5
            Left            =   1140
            MaxLength       =   11
            TabIndex        =   134
            Top             =   600
            Width           =   1095
         End
         Begin VB.ComboBox cboIndependiente 
            BackColor       =   &H00D6FEFE&
            CausesValidation=   0   'False
            ForeColor       =   &H00000000&
            Height          =   315
            Left            =   8400
            Style           =   2  'Dropdown List
            TabIndex        =   144
            Top             =   960
            Width           =   975
         End
         Begin VB.TextBox txtDocum 
            BackColor       =   &H00D6FEFE&
            Enabled         =   0   'False
            ForeColor       =   &H00000000&
            Height          =   285
            Index           =   6
            Left            =   2760
            MaxLength       =   40
            TabIndex        =   138
            Top             =   600
            Width           =   4755
         End
         Begin Threed.SSCommand cmdIrAlCuerpo 
            Height          =   600
            Left            =   9600
            TabIndex        =   146
            ToolTipText     =   "Ir al cuerpo del documento"
            Top             =   720
            Width           =   600
            _Version        =   65536
            _ExtentX        =   1058
            _ExtentY        =   1058
            _StockProps     =   78
            ForeColor       =   16777215
            AutoSize        =   1
            Picture         =   "windoc.frx":09BE
         End
         Begin Threed.SSCommand CmdSelecTercero 
            Height          =   375
            Left            =   2180
            TabIndex        =   136
            ToolTipText     =   "Selecci�n de Terceros"
            Top             =   570
            Width           =   615
            _Version        =   65536
            _ExtentX        =   1085
            _ExtentY        =   661
            _StockProps     =   78
            BevelWidth      =   0
            RoundedCorners  =   0   'False
            Outline         =   0   'False
            Picture         =   "windoc.frx":0CD8
         End
         Begin VB.Label LblFecha 
            Caption         =   "Fecha (D/M/A):"
            Height          =   255
            Index           =   0
            Left            =   7740
            TabIndex        =   150
            Top             =   240
            Width           =   1215
         End
         Begin VB.Label LblNumero 
            Alignment       =   1  'Right Justify
            Caption         =   "N�mero:"
            Height          =   255
            Left            =   6120
            TabIndex        =   149
            Top             =   240
            Width           =   630
         End
         Begin VB.Label Label8 
            Alignment       =   1  'Right Justify
            Caption         =   "Comprobante:"
            Height          =   195
            Left            =   60
            TabIndex        =   148
            Top             =   240
            Width           =   1010
         End
         Begin VB.Label lblBODEGA 
            Alignment       =   1  'Right Justify
            Caption         =   "Bodega:"
            Height          =   255
            Left            =   60
            TabIndex        =   147
            Top             =   960
            Width           =   1010
         End
         Begin VB.Label lblDESTINO 
            Alignment       =   1  'Right Justify
            Caption         =   "Destino:"
            Height          =   255
            Left            =   4320
            TabIndex        =   145
            Top             =   1020
            Width           =   750
         End
         Begin VB.Label LblProveedor 
            Alignment       =   1  'Right Justify
            Caption         =   "Tercero:"
            Height          =   255
            Left            =   60
            TabIndex        =   143
            Top             =   600
            Width           =   1010
         End
         Begin VB.Label lblINDEPE 
            Alignment       =   1  'Right Justify
            Caption         =   "Independiente :"
            Height          =   255
            Left            =   8160
            TabIndex        =   141
            Top             =   720
            Width           =   1215
         End
      End
      Begin VB.Frame FraOpciones 
         BackColor       =   &H8000000A&
         Height          =   1095
         Left            =   6480
         TabIndex        =   123
         Top             =   5550
         Width           =   3975
         Begin Threed.SSCommand cmdOpciones 
            Height          =   735
            Index           =   2
            Left            =   2040
            TabIndex        =   124
            Top             =   240
            Width           =   855
            _Version        =   65536
            _ExtentX        =   1508
            _ExtentY        =   1296
            _StockProps     =   78
            Caption         =   "&Imprimir"
            RoundedCorners  =   0   'False
            Picture         =   "windoc.frx":112A
         End
         Begin Threed.SSCommand cmdOpciones 
            Height          =   735
            Index           =   1
            Left            =   1080
            TabIndex        =   125
            Top             =   240
            Width           =   855
            _Version        =   65536
            _ExtentX        =   1508
            _ExtentY        =   1296
            _StockProps     =   78
            Caption         =   "A&nular"
            Picture         =   "windoc.frx":17A4
         End
         Begin Threed.SSCommand cmdOpciones 
            Height          =   735
            Index           =   0
            Left            =   120
            TabIndex        =   126
            Top             =   240
            Width           =   855
            _Version        =   65536
            _ExtentX        =   1508
            _ExtentY        =   1296
            _StockProps     =   78
            Caption         =   "&Guardar"
            Picture         =   "windoc.frx":193E
         End
         Begin Threed.SSCommand cmdOpciones 
            Height          =   735
            Index           =   3
            Left            =   3000
            TabIndex        =   127
            ToolTipText     =   "Limpia Pantalla"
            Top             =   240
            Width           =   855
            _Version        =   65536
            _ExtentX        =   1508
            _ExtentY        =   1296
            _StockProps     =   78
            Caption         =   "&Limpiar"
            AutoSize        =   1
            Picture         =   "windoc.frx":1FB8
         End
      End
      Begin VB.Frame FraArti 
         BackColor       =   &H8000000A&
         Caption         =   "Art�culos"
         Enabled         =   0   'False
         Height          =   3495
         Left            =   120
         TabIndex        =   115
         Top             =   1680
         Width           =   8895
         Begin VB.Frame fraBRR 
            BorderStyle     =   0  'None
            Height          =   600
            Left            =   7980
            TabIndex        =   116
            Top             =   1200
            Width           =   825
            Begin VB.OptionButton opcBONO 
               Caption         =   "No"
               Height          =   240
               Left            =   100
               TabIndex        =   118
               Top             =   330
               Width           =   650
            End
            Begin VB.OptionButton opcBOSEL 
               Caption         =   "Sel"
               Height          =   240
               Left            =   100
               TabIndex        =   117
               Top             =   120
               Value           =   -1  'True
               Width           =   650
            End
         End
         Begin MSFlexGridLib.MSFlexGrid GrdArticulos 
            Height          =   3135
            Left            =   120
            TabIndex        =   119
            Top             =   240
            Width           =   7815
            _ExtentX        =   13785
            _ExtentY        =   5530
            _Version        =   393216
            Cols            =   5
            FixedCols       =   0
            ForeColorSel    =   -2147483635
            FocusRect       =   2
            MergeCells      =   1
         End
         Begin Threed.SSCommand cmdIconos 
            Height          =   465
            HelpContextID   =   40
            Index           =   1
            Left            =   8160
            TabIndex        =   120
            ToolTipText     =   "Agregar Art�culos"
            Top             =   360
            Width           =   495
            _Version        =   65536
            _ExtentX        =   873
            _ExtentY        =   820
            _StockProps     =   78
            ForeColor       =   16777215
            Picture         =   "windoc.frx":24FA
         End
         Begin Threed.SSCommand cmdIconos 
            Height          =   465
            HelpContextID   =   40
            Index           =   2
            Left            =   8160
            TabIndex        =   121
            ToolTipText     =   "Quitar Art�culos"
            Top             =   1920
            Width           =   495
            _Version        =   65536
            _ExtentX        =   873
            _ExtentY        =   820
            _StockProps     =   78
            ForeColor       =   16777215
            Picture         =   "windoc.frx":294C
         End
         Begin Threed.SSCommand cmdIconos 
            Height          =   465
            HelpContextID   =   40
            Index           =   3
            Left            =   8160
            TabIndex        =   122
            ToolTipText     =   "Exportar, importar Articulos"
            Top             =   2760
            Width           =   495
            _Version        =   65536
            _ExtentX        =   873
            _ExtentY        =   820
            _StockProps     =   78
            ForeColor       =   16777215
            AutoSize        =   1
            Picture         =   "windoc.frx":2A5E
         End
      End
      Begin VB.Frame FraPie 
         BackColor       =   &H8000000A&
         Caption         =   "Observaciones"
         Height          =   1335
         Left            =   120
         TabIndex        =   113
         Top             =   5280
         Width           =   6135
         Begin VB.TextBox txtDocum 
            Height          =   1005
            Index           =   8
            Left            =   120
            MaxLength       =   250
            MultiLine       =   -1  'True
            TabIndex        =   114
            Top             =   240
            Width           =   5895
         End
      End
      Begin VB.Frame fraTOTALES 
         Enabled         =   0   'False
         Height          =   1215
         Left            =   6480
         TabIndex        =   92
         Top             =   6700
         Width           =   3975
         Begin VB.TextBox txtSUBTO 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00D6FEFE&
            Height          =   285
            Left            =   1560
            TabIndex        =   98
            Top             =   190
            Width           =   1935
         End
         Begin VB.TextBox txtIMPUE 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00D6FEFE&
            Height          =   285
            Left            =   1560
            TabIndex        =   96
            Top             =   510
            Width           =   1935
         End
         Begin VB.TextBox txtTOTAL 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00D6FEFE&
            Height          =   285
            Left            =   1560
            TabIndex        =   94
            Top             =   840
            Width           =   1935
         End
         Begin VB.Label Label1 
            Alignment       =   1  'Right Justify
            Caption         =   "Sub Total:"
            Height          =   255
            Index           =   1
            Left            =   120
            TabIndex        =   105
            Top             =   240
            Width           =   1215
         End
         Begin VB.Label Label2 
            Alignment       =   1  'Right Justify
            Caption         =   "Impuesto:"
            Height          =   255
            Index           =   1
            Left            =   120
            TabIndex        =   102
            Top             =   585
            Width           =   1215
         End
         Begin VB.Label Label3 
            Alignment       =   1  'Right Justify
            Caption         =   "Total:"
            Height          =   255
            Left            =   120
            TabIndex        =   100
            Top             =   900
            Width           =   1215
         End
      End
      Begin Threed.SSCommand CmdCuentas 
         Height          =   375
         Left            =   9120
         TabIndex        =   90
         ToolTipText     =   "Registrar Movimiento Contable"
         Top             =   5150
         Visible         =   0   'False
         Width           =   1335
         _Version        =   65536
         _ExtentX        =   2355
         _ExtentY        =   661
         _StockProps     =   78
         Caption         =   "&Contabilidad"
      End
      Begin MSComctlLib.ProgressBar proARTICULOS 
         Height          =   300
         Left            =   6480
         TabIndex        =   110
         Top             =   7200
         Width           =   3975
         _ExtentX        =   7011
         _ExtentY        =   529
         _Version        =   393216
         Appearance      =   1
      End
      Begin MSChart20Lib.MSChart chaSALDOS 
         Height          =   2805
         Left            =   120
         OleObjectBlob   =   "windoc.frx":2D78
         TabIndex        =   112
         Top             =   5175
         Width           =   6135
      End
      Begin ComctlLib.StatusBar staINFORMA 
         Height          =   285
         Left            =   240
         TabIndex        =   155
         Top             =   8040
         Width           =   9975
         _ExtentX        =   17595
         _ExtentY        =   503
         SimpleText      =   ""
         _Version        =   327682
         BeginProperty Panels {0713E89E-850A-101B-AFC0-4210102A8DA7} 
            NumPanels       =   9
            BeginProperty Panel1 {0713E89F-850A-101B-AFC0-4210102A8DA7} 
               AutoSize        =   2
               Object.Width           =   1402
               MinWidth        =   1411
               Text            =   "Nuevo"
               TextSave        =   "Nuevo"
               Key             =   "NUEVO"
               Object.Tag             =   ""
            EndProperty
            BeginProperty Panel2 {0713E89F-850A-101B-AFC0-4210102A8DA7} 
               Alignment       =   2
               Bevel           =   0
               Object.Width           =   1764
               MinWidth        =   1764
               Text            =   "Costo Total:"
               TextSave        =   "Costo Total:"
               Key             =   "lblCOS"
               Object.Tag             =   ""
            EndProperty
            BeginProperty Panel3 {0713E89F-850A-101B-AFC0-4210102A8DA7} 
               Key             =   "COSTO"
               Object.Tag             =   ""
            EndProperty
            BeginProperty Panel4 {0713E89F-850A-101B-AFC0-4210102A8DA7} 
               Alignment       =   2
               Bevel           =   0
               Object.Width           =   2117
               MinWidth        =   2117
               Text            =   "Costo Rengl�n:"
               TextSave        =   "Costo Rengl�n:"
               Key             =   "lblPAR"
               Object.Tag             =   ""
            EndProperty
            BeginProperty Panel5 {0713E89F-850A-101B-AFC0-4210102A8DA7} 
               Key             =   "PARCIAL"
               Object.Tag             =   ""
            EndProperty
            BeginProperty Panel6 {0713E89F-850A-101B-AFC0-4210102A8DA7} 
               Alignment       =   2
               Bevel           =   0
               Object.Width           =   529
               MinWidth        =   529
               Text            =   "#"
               TextSave        =   "#"
               Object.Tag             =   ""
            EndProperty
            BeginProperty Panel7 {0713E89F-850A-101B-AFC0-4210102A8DA7} 
               Object.Width           =   1235
               MinWidth        =   1235
               Key             =   "RNGLNS"
               Object.Tag             =   ""
            EndProperty
            BeginProperty Panel8 {0713E89F-850A-101B-AFC0-4210102A8DA7} 
               Alignment       =   2
               Bevel           =   2
               Text            =   "Saldos X Bodega"
               TextSave        =   "Saldos X Bodega"
               Key             =   "SALBOD"
               Object.Tag             =   ""
            EndProperty
            BeginProperty Panel9 {0713E89F-850A-101B-AFC0-4210102A8DA7} 
               Key             =   "SLDSEL"
               Object.Tag             =   ""
            EndProperty
         EndProperty
      End
   End
   Begin VB.Frame frmSALIDA 
      Caption         =   "Salida"
      Height          =   1695
      Left            =   6720
      TabIndex        =   17
      Top             =   120
      Visible         =   0   'False
      Width           =   3615
      Begin VB.CommandButton cmdPROCEDER 
         Caption         =   "Proceder"
         Height          =   375
         Index           =   19
         Left            =   960
         TabIndex        =   159
         Top             =   960
         Width           =   1695
      End
      Begin VB.TextBox TxtSaliA 
         Enabled         =   0   'False
         Height          =   285
         Index           =   7
         Left            =   1440
         MaxLength       =   16
         TabIndex        =   19
         Top             =   360
         Width           =   1575
      End
      Begin VB.Label Label15 
         AutoSize        =   -1  'True
         Caption         =   "Valor:"
         Height          =   195
         Left            =   840
         TabIndex        =   20
         Top             =   360
         Width           =   405
      End
   End
   Begin VB.Frame FrmDEVOLFACTURA 
      Caption         =   "DevolFactura"
      Height          =   2775
      Left            =   4440
      TabIndex        =   103
      Top             =   1560
      Visible         =   0   'False
      Width           =   5925
      Begin VB.CommandButton cmdPROCEDER 
         Caption         =   "Proceder"
         Height          =   375
         Index           =   12
         Left            =   2280
         TabIndex        =   157
         Top             =   2280
         Width           =   1695
      End
      Begin VB.TextBox TxtDevo 
         BackColor       =   &H80000004&
         Enabled         =   0   'False
         Height          =   285
         Index           =   9
         Left            =   4440
         MaxLength       =   5
         TabIndex        =   107
         Top             =   1320
         Width           =   615
      End
      Begin VB.TextBox TxtDevo 
         BackColor       =   &H80000004&
         Enabled         =   0   'False
         Height          =   285
         Index           =   10
         Left            =   4440
         MaxLength       =   5
         TabIndex        =   111
         Top             =   1800
         Width           =   615
      End
      Begin VB.TextBox TxtDevo 
         Enabled         =   0   'False
         Height          =   285
         Index           =   15
         Left            =   3720
         MaxLength       =   16
         TabIndex        =   101
         Top             =   840
         Width           =   1575
      End
      Begin VB.TextBox TxtDevo 
         Enabled         =   0   'False
         Height          =   285
         Index           =   16
         Left            =   960
         MaxLength       =   16
         TabIndex        =   104
         Top             =   1200
         Width           =   1575
      End
      Begin VB.TextBox TxtDevo 
         Enabled         =   0   'False
         Height          =   285
         Index           =   12
         Left            =   960
         MaxLength       =   16
         TabIndex        =   89
         Top             =   480
         Width           =   1575
      End
      Begin VB.TextBox TxtDevo 
         Enabled         =   0   'False
         Height          =   285
         Index           =   13
         Left            =   3720
         MaxLength       =   16
         TabIndex        =   93
         Top             =   480
         Width           =   1575
      End
      Begin VB.TextBox TxtDevo 
         Enabled         =   0   'False
         Height          =   285
         Index           =   14
         Left            =   960
         MaxLength       =   16
         TabIndex        =   97
         Top             =   840
         Width           =   1575
      End
      Begin VB.Label Label25 
         Caption         =   "% Descuento :"
         Height          =   255
         Left            =   3360
         TabIndex        =   106
         Top             =   1320
         Width           =   1095
      End
      Begin VB.Label Label24 
         Caption         =   "% Ret Fuente :"
         Height          =   255
         Left            =   3360
         TabIndex        =   109
         Top             =   1800
         Width           =   1095
      End
      Begin VB.Label LblRete 
         AutoSize        =   -1  'True
         Caption         =   "Ret Fuente :"
         Height          =   195
         Index           =   3
         Left            =   2640
         TabIndex        =   99
         Top             =   840
         Width           =   885
      End
      Begin VB.Label Label14 
         Caption         =   "Bruto :"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   240
         TabIndex        =   87
         Top             =   480
         Width           =   495
      End
      Begin VB.Label Label13 
         Caption         =   "Neto :"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   240
         TabIndex        =   15
         Top             =   1200
         Width           =   495
      End
      Begin VB.Label Label12 
         Caption         =   "Descuentos :"
         Height          =   255
         Left            =   2640
         TabIndex        =   91
         Top             =   480
         Width           =   975
      End
      Begin VB.Label LblImpu 
         Caption         =   "Iva :"
         Height          =   255
         Index           =   3
         Left            =   240
         TabIndex        =   95
         Top             =   840
         Width           =   375
      End
   End
   Begin VB.Frame FrmENTRADA 
      Caption         =   "Entrada"
      Height          =   3255
      Left            =   360
      TabIndex        =   0
      Top             =   5160
      Visible         =   0   'False
      Width           =   10000
      Begin VB.CommandButton cmdPROCEDER 
         Caption         =   "Proceder"
         Height          =   375
         Index           =   3
         Left            =   3840
         TabIndex        =   86
         Top             =   2640
         Width           =   1695
      End
      Begin VB.TextBox TxtCompra 
         Height          =   285
         Index           =   10
         Left            =   2040
         MaxLength       =   8
         TabIndex        =   33
         Top             =   1560
         Width           =   855
      End
      Begin VB.OptionButton OptProveedor 
         Caption         =   "Proveedor"
         Height          =   255
         Left            =   1560
         TabIndex        =   31
         Top             =   1080
         Value           =   -1  'True
         Width           =   1095
      End
      Begin VB.OptionButton OptParticular 
         Caption         =   "Particular"
         Height          =   255
         Left            =   2760
         TabIndex        =   32
         Top             =   1080
         Width           =   1095
      End
      Begin VB.TextBox TxtEntra 
         Enabled         =   0   'False
         Height          =   285
         Index           =   13
         Left            =   7680
         MaxLength       =   16
         TabIndex        =   41
         Top             =   1440
         Width           =   1575
      End
      Begin VB.TextBox TxtEntra 
         Enabled         =   0   'False
         Height          =   285
         Index           =   15
         Left            =   7680
         MaxLength       =   16
         TabIndex        =   51
         Top             =   2640
         Width           =   1575
      End
      Begin VB.TextBox TxtEntra 
         Enabled         =   0   'False
         Height          =   285
         Index           =   10
         Left            =   5040
         MaxLength       =   16
         TabIndex        =   34
         Top             =   1080
         Width           =   1575
      End
      Begin VB.TextBox TxtEntra 
         Height          =   285
         Index           =   14
         Left            =   5040
         MaxLength       =   16
         TabIndex        =   47
         Top             =   2160
         Width           =   1575
      End
      Begin VB.TextBox TxtEntra 
         Enabled         =   0   'False
         Height          =   285
         Index           =   11
         Left            =   7680
         MaxLength       =   16
         TabIndex        =   35
         Top             =   1080
         Width           =   1575
      End
      Begin VB.TextBox TxtEntra 
         Enabled         =   0   'False
         Height          =   285
         Index           =   12
         Left            =   5040
         MaxLength       =   16
         TabIndex        =   39
         Top             =   1440
         Width           =   1575
      End
      Begin VB.TextBox TxtEntra 
         Enabled         =   0   'False
         Height          =   285
         Index           =   18
         Left            =   5040
         MaxLength       =   16
         TabIndex        =   43
         Top             =   1800
         Width           =   1575
      End
      Begin VB.TextBox TxtEntra 
         Enabled         =   0   'False
         Height          =   285
         Index           =   19
         Left            =   7680
         MaxLength       =   16
         TabIndex        =   45
         Top             =   1800
         Width           =   1575
      End
      Begin VB.TextBox TxtEntra 
         Height          =   285
         Index           =   20
         Left            =   7680
         MaxLength       =   16
         TabIndex        =   49
         Top             =   2160
         Width           =   1575
      End
      Begin VB.TextBox TxtEntra 
         Height          =   285
         Index           =   7
         Left            =   2160
         MaxLength       =   7
         TabIndex        =   24
         Top             =   360
         Width           =   615
      End
      Begin VB.TextBox TxtEntra 
         Height          =   285
         Index           =   8
         Left            =   4500
         MaxLength       =   7
         TabIndex        =   26
         Top             =   360
         Width           =   615
      End
      Begin VB.TextBox TxtEntra 
         Height          =   285
         Index           =   16
         Left            =   6720
         MaxLength       =   7
         TabIndex        =   28
         Top             =   360
         Width           =   615
      End
      Begin VB.TextBox TxtEntra 
         Height          =   285
         Index           =   17
         Left            =   8715
         MaxLength       =   7
         TabIndex        =   30
         Top             =   360
         Width           =   615
      End
      Begin VB.Label LblNuFac 
         Caption         =   "No Factura :"
         Height          =   255
         Left            =   1080
         TabIndex        =   8
         Top             =   1560
         Width           =   975
      End
      Begin VB.Label Label6 
         AutoSize        =   -1  'True
         Caption         =   "Pagar Fletes a:"
         Height          =   195
         Left            =   360
         TabIndex        =   7
         Top             =   1080
         Width           =   1065
      End
      Begin VB.Label LblRete 
         Caption         =   "Rete. Fuente :"
         Height          =   255
         Index           =   0
         Left            =   6720
         TabIndex        =   12
         Top             =   1440
         Width           =   1095
      End
      Begin VB.Label LblBruto 
         Caption         =   "Bruto :"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   4320
         TabIndex        =   9
         Top             =   1080
         Width           =   495
      End
      Begin VB.Label LblNeto 
         Caption         =   "Neto :"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   6720
         TabIndex        =   21
         Top             =   2640
         Width           =   495
      End
      Begin VB.Label LblFletes 
         Caption         =   "Fletes "
         Height          =   255
         Left            =   4320
         TabIndex        =   16
         Top             =   2160
         Width           =   615
      End
      Begin VB.Label LblDescu 
         Caption         =   "Descuentos "
         Height          =   255
         Left            =   6720
         TabIndex        =   10
         Top             =   1080
         Width           =   975
      End
      Begin VB.Label LblImpu 
         Caption         =   "IVA "
         Height          =   255
         Index           =   0
         Left            =   4320
         TabIndex        =   11
         Top             =   1440
         Width           =   375
      End
      Begin VB.Label LblImpu 
         AutoSize        =   -1  'True
         Caption         =   "Rete IVA"
         Height          =   195
         Index           =   1
         Left            =   4320
         TabIndex        =   13
         Top             =   1800
         Width           =   645
      End
      Begin VB.Label LblRete 
         Caption         =   "Rete ICA"
         Height          =   255
         Index           =   1
         Left            =   6720
         TabIndex        =   14
         Top             =   1800
         Width           =   1095
      End
      Begin VB.Label Label5 
         Caption         =   "Descuento Adicional CXP"
         Height          =   375
         Left            =   6720
         TabIndex        =   18
         Top             =   2160
         Width           =   975
      End
      Begin VB.Label LblDescuento 
         Caption         =   "% Descuento :"
         Height          =   255
         Left            =   840
         TabIndex        =   3
         Top             =   360
         Width           =   1095
      End
      Begin VB.Label LblPrete 
         Caption         =   "% Ret Fuente :"
         Height          =   255
         Left            =   3420
         TabIndex        =   4
         Top             =   360
         Width           =   1095
      End
      Begin VB.Label Label4 
         Caption         =   "% Rete IVA:"
         Height          =   255
         Left            =   5490
         TabIndex        =   5
         Top             =   360
         Width           =   1095
      End
      Begin VB.Label Label2 
         Caption         =   "% Rete ICA:"
         Height          =   255
         Index           =   0
         Left            =   7755
         TabIndex        =   6
         Top             =   360
         Width           =   1095
      End
   End
   Begin VB.Frame FrmFACTURA 
      Caption         =   "Factura"
      Height          =   3120
      Left            =   4440
      TabIndex        =   2
      Top             =   3000
      Visible         =   0   'False
      Width           =   5925
      Begin VB.CommandButton cmdPROCEDER 
         Caption         =   "Proceder"
         Height          =   375
         Index           =   11
         Left            =   2280
         TabIndex        =   156
         Top             =   2520
         Width           =   1695
      End
      Begin VB.TextBox TxtVenta 
         Height          =   285
         Index           =   12
         Left            =   1920
         MaxLength       =   5
         TabIndex        =   83
         Top             =   2040
         Width           =   615
      End
      Begin VB.TextBox TxtVenta 
         Height          =   285
         Index           =   13
         Left            =   4200
         MaxLength       =   5
         TabIndex        =   85
         Top             =   2040
         Width           =   615
      End
      Begin VB.TextBox TxtVenta 
         Height          =   285
         Index           =   4
         Left            =   1440
         MaxLength       =   3
         TabIndex        =   75
         Text            =   "0"
         Top             =   1560
         Width           =   375
      End
      Begin VB.TextBox TxtVenta 
         Enabled         =   0   'False
         Height          =   285
         Index           =   5
         Left            =   4320
         MaxLength       =   2
         TabIndex        =   77
         Top             =   1560
         Width           =   375
      End
      Begin VB.TextBox TxtVenta 
         Enabled         =   0   'False
         Height          =   285
         Index           =   6
         Left            =   4680
         MaxLength       =   2
         TabIndex        =   79
         Top             =   1560
         Width           =   375
      End
      Begin VB.TextBox TxtVenta 
         Enabled         =   0   'False
         Height          =   285
         Index           =   7
         Left            =   5040
         MaxLength       =   4
         TabIndex        =   81
         Top             =   1560
         Width           =   495
      End
      Begin VB.TextBox TxtVenta 
         Enabled         =   0   'False
         Height          =   285
         Index           =   17
         Left            =   960
         MaxLength       =   16
         TabIndex        =   63
         Top             =   720
         Width           =   1575
      End
      Begin VB.TextBox TxtVenta 
         Enabled         =   0   'False
         Height          =   285
         Index           =   16
         Left            =   3960
         MaxLength       =   16
         TabIndex        =   59
         Top             =   360
         Width           =   1575
      End
      Begin VB.TextBox TxtVenta 
         Height          =   285
         Index           =   19
         Left            =   960
         MaxLength       =   16
         TabIndex        =   71
         Top             =   1080
         Width           =   1575
      End
      Begin VB.TextBox TxtVenta 
         Enabled         =   0   'False
         Height          =   285
         Index           =   15
         Left            =   960
         MaxLength       =   16
         TabIndex        =   55
         Top             =   360
         Visible         =   0   'False
         Width           =   1575
      End
      Begin VB.TextBox TxtVenta 
         Enabled         =   0   'False
         Height          =   285
         Index           =   20
         Left            =   3960
         MaxLength       =   16
         TabIndex        =   73
         Top             =   1080
         Width           =   1575
      End
      Begin VB.TextBox TxtVenta 
         Enabled         =   0   'False
         Height          =   285
         Index           =   18
         Left            =   3960
         MaxLength       =   16
         TabIndex        =   67
         Top             =   720
         Width           =   1575
      End
      Begin VB.Label Label23 
         Caption         =   "% Descuento :"
         Height          =   255
         Left            =   840
         TabIndex        =   82
         Top             =   2040
         Width           =   1215
      End
      Begin VB.Label Label22 
         Caption         =   "% Ret Fuente :"
         Height          =   255
         Left            =   3000
         TabIndex        =   84
         Top             =   2040
         Width           =   1095
      End
      Begin VB.Label LblPlazo 
         Caption         =   "D�as de Plazo :"
         Height          =   255
         Left            =   240
         TabIndex        =   74
         Top             =   1560
         Width           =   1095
      End
      Begin VB.Label LblFecha 
         Caption         =   "Fecha de Vencimiento :"
         Height          =   255
         Index           =   1
         Left            =   2520
         TabIndex        =   76
         Top             =   1560
         Width           =   1695
      End
      Begin VB.Label LblImpu 
         Caption         =   "Iva :"
         Height          =   255
         Index           =   2
         Left            =   240
         TabIndex        =   61
         Top             =   720
         Width           =   375
      End
      Begin VB.Label Label11 
         Caption         =   "Descuentos :"
         Height          =   255
         Left            =   2760
         TabIndex        =   57
         Top             =   360
         Width           =   975
      End
      Begin VB.Label Label10 
         Caption         =   "Fletes :"
         Height          =   255
         Left            =   240
         TabIndex        =   69
         Top             =   1080
         Width           =   615
      End
      Begin VB.Label Label9 
         Caption         =   "Neto :"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   2760
         TabIndex        =   72
         Top             =   1080
         Width           =   495
      End
      Begin VB.Label Label7 
         Caption         =   "Bruto :"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   240
         TabIndex        =   53
         Top             =   360
         Width           =   495
      End
      Begin VB.Label LblRete 
         Caption         =   "Rete. Fuente :"
         Height          =   255
         Index           =   2
         Left            =   2760
         TabIndex        =   65
         Top             =   720
         Width           =   1095
      End
   End
   Begin MSFlexGridLib.MSFlexGrid GrdArti 
      Height          =   2055
      Left            =   1200
      TabIndex        =   1
      Top             =   3960
      Visible         =   0   'False
      Width           =   5775
      _ExtentX        =   10186
      _ExtentY        =   3625
      _Version        =   393216
      Cols            =   10
      FixedCols       =   0
      ForeColorSel    =   -2147483635
      FocusRect       =   2
      MergeCells      =   1
   End
   Begin VB.Frame frmCOMPRAS 
      Caption         =   "Compras"
      Height          =   3615
      Left            =   4440
      TabIndex        =   36
      Top             =   2040
      Visible         =   0   'False
      Width           =   5925
      Begin VB.TextBox TxtCompra 
         Enabled         =   0   'False
         Height          =   285
         Index           =   5
         Left            =   2520
         MaxLength       =   2
         TabIndex        =   166
         Top             =   3000
         Width           =   375
      End
      Begin VB.TextBox TxtCompra 
         Enabled         =   0   'False
         Height          =   285
         Index           =   6
         Left            =   2880
         MaxLength       =   2
         TabIndex        =   165
         Top             =   3000
         Width           =   375
      End
      Begin VB.TextBox TxtCompra 
         Enabled         =   0   'False
         Height          =   285
         Index           =   7
         Left            =   3240
         MaxLength       =   4
         TabIndex        =   164
         Top             =   3000
         Width           =   495
      End
      Begin VB.TextBox TxtCompra 
         Height          =   285
         Index           =   0
         Left            =   4200
         MaxLength       =   8
         TabIndex        =   161
         Top             =   2400
         Width           =   855
      End
      Begin VB.TextBox TxtCompra 
         Height          =   285
         Index           =   4
         Left            =   2040
         MaxLength       =   3
         TabIndex        =   160
         Text            =   "0"
         Top             =   2400
         Width           =   375
      End
      Begin VB.TextBox TxtCompra 
         Enabled         =   0   'False
         Height          =   285
         Index           =   14
         Left            =   1080
         MaxLength       =   16
         TabIndex        =   52
         Top             =   600
         Width           =   1575
      End
      Begin VB.TextBox TxtCompra 
         Enabled         =   0   'False
         Height          =   285
         Index           =   13
         Left            =   3960
         MaxLength       =   16
         TabIndex        =   50
         Top             =   240
         Width           =   1575
      End
      Begin VB.TextBox TxtCompra 
         Enabled         =   0   'False
         Height          =   285
         Index           =   16
         Left            =   1080
         MaxLength       =   16
         TabIndex        =   48
         Top             =   1320
         Width           =   1575
      End
      Begin VB.TextBox TxtCompra 
         Enabled         =   0   'False
         Height          =   285
         Index           =   12
         Left            =   1080
         MaxLength       =   16
         TabIndex        =   46
         Top             =   240
         Width           =   1575
      End
      Begin VB.TextBox TxtCompra 
         Enabled         =   0   'False
         Height          =   285
         Index           =   17
         Left            =   3960
         MaxLength       =   16
         TabIndex        =   44
         Top             =   1680
         Width           =   1575
      End
      Begin VB.TextBox TxtCompra 
         Enabled         =   0   'False
         Height          =   285
         Index           =   15
         Left            =   3960
         MaxLength       =   16
         TabIndex        =   42
         Top             =   600
         Width           =   1575
      End
      Begin VB.TextBox TxtCompra 
         Enabled         =   0   'False
         Height          =   285
         Index           =   18
         Left            =   1080
         MaxLength       =   16
         TabIndex        =   40
         Top             =   960
         Width           =   1575
      End
      Begin VB.TextBox TxtCompra 
         Enabled         =   0   'False
         Height          =   285
         Index           =   19
         Left            =   3960
         MaxLength       =   16
         TabIndex        =   38
         Top             =   960
         Width           =   1575
      End
      Begin VB.TextBox TxtCompra 
         Enabled         =   0   'False
         Height          =   285
         Index           =   20
         Left            =   3960
         MaxLength       =   16
         TabIndex        =   37
         Top             =   1320
         Width           =   1575
      End
      Begin VB.Label LblFecha 
         Caption         =   "Fecha de Vencimiento :"
         Height          =   255
         Index           =   2
         Left            =   720
         TabIndex        =   167
         Top             =   3000
         Width           =   1695
      End
      Begin VB.Label Label27 
         Caption         =   "No Factura :"
         Height          =   255
         Left            =   3240
         TabIndex        =   163
         Top             =   2400
         Width           =   975
      End
      Begin VB.Label Label26 
         Caption         =   "D�as de Plazo :"
         Height          =   255
         Left            =   840
         TabIndex        =   162
         Top             =   2400
         Width           =   1095
      End
      Begin VB.Label LblImpu 
         Caption         =   "Iva :"
         Height          =   255
         Index           =   4
         Left            =   360
         TabIndex        =   70
         Top             =   600
         Width           =   375
      End
      Begin VB.Label Label21 
         Caption         =   "Descuentos :"
         Height          =   255
         Left            =   2880
         TabIndex        =   68
         Top             =   240
         Width           =   975
      End
      Begin VB.Label Label20 
         Caption         =   "Fletes :"
         Height          =   255
         Left            =   360
         TabIndex        =   66
         Top             =   1320
         Width           =   615
      End
      Begin VB.Label Label19 
         Caption         =   "Neto :"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   2880
         TabIndex        =   64
         Top             =   1680
         Width           =   495
      End
      Begin VB.Label Label18 
         Caption         =   "Bruto :"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   360
         TabIndex        =   62
         Top             =   240
         Width           =   495
      End
      Begin VB.Label LblRete 
         Caption         =   "Rete. Fuente :"
         Height          =   255
         Index           =   4
         Left            =   2880
         TabIndex        =   60
         Top             =   600
         Width           =   1095
      End
      Begin VB.Label Label1 
         Caption         =   "ReteICA:"
         Height          =   255
         Index           =   0
         Left            =   2880
         TabIndex        =   58
         Top             =   960
         Width           =   1095
      End
      Begin VB.Label Label2 
         AutoSize        =   -1  'True
         Caption         =   "Rete IVA "
         Height          =   195
         Index           =   2
         Left            =   360
         TabIndex        =   56
         Top             =   960
         Width           =   690
      End
      Begin VB.Label Label17 
         Caption         =   "Descuento Adicional CXP"
         Height          =   375
         Left            =   2880
         TabIndex        =   54
         Top             =   1320
         Width           =   975
      End
   End
End
Attribute VB_Name = "FrmGENERAL"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
'Dim Tabla As String
'Dim Gol, Bad As Boolean
'Variables para controlar el foco del grid de Art�culos
Public OpcCod        As String   'Opci�n de seguridad

'Variables de interface
Public SiMuestra    As Boolean
Dim ValReg          As Double
Dim Ppto            As Boolean
Dim ArtiPpto()
Dim ArtiFlete       As String
Dim DescArtiF       As String
Dim Conse_CXC       As Long
Dim Conse_CXP       As Long
Dim blnContab       As Boolean 'tiene interface contabilidad
Dim blnXPagar       As Boolean 'tiene interface cuentas x pagar
Dim blnXCobrar      As Boolean 'tiene interface cuentas x cobrar
Dim blnDesdeCompras As Boolean 'tiene interface cuentas x cobrar
Dim vCbCell         As Boolean

Private focol As Long 'n�mero de columna de la grilla que tiene el foco
Private fofil As Long 'n�mero de fila de la grilla que tiene el foco
Private AuTiDoc As Long 'Autoum�rico en la tabla IN_DOCUMENTO
Private Mensaje As String  'Nombre del documento que se esta procesando
Private EsNuevo As Boolean 'Bandera para saber si el documento es nuevo
Private EsAnul  As Boolean 'Bandera para saber si el documento esta anulado
Private SeActi  As Boolean 'Bandera para saber si el documento ya se activo
Private FiActiv As Long 'numero de la fila de la grilla activa
Private FechUlt As Long 'Fecha de creaci�n del �ltimo documento de ese tipo
Private FechCie As Long 'Fecha de creaci�n del �ltimo documento de ese tipo
Private DefDGrl As String 'cuerda que tiene los parametros para l�a cracion de la grilla
Private CntGrll As Variant 'Contenido de la celda activa de la grilla de articulos

Private vGriAutoArticulo        As Byte 'N�mero de columna de la grilla que contiene el autonum�rico del art�culo
Private vGriCodigoArticulo      As Byte 'N�mero de columna de la grilla que contiene el c�digo del art�culo
Private vGriNombreArticulo      As Byte 'N�mero de columna de la grilla que contiene el nombre del art�culo
Private vGriAutoUnidad          As Byte 'N�mero de columna de la grilla que contiene el Auto de la unidad de distribuci�n
Private vGriNombreUnidad        As Byte 'N�mero de columna de la grilla que contiene el nombre de la unidad de distribuci�n
Private vGriMultiplicador       As Byte 'N�mero de columna de la grilla que contiene el Muntiplicador de la unidad de distribuci�n
Private vGriDivisor             As Byte 'N�mero de columna de la grilla que contiene el Divisor de la unidad de distribuci�n
Private vGriUltimoCosto         As Byte 'N�mero de columna de la grilla que contiene el �ltimo costo
Private vGriCantidad            As Byte 'N�mero de columna de la grilla que contiene la cantidad
Private vGriCostoSinIVA         As Byte 'N�mero de columna de la grilla que contiene el Costo unitario sin IVA
Private vGriPorceIVA            As Byte 'N�mero de columna de la grilla que contiene el Porcentaje de IVA
Private vGriCostoUnidad         As Byte 'N�mero de columna de la grilla que contiene el Costo unitario con IVA
Private vGriCostoTotal          As Byte 'N�mero de columna de la grilla que contiene el Costo total
Private vGriFecVence            As Byte 'N�mero de columna de la grilla que contiene la fecha de vancimiento
Private vGriFecEntrada          As Byte 'N�mero de columna de la grilla que contiene la fecha de entrada
Private vGriCantidadFija        As Byte 'N�mero de columna de la grilla que contiene el Cantidad del docomento original
Private vGriAutoDocumento       As Byte 'N�mero de columna de la grilla que contiene el Auto num�rico del Doc original
Private vSeleccionada           As Byte 'N�mero de columna de la grilla 0 si no tiene chulo,0 si no
Private vMarcar                As Byte 'N�mero de columna de la grilla para el chulo

Private vProXLts As Boolean

'vGriAutoArticulo, vGriCodigoArticulo, vGriNombreArticulo, vGriAutoUnidad, vGriNombreUnidad
'vGriMultiplicador, vGriDivisor, vGriUltimoCosto, vGriCantidad, vGriCostoSinIVA, vGriPorceIVA
'vGriCostoUnidad, vGriCostoTotal, vGriFecVence, vGriFecEntrada, vGriCantidadFija, vGriAutoDocumento

'    Me.ColAutoArticulo = 0
'    Me.ColCodigoArticulo = 1
'    Me.ColNombreArticulo = 2
'    Me.ColAutoUnidad = 3
'    Me.ColNombreUnidad = 4
'    Me.ColMultiplicador = 5
'    Me.ColDivisor = 6
'    Me.ColUltimoCosto = 7
'    Me.ColCantidad = 8
'    Me.ColCostoSinIVA = 9
'    Me.ColPorceIVA = 10
'    Me.ColCostoUnidad = 11
'    Me.ColCostoTotal = 12
'    Me.ColFecVence = 13
'    Me.ColFecEntrada = 14
'    Me.ColCantidadFija = 15
'    Me.ColAutoDocumento = 16

Private WithEvents WinDoc               As WinDocumento
Attribute WinDoc.VB_VarHelpID = -1
Private WithEvents Documento            As ElDocumento
Attribute Documento.VB_VarHelpID = -1

Private Sub chaSALDOS_SeriesSelected(Series As Integer, MouseFlags As Integer, Cancel As Integer)
    Me.chaSALDOS.Column = Series
    Me.chaSALDOS.Row = 1
    Me.staINFORMA.Panels("SALBOD").Text = Me.chaSALDOS.ColumnLabel
    Me.staINFORMA.Panels("SLDSEL").Text = Aplicacion.Formatear_Cantidad(Me.chaSALDOS.Data)
    Me.chaSALDOS.DataGrid.RowLabel(1, 1) = Trim(Me.GrdArticulos.TextMatrix(Me.FilaDeGrillaActiva, Me.ColNombreArticulo))
    Me.chaSALDOS.DataGrid.RowLabel(1, 2) = Me.staINFORMA.Panels("SALBOD").Text & ":" & Me.staINFORMA.Panels("SLDSEL").Text
End Sub

Private Sub cmdIconos_Click(Index As Integer)
    Dim Cuer As String, CnTr As Integer, Dicc As Boolean
    Select Case Index
    Case 0
    Case 1: If Documento.Encabezado.EsNuevoDocumento And Not Me.FraRequi.Visible Then WinDoc.AgregarArticulos
    Case 2
        If Documento.Encabezado.EsNuevoDocumento Then
            CnTr = GrdArticulos.Rows - 1
            If Me.opcBOSEL.Value Then
                While (CnTr > 0 And GrdArticulos.Rows > 1)
                    If Me.GrdArticulos.TextMatrix(CnTr, vSeleccionada) = "1" Then
                        WinDoc.EliminarFila (CnTr)
                    End If
                    CnTr = CnTr - 1
                Wend
            ElseIf Me.opcBONO.Value Then
                While (CnTr > 0 And GrdArticulos.Rows > 1)
                    If Not Me.GrdArticulos.TextMatrix(CnTr, vSeleccionada) = "1" Then
                        WinDoc.EliminarFila (CnTr)
                    End If
                    CnTr = CnTr - 1
                Wend
            End If
'            If Me.opcBONO.Value And Me.GrdArticulos.TextMatrix(Cntr, vSeleccionada) = "0" Then WinDoc.EliminarFila (Cntr)
'            WinDoc.EliminarFila (Me.FilaActiva)
            If Not GrdArticulos.Rows > 2 Then GrdArticulos.Rows = 2
            GrdArticulos.Row = 1
            Me.FilaActiva = GrdArticulos.Row
        End If
    Case 3
        Set FrmEXPOIMPO.ElDocumento = Documento
        Set FrmEXPOIMPO.FgridArtic = Me.GrdArticulos
        WinDoc.ProcesarEventosGrilla = False
        Cuer = ""
        FrmEXPOIMPO.PosicionesEnGrilla = Cuer & Me.ColCodigoArticulo & Cuer & _
            Coma & Cuer & Me.ColCantidad & Cuer & _
            Coma & Cuer & Me.ColCostoUnidad & Cuer
        Set FrmEXPOIMPO.ElFormulario = Me
        FrmEXPOIMPO.Show vbModal
        WinDoc.ProcesarEventosGrilla = True
        Call WinDoc_CambioRenglonGrilla(0, WinDoc.Calcular_Documento(Me.ColCantidad, Me.ColCostoUnidad))
    End Select
End Sub

Private Sub cmdDocsPadre_Click(Index As Integer)
    Dim aux As Integer
    Select Case Index
    Case 0
        Me.fraTOTALES.Visible = False
        WinDoc.SeleccionarDocumentoPadre (Not Documento.Encabezado.EsInterno)
        Me.fraTOTALES.Visible = True
        Call WinDoc_CambioRenglonGrilla(0, WinDoc.Calcular_Documento(Me.ColCantidad, Me.ColCostoUnidad))
'        WinDoc.SeleccionarDocumentoPadre ((Not Documento.Encabezado.EsIndependiente) And (Not Documento.Encabezado.RomDepende))
    Case 1 'Elimina los items seleccionados
        'de la lista de requisiciones
        aux = 1
        Do While aux <= lstDocsPadre.ListCount
           If lstDocsPadre.Selected(aux - 1) = True Then
              lstDocsPadre.RemoveItem (aux - 1)
              aux = 1
           Else
              aux = aux + 1
           End If
        Loop
'               Call Agregar_Arti_Requi
    End Select
End Sub

Private Sub IrAlCuerpo()
    Dim Cuerda As String
    If Me.cboConsecutivo.ListIndex < 0 Then Cuerda = Cuerda & IIf(Len(Cuerda) > 0, ", ", "") & "Consecutivo"
    If Len(Trim(Me.txtDocum(5).Text)) = 0 Then Cuerda = Cuerda & IIf(Len(Cuerda) > 0, ", ", "") & "Tercero"
    If Me.cboBodega.ListIndex < 0 Then Cuerda = Cuerda & IIf(Len(Cuerda) > 0, ", ", "") & "Bodega de origen"
    If Documento.Encabezado.EsInterno Then If Me.cboBodDestino.ListIndex < 0 Then Cuerda = Cuerda & IIf(Len(Cuerda) > 0, ", ", "") & "Bodega de destino"
    If Len(Cuerda) > 0 Then MsgBox ("Falta informaci�n en:" & Cuerda): Exit Sub
    Me.FraGeneral.Enabled = False
    Documento.Encabezado.EsIndependiente = True
    If Not Documento.Encabezado.EsNuevoDocumento Then Documento.Encabezado.EsIndependiente = False
    If Not Documento.Encabezado.EsNuevoDocumento Then Me.CmdCuentas.Visible = False
'    If Me.cboIndependiente.Visible And Me.cboIndependiente.ListIndex = 0 And Documento.Encabezado.EsNuevoDocumento Then Me.FraRequi.Visible = True
    If Me.cboIndependiente.ListIndex = 0 And Documento.Encabezado.EsNuevoDocumento Then Me.FraRequi.Visible = True
    If Me.FraRequi.Visible Then Documento.Encabezado.EsIndependiente = False
'    If Not Documento.Encabezado.EsNuevoDocumento Then Documento.Encabezado.EsIndependiente = False
    Me.FraArti.Enabled = True
    Me.FraPie.Enabled = True
    Me.FraOpciones.Enabled = True
    Documento.Encabezado.NumeroCOMPROBANTE = Me.cboConsecutivo.ItemData(Me.cboConsecutivo.ListIndex)
    Documento.Encabezado.BodegaORIGEN = Me.cboBodega.ItemData(Me.cboBodega.ListIndex)
'    Documento.Encabezado.BodegaDESTINO = 0
    Documento.Encabezado.Tercero.IniXNit (Me.txtDocum(5))
'    If Documento.Encabezado.EsInterno Then Documento.Encabezado.BodegaDESTINO = Me.cboBodega.ItemData(Me.cboBodega.ListIndex)
    Me.staINFORMA.Visible = True
    Me.FraArti.BackColor = &H8000000F
    Me.FraPie.BackColor = &H8000000F
    Me.FraRequi.BackColor = &H8000000F
    Me.FraOpciones.BackColor = &H8000000F
    Me.FraGeneral.BackColor = &H8000000A
    Me.DefinicionDeGrilla = "Autoart,+0,Marcar,800," & _
        "Codigo,1900," & _
        "Nombre,3500," & _
        "AutoUndMedida,+0," & _
        "Und Medida,1000," & _
        "Multiplicador,+0," & _
        "Divisor,+0," & _
        "Ultimo Costo,+0," & _
        "Cantidad,+800," & _
        "Sin IVA,+0," & _
        "IVA,1," & _
        "Costo Unitario,+1500," & _
        "Costo Total,+1500," & _
        "Fecha Vencimiento,+0," & _
        "Fecha Entrada,+0," & _
        "Pertenece A,+0," & _
        "Control C/dad,+0," & _
        "Auto Doc,+0," & _
        "Select,+0"
'    Me.DefinicionDeGrilla = "Autoart,+0," & _
'        "Codigo,1900," & _
'        "Nombre,3500," & _
'        "AutoUndMedida,+0," & _
'        "Und Medida,1000," & _
'        "Multiplicador,+0," & _
'        "Divisor,+0," & _
'        "Ultimo Costo,+0," & _
'        "Cantidad,+800," & _
'        "Sin IVA,+0," & _
'        "IVA,1," & _
'        "Costo Unitario,+1500," & _
'        "Costo Total,+1500," & _
'        "Fecha Vencimiento,+0," & _
'        "Fecha Entrada,+0," & _
'        "Pertenece A,+0," & _
'        "Control C/dad,+0," & _
'        "Auto Doc,+0," & _
'        "Select,+0," & _
'        "Marcar,1000"
    If Documento.Encabezado.EsIndependiente Then
        Select Case Documento.Encabezado.TipDeDcmnt
        Case Entrada, ODCompra
            Me.DefinicionDeGrilla = "Autoart,+0,Marcar,800," & _
                "Codigo,1900," & _
                "Nombre,3500," & _
                "AutoUndMedida,+0," & _
                "Und Medida,1000," & _
                "Multiplicador,+0," & _
                "Divisor,+1," & _
                "Ultimo Costo,+1500," & _
                "Cantidad,+800," & _
                "Sin IVA,+1500," & _
                "IVA,+800," & _
                "Costo Unitario,+1500," & _
                "Costo Total,+1500," & _
                "Fecha Vencimiento,1200," & _
                "Fecha Entrada,1200," & _
                "Pertenece A,+0," & _
                "Control C/dad,+0," & _
                "Auto Doc,+0," & _
                "Select,+0"
'            Me.DefinicionDeGrilla = "Autoart,+0," & _
'                "Codigo,1900," & _
'                "Nombre,3500," & _
'                "AutoUndMedida,+0," & _
'                "Und Medida,1000," & _
'                "Multiplicador,+0," & _
'                "Divisor,+1," & _
'                "Ultimo Costo,+1500," & _
'                "Cantidad,+800," & _
'                "Sin IVA,+1500," & _
'                "IVA,+800," & _
'                "Costo Unitario,+1500," & _
'                "Costo Total,+1500," & _
'                "Fecha Vencimiento,1200," & _
'                "Fecha Entrada,1200," & _
'                "Pertenece A,+0," & _
'                "Control C/dad,+0," & _
'                "Auto Doc,+0," & _
'                "Select,+0," & _
'                "Marcar,1000"
        End Select
    End If
    Call GrdFDef(GrdArticulos, 0, 0, Me.DefinicionDeGrilla)
End Sub

Private Sub cmdIrAlCuerpo_Click()
    Call IrAlCuerpo
End Sub

Private Sub cmdPROCEDER_Click(Index As Integer)
    If Me.frmBAJA.Visible Then Me.frmBAJA.Visible = False
    If Me.frmCOMPRAS.Visible Then Me.frmCOMPRAS.Visible = False
    If Me.FrmDEVOLFACTURA.Visible Then Me.FrmDEVOLFACTURA.Visible = False
    If Me.FrmENTRADA.Visible Then Me.FrmENTRADA.Visible = False
    If Me.FrmFACTURA.Visible Then Me.FrmFACTURA.Visible = False
    Me.frmINVENTARIOS.Visible = True
    Select Case Index
    Case Is = CompraDElementos
        VerMovimientoContable CompraDElementos
    Case Is = Entrada
        VerMovimientoContable Documento.Encabezado.TipDeDcmnt
    Case Is = DevolucionEntrada
        VerMovimientoContable Documento.Encabezado.TipDeDcmnt
    Case Is = Salida
        VerMovimientoContable Documento.Encabezado.TipDeDcmnt
    Case Is = FacturaVenta
        VerMovimientoContable Documento.Encabezado.TipDeDcmnt
    Case Is = DevolucionFacturaVenta
        VerMovimientoContable Documento.Encabezado.TipDeDcmnt
    Case Is = BajaConsumo
        VerMovimientoContable Documento.Encabezado.TipDeDcmnt
    Case Is = DevolucionBaja
        VerMovimientoContable Documento.Encabezado.TipDeDcmnt
    End Select
End Sub

Private Sub CmdSelec_Click(Index As Integer)
    Select Case Index
    Case 0: Codigo = NUL$
'        If UCase(UserId) = "ADMINISTRADOR" Or DepeUsua = 0 Then
'           Codigo = Seleccion("CENTRO_COSTO", "NO_NOMB_CECO", "CD_CODI_CECO,NO_NOMB_CECO", "DEPENDENCIAS", NUL$)
'        Else
'           Condicion = "CD_USUA_RUD=" & Comi & UserId & Comi
'           Condicion = Condicion & " AND CD_DEPE_RUD = CD_CODI_CECO"
'           Codigo = Seleccion("CENTRO_COSTO, R_USUA_DEPE", "NO_NOMB_CECO", "CD_CODI_CECO,NO_NOMB_CECO", "DEPENDENCIAS POR USUARIO", Condicion)
'        End If
        Codigo = Seleccion("CENTRO_COSTO", "NO_NOMB_CECO", "CD_CODI_CECO,NO_NOMB_CECO", "DEPENDENCIAS", NUL$)
        If Codigo <> NUL$ Then Me.TxtBajaD(4).Text = Codigo
        Me.TxtBajaD(4).SetFocus
    End Select
End Sub

Private Sub CmdSelecTercero_Click()
    Codigo = NUL$
    Codigo = Seleccion("TERCERO", "NO_NOMB_TERC", "CD_CODI_TERC,NO_NOMB_TERC", "TERCEROS", NUL$)
    If Codigo <> NUL$ Then txtDocum(5) = Codigo
    txtDocum(5).SetFocus
End Sub

Private Sub Documento_ArticuloProcesado(ByVal EnCual As Integer, ByVal Maxi As Integer)
'    If Me.fraTOTALES.Visible Then Me.fraTOTALES.Visible = False
    If Me.proARTICULOS.Max <> Maxi Then Me.proARTICULOS.Max = Maxi
    Me.proARTICULOS.Value = EnCual
    Me.proARTICULOS.Refresh
End Sub

Private Sub Form_KeyPress(Tecla As Integer)
    If Tecla = vbCr Then Tecla = vbTab
End Sub

Private Sub Form_Activate()
    If blnDesdeCompras Then Exit Sub
    Dim ArrEDC(3) As Variant
    If Not Me.SeActivoFormulario Then
        Me.staINFORMA.Visible = False
        Me.SeActivoFormulario = True
        Documento.Encabezado.EsNuevoDocumento = True
'        Result = LoadData("PARAMETROS_INVE", "ID_CONT_APLI, ID_PRES_APLI, CD_ORDE_APLI, CD_ARFL_APLI", NUL$, ArrEDC())
'Dim blnContab       As Boolean
'Dim blnXPagar       As Boolean
'Dim blnXCobrar      As Boolean
'        If Result <> FAIL Then blnContab = IIf(ArrEDC(0) = "S", True, False)
        blnContab = Buscar_Modulos_Integrados("PARAMETROS_INVE", "ID_CONT_APLI")
        blnXPagar = Buscar_Modulos_Integrados("PARAMETROS_INVE", "ID_CXP_APLI")
        blnXCobrar = Buscar_Modulos_Integrados("PARAMETROS_INVE", "ID_CXC_APLI")
        Me.CmdCuentas.Visible = Me.TieneContabilidad
'        Me.staExistencia.Visible = False
        Me.FraPie.Enabled = False
        Me.FraPie.Visible = True
        Me.chaSALDOS.Visible = False
        Me.staINFORMA.Panels("SALBOD").Text = "Saldos X Bodega" 'Saldos X Bodega
        Me.FraOpciones.Enabled = False
        Documento.Encabezado.TipDeDcmnt = Me.AutoTipoDocumento
        Call GrdFDef(GrdArticulos, 0, 0, Me.DefinicionDeGrilla)
        Documento.Encabezado.Actualiza
        Me.Caption = Documento.Encabezado.NombreDocumento
'        Me.FraRequi.Caption = Left(Me.Caption, 8)
'        If Documento.Encabezado.CualDepende = 0 Then Me.cboIndependiente.Visible = False: Me.lblINDEPE.Visible = False
'        If Documento.Encabezado.RomDepende Then Me.cboIndependiente.Enabled = True
        Me.FechaCierre = Documento.Encabezado.FechaCierre
        Me.UltimaFechaDocumento = UltimaFechaCreacionDocumento(Documento.Encabezado.TipDeDcmnt, Me.FechaCierre)
        Me.txtDocum(1).Text = Right(String(2, "0") & Trim(Day(Me.UltimaFechaDocumento)), 2)
        Me.txtDocum(2).Text = Right(String(2, "0") & Trim(Month(Me.FechaCierre)), 2)
        Me.txtDocum(3).Text = Right(String(4, "0") & Trim(Year(Me.FechaCierre)), 4)
        Me.txtDocum(5).Text = Documento.Encabezado.Tercero.Nit
        Documento.Encabezado.Tercero.IniXNit
        Me.txtDocum(6).Text = Documento.Encabezado.Tercero.Nombre
        Call Fecha_Docu
        Me.FilaDeGrillaActiva = GrdArticulos.Rows - 1
        Me.ColumnaDeGrillaActiva = 2
        Set WinDoc.ElDocumento = Documento
        Call CargarClassWinDoc(WinDoc, cboConsecutivo, cboBodega, cboBodDestino, _
            GrdArticulos, txtNumero, Me.AutoTipoDocumento, lstDocsPadre, cboIndependiente)
'        AutonumPerfil = 1
        WinDoc.CargarCombos
        WinDoc.ProcesarEventosGrilla = True
    End If
End Sub

Private Sub Form_Load()
    Set WinDoc = New WinDocumento
    Set Documento = New ElDocumento
    Call CenterForm(MDI_Inventarios, Me)
'    Call Leer_Permisos("02005", cmdOpciones(0), cmdOpciones(1), cmdOpciones(2))
' 1) = Me.ColAutoArticulo
' 2) = Me.ColCodigoArticulo
' 3) = Me.ColNombreArticulo
' 4) = Me.ColAutoUnidad
' 5) = Me.ColNombreUnidad
' 6) = Me.ColMultiplicador
' 7) = Me.ColDivisor
' 8) = Me.ColUltimoCosto
' 9) = Me.ColCantidad
'10) = Me.ColCostoSinIVA
'11) = Me.ColPorceIVA
'12) = Me.ColCostoUnidad
'13) = Me.ColCostoTotal
'14) = Me.ColFecVence
'15) = Me.ColFecEntrada
'16) = Me.ColCantidadFija
'17) = Me.ColAutoDocumento
'    Me.DefinicionDeGrilla = "Autoart,1," & _
'        "Codigo,1900," & _
'        "Nombre,3500," & _
'        "AutoUndMedida,1," & _
'        "Und Medida,1000," & _
'        "Multiplicador,1," & _
'        "Divisor,1," & _
'        "Cantidad,+800," & _
'        "Costo Sin IVA,+1500," & _
'        "Porce IVA,+800," & _
'        "Costo Unitario,+1500," & _
'        "Costo Total,+1500," & _
'        "Fecha Vencimiento,1200," & _
'        "Fecha Entrada,1200," & _
'        "Pertenece A,1," & _
'        "Control C/dad,1," & _
'        "Auto Doc,1"
'    GrdArticulos.FormatString = Me.DefinicionDeGrilla
    Aplicacion.SetMyCantDecCant = 0
    Aplicacion.DeseoMyCantDecCant = True
    Aplicacion.SetMyCantDecVal = 2
    Aplicacion.DeseoMyCantDecVal = True
    Me.lstSALXBOD.ListItems.Clear
    Me.lstSALXBOD.ColumnHeaders.Clear
    Me.lstSALXBOD.ColumnHeaders.Add , "COD", "C�digo", 0.2 * Me.lstSALXBOD.Width
    Me.lstSALXBOD.ColumnHeaders.Add , "NOM", "Nombre", 0.5 * Me.lstSALXBOD.Width
    Me.lstSALXBOD.ColumnHeaders.Add , "SAL", "Saldo", 0.2 * Me.lstSALXBOD.Width
    Me.lstSALXBOD.ColumnHeaders("SAL").Alignment = lvwColumnRight
    Me.lstSALXBOD.ColumnHeaders.Add , "MIN", "M�nimo", 0.2 * Me.lstSALXBOD.Width
    Me.lstSALXBOD.ColumnHeaders("MIN").Alignment = lvwColumnRight
    Me.lstSALXBOD.ColumnHeaders.Add , "REP", "Reposi", 0.2 * Me.lstSALXBOD.Width
    Me.lstSALXBOD.ColumnHeaders("REP").Alignment = lvwColumnRight
    Me.lstSALXBOD.ColumnHeaders.Add , "MAX", "M�ximo", 0.2 * Me.lstSALXBOD.Width
    Me.lstSALXBOD.ColumnHeaders("MAX").Alignment = lvwColumnRight
End Sub

Private Sub cmdOpciones_Click(Index As Integer)
    Dim Formula As String, Reporte As String, ArrCPB() As Variant
    Dim NumeroCompro As Long, FechaEntera As Long
    Dim Dueno As New ElTercero
    Dueno.IniXNit
    Documento.Encabezado.NumeroCOMPROBANTE = Me.cboConsecutivo.ItemData(Me.cboConsecutivo.ListIndex)
    Select Case Index
    Case Is = 0
        Me.fraTOTALES.Visible = False
        If Documento.Encabezado.EsNuevoDocumento And Me.GrdArticulos.Rows > 1 Then
            If BeginTran(STranIUp & "KARDEX") = FAIL Then GoTo FLLBGNTRN
            Me.fraTOTALES.Visible = False
            NumeroCompro = WinDoc.Guardar(False)
            Me.fraTOTALES.Visible = True
            If Not NumeroCompro > 0 Then GoTo FLLCMMTRS
            If Me.TieneContabilidad Then
                Select Case Documento.Encabezado.TipDeDcmnt
                Case Is = Entrada
                    calcular_entra False
                    muestra_cuenta "S", Me
                    If Not continuar Then GoTo FLLBGNTRN
'                    Call Guardar_Movimiento_Contable("ENTR", fechag, Buscar_Comprobante_Concepto(0), TxtEntra(0), Mid("ENTRADA A ALMACEN " & TxtEntra(9), 1, 250), NUL$)
                    Call Guardar_Movimiento_Contable("ENTR", Documento.Encabezado.FechaDocumento, Buscar_Comprobante_Concepto(0), NumeroCompro, Mid("ENTRADA A ALMACEN " & Documento.Encabezado.Observaciones, 1, 250), NUL$)
'                    If Buscar_Modulos_Integrados("PARAMETROS_INVE", "ID_CXP_APLI") = True Then
'                        Conse_CXP = NumeroCompro
'                        Graba_cxp Documento.Encabezado.FechaDocumento, Documento.Encabezado.FechaVencimiento
'                    End If
                    Call GuardaValoresParticulatres
                Case Is = Salida
                    calcular_salidaA
                    muestra_cuenta "S", Me
                    If Not continuar Then GoTo FLLBGNTRN
'                    Call Guardar_Movimiento_Contable("SALI", fechag, Buscar_Comprobante_Concepto(7), TxtSaliA(0), Mid("SALIDA DE ALMACEN " & TxtSaliA(6), 1, 250), NUL$)
                    Call Guardar_Movimiento_Contable("SALI", Documento.Encabezado.FechaDocumento, Buscar_Comprobante_Concepto(7), NumeroCompro, Mid("SALIDA DE ALMACEN " & Documento.Encabezado.Observaciones, 1, 250), NUL$)
                Case Is = FacturaVenta
                    calcular_venta
                    muestra_cuenta "S", Me
                    If Not continuar Then GoTo FLLBGNTRN
'                    Call Guardar_Movimiento_Contable("VENT", fechag, Buscar_Comprobante_Concepto(2), TxtVenta(0), Mid("VENTA " & TxtVenta(14), 1, 250), NUL$)
                    Call Guardar_Movimiento_Contable("VENT", Documento.Encabezado.FechaDocumento, Buscar_Comprobante_Concepto(2), NumeroCompro, Mid("VENTA " & Documento.Encabezado.Observaciones, 1, 250), NUL$)
'                    If Buscar_Modulos_Integrados("PARAMETROS_INVE", "ID_CXC_APLI") = True Then
                    If Me.TieneCtasXCobrar Then
                        Conse_CXC = NumeroCompro
                        Graba_cxc Documento.Encabezado.FechaDocumento, Documento.Encabezado.FechaVencimiento
                    End If
                    Call GuardaValoresParticulatres
                Case Is = DevolucionFacturaVenta
                    calcular_devo
                    muestra_cuenta "S", Me
                    If Not continuar Then GoTo FLLBGNTRN
'                    Call Guardar_Movimiento_Contable("DEVC", fechag, Buscar_Comprobante_Concepto(3), TxtDevo(0), Mid("DEVOLUCION DE CLIENTES " & TxtDevo(11), 1, 250), NUL$)
                    Call Guardar_Movimiento_Contable("DEVC", Documento.Encabezado.FechaDocumento, Buscar_Comprobante_Concepto(3), NumeroCompro, Mid("DEVOLUCION DE CLIENTES " & Documento.Encabezado.Observaciones, 1, 250), NUL$)
                Case Is = BajaConsumo
                    calcular_salidaD
                    muestra_cuenta "S", Me
                    If Not continuar Then GoTo FLLBGNTRN
'                    Call Guardar_Movimiento_Contable("BAJA", fechag, Buscar_Comprobante_Concepto(5), TxtBajaD(0), Mid("SALIDA DE DEPENDENCIA " & TxtBajaD(8), 1, 250), NUL$)
                    Call Guardar_Movimiento_Contable("BAJA", Documento.Encabezado.FechaDocumento, Buscar_Comprobante_Concepto(5), NumeroCompro, Mid("SALIDA DE DEPENDENCIA " & Documento.Encabezado.Observaciones, 1, 250), NUL$)
                End Select
            End If
            If CommitTran = FAIL Then GoTo FLLCMMTRS
            If NumeroCompro <> FAIL Then Call cmdOpciones_Click(3)
            GoTo SLRDLTRNS
        End If
        Me.fraTOTALES.Visible = True
FLLBGNTRN:
FLLCMMTRS:
        Call RollBackTran
SLRDLTRNS:
        Call WinDoc_CambioRenglonGrilla(0, WinDoc.Calcular_Documento(Me.ColCantidad, Me.ColCostoUnidad))
    Case Is = 1
        If Documento.Encabezado.EsNuevoDocumento Then Exit Sub
        FechaEntera = Documento.Encabezado.FechaDocumento
'        If Documento.Encabezado.EstadoDocumento = "N" And (Month(Me.FechaCierre) = Month(Documento.Encabezado.FechaDocumento)) And (Year(Me.FechaCierre) = Year(Documento.Encabezado.FechaDocumento)) Then
        If Documento.Encabezado.EstadoDocumento = "N" And CLng(Documento.Encabezado.FechaDocumento) > Me.FechaCierre Then
            Select Case Documento.Encabezado.TipDeDcmnt
            Case Is = Cotizacion
                Documento.Encabezado.TipDeDcmnt = AnulaCotizacion
            Case Is = ODCompra
                Documento.Encabezado.TipDeDcmnt = AnulaODCompra
            Case Is = Entrada
                Documento.Encabezado.TipDeDcmnt = AnulaEntrada
            Case Is = Requisicion
                Documento.Encabezado.TipDeDcmnt = AnulaRequisicion
            Case Is = Despacho
                Documento.Encabezado.TipDeDcmnt = AnulaDespacho
            Case Is = BajaConsumo
                Documento.Encabezado.TipDeDcmnt = AnulaBaja
            Case Is = Salida
                Documento.Encabezado.TipDeDcmnt = AnulaSalida
            Case Is = FacturaVenta
                Documento.Encabezado.TipDeDcmnt = AnulaFacturaVenta
            End Select
            Documento.Encabezado.Actualiza
            Campos = "NU_AUTO_COMP,TX_NOMB_COMP"
            Desde = "IN_COMPROBANTE"
            Condi = "NU_AUTO_DOCU_COMP=" & Documento.Encabezado.TipDeDcmnt
            ReDim ArrCPB(1)
            Result = LoadData(Desde, Campos, Condi, ArrCPB())
            If Result <> FAIL Then
                If Encontro Then
                    Documento.Encabezado.NumeroCOMPROBANTE = CLng(ArrCPB(0)) 'NU_AUTO_COMP
'/////////////////
'                    GrabarDocumento (NumeroCompro)
                    If BeginTran(STranIUp & "KARDEX") = FAIL Then GoTo FAILGNTRN
                    Me.fraTOTALES.Visible = False
                    NumeroCompro = WinDoc.Guardar(False)
                    Me.fraTOTALES.Visible = True
                    If Not NumeroCompro > 0 Then GoTo FAILMMTRS
                    If Me.TieneContabilidad Then
                        Select Case Documento.Encabezado.TipDeDcmnt
                        Case Is = AnulaEntrada
'                            Call Guardar_Movimiento_Contable("ENTR", fechag, Buscar_Comprobante_Concepto(0), TxtEntra(0), Mid("ENTRADA A ALMACEN " & TxtEntra(9), 1, 250), NUL$)
                            Call Eliminar_Comprobante(Buscar_Comprobante_Concepto(0), CLng(Me.txtNumero.Text))
                        Case Is = AnulaSalida
                            Call Eliminar_Comprobante(Buscar_Comprobante_Concepto(7), CLng(Me.txtNumero.Text))
                        Case Is = AnulaFacturaVenta
                            Call Eliminar_Comprobante(Buscar_Comprobante_Concepto(2), CLng(Me.txtNumero.Text))
                        Case Is = AnulaBaja
                            Call Eliminar_Comprobante(Buscar_Comprobante_Concepto(5), CLng(Me.txtNumero.Text))
                        End Select
                    End If
                    If CommitTran = FAIL Then GoTo FAILMMTRS
                    If NumeroCompro <> FAIL Then Call cmdOpciones_Click(3)
                    GoTo EXITLTRNS
                    Me.fraTOTALES.Visible = True
FAILGNTRN:
FAILMMTRS:
                    Call RollBackTran
EXITLTRNS:
'/////////////////
                Else
                    MsgBox ("No existe Comprobante para procesar la transacci�n")
                End If
            End If
            Select Case Documento.Encabezado.TipDeDcmnt
            Case Is = AnulaCotizacion
                Documento.Encabezado.TipDeDcmnt = Cotizacion
            Case Is = AnulaODCompra
                Documento.Encabezado.TipDeDcmnt = ODCompra
            Case Is = AnulaEntrada
                Documento.Encabezado.TipDeDcmnt = Entrada
            Case Is = AnulaRequisicion
                Documento.Encabezado.TipDeDcmnt = Requisicion
            Case Is = AnulaDespacho
                Documento.Encabezado.TipDeDcmnt = Despacho
            Case Is = AnulaBaja
                Documento.Encabezado.TipDeDcmnt = BajaConsumo
            Case Is = AnulaSalida
                Documento.Encabezado.TipDeDcmnt = Salida
            Case Is = AnulaFacturaVenta
                Documento.Encabezado.TipDeDcmnt = FacturaVenta
            End Select
            Documento.Encabezado.Actualiza
            WinDoc.CargarCombos
        Else
            Call MsgBox("El documento no puede ser anulado", vbInformation)
        End If
    Case Is = 2
        If Documento.Encabezado.EsNuevoDocumento Then Exit Sub
        Select Case Documento.Encabezado.TipDeDcmnt
        
'        Case Is = 14, 16, 13, 15, 18, 21, 22, 20, 23
        Case Is = AnulaBaja, AnulaDespacho, AnulaEntrada, AnulaFacturaVenta, AnulaODCompra, AnulaRequisicion, AnulaSalida, AnulaCotizacion, AnulaTraslado
            Formula = "{IN_ENCABEZADO.NU_AUTO_DOCU_ENCA}=" & Documento.Encabezado.TipDeDcmnt & _
                " AND {IN_ENCABEZADO.NU_COMP_ENCA}=" & CLng(Me.txtNumero) & _
                " AND {IN_DETALLE.NU_AUTO_BODE_DETA}=" & Me.cboBodega.ItemData(Me.cboBodega.ListIndex)
        Case DevolucionEntrada, DevolucionDespacho, DevolucionBaja, DevolucionSalida, DevolucionFacturaVenta
            Formula = "{IN_DETALLE.NU_AUTO_ORGCABE_DETA}={IN_ENCABEZADO.NU_AUTO_ENCA} AND " & _
                "{IN_DETALLE.NU_AUTO_ORGCABE_DETA}={IN_DETALLE.NU_AUTO_MODCABE_DETA} AND " & _
                "{IN_DETALLE.NU_AUTO_MODCABE_DETA}={IN_ENCABEZADO_1.NU_AUTO_ENCA} AND " & _
                "{IN_DETALLE.NU_AUTO_BODE_DETA}=" & Me.cboBodega.ItemData(Me.cboBodega.ListIndex) & " AND " & _
                "{IN_ENCABEZADO.NU_AUTO_DOCU_ENCA}=" & Documento.Encabezado.TipDeDcmnt & " AND " & _
                "{IN_ENCABEZADO.NU_COMP_ENCA}=" & CLng(Me.txtNumero)
        Case Else
            Formula = "{IN_DETALLE.NU_AUTO_ORGCABE_DETA}={IN_ENCABEZADO.NU_AUTO_ENCA} AND " & _
                "{IN_DETALLE.NU_AUTO_MODCABE_DETA}={IN_ENCABEZADO_1.NU_AUTO_ENCA} AND " & _
                "{IN_DETALLE.NU_AUTO_BODE_DETA}=" & Me.cboBodega.ItemData(Me.cboBodega.ListIndex) & " AND " & _
                "{IN_ENCABEZADO.NU_AUTO_DOCU_ENCA}=" & Documento.Encabezado.TipDeDcmnt & " AND " & _
                "{IN_ENCABEZADO.NU_COMP_ENCA}=" & CLng(Me.txtNumero)
'            Formula = "{IN_ENCABEZADO.NU_AUTO_DOCU_ENCA}=" & Documento.Encabezado.TipDeDcmnt & _
'                " AND {IN_ENCABEZADO.NU_COMP_ENCA}=" & CLng(Me.txtNumero) & _
'                " AND {IN_ENCABEZADO_1.NU_AUTO_DOCU_ENCA}=" & Documento.Encabezado.TipDeDcmnt & _
'                " AND {IN_ENCABEZADO_1.NU_COMP_ENCA}=" & CLng(Me.txtNumero) & _
'                " AND {IN_DETALLE.NU_AUTO_BODE_DETA}=" & Me.cboBodega.ItemData(Me.cboBodega.ListIndex)
        End Select
'1   COTIZA
'2   ORDEN
'3   ENTRA
'4   DEVENT
'5   REQUE
'6   DESPA
'8   BAJA
'9   DEVBJ
'10  TRASL
'11  FACTR
'12  DEVFAC
'13  ANLENT
'14  ANLBAJ
'15  ANLFAC
'16  ANLDES
'17  DEVDES
'18  ANLORD
        MDI_Inventarios.Crys_Listar.Formulas(10) = "ENTIDAD=" & Comi & Trim(Dueno.Nombre) & ", NIT:" & Trim(Dueno.Nit) & Comi
        Reporte = "infDCANL_COS.rpt"
        Select Case Documento.Encabezado.TipDeDcmnt
        Case Is = Cotizacion
            Reporte = "infDCSOL_COS.rpt"
        Case Is = ODCompra
            Reporte = "infDCODC_COS.rpt"
'        Case Is = AnulaODCompra
'            Reporte = "infDCANL_COS.rpt"
        Case Is = Entrada
            Reporte = "infDCENT_COS.rpt"
        Case Is = DevolucionEntrada
            Reporte = "infDCDVE_COS.rpt"
'        Case Is = AnulaEntrada
'                Reporte = "infDCENT_COS.rpt"
        Case Is = Requisicion
            Reporte = "infDCREQ_COS.rpt"
        Case Is = Despacho
            Reporte = "infDCDES_COS.rpt"
        Case Is = DevolucionDespacho
            Reporte = "infDCDVD_COS.rpt"
        Case Is = BajaConsumo
            Reporte = "infDCBAJ_COS.rpt"
        Case Is = DevolucionBaja
            Reporte = "infDCDVB_COS.rpt"
        Case Is = Traslado
            Reporte = "infDCTRA_COS.rpt"
        End Select
        Call ListarD(Reporte, Formula, crptToWindow, Documento.Encabezado.NombreDocumento, MDI_Inventarios, True)
        MDI_Inventarios.Crys_Listar.Formulas(10) = ""
    Case Is = 3
        Me.opcORG.Value = True
        Me.opcACT.Value = False
        Me.chaSALDOS.Visible = False
        Me.FraPie.Visible = True
'        Call GrdFDef(GrdArticulos, 0, 0, Me.DefinicionDeGrilla)
'        Me.GrdArticulos.Rows = 2
        Me.GrdArticulos.Rows = 2
        Me.cboConsecutivo.ListIndex = -1
        Me.cboBodega.ListIndex = -1
        Me.cboBodDestino.ListIndex = -1
        Me.txtDocum(1) = Format(Me.UltimaFechaDocumento, "dd")
        Me.txtDocum(2) = Format(Me.UltimaFechaDocumento, "mm")
        Me.txtDocum(3) = Format(Me.UltimaFechaDocumento, "yyyy")
        Me.txtDocum(5).Text = ""
        Me.txtDocum(6).Text = ""
        Me.txtDocum(8).Text = ""
        Me.lstDocsPadre.Clear
        'Me.GrdArticulos.Clear
        ''Para posici�n inicial en el grid
        Me.FilaDeGrillaActiva = GrdArticulos.Rows - 1
        Me.ColumnaDeGrillaActiva = 2
        WinDoc.LimpiaLineaGrilla 1, Me.ColCodigoArticulo, ""
        Documento.Encabezado.EsNuevoDocumento = True
        Me.EstaAnulado = False
        Me.FraArti.Enabled = False
        Me.FraRequi.Visible = False
        Me.FraGeneral.Enabled = True
        Me.CmdCuentas.Visible = Me.TieneContabilidad
        If Me.cboConsecutivo.Enabled Then
            Me.cboConsecutivo.SetFocus
        Else
            If Me.cboConsecutivo.ListCount > 0 Then Me.cboConsecutivo.ListIndex = 0
            Me.txtNumero.SetFocus
        End If
        Me.FraOpciones.Enabled = False
        Me.FraArti.BackColor = &H8000000A
        Me.FraPie.BackColor = &H8000000A
        Me.FraRequi.BackColor = &H8000000A
        Me.FraOpciones.BackColor = &H8000000A
        Me.FraGeneral.BackColor = &H8000000F
        WinDoc.CargarCombos
        Me.txtDocum(5).Text = Dueno.Nit
        Me.txtDocum(6).Text = Dueno.Nombre
        Me.staINFORMA.Panels("NUEVO").Text = "NUEVO"
        Me.staINFORMA.Panels("COSTO").Text = ""
        Me.staINFORMA.Panels("PARCIAL").Text = ""
        Me.staINFORMA.Panels("SALBOD").Text = "Saldos X Bodega"
        Me.staINFORMA.Visible = False
        Set Dueno = Nothing
    End Select
End Sub

Private Sub cmdOpciones_GotFocus(Index As Integer)
 Select Case Index
    Case 0: Msglin "Pulse este icono para GrabarDocumento"
    Case 1: Msglin "Pulse este icono para anular"
    Case 2: Msglin "Pulse este icono para imprimir"
    Case 3: Msglin "Pulse este icono limpiar y empezar de nuevo"
  End Select
End Sub

Private Sub BuscarDocumento()
    Dim ArrTemp()
    Dim UnRsltd As Long
    If cboConsecutivo.ListIndex = -1 Then Exit Sub
    Call Documento.REMALLArticulo
    Documento.Encabezado.NumeroCOMPROBANTE = cboConsecutivo.ItemData(cboConsecutivo.ListIndex)
    
    If Me.opcACT.Value Then
        UnRsltd = Documento.CargaDocumentoACTUAL(CLng(txtNumero))
    ElseIf Me.opcORG.Value Then
        UnRsltd = Documento.CargaDocumentORIGINAL(CLng(txtNumero))
    End If
    If UnRsltd = ResultConsulta.Encontroinfo Then
'        Select Case Documento.Encabezado.EstadoDocumento
'        Case Is = "N"
'        Case Is = "M"
'            Result = MsgBox("El documento fue modificado", vbInformation)
'        Case Is = "A"
'            Result = MsgBox("El documento fue anulado", vbInformation)
'        End Select
'        Dim ArrTYU(0) As Variant
'        Result = LoadData("PARAMETROS_INVE", "ID_CONT_APLI", NUL$, ArrTYU())
'        If ArrTYU(0) = "S" Then Call CargaValoresParticulatres
        If Me.TieneContabilidad Then Call CargaValoresParticulatres
        Select Case Me.AutoTipoDocumento
        Case Is = Entrada
            Me.DefinicionDeGrilla = "Autoart,+0,Marcar,800," & _
                "Codigo,1900," & _
                "Nombre,3500," & _
                "AutoUndMedida,+0," & _
                "Und Medida,1000," & _
                "Multiplicador,+0," & _
                "Divisor,+0," & _
                "Ultimo Costo,+0," & _
                "Cantidad,+800," & _
                "Costo Sin IVA,+0," & _
                "Porce IVA,+0," & _
                "Costo Unitario,+1500," & _
                "Costo Total,+1500," & _
                "Fecha Vencimiento,1200," & _
                "Fecha Entrada,1200," & _
                "Pertenece A,+0," & _
                "Control C/dad,+0," & _
                "Auto Doc,+0," & _
                "Select,+0"
'            Me.DefinicionDeGrilla = "Autoart,+0," & _
'                "Codigo,1900," & _
'                "Nombre,3500," & _
'                "AutoUndMedida,+0," & _
'                "Und Medida,1000," & _
'                "Multiplicador,+0," & _
'                "Divisor,+0," & _
'                "Ultimo Costo,+0," & _
'                "Cantidad,+800," & _
'                "Costo Sin IVA,+0," & _
'                "Porce IVA,+0," & _
'                "Costo Unitario,+1500," & _
'                "Costo Total,+1500," & _
'                "Fecha Vencimiento,1200," & _
'                "Fecha Entrada,1200," & _
'                "Pertenece A,+0," & _
'                "Control C/dad,+0," & _
'                "Auto Doc,+0," & _
'                "Select,+0," & _
'                "Marcar,1000"
        End Select
        Call GrdFDef(GrdArticulos, 0, 0, Me.DefinicionDeGrilla)
'        Me.cboIndependiente.Visible = True: Me.lblINDEPE.Visible = True
        Me.cboIndependiente.ListIndex = 0
        Documento.Encabezado.EsNuevoDocumento = False
        Me.staINFORMA.Panels("NUEVO").Text = "CREADO"
'        Me.UltimaFechaDocumento = CLng(Documento.Encabezado.FechaDocumento)
        Me.txtDocum(1) = Format(Documento.Encabezado.FechaDocumento, "dd")
        Me.txtDocum(2) = Format(Documento.Encabezado.FechaDocumento, "mm")
        Me.txtDocum(3) = Format(Documento.Encabezado.FechaDocumento, "yyyy")
        Me.txtDocum(5) = Documento.Encabezado.Tercero.Nit
        txtDocum(6) = Documento.Encabezado.Tercero.Nombre
        txtDocum(8) = Documento.Encabezado.Observaciones
        txtNumero.Tag = Documento.Encabezado.AutoDelDocCARGADO
        ReDim ArrTemp(0)
        Result = LoadData("IN_BODEGA", "TX_NOMB_BODE", "NU_AUTO_BODE=" & Documento.Encabezado.BodegaORIGEN, ArrTemp())
        Me.cboBodega.Clear
        If Result <> FAIL And Encontro Then
            Me.cboBodega.AddItem ArrTemp(0)
            Me.cboBodega.ItemData(Me.cboBodega.NewIndex) = Documento.Encabezado.BodegaORIGEN
            Me.cboBodega.ListIndex = Me.cboBodega.NewIndex
        End If
        Result = LoadData("IN_BODEGA", "TX_NOMB_BODE", "NU_AUTO_BODE=" & Documento.Encabezado.BodegaDESTINO, ArrTemp())
        Me.cboBodDestino.Clear
        If Result <> FAIL And Encontro Then
            Me.cboBodDestino.AddItem ArrTemp(0)
            Me.cboBodDestino.ItemData(Me.cboBodDestino.NewIndex) = Documento.Encabezado.BodegaDESTINO
            Me.cboBodDestino.ListIndex = Me.cboBodDestino.NewIndex
        End If
        GrdArticulos.Rows = 1
        Me.fraTOTALES.Visible = False
        Call Documento.LlenaFgridConArticulos(GrdArticulos, Me, Documento.Encabezado.AutoDelDocCARGADO)
        Me.fraTOTALES.Visible = True
        If Documento.Encabezado.EstadoDocumento = "A" Then
            Me.EstaAnulado = True
            Call MsgBox("Documento ANULADO", vbInformation)
        ElseIf Documento.Encabezado.EstadoDocumento = "M" Then
            Call MsgBox("Documento MODIFICADO", vbInformation)
        End If
        Me.staINFORMA.Panels("COSTO").Text = Aplicacion.Formatear_Valor(WinDoc.Calcular_Documento(Me.ColCantidad, Me.ColCostoUnidad))
        Call IrAlCuerpo
    Else
        Select Case Documento.Encabezado.TipDeDcmnt
        
        Case Is = AnulaBaja, AnulaDespacho, AnulaEntrada, AnulaFacturaVenta, AnulaODCompra, AnulaRequisicion, AnulaSalida, AnulaCotizacion, AnulaTraslado
            Me.txtNumero.SetFocus
        Case Else
            Call Fecha_Docu
        End Select
    End If
End Sub

Private Sub GrabarDocumentoOBSOLETE(ByRef Comproba As Long)
   ReDim Arr(0)
   Dim fechag As Variant
   
   DoEvents
   
   Call MouseClock
   Msglin "Ingresando informaci�n del " & Documento.Encabezado.NombreDocumento

   If txtNumero = NUL$ Then
      Call Mensaje1("Se deben especificar los datos completos del " & Documento.Encabezado.NombreDocumento, 3)
      Call MouseNorm: Exit Sub
   End If
   
'   Call Calcular_Documento
   If GrdArticulos.TextMatrix(1, Me.ColAutoArticulo) = NUL$ Then
      Call Mensaje1(Documento.Encabezado.NombreDocumento & " sin Art�culos", 3)
      Call MouseNorm: Exit Sub
   End If
   
   If txtDocum(1) = NUL$ Or txtDocum(2) = NUL$ Or txtDocum(3) = NUL$ Then
      If Buscar_Fecha_Cierre(txtDocum(1), txtDocum(2), txtDocum(3)) = False Then
         Call cmdOpciones_Click(3)
      End If
   End If
   fechag = txtDocum(1) & "/" & txtDocum(2) & "/" & txtDocum(3)
   
   If IsDate(fechag) = False Then
      Call Mensaje1("Revise la fecha", 3)
      Call MouseNorm: Exit Sub
   End If
   Documento.Encabezado.FechaDocumento = fechag
'    Me.proARTICULOS.Value = 0
'    Me.proARTICULOS.Visible = True
'    Me.proARTICULOS.Min = 0
'    Me.proARTICULOS.Max = GrdArticulos.Rows
   Me.fraTOTALES.Visible = False
   Comproba = WinDoc.Guardar(True)
   Me.fraTOTALES.Visible = True
'   proARTICULOS.Visible = False
   If Comproba <> FAIL Then Call cmdOpciones_Click(3)
   Call MouseNorm
   Msglin NUL$
End Sub

Private Sub AnularDocumento()
   DoEvents
   
   Call MouseClock
   
   Msglin "Anulando un " & Documento.Encabezado.NombreDocumento
   If (txtNumero = NUL$) Then
       Call Mensaje1("Ingrese el N�mero del " & Documento.Encabezado.NombreDocumento, 1)
       Call MouseNorm: Msglin NUL$: Exit Sub
   End If
   'valida la fecha de cierre
   If Valid_Fecha_Cie(CDate(Me.txtDocum(1) & "/" & Me.txtDocum(2) & "/" & Me.txtDocum(3))) Then Call MouseNorm: Exit Sub
   Call MouseNorm
   Msglin NUL$
End Sub

Private Sub staINFORMA_PanelClick(ByVal Panel As ComctlLib.Panel)
    Select Case Panel.Key
    Case Is = "lblCOS"
        Me.staINFORMA.Panels("COSTO").Text = Aplicacion.Formatear_Valor(WinDoc.Calcular_Documento(Me.ColCantidad, Me.ColCostoUnidad))
    Case Is = "lblPAR"
        Me.staINFORMA.Panels("PARCIAL").Text = Aplicacion.Formatear_Valor(WinDoc.Calcular_Renglon(Me.FilaDeGrillaActiva, Me.ColCantidad, Me.ColCostoUnidad, Me.ColMultiplicador, Me.ColDivisor))
    Case Is = "SALBOD"
        If Me.FraPie.Visible Then
            Me.staINFORMA.Panels("SALBOD").Text = "Observaciones"
            Me.FraPie.Visible = False
            Me.lstSALXBOD.Visible = False
            Me.chaSALDOS.Visible = True
            Call WinDoc.vFGrid_RowColChange
        ElseIf Me.chaSALDOS.Visible Then
            Me.staINFORMA.Panels("SALBOD").Text = "Saldos X Bodega"
            Me.chaSALDOS.Visible = False
            Me.FraPie.Visible = False
            Me.lstSALXBOD.Visible = True
        ElseIf Me.lstSALXBOD.Visible Then
            Me.staINFORMA.Panels("SALBOD").Text = "Saldos X Bodega"
            Me.lstSALXBOD.Visible = False
            Me.chaSALDOS.Visible = False
            Me.FraPie.Visible = True
        End If
    End Select
End Sub

Private Sub txtDocum_GotFocus(Index As Integer)
  txtDocum(Index).SelStart = 0
  txtDocum(Index).SelLength = Len(txtDocum(Index).Text)
  Select Case Index
    Case 0: Msglin "Digite el N�mero del " & Documento.Encabezado.NombreDocumento
    Case 1: Msglin "Digite la Fecha del " & Documento.Encabezado.NombreDocumento
            If txtNumero = NUL$ Then txtNumero.SetFocus: Exit Sub
    Case 4: Msglin "Digite la Bodega del " & Documento.Encabezado.NombreDocumento
    Case 9: Msglin "Digite las Observaciones del " & Documento.Encabezado.NombreDocumento
  End Select
End Sub

Private Sub txtDocum_KeyDown(Index As Integer, KeyCode As Integer, Shift As Integer)
    Select Case Index
    Case 0: If KeyCode = vbKeyDown Then txtDocum(1).SetFocus: Exit Sub
    Case 1:
        If KeyCode = vbKeyUp Then
            If txtNumero.Enabled = True Then
               txtNumero.SetFocus
            End If
            Exit Sub
        End If
        If KeyCode = vbKeyDown Then
'            If txtDocum(4).Enabled = True Then
'               txtDocum(4).SetFocus
'            End If
            Exit Sub
        End If
    Case 4:
        If KeyCode = vbKeyUp Then
            If txtDocum(1).Enabled = True Then
               txtDocum(1).SetFocus
            End If
            Exit Sub
        End If
        If KeyCode = vbKeyDown Then
            cmdDocsPadre(0).SetFocus
            Exit Sub
        End If
    End Select
End Sub

Private Sub txtDocum_KeyPress(Index As Integer, KeyAscii As Integer)
 Select Case Index
   Case 0, 1:
          Call ValKeyNum(KeyAscii)
          If KeyAscii = vbKeyReturn Then
             If Index = 0 Then
                txtDocum(1).SetFocus
             End If
             If Index = 1 Then
                txtDocum(4).SetFocus
             End If
          End If
    Case 5:
        Call Cambiar_Enter(KeyAscii)
   Case 4, 6:
          Call ValKeyAlfaNum(KeyAscii)
          If KeyAscii = vbKeyReturn Then
             If Index = 4 Then
                cmdDocsPadre(0).SetFocus
             End If
          End If
 End Select
End Sub

Private Sub txtDocum_LostFocus(Index As Integer)
     Msglin NUL$
     Select Case Index
        Case Is = 0
            If txtNumero <> NUL$ Then BuscarDocumento
        Case Is = 1
            If Documento.Encabezado.EsNuevoDocumento Then
                If txtDocum(1) = NUL$ And txtNumero <> NUL$ Then txtDocum(1) = Format(Me.UltimaFechaDocumento, "dd")
                If Not IsDate(txtDocum(1) & "/" & txtDocum(2) & "/" & txtDocum(3)) Then
                   Call Mensaje1("Fecha no V�lida", 3)
                   txtDocum(1) = Format(Me.UltimaFechaDocumento, "dd")
                   txtDocum(1).SetFocus
                   Exit Sub
                End If
                If Me.UltimaFechaDocumento > 0 Then
                   If CDate(Me.UltimaFechaDocumento) > CDate(txtDocum(1) & "/" & txtDocum(2) & "/" & txtDocum(3)) Then
                        Call Mensaje1("No puede devolver la fecha del documento", 2)
                        txtDocum(1) = Format(Me.UltimaFechaDocumento, "dd")
                        txtDocum(1).SetFocus
                   End If
                End If
            End If
        Case Is = 5
            If txtDocum(5) <> NUL$ Then BuscarTercero
     End Select
End Sub

Private Sub GrdArticulos_GotFocus()
    On Error GoTo salto
    GrdArticulos.Row = Me.FilaDeGrillaActiva
    'GrdArticulos.col = me.ColumnaDeGrillaActiva
    If GrdArticulos.Row >= 6 Then GrdArticulos.TopRow = GrdArticulos.Row
salto:
    On Error GoTo 0
End Sub

Private Sub GrdArticulos_LostFocus()
'If EncontroT = False Then Calcular_Documento
''El foco del grid de Art�culos est� en la
    Me.FilaActiva = GrdArticulos.Row
 ''El foco del grid de Art�culos est� en la
 ''primera casilla por defecto
Me.FilaDeGrillaActiva = GrdArticulos.Rows - 1
Me.ColumnaDeGrillaActiva = 2
End Sub

Private Sub Fecha_Docu()
Dim FC As String
    If Me.UltimaFechaDocumento > 0 Then
       FC = txtDocum(1) & "/" & txtDocum(2) & "/" & txtDocum(3)
       If IsDate(FC) Then
          If CDate(FC) < CDate(Me.UltimaFechaDocumento) Then
             txtDocum(1) = Format(Me.UltimaFechaDocumento, "dd")
             txtDocum(2) = Format(Me.UltimaFechaDocumento, "mm")
             txtDocum(3) = Format(Me.UltimaFechaDocumento, "yyyy")
          End If
       Else
          txtDocum(1) = Format(Me.UltimaFechaDocumento, "dd")
          txtDocum(2) = Format(Me.UltimaFechaDocumento, "mm")
          txtDocum(3) = Format(Me.UltimaFechaDocumento, "yyyy")
       End If
    End If
End Sub
'
'Private Sub txtDocum(5)_KeyPress(KeyAscii As Integer)
'Call Cambiar_Enter(KeyAscii)
'End Sub
'

Private Sub BuscarTercero()
ReDim Arr(0)
   Condicion = "CD_CODI_TERC=" & Comi & Cambiar_Comas_Comillas(txtDocum(5)) & Comi
               
   Result = LoadData("TERCERO", "NO_NOMB_TERC", Condicion, Arr())
   If (Result <> False) Then
     If Encontro Then
        Me.txtDocum(6) = Arr(0)
     Else
        Me.txtDocum(5) = NUL$
        Me.txtDocum(6) = NUL$
        If Me.txtDocum(5).Enabled Then Me.txtDocum(5).SetFocus
     End If
   End If
End Sub

Private Sub TxtNumero_GotFocus()
'    If Not Me.fraTOTALES.Visible Then Me.fraTOTALES.Visible = True
    Msglin "Digite el N�mero de la " & Documento.Encabezado.NombreDocumento
End Sub

Private Sub TxtNumero_LostFocus()
    If txtNumero <> NUL$ Then BuscarDocumento
End Sub

Function Cambiar_Comas_Comillas(cadena As String) As String
   cadena = Trim$(cadena)
   cadena = Replace(cadena, "'", "^")
   cadena = Replace(cadena, ",", "*")
   Cambiar_Comas_Comillas = cadena
End Function
'blnDesdeCompras
Public Property Get TieneContabilidad() As Boolean
    TieneContabilidad = blnContab
End Property

Public Property Let EsDesdeCompras(lgi As Boolean)
    blnDesdeCompras = lgi
End Property

Public Property Get EsDesdeCompras() As Boolean
    EsDesdeCompras = blnDesdeCompras
End Property

Public Property Get TieneCtasXCobrar() As Boolean
    TieneCtasXCobrar = blnXCobrar
End Property

Public Property Get TieneCtasXPagar() As Boolean
    TieneCtasXPagar = blnXPagar
End Property

Public Property Let AutoTipoDocumento(num As Long)
    AuTiDoc = num
End Property

Public Property Get AutoTipoDocumento() As Long
    AutoTipoDocumento = AuTiDoc
End Property

Public Property Let SeActivoFormulario(lgi As Boolean)
    SeActi = lgi
End Property

Public Property Get SeActivoFormulario() As Boolean
    SeActivoFormulario = SeActi
End Property

Public Property Let EstaAnulado(lgi As Boolean)
    EsAnul = lgi
End Property

Public Property Get EstaAnulado() As Boolean
    EstaAnulado = EsAnul
End Property

Public Property Let FilaActiva(num As Long)
    FiActiv = num
End Property

Public Property Get FilaActiva() As Long
    FilaActiva = FiActiv
End Property

Public Property Let FilaDeGrillaActiva(num As Long)
    fofil = num
End Property

Public Property Get FilaDeGrillaActiva() As Long
    FilaDeGrillaActiva = fofil
End Property

Public Property Let ColumnaDeGrillaActiva(num As Long)
    focol = num
End Property

Public Property Get ColumnaDeGrillaActiva() As Long
    ColumnaDeGrillaActiva = focol
End Property

Public Property Let UltimaFechaDocumento(cue As Long)
    FechUlt = cue
End Property

Public Property Get UltimaFechaDocumento() As Long
    UltimaFechaDocumento = FechUlt
End Property

Public Property Let FechaCierre(num As Long)
    FechCie = num
End Property

Public Property Get FechaCierre() As Long
    FechaCierre = FechCie
End Property

Public Property Let DefinicionDeGrilla(cue As String)
    DefDGrl = cue
End Property

Public Property Get DefinicionDeGrilla() As String
    DefinicionDeGrilla = DefDGrl
End Property

Public Property Let ElContenidoDeCelda(cue As String)
    CntGrll = cue
End Property

Public Property Get ElContenidoDeCelda() As String
    ElContenidoDeCelda = CntGrll
End Property

'vGriAutoArticulo, vGriCodigoArticulo, vGriNombreArticulo, vGriAutoUnidad, vGriNombreUnidad
'vGriMultiplicador, vGriDivisor, vGriCantidad, vGriCostoUnidad, vGriCostoTotal
'vGriFecVence, vGriFecEntrada, vGriCantidadFija, vGriAutoDocumento

Public Property Let ColAutoArticulo(num As Byte)
    vGriAutoArticulo = num
End Property

Public Property Get ColAutoArticulo() As Byte
    ColAutoArticulo = vGriAutoArticulo
End Property

Public Property Let ColCodigoArticulo(num As Byte)
    vGriCodigoArticulo = num
End Property

Public Property Get ColCodigoArticulo() As Byte
    ColCodigoArticulo = vGriCodigoArticulo
End Property

Public Property Let ColNombreArticulo(num As Byte)
    vGriNombreArticulo = num
End Property

Public Property Get ColNombreArticulo() As Byte
    ColNombreArticulo = vGriNombreArticulo
End Property

Public Property Let ColAutoUnidad(num As Byte)
    vGriAutoUnidad = num
End Property

Public Property Get ColAutoUnidad() As Byte
    ColAutoUnidad = vGriAutoUnidad
End Property

Public Property Let ColNombreUnidad(num As Byte)
    vGriNombreUnidad = num
End Property

Public Property Get ColNombreUnidad() As Byte
    ColNombreUnidad = vGriNombreUnidad
End Property

Public Property Let ColMultiplicador(num As Byte)
    vGriMultiplicador = num
End Property

Public Property Get ColMultiplicador() As Byte
    ColMultiplicador = vGriMultiplicador
End Property

Public Property Let ColDivisor(num As Byte)
    vGriDivisor = num
End Property

Public Property Get ColDivisor() As Byte
    ColDivisor = vGriDivisor
End Property

Public Property Let ColCantidad(num As Byte)
    vGriCantidad = num
End Property

Public Property Get ColCantidad() As Byte
    ColCantidad = vGriCantidad
End Property

Public Property Let ColCostoUnidad(num As Byte)
    vGriCostoUnidad = num
End Property

Public Property Get ColCostoUnidad() As Byte
    ColCostoUnidad = vGriCostoUnidad
End Property

Public Property Let ColCostoTotal(num As Byte)
    vGriCostoTotal = num
End Property

Public Property Get ColCostoTotal() As Byte
    ColCostoTotal = vGriCostoTotal
End Property

'vGriFecVence, vGriFecEntrada, vGriCantidadFija, vGriAutoDocumento

Public Property Let ColFecVence(num As Byte)
    vGriFecVence = num
End Property

Public Property Get ColFecVence() As Byte
    ColFecVence = vGriFecVence
End Property

Public Property Let ColFecEntrada(num As Byte)
    vGriFecEntrada = num
End Property

Public Property Get ColFecEntrada() As Byte
    ColFecEntrada = vGriFecEntrada
End Property

Public Property Let ColCantidadFija(num As Byte)
    vGriCantidadFija = num
End Property

Public Property Get ColCantidadFija() As Byte
    ColCantidadFija = vGriCantidadFija
End Property

Public Property Let ColAutoDocumento(num As Byte)
    vGriAutoDocumento = num
End Property

Public Property Get ColAutoDocumento() As Byte
    ColAutoDocumento = vGriAutoDocumento
End Property

Public Property Let ColUltimoCosto(num As Byte)
    vGriUltimoCosto = num
End Property

Public Property Get ColUltimoCosto() As Byte
    ColUltimoCosto = vGriUltimoCosto
End Property

'vGriCostoSinIVA, vGriPorceIVA

Public Property Let ColCostoSinIVA(num As Byte)
    vGriCostoSinIVA = num
End Property

Public Property Get ColCostoSinIVA() As Byte
    ColCostoSinIVA = vGriCostoSinIVA
End Property

Public Property Let ColPorceIVA(num As Byte)
    vGriPorceIVA = num
End Property

Public Property Get ColPorceIVA() As Byte
    ColPorceIVA = vGriPorceIVA
End Property

Public Property Let Seleccionada(num As Byte)
    vSeleccionada = num
End Property

Public Property Get Seleccionada() As Byte
    Seleccionada = vSeleccionada
End Property

Public Property Let Marcar(num As Byte)
    vMarcar = num
End Property

Public Property Get Marcar() As Byte
    Marcar = vMarcar
End Property

Public Property Let ProcesaEntradaXlotes(bol As Boolean)
    vProXLts = bol
End Property

Public Property Get ProcesaEntradaXlotes() As Boolean
    ProcesaEntradaXlotes = vProXLts
End Property

Public Property Let CambieAlgoEnCeldaDeGrilla(bol As Boolean)
    vCbCell = bol
End Property

Public Property Get CambieAlgoEnCeldaDeGrilla() As Boolean
    CambieAlgoEnCeldaDeGrilla = vCbCell
End Property

'ColAutoArticulo, ColCodigoArticulo, ColNombreArticulo, ColAutoUnidad, ColNombreUnidad
'ColMultiplicador, ColDivisor, ColUltimoCosto, ColCantidad, ColCostoUnidad, ColCostoTotal
'ColFecVence, ColFecEntrada, ColCantidadFija, ColAutoDocumento
' 0) = Articulo.Numero
' 1) = Articulo.Codigo
' 2) = Articulo.Nombre
' 3) = Articulo.UnidadXBodega(vAutoBodegaOrigen).Autonumerico
' 4) = Articulo.UnidadXBodega(vAutoBodegaOrigen).NombreDeUnidad
' 5) = Articulo.UnidadXBodega(vAutoBodegaOrigen).Multiplicador
' 6) = Articulo.UnidadXBodega(vAutoBodegaOrigen).Divisor
' 7) = Aplicacion.Formatear_Cantidad(0) 'Ultimo costo
' 8) = Aplicacion.Formatear_Cantidad(0) 'cantidad
' 9) = Aplicacion.Formatear_Valor(0) 'Costo Promedio Unitario
'10) = Aplicacion.Formatear_Valor(0) 'Costo Promedio Total
'11) = Fecha de vencimiento
'12) = Fecha de entrada
'13) = Cantidad repetido para las devoluciones
'14) = Auto del doc cargado

' 0) = Screen.ActiveForm.ColAutoArticulo
' 1) = Screen.ActiveForm.ColCodigoArticulo
' 2) = Screen.ActiveForm.ColNombreArticulo
' 3) = Screen.ActiveForm.ColAutoUnidad
' 4) = Screen.ActiveForm.ColNombreUnidad
' 5) = Screen.ActiveForm.ColMultiplicador
' 6) = Screen.ActiveForm.ColDivisor
' 7) = Screen.ActiveForm.ColUltimoCosto
' 8) = Screen.ActiveForm.ColCantidad
' 9) = Screen.ActiveForm.ColCostoUnidad
'10) = Screen.ActiveForm.ColCostoTotal
'11) = Screen.ActiveForm.ColFecVence
'12) = Screen.ActiveForm.ColFecEntrada
'13) = Screen.ActiveForm.ColCantidadFija
'14) = Screen.ActiveForm.ColAutoDocumento

Private Sub CargarClassWinDoc(ByRef WinDoc As WinDocumento, _
        ByRef cboConsecutivo As VB.ComboBox, ByRef cboBodega As VB.ComboBox, _
        ByRef CboBodegaDest As VB.ComboBox, ByRef Fgrid As MSFlexGrid, _
        ByRef txtNumero As VB.TextBox, ByVal TipoDoc As TipoDocumento, _
        ByRef LstDocuPadre As VB.ListBox, ByRef cboIndpn As VB.ComboBox)
'    WinDoc.TipoDoc = TipoDoc
    Set WinDoc.cboConsecutivo = cboConsecutivo
    Set WinDoc.CboBodegaOrigen = cboBodega
    Set WinDoc.CboBodegaDestino = CboBodegaDest
    Set WinDoc.cboIndependiente = cboIndpn
    Set WinDoc.FgridArtic = Fgrid
    Set WinDoc.txtNumero = txtNumero
    Set WinDoc.LstDocuPadre = LstDocuPadre
End Sub

Private Sub WinDoc_CambioComprobante(ByVal RompeDepe As Boolean)
    Me.cboIndependiente.Enabled = True
    Me.cboIndependiente.Visible = False
    Me.lblINDEPE.Visible = False
    Me.cboIndependiente.ListIndex = 0
    If Not WinDoc.ElConsecutivo.RompeDependencia Then Exit Sub
    If Documento.Encabezado.CualDepende = 0 Then
        Me.cboIndependiente.ListIndex = 1
        Me.cboIndependiente.Enabled = False
        Me.cboIndependiente.Visible = True
    Else
        Me.cboIndependiente.Visible = True
    End If
    Me.lblINDEPE.Visible = True
End Sub

Private Sub WinDoc_CambioRenglonGrilla(ByVal fila As Double, ByVal TOTAL As Double)
'    Me.staINFORMA.Panels("COSTO").Text = Aplicacion.Formatear_Valor(Calcular_Documento(Me.ColCantidad, Me.ColCostoUnidad))
'    Me.staINFORMA.Panels("PARCIAL").Text = Aplicacion.Formatear_Valor(Calcular_Renglon(Me.FilaDeGrillaActiva, Me.ColCantidad, Me.ColCostoUnidad))
    Me.txtSUBTO.Text = Format(TOTAL - WinDoc.ImpuestoDeLaGrilla, "#,##0")
    Me.txtIMPUE.Text = Format(WinDoc.ImpuestoDeLaGrilla, "#,##0")
    Me.txtTOTAL.Text = Format(TOTAL, "#,##0")
    Me.staINFORMA.Panels("COSTO").Text = Aplicacion.Formatear_Valor(TOTAL)
    Me.staINFORMA.Panels("PARCIAL").Text = Aplicacion.Formatear_Valor(fila)
    Me.staINFORMA.Panels("RNGLNS").Text = Aplicacion.Formatear_Cantidad(WinDoc.NumeroArticulosGrilla)
'RNGLNS
End Sub

Private Sub WinDoc_ExisteBodegaDestino(ByVal NumItems As Long)
    Me.cboBodDestino.Visible = False
    Me.cboBodDestino.Enabled = False
    Me.lblDESTINO.Visible = False
    If Not NumItems > 0 Then Exit Sub
    Me.lblDESTINO.Visible = True
    Me.cboBodDestino.Visible = True
    Me.cboBodDestino.Enabled = True
    If Not Me.cboBodDestino.ListCount = 1 Then Exit Sub
    Me.cboBodDestino.ListIndex = 0
    Me.cboBodDestino.Enabled = False
End Sub

Private Sub CmdCuentas_Click()
    Me.frmINVENTARIOS.Visible = False
    Select Case Documento.Encabezado.TipDeDcmnt
    Case Is = Entrada
'        If Not Documento.NroDeArticulos > 0 Then
'            Dim Lari As VB.Form
'            Set Lari = FrmFASECOMPRA
'            Lari.Proveedor = Documento.Encabezado.Tercero.Nit
'            Lari.QuienLoLlama = Me
'            Lari.Show 1
'            If Me.GrdArti.Rows > 1 Then
''                calcular_entra Me.ProcesaEntradaXlotes
'                Call CentraMarco(Me.frmCOMPRAS)
'                Me.frmCOMPRAS.Visible = True
'                Call cmdPROCEDER_Click(CompraDElementos)
''                If Buscar_Modulos_Integrados("PARAMETROS_INVE", "ID_CXP_APLI") = True Then
''                    Documento.Encabezado.FechaVencimiento = CDate(DateAdd("d", CInt(Me.TxtCompra(4).Text), Documento.Encabezado.FechaDocumento))
''                    Graba_cxp Documento.Encabezado.FechaDocumento, Documento.Encabezado.FechaVencimiento
''                End If
'            Else
'                Me.frmINVENTARIOS.Visible = True
'            End If
'        Else
'            Me.frmINVENTARIOS.Visible = True
'        End If
        calcular_entra False
        Call CentraMarco(Me.FrmENTRADA)
        Me.FrmENTRADA.Visible = True
    Case Is = DevolucionEntrada
        Call cmdPROCEDER_Click(DevolucionEntrada)
    Case Is = Salida
        Call calcular_salidaA
        Me.frmSALIDA.Visible = True
        Call CentraMarco(Me.frmSALIDA)
    Case Is = FacturaVenta
        Call calcular_venta
        Call CentraMarco(Me.FrmFACTURA)
        Me.FrmFACTURA.Visible = True
    Case Is = DevolucionFacturaVenta
        Call calcular_devo
        Call cmdPROCEDER_Click(DevolucionFacturaVenta)
    Case Is = BajaConsumo
        Call calcular_salidaD
        Call CentraMarco(Me.frmBAJA)
        Me.frmBAJA.Visible = True
    Case Is = DevolucionBaja
    Case Else
        Me.frmINVENTARIOS.Visible = True
    End Select
End Sub
   
Private Sub VerMovimientoContable(ParaCual As Integer)
   'PASO A CONTA
    Select Case ParaCual
    Case Is = CompraDElementos
    Case Is = Entrada
        SiMuestra = True
        If Documento.Encabezado.EsNuevoDocumento Then
            calcular_entra Me.ProcesaEntradaXlotes
            muestra_cuenta "S", Me
        Else
        'Trae cuentas grabadas
            Call Mostrar_Movimiento_Contable("ENTR", Me.txtNumero)
            muestra_cuenta "N", Me
        End If
    Case Is = DevolucionEntrada
    Case Is = Salida
        SiMuestra = True
        If Documento.Encabezado.EsNuevoDocumento Then
            Call calcular_salidaA
            muestra_cuenta "S", Me
        Else
        'Trae cuentas grabadas
            Call Mostrar_Movimiento_Contable("SALI", Me.txtNumero)
            muestra_cuenta "N", Me
        End If
    Case Is = FacturaVenta
        SiMuestra = True
        If Documento.Encabezado.EsNuevoDocumento Then
            Call calcular_venta
            muestra_cuenta "S", Me
        Else
        'Trae cuentas grabadas
            Call Mostrar_Movimiento_Contable("VENT", Me.txtNumero)
            muestra_cuenta "N", Me
        End If
    Case Is = DevolucionFacturaVenta
        If Documento.Encabezado.EsNuevoDocumento Then
            Call calcular_devo
            muestra_cuenta "S", Me
        Else
        'Trae cuentas grabadas
            Call Mostrar_Movimiento_Contable("DEVC", Me.txtNumero)
            muestra_cuenta "N", Me
        End If
    Case Is = BajaConsumo
        If Documento.Encabezado.EsNuevoDocumento Then
            Call calcular_salidaD
            muestra_cuenta "S", Me
        Else
        'Trae cuentas grabadas
            Call Mostrar_Movimiento_Contable("BAJA", Me.txtNumero)
            muestra_cuenta "N", Me
        End If
    Case Is = DevolucionBaja
    End Select
   ''''''''''''''
End Sub

Private Sub calcular_entra(xLote As Boolean)
    Dim columna As Long 'Para que conserve la posici�n
    Dim fila As Long 'Para que conserve la posici�n
    '''''''''''
    Dim bruto, neto As Double
    Dim impuesto As Double
    Dim retef, ReteIVA, ReteICA As Double
    Dim descu As Double
    Dim descu2 As Double
    Dim fletes As Double
    Dim fletes2 As Double
    Dim desitem As Double
    Dim desitema As Double
    Dim desitemv As Double
    Dim Costo As Double
    Dim items As Integer
    Dim aux As Integer
    Dim SumCosto As Double
    'Dim vbase As Double
    'PASO A CONTA
    Dim valo_cont(1 To 6) As Variant
'/////////////
    If Not xLote Then CargaGrillaInterface Me
'/////////////
    columna = GrdArti.Col
    fila = GrdArti.Row
    VBase = 0
    '''
    '''Suma todos los descuentos
    descu2 = 0
    aux = 1
    Do While aux <= 20
        If Mdesc(aux, 2) = "" Then
           aux = 21
        Else
           If Trim(Mdesc(aux, 2)) = NUL$ Then Mdesc(aux, 2) = 0
           descu2 = descu2 + CDbl(Mdesc(aux, 2))
        End If
        aux = aux + 1
    Loop
    ''''''''
    If GrdArti.TextMatrix(GrdArti.Rows - 1, 1) <> "" Then
        items = GrdArti.Rows - 1
    Else
        items = GrdArti.Rows - 2
    End If
    '''Divide el valor de los descuentos en todos los art�culos
    If items <> 0 Then
        desitemv = descu2 / items
    End If
    ''''''''''''
    ''Divide el valor de los fletes en cada item
    If TxtEntra(14).Text = "" Then
        TxtEntra(14).Text = "0"
    End If
    
    If Trim(TxtEntra(14).Text) = NUL$ Then TxtEntra(14).Text = 0
    fletes = CDbl(TxtEntra(14).Text)
    If items <> 0 Then fletes2 = fletes / items
    ''''''''''''
    GrdArti.Row = 0
    desitema = 0
    bruto = 0
    SumCosto = 0
    If Not IsNumeric(TxtEntra(7).Text) Then TxtEntra(7).Text = "0"
    On Error GoTo salto
    While GrdArti.Row <= GrdArti.Rows - 1
        GrdArti.Row = GrdArti.Row + 1
        If GrdArti.TextMatrix(GrdArti.Row, 1) <> NUL$ Then
            If GrdArti.TextMatrix(GrdArti.Row, 2) = "" Or IsNumeric(GrdArti.TextMatrix(GrdArti.Row, 2)) = False Then
              GrdArti.TextMatrix(GrdArti.Row, 2) = "0"
            End If
            If GrdArti.TextMatrix(GrdArti.Row, 3) = "" Or IsNumeric(GrdArti.TextMatrix(GrdArti.Row, 3)) = False Then
              GrdArti.TextMatrix(GrdArti.Row, 3) = "0"
            End If
            If GrdArti.TextMatrix(GrdArti.Row, 5) = "" Or IsNumeric(GrdArti.TextMatrix(GrdArti.Row, 5)) = False Then
              GrdArti.TextMatrix(GrdArti.Row, 5) = "0"
            End If
            GrdArti.TextMatrix(GrdArti.Row, 4) = CDbl(GrdArti.TextMatrix(GrdArti.Row, 2)) * CDbl(GrdArti.TextMatrix(GrdArti.Row, 3))
            Costo = CDbl(GrdArti.TextMatrix(GrdArti.Row, 2)) * CDbl(GrdArti.TextMatrix(GrdArti.Row, 3))
            desitem = Round((CSng(TxtEntra(7).Text) * Costo) / 100)
            desitema = Round(desitema + desitem)
            SumCosto = SumCosto + (Costo - desitem - desitemv)
            Costo = (((Costo - desitem - desitemv) * CDbl(GrdArti.TextMatrix(GrdArti.Row, 5))) / 100)
      
     ' SILVIA : SE LE AGREGO LA VARIABLE QUE LE RESTA EL DESCUENTO
            VBase = VBase + CDbl(IIf(GrdArti.TextMatrix(GrdArti.Row, 5) <> 0, val(GrdArti.TextMatrix(GrdArti.Row, 4)) - val(desitem), 0))
      
            Costo = Round(Costo)
            Costo = Aplicacion.Formatear_Valor(Costo)
      ''Si la empresa es del estado el iva hace parte del costo
            If Empresa_Comercial = False Then
                GrdArti.TextMatrix(GrdArti.Row, 4) = Costo + fletes2 + (CDbl(GrdArti.TextMatrix(GrdArti.Row, 2)) * CDbl(GrdArti.TextMatrix(GrdArti.Row, 3))) - desitem - desitemv
            Else
                GrdArti.TextMatrix(GrdArti.Row, 4) = fletes2 + (CDbl(GrdArti.TextMatrix(GrdArti.Row, 2)) * CDbl(GrdArti.TextMatrix(GrdArti.Row, 3))) - desitem - desitemv
            End If
            ''''''''''''
            GrdArti.TextMatrix(GrdArti.Row, 4) = Aplicacion.Formatear_Valor(GrdArti.TextMatrix(GrdArti.Row, 4))
            GrdArti.TextMatrix(GrdArti.Row, 6) = Costo
            GrdArti.TextMatrix(GrdArti.Row, 8) = GrdArti.TextMatrix(GrdArti.Row, 4) - fletes2
            GrdArti.TextMatrix(GrdArti.Row, 8) = Aplicacion.Formatear_Valor(GrdArti.TextMatrix(GrdArti.Row, 8))
        End If
    Wend
salto:
    On Error GoTo 0
    impuesto = Round(sumar_items(GrdArti, 6))
    ''Si la empresa es del estado el iva hace parte del costo
    If Empresa_Comercial = False Then
        bruto = sumar_items(GrdArti, 4) - impuesto - fletes + descu2 + desitema
        'bruto = bruto - impuesto - fletes
    Else
        bruto = sumar_items(GrdArti, 4) - fletes + descu2 + desitema
        'bruto = bruto - fletes
    End If
    '''''''''''''
'    If TxtEntra(7) = NUL$ Then TxtEntra(7) = 0
'    If TxtEntra(8) = NUL$ Then TxtEntra(8) = 0
'    If TxtEntra(16) = NUL$ Then TxtEntra(16) = 0
'    If TxtEntra(17) = NUL$ Then TxtEntra(17) = 0
'    If TxtEntra(20) = NUL$ Then TxtEntra(20) = 0
    If Not IsNumeric(TxtEntra(7).Text) Then TxtEntra(7) = 0
    If Not IsNumeric(TxtEntra(8).Text) Then TxtEntra(8) = 0
    If Not IsNumeric(TxtEntra(16).Text) Then TxtEntra(16) = 0
    If Not IsNumeric(TxtEntra(17).Text) Then TxtEntra(17) = 0
    If Not IsNumeric(TxtEntra(20).Text) Then TxtEntra(20) = 0
    descu = Round((CSng(TxtEntra(7).Text) * bruto) / 100)
    retef = Round((CSng(TxtEntra(8).Text) * SumCosto) / 100)
    ReteIVA = Round((CSng(TxtEntra(16).Text) * impuesto) / 100)
    ReteICA = Round((CSng(TxtEntra(17).Text) * SumCosto) / 100)
    neto = bruto - descu - descu2 - ReteIVA - ReteICA - CDbl(TxtEntra(20))
    
    'Si el proveedor cobra los fletes o son por aparte
    If OptProveedor.Value = True Then
        neto = neto + impuesto + fletes - retef
    Else
        neto = neto + impuesto - retef
    End If
    
    descu = descu + descu2
    TxtEntra(10).Text = Aplicacion.Formatear_Valor(bruto)
    TxtEntra(11).Text = Aplicacion.Formatear_Valor(descu)
    TxtEntra(12).Text = Aplicacion.Formatear_Valor(impuesto)
    TxtEntra(13).Text = Aplicacion.Formatear_Valor(retef)
    TxtEntra(14).Text = Aplicacion.Formatear_Valor(fletes)
    TxtEntra(15).Text = Aplicacion.Formatear_Valor(neto)
    TxtEntra(18).Text = Aplicacion.Formatear_Valor(ReteIVA)
    TxtEntra(19).Text = Aplicacion.Formatear_Valor(ReteICA)
    TxtEntra(20).Text = Aplicacion.Formatear_Valor(TxtEntra(20))
 
    ''PASO A CONTA
    valo_cont(1) = bruto
    valo_cont(2) = descu
    valo_cont(3) = impuesto
    valo_cont(4) = retef
    valo_cont(5) = fletes
    valo_cont(6) = neto
    If Me.TieneContabilidad Then Interface_ContableEntrada
    '''''''''''''''''
    ValReg = bruto - descu + impuesto + fletes
    On Error GoTo TRMNR
    
    If Ppto And GrdArti.TextMatrix(1, 0) <> NUL$ Then
        ArtiPpto = Articulos_Presupuesto(GrdArti, "NU_COMP_CUEN,NU_CDPE_CUEN", 8, 0)
        If ArtiFlete <> NUL$ And CDbl(TxtEntra(14)) > 0 Then
            ReDim Preserve ArtiPpto(3, UBound(ArtiPpto(), 2) + 1)
            ArtiPpto(0, UBound(ArtiPpto(), 2)) = ArtiFlete
            ArtiPpto(3, UBound(ArtiPpto(), 2)) = CDbl(TxtEntra(14))
        End If
        'Txtpres_LostFocus Verificar que hac�a aqu�, freddy
    End If
TRMNR:
    On Error GoTo 0
    GrdArti.Col = columna  ''Recupera la posici�n
    GrdArti.Row = fila  ''Recupera la posici�n
End Sub

Private Sub calcular_salidaA()
    Dim columna As Long 'Para que conserve la posici�n
    Dim fila As Long 'Para que conserve la posici�n
    Dim valor As Double
    '''''''''''
    'PASO A CONTA
    Dim valo_cont(1 To 2) As Variant
    ''''''''''
    columna = GrdArti.Col
    fila = GrdArti.Row
    '''
    GrdArti.Row = 0
'/////////////
    CargaGrillaInterface Me
'/////////////
    On Error GoTo salto
    While GrdArti.Row <= GrdArti.Rows - 1
        GrdArti.Row = GrdArti.Row + 1
        If GrdArti.TextMatrix(GrdArti.Row, 1) <> NUL$ Then
            If GrdArti.TextMatrix(GrdArti.Row, 2) = "" Or IsNumeric(GrdArti.TextMatrix(GrdArti.Row, 2)) = False Then
                GrdArti.TextMatrix(GrdArti.Row, 2) = "0"
            End If
            If GrdArti.TextMatrix(GrdArti.Row, 3) = "" Or IsNumeric(GrdArti.TextMatrix(GrdArti.Row, 3)) = False Then
                GrdArti.TextMatrix(GrdArti.Row, 3) = "0"
            End If
            'Precio total
            valor = CDbl(GrdArti.TextMatrix(GrdArti.Row, 2)) * CDbl(GrdArti.TextMatrix(GrdArti.Row, 3))
            GrdArti.TextMatrix(GrdArti.Row, 4) = Aplicacion.Formatear_Valor(valor)
        End If
    Wend
salto:
    On Error GoTo 0
    TxtSaliA(7).Text = Aplicacion.Formatear_Valor(sumar_items(GrdArti, 4))
    '''
    ''PASO A CONTA
    valo_cont(1) = TxtSaliA(7).Text
    valo_cont(2) = TxtSaliA(7).Text
    If Me.TieneContabilidad Then Call Interface_ContableSalida
    '''''''''''''''''
    GrdArti.Col = columna  ''Recupera la posici�n
    GrdArti.Row = fila  ''Recupera la posici�n
End Sub

Private Sub calcular_venta()
    Dim columna As Long 'Para que conserve la posici�n
    Dim fila As Long 'Para que conserve la posici�n
    '''''''''''
    Dim bruto As Double
    Dim descu As Double
    Dim descu2 As Double
    Dim desitem As Double
    Dim desitemv As Double
    Dim impuesto As Double
    Dim retef As Double
    Dim fletes As Double
    Dim neto As Double
    Dim valor As Double
    Dim items As Integer
    Dim aux As Integer
    
    ''''''''''
    'PASO A CONTA
    Dim valo_cont(1 To 8) As Variant
    ''''''''''
    columna = GrdArti.Col
    fila = GrdArti.Row
'/////////////
    CargaGrillaInterface Me
'/////////////
    VBase = 0
    '''
    '''Suma todos los descuentos
    descu2 = 0
    aux = 1
    Do While aux <= 20
     If Mdesc(aux, 2) = "" Then
       aux = 21
     Else
       descu2 = descu2 + CDbl(Mdesc(aux, 2))
     End If
     aux = aux + 1
    Loop
    ''''''''
    If GrdArti.TextMatrix(GrdArti.Rows - 1, 1) <> "" Then
      items = GrdArti.Rows - 1
    Else
      items = GrdArti.Rows - 2
    End If
    '''Divide el valor de los descuentos en todos los art�culos
    If items <> 0 Then
      desitemv = descu2 / items
    End If
    ''''''''''''
    If TxtVenta(19).Text = "" Then
      TxtVenta(19).Text = "0"
    End If
    fletes = CDbl(TxtVenta(19).Text)
    
    GrdArti.Row = 0
    If Not IsNumeric(TxtVenta(12).Text) Then TxtVenta(12).Text = "0"
    On Error GoTo salto
    While GrdArti.Row <= GrdArti.Rows - 1
        GrdArti.Row = GrdArti.Row + 1
        If GrdArti.TextMatrix(GrdArti.Row, 1) <> NUL$ Then
            If GrdArti.TextMatrix(GrdArti.Row, 2) = "" Or IsNumeric(GrdArti.TextMatrix(GrdArti.Row, 2)) = False Then
                GrdArti.TextMatrix(GrdArti.Row, 2) = "0"
            End If
            If GrdArti.TextMatrix(GrdArti.Row, 3) = "" Or IsNumeric(GrdArti.TextMatrix(GrdArti.Row, 3)) = False Then
                GrdArti.TextMatrix(GrdArti.Row, 3) = "0"
            End If
            If GrdArti.TextMatrix(GrdArti.Row, 5) = "" Or IsNumeric(GrdArti.TextMatrix(GrdArti.Row, 5)) = False Then
                GrdArti.TextMatrix(GrdArti.Row, 5) = "0"
            End If
            valor = CDbl(GrdArti.TextMatrix(GrdArti.Row, 2)) * CDbl(GrdArti.TextMatrix(GrdArti.Row, 3))
            GrdArti.TextMatrix(GrdArti.Row, 4) = Format(valor, "############0.#0")
            desitem = (CSng(TxtVenta(12).Text) * valor) / 100
            valor = (((valor - desitem - desitemv) * CDbl(GrdArti.TextMatrix(GrdArti.Row, 5)) / 100))
            GrdArti.TextMatrix(GrdArti.Row, 6) = valor
            valor = CDbl(GrdArti.TextMatrix(GrdArti.Row, 2)) * CDbl(GrdArti.TextMatrix(GrdArti.Row, 3))
            valor = valor - desitem - desitemv + CDbl(GrdArti.TextMatrix(GrdArti.Row, 6))
            GrdArti.TextMatrix(GrdArti.Row, 9) = Format(valor, "############0.##00")
            
            ' SILVIA : SE LE AGREGO LA VARIABLE QUE LE RESTA EL DESCUENTO
            VBase = VBase + CDbl(IIf(GrdArti.TextMatrix(GrdArti.Row, 5) <> 0, val(GrdArti.TextMatrix(GrdArti.Row, 4)) - val(desitem), 0))
        End If
    Wend
salto:
    On Error GoTo 0
    bruto = sumar_items(GrdArti, 4)
    If Not IsNumeric(TxtVenta(12).Text) Then TxtVenta(12).Text = "0"
    If Not IsNumeric(TxtVenta(13).Text) Then TxtVenta(13).Text = "0"
    descu = Round((CSng(TxtVenta(12).Text) * bruto) / 100)
    descu2 = Round(descu2)
    descu = Round(descu + descu2)
    impuesto = Round(sumar_items(GrdArti, 6))
    retef = Round((CSng(TxtVenta(13).Text) * bruto) / 100)
    neto = bruto - descu + impuesto - retef + fletes
    TxtVenta(15).Text = Format(bruto, "####,###,###,##0.#0")
    TxtVenta(16).Text = Format(descu, "####,###,###,##0.#0")
    TxtVenta(17).Text = Format(impuesto, "####,###,###,##0.#0")
    TxtVenta(18).Text = Format(retef, "####,###,###,##0.#0")
    TxtVenta(19).Text = Format(fletes, "####,###,###,##0.#0")
    TxtVenta(20).Text = Format(neto, "####,###,###,##0.#0")
    '''
    ''PASO A CONTA
    valo_cont(1) = bruto
    valo_cont(2) = descu
    valo_cont(3) = impuesto
    valo_cont(4) = retef
    valo_cont(5) = fletes
    valo_cont(6) = neto
    valo_cont(7) = calcular_costo(GrdArti, 2, 7)
    valo_cont(8) = valo_cont(7)

     If Me.TieneContabilidad Then Call Interface_ContableVenta
    '''''''''''''''''
    If Ppto And GrdArti.TextMatrix(1, 0) <> NUL$ Then
       MatPpto = Articulos_Presupuesto(GrdArti, "NU_COMP_CUEN,NU_CDPE_CUEN", 9, 1)
       If ArtiFlete <> NUL$ And CDbl(TxtVenta(19)) > 0 Then
          ReDim Preserve MatPpto(3, UBound(MatPpto(), 2) + 1)
          MatPpto(0, UBound(MatPpto(), 2)) = ArtiFlete
          MatPpto(1, UBound(MatPpto(), 2)) = DescArtiF
          MatPpto(2, UBound(MatPpto(), 2)) = CDbl(TxtVenta(19))
          MatPpto(3, UBound(MatPpto(), 2)) = CDbl(TxtVenta(19))
       End If
    End If
    GrdArti.Col = columna  ''Recupera la posici�n
    GrdArti.Row = fila  ''Recupera la posici�n
End Sub

Private Sub calcular_salidaD()
    Dim columna As Long 'Para que conserve la posici�n
    Dim fila As Long 'Para que conserve la posici�n
    Dim valor As Double
    '''''''''''
    'PASO A CONTA
    Dim valo_cont(1 To 2) As Variant
    Dim valo_cont2(1 To 4) As Variant
    ''''''''''
    columna = GrdArti.Col
    fila = GrdArti.Row
    '''
'/////////////
    CargaGrillaInterface Me
'/////////////
    GrdArti.Row = 0
    On Error GoTo salto
    While GrdArti.Row <= GrdArti.Rows - 1
        GrdArti.Row = GrdArti.Row + 1
        If GrdArti.TextMatrix(GrdArti.Row, 1) <> NUL$ Then
            If GrdArti.TextMatrix(GrdArti.Row, 2) = "" Or IsNumeric(GrdArti.TextMatrix(GrdArti.Row, 2)) = False Then
                GrdArti.TextMatrix(GrdArti.Row, 2) = "0"
            End If
            If GrdArti.TextMatrix(GrdArti.Row, 3) = "" Or IsNumeric(GrdArti.TextMatrix(GrdArti.Row, 3)) = False Then
                GrdArti.TextMatrix(GrdArti.Row, 3) = "0"
            End If
            'Precio total
            valor = CDbl(GrdArti.TextMatrix(GrdArti.Row, 2)) * CDbl(GrdArti.TextMatrix(GrdArti.Row, 3))
            GrdArti.TextMatrix(GrdArti.Row, 4) = Aplicacion.Formatear_Valor(valor)
            'Costo unitario
            GrdArti.TextMatrix(GrdArti.Row, 5) = Aplicacion.Formatear_Valor(Buscar_Valor_Arti(GrdArti.TextMatrix(GrdArti.Row, 0), "VL_COPR_ARTI"))
            'Costo Total
            valor = CDbl(GrdArti.TextMatrix(GrdArti.Row, 2)) * CDbl(GrdArti.TextMatrix(GrdArti.Row, 5))
            GrdArti.TextMatrix(GrdArti.Row, 6) = Aplicacion.Formatear_Valor(valor)
        End If
    Wend
salto:
    On Error GoTo 0
    TxtBajaD(9).Text = Aplicacion.Formatear_Valor(sumar_items(GrdArti, 4))
    '''
    ''PASO A CONTA
    If Not IsNumeric(TxtBajaD(9).Text) Then TxtBajaD(9).Text = "0"
    If OptSalida(1).Value = True Then
        valo_cont2(1) = TxtBajaD(9).Text
        valo_cont2(2) = TxtBajaD(9).Text
        valo_cont2(3) = sumar_items(GrdArti, 6)
        valo_cont2(4) = valo_cont2(3)
    Else
        valo_cont(1) = TxtBajaD(9).Text
        valo_cont(2) = TxtBajaD(9).Text
    End If
    If Me.TieneContabilidad Then Call Interface_ContableBajaConsumo
    '''''''''''''''''
    GrdArti.Col = columna  ''Recupera la posici�n
    GrdArti.Row = fila  ''Recupera la posici�n
End Sub

Private Sub calcular_devo()
    Dim columna As Long 'Para que conserve la posici�n
    Dim fila As Long 'Para que conserve la posici�n
    '''''''''''
    Dim bruto As Double
    Dim descu As Double
    Dim descu2 As Double
    Dim desitem As Double
    Dim desitemv As Double
    Dim impuesto As Double
    Dim retef As Double
    Dim neto As Double
    Dim valor As Double
    Dim items As Integer
    Dim aux As Integer
    ''''''''''
    'PASO A CONTA
    Dim valo_cont(1 To 7) As Variant
    ''''''''''
    columna = GrdArti.Col
    fila = GrdArti.Row
'/////////////
    CargaGrillaInterface Me
'/////////////
    '''
    '''Suma todos los descuentos
    descu2 = 0
    aux = 1
    Do While aux <= 20
     If Mdesc(aux, 2) = "" Then
       aux = 21
     Else
       descu2 = descu2 + CDbl(Mdesc(aux, 2))
     End If
     aux = aux + 1
    Loop
    ''''''''
    If GrdArti.TextMatrix(GrdArti.Rows - 1, 1) <> "" Then
      items = GrdArti.Rows - 1
    Else
      items = GrdArti.Rows - 2
    End If
    '''Divide el valor de los descuentos en todos los art�culos
    If items <> 0 Then
      desitemv = descu2 / items
    End If
    ''''''''''''
    GrdArti.Row = 0
    If Not IsNumeric(TxtDevo(9).Text) Then TxtDevo(9).Text = "0"
    On Error GoTo salto
    While GrdArti.Row <= GrdArti.Rows - 1
        GrdArti.Row = GrdArti.Row + 1
        If GrdArti.TextMatrix(GrdArti.Row, 1) <> NUL$ Then
            If GrdArti.TextMatrix(GrdArti.Row, 2) = "" Or IsNumeric(GrdArti.TextMatrix(GrdArti.Row, 2)) = False Then
                GrdArti.TextMatrix(GrdArti.Row, 2) = "0"
            End If
            If GrdArti.TextMatrix(GrdArti.Row, 3) = "" Or IsNumeric(GrdArti.TextMatrix(GrdArti.Row, 3)) = False Then
                GrdArti.TextMatrix(GrdArti.Row, 3) = "0"
            End If
            If GrdArti.TextMatrix(GrdArti.Row, 5) = "" Or IsNumeric(GrdArti.TextMatrix(GrdArti.Row, 5)) = False Then
                GrdArti.TextMatrix(GrdArti.Row, 5) = "0"
            End If
            valor = CDbl(GrdArti.TextMatrix(GrdArti.Row, 2)) * CDbl(GrdArti.TextMatrix(GrdArti.Row, 3))
            GrdArti.TextMatrix(GrdArti.Row, 4) = Aplicacion.Formatear_Valor(valor)
            desitem = (CSng(TxtDevo(9).Text) * valor) / 100
            valor = Round((((valor - desitem - desitemv) * CDbl(GrdArti.TextMatrix(GrdArti.Row, 5))) / 100))
            GrdArti.TextMatrix(GrdArti.Row, 6) = valor
            valor = CDbl(GrdArti.TextMatrix(GrdArti.Row, 2)) * CDbl(GrdArti.TextMatrix(GrdArti.Row, 3))
            valor = Round((((valor - desitem - desitemv) * CDbl(GrdArti.TextMatrix(GrdArti.Row, 5))) / 100)) + (valor - desitem - desitemv)
            GrdArti.TextMatrix(GrdArti.Row, 9) = Aplicacion.Formatear_Valor(valor)
        End If
    Wend
salto:
    On Error GoTo 0
    bruto = sumar_items(GrdArti, 4)
    If Not IsNumeric(TxtDevo(9).Text) Then TxtDevo(9).Text = "0"
    If Not IsNumeric(TxtDevo(10).Text) Then TxtDevo(10).Text = "0"
    descu = Round((CSng(TxtDevo(9).Text) * bruto) / 100)
    descu = Round(descu + descu2)
    impuesto = Round(sumar_items(GrdArti, 6))
    retef = Round((CSng(TxtDevo(10).Text) * bruto) / 100)
    neto = bruto - descu + impuesto - retef
    TxtDevo(12).Text = Aplicacion.Formatear_Valor(bruto)
    TxtDevo(13).Text = Aplicacion.Formatear_Valor(descu)
    TxtDevo(14).Text = Aplicacion.Formatear_Valor(impuesto)
    TxtDevo(15).Text = Aplicacion.Formatear_Valor(retef)
    TxtDevo(16).Text = Aplicacion.Formatear_Valor(neto)
    '''
    ''PASO A CONTA
    valo_cont(1) = bruto
    valo_cont(2) = descu
    valo_cont(3) = impuesto
    valo_cont(4) = retef
    valo_cont(5) = neto
    valo_cont(6) = calcular_costo(GrdArti, 2, 7)
    valo_cont(7) = valo_cont(6)
    If Me.TieneContabilidad Then Call Interface_ContableDevolucion
     
    '''''''''''''''''
    If Ppto And GrdArti.TextMatrix(1, 0) <> NUL$ Then
       MatPpto = Articulos_Presupuesto(GrdArti, "NU_COMP_CUEN,NU_CDPE_CUEN", 9, 1)
    End If
    GrdArti.Col = columna  ''Recupera la posici�n
    GrdArti.Row = fila  ''Recupera la posici�n
End Sub

'Private Sub Agregar_Arti_Entra()
'ReDim Arr(9) 'Para cargar valores de las entradas
''Para recorrer art�culos de las entradas
'ReDim MArr(3, 0)
'Dim c As Integer
''''''
'ReDim Arr2(0) 'Para cargar la descripci�n del art�culo
'Dim aux As Integer 'Para recorrer lista
''Para acumular valores de las entradas
'Dim bruto As Double
'Dim descuentos As Double
'Dim iva, RIVA, RICA As Double
'Dim retencion As Double
'Dim fletes As Double
'Dim neto As Double
'Dim DescAdic As Double
'''''''''''''''''''''
''PASO A CONTA
'Dim valo_cont(1 To 6) As Variant
'''''''''''
'bruto = 0
'descuentos = 0
'iva = 0: RIVA = 0: RICA = 0
'retencion = 0
'fletes = 0
'neto = 0: DescAdic = 0
''Limpia el grid de art�culos
'GrdArti.Clear
'GrdArti.Rows = 2
'Titulos_Grid
'''''
'If LstEntra.ListCount > 0 Then
'    Call Ordena_Lista_Num(LstEntra) 'Ordena la lista
'    aux = 0
'    While aux < LstEntra.ListCount
'        Condicion = "CD_CODI_ENTR =" & LstEntra.List(aux)
'        Result = LoadData("ENTRADA_ALMACEN", "VL_BRUT_ENTR, VL_DESC_ENTR, VL_IMPU_ENTR, VL_RETE_ENTR, VL_FLET_ENTR, VL_NETO_ENTR, ID_FLET_ENTR, VL_RIVA_ENTR, VL_RICA_ENTR, VL_DEAD_ENTR", Condicion, Arr())
'        If (Result <> False) Then
'            If Encontro Then
'                If Arr(0) = NUL$ Then Arr(0) = "0"
'                If Arr(1) = NUL$ Then Arr(1) = "0"
'                If Arr(2) = NUL$ Then Arr(2) = "0"
'                If Arr(3) = NUL$ Then Arr(3) = "0"
'                If Arr(7) = NUL$ Then Arr(7) = "0"
'                If Arr(8) = NUL$ Then Arr(8) = "0"
'                If Arr(4) = NUL$ Then Arr(4) = "0"
'                bruto = bruto + CDbl(Arr(0))
'                descuentos = descuentos + CDbl(Arr(1))
'                iva = iva + CDbl(Arr(2))
'                retencion = retencion + CDbl(Arr(3))
'                If Arr(6) = "S" Then fletes = fletes + CDbl(Arr(4))
'                RIVA = CDbl(Arr(7)) + RIVA
'                RICA = CDbl(Arr(8)) + RICA
'                DescAdic = DescAdic + CDbl(Arr(9))
'                neto = neto + CDbl(Arr(5))
'                'Busca los art�culos de las entradas
'                Condicion = "CD_ENTR_ENAR=" & LstEntra.List(aux)
'                Result = LoadMulData("R_ENTRA_ARTI", "CD_ARTI_ENAR, CT_CANT_ENAR, VL_COUN_ENAR, VL_COTO_ENAR", Condicion, MArr())
'                If (Result <> False) Then
'                    If Encontro Then
'                        c = 0
'                        Do While c <= UBound(MArr, 2)
'                            If Revisa_FGrd3(GrdArti, CStr(MArr(0, c)), 0) = False Then
'                                'Trae la descripci�n del art�culo
'                                Condicion = "CD_CODI_ARTI=" & Comi & MArr(0, c) & Comi
'                                Result = LoadData("ARTICULO", "NO_NOMB_ARTI", Condicion, Arr2())
'                                ''''
'                                If GrdArti.TextMatrix(GrdArti.Rows - 1, 0) = NUL$ Then
'                                    GrdArti.TextMatrix(GrdArti.Rows - 1, 0) = MArr(0, c)
'                                    GrdArti.TextMatrix(GrdArti.Rows - 1, 1) = Arr2(0)
'                                    GrdArti.TextMatrix(GrdArti.Rows - 1, 2) = Aplicacion.Formatear_Cantidad(MArr(1, c)) 'cantidad
'                                    GrdArti.TextMatrix(GrdArti.Rows - 1, 3) = Aplicacion.Formatear_Valor(MArr(2, c)) 'valor unidad
'                                    GrdArti.TextMatrix(GrdArti.Rows - 1, 4) = Aplicacion.Formatear_Valor(MArr(3, c)) 'valor total
'                                Else
'                                    GrdArti.AddItem MArr(0, c) & vbTab & Arr2(0) & vbTab & Aplicacion.Formatear_Cantidad(MArr(1, c)) & vbTab & Aplicacion.Formatear_Valor(MArr(2, c)) & vbTab & Aplicacion.Formatear_Valor(MArr(3, c))
'                                End If
'                            Else
'                                'Si el art�culo ya est� en la lista
'                                'entonces incrementa sus valores
'                                'Cantidad
'                                GrdArti.TextMatrix(GrdArti.Row, 2) = Aplicacion.Formatear_Cantidad(CDbl(GrdArti.TextMatrix(GrdArti.Row, 2)) + CDbl(MArr(1, c)))
'                                'Costo Total
'                                GrdArti.TextMatrix(GrdArti.Row, 4) = Aplicacion.Formatear_Valor(CDbl(GrdArti.TextMatrix(GrdArti.Row, 4)) + CDbl(MArr(3, c)))
'                                'Costo Unitario
'                                GrdArti.TextMatrix(GrdArti.Row, 3) = Aplicacion.Formatear_Valor(CDbl(GrdArti.TextMatrix(GrdArti.Row, 4) / CDbl(GrdArti.TextMatrix(GrdArti.Row, 2))))
'                            End If
'                            c = c + 1
'                        Loop
'                    End If
'                End If
'            End If
'        End If
'        aux = aux + 1
'    Wend
'    TxtCompra(12) = Aplicacion.Formatear_Valor(bruto)
'    TxtCompra(13) = Aplicacion.Formatear_Valor(descuentos)
'    TxtCompra(14) = Aplicacion.Formatear_Valor(iva)
'    TxtCompra(15) = Aplicacion.Formatear_Valor(retencion)
'    TxtCompra(16) = Aplicacion.Formatear_Valor(fletes)
'    TxtCompra(17) = Aplicacion.Formatear_Valor(neto)
'    TxtCompra(18) = Aplicacion.Formatear_Valor(RIVA)
'    TxtCompra(19) = Aplicacion.Formatear_Valor(RICA)
'    TxtCompra(20) = Aplicacion.Formatear_Valor(DescAdic)
'End If
'''PASO A CONTA
'valo_cont(1) = bruto
'valo_cont(2) = descuentos
'valo_cont(3) = iva
'valo_cont(4) = retencion
'valo_cont(5) = fletes
'valo_cont(6) = neto
'If CmdCuentas.Enabled = True Then
'Cuentas_Contables_Movi 1, valo_cont, TxtCompra(8), NUL$
'End If
''''''''''''''''''
'End Sub

Public Sub Interface_ContableEntrada()
Dim Arr(3)
Dim CtasG(3)
Dim i As Integer
ReDim CtasConta(3, 0)
    CtasConta = Cuentas_Articulos(GrdArti, "CD_ENTR_GRUP", "D")
    Arr(0) = TxtEntra(12): Arr(1) = TxtEntra(18)
    Arr(2) = TxtEntra(19): Arr(3) = TxtEntra(13)
    
    Result = LoadData("PARAMETROS_INVE", "CD_DECO_APLI,CD_FLET_APLI,CD_DEAD_APLI,CD_CXP_APLI", NUL$, CtasG())
    If Not IsNumeric(TxtEntra(11)) Then TxtEntra(11) = 0
    If CDbl(TxtEntra(11)) > 0 Then
        i = UBound(CtasConta, 2) + 1
        ReDim Preserve CtasConta(3, i)
        CtasConta(0, i) = CtasG(0)
        CtasConta(1, i) = TxtEntra(11)
        CtasConta(2, i) = 0
        CtasConta(3, i) = "C"
    End If
    CtasConta = Cuentas_Movimiento(0, Arr(), CDbl(TxtEntra(10)) - TxtEntra(11))
    If Not IsNumeric(TxtEntra(14)) Then TxtEntra(14) = 0
    If CDbl(TxtEntra(14)) > 0 Then
        i = UBound(CtasConta, 2) + 1
        ReDim Preserve CtasConta(3, i)
        CtasConta(0, i) = CtasG(1)
        CtasConta(1, i) = TxtEntra(14)
        CtasConta(2, i) = 0
        CtasConta(3, i) = "D"
    End If
    If CDbl(TxtEntra(20)) > 0 Then
        i = UBound(CtasConta, 2) + 1
        ReDim Preserve CtasConta(3, i)
        CtasConta(0, i) = CtasG(2)
        CtasConta(1, i) = TxtEntra(20)
        CtasConta(2, i) = 0
        CtasConta(3, i) = "C"
    End If
    If CDbl(TxtEntra(15)) > 0 Then
        i = UBound(CtasConta, 2) + 1
        ReDim Preserve CtasConta(3, i)
        CtasConta(0, i) = CtasG(3)
        CtasConta(1, i) = TxtEntra(15)
        CtasConta(2, i) = 0
        CtasConta(3, i) = "C"
    End If
'    Call Cuentas_Contables_Movi(0, CtasConta, txtDocum(5), NUL$)
    Call Cuentas_Contables_Movi(0, CtasConta, Documento.Encabezado.Tercero.Nit, NUL$)
    SiMuestra = False
End Sub

Private Sub Interface_ContableSalida()
Dim Arr(3)
Dim CtasG(2)
Dim i As Integer
ReDim CtasConta(3, 0)
    CtasConta = Cuentas_Articulos_Costo(GrdArti, "CD_SALI_GRUP", "CD_GAST_GRUP", "C")
'    Call Cuentas_Contables_Movi(7, CtasConta, TxtSaliA(4), NUL$)
    Call Cuentas_Contables_Movi(7, CtasConta, Documento.Encabezado.Tercero.Nit, NUL$)
End Sub

Private Sub Interface_ContableVenta()
Dim Arr(3)
Dim CtasG(2)
Dim i As Integer

ReDim CtasConta(3, 0)
    CtasConta = Cuentas_Articulos(GrdArti, "CD_INGR_GRUP", "C")
    Result = LoadData("PARAMETROS_INVE", "CD_DEVE_APLI,CD_FLET_APLI,CD_CXC_APLI", NUL$, CtasG())
    If Not IsNumeric(TxtVenta(16)) Then TxtVenta(16) = 0
    If CDbl(TxtVenta(16)) > 0 Then
        i = UBound(CtasConta(), 2) + 1
        ReDim Preserve CtasConta(3, i)
        CtasConta(0, i) = CtasG(0) 'SE CAMBIO ESTE SUBINDICE QUE ERA UNO PARA QUE LA CUENTA SE ENVIE A LA CUENTA CORRECTA.
        CtasConta(1, i) = TxtVenta(16)
        CtasConta(2, i) = 0
        CtasConta(3, i) = "D"
    End If
    Arr(0) = TxtVenta(17): Arr(1) = 0
    Arr(2) = 0: Arr(3) = TxtVenta(18)
    CtasConta = Cuentas_Factura(2, Arr(), CDbl(TxtVenta(15)))
    If CDbl(TxtVenta(19)) > 0 Then
        i = UBound(CtasConta(), 2) + 1
        ReDim Preserve CtasConta(3, i)
        CtasConta(0, i) = CtasG(1)
        CtasConta(1, i) = TxtVenta(19)
        CtasConta(2, i) = 0
        CtasConta(3, i) = "C"
    End If
    If CDbl(TxtVenta(20)) > 0 Then
        i = UBound(CtasConta(), 2) + 1
        ReDim Preserve CtasConta(3, i)
        CtasConta(0, i) = CtasG(2)
        CtasConta(1, i) = TxtVenta(20)
        CtasConta(2, i) = 0
        CtasConta(3, i) = "D"
    End If
    CtasConta = Cuentas_Articulos_Costo(GrdArti, "CD_SALI_GRUP", "CD_COST_GRUP", "C") 'For I = 0 To UBound(CtasConta(), 2)
'    Call Cuentas_Contables_Movi(2, CtasConta, TxtVenta(8), NUL$)
    Call Cuentas_Contables_Movi(2, CtasConta, Documento.Encabezado.Tercero.Nit, NUL$)
    SiMuestra = False
End Sub

Private Sub Interface_ContableDevolucion()
Dim Arr(3)
Dim CtasG(1)
Dim i As Integer
ReDim CtasConta(3, 0)
    CtasConta = Cuentas_Articulos(GrdArti, "CD_INGR_GRUP", "D")
    Result = LoadData("PARAMETROS_INVE", "CD_DEVE_APLI,CD_CXC_APLI", NUL$, CtasG())
    If Not IsNumeric(TxtDevo(13)) Then TxtDevo(13) = 0
    If CDbl(TxtDevo(13)) > 0 Then 'descuentos
        i = UBound(CtasConta(), 2) + 1
        ReDim Preserve CtasConta(3, i)
        CtasConta(0, i) = CtasG(0)
        CtasConta(1, i) = TxtDevo(13)
        CtasConta(2, i) = 0
        CtasConta(3, i) = "C"
    End If
    Arr(0) = TxtDevo(14) 'iva
    Arr(1) = 0
    Arr(2) = 0
    Arr(3) = TxtDevo(15) 'retefuente
    CtasConta = Cuentas_Movimiento(3, Arr(), CDbl(TxtDevo(12)))
    If CDbl(TxtDevo(16)) > 0 Then
        i = UBound(CtasConta(), 2) + 1
        ReDim Preserve CtasConta(3, i)
        CtasConta(0, i) = CtasG(1)
        CtasConta(1, i) = TxtDevo(16)
        CtasConta(2, i) = 0
        CtasConta(3, i) = "C"
    End If
    CtasConta = Cuentas_Articulos_Costo(GrdArti, "CD_SALI_GRUP", "CD_COST_GRUP", "D") 'For I = 0 To UBound(CtasConta(), 2)
'    Call Cuentas_Contables_Movi(3, CtasConta, TxtDevo(5), NUL$)
    Call Cuentas_Contables_Movi(3, CtasConta, Documento.Encabezado.Tercero.Nit, NUL$)
'    SiMuestra = False
End Sub

Private Sub Interface_ContableBajaConsumo()
Dim Arr(3)
Dim CtasG(2)
Dim i As Integer
ReDim CtasConta(3, 0)
    CtasConta = Cuentas_Articulos_Costo(GrdArti, "CD_SALI_GRUP", "CD_COST_GRUP", "C")
'    Call Cuentas_Contables_Movi(5, CtasConta, TxtBajaD(6), TxtBajaD(4))
    Call Cuentas_Contables_Movi(5, CtasConta, Documento.Encabezado.Tercero.Nit, TxtBajaD(4))
End Sub

Private Sub Graba_cxc(ByVal fechag As Variant, ByVal fechav As Variant)
    Dim NumCxC As Long
      
      'Valores = "CD_CONCE_CXC= 'VINV'" & Coma
    Valores = "CD_CONCE_CXC=" & Comi & "VINV" & Comi & Coma
    Valores = Valores & "CD_NUME_CXC=" & Conse_CXC & Coma
    Valores = Valores & "FE_FECH_CXC=" & Comi & fechag & Comi & Coma
    Valores = Valores & "FE_VENC_CXC=" & Comi & fechav & Comi & Coma
    Valores = Valores & "CD_TERC_CXC=" & Comi & Cambiar_Comas_Comillas(Documento.Encabezado.Tercero.Nit) & Comi & Coma
'    Valores = Valores & "NU_FACL_CXC=" & CDbl(TxtVenta(0)) & Coma
    Valores = Valores & "NU_FACL_CXC=" & CDbl(Conse_CXC) & Coma
    Valores = Valores & "VL_BRUT_CXC=" & CDbl(TxtVenta(15)) & Coma
    Valores = Valores & "VL_BRUE_CXC=0" & Coma
    Valores = Valores & "VL_NETO_CXC=" & CDbl(TxtVenta(20)) & Coma
    Valores = Valores & "VL_CANC_CXC=0" & Coma
    Valores = Valores & "VL_NCRE_CXC=0" & Coma
    Valores = Valores & "VL_NDEB_CXC=0" & Coma
    Valores = Valores & "NU_RECA_CXC=0" & Coma
    Valores = Valores & "DE_OBSE_CXC=" & Comi & Cambiar_Comas_Comillas(Documento.Encabezado.Tercero.Nit) & Comi & Coma
    ''Fletes y retencion
    Valores = Valores & "VL_FLET_CXC=" & CDbl(TxtVenta(19)) & Coma
    Valores = Valores & "VL_RETE_CXC=" & CDbl(TxtVenta(18)) & Coma
    ''Descuentos e Impuestos
    Valores = Valores & "VL_DESC_CXC=" & CDbl(TxtVenta(16)) & Coma
    Valores = Valores & "VL_IMPU_CXC=" & CDbl(TxtVenta(17)) & Coma
    ''Monto Escrito
    Valores = Valores & "DE_MONT_CXC=" & Comi & Monto_Escrito(CDbl(TxtVenta(20))) & Comi & Coma
    ''Estado
    Valores = Valores & "ID_ESTA_CXC=1" & Coma
    Valores = Valores & "ID_PPTO_CXC=0"
    Result = DoInsertSQL("C_X_C", Valores)
    ''Actualiza el consecutivo del concepto
    Result = DoUpdate("CONCEPTO", "NU_CONS_CONC=" & Conse_CXC, "ID_TIPO_CONC=0 And NU_CONS_CONC <" & Conse_CXC)
    If Result <> FAIL Then Result = DoUpdate("PARAMETROS_CXC", "NU_CXCO_APLI=" & Conse_CXC, NUL$)
End Sub

'Private Sub Graba_cxp(ByVal fechag As Variant, ByVal fechav As Variant)
'    Valores = "CD_CONCE_CXP=" & Comi & "CINV" & Comi & Coma
'    Valores = Valores & "CD_NUME_CXP=" & Conse_CXP & Coma
'    Valores = Valores & "FE_FECH_CXP=" & Comi & fechag & Comi & Coma
'    Valores = Valores & "FE_VENC_CXP=" & Comi & fechav & Comi & Coma
'    Valores = Valores & "CD_TERC_CXP=" & Comi & Cambiar_Comas_Comillas(Documento.Encabezado.Tercero.Nit) & Comi & Coma
'    If Not IsNumeric(TxtCompra(10).Text) Then TxtCompra(10).Text = "0"
'    Valores = Valores & "NU_FAPR_CXP=" & CDbl(TxtCompra(10)) & Coma
'    Valores = Valores & "VL_BRUT_CXP=" & CDbl(TxtEntra(10)) & Coma
'    Valores = Valores & "VL_BRUE_CXP=0" & Coma
'    Valores = Valores & "VL_NETO_CXP=0" & CDbl(TxtEntra(15)) & Coma
'    Valores = Valores & "VL_CANC_CXP=0" & Coma
'    Valores = Valores & "VL_NCRE_CXP=0" & Coma
'    Valores = Valores & "VL_NDEB_CXP=0" & Coma
'    Valores = Valores & "NU_EGRE_CXP=0" & Coma
'    Valores = Valores & "DE_OBSE_CXP=" & Comi & Cambiar_Comas_Comillas(Documento.Encabezado.Observaciones) & Comi & Coma
'    ''Fletes y retencion
'    Valores = Valores & "VL_FLET_CXP=" & CDbl(TxtEntra(14)) & Coma
'    Valores = Valores & "VL_RETE_CXP=" & CDbl(TxtEntra(13)) & Coma
'    ''Descuentos e Impuestos
'    Valores = Valores & "VL_DESC_CXP=" & CDbl(TxtEntra(11)) & Coma
'    Valores = Valores & "VL_IMPU_CXP=" & CDbl(TxtEntra(12)) & Coma
'    ''Monto Escrito
'    Valores = Valores & "DE_MONT_CXP=" & Comi & Monto_Escrito(CDbl(TxtEntra(15))) & Comi & Coma
'    ''Estado
'    Valores = Valores & "ID_ESTA_CXP=1" & Coma
'    Valores = Valores & "ID_PPTO_CXP=0" ' Realizado para que funcione la interface con cxpagar giu
'    Debug.Print Valores
'    Result = DoInsertSQL("C_X_P", Valores)
'    ''Actualiza el consecutivo del concepto
'    If Result <> FAIL Then Result = DoUpdate("CONCEPTO", "NU_CONS_CONC=" & Conse_CXP, "ID_TIPO_CONC=7 And NU_CONS_CONC <" & Conse_CXP)
'    If Result <> FAIL Then Result = DoUpdate("PARAMETROS_CXP", "NU_CXPA_APLI=" & Conse_CXP, NUL$)
'End Sub

'Private Sub Graba_cxp(ByVal fechag As Variant, ByVal fechav As Variant)
'    Valores = "CD_CONCE_CXP=" & Comi & "CINV" & Comi & Coma
'    Valores = Valores & "CD_NUME_CXP=" & CDbl(Conse_CXP) & Coma
'    Valores = Valores & "FE_FECH_CXP=" & Comi & fechag & Comi & Coma
'    Valores = Valores & "FE_VENC_CXP=" & Comi & fechav & Comi & Coma
'    Valores = Valores & "CD_TERC_CXP=" & Comi & Cambiar_Comas_Comillas(TxtCompra(8)) & Comi & Coma
'    Valores = Valores & "NU_FAPR_CXP=" & CDbl(TxtCompra(10)) & Coma
'    Valores = Valores & "VL_BRUT_CXP=" & CDbl(TxtCompra(12)) & Coma
'    Valores = Valores & "VL_BRUE_CXP=0" & Coma
'    Valores = Valores & "VL_NETO_CXP=0" & CDbl(TxtCompra(17)) & Coma
'    Valores = Valores & "VL_CANC_CXP=0" & Coma
'    Valores = Valores & "VL_NCRE_CXP=0" & Coma
'    Valores = Valores & "VL_NDEB_CXP=0" & Coma
'    Valores = Valores & "NU_EGRE_CXP=0" & Coma
'    Valores = Valores & "DE_OBSE_CXP=" & Comi & Cambiar_Comas_Comillas(TxtCompra(11)) & Comi & Coma
'    ''Fletes y retencion
'    Valores = Valores & "VL_FLET_CXP=" & CDbl(TxtCompra(16)) & Coma
'    Valores = Valores & "VL_RETE_CXP=" & CDbl(TxtCompra(15)) & Coma
'    ''Descuentos e Impuestos
'    Valores = Valores & "VL_DESC_CXP=" & CDbl(TxtCompra(13)) & Coma
'    Valores = Valores & "VL_IMPU_CXP=" & CDbl(TxtCompra(14)) & Coma
'    ''Monto Escrito
'    Valores = Valores & "DE_MONT_CXP=" & Comi & Monto_Escrito(CDbl(TxtCompra(17))) & Comi & Coma
'    ''Estado
'    Valores = Valores & "ID_ESTA_CXP=1" & Coma
'    Valores = Valores & "ID_PPTO_CXP=0" ' Realizado para que funcione la interface con cxpagar giu
'    Debug.Print Valores
'    Result = DoInsertSQL("C_X_P", Valores)
'    ''Actualiza el consecutivo del concepto
'    If Result <> FAIL Then Result = DoUpdate("CONCEPTO", "NU_CONS_CONC=" & Conse_CXP, "ID_TIPO_CONC=7 And NU_CONS_CONC <" & Conse_CXP)
'    If Result <> FAIL Then Result = DoUpdate("PARAMETROS_CXP", "NU_CXPA_APLI=" & Conse_CXP, NUL$)
'End Sub

Private Sub CentraMarco(ByRef Mar As VB.Frame)
    Mar.Top = (Me.Height - Mar.Height) / 2
    Mar.Left = (Me.Width - Mar.Width) / 2
End Sub

Private Sub GuardaValoresParticulatres()
    Dim Fec As String
    Select Case Documento.Encabezado.TipDeDcmnt
    Case Is = Entrada
'Tabla: IN_PARTICULAR
'1            PORDES       PORCENTAJE DE DESCUENTO                            3
'2            RETFUE       PORCENTAJE DE RETENCION EN LA FUENTE               3
'3            RETIVA       PORCENTAJE DE RETENCION DE IVA                     3
'4            RETICA       PORCENTAJE DE RETENCION DE ICA                     3
'5            FLETES       COSTO DE LOS FLETES                                3
'6            DESADI       PORCENTAJE DE DESCUENTO ADICIONAL                  3
'7            PROPAR       PAGA FLETES PROVEDOR/PARTICULAR                    3
'8            FACTUR       NUMERO DE FACTURA                                  3
        If IsNumeric(Me.TxtEntra(7).Text) Then Call InsertaValoresParticulares(1, CSng(Me.TxtEntra(7).Text))
        If IsNumeric(Me.TxtEntra(8).Text) Then Call InsertaValoresParticulares(2, CSng(Me.TxtEntra(8).Text))
        If IsNumeric(Me.TxtEntra(16).Text) Then Call InsertaValoresParticulares(3, CSng(Me.TxtEntra(16).Text))
        If IsNumeric(Me.TxtEntra(17).Text) Then Call InsertaValoresParticulares(4, CSng(Me.TxtEntra(17).Text))
        If IsNumeric(Me.TxtEntra(14).Text) Then Call InsertaValoresParticulares(5, CSng(Me.TxtEntra(14).Text))
        If IsNumeric(Me.TxtEntra(20).Text) Then Call InsertaValoresParticulares(6, CSng(Me.TxtEntra(20).Text))
        If Me.OptParticular.Value Then Call InsertaValoresParticulares(7, 1)
        If IsNumeric(Me.TxtCompra(10).Text) Then Call InsertaValoresParticulares(8, CSng(Me.TxtCompra(10).Text))
    Case Is = FacturaVenta
        If IsNumeric(Me.TxtVenta(4).Text) Then Call InsertaValoresParticulares(9, CSng(Me.TxtVenta(4).Text))
        If IsNumeric(TxtVenta(5).Text) And IsNumeric(TxtVenta(6).Text) And IsNumeric(TxtVenta(7).Text) Then
            Fec = CInt(IsNumeric(TxtVenta(5).Text)) & "/" & CInt(IsNumeric(TxtVenta(6).Text)) & "/" & CInt(IsNumeric(TxtVenta(7).Text))
            If IsDate(Fec) Then Call InsertaValoresParticulares(10, CSng(CDate(Fec)))
        End If
        If IsNumeric(Me.TxtVenta(12).Text) Then Call InsertaValoresParticulares(11, CSng(Me.TxtVenta(12).Text))
        If IsNumeric(Me.TxtVenta(13).Text) Then Call InsertaValoresParticulares(12, CSng(Me.TxtVenta(13).Text))
        If IsNumeric(Me.TxtVenta(19).Text) Then Call InsertaValoresParticulares(14, CSng(Me.TxtVenta(13).Text))
    End Select
End Sub

Private Sub CargaValoresParticulatres()
    Dim ArrPRT()
    Dim qaz As Integer
    Campos = "NU_AUTO_PART_PRPR, NU_VALO_PRPR"
    Desde = "IN_PARTIPARTI"
    Condi = "NU_AUTO_ENCA_PRPR=" & Documento.Encabezado.AutoDelDocCARGADO
    ReDim ArrPRT(1, 0)
    Result = LoadMulData(Desde, Campos, Condi, ArrPRT())
    If Result = FAIL Then Exit Sub
    If Not Encontro Then Exit Sub
    
    Select Case Documento.Encabezado.TipDeDcmnt
    Case Is = Entrada
        For qaz = 0 To UBound(ArrPRT, 2)
            Select Case ArrPRT(0, qaz)
            Case Is = 1
                Me.TxtEntra(7).Text = Formatear_Porcentaje(ArrPRT(1, qaz))
            Case Is = 2
                Me.TxtEntra(8).Text = Formatear_Porcentaje(ArrPRT(1, qaz))
            Case Is = 3
                Me.TxtEntra(16).Text = Formatear_Porcentaje(ArrPRT(1, qaz))
            Case Is = 4
                Me.TxtEntra(17).Text = Formatear_Porcentaje(ArrPRT(1, qaz))
            Case Is = 5
                Me.TxtEntra(14).Text = Formatear_Valor(ArrPRT(1, qaz))
            Case Is = 6
                Me.TxtEntra(20).Text = Formatear_Porcentaje(ArrPRT(1, qaz))
            Case Is = 7
                Me.OptParticular.Value = (Not CInt(ArrPRT(1, qaz)) = 0)
            Case Is = 8
                Me.TxtCompra(10).Text = Formatear_Cantidad(ArrPRT(1, qaz))
            End Select
        Next
    Case Is = DevolucionEntrada
    Case Is = FacturaVenta
        For qaz = 0 To UBound(ArrPRT, 2)
            Select Case ArrPRT(0, qaz)
            Case Is = 9
                Me.TxtVenta(4).Text = Formatear_Cantidad(ArrPRT(1, qaz))
            Case Is = 10
                Me.TxtVenta(5).Text = Formatear_Cantidad(Day(CDate(ArrPRT(1, qaz))))
                Me.TxtVenta(6).Text = Formatear_Cantidad(Month(CDate(ArrPRT(1, qaz))))
                Me.TxtVenta(7).Text = Formatear_Cantidad(Year(CDate(ArrPRT(1, qaz))))
            Case Is = 11
                Me.TxtVenta(12).Text = Formatear_Porcentaje(ArrPRT(1, qaz))
            Case Is = 12
                Me.TxtVenta(13).Text = Formatear_Porcentaje(ArrPRT(1, qaz))
            Case Is = 14
                Me.TxtVenta(19).Text = Formatear_Valor(ArrPRT(1, qaz))
            End Select
        Next
    Case Is = DevolucionFacturaVenta
    End Select
End Sub

Private Sub InsertaValoresParticulares(tipo As Long, Valo As Single)
    Dim Cuer As String
    If Valo <> 0 Then
        Cuer = Documento.Encabezado.AutoDelDocCARGADO & "," & tipo & "," & Valo
        Result = DoInsert("IN_PARTIPARTI", Cuer)
    End If
End Sub
