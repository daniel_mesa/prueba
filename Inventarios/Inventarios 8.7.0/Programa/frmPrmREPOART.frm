VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Object = "{C932BA88-4374-101B-A56C-00AA003668DC}#1.1#0"; "MSMASK32.OCX"
Object = "{0BA686C6-F7D3-101A-993E-0000C0EF6F5E}#1.0#0"; "THREED32.OCX"
Begin VB.Form frmPrmREPOART 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Parametros para los ARTICULOS"
   ClientHeight    =   7095
   ClientLeft      =   45
   ClientTop       =   435
   ClientWidth     =   11475
   Icon            =   "frmPrmREPOART.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   7095
   ScaleWidth      =   11475
   Begin VB.CheckBox chkTERCEROS 
      Caption         =   "Todos Los Terceros"
      Height          =   375
      Left            =   7800
      TabIndex        =   3
      Top             =   3480
      Width           =   3135
   End
   Begin VB.CheckBox chkARTICULOS 
      Caption         =   "Todos Los Art�culos"
      Height          =   375
      Left            =   600
      TabIndex        =   1
      Top             =   480
      Width           =   3255
   End
   Begin VB.Frame fraRANGO 
      Height          =   1095
      Left            =   690
      TabIndex        =   16
      Top             =   5880
      Width           =   10005
      Begin VB.Frame Frame2 
         Height          =   615
         Left            =   120
         TabIndex        =   22
         Top             =   270
         Width           =   3885
         Begin VB.OptionButton Optestado 
            Caption         =   "Anulados"
            Height          =   315
            Index           =   2
            Left            =   2460
            TabIndex        =   9
            Top             =   210
            Width           =   1300
         End
         Begin VB.OptionButton Optestado 
            Caption         =   "No Anulados"
            Height          =   315
            Index           =   1
            Left            =   1050
            TabIndex        =   8
            Top             =   210
            Width           =   1300
         End
         Begin VB.OptionButton Optestado 
            Caption         =   "Todos"
            Height          =   315
            Index           =   0
            Left            =   90
            TabIndex        =   7
            Top             =   210
            Value           =   -1  'True
            Width           =   1300
         End
      End
      Begin MSMask.MaskEdBox txtFCDESDE 
         Height          =   315
         Left            =   4800
         TabIndex        =   11
         Top             =   600
         Width           =   1215
         _ExtentX        =   2143
         _ExtentY        =   556
         _Version        =   393216
         PromptChar      =   "_"
      End
      Begin MSMask.MaskEdBox txtFCHASTA 
         Height          =   315
         Left            =   7080
         TabIndex        =   12
         Top             =   630
         Width           =   1215
         _ExtentX        =   2143
         _ExtentY        =   556
         _Version        =   393216
         PromptChar      =   "_"
      End
      Begin VB.Frame Frame1 
         Height          =   615
         Left            =   360
         TabIndex        =   19
         Top             =   240
         Visible         =   0   'False
         Width           =   3225
         Begin VB.OptionButton opcDET 
            Caption         =   "Detallado"
            Height          =   315
            Left            =   1800
            TabIndex        =   21
            Top             =   240
            Width           =   1300
         End
         Begin VB.OptionButton opcRES 
            Caption         =   "Resumido"
            Height          =   315
            Left            =   240
            TabIndex        =   20
            Top             =   200
            Value           =   -1  'True
            Width           =   1300
         End
      End
      Begin VB.CheckBox chkFCRANGO 
         Caption         =   "Condicionar las fechas del reporte"
         Height          =   315
         Left            =   4800
         TabIndex        =   10
         Top             =   240
         Width           =   3135
      End
      Begin Threed.SSCommand cmdIMPRIMIR 
         Height          =   735
         Index           =   2
         Left            =   8760
         TabIndex        =   13
         Top             =   240
         Width           =   855
         _Version        =   65536
         _ExtentX        =   1508
         _ExtentY        =   1296
         _StockProps     =   78
         Caption         =   "&IMPRIMIR"
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   6
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         RoundedCorners  =   0   'False
         Picture         =   "frmPrmREPOART.frx":058A
      End
      Begin VB.Label lblDESDE 
         Alignment       =   1  'Right Justify
         Caption         =   "Desde:"
         Height          =   255
         Left            =   3960
         TabIndex        =   18
         Top             =   600
         Width           =   735
      End
      Begin VB.Label Label1 
         Alignment       =   1  'Right Justify
         Caption         =   "Hasta:"
         Height          =   255
         Left            =   6240
         TabIndex        =   17
         Top             =   600
         Width           =   735
      End
   End
   Begin MSComctlLib.ProgressBar pgbLLEVO 
      Height          =   255
      Left            =   6480
      TabIndex        =   14
      Top             =   120
      Visible         =   0   'False
      Width           =   4455
      _ExtentX        =   7858
      _ExtentY        =   450
      _Version        =   393216
      Appearance      =   1
   End
   Begin VB.ComboBox cboTIPO 
      Height          =   315
      Left            =   3240
      TabIndex        =   0
      Text            =   "Combo1"
      Top             =   120
      Width           =   2655
   End
   Begin MSComctlLib.ImageList imgBotones 
      Left            =   10920
      Top             =   3960
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   20
      ImageHeight     =   14
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   7
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmPrmREPOART.frx":0C54
            Key             =   ""
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmPrmREPOART.frx":0FA6
            Key             =   "ASG"
         EndProperty
         BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmPrmREPOART.frx":1260
            Key             =   "CAN"
         EndProperty
         BeginProperty ListImage4 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmPrmREPOART.frx":15B2
            Key             =   "SAV"
         EndProperty
         BeginProperty ListImage5 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmPrmREPOART.frx":18CC
            Key             =   "ADD"
         EndProperty
         BeginProperty ListImage6 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmPrmREPOART.frx":1B86
            Key             =   "DEL"
         EndProperty
         BeginProperty ListImage7 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmPrmREPOART.frx":1FC8
            Key             =   "CHA"
         EndProperty
      EndProperty
   End
   Begin MSComctlLib.ListView lstARTICULOS 
      Height          =   2295
      Left            =   480
      TabIndex        =   2
      Top             =   960
      Width           =   10500
      _ExtentX        =   18521
      _ExtentY        =   4048
      View            =   3
      LabelEdit       =   1
      MultiSelect     =   -1  'True
      LabelWrap       =   -1  'True
      HideSelection   =   0   'False
      AllowReorder    =   -1  'True
      Checkboxes      =   -1  'True
      FullRowSelect   =   -1  'True
      GridLines       =   -1  'True
      _Version        =   393217
      ForeColor       =   -2147483640
      BackColor       =   -2147483643
      BorderStyle     =   1
      Appearance      =   1
      NumItems        =   2
      BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Text            =   "Nombre del Art�culo"
         Object.Width           =   7761
      EndProperty
      BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   1
         Text            =   "C�digo"
         Object.Width           =   3881
      EndProperty
   End
   Begin MSComctlLib.ListView lstBODEGAS 
      Height          =   1935
      Left            =   480
      TabIndex        =   4
      Top             =   3840
      Width           =   3255
      _ExtentX        =   5741
      _ExtentY        =   3413
      View            =   3
      LabelEdit       =   1
      MultiSelect     =   -1  'True
      LabelWrap       =   -1  'True
      HideSelection   =   -1  'True
      FullRowSelect   =   -1  'True
      GridLines       =   -1  'True
      _Version        =   393217
      ForeColor       =   -2147483640
      BackColor       =   -2147483643
      BorderStyle     =   1
      Appearance      =   1
      NumItems        =   0
   End
   Begin MSComctlLib.ListView lstTERCEROS 
      Height          =   1935
      Left            =   7680
      TabIndex        =   6
      Top             =   3840
      Width           =   3255
      _ExtentX        =   5741
      _ExtentY        =   3413
      View            =   3
      LabelEdit       =   1
      MultiSelect     =   -1  'True
      LabelWrap       =   -1  'True
      HideSelection   =   -1  'True
      FullRowSelect   =   -1  'True
      GridLines       =   -1  'True
      _Version        =   393217
      ForeColor       =   -2147483640
      BackColor       =   -2147483643
      BorderStyle     =   1
      Appearance      =   1
      NumItems        =   0
   End
   Begin MSComctlLib.ListView lstCOMPRO 
      Height          =   1935
      Left            =   4080
      TabIndex        =   5
      Top             =   3840
      Width           =   3255
      _ExtentX        =   5741
      _ExtentY        =   3413
      View            =   3
      LabelEdit       =   1
      MultiSelect     =   -1  'True
      LabelWrap       =   -1  'True
      HideSelection   =   -1  'True
      FullRowSelect   =   -1  'True
      GridLines       =   -1  'True
      _Version        =   393217
      ForeColor       =   -2147483640
      BackColor       =   -2147483643
      BorderStyle     =   1
      Appearance      =   1
      NumItems        =   0
   End
   Begin VB.Label txtTIPO 
      Alignment       =   1  'Right Justify
      Caption         =   "Tipo de Art�culos:"
      Height          =   315
      Left            =   600
      TabIndex        =   15
      Top             =   120
      Width           =   2415
   End
End
Attribute VB_Name = "frmPrmREPOART"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Dim ArrUXB() As Variant
Dim Articulo As UnArticulo
Dim Dueno As New ElTercero
'Dim LaBodega As String, LaAccion As String
Dim LaAccion As String      'DEPURACION DE CODIGO
'JAUM T29577 Inicio se deja linea en comentario
'Dim CnTdr As Integer, NumEnCombo As Integer
Dim CnTdr As Double 'Contador
Dim NumEnCombo As Integer
'JAUM T29577 Fin
Dim strDBODEGAS As String, strDCOMPRO As String, strDTERCEROS As String
Dim strTITFECHA As String, strTITRANGO As String, strARTICULOS As String
Public OpcCod        As String   'Opci�n de seguridad

Private Sub cboTIPO_GotFocus()
    NumEnCombo = Me.cboTIPO.ListIndex
End Sub

Private Sub CboTipo_KeyPress(KeyAscii As Integer)
Call Cambiar_Enter(KeyAscii) 'NYCM M2486
End Sub

Private Sub chkARTICULOS_KeyPress(KeyAscii As Integer)
Call Cambiar_Enter(KeyAscii) 'NYCM M2486
End Sub

Private Sub chkfcRANGO_Click()
    Me.txtFCDESDE.Enabled = False
    Me.txtFCHASTA.Enabled = False
    If Not CBool(Me.chkFCRANGO.Value) = True Then Exit Sub
    Me.txtFCDESDE.Enabled = True
    Me.txtFCHASTA.Enabled = True
End Sub
Private Sub chkFCRANGO_KeyPress(KeyAscii As Integer)
Call Cambiar_Enter(KeyAscii) 'NYCM M2486
End Sub

Private Sub chkTERCEROS_KeyPress(KeyAscii As Integer)
Call Cambiar_Enter(KeyAscii) 'NYCM M2486
End Sub

Private Sub CmdImprimir_Click(Index As Integer)
'strDBODEGAS, strDCOMPRO, strARTICULOS, strTITFECHA
    Call Limpiar_CrysListar
    strDBODEGAS = "": strDCOMPRO = "": strARTICULOS = "": strDTERCEROS = ""
    Dim Renglon As MSComctlLib.ListItem, txtDR As String * 1
    
    If Me.chkARTICULOS.Value = 0 Then
        For Each Renglon In Me.lstARTICULOS.ListItems
            If Renglon.Selected Then strARTICULOS = strARTICULOS & IIf(Len(strARTICULOS) > 0, " OR ", "") & "{ARTICULO.CD_CODI_ARTI}='" & Renglon.ListSubItems("COAR").Text & "'"
        Next
    End If
    If Len(strARTICULOS) > 0 Then strARTICULOS = "(" & strARTICULOS & ")"
    
    For Each Renglon In Me.lstBODEGAS.ListItems
        If Renglon.Selected Then strDBODEGAS = strDBODEGAS & IIf(Len(strDBODEGAS) > 0, " OR ", "") & "{IN_BODEGA.TX_CODI_BODE}='" & Renglon.ListSubItems("COD").Text & "'"
    Next
    If Len(strDBODEGAS) > 0 Then strDBODEGAS = "(" & strDBODEGAS & ")"
    
    If Len(strARTICULOS) > 0 And Len(strDBODEGAS) > 0 Then
        strARTICULOS = strARTICULOS & " AND " & strDBODEGAS
    Else
        strARTICULOS = strARTICULOS & strDBODEGAS
    End If
    
    If Me.chkTERCEROS.Value = 0 Then
        For Each Renglon In Me.lstTERCEROS.ListItems
            If Renglon.Selected Then strDTERCEROS = strDTERCEROS & IIf(Len(strDTERCEROS) > 0, " OR ", "") & "{TERCERO.CD_CODI_TERC}='" & Renglon.ListSubItems("COD").Text & "'"
        Next
    End If
    If Len(strDTERCEROS) > 0 Then strDTERCEROS = "(" & strDTERCEROS & ")"
    
    If Len(strARTICULOS) > 0 And Len(strDTERCEROS) > 0 Then
        strARTICULOS = strARTICULOS & " AND " & strDTERCEROS
    Else
        strARTICULOS = strARTICULOS & strDTERCEROS
    End If
    
    For Each Renglon In Me.lstCOMPRO.ListItems
        If Renglon.Selected Then strDCOMPRO = strDCOMPRO & IIf(Len(strDCOMPRO) > 0, " OR ", "") & "{IN_COMPROBANTE.TX_CODI_COMP}='" & Renglon.ListSubItems("COD").Text & "'"
    Next
    If Len(strDCOMPRO) > 0 Then strDCOMPRO = "(" & strDCOMPRO & ")"
    
    If Len(strARTICULOS) > 0 And Len(strDCOMPRO) > 0 Then
        strARTICULOS = strARTICULOS & " AND " & strDCOMPRO
    Else
        strARTICULOS = strARTICULOS & strDCOMPRO
    End If
    
'    If CBool(Me.chkFCRANGO.Value) Then strARTICULOS = strARTICULOS & IIf(Len(strARTICULOS) > 0, " AND ", "") & _
'        "({IN_KARDEX.FE_FECH_KARD} in CDate('" & Me.txtFCDESDE.Text & "') to CDate('" & Me.txtFCHASTA.Text & "'))"
'infARTIxDOCU.rpt
    strTITFECHA = "A LA FECHA"
    If CBool(Me.chkFCRANGO.Value) Then
        strARTICULOS = strARTICULOS & IIf(Len(strARTICULOS) > 0, " AND ", "") & _
            "({IN_ENCABEZADO.FE_CREA_ENCA} in CDate('" & Me.txtFCDESDE.Text & "') to CDate('" & Me.txtFCHASTA.Text & "'))"
        strTITFECHA = "RANGO DE FECHAS: Del " & Me.txtFCDESDE.Text & " al " & Me.txtFCHASTA.Text
    End If
    'NYCM R1800-----
    If Optestado(1) Then
        strARTICULOS = strARTICULOS & " AND {IN_ENCABEZADO.TX_ESTA_ENCA} <> " & "'A' "
    
    ElseIf Optestado(2) Then
        strARTICULOS = strARTICULOS & " AND {IN_ENCABEZADO.TX_ESTA_ENCA} = " & "'A'"
    End If
    '/---------------
    strARTICULOS = strARTICULOS & IIf(Len(strARTICULOS) = 0, "", " AND ") & "({IN_DETALLE.NU_AUTO_ORGCABE_DETA}={IN_DETALLE.NU_AUTO_MODCABE_DETA})"
    Debug.Print strARTICULOS
    MDI_Inventarios.Crys_Listar.Formulas(8) = "RAGFecha=" & Comi & strTITFECHA & Comi
    MDI_Inventarios.Crys_Listar.Formulas(9) = "RAGCompr=" & Comi & strTITRANGO & Comi
    MDI_Inventarios.Crys_Listar.Formulas(10) = "ENTIDAD=" & Comi & Trim(Dueno.Nombre) & ", NIT:" & Trim(Dueno.Nit) & Comi
    txtDR = IIf(Me.opcDET.Value, "D", "R")
    Call ListarD("infARTIxDOCU.rpt", strARTICULOS, crptToWindow, "ARTICULOS X DOCUMENTO", MDI_Inventarios, True)
    strDCOMPRO = ""
    
'Formula = "{IN_ENCABEZADO.NU_AUTO_DOCU_ENCA}=" & Documento.Encabezado.TipDeDcmnt & _
'    " AND {IN_ENCABEZADO.NU_COMP_ENCA}=" & CLng(Me.txtNumero) & _
'    " AND {IN_ENCABEZADO_1.NU_COMP_ENCA}=" & CLng(Me.txtNumero) & _
'    " AND {IN_DETALLE.NU_AUTO_BODE_DETA}=" & Me.cboBodega.ItemData(Me.cboBodega.ListIndex)
    
    MDI_Inventarios.Crys_Listar.Formulas(8) = ""
    MDI_Inventarios.Crys_Listar.Formulas(9) = ""
    MDI_Inventarios.Crys_Listar.Formulas(10) = ""
'    Call ListarD("infARTIxDOCU" & txtDR & ".rpt", strARTICULOS, crptToWindow, "ARTICULOS X DOCUMENTO", MDI_Inventarios)
'    Call ListarD("infKARDEX" & IIf(Me.opcCON, "CON", "SIN") & ".rpt", strARTICULOS, crptToWindow, "ARTICULOS X DOCUMENTO", MDI_Inventarios)
End Sub

Private Sub Form_Load()
    Call CenterForm(MDI_Inventarios, Me)
    Me.cboTIPO.Clear
    Dueno.IniXNit (Dueno.Nit)
    Me.cboTIPO.AddItem "Para la Venta"
    Me.cboTIPO.AddItem "Para El Consumo"
    NumEnCombo = Me.cboTIPO.ListIndex
    If Len(FormatDateTime(Date, vbShortDate)) = 8 Then
        Me.txtFCDESDE.Mask = "##/##/##"
        Me.txtFCHASTA.Mask = "##/##/##"
    Else
        Me.txtFCDESDE.Mask = "##/##/####"
        Me.txtFCHASTA.Mask = "##/##/####"
    End If
    Me.txtFCDESDE.Text = FormatDateTime(DateAdd("d", -(Day(Date) - 1), Date), vbShortDate)
    Me.txtFCHASTA.Text = FormatDateTime(DateAdd("d", -1, DateAdd("M", 1, DateAdd("d", -(Day(Date) - 1), Date))), vbShortDate)
    Me.chkFCRANGO.Value = False
    Me.txtFCDESDE.Enabled = False
    Me.txtFCHASTA.Enabled = False
    
    Me.lstBODEGAS.ListItems.Clear
    Me.lstBODEGAS.Checkboxes = False
    Me.lstBODEGAS.MultiSelect = True
    Me.lstBODEGAS.HideSelection = False
    Me.lstBODEGAS.ColumnHeaders.Clear
    Me.lstBODEGAS.ColumnHeaders.Add , "NOM", "Bodega", 0.66 * Me.lstBODEGAS.Width
    Me.lstBODEGAS.ColumnHeaders.Add , "COD", "C�digo", 0.34 * Me.lstBODEGAS.Width
    
    Me.lstCOMPRO.ListItems.Clear
    Me.lstCOMPRO.Checkboxes = False
    Me.lstCOMPRO.MultiSelect = True
    Me.lstCOMPRO.HideSelection = False
    Me.lstCOMPRO.ColumnHeaders.Clear
    Me.lstCOMPRO.ColumnHeaders.Add , "NOM", "Comprobante", 0.66 * Me.lstCOMPRO.Width
    Me.lstCOMPRO.ColumnHeaders.Add , "COD", "C�digo", 0.34 * Me.lstCOMPRO.Width
    
    Me.lstTERCEROS.ListItems.Clear
    Me.lstTERCEROS.Checkboxes = False
    Me.lstTERCEROS.MultiSelect = True
    Me.lstTERCEROS.HideSelection = False
    Me.lstTERCEROS.ColumnHeaders.Clear
    Me.lstTERCEROS.ColumnHeaders.Add , "NOM", "Tercero", 0.66 * Me.lstCOMPRO.Width
    Me.lstTERCEROS.ColumnHeaders.Add , "COD", "Nit", 0.34 * Me.lstCOMPRO.Width
    
    Me.lstARTICULOS.ListItems.Clear
    Me.lstARTICULOS.Checkboxes = False
    Me.lstARTICULOS.MultiSelect = True
    Me.lstARTICULOS.HideSelection = False
    Me.lstARTICULOS.Width = 10500
    Me.lstARTICULOS.ColumnHeaders.Clear
    Me.lstARTICULOS.ColumnHeaders.Add , "NOAR", "Nombre del art�culo", 0.4 * Me.lstARTICULOS.Width
    Me.lstARTICULOS.ColumnHeaders.Add , "COAR", "C�digo", 0.12 * Me.lstARTICULOS.Width
    Me.lstARTICULOS.ColumnHeaders.Add , "NOCO", "Unidad de conteo", 0.12 * Me.lstARTICULOS.Width
    Me.lstARTICULOS.ColumnHeaders.Add , "COPR", "Costo", 0.1 * Me.lstARTICULOS.Width
    Me.lstARTICULOS.ColumnHeaders("COPR").Alignment = lvwColumnRight
    Me.lstARTICULOS.ColumnHeaders.Add , "NOUS", "Uso", 0.12 * Me.lstARTICULOS.Width
    Me.lstARTICULOS.ColumnHeaders.Add , "NOGR", "Grupo", 0.12 * Me.lstARTICULOS.Width
    Me.lstARTICULOS.ColumnHeaders.Add , "AUCO", "AutoUNC", 0
    Me.lstARTICULOS.ColumnHeaders.Add , "COGR", "CodGRU", 0
    Me.lstARTICULOS.ColumnHeaders.Add , "COUS", "CodUSO", 0
FALLO:
End Sub

Private Sub cboTIPO_LostFocus()
    If NumEnCombo = Me.cboTIPO.ListIndex Then Exit Sub
    Dim tipo As String * 1
    Me.lstARTICULOS.ListItems.Clear
    Me.pgbLLEVO.Value = 0
    Me.pgbLLEVO.Visible = True
    Select Case Me.cboTIPO.ListIndex
    Case Is = -1
        tipo = "0"
    Case Is = 0
        tipo = "V"
    Case Is = 1
        tipo = "C"
    End Select
    
'    Campos = "NU_AUTO_BODE, NU_AUTO_BODE_BODE, TX_CODI_BODE, TX_NOMB_BODE, CD_CODI_CECO_BODE, TX_VENTA_BODE"
    Campos = "DISTINCT NU_AUTO_BODE, NU_AUTO_BODE_BODE, TX_CODI_BODE, TX_NOMB_BODE"
    Desde = "IN_BODEGA"
    Condi = "TX_VENTA_BODE='" & tipo & "' ORDER BY TX_NOMB_BODE"

    ReDim ArrUXB(3, 0)
    Result = LoadMulData(Desde, Campos, Condi, ArrUXB())
    If Result = FAIL Then GoTo FALLO
    lstBODEGAS.ListItems.Clear
    On Error GoTo SIBOD
    For CnTdr = 0 To UBound(ArrUXB, 2)
        Me.lstBODEGAS.ListItems.Add , "B" & ArrUXB(2, CnTdr), ArrUXB(3, CnTdr)
        Me.lstBODEGAS.ListItems("B" & ArrUXB(2, CnTdr)).Tag = ArrUXB(0, CnTdr)
        Me.lstBODEGAS.ListItems("B" & ArrUXB(2, CnTdr)).ListSubItems.Add , "COD", ArrUXB(2, CnTdr)
        If Len(strDBODEGAS) = 0 Then strDBODEGAS = ""
        strDBODEGAS = strDBODEGAS & IIf(Len(strDBODEGAS) = 0, "", ",") & ArrUXB(0, CnTdr)
SIBOD:
    Next
    On Error GoTo 0
    
    If Len(strDBODEGAS) > 0 Then 'NMSR M3849
'    Campos = "DISTINCT NU_AUTO_COMP, TX_CODI_COMP, TX_NOMB_COMP, TX_PRFJ_COMP, NU_AUTO_DOCU_COMP, NU_COMP_COMP, NU_AUTO_ANUL_COMP, TX_REQU_COMP, TX_ROMP_COMP, NU_AUTO_BODE_RBOCO"
    Campos = "DISTINCT NU_AUTO_COMP, TX_CODI_COMP, TX_NOMB_COMP"
    Desde = "IN_COMPROBANTE, IN_R_BODE_COMP"
    Condi = "NU_AUTO_COMP = NU_AUTO_COMP_RBOCO" & _
        " AND NU_AUTO_BODE_RBOCO IN (" & strDBODEGAS & ") ORDER BY TX_NOMB_COMP"
    ReDim ArrUXB(2, 0)
    Result = LoadMulData(Desde, Campos, Condi, ArrUXB())
    If Result = FAIL Then GoTo FALLO
    Me.lstCOMPRO.ListItems.Clear
    On Error GoTo SICOM
    For CnTdr = 0 To UBound(ArrUXB, 2)
        Me.lstCOMPRO.ListItems.Add , "C" & ArrUXB(1, CnTdr), ArrUXB(2, CnTdr)
        Me.lstCOMPRO.ListItems("C" & ArrUXB(1, CnTdr)).Tag = ArrUXB(0, CnTdr)
        Me.lstCOMPRO.ListItems("C" & ArrUXB(1, CnTdr)).ListSubItems.Add , "COD", ArrUXB(1, CnTdr)
        If Len(strDCOMPRO) = 0 Then strDCOMPRO = ""
        strDCOMPRO = strDCOMPRO & IIf(Len(strDCOMPRO) = 0, "", ",") & ArrUXB(0, CnTdr)
SICOM:
    Next
    'NMSR M3849
    End If
    If Len(strDCOMPRO) > 0 Then
    'NMSR M3849
    Campos = "DISTINCT CD_CODI_TERC, NO_NOMB_TERC"
    Desde = "IN_ENCABEZADO, TERCERO"
    Condi = "CD_CODI_TERC_ENCA = CD_CODI_TERC AND NU_AUTO_COMP_ENCA IN (" & strDCOMPRO & ") ORDER BY NO_NOMB_TERC"
    ReDim ArrUXB(1, 0)
    Result = LoadMulData(Desde, Campos, Condi, ArrUXB())
    If Result = FAIL Then GoTo FALLO
    On Error GoTo SITER
    Me.lstTERCEROS.ListItems.Clear
    For CnTdr = 0 To UBound(ArrUXB, 2)
        Me.lstTERCEROS.ListItems.Add , "T" & ArrUXB(0, CnTdr), ArrUXB(1, CnTdr)
        Me.lstTERCEROS.ListItems("T" & ArrUXB(0, CnTdr)).Tag = ArrUXB(0, CnTdr)
        Me.lstTERCEROS.ListItems("T" & ArrUXB(0, CnTdr)).ListSubItems.Add , "COD", ArrUXB(0, CnTdr)
SITER:
    Next
    End If 'NMSR M3849
    Campos = "NU_AUTO_ARTI, CD_CODI_ARTI, NO_NOMB_ARTI, NU_AUTO_UNVE_ARTI, TX_PARA_ARTI, " & _
        "DE_DESC_GRUP, DE_DESC_USOS, TX_NOMB_UNVE, CD_GRUP_ARTI, CD_USOS_ARTI, " & _
        "NU_MULT_UNVE, NU_DIVI_UNVE, VL_COPR_ARTI"
    Desde = "ARTICULO, USOS, GRUP_ARTICULO, IN_UNDVENTA"
    Condi = "CD_CODI_GRUP = CD_GRUP_ARTI AND CD_CODI_USOS = CD_USOS_ARTI"
    Condi = Condi & " AND NU_AUTO_UNVE = NU_AUTO_UNVE_ARTI"
    Condi = Condi & " AND TX_PARA_ARTI='" & tipo & "' ORDER BY NO_NOMB_ARTI"
    ReDim ArrUXB(12, 0)
    Result = LoadMulData(Desde, Campos, Condi, ArrUXB())
    If Result = FAIL Then GoTo FALLO
    If Not Encontro Then GoTo NOENC
    Me.pgbLLEVO.Min = 0
    Me.pgbLLEVO.Value = 0
    Me.pgbLLEVO.Max = UBound(ArrUXB, 2) + 1
    Dim Articulo As New UnArticulo
    For CnTdr = 0 To UBound(ArrUXB, 2)
        Me.pgbLLEVO.Value = CnTdr
        Articulo.AutoNumero = ArrUXB(0, CnTdr)
        Articulo.Codigo = ArrUXB(1, CnTdr)
        Articulo.Nombre = ArrUXB(2, CnTdr)
        Articulo.AutoUDConteo = ArrUXB(3, CnTdr)
        Articulo.NombreGrupo = ArrUXB(5, CnTdr)
        Articulo.NombreUso = ArrUXB(6, CnTdr)
        Articulo.NombreUndConteo = ArrUXB(7, CnTdr)
        Articulo.ParaQueEs = InStr(1, "VC", ArrUXB(4, CnTdr)) - 1
        Articulo.CodigoGrupo = ArrUXB(8, CnTdr)
        Articulo.CodigoUso = ArrUXB(9, CnTdr)
        If Not IsNumeric(ArrUXB(12, CnTdr)) Then ArrUXB(12, CnTdr) = "0"
        Articulo.CostoPromedio = ArrUXB(12, CnTdr)
        On Error GoTo NLADD
        Me.lstARTICULOS.ListItems.Add , "A" & Articulo.AutoNumero, Trim(Articulo.Nombre)
        Me.lstARTICULOS.ListItems("A" & Articulo.AutoNumero).Tag = CStr(Articulo.AutoNumero)
'lstArticulos, NOAR, COAR, NOCO, COPR, NOUS, NOGR, AUCO, COGR, COUS, MUCO, DICO
        Me.lstARTICULOS.ListItems("A" & Articulo.AutoNumero).ListSubItems.Add , "COAR", Trim(Articulo.Codigo)
        Me.lstARTICULOS.ListItems("A" & Articulo.AutoNumero).ListSubItems.Add , "NOCO", Trim(Articulo.NombreUndConteo)
        Me.lstARTICULOS.ListItems("A" & Articulo.AutoNumero).ListSubItems.Add , "COPR", Format(Articulo.CostoPromedio, "###,###,##0.00")
        Me.lstARTICULOS.ListItems("A" & Articulo.AutoNumero).ListSubItems.Add , "NOUS", Trim(Articulo.NombreUso)
        Me.lstARTICULOS.ListItems("A" & Articulo.AutoNumero).ListSubItems.Add , "NOGR", Trim(Articulo.NombreGrupo)
        Me.lstARTICULOS.ListItems("A" & Articulo.AutoNumero).ListSubItems.Add , "AUCO", Trim(Articulo.AutoUDConteo)
        Me.lstARTICULOS.ListItems("A" & Articulo.AutoNumero).ListSubItems.Add , "COGR", Trim(Articulo.CodigoGrupo)
        Me.lstARTICULOS.ListItems("A" & Articulo.AutoNumero).ListSubItems.Add , "COUS", Trim(Articulo.CodigoUso)
NLADD:
        On Error GoTo 0
    Next
    Set Articulo = Nothing
NOENC:
FALLO:
    Me.pgbLLEVO.Visible = False
End Sub

Private Sub lstARTICULOS_ColumnClick(ByVal ColumnHeader As MSComctlLib.ColumnHeader)
    Me.lstARTICULOS.SortKey = ColumnHeader.Index - 1
    Me.lstARTICULOS.Sorted = True
End Sub

Private Sub lstARTICULOS_KeyPress(KeyAscii As Integer)
Call Cambiar_Enter(KeyAscii) 'NYCM M2486
End Sub

Private Sub lstBODEGAS_ColumnClick(ByVal ColumnHeader As MSComctlLib.ColumnHeader)
    Me.lstBODEGAS.SortKey = ColumnHeader.Index - 1
    Me.lstBODEGAS.Sorted = True
End Sub

Private Sub lstBODEGAS_KeyPress(KeyAscii As Integer)
Call Cambiar_Enter(KeyAscii) 'NYCM M2486
End Sub

Private Sub lstCOMPRO_ColumnClick(ByVal ColumnHeader As MSComctlLib.ColumnHeader)
    Me.lstCOMPRO.SortKey = ColumnHeader.Index - 1
    Me.lstCOMPRO.Sorted = True
End Sub

Private Sub lstCOMPRO_KeyPress(KeyAscii As Integer)
Call Cambiar_Enter(KeyAscii) 'NYCM M2486
End Sub

Private Sub lstTERCEROS_ColumnClick(ByVal ColumnHeader As MSComctlLib.ColumnHeader)
    Me.lstTERCEROS.SortKey = ColumnHeader.Index - 1
    Me.lstTERCEROS.Sorted = True
End Sub

Private Sub lstTERCEROS_KeyPress(KeyAscii As Integer)
Call Cambiar_Enter(KeyAscii) 'NYCM M2486
End Sub

Private Sub Optestado_KeyPress(Index As Integer, KeyAscii As Integer)
Call Cambiar_Enter(KeyAscii) 'NYCM M2486
End Sub

Private Sub txtFCDESDE_KeyPress(KeyAscii As Integer)
Call Cambiar_Enter(KeyAscii) 'NYCM M2486
End Sub

Private Sub txtFCDESDE_LostFocus()
    If IsDate(Me.txtFCDESDE.Text) Then Exit Sub
    Me.txtFCDESDE.Text = FormatDateTime(DateAdd("d", -(Day(Date) - 1), Date), vbShortDate)
    Me.txtFCDESDE.SetFocus
End Sub

Private Sub txtFCHASTA_KeyPress(KeyAscii As Integer) 'smdl m1876
    Call Cambiar_Enter(KeyAscii)
End Sub

Private Sub txtFCHASTA_LostFocus()
    'smdl m1876
    If IsDate(Me.txtFCHASTA.Text) Then
       If DateDiff("d", CDate(Me.txtFCDESDE.Text), CDate(Me.txtFCHASTA)) < 0 Then
         Call Mensaje1("La fecha final debe ser mayor a la inicial.", 3)
         Me.txtFCHASTA.SetFocus
       End If
    Else
    'smdl m1876
       Me.txtFCHASTA.Text = FormatDateTime(DateAdd("d", -1, DateAdd("M", 1, DateAdd("d", -(Day(Date) - 1), Date))), vbShortDate)
       Me.txtFCHASTA.SetFocus
    End If
End Sub

