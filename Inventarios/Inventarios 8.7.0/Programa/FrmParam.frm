VERSION 5.00
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Object = "{0BA686C6-F7D3-101A-993E-0000C0EF6F5E}#1.0#0"; "THREED32.OCX"
Begin VB.Form FrmParam 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Par�metros de la Aplicaci�n"
   ClientHeight    =   4965
   ClientLeft      =   2955
   ClientTop       =   1635
   ClientWidth     =   6855
   Icon            =   "FrmParam.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   ScaleHeight     =   4965
   ScaleWidth      =   6855
   Begin VB.Frame FraModulos 
      Caption         =   "Integrar con M�dulos de"
      Height          =   630
      Left            =   60
      TabIndex        =   0
      Top             =   120
      Width           =   5595
      Begin VB.CheckBox ChkModulo 
         Caption         =   "Contabilidad"
         Height          =   255
         Index           =   0
         Left            =   120
         TabIndex        =   1
         Top             =   255
         Width           =   1215
      End
      Begin VB.CheckBox ChkModulo 
         Caption         =   "Cartera"
         Height          =   255
         Index           =   1
         Left            =   1425
         TabIndex        =   2
         Top             =   255
         Width           =   915
      End
      Begin VB.CheckBox ChkModulo 
         Caption         =   "Cuentas por Pagar"
         Height          =   255
         Index           =   2
         Left            =   2460
         TabIndex        =   3
         Top             =   255
         Width           =   1695
      End
      Begin VB.CheckBox ChkModulo 
         Caption         =   "Presupuesto"
         Height          =   255
         Index           =   3
         Left            =   4275
         TabIndex        =   4
         Top             =   255
         Width           =   1260
      End
   End
   Begin VB.Frame framActions 
      Height          =   2085
      Left            =   5745
      TabIndex        =   41
      Top             =   105
      Width           =   1065
      Begin Threed.SSCommand Cmd_Botones 
         Height          =   735
         HelpContextID   =   120
         Index           =   1
         Left            =   120
         TabIndex        =   43
         ToolTipText     =   "Salir"
         Top             =   1140
         Width           =   855
         _Version        =   65536
         _ExtentX        =   1508
         _ExtentY        =   1296
         _StockProps     =   78
         Caption         =   "&SALIR"
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   6
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Picture         =   "FrmParam.frx":058A
      End
      Begin Threed.SSCommand Cmd_Botones 
         Height          =   750
         HelpContextID   =   120
         Index           =   0
         Left            =   105
         TabIndex        =   42
         ToolTipText     =   "Guardar"
         Top             =   270
         Width           =   855
         _Version        =   65536
         _ExtentX        =   1508
         _ExtentY        =   1323
         _StockProps     =   78
         Caption         =   "&GUARDAR"
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   6
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Picture         =   "FrmParam.frx":0C54
      End
   End
   Begin TabDlg.SSTab SstParam 
      Height          =   4110
      Left            =   75
      TabIndex        =   5
      Top             =   825
      Width           =   5565
      _ExtentX        =   9816
      _ExtentY        =   7250
      _Version        =   393216
      Tabs            =   4
      TabsPerRow      =   4
      TabHeight       =   882
      TabCaption(0)   =   "Inventarios"
      TabPicture(0)   =   "FrmParam.frx":131E
      Tab(0).ControlEnabled=   -1  'True
      Tab(0).Control(0)=   "FraGeneral"
      Tab(0).Control(0).Enabled=   0   'False
      Tab(0).ControlCount=   1
      TabCaption(1)   =   "Interfaz  Presupuesto"
      TabPicture(1)   =   "FrmParam.frx":133A
      Tab(1).ControlEnabled=   0   'False
      Tab(1).Control(0)=   "FramPpto"
      Tab(1).ControlCount=   1
      TabCaption(2)   =   "Interfaz Contable"
      TabPicture(2)   =   "FrmParam.frx":1356
      Tab(2).ControlEnabled=   0   'False
      Tab(2).Control(0)=   "ChkLastMesOpen"
      Tab(2).Control(1)=   "FramContable"
      Tab(2).ControlCount=   2
      TabCaption(3)   =   "Interfaz  CxP"
      TabPicture(3)   =   "FrmParam.frx":1372
      Tab(3).ControlEnabled=   0   'False
      Tab(3).Control(0)=   "FramCxP"
      Tab(3).ControlCount=   1
      Begin VB.CheckBox ChkLastMesOpen 
         Caption         =   "Utilizar �ltimo mes abierto"
         Height          =   255
         Left            =   -74160
         TabIndex        =   38
         Top             =   2520
         Width           =   2175
      End
      Begin VB.Frame FramCxP 
         Height          =   1695
         Left            =   -74520
         TabIndex        =   39
         Top             =   960
         Width           =   4695
         Begin VB.ComboBox CmbDepeCxP 
            Height          =   315
            Left            =   1440
            Style           =   2  'Dropdown List
            TabIndex        =   40
            Top             =   720
            Width           =   2775
         End
         Begin VB.Label Label1 
            Caption         =   "Dependencia"
            Height          =   255
            Index           =   4
            Left            =   240
            TabIndex        =   50
            Top             =   720
            Width           =   1095
         End
      End
      Begin VB.Frame FramContable 
         Height          =   1170
         Left            =   -74175
         TabIndex        =   34
         Top             =   1185
         Width           =   3600
         Begin VB.Label LblParCont 
            Caption         =   "Cuentas por &Grupo de Articulos y Dependencia"
            Height          =   195
            Index           =   2
            Left            =   120
            TabIndex        =   37
            Top             =   810
            Width           =   3330
         End
         Begin VB.Label LblParCont 
            Caption         =   "C&uentas por Movimiento"
            Height          =   195
            Index           =   1
            Left            =   120
            TabIndex        =   36
            Top             =   510
            Width           =   3330
         End
         Begin VB.Label LblParCont 
            Caption         =   "&Cuentas Generales"
            Height          =   195
            Index           =   0
            Left            =   120
            TabIndex        =   35
            Top             =   210
            Width           =   3330
         End
      End
      Begin VB.Frame FraGeneral 
         Height          =   3270
         Left            =   90
         TabIndex        =   6
         Top             =   585
         Width           =   5325
         Begin VB.CheckBox ChkImpAntSCon 
            Caption         =   "Imprimir documento antes de siguiente consecutivo"
            Height          =   195
            Left            =   120
            TabIndex        =   18
            Top             =   2880
            Width           =   4815
         End
         Begin VB.CheckBox ChkImpComp 
            Caption         =   "Causar impuestos �nicamente en la compra"
            Height          =   495
            Left            =   120
            TabIndex        =   17
            Top             =   2280
            Width           =   2415
         End
         Begin VB.CheckBox chkLisPrecio 
            Caption         =   "Modifica listas de precio autom�ticamente"
            Height          =   375
            Left            =   2640
            TabIndex        =   22
            Top             =   2280
            Width           =   2550
         End
         Begin VB.CheckBox ChkExAutOrden 
            Caption         =   "Exigir Autorizacion de Orden de Compra"
            Height          =   375
            Left            =   2625
            TabIndex        =   20
            Top             =   1320
            Width           =   2550
         End
         Begin VB.Frame FramDecimales 
            Caption         =   "Mostrar decimales En..."
            Height          =   1335
            Left            =   180
            TabIndex        =   10
            Top             =   795
            Width           =   2235
            Begin VB.TextBox TxtDecVal 
               Height          =   315
               Left            =   1440
               MaxLength       =   1
               TabIndex        =   12
               Top             =   240
               Width           =   630
            End
            Begin VB.TextBox TxtDecPorc 
               Height          =   315
               Left            =   1440
               MaxLength       =   1
               TabIndex        =   16
               Top             =   900
               Width           =   630
            End
            Begin VB.TextBox TxtDecCant 
               Height          =   315
               Left            =   1440
               MaxLength       =   1
               TabIndex        =   14
               Top             =   570
               Width           =   630
            End
            Begin VB.CheckBox ChkDecPorc 
               Caption         =   "Porcentajes"
               Height          =   315
               Left            =   195
               TabIndex        =   15
               Top             =   900
               Width           =   1215
            End
            Begin VB.CheckBox ChkDecCant 
               Caption         =   "Cantidades"
               Height          =   315
               Left            =   195
               TabIndex        =   13
               Top             =   570
               Width           =   1125
            End
            Begin VB.CheckBox ChkDecVal 
               Caption         =   "Valores"
               Height          =   315
               Left            =   195
               TabIndex        =   11
               Top             =   240
               Width           =   885
            End
         End
         Begin VB.TextBox TxtPara 
            Height          =   285
            Index           =   1
            Left            =   2295
            MaxLength       =   4
            TabIndex        =   9
            Top             =   360
            Width           =   615
         End
         Begin VB.TextBox TxtPara 
            Height          =   285
            Index           =   0
            Left            =   1935
            MaxLength       =   2
            TabIndex        =   8
            Top             =   360
            Width           =   375
         End
         Begin VB.CheckBox ChkDepe 
            Caption         =   "Validar las dependencia seg�n el usuario"
            Height          =   375
            Left            =   2625
            TabIndex        =   21
            Top             =   1815
            Width           =   2550
         End
         Begin VB.CheckBox ChkExAuto 
            Caption         =   "Exigir Orden de Compra para Ingresos  a almacen"
            Height          =   375
            Left            =   2625
            TabIndex        =   19
            Top             =   825
            Width           =   2550
         End
         Begin VB.Label LblMarca 
            Caption         =   " mm  / aaaa"
            Height          =   255
            Left            =   1935
            TabIndex        =   48
            Top             =   120
            Width           =   975
         End
         Begin VB.Label LblFecha 
            Caption         =   "Fecha de Ultimo Cierre :"
            Height          =   255
            Left            =   165
            TabIndex        =   7
            Top             =   360
            Width           =   1815
         End
      End
      Begin VB.Frame FramPpto 
         Height          =   3060
         Left            =   -74865
         TabIndex        =   23
         Top             =   675
         Width           =   5265
         Begin VB.CheckBox ChkModValEntDep 
            Caption         =   "Modificar valores de entrada dependiente"
            Height          =   195
            Left            =   240
            TabIndex        =   33
            Top             =   2760
            Width           =   3375
         End
         Begin VB.ComboBox CmbDepe 
            Height          =   315
            Left            =   1560
            Style           =   2  'Dropdown List
            TabIndex        =   26
            Top             =   960
            Width           =   2775
         End
         Begin VB.ComboBox CmbOrde 
            Height          =   315
            Left            =   1575
            Style           =   2  'Dropdown List
            TabIndex        =   24
            Top             =   240
            Width           =   2775
         End
         Begin VB.ComboBox CmbContrato 
            Height          =   315
            Left            =   1575
            Style           =   2  'Dropdown List
            TabIndex        =   25
            Top             =   585
            Width           =   2775
         End
         Begin VB.TextBox Txt_Articulo 
            Height          =   285
            Index           =   0
            Left            =   1575
            MaxLength       =   20
            TabIndex        =   27
            Top             =   1305
            Width           =   1935
         End
         Begin VB.TextBox Txt_Articulo 
            Height          =   285
            Index           =   1
            Left            =   1575
            MaxLength       =   20
            TabIndex        =   30
            Top             =   2025
            Width           =   1935
         End
         Begin Threed.SSCommand CmdPpto 
            Height          =   375
            Index           =   0
            Left            =   3360
            TabIndex        =   28
            ToolTipText     =   "Selecci�n de Cuenta"
            Top             =   1200
            Width           =   615
            _Version        =   65536
            _ExtentX        =   1085
            _ExtentY        =   661
            _StockProps     =   78
            BevelWidth      =   0
            RoundedCorners  =   0   'False
            Outline         =   0   'False
            Picture         =   "FrmParam.frx":138E
         End
         Begin Threed.SSCommand CmdPpto 
            Height          =   375
            Index           =   1
            Left            =   3480
            TabIndex        =   31
            ToolTipText     =   "Selecci�n de Cuenta"
            Top             =   1920
            Width           =   615
            _Version        =   65536
            _ExtentX        =   1085
            _ExtentY        =   661
            _StockProps     =   78
            BevelWidth      =   0
            RoundedCorners  =   0   'False
            Outline         =   0   'False
            Picture         =   "FrmParam.frx":1D40
         End
         Begin VB.Label Label1 
            Caption         =   "Dependencia"
            Height          =   255
            Index           =   3
            Left            =   120
            TabIndex        =   49
            Top             =   960
            Width           =   1215
         End
         Begin VB.Label LblOrde 
            Caption         =   "Ordenador del Gasto"
            Height          =   375
            Left            =   135
            TabIndex        =   47
            Top             =   240
            Width           =   1215
         End
         Begin VB.Label Label1 
            Caption         =   "Tipo de Contrato"
            Height          =   255
            Index           =   0
            Left            =   135
            TabIndex        =   46
            Top             =   585
            Width           =   1215
         End
         Begin VB.Label Label1 
            Caption         =   "Art�culo Gastos Fletes:"
            Height          =   375
            Index           =   2
            Left            =   135
            TabIndex        =   45
            Top             =   1305
            Width           =   1215
         End
         Begin VB.Label lbl_articulo 
            BackColor       =   &H8000000E&
            BorderStyle     =   1  'Fixed Single
            Height          =   285
            Index           =   0
            Left            =   1575
            TabIndex        =   29
            Top             =   1665
            Width           =   2895
         End
         Begin VB.Label lbl_articulo 
            BackColor       =   &H8000000E&
            BorderStyle     =   1  'Fixed Single
            Height          =   285
            Index           =   1
            Left            =   1575
            TabIndex        =   32
            Top             =   2400
            Width           =   2895
         End
         Begin VB.Label Label1 
            Caption         =   "Art�culo Ingresos Fletes:"
            Height          =   375
            Index           =   1
            Left            =   135
            TabIndex        =   44
            Top             =   2025
            Width           =   1215
         End
      End
   End
End
Attribute VB_Name = "FrmParam"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Dim CodCont(), CodOrde(), CodDepe(), CodDepeCxP()
Dim Existe As Boolean
Public OpcCod        As String   'Opci�n de seguridad

Private Sub ChkDecCant_Click()
  If Me.ChkDecCant.Value = vbChecked Then Me.TxtDecCant.Enabled = True Else Me.TxtDecCant.Enabled = False
End Sub

Private Sub ChkDecPorc_Click()
  If Me.ChkDecPorc.Value = vbChecked Then Me.TxtDecPorc.Enabled = True Else Me.TxtDecPorc.Enabled = False
End Sub

Private Sub ChkDecVal_Click()
  If Me.ChkDecVal.Value = vbChecked Then Me.TxtDecVal.Enabled = True Else Me.TxtDecVal.Enabled = False
End Sub
'AASV R1883
Private Sub ChkImpAntSCon_KeyPress(KeyAscii As Integer)
Call Cambiar_Enter(KeyAscii)
End Sub
'HRR R1873
Private Sub ChkLastMesOpen_KeyPress(KeyAscii As Integer)
   Call Cambiar_Enter(KeyAscii)
End Sub
'HRR R1873

Private Sub ChkModulo_Click(Index As Integer)
Select Case Index
  Case 0:
          If ChkModulo(0).Value = vbChecked Then
            SstParam.TabEnabled(2) = True
            SstParam.Tab = 2
          Else
            SstParam.TabEnabled(2) = False
            SstParam.Tab = 0
          End If
  Case 3:
          If ChkModulo(3).Value = vbChecked Then
            SstParam.TabEnabled(1) = True
            SstParam.Tab = 1
            Call Cargar_Tablas
            If CmbContrato.ListCount > 0 Then CmbContrato.ListIndex = 0
            If CmbOrde.ListCount > 0 Then CmbOrde.ListIndex = 0
            If CmbDepe.ListCount > 0 Then CmbDepe.ListIndex = 0
          Else
            SstParam.TabEnabled(1) = False
            SstParam.Tab = 0
          End If
  Case 2:
          If ChkModulo(2).Value = vbChecked Then
            SstParam.TabEnabled(3) = True
            SstParam.Tab = 3
            Result = loadctrl("CENTRO_COSTO ORDER BY NO_NOMB_CECO", "NO_NOMB_CECO", "CD_CODI_CECO", CmbDepeCxP, CodDepeCxP(), NUL$)
            If CmbDepeCxP.ListCount > 0 Then CmbDepeCxP.ListIndex = 0
          Else
            SstParam.TabEnabled(3) = False
            SstParam.Tab = 0
          End If
End Select
End Sub
Private Sub CmbContrato_KeyPress(KeyAscii As Integer)
    Call Cambiar_Enter(KeyAscii)
End Sub

Private Sub CmbDepe_KeyPress(KeyAscii As Integer)
    Call Cambiar_Enter(KeyAscii)
End Sub

Private Sub CmbOrde_KeyPress(KeyAscii As Integer)
    Call Cambiar_Enter(KeyAscii)
End Sub

Private Sub CmdPpto_Click(Index As Integer)
  Codigo = NUL$
    If Index = 0 Then
       Codigo = SelNivel("GASTOS", "DE_DESC_GAST", "CD_CODI_GAST,DE_DESC_GAST", "ARTICULOS DE GASTOS", NUL$)
    Else
       Codigo = SelNivel("INGRESOS", "DE_DESC_INGR", "CD_CODI_INGR,DE_DESC_INGR", "ARTICULOS DE INGRESOS", NUL$)
    End If
    If Codigo <> NUL$ Then Txt_Articulo(Index) = Codigo
    Txt_Articulo_LostFocus (Index)
    Txt_Articulo(Index).SetFocus
End Sub

'ALTER TABLE [PARAMETROS_INVE] ADD [ID_UTILID_PINV] [VARCHAR] (1) NULL
Private Sub Form_Load()
  lnformaactiva = Me.hwnd 'JJRG 15639
  Call CenterForm(MDI_Inventarios, Me)
  'Cmd_Botones(0).Enabled = Leer_Permiso("04001", "ID_CREA_PERM")
  'INICIO AFMG T21584 SE DEJA EN COMENTARIO
  'Call Leer_Permiso("FRMPARAM", Cmd_Botones(0), "G")
  'If Cmd_Botones(0).Enabled = False Then MsgBox "Permiso denegado,consulte al administrador", vbExclamation + vbOKOnly: Result = FAIL: Exit Sub
  'FIN AFMG T21584
  Call Cargar_Datos
  Me.SstParam.Tab = 0
End Sub

Private Sub Form_Unload(Cancel As Integer)
  lnformaactiva = 0 'jjrg 15639
  Aplicacion.CargarParametrosAplicacion
End Sub

Private Sub FramContable_MouseMove(Button As Integer, Shift As Integer, x As Single, Y As Single)
Dim lblCtrl As control
  For Each lblCtrl In Me.Controls
    If lblCtrl.Name = "LblParCont" Then
      lblCtrl.ForeColor = vbMenuText
      lblCtrl.BackColor = vbMenuBar
    End If
  Next
End Sub


Private Sub LblParCont_Click(Index As Integer)
Select Case Index
  Case 0: FrmCuGE.Show
  Case 1: FrmParco.Show
  Case 2: FrmGrupCuen.Show
End Select
End Sub

Private Sub LblParCont_MouseMove(Index As Integer, Button As Integer, Shift As Integer, x As Single, Y As Single)
  CambiarSel LblParCont(Index)
End Sub

Private Sub Txt_Articulo_GotFocus(Index As Integer)
    Txt_Articulo(Index).SelStart = 0
    Txt_Articulo(Index).SelLength = Len(Txt_Articulo(Index))
End Sub
Private Sub Txt_Articulo_KeyPress(Index As Integer, KeyAscii As Integer)
    Call Cambiar_Enter(KeyAscii)
End Sub
Private Sub Txt_Articulo_LostFocus(Index As Integer)
    Txt_Articulo(Index) = Trim(Txt_Articulo(Index))
    If Txt_Articulo(Index) = NUL$ Then
       lbl_articulo(Index) = NUL$
    Else
        Call Buscar_Articulo(Index)
    End If
End Sub
Private Sub Buscar_Articulo(Index As Integer)
ReDim Arr(0)
    If Txt_Articulo(Index) = NUL$ Then lbl_articulo(Index) = NUL$: Exit Sub
    If Index = 0 Then
       Result = LoadData("GASTOS", "DE_DESC_GAST", "CD_CODI_GAST=" & Comi & Txt_Articulo(Index) & Comi, Arr())
    Else
       Result = LoadData("INGRESOS", "DE_DESC_INGR", "CD_CODI_INGR=" & Comi & Txt_Articulo(Index) & Comi, Arr())
    End If
    lbl_articulo(Index) = Arr(0)
    If Arr(0) <> NUL$ Then
        If Valida_Nivel_Articulo(Txt_Articulo(Index), CByte(Index)) Then
           Call Mensaje1("Verifique que la Articulo sea de �ltimo nivel!", 2)
           Txt_Articulo(Index).SetFocus
        End If
    Else
        Call Mensaje1("Articulo no registrado !", 2)
        Txt_Articulo(Index) = NUL$
    End If
End Sub

Private Sub TxtDecCant_KeyPress(KeyAscii As Integer)
    Call ValKeyNum(KeyAscii)    'LJSA M3958
    
End Sub

Private Sub TxtDecCant_LostFocus()
    'INICIO JAGS T9979 PNC
    If TxtDecCant > "2" Then
            TxtDecCant.Text = "2"
    End If
    'FIN JAGS T9979 PNC
End Sub

Private Sub TxtDecPorc_KeyPress(KeyAscii As Integer)
    Call ValKeyNum(KeyAscii)    'LJSA M3958
End Sub

Private Sub TxtDecPorc_LostFocus()
    'INICIO JAGS T9979
    If TxtDecPorc > "2" Then
            TxtDecPorc.Text = "2"
    End If
    'FIN JAGS T9979
End Sub

Private Sub TxtDecVal_KeyPress(KeyAscii As Integer)
    Call ValKeyNum(KeyAscii)    'LJSA M3958
End Sub
Private Sub TxtDecVal_LostFocus()
    'INICIO JAGS T9979
    If TxtDecVal > "2" Then
            TxtDecVal.Text = "2"
    End If
    'FIN JAGS T9979
End Sub

Private Sub TxtPara_GotFocus(Index As Integer)
    TxtPara(Index).SelStart = 0
    TxtPara(Index).SelLength = Len(TxtPara(Index))
    
End Sub
Private Sub TxtPara_KeyPress(Index As Integer, KeyAscii As Integer)
    Call ValKeyNum(KeyAscii)
    Call Cambiar_Enter(KeyAscii)
End Sub
Private Sub ChkModulo_KeyPress(Index As Integer, KeyAscii As Integer)
    Call Cambiar_Enter(KeyAscii)
End Sub
Private Sub Cmd_Botones_Click(Index As Integer)
    Select Case Index
        Case 0: Call Grabar
'                MDI_Inventarios.SubParaCont.Enabled = Buscar_Integracion("ID_CONT_APLI")
        Case 1: lnformaactiva = 0: Unload Me 'jjrg 15639
    End Select
End Sub
Private Sub Cargar_Datos()
   Dim CantDec As Integer
   ReDim Arr(10)
   Dim i As Integer
   Existe = Aplicacion.Exist_Reg_Parametros
   TxtPara(0) = Format(Aplicacion.Fecha_Ult_Cierre, "mm")
   TxtPara(1) = Format(Aplicacion.Fecha_Ult_Cierre, "yyyy")

   ChkModulo(0).Value = IIf(Aplicacion.Interfaz_Contabilidad, vbChecked, vbUnchecked)
   ChkModulo(1).Value = IIf(Aplicacion.Interfaz_CxC, vbChecked, vbUnchecked)
   ChkModulo(2).Value = IIf(Aplicacion.Interfaz_CxP, vbChecked, vbUnchecked)
   ChkModulo(3).Value = IIf(Aplicacion.Interfaz_Presupuesto, vbChecked, vbUnchecked)
   ChkDepe.Value = IIf(Aplicacion.ValDependencia_X_Usuario, vbChecked, vbUnchecked)
   ChkModulo_Click (0)
   ChkModulo_Click (2)
   ChkModulo_Click (3)
   
   'If Aplicacion.Ppto_Tipo_Contrato <> NUL$ And Val(Aplicacion.Ppto_Tipo_Contrato) <> 0 Then
   If Aplicacion.Ppto_Tipo_Contrato <> NUL$ Then               'PedroJ
      i = FindInArr(CodCont(), Aplicacion.Ppto_Tipo_Contrato)
   Else
      i = -1
   End If
   CmbContrato.ListIndex = i
   'If CStr(Aplicacion.Ppto_Ordenador) <> NUL$ And Aplicacion.Ppto_Ordenador <> 0 Then 'JAUM T33493-R32409 Se deja linea en comentario
   If CStr(Aplicacion.Ppto_Ordenador) <> NUL$ And Len(Aplicacion.Ppto_Ordenador) <> 0 Then 'JAUM T33493-R32409
      i = FindInArr(CodOrde(), CStr(Aplicacion.Ppto_Ordenador))
   Else
      i = -1
   End If
   CmbOrde.ListIndex = i
   If CStr(Aplicacion.Ppto_Dependencia) <> NUL$ Then
      i = FindInArr(CodDepe(), Aplicacion.Ppto_Dependencia)
   Else
      i = -1
   End If
   CmbDepe.ListIndex = i
   Txt_Articulo(0) = Aplicacion.Ppto_ArticuloGastos_Fletes
   Txt_Articulo_LostFocus (0)
   Txt_Articulo(1) = Aplicacion.Ppto_ArticuloIngresos_Fletes
   Txt_Articulo_LostFocus (1)
   ChkExAuto.Value = IIf(Aplicacion.Exigir_OrdenCompra_IngAlma, vbChecked, vbUnchecked)
   chkLisPrecio.Value = IIf(Aplicacion.Cambia_listas_Precio, vbChecked, vbUnchecked)
   Me.ChkDecCant.Value = IIf(Aplicacion.VerDecimales_Cantidades(CantDec), vbChecked, vbUnchecked)
   If Me.ChkDecCant.Value = vbChecked Then Me.TxtDecCant.Enabled = True Else Me.TxtDecCant.Enabled = False
   Me.TxtDecCant.Text = CStr(CantDec)
   Me.ChkDecVal.Value = IIf(Aplicacion.VerDecimales_Valores(CantDec), vbChecked, vbUnchecked)
   If Me.ChkDecVal.Value = vbChecked Then Me.TxtDecVal.Enabled = True Else Me.TxtDecVal.Enabled = False
   Me.TxtDecVal.Text = CStr(CantDec)
   Me.ChkDecPorc.Value = IIf(Aplicacion.VerDecimales_Porcentajes(CantDec), vbChecked, vbUnchecked)
   If Me.ChkDecPorc.Value = vbChecked Then Me.TxtDecPorc.Enabled = True Else Me.TxtDecPorc.Enabled = False
   Me.TxtDecPorc.Text = CStr(CantDec)
   Me.ChkExAutOrden.Value = IIf(Aplicacion.Exigir_AutOrdCompra_IngAlma, vbChecked, vbUnchecked)
   Me.chkLisPrecio.Value = IIf(Aplicacion.Cambia_listas_Precio, vbChecked, vbUnchecked)
   Me.ChkModValEntDep.Value = IIf(Aplicacion.ModValEntDep, vbChecked, vbUnchecked) 'HRR Req1553
   Me.ChkImpComp.Value = IIf(Aplicacion.PinvImpComp, 1, 0) 'HRR R1853
   Me.ChkImpComp.Tag = IIf(Aplicacion.PinvImpComp, 1, 0) 'HRR R1853
   Me.ChkLastMesOpen.Value = IIf(Aplicacion.PinvLastMesOpen, 1, 0) 'HRR R1873
   Me.ChkLastMesOpen.Tag = IIf(Aplicacion.PinvLastMesOpen, 1, 0) 'HRR R1873
   ChkImpAntSCon.Value = IIf(Aplicacion.PinvImpAntSCon, vbChecked, vbUnchecked) 'AASV R1883
   
       
   If CStr(Aplicacion.CxP_Dependencia) <> NUL$ Then
      i = FindInArr(CodDepeCxP(), Aplicacion.CxP_Dependencia)
   Else
      i = -1
   End If
   CmbDepeCxP.ListIndex = i
    
    
'    End If
End Sub
Private Sub Grabar()
   Dim fechag As Variant
   Dim Mensaje As String
   DoEvents
   Call MouseClock
   Msglin "Ingresando informaci�n al Archivo de PARAMETROS"
    
   If TxtPara(0) = NUL$ Or TxtPara(1) = NUL$ Then
      Call Mensaje1("Debe especificar la fecha de �ltimo cierre!!!", 3)
      Call MouseNorm: Exit Sub
   End If
   'SKRV t15236
   If TxtPara(0) > 12 Then
      Call Mensaje1("El numero de mes no es valido!!!", 3)
      Call MouseNorm: Exit Sub
   End If
   
   fechag = "01/" & TxtPara(0) & "/" & TxtPara(1)
   fechag = DateAdd("M", 1, fechag)
   fechag = DateAdd("d", -1, fechag)
   fechag = Format(fechag, FormatF)    'REOL M2222
   If IsDate(fechag) = False Then
      Call Mensaje1("Fecha de �ltimo cierre incorrecta!!!", 3)
      Call MouseNorm: Exit Sub
   End If
   
   If ChkModulo(2).Value = 1 Then
      If CmbDepeCxP.ListIndex = -1 Then
         Call Mensaje1("Debe seleccionar la dependencia para CxP !!!", 3)
         Call MouseNorm: Exit Sub
      End If
   End If
   
   If ChkModulo(3).Value = 1 Then
      If CmbContrato.ListIndex = -1 Or CmbOrde.ListIndex = -1 Or CmbDepe.ListIndex = -1 Then
         Call Mensaje1("Debe seleccionar ordenador, contrato y dependencia !!!", 3)
         Call MouseNorm: Exit Sub
      End If
   End If
   
   'HRR R1853
   If ChkImpComp.Value <> ChkImpComp.Tag Then
      ReDim Arr(0)
      Result = LoadData("VW_ENTRSINCOMP", "COUNT(NU_AUTO_ENCA)", NUL$, Arr)
      If Result <> FAIL And Encontro Then
         If Arr(0) <> NUL$ Then
            If Arr(0) > 0 Then
               'Call Mensaje1("No se puede cambiar la opci�n ''causar impuestos unicamente en la compra''" & vbCrLf & "porque existen entradas sin compra asociada, y se pueden presentar inconsistencias !!!", 2) 'Se Comenta el codigo 'JSCR T29856
               Call Mensaje1("No se puede cambiar la opci�n ''causar impuestos �nicamente en la compra''" & vbCrLf & "porque existen entradas sin compra asociada, y se pueden presentar inconsistencias !!!", 2) 'JSCR T29856
               Call MouseNorm: Exit Sub
            End If
         End If
      End If
   End If
   'HRR R1853
    
   If (BeginTran(STranIUp & "PARAMETROS_INVE") <> FAIL) Then
      Valores = "FE_CIER_APLI=" & Comi & fechag & Comi
      Valores = Valores & Coma & "ID_CONT_APLI=" & Comi & IIf(ChkModulo(0).Value = 1, "S", "N") & Comi
      Valores = Valores & Coma & "ID_PRES_APLI=" & Comi & IIf(ChkModulo(3).Value = 1, "S", "N") & Comi
      Valores = Valores & Coma & "ID_PRED_APLI=" & Comi & "N" & Comi
      Valores = Valores & Coma & "ID_INCO_APLI=" & Comi & "N" & Comi
      Valores = Valores & Coma & "ID_SEPU_APLI=" & Comi & "N" & Comi
      Valores = Valores & Coma & "ID_PACI_APLI=" & Comi & "N" & Comi
      Valores = Valores & Coma & "ID_NOMI_APLI=" & Comi & "N" & Comi
      Valores = Valores & Coma & "ID_ACTI_APLI=" & Comi & "N" & Comi
      Valores = Valores & Coma & "ID_CXC_APLI=" & Comi & IIf(ChkModulo(1).Value = 1, "S", "N") & Comi
      Valores = Valores & Coma & "ID_CXP_APLI=" & Comi & IIf(ChkModulo(2).Value = 1, "S", "N") & Comi
      Valores = Valores & Coma & "ID_DEUS_APLI=" & IIf(ChkDepe.Value = 1, "'S'", "'N'")
      If ChkModulo(3).Value = 1 Then
         Valores = Valores & Coma & "CD_CONT_APLI=" & Comi & CodCont(CmbContrato.ListIndex) & Comi
         'Valores = Valores & Coma & "CD_ORDE_APLI=" & CodOrde(CmbOrde.ListIndex) 'JAUM T33493-R32409 Se deja linea en comentario
         'Valores = Valores & Coma & "CD_ORDE_APLI = '" & CodOrde(CmbOrde.ListIndex) & "'" 'JAUM T33493-R32409 'JAUM T33864 se deja linea en comentario
         Valores = Valores & Coma & "CD_ORDE_APLI = '" & CodOrde(CmbOrde.ListIndex) & Comi 'JAUM T33864
         Valores = Valores & Coma & "CD_DEPE_APLI=" & Comi & CodDepe(CmbDepe.ListIndex) & Comi
        
      Else
         Valores = Valores & Coma & "CD_CONT_APLI=" & Comi & NUL$ & Comi
         'Valores = Valores & Coma & "CD_ORDE_APLI=" & SCero 'JAUM T33493-R32409 Se deja linea en comentario
         Valores = Valores & Coma & "CD_ORDE_APLI = NULL" 'JAUM T33493-R32409
         Valores = Valores & Coma & "CD_DEPE_APLI=" & Comi & NUL$ & Comi
      End If
      'HRR Req1553
      Valores = Valores & Coma & "ID_ENTDEP_PINV = " & Comi & IIf(Me.ChkModValEntDep.Value = vbChecked, "S", "N") & Comi
      'Req1553
      Valores = Valores & Coma & "CD_ARFL_APLI=" & Comi & Txt_Articulo(0) & Comi
      Valores = Valores & Coma & "CD_FLIN_APLI=" & Comi & Txt_Articulo(1) & Comi
      Valores = Valores & Coma & "ID_NOAU_PINV=" & Comi & IIf(ChkExAuto.Value = vbChecked, "S", "N") & Comi
      Valores = Valores & Coma & "ID_DECVAL_PINV = " & Comi & IIf(Me.ChkDecVal.Value = vbChecked, "S", "N") & Comi
      If Me.ChkDecVal.Value = vbChecked Then Valores = Valores & Coma & "NU_DECVAL_PINV = " & Val(Me.TxtDecVal.Text)
      Valores = Valores & Coma & "ID_DECCANT_PINV = " & Comi & IIf(Me.ChkDecCant.Value = vbChecked, "S", "N") & Comi
      If Me.ChkDecCant.Value = vbChecked Then Valores = Valores & Coma & "NU_DECCANT_PINV = " & Val(Me.TxtDecCant.Text)
      Valores = Valores & Coma & "ID_DECPORC_PINV = " & Comi & IIf(Me.ChkDecPorc.Value = vbChecked, "S", "N") & Comi
      If Me.ChkDecPorc.Value = vbChecked Then Valores = Valores & Coma & "NU_DECPORC_PINV = " & Val(Me.TxtDecPorc.Text)
      Valores = Valores & Coma & "ID_AUTORCO_PINV = " & Comi & IIf(Me.ChkExAutOrden.Value = vbChecked, "S", "N") & Comi
      Valores = Valores & Coma & "ID_UTILID_PINV = " & Comi & IIf(Me.chkLisPrecio.Value = vbChecked, "S", "N") & Comi
      
      
      If Me.ChkModulo(2).Value = 1 Then
         Valores = Valores & Coma & "CD_DEPE_CXP_APLI= " & Comi & CodDepeCxP(CmbDepeCxP.ListIndex) & Comi
      Else
         Valores = Valores & Coma & "CD_DEPE_CXP_APLI=" & Comi & NUL$ & Comi
      End If
      
      'HRR R1853
      Valores = Valores & Coma & "TX_IMPCOMP_PINV=" & Comi & IIf(ChkImpComp.Value = vbChecked, "1", "0") & Comi
      'HRR R1853
      
      'HRR R1873
      Valores = Valores & Coma & "TX_MESOPEN_APLI=" & Comi & IIf(ChkLastMesOpen.Value = vbChecked, "1", "0") & Comi
      'HRR R1873
      'AASV R1883
      Valores = Valores & Coma & "TX_IMPDOC_PINV = " & Comi & IIf(ChkImpAntSCon.Value = vbChecked, "S", "N") & Comi
      'AASV R1883
      If (Not Existe) Then
         Result = DoInsertSQL("PARAMETROS_INVE", Valores)
         'If (Result <> FAIL) Then Result = Auditor("PARAMETROS_INVE", TranIns, LastCmd)
         'NYCM R1759-------------------------
         If (Result <> FAIL) Then
            Result = Auditor("PARAMETROS_INVE", TranIns, LastCmd)
            If ChkModulo(0).Value = 1 Then
               Mensaje = ""
               Mensaje = "Activo la interfase a Contabilidad"
               Result = Auditor("PARAMETROS_INVE", TranIns, Mensaje)
            ElseIf ChkModulo(0).Value = 0 Then
               Mensaje = ""
               Mensaje = Mensaje & "Desactivo la interfase a Contabilidad"
               Result = Auditor("PARAMETROS_INVE", TranIns, Mensaje)
            End If
            If ChkModulo(1).Value = 1 Then
               Mensaje = ""
               Mensaje = "Activo la interfase a Cartera"
               Result = Auditor("PARAMETROS_INVE", TranIns, Mensaje)
            ElseIf ChkModulo(1).Value = 0 Then
               Mensaje = ""
               Mensaje = Mensaje & "Desactivo la interfase a Cartera"
               Result = Auditor("PARAMETROS_INVE", TranIns, Mensaje)
            End If
            If ChkModulo(2).Value = 1 Then
               Mensaje = ""
               Mensaje = "Activo la interfase a CxP"
               Result = Auditor("PARAMETROS_INVE", TranIns, Mensaje)
            ElseIf ChkModulo(2).Value = 0 Then
               Mensaje = ""
               Mensaje = "Desactivo la interfase a CxP"
               Result = Auditor("PARAMETROS_INVE", TranIns, Mensaje)
            End If
            If ChkModulo(3).Value = 1 Then
               Mensaje = ""
               Mensaje = "Activo la interfase a Presupuesto"
               Result = Auditor("PARAMETROS_INVE", TranIns, Mensaje)
            ElseIf ChkModulo(3).Value = 0 Then
               Mensaje = ""
               Mensaje = "Desactivo la interfase a Presupuesto"
               Result = Auditor("PARAMETROS_INVE", TranIns, Mensaje)
            End If
           
           
           
         End If
        '-----------------------------------
      Else
         If WarnMsg("Desea Modificar los Par�metros de la Aplicaci�n ? ", 256) Then
            Result = DoUpdate("PARAMETROS_INVE", Valores, NUL$)
            ' If Result <> FAIL Then Result = Auditor("PARAMETROS_INVE", TranUpd, LastCmd)
            'NYCM R1759 -------------------------
            If Result <> FAIL Then
               Result = Auditor("PARAMETROS_INVE", TranUpd, LastCmd)
               If Result = FAIL Then: Call RollBackTran: Call MouseNorm: Exit Sub 'AFMG T21584
               If ChkModulo(0).Value = 1 Then
                  Mensaje = ""
                  Mensaje = "Activo la interfase a Contabilidad"
                  Result = Auditor("PARAMETROS_INVE", TranIns, Mensaje)
               ElseIf ChkModulo(0).Value = 0 Then
                  Mensaje = ""
                  Mensaje = Mensaje & "Desactivo la interfase a Contabilidad"
                  Result = Auditor("PARAMETROS_INVE", TranIns, Mensaje)
               End If
               If ChkModulo(1).Value = 1 Then
                  Mensaje = ""
                  Mensaje = "Activo la interfase a Cartera"
                  Result = Auditor("PARAMETROS_INVE", TranIns, Mensaje)
               ElseIf ChkModulo(1).Value = 0 Then
                  Mensaje = ""
                  Mensaje = Mensaje & "Desactivo la interfase a Cartera"
                  Result = Auditor("PARAMETROS_INVE", TranIns, Mensaje)
               End If
               If ChkModulo(2).Value = 1 Then
                  Mensaje = ""
                  Mensaje = "Activo la interfase a CxP"
                  Result = Auditor("PARAMETROS_INVE", TranIns, Mensaje)
               ElseIf ChkModulo(2).Value = 0 Then
                  Mensaje = ""
                  Mensaje = "Desactivo la interfase a CxP"
                  Result = Auditor("PARAMETROS_INVE", TranIns, Mensaje)
               End If
               If ChkModulo(3).Value = 1 Then
                  Mensaje = ""
                  Mensaje = "Activo la interfase a Presupuesto"
                  Result = Auditor("PARAMETROS_INVE", TranIns, Mensaje)
               ElseIf ChkModulo(3).Value = 0 Then
                  Mensaje = ""
                  Mensaje = "Desactivo la interfase a Presupuesto"
                  Result = Auditor("PARAMETROS_INVE", TranIns, Mensaje)
               End If
            End If
            '/---------------------------------
         End If
      End If
      'HRR R1853
      If ChkImpComp.Value <> ChkImpComp.Tag And Result <> FAIL Then
         Mensaje = IIf(ChkImpComp.Value = vbChecked, "Activo causar impuestos unicamente en la compra", "Desactivo causar impuestos unicamente en la compra")
         Result = Auditor("PARAMETROS_INVE", TranIns, Mensaje)
      End If
      'HRR R1853
      'HRR R1873
      If ChkLastMesOpen.Value <> ChkLastMesOpen.Tag And Result <> FAIL Then
         Mensaje = IIf(ChkLastMesOpen.Value = vbChecked, "Interfaz contable - Activo Utilizar ultimo mes abierto", "Interfaz contable - Desactivo Utilizar ultimo mes abierto")
         Result = Auditor("PARAMETROS_INVE", TranIns, Mensaje)
      End If
      'HRR R1873
   End If
   If (Result <> FAIL) Then
      If (CommitTran() <> FAIL) Then
         If ChkModulo(3).Value = 1 Then
            OrdeSel = CodOrde(CmbOrde.ListIndex)
            Depesel = CodDepe(CmbDepe.ListIndex)
         Else
            OrdeSel = 0
            Depesel = ""
         End If
         If ChkModulo(2).Value = 1 Then
            DepeCxPsel = CodDepeCxP(CmbDepeCxP.ListIndex)
         Else
            DepeCxPsel = ""
         End If

         Unload Me
      Else
         Call RollBackTran
      End If
   Else
      Call RollBackTran
   End If
   Call MouseNorm
   Msglin NUL$
End Sub

Private Sub Cargar_Tablas()
    Result = loadctrl("ORDENADOR", "NO_NOMB_ORDE", "CD_CODI_ORDE", CmbOrde, CodOrde(), "TX_ESTA_ORDE = '0' ORDER BY NO_NOMB_ORDE")  'JACC R1310 2034
'    Result = loadctrl("ORDENADOR ORDER BY NO_NOMB_ORDE", "NO_NOMB_ORDE", "CD_CODI_ORDE", CmbOrde, CodOrde(), NUL$)
'    Result = loadctrl("DEPENDENCIA ORDER BY DE_DESC_DEPE", "DE_DESC_DEPE", "CD_CODI_DEPE", CmbDepe, CodDepe(), NUL$)
    Result = loadctrl("CENTRO_COSTO ORDER BY NO_NOMB_CECO", "NO_NOMB_CECO", "CD_CODI_CECO", CmbDepe, CodDepe(), NUL$)
    Result = loadctrl("CONTRATO ORDER BY DE_DESC_CONT", "DE_DESC_CONT", "CD_CODI_CONT", CmbContrato, CodCont(), NUL$)
End Sub

Private Sub CambiarSel(lbl As Label)
Dim l As control
   For Each l In Me.Controls
      If TypeOf l Is Label Then
      If l.Container.Name = lbl.Container.Name Then
         l.ForeColor = vbMenuText
         l.BackColor = vbMenuBar
      End If
      End If
   Next
   lbl.BackColor = vbHighlight
   lbl.ForeColor = vbHighlightText
End Sub

