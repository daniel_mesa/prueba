VERSION 5.00
Object = "{00025600-0000-0000-C000-000000000046}#5.2#0"; "CRYSTL32.OCX"
Object = "{C932BA88-4374-101B-A56C-00AA003668DC}#1.1#0"; "MSMASK32.OCX"
Object = "{0BA686C6-F7D3-101A-993E-0000C0EF6F5E}#1.0#0"; "THREED32.OCX"
Begin VB.Form FrmKuso 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "K�rdex Almac�n x Usos"
   ClientHeight    =   2955
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   3915
   Icon            =   "FrmKuso.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   ScaleHeight     =   2955
   ScaleWidth      =   3915
   Begin VB.CheckBox ChkDetallado 
      Caption         =   "Detallado"
      Height          =   255
      Left            =   120
      TabIndex        =   8
      Top             =   1560
      Width           =   1095
   End
   Begin VB.Frame FraFecha 
      Caption         =   "Fecha    (dd/mm/aaaa)"
      Height          =   855
      Left            =   0
      TabIndex        =   5
      Top             =   720
      Width           =   3855
      Begin MSMask.MaskEdBox MskFecha 
         Height          =   375
         Index           =   0
         Left            =   720
         TabIndex        =   6
         Top             =   360
         Width           =   1095
         _ExtentX        =   1931
         _ExtentY        =   661
         _Version        =   393216
         MaxLength       =   10
         Mask            =   "##/##/####"
         PromptChar      =   "_"
      End
      Begin MSMask.MaskEdBox MskFecha 
         Height          =   375
         Index           =   1
         Left            =   2640
         TabIndex        =   7
         Top             =   360
         Width           =   1095
         _ExtentX        =   1931
         _ExtentY        =   661
         _Version        =   393216
         MaxLength       =   10
         Mask            =   "##/##/####"
         PromptChar      =   "_"
      End
      Begin VB.Label LblFeFinal 
         Caption         =   "Final :"
         Height          =   255
         Left            =   2160
         TabIndex        =   12
         Top             =   360
         Width           =   495
      End
      Begin VB.Label LblFeIni 
         Caption         =   "Inicial :"
         Height          =   255
         Left            =   120
         TabIndex        =   11
         Top             =   360
         Width           =   495
      End
   End
   Begin VB.Frame FraUsos 
      Caption         =   "Rango de Usos de Art�culos"
      Height          =   855
      Left            =   0
      TabIndex        =   0
      Top             =   0
      Width           =   3855
      Begin VB.TextBox TxtRango 
         Height          =   285
         Index           =   1
         Left            =   2520
         MaxLength       =   4
         TabIndex        =   3
         Text            =   "ZZZZ"
         Top             =   360
         Width           =   615
      End
      Begin VB.TextBox TxtRango 
         Height          =   285
         Index           =   0
         Left            =   720
         MaxLength       =   4
         TabIndex        =   1
         Text            =   "0"
         Top             =   360
         Width           =   615
      End
      Begin Threed.SSCommand CmdSelec 
         Height          =   375
         Index           =   0
         Left            =   1200
         TabIndex        =   2
         ToolTipText     =   "Selecci�n de Usos de Art�culos"
         Top             =   240
         Width           =   615
         _Version        =   65536
         _ExtentX        =   1085
         _ExtentY        =   661
         _StockProps     =   78
         BevelWidth      =   0
         RoundedCorners  =   0   'False
         Outline         =   0   'False
         Picture         =   "FrmKuso.frx":058A
      End
      Begin Threed.SSCommand CmdSelec 
         Height          =   375
         Index           =   1
         Left            =   3000
         TabIndex        =   4
         ToolTipText     =   "Selecci�n de Usos de Art�culos"
         Top             =   240
         Width           =   615
         _Version        =   65536
         _ExtentX        =   1085
         _ExtentY        =   661
         _StockProps     =   78
         BevelWidth      =   0
         RoundedCorners  =   0   'False
         Outline         =   0   'False
         Picture         =   "FrmKuso.frx":0F3C
      End
      Begin VB.Label LblInicio 
         AutoSize        =   -1  'True
         Caption         =   "Inicial : "
         Height          =   195
         Left            =   120
         TabIndex        =   10
         Top             =   360
         Width           =   540
      End
      Begin VB.Label LblHasta 
         AutoSize        =   -1  'True
         Caption         =   "Final : "
         Height          =   195
         Left            =   2040
         TabIndex        =   9
         Top             =   360
         Width           =   465
      End
   End
   Begin Crystal.CrystalReport Crys_Listar 
      Left            =   0
      Top             =   840
      _ExtentX        =   741
      _ExtentY        =   741
      _Version        =   348160
      WindowLeft      =   20
      WindowWidth     =   750
      WindowHeight    =   450
      WindowControlBox=   -1  'True
      WindowMaxButton =   -1  'True
      WindowMinButton =   -1  'True
      WindowControls  =   -1  'True
      PrintFileLinesPerPage=   60
      WindowShowCloseBtn=   -1  'True
      WindowShowSearchBtn=   -1  'True
      WindowShowPrintSetupBtn=   -1  'True
      WindowShowRefreshBtn=   -1  'True
   End
   Begin Threed.SSCommand SCmd_Options 
      Height          =   375
      Index           =   3
      Left            =   960
      TabIndex        =   13
      Top             =   3000
      Visible         =   0   'False
      Width           =   975
      _Version        =   65536
      _ExtentX        =   1720
      _ExtentY        =   661
      _StockProps     =   78
   End
   Begin Threed.SSCommand SCmd_Options 
      Height          =   375
      Index           =   4
      Left            =   2040
      TabIndex        =   14
      Top             =   3120
      Visible         =   0   'False
      Width           =   975
      _Version        =   65536
      _ExtentX        =   1720
      _ExtentY        =   661
      _StockProps     =   78
   End
   Begin Threed.SSCommand SCmd_Options 
      Height          =   735
      Index           =   1
      Left            =   1560
      TabIndex        =   15
      Top             =   2160
      Width           =   855
      _Version        =   65536
      _ExtentX        =   1508
      _ExtentY        =   1296
      _StockProps     =   78
      Caption         =   "&IMPRIMIR"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   6
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Picture         =   "FrmKuso.frx":18EE
   End
   Begin Threed.SSCommand SCmd_Options 
      Height          =   735
      Index           =   2
      Left            =   2640
      TabIndex        =   16
      Top             =   2160
      Width           =   855
      _Version        =   65536
      _ExtentX        =   1508
      _ExtentY        =   1296
      _StockProps     =   78
      Caption         =   "&SALIR"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   6
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Picture         =   "FrmKuso.frx":1FB8
   End
   Begin Threed.SSCommand SCmd_Options 
      Height          =   735
      Index           =   0
      Left            =   480
      TabIndex        =   17
      Top             =   2160
      Width           =   855
      _Version        =   65536
      _ExtentX        =   1508
      _ExtentY        =   1296
      _StockProps     =   78
      Caption         =   "&PANTALLA"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   6
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Picture         =   "FrmKuso.frx":2682
   End
   Begin VB.Label LblReg 
      Height          =   255
      Left            =   120
      TabIndex        =   18
      Top             =   1800
      Width           =   3735
   End
End
Attribute VB_Name = "FrmKuso"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Dim Cantidad As Double
Dim CantiK As Double
Dim Costo As Double
Dim CostoK As Double
Dim Rstdatos As ADODB.Recordset
Public OpcCod        As String   'Opci�n de seguridad

Private Sub Form_Load()
    Dim ElClnt As New ElTercero
    ElClnt.IniXNit
 Call CenterForm(MDI_Inventarios, Me)
' Call Leer_Permisos("03023", SCmd_Options(3), SCmd_Options(3), SCmd_Options(0))
 Crys_Listar.Formulas(0) = "ENTIDAD='" & ElClnt.Nombre & Comi
 Crys_Listar.Formulas(1) = "NIT= 'NIT. " & ElClnt.Nit & Comi
 Crys_Listar.Formulas(2) = "HORA= 'Hora: " & Format(Now, "hh:mm") & Comi
 Crys_Listar.Formulas(3) = "USUARIO = 'Usuario:" & UserId & Comi
' Crys_Listar.Connect = conCrys
 SCmd_Options(1).Enabled = SCmd_Options(0).Enabled
 MskFecha(0) = Hoy
 MskFecha(1) = Hoy
End Sub
Private Sub TxtRango_GotFocus(Index As Integer)
    TxtRango(Index).SelStart = 0
    TxtRango(Index).SelLength = Len(TxtRango(Index).Text)
End Sub
Private Sub TxtRango_KeyPress(Index As Integer, KeyAscii As Integer)
    Call ValKeyAlfaNum(KeyAscii)
    If KeyAscii = 13 Then
     If Index = 1 Then
      MskFecha(0).SetFocus
     Else
      TxtRango(Index + 1).SetFocus
     End If
    End If
End Sub
Private Sub TxtRango_KeyDown(Index As Integer, KeyCode As Integer, Shift As Integer)
 Select Case Index
    Case 0
           If KeyCode = 40 Then
              TxtRango(1).SetFocus
           End If
    Case 1
           If KeyCode = 38 Then
              TxtRango(Index - 1).SetFocus
           End If
           If KeyCode = 40 Then
              MskFecha(0).SetFocus
           End If
 End Select
End Sub
Private Sub MskFecha_GotFocus(Index As Integer)
    MskFecha(Index).SelStart = 0
    MskFecha(Index).SelLength = Len(MskFecha(Index).Text)
End Sub
Private Sub MskFecha_KeyPress(Index As Integer, KeyAscii As Integer)
    Call ValKeyFecha(KeyAscii, MskFecha(Index))
    Call Cambiar_Enter(KeyAscii)
End Sub
Private Sub MskFecha_LostFocus(Index As Integer)
   Call ValFecha(MskFecha(Index), 0)
End Sub
Private Sub MskFecha_KeyDown(Index As Integer, KeyCode As Integer, Shift As Integer)
 If Index = 0 Then
   If KeyCode = 38 Then
      TxtRango(1).SetFocus
   End If
   If KeyCode = 40 Then
      MskFecha(1).SetFocus
   End If
 Else
   If KeyCode = 38 Then
      MskFecha(0).SetFocus
   End If
   If KeyCode = 40 Then
      SCmd_Options(0).SetFocus
   End If
 End If
End Sub
Private Sub ChkDetallado_KeyPress(KeyAscii As Integer)
   Call Cambiar_Enter(KeyAscii)
End Sub
Private Sub CmdSelec_Click(Index As Integer)
    Codigo = NUL$
    Codigo = Seleccion("USOS", "DE_DESC_USOS", "CD_CODI_USOS,DE_DESC_USOS", "USOS DE ARTICULOS", NUL$)
    If Codigo <> NUL$ Then TxtRango(Index) = Codigo
    TxtRango(Index).SetFocus
End Sub
Private Sub SCmd_Options_Click(Index As Integer)
    Screen.MousePointer = 11
    Select Case Index
        '''True = pantalla
        '''False = impresora
        Case 0: Call Informe(True)
        Case 1: Call Informe(False)
        Case 2: Unload Me
    End Select
    Screen.MousePointer = 0
End Sub
Private Sub Informe(ByVal pantalla As Boolean)
Dim ArrTemp() As Variant
Dim nomrep As String
  Dim algo
  If TxtRango(0) > TxtRango(1) Or TxtRango(1) = "" Then
     Call Mensaje1("Revise rango de usos", 3)
     Exit Sub
  End If
  algo = DateDiff("d", MskFecha(0), MskFecha(1))
  If CLng(algo) < 0 Then
     Call Mensaje1("Fecha Inicial Mayor que Fecha Final", 3)
     Exit Sub
  End If
  'validar que el mes de donde se van a tomar los saldos iniciales este cerrado
  ReDim ArrTemp(0)
  Result = LoadData("PARAMETROS_INVE", "FE_CIER_APLI", NUL$, ArrTemp())
  If Result <> FAIL And Encontro Then
     If (CLng(Format(MskFecha(0), "yyyymm")) - 1) > CLng(Format(ArrTemp(0), "yyyymm")) Then
        Call Mensaje1("No se puede generar el listado porque " & MonthName(CLng(Format(MskFecha(0), "mm")) - 1) & " de " & Format(MskFecha(0), "yyyy") & " no se ha cerrado", 1)
        Call MouseNorm
        Exit Sub
     End If
  Else
     Call Mensaje1("No se pudo obtener la fecha de cierre", 1)
     Call MouseNorm
     Exit Sub
  End If

  ''Env�a rango de fechas al informe
  Crys_Listar.Formulas(4) = "FECHAI= '" & MskFecha(0) & Comi
  Crys_Listar.Formulas(5) = "FECHAF= '" & MskFecha(1) & Comi
  ''''''''''''''''''''''''''''''''''
  Crys_Listar.WindowTitle = "K�rdex Almac�n por Usos"
  If ChkDetallado.Value = 0 Then
     Crys_Listar.ReportFileName = DirTrab + "kualma.RPT"
     nomrep = "kualma.RPT"
  Else
     Crys_Listar.ReportFileName = DirTrab + "kualmad.RPT"
     nomrep = "kualmad.RPT"
  End If
  Crys_Listar.SelectionFormula = "{USOS.CD_CODI_USOS} in '" & TxtRango(0) & "' to '" & TxtRango(1) & Comi
  If SCmd_Options(4).Enabled = False Then
     Call Mensaje1("Permiso Denegado", 3)
  Else
     Crea_Temporales ''Crea temporal para impresi�n
     On Error GoTo control
     If pantalla = True Then
        Crys_Listar.Destination = crptToWindow
     Else
        Crys_Listar.Destination = crptToPrinter
     End If
     Crys_Listar.Action = 1
  End If
  Exit Sub
control:
  Call Mensaje1(ERR.Number & ": " & ERR.Description & "  Nombre del Reporte : " & nomrep, 1)
End Sub
''Crea temporal para informaci�n del k�rdex x Usos
Private Sub Crea_Temporales()
Dim Fuc As String
  Valores = " CT_CANT_SALD=0,  VL_COST_SALD=0 "
  Result = DoUpdate("SAL_ANT", Valores, NUL$)
  
  Fuc = "01/" & Format(MskFecha(0), "mm") & "/" & Format(MskFecha(0), "yyyy")
  Fuc = DateAdd("d", -1, Fuc)
  
  Condicion = "ID_ESTA_KARD <> " & "2"
'  condicion = condicion & " And FE_FECH_KARD < " & fecha(MskFecha(0))
'  condicion = condicion & " And FE_FECH_KARD >= " & fecha(MskFecha(0))
'  condicion = condicion & " And FE_FECH_KARD <= " & fecha(MskFecha(1))
  Condicion = Condicion & " And CD_ORDO_KARD <> " & "2"
  Condicion = Condicion & " And CD_ORDO_KARD <> " & "4"
  Condicion = Condicion & " And CD_ORDO_KARD <> " & "5"
  Condicion = Condicion & " And CD_ARTI_KARD = CD_CODI_ARTI"
  Condicion = Condicion & " AND CD_USOS_ARTI >= " & Comi & TxtRango(0) & Comi
  Condicion = Condicion & " AND CD_USOS_ARTI <= " & Comi & TxtRango(1) & Comi
  Condicion = Condicion & " ORDER BY CD_ARTI_KARD"
  Set Rstdatos = New ADODB.Recordset
  Call SelectRST("KARDEX_ARTI, ARTICULO", "DISTINCT CD_ARTI_KARD", Condicion, Rstdatos)
  If (Result <> False) Then
    If Not Rstdatos.EOF Then
      Rstdatos.MoveFirst
      LblReg = NUL$
      Do While Not Rstdatos.EOF
        LblReg = "Leyendo Articulo: " & Rstdatos.Fields(0)
        DoEvents
        Cantidad = Saldo_Inicial(Rstdatos.Fields(0), Fuc, Costo)
        CantiK = Saldo_Kardex(Rstdatos.Fields(0), MskFecha(0), CostoK)
                   
        Cantidad = Cantidad + CantiK
        Costo = Costo + CostoK
        
        If Cantidad = 0 Then Costo = 0
            
        Condicion = "CD_ARTI_SALD = '" & Rstdatos.Fields(0) & Comi
        Valores = " CT_CANT_SALD=" & Cantidad & Coma
        Valores = Valores & " VL_COST_SALD=" & Format(Costo, "############0.##00")
        Result = DoUpdate("SAL_ANT", Valores, Condicion)
        Rstdatos.MoveNext
      Loop
    End If
  End If
  Rstdatos.Close
  LblReg = NUL$
End Sub

