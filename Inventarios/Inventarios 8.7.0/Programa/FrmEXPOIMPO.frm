VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Object = "{0BA686C6-F7D3-101A-993E-0000C0EF6F5E}#1.0#0"; "THREED32.OCX"
Begin VB.Form FrmEXPOIMPO 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Archivos Planos"
   ClientHeight    =   6375
   ClientLeft      =   4185
   ClientTop       =   2970
   ClientWidth     =   5820
   Icon            =   "FrmEXPOIMPO.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   ScaleHeight     =   6375
   ScaleWidth      =   5820
   Begin MSComctlLib.ListView lstCAMPOS 
      Height          =   3375
      Left            =   480
      TabIndex        =   10
      Top             =   600
      Width           =   4815
      _ExtentX        =   8493
      _ExtentY        =   5953
      View            =   3
      LabelWrap       =   -1  'True
      HideSelection   =   -1  'True
      Checkboxes      =   -1  'True
      _Version        =   393217
      ForeColor       =   -2147483640
      BackColor       =   -2147483643
      BorderStyle     =   1
      Appearance      =   1
      NumItems        =   0
   End
   Begin MSComctlLib.ProgressBar PrgPlano 
      Height          =   255
      Left            =   840
      TabIndex        =   0
      Top             =   4680
      Width           =   4095
      _ExtentX        =   7223
      _ExtentY        =   450
      _Version        =   393216
      BorderStyle     =   1
      Appearance      =   1
   End
   Begin VB.Frame Frame2 
      Height          =   1095
      Left            =   600
      TabIndex        =   1
      Top             =   5160
      Width           =   4575
      Begin Threed.SSCommand SCmd_Options 
         Height          =   735
         Index           =   0
         Left            =   240
         TabIndex        =   2
         Top             =   240
         Width           =   855
         _Version        =   65536
         _ExtentX        =   1508
         _ExtentY        =   1296
         _StockProps     =   78
         Caption         =   "&DESCARGA"
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   6
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Picture         =   "FrmEXPOIMPO.frx":058A
      End
      Begin Threed.SSCommand SCmd_Options 
         Height          =   735
         Index           =   1
         Left            =   1320
         TabIndex        =   3
         Top             =   240
         Width           =   855
         _Version        =   65536
         _ExtentX        =   1508
         _ExtentY        =   1296
         _StockProps     =   78
         Caption         =   "&CARGAR"
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   6
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Picture         =   "FrmEXPOIMPO.frx":0C54
      End
      Begin Threed.SSCommand SCmd_Options 
         Height          =   735
         Index           =   3
         Left            =   3480
         TabIndex        =   4
         Top             =   240
         Width           =   855
         _Version        =   65536
         _ExtentX        =   1508
         _ExtentY        =   1296
         _StockProps     =   78
         Caption         =   "&SALIR"
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   6
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Picture         =   "FrmEXPOIMPO.frx":131E
      End
      Begin Threed.SSCommand SCmd_Detener 
         Height          =   735
         Left            =   2400
         TabIndex        =   5
         Top             =   240
         Width           =   855
         _Version        =   65536
         _ExtentX        =   1508
         _ExtentY        =   1296
         _StockProps     =   78
         Caption         =   "CANCELAR"
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   6
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Picture         =   "FrmEXPOIMPO.frx":19E8
      End
   End
   Begin VB.Label Lbl_Errores 
      Caption         =   "0"
      Height          =   255
      Left            =   3960
      TabIndex        =   9
      Top             =   4320
      Width           =   855
   End
   Begin VB.Label Lbl_Procesados 
      Caption         =   "0"
      Height          =   255
      Left            =   1920
      TabIndex        =   8
      Top             =   4320
      Width           =   855
   End
   Begin VB.Label Label2 
      Caption         =   "Errores:"
      Height          =   255
      Left            =   3360
      TabIndex        =   7
      Top             =   4320
      Width           =   615
   End
   Begin VB.Label Label1 
      Caption         =   "Procesados:"
      Height          =   255
      Left            =   960
      TabIndex        =   6
      Top             =   4320
      Width           =   975
   End
End
Attribute VB_Name = "FrmEXPOIMPO"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Dim texto As String
Dim NumReg As Long
Dim BandErr, BandStop As Boolean
'Dim ArrTarifa(), CodTarifaBase() As Variant        'DEPURACION DE CODIGO
'Dim erroresCargue As Integer       'DEPURACION DE CODIGO
Dim LarReg As Integer
Dim Articulo                        As New ElArticulo
Private vFGrid                     As MSFlexGrid
Private vDocumento             As ElDocumento
Private vFormulario              As Form
Private PosEnGri                 As String
Private UtilizArti As String 'AASV M5606 Guarda eltipo el tipo de Bodega V = Venta C = Consumo

Private Sub Form_Load()
    Call CenterForm(MDI_Inventarios, Me)
    BandProcLote = 0
    BandStop = False
    Me.lstCAMPOS.ListItems.Clear
    Me.lstCAMPOS.ColumnHeaders.Add , "CNT", "Contenido", 0.8 * Me.lstCAMPOS.Width
    Me.lstCAMPOS.ColumnHeaders.Item("CNT").Alignment = lvwColumnLeft
    Me.lstCAMPOS.ColumnHeaders.Add , "POS", "Posici�n", 0
    Me.lstCAMPOS.ColumnHeaders.Item("POS").Alignment = lvwColumnRight
    Me.lstCAMPOS.ListItems.Add , "COD", "C�digo"
    Me.lstCAMPOS.ListItems.Item("COD").ListSubItems.Add , "POS", CStr(vFormulario.ColCodigoArticulo)
    Me.lstCAMPOS.ListItems.Add , "NOM", "Nombre"
    Me.lstCAMPOS.ListItems.Item("NOM").ListSubItems.Add , "POS", CStr(vFormulario.ColNombreArticulo)
    Me.lstCAMPOS.ListItems.Add , "CAN", "Cantidad"
    Me.lstCAMPOS.ListItems.Item("CAN").ListSubItems.Add , "POS", CStr(vFormulario.ColCantidad)
    Me.lstCAMPOS.ListItems.Add , "COS", "Costo antes de impuesto"
    Me.lstCAMPOS.ListItems.Item("COS").ListSubItems.Add , "POS", CStr(vFormulario.ColCostoSinIVA)
    Me.lstCAMPOS.ListItems.Add , "IMP", "Impuesto"
    Me.lstCAMPOS.ListItems.Item("IMP").ListSubItems.Add , "POS", CStr(vFormulario.ColPorceIVA)
    Me.lstCAMPOS.ListItems.Add , "CST", "Costo con impuesto"
    Me.lstCAMPOS.ListItems.Item("CST").ListSubItems.Add , "POS", CStr(vFormulario.ColCostoUnidad)
    Me.lstCAMPOS.ListItems.Add , "UND", "Presentaci�n"
    Me.lstCAMPOS.ListItems.Item("UND").ListSubItems.Add , "POS", CStr(vFormulario.ColNombreUnidad)
    Me.lstCAMPOS.ListItems.Add , "MLT", "Multiplicador"
    Me.lstCAMPOS.ListItems.Item("MLT").ListSubItems.Add , "POS", CStr(vFormulario.ColMultiplicador)
    Me.lstCAMPOS.ListItems.Add , "DVD", "Divisor"
    Me.lstCAMPOS.ListItems.Item("DVD").ListSubItems.Add , "POS", CStr(vFormulario.ColDivisor)
' 1) = Me.ColAutoArticulo
' 2) = Me.ColCodigoArticulo
' 3) = Me.ColNombreArticulo
' 4) = Me.ColAutoUnidad
' 5) = Me.ColNombreUnidad
' 6) = Me.ColMultiplicador
' 7) = Me.ColDivisor
' 8) = Me.ColUltimoCosto
' 9) = Me.ColCantidad
'10) = Me.ColCostoSinIVA
'11) = Me.ColPorceIVA
'12) = Me.ColCostoUnidad
'13) = Me.ColCostoTotal
'14) = Me.ColFecVence
'15) = Me.ColFecEntrada
'16) = Me.ColCantidadFija
'17) = Me.ColAutoDocumento
    Me.lstCAMPOS.ListItems.Item("COD").Checked = True
    Me.lstCAMPOS.ListItems.Item("NOM").Checked = False
    Me.lstCAMPOS.ListItems.Item("CAN").Checked = True
    Me.lstCAMPOS.ListItems.Item("COS").Checked = False
    Me.lstCAMPOS.ListItems.Item("IMP").Checked = False
    Me.lstCAMPOS.ListItems.Item("CST").Checked = True
    Me.lstCAMPOS.ListItems.Item("UND").Checked = False
    Me.lstCAMPOS.ListItems.Item("MLT").Checked = False
    Me.lstCAMPOS.ListItems.Item("DVD").Checked = False
End Sub

Private Sub SCmd_Detener_Click()
If BandProcLote Then
   If MsgBox("�Esta seguro que desea detener el proceso?", vbExclamation) = vbYes Then
      BandStop = True
   End If
End If
End Sub

Private Sub SCmd_Options_Click(Index As Integer)
If BandProcLote = 1 Then Exit Sub
Select Case Index
   Case 0: Call DescargarTarifa
   Case 1: If vDocumento.Encabezado.EsNuevoDocumento And vDocumento.Encabezado.EsIndependiente Then Call CargarTarifas
   Case 3: Unload Me
End Select
End Sub

Sub CargarTarifas()
   Dim TotaLeidos As Long
   Dim TotalLinea As Double
   'Dim Procesados As Integer, CuentaComas As Integer
   Dim CuentaComas As Integer      'DEPURACION DE CODIGO
   Dim UnItem As MSComctlLib.ListItem
   'Dim Linea As String, Estoy As String, Faltan As String, Iva As String
   Dim Estoy As String, Faltan As String, Iva As String        'DEPURACION DE CODIGO
   Dim SIva As String 'REOL253
   Dim Errores
   Dim UsarCostoPlano As Boolean       'PJCA M968
   'Dim ArrCostProm() As Variant 'APGR T7006
   
   On Error GoTo control
   PrgPlano.Value = 0
   'If MDI_Inventarios.CMDialog1.InitDir = NUL$ Then MDI_Inventarios.CMDialog1.InitDir = DirDB
   If MDI_Inventarios.CMDialog1.InitDir = NUL$ Then MDI_Inventarios.CMDialog1.InitDir = App.Path
   MDI_Inventarios.CMDialog1.Filter = "(TODOS)|*"
   MDI_Inventarios.CMDialog1.Action = 1
   BandErr = False
   If MDI_Inventarios.CMDialog1.filename <> "" Then
      If FileLen(MDI_Inventarios.CMDialog1.filename) = 0 Then Call Mensaje1("El archivo se encuentra vacio, cambie de archivo", 3): Exit Sub 'REOL M253
      PrgPlano.Max = FileLen(MDI_Inventarios.CMDialog1.filename)
      Open MDI_Inventarios.CMDialog1.filename For Input As #1
      Open App.Path & "\ErrorDeCarga.txt" For Output As #2
      NumReg = 1
      BandProcLote = 1
      PrgPlano.Value = PrgPlano.Min
      DoEvents
      Errores = 0
      BandStop = False
      vFGrid.Rows = 1
      LarReg = 0: TotaLeidos = 0
      Me.PosicionesEnGrilla = ""
      For Each UnItem In Me.lstCAMPOS.ListItems
         If UnItem.Checked Then Me.PosicionesEnGrilla = Me.PosicionesEnGrilla & IIf(Len(Me.PosicionesEnGrilla) = 0, "", Coma) & UnItem.ListSubItems("POS").Text
      Next
      CuentaComas = CuantosHay(Me.PosicionesEnGrilla, Coma)
      Do While Not EOF(1)
         DoEvents
         If BandStop = True Then Exit Do
         Line Input #1, texto
         
         '----------------------------------------------------------------------------------------------------------------------------------------
         ' GMS M1520.
         ' V�lida si hay registros por procesar.
         If texto = NUL$ Then GoTo Salida
         '----------------------------------------------------------------------------------------------------------------------------------------
         
         'TotaLeidos = TotaLeidos + Len(Faltan)
         TotaLeidos = TotaLeidos + Len(SIva)
         
         LarReg = TotaLeidos / NumReg
         If PrgPlano.Value + LarReg < PrgPlano.Max Then PrgPlano.Value = PrgPlano.Value + LarReg
         '----------------------------------------------------------------------------------------------------------------------------------------
         ' GMS M1520.
         ' Muestra en mensaje de alerta si la estructura del archivo no corresponde, a la especificada
         ' por el usuario.
         '----------------------------------------------------------------------------------------------------------------------------------------
         'If CuantosHay(texto, Coma) <> CuentaComas Then GoTo OtrReg
         If CuantosHay(texto, Coma) <> CuentaComas Then Call Mensaje1("La estructura del archivo no corresponde a la especificada", 3): GoTo Salida
         '----------------------------------------------------------------------------------------------------------------------------------------
         Estoy = Trim(SacarUnoXClave(texto, Me.PosicionesEnGrilla, Coma, vFormulario.ColCodigoArticulo))
         'JLPB T28538 INICIO
         If vDocumento.Encabezado.TipDeDcmnt = BajaConsumo Or vDocumento.Encabezado.TipDeDcmnt = Despacho Then
            Faltan = Val(fnDevDato("ARTICULO", "VL_COPR_ARTI", "CD_CODI_ARTI='" & Estoy & Comi, True))
         Else
            'JLPB T28538 FIN
            Iva = Trim(SacarUnoXClave(texto, Me.PosicionesEnGrilla, Coma, vFormulario.ColPorceIVA)) 'REOL253
            SIva = Trim(SacarUnoXClave(texto, Me.PosicionesEnGrilla, Coma, vFormulario.ColCostoSinIVA)) 'REOL253
            Faltan = Trim(SacarUnoXClave(texto, Me.PosicionesEnGrilla, Coma, vFormulario.ColCostoUnidad))
         End If 'JLPB T28538
         
         If Iva = NUL$ Then Iva = "0" 'REOL253
         If SIva = NUL$ Then SIva = "0" 'GMS M1521
         If Len(Faltan) = 0 Then Faltan = "0"
         'JLPB T36877 INICIO
         With vDocumento.Encabezado
            If .TipDeDcmnt <> Cotizacion And .TipDeDcmnt <> AnulaCotizacion And .TipDeDcmnt <> ODCompra And .TipDeDcmnt <> AnulaODCompra And .TipDeDcmnt <> Entrada And .TipDeDcmnt <> DevolucionEntrada And .TipDeDcmnt <> AnulaEntrada Then
               Iva = 0
               SIva = 0
            End If
         End With
         'JLPB T36877 FIN
         If ValidarArticulo(Estoy) Then
            If vDocumento.Encabezado.TipDeDcmnt = 10 Then Iva = "0": SIva = "0" 'JAUM T29703
            Articulo.PorcentajeIVA = Iva 'REOL253
            ' If SIva <> NUL$ Then Articulo.CostoPromedio = SIva 'REOL253
'            If SIva <> NUL$ Or SIva <> "0" Then Articulo.CostoPromedio = SIva 'GMS M1521
            If SIva <> NUL$ And SIva <> "0" Then Articulo.CostoPromedio = SIva 'REOL M2592
            vFGrid.Rows = vFGrid.Rows + 1
            vFGrid.Row = vFGrid.Rows - 1
            vFGrid.TextMatrix(vFGrid.Row, vFormulario.ColAutoArticulo) = Articulo.Numero
            vFGrid.TextMatrix(vFGrid.Row, vFormulario.ColCodigoArticulo) = Articulo.Codigo
            vFGrid.TextMatrix(vFGrid.Row, vFormulario.ColNombreArticulo) = Articulo.Nombre
            If Not Articulo.CargarUnidadXBodega(vDocumento.Encabezado.BodegaORIGEN) = Encontroinfo Then GoTo OtrReg
            vFGrid.TextMatrix(vFGrid.Row, vFormulario.ColAutoUnidad) = Articulo.UnidadXBodega(vDocumento.Encabezado.BodegaORIGEN).AutoNumerico
            vFGrid.TextMatrix(vFGrid.Row, vFormulario.ColNombreUnidad) = Articulo.UnidadXBodega(vDocumento.Encabezado.BodegaORIGEN).NombreDeUnidad
            vFGrid.TextMatrix(vFGrid.Row, vFormulario.ColMultiplicador) = Articulo.UnidadXBodega(vDocumento.Encabezado.BodegaORIGEN).Multiplicador
            vFGrid.TextMatrix(vFGrid.Row, vFormulario.ColDivisor) = Articulo.UnidadXBodega(vDocumento.Encabezado.BodegaORIGEN).Divisor
            'APGR T7006 PNC
            'APGR T7006  INICIO - Se deja en comentario.
'            ReDim ArrCostProm(0)
'            Result = LoadData("ARTICULO", "VL_COPR_ARTI", "CD_CODI_ARTI='" & Articulo.Codigo & "'", ArrCostProm())
'            If Not ArrCostProm(0) = Articulo.UltimoCosto Then MsgBox ("El valor del Art�culo " & Trim(Articulo.Codigo) & " del archivo no corresponde al costo promedio"), vbInformation: GoTo Salida
            'APGR T7006  INICIO
            vFGrid.TextMatrix(vFGrid.Row, vFormulario.ColUltimoCosto) = Aplicacion.Formatear_Valor(Articulo.UltimoCosto)
            Estoy = Trim(SacarUnoXClave(texto, Me.PosicionesEnGrilla, Coma, vFormulario.ColCantidad))
            If Len(Estoy) = 0 Then Estoy = "0"

            vFGrid.TextMatrix(vFGrid.Row, vFormulario.ColCantidad) = Aplicacion.Formatear_Cantidad(Estoy)
            'JAUM T29703 Inicio
            If vDocumento.Encabezado.TipDeDcmnt = 10 Then
               UsarCostoPlano = False
            Else
            'JAUM T29703 Fin
               If InStr(1, Me.PosicionesEnGrilla, "14") > 1 Then UsarCostoPlano = True     'Saber si se usa el costo del plano o de la BD  PJCA M968
            End If 'JAUM T29703
            Select Case vDocumento.Encabezado.TipDeDcmnt
               Case Entrada, ODCompra
                  '''''''''''''''REOL253
                  'vFGrid.TextMatrix(vFGrid.Row, vFormulario.ColCostoUnidad) = Aplicacion.Formatear_Valor(CDbl(Faltan))
                  'TotalLinea = Aplicacion.Formatear_Valor(CDbl(Faltan))
                  
                  'GMS M1521
                  'If UsarCostoPlano Then Articulo.CostoPromedio = Faltan       'PJCA M968
                  If UsarCostoPlano Then
                     If SIva = "0" Then
                        SIva = Aplicacion.Formatear_Valor((Faltan * 100) / (100 + Iva))
                     End If
                     Articulo.CostoPromedio = SIva
                  End If
                  
                  vFGrid.TextMatrix(vFGrid.Row, vFormulario.ColCostoUnidad) = Aplicacion.Formatear_Valor(Articulo.CostoPromedio * (Iva / 100 + 1))
                  TotalLinea = Articulo.CostoPromedio
               
               Case FacturaVenta
                  '-----------------------------------------------------------------------------
                  'GMS 1559
                  '-----------------------------------------------------------------------------
                  If UsarCostoPlano Then
                     If SIva = "0" Then
                        SIva = Aplicacion.Formatear_Valor((Faltan * 100) / (100 + Iva))
                     End If
                     Articulo.CostoPromedio = SIva
                  End If
                  vFGrid.TextMatrix(vFGrid.Row, vFormulario.ColCostoUnidad) = Aplicacion.Formatear_Valor(Articulo.CostoPromedio * (Iva / 100 + 1))
                  TotalLinea = Articulo.CostoPromedio
   
                  vFGrid.TextMatrix(vFGrid.Row, vFormulario.ColVentaSinIVA) = Aplicacion.Formatear_Valor(100 * CDbl(vFGrid.TextMatrix(vFGrid.Row, vFormulario.ColCostoUnidad)) / (100 + Articulo.PorcentajeIVA))
                  vFGrid.TextMatrix(vFGrid.Row, vFormulario.ColVentaTotal) = Aplicacion.Formatear_Valor(Articulo.CostoPromedio * (Iva / 100 + 1))
   
                  vFGrid.TextMatrix(vFGrid.Row, vFormulario.ColDescPorcentual) = Aplicacion.Formatear_Valor(0)
                  vFGrid.TextMatrix(vFGrid.Row, vFormulario.ColDescAbsoluto) = Aplicacion.Formatear_Valor(0)
                  '-----------------------------------------------------------------------------
               Case Else
                  'vFGrid.TextMatrix(vFGrid.Row, vFormulario.ColCostoUnidad) = Aplicacion.Formatear_Valor(Articulo.CostoPromedio)
                  'TotalLinea = Articulo.CostoPromedio
                  'vFGrid.TextMatrix(vFGrid.Row, vFormulario.ColCostoUnidad) = Aplicacion.Formatear_Valor(CDbl(Faltan))
                  'TotalLinea = Aplicacion.Formatear_Valor(CDbl(Faltan))
                  
                  'GMS M1521
                  'If UsarCostoPlano Then Articulo.CostoPromedio = Faltan 'REOL M1414   M2592
                  'If UsarCostoPlano And vDocumento.Encabezado.TipDeDcmnt <> Salida And Despacho <> vDocumento.Encabezado.TipDeDcmnt Then Articulo.CostoPromedio = Faltan 'JACC M5619 ''APGR T7006 PNC
                  If UsarCostoPlano And vDocumento.Encabezado.TipDeDcmnt <> Salida And vDocumento.Encabezado.TipDeDcmnt <> Requisicion And Despacho <> vDocumento.Encabezado.TipDeDcmnt Then Articulo.CostoPromedio = Faltan 'APGR T7006 PNC
                  
                  
'                 If UsarCostoPlano Then Articulo.CostoPromedio = SIva
                  vFGrid.TextMatrix(vFGrid.Row, vFormulario.ColCostoUnidad) = Aplicacion.Formatear_Valor(Articulo.CostoPromedio * (Iva / 100 + 1))
                  TotalLinea = Articulo.CostoPromedio
               
                  '''''''''''''''REOL253
            End Select
            
            'vFGrid.TextMatrix(vFGrid.Row, vFormulario.ColCostoSinIVA) = Aplicacion.Formatear_Valor(CDbl(Faltan) * (1 - Articulo.PorcentajeIVA / 100))
            vFGrid.TextMatrix(vFGrid.Row, vFormulario.ColCostoSinIVA) = Aplicacion.Formatear_Valor(100 * CDbl(vFGrid.TextMatrix(vFGrid.Row, vFormulario.ColCostoUnidad)) / (100 + Articulo.PorcentajeIVA))
            vFGrid.TextMatrix(vFGrid.Row, vFormulario.ColPorceIVA) = Aplicacion.Formatear_Valor(Articulo.PorcentajeIVA)
'            TotalLinea = TotalLinea * CDbl(Estoy)
            TotalLinea = TotalLinea * CDbl(Estoy) * (1 + (Iva / 100))   'REOL M2460
            vFGrid.TextMatrix(vFGrid.Row, vFormulario.ColCostoTotal) = Aplicacion.Formatear_Valor(TotalLinea)
            vFGrid.TextMatrix(vFGrid.Row, vFormulario.ColFecVence) = FormatDateTime(IIf(Articulo.LoteVence, Articulo.FechaVence, CDate(2)), vbShortDate)
            vFGrid.TextMatrix(vFGrid.Row, vFormulario.ColFecEntrada) = FormatDateTime(IIf(Articulo.LoteEntrada, Articulo.FechaEntrada, CDate(2)), vbShortDate)
            vFGrid.TextMatrix(vFGrid.Row, vFormulario.ColCantidadFija) = vFGrid.TextMatrix(vFGrid.Row, vFormulario.ColCantidad)
            vFGrid.TextMatrix(vFGrid.Row, vFormulario.ColAutoDocumento) = "0"
            'vFGrid.TextMatrix(vFGrid.Row, vFormulario.ColVenUndConDes) = "0"   'PedroJ
            vFGrid.TextMatrix(vFGrid.Row, vFormulario.ColVenUndConDes) = IIf(vDocumento.Encabezado.TipDeDcmnt = FacturaVenta, Aplicacion.Formatear_Valor(Articulo.CostoPromedio * (Iva / 100 + 1)), "0") 'GMS M1559
            
         Else
            Print #2, "Linea:" & NumReg & ", C�digo:" & Estoy
            Errores = Errores + 1
         End If
''        Faltan = texto
''        TotaLeidos = TotaLeidos + Len(Faltan)
''        LarReg = TotaLeidos / NumReg
''        If PrgPlano.Value + LarReg < PrgPlano.Max Then PrgPlano.Value = PrgPlano.Value + LarReg
''        Estoy = ParteIzquierda(Faltan, ",")
''        If Len(Estoy) = 0 Then Estoy = Faltan
''        If ValidarArticulo(Estoy) Then
''            Faltan = ParteDerecha(Faltan, ",")
''            vFGrid.Rows = vFGrid.Rows + 1
''            vFGrid.Row = vFGrid.Rows - 1
''            vFGrid.TextMatrix(vFGrid.Row, vFormulario.ColAutoArticulo) = Articulo.Numero
''            vFGrid.TextMatrix(vFGrid.Row, vFormulario.ColCodigoArticulo) = Articulo.Codigo
''            vFGrid.TextMatrix(vFGrid.Row, vFormulario.ColNombreArticulo) = Articulo.Nombre
''            If Not Articulo.CargarUnidadXBodega(vDocumento.Encabezado.BodegaORIGEN) = Encontroinfo Then GoTo OtrReg
''            vFGrid.TextMatrix(vFGrid.Row, vFormulario.ColAutoUnidad) = Articulo.UnidadXBodega(vDocumento.Encabezado.BodegaORIGEN).AutoNumerico
''            vFGrid.TextMatrix(vFGrid.Row, vFormulario.ColNombreUnidad) = Articulo.UnidadXBodega(vDocumento.Encabezado.BodegaORIGEN).NombreDeUnidad
''            vFGrid.TextMatrix(vFGrid.Row, vFormulario.ColMultiplicador) = Articulo.UnidadXBodega(vDocumento.Encabezado.BodegaORIGEN).Multiplicador
''            vFGrid.TextMatrix(vFGrid.Row, vFormulario.ColDivisor) = Articulo.UnidadXBodega(vDocumento.Encabezado.BodegaORIGEN).Divisor
''            Estoy = ParteIzquierda(Faltan, ",")
''            If Len(Estoy) = 0 Then Estoy = Faltan
''            Faltan = ParteDerecha(Faltan, ",")
''            vFGrid.TextMatrix(vFGrid.Row, vFormulario.ColUltimoCosto) = Aplicacion.Formatear_Valor(Articulo.UltimoCosto)
''            vFGrid.TextMatrix(vFGrid.Row, vFormulario.ColCantidad) = Aplicacion.Formatear_Cantidad(Estoy)
''            Select Case vDocumento.Encabezado.TipDeDcmnt
''            Case Entrada, ODCompra
''               vFGrid.TextMatrix(vFGrid.Row, vFormulario.ColCostoUnidad) = Aplicacion.Formatear_Valor(CDbl(Faltan))
''               TotalLinea = Aplicacion.Formatear_Valor(CDbl(Faltan))
''            Case Else
''               vFGrid.TextMatrix(vFGrid.Row, vFormulario.ColCostoUnidad) = Aplicacion.Formatear_Valor(Articulo.CostoPromedio)
''               TotalLinea = Articulo.CostoPromedio
''            End Select
'''            vFGrid.TextMatrix(vFGrid.Row, vFormulario.ColCostoSinIVA) = Aplicacion.Formatear_Valor(CDbl(Faltan) * (1 - Articulo.PorcentajeIVA / 100))
''            vFGrid.TextMatrix(vFGrid.Row, vFormulario.ColCostoSinIVA) = Aplicacion.Formatear_Valor(100 * CDbl(vFGrid.TextMatrix(vFGrid.Row, vFormulario.ColCostoUnidad)) / (100 + Articulo.PorcentajeIVA))
''            vFGrid.TextMatrix(vFGrid.Row, vFormulario.ColPorceIVA) = Aplicacion.Formatear_Valor(Articulo.PorcentajeIVA)
''            TotalLinea = TotalLinea * CDbl(Estoy)
''            vFGrid.TextMatrix(vFGrid.Row, vFormulario.ColCostoTotal) = Aplicacion.Formatear_Valor(TotalLinea)
''            vFGrid.TextMatrix(vFGrid.Row, vFormulario.ColFecVence) = FormatDateTime(IIf(Articulo.LoteVence, Articulo.FechaVence, CDate(2)), vbShortDate)
''            vFGrid.TextMatrix(vFGrid.Row, vFormulario.ColFecEntrada) = FormatDateTime(IIf(Articulo.LoteEntrada, Articulo.FechaEntrada, CDate(2)), vbShortDate)
''            vFGrid.TextMatrix(vFGrid.Row, vFormulario.ColCantidadFija) = vFGrid.TextMatrix(vFGrid.Row, vFormulario.ColCantidad)
''            vFGrid.TextMatrix(vFGrid.Row, vFormulario.ColAutoDocumento) = "0"
''        Else
''            Print #2, "Linea:" & NumReg & ", C�digo:" & Estoy
''            Errores = Errores + 1
''        End If
         Lbl_Procesados.Caption = NumReg & " de " & CLng(PrgPlano.Max / IIf(LarReg > 0, LarReg, 20))
         Lbl_Procesados.Refresh
         Lbl_Errores.Caption = Errores
         Lbl_Errores.Refresh
OtrReg:
         NumReg = NumReg + 1
      Loop
Salida:
      Close (1): Close (2)
      vFGrid.Col = vFormulario.ColNombreArticulo
      vFGrid.Sort = flexSortGenericAscending
      BandProcLote = 0
      Lbl_Procesados.Caption = NumReg & " de " & NumReg
      Lbl_Procesados.Refresh
      'DAHV T1607 - INICIO
      If Not vFGrid.TextMatrix(vFGrid.Rows - 1, vFormulario.ColAutoArticulo) = NUL$ Then
         vFGrid.Rows = vFGrid.Rows + 1
      End If
      'DAHV T1607 - FIN
      If Errores > 0 Then MsgBox "Se encontro errores, el resumen esta en el archivo:" & App.Path & "\ErrorDeCarga.txt", vbCritical
   End If
   PrgPlano.Value = 0
   Exit Sub
control:
   If ERR.Number <> 0 Then
      Call ConvertErr
      Close (1)
   End If
   Call MouseNorm
   PrgPlano.Value = 0
End Sub

Sub DescargarTarifa()
'Dim Linea As String, Estoy As String, Faltan As String, elOrden As String
Dim Linea As String     'DEPURACION DE CODIGO
Dim Procesados As Integer, ColEnGrilla As Integer, ColActual As Integer
Dim UnItem As MSComctlLib.ListItem
'Dim Errores As Integer     'DEPURACION DE CODIGO
Dim TOTAL As Integer
Bandera = 0
PrgPlano.Value = 0
'If MDI_Inventarios.CMDialog1.InitDir = NUL$ Then MDI_Inventarios.CMDialog1.InitDir = DirDB
If MDI_Inventarios.CMDialog1.InitDir = NUL$ Then MDI_Inventarios.CMDialog1.InitDir = App.Path
MDI_Inventarios.CMDialog1.filename = "" 'REOL M253
MDI_Inventarios.CMDialog1.Filter = "(TODOS)|*"
MDI_Inventarios.CMDialog1.Action = 2
BandErr = False
If MDI_Inventarios.CMDialog1.filename <> "" Then
    On Error GoTo control
    Open MDI_Inventarios.CMDialog1.filename For Output As #1
    BandProcLote = 1
    PrgPlano.Max = vFGrid.Rows
    TOTAL = vFGrid.Rows - 1
    PrgPlano.Value = 0
    PrgPlano.Min = 0
    BandStop = False
    'Me.PosicionesEnGrilla = "1,7"
    Me.PosicionesEnGrilla = ""
    
    'GMS M1559
    If vDocumento.Encabezado.TipDeDcmnt = FacturaVenta Then
        For Each UnItem In Me.lstCAMPOS.ListItems
            If UnItem.Checked Then Me.PosicionesEnGrilla = Me.PosicionesEnGrilla & IIf(Len(Me.PosicionesEnGrilla) = 0, "", Coma) & _
                                                IIf(UnItem.ListSubItems("POS").Text = vFormulario.ColCostoSinIVA, vFormulario.ColVentaSinIVA, IIf(UnItem.ListSubItems("POS").Text = vFormulario.ColCostoUnidad, vFormulario.ColVentaTotal, UnItem.ListSubItems("POS").Text))
        Next
    Else
        For Each UnItem In Me.lstCAMPOS.ListItems
            If UnItem.Checked Then Me.PosicionesEnGrilla = Me.PosicionesEnGrilla & IIf(Len(Me.PosicionesEnGrilla) = 0, "", Coma) & UnItem.ListSubItems("POS").Text
        Next
    End If
    
    For Procesados = 1 To TOTAL
        Linea = ""
        For ColActual = 1 To CuantosHay(Me.PosicionesEnGrilla, Coma) + 1
            ColEnGrilla = SacarUnoXPosicion(Me.PosicionesEnGrilla, Coma, ColActual)
            Linea = Linea & IIf(Len(Linea) = 0, "", Coma) & Replace(Trim(vFGrid.TextMatrix(Procesados, ColEnGrilla)), Coma, "")
        Next
''        Faltan = Me.PosicionesEnGrilla
''        If Not InStr(1, Faltan, ",") > 0 Then GoTo OtrReg
''        Do
''            Estoy = ParteIzquierda(Faltan, ",")
''            If Len(Estoy) = 0 Then Estoy = Faltan
''            ColActual = CByte(Estoy)
''            Linea = Linea & IIf(Len(Linea) > 0, ",", "") & Replace(vFGrid.TextMatrix(Procesados, ColActual), ",", "")
''            DoEvents
''            If BandStop Then Exit For
''            Faltan = ParteDerecha(Faltan, ",")
''        Loop Until Len(Faltan) = 0
        Debug.Print Linea
        Print #1, Linea
        Lbl_Procesados.Caption = PrgPlano.Value + 1 & " de " & TOTAL
        Lbl_Procesados.Refresh
        PrgPlano.Value = PrgPlano.Value + 1
OtrReg:
    Next
    Close (1)
    BandProcLote = 0
    PrgPlano.Value = 0
End If
Exit Sub
control:
If ERR.Number <> 0 Then
   Call ConvertErr
   Close (1)
End If
Call MouseNorm
PrgPlano.Value = 0

End Sub

Function ValidarArticulo(Codigo As String) As Boolean
    ValidarArticulo = True
    'If ValidaTipoEstadoArti(Codigo, UtilizArticulo) = Encontroinfo Then 'AASV M5606 Si el tipo del articulo coincide con el de la bodega y esta activo
    If ValidaTipoEstadoArti(Codigo, UtilizArticulo) Then  'AASV M5606 Nota: 25766 Se modifica ya que Encontroinfo = 1  Si el tipo del articulo coincide con el de la bodega y esta activo
       If Articulo.IniciaXCodigo(Codigo) = Encontroinfo Then Exit Function
       If Articulo.IniciaXBarra(Codigo) = Encontroinfo Then Exit Function
    End If 'AASV M5606
    ValidarArticulo = False
End Function

Public Property Set FgridArtic(Fgrid As MSFlexGrid)
    Set vFGrid = Fgrid
End Property

Public Property Set ElDocumento(ElDoc As ElDocumento)
    Set vDocumento = ElDoc
End Property

Public Property Set ElFormulario(ElFrm As Form)
    Set vFormulario = ElFrm
End Property

Public Property Let PosicionesEnGrilla(Cue As String)
    PosEnGri = Cue
End Property

Public Property Get PosicionesEnGrilla() As String
    PosicionesEnGrilla = PosEnGri
End Property
'---------------------------------------------------------------------------------------
' Procedure : UtilizArticulo
' DateTime  : 05/08/2009 13:42
' Author    : albert_silva
' Purpose   : M5606 Se asignan los valores V cuando la bodega almacena para la venta y C cuando almacena para consumo
'---------------------------------------------------------------------------------------
'
Public Property Let UtilizArticulo(VenCom As String)
    UtilizArti = VenCom
End Property

'---------------------------------------------------------------------------------------
' Procedure : UtilizArticulo
' DateTime  : 05/08/2009 13:50
' Author    : albert_silva
' Purpose   : M5606 Se asignan los valores V cuando la bodega almacena para la venta y C cuando almacena para consumo
'---------------------------------------------------------------------------------------
'
Public Property Get UtilizArticulo() As String
    UtilizArticulo = UtilizArti
End Property

'---------------------------------------------------------------------------------------
' Procedure : ValidaTipoEstadoArti
' DateTime  : 05/08/2009 14:16
' Author    : albert_silva
' Purpose   : M5606 Valida que los articulos a cargar sean del mismo tipo de la bodega y esten activos
'---------------------------------------------------------------------------------------
'
Function ValidaTipoEstadoArti(CodArti As String, TipoBode As String) As Boolean
Dim arr() As Variant
ReDim arr(0)

   Desde = "ARTICULO"
   Campos = "CD_CODI_ARTI"
   Condicion = "CD_CODI_ARTI = '" & CodArti & Comi
   Condicion = Condicion & " AND TX_ESTA_ARTI <> '1'"
   Condicion = Condicion & " AND  TX_PARA_ARTI='" & TipoBode & Comi
   If Result <> FAIL Then Result = LoadData(Desde, Campos, Condicion, arr())
   ValidaTipoEstadoArti = Encontro

End Function



