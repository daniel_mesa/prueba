Attribute VB_Name = "Transito"
Option Explicit
Type CaractPlan
   AutNum      As Long
   TipoCobe    As String
   VlTopeCobe  As Double
   PeriodCobe  As String
   AutGrLico   As Long 'autonum�rico de los limites de cobertura espec�ficos del plan
   TipoPers    As String 'Indica el tipo de persona para la cobertura del plan (P)ersona o (Nucleo)
   IndToCoPart As String
   VlToCopart  As Double
   PrToCoPart  As Double
End Type
Type ItemTransito
   NumTransito       As Double
   AutItem           As Double
   AutPlan           As Double
   autContrato       As Double
   FechIniCont       As Date
   FechFinCont       As Date
   factura           As String
   AutPrestador      As Double
   NombPrestador     As String
   AutPrestacion     As Double
   AutItemManualTar  As Double
   CodiPrestacion    As String
   NombPrestaci�n    As String
   CantPresen        As Double
   VlUnitPresen      As Double
   VlTotalPresen     As Double
   VlTope            As Double
   VlRecorte         As Double
   AutDeducible      As Double
   VlDeducible       As Double
   VlBase            As Double
   IndiCobertura     As String
   AutCobertura      As Double
   PorcCubierto      As Double
   CantCubierta      As Double
   VlCubierto        As Double
   PorcCredito       As Double
   VlCredito         As Double
   VlXCobrar         As Double
   VlXPagar          As Double
   AutTipoAten       As Double
   NombTipAten       As String
   AutAtencion       As Double
   NombAtencion      As String
   AutFactura        As Double
   FechaFactura      As Date
   AutConvenio       As Double
   VlConvenio        As Double
   Motivo            As String
   NombCobertura     As String
   AutParentesco     As Double
   AutCausaExterna   As Double
   AutDiagnostico    As Double
   AutTiponucleo     As Double
   AutGrupoCie       As Double
   AutBeneficiario   As Double
   AutTitular        As Double
   AutTercBeneficiario As Double
   AutTercTitular      As Double
   TipoCuadro        As String
   TipoCiudad        As String
   AutCiudad         As Double
   AutProfesional    As Double
End Type
'Variable para el campo de Observaciones de la carta de
'Autorizaci�n del cr�dito
Public Observ_Credito As String

Public Function Ingresar_EstadoTran(ByVal NumTran As Long, ByVal Estado As String, Optional ByVal AutMotiDev As Double) As Integer
Valores = "FE_SALI_ESTR=" & FFechaIns(Nowserver) & Coma
Valores = Valores & "TX_ACTU_ESTR='N'"
condi = "NU_NUME_TRAN_ESTR=" & NumTran & " AND TX_ACTU_ESTR='S'"
If Result <> FAIL Then Result = DoUpdate("ESTADO_TRANSITO", Valores, condi)
Valores = "NU_NUME_TRAN_ESTR=" & NumTran & Coma
Valores = Valores & "TX_ESTA_ESTR=" & Comi & Estado & Comi & Coma
If AutMotiDev <> 0 Then
   Valores = Valores & "NU_AUTO_MODE_ESTR=" & AutMotiDev & Coma
Else
   Valores = Valores & "NU_AUTO_MODE_ESTR=NULL" & Coma
End If
Valores = Valores & "FE_INGR_ESTR=" & FFechaIns(Nowserver) & Coma
Valores = Valores & "FE_SALI_ESTR=" & FFechaIns(Nowserver) & Coma
Valores = Valores & "TX_ACTU_ESTR='S'" & Coma
Valores = Valores & "NU_AUTO_CONE_ESTR=" & NumConex
If Result <> FAIL Then Result = DoInsertSQL("ESTADO_TRANSITO", Valores)
Ingresar_EstadoTran = Result
End Function

Public Function Buscar_CoberturaPrest(ByVal AutPlan As Long, ByVal AutTiat As Long, ByVal AutAten As Long _
                  , ByVal AutPare As Long, ByVal AutCaex As Long, ByVal AutPrest As Long, ByVal AutDiag As Long _
                  , ByVal AutTiNu As Long, ByVal TipCuad As String, ByVal AutPrestador As Long, ByVal TipCiud As String _
                  , ByRef IndCobe As String, ByRef AutCobe As Double, ByRef NombCobe As String, ByRef IndDeduCobe As String _
                  , ByRef TipCobe As String, ByRef VlCobe As Double, ByRef PrCobe As Double, ByRef CtCobe As Double, ByRef Motivo As String _
                  , ByVal FechaLiq As Date, ByVal autContrato As Double, ByVal AutCopnBenef As Double) As Integer
Dim ArrCobe(), ArrTemp() As Variant
Dim i, ContCobe As Integer
Dim BandCuad, BandCiud, BandCuadEsp As Boolean
Dim Mens As String
Dim Band As Boolean
Dim PorcExclusiondiag   As Double
'buscar las coberturas en las que se cumple alguna condici�n
Campos = "NU_AUTO_COPL,TX_DESC_COPL,COUNT(*),''"
Desde = "(SELECT NU_AUTO_COPL,TX_DESC_COPL FROM COBERTURA_PLAN,R_COBEPLAN_TIPO_ATENCION WHERE"
Desde = Desde & " NU_AUTO_COPL=NU_AUTO_COPL_CPTA AND NU_AUTO_PLAN_COPL=" & AutPlan
Desde = Desde & " AND NU_AUTO_TIAT_CPTA=" & AutTiat
Desde = Desde & " UNION ALL SELECT NU_AUTO_COPL,TX_DESC_COPL FROM COBERTURA_PLAN,R_COBEPLAN_ATENCION"
Desde = Desde & " WHERE NU_AUTO_COPL=NU_AUTO_COPL_CPAT AND NU_AUTO_PLAN_COPL=" & AutPlan
Desde = Desde & " AND NU_AUTO_ATEN_CPAT=" & AutAten
Desde = Desde & " UNION ALL SELECT NU_AUTO_COPL,TX_DESC_COPL FROM COBERTURA_PLAN,R_COBEPLAN_PARENTESCO"
Desde = Desde & " WHERE NU_AUTO_COPL=NU_AUTO_COPL_CPPA AND NU_AUTO_PLAN_COPL=" & AutPlan
Desde = Desde & " AND NU_AUTO_PARE_CPPA=" & AutPare
Desde = Desde & " UNION ALL SELECT NU_AUTO_COPL,TX_DESC_COPL FROM COBERTURA_PLAN,R_COBEPLAN_CAUSA_EXTERNA"
Desde = Desde & " WHERE NU_AUTO_COPL=NU_AUTO_COPL_CPCE AND NU_AUTO_PLAN_COPL=" & AutPlan
Desde = Desde & " AND NU_AUTO_CAEX_CPCE=" & AutCaex
Desde = Desde & " UNION ALL SELECT DISTINCT NU_AUTO_COPL,TX_DESC_COPL FROM COBERTURA_PLAN,R_COBEPLAN_CLASIFPREST,R_PREST_CLASPREST"
Desde = Desde & " WHERE NU_AUTO_COPL=NU_AUTO_COPL_CPCP AND NU_AUTO_CLPR_CPCP=NU_AUTO_CLPR_PRCP"
Desde = Desde & " AND NU_AUTO_PLAN_COPL=" & AutPlan & " AND NU_AUTO_PRES_PRCP=" & AutPrest
Desde = Desde & " UNION ALL SELECT DISTINCT NU_AUTO_COPL,TX_DESC_COPL FROM COBERTURA_PLAN,R_COBEPLAN_CLASIFDIAG,R_DIAG_CLASDIAG"
Desde = Desde & " WHERE NU_AUTO_COPL=NU_AUTO_COPL_CPCD AND NU_AUTO_CLDI_CPCD=NU_AUTO_CLDI_DICD"
Desde = Desde & " AND NU_AUTO_PLAN_COPL=" & AutPlan & " AND NU_AUTO_DIAG_DICD=" & AutDiag
Desde = Desde & " UNION ALL SELECT NU_AUTO_COPL,TX_DESC_COPL FROM COBERTURA_PLAN,R_COBEPLAN_TIPO_NUCLEO"
Desde = Desde & " WHERE NU_AUTO_COPL=NU_AUTO_COPL_CPTN AND NU_AUTO_PLAN_COPL=" & AutPlan
Desde = Desde & " AND NU_AUTO_TINU_CPTN=" & AutTiNu & ") GROUP BY NU_AUTO_COPL,TX_DESC_COPL"
ReDim ArrCobe(3, 0)
Result = LoadMulData(Desde, Campos, NUL$, ArrCobe())
If Result <> FAIL And Encontro Then
   'BUSCAR LAS COBERTURAS QUE CUMPLEN TODAS LAS CONDICIONES
   For i = 0 To UBound(ArrCobe, 2)
      Campos = "COUNT(*)"
      Desde = "(SELECT DISTINCT NU_AUTO_COPL FROM COBERTURA_PLAN,R_COBEPLAN_TIPO_ATENCION WHERE"
      Desde = Desde & " NU_AUTO_COPL=NU_AUTO_COPL_CPTA AND NU_AUTO_PLAN_COPL=" & AutPlan
      Desde = Desde & " AND NU_AUTO_COPL=" & ArrCobe(0, i)
      Desde = Desde & " UNION ALL SELECT DISTINCT NU_AUTO_COPL FROM COBERTURA_PLAN,R_COBEPLAN_ATENCION"
      Desde = Desde & " WHERE NU_AUTO_COPL=NU_AUTO_COPL_CPAT AND NU_AUTO_PLAN_COPL=" & AutPlan
      Desde = Desde & " AND NU_AUTO_COPL=" & ArrCobe(0, i)
      Desde = Desde & " UNION ALL SELECT DISTINCT NU_AUTO_COPL FROM COBERTURA_PLAN,R_COBEPLAN_PARENTESCO"
      Desde = Desde & " WHERE NU_AUTO_COPL=NU_AUTO_COPL_CPPA AND NU_AUTO_PLAN_COPL=" & AutPlan
      Desde = Desde & " AND NU_AUTO_COPL=" & ArrCobe(0, i)
      Desde = Desde & " UNION ALL SELECT DISTINCT NU_AUTO_COPL FROM COBERTURA_PLAN,R_COBEPLAN_CAUSA_EXTERNA"
      Desde = Desde & " WHERE NU_AUTO_COPL=NU_AUTO_COPL_CPCE AND NU_AUTO_PLAN_COPL=" & AutPlan
      Desde = Desde & " AND NU_AUTO_COPL=" & ArrCobe(0, i)
'      Desde = Desde & " UNION ALL SELECT DISTINCT NU_AUTO_COPL FROM COBERTURA_PLAN,R_COBEPLAN_CLASIFPREST,R_PREST_CLASPREST"
'      Desde = Desde & " WHERE NU_AUTO_COPL=NU_AUTO_COPL_CPCP AND NU_AUTO_CLPR_CPCP=NU_AUTO_CLPR_PRCP"
'      Desde = Desde & " AND NU_AUTO_PLAN_COPL=" & AutPlan & " AND NU_AUTO_COPL=" & ArrCobe(0, i)
      Desde = Desde & " UNION ALL SELECT DISTINCT NU_AUTO_COPL FROM COBERTURA_PLAN,R_COBEPLAN_CLASIFPREST"
      Desde = Desde & " WHERE NU_AUTO_COPL=NU_AUTO_COPL_CPCP"
      Desde = Desde & " AND NU_AUTO_PLAN_COPL=" & AutPlan & " AND NU_AUTO_COPL=" & ArrCobe(0, i)
'      Desde = Desde & " UNION ALL SELECT DISTINCT NU_AUTO_COPL FROM COBERTURA_PLAN,R_COBEPLAN_CLASIFDIAG,R_DIAG_CLASDIAG"
'      Desde = Desde & " WHERE NU_AUTO_COPL=NU_AUTO_COPL_CPCD AND NU_AUTO_CLDI_CPCD=NU_AUTO_CLDI_DICD"
'      Desde = Desde & " AND NU_AUTO_PLAN_COPL=" & AutPlan & " AND NU_AUTO_COPL=" & ArrCobe(0, i)
      Desde = Desde & " UNION ALL SELECT DISTINCT NU_AUTO_COPL FROM COBERTURA_PLAN,R_COBEPLAN_CLASIFDIAG"
      Desde = Desde & " WHERE NU_AUTO_COPL=NU_AUTO_COPL_CPCD"
      Desde = Desde & " AND NU_AUTO_PLAN_COPL=" & AutPlan & " AND NU_AUTO_COPL=" & ArrCobe(0, i)
      Desde = Desde & " UNION ALL SELECT DISTINCT NU_AUTO_COPL FROM COBERTURA_PLAN,R_COBEPLAN_TIPO_NUCLEO"
      Desde = Desde & " WHERE NU_AUTO_COPL=NU_AUTO_COPL_CPTN AND NU_AUTO_PLAN_COPL=" & AutPlan
      Desde = Desde & " AND NU_AUTO_COPL=" & ArrCobe(0, i) & ")"
      ReDim ArrTemp(0)
      Result = LoadData(Desde, Campos, NUL$, ArrTemp())
      If Result <> FAIL And Encontro Then
         If ArrCobe(2, i) <> ArrTemp(0) Then
            ArrCobe(0, i) = ""
         End If
      End If
   Next
End If
For i = 0 To UBound(ArrCobe, 2)
   If ArrCobe(0, i) <> "" Then
      ReDim ArrTemp(2)
      Campos = "TX_CUAD_COPL,NU_AUTO_PRTD_COPL,TX_CIUD_COPL"
      condi = "NU_AUTO_COPL=" & ArrCobe(0, i)
      Result = LoadData("COBERTURA_PLAN", Campos, condi, ArrTemp())
      BandCuad = False
      BandCiud = False
      If Result <> FAIL And Encontro Then
         'Validar el tipo de cuadro
         ArrCobe(3, i) = ArrTemp(0)
         Select Case ArrTemp(0)
            Case "A": If TipCuad <> "C" Then BandCuad = True
            Case "C": If TipCuad = "C" Then BandCuad = True
'            Case "E": If TipCuad <> "C" And Arrtemp(1) = AutPrestador Then BandCuad = True: BandCuadEsp = True
            Case "E":
                     If CLng(ArrTemp(1)) = AutPrestador Then BandCuad = True: BandCuadEsp = True
            Case "T": BandCuad = True
         End Select
         'Validar la ciudad
         Select Case ArrTemp(2)
            Case "D": If TipCiud = "D" Then BandCiud = True
            Case "F": If TipCiud = "F" Then BandCiud = True
            Case "T": BandCiud = True
         End Select
         If Not (BandCuad And BandCiud) Then
            ArrCobe(0, i) = ""
         Else
            AutCobe = CDbl(ArrCobe(0, i))
            ContCobe = ContCobe + 1
         End If
      End If
   End If
Next
'quitar las coberturas con cuadro abierto o cuadro cerrado cuando hay coberturas que aplican de cuadro espec�fico
If ContCobe > 1 And BandCuadEsp Then
   ContCobe = 0
   For i = 0 To UBound(ArrCobe, 2)
      If ArrCobe(0, i) <> "" Then
         If ArrCobe(3, i) <> "E" Then
            ArrCobe(0, i) = ""
         Else
            ContCobe = ContCobe + 1
            AutCobe = CDbl(ArrCobe(0, i))
         End If
      End If
   Next
End If
Select Case ContCobe
   Case 0:  IndCobe = "N": AutCobe = 0: IndDeduCobe = "": TipCobe = "": VlCobe = 0: PrCobe = 0: CtCobe = 0
            ReDim ArrTemp(0)
            Desde = "COBERTURA_PLAN,R_COBEPLAN_CLASIFDIAG,R_DIAG_CLASDIAG"
            Campos = "DISTINCT NU_AUTO_COPL"
            condi = "NU_AUTO_COPL=NU_AUTO_COPL_CPCD"
            condi = condi & " AND NU_AUTO_CLDI_CPCD=NU_AUTO_CLDI_DICD"
            condi = condi & " AND NU_AUTO_PLAN_COPL=" & AutPlan & " AND NU_AUTO_DIAG_DICD=" & AutDiag
            Result = LoadData(Desde, Campos, condi, ArrTemp())
            If Result <> FAIL And Not Encontro Then Motivo = Motivo & "/ El Diagnostico no se encuentra en ninguna cobertura": Band = True
            Desde = "COBERTURA_PLAN,R_COBEPLAN_CLASIFPREST,R_PREST_CLASPREST"
            condi = "NU_AUTO_COPL=NU_AUTO_COPL_CPCP"
            condi = condi & " AND NU_AUTO_CLPR_CPCP=NU_AUTO_CLPR_PRCP"
            condi = condi & " AND NU_AUTO_PLAN_COPL=" & AutPlan & " AND NU_AUTO_PRES_PRCP=" & AutPrest
            Result = LoadData(Desde, Campos, condi, ArrTemp())
            If Result <> FAIL And Not Encontro Then Motivo = Motivo & "/ La Prestaci�n no se encuentra en ninguna cobertura": Band = True
            If Band = False Then Motivo = Motivo & "/ Para el diagnostico y la prestaci�n seleccionados no se encuentra ninguna cobertura"
            Buscar_CoberturaPrest = SUCCEED
            
   Case 1:
            Band = Validar_Carencias(AutCopnBenef, FechaLiq, AutTiat, AutDiag, AutPrest)
            If Band Then
               IndCobe = "N": AutCobe = 0: IndDeduCobe = "": TipCobe = "": VlCobe = 0: PrCobe = 0: CtCobe = 0
               Motivo = Motivo & "/ No ha superado el periodo de carencias"
            Else
               Band = validar_Exclusion(AutCopnBenef, AutDiag, "P", Empty, PorcExclusiondiag)
               If Band Then Motivo = Motivo & "/ La cobertura para el Diagn�stico ser� al " & PorcExclusiondiag & "%"
               Campos = "TX_DEDU_COPL,TX_TIPO_COPL,NU_VALO_COPL,NU_PORC_COPL,NU_CANT_COPL,TX_DESC_COPL"
               condi = "NU_AUTO_COPL=" & AutCobe
               ReDim ArrTemp(5)
               Result = LoadData("COBERTURA_PLAN", Campos, condi, ArrTemp())
               If Result <> FAIL And Encontro Then
                  IndCobe = "S"
                  IndDeduCobe = ArrTemp(0)
                  TipCobe = ArrTemp(1)
                  VlCobe = CDbl(ArrTemp(2))
                  PrCobe = CDbl(ArrTemp(3))
                  CtCobe = CDbl(ArrTemp(4))
                  NombCobe = ArrTemp(5)
               End If
            End If
            Buscar_CoberturaPrest = SUCCEED
            
   Case Else:
            For i = 0 To UBound(ArrCobe, 2)
               If ArrCobe(0, i) <> "" Then
                  Mens = Mens & ArrCobe(1, i) & Chr(13)
               End If
            Next
            Call Mensaje1("El servicio se encuentra en m�s de una cobertura" & Chr(13) & Mens, 3)
            Buscar_CoberturaPrest = FAIL
End Select
End Function

Public Function Valor_Referencia(ByVal AutoNumPlan As Long, ByVal AutoNumContr As Long, ByVal AutoNumPrest As Long, _
               ByVal Fecha As Date, ByRef ValItems() As Variant, ByVal AutItemManualTar As Double, _
               ByVal AutCiudad As Double) As Double
Dim ArrTemp() As Variant
Dim PrCiud, PrPlan, ValRef As Double
Dim TipPrCiud, TipPrPlan As String
Dim BandPrCiud, BandPrPlan As Boolean
Dim AutoNumManualTar As Long
Dim i As Integer
'Buscar si tiene factor de correcci�n geogr�fico
ReDim ArrTemp(1)
Campos = "NU_PORC_PLCI,TX_TIPORC_PLCI"
Desde = "R_PLAN_CIUDAD"
condi = "NU_AUTO_PLAN_PLCI=" & AutoNumPlan & " AND NU_AUTO_CIUD_PLCI=" & AutCiudad
Result = LoadData(Desde, Campos, condi, ArrTemp())
If Result <> FAIL And Encontro Then
   PrCiud = CDbl(ArrTemp(0))
   TipPrCiud = ArrTemp(1)
   BandPrCiud = True
Else
   PrCiud = 0
   TipPrCiud = NUL$
   BandPrCiud = False
End If
'BUSCAR EL MANUAL TARIFARIO DEL PLAN
Campos = "NU_AUTO_MATA_PLAN,TX_INPORC_PLAN,TX_TIPORC_PLAN,NU_POMATA_PLAN"
condi = "NU_AUTO_PLAN=" & AutoNumPlan
ReDim ArrTemp(3)
Result = LoadData("PLAN", Campos, condi, ArrTemp())
If Result <> FAIL And Encontro Then
   AutoNumManualTar = ArrTemp(0)
   BandPrPlan = IIf(ArrTemp(1) = "S", True, False)
   TipPrPlan = ArrTemp(2)
   PrPlan = CDbl(ArrTemp(3))
Else
   AutoNumManualTar = 0
   BandPrPlan = False
   TipPrPlan = ""
   PrPlan = 0
End If
'buscar el valor del servicio
ValRef = Valor_Prestacion(AutoNumManualTar, AutoNumPrest, Fecha, ValItems(), AutItemManualTar, "M")
If BandPrPlan And ValRef <> 0 Then 'si el plan tiene porcentaje de aumento o de descuento
   ValRef = 0
   For i = 0 To UBound(ValItems, 2)
      Select Case TipPrPlan
         Case "A": 'AUMENTO
                  ValItems(2, i) = ValItems(2, i) * (1 + (PrPlan / 100))
                  ValRef = ValRef + ValItems(2, i)
         Case "D": 'DESCUENTO
                  ValItems(2, i) = ValItems(2, i) * (1 - (PrPlan / 100))
                  ValRef = ValRef + ValItems(2, i)
      End Select
   Next
End If
If BandPrCiud And ValRef <> 0 Then 'si el plan tiene FACTOR DE CORRECCI�N GEOGRAFICA
   ValRef = 0
   For i = 0 To UBound(ValItems, 2)
      Select Case TipPrCiud
         Case "A": 'AUMENTO
                  ValItems(2, i) = ValItems(2, i) * (1 + (PrCiud / 100))
                  ValRef = ValRef + ValItems(2, i)
         Case "D": 'DESCUENTO
                  ValItems(2, i) = ValItems(2, i) * (1 - (PrCiud / 100))
                  ValRef = ValRef + ValItems(2, i)
      End Select
   Next
End If
Valor_Referencia = RoundConDec(ValRef)
End Function

'TipoValor=C convenio, toma el valor de punto del convenio
'TipoValor=M Manual tarifario,toma el valor de punto del manual tarifario
Public Function Valor_Prestacion(ByVal AutoNumManualTar, AutoNumPrest As Long, ByVal Fecha As Date, ByRef ValItems() As Variant, _
                                 ByVal AutItemManualTar As Double, ByVal TipoValor As String, Optional ByVal AutConvenio As Long) As Double
Dim ArrTemp(), ArrValItem() As Variant
Dim AutoNumPeri As Long
Dim ValUnid, ValPrest As Double
Dim i As Integer

'Buscar el periodo de la tarifa
Campos = "NU_AUTO_PETA"
condi = "NU_AUTO_MATA_PETA=" & AutoNumManualTar & " AND FE_FEIN_PETA<=" & FFechaCon(Fecha)
condi = condi & " AND FE_FEFI_PETA>=" & FFechaCon(Fecha)
ReDim ArrTemp(0)
Result = LoadData("PERIODO_TARIFA", Campos, condi, ArrTemp())
If Result <> FAIL And Encontro Then
   AutoNumPeri = CLng(ArrTemp(0))
Else
   Call Mensaje1("No se encontro un periodo del manual tarifario para la fecha: " & Fecha, 3)
   Exit Function
End If

'buscar la tarifa por cada item del manual tarifario
If TipoValor = "M" Then
   Campos = "NU_AUTO_MTIT,TX_DESC_MTIT,TX_TIPO_TARI,NU_CANT_TARI,NU_VALO_MTVP"
   Desde = "MANUAL_TARIFARIO_ITEM,TARIFA,MANUAL_VALOR_PUNTO"
   condi = "NU_AUTO_MTIT=NU_AUTO_MTIT_TARI AND NU_AUTO_MATA_MTIT=" & AutoNumManualTar
   condi = condi & " AND NU_AUTO_PRES_TARI=" & AutoNumPrest & " AND NU_AUTO_PETA_TARI=" & AutoNumPeri
   condi = condi & " AND NU_AUTO_MTIT_MTVP=NU_AUTO_MTIT AND NU_AUTO_PETA_MTVP=" & AutoNumPeri
   If AutItemManualTar <> 0 Then
      condi = condi & " AND NU_AUTO_MTIT_TARI=" & AutItemManualTar
   End If
ElseIf TipoValor = "C" Then
   Campos = "NU_AUTO_MTIT,TX_DESC_MTIT,TX_TIPO_TARI,NU_CANT_TARI,NU_VALO_VAPU"
   Desde = "MANUAL_TARIFARIO_ITEM,TARIFA,VALOR_PUNTO"
   condi = "NU_AUTO_MTIT=NU_AUTO_MTIT_TARI AND NU_AUTO_MATA_MTIT=" & AutoNumManualTar
   condi = condi & " AND NU_AUTO_PRES_TARI=" & AutoNumPrest & " AND NU_AUTO_PETA_TARI=" & AutoNumPeri
   condi = condi & " AND NU_AUTO_MTIT_VAPU=NU_AUTO_MTIT AND NU_AUTO_CONV_VAPU=" & AutConvenio
   If AutItemManualTar <> 0 Then
      condi = condi & " AND NU_AUTO_MTIT_TARI=" & AutItemManualTar
   End If
End If
ReDim ArrValItem(4, 0)

Result = LoadMulData(Desde, Campos, condi, ArrValItem())
If Result <> FAIL And Encontro Then
   ReDim ValItems(2, UBound(ArrValItem, 2))
   For i = 0 To UBound(ArrValItem, 2)
      ValItems(0, i) = ArrValItem(0, i)
      ValItems(1, i) = ArrValItem(1, i)
      Select Case ArrValItem(2, i)
         Case "U": 'liquidacion por unidades
'                  If i = 0 Then 'buscar el valor de la unidad
'                     condi = "NU_AUTO_GRPR_VLPT=NU_AUTO_GRPR_PRES AND NU_AUTO_PRES=" & AutoNumPrest
'                     condi = condi & " AND NU_AUTO_MATA_VLPT=" & AutoNumManualTar
'                     condi = condi & " AND NU_AUTO_PETA_TARI=" & AutoNumPeri
'                     ReDim ArrTemp(0)
'                     Result = LoadData("VALOR_PUNTO,PRESTACION", "NU_VALR_VLPT", condi, ArrTemp())
'                     If Result <> FAIL And Encontro Then
'                        ValUnid = IIf(ArrTemp(0) = "", 0, CDbl(ArrTemp(0)))
'                     Else
'                        Call Mensaje1("No se pudo obtener el valor de punto por grupo de prestaciones", 3)
'                        Exit Function
'                     End If
'                  End If
                  ValUnid = ArrValItem(4, i)
                  ValItems(2, i) = RoundConDec(ArrValItem(3, i) * ValUnid)
                  ValPrest = ValPrest + CDbl(ValItems(2, i))
         Case "V": 'liquidacion por valor
                  ValItems(2, i) = CDbl(ArrValItem(3, i))
                  ValPrest = ValPrest + CDbl(ValItems(2, i))
      End Select
   Next
   Valor_Prestacion = ValPrest
Else
   Call Mensaje1("No se pudo obtener el valor por item del manual tarifario para la prestaci�n", 3)
   Exit Function
End If
End Function

Public Function Valor_Prestacion_Convenio(ByVal AutonumConv As Long, ByVal AutoNumPrest As Long, ByVal Fecha As Date, _
               ByRef ValItems() As Variant, ByVal AutItemManualTar As Double) As Double
Dim ArrTemp() As Variant
Dim PrConv, ValPrest As Double
Dim TipPrConv As String
Dim BandPrConv As Boolean
Dim AutoNumManualTar As Long
Dim i As Integer

'BUSCAR EL MANUAL TARIFARIO DEL convenio
Campos = "NU_AUTO_MATA_COTA,TX_TIPO_COTA,NU_PORC_COTA"
condi = "NU_AUTO_GRPR_COTA=NU_AUTO_GRPR_PRES AND NU_AUTO_CONV_COTA=" & AutonumConv
condi = condi & " AND NU_AUTO_PRES=" & AutoNumPrest
ReDim ArrTemp(2)
Result = LoadData("R_CONVENIO_TARIFA,PRESTACION", Campos, condi, ArrTemp())
If Result <> FAIL And Encontro Then
   AutoNumManualTar = ArrTemp(0)
   BandPrConv = IIf(ArrTemp(1) = "P", False, True)
   TipPrConv = ArrTemp(1)
   PrConv = CDbl(ArrTemp(2))
Else
   AutoNumManualTar = 0
   BandPrConv = False
   TipPrConv = ""
   PrConv = 0
   Call Mensaje1("No se ha definido un manual tarifario en el convenio de este prestador para el grupo de prestaci�n al cual pertenece este servicio", 3)
   Exit Function
End If
'buscar el valor del servicio
ValPrest = Valor_Prestacion(AutoNumManualTar, AutoNumPrest, Fecha, ValItems(), AutItemManualTar, "C", AutonumConv)
If BandPrConv And ValPrest <> 0 Then 'si el plan tiene porcentaje de aumento o de descuento
   ValPrest = 0
   For i = 0 To UBound(ValItems, 2)
      Select Case TipPrConv
         Case "R": 'AUMENTO-RECARGO
                  ValItems(2, i) = ValItems(2, i) * (1 + (PrConv / 100))
                  ValPrest = ValPrest + ValItems(2, i)
         Case "D": 'DESCUENTO
                  ValItems(2, i) = ValItems(2, i) * (1 - (PrConv / 100))
                  ValPrest = ValPrest + ValItems(2, i)
      End Select
   Next
End If
Valor_Prestacion_Convenio = RoundConDec(ValPrest)
End Function

Public Function Buscar_DeduciblePrest(ByVal AutPlan As Long, ByVal AutTiat As Long, ByVal TipCuad As String _
               , ByVal AutPrestador As Long, ByVal VlLiq As Double, ByVal AutCont As Long, ByVal AutGrCi As Long _
               , ByVal AutBen As Long, ByVal AutTit As Long, ByVal NumeTran As Long, ByVal FechIniCont As Date _
               , ByVal FechFinCont As Date, ByVal FechPrest As Date, ByRef AutDedu As Double, ByRef VlDedu As Double _
               , ByVal AutTercBen As Long, ByVal AutTercTit As Long, ByVal TipoTrans As String) As Integer
Dim ArrDedu(), ArrTemp(), ArrAcum() As Variant
Dim i, j, ContDedu As Integer
Dim BandCuad, BandTiat As Boolean
Dim FechIniPer, FechFinPer As Date
Dim DurCont, CantPer As Integer
Dim valor As Double
Dim Cant As Integer
'buscar todos los deducibles del plan
Campos = "NU_AUTO_DEDU,TX_CUAD_DEDU,NU_AUTO_PRTD_DEDU"
Desde = "DEDUCIBLE_PLAN"
condi = "NU_AUTO_PLAN_DEDU=" & AutPlan
ReDim ArrDedu(2, 0)
Result = LoadMulData(Desde, Campos, condi, ArrDedu())
If Result <> FAIL And Encontro Then
   'verificar el tipo de cuadro de cada uno de los deducibles
   For i = 0 To UBound(ArrDedu, 2)
      BandCuad = False
      Select Case ArrDedu(1, i)
         Case "A": If TipCuad <> "C" Then BandCuad = True
         Case "C": If TipCuad = "C" Then BandCuad = True
'         Case "E": If TipCuad <> "C" And ArrDedu(2, i) = AutPrestador Then BandCuad = True
         Case "E": If ArrDedu(2, i) = AutPrestador Then BandCuad = True
         Case "T": BandCuad = True
      End Select
      If BandCuad = False Then ArrDedu(0, i) = ""
   Next
End If
For i = 0 To UBound(ArrDedu, 2)
   If ArrDedu(0, i) <> "" Then
      ReDim ArrTemp(0, 0)
      Campos = "NU_AUTO_TIAT_DPTA"
      condi = "NU_AUTO_DEDU_DPTA=" & ArrDedu(0, i)
      Result = LoadMulData("R_DEDUPLAN_TIPO_ATENCION", Campos, condi, ArrTemp())
      BandTiat = False
      If Result <> FAIL Then
         If Encontro Then
            For j = 0 To UBound(ArrTemp, 2)
               If ArrTemp(0, j) = AutTiat Then
                  BandTiat = True
                  Exit For
               End If
            Next
         Else
            BandTiat = True
         End If
      End If
      If Not (BandTiat) Then
         ArrDedu(0, i) = ""
      Else
         AutDedu = CDbl(ArrDedu(0, i))
         ContDedu = ContDedu + 1
      End If
      
   End If
Next
Select Case ContDedu
   Case 0:  VlDedu = 0
            Buscar_DeduciblePrest = SUCCEED
   Case 1:  'Buscar las caracter�sticas del deducible que cumple con las condiciones
            Campos = "TX_TIPO_DEDU,NU_VALO_DEDU,TX_PERI_DEDU,NU_CANT_DEDU,NU_PERI_DEDU,TX_INCA_DEDU,TX_PERS_DEDU"
            condi = "NU_AUTO_DEDU=" & AutDedu
            ReDim ArrTemp(6)
            Result = LoadData("DEDUCIBLE_PLAN", Campos, condi, ArrTemp())
            If Result <> FAIL And Encontro Then
               'CALCULAR EL VALOR TOTAL DEL DEDUCIBLE QUE SE DEBE COBRAR
               'calcular el deducible segun el tipo de deducible
               Select Case ArrTemp(0)
                  Case "V": VlDedu = CDbl(ArrTemp(1))
                  Case "P": VlDedu = VlLiq * (CDbl(ArrTemp(1)) / 100)
               End Select
               If ArrTemp(2) = "N" Then 'si el deducible no es periodico se cobra un deducible por cada reclamo
                  
'                  If ArrTemp(5) = "S" Then 'Si el deducible es por incapacidad
'                     ReDim ArrAcum(0)
'                     'buscar en el acumulado cuanto se ha cobrado en deducibles
'                     Campos = "SUM(NU_ACUM_REAC)"
'                     Desde = "REEMBOLSO_ACUMULADO"
'                     condi = "NU_AUTO_CONT_REAC=" & AutCont & " AND NU_AUTO_GRCI_REAC=" & AutGrCi
'                     condi = condi & "NU_AUTO_PLAN_REAC=" & AutPlan
'                     Select Case ArrTemp(6)
'                        Case "P": condi = condi & " AND NU_AUTO_PENABEN_REAC=" & AutBen
'                        Case "G": condi = condi & " AND NU_AUTO_PENATIT_REAC=" & AutTit
'                     End Select
'                     Result = LoadData(Desde, Campos, condi, ArrAcum())
'                     If ArrAcum(0) = "" Then ArrAcum(0) = 0
'                     'calcular cuanto se ha cobrado en deducibles Del Tipo que se va a aplicar
'                     Campos = "SUM(NU_AVLDED_REDE)+ " & ArrAcum(0)
'                     Desde = "TRANSITO,ESTADO_TRANSITO,REEMBOLSO,REEMBOLSO_FACTURA,REEMBOLSO_DETALLE"
'                     condi = "NU_NUME_TRAN=NU_NUME_TRAN_ESTR AND NU_NUME_TRAN=NU_NUME_TRAN_REEM"
'                     condi = condi & " AND NU_AUTO_REEM=NU_AUTO_REEM_REFA AND NU_AUTO_REFA=NU_AUTO_REFA_REDE"
'                     condi = condi & " AND TX_ESTA_ESTR IN('R','I','DI','A','P') AND TX_ACTU_ESTR='S'"
'                     condi = condi & " AND NU_AUTO_DEDU_REDE=" & AutDedu & " AND NU_AUTO_PLAN_TRAN=" & AutPlan
'                     condi = condi & " AND NU_AUTO_CONT_TRAN=" & AutCont & " AND NU_AUTO_GRCI_REEM=" & AutGrCi
'                     Select Case ArrTemp(6)
'                        Case "P": condi = condi & " AND NU_AUTO_PENABEN_TRAN=" & AutBen
'                        Case "G": condi = condi & " AND NU_AUTO_PENATIT_TRAN=" & AutTit
'                     End Select
'                     ReDim ArrAcum(0)
'                     Result = LoadData(Desde, Campos, condi, ArrAcum())
'                     If Result <> FAIL And Encontro Then
'                        'CALCULA EL SALDO QUE SE PUEDE APLICAR DEL DEDUCIBLE
'                        If ArrAcum(0) = "" Then ArrAcum(0) = 0
'                        If CDbl(ArrAcum(0)) >= VlDedu Then
'                           VlDedu = 0
'                        Else
'                           VlDedu = VlDedu - CDbl(ArrAcum(0))
'                        End If
'                     End If
'                  ElseIf ArrTemp(5) = "N" Then 'Si el deducible NO es por incapacidad
'                 End If

                  'buscar de este deducible cuanto se ha aplicado en el reembolso o credito actual
                  If TipoTrans = "R" Then
                     Campos = "SUM(NU_AVLDED_REDE)"
                     Desde = "REEMBOLSO,REEMBOLSO_FACTURA,REEMBOLSO_DETALLE"
                     condi = "NU_AUTO_REEM=NU_AUTO_REEM_REFA AND NU_AUTO_REFA=NU_AUTO_REFA_REDE"
                     condi = condi & " AND NU_NUME_TRAN_REEM=" & NumeTran & " AND NU_AUTO_DEDU_REDE=" & AutDedu
                     ReDim ArrAcum(0)
                     Result = LoadData(Desde, Campos, condi, ArrAcum())
                     If Result <> FAIL And Encontro Then
                        If ArrAcum(0) = "" Then ArrAcum(0) = 0
                        'calcula el saldo que se puede aplicar del deducible
                        If CDbl(ArrAcum(0)) >= VlDedu Then
                           VlDedu = 0
                        Else
                           VlDedu = VlDedu - CDbl(ArrAcum(0))
                        End If
                     End If
                  ElseIf TipoTrans = "C" Then   'EA
                        Campos = "SUM(NU_VLDED_CRDE_TMP)"
                        Desde = "CREDITO_DETALLE_TMP"
                        condi = "NU_AUTO_CONE_CRDE_TMP=" & NumeTran & " AND NU_AUTO_DEDU_CRDE_TMP=" & AutDedu
                        ReDim ArrAcum(0)
                        Result = LoadData(Desde, Campos, condi, ArrAcum())
                        If Result <> FAIL And Encontro Then
                           If ArrAcum(0) = "" Then ArrAcum(0) = 0
                           'calcula el saldo que se puede aplicar del deducible
                           If CDbl(ArrAcum(0)) >= VlDedu Then
                              VlDedu = 0
                           Else
                              VlDedu = VlDedu - CDbl(ArrAcum(0))
                           End If
                        End If
                  End If
               
               ElseIf ArrTemp(2) = "S" Then 'si el deducible es periodico
                  'DurCont = DateDiff("m", FechIniCont, FechFinCont) 'calclulo la duraci�n del contrato en meses   Modificado EA 19-03-2004
                  DurCont = Cantidad_Meses(FechIniCont, FechFinCont)
                  If CInt(ArrTemp(4)) <= DurCont Then 'si la cantidad de meses del periodo es mayor que la del contrato
                     CantPer = DurCont Mod CInt(ArrTemp(4)) 'calcular la cantidad de periodos
                     If CantPer <> 0 Then
                        CantPer = (DurCont / CInt(ArrTemp(4))) + 1
                     Else
                        CantPer = DurCont / CInt(ArrTemp(4))
                     End If
                  Else
                     CantPer = 1
                  End If
                  FechIniPer = FechIniCont
                  For i = 1 To CantPer 'buscar el periodo en el que se encuentra el reembolso
                     FechFinPer = DateAdd("m", CInt(ArrTemp(4)), FechIniPer)
                     If FechFinPer > FechFinCont Then FechFinPer = FechFinCont
                     If FechPrest >= FechIniPer And FechPrest <= FechFinPer Then Exit For
                     FechIniPer = FechFinPer
                  Next
                  Select Case ArrTemp(0)
                     Case "V": 'si el deducible es un valor
                              'buscar en el acumulado cuanto se ha cobrado en deducibles
                              ReDim ArrAcum(0)
                              Campos = "SUM(NU_DEDU_REAC)"
                              Desde = "REEMBOLSO_ACUMULADO,PERSONA_NATURAL BEN,PERSONA_NATURAL TIT"
                              condi = "NU_AUTO_CONT_REAC=" & AutCont
                              condi = condi & " AND NU_AUTO_PENABEN_REAC=BEN.NU_AUTO_PENA"
                              condi = condi & " AND NU_AUTO_PENATIT_REAC=TIT.NU_AUTO_PENA"
                              condi = condi & " AND NU_AUTO_PLAN_REAC=" & AutPlan & " AND NU_AUTO_TIAT_REAC=" & AutTiat
                              If ArrTemp(5) = "S" Then ' si el deducible es por incapacidad
                                 condi = condi & " AND NU_AUTO_GRCI_REAC=" & AutGrCi
                              End If
                              Select Case ArrTemp(6)
                                 Case "P": condi = condi & " AND BEN.NU_AUTO_TERC_PENA=" & AutTercBen
                                 Case "G": condi = condi & " AND TIT.NU_AUTO_TERC_PENA=" & AutTercTit
                              End Select
                              Result = LoadData(Desde, Campos, condi, ArrAcum())
                              If ArrAcum(0) = "" Then ArrAcum(0) = 0
                              valor = CDbl(ArrAcum(0))
                              'calcular cuanto se ha cobrado en deducibles Del Tipo que se va a aplicar
                              Campos = "SUM(NU_AVLDED_REDE)"
                              Desde = "TRANSITO,ESTADO_TRANSITO,REEMBOLSO,REEMBOLSO_FACTURA,REEMBOLSO_DETALLE"
                              condi = "NU_NUME_TRAN=NU_NUME_TRAN_ESTR AND NU_NUME_TRAN=NU_NUME_TRAN_REEM"
                              condi = condi & " AND NU_AUTO_REEM=NU_AUTO_REEM_REFA AND NU_AUTO_REFA=NU_AUTO_REFA_REDE"
                              condi = condi & " AND TX_ESTA_ESTR IN('RR','IR','DIR','AR','PR') AND TX_ACTU_ESTR='S'"
                              condi = condi & " AND NU_AUTO_DEDU_REDE=" & AutDedu & " AND NU_AUTO_PLAN_TRAN=" & AutPlan
                              condi = condi & " AND FE_FACT_REFA>=" & FFechaIns(CStr(FechIniPer))
                              condi = condi & " AND FE_FACT_REFA<=" & FFechaIns(CStr(FechFinPer))
                              condi = condi & " AND NU_AUTO_CONT_TRAN=" & AutCont
                              If ArrTemp(5) = "S" Then 'Si el deducible es por incapacidad
                                 condi = condi & " AND NU_AUTO_GRCI_REEM=" & AutGrCi
                              End If
                              Select Case ArrTemp(6)
                                 Case "P": condi = condi & " AND NU_AUTO_COPNBEN_TRAN=" & AutBen
                                 Case "G": condi = condi & " AND NU_AUTO_COPNTIT_TRAN=" & AutTit
                              End Select
                              ReDim ArrAcum(0)
                              Result = LoadData(Desde, Campos, condi, ArrAcum())
                              If ArrAcum(0) = "" Then ArrAcum(0) = 0
                              '********* creditos ****************
                              valor = valor + CDbl(ArrAcum(0))
                              Campos = "SUM(NU_VLDED_CRDE)"
                              Desde = "TRANSITO,ESTADO_TRANSITO,CREDITO,CREDITO_DETALLE"
                              condi = "NU_NUME_TRAN=NU_NUME_TRAN_ESTR AND NU_NUME_TRAN=NU_NUME_TRAN_CRED"
                              condi = condi & " AND NU_AUTO_CRED=NU_AUTO_CRED_CRDE"
                              condi = condi & " AND TX_ESTA_ESTR IN('IC','AC') AND TX_ACTU_ESTR='S'"
                              condi = condi & " AND NU_AUTO_DEDU_CRDE=" & AutDedu & " AND NU_AUTO_PLAN_TRAN=" & AutPlan
                              condi = condi & " AND FE_PRES_CRED>=" & FFechaIns(CStr(FechIniPer))
                              condi = condi & " AND FE_PRES_CRED<=" & FFechaIns(CStr(FechFinPer))
                              condi = condi & " AND NU_AUTO_CONT_TRAN=" & AutCont
                              If ArrTemp(5) = "S" Then 'Si el deducible es por incapacidad
                                 condi = condi & " AND NU_AUTO_GRCI_CRED=" & AutGrCi
                              End If
                              Select Case ArrTemp(6)
                                 Case "P": condi = condi & " AND NU_AUTO_COPNBEN_TRAN=" & AutBen
                                 Case "G": condi = condi & " AND NU_AUTO_COPNTIT_TRAN=" & AutTit
                              End Select
                              ReDim ArrAcum(0)
                              Result = LoadData(Desde, Campos, condi, ArrAcum())
                              If TipoTrans = "C" Then   'EA
                                 If ArrAcum(0) = "" Then ArrAcum(0) = 0
                                 valor = valor + CDbl(ArrAcum(0))
                                 Campos = "SUM(NU_VLDED_CRDE_TMP)"
                                 Desde = "CREDITO_DETALLE_TMP"
                                 condi = "NU_AUTO_CONE_CRDE_TMP=" & NumeTran & " AND NU_AUTO_DEDU_CRDE_TMP=" & AutDedu
                                 ReDim ArrAcum(0)
                                 Result = LoadData(Desde, Campos, condi, ArrAcum())
                              End If
                              If Result <> FAIL And Encontro Then
                                 If ArrAcum(0) = "" Then ArrAcum(0) = 0
                                 valor = valor + CDbl(ArrAcum(0))
                                 If valor >= (CDbl(ArrTemp(1)) * CDbl(ArrTemp(3))) Then
                                    VlDedu = 0
                                 Else
                                    VlDedu = (CDbl(ArrTemp(1)) * CDbl(ArrTemp(3))) - valor
                                    If VlDedu > CDbl(ArrTemp(1)) Then
                                       VlDedu = CDbl(ArrTemp(1))
                                    End If
                                 End If
                              End If
                     Case "P": 'si el deducible es un porcentaje
                              'BUSCAR LA CANTIDAD DE DEDUCIBLES QUE SE HAN COBRADO
                              Campos = "COUNT(*)"
                              Desde = "(SELECT NU_NUME_TRAN FROM TRANSITO,ESTADO_TRANSITO,REEMBOLSO,REEMBOLSO_FACTURA,REEMBOLSO_DETALLE"
                              Desde = Desde & " WHERE NU_NUME_TRAN=NU_NUME_TRAN_ESTR AND NU_NUME_TRAN=NU_NUME_TRAN_REEM"
                              Desde = Desde & " AND NU_AUTO_REEM=NU_AUTO_REEM_REFA AND NU_AUTO_REFA=NU_AUTO_REFA_REDE"
                              Desde = Desde & " AND TX_ESTA_ESTR IN('RR','IR','DIR','AR','PR') AND TX_ACTU_ESTR='S'"
                              Desde = Desde & " AND NU_AUTO_DEDU_REDE=" & AutDedu & " AND NU_AUTO_PLAN_TRAN=" & AutPlan
                              Desde = Desde & " AND FE_FACT_REFA>=" & FFechaIns(CStr(FechIniPer))
                              Desde = Desde & " AND FE_FACT_REFA<=" & FFechaIns(CStr(FechFinPer))
                              Desde = Desde & " AND NU_AUTO_CONT_TRAN=" & AutCont
                              If ArrTemp(5) = "S" Then 'Si el deducible es por incapacidad
                                 Desde = Desde & " AND NU_AUTO_GRCI_REEM=" & AutGrCi
                              End If
                              Select Case ArrTemp(6)
                                 Case "P": Desde = Desde & " AND NU_AUTO_COPNBEN_TRAN=" & AutBen
                                 Case "G": Desde = Desde & " AND NU_AUTO_COPNTIT_TRAN=" & AutTit
                              End Select
                              Desde = Desde & " GROUP BY NU_NUME_TRAN)"
                              ReDim ArrAcum(0)
                              Result = LoadData(Desde, Campos, NUL$, ArrAcum())
                              '********* creditos ****************
                              If ArrAcum(0) = "" Then ArrAcum(0) = 0          'EA
                              Cant = CDbl(ArrAcum(0))
                              Campos = "COUNT(*)"
                              Desde = "(SELECT NU_NUME_TRAN FROM TRANSITO,ESTADO_TRANSITO,CREDITO,CREDITO_DETALLE"
                              Desde = Desde & " WHERE NU_NUME_TRAN=NU_NUME_TRAN_ESTR AND NU_NUME_TRAN=NU_NUME_TRAN_CRED"
                              Desde = Desde & " AND NU_AUTO_CRED=NU_AUTO_CRED_CRDE"
                              Desde = Desde & " AND TX_ESTA_ESTR IN('IC','AC') AND TX_ACTU_ESTR='S'"
                              Desde = Desde & " AND NU_AUTO_DEDU_CRDE=" & AutDedu & " AND NU_AUTO_PLAN_TRAN=" & AutPlan
                              Desde = Desde & " AND FE_PRES_CRED>=" & FFechaIns(CStr(FechIniPer))
                              Desde = Desde & " AND FE_PRES_CRED<=" & FFechaIns(CStr(FechFinPer))
                              Desde = Desde & " AND NU_AUTO_CONT_TRAN=" & AutCont
                              If ArrTemp(5) = "S" Then
                                 Desde = Desde & " AND NU_AUTO_GRCI_CRED=" & AutGrCi
                              End If
                              Select Case ArrTemp(6)
                                 Case "P": Desde = Desde & " AND NU_AUTO_COPNBEN_TRAN=" & AutBen
                                 Case "G": Desde = Desde & " AND NU_AUTO_COPNTIT_TRAN=" & AutTit
                              End Select
                              Desde = Desde & " GROUP BY NU_NUME_TRAN)"
                              ReDim ArrAcum(0)
                              Result = LoadData(Desde, Campos, NUL$, ArrAcum())
                              If TipoTrans = "C" Then   'EA
                                 If ArrAcum(0) = "" Then ArrAcum(0) = 0
                                 Cant = Cant + CDbl(ArrAcum(0))
                                 Campos = "COUNT(*)"
                                 Desde = "(SELECT NU_AUTO_CONE_CRDE_TMP FROM CREDITO_DETALLE_TMP"
                                 Desde = Desde & " WHERE NU_AUTO_CONE_CRDE_TMP=" & NumeTran & " AND NU_AUTO_DEDU_CRDE_TMP=" & AutDedu
                                 Desde = Desde & " GROUP BY NU_AUTO_CONE_CRDE_TMP)"
                                 ReDim ArrAcum(0)
                                 Result = LoadData(Desde, Campos, NUL$, ArrAcum())
                              End If
                              If Result <> FAIL And Encontro Then
                                 If ArrAcum(0) = "" Then ArrAcum(0) = 0
                                 Cant = Cant + CDbl(ArrAcum(0))
                                 If Cant >= CDbl(ArrTemp(3)) Then
                                    VlDedu = 0
                                 Else
                                    VlDedu = VlLiq * (CDbl(ArrTemp(1)) / 100)
                                 End If
                              End If
                  End Select
               End If
               'Ajustar el valor del deducible con el valor liquidado o de la prestaci�n
               If VlDedu > VlLiq Then
                  VlDedu = VlLiq
               End If
            End If
            VlDedu = RoundConDec(VlDedu)
            Buscar_DeduciblePrest = SUCCEED
   Case Else: Call Mensaje1("M�s de un Deducible aplica para esta prestaci�n", 3)
               Buscar_DeduciblePrest = FAIL
End Select
End Function
'RETORNA FALSE SI EL CONTRATO NO ESTA EN MORA
'RETORNA TRUE SI EL CONTRATO ESTA EN MORA
Public Function Buscar_MoraXFecha(ByVal AutCont As Long, ByVal Fecha As Date, Optional ByVal Autotiat As Double, Optional ByVal autonegocio As Double) As Boolean
Dim ArrTemp()     As Variant
Dim arrtmp()      As Variant
Dim i             As Integer
Dim j             As Integer
Dim dia           As Integer
Dim FechIniCuot   As Date
Dim FechGenFact   As Date
Dim FechMora      As Date
Dim Msg           As String
Dim Saldo         As Double
Dim Cuota         As Integer

ReDim arr(0)
   '0.Buscar el numero de dias de mora de acuerdo al tipo de atencion y el negocio
   Campos = "NU_DIAS_NETA"
   Desde = "R_NEGOCIO_TIPO_ATENCION"
   condi = "NU_AUTO_NEGO_NETA = " & autonegocio
   condi = condi & " AND NU_AUTO_TIAT_NETA = " & Autotiat
   Result = LoadData(Desde, Campos, condi, arr())
   If Result <> FAIL And Encontro Then
      dia = CInt(arr(0))
   End If

   '1.buscar a que cuota del contrato pertenece la fecha
   ReDim arr(0)
   Campos = "NU_NUME_CUOT"
   Desde = "CUOTA_CONTRATO"
   condi = "NU_AUTO_CONT_CUOT=" & AutCont & " AND FE_INIC_CUOT<=" & FFechaCon(CStr(Fecha))
   condi = condi & " AND FE_VENC_CUOT>=" & FFechaCon(CStr(Fecha))
   condi = condi & " AND TX_ESTA_CUOT = 'F'"
   Result = LoadData(Desde, Campos, condi, arr())
   If Result <> FAIL And Encontro Then
      '2.Busca los autonumericos de las cuotas a la fecha actual
      Cuota = arr(0)
      ReDim arrtmp(0, 0)
      Campos = "NU_AUTO_CUOT"
      Desde = "CUOTA_CONTRATO"
      condi = "NU_AUTO_CONT_CUOT=" & AutCont
      condi = condi & " AND TX_ESTA_CUOT = 'F'"
      condi = condi & " AND NU_NUME_CUOT <= " & Cuota
      Result = LoadMulData(Desde, Campos, condi, arrtmp())
      If Result <> FAIL And Encontro Then
         For i = 0 To UBound(arrtmp, 2)
            '3.verificar que la cuota tenga saldos
            ReDim ArrTemp(4, 0)
            Campos = "NU_AUTO_TIMO, TX_FACT_TIMO, FE_EMIS_TIMO,NU_SALD_TIMO,NU_NUME_CUOT"
            Desde = "CUOTA_CONTRATO,R_CUOTA_TIPO_MOVIMIENTO,TIPO_MOVIMIENTO"
            Condicion = "NU_AUTO_CUOT = NU_AUTO_CUOT_CUTI"
            Condicion = Condicion & " AND NU_AUTO_TIMO_CUTI = NU_AUTO_TIMO"
            Condicion = Condicion & " AND TX_ESTA_TIMO <> 'A'"
            Condicion = Condicion & " AND TX_ESTA_TIMO <> 'C'"
            Condicion = Condicion & " AND TX_TIPO_TIMO IN ('FA','ND')"
            Condicion = Condicion & " AND NU_AUTO_CUOT_CUTI=" & CLng(arrtmp(0, i))
            Result = LoadMulData(Desde, Campos, Condicion, ArrTemp())
            If Result <> FAIL And Encontro Then
               For j = 0 To UBound(ArrTemp, 2)
                  '4. busca la cuota en la cual fue emitida el documento
                  FechGenFact = ArrTemp(2, j)
                  If Not IsNumeric(ArrTemp(3, j)) Then ArrTemp(3, j) = 0
                  Saldo = CDbl(ArrTemp(3, j))
                  ReDim arr(0)
                  Campos = "FE_INIC_CUOT"
                  Desde = "CUOTA_CONTRATO"
                  condi = "NU_AUTO_CONT_CUOT=" & AutCont & " AND NU_NUME_CUOT =" & CLng(ArrTemp(4, j))
                  Result = LoadData(Desde, Campos, condi, arr())
                  If Result <> FAIL Then
                     FechIniCuot = arr(0)
                     If FechGenFact > FechIniCuot Then
                        FechMora = DateAdd("d", dia, FechGenFact)
                     Else
                        FechMora = DateAdd("d", dia, FechIniCuot)
                     End If
                     '5.buscar cuando se cancelo la cuota.
                     ReDim arr(1)
                     Campos = "NU_DOCS_RECA,FE_FECH_RECA"
                     Desde = "RECAUDO"
                     Condicion = "NU_AUTO_TIMO_RECA =" & CLng(ArrTemp(0, j))
                     Condicion = Condicion & " AND TX_ESTA_RECA <> 'AN'"
                     Result = LoadData(Desde, Campos, Condicion, arr())
                     If Result <> FAIL And Encontro Then
                        If CDate(arr(1)) > FechMora And Saldo <> 0 Then
                           Buscar_MoraXFecha = True
                           Call Mensaje1("El contrato se encuentra en mora, " & "documento: " & ArrTemp(1, j), 3)
                           Exit For
                        Else
                           Buscar_MoraXFecha = False
                        End If
                     Else
                        If Fecha > FechMora Then
                           Buscar_MoraXFecha = True
                           Call Mensaje1("El contrato se encuentra en mora, " & "documento: " & ArrTemp(1, j), 3)
                           Exit For
                        Else
                           Buscar_MoraXFecha = False
                        End If
                     End If
                  End If
               Next
            End If
            If Buscar_MoraXFecha = True Then Exit For
         Next
      Else
         Call Mensaje1("El contrato no se encuentra facturado", 3)
         Buscar_MoraXFecha = True
      End If
   Else
      Call Mensaje1("El contrato no se encuentra facturado", 3)
      Buscar_MoraXFecha = True
   End If
   
   '6 Busca si existen copagos pendientes por pagar para el contrato
   ReDim arrtmp(0, 0)
   Campos = "TX_FACT_TIMO"
   Desde = "TIPO_MOVIMIENTO"
   condi = "TX_TIPO_TIMO = 'CO' AND TX_ESTA_TIMO = 'S'"
   condi = condi & " AND NU_AUTO_CONT_TIMO = " & AutCont
   condi = condi & " AND FE_GENE_TIMO >= (SELECT FE_INIC_CONT FROM CONTRATO WHERE NU_AUTO_CONT = " & AutCont & ")"
   Result = LoadMulData(Desde, Campos, condi, arrtmp())
   If Result <> FAIL And Encontro Then
      For i = 0 To UBound(arrtmp, 2)
         If i = 0 Then Msg = arrtmp(0, i) & Coma
         If i > 0 Then Msg = Msg & arrtmp(0, i) & Coma
      Next
      Msg = Mid(Msg, 1, Len(Msg) - 1)
      Call Mensaje1("El contrato tiene copagos pendientes, " & "documento: " & Msg, 3)
   End If

End Function

'''''RECIBE AUTO DEL CONTRATO, AFILIADO, ATENCION, DX, PRESTACION
'''''RETORNA TRUE SI ESTA EN CARENCIAS EL AFILIADO
'''''RETORNA FALSE SI NO ESTA EN CARENCIAS EL AFILIADO
''''Public Function Validar_Carencias(ByVal AutCopnBen As Double, ByVal Fecha As Date, ByVal AutTipAtencion As Double, _
''''                                 ByVal AutDiag As Double, ByVal AutPrestacion As Double) As Boolean
''''Dim ArrCaren() As Variant
''''Dim ArrTemp() As Variant
''''Dim Cad As Variant
''''Dim ContCaren As Integer
''''Dim dias As Integer
''''Dim Horas As Integer
''''Dim meses As Integer
''''Dim Mens As String
''''Dim i As Integer
''''Dim BandTiat As Boolean, BandDiag As Boolean, BandPrestacion As Boolean
''''
''''   ReDim ArrTemp(1)
''''   condi = "NU_AUTO_COPN = " & AutCopnBen
''''   Result = LoadData("R_CONTRATO_AFILIADO", "TX_CARE_COPN,FE_PLAN_COPN", condi, ArrTemp())
''''   If Result <> FAIL And Encontro Then
''''      If ArrTemp(0) = "N" Then Validar_Carencias = True: Exit Function 'si el afiliado no pasa carencias
''''      'si el afiliado pasa carencias
''''      dias = Abs(DateDiff("D", CDate(ArrTemp(1)), Fecha))
''''      Horas = dias * 24
''''      meses = dias / 30
''''
''''      'CARGAR LAS CARENCIAS DEL AFILIADO
''''      ReDim ArrCaren(4, 0)
''''      Campos = "NU_AUTO_CARE,TX_DESC_CARE,TX_TIPE_CARE,NU_CANTPE_CARE,'N'"
''''      Desde = "R_CONTRATO_AFILIADO,GRUPO_CARENCIA,CARENCIA"
''''      condi = "NU_AUTO_GRCA_COPN=NU_AUTO_GRCA AND NU_AUTO_GRCA=NU_AUTO_GRCA_CARE"
''''      condi = condi & " AND NU_AUTO_COPN=" & AutCopnBen
''''      Result = LoadMulData(Desde, Campos, condi, ArrCaren())
''''      If Result <> FAIL And Encontro Then
''''         'QUITAR LAS CARENCIAS QUE YA SE SUPERARON
''''         For i = 0 To UBound(ArrCaren, 2)
''''            Select Case ArrCaren(2, i)
''''               Case "D":
''''                        If ArrCaren(3, i) < dias Then ArrCaren(4, i) = "S"
''''               Case "H":
''''                        If ArrCaren(3, i) < Horas Then ArrCaren(4, i) = "S"
''''               Case "M":
''''                        If ArrCaren(3, i) < meses Then ArrCaren(4, i) = "S"
''''            End Select
''''         Next
''''      End If
''''      'Verificar las condiciones de las carencias que no se han superado
''''      For i = 0 To UBound(ArrCaren, 2)
''''         If ArrCaren(4, i) = "N" Then
''''            'validar el tipo de atenci�n
''''            ReDim ArrTemp(0)
''''            Campos = "NU_AUTO_CARE"
''''            Desde = "CARENCIA,R_CARENCIA_TIPO_ATENCION"
''''            condi = "NU_AUTO_CARE=NU_AUTO_CARE_CATA AND NU_AUTO_CARE=" & ArrCaren(0, i)
''''            condi = condi & " AND NU_AUTO_TIAT_CATA=" & AutTipAtencion
''''            Result = LoadData(Desde, Campos, condi, ArrTemp())
''''            If Result <> FAIL And Encontro Then
''''               BandTiat = True
''''            End If
''''            'validar la clasificaci�n de Diagn�stico
''''            ReDim ArrTemp(0)
''''            Campos = "NU_AUTO_CARE"
''''            Desde = "CARENCIA,CLASIFICACION_DIAGNOSTICO,R_DIAG_CLASDIAG"
''''            condi = "NU_AUTO_CLDI_CARE=NU_AUTO_CLDI AND NU_AUTO_CLDI=NU_AUTO_CLDI_DICD"
''''            condi = condi & " AND NU_AUTO_DIAG_DICD=" & AutDiag
''''            condi = condi & " AND NU_AUTO_CARE=" & ArrCaren(0, i)
''''            Result = LoadData(Desde, Campos, condi, ArrTemp())
''''            If Result <> FAIL And Encontro Then
''''               BandDiag = True
''''            End If
''''            'validar la clasificaci�n de Prestaciones
''''            ReDim ArrTemp(0)
''''            Campos = "NU_AUTO_CARE"
''''            Desde = "CARENCIA,CLASIFICACION_PRESTACION,R_PREST_CLASPREST"
''''            condi = "NU_AUTO_CLPR_CARE=NU_AUTO_CLPR AND NU_AUTO_CLPR=NU_AUTO_CLPR_PRCP"
''''            condi = condi & " AND NU_AUTO_PRES_PRCP=" & AutDiag
''''            condi = condi & " AND NU_AUTO_CARE=" & ArrCaren(0, i)
''''            Result = LoadData(Desde, Campos, condi, ArrTemp())
''''            If Result <> FAIL And Encontro Then
''''               BandPrestacion = True
''''            End If
''''            If (AutTipAtencion <> 0 And BandTiat) Or AutTipAtencion = 0 Then BandTiat = True
''''            If (AutDiag <> 0 And BandDiag) Or AutDiag = 0 Then BandDiag = True
''''            If (AutPrestacion <> 0 And BandPrestacion) Or AutPrestacion = 0 Then BandPrestacion = True
''''            If Not BandTiat Or Not BandDiag Or Not BandPrestacion Then
''''               ArrCaren(4, i) = "N"
''''            End If
''''         End If
''''      Next
''''      For i = 0 To UBound(ArrCaren, 2)
''''         If ArrCaren(4, i) = "N" Then
''''            ContCaren = ContCaren + 1
''''            Mens = Mens & Chr(13) & ArrCaren(1, i)
''''         End If
''''      Next
''''      If Mens <> "" Then
''''         If ContCaren = UBound(ArrCaren, 2) + 1 Then
''''            Validar_Carencias = True
''''            Call Mensaje1("El afiliado no ha superado los periodos de carencia", 3)
''''         Else
''''            Call Mensaje1("El afiliado no ha superado los siguientes periodos de carencia: " & Chr(13) & Mens, 3)
''''         End If
''''      Else
''''         Validar_Carencias = False
''''      End If
''''
''''   End If
''''End Function

'RECIBE AUTO DEL CONTRATO, AFILIADO, ATENCION, DX, PRESTACION
'RETORNA TRUE SI ESTA EN CARENCIAS EL AFILIADO
'RETORNA FALSE SI NO ESTA EN CARENCIAS EL AFILIADO
Public Function Validar_Carencias(ByVal AutCopnBen As Double, ByVal Fecha As Date, ByVal AutTipAtencion As Double, _
                                 ByVal AutDiag As Double, ByVal AutPrestacion As Double) As Boolean
Dim ArrCaren() As Variant
Dim ArrTemp() As Variant
Dim Cad As Variant
Dim ContCaren As Integer
Dim dias As Integer
Dim Horas As Integer
Dim meses As Integer
Dim Mens As String
Dim i As Integer
Dim j As Integer
Dim TipCare  As String
Dim BandTiat As Boolean, BandDiag As Boolean, BandPrestacion As Boolean
Dim cont As Integer

   ReDim ArrTemp(1)
   condi = "NU_AUTO_COPN = " & AutCopnBen
   Result = LoadData("R_CONTRATO_AFILIADO", "TX_CARE_COPN,FE_PLAN_COPN", condi, ArrTemp())
   If Result <> FAIL And Encontro Then
      If ArrTemp(0) = "N" Then Validar_Carencias = False: Exit Function 'si el afiliado no pasa carencias
      'si el afiliado pasa carencias
      dias = Abs(DateDiff("D", CDate(ArrTemp(1)), Fecha))
      Horas = dias * 24
      meses = dias / 30
            
      'CARGAR LAS CARENCIAS DEL AFILIADO
      ReDim ArrCaren(5, 0)
      Campos = "NU_AUTO_CARE,TX_DESC_CARE,TX_TIPE_CARE,NU_CANTPE_CARE,'N',''"
      Desde = "R_CONTRATO_AFILIADO,GRUPO_CARENCIA,CARENCIA"
      condi = "NU_AUTO_GRCA_COPN=NU_AUTO_GRCA AND NU_AUTO_GRCA=NU_AUTO_GRCA_CARE"
      condi = condi & " AND NU_AUTO_COPN=" & AutCopnBen
      Result = LoadMulData(Desde, Campos, condi, ArrCaren())
      If Result <> FAIL And Encontro Then
         'QUITAR LAS CARENCIAS QUE YA SE SUPERARON
         For i = 0 To UBound(ArrCaren, 2)
            Select Case ArrCaren(2, i)
               Case "D":
                        If ArrCaren(3, i) < dias Then ArrCaren(4, i) = "S"
               Case "H":
                        If ArrCaren(3, i) < Horas Then ArrCaren(4, i) = "S"
               Case "M":
                        If ArrCaren(3, i) < meses Then ArrCaren(4, i) = "S"
            End Select
         Next
      End If
      For i = 0 To UBound(ArrCaren, 2)
         BandTiat = False: BandDiag = False: BandPrestacion = False: TipCare = NUL$
         If ArrCaren(4, i) = "N" Then
            ReDim arr(0)
            condi = "NU_AUTO_CARE=NU_AUTO_CARE_CATA AND NU_AUTO_CARE=" & ArrCaren(0, i)
            Result = LoadData("CARENCIA,R_CARENCIA_TIPO_ATENCION", "NU_AUTO_CARE", condi, arr())
            If Result <> FAIL And Encontro Then
               If IsNumeric(arr(0)) Then TipCare = "T"
            End If
            
            ReDim arr(1)
            condi = "NU_AUTO_CARE=" & ArrCaren(0, i)
            Result = LoadData("CARENCIA", "NU_AUTO_CLDI_CARE,NU_AUTO_CLPR_CARE", condi, arr())
            If Result <> FAIL And Encontro Then
               If IsNumeric(arr(0)) Then TipCare = TipCare & "D"
               If IsNumeric(arr(1)) Then TipCare = TipCare & "P"
            End If
            
            If InStr(1, TipCare, "T") > 0 Then
               'validar el tipo de atenci�n
               ReDim ArrTemp(0)
               Campos = "NU_AUTO_CARE"
               Desde = "CARENCIA,R_CARENCIA_TIPO_ATENCION"
               condi = "NU_AUTO_CARE=NU_AUTO_CARE_CATA AND NU_AUTO_CARE=" & ArrCaren(0, i)
               condi = condi & " AND NU_AUTO_TIAT_CATA=" & AutTipAtencion
               Result = LoadData(Desde, Campos, condi, ArrTemp())
               If Result <> FAIL And Encontro Then
                  BandTiat = True
               End If
            End If
            
            If InStr(1, TipCare, "D") > 0 Then
               'validar la clasificaci�n de Diagn�stico
               ReDim ArrTemp(0)
               Campos = "NU_AUTO_CARE"
               Desde = "CARENCIA,CLASIFICACION_DIAGNOSTICO,R_DIAG_CLASDIAG"
               condi = "NU_AUTO_CLDI_CARE=NU_AUTO_CLDI AND NU_AUTO_CLDI=NU_AUTO_CLDI_DICD"
               condi = condi & " AND NU_AUTO_DIAG_DICD=" & AutDiag
               condi = condi & " AND NU_AUTO_CARE=" & ArrCaren(0, i)
               Result = LoadData(Desde, Campos, condi, ArrTemp())
               If Result <> FAIL And Encontro Then
                  BandDiag = True
               End If
            End If
            
            If InStr(1, TipCare, "P") > 0 Then
               'validar la clasificaci�n de Prestaciones
               ReDim ArrTemp(0)
               Campos = "NU_AUTO_CARE"
               Desde = "CARENCIA,CLASIFICACION_PRESTACION,R_PREST_CLASPREST"
               condi = "NU_AUTO_CLPR_CARE=NU_AUTO_CLPR AND NU_AUTO_CLPR=NU_AUTO_CLPR_PRCP"
               condi = condi & " AND NU_AUTO_PRES_PRCP=" & AutPrestacion
               condi = condi & " AND NU_AUTO_CARE=" & ArrCaren(0, i)
               Result = LoadData(Desde, Campos, condi, ArrTemp())
               If Result <> FAIL And Encontro Then
                  BandPrestacion = True
               End If
            End If
            
            cont = 0
            For j = 0 To Len(TipCare) - 1
               Cad = Mid(TipCare, j + 1, 1)
               If (InStr(1, Cad, "T") > 0) And BandTiat Then cont = cont + 1
               If (InStr(1, Cad, "D") > 0) And BandDiag Then cont = cont + 1
               If (InStr(1, Cad, "P") > 0) And BandPrestacion Then cont = cont + 1
               If cont = Len(TipCare) Then
                  Validar_Carencias = True
                  ArrCaren(5, i) = "N"
               Else
                  ArrCaren(5, i) = "S"
               End If
            Next
         End If
      Next
   End If
   
   For i = 0 To UBound(ArrCaren, 2)
      If ArrCaren(5, i) = "N" Then
         Mens = Mens & Chr(13) & ArrCaren(1, i)
      End If
   Next
   
   If Mens <> "" Then Call Mensaje1("El afiliado no ha superado los siguientes periodos de carencia: " & Chr(13) & Mens, 3)
   


End Function


'Public Sub Cargar_Incapacidades(ByRef Lstw_Incap As ListView, ByRef Lstw_IncapRecl As ListView, ByVal AutDiag As Double, _
'            ByVal AutTerc As Double, ByVal AutCont As Double, ByRef VlTope As Double)
'Dim i             As Integer
'Dim CodGrupos     As String
'Dim ArrValores()  As Variant
'Dim Item          As ListItem
'
''Busca los grupos de cie , donde se encuentra el diagnostico
'Campos = "TX_CODI_GRCI,TX_DESC_GRCI,0,0,0,0"
'Desde = "GRUPO_CIE,R_DIAG_CIE"
'condi = "NU_AUTO_GRCI=NU_AUTO_GRCI_DICI"
'condi = condi & " AND NU_AUTO_DIAG_DICI=" & AutDiag
'condi = condi & " AND TX_TOPE_GRCI='S' ORDER BY TX_DESC_GRCI"
'Result = LoadListView(Lstw_Incap, Desde, Campos, "NU_AUTO_GRCI", condi)
'If Result <> FAIL And Encontro Then
'   For i = 1 To Lstw_Incap.ListItems.Count
'      CodGrupos = CodGrupos & Mid(Lstw_Incap.ListItems(i).Key, 2) & Coma
'   Next
'   CodGrupos = Mid(CodGrupos, 1, Len(CodGrupos) - 1)
'   'Busca las incapacidades reclamadas por el contrato
'   Campos = "TX_CODI_GRCI,TX_DESC_GRCI,0,0,0,0"
'   Desde = "TRANSITO,REEMBOLSO,GRUPO_CIE,R_CONTRATO_AFILIADO,PERSONA_NATURAL"
'   condi = "NU_NUME_TRAN=NU_NUME_TRAN_REEM AND NU_AUTO_GRCI_REEM=NU_AUTO_GRCI"
'   condi = condi & " AND NU_AUTO_COPNBEN_TRAN=NU_AUTO_COPN"
'   condi = condi & " AND NU_AUTO_PENA_COPN=NU_AUTO_PENA"
'   condi = condi & " AND NU_AUTO_TERC_PENA=" & AutTerc
'   condi = condi & " AND NU_AUTO_CONT_TRAN=" & AutCont
'   If CodGrupos <> "" Then
'      condi = condi & " AND NU_AUTO_GRCI NOT IN(" & CodGrupos & ")"
'   End If
'   condi = condi & " UNION SELECT NU_AUTO_GRCI,TX_CODI_GRCI,TX_DESC_GRCI,0,0,0,0"
'   condi = condi & " FROM REEMBOLSO_ACUMULADO,PERSONA_NATURAL,GRUPO_CIE"
'   condi = condi & " WHERE NU_AUTO_PENABEN_REAC=NU_AUTO_PENA AND NU_AUTO_TERC_PENA=" & AutTerc
'   condi = condi & " AND NU_AUTO_GRCI_REAC=NU_AUTO_GRCI"
'   condi = condi & " AND NU_AUTO_CONT_REAC=" & AutCont
'   condi = condi & " UNION SELECT NU_AUTO_GRCI,TX_CODI_GRCI,TX_DESC_GRCI,0,0,0,0"
'   condi = condi & " From TRANSITO,CREDITO,GRUPO_CIE,R_CONTRATO_AFILIADO,PERSONA_NATURAL"
'   condi = condi & " Where NU_NUME_TRAN=NU_NUME_TRAN_CRED AND NU_AUTO_GRCI_CRED=NU_AUTO_GRCI"
'   condi = condi & " AND NU_AUTO_COPNBEN_TRAN=NU_AUTO_COPN"
'   condi = condi & " AND NU_AUTO_PENA_COPN=NU_AUTO_PENA"
'   condi = condi & " AND NU_AUTO_TERC_PENA=" & AutTerc
'   condi = condi & " AND NU_AUTO_CONT_TRAN=" & AutCont
'   If CodGrupos <> "" Then
'      condi = condi & " AND NU_AUTO_GRCI NOT IN(" & CodGrupos & ")"
'   End If
'   condi = condi & " ORDER BY TX_DESC_GRCI"
'   Result = LoadListView(Lstw_IncapRecl, Desde, Campos, "NU_AUTO_GRCI", condi)
'End If
'CodGrupos = ""
'For i = 1 To Lstw_Incap.ListItems.Count
'   CodGrupos = CodGrupos & Mid(Lstw_Incap.ListItems(i).Key, 2) & Coma
'Next
'For i = 1 To Lstw_IncapRecl.ListItems.Count
'   CodGrupos = CodGrupos & Mid(Lstw_IncapRecl.ListItems(i).Key, 2) & Coma
'Next
'If CodGrupos <> "" Then
'   CodGrupos = Mid(CodGrupos, 1, Len(CodGrupos) - 1)
'   'BUSCAR LOS VALORES DE LAS INCAPACIDADES
'   '******* PAGADO *********
'   ReDim ArrValores(1, 0)
'   Campos = "TX_CODI_GRCI,SUM(NU_VLCUB_REEM)"
'   Desde = "TRANSITO,ESTADO_TRANSITO,REEMBOLSO,PERSONA_NATURAL,GRUPO_CIE,R_CONTRATO_AFILIADO"
'   condi = "NU_NUME_TRAN=NU_NUME_TRAN_ESTR AND NU_NUME_TRAN=NU_NUME_TRAN_REEM"
'   condi = condi & " AND NU_AUTO_COPNBEN_TRAN=NU_AUTO_COPN"
'   condi = condi & " AND NU_AUTO_PENA_COPN=NU_AUTO_PENA AND TX_ESTA_ESTR='PR' AND TX_ACTU_ESTR='S'"
'   condi = condi & " AND NU_AUTO_TERC_PENA=" & AutTerc
'   condi = condi & " AND NU_AUTO_CONT_TRAN=" & AutCont
'   condi = condi & " AND TX_TIPO_TRAN='R' AND NU_AUTO_GRCI_REEM=NU_AUTO_GRCI"
'   condi = condi & " AND NU_AUTO_GRCI_REEM IN(" & CodGrupos & ") GROUP BY TX_CODI_GRCI"
'   condi = condi & " UNION SELECT TX_CODI_GRCI,SUM(NU_ACUM_REAC) FROM REEMBOLSO_ACUMULADO,PERSONA_NATURAL,GRUPO_CIE"
'   condi = condi & " WHERE NU_AUTO_PENABEN_REAC=NU_AUTO_PENA AND NU_AUTO_TERC_PENA=" & AutTerc
'   condi = condi & " AND NU_AUTO_GRCI_REAC=NU_AUTO_GRCI"
'   condi = condi & " AND NU_AUTO_CONT_REAC=" & AutCont
'   condi = condi & " AND NU_AUTO_GRCI_REAC IN(" & CodGrupos & ") GROUP BY TX_CODI_GRCI"
''   condi = condi & " UNION SELECT TX_CODI_GRCI,SUM(NU_VLCR_CRED) FROM TRANSITO,ESTADO_TRANSITO,CREDITO,PERSONA_NATURAL,GRUPO_CIE,R_CONTRATO_AFILIADO"
''   condi = condi & " Where NU_NUME_TRAN=NU_NUME_TRAN_ESTR AND NU_NUME_TRAN=NU_NUME_TRAN_CRED"
''   condi = condi & " AND NU_AUTO_COPNBEN_TRAN=NU_AUTO_COPN"
''   condi = condi & " AND NU_AUTO_PENA_COPN=NU_AUTO_PENA AND TX_ESTA_ESTR IN ('IC','AC') AND TX_ACTU_ESTR='S'"
''   condi = condi & " AND NU_AUTO_TERC_PENA=" & AutTerc
''   condi = condi & " AND NU_AUTO_CONT_TRAN=" & AutCont
''   condi = condi & " AND TX_TIPO_TRAN='C' AND NU_AUTO_GRCI_CRED=NU_AUTO_GRCI"
''   condi = condi & " AND NU_AUTO_GRCI_CRED IN(" & CodGrupos & ") GROUP BY TX_CODI_GRCI"
'   Result = LoadMulData(Desde, Campos, condi, ArrValores())
'   If Result <> FAIL And Encontro Then
'      For i = 0 To UBound(ArrValores, 2)
'         Set Item = Nothing
'         Set Item = Lstw_Incap.FindItem(ArrValores(0, i))
'         If Item Is Nothing Then Set Item = Lstw_IncapRecl.FindItem(ArrValores(0, i))
'         If Not Item Is Nothing Then
'            Item.SubItems(3) = CDbl(Item.SubItems(3)) + CDbl(ArrValores(1, i))
'         End If
'      Next
'   End If
'   '******CREDITOS**************
'   ReDim ArrValores(1, 0)
'   Campos = "TX_CODI_GRCI,SUM(NU_VLCR_CRED)"
'   Desde = "TRANSITO,ESTADO_TRANSITO,CREDITO,PERSONA_NATURAL,GRUPO_CIE,R_CONTRATO_AFILIADO "
'   condi = "NU_NUME_TRAN=NU_NUME_TRAN_ESTR AND NU_NUME_TRAN=NU_NUME_TRAN_CRED"
'   condi = condi & " AND NU_AUTO_COPNBEN_TRAN=NU_AUTO_COPN"
'   condi = condi & " AND NU_AUTO_PENA_COPN=NU_AUTO_PENA AND TX_ACTU_ESTR='S'"
'   condi = condi & " AND TX_ESTA_ESTR IN ('IC','AC')"
'   condi = condi & " AND NU_AUTO_TERC_PENA=" & AutTerc
'   condi = condi & " AND NU_AUTO_CONT_TRAN=" & AutCont
'   condi = condi & " AND TX_TIPO_TRAN='C' AND NU_AUTO_GRCI_CRED=NU_AUTO_GRCI"
'   condi = condi & " AND NU_AUTO_GRCI_CRED IN(" & CodGrupos & ") GROUP BY TX_CODI_GRCI"
'   Result = LoadMulData(Desde, Campos, condi, ArrValores())
'   If Result <> FAIL And Encontro Then
'      For i = 0 To UBound(ArrValores, 2)
'         Set Item = Nothing
'         Set Item = Lstw_Incap.FindItem(ArrValores(0, i))
'         If Item Is Nothing Then Set Item = Lstw_IncapRecl.FindItem(ArrValores(0, i))
'         If Not Item Is Nothing Then
'            Item.SubItems(4) = CDbl(Item.SubItems(4)) + CDbl(ArrValores(1, i))
'         End If
'      Next
'   End If
'   '******* REEMBOLSOS *********
'   ReDim ArrValores(1, 0)
'   Campos = "TX_CODI_GRCI,SUM(NU_VLPRE_REEM)"
'   Desde = "TRANSITO,ESTADO_TRANSITO,REEMBOLSO,PERSONA_NATURAL,GRUPO_CIE,R_CONTRATO_AFILIADO"
'   condi = "NU_NUME_TRAN=NU_NUME_TRAN_ESTR AND NU_NUME_TRAN=NU_NUME_TRAN_REEM"
'   condi = condi & " AND NU_AUTO_COPNBEN_TRAN=NU_AUTO_COPN"
'   condi = condi & " AND NU_AUTO_PENA_COPN=NU_AUTO_PENA AND TX_ESTA_ESTR='RR' AND TX_ACTU_ESTR='S'"
'   condi = condi & " AND NU_AUTO_TERC_PENA=" & AutTerc
'   condi = condi & " AND NU_AUTO_CONT_TRAN=" & AutCont
'   condi = condi & " AND TX_TIPO_TRAN='R' AND NU_AUTO_GRCI_REEM=NU_AUTO_GRCI"
'   condi = condi & " AND NU_AUTO_GRCI_REEM IN(" & CodGrupos & ") GROUP BY TX_CODI_GRCI"
'   condi = condi & " UNION SELECT TX_CODI_GRCI,SUM(NU_VLCUB_REEM)"
'   condi = condi & " FROM TRANSITO,ESTADO_TRANSITO,REEMBOLSO,PERSONA_NATURAL,GRUPO_CIE,R_CONTRATO_AFILIADO"
'   condi = condi & " WHERE NU_NUME_TRAN=NU_NUME_TRAN_ESTR AND NU_NUME_TRAN=NU_NUME_TRAN_REEM"
'   condi = condi & " AND NU_AUTO_COPNBEN_TRAN=NU_AUTO_COPN"
'   condi = condi & " AND NU_AUTO_PENA_COPN=NU_AUTO_PENA AND TX_ACTU_ESTR='S'"
'   condi = condi & " AND TX_ESTA_ESTR IN('IR','DIR','AR')"
'   condi = condi & " AND NU_AUTO_TERC_PENA=" & AutTerc
'   condi = condi & " AND NU_AUTO_CONT_TRAN=" & AutCont
'   condi = condi & " AND TX_TIPO_TRAN='R' AND NU_AUTO_GRCI_REEM=NU_AUTO_GRCI"
'   condi = condi & " AND NU_AUTO_GRCI_REEM IN(" & CodGrupos & ") GROUP BY TX_CODI_GRCI"
'   Result = LoadMulData(Desde, Campos, condi, ArrValores())
'   If Result <> FAIL And Encontro Then
'      For i = 0 To UBound(ArrValores, 2)
'         Set Item = Nothing
'         Set Item = Lstw_Incap.FindItem(ArrValores(0, i))
'         If Item Is Nothing Then Set Item = Lstw_IncapRecl.FindItem(ArrValores(0, i))
'         If Not Item Is Nothing Then
'            Item.SubItems(5) = CDbl(Item.SubItems(5)) + CDbl(ArrValores(1, i))
'         End If
'      Next
'   End If
'   'CALCULAR EL SALDO
'   With Lstw_Incap
'      For i = 1 To .ListItems.Count
'         .ListItems(i).SubItems(2) = VlTope - CDbl(.ListItems(i).SubItems(3)) - CDbl(.ListItems(i).SubItems(4)) - CDbl(.ListItems(i).SubItems(5))
'      Next
'   End With
'   With Lstw_IncapRecl
'      For i = 1 To .ListItems.Count
'         .ListItems(i).SubItems(2) = VlTope - CDbl(.ListItems(i).SubItems(3)) - CDbl(.ListItems(i).SubItems(4)) - CDbl(.ListItems(i).SubItems(5))
'      Next
'   End With
'End If
'End Sub

Public Sub Cargar_Incapacidades(ByRef Lstw_Incap As ListView, ByRef Lstw_IncapRecl As ListView, ByVal AutDiag As Double, _
            ByVal AutTerc As Double, ByVal AutCont As Double, ByRef VlTope As Double, ByVal AutPlan As Double)
Dim i             As Integer
Dim CodGrupos     As String
Dim ArrValores()  As Variant
Dim Item          As ListItem
Dim VlTopeIncap   As Double

'Busca los grupos de cie , donde se encuentra el diagnostico
Campos = "TX_CODI_GRCI,TX_DESC_GRCI,0,0,0,0"
Desde = "GRUPO_CIE,R_DIAG_CIE"
condi = "NU_AUTO_GRCI=NU_AUTO_GRCI_DICI"
condi = condi & " AND NU_AUTO_DIAG_DICI=" & AutDiag
condi = condi & " AND TX_TOPE_GRCI='S' ORDER BY TX_DESC_GRCI"
Result = LoadListView(Lstw_Incap, Desde, Campos, "NU_AUTO_GRCI", condi)
If Result <> FAIL And Encontro Then
   For i = 1 To Lstw_Incap.ListItems.Count
      CodGrupos = CodGrupos & Mid(Lstw_Incap.ListItems(i).Key, 2) & Coma
   Next
   CodGrupos = Mid(CodGrupos, 1, Len(CodGrupos) - 1)
   'Busca las incapacidades reclamadas por el contrato
   Campos = "TX_CODI_GRCI,TX_DESC_GRCI,0,0,0,0"
   Desde = "TRANSITO,REEMBOLSO,GRUPO_CIE,R_CONTRATO_AFILIADO,PERSONA_NATURAL"
   condi = "NU_NUME_TRAN=NU_NUME_TRAN_REEM AND NU_AUTO_GRCI_REEM=NU_AUTO_GRCI"
   condi = condi & " AND NU_AUTO_COPNBEN_TRAN=NU_AUTO_COPN"
   condi = condi & " AND NU_AUTO_PENA_COPN=NU_AUTO_PENA"
   condi = condi & " AND NU_AUTO_TERC_PENA=" & AutTerc
   condi = condi & " AND NU_AUTO_CONT_TRAN=" & AutCont
   If CodGrupos <> "" Then
      condi = condi & " AND NU_AUTO_GRCI NOT IN(" & CodGrupos & ")"
   End If
   condi = condi & " UNION SELECT NU_AUTO_GRCI,TX_CODI_GRCI,TX_DESC_GRCI,0,0,0,0"
   condi = condi & " FROM REEMBOLSO_ACUMULADO,PERSONA_NATURAL,GRUPO_CIE"
   condi = condi & " WHERE NU_AUTO_PENABEN_REAC=NU_AUTO_PENA AND NU_AUTO_TERC_PENA=" & AutTerc
   condi = condi & " AND NU_AUTO_GRCI_REAC=NU_AUTO_GRCI"
   condi = condi & " AND NU_AUTO_CONT_REAC=" & AutCont
   If CodGrupos <> "" Then
      condi = condi & " AND NU_AUTO_GRCI_REAC NOT IN(" & CodGrupos & ")"
   End If
   condi = condi & " UNION SELECT NU_AUTO_GRCI,TX_CODI_GRCI,TX_DESC_GRCI,0,0,0,0"
   condi = condi & " From TRANSITO,CREDITO,GRUPO_CIE,R_CONTRATO_AFILIADO,PERSONA_NATURAL"
   condi = condi & " Where NU_NUME_TRAN=NU_NUME_TRAN_CRED AND NU_AUTO_GRCI_CRED=NU_AUTO_GRCI"
   condi = condi & " AND NU_AUTO_COPNBEN_TRAN=NU_AUTO_COPN"
   condi = condi & " AND NU_AUTO_PENA_COPN=NU_AUTO_PENA"
   condi = condi & " AND NU_AUTO_TERC_PENA=" & AutTerc
   condi = condi & " AND NU_AUTO_CONT_TRAN=" & AutCont
   If CodGrupos <> "" Then
      condi = condi & " AND NU_AUTO_GRCI NOT IN(" & CodGrupos & ")"
   End If
   condi = condi & " ORDER BY TX_DESC_GRCI"
   Result = LoadListView(Lstw_IncapRecl, Desde, Campos, "NU_AUTO_GRCI", condi)
Else
   Call Mensaje1("El diagnostico no tiene asignado un grupo de CIE", 3)
End If
CodGrupos = ""
For i = 1 To Lstw_Incap.ListItems.Count
   CodGrupos = CodGrupos & Mid(Lstw_Incap.ListItems(i).Key, 2) & Coma
Next
For i = 1 To Lstw_IncapRecl.ListItems.Count
   CodGrupos = CodGrupos & Mid(Lstw_IncapRecl.ListItems(i).Key, 2) & Coma
Next
If CodGrupos <> "" Then
   CodGrupos = Mid(CodGrupos, 1, Len(CodGrupos) - 1)
   'BUSCAR LOS VALORES DE LAS INCAPACIDADES
   With Lstw_Incap
      For i = 1 To .ListItems.Count
         .ListItems(i).SubItems(3) = VlPagado_Incapacidad(Mid(.ListItems(i).Key, 2), AutTerc, AutCont, "T")
         .ListItems(i).SubItems(4) = VlCreditos_Incapacidad(Mid(.ListItems(i).Key, 2), AutTerc, AutCont, "T")
         .ListItems(i).SubItems(5) = VlReembolsos_Incapacidad(Mid(.ListItems(i).Key, 2), AutTerc, AutCont, "T")
         'buscar el tope para esta incapacidad
         'VlTopeIncap = Buscar_Tope_Incapacidad(Mid(.ListItems(i).Key, 2), AutPlan) Modificado EA 18-Mar-2004
         VlTopeIncap = 0
         If VlTopeIncap = 0 Then
            VlTopeIncap = VlTope
            .ListItems(i).SubItems(2) = RoundConDec(VlTopeIncap - CDbl(.ListItems(i).SubItems(3)) - CDbl(.ListItems(i).SubItems(4)) - CDbl(.ListItems(i).SubItems(5)))
         Else
            .ListItems(i).SubItems(2) = "Plan"
         End If
      Next
   End With
   With Lstw_IncapRecl
      For i = 1 To .ListItems.Count
         .ListItems(i).SubItems(3) = VlPagado_Incapacidad(Mid(.ListItems(i).Key, 2), AutTerc, AutCont, "T")
         .ListItems(i).SubItems(4) = VlCreditos_Incapacidad(Mid(.ListItems(i).Key, 2), AutTerc, AutCont, "T")
         .ListItems(i).SubItems(5) = VlReembolsos_Incapacidad(Mid(.ListItems(i).Key, 2), AutTerc, AutCont, "T")
         'buscar el tope para esta incapacidad
         'VlTopeIncap = Buscar_Tope_Incapacidad(Mid(.ListItems(i).Key, 2), AutPlan)  Modificado EA 18-Mar-2004
         VlTopeIncap = 0
         If VlTopeIncap = 0 Then
            VlTopeIncap = VlTope
            .ListItems(i).SubItems(2) = RoundConDec(VlTopeIncap - CDbl(.ListItems(i).SubItems(3)) - CDbl(.ListItems(i).SubItems(4)) - CDbl(.ListItems(i).SubItems(5)))
         Else
            .ListItems(i).SubItems(2) = "Plan"
         End If
      Next
   End With
End If
End Sub

Public Sub InicializarItemTrans(ByRef ItemTrans As ItemTransito)
With ItemTrans
.NumTransito = 0
.AutItem = 0
.AutPlan = 0
.autContrato = 0
.FechIniCont = Empty
.FechFinCont = Empty
.factura = NUL$
.AutPrestador = 0
.NombPrestador = NUL$
.AutPrestacion = 0
.AutItemManualTar = 0
.CodiPrestacion = NUL$
.NombPrestaci�n = NUL$
.CantPresen = 0
.VlUnitPresen = 0
.VlTotalPresen = 0
.VlTope = 0
.VlRecorte = 0
.AutDeducible = 0
.VlDeducible = 0
.VlBase = 0
.IndiCobertura = NUL$
.AutCobertura = 0
.PorcCubierto = 0
.CantCubierta = 0
.VlCubierto = 0
.PorcCredito = 0
.VlCredito = 0
.VlXCobrar = 0
.VlXPagar = 0
.AutTipoAten = 0
.NombTipAten = NUL$
.AutAtencion = 0
.NombAtencion = NUL$
.AutFactura = 0
.FechaFactura = Empty
.AutConvenio = 0
.VlConvenio = 0
.Motivo = NUL$
.NombCobertura = NUL$
.AutParentesco = 0
.AutCausaExterna = 0
.AutDiagnostico = 0
.AutTiponucleo = 0
.AutPrestador = 0
.AutGrupoCie = 0
.AutBeneficiario = 0
.AutTitular = 0
.AutTercBeneficiario = 0
.AutTercTitular = 0
.TipoCuadro = NUL$
.TipoCiudad = NUL$
.AutCiudad = 0
.AutProfesional = 0
End With
End Sub

'TipoCuadro: C=Cerrado,A=Abierto
'TipoCiudad: D=Dentro de la ciudad,F=Fuera de la ciudad
'TipoTrans: Tipo de transito C=Credito, R=Reembolso
'OpcCred: Opcion de credito C=Consulta
Public Function LiquidarItemTrans(ByRef ItemTrans As ItemTransito, ByRef Plan As CaractPlan, _
                           ByVal autContrato As Double, ByVal AutPrestacion As Double, ByVal CantPresen As Double, _
                           ByVal VlTotalPresen As Double, ByVal AutConvenio As Double, ByVal AutTipoAten As Double, _
                           ByVal AutAtencion As Double, ByVal FechaLiq As Date, ByVal AutParentesco As Double, _
                           ByVal AutCausaExterna As Double, ByVal AutDiagnostico As Double, ByVal AutTiponucleo As Double, _
                           ByVal TipoCuadro As String, ByVal TipoCiudad As String, ByVal AutPrestador As Double, _
                           ByVal AutGrupoCie As Double, ByVal AutBeneficiario As Double, AutTitular As Double, _
                           ByVal AutTercBeneficiario As Double, AutTercTitular As Double, _
                           ByVal NumTransito As Double, ByVal FechIniCont As Date, ByVal FechFinCont As Date, _
                           ByVal Porcred As Double, ByVal TipoCred As String, ByVal AutItemManualTar As Double, _
                           ByVal AutCiudad As Double, _
                           Optional ByVal PorcCubiertoEdad As Double, Optional ByVal PorcExclusiondiag As Double, _
                           Optional ByVal OpcReemb As String, Optional ByVal PorcCooBen As Double, _
                           Optional ByVal TipoTrans As String, Optional ByVal OpcCred As String) As Integer
                     
Dim VlRef, VlLiqUnit As Double
Dim ValItems() As Variant
Dim ArrAcum() As Variant
Dim IndDeduCobe As String, TipCobe As String
Dim VlCubre As Double, PrCubre As Double, CtCubre As Double
Dim VlConsumido As Double
With ItemTrans
.AutPlan = Plan.AutNum
.autContrato = autContrato
.AutPrestacion = AutPrestacion
.AutItemManualTar = AutItemManualTar
.FechaFactura = FechaLiq
.CantPresen = CantPresen
.AutConvenio = AutConvenio
.VlTotalPresen = VlTotalPresen
.AutTipoAten = AutTipoAten
.AutAtencion = AutAtencion
.AutParentesco = AutParentesco
.AutCausaExterna = AutCausaExterna
.AutDiagnostico = AutDiagnostico
.AutTiponucleo = AutTiponucleo
.AutPrestador = AutPrestador
.AutGrupoCie = AutGrupoCie
.AutBeneficiario = AutBeneficiario
.AutTitular = AutTitular
.NumTransito = NumTransito
.AutTercBeneficiario = AutTercBeneficiario
.AutTercTitular = AutTercTitular
.FechIniCont = FechIniCont
.FechFinCont = FechFinCont
.PorcCredito = Porcred
.AutCiudad = AutCiudad
.Motivo = NUL$

VlRef = Valor_Referencia(.AutPlan, .autContrato, .AutPrestacion, .FechaFactura, ValItems(), .AutItemManualTar, .AutCiudad)
VlLiqUnit = VlRef
VlRef = VlRef * .CantPresen
If .AutConvenio <> 0 Then
   .VlConvenio = Valor_Prestacion_Convenio(.AutConvenio, .AutPrestacion, .FechaFactura, ValItems(), .AutItemManualTar)
   If VlRef = 0 Or (.VlConvenio <> 0 And .VlConvenio <= VlRef) Then
      VlRef = .VlConvenio
      VlLiqUnit = VlRef
      VlRef = VlRef * .CantPresen
   End If
End If

If VlRef = 0 Then .Motivo = "/ El valor de la prestaci�n es cero"

If .VlTotalPresen < VlRef Then
   .VlTope = .VlTotalPresen
   VlLiqUnit = RoundConDec(.VlTotalPresen / .CantPresen)
Else
   .VlTope = VlRef
End If
.VlRecorte = RoundConDec(.VlTotalPresen - .VlTope)
'verificar si ya se superaron los periodos de carencia.
'buscar si el servicio tiene cobertura o no.
Result = Buscar_CoberturaPrest(.AutPlan, .AutTipoAten, .AutAtencion, .AutParentesco, .AutCausaExterna, .AutPrestacion, _
         .AutDiagnostico, .AutTiponucleo, TipoCuadro, .AutPrestador, TipoCiudad, .IndiCobertura, .AutCobertura, _
         .NombCobertura, IndDeduCobe, TipCobe, VlCubre, PrCubre, CtCubre, .Motivo, FechaLiq, autContrato, AutBeneficiario)

If Result <> FAIL Then
   If .IndiCobertura = "S" Then
      If IndDeduCobe = "S" Then
         Result = Buscar_DeduciblePrest(.AutPlan, .AutTipoAten, TipoCuadro, .AutPrestador, .VlTope, .autContrato, _
                  .AutGrupoCie, .AutBeneficiario, .AutTitular, .NumTransito, .FechIniCont, .FechFinCont, .FechaFactura, _
                  .AutDeducible, .VlDeducible, .AutTercBeneficiario, .AutTercTitular, TipoTrans)
      End If
      
      If OpcReemb = "CB" Then
         .PorcCubierto = PorcCooBen
         PrCubre = PorcCooBen
         PorcCubiertoEdad = 100
         .VlDeducible = 0
      Else
         .PorcCubierto = PrCubre
      End If
      .VlBase = RoundConDec(.VlTope - .VlDeducible)
      .CantCubierta = .CantPresen
      
      If InStr(1, TipCobe, "P") > 0 Then .VlCubierto = RoundConDec(.VlBase * (PrCubre / 100))
      Result = Calcular_pagado(.AutCobertura, .AutPlan, Plan, .AutGrupoCie, .autContrato, .AutBeneficiario, .AutTitular, _
               TipCobe, CtCubre, .CantCubierta, .VlCubierto, VlLiqUnit, VlCubre, TipoCred, .VlCredito, PorcCubiertoEdad, _
               PorcExclusiondiag, .Motivo, .NumTransito, TipoTrans)
      'se valida que el valor cubierto no supere el tope del plan
      VlConsumido = VlConsumido_Incapacidad(.AutGrupoCie, .AutTercBeneficiario, .autContrato, "T", .NumTransito, TipoTrans)
      If (.VlCubierto + VlConsumido) > Plan.VlTopeCobe Then
         If VlConsumido > Plan.VlTopeCobe Then
            .VlCubierto = 0
         Else
            .VlCubierto = RoundConDec(Plan.VlTopeCobe - VlConsumido)
         End If
         If .Motivo <> "" Then .Motivo = .Motivo & " / "
         .Motivo = .Motivo & "Se supero el tope del plan"
      End If
   End If
   
   If .IndiCobertura = "N" Then
      .VlDeducible = 0
      .VlBase = 0
   ElseIf .VlCubierto = 0 Then
      .IndiCobertura = "N"
      .PorcCubierto = 0
      .CantCubierta = 0
   End If

   If TipoCred <> "" Then
      Select Case TipoCred
'         Case "E": .VlCredito = RoundConDec(.VlTotalPresen * (.PorcCredito / 100))
         Case "E":   .VlCredito = RoundConDec(.VlTope * (.PorcCredito / 100))
         Case "C":
                     If OpcCred = "C" Then
                        .PorcCredito = 100
                        .VlCredito = .VlTotalPresen
                     Else
                        .PorcCredito = PrCubre
                        .VlCredito = .VlCubierto
                     End If
         Case "T":   .PorcCredito = 100
                     .VlCredito = .VlTotalPresen
      End Select
   End If
   If TipoCred <> "" And .VlCredito > .VlCubierto Then
      .VlXCobrar = RoundConDec(.VlCredito - .VlCubierto)
   End If
   
   If TipoCred <> "" Then
      .VlXPagar = .VlCredito
   Else
      .VlXPagar = .VlCubierto
   End If
   
   If .IndiCobertura = "N" Then
      .PorcCredito = 0
   End If
   
End If
End With

LiquidarItemTrans = Result
End Function

'RECIBE AUTO DEL beneficiario, auto del diagn�stico, Tipo exclusi�n (P=Porcentaje,T=Tiempo)
'RETORNA TRUE SI el diagn�stico esta excluido
'RETORNA FALSE SI el diagn�stico NO esta excluido
Public Function validar_Exclusion(ByVal AutCopnBen As Double, ByVal AutDiag As Double, ByVal TipoExcl As String, ByVal Fecha As Date, Optional ByRef PorcExclusiondiag As Double) As Boolean
Dim ArrTemp() As Variant
Dim dias, meses As Double
Dim CantExcl As Double
'validar exclusiones
ReDim ArrTemp(0)
Desde = "EXCLUSION,GRUPO_CIE,R_DIAG_CIE,DECLARACION_SALUD,R_CONTRATO_AFILIADO"
Campos = "TX_VALO_EXCL"
condi = "NU_AUTO_CIE_EXCL=NU_AUTO_GRCI AND NU_AUTO_GRCI=NU_AUTO_GRCI_DICI"
condi = condi & " AND NU_AUTO_DESA_EXCL= NU_AUTO_DESA"
condi = condi & " AND NU_AUTO_COPN_DESA = NU_AUTO_COPN"
condi = condi & " AND NU_AUTO_COPN=" & AutCopnBen & " AND NU_AUTO_DIAG_DICI=" & AutDiag
condi = condi & " AND TX_TIPO_EXCL=" & Comi & TipoExcl & Comi
Result = LoadData(Desde, Campos, condi, ArrTemp())
If Result <> FAIL And Encontro Then
   If TipoExcl = "P" Then
      Call Mensaje1("La cobertura para este Diagn�stico ser� al " & ArrTemp(0) & "%", 3)
      PorcExclusiondiag = CDbl(ArrTemp(0))
      validar_Exclusion = True
   End If
   If TipoExcl = "T" Then
      CantExcl = ArrTemp(0)
      ReDim ArrTemp(0)
      condi = "NU_AUTO_COPN = " & AutCopnBen
      Result = LoadData("R_CONTRATO_AFILIADO", "FE_PLAN_COPN", condi, ArrTemp())
      If Result <> FAIL And Encontro Then
         'si el afiliado pasa carencias
         dias = Abs(DateDiff("D", CDate(ArrTemp(0)), Fecha))
         meses = dias / 30
         If meses <= CantExcl Then
            Call Mensaje1("EL Diagn�stico no ha superado el periodo de exclusi�n", 3)
            validar_Exclusion = True
         End If
      End If
   End If
End If
End Function

Public Function Buscar_Cobertura(ByVal AutPlan As Long, ByVal AutTiat As Long, ByVal AutAten As Long _
                  , ByVal AutPare As Long, ByVal AutCaex As Long, ByVal AutDiag As Long _
                  , ByVal AutTiNu As Long, ByVal TipCuad As String, ByVal AutPrestador As Long, ByVal TipCiud As String _
                  , ByRef IndCobe As String, ByRef AutCobe As Double, ByRef NombCobe As String, ByRef IndDeduCobe As String _
                  , ByRef TipCobe As String, ByRef VlCobe As Double, ByRef PrCobe As Double, ByRef CtCobe As Double _
                  , ByRef ArrCobertura As Variant) As Integer

' ByVal AutPrest As Long

Dim ArrCobe(), ArrTemp() As Variant
Dim i, j, ContCobe As Integer
Dim BandCuad, BandCiud, BandCuadEsp As Boolean
Dim Mens As String

'buscar las coberturas en las que se cumple alguna condici�n
Campos = "NU_AUTO_COPL,TX_DESC_COPL,COUNT(*),''"
Desde = "(SELECT NU_AUTO_COPL,TX_DESC_COPL FROM COBERTURA_PLAN,R_COBEPLAN_TIPO_ATENCION WHERE"
Desde = Desde & " NU_AUTO_COPL=NU_AUTO_COPL_CPTA AND NU_AUTO_PLAN_COPL=" & AutPlan
Desde = Desde & " AND NU_AUTO_TIAT_CPTA=" & AutTiat
Desde = Desde & " UNION ALL SELECT NU_AUTO_COPL,TX_DESC_COPL FROM COBERTURA_PLAN,R_COBEPLAN_ATENCION"
Desde = Desde & " WHERE NU_AUTO_COPL=NU_AUTO_COPL_CPAT AND NU_AUTO_PLAN_COPL=" & AutPlan
Desde = Desde & " AND NU_AUTO_ATEN_CPAT=" & AutAten
Desde = Desde & " UNION ALL SELECT NU_AUTO_COPL,TX_DESC_COPL FROM COBERTURA_PLAN,R_COBEPLAN_PARENTESCO"
Desde = Desde & " WHERE NU_AUTO_COPL=NU_AUTO_COPL_CPPA AND NU_AUTO_PLAN_COPL=" & AutPlan
Desde = Desde & " AND NU_AUTO_PARE_CPPA=" & AutPare
Desde = Desde & " UNION ALL SELECT NU_AUTO_COPL,TX_DESC_COPL FROM COBERTURA_PLAN,R_COBEPLAN_CAUSA_EXTERNA"
Desde = Desde & " WHERE NU_AUTO_COPL=NU_AUTO_COPL_CPCE AND NU_AUTO_PLAN_COPL=" & AutPlan
Desde = Desde & " AND NU_AUTO_CAEX_CPCE=" & AutCaex
'''Desde = Desde & " UNION ALL SELECT NU_AUTO_COPL,TX_DESC_COPL FROM COBERTURA_PLAN,R_COBEPLAN_CLASIFPREST,R_PREST_CLASPREST"
'''Desde = Desde & " WHERE NU_AUTO_COPL=NU_AUTO_COPL_CPCP AND NU_AUTO_CLPR_CPCP=NU_AUTO_CLPR_PRCP"
'''Desde = Desde & " AND NU_AUTO_PLAN_COPL=" & AutPlan & " AND NU_AUTO_PRES_PRCP=" & AutPrest
Desde = Desde & " UNION ALL SELECT NU_AUTO_COPL,TX_DESC_COPL FROM COBERTURA_PLAN,R_COBEPLAN_CLASIFDIAG,R_DIAG_CLASDIAG"
Desde = Desde & " WHERE NU_AUTO_COPL=NU_AUTO_COPL_CPCD AND NU_AUTO_CLDI_CPCD=NU_AUTO_CLDI_DICD"
Desde = Desde & " AND NU_AUTO_PLAN_COPL=" & AutPlan & " AND NU_AUTO_DIAG_DICD=" & AutDiag
Desde = Desde & " UNION ALL SELECT NU_AUTO_COPL,TX_DESC_COPL FROM COBERTURA_PLAN,R_COBEPLAN_TIPO_NUCLEO"
Desde = Desde & " WHERE NU_AUTO_COPL=NU_AUTO_COPL_CPTN AND NU_AUTO_PLAN_COPL=" & AutPlan
Desde = Desde & " AND NU_AUTO_TINU_CPTN=" & AutTiNu & ") GROUP BY NU_AUTO_COPL,TX_DESC_COPL"
ReDim ArrCobe(3, 0)
Result = LoadMulData(Desde, Campos, NUL$, ArrCobe())
If Result <> FAIL And Encontro Then
   'BUSCAR LAS COBERTURAS QUE CUMPLEN TODAS LAS CONDICIONES
   For i = 0 To UBound(ArrCobe, 2)
      Campos = "COUNT(*)"
      Desde = "(SELECT DISTINCT NU_AUTO_COPL FROM COBERTURA_PLAN,R_COBEPLAN_TIPO_ATENCION WHERE"
      Desde = Desde & " NU_AUTO_COPL=NU_AUTO_COPL_CPTA AND NU_AUTO_PLAN_COPL=" & AutPlan
      Desde = Desde & " AND NU_AUTO_COPL=" & ArrCobe(0, i)
      Desde = Desde & " UNION ALL SELECT DISTINCT NU_AUTO_COPL FROM COBERTURA_PLAN,R_COBEPLAN_ATENCION"
      Desde = Desde & " WHERE NU_AUTO_COPL=NU_AUTO_COPL_CPAT AND NU_AUTO_PLAN_COPL=" & AutPlan
      Desde = Desde & " AND NU_AUTO_COPL=" & ArrCobe(0, i)
      Desde = Desde & " UNION ALL SELECT DISTINCT NU_AUTO_COPL FROM COBERTURA_PLAN,R_COBEPLAN_PARENTESCO"
      Desde = Desde & " WHERE NU_AUTO_COPL=NU_AUTO_COPL_CPPA AND NU_AUTO_PLAN_COPL=" & AutPlan
      Desde = Desde & " AND NU_AUTO_COPL=" & ArrCobe(0, i)
      Desde = Desde & " UNION ALL SELECT DISTINCT NU_AUTO_COPL FROM COBERTURA_PLAN,R_COBEPLAN_CAUSA_EXTERNA"
      Desde = Desde & " WHERE NU_AUTO_COPL=NU_AUTO_COPL_CPCE AND NU_AUTO_PLAN_COPL=" & AutPlan
      Desde = Desde & " AND NU_AUTO_COPL=" & ArrCobe(0, i)
'      Desde = Desde & " UNION ALL SELECT DISTINCT NU_AUTO_COPL FROM COBERTURA_PLAN,R_COBEPLAN_CLASIFPREST,R_PREST_CLASPREST"
'      Desde = Desde & " WHERE NU_AUTO_COPL=NU_AUTO_COPL_CPCP AND NU_AUTO_CLPR_CPCP=NU_AUTO_CLPR_PRCP"
'      Desde = Desde & " AND NU_AUTO_PLAN_COPL=" & AutPlan & " AND NU_AUTO_COPL=" & ArrCobe(0, i)
'''      Desde = Desde & " UNION ALL SELECT DISTINCT NU_AUTO_COPL FROM COBERTURA_PLAN,R_COBEPLAN_CLASIFPREST"
'''      Desde = Desde & " WHERE NU_AUTO_COPL=NU_AUTO_COPL_CPCP"
'''      Desde = Desde & " AND NU_AUTO_PLAN_COPL=" & AutPlan & " AND NU_AUTO_COPL=" & ArrCobe(0, i)
'      Desde = Desde & " UNION ALL SELECT DISTINCT NU_AUTO_COPL FROM COBERTURA_PLAN,R_COBEPLAN_CLASIFDIAG,R_DIAG_CLASDIAG"
'      Desde = Desde & " WHERE NU_AUTO_COPL=NU_AUTO_COPL_CPCD AND NU_AUTO_CLDI_CPCD=NU_AUTO_CLDI_DICD"
'      Desde = Desde & " AND NU_AUTO_PLAN_COPL=" & AutPlan & " AND NU_AUTO_COPL=" & ArrCobe(0, i)
      Desde = Desde & " UNION ALL SELECT DISTINCT NU_AUTO_COPL FROM COBERTURA_PLAN,R_COBEPLAN_CLASIFDIAG"
      Desde = Desde & " WHERE NU_AUTO_COPL=NU_AUTO_COPL_CPCD"
      Desde = Desde & " AND NU_AUTO_PLAN_COPL=" & AutPlan & " AND NU_AUTO_COPL=" & ArrCobe(0, i)
      Desde = Desde & " UNION ALL SELECT DISTINCT NU_AUTO_COPL FROM COBERTURA_PLAN,R_COBEPLAN_TIPO_NUCLEO"
      Desde = Desde & " WHERE NU_AUTO_COPL=NU_AUTO_COPL_CPTN AND NU_AUTO_PLAN_COPL=" & AutPlan
      Desde = Desde & " AND NU_AUTO_COPL=" & ArrCobe(0, i) & ")"
      ReDim ArrTemp(0)
      Result = LoadData(Desde, Campos, NUL$, ArrTemp())
      If Result <> FAIL And Encontro Then
         If ArrCobe(2, i) <> ArrTemp(0) Then
            ArrCobe(0, i) = ""
         End If
      End If
   Next
End If
j = 0
For i = 0 To UBound(ArrCobe, 2)
   If ArrCobe(0, i) <> "" Then
      ReDim ArrTemp(2)
      Campos = "TX_CUAD_COPL,NU_AUTO_PRTD_COPL,TX_CIUD_COPL"
      condi = "NU_AUTO_COPL=" & ArrCobe(0, i)
      Result = LoadData("COBERTURA_PLAN", Campos, condi, ArrTemp())
      BandCuad = False
      BandCiud = False
      If Result <> FAIL And Encontro Then
         'Validar el tipo de cuadro
         ArrCobe(3, i) = ArrTemp(0)
         Select Case ArrTemp(0)
            Case "A": If TipCuad <> "C" Then BandCuad = True
            Case "C": If TipCuad = "C" Then BandCuad = True
            Case "E": If TipCuad <> "C" And ArrTemp(1) = AutPrestador Then BandCuad = True: BandCuadEsp = True
            Case "T": BandCuad = True
         End Select
         'Validar la ciudad
         Select Case ArrTemp(2)
            Case "D": If TipCiud = "D" Then BandCiud = True
            Case "F": If TipCiud = "F" Then BandCiud = True
            Case "T": BandCiud = True
         End Select
         If Not (BandCuad And BandCiud) Then
            ArrCobe(0, i) = ""
         Else
            AutCobe = CDbl(ArrCobe(0, i))
            ReDim Preserve ArrCobertura(7, j)
            ArrCobertura(0, j) = AutCobe
            j = j + 1
            ContCobe = ContCobe + 1
         End If
      End If
   End If
Next
'quitar las coberturas con cuadro abierto cuando hay coberturas que aplican de cuadro espec�fico
If ContCobe > 1 And BandCuadEsp Then
   ContCobe = 0
   For i = 0 To UBound(ArrCobe, 2)
      If ArrCobe(0, i) <> "" Then
         If ArrCobe(3, i) <> "E" Then
            ArrCobe(0, i) = ""
         Else
            ContCobe = ContCobe + 1
         End If
      End If
   Next
End If

Select Case ContCobe
   Case 0:  IndCobe = "N": AutCobe = 0: IndDeduCobe = "": TipCobe = "": VlCobe = 0: PrCobe = 0: CtCobe = 0
            Buscar_Cobertura = SUCCEED
   Case Else:
            For i = 0 To UBound(ArrCobertura, 2)
               Campos = "TX_DEDU_COPL,TX_TIPO_COPL,NU_VALO_COPL,NU_PORC_COPL,NU_CANT_COPL,TX_DESC_COPL"
               condi = "NU_AUTO_COPL=" & ArrCobertura(0, i) 'AutCobe
               ReDim ArrTemp(5)
               Result = LoadData("COBERTURA_PLAN", Campos, condi, ArrTemp())
               If Result <> FAIL And Encontro Then
                  IndCobe = "S"
                  IndDeduCobe = ArrTemp(0)
                  TipCobe = ArrTemp(1)
                  ArrCobertura(1, i) = ArrTemp(5)
                  ArrCobertura(2, i) = CDbl(ArrTemp(2))
                  ArrCobertura(3, i) = CDbl(ArrTemp(3))
                  ArrCobertura(4, i) = CDbl(ArrTemp(4))
                  
'                  VlCobe = CDbl(Arrtemp(2))
'                  PrCobe = CDbl(Arrtemp(3))
'                  CtCobe = CDbl(Arrtemp(4))
'                  NombCobe = Arrtemp(5)
               End If
            Next
            Buscar_Cobertura = SUCCEED
'   Case Else:
'            For i = 0 To UBound(ArrCobe, 2)
'               If ArrCobe(0, i) <> "" Then
'                  Mens = Mens & ArrCobe(1, i) & Chr(13)
'               End If
'            Next
'            Call Mensaje1("El servicio se encuentra en m�s de una cobertura" & Chr(13) & Mens, 3)
'            Buscar_Cobertura = FAIL
End Select


End Function

Public Function Calcular_pagado(ByVal AutCobertura As Long, ByVal AutPlan As Long, ByRef Plan As CaractPlan, ByVal AutGrupoCie As Long, _
                                ByVal autContrato As Long, ByVal AutBeneficiario As Long, ByVal AutTitular As Long, _
                                ByVal TipCobe As String, ByVal CtCubre As Double, ByRef CantCubierta As Double, ByRef VlCubierto As Double, _
                                ByVal VlLiqUnit As Double, ByVal VlCubre As Double, ByVal TipoCred As String, ByRef VlCredito As Double, _
                                ByVal PorcCubiertoEdad As Double, ByVal PorcExclusiondiag As Double, Optional ByRef Motivo As String, Optional ByVal NumeTran As Long, Optional ByVal TipoTrans As String) As Integer
Dim ArrAcum() As Variant
Dim AcumCant As Double
Dim AcumVlor As Double
Dim i As Integer


      ReDim ArrAcum(1, 0)
      'calcular cuanto se ha pagado en reembolsos de la cobertura que se aplica
      Campos = "SUM(NU_ACANCOB_REDE),SUM(NU_AVLCOB_REDE)"
      Desde = "TRANSITO,ESTADO_TRANSITO,REEMBOLSO,REEMBOLSO_FACTURA,REEMBOLSO_DETALLE"
      condi = "NU_NUME_TRAN=NU_NUME_TRAN_ESTR AND NU_NUME_TRAN=NU_NUME_TRAN_REEM"
      condi = condi & " AND NU_AUTO_REEM=NU_AUTO_REEM_REFA AND NU_AUTO_REFA=NU_AUTO_REFA_REDE"
      condi = condi & " AND TX_ESTA_ESTR IN('RR','IR','DIR','AR','PR') AND TX_ACTU_ESTR='S'"
      condi = condi & " AND NU_AUTO_COPL_REDE=" & AutCobertura & " AND NU_AUTO_PLAN_TRAN=" & AutPlan
      Select Case Plan.TipoCobe 'Tipo de cobertura
         Case "I": 'por incapacidad
                  condi = condi & " AND NU_AUTO_GRCI_REEM=" & AutGrupoCie
         Case "A": 'anual
         Case "V": 'Limite vitalicio
      End Select
      Select Case Plan.PeriodCobe 'Periodo de cobertura
         Case "V": 'Vigencia de contrato
                  condi = condi & " AND NU_AUTO_CONT_TRAN=" & autContrato
         Case "F": 'Fecha Calendario
                  condi = condi & " AND NU_AUTO_CONT_TRAN=" & autContrato
      End Select
      Select Case Plan.TipoPers
         Case "P": condi = condi & " AND NU_AUTO_COPNBEN_TRAN=" & AutBeneficiario  'por persona
         Case "N": condi = condi & " AND NU_AUTO_COPNTIT_TRAN=" & AutTitular  'por nucleo
      End Select
      condi = condi & " UNION "
      condi = condi & " SELECT SUM(NU_CANCOB_CRDE), SUM(NU_VLCOB_CRDE)"
      condi = condi & " FROM TRANSITO,ESTADO_TRANSITO,CREDITO,CREDITO_DETALLE"
      condi = condi & " WHERE NU_NUME_TRAN = NU_NUME_TRAN_ESTR"
      condi = condi & " AND NU_NUME_TRAN = NU_NUME_TRAN_CRED"
      condi = condi & " AND NU_AUTO_CRED = NU_AUTO_CRED_CRDE"
      condi = condi & " AND TX_ESTA_ESTR IN('AC')"
      condi = condi & " AND TX_ACTU_ESTR='S'"
      condi = condi & " AND NU_AUTO_COPL_CRDE=" & AutCobertura & " AND NU_AUTO_PLAN_TRAN=" & AutPlan
      Select Case Plan.TipoCobe 'Tipo de cobertura
         Case "I": 'por incapacidad
                  condi = condi & " AND NU_AUTO_GRCI_CRED=" & AutGrupoCie
         Case "A": 'anual
         Case "V": 'Limite vitalicio
      End Select
      Select Case Plan.PeriodCobe 'Periodo de cobertura
         Case "V": 'Vigencia de contrato
                  condi = condi & " AND NU_AUTO_CONT_TRAN=" & autContrato
         Case "F": 'Fecha Calendario
                  condi = condi & " AND NU_AUTO_CONT_TRAN=" & autContrato
      End Select
      Select Case Plan.TipoPers
         Case "P": condi = condi & " AND NU_AUTO_COPNBEN_TRAN=" & AutBeneficiario  'por persona
         Case "N": condi = condi & " AND NU_AUTO_COPNTIT_TRAN=" & AutTitular  'por nucleo
      End Select
      If TipoTrans = "C" Then
         'Credito Actual
         condi = condi & " UNION "
         condi = condi & " SELECT SUM(NU_CANCOB_CRDE_TMP), SUM(NU_VLCOB_CRDE_TMP)"
         condi = condi & " FROM CREDITO_DETALLE_TMP"
         condi = condi & " WHERE NU_AUTO_CONE_CRDE_TMP =" & NumeTran
         condi = condi & " AND NU_AUTO_COPL_CRDE_TMP=" & AutCobertura
      End If
      If Result <> FAIL Then Result = LoadMulData(Desde, Campos, condi, ArrAcum())
      If Result <> FAIL And Encontro Then
         AcumCant = 0
         AcumVlor = 0
         For i = 0 To UBound(ArrAcum, 2)
            ArrAcum(0, i) = IIf(ArrAcum(0, i) = NUL$, 0, ArrAcum(0, i))
            AcumCant = AcumCant + CDbl(ArrAcum(0, i))
            ArrAcum(1, i) = IIf(ArrAcum(1, i) = NUL$, 0, ArrAcum(1, i))
            AcumVlor = AcumVlor + CDbl(ArrAcum(1, i))
         Next
         'verificar si la cantidad de servicios cubiertos supera la cantidad de cobertura
         If InStr(1, TipCobe, "T") > 0 Then 'Si la cobertura tiene limite de cantidad
            If AcumCant >= CtCubre Then 'si ya se supero la cantidad de cobertura
               CantCubierta = 0
               VlCubierto = 0
               If Motivo <> "" Then Motivo = Motivo & "/"
               Motivo = Motivo & "Se super� la cantidad de cobertura"
            Else
               If CantCubierta > CtCubre - AcumCant Then
                  CantCubierta = CtCubre - AcumCant
                  VlCubierto = CantCubierta * VlLiqUnit
                  If Motivo <> "" Then Motivo = Motivo & "/"
                  Motivo = Motivo & "Se super� la cantidad de cobertura"
               End If
            End If
         End If
         If InStr(1, TipCobe, "V") > 0 Then 'Si la cobertura tiene limite de valor
            If AcumVlor >= VlCubre Then 'si ya se supero el valor de cobertura
               VlCubierto = 0
               If Motivo <> "" Then Motivo = Motivo & "/"
               Motivo = Motivo & "Se super� el valor de la cobertura"
            Else
               If VlCubierto > (VlCubre - AcumVlor) Then
                  VlCubierto = (VlCubre - AcumVlor)
                  If Motivo <> "" Then Motivo = Motivo & "/"
                  Motivo = Motivo & "Se super� el valor de la cobertura"
               End If
            End If
         End If
         
         'Se aplica los porcentajes de cobertura por edad y
         'por exclusion de diagnostico
         If PorcCubiertoEdad > 0 Then
            VlCubierto = VlCubierto * (PorcCubiertoEdad / 100)
            If Motivo <> "" Then Motivo = Motivo & " / "
            Motivo = Motivo & "Aplica cobertura por edad al " & PorcCubiertoEdad & "%"
         End If
         
         If PorcExclusiondiag > 0 Then
            VlCubierto = VlCubierto * (PorcExclusiondiag / 100)
            If Motivo <> "" Then Motivo = Motivo & " / "
            Motivo = Motivo & "Aplica cobertura por exclusi�n de diagnostico al " & PorcCubiertoEdad & "%"
         End If
         
         If TipoCred <> "E" Then VlCredito = VlCubierto
      End If
VlCubierto = RoundConDec(VlCubierto)
VlCredito = RoundConDec(VlCredito)
Calcular_pagado = Result

End Function

'Parametros de entrada:    AutoNumerico del Diagnostico
'                          Autonumerico de la Persona Natural
'                          Fecha hasta la cual se desea calcular la edad

Public Function Validar_Diagnostico(ByVal AutoDiag As Long, ByVal AutoTercPena As Long, ByVal FechAct As Date) As Integer
Dim arrafi() As Variant
Dim Edad As Long

   'Busca los parametros del diagnostico
   ReDim arr(2)
   Condicion = "NU_AUTO_DIAG=" & AutoDiag
   If Result <> FAIL Then Result = LoadData("DIAGNOSTICO", "NU_APLI_DIAG,NU_EDIN_DIAG,NU_EDFI_DIAG", Condicion, arr)
   If Result = FAIL Or Not Encontro Then Exit Function

   'Valida la edad y genero de la persona contra el diagnostico
   ReDim arrafi(1)
   Condicion = "NU_AUTO_TERC_PENA=" & AutoTercPena
   If Result <> FAIL Then Result = LoadData("PERSONA_NATURAL", "TX_GENE_PENA,FE_NACI_PENA", Condicion, arrafi)
   If Result = FAIL Or Not Encontro Then Exit Function

   'Verifica que aplique el genero
   Edad = DateDiff("yyyy", arrafi(1), FechAct)
   If arr(0) = "1" And arrafi(0) = "M" Then
      Call Mensaje1("Este diagnostico aplica solo al genero femenino", 3): Validar_Diagnostico = 0: Exit Function
   ElseIf arr(0) = "2" And arrafi(0) = "F" Then
      Call Mensaje1("Este diagnostico aplica solo al genero masculino", 3): Validar_Diagnostico = 0: Exit Function
   ElseIf Edad < CLng(arr(1)) Or Edad > CLng(arr(2)) Then
      Call Mensaje1("La edad de la persona debe esta entre " & arr(1) & " y " & arr(2) & " a�os", 3): Validar_Diagnostico = 0: Exit Function
   End If
   
   Validar_Diagnostico = 1
   
End Function
'TipAutTerc=Tipo de autonum�rico del tercero T-Tercero,C-R_contrato_afiliado
Public Function VlPagado_Incapacidad(ByVal AutIncap As Double, ByVal AutTerc As Double, ByVal AutCont As Double, _
                                    ByVal TipAutTerc As String) As Double
Dim i As Integer
Dim ArrValores() As Variant
'******* PAGADO *********
ReDim ArrValores(0, 0)
Campos = "SUM(NU_AVLCOB_REDE)"
Desde = "TRANSITO,ESTADO_TRANSITO,REEMBOLSO,REEMBOLSO_PRESTACION,REEMBOLSO_DETALLE,PERSONA_NATURAL,GRUPO_CIE,"
Desde = Desde & "R_CONTRATO_AFILIADO"
condi = "NU_NUME_TRAN=NU_NUME_TRAN_ESTR AND NU_NUME_TRAN=NU_NUME_TRAN_REEM"
condi = condi & " AND NU_AUTO_REEM=NU_AUTO_REEM_REPR AND NU_AUTO_REPR=NU_AUTO_REPR_REDE"
condi = condi & " AND NU_AUTO_COPNBEN_TRAN=NU_AUTO_COPN"
condi = condi & " AND NU_AUTO_PENA_COPN=NU_AUTO_PENA AND TX_ESTA_ESTR='PR' AND TX_ACTU_ESTR='S'"
If TipAutTerc = "T" Then
   condi = condi & " AND NU_AUTO_TERC_PENA=" & AutTerc
ElseIf TipAutTerc = "C" Then
   condi = condi & " AND NU_AUTO_COPN=" & AutTerc
End If
condi = condi & " AND NU_AUTO_CONT_TRAN=" & AutCont
condi = condi & " AND TX_TIPO_TRAN='R' AND NU_AUTO_GRCI_REEM=NU_AUTO_GRCI"
condi = condi & " AND NU_AUTO_GRCI_REEM=" & AutIncap & " GROUP BY NU_AUTO_GRCI"
condi = condi & " UNION SELECT SUM(NU_ACUM_REAC) FROM REEMBOLSO_ACUMULADO,PERSONA_NATURAL,GRUPO_CIE"
condi = condi & " WHERE NU_AUTO_PENABEN_REAC=NU_AUTO_PENA"
If TipAutTerc = "T" Then
   condi = condi & " AND NU_AUTO_TERC_PENA=" & AutTerc
ElseIf TipAutTerc = "C" Then
   condi = condi & " AND NU_AUTO_COPN=" & AutTerc
End If
condi = condi & " AND NU_AUTO_GRCI_REAC=NU_AUTO_GRCI"
condi = condi & " AND NU_AUTO_CONT_REAC=" & AutCont
condi = condi & " AND NU_AUTO_GRCI_REAC=" & AutIncap & "GROUP BY NU_AUTO_GRCI"
Result = LoadMulData(Desde, Campos, condi, ArrValores())
If Result <> FAIL And Encontro Then
   For i = 0 To UBound(ArrValores, 2)
      VlPagado_Incapacidad = VlPagado_Incapacidad + CDbl(ArrValores(0, i))
   Next
End If
VlPagado_Incapacidad = RoundConDec(VlPagado_Incapacidad)
End Function

'TipAutTerc=Tipo de autonum�rico del tercero T-Tercero,C-R_contrato_afiliado
Public Function VlCreditos_Incapacidad(ByVal AutIncap As Double, ByVal AutTerc As Double, ByVal AutCont As Double, _
                                       ByVal TipAutTerc As String, Optional ByVal NumeTran As Long, Optional ByVal TipoTrans As String) As Double
Dim i As Integer
Dim ArrValores() As Variant
'******CREDITOS**************
ReDim ArrValores(0)
Campos = "SUM(NU_VLCR_CRED)"
Desde = "TRANSITO,ESTADO_TRANSITO,CREDITO,PERSONA_NATURAL,GRUPO_CIE,R_CONTRATO_AFILIADO "
condi = "NU_NUME_TRAN=NU_NUME_TRAN_ESTR AND NU_NUME_TRAN=NU_NUME_TRAN_CRED"
condi = condi & " AND NU_AUTO_COPNBEN_TRAN=NU_AUTO_COPN"
condi = condi & " AND NU_AUTO_PENA_COPN=NU_AUTO_PENA AND TX_ACTU_ESTR='S'"
condi = condi & " AND TX_ESTA_ESTR IN ('IC','AC')"
If TipAutTerc = "T" Then
   condi = condi & " AND NU_AUTO_TERC_PENA=" & AutTerc
ElseIf TipAutTerc = "C" Then
   condi = condi & " AND NU_AUTO_COPN=" & AutTerc
End If
condi = condi & " AND NU_AUTO_CONT_TRAN=" & AutCont
condi = condi & " AND TX_TIPO_TRAN='C' AND NU_AUTO_GRCI_CRED=NU_AUTO_GRCI"
condi = condi & " AND NU_AUTO_GRCI_CRED=" & AutIncap & " GROUP BY NU_AUTO_GRCI"
Result = LoadData(Desde, Campos, condi, ArrValores())
If Result <> FAIL And Encontro Then
   If ArrValores(0) = "" Then ArrValores(0) = 0
   VlCreditos_Incapacidad = CDbl(ArrValores(0))
End If
If TipoTrans = "C" Then
   'CREDITO ACTUAL
   Campos = "SUM(NU_VLCRE_CRDE_TMP)"
   Desde = "CREDITO_DETALLE_TMP"
   condi = "NU_AUTO_CONE_CRDE_TMP =" & CLng(NumeTran)
   Result = LoadData(Desde, Campos, condi, ArrValores())
   If Result <> FAIL And Encontro Then
      If ArrValores(0) = "" Then ArrValores(0) = 0
      VlCreditos_Incapacidad = CDbl(ArrValores(0))
   End If
End If

VlCreditos_Incapacidad = RoundConDec(VlCreditos_Incapacidad)
End Function
'TipAutTerc=Tipo de autonum�rico del tercero T-Tercero,C-R_contrato_afiliado
Public Function VlReembolsos_Incapacidad(ByVal AutIncap As Double, ByVal AutTerc As Double, ByVal AutCont As Double, _
                                          ByVal TipAutTerc As String) As Double
Dim i As Integer
Dim ArrValores() As Variant
'******* REEMBOLSOS *********
ReDim ArrValores(0, 0)
Campos = "SUM(NU_VLPRE_REEM)"
Desde = "TRANSITO,ESTADO_TRANSITO,REEMBOLSO,PERSONA_NATURAL,GRUPO_CIE,R_CONTRATO_AFILIADO"
condi = "NU_NUME_TRAN=NU_NUME_TRAN_ESTR AND NU_NUME_TRAN=NU_NUME_TRAN_REEM"
condi = condi & " AND NU_AUTO_COPNBEN_TRAN=NU_AUTO_COPN"
condi = condi & " AND NU_AUTO_PENA_COPN=NU_AUTO_PENA AND TX_ESTA_ESTR='RR' AND TX_ACTU_ESTR='S'"
If TipAutTerc = "T" Then
   condi = condi & " AND NU_AUTO_TERC_PENA=" & AutTerc
ElseIf TipAutTerc = "C" Then
   condi = condi & " AND NU_AUTO_COPN=" & AutTerc
End If
condi = condi & " AND NU_AUTO_CONT_TRAN=" & AutCont
condi = condi & " AND NU_AUTO_GRCI_REEM=NU_AUTO_GRCI"
condi = condi & " AND NU_AUTO_GRCI_REEM=" & AutIncap
condi = condi & " AND NU_NUME_TRAN NOT IN( SELECT NU_NUME_TRAN FROM "
condi = condi & " TRANSITO,ESTADO_TRANSITO,REEMBOLSO,REEMBOLSO_PRESTACION,REEMBOLSO_DETALLE,PERSONA_NATURAL,"
condi = condi & " GRUPO_CIE,R_CONTRATO_AFILIADO"
condi = condi & " WHERE NU_NUME_TRAN=NU_NUME_TRAN_ESTR AND NU_NUME_TRAN=NU_NUME_TRAN_REEM"
condi = condi & " AND NU_AUTO_REEM=NU_AUTO_REEM_REPR AND NU_AUTO_REPR=NU_AUTO_REPR_REDE"
condi = condi & " AND NU_AUTO_COPNBEN_TRAN=NU_AUTO_COPN"
condi = condi & " AND NU_AUTO_PENA_COPN=NU_AUTO_PENA AND TX_ESTA_ESTR='RR' AND TX_ACTU_ESTR='S'"
If TipAutTerc = "T" Then
   condi = condi & " AND NU_AUTO_TERC_PENA=" & AutTerc
ElseIf TipAutTerc = "C" Then
   condi = condi & " AND NU_AUTO_COPN=" & AutTerc
End If
condi = condi & " AND NU_AUTO_CONT_TRAN=" & AutCont
condi = condi & " AND NU_AUTO_GRCI_REEM=NU_AUTO_GRCI"
condi = condi & " AND NU_AUTO_GRCI_REEM=" & AutIncap & ")"
condi = condi & " GROUP BY NU_AUTO_GRCI"
condi = condi & " UNION SELECT SUM(NU_AVLCOB_REDE)"
condi = condi & " FROM TRANSITO,ESTADO_TRANSITO,REEMBOLSO,REEMBOLSO_PRESTACION,REEMBOLSO_DETALLE,"
condi = condi & " PERSONA_NATURAL,GRUPO_CIE,R_CONTRATO_AFILIADO"
condi = condi & " WHERE NU_NUME_TRAN=NU_NUME_TRAN_ESTR AND NU_NUME_TRAN=NU_NUME_TRAN_REEM"
condi = condi & " AND NU_AUTO_REEM=NU_AUTO_REEM_REPR AND NU_AUTO_REPR=NU_AUTO_REPR_REDE"
condi = condi & " AND NU_AUTO_COPNBEN_TRAN=NU_AUTO_COPN"
condi = condi & " AND NU_AUTO_PENA_COPN=NU_AUTO_PENA AND TX_ACTU_ESTR='S'"
condi = condi & " AND TX_ESTA_ESTR IN('RR','IR','DIR','AR')"
If TipAutTerc = "T" Then
   condi = condi & " AND NU_AUTO_TERC_PENA=" & AutTerc
ElseIf TipAutTerc = "C" Then
   condi = condi & " AND NU_AUTO_COPN=" & AutTerc
End If
condi = condi & " AND NU_AUTO_CONT_TRAN=" & AutCont
condi = condi & " AND NU_AUTO_GRCI_REEM=NU_AUTO_GRCI"
condi = condi & " AND NU_AUTO_GRCI_REEM =" & AutIncap & " GROUP BY NU_AUTO_GRCI"
Result = LoadMulData(Desde, Campos, condi, ArrValores())
If Result <> FAIL And Encontro Then
   For i = 0 To UBound(ArrValores, 2)
      VlReembolsos_Incapacidad = VlReembolsos_Incapacidad + CDbl(ArrValores(0, i))
   Next
End If
VlReembolsos_Incapacidad = RoundConDec(VlReembolsos_Incapacidad)
End Function
'TipAutTerc=Tipo de autonum�rico del tercero T-Tercero,C-R_contrato_afiliado
Public Function VlConsumido_Incapacidad(ByVal AutIncap As Double, ByVal AutTerc As Double, ByVal AutCont As Double, _
                                       ByVal TipAutTerc As String, Optional ByVal NumeTran As Long, Optional ByVal TipoTrans As String) As Double
Dim VlConsumido As Double
VlConsumido = VlConsumido + VlPagado_Incapacidad(AutIncap, AutTerc, AutCont, TipAutTerc)
VlConsumido = VlConsumido + VlCreditos_Incapacidad(AutIncap, AutTerc, AutCont, TipAutTerc, NumeTran, TipoTrans)
VlConsumido = VlConsumido + VlReembolsos_Incapacidad(AutIncap, AutTerc, AutCont, TipAutTerc)
VlConsumido_Incapacidad = RoundConDec(VlConsumido)
End Function

Public Function PorcCoberturaXEdad(ByVal AutoNumContr As Double, ByVal FechaNaciBen As Date, ByVal FechaCobe As Date) As Double
Dim ArrTemp() As Variant
Dim EdadBen As Long
If Result <> FAIL Then
   'buscar la cobertura por edad
   'EdadBen = DateDiff("yyyy", FechaNaciBen, FechaCobe)
   Result = Calcular_EdadPersona(FechaNaciBen, FechaCobe, EdadBen)
   ReDim ArrTemp(0)
   Campos = "NU_COBE_CEDC"
   condi = "NU_EDINI_CEDC<=" & EdadBen & " AND NU_EDFIN_CEDC>=" & EdadBen
   condi = condi & " AND NU_AUTO_CONT_CEDC=" & AutoNumContr
   Result = LoadData("COBERTURA_EDAD_CONTRATO", Campos, condi, ArrTemp())
   If Result <> FAIL And Encontro Then
      PorcCoberturaXEdad = CDbl(ArrTemp(0))
   End If
End If
End Function

'Cad:    Indicador Estado del transito (Sigla)
'Estado: Descripcion estado del transito
'Tipo:   Tipo de estado del transito (C: Credito,R: Reembolso)

Function Estado_Cred_Reem(ByVal Cad As String, ByRef Estado As String, ByRef Tipo As String) As Integer

   Select Case Cad
      'Credito
      Case "AC":  Estado = "Cr�dito Aprobado": Tipo = "C"
      Case "DC":  Estado = "Cr�dito Devuelto": Tipo = "C"
      Case "IC":  Estado = "Cr�dito Ingresado": Tipo = "C"
      Case "NC":  Estado = "Cr�dito Negado": Tipo = "C"
      Case "ANC": Estado = "Cr�dito Anulado": Tipo = "C"
      'Reembolso
      Case "RR":  Estado = "Reembolso Recibido": Tipo = "R"
      Case "RER": Estado = "Reembolso Recibido para Estudio": Tipo = "R"
      Case "IR":  Estado = "Reembolso Ingresado": Tipo = "R"
      Case "DIR": Estado = "Reembolso Distribuido": Tipo = "R"
      Case "DR":  Estado = "Reembolso Devuelto": Tipo = "R"
      Case "NR":  Estado = "Reembolso Negado": Tipo = "R"
      Case "AR":  Estado = "Reembolso Auditado": Tipo = "R"
      Case "PR":  Estado = "Reembolso Pagado": Tipo = "R"
      Case "ANR":  Estado = "Reembolso Anulado": Tipo = "R"
   End Select
   Estado_Cred_Reem = 1
End Function


Function Cargue_Reembolso_Detalle(ByVal NumTransito As Long, ByVal AutReemb As Double, ByVal AutTerc As Long) As Integer
Dim ArrTemp()     As Variant
Dim ArrCiud()    As Variant
Dim AutReembPrest As Double
Dim i             As Integer
Dim AutCiudad     As Double

   ReDim ArrCiud(0)
   Campos = "NU_AUTO_CIUD_SECT"
   Desde = "TERCERO,DIRECCION,SECTOR"
   condi = "NU_AUTO_TERC=NU_AUTO_TERC_DIRE"
   condi = condi & " AND NU_AUTO_SECT_DIRE=NU_AUTO_SECT AND TX_ESTA_DIRE='A'"
   condi = condi & " AND NU_AUTO_TERC=" & Comi & AutTerc & Comi
   Result = LoadData(Desde, Campos, condi, ArrCiud())
   If Result <> FAIL And Encontro Then
      AutCiudad = ArrCiud(0)
   Else
      Call Mensaje1("No se pudo obtener la ciudad del prestador", 3)
      Result = FAIL
   End If
   
   ReDim ArrTemp(21, 0)
   Campos = "NU_AUTO_TIAT_CRED,NU_AUTO_ATEN_CRED,NU_AUTO_PRES_CRDE,NU_CANT_CRDE,NU_VLPRES_CRDE,TX_COBE_CRDE,"
   Campos = Campos & "NU_AUTO_COPL_CRDE,NU_VLLIQ_CRDE,NU_AUTO_DEDU_CRDE,"
   Campos = Campos & "NU_VLDED_CRDE,NU_VLBAS_CRDE,NU_PORCOB_CRDE,NU_CANCOB_CRDE,NU_VLCOB_CRDE,NU_PORCRE_CRDE,NU_VLCRE_CRDE,"
   Campos = Campos & "NU_VLXCOB_CRDE,NU_VLAPAG_CRDE,NU_AUTO_CONV_CRDE,NU_VLCONV_CRDE,TX_TICUAD_CRDE,TX_TICIUD_CRDE"
   Desde = "CREDITO,CREDITO_DETALLE"
   condi = "NU_AUTO_CRED=NU_AUTO_CRED_CRDE AND NU_NUME_TRAN_CRED=" & NumTransito
   If Result <> FAIL Then Result = LoadMulData(Desde, Campos, condi, ArrTemp())
   If Result <> FAIL And Encontro Then
      For i = 0 To UBound(ArrTemp, 2)
         Valores = "NU_AUTO_REEM_REPR=" & AutReemb & Coma
         Valores = Valores & "NU_AUTO_PRES_REPR=" & ArrTemp(2, i)
         If Result <> FAIL Then Result = DoInsertSQL("REEMBOLSO_PRESTACION", Valores)
         condi = "NU_AUTO_REEM_REPR=" & AutReemb & " AND NU_AUTO_PRES_REPR=" & ArrTemp(2, i)
         If Result <> FAIL Then AutReembPrest = GetMaxCod("REEMBOLSO_PRESTACION", "NU_AUTO_REPR", condi)
         Valores = "NU_AUTO_REPR_REDE=" & AutReembPrest & Coma
         Valores = Valores & "NU_AUTO_REFA_REDE=NULL" & Coma
         Valores = Valores & "NU_AUTO_TIAT_REDE=" & ArrTemp(0, i) & Coma
         Valores = Valores & "NU_AUTO_ATEN_REDE=" & ArrTemp(1, i) & Coma
         Valores = Valores & "NU_AUTO_MTIT_REDE=NULL" & Coma
         Valores = Valores & "NU_CANT_REDE=" & ArrTemp(3, i) & Coma
         Valores = Valores & "NU_VLPRES_REDE=" & ArrTemp(4, i) & Coma
         Valores = Valores & "TX_COBE_REDE=" & Comi & ArrTemp(5, i) & Comi & Coma
         Valores = Valores & "NU_AUTO_COPL_REDE=" & IIf(ArrTemp(5, i) = "S", ArrTemp(6, i), "NULL") & Coma
         Valores = Valores & "NU_VLLIQ_REDE=" & ArrTemp(7, i) & Coma
         Valores = Valores & "NU_AUTO_DEDU_REDE=" & IIf(ArrTemp(8, i) <> 0, ArrTemp(8, i), "NULL") & Coma
         Valores = Valores & "NU_VLDED_REDE=" & ArrTemp(9, i) & Coma
         Valores = Valores & "NU_VLBAS_REDE=" & ArrTemp(10, i) & Coma
         Valores = Valores & "NU_PORCOB_REDE=" & ArrTemp(11, i) & Coma
         Valores = Valores & "NU_CANCOB_REDE=" & ArrTemp(12, i) & Coma
         Valores = Valores & "NU_VLCOB_REDE=" & ArrTemp(13, i) & Coma
         Valores = Valores & "NU_PORCRE_REDE=" & ArrTemp(14, i) & Coma
         Valores = Valores & "NU_VLCRE_REDE=" & ArrTemp(15, i) & Coma
         Valores = Valores & "NU_VLXCOB_REDE=" & ArrTemp(16, i) & Coma
         Valores = Valores & "NU_VLAPAG_REDE=" & ArrTemp(17, i) & Coma
         Valores = Valores & "NU_ACANCOB_REDE=" & ArrTemp(12, i) & Coma
         Valores = Valores & "NU_AVLLIQ_REDE=" & ArrTemp(7, i) & Coma
         Valores = Valores & "NU_AVLDED_REDE=" & ArrTemp(9, i) & Coma
         Valores = Valores & "NU_AVLBAS_REDE=" & ArrTemp(10, i) & Coma
         Valores = Valores & "TX_ACOBE_REDE=" & Comi & ArrTemp(5, i) & Comi & Coma
         Valores = Valores & "NU_APORCOB_REDE=" & ArrTemp(11, i) & Coma
         Valores = Valores & "NU_AVLCOB_REDE=" & ArrTemp(13, i) & Coma
         Valores = Valores & "NU_AVLXCOB_REDE=" & ArrTemp(16, i) & Coma
         Valores = Valores & "NU_AVLAPAG_REDE=" & ArrTemp(17, i) & Coma
         If ArrTemp(18, i) <> "" Then
            Valores = Valores & "NU_AUTO_CONV_REDE=" & ArrTemp(18, i) & Coma
         Else
            Valores = Valores & "NU_AUTO_CONV_REDE=NULL" & Coma
         End If
         Valores = Valores & "NU_VLCONV_REDE=" & ArrTemp(19, i) & Coma
         Valores = Valores & "TX_MOTI_REDE=''" & Coma
         Valores = Valores & "TX_TICUAD_REDE=" & Comi & ArrTemp(20, i) & Comi & Coma
         Valores = Valores & "TX_TICIUD_REDE=" & Comi & ArrTemp(21, i) & Comi
         If Result <> FAIL Then Result = DoInsertSQL("REEMBOLSO_DETALLE", Valores)
      Next
   End If
   Valores = "NU_AUTO_TERC_REFA=" & AutTerc & Coma
   Valores = Valores & "NU_AUTO_CIUD_REFA=" & AutCiudad
   condi = "NU_AUTO_REEM_REFA=" & AutReemb
   If Result <> FAIL Then Result = DoUpdate("REEMBOLSO_FACTURA", Valores, condi)

   Cargue_Reembolso_Detalle = Result
   
End Function

Function Grabar_Orden_Pago(ByVal MSF_CXpagar As MSFlexGrid, ByVal NumTran As Long) As Integer
Dim i As Integer

With MSF_CXpagar
   For i = 1 To .Rows - 1
      If CDbl(.TextMatrix(i, 2)) > 0 Then
         Valores = "NU_NUME_TRAN_ORPA=" & NumTran & Coma
         Valores = Valores & "NU_AUTO_TERC_ORPA=" & .TextMatrix(i, 0) & Coma
         Valores = Valores & "NU_VLBASE_ORPA=" & .TextMatrix(i, 2) & Coma
         Valores = Valores & "NU_VLCOPA_ORPA=" & .TextMatrix(i, 3) & Coma
         Valores = Valores & "NU_VLDESC_ORPA=" & .TextMatrix(i, 6) & Coma
         Valores = Valores & "NU_VLNETO_ORPA=" & .TextMatrix(i, 7)
         If Result <> FAIL Then Result = DoInsertSQL("ORDEN_PAGO", Valores)
      End If
   Next
End With

Grabar_Orden_Pago = Result

End Function

'DocRec: Ducumento para el recaudo
         'CA01- Cancelado con garantia
         'CA02- Cancelado con cruce
         'CA03- Cancelado en el cr�dito.

Function Grabar_Copago_Gen(ByVal Msf_Copago As MSFlexGrid, ByVal NumTran As Long, ByVal BandGarantia As Boolean, _
                           ByVal DocRec As String, ByVal AutMotiCopa As Double) As Integer
Dim i As Integer
Dim AutNumTiMo As Double

With Msf_Copago
   For i = 1 To .Rows - 1
      Valores = "TX_TIPO_TIMO='CO'" & Coma
      Valores = Valores & "NU_AUTO_NUDO_TIMO=NULL" & Coma
      Valores = Valores & "TX_FACT_TIMO=" & Comi & NumTran & Comi & Coma
      'If BandGarantia Or DocRec = "CA03" Then
      If BandGarantia Then
         Valores = Valores & "TX_ESTA_TIMO='C'" & Coma
      Else
         Valores = Valores & "TX_ESTA_TIMO='S'" & Coma
      End If
      Valores = Valores & "FE_GENE_TIMO=" & FFechaIns(Hoy) & Coma
      Valores = Valores & "FE_VENC_TIMO=" & FFechaIns(Hoy) & Coma
      Valores = Valores & "NU_VALO_TIMO=" & .TextMatrix(i, 2) & Coma
      Valores = Valores & "NU_AUTO_TERC_TIMO=" & .TextMatrix(i, 0) & Coma
      Valores = Valores & "NU_AUTO_NEGO_TIMO=" & .TextMatrix(i, 6) & Coma
      Valores = Valores & "NU_AUTO_SUCU_TIMO=" & .TextMatrix(i, 7) & Coma
      Valores = Valores & "NU_AUTO_BROK_TIMO=NULL" & Coma
      Valores = Valores & "NU_AUTO_VEND_TIMO=NULL" & Coma
      Valores = Valores & "NU_AUTO_PLAN_TIMO=" & .TextMatrix(i, 8) & Coma
      Valores = Valores & "FE_INVI_TIMO=" & FFechaIns(Hoy) & Coma
      Valores = Valores & "FE_FIVI_TIMO=" & FFechaIns(Hoy) & Coma
      Valores = Valores & "FE_INPE_TIMO=" & FFechaIns(Hoy) & Coma
      Valores = Valores & "FE_FIPE_TIMO=" & FFechaIns(Hoy) & Coma
      Valores = Valores & "NU_AUTO_PEPA_TIMO=NULL" & Coma
      Valores = Valores & "NU_AUTO_FOPA_TIMO=NULL" & Coma
      Valores = Valores & "NU_AUTO_BANC_TIMO=NULL" & Coma
      Valores = Valores & "TX_CUEN_TIMO=''" & Coma
      Valores = Valores & "NU_AUTO_CONT_TIMO=" & .TextMatrix(i, 5) & Coma
      'If BandGarantia Or DocRec = "CA03" Then
      If BandGarantia Then
         Valores = Valores & "NU_SALD_TIMO=0" & Coma
      Else
         Valores = Valores & "NU_SALD_TIMO=" & .TextMatrix(i, 2) & Coma
      End If
      Valores = Valores & "NU_AUTO_DIRE_TIMO=NULL" & Coma
      Valores = Valores & "TX_RUTI_TIMO='N'" & Coma
      Valores = Valores & "NU_AUTO_CONE_TIMO=" & NumConex
      If Result <> FAIL Then Result = DoInsertSQL("TIPO_MOVIMIENTO", Valores)
      'guardar el Motivo del copago
      Valores = "NU_AUTO_MTCO_TRAN=" & AutMotiCopa
      condi = "NU_NUME_TRAN=" & NumTran
      If Result <> FAIL Then Result = DoUpdate("TRANSITO", Valores, condi)
      'If BandGarantia Or DocRec = "CA03" Then
      If BandGarantia Then
         condi = "TX_FACT_TIMO=" & Comi & NumTran & Comi & " AND TX_TIPO_TIMO='CO'"
         If Result <> FAIL Then AutNumTiMo = GetMaxCod("TIPO_MOVIMIENTO", "NU_AUTO_TIMO", condi)
         Valores = "TX_DOCS_RECA=" & Comi & DocRec & Comi & Coma
         Valores = Valores & "NU_DOCS_RECA=" & NumTran & Coma
         Valores = Valores & "FE_FECH_RECA=" & FFechaIns(Hoy) & Coma
         Valores = Valores & "NU_AUTO_TIMO_RECA=" & AutNumTiMo & Coma
         Valores = Valores & "NU_VALO_RECA=" & .TextMatrix(i, 2) & Coma
         Valores = Valores & "TX_ESTA_RECA='AC'"
         If Result <> FAIL Then Result = DoInsertSQL("RECAUDO", Valores)
      End If
   Next
End With

Grabar_Copago_Gen = Result
End Function

'DocRec: Ducumento para el recaudo
         'CA01- Cancelado con garantia
         'CA02- Cancelado con cruce
         'CA03- Cancelado en el cr�dito.

Function Cruzar_Copago(ByVal Msf_CopagoPend As MSFlexGrid, ByVal NumTran As Long) As Integer
Dim ArrTemp() As Variant
Dim i As Integer

With Msf_CopagoPend
   For i = 1 To .Rows - 1
      If .TextMatrix(i, 0) = "S" Then 'si el copago esta seleccionado
         Valores = "NU_SALD_TIMO=NU_SALD_TIMO"
         condi = "NU_AUTO_TIMO=" & .TextMatrix(i, 2) 'Bloquear el registro del copago
         If Result <> FAIL Then Result = DoUpdate("TIPO_MOVIMIENTO", Valores, condi)
         ReDim ArrTemp(0)
         Campos = "NU_SALD_TIMO"
         Desde = "TIPO_MOVIMIENTO"
         condi = "NU_AUTO_TIMO=" & .TextMatrix(i, 2)
         If Result <> FAIL Then Result = LoadData(Desde, Campos, condi, ArrTemp())
         If Result <> FAIL Then
            If Encontro Then
               If CDbl(ArrTemp(0)) <> CDbl(.TextMatrix(i, 5)) Then
                  Call Mensaje1("El saldo del copago ha cambiado", 3)
                  Result = FAIL
               End If
            Else
               Call Mensaje1("No se encontro el saldo del copago", 3)
               Result = FAIL
            End If
         End If
      End If
   Next
   'actualizar el estado de los copagos y registrar el cruce
   For i = 1 To .Rows - 1
      If .TextMatrix(i, 0) = "S" Then 'si el copago esta seleccionado
         Valores = "NU_SALD_TIMO=" & .TextMatrix(i, 7)
         If CDbl(.TextMatrix(i, 7)) = 0 Then
            Valores = Valores & Coma & "TX_ESTA_TIMO='C'"
         End If
         condi = "NU_AUTO_TIMO=" & .TextMatrix(i, 2)
         If Result <> FAIL Then Result = DoUpdate("TIPO_MOVIMIENTO", Valores, condi)
         Valores = "TX_DOCS_RECA='CA02'" & Coma
         Valores = Valores & "NU_DOCS_RECA=" & NumTran & Coma
         Valores = Valores & "FE_FECH_RECA=" & FFechaIns(Hoy) & Coma
         Valores = Valores & "NU_AUTO_TIMO_RECA=" & .TextMatrix(i, 2) & Coma
         Valores = Valores & "NU_VALO_RECA=" & .TextMatrix(i, 6) & Coma
         Valores = Valores & "TX_ESTA_RECA='AC'"
         If Result <> FAIL Then Result = DoInsertSQL("RECAUDO", Valores)
      End If
   Next
End With

Cruzar_Copago = Result

End Function

'TipoReemb: Tipo de reembolso
            'NO: Reembolso(Normal)
            'CR: Credito
            
Function Actualizar_Reembolso(ByVal NumTran As Long, ByVal TipoReemb As String) As Integer
Dim ArrTemp() As Variant
Dim i As Integer

   ReDim ArrTemp(3)
   Campos = "SUM(NU_AVLDED_REDE),SUM(NU_AVLCOB_REDE),SUM(NU_AVLAPAG_REDE),SUM(NU_AVLXCOB_REDE)"
   Desde = "REEMBOLSO,REEMBOLSO_PRESTACION,REEMBOLSO_DETALLE"
   condi = "NU_AUTO_REEM=NU_AUTO_REEM_REPR AND NU_AUTO_REPR=NU_AUTO_REPR_REDE"
   condi = condi & " AND NU_NUME_TRAN_REEM=" & NumTran
   If Result <> FAIL Then Result = LoadData(Desde, Campos, condi, ArrTemp())
   If Result <> FAIL And Encontro Then
      For i = 0 To UBound(ArrTemp, 1)
         If ArrTemp(i) = "" Then ArrTemp(i) = 0
      Next
      Valores = "NU_VLDED_REEM=" & ArrTemp(0) & Coma
      Valores = Valores & "NU_VLCUB_REEM=" & ArrTemp(1) & Coma
      Valores = Valores & "NU_VLGADM_REEM=0" & Coma
      Valores = Valores & "NU_VLXPAG_REEM=" & ArrTemp(2) & Coma
      Valores = Valores & "NU_VLXCOB_REEM=" & ArrTemp(3)
      condi = "NU_NUME_TRAN_REEM=" & NumTran
      If Result <> FAIL Then Result = DoUpdate("REEMBOLSO", Valores, condi)
   End If
   If TipoReemb = "NO" Then 'guardar los gastos administrativos
      ReDim ArrTemp(0)
      Campos = "SUM(NU_VLDESC_ORPA)"
      Desde = "ORDEN_PAGO"
      condi = "NU_NUME_TRAN_ORPA=" & NumTran
      If Result <> FAIL Then Result = LoadData(Desde, Campos, condi, ArrTemp())
      If Result <> FAIL And Encontro Then
         If ArrTemp(0) = "" Then ArrTemp(0) = 0
         Valores = "NU_VLGADM_REEM=" & ArrTemp(0)
         condi = "NU_NUME_TRAN_REEM=" & NumTran
         If Result <> FAIL Then Result = DoUpdate("REEMBOLSO", Valores, condi)
      End If
   End If
   

Actualizar_Reembolso = Result
End Function

Function Contabilizar_Orden_Pago(ByVal NumTran As Long, ByVal TipoReemb As String, ByRef ItemCompCont As Comprobante, ByRef ArrCompCont As Variant) As Integer
Dim ArrMulTemp() As Variant
Dim i As Integer

   'contabilizar ordenes de pago
   If Result <> FAIL Then
      ReDim ArrMulTemp(0, 0)
      condi = "NU_NUME_TRAN_ORPA=" & NumTran
      If Result <> FAIL Then Result = LoadMulData("ORDEN_PAGO", "NU_AUTO_ORPA", condi, ArrMulTemp())
      If Result <> FAIL And Encontro Then
         For i = 0 To UBound(ArrMulTemp, 2)
            If Result <> FAIL Then Result = Apot_OrdenPago(ArrMulTemp(0, i), "", TipoReemb, ItemCompCont)
            If Result <> FAIL Then
               ReDim Preserve ArrCompCont(3, UBound(ArrCompCont, 2) + 1)
               ArrCompCont(0, UBound(ArrCompCont, 2)) = ItemCompCont.Contabilidad
               ArrCompCont(1, UBound(ArrCompCont, 2)) = ItemCompCont.TipoDocumento
               ArrCompCont(2, UBound(ArrCompCont, 2)) = ItemCompCont.Fecha
               ArrCompCont(3, UBound(ArrCompCont, 2)) = ItemCompCont.Numero
            End If
         Next
      End If
   End If
   'Contabilizar los copagos
   If Result <> FAIL Then
      ReDim ArrMulTemp(0, 0)
      condi = "TX_FACT_TIMO=" & Comi & NumTran & Comi & " AND TX_TIPO_TIMO='CO' AND TX_ESTA_TIMO='S'"
      If Result <> FAIL Then Result = LoadMulData("TIPO_MOVIMIENTO", "NU_AUTO_TIMO", condi, ArrMulTemp())
      If Result <> FAIL And Encontro Then
         For i = 0 To UBound(ArrMulTemp, 2)
            If Result <> FAIL Then Result = Apot_Copago(ArrMulTemp(0, i), "", ItemCompCont)
            If Result <> FAIL Then
               ReDim Preserve ArrCompCont(3, UBound(ArrCompCont, 2) + 1)
               ArrCompCont(0, UBound(ArrCompCont, 2)) = ItemCompCont.Contabilidad
               ArrCompCont(1, UBound(ArrCompCont, 2)) = ItemCompCont.TipoDocumento
               ArrCompCont(2, UBound(ArrCompCont, 2)) = ItemCompCont.Fecha
               ArrCompCont(3, UBound(ArrCompCont, 2)) = ItemCompCont.Numero
            End If
         Next
      End If
   End If

Contabilizar_Orden_Pago = Result
End Function

Public Function Autorizar_Pago_Credito(ByVal MSF_CXpagar As MSFlexGrid, ByVal Msf_Copago As MSFlexGrid, _
                                        ByVal NumTran As Long, ByVal BandGarantia As Boolean, ByVal Interfase As Byte, _
                                        ByRef ItemCompCont As Comprobante, ByRef ArrCompCont As Variant, _
                                        ByVal AutMotiCopa As Double) As Integer

Result = Grabar_Orden_Pago(MSF_CXpagar, NumTran) 'grabar las ordenes de pago
If Result <> FAIL Then Result = Grabar_Copago_Gen(Msf_Copago, NumTran, BandGarantia, "CA03", AutMotiCopa) 'grabar los copagos generados
If Result <> FAIL Then Result = Actualizar_Reembolso(NumTran, "CR")  'Actualizar Reembolso
If Result <> FAIL Then Result = Ingresar_EstadoTran(NumTran, "AR")
If Result <> FAIL And Interfase = 1 Then Result = Contabilizar_Orden_Pago(NumTran, "CR", ItemCompCont, ArrCompCont) 'contabilizar ordenes de pago
Autorizar_Pago_Credito = Result

End Function

Function Buscar_Tope_Incapacidad(ByVal AutIncap As Double, ByVal AutPlan As Double) As Double
Dim ArrTemp() As Variant
ReDim ArrTemp(0)
'Buscar el tope por cobertura
Campos = "NU_VALO_COPL"
Desde = "PLAN,COBERTURA_PLAN,R_COBEPLAN_CLASIFDIAG,R_DIAG_CLASDIAG,R_DIAG_CIE,GRUPO_CIE"
condi = "NU_AUTO_PLAN=NU_AUTO_PLAN_COPL AND NU_AUTO_PLAN=" & AutPlan
condi = condi & " AND NU_AUTO_COPL=NU_AUTO_COPL_CPCD"
condi = condi & " AND NU_AUTO_CLDI_CPCD=NU_AUTO_CLDI_DICD"
condi = condi & " AND NU_AUTO_DIAG_DICI=NU_AUTO_DIAG_DICD"
condi = condi & " AND NU_AUTO_GRCI_DICI=NU_AUTO_GRCI"
condi = condi & " AND NU_AUTO_GRCI=" & AutIncap
condi = condi & " AND NU_VALO_COPL>0 ORDER BY NU_VALO_COPL DESC"
If Result <> FAIL Then
Result = LoadData(Desde, Campos, condi, ArrTemp())
End If
If Result <> FAIL And Encontro Then
   If ArrTemp(0) = "" Then ArrTemp(0) = 0
   Buscar_Tope_Incapacidad = ArrTemp(0)
End If
End Function

Public Sub Anular_Transito(ByVal NumTran As Double)
Dim ArrTemp(), ArrMulTemp(), ArrOrPa() As Variant
Dim EstadoTran As String
Dim Interfase  As Byte        'indica si tiene activa la interfase con Apoteosys 1-Si, 0-No
Dim i As Integer
Interfase = CargarParametro("12")
'buscar el estado actual del transito
ReDim ArrTemp(0)
Campos = "TX_ESTA_ESTR"
Desde = "ESTADO_TRANSITO"
condi = "NU_NUME_TRAN_ESTR=" & NumTran & " AND TX_ACTU_ESTR='S'"
Result = LoadData(Desde, Campos, condi, ArrTemp())
If Result <> FAIL And Encontro Then
   EstadoTran = ArrTemp(0)
Else
   Call Mensaje1("No se pudo obtener el Estado del transito", 3)
   Exit Sub
End If
Select Case EstadoTran
   Case "ANR": Call Mensaje1("El transito ya se encuentra anulado", 3)
               Exit Sub
   Case Else: 'verificar en apoteosys que Las ordenes de pago generadas no hayan sido canceladas
               FrmAnulReemb.NumTran = NumTran
               FrmAnulReemb.Show 1
End Select
End Sub

Public Sub Imprimir_Carta_Negacion(ByRef Titulo As String, ByRef Archivo As String, ByRef Formula As String, ByRef No_Copias As Integer, ByRef Destino As Boolean, Arr_Datos() As String, Tipo As Integer)
err.Clear

   With MDI_Plantilla.Crys_Listar
      On Error GoTo Control
      Call Limpiar_CrysListar
      .WindowTitle = Titulo
      If Destino Then
         .Destination = crptToPrinter
         .CopiesToPrinter = No_Copias
      Else
         .Destination = crptToWindow
      End If
      If Tipo = 1 Then 'Credito
         .Formulas(0) = "Titular=" & Comi & Arr_Datos(0) & Comi
         .Formulas(1) = "Tipo_Credito=" & Comi & Arr_Datos(1) & Comi
         .Formulas(2) = "Beneficiario=" & Comi & Arr_Datos(2) & Comi
         .Formulas(3) = "ID_Beneficiario=" & Comi & Arr_Datos(3) & Comi
         .Formulas(4) = "Tipo_Atencion=" & Comi & Arr_Datos(4) & Comi
      ElseIf Tipo = 2 Then 'Reembolso
         .Formulas(0) = "Titular=" & Comi & Arr_Datos(0) & Comi
         .Formulas(1) = "Beneficiario=" & Comi & Arr_Datos(1) & Comi
         .Formulas(2) = "ID_Beneficiario=" & Comi & Arr_Datos(2) & Comi
      End If
      .ReportFileName = Archivo
      .SelectionFormula = Formula
      Debug.Print "Formula de selecci�n " & .SelectionFormula
      .Action = 1
   End With

Control:

   If err.Description <> "" Then Call Mensaje1(err.Number & ": " & err.Description, 1)

End Sub

Function Validar_Cobertura_Diagnostico(ByVal AutoDiag As Double, ByVal AutoPlan As Double) As Integer
Dim ArrTemp() As Variant
Dim ArrMulTemp() As Variant
'validar si el diagn�stico se encuentra excluido
ReDim ArrTemp(0)
Result = LoadData("DIAGNOSTICO", "TX_EXCL_DIAG", "NU_AUTO_DIAG=" & AutoDiag, ArrTemp())
If Result <> FAIL And Encontro Then
   If ArrTemp(0) = "S" Then
      Call Mensaje1("El Diagn�stico no tiene cobertura porque se encuentra excluido", 3)
      Validar_Cobertura_Diagnostico = FAIL
      Exit Function
   End If
End If
'buscar las coberturas del plan que no tienen clasificaci�n de diagn�sticos
ReDim ArrMulTemp(0, 0)
Campos = "NU_AUTO_COPL"
Desde = "COBERTURA_PLAN"
condi = "NU_AUTO_PLAN_COPL=" & AutoPlan & " AND NU_AUTO_COPL NOT IN(SELECT NU_AUTO_COPL_CPCD"
condi = condi & " FROM COBERTURA_PLAN,R_COBEPLAN_CLASIFDIAG WHERE NU_AUTO_PLAN_COPL=" & AutoPlan
condi = condi & " AND NU_AUTO_COPL=NU_AUTO_COPL_CPCD)"
Result = LoadMulData(Desde, Campos, condi, ArrMulTemp())
If Result <> FAIL And Encontro Then 'si encuentra el diagn�stico tiene cobertura
   Validar_Cobertura_Diagnostico = SUCCEED
   Exit Function
End If
'buscar el diagn�stico en las coberturas del plan que tienen clasificaci�n de prestaci�n
ReDim ArrMulTemp(0, 0)
Campos = "NU_AUTO_COPL"
Desde = "COBERTURA_PLAN,R_COBEPLAN_CLASIFDIAG,CLASIFICACION_DIAGNOSTICO,R_DIAG_CLASDIAG"
condi = "NU_AUTO_COPL=NU_AUTO_COPL_CPCD AND NU_AUTO_CLDI_CPCD=NU_AUTO_CLDI"
condi = condi & " AND NU_AUTO_CLDI=NU_AUTO_CLDI_DICD AND NU_AUTO_PLAN_COPL=" & AutoPlan
condi = condi & " AND NU_AUTO_DIAG_DICD=" & AutoDiag
Result = LoadMulData(Desde, Campos, condi, ArrMulTemp())
If Result <> FAIL And Encontro Then
   Validar_Cobertura_Diagnostico = SUCCEED
Else
   Call Mensaje1("El Diagn�stico no tiene cobertura porque no se encuentra incluido en ninguna cobertura del plan", 3)
   Validar_Cobertura_Diagnostico = FAIL
End If
End Function

'TipoVal=tipo de valores que se mostraran L-Liquidado,A-Auditado
Sub Cargar_Totales_Transito(ByVal NumTran As Double, ByVal TipoVal As String, ByRef Frm_OrdenPago As Frame _
                           , ByRef Msf_OrdenPago As MSFlexGrid, ByRef Frm_Copago As Frame, ByRef Msf_Copago As MSFlexGrid _
                           , ByRef Frm_CopagoPend As Frame, ByRef Msf_CopagoPend As MSFlexGrid, ByRef Img As ImageList _
                           , Optional ByRef TipoReemb As String, Optional ByRef BanGaran, Optional ByVal EstadoTransito As String = NUL$)
'Dim TipoReemb As String
Dim BandGastAdm As Boolean
Dim BandGarantia As Boolean
Dim PorcRetePeJu As Double, PorcRetePeNa As Double
Dim VlBase As Double
Dim VlCopago As Double
Dim VlBaseAux As Double
Dim Descuento As Double
Dim VlDesc As Double
Dim VlXPagar As Double
Dim SaldoOrPa As Double
Dim SaldoCopa As Double
Dim i As Integer
Dim j As Integer
Dim TipoDesc As String
ReDim ArrTemp(1)
Campos = "TX_TIPO_REEM,TX_ADMI_REEM"
Desde = "REEMBOLSO,TERCERO"
condi = "NU_AUTO_TERC_REEM=NU_AUTO_TERC AND NU_NUME_TRAN_REEM=" & NumTran
Result = LoadData(Desde, Campos, condi, ArrTemp())
If Result <> FAIL And Encontro Then
   TipoReemb = ArrTemp(0)
   BandGastAdm = IIf(ArrTemp(1) = "S", True, False)
Else
   Call Mensaje1("No se encontraron los datos del transito", 3)
   Exit Sub
End If
'Copagos
If TipoReemb = "NO" Then
   Frm_Copago.Visible = False
   Frm_CopagoPend.Visible = True
   Desde = "TRANSITO,R_CONTRATO_AFILIADO,PERSONA_NATURAL,TIPO_MOVIMIENTO"
   condi = "NU_AUTO_COPNTIT_TRAN=NU_AUTO_COPN AND NU_AUTO_PENA_COPN=NU_AUTO_PENA"
   condi = condi & " AND NU_AUTO_TERC_PENA=NU_AUTO_TERC_TIMO"
   condi = condi & " AND NU_AUTO_CONT_TRAN=NU_AUTO_CONT_TIMO"
'   condi = condi & " AND NU_AUTO_MTCO_TRAN=NU_AUTO_MTCO"
   condi = condi & " AND TX_TIPO_TIMO='CO' AND NU_NUME_TRAN=" & NumTran
   If EstadoTransito <> "AR" Or EstadoTransito <> "ANR" Then
      Campos = "'','',NU_AUTO_TIMO,TX_FACT_TIMO,FE_GENE_TIMO,NU_SALD_TIMO,0,NU_SALD_TIMO,'',''"
      condi = condi & " AND TX_ESTA_TIMO='S'"
   Else
      Campos = "'','',NU_AUTO_TIMO,TX_FACT_TIMO,FE_GENE_TIMO,NU_VALO_RECA,0,NU_VALO_RECA,'',''"
      Desde = Desde & ",RECAUDO"
      condi = condi & " AND TX_ESTA_TIMO='C'"
      condi = condi & " AND NU_AUTO_TIMO=NU_AUTO_TIMO_RECA"
      condi = condi & " AND NU_DOCS_RECA=" & NumTran
   End If
   condi = condi & " ORDER BY FE_GENE_TIMO ASC"
   Result = LoadfGrid(Msf_CopagoPend, Desde, Campos, condi)
   For i = 1 To Msf_CopagoPend.Rows - 1
      ReDim ArrTemp(1)
      Campos = "NU_AUTO_MTCO,TX_DESC_MTCO"
      Desde = "TRANSITO,MOTIVO_COPAGO"
      condi = "NU_AUTO_MTCO_TRAN=NU_AUTO_MTCO AND NU_NUME_TRAN=" & Msf_CopagoPend.TextMatrix(i, 3)
      Result = LoadData(Desde, Campos, condi, ArrTemp())
      If Result <> FAIL And Encontro Then
         Msf_CopagoPend.TextMatrix(i, 8) = ArrTemp(0)
         Msf_CopagoPend.TextMatrix(i, 9) = ArrTemp(1)
      End If
   Next
ElseIf TipoReemb = "CR" Then
   Frm_Copago.Visible = True
   Frm_CopagoPend.Visible = False
   Msf_Copago.Rows = 1
   ReDim ArrTemp(6)
   Campos = "NU_AUTO_TERC,TX_DESC_TERC,SUM(" & IIf(TipoVal = "A", "NU_AVLXCOB_REDE)", "NU_VLXCOB_REDE)")
   Campos = Campos & ",NU_AUTO_CONT_TRAN,NU_AUTO_NEGO_CONT,NU_AUTO_EMPR_REEM,NU_AUTO_PLAN_TRAN"
   Desde = "TRANSITO,CONTRATO,REEMBOLSO,REEMBOLSO_PRESTACION,REEMBOLSO_DETALLE,TERCERO,R_CONTRATO_AFILIADO,PERSONA_NATURAL"
   condi = "NU_AUTO_CONT_TRAN=NU_AUTO_CONT AND NU_NUME_TRAN=NU_NUME_TRAN_REEM"
   condi = condi & " AND NU_AUTO_REEM=NU_AUTO_REEM_REPR AND NU_AUTO_REPR=NU_AUTO_REPR_REDE"
   condi = condi & " AND NU_AUTO_COPNTIT_TRAN=NU_AUTO_COPN AND NU_AUTO_PENA_COPN=NU_AUTO_PENA"
   condi = condi & " AND NU_AUTO_TERC_PENA=NU_AUTO_TERC"
'   condi = condi & " AND NU_AUTO_TERC_CONT=NU_AUTO_TERC"
   condi = condi & " AND NU_NUME_TRAN_REEM=" & NumTran
   condi = condi & " GROUP BY NU_AUTO_TERC,TX_DESC_TERC,NU_AUTO_CONT_TRAN,NU_AUTO_NEGO_CONT,NU_AUTO_EMPR_REEM,NU_AUTO_PLAN_TRAN"
   Result = LoadData(Desde, Campos, condi, ArrTemp())
   If Result <> FAIL And Encontro Then
      If ArrTemp(2) > 0 Then
         Msf_Copago.AddItem ArrTemp(0) & vbTab & ArrTemp(1) & vbTab & ArrTemp(2) & vbTab & vbTab & vbTab & ArrTemp(3) & vbTab & ArrTemp(4) & vbTab & ArrTemp(5) & vbTab & ArrTemp(6)
      End If
   End If
   If Msf_Copago.Rows > 1 Then
      Campos = "TX_GARA_CRCO_CRED"
      Desde = "CREDITO"
      condi = "NU_NUME_TRAN_CRED=" & NumTran
      ReDim ArrTemp(0)
      Result = LoadData(Desde, Campos, condi, ArrTemp())
      If Result <> FAIL And Encontro Then
         Msf_Copago.TextMatrix(1, 3) = ArrTemp(0)
         If ArrTemp(0) = "S" Then
            BandGarantia = True
            BanGaran = True
            Msf_Copago.TextMatrix(1, 4) = "Con Garantia"
         ElseIf ArrTemp(0) = "N" Then
            BandGarantia = False
            BanGaran = False
            Msf_Copago.TextMatrix(1, 4) = "Sin Garantia"
         End If
      End If
   End If
End If
'Ordenes de pago
ReDim ArrTemp(3)
PorcRetePeJu = CargarParam("83")
PorcRetePeNa = CargarParam("84")
Msf_OrdenPago.Rows = 1
If TipoReemb = "NO" Then
   Msf_OrdenPago.TextMatrix(0, 6) = "G. Administr"
   If TipoVal = "A" Then
      Campos = "NU_AUTO_TERC,TX_DESC_TERC,SUM(NU_AVLAPAG_REDE),SUM(NU_AVLXCOB_REDE),NU_PRES_TERC,NU_PROF_TERC,NU_PRNA_TERC"
   ElseIf TipoVal = "L" Then
      Campos = "NU_AUTO_TERC,TX_DESC_TERC,SUM(NU_VLAPAG_REDE),SUM(NU_VLXCOB_REDE),NU_PRES_TERC,NU_PROF_TERC,NU_PRNA_TERC"
   End If
   Desde = "REEMBOLSO,REEMBOLSO_PRESTACION,REEMBOLSO_DETALLE,TERCERO"
   condi = "NU_AUTO_REEM=NU_AUTO_REEM_REPR AND NU_AUTO_REPR=NU_AUTO_REPR_REDE"
   condi = condi & " AND NU_AUTO_TERC_REEM=NU_AUTO_TERC"
   condi = condi & " AND NU_NUME_TRAN_REEM=" & NumTran
   condi = condi & " GROUP BY NU_AUTO_TERC,TX_DESC_TERC,NU_PRES_TERC,NU_PROF_TERC,NU_PRNA_TERC"
ElseIf TipoReemb = "CR" Then
   Msf_OrdenPago.TextMatrix(0, 6) = "Retenci�n"
   If TipoVal = "A" Then
      Campos = "NU_AUTO_TERC,TX_DESC_TERC,SUM(NU_AVLAPAG_REDE),SUM(NU_AVLXCOB_REDE),NU_PRES_TERC,NU_PROF_TERC,NU_PRNA_TERC"
   ElseIf TipoVal = "L" Then
      Campos = "NU_AUTO_TERC,TX_DESC_TERC,SUM(NU_VLAPAG_REDE),SUM(NU_VLXCOB_REDE),NU_PRES_TERC,NU_PROF_TERC,NU_PRNA_TERC"
   End If
   Desde = "REEMBOLSO,REEMBOLSO_PRESTACION,REEMBOLSO_DETALLE,TERCERO,REEMBOLSO_FACTURA"
   condi = "NU_AUTO_REEM=NU_AUTO_REEM_REPR AND NU_AUTO_REPR=NU_AUTO_REPR_REDE"
   condi = condi & " AND NU_AUTO_REFA_REDE=NU_AUTO_REFA"
   condi = condi & " AND NU_AUTO_TERC_REFA=NU_AUTO_TERC"
   condi = condi & " AND NU_NUME_TRAN_REEM=" & NumTran
   condi = condi & " GROUP BY NU_AUTO_TERC,TX_DESC_TERC,NU_PRES_TERC,NU_PROF_TERC,NU_PRNA_TERC"
End If
ReDim ArrMulTemp(6, 0)
Result = LoadMulData(Desde, Campos, condi, ArrMulTemp())
If Result <> FAIL And Encontro Then
   For i = 0 To UBound(ArrMulTemp, 2)
      VlBase = CDbl(ArrMulTemp(2, i))
      If TipoReemb = "CR" Then 'Retenci�n
         If BandGarantia Then
            VlCopago = ArrMulTemp(3, i)
         Else
            VlCopago = 0
         End If
         VlBaseAux = RoundConDec(VlBase - VlCopago)
         TipoDesc = "P"
         If ArrMulTemp(4, i) = 1 Then ' si es prestador juridico
            Descuento = PorcRetePeJu
            VlDesc = RoundConDec(VlBaseAux * (PorcRetePeJu / 100))
         ElseIf ArrMulTemp(5, i) = 1 Or ArrMulTemp(6, i) = 1 Then 'si es m�dico o prestador natural
            VlDesc = RoundConDec(VlBaseAux * (PorcRetePeNa / 100))
            Descuento = PorcRetePeNa
         End If
         VlXPagar = RoundConDec(VlBase - VlCopago - VlDesc)
      ElseIf TipoReemb = "NO" Then 'Gastos Administrativos
         VlCopago = 0
         TipoDesc = "V"
         If BandGastAdm Then
            VlDesc = CargarParametro("73")
         Else
            VlDesc = 0
         End If
         VlXPagar = VlBase - VlCopago
         Result = FAIL
         If VlXPagar >= VlDesc And Result = FAIL Then VlXPagar = VlXPagar - VlDesc: Result = SUCCEED
         If VlXPagar < VlDesc And Result = FAIL Then VlDesc = VlXPagar: VlXPagar = VlXPagar - VlDesc: Result = SUCCEED
      End If
'      If BandGarantia Then
'         VlCopago = ArrMulTemp(3, i)
'      Else
'         VlCopago = 0
'      End If
'      VlBase = RoundConDec(CDbl(ArrMulTemp(2, i)) - VlCopago)
'      If TipoReemb = "CR" Then 'Retenci�n
'         If ArrMulTemp(4, i) = 1 Then ' si es prestador juridico
'            VlDesc = RoundConDec(VlBase * (PorcRetePeJu / 100))
'         ElseIf ArrMulTemp(5, i) = 1 Or ArrMulTemp(6, i) = 1 Then 'si es m�dico o prestador natural
'            VlDesc = RoundConDec(VlBase * (PorcRetePeNa / 100))
'         End If
'         VlXPagar = RoundConDec(VlBase - VlDesc)
'      ElseIf TipoReemb = "NO" Then 'Gastos Administrativos
'         If BandGastAdm Then
'            VlDesc = CargarParametro("73")
'         Else
'            VlDesc = 0
'         End If
'         VlXPagar = VlBase - VlDesc
'      End If
      Valores = ArrMulTemp(0, i) & vbTab & ArrMulTemp(1, i) & vbTab & VlBase & vbTab & VlCopago & vbTab & TipoDesc & vbTab
      Valores = Valores & Descuento & vbTab & VlDesc & vbTab & VlXPagar
      Msf_OrdenPago.AddItem Valores
      Msf_OrdenPago.Row = Msf_OrdenPago.Rows - 1
      Msf_OrdenPago.Col = 3
      Msf_OrdenPago.CellForeColor = vbRed
      Msf_OrdenPago.Col = 6
      Msf_OrdenPago.CellForeColor = vbRed
   Next
End If
'buscar los copagos que se van a aplicar
For i = 1 To Msf_CopagoPend.Rows - 1
   Msf_CopagoPend.Row = i
   Msf_CopagoPend.Col = 6
   Msf_CopagoPend.CellForeColor = vbRed
   For j = 1 To Msf_OrdenPago.Rows - 1
      SaldoOrPa = RoundConDec(CDbl(Msf_OrdenPago.TextMatrix(j, 2)) - CDbl(Msf_OrdenPago.TextMatrix(j, 3)))
      If SaldoOrPa > 0 Then
         SaldoCopa = RoundConDec(CDbl(Msf_CopagoPend.TextMatrix(i, 5)) - CDbl(Msf_CopagoPend.TextMatrix(i, 6)))
         Msf_CopagoPend.TextMatrix(i, 0) = "S"
         Msf_CopagoPend.Col = 1
         Set Msf_CopagoPend.CellPicture = Img.ListImages(1).Picture
         If SaldoCopa <= SaldoOrPa Then
            Msf_OrdenPago.TextMatrix(j, 3) = CDbl(Msf_OrdenPago.TextMatrix(j, 3)) + SaldoCopa
            Msf_CopagoPend.TextMatrix(i, 6) = SaldoCopa
            SaldoCopa = 0
         Else
            Msf_OrdenPago.TextMatrix(j, 3) = CDbl(Msf_OrdenPago.TextMatrix(j, 3)) + SaldoOrPa
            Msf_CopagoPend.TextMatrix(i, 6) = SaldoOrPa
         End If
         If Msf_OrdenPago.TextMatrix(j, 4) = "P" And Msf_OrdenPago.TextMatrix(j, 5) <> 0 Then
            Msf_OrdenPago.TextMatrix(j, 6) = RoundConDec(CDbl(Msf_OrdenPago.TextMatrix(j, 2)) - CDbl(Msf_OrdenPago.TextMatrix(j, 3)))
            Msf_OrdenPago.TextMatrix(j, 6) = RoundConDec(CDbl(Msf_OrdenPago.TextMatrix(j, 6)) * (CDbl(Msf_OrdenPago.TextMatrix(j, 6)) / 100))
         End If
         Msf_OrdenPago.TextMatrix(j, 7) = RoundConDec(CDbl(Msf_OrdenPago.TextMatrix(j, 2)) - CDbl(Msf_OrdenPago.TextMatrix(j, 3)) - CDbl(Msf_OrdenPago.TextMatrix(j, 6)))
         If SaldoCopa = 0 Then Exit For
      End If
   Next
   Msf_CopagoPend.TextMatrix(i, 7) = RoundConDec(CDbl(Msf_CopagoPend.TextMatrix(i, 5)) - CDbl(Msf_CopagoPend.TextMatrix(i, 6)))
Next
End Sub


Sub Fecha_Laborable(ByVal FechIni As Date, ByRef FechFin As Date, ByVal dia As Integer)
Dim Band    As Integer

FechFin = DateAdd("d", dia, FechIni)
ReDim arr(0)
Band = 0
Campos = "COUNT(FE_FECH_CALE)"
condi = "NU_TIPO_CALE=0"
condi = condi & " AND FE_FECH_CALE >" & FFechaCon(FechIni)
condi = condi & " AND FE_FECH_CALE <=" & FFechaCon(FechFin)
Result = LoadData("CALENDARIO", Campos, condi, arr())
If Result <> FAIL Then
   dia = dia + CLng(arr(0))
   FechFin = DateAdd("d", dia, FechIni)
   While Band = 0
      Campos = "FE_FECH_CALE"
      condi = "NU_TIPO_CALE=0 AND FE_FECH_CALE =" & FFechaCon(FechFin)
      Result = LoadData("CALENDARIO", Campos, condi, arr())
      If Result <> FAIL And Encontro Then
         FechFin = DateAdd("d", 1, FechFin)
         Band = 0
      Else
         Band = 1
      End If
   Wend
End If

End Sub
