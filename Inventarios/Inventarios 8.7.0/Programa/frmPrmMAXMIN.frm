VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Object = "{C932BA88-4374-101B-A56C-00AA003668DC}#1.1#0"; "MSMASK32.OCX"
Begin VB.Form frmPrmMAXMIN 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "M�ximos, Minimos, Reposici�n  X Bodega"
   ClientHeight    =   7095
   ClientLeft      =   45
   ClientTop       =   435
   ClientWidth     =   11475
   Icon            =   "frmPrmMAXMIN.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   7095
   ScaleWidth      =   11475
   Begin VB.Frame frmCONSUMO 
      Caption         =   "Condiciones para C�lculo de M�ximos y M�nimos"
      Height          =   1575
      Left            =   480
      TabIndex        =   14
      Top             =   3360
      Visible         =   0   'False
      Width           =   10575
      Begin MSComCtl2.DTPicker dtpDESDE 
         Height          =   375
         Left            =   2280
         TabIndex        =   17
         Top             =   360
         Width           =   1695
         _ExtentX        =   2990
         _ExtentY        =   661
         _Version        =   393216
         Format          =   100270081
         CurrentDate     =   37987
      End
      Begin VB.CommandButton cmdCALCULAR 
         Caption         =   "&CALCULAR"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   6
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   495
         Left            =   9240
         Picture         =   "frmPrmMAXMIN.frx":058A
         Style           =   1  'Graphical
         TabIndex        =   21
         Top             =   600
         Width           =   975
      End
      Begin MSMask.MaskEdBox txtFRACCION 
         Height          =   375
         Left            =   2280
         TabIndex        =   20
         Top             =   960
         Width           =   735
         _ExtentX        =   1296
         _ExtentY        =   661
         _Version        =   393216
         AllowPrompt     =   -1  'True
         MaxLength       =   3
         PromptChar      =   "0"
      End
      Begin MSComCtl2.DTPicker dtpHASTA 
         Height          =   375
         Left            =   6360
         TabIndex        =   18
         Top             =   360
         Width           =   1695
         _ExtentX        =   2990
         _ExtentY        =   661
         _Version        =   393216
         Format          =   100270081
         CurrentDate     =   37987
      End
      Begin VB.Label Label2 
         Alignment       =   1  'Right Justify
         Caption         =   "Fracci�n de Tiempo:"
         Height          =   375
         Left            =   240
         TabIndex        =   19
         Top             =   960
         Width           =   1995
      End
      Begin VB.Label Label4 
         Alignment       =   1  'Right Justify
         Caption         =   "Consumo Desde:"
         Height          =   375
         Left            =   840
         TabIndex        =   16
         Top             =   360
         Width           =   1395
      End
      Begin VB.Label Label3 
         Alignment       =   1  'Right Justify
         Caption         =   "Consumo Hasta:"
         Height          =   375
         Left            =   4920
         TabIndex        =   15
         Top             =   360
         Width           =   1395
      End
   End
   Begin MSComctlLib.ImageList imgBotones 
      Left            =   10800
      Top             =   4200
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   20
      ImageHeight     =   14
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   10
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmPrmMAXMIN.frx":08FC
            Key             =   ""
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmPrmMAXMIN.frx":0FC6
            Key             =   "ASG"
         EndProperty
         BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmPrmMAXMIN.frx":2018
            Key             =   "CAN"
         EndProperty
         BeginProperty ListImage4 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmPrmMAXMIN.frx":25AA
            Key             =   "SAV"
         EndProperty
         BeginProperty ListImage5 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmPrmMAXMIN.frx":28FC
            Key             =   "ADD"
         EndProperty
         BeginProperty ListImage6 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmPrmMAXMIN.frx":354E
            Key             =   "DEL"
         EndProperty
         BeginProperty ListImage7 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmPrmMAXMIN.frx":3C18
            Key             =   "CHA"
         EndProperty
         BeginProperty ListImage8 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmPrmMAXMIN.frx":429A
            Key             =   "PRN"
            Object.Tag             =   "PRN"
         EndProperty
         BeginProperty ListImage9 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmPrmMAXMIN.frx":4EEE
            Key             =   "CSM"
         EndProperty
         BeginProperty ListImage10 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmPrmMAXMIN.frx":5B42
            Key             =   "MM"
         EndProperty
      EndProperty
   End
   Begin VB.Frame fraMAXMIN 
      Caption         =   "Valores para el Art�culo:"
      Height          =   1575
      Left            =   480
      TabIndex        =   3
      Top             =   5280
      Visible         =   0   'False
      Width           =   10575
      Begin MSMask.MaskEdBox txtMIN 
         Height          =   375
         Left            =   2280
         TabIndex        =   10
         Top             =   480
         Width           =   1695
         _ExtentX        =   2990
         _ExtentY        =   661
         _Version        =   393216
         PromptInclude   =   0   'False
         MaxLength       =   7
         PromptChar      =   "_"
      End
      Begin VB.CommandButton btnACTUA 
         Caption         =   "&ACTUALIZAR"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   6
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   495
         Left            =   9240
         Picture         =   "frmPrmMAXMIN.frx":6300
         Style           =   1  'Graphical
         TabIndex        =   9
         Top             =   600
         Width           =   975
      End
      Begin MSMask.MaskEdBox txtREPO 
         Height          =   375
         Left            =   2280
         TabIndex        =   12
         Top             =   960
         Width           =   1695
         _ExtentX        =   2990
         _ExtentY        =   661
         _Version        =   393216
         PromptInclude   =   0   'False
         MaxLength       =   7
         PromptChar      =   "_"
      End
      Begin MSMask.MaskEdBox txtMAX 
         Height          =   375
         Left            =   6360
         TabIndex        =   11
         Top             =   480
         Width           =   1695
         _ExtentX        =   2990
         _ExtentY        =   661
         _Version        =   393216
         PromptInclude   =   0   'False
         MaxLength       =   7
         PromptChar      =   "_"
      End
      Begin MSMask.MaskEdBox txtMAXADES 
         Height          =   375
         Left            =   6360
         TabIndex        =   13
         Top             =   960
         Width           =   1695
         _ExtentX        =   2990
         _ExtentY        =   661
         _Version        =   393216
         PromptInclude   =   0   'False
         MaxLength       =   7
         PromptChar      =   "_"
      End
      Begin VB.Label lblMAXADES 
         Alignment       =   1  'Right Justify
         Caption         =   "MAX. C/dad a despachar:"
         Height          =   375
         Left            =   4320
         TabIndex        =   7
         Top             =   960
         Width           =   1995
      End
      Begin VB.Label lblREPO 
         Alignment       =   1  'Right Justify
         Caption         =   "C/dad Reposici�n:"
         Height          =   375
         Left            =   840
         TabIndex        =   6
         Top             =   960
         Width           =   1395
      End
      Begin VB.Label lblMAX 
         Alignment       =   1  'Right Justify
         Caption         =   "Stock M�ximo:"
         Height          =   375
         Left            =   4920
         TabIndex        =   5
         Top             =   480
         Width           =   1395
      End
      Begin VB.Label lblMIN 
         Alignment       =   1  'Right Justify
         Caption         =   "Stock M�nimo:"
         Height          =   375
         Left            =   840
         TabIndex        =   4
         Top             =   480
         Width           =   1395
      End
   End
   Begin MSComctlLib.ListView lstSeleccion 
      Height          =   3735
      Left            =   7320
      TabIndex        =   1
      Top             =   1080
      Visible         =   0   'False
      Width           =   3495
      _ExtentX        =   6165
      _ExtentY        =   6588
      View            =   3
      LabelEdit       =   1
      MultiSelect     =   -1  'True
      LabelWrap       =   -1  'True
      HideSelection   =   0   'False
      FullRowSelect   =   -1  'True
      GridLines       =   -1  'True
      _Version        =   393217
      ForeColor       =   -2147483640
      BackColor       =   -2147483643
      BorderStyle     =   1
      Appearance      =   1
      NumItems        =   0
   End
   Begin MSComctlLib.ListView lstArticulos 
      Height          =   5535
      Left            =   480
      TabIndex        =   2
      Top             =   1080
      Width           =   6420
      _ExtentX        =   11324
      _ExtentY        =   9763
      View            =   3
      LabelEdit       =   1
      MultiSelect     =   -1  'True
      LabelWrap       =   -1  'True
      HideSelection   =   0   'False
      AllowReorder    =   -1  'True
      FullRowSelect   =   -1  'True
      GridLines       =   -1  'True
      _Version        =   393217
      ForeColor       =   -2147483640
      BackColor       =   -2147483643
      BorderStyle     =   1
      Appearance      =   1
      NumItems        =   2
      BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Text            =   "Nombre del Art�culo"
         Object.Width           =   7761
      EndProperty
      BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   1
         Text            =   "C�digo"
         Object.Width           =   3881
      EndProperty
   End
   Begin MSComctlLib.Toolbar tlbACCIONES 
      Align           =   1  'Align Top
      Height          =   630
      Left            =   0
      TabIndex        =   8
      Top             =   0
      Width           =   11475
      _ExtentX        =   20241
      _ExtentY        =   1111
      ButtonWidth     =   1535
      ButtonHeight    =   953
      AllowCustomize  =   0   'False
      Appearance      =   1
      _Version        =   393216
      BorderStyle     =   1
   End
   Begin VB.Label lblInforma 
      Caption         =   "Informaci�n"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   320
      Left            =   480
      TabIndex        =   0
      Top             =   740
      Width           =   10335
   End
End
Attribute VB_Name = "frmPrmMAXMIN"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Dim ArrUXB() As Variant
Dim Articulo As UnArticulo
Dim LaBodega As Long, LaAccion As String
Public OpcCod As String   'Opci�n de seguridad
'NU_AUTO_ARTI, CD_CODI_ARTI, NO_NOMB_ARTI, NU_AUTO_UNVE_ARTI, TX_VENCE_ARTI, TX_PARA_ARTI,
'TX_ENTRA_ARTI, CD_GRUP_ARTI, CD_USOS_ARTI, VL_ULCO_ARTI, VL_COPR_ARTI, CT_EXIS_ARTI, DE_OBSE_ARTI, ID_TIPO_ARTIID_TIPO_ARTI, CD_RIPS_ARTI, NU_INDPYP_ARTI

Private Sub btnACTUA_Click()
    Dim Uno As MSComctlLib.ListItem
    For Each Uno In Me.lstSeleccion.ListItems
        If Uno.Selected Then
            Uno.ListSubItems("MAXI").Text = IIf(Len(Me.txtMAX.Text) > 0, CLng(Val(Me.txtMAX.Text)), 0)      'REOL M2070
            Uno.ListSubItems("MINI").Text = IIf(Len(Me.txtMIN.Text) > 0, CLng(Val(Me.txtMIN.Text)), 0)        'REOL M2070
            Uno.ListSubItems("REPO").Text = IIf(Len(Me.txtREPO.Text) > 0, CLng(Val(Me.txtREPO.Text)), 0)    'REOL M2070
            Uno.ListSubItems("DESP").Text = IIf(Len(Me.txtMAXADES.Text) > 0, CLng(Val(Me.txtMAXADES.Text)), 0)  'REOL M2070
        End If
    Next
End Sub

Private Sub cmdCALCULAR_Click()
    Dim ArrCSM()
    Dim Uno As MSComctlLib.ListItem
    Dim CntMinima As Single, CntReorden As Single
    Dim CntMaxima As Single, CntMeses As Single, CntPromedio As Single
    Dim LosAutoArticulo As String
    Dim EnExistencia As Long
    For Each Uno In Me.lstSeleccion.ListItems
        LosAutoArticulo = LosAutoArticulo & IIf(Len(LosAutoArticulo) = 0, "(", ",") & Trim(Uno.Tag)
    Next
    LosAutoArticulo = LosAutoArticulo & IIf(Len(LosAutoArticulo) = 0, "", ")")
    If Me.dtpHASTA.Value > Me.dtpDESDE.Value Then
        CntMeses = CSng((Me.dtpHASTA.Value - Me.dtpDESDE.Value) * 12 / 365)
    End If
    If Len(LosAutoArticulo) > 0 And CntMeses > 0 Then
        For Each Uno In Me.lstSeleccion.ListItems
            Uno.ListSubItems("MAXI").Text = "0"
            Uno.ListSubItems("MINI").Text = "0"
            Uno.ListSubItems("REPO").Text = "0"
'            Uno.ListSubItems("DESP").Text = "0"
        Next
        Condi = ""
        Condi = Condi & "NU_AUTO_ORGCABE_KARD=NU_AUTO_ENCA"
        Condi = Condi & " AND NU_AUTO_DOCU_ENCA=NU_AUTO_DOCU"
        Condi = Condi & " AND NU_AUTO_BODE_KARD=" & LaBodega
        Condi = Condi & " AND NU_AUTO_DOCU IN " & "(" & _
            BajaConsumo & "," & DevolucionBaja & "," & AnulaBaja & "," & _
            Salida & "," & DevolucionSalida & "," & AnulaSalida & "," & _
            FacturaVenta & "," & DevolucionFacturaVenta & "," & AnulaFacturaVenta & ")"
        Condi = Condi & " AND NU_AUTO_ARTI_KARD IN " & LosAutoArticulo
        Condi = Condi & " AND FE_FECH_KARD >= " & FFechaCon(Me.dtpDESDE.Value)
        Condi = Condi & " AND FE_FECH_KARD <= " & FFechaCon(Me.dtpHASTA.Value)
        Condi = Condi & " GROUP BY NU_AUTO_ARTI_KARD"
        Condi = Condi & " HAVING SUM((NU_SALIDA_KARD-NU_ENTRAD_KARD)*NU_MULT_KARD/NU_DIVI_KARD)>0"
        Desde = "IN_KARDEX, IN_ENCABEZADO, IN_DOCUMENTO"
'    NoDefinido = 0
'    Cotizacion = 1
'    AnulaCotizacion = 20
'    ODCompra = 2
'    AnulaODCompra = 18
'    Entrada = 3
'    DevolucionEntrada = 4
'    AnulaEntrada = 13
'    Requisicion = 5
'    AnulaRequisicion = 21
'    Despacho = 6
'    DevolucionDespacho = 17
'    AnulaDespacho = 16
'    BajaConsumo = 8
'    DevolucionBaja = 9
'    AnulaBaja = 14
'    Traslado = 10
'    AnulaTraslado = 23
'    Salida = 19
'    DevolucionSalida = 24
'    AnulaSalida = 22
'    FacturaVenta = 11
'    DevolucionFacturaVenta = 12
'    AnulaFacturaVenta = 15
'    CompraDElementos = 25
'    AprovechaDonacion = 26
        Campos = "NU_AUTO_ARTI_KARD, SUM((NU_SALIDA_KARD-NU_ENTRAD_KARD)*NU_MULT_KARD/NU_DIVI_KARD)"
        Debug.Print "SELECT " & Campos & " FROM " & Desde & " WHERE " & Condi
        ReDim ArrCSM(1, 0)
        Result = LoadMulData(Desde, Campos, Condi, ArrCSM())
        If Result = FAIL Then Exit Sub
        If Not Encontro Then Exit Sub
        For EnExistencia = 0 To UBound(ArrCSM, 2)
            Set Uno = Me.lstSeleccion.ListItems("A" & ArrCSM(0, EnExistencia))
            If CSng(ArrCSM(1, EnExistencia)) > 0 Then
                CntPromedio = CSng(ArrCSM(1, EnExistencia)) / CntMeses
                CntMinima = CSng(CntPromedio * CSng(Me.txtFRACCION.Text))
                Uno.ListSubItems("MINI").Text = CLng(CntMinima)
                CntReorden = (CntMinima + CntPromedio) * CSng(Me.txtFRACCION.Text)
                Uno.ListSubItems("REPO").Text = CLng(CntReorden)
                CntMaxima = CntMinima + CntReorden + CntPromedio
                Uno.ListSubItems("MAXI").Text = CLng(CntMaxima)
            End If
        Next
    End If
End Sub

Private Sub Form_Load()
'    Dim CnTdr As Integer, Punto As MSComctlLib.Node
    Dim CnTdr As Integer        'DEPURACION DE CODIGO
'    Call Main
    Call CenterForm(MDI_Inventarios, Me)
    Me.lblInforma.Caption = ""
    Me.lblInforma.Visible = False
    Me.txtMAX.Mask = String(Me.txtMAX.MaxLength, "9")
    Me.txtMIN.Mask = String(Me.txtMIN.MaxLength, "9")
    Me.txtREPO.Mask = String(Me.txtREPO.MaxLength, "9")
    Me.txtMAXADES.Mask = String(Me.txtMAXADES.MaxLength, "9")
    Me.txtFRACCION.Mask = "#.##"
    LaBodega = Date
    Me.dtpHASTA.Value = DateAdd("d", -Day(LaBodega), LaBodega)
    Me.dtpDESDE.Value = DateAdd("m", -6, Me.dtpHASTA.Value)
    Me.dtpDESDE.Value = DateAdd("d", 1, Me.dtpDESDE.Value)

'    Me.txtMAX.Text = Left("0" & String(Me.txtMAX.MaxLength, " "), Me.txtMAX.MaxLength)
'    Me.txtMIN.Text = Left("0" & String(Me.txtMIN.MaxLength, " "), Me.txtMIN.MaxLength)
'    Me.txtREPO.Text = Left("0" & String(Me.txtREPO.MaxLength, " "), Me.txtREPO.MaxLength)
'    Me.txtMAXADES.Text = Left("0" & String(Me.txtMAXADES.MaxLength, " "), Me.txtMAXADES.MaxLength)
    
    Me.txtMAX.Text = "0"
    Me.txtMIN.Text = "0"
    Me.txtREPO.Text = "0"
    Me.txtMAXADES.Text = "0"
    
    Me.tlbACCIONES.ImageList = Me.imgBotones
    Me.tlbACCIONES.Buttons.Clear
    Me.tlbACCIONES.Buttons.Add , "ASIGNAR", "&Asignar"
    Me.tlbACCIONES.Buttons("ASIGNAR").Image = "ASG"
    Me.tlbACCIONES.Buttons("ASIGNAR").Style = tbrDropdown
    Me.tlbACCIONES.Buttons.Add , "CAMBIAR", "&Cambiar"
    Me.tlbACCIONES.Buttons("CAMBIAR").Style = tbrDropdown
    Me.tlbACCIONES.Buttons("CAMBIAR").Image = "CHA"
    Me.tlbACCIONES.Buttons.Add , "GUARDAR", "&Guardar"
    Me.tlbACCIONES.Buttons("GUARDAR").Image = "SAV"
    Me.tlbACCIONES.Buttons("GUARDAR").Style = tbrDefault
    Me.tlbACCIONES.Buttons.Add , "IMPRIMIR", "&Imprimir"
    Me.tlbACCIONES.Buttons("IMPRIMIR").Image = "PRN"
    Me.tlbACCIONES.Buttons("IMPRIMIR").Style = tbrDefault
    Me.tlbACCIONES.Buttons.Add , "CANCELAR", "&Cancelar"
    Me.tlbACCIONES.Buttons("CANCELAR").Image = "CAN"
    Me.tlbACCIONES.Buttons("CANCELAR").Style = tbrDefault
    Me.tlbACCIONES.Buttons.Add , "CONSUMO", "C&onsumo"
    Me.tlbACCIONES.Buttons("CONSUMO").Image = "CSM" 'APGR T3713
    Me.tlbACCIONES.Buttons("CONSUMO").Style = tbrDefault
    Me.tlbACCIONES.Buttons("GUARDAR").Enabled = False
    Me.tlbACCIONES.Buttons("CANCELAR").Enabled = False
    Me.tlbACCIONES.Buttons("CONSUMO").Enabled = False

'NU_AUTO_BODE, NU_AUTO_BODE_BODE, TX_CODI_BODE, TX_NOMB_BODE, CD_CODI_CECO_BODE, TX_VENTA_BODE
'FROM IN_BODEGA;
    Campos = "NU_AUTO_BODE, NU_AUTO_BODE_BODE, TX_CODI_BODE, TX_NOMB_BODE, " & _
        "CD_CODI_CECO_BODE, TX_VENTA_BODE"
    Desde = "IN_BODEGA ORDER BY TX_NOMB_BODE"
    Condi = ""
    ReDim ArrUXB(5, 0)
    Result = LoadMulData(Desde, Campos, Condi, ArrUXB())
    If Result = FAIL Then GoTo FALLO
    For CnTdr = 0 To UBound(ArrUXB, 2)
        Me.tlbACCIONES.Buttons("ASIGNAR").ButtonMenus.Add , "B" & ArrUXB(0, CnTdr), ArrUXB(3, CnTdr)
        Me.tlbACCIONES.Buttons("ASIGNAR").ButtonMenus("B" & ArrUXB(0, CnTdr)).Tag = CStr(ArrUXB(0, CnTdr))
        Me.tlbACCIONES.Buttons("CAMBIAR").ButtonMenus.Add , "B" & ArrUXB(0, CnTdr), ArrUXB(3, CnTdr)
        Me.tlbACCIONES.Buttons("CAMBIAR").ButtonMenus("B" & ArrUXB(0, CnTdr)).Tag = CStr(ArrUXB(0, CnTdr))
    Next
    
    Campos = "NU_AUTO_ARTI, CD_CODI_ARTI, NO_NOMB_ARTI, NU_AUTO_UNVE_ARTI, TX_PARA_ARTI, " & _
        "DE_DESC_GRUP, DE_DESC_USOS, TX_NOMB_UNVE, CD_GRUP_ARTI, CD_USOS_ARTI, " & _
        "NU_MULT_UNVE, NU_DIVI_UNVE"
    
'NU_AUTO_ARTI, CD_CODI_ARTI, NO_NOMB_ARTI, NU_AUTO_UNVE_ARTI, TX_PARA_ARTI, DE_DESC_GRUP, DE_DESC_USOS, TX_NOMB_UNVE
'FROM ARTICULO, USOS, GRUP_ARTICULO, IN_UNDVENTA

    Desde = "ARTICULO, USOS, GRUP_ARTICULO, IN_UNDVENTA"
    Condi = "CD_CODI_GRUP = CD_GRUP_ARTI AND CD_CODI_USOS = CD_USOS_ARTI AND " & _
        "NU_AUTO_UNVE = NU_AUTO_UNVE_ARTI ORDER BY NO_NOMB_ARTI"
    ReDim ArrUXB(11, 0)
    Result = LoadMulData(Desde, Campos, Condi, ArrUXB())
    If Result = FAIL Then GoTo FALLO
    Me.tlbACCIONES.Buttons("ASIGNAR").Enabled = True
    Me.tlbACCIONES.Buttons("CAMBIAR").Enabled = True
    If Not Encontro Then GoTo NOENC
    Me.lstArticulos.ListItems.Clear
    Me.lstArticulos.Width = 10500
    Me.lstArticulos.ColumnHeaders.Clear
'lstArticulos, NOAR, COAR, NOCO, NOUS, NOGR, AUCO, COGR, COUS
    Me.lstArticulos.ColumnHeaders.Add , "NOAR", "Nombre del art�culo", 0.4 * Me.lstArticulos.Width
    Me.lstArticulos.ColumnHeaders.Add , "COAR", "C�digo", 0.15 * Me.lstArticulos.Width
    Me.lstArticulos.ColumnHeaders.Add , "NOCO", "Unidad de conteo", 0.15 * Me.lstArticulos.Width
    Me.lstArticulos.ColumnHeaders.Add , "NOUS", "Uso", 0.15 * Me.lstArticulos.Width
    Me.lstArticulos.ColumnHeaders.Add , "NOGR", "Grupo", 0.15 * Me.lstArticulos.Width
    Me.lstArticulos.ColumnHeaders.Add , "AUCO", "AutoUNC", 0
    Me.lstArticulos.ColumnHeaders.Add , "COGR", "CodGRU", 0
    Me.lstArticulos.ColumnHeaders.Add , "COUS", "CodUSO", 0
    Me.lstArticulos.MultiSelect = True
    Dim Articulo As New UnArticulo
    For CnTdr = 0 To UBound(ArrUXB, 2)
        Articulo.AutoNumero = ArrUXB(0, CnTdr)
        Articulo.Codigo = ArrUXB(1, CnTdr)
        Articulo.Nombre = ArrUXB(2, CnTdr)
        Articulo.AutoUDConteo = ArrUXB(3, CnTdr)
        Articulo.NombreGrupo = ArrUXB(5, CnTdr)
        Articulo.NombreUso = ArrUXB(6, CnTdr)
        Articulo.NombreUndConteo = ArrUXB(7, CnTdr)
        Articulo.ParaQueEs = InStr(1, "VC", ArrUXB(4, CnTdr)) - 1
        Articulo.CodigoGrupo = ArrUXB(8, CnTdr)
        Articulo.CodigoUso = ArrUXB(9, CnTdr)
        Me.lstArticulos.ListItems.Add , "A" & Articulo.AutoNumero, Trim(Articulo.Nombre)
        Me.lstArticulos.ListItems("A" & Articulo.AutoNumero).Tag = CStr(Articulo.AutoNumero)
'lstArticulos, NOAR, COAR, NOCO, NOUS, NOGR, AUCO, COGR, COUS, MUCO, DICO
        Me.lstArticulos.ListItems("A" & Articulo.AutoNumero).ListSubItems.Add , "COAR", Trim(Articulo.Codigo)
        Me.lstArticulos.ListItems("A" & Articulo.AutoNumero).ListSubItems.Add , "NOCO", Trim(Articulo.NombreUndConteo)
        Me.lstArticulos.ListItems("A" & Articulo.AutoNumero).ListSubItems.Add , "NOUS", Trim(Articulo.NombreUso)
        Me.lstArticulos.ListItems("A" & Articulo.AutoNumero).ListSubItems.Add , "NOGR", Trim(Articulo.NombreGrupo)
        Me.lstArticulos.ListItems("A" & Articulo.AutoNumero).ListSubItems.Add , "AUCO", Trim(Articulo.AutoUDConteo)
        Me.lstArticulos.ListItems("A" & Articulo.AutoNumero).ListSubItems.Add , "COGR", Trim(Articulo.CodigoGrupo)
        Me.lstArticulos.ListItems("A" & Articulo.AutoNumero).ListSubItems.Add , "COUS", Trim(Articulo.CodigoUso)
    Next
    Set Articulo = Nothing
NOENC:
FALLO:
End Sub

Private Sub lstArticulos_ColumnClick(ByVal ColumnHeader As MSComctlLib.ColumnHeader)
    Me.lstArticulos.SortKey = ColumnHeader.Index - 1
    Me.lstArticulos.Sorted = True
End Sub

Private Sub lstSeleccion_ItemClick(ByVal Item As MSComctlLib.ListItem)
    Me.fraMAXMIN.Caption = Item.Text
'    Me.txtMAX.Text = IIf(Len(Item.ListSubItems("MAXI").Text) > 0, Trim(Me.lstSeleccion.SelectedItem.ListSubItems("MAXI").Text), "0")
'    Me.txtMIN.Text = IIf(Len(Item.ListSubItems("MINI").Text) > 0, Trim(Me.lstSeleccion.SelectedItem.ListSubItems("MINI").Text), "0")
'    Me.txtREPO.Text = IIf(Len(Item.ListSubItems("REPO").Text) > 0, Trim(Me.lstSeleccion.SelectedItem.ListSubItems("REPO").Text), "0")
'    Me.txtMAXADES.Text = IIf(Len(Item.ListSubItems("DESP").Text) > 0, Trim(Me.lstSeleccion.SelectedItem.ListSubItems("DESP").Text), "0")
    Me.txtMAX.Text = IIf(Len(Item.ListSubItems("MAXI").Text) > 0, Trim(Item.ListSubItems("MAXI").Text), "0")
    Me.txtMIN.Text = IIf(Len(Item.ListSubItems("MINI").Text) > 0, Trim(Item.ListSubItems("MINI").Text), "0")
    Me.txtREPO.Text = IIf(Len(Item.ListSubItems("REPO").Text) > 0, Trim(Item.ListSubItems("REPO").Text), "0")
    Me.txtMAXADES.Text = IIf(Len(Item.ListSubItems("DESP").Text) > 0, Trim(Item.ListSubItems("DESP").Text), "0")
End Sub

Private Sub lstSeleccion_KeyDown(KeyCode As Integer, Shift As Integer)
    Select Case KeyCode
    Case vbKeyUp, vbKeyDown
    End Select
End Sub

Private Sub tlbACCIONES_ButtonClick(ByVal Boton As MSComctlLib.Button)
    Dim Uno As MSComctlLib.ListItem
    Me.lblInforma.Visible = False
    Select Case Boton.Key
    Case Is = "IMPRIMIR"
        Call Limpiar_CrysListar
        Call ListarD("MaxYMin.rpt", "", crptToWindow, "M�ximos y M�nimos X Bodega", MDI_Inventarios, True)
    Case Is = "GUARDAR"
        Me.tlbACCIONES.Buttons("ASIGNAR").Enabled = True
        Me.tlbACCIONES.Buttons("CAMBIAR").Enabled = True
        Me.tlbACCIONES.Buttons("IMPRIMIR").Enabled = True
        Me.tlbACCIONES.Buttons("GUARDAR").Enabled = False
        Me.tlbACCIONES.Buttons("CANCELAR").Enabled = False
        Me.tlbACCIONES.Buttons("CONSUMO").Enabled = False
        Select Case LaAccion
        Case Is = "CAMBIAR"
            For Each Uno In Me.lstSeleccion.ListItems
                If Len(Uno.ListSubItems("MAXI").Text) = 0 Then Uno.ListSubItems("MAXI").Text = "0"
                If Len(Uno.ListSubItems("MINI").Text) = 0 Then Uno.ListSubItems("MINI").Text = "0"
                If Len(Uno.ListSubItems("REPO").Text) = 0 Then Uno.ListSubItems("REPO").Text = "0"
                If Len(Uno.ListSubItems("DESP").Text) = 0 Then Uno.ListSubItems("DESP").Text = "0"
                If CLng(Uno.ListSubItems("MAXI").Text) >= 0 And _
                    CLng(Uno.ListSubItems("MINI").Text) >= 0 And _
                    CLng(Uno.ListSubItems("REPO").Text) >= 0 And _
                    CLng(Uno.ListSubItems("DESP").Text) >= 0 Then
'NU_AUTO_BODE_LIBA, NU_AUTO_ARTI_LIBA, NU_STMAX_LIBA, NU_STMIN_LIBA, NU_REPOS_LIBA, NU_MXDES_LIBA
'IN_LIMITES_BODE_ARTI;
                    Valores = ""
                    Valores = Valores & "NU_STMAX_LIBA=" & CLng(Uno.ListSubItems("MAXI").Text)
                    Valores = Valores & ", NU_STMIN_LIBA=" & CLng(Uno.ListSubItems("MINI").Text)
                    Valores = Valores & ", NU_REPOS_LIBA=" & CLng(Uno.ListSubItems("REPO").Text)
                    Valores = Valores & ", NU_MXDES_LIBA=" & CLng(Uno.ListSubItems("DESP").Text)
                    Condi = "NU_AUTO_BODE_LIBA=" & LaBodega & _
                        " AND NU_AUTO_ARTI_LIBA=" & Uno.Tag
                    Result = DoUpdate("IN_LIMITES_BODE_ARTI", Valores, Condi)
                End If
            Next
        Case Is = "ASIGNAR"
            For Each Uno In Me.lstSeleccion.ListItems
                If Len(Uno.ListSubItems("MAXI").Text) = 0 Then Uno.ListSubItems("MAXI").Text = "0"
                If Len(Uno.ListSubItems("MINI").Text) = 0 Then Uno.ListSubItems("MINI").Text = "0"
                If Len(Uno.ListSubItems("REPO").Text) = 0 Then Uno.ListSubItems("REPO").Text = "0"
                If Len(Uno.ListSubItems("DESP").Text) = 0 Then Uno.ListSubItems("DESP").Text = "0"
                If CLng(Uno.ListSubItems("MAXI").Text) >= 0 And _
                    CLng(Uno.ListSubItems("MINI").Text) >= 0 And _
                    CLng(Uno.ListSubItems("REPO").Text) >= 0 And _
                    CLng(Uno.ListSubItems("DESP").Text) >= 0 Then
'NU_AUTO_BODE_LIBA, NU_AUTO_ARTI_LIBA, NU_STMAX_LIBA, NU_STMIN_LIBA, NU_REPOS_LIBA, NU_MXDES_LIBA
'IN_LIMITES_BODE_ARTI;
                    Valores = ""
                    Valores = Valores & "NU_STMAX_LIBA=" & CLng(Uno.ListSubItems("MAXI").Text)
                    Valores = Valores & ", NU_STMIN_LIBA=" & CLng(Uno.ListSubItems("MINI").Text)
                    Valores = Valores & ", NU_REPOS_LIBA=" & CLng(Uno.ListSubItems("REPO").Text)
                    Valores = Valores & ", NU_MXDES_LIBA=" & CLng(Uno.ListSubItems("DESP").Text)
                    Valores = Valores & ", NU_AUTO_BODE_LIBA=" & LaBodega
                    Valores = Valores & ", NU_AUTO_ARTI_LIBA=" & Uno.Tag
                    Result = DoInsertSQL("IN_LIMITES_BODE_ARTI", Valores)
                End If
            Next
        End Select
        Me.lstSeleccion.Visible = False
        Me.fraMAXMIN.Visible = False
        Me.lstArticulos.Visible = True
    Case Is = "CANCELAR"
        Me.tlbACCIONES.Buttons("ASIGNAR").Enabled = True
        Me.tlbACCIONES.Buttons("CAMBIAR").Enabled = True
        Me.tlbACCIONES.Buttons("IMPRIMIR").Enabled = True
        Me.tlbACCIONES.Buttons("GUARDAR").Enabled = False
        Me.tlbACCIONES.Buttons("CANCELAR").Enabled = False
        Me.tlbACCIONES.Buttons("CONSUMO").Enabled = False
        Me.lstSeleccion.Visible = False
        Me.fraMAXMIN.Visible = False
        Me.frmCONSUMO.Visible = False
        Me.lstArticulos.Visible = True
    Case Is = "CONSUMO"
        Me.lblInforma.Visible = True
        If Me.fraMAXMIN.Visible Then
            If Len(Me.txtFRACCION.Text) = 0 Then Me.txtFRACCION.Text = "0.5"
            Me.fraMAXMIN.Visible = False
            Me.frmCONSUMO.Top = Me.fraMAXMIN.Top
            Me.frmCONSUMO.Visible = True
            Me.tlbACCIONES.Buttons("CONSUMO").Image = "MM" 'APGR T3713
            Me.tlbACCIONES.Buttons("CONSUMO").Caption = "Ma&x Min"
        Else
            Me.frmCONSUMO.Visible = False
            Me.fraMAXMIN.Visible = True
            Me.tlbACCIONES.Buttons("CONSUMO").Caption = "C&onsumo"
            Me.tlbACCIONES.Buttons("CONSUMO").Image = "CSM" 'APGR T3713
        End If
    End Select
End Sub

Private Sub tlbACCIONES_ButtonMenuClick(ByVal ButtonMenu As MSComctlLib.ButtonMenu)
    Dim CnTr As Integer
    Dim Uno As MSComctlLib.ListItem
    Dim ClaSelect As String, ClaBorrar As String, AutBuscar As String
    Dim Ardclear() As String 'AASV M6056
    Dim BoEncontro As Boolean 'AASV M6056
    Dim Ini As Integer 'AASV M6056
    LaBodega = CLng(ButtonMenu.Tag)
    LaAccion = ButtonMenu.Parent.Key
    Select Case LaAccion
    Case Is = "ASIGNAR"
        Me.lblInforma.Caption = "Asignando M�ximo, M�nimo, Reposici�n; en la bodega: " & ButtonMenu.Text
        Me.lblInforma.Visible = True
        Me.tlbACCIONES.Buttons("ASIGNAR").Enabled = False
        Me.tlbACCIONES.Buttons("CAMBIAR").Enabled = False
        Me.tlbACCIONES.Buttons("IMPRIMIR").Enabled = False
        Me.tlbACCIONES.Buttons("GUARDAR").Enabled = True
        Me.tlbACCIONES.Buttons("CANCELAR").Enabled = True
        Me.tlbACCIONES.Buttons("CONSUMO").Enabled = True
        Me.lstArticulos.Visible = False
        Me.lstSeleccion.Top = Me.lstArticulos.Top
        Me.lstSeleccion.Left = Me.lstArticulos.Left
        Me.lstSeleccion.Width = Me.lstArticulos.Width
'NU_AUTO_BODE_RBAU, NU_AUTO_ARTI_RBAU, NU_AUTO_UNVE_RBAU
'IN_R_BODE_ARTI_UNDVENTA;
        
'NU_AUTO_BODE_LIBA, NU_AUTO_ARTI_LIBA, NU_STMAX_LIBA, NU_STMIN_LIBA, NU_REPOS_LIBA, NU_MXDES_LIBA
'IN_LIMITES_BODE_ARTI;
        
        Campos = "NU_AUTO_BODE_LIBA, NU_AUTO_ARTI_LIBA, NU_STMAX_LIBA, NU_STMIN_LIBA, NU_REPOS_LIBA, NU_MXDES_LIBA"
        Desde = "IN_LIMITES_BODE_ARTI"
        Condi = "NU_AUTO_BODE_LIBA=" & ButtonMenu.Tag
        ReDim ArrUXB(2, 0)
        Result = LoadMulData(Desde, Campos, Condi, ArrUXB())
        If Result = FAIL Then GoTo FALLO
        For CnTr = 0 To UBound(ArrUXB, 2)
            AutBuscar = AutBuscar & IIf(Len(AutBuscar) = 0, "0", ",0") & ArrUXB(1, CnTr)
        Next
FALLO:
        For Each Uno In Me.lstSeleccion.ListItems
            If Me.lstArticulos.ListItems(Uno.Key).Selected Then
                ClaSelect = ClaSelect & IIf(Len(ClaSelect) = 0, "0", ",0") & Uno.Tag
                Me.lstArticulos.ListItems(Uno.Key).Selected = False
            Else
                ClaBorrar = ClaBorrar & IIf(Len(ClaBorrar) = 0, "0", ",0") & Uno.Tag
            End If
        Next
'lstArticulos, NOAR, COAR, NOCO, NOUS, NOGR, AUCO, COGR, COUS
'lstSeleccion, NOAR, AUCO, NOCO, AUDI, NODI
'lstSeleccion, NOAR, AUCO, NOCO, MINI, MAXI, REPO
        Me.lstSeleccion.ColumnHeaders.Clear
        Me.lstSeleccion.ColumnHeaders.Add , "NOAR", "Nombre", 0.3 * Me.lstSeleccion.Width
        Me.lstSeleccion.ColumnHeaders.Add , "COAR", "C�digo", 0.15 * Me.lstSeleccion.Width
        Me.lstSeleccion.ColumnHeaders.Add , "AUCO", "AutoCON", 0
        Me.lstSeleccion.ColumnHeaders.Add , "NOCO", "Und de Conteo", 0.2 * Me.lstSeleccion.Width
        Me.lstSeleccion.ColumnHeaders.Add , "MINI", "Stk M�nimo", 0.1 * Me.lstSeleccion.Width
        Me.lstSeleccion.ColumnHeaders.Add , "MAXI", "Stk M�ximo", 0.1 * Me.lstSeleccion.Width
        Me.lstSeleccion.ColumnHeaders.Add , "REPO", "C/d Reposi", 0.1 * Me.lstSeleccion.Width
        Me.lstSeleccion.ColumnHeaders.Add , "DESP", "Max a Desp", 0.1 * Me.lstSeleccion.Width
        Me.lstSeleccion.ColumnHeaders.Add , "CSMO", "Cons X Bod", 0.1 * Me.lstSeleccion.Width
        Ardclear = Split(ClaBorrar, ",") 'AASV M6056
        For Each Uno In Me.lstArticulos.ListItems 'lstSeleccion
'AASV M6056 Inicio
'            'If InStr(1, ClaBorrar, "0" & Uno.Tag) Then
'            If ClaBorrar Like "0" & Uno.Tag Then 'natalia
'                Me.lstSeleccion.ListItems.Remove Uno.Key
'            Else
'                If InStr(1, AutBuscar, "0" & Uno.Tag) Then
'                    On Error Resume Next
'                    Me.lstSeleccion.ListItems.Remove Uno.Key
'                    On Error GoTo 0
'                    Uno.Selected = False
'                End If
'            End If
            
            BoEncontro = False
            For Ini = 0 To UBound(Ardclear)
                  If "0" & Uno.Tag = Ardclear(Ini) And Ardclear(Ini) <> "0" Then Me.lstSeleccion.ListItems.Remove Uno.Key: BoEncontro = True: Exit For
            Next
            If BoEncontro = False Then
                For Ini = 0 To UBound(ArrUXB, 2)
                  If Uno.Tag = ArrUXB(1, Ini) Then
                     On Error Resume Next
                     Me.lstSeleccion.ListItems.Remove Uno.Key
                     On Error GoTo 0
                     Uno.Selected = False
                  End If
                Next
            End If
'AASV M6056 Fin
            If Uno.Selected Then
                Me.lstSeleccion.ListItems.Add , "A" & Uno.Tag, Uno.Text
                Me.lstSeleccion.ListItems("A" & Uno.Tag).Tag = Uno.Tag
                Me.lstSeleccion.ListItems("A" & Uno.Tag).ListSubItems.Add , "COAR", Uno.ListSubItems("COAR").Text
                Me.lstSeleccion.ListItems("A" & Uno.Tag).ListSubItems.Add , "AUCO", Uno.ListSubItems("AUCO").Text
                Me.lstSeleccion.ListItems("A" & Uno.Tag).ListSubItems.Add , "NOCO", Uno.ListSubItems("NOCO").Text
                Me.lstSeleccion.ListItems("A" & Uno.Tag).ListSubItems.Add , "MINI", ""
                Me.lstSeleccion.ListItems("A" & Uno.Tag).ListSubItems.Add , "MAXI", ""
                Me.lstSeleccion.ListItems("A" & Uno.Tag).ListSubItems.Add , "REPO", ""
                Me.lstSeleccion.ListItems("A" & Uno.Tag).ListSubItems.Add , "DESP", ""
            End If
            If InStr(1, ClaSelect, "0" & Uno.Tag) Then Uno.Selected = True
            If InStr(1, AutBuscar, "0" & Uno.Tag) Then Uno.Selected = True
        Next
        Me.lstSeleccion.Visible = True
        Me.fraMAXMIN.Visible = True
    Case Is = "CAMBIAR"
        Me.lblInforma.Caption = "Cambiando unidades en la bodega: " & ButtonMenu.Text
        Me.lblInforma.Visible = True
        Me.tlbACCIONES.Buttons("ASIGNAR").Enabled = False
        Me.tlbACCIONES.Buttons("CAMBIAR").Enabled = False
        Me.tlbACCIONES.Buttons("IMPRIMIR").Enabled = False
        Me.tlbACCIONES.Buttons("GUARDAR").Enabled = True
        Me.tlbACCIONES.Buttons("CANCELAR").Enabled = True
        Me.tlbACCIONES.Buttons("CONSUMO").Enabled = True
        Me.lstArticulos.Visible = False
        Me.lstSeleccion.Top = Me.lstArticulos.Top
        Me.lstSeleccion.Left = Me.lstArticulos.Left
        Me.lstSeleccion.Width = Me.lstArticulos.Width

'SELECT IN_R_BODE_ARTI_UNDVENTA.NU_AUTO_BODE_RBAU, IN_R_BODE_ARTI_UNDVENTA.NU_AUTO_ARTI_RBAU, IN_R_BODE_ARTI_UNDVENTA.NU_AUTO_UNVE_RBAU, IN_UNDVENTA.TX_CODI_UNVE, IN_UNDVENTA.TX_NOMB_UNVE, IN_UNDVENTA.NU_MULT_UNVE, IN_UNDVENTA.NU_DIVI_UNVE, ARTICULO.CD_CODI_ARTI, ARTICULO.NO_NOMB_ARTI
'FROM ARTICULO IN_UNDVENTA IN_R_BODE_ARTI_UNDVENTA ON IN_UNDVENTA.NU_AUTO_UNVE = IN_R_BODE_ARTI_UNDVENTA.NU_AUTO_UNVE_RBAU) ON ARTICULO.NU_AUTO_ARTI = IN_R_BODE_ARTI_UNDVENTA.NU_AUTO_ARTI_RBAU
'WHERE (((IN_R_BODE_ARTI_UNDVENTA.NU_AUTO_BODE_RBAU)=10));

'NU_AUTO_BODE_LIBA, NU_AUTO_ARTI_LIBA, NU_STMAX_LIBA, NU_STMIN_LIBA, NU_REPOS_LIBA
'IN_LIMITES_BODE_ARTI;

'        Campos = "NU_AUTO_BODE_RBAU, NU_AUTO_ARTI_RBAU, NU_AUTO_UNVE_RBAU, " & _
'            "DISTRI.TX_CODI_UNVE AS DISCODUNVE, DISTRI.TX_NOMB_UNVE AS DISNOMUNVE, " & _
'            "CONTEO.NU_MULT_UNVE AS CONMUL, CONTEO.NU_DIVI_UNVE AS CONDIV, CD_CODI_ARTI, " & _
'            "NO_NOMB_ARTI, NU_AUTO_UNVE_ARTI, CONTEO.TX_CODI_UNVE AS CONCODUNVE, " & _
'            "CONTEO.TX_NOMB_UNVE AS CONNOMUNVE, DISTRI.NU_MULT_UNVE AS DISMUL, " & _
'            "DISTRI.NU_DIVI_UNVE AS DISDIV"
'        Desde = "ARTICULO, IN_UNDVENTA AS CONTEO, IN_R_BODE_ARTI_UNDVENTA, IN_UNDVENTA AS DISTRI"
'        Condi = "(DISTRI.NU_AUTO_UNVE=NU_AUTO_UNVE_RBAU) AND (CONTEO.NU_AUTO_UNVE=NU_AUTO_UNVE_ARTI) AND (NU_AUTO_ARTI=NU_AUTO_ARTI_RBAU) AND NU_AUTO_BODE_RBAU=" & ButtonMenu.Tag
        Campos = "NU_AUTO_BODE_LIBA, NU_AUTO_ARTI_LIBA, NU_STMAX_LIBA, NU_STMIN_LIBA, NU_REPOS_LIBA, NU_MXDES_LIBA, " & _
            "CONTEO.NU_AUTO_UNVE, CONTEO.NU_MULT_UNVE AS CONMUL, CONTEO.NU_DIVI_UNVE AS CONDIV, " & _
            "CD_CODI_ARTI, NO_NOMB_ARTI, NU_AUTO_UNVE_ARTI, CONTEO.TX_CODI_UNVE AS CONCODUNVE, " & _
            "CONTEO.TX_NOMB_UNVE AS CONNOMUNVE"
        Desde = "ARTICULO, IN_UNDVENTA AS CONTEO, IN_LIMITES_BODE_ARTI"
        Condi = "CONTEO.NU_AUTO_UNVE=NU_AUTO_UNVE_ARTI AND NU_AUTO_ARTI=NU_AUTO_ARTI_LIBA AND NU_AUTO_BODE_LIBA=" & ButtonMenu.Tag
        ReDim ArrUXB(13, 0)
        Result = LoadMulData(Desde, Campos, Condi, ArrUXB())
        If Result = FAIL Then GoTo FALLO
'lstArticulos, NOAR, COAR, NOCO, NOUS, NOGR, AUCO, COGR, COUS
'lstSeleccion, NOAR, AUCO, NOCO, AUDI, NODI
'lstSeleccion, NOAR, AUCO, NOCO, MINI, MAXI, REPO
        Me.lstSeleccion.ListItems.Clear
        Me.lstSeleccion.ColumnHeaders.Clear
        Me.lstSeleccion.ColumnHeaders.Add , "NOAR", "Nombre", 0.3 * Me.lstSeleccion.Width
        Me.lstSeleccion.ColumnHeaders.Add , "COAR", "C�digo", 0.15 * Me.lstSeleccion.Width
        Me.lstSeleccion.ColumnHeaders.Add , "AUCO", "AutoCON", 0
        Me.lstSeleccion.ColumnHeaders.Add , "NOCO", "Und de Conteo", 0.2 * Me.lstSeleccion.Width
        Me.lstSeleccion.ColumnHeaders.Add , "MINI", "Stk M�nimo", 0.1 * Me.lstSeleccion.Width
        Me.lstSeleccion.ColumnHeaders.Add , "MAXI", "Stk M�ximo", 0.1 * Me.lstSeleccion.Width
        Me.lstSeleccion.ColumnHeaders.Add , "REPO", "C/d Reposi", 0.1 * Me.lstSeleccion.Width
        Me.lstSeleccion.ColumnHeaders.Add , "DESP", "Max a Desp", 0.1 * Me.lstSeleccion.Width
        Me.lstSeleccion.ColumnHeaders.Add , "CSMO", "Cons X Bod", 0.1 * Me.lstSeleccion.Width
        If Not Encontro Then GoTo NOENC
        For CnTr = 0 To UBound(ArrUXB, 2)
            Me.lstSeleccion.ListItems.Add , "A" & ArrUXB(1, CnTr), ArrUXB(10, CnTr)
            Me.lstSeleccion.ListItems("A" & ArrUXB(1, CnTr)).Tag = ArrUXB(1, CnTr)
'            Me.lstSeleccion.ListItems("A" & Uno.Tag).ListSubItems.Add , "COAR", ArrUXB(9, CnTr)
'            Me.lstSeleccion.ListItems("A" & Uno.Tag).ListSubItems.Add , "AUCO", ArrUXB(5, CnTr)
'            Me.lstSeleccion.ListItems("A" & Uno.Tag).ListSubItems.Add , "NOCO", ArrUXB(13, CnTr)
            Me.lstSeleccion.ListItems("A" & ArrUXB(1, CnTr)).ListSubItems.Add , "COAR", ArrUXB(9, CnTr)
            Me.lstSeleccion.ListItems("A" & ArrUXB(1, CnTr)).ListSubItems.Add , "AUCO", ArrUXB(5, CnTr)
            Me.lstSeleccion.ListItems("A" & ArrUXB(1, CnTr)).ListSubItems.Add , "NOCO", ArrUXB(13, CnTr)
            Me.lstSeleccion.ListItems("A" & ArrUXB(1, CnTr)).ListSubItems.Add , "MINI", ArrUXB(3, CnTr)
            Me.lstSeleccion.ListItems("A" & ArrUXB(1, CnTr)).ListSubItems.Add , "MAXI", ArrUXB(2, CnTr)
            Me.lstSeleccion.ListItems("A" & ArrUXB(1, CnTr)).ListSubItems.Add , "REPO", ArrUXB(4, CnTr)
            Me.lstSeleccion.ListItems("A" & ArrUXB(1, CnTr)).ListSubItems.Add , "DESP", ArrUXB(5, CnTr)
        Next
NOENC:
        Me.lstSeleccion.Visible = True
        Me.fraMAXMIN.Visible = True
    End Select
End Sub

Private Sub Form_KeyPress(KeyAscii As Integer)
    If KeyAscii = vbKeyReturn Then KeyAscii = vbKeyTab
End Sub

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyReturn Then KeyCode = vbKeyTab
End Sub

