VERSION 5.00
Begin VB.Form FrmClave 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Clave Inicializacion de Movimiento"
   ClientHeight    =   2280
   ClientLeft      =   3840
   ClientTop       =   3975
   ClientWidth     =   3240
   Icon            =   "FrmClave.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   1290
   ScaleMode       =   0  'User
   ScaleWidth      =   4155
   Begin VB.Frame Frame1 
      Height          =   2175
      Left            =   120
      TabIndex        =   0
      Top             =   0
      Width           =   3015
      Begin VB.CommandButton Cmdcontinua 
         Caption         =   "&CONTINUAR"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   6
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   495
         Left            =   720
         Picture         =   "FrmClave.frx":058A
         Style           =   1  'Graphical
         TabIndex        =   3
         Top             =   1440
         Width           =   1335
      End
      Begin VB.TextBox TxtClave 
         Alignment       =   2  'Center
         Height          =   315
         IMEMode         =   3  'DISABLE
         Left            =   720
         MaxLength       =   15
         PasswordChar    =   "*"
         TabIndex        =   2
         Top             =   960
         WhatsThisHelpID =   1002
         Width           =   1455
      End
      Begin VB.Label LblOpcion 
         Caption         =   "0"
         Height          =   255
         Left            =   240
         TabIndex        =   4
         Top             =   1680
         Visible         =   0   'False
         Width           =   255
      End
      Begin VB.Label Label1 
         Alignment       =   2  'Center
         Caption         =   "Digite La Clave de Acceso"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   495
         Left            =   360
         TabIndex        =   1
         Top             =   360
         Width           =   2175
      End
   End
End
Attribute VB_Name = "FrmClave"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
    Dim Final As Boolean
    Dim OPC As Integer
'    Dim Iprog As Integer       'DEPURACION DE CODIGO
    Dim Opcion As Byte
    Public OpcCod As String   'Opci�n de seguridad
Private Sub CmdContinua_Click()
'  Dim Msg As String        'DEPURACION DE CODIGO
  Dim InOpcion As Integer 'GMS R2074
  Call Valclave
  If TxtClave = "" Then TxtClave.SetFocus: Exit Sub
  If Result <> FAIL Then
      InOpcion = Val(LblOpcion) 'GMS R2074
      
      'Select Case LblOpcion
      Select Case InOpcion 'GMS R2074
      Case 0:
            Unload Me
            OPC = MsgBox("Este proceso borra todo el movimiento" _
            & " de Inventarios que se ha digitado hasta el momento. " _
            & "Esta seguro de inicializar el movimiento ?", vbInformation + vbYesNo, "ADVERTENCIA")
            If OPC = vbYes Then
                Final = False: 'Iprog = 0: Result = 0
                Call MouseClock
                Call Beep: Call Beep
                FrmIniMov.Show
            Else
                If Final = True Then Call MsgBox("�El Proceso Fue Cancelado!", vbInformation)
            End If
            Msglin NUL$
            Call MouseNorm
      
      Case 1: 'Ajuste de costo promedio. GMS R2074
            Unload Me
            OPC = MsgBox("Este proceso ajusta el costo promedio" _
            & " para el movimiento de los art�culos seleccionados. " _
            & "Esta seguro de realizar este proceso ?", vbInformation + vbYesNo, "ADVERTENCIA")
            If OPC = vbYes Then
                 Final = False: 'Iprog = 0: Result = 0
                 Call MouseClock
                 Call Beep: Call Beep
                 FrmAjusteCostoProm.Show
            Else
                   If Final = True Then Call MsgBox("�El Proceso Fue Cancelado!", vbInformation)
            End If
            Msglin NUL$
            Call MouseNorm
            
       Case 2: 'JJRG T14333
            Unload Me
            frmPARAM.Show
     End Select
  Else
        'JJRG 15641
        'MsgBox "Clave Incorrecta, Consulte al Administrador", vbExclamation + vbOKOnly
        MsgBox "Permiso denegado,consulte al administrador", vbExclamation + vbOKOnly
        Opcion = Opcion + 1
        If Opcion >= 3 Then Unload Me Else TxtClave.SetFocus
        'JJRG 15641
  End If
End Sub

Private Sub Form_Load()
Opcion = 0
'Call CenterForm(MDI_Inventarios, FrmClave) APGR T3719
Call CenterForm(MDI_Inventarios, Me) 'APGR T3719
End Sub

Private Sub TxtClave_GotFocus()
   TxtClave.SelStart = 0
   TxtClave.SelLength = Len(TxtClave)
End Sub
Private Sub TxtClave_KeyPress(KeyAscii As Integer)
   Call Cambiar_Enter(KeyAscii)
End Sub
Private Sub TxtClave_LostFocus()
   TxtClave = Trim(TxtClave)
End Sub

Function Valclave() As Integer
   Dim Clave As String
   ReDim Arr(0)
   Dim Pos As Integer
   Dim cad As String
   'INICIO AFMG T20266- R17890
   Dim ArrPerm(0) As Variant
   If LblOpcion = 2 Then 'Cuando se ingresa a parametros de la aplicacion el label toma valor de 2
      Call Leer_Permiso_ProcEspParam("frmparam", "G")
      If Result <> FAIL Then
         Result = LoadData(NombTabUsr, "TX_PASS_USUA", "TX_IDEN_USUA=" & Comi & UCase(UserId) & Comi, Arr())
         If Result <> FAIL And Encontro Then
            Clave = Arr(0)
            Result = IIf(ConvertCadenaHexa(Encriptar(UCase(Trim(TxtClave)), UCase(Trim(UserId)))) = Clave, SUCCEED, FAIL)
            Valclave = Result
         Else
            Result = FAIL
         End If
      Else
         Result = FAIL
      End If
      If Result = FAIL Then
         Exit Function
      End If
      Exit Function
   End If
   'FIN AFMG T20266- R17890
   If (UCase(UserId) = "ADMINISTRADOR") Then
      Result = LoadData(NombTabUsr, "TX_PASS_USUA", "TX_IDEN_USUA=" & Comi & UCase("ADMINISTRADOR") & Comi, Arr())
      If Result <> FAIL And Encontro Then
         Clave = Arr(0)
         Result = IIf(ConvertCadenaHexa(Encriptar(UCase(Trim(TxtClave)), UCase(Trim("ADMINISTRADOR")))) = Clave, SUCCEED, FAIL)
      End If
   Else
    Result = FAIL
   End If
   Valclave = Result
End Function




