VERSION 5.00
Object = "{0BA686C6-F7D3-101A-993E-0000C0EF6F5E}#1.0#0"; "THREED32.OCX"
Begin VB.Form FrmDescuentos 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Descuentos"
   ClientHeight    =   4065
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   6030
   Icon            =   "FrmDescu.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   ScaleHeight     =   4065
   ScaleWidth      =   6030
   Begin VB.Frame Frame1 
      Height          =   2655
      Left            =   120
      TabIndex        =   0
      Top             =   120
      Width           =   5775
      Begin VB.CheckBox ChkDescCond 
         Caption         =   "Descuento Condicionado"
         Height          =   495
         Left            =   3840
         TabIndex        =   8
         Top             =   1560
         Width           =   1455
      End
      Begin VB.OptionButton OptPorce 
         Caption         =   "&Porcentaje (%) :"
         Height          =   255
         Left            =   120
         TabIndex        =   3
         Top             =   1200
         Value           =   -1  'True
         Width           =   1455
      End
      Begin VB.TextBox TxtDescu 
         Enabled         =   0   'False
         Height          =   285
         Index           =   3
         Left            =   3840
         MaxLength       =   18
         TabIndex        =   6
         Top             =   1200
         Visible         =   0   'False
         Width           =   1815
      End
      Begin VB.TextBox TxtDescu 
         Height          =   285
         Index           =   5
         Left            =   1560
         MaxLength       =   15
         TabIndex        =   9
         Top             =   2160
         Width           =   1575
      End
      Begin VB.TextBox TxtDescu 
         Height          =   285
         Index           =   4
         Left            =   1560
         MaxLength       =   18
         TabIndex        =   7
         Top             =   1680
         Width           =   1815
      End
      Begin VB.TextBox TxtDescu 
         Height          =   285
         Index           =   2
         Left            =   1560
         MaxLength       =   5
         TabIndex        =   4
         Top             =   1200
         Width           =   705
      End
      Begin VB.TextBox TxtDescu 
         Height          =   285
         Index           =   1
         Left            =   1560
         MaxLength       =   30
         TabIndex        =   2
         Top             =   720
         Width           =   4095
      End
      Begin VB.TextBox TxtDescu 
         Height          =   285
         Index           =   0
         Left            =   1560
         MaxLength       =   3
         TabIndex        =   1
         Top             =   240
         Width           =   495
      End
      Begin Threed.SSCommand CmdSelec 
         Height          =   375
         Left            =   3120
         TabIndex        =   10
         ToolTipText     =   "Selecci�n de Cuentas Contables"
         Top             =   2160
         Width           =   615
         _Version        =   65536
         _ExtentX        =   1085
         _ExtentY        =   661
         _StockProps     =   78
         BevelWidth      =   0
         RoundedCorners  =   0   'False
         Outline         =   0   'False
         Picture         =   "FrmDescu.frx":058A
      End
      Begin Threed.SSCommand CmdLimpia 
         Height          =   375
         Left            =   5160
         TabIndex        =   17
         ToolTipText     =   "Limpia Pantalla"
         Top             =   240
         Width           =   375
         _Version        =   65536
         _ExtentX        =   661
         _ExtentY        =   661
         _StockProps     =   78
         Picture         =   "FrmDescu.frx":0F3C
      End
      Begin VB.OptionButton OptValor 
         Caption         =   "&Valor :"
         Enabled         =   0   'False
         Height          =   255
         Left            =   3000
         TabIndex        =   5
         Top             =   1200
         Visible         =   0   'False
         Width           =   855
      End
      Begin VB.Label LblCuenta 
         Caption         =   "Cuenta C&ontable :"
         Height          =   255
         Left            =   120
         TabIndex        =   21
         Top             =   2160
         Width           =   1335
      End
      Begin VB.Label LblTope 
         Caption         =   "&A partir de :"
         Height          =   255
         Left            =   120
         TabIndex        =   20
         Top             =   1680
         Width           =   1095
      End
      Begin VB.Label LblDescripcion 
         Caption         =   "&Descripci�n :"
         Height          =   255
         Left            =   120
         TabIndex        =   19
         Top             =   720
         Width           =   975
      End
      Begin VB.Label LblCodigo 
         Caption         =   "&C�digo :"
         Height          =   255
         Left            =   120
         TabIndex        =   18
         Top             =   240
         Width           =   735
      End
   End
   Begin VB.Frame Frame2 
      Height          =   1095
      Left            =   480
      TabIndex        =   11
      Top             =   2880
      Width           =   4935
      Begin Threed.SSCommand SCmd_Options 
         Height          =   735
         Index           =   0
         Left            =   120
         TabIndex        =   12
         Top             =   240
         Width           =   855
         _Version        =   65536
         _ExtentX        =   1508
         _ExtentY        =   1296
         _StockProps     =   78
         Caption         =   "&GUARDAR"
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   6
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Picture         =   "FrmDescu.frx":1606
      End
      Begin Threed.SSCommand SCmd_Options 
         Height          =   735
         Index           =   1
         Left            =   1080
         TabIndex        =   13
         Top             =   240
         Visible         =   0   'False
         Width           =   855
         _Version        =   65536
         _ExtentX        =   1508
         _ExtentY        =   1296
         _StockProps     =   78
         Caption         =   "&BORRAR"
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   6
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Picture         =   "FrmDescu.frx":1CD0
      End
      Begin Threed.SSCommand SCmd_Options 
         Height          =   735
         Index           =   3
         Left            =   3000
         TabIndex        =   15
         Top             =   240
         Width           =   855
         _Version        =   65536
         _ExtentX        =   1508
         _ExtentY        =   1296
         _StockProps     =   78
         Caption         =   "&LISTAR"
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   6
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Picture         =   "FrmDescu.frx":239A
      End
      Begin Threed.SSCommand SCmd_Options 
         Height          =   735
         Index           =   4
         Left            =   3960
         TabIndex        =   16
         Top             =   240
         Width           =   855
         _Version        =   65536
         _ExtentX        =   1508
         _ExtentY        =   1296
         _StockProps     =   78
         Caption         =   "&SALIR"
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   6
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Picture         =   "FrmDescu.frx":2A64
      End
      Begin Threed.SSCommand SCmd_Options 
         Height          =   735
         Index           =   2
         Left            =   2040
         TabIndex        =   14
         Top             =   240
         Width           =   855
         _Version        =   65536
         _ExtentX        =   1508
         _ExtentY        =   1296
         _StockProps     =   78
         Caption         =   "S&ELECCION"
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   6
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Picture         =   "FrmDescu.frx":312E
      End
   End
End
Attribute VB_Name = "FrmDescuentos"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Public OpcCod        As String   'Opci�n de seguridad
Public Tabla As String
Public Mensaje As String
Public EncontroT As Boolean
Private vEsNuevo As Boolean

Private Sub ChkDescCond_KeyPress(KeyAscii As Integer)
    Call Cambiar_Enter(KeyAscii)        'LJSA M4311
End Sub


Private Sub CmdLimpia_Click()
    Call Limpiar
End Sub
Private Sub CmdSelec_Click()
   Dim CodigoD As String
   Codigo = NUL$
   'JLPB T29905/R28918 INICIO Se deja en comentario la siguiente linea
   'CodigoD = Seleccion("CUENTAS", "NO_NOMB_CUEN", "CD_CODI_CUEN,NO_NOMB_CUEN", "CUENTAS CONTABLES", NUL$)
   Condicion = "TX_PUCNIIF_CUEN<>'1'"
   CodigoD = Seleccion("CUENTAS", "NO_NOMB_CUEN", "CD_CODI_CUEN,NO_NOMB_CUEN", "CUENTAS CONTABLES", Condicion)
   'JLPB T29905/R28918 FIN
   If CodigoD <> NUL$ Then TxtDescu(5) = CodigoD
   TxtDescu(5).SetFocus
End Sub
Private Sub Form_Load()
    Call CenterForm(MDI_Inventarios, Me)
    Tabla = "DESCUENTO"
    Mensaje = "Descuento"
'    Call Leer_Permisos("01002", SCmd_Options(0), SCmd_Options(1), SCmd_Options(3))
End Sub
Private Sub OptPorce_Click()
  TxtDescu(3) = "0"
  TxtDescu(3).Enabled = False
  TxtDescu(2).Enabled = True
End Sub
Private Sub OptValor_Click()
  TxtDescu(2) = "0"
  TxtDescu(2).Enabled = False
  TxtDescu(3).Enabled = True
End Sub
Private Sub OptPorce_KeyPress(KeyAscii As Integer)
 If OptPorce.Value = True Then
   If KeyAscii = 13 Then
     TxtDescu(2).SetFocus
   End If
 Else
   If KeyAscii = 13 Then
     TxtDescu(3).SetFocus
   End If
 End If
End Sub
Private Sub OptValor_KeyPress(KeyAscii As Integer)
  If OptValor.Value = True Then
   If KeyAscii = 13 Then
     TxtDescu(3).SetFocus
   End If
  Else
   If KeyAscii = 13 Then
     TxtDescu(4).SetFocus
   End If
  End If
End Sub
Private Sub SCmd_Options_Click(Index As Integer)
Dim descu As String
    Select Case Index
        Case 0: Encontro = EncontroT
                Grabar
        Case 1: Borrar
        Case 2: descu = Seleccion(Tabla, "DE_NOMB_DESC", "CD_CODI_DESC,DE_NOMB_DESC", "DESCUENTOS", NUL$)
                If descu <> NUL$ Then TxtDescu(0) = descu
                TxtDescu(0).SetFocus
                SCmd_Options(2).SetFocus
        Case 3: Call Listar("DESCUENT.rpt", Tabla, "CD_CODI_DESC", "DE_NOMB_DESC", "Descuentos", NUL$, NUL$, NUL$, NUL$, NUL$)
                Me.SetFocus
        Case 4: Unload Me
    End Select
End Sub
Private Sub SCmd_Options_GotFocus(Index As Integer)
 Select Case Index
    Case 0: Msglin "Pulse este icono para grabar"
    Case 1: Msglin "Pulse este icono para borrar"
    Case 2: Msglin "Pulse este icono para selecci�n"
    Case 3: Msglin "Pulse este icono para generar un listado"
    Case 4: Msglin "Pulse este icono para cerrar la ventana "
  End Select
End Sub
Private Sub TxtDescu_GotFocus(Index As Integer)
   TxtDescu(Index).SelStart = 0
   TxtDescu(Index).SelLength = Len(TxtDescu(Index).Text)
   Select Case Index
      Case 0: Msglin "Digite el C�digo del " & Mensaje
      Case 1: Msglin "Digite la Descripci�n del " & Mensaje
         If TxtDescu(0) = NUL$ Then TxtDescu(0).SetFocus: Exit Sub
      Case 2: Msglin "Digite el Porcentaje del " & Mensaje
      Case 3: Msglin "Digite el Valor del " & Mensaje
      Case 4: Msglin "Digite el Valor Base del " & Mensaje
      Case 5: Msglin "Digite la Cuenta Contable del " & Mensaje
   End Select
End Sub
Private Sub TxtDescu_KeyDown(Index As Integer, KeyCode As Integer, Shift As Integer)
'Dim Terc As String     'DEPURACION DE CODIGO
    Select Case Index
        Case 0: If KeyCode = 40 Then TxtDescu(Index + 1).SetFocus: Exit Sub
        Case 5:
                If KeyCode = 38 Then
                 If TxtDescu(Index - 1).Enabled = True Then
                  TxtDescu(Index - 1).SetFocus: Exit Sub
                 End If
                End If
        Case Else:
                    If KeyCode = 38 Then
                       If TxtDescu(Index - 1).Enabled = True Then
                          TxtDescu(Index - 1).SetFocus
                       End If
                    Else
                     If KeyCode = 40 Then
                       If TxtDescu(Index + 1).Enabled = True Then
                          TxtDescu(Index + 1).SetFocus
                       End If
                     End If
                    End If
    End Select
End Sub
Private Sub TxtDescu_KeyPress(Index As Integer, KeyAscii As Integer)
 If Index = 2 Then
  Call ValKeyReal(TxtDescu(2), KeyAscii)
  TxtDescu(Index).MaxLength = Determinar_Longitud(TxtDescu(Index), 4) 'JLPB T20945
 Else
  If Index = 3 Or Index = 4 Then
    Call ValKeyNum(KeyAscii)
  Else
    Call ValKeyAlfaNum(KeyAscii)
  End If
 End If
 If Index = 5 Then
  If KeyAscii = 13 Then
   SCmd_Options(0).SetFocus
  End If
 Else
   Call Cambiar_Enter(KeyAscii)
 End If
End Sub
Private Sub TxtDescu_LostFocus(Index As Integer)
   Msglin NUL$
   'LJSA M4601 --- Se pone en comentario ----
   ''If Not IsNumeric(TxtDescu(Index)) Then TxtDescu(Index) = 0#        'LJSA M4200
   ''LJSA M4200
   'If Not IsNumeric(TxtDescu(Index)) And Index <> 1 Then
   '   If Index <> 5 Then TxtDescu(Index) = 0#
   'End If
   ''LJSA M4200
   'LJSA M4601 --- Se pone en comentario ----
   'LJSA M4601 -----------------
   If Index <> 1 And Index <> 5 And Index <> 0 Then
      If Val(TxtDescu(Index).Text) = 0 Then TxtDescu(Index).Text = Format(0, "#0.##00")       'LJSA M4601
   End If
   'LJSA M4601 -----------------
   Select Case Index
      Case 0: If TxtDescu(0) <> NUL$ Then Buscar_Descuento
      Case 2:
         If TxtDescu(Index) = NUL$ Then
            'TxtDescu(Index) = "#0.##00"
            TxtDescu(Index) = "0"
         Else
            If TxtDescu(Index) <= 99.9999 Then
               TxtDescu(Index) = Format(TxtDescu(Index), "#0.##00")
            Else
               Call Mensaje1("Revise el porcentaje del " & Mensaje, 3)
               TxtDescu(Index) = "#0.##00"
               If TxtDescu(Index).Enabled = True Then
                  TxtDescu(Index).SetFocus: Exit Sub
               End If
            End If
         End If
      Case 3, 4:
         If TxtDescu(Index) = NUL$ Then
            TxtDescu(Index) = "0"
         Else
            TxtDescu(Index) = Format(TxtDescu(Index), "#,###,###,###,##0.#0")
         End If
      Case 5: If TxtDescu(5) <> NUL$ Then Buscar_Cuenta (5)
   End Select
End Sub

Private Sub Buscar_Descuento()
'ReDim Arr(5)
ReDim arr(6)        'LJSA M4311
Dim I As Byte
Dim StDes As String 'JLPB T20945
   Condicion = "CD_CODI_DESC=" & Comi & Cambiar_Comas_Comillas(TxtDescu(0)) & Comi
               
   Result = LoadData(Tabla, Asterisco, Condicion, arr())
   If (Result <> False) Then
     If Encontro Then
        vEsNuevo = False
        TxtDescu(1) = arr(1)
        'JLPB T20945 INICIO LA SIGUIENTE LINEA SE DEJA EN COMENTARIO
        'TxtDescu(2) = Format(Arr(2), "#0.##00")
        StDes = Mid(arr(2), InStr(1, arr(2), ".") + 1, 5)
        If Mid(StDes, 3, 1) = 0 Or Mid(StDes, 3, 1) = 9 And Mid(StDes, 2, 1) <> 9 Then StDes = Mid(arr(2), InStr(1, arr(2), ".") + 1, 2)
        If Len(StDes) = 2 Then
           TxtDescu(2) = Format(arr(2), "####0.00")
        Else
           TxtDescu(2) = Format(arr(2), "####0.0")
        End If
        'JLPB T20945 FIN
        TxtDescu(3) = Format(arr(5), "#,###,###,###,##0.#0")
        TxtDescu(4) = Format(arr(3), "#,###,###,###,##0.#0")
        TxtDescu(5) = arr(4)
        ChkDescCond.Value = arr(6)      'LJSA M4311
        
        If CDbl(arr(2)) <> 0 Then
         OptPorce.Value = True
         TxtDescu(2).Enabled = True
         TxtDescu(3).Enabled = False
        Else
          If CDbl(arr(5)) <> 0 Then
           OptValor.Value = True
           TxtDescu(3).Enabled = True
           TxtDescu(2).Enabled = False
          Else
           OptPorce.Value = True
           TxtDescu(2).Enabled = True
           TxtDescu(3).Enabled = False
          End If
        End If
        
        EncontroT = True
     Else
        vEsNuevo = True
        For I = 1 To 5
          If I = 2 Or I = 3 Or I = 4 Then
             TxtDescu(I) = "0"
          Else
             TxtDescu(I) = NUL$
          End If
        Next I
        OptPorce.Value = True
        TxtDescu(3).Enabled = False
        TxtDescu(2).Enabled = True
        EncontroT = False
     End If
   End If
End Sub

Private Sub Buscar_Cuenta(ByVal indice As Integer)
   ReDim arr(0)
   Condicion = "CD_CODI_CUEN=" & Comi & Cambiar_Comas_Comillas(TxtDescu(indice)) & Comi
   Condicion = Condicion & " AND TX_PUCNIIF_CUEN<>'1'" 'JLPB T29905/R28918
   Result = LoadData("CUENTAS", "CD_CODI_CUEN", Condicion, arr())
   If (Result <> False) Then
      If Encontro Then
         TxtDescu(indice) = arr(0)
         If Valida_Nivel_Cuentas(TxtDescu(indice)) = True Then
            Call Mensaje1("La cuenta no es de �ltimo nivel", 3)
            TxtDescu(indice) = NUL$
            'TxtDescu(Indice).SetFocus      LJSA M4200 Depuraci�n de c�digo
         End If
      Else
         TxtDescu(indice) = NUL$
         TxtDescu(indice).SetFocus
      End If
   End If
End Sub
Private Sub Grabar()
   DoEvents
   
   Call MouseClock
   Msglin "Ingresando informaci�n del " & Mensaje

   If (TxtDescu(0) = NUL$ Or TxtDescu(1) = NUL$) Then
      Call Mensaje1("Se deben especificar los datos completos del " & Mensaje, 3)
      Call MouseNorm: Exit Sub
   End If
   
   If OptPorce.Value = True Then
     If CSng(TxtDescu(2)) = 0 Then
      Call Mensaje1("Se debe especificar el porcentaje del " & Mensaje, 3)
      Call MouseNorm: Exit Sub
     End If
   Else
     If CDbl(TxtDescu(3)) = 0 Then
      Call Mensaje1("Se debe especificar el valor del " & Mensaje, 3)
      Call MouseNorm: Exit Sub
     End If
   End If
    If Valida_Nivel_Cuentas(TxtDescu(5).Text) Then
        Call Mensaje1("Verifique que la cuenta contable sea de �ltimo nivel!", 2)
        Call MouseNorm: Exit Sub
    End If
   
   If (BeginTran(STranIUp & Tabla) <> FAIL) Then
       If vEsNuevo Then
          Valores = "CD_CODI_DESC = " & Comi & Cambiar_Comas_Comillas(TxtDescu(0)) & Comi
          Valores = Valores & Coma & "DE_NOMB_DESC = " & Comi & Cambiar_Comas_Comillas(TxtDescu(1)) & Comi
          Debug.Print TxtDescu(2)
          Debug.Print CSng(TxtDescu(2))
          Valores = Valores & Coma & "PR_PORC_DESC = " & CSng(TxtDescu(2))
          Valores = Valores & Coma & "VL_TOPE_DESC = " & CDbl(TxtDescu(4))
          Valores = Valores & Coma & "CD_CUEN_DESC = " & Comi & Cambiar_Comas_Comillas(TxtDescu(5)) & Comi
          Valores = Valores & Coma & "VL_VALO_DESC = " & CDbl(TxtDescu(3))
          Valores = Valores & Coma & "TX_TIPO_DESC=" & Comi & ChkDescCond.Value & Comi      'LJSA M4311
          'GAPM M6401 INICIO
          Valores = Valores & Coma & "TX_TIPDESC_DESC='3'"
          'GAPM M6401 FIN
          Result = DoInsertSQL(Tabla, Valores)
          If (Result <> FAIL) Then Result = Auditor(Tabla, TranIns, LastCmd)
       Else
         If WarnMsg("Desea Modificar la Informaci�n del " & Mensaje, 256) Then
            Valores = "DE_NOMB_DESC=" & Comi & Cambiar_Comas_Comillas(TxtDescu(1)) & Comi & Coma
            Debug.Print TxtDescu(2)
            Debug.Print CSng(TxtDescu(2))
            Valores = Valores & " PR_PORC_DESC=" & CSng(TxtDescu(2)) & Coma
            Valores = Valores & " VL_TOPE_DESC=" & CDbl(TxtDescu(4)) & Coma
            Valores = Valores & " CD_CUEN_DESC=" & Comi & Cambiar_Comas_Comillas(TxtDescu(5)) & Comi & Coma
            Valores = Valores & " VL_VALO_DESC=" & CDbl(TxtDescu(3))
            Valores = Valores & Coma & "TX_TIPO_DESC=" & Comi & ChkDescCond.Value & Comi      'LJSA M4311
            'GAPM M6401 INICIO
            Valores = Valores & Coma & "TX_TIPDESC_DESC='3'"
            'GAPM M6401 FIN
            Condicion = "CD_CODI_DESC=" & Comi & Cambiar_Comas_Comillas(TxtDescu(0)) & Comi
            
            Result = DoUpdate(Tabla, Valores, Condicion)
            If (Result <> FAIL) Then Result = Auditor(Tabla, TranUpd, LastCmd)
         End If
       End If
   End If
   If (Result <> FAIL) Then
      If (CommitTran() <> FAIL) Then
          Limpiar
      Else
         Call RollBackTran
      End If
   Else
      Call RollBackTran
   End If
   Call MouseNorm
   Msglin NUL$
End Sub
Private Sub Borrar()
   DoEvents
   
   Call MouseClock
   
   Msglin "Borrando un " & Mensaje
   If (TxtDescu(0) = NUL$) Then
       Call Mensaje1("Ingrese el c�digo del " & Mensaje, 1)
       Call MouseNorm: Msglin NUL$: Exit Sub
   End If
   If (EncontroT <> False) Then
       If (Not WarnDel()) Then Call MouseNorm: Msglin NUL$: Exit Sub
        Condicion = "CD_CODI_DESC=" & Comi & Cambiar_Comas_Comillas(TxtDescu(0)) & Comi
   
        If (BeginTran(STranDel & Tabla) <> FAIL) Then
            Result = DoDelete(Tabla, Condicion)
            If (Result <> FAIL) Then Result = Auditor(Tabla, TranDel, LastCmd)
        End If
   Else
       Call Mensaje1("No se pudo borrar el " & Mensaje & " de la base de datos, debido a que no existe !!!...", 1)
       Call MouseNorm: Msglin NUL$: Exit Sub
   End If
   If (Result <> FAIL) Then
       If (CommitTran() <> FAIL) Then
          Limpiar
       Else
          Call RollBackTran
       End If
   Else
       Call RollBackTran
   End If
   Call MouseNorm
   Msglin NUL$
End Sub
Private Sub Limpiar()
Dim I As Byte
    For I = 0 To 5
     If I = 2 Or I = 3 Or I = 4 Then
        TxtDescu(I) = "0.00"
     Else
        TxtDescu(I) = NUL$
     End If
    Next I
    TxtDescu(2).Text = "0.0000"
    OptPorce.Value = True
    TxtDescu(3).Enabled = False
    TxtDescu(2).Enabled = True
    TxtDescu(0).SetFocus
    ChkDescCond.Value = 0       'LJSA M4311
End Sub
