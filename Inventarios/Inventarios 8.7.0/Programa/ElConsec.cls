VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "ElConsec"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit

Private AuCn As Long 'Autonum�rico
Private Codi As String * 10 'C�digo
Private Nomb As String * 50 'Nombre
Private Pref As String * 2 'Prefijo
Private AuDo As Long 'Autonum�rico en IN_DOCUMENTO
Private NAct As Long 'N�mero actual
Private aNla As Long 'Autonum�rico que lo anula en IN_COMPROBANTE
Private Requ As String * 1 'Requiere autorizacion
Private VarAutoConsecutivo As Long
Private VarNumeroActual As Long

Public Property Let AutoNumero(num As Long)
    AuCn = num
End Property

Public Property Get AutoNumero() As Long
    AutoNumero = AuCn
End Property

Public Property Let RequereAutorizacion(cue As String)
    Requ = cue
End Property

Public Property Get RequereAutorizacion() As String
    RequereAutorizacion = Requ
End Property

Public Property Let Codigo(cue As String)
    Codi = cue
End Property

Public Property Get Codigo() As String
    Codigo = Codi
End Property

Public Property Let Nombre(cue As String)
    Nomb = cue
End Property

Public Property Get Nombre() As String
    Nombre = Nomb
End Property

Public Property Let Prefijo(cue As String)
    Pref = cue
End Property

Public Property Get Prefijo() As String
    Prefijo = Pref
End Property

Public Property Let AutoDocu(num As Long)
    AuDo = num
End Property

Public Property Get AutoDocu() As Long
    AutoDocu = AuDo
End Property

Public Property Let NumActual(num As Long)
    NAct = num
End Property

Public Property Get NumActual() As Long
    NumActual = NAct
End Property

Public Property Let AutoAnula(num As Long)
    aNla = num
End Property

Public Property Get AutoAnula() As Long
    AutoAnula = aNla
End Property

Private Sub Class_Initialize()
    Requ = "N"
End Sub

Public Property Let AutoConsecutivo(auto As Long)
VarAutoConsecutivo = auto
End Property

Public Property Get AutoConsecutivo() As Long
AutoConsecutivo = VarAutoConsecutivo
End Property

Public Property Get NumeroActual() As Long
Dim ArrCMP() As Variant
Dim Mpos As String, Desd As String, Dici As String
Mpos = "NU_COMP_COMP"
Desd = "IN_COMPROBANTE"
Dici = "NU_AUTO_COMP=" & VarAutoConsecutivo
ReDim ArrCMP(0)
Result = LoadData(Desd, Mpos, Dici, ArrCMP())
If Result <> FAIL And Encontro Then
   VarNumeroActual = ArrCMP(0)
Else
   Call Mensaje1("No se pudo obtener el n�mero actual del consecutivo", 3)
End If
NumeroActual = VarNumeroActual
End Property



