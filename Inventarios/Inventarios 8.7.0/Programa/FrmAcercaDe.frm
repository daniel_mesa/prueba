VERSION 5.00
Begin VB.Form FrmAcercaDe 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Acerca de CNT- Inventarios "
   ClientHeight    =   4830
   ClientLeft      =   1110
   ClientTop       =   2580
   ClientWidth     =   5880
   ClipControls    =   0   'False
   Icon            =   "FrmAcercaDe.frx":0000
   LinkTopic       =   "Form2"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   3333.752
   ScaleMode       =   0  'User
   ScaleWidth      =   5521.624
   Begin VB.CommandButton CmdRestablecer 
      Caption         =   "Restablecer Versi�n DB "
      Height          =   285
      Left            =   3120
      MaskColor       =   &H00FF0000&
      Style           =   1  'Graphical
      TabIndex        =   4
      Tag             =   "Restablecer la version en la BD."
      Top             =   1200
      Visible         =   0   'False
      Width           =   2550
   End
   Begin VB.PictureBox Picture1 
      Appearance      =   0  'Flat
      AutoSize        =   -1  'True
      BackColor       =   &H80000004&
      BorderStyle     =   0  'None
      ClipControls    =   0   'False
      FillColor       =   &H80000000&
      ForeColor       =   &H80000004&
      Height          =   765
      Left            =   3840
      Picture         =   "FrmAcercaDe.frx":058A
      ScaleHeight     =   537.285
      ScaleMode       =   0  'User
      ScaleWidth      =   1327.41
      TabIndex        =   3
      Top             =   3960
      Width           =   1890
   End
   Begin VB.PictureBox picIcon 
      Appearance      =   0  'Flat
      BorderStyle     =   0  'None
      ClipControls    =   0   'False
      ForeColor       =   &H80000008&
      Height          =   840
      Left            =   120
      Picture         =   "FrmAcercaDe.frx":5180
      ScaleHeight     =   589.96
      ScaleMode       =   0  'User
      ScaleWidth      =   2159.675
      TabIndex        =   2
      Top             =   360
      Width           =   3075
   End
   Begin VB.CommandButton cmdOK 
      Cancel          =   -1  'True
      Caption         =   "ACEPTAR"
      Default         =   -1  'True
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   6
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   465
      Left            =   240
      Picture         =   "FrmAcercaDe.frx":C242
      Style           =   1  'Graphical
      TabIndex        =   1
      Top             =   3600
      Width           =   2010
   End
   Begin VB.CommandButton cmdSysInfo 
      Caption         =   "&INFORMACION"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   6
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   465
      Left            =   240
      Picture         =   "FrmAcercaDe.frx":C734
      Style           =   1  'Graphical
      TabIndex        =   0
      Top             =   4200
      Width           =   2010
   End
   Begin VB.Label LblLic 
      Caption         =   "Tipo de licencia:"
      Height          =   255
      Left            =   240
      TabIndex        =   11
      Top             =   1560
      Width           =   5295
   End
   Begin VB.Label lblDescription 
      Alignment       =   2  'Center
      BorderStyle     =   1  'Fixed Single
      Caption         =   "  DEMO: CNT SISTEMAS DE INFORMACION LTDA  "
      ForeColor       =   &H00000000&
      Height          =   300
      Left            =   165
      TabIndex        =   10
      Top             =   2040
      Width           =   5400
   End
   Begin VB.Label LblFecha 
      AutoSize        =   -1  'True
      Caption         =   "MES de A�o"
      Height          =   195
      Left            =   3240
      TabIndex        =   9
      Top             =   840
      Width           =   2550
   End
   Begin VB.Label LblVersionBd 
      Caption         =   "Versi�n   2.0"
      Height          =   225
      Left            =   3240
      TabIndex        =   8
      Top             =   495
      Width           =   2550
   End
   Begin VB.Label lblVersion 
      Caption         =   "Versi�n  "
      Height          =   225
      Left            =   3225
      TabIndex        =   7
      Top             =   135
      Width           =   2550
   End
   Begin VB.Label LblInformar 
      Caption         =   $"FrmAcercaDe.frx":CC26
      ForeColor       =   &H00000000&
      Height          =   855
      Left            =   180
      TabIndex        =   6
      Top             =   2520
      Width           =   5250
   End
   Begin VB.Label Label1 
      Caption         =   "Se autoriza el uso de este producto a :"
      Height          =   210
      Left            =   240
      TabIndex        =   5
      Top             =   1800
      Width           =   3375
   End
End
Attribute VB_Name = "FrmAcercaDe"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
' Opciones de seguridad de claves del Registro...
Const READ_CONTROL = &H20000
Const KEY_QUERY_VALUE = &H1
Const KEY_SET_VALUE = &H2
Const KEY_CREATE_SUB_KEY = &H4
Const KEY_ENUMERATE_SUB_KEYS = &H8
Const KEY_NOTIFY = &H10
Const KEY_CREATE_LINK = &H20
Const KEY_ALL_ACCESS = KEY_QUERY_VALUE + KEY_SET_VALUE + _
                       KEY_CREATE_SUB_KEY + KEY_ENUMERATE_SUB_KEYS + _
                       KEY_NOTIFY + KEY_CREATE_LINK + READ_CONTROL
                     
' Tipos principales de claves del Registro...
Const HKEY_LOCAL_MACHINE = &H80000002
Const ERROR_SUCCESS = 0
Const REG_SZ = 1                         ' Cadena Unicode terminada en Null
Const REG_DWORD = 4                      ' N�mero de 32 bits

Const gREGKEYSYSINFOLOC = "SOFTWARE\Microsoft\Shared Tools Location"
Const gREGVALSYSINFOLOC = "MSINFO"
Const gREGKEYSYSINFO = "SOFTWARE\Microsoft\Shared Tools\MSINFO"
Const gREGVALSYSINFO = "PATH"

Private Declare Function RegOpenKeyEx Lib "advapi32" Alias "RegOpenKeyExA" (ByVal hKey As Long, ByVal lpSubKey As String, ByVal ulOptions As Long, ByVal samDesired As Long, ByRef phkResult As Long) As Long
Private Declare Function RegQueryValueEx Lib "advapi32" Alias "RegQueryValueExA" (ByVal hKey As Long, ByVal lpValueName As String, ByVal lpReserved As Long, ByRef lpType As Long, ByVal lpData As String, ByRef lpcbData As Long) As Long
Private Declare Function RegCloseKey Lib "advapi32" (ByVal hKey As Long) As Long

Private Sub CmdRestablecer_Click()
Dim Arr(0)
   If Connect Then
      Result = LoadData("PARAMETROS_INVE", "NU_NVERDB_PINV", NUL$, Arr)
      If Result <> FAIL And Encontro Then
         Result = DoUpdate("PARAMETROS_INVE", "NU_NVERDB_PINV='', ID_REST_PINV='S'", NUL$)
      Else
         Valores = "NU_NVERDB_PINV='',ID_REST_PINV='S'"
         Result = DoInsertSQL("PARAMETROS_INVE", Valores)
      End If
      If Result <> FAIL Then
         Mensaje1 "La versi�n de la base de datos se ha restablecido.", 2
         Unload Me
      End If
   Else
      Mensaje1 "No esta conectado a la base de datos.", 3
   End If
End Sub

Private Sub cmdSysInfo_Click()
    Call StartSysInfo
'    Call DecimalesListar
End Sub
Private Sub cmdOK_Click()
    Unload Me
End Sub

Private Sub Form_Load()
   Call CenterForm(MDI_Inventarios, FrmAcercaDe)
   Call ValidaLicencia
   lblVersion.Caption = "Versi�n Software " & App.Major & "." & App.Minor & "." & App.Revision
   lblDescription.Caption = EntiLic
   If lblDescription.Caption = "" Then
     lblDescription.Caption = "Cnt Sistemas de Informaci�n Ltda."
   End If
   ReDim Arr(0)
   If Connect Then
      Result = LoadData("PARAMETROS_INVE", "NU_NVERDB_PINV", NUL$, Arr())
      If Result <> FAIL And Encontro Then
         LblVersionBd.Caption = "Versi�n Base de datos " & Arr(0)
      Else
         LblVersionBd.Caption = "Sin Verificar" & Arr(0)
      End If
   Else
      LblVersionBd.Caption = "Sin Verificar" & Arr(0)
   End If
'   LblFecha.Caption = Format(FechaLic, "dd/mm/yyyy")
   
   'HRR R1249
   If NumStation > 0 Then
      LblLic.Caption = LblLic.Caption & " Concurrente con " & NumStation & " estacion(es) habilitada(s)"
   Else
      LblLic.Caption = LblLic.Caption & " Corporativa"
   End If
   'R1249
   
End Sub
Public Sub StartSysInfo()
    On Error GoTo SysInfoErr
  
    Dim rc As Long
    Dim SysInfoPath As String
    
    ' Prueba a obtener del Registro la informaci�n del sistema sobre el nombre y la ruta del programa...
    If GetKeyValue(HKEY_LOCAL_MACHINE, gREGKEYSYSINFO, gREGVALSYSINFO, SysInfoPath) Then
    ' Prueba a obtener del Registro la informaci�n del sistema sobre la ruta del programa...
    ElseIf GetKeyValue(HKEY_LOCAL_MACHINE, gREGKEYSYSINFOLOC, gREGVALSYSINFOLOC, SysInfoPath) Then
        ' Comprueba la existencia de una versi�n conocida de un archivo de 32 bits
        If (Dir(SysInfoPath & "\MSINFO32.EXE") <> "") Then
            SysInfoPath = SysInfoPath & "\MSINFO32.EXE"
            
        ' Error - Imposible encontrar el archivo...
        Else
            GoTo SysInfoErr
        End If
    ' Error - Imposible encontrar la entrada de Registro...
    Else
        GoTo SysInfoErr
    End If
    
    Call Shell(SysInfoPath, vbNormalFocus)
    
    Exit Sub
SysInfoErr:
    MsgBox "System Information Is Unavailable At This Time", vbOKOnly
End Sub

Public Function GetKeyValue(KeyRoot As Long, KeyName As String, SubKeyRef As String, ByRef KeyVal As String) As Boolean
    Dim i As Long                                           ' Contador de bucle
    Dim rc As Long                                          ' C�digo de retorno
    Dim hKey As Long                                        ' Controlador a una clave de Registro abierta
'    Dim hDepth As Long         'DEPURACION DE CODIGO
    Dim KeyValType As Long                                  ' Tipo de dato de una clave de Registro
    Dim tmpVal As String                                    ' Almac�n temporal de una valor de clave de Registro
    Dim KeyValSize As Long                                  ' Tama�o de la variable de la clave de Registro
    '------------------------------------------------------------
    ' Abre la clave de Registro en la ra�z {HKEY_LOCAL_MACHINE...}
    '------------------------------------------------------------
    rc = RegOpenKeyEx(KeyRoot, KeyName, 0, KEY_ALL_ACCESS, hKey) ' Abre la clave de Registro
    
    If (rc <> ERROR_SUCCESS) Then GoTo GetKeyError          ' Trata el error...
    
    tmpVal = String$(1024, 0)                               ' Asigna espacio para la variable
    KeyValSize = 1024                                       ' Marca el tama�o de la variable
    
    '------------------------------------------------------------
    ' Recupera valores de claves de Registro...
    '------------------------------------------------------------
    rc = RegQueryValueEx(hKey, SubKeyRef, 0, _
                         KeyValType, tmpVal, KeyValSize)    ' Obtiene o crea un valor de clave
                        
    If (rc <> ERROR_SUCCESS) Then GoTo GetKeyError          ' Trata el error
    
    If (Asc(Mid(tmpVal, KeyValSize, 1)) = 0) Then           ' Win95 agrega una cadena terminada en Null...
        tmpVal = Left(tmpVal, KeyValSize - 1)               ' Se encontr� Null, se extrae de la cadena
    Else                                                    ' WinNT no tiene una cadena terminada en Null...
        tmpVal = Left(tmpVal, KeyValSize)                   ' No se encontr� Null, s�lo se extrae la cadena
    End If
    '------------------------------------------------------------
    ' Determina el tipo de valor de la clave para conversi�n...
    '------------------------------------------------------------
    Select Case KeyValType                                  ' Busca tipos de datos...
    Case REG_SZ                                             ' Tipo de dato de la cadena de la clave de Registro
        KeyVal = tmpVal                                     ' Copia el valor de la cadena
    Case REG_DWORD                                          ' El tipo de dato de la cadena de la clave es Double Word
        For i = Len(tmpVal) To 1 Step -1                    ' Convierte cada byte
            KeyVal = KeyVal + Hex(Asc(Mid(tmpVal, i, 1)))   ' Genera el valor car�cter a car�cter
        Next
        KeyVal = Format$("&h" + KeyVal)                     ' Convierte Double Word a String
    End Select
    
    GetKeyValue = True                                      ' Vuelve con �xito
    rc = RegCloseKey(hKey)                                  ' Cierra la clave de Registro
    Exit Function                                           ' Salir
    
GetKeyError:      ' Restaurar despu�s de que ocurra un error...
    KeyVal = ""                                             ' Establece el valor de retorno para una cadena vac�a
    GetKeyValue = False                                     ' Devuelve un error
    rc = RegCloseKey(hKey)                                  ' Cierra la clave de Registro
End Function

Private Sub picIcon_Click()
    FrmCmd.Show 1 'NMSR R1629
End Sub

'Private Sub Form_Unload(Cancel As Integer)
'   If Connect Then Call Carga_Datos_Empresa
'End Sub

Private Sub Picture1_DblClick()
'    Call AdicionesALaBase
End Sub
