VERSION 5.00
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "Comdlg32.ocx"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Object = "{C932BA88-4374-101B-A56C-00AA003668DC}#1.1#0"; "MSMASK32.OCX"
Object = "{0BA686C6-F7D3-101A-993E-0000C0EF6F5E}#1.0#0"; "THREED32.OCX"
Object = "{0002E540-0000-0000-C000-000000000046}#1.0#0"; "MSOWC.DLL"
Begin VB.Form frmPrmRENYGAS 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Parametros RENTAS y GASTOS"
   ClientHeight    =   7095
   ClientLeft      =   45
   ClientTop       =   435
   ClientWidth     =   11475
   Icon            =   "frmPrmRENYGAS.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   7095
   ScaleWidth      =   11475
   Begin VB.Frame frmPARAM 
      Height          =   6855
      Left            =   120
      TabIndex        =   1
      Top             =   120
      Visible         =   0   'False
      Width           =   11175
      Begin VB.ComboBox cboTIPO 
         Height          =   315
         Left            =   2760
         TabIndex        =   10
         Text            =   "Combo1"
         Top             =   240
         Width           =   2655
      End
      Begin VB.Frame fraRANGO 
         Height          =   2175
         Left            =   6000
         TabIndex        =   3
         Top             =   4080
         Width           =   4815
         Begin VB.TextBox txtNUMES 
            Height          =   375
            Left            =   1920
            Locked          =   -1  'True
            TabIndex        =   15
            Text            =   "3"
            Top             =   1440
            Width           =   375
         End
         Begin MSComCtl2.UpDown updNUMES 
            Height          =   375
            Left            =   2295
            TabIndex        =   14
            Top             =   1440
            Width           =   240
            _ExtentX        =   450
            _ExtentY        =   661
            _Version        =   393216
            Value           =   1
            BuddyControl    =   "txtNUMES"
            BuddyDispid     =   196612
            OrigLeft        =   1680
            OrigTop         =   1320
            OrigRight       =   1920
            OrigBottom      =   1695
            Max             =   12
            Min             =   1
            SyncBuddy       =   -1  'True
            BuddyProperty   =   0
            Enabled         =   -1  'True
         End
         Begin MSMask.MaskEdBox txtFCDESDE 
            Height          =   315
            Left            =   960
            TabIndex        =   4
            Top             =   480
            Width           =   1215
            _ExtentX        =   2143
            _ExtentY        =   556
            _Version        =   393216
            PromptChar      =   "_"
         End
         Begin MSMask.MaskEdBox txtFCHASTA 
            Height          =   315
            Left            =   3240
            TabIndex        =   5
            Top             =   480
            Width           =   1215
            _ExtentX        =   2143
            _ExtentY        =   556
            _Version        =   393216
            PromptChar      =   "_"
         End
         Begin Threed.SSCommand cmdIMPRIMIR 
            Height          =   735
            Left            =   3600
            TabIndex        =   6
            ToolTipText     =   "Generar informe en hoja de Excel"
            Top             =   1200
            Width           =   855
            _Version        =   65536
            _ExtentX        =   1508
            _ExtentY        =   1296
            _StockProps     =   78
            Caption         =   "&GENERAR"
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   6
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            RoundedCorners  =   0   'False
            Picture         =   "frmPrmRENYGAS.frx":058A
         End
         Begin VB.Label Label3 
            Caption         =   "Meses"
            Height          =   255
            Left            =   2640
            TabIndex        =   17
            Top             =   1485
            Width           =   615
         End
         Begin VB.Label Label2 
            Alignment       =   1  'Right Justify
            Caption         =   "Inventario promedio sobre los �ltimos"
            Height          =   495
            Left            =   360
            TabIndex        =   16
            Top             =   1440
            Width           =   1455
         End
         Begin VB.Label Label1 
            Alignment       =   1  'Right Justify
            Caption         =   "Hasta:"
            Height          =   255
            Left            =   2400
            TabIndex        =   8
            Top             =   480
            Width           =   735
         End
         Begin VB.Label lblDESDE 
            Alignment       =   1  'Right Justify
            Caption         =   "Desde:"
            Height          =   255
            Left            =   120
            TabIndex        =   7
            Top             =   480
            Width           =   735
         End
      End
      Begin VB.CheckBox chkGRUPOS 
         Caption         =   "Todos los Grupos"
         Height          =   375
         Left            =   120
         TabIndex        =   2
         Top             =   600
         Width           =   2895
      End
      Begin MSComctlLib.ProgressBar pgbLLEVO 
         Height          =   255
         Left            =   6000
         TabIndex        =   9
         Top             =   240
         Visible         =   0   'False
         Width           =   4455
         _ExtentX        =   7858
         _ExtentY        =   450
         _Version        =   393216
         Appearance      =   1
      End
      Begin MSComctlLib.ListView lstBODEGAS 
         Height          =   2055
         Left            =   240
         TabIndex        =   11
         Top             =   4200
         Width           =   5535
         _ExtentX        =   9763
         _ExtentY        =   3625
         View            =   3
         LabelEdit       =   1
         LabelWrap       =   -1  'True
         HideSelection   =   -1  'True
         FullRowSelect   =   -1  'True
         GridLines       =   -1  'True
         _Version        =   393217
         ForeColor       =   -2147483640
         BackColor       =   -2147483643
         BorderStyle     =   1
         Appearance      =   1
         NumItems        =   0
      End
      Begin MSComctlLib.ListView lstGRUPOS 
         Height          =   2895
         Left            =   240
         TabIndex        =   12
         Top             =   1080
         Width           =   10500
         _ExtentX        =   18521
         _ExtentY        =   5106
         View            =   3
         LabelEdit       =   1
         MultiSelect     =   -1  'True
         LabelWrap       =   -1  'True
         HideSelection   =   0   'False
         AllowReorder    =   -1  'True
         Checkboxes      =   -1  'True
         FullRowSelect   =   -1  'True
         GridLines       =   -1  'True
         _Version        =   393217
         ForeColor       =   -2147483640
         BackColor       =   -2147483643
         BorderStyle     =   1
         Appearance      =   1
         NumItems        =   2
         BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Text            =   "Nombre del Art�culo"
            Object.Width           =   7761
         EndProperty
         BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   1
            Text            =   "C�digo"
            Object.Width           =   3881
         EndProperty
      End
      Begin VB.Label txtTIPO 
         Alignment       =   1  'Right Justify
         Caption         =   "Tipo de Art�culos:"
         Height          =   315
         Left            =   120
         TabIndex        =   13
         Top             =   240
         Width           =   2415
      End
   End
   Begin MSComctlLib.ImageList imgBotones 
      Left            =   10920
      Top             =   3960
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   20
      ImageHeight     =   14
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   7
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmPrmRENYGAS.frx":0F0C
            Key             =   ""
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmPrmRENYGAS.frx":1046
            Key             =   "ASG"
         EndProperty
         BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmPrmRENYGAS.frx":1360
            Key             =   "CAN"
         EndProperty
         BeginProperty ListImage4 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmPrmRENYGAS.frx":167A
            Key             =   "SAV"
         EndProperty
         BeginProperty ListImage5 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmPrmRENYGAS.frx":1994
            Key             =   "ADD"
         EndProperty
         BeginProperty ListImage6 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmPrmRENYGAS.frx":1CAE
            Key             =   "DEL"
         EndProperty
         BeginProperty ListImage7 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmPrmRENYGAS.frx":1FC8
            Key             =   "CHA"
         EndProperty
      EndProperty
   End
   Begin MSComDlg.CommonDialog dlgARCHIVO 
      Left            =   10920
      Top             =   0
      _ExtentX        =   847
      _ExtentY        =   847
      _Version        =   393216
   End
   Begin Threed.SSCommand cmdSAVE 
      Height          =   735
      Left            =   9320
      TabIndex        =   18
      ToolTipText     =   "Guardar la informaci�n en archivo Excel"
      Top             =   6120
      Width           =   855
      _Version        =   65536
      _ExtentX        =   1508
      _ExtentY        =   1296
      _StockProps     =   78
      Caption         =   "&GUARDAR"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   6
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Picture         =   "frmPrmRENYGAS.frx":22E2
   End
   Begin OWC.Spreadsheet wksRENYGAS 
      Height          =   5655
      Left            =   120
      TabIndex        =   0
      Top             =   360
      Visible         =   0   'False
      Width           =   11220
      HTMLURL         =   ""
      HTMLData        =   $"frmPrmRENYGAS.frx":29AC
      DataType        =   "HTMLDATA"
      AutoFit         =   0   'False
      DisplayColHeaders=   -1  'True
      DisplayGridlines=   -1  'True
      DisplayHorizontalScrollBar=   -1  'True
      DisplayRowHeaders=   -1  'True
      DisplayTitleBar =   0   'False
      DisplayToolbar  =   0   'False
      DisplayVerticalScrollBar=   -1  'True
      EnableAutoCalculate=   -1  'True
      EnableEvents    =   -1  'True
      MoveAfterReturn =   -1  'True
      MoveAfterReturnDirection=   0
      RightToLeft     =   0   'False
      ViewableRange   =   "1:65536"
   End
   Begin Threed.SSCommand cmdBACK 
      Height          =   735
      HelpContextID   =   40
      Left            =   10440
      TabIndex        =   19
      ToolTipText     =   "Regresar a pantalla de par�metros"
      Top             =   6120
      Width           =   855
      _Version        =   65536
      _ExtentX        =   1508
      _ExtentY        =   1296
      _StockProps     =   78
      Caption         =   "&ATRAS"
      ForeColor       =   -2147483640
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   6
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Picture         =   "frmPrmRENYGAS.frx":3082
   End
End
Attribute VB_Name = "frmPrmRENYGAS"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Dim ArrUXB() As Variant
'Dim Articulo As UnArticulo     'DEPURACION DE CODIGO
Dim Dueno As New ElTercero
'Dim LaBodega As String, LaAccion As String, vTipo As String * 1
Dim LaAccion As String, vTipo As String * 1     'DEPURACION DE CODIGO
Dim CnTdr As Integer, NumEnCombo As Integer, vclave As String
Public OpcCod        As String   'Opci�n de seguridad
'NU_AUTO_ARTI, CD_CODI_ARTI, NO_NOMB_ARTI, NU_AUTO_UNVE_ARTI, TX_VENCE_ARTI, TX_PARA_ARTI,
'TX_ENTRA_ARTI, CD_GRUP_ARTI, CD_USOS_ARTI, VL_ULCO_ARTI, VL_COPR_ARTI, CT_EXIS_ARTI, DE_OBSE_ARTI, ID_TIPO_ARTIID_TIPO_ARTI, CD_RIPS_ARTI, NU_INDPYP_ARTI

Private Sub cboTIPO_GotFocus()
    NumEnCombo = Me.cboTIPO.ListIndex
End Sub

Private Sub cmdBACK_Click()
    Me.wksRENYGAS.ActiveSheet.UsedRange.Clear
    Me.wksRENYGAS.Visible = False
    Me.FrmParam.Visible = True
    Me.cmdBACK.Visible = False
    Me.cmdSAVE.Visible = False
End Sub

Private Sub CmdImprimir_Click()
    Dim Renglon As MSComctlLib.ListItem
    Dim ArrRYG() As Variant
    Dim strGRUPOS As String, strBODEGAS As String
    Dim strTITRANGO As String, strTITFECHA As String
    Dim DocumentoCompras As String, DocumentoVentas As String
    Dim FecDesde As Date, FecHasta As Date
    Dim vContar As Long, UnGrupo As ElGrupo
    Dim vFil As Long, vCol As Long
    Dim CllResulta As New Collection, vclave As String, vCualBodega As String
    Dim LosGrupos As String, LasBodegas As String
    
    'REOL MANTIS: 0000576
    If Me.lstGRUPOS.ListItems.Count = 0 Then Exit Sub
    
    FecDesde = CDate(Me.txtFCDESDE.Text): FecHasta = CDate(Me.txtFCHASTA.Text)
    If Me.chkGRUPOS.Value = 0 Then
        For Each Renglon In Me.lstGRUPOS.ListItems
            If Renglon.Selected Then strGRUPOS = strGRUPOS & IIf(Len(strGRUPOS) > 0, " OR ", "") & "{Query.CD_GRUP_ARTI}='" & Renglon.ListSubItems("COAR").Text & "'"
            If Renglon.Selected Then LosGrupos = LosGrupos & IIf(Len(LosGrupos) > 0, Coma, "") & "'" & Renglon.ListSubItems("COAR").Text & "'"
        Next
    End If
    For Each Renglon In Me.lstBODEGAS.ListItems
        If Renglon.Selected Then vCualBodega = Renglon.ListSubItems("COD").Text
        If Renglon.Selected Then strBODEGAS = strBODEGAS & IIf(Len(strBODEGAS) > 0, " OR ", "") & "{Query.TX_CODI_BODE}='" & Renglon.ListSubItems("COD").Text & "'"
        If Renglon.Selected Then LasBodegas = LasBodegas & IIf(Len(LasBodegas) > 0, Coma, "") & "'" & Renglon.ListSubItems("COD").Text & "'"
    Next
    If Len(LosGrupos) > 0 Then LosGrupos = "(" & LosGrupos & ")"
    If Len(LasBodegas) > 0 Then LasBodegas = "(" & LasBodegas & ")"
    
    If Len(strGRUPOS) > 0 Then strGRUPOS = "(" & strGRUPOS & ")"
    If Len(strBODEGAS) > 0 Then strBODEGAS = "(" & strBODEGAS & ")"
    If Len(strGRUPOS) > 0 And Len(strBODEGAS) > 0 Then
        strGRUPOS = strGRUPOS & " AND " & strBODEGAS
    Else
        strGRUPOS = strGRUPOS & strBODEGAS
    End If
    strTITRANGO = "TODOS LOS COMPROBANTES"
    strTITFECHA = "A LA FECHA"
    FecDesde = CDate(Me.txtFCDESDE.Text): FecHasta = CDate(Me.txtFCHASTA.Text)
    strGRUPOS = strGRUPOS & IIf(Len(strGRUPOS) > 0, " AND ", "") & _
        "DateValue (Val (Mid ({Query.FE_FECH_KARD}, 1, 4)), Val (Mid ({Query.FE_FECH_KARD}, 6, 2)), Val (Mid ({Query.FE_FECH_KARD}, 9, 2)))>DateValue (" & Year(FecDesde) & "," & Month(FecDesde) & "," & Day(FecDesde) & ") AND " & _
        "DateValue (Val (Mid ({Query.FE_FECH_KARD}, 1, 4)), Val (Mid ({Query.FE_FECH_KARD}, 6, 2)), Val (Mid ({Query.FE_FECH_KARD}, 9, 2)))<DateValue (" & Year(FecHasta) & "," & Month(FecHasta) & "," & Day(FecHasta) & ") "
    strTITFECHA = "RANGO DE FECHAS: Del " & Me.txtFCDESDE.Text & " al " & Me.txtFCHASTA.Text
    strGRUPOS = strGRUPOS & IIf(Len(strGRUPOS) > 0, " AND ", "") & "{Query.TX_PARA_ARTI}='" & vTipo & "' AND {Query.TX_AFEC_DOCU}='S'"
''''''''
    Campos = "CD_CODI_GRUP, DE_DESC_GRUP"
    Desde = "GRUP_ARTICULO ORDER BY DE_DESC_GRUP"
    Condi = ""
    ReDim ArrRYG(1, 0)
    Result = LoadMulData(Desde, Campos, Condi, ArrRYG())
    On Error Resume Next
    For vContar = 0 To UBound(ArrRYG, 2)
        vclave = "G" & ArrRYG(0, vContar)
        Set UnGrupo = Nothing
        Set UnGrupo = CllResulta.Item(vclave)
        If UnGrupo Is Nothing Then
            Set UnGrupo = New ElGrupo
            UnGrupo.Codigo = ArrRYG(0, vContar)
            UnGrupo.Nombre = ArrRYG(1, vContar)
            CllResulta.Add UnGrupo, vclave
        End If
    Next
    On Error GoTo 0
''''''''
    Campos = ""
    For vContar = 0 To CInt(Me.txtNUMES.Text)
        If vContar = 0 Then
            Campos = vContar & " AS PERIODO, TX_CODI_BODE, TX_NOMB_BODE, CD_CODI_ARTI, NO_NOMB_ARTI, CD_CODI_GRUP, DE_DESC_GRUP, NU_ACTUNMR_KARD, NU_ACTUDNM_KARD, NU_COSTO_KARD"
            Desde = "IN_KARDEX, IN_BODEGA, ARTICULO, GRUP_ARTICULO"
            'Condi = "NU_AUTO_BODE_KARD=NU_AUTO_BODE AND " & _
                "NU_AUTO_ARTI_KARD=NU_AUTO_ARTI AND " & _
                "TX_CODI_BODE='" & vCualBodega & "' AND " & _
                "CD_GRUP_ARTI=CD_CODI_GRUP AND " & _
                "NU_AUTO_KARD IN (SELECT MAX(NU_AUTO_KARD) FROM IN_KARDEX WHERE FE_FECH_KARD<" & FFechaCon(DateAdd("d", 1, FecHasta)) & " GROUP BY NU_AUTO_BODE_KARD, NU_AUTO_ARTI_KARD) "
            Condi = "NU_AUTO_BODE_KARD=NU_AUTO_BODE AND " & _
                "NU_AUTO_ARTI_KARD=NU_AUTO_ARTI AND " & _
                "TX_CODI_BODE IN " & LasBodegas & " AND " & _
                "CD_GRUP_ARTI=CD_CODI_GRUP AND "
            'REOL MANTIS: 0000576
                If chkGRUPOS.Value = 0 Then Condi = Condi & "CD_CODI_GRUP IN " & LosGrupos & " AND "
                Condi = Condi & "NU_AUTO_KARD IN (SELECT MAX(NU_AUTO_KARD) FROM IN_KARDEX WHERE FE_FECH_KARD<" & FFechaCon(DateAdd("d", 1, FecHasta)) & " GROUP BY NU_AUTO_BODE_KARD, NU_AUTO_ARTI_KARD) "
        Else
            'Condi = Condi & "UNION SELECT " & vContar & " AS PERIODO, TX_CODI_BODE, TX_NOMB_BODE, CD_CODI_ARTI, NO_NOMB_ARTI, CD_CODI_GRUP, DE_DESC_GRUP, NU_ACTUNMR_KARD, NU_ACTUDNM_KARD, NU_COSTO_KARD " & _
                "FROM IN_KARDEX, IN_BODEGA, ARTICULO, GRUP_ARTICULO " & _
                "WHERE NU_AUTO_BODE_KARD=NU_AUTO_BODE AND " & _
                "NU_AUTO_ARTI_KARD=NU_AUTO_ARTI AND " & _
                "TX_CODI_BODE='" & vCualBodega & "' AND " & _
                "CD_GRUP_ARTI=CD_CODI_GRUP AND " & _
                "NU_AUTO_KARD IN (SELECT MAX(NU_AUTO_KARD) FROM IN_KARDEX WHERE FE_FECH_KARD<" & FFechaCon(DateAdd("m", -vContar + 1, FecDesde)) & " GROUP BY NU_AUTO_BODE_KARD, NU_AUTO_ARTI_KARD) "
             Condi = Condi & "UNION SELECT " & vContar & " AS PERIODO, TX_CODI_BODE, TX_NOMB_BODE, CD_CODI_ARTI, NO_NOMB_ARTI, CD_CODI_GRUP, DE_DESC_GRUP, NU_ACTUNMR_KARD, NU_ACTUDNM_KARD, NU_COSTO_KARD " & _
                "FROM IN_KARDEX, IN_BODEGA, ARTICULO, GRUP_ARTICULO " & _
                "WHERE NU_AUTO_BODE_KARD=NU_AUTO_BODE AND " & _
                "NU_AUTO_ARTI_KARD=NU_AUTO_ARTI AND " & _
                "TX_CODI_BODE IN " & LasBodegas & " AND " & _
                "CD_GRUP_ARTI=CD_CODI_GRUP AND "
            'REOL MANTIS: 0000576
                If chkGRUPOS.Value = 0 Then Condi = Condi & "CD_CODI_GRUP IN " & LosGrupos & " AND "
                Condi = Condi & "NU_AUTO_KARD IN (SELECT MAX(NU_AUTO_KARD) FROM IN_KARDEX WHERE FE_FECH_KARD<" & FFechaCon(DateAdd("m", -vContar + 1, FecDesde)) & " GROUP BY NU_AUTO_BODE_KARD, NU_AUTO_ARTI_KARD) "
        End If
    Next
    Condi = Condi & "ORDER BY TX_NOMB_BODE, NO_NOMB_ARTI, PERIODO"
''''''''
    ReDim ArrRYG(9, 0)
    Result = LoadMulData(Desde, Campos, Condi, ArrRYG())
    On Error Resume Next
    For vContar = 0 To UBound(ArrRYG, 2)
        vclave = "G" & ArrRYG(5, vContar)
        Set UnGrupo = Nothing
        Set UnGrupo = CllResulta.Item(vclave)
        If Not UnGrupo Is Nothing Then
            UnGrupo.TieneInfo = True
            'If CInt(ArrRYG(0, vContar)) = 0 Then UnGrupo.InventaFinal = UnGrupo.InventaFinal + (CLng(ArrRYG(7, vContar)) * CSng(ArrRYG(9, vContar)) / CLng(ArrRYG(8, vContar)))
            'If CInt(ArrRYG(0, vContar)) = 1 Then UnGrupo.AcumulaDebito = UnGrupo.AcumulaDebito + (CLng(ArrRYG(7, vContar)) * CSng(ArrRYG(9, vContar)) / CLng(ArrRYG(8, vContar)))
            If CInt(ArrRYG(0, vContar)) = 0 Then UnGrupo.InventaFinal = UnGrupo.InventaFinal + (CLng(ArrRYG(7, vContar)) * CSng(ArrRYG(9, vContar)) / CLng(ArrRYG(8, vContar))): UnGrupo.InventaFinal = CSng(Aplicacion.Formatear_Valor(UnGrupo.InventaFinal))  'JACC DEPURACION NOV 2008 'HRR M4537
            If CInt(ArrRYG(0, vContar)) = 1 Then UnGrupo.AcumulaDebito = UnGrupo.AcumulaDebito + (CLng(ArrRYG(7, vContar)) * CSng(ArrRYG(9, vContar)) / CLng(ArrRYG(8, vContar))): UnGrupo.AcumulaDebito = CSng(Aplicacion.Formatear_Valor(UnGrupo.AcumulaDebito)) 'JACC DEPURACION NOV 2008 'HRR M4537
            
            If Not (CInt(ArrRYG(0, vContar)) = 0) Then
                UnGrupo.cuenta = UnGrupo.cuenta + 1
                UnGrupo.AcumulaCredito = UnGrupo.AcumulaCredito + (CLng(ArrRYG(7, vContar)) * CSng(ArrRYG(9, vContar)) / CLng(ArrRYG(8, vContar)))
                UnGrupo.AcumulaCredito = CSng(Aplicacion.Formatear_Valor(UnGrupo.AcumulaCredito)) 'JACC DEPURACION NOV 2008 'HRR M4537
            End If
        End If
    Next
    On Error GoTo 0
'    For vContar = 1 To CllResulta.Count
'        Set UnGrupo = CllResulta.Item(vContar)
'        Me.wksRENYGAS.Cells(1, vContar).Value = UnGrupo.Codigo
'        Me.wksRENYGAS.Cells(2, vContar).Value = UnGrupo.Nombre
'        Me.wksRENYGAS.Cells(3, vContar).Value = UnGrupo.AcumulaDebito
'        Me.wksRENYGAS.Cells(4, vContar).Value = UnGrupo.AcumulaCredito / CInt(Me.txtNUMES.Text)
'    Next
''''''''
    Campos = "NU_AUTO_KARD AS AUTONUME, ORG.NU_AUTO_ENCA, BDOR.TX_CODI_BODE, BDOR.TX_NOMB_BODE, " & _
        "NU_AUTO_ARTI_KARD, CD_GRUP_ARTI, DE_DESC_GRUP, CD_CODI_ARTI, TX_PARA_ARTI, DOOR.TX_AFEC_DOCU, " & _
        "NO_NOMB_ARTI, DE_DESC_ARTI, FE_FECH_KARD, COOR.TX_NOMB_COMP, COOR.NU_COMP_COMP, NU_ENTRAD_KARD, NU_SALIDA_KARD, NU_COSTO_KARD, " & _
        "NU_MULT_KARD, NU_DIVI_KARD, NU_ACTUNMR_KARD, NU_ACTUDNM_KARD, BDDS.TX_CODI_BODE AS CD_CODI_TERC, BDDS.TX_NOMB_BODE AS NO_NOMB_TERC, DOOR.TX_INTE_DOCU, NU_VALOR_KARD "
    Desde = "IN_KARDEX, ARTICULO, GRUP_ARTICULO, IN_BODEGA AS BDOR, IN_BODEGA AS BDDS, IN_ENCABEZADO AS ORG, IN_ENCABEZADO AS MOD1, TERCERO, " & _
        "IN_COMPROBANTE As COOR, IN_COMPROBANTE As COMO, IN_DOCUMENTO As DOOR, IN_DOCUMENTO As DOMO"
    Condi = "(NU_AUTO_ARTI_KARD=NU_AUTO_ARTI) AND " & _
        "(CD_GRUP_ARTI=CD_CODI_GRUP) AND " & _
        "(NU_AUTO_BODE_KARD=BDOR.NU_AUTO_BODE) AND " & _
        "(ORG.NU_AUTO_BODEDST_ENCA=BDDS.NU_AUTO_BODE) AND " & _
        "(NU_AUTO_ORGCABE_KARD=ORG.NU_AUTO_ENCA) AND " & _
        "(ORG.NU_AUTO_COMP_ENCA=COOR.NU_AUTO_COMP) AND " & _
        "(NU_AUTO_MODCABE_KARD=MOD1.NU_AUTO_ENCA) AND " & _
        "(MOD1.NU_AUTO_COMP_ENCA=COMO.NU_AUTO_COMP) AND " & _
        "(ORG.NU_AUTO_DOCU_ENCA=DOOR.NU_AUTO_DOCU) AND " & _
        "(MOD1.NU_AUTO_DOCU_ENCA=DOMO.NU_AUTO_DOCU) AND " & _
        "(ORG.CD_CODI_TERC_ENCA=CD_CODI_TERC) AND "
    'Condi = Condi & "(TX_PARA_ARTI='V') AND " & _
        "(BDOR.TX_CODI_BODE='" & vCualBodega & "') AND " & _
        "(DOOR.TX_AFEC_DOCU='S') AND " & _
        "(FE_FECH_KARD>" & FFechaCon(DateAdd("d", -1, FecDesde)) & " AND FE_FECH_KARD<" & FFechaCon(DateAdd("d", 1, FecHasta)) & ") AND " & _
        "(NU_AUTO_BODE_KARD=ORG.NU_AUTO_BODEORG_ENCA) "
    Condi = Condi & "(TX_PARA_ARTI='V') AND " & _
        "(BDOR.TX_CODI_BODE IN " & LasBodegas & ") AND "
    'REOL MANTIS: 0000576
        If chkGRUPOS.Value = 0 Then Condi = Condi & "(GRUP_ARTICULO.CD_CODI_GRUP IN " & LosGrupos & ") AND "
        
    Condi = Condi & "(DOOR.TX_AFEC_DOCU='S') AND " & _
        "(FE_FECH_KARD>" & FFechaCon(DateAdd("d", -1, FecDesde)) & " AND FE_FECH_KARD<" & FFechaCon(DateAdd("d", 1, FecHasta)) & ") AND " & _
        "(NU_AUTO_BODE_KARD=ORG.NU_AUTO_BODEORG_ENCA) "
    Condi = Condi & "UNION SELECT NU_AUTO_KARD AS AUTONUME, ORG.NU_AUTO_ENCA, BDDS.TX_CODI_BODE, BDDS.TX_NOMB_BODE, " & _
        "NU_AUTO_ARTI_KARD, CD_GRUP_ARTI, DE_DESC_GRUP, CD_CODI_ARTI, TX_PARA_ARTI, DOOR.TX_AFEC_DOCU, " & _
        "NO_NOMB_ARTI, DE_DESC_ARTI, FE_FECH_KARD, COOR.TX_NOMB_COMP, COOR.NU_COMP_COMP, NU_ENTRAD_KARD, NU_SALIDA_KARD, NU_COSTO_KARD, " & _
        "NU_MULT_KARD, NU_DIVI_KARD, NU_ACTUNMR_KARD, NU_ACTUDNM_KARD, BDOR.TX_CODI_BODE AS CD_CODI_TERC, BDOR.TX_NOMB_BODE AS NO_NOMB_TERC, DOOR.TX_INTE_DOCU, NU_VALOR_KARD " & _
        "FROM IN_KARDEX, ARTICULO, GRUP_ARTICULO, IN_BODEGA AS BDOR, IN_BODEGA AS BDDS, IN_ENCABEZADO AS ORG, IN_ENCABEZADO AS MOD1, TERCERO, " & _
        "IN_COMPROBANTE As Coor, IN_COMPROBANTE As COMO, IN_DOCUMENTO As DOOR, IN_DOCUMENTO As DOMO " & _
        "WHERE (NU_AUTO_ARTI_KARD=NU_AUTO_ARTI) AND " & _
        "(CD_GRUP_ARTI=CD_CODI_GRUP) AND " & _
        "(ORG.NU_AUTO_BODEORG_ENCA=BDOR.NU_AUTO_BODE) AND " & _
        "(ORG.NU_AUTO_BODEDST_ENCA=BDDS.NU_AUTO_BODE) AND " & _
        "(NU_AUTO_ORGCABE_KARD=ORG.NU_AUTO_ENCA) AND " & _
        "(ORG.NU_AUTO_COMP_ENCA=COOR.NU_AUTO_COMP) AND " & _
        "(NU_AUTO_MODCABE_KARD=MOD1.NU_AUTO_ENCA) AND " & _
        "(MOD1.NU_AUTO_COMP_ENCA=COMO.NU_AUTO_COMP) AND " & _
        "(ORG.NU_AUTO_DOCU_ENCA=DOOR.NU_AUTO_DOCU) AND " & _
        "(MOD1.NU_AUTO_DOCU_ENCA=DOMO.NU_AUTO_DOCU) AND " & _
        "(ORG.CD_CODI_TERC_ENCA=CD_CODI_TERC) AND "
    'Condi = Condi & "(TX_PARA_ARTI='V') AND " & _
        "(BDOR.TX_CODI_BODE='" & vCualBodega & "') AND " & _
        "(DOOR.TX_AFEC_DOCU='S') AND " & _
        "(FE_FECH_KARD>" & FFechaCon(DateAdd("d", -1, FecDesde)) & " AND FE_FECH_KARD<" & FFechaCon(DateAdd("d", 1, FecHasta)) & ") AND " & _
        "(NOT NU_AUTO_BODE_KARD=ORG.NU_AUTO_BODEORG_ENCA)"
    Condi = Condi & "(TX_PARA_ARTI='V') AND " & _
        "(BDOR.TX_CODI_BODE IN " & LasBodegas & ") AND "
    'REOL MANTIS: 0000576
        If chkGRUPOS.Value = 0 Then Condi = Condi & "(GRUP_ARTICULO.CD_CODI_GRUP IN " & LosGrupos & ") AND "
    
    Condi = Condi & "(DOOR.TX_AFEC_DOCU='S') AND " & _
        "(FE_FECH_KARD>" & FFechaCon(DateAdd("d", -1, FecDesde)) & " AND FE_FECH_KARD<" & FFechaCon(DateAdd("d", 1, FecHasta)) & ") AND " & _
        "(NOT NU_AUTO_BODE_KARD=ORG.NU_AUTO_BODEORG_ENCA)"
    Condi = Condi & "UNION SELECT NU_AUTO_KARD AS AUTONUME, ORG.NU_AUTO_ENCA, BDOR.TX_CODI_BODE, BDOR.TX_NOMB_BODE, " & _
        "NU_AUTO_ARTI_KARD, CD_GRUP_ARTI, DE_DESC_GRUP, CD_CODI_ARTI, TX_PARA_ARTI, DOOR.TX_AFEC_DOCU, " & _
        "NO_NOMB_ARTI, DE_DESC_ARTI, FE_FECH_KARD, COOR.TX_NOMB_COMP, COOR.NU_COMP_COMP, NU_ENTRAD_KARD, NU_SALIDA_KARD, NU_COSTO_KARD, " & _
        "NU_MULT_KARD , NU_DIVI_KARD, NU_ACTUNMR_KARD, NU_ACTUDNM_KARD, CD_CODI_TERC, NO_NOMB_TERC, DOOR.TX_INTE_DOCU, NU_VALOR_KARD " & _
        "FROM IN_KARDEX, ARTICULO, GRUP_ARTICULO, IN_BODEGA AS BDOR, IN_ENCABEZADO AS ORG, IN_ENCABEZADO AS MOD1, TERCERO, " & _
        "IN_COMPROBANTE As COOR, IN_COMPROBANTE As COMO, IN_DOCUMENTO As DOOR, IN_DOCUMENTO As DOMO " & _
        "WHERE (NU_AUTO_ARTI_KARD=NU_AUTO_ARTI) AND " & _
        "(CD_GRUP_ARTI=CD_CODI_GRUP) AND " & _
        "(ORG.NU_AUTO_BODEORG_ENCA=BDOR.NU_AUTO_BODE) AND " & _
        "(ORG.NU_AUTO_BODEDST_ENCA=0) AND " & _
        "(NU_AUTO_ORGCABE_KARD=ORG.NU_AUTO_ENCA) AND " & _
        "(ORG.NU_AUTO_COMP_ENCA=COOR.NU_AUTO_COMP) AND " & _
        "(NU_AUTO_MODCABE_KARD=MOD1.NU_AUTO_ENCA) AND " & _
        "(MOD1.NU_AUTO_COMP_ENCA=COMO.NU_AUTO_COMP) AND " & _
        "(ORG.NU_AUTO_DOCU_ENCA=DOOR.NU_AUTO_DOCU) AND " & _
        "(MOD1.NU_AUTO_DOCU_ENCA=DOMO.NU_AUTO_DOCU) AND "
    'Condi = Condi & "(TX_PARA_ARTI='V') AND " & _
        "(BDOR.TX_CODI_BODE='" & vCualBodega & "') AND " & _
        "(DOOR.TX_AFEC_DOCU='S') AND " & _
        "(FE_FECH_KARD>" & FFechaCon(DateAdd("d", -1, FecDesde)) & " AND FE_FECH_KARD<" & FFechaCon(DateAdd("d", 1, FecHasta)) & ") AND " & _
        "(ORG.CD_CODI_TERC_ENCA=CD_CODI_TERC) ORDER BY AUTONUME"
    Condi = Condi & "(TX_PARA_ARTI='V') AND " & _
        "(BDOR.TX_CODI_BODE IN " & LasBodegas & ") AND "
    'REOL MANTIS: 0000576
        If chkGRUPOS.Value = 0 Then Condi = Condi & "(GRUP_ARTICULO.CD_CODI_GRUP IN " & LosGrupos & ") AND "
    
    Condi = Condi & "(DOOR.TX_AFEC_DOCU='S') AND " & _
        "(FE_FECH_KARD>" & FFechaCon(DateAdd("d", -1, FecDesde)) & " AND FE_FECH_KARD<" & FFechaCon(DateAdd("d", 1, FecHasta)) & ") AND " & _
        "(ORG.CD_CODI_TERC_ENCA=CD_CODI_TERC) ORDER BY AUTONUME"
''''''''
    ReDim ArrRYG(25, 0)
    Result = LoadMulData(Desde, Campos, Condi, ArrRYG())
    On Error Resume Next
    For vContar = 0 To UBound(ArrRYG, 2)
        vclave = "G" & ArrRYG(5, vContar)
        Set UnGrupo = Nothing
        Set UnGrupo = CllResulta.Item(vclave)
        If Not UnGrupo Is Nothing Then
            UnGrupo.TieneInfo = True
            UnGrupo.AcumulaEntradaCosto = UnGrupo.AcumulaEntradaCosto + (CLng(ArrRYG(15, vContar)) * CLng(ArrRYG(17, vContar)) * CSng(ArrRYG(18, vContar)) / CLng(ArrRYG(19, vContar)))
            UnGrupo.AcumulaSalidaCosto = UnGrupo.AcumulaSalidaCosto + (CLng(ArrRYG(16, vContar)) * CLng(ArrRYG(17, vContar)) * CSng(ArrRYG(18, vContar)) / CLng(ArrRYG(19, vContar)))
            UnGrupo.AcumulaEntradaVenta = UnGrupo.AcumulaEntradaVenta + (CLng(ArrRYG(15, vContar)) * CLng(ArrRYG(25, vContar)) * CSng(ArrRYG(18, vContar)) / CLng(ArrRYG(19, vContar)))
            UnGrupo.AcumulaSalidaVenta = UnGrupo.AcumulaSalidaVenta + (CLng(ArrRYG(16, vContar)) * CLng(ArrRYG(25, vContar)) * CSng(ArrRYG(18, vContar)) / CLng(ArrRYG(19, vContar)))
            
            ''JACC DEPURACION NOV 2008 'HRR M4537
            UnGrupo.AcumulaEntradaCosto = CSng(Aplicacion.Formatear_Valor(UnGrupo.AcumulaEntradaCosto))
            UnGrupo.AcumulaSalidaCosto = CSng(Aplicacion.Formatear_Valor(UnGrupo.AcumulaSalidaCosto))
            UnGrupo.AcumulaEntradaVenta = CSng(Aplicacion.Formatear_Valor(UnGrupo.AcumulaEntradaVenta))
            UnGrupo.AcumulaSalidaVenta = CSng(Aplicacion.Formatear_Valor(UnGrupo.AcumulaSalidaVenta))
            'JACC DEPURACION NOV 2008 'HRR M4537
            
        End If
    Next
    On Error GoTo 0
''''''''''''''''
    DocumentoVentas = "(" & FacturaVenta & "," & DevolucionFacturaVenta & "," & AnulaFacturaVenta
    DocumentoVentas = DocumentoVentas & "," & BajaConsumo & "," & DevolucionBaja & "," & AnulaBaja
    DocumentoVentas = DocumentoVentas & "," & Salida & "," & DevolucionSalida & "," & AnulaSalida & ")"
    
    DocumentoCompras = "(" & Entrada & "," & DevolucionEntrada & "," & AnulaEntrada
    DocumentoCompras = DocumentoCompras & "," & AprovechaDonacion & "," & DevolucionAprovecha & "," & AnulaAprovecha & ")"
    strGRUPOS = "": strBODEGAS = ""
    If Me.chkGRUPOS.Value = 0 Then
        For Each Renglon In Me.lstGRUPOS.ListItems
            If Renglon.Selected Then strGRUPOS = strGRUPOS & IIf(Len(strGRUPOS) > 0, ",", "") & "'" & Renglon.ListSubItems("COAR").Text & "'"
        Next
    End If
    For Each Renglon In Me.lstBODEGAS.ListItems
'        If Renglon.Selected Then strBODEGAS = strBODEGAS & IIf(Len(strBODEGAS) > 0, ",", "") & "'" & Renglon.ListSubItems("COD").Text & "'"
        If Renglon.Selected Then strBODEGAS = strBODEGAS & IIf(Len(strBODEGAS) > 0, ",", "") & Renglon.Tag
    Next
    If Len(strGRUPOS) > 0 Then strGRUPOS = " AND CD_GRUP_ARTI IN (" & strGRUPOS & ")"
    strBODEGAS = "(" & strBODEGAS & ")"
'    Condi = "SELECT NU_AUTO_ENCA FROM IN_ENCABEZADO, IN_DOCUMENTO WHERE " & _
'        "NU_AUTO_DOCU_ENCA=NU_AUTO_DOCU AND NU_AUTO_DOCU IN " & DocumentoVentas & _
'        " AND (FE_CREA_ENCA>'" & FormatDateTime(DateAdd("d", -1, FecDesde), vbShortDate) & "' AND FE_CREA_ENCA<'" & FormatDateTime(DateAdd("d", 1, FecHasta), vbShortDate) & "')" & _
'        " AND NU_AUTO_BODEORG_ENCA IN " & strBODEGAS
    Condi = "SELECT NU_AUTO_ENCA FROM IN_ENCABEZADO, IN_DOCUMENTO WHERE " & _
        "NU_AUTO_DOCU_ENCA=NU_AUTO_DOCU AND NU_AUTO_DOCU IN " & DocumentoVentas & _
        " AND (FE_CREA_ENCA>" & FFechaCon(DateAdd("d", -1, FecDesde)) & " AND FE_CREA_ENCA<" & FFechaCon(DateAdd("d", 1, FecHasta)) & ")" & _
        " AND NU_AUTO_BODEORG_ENCA IN " & strBODEGAS
    Campos = "TX_CODI_COMP, TX_NOMB_COMP, NU_AUTO_ARTI_KARD, CD_GRUP_ARTI, NU_ENTRAD_KARD, NU_SALIDA_KARD, NU_MULT_KARD, NU_DIVI_KARD, " & _
        "NU_COSTO_KARD, NU_VALOR_KARD, NU_AUTO_UNVE_KARD, TX_NOMB_UNVE, CD_CODI_ARTI, NO_NOMB_ARTI"
    Campos = Campos & ",NU_AUTO_ORGCABE_KARD"
    Desde = "IN_KARDEX, IN_UNDVENTA, ARTICULO, IN_ENCABEZADO, IN_COMPROBANTE"
    Condi = " NU_AUTO_UNVE_KARD=NU_AUTO_UNVE" & _
        " AND NU_AUTO_ORGCABE_KARD=NU_AUTO_ENCA" & _
        " AND NU_AUTO_COMP_ENCA=NU_AUTO_COMP" & _
        " AND NU_AUTO_ARTI_KARD=NU_AUTO_ARTI" & _
        " AND NU_AUTO_ORGCABE_KARD IN (" & Condi & ")" & strGRUPOS
    ReDim ArrRYG(14, 0)
    Result = LoadMulData(Desde, Campos, Condi, ArrRYG())
    On Error Resume Next
    For vContar = 0 To UBound(ArrRYG, 2)
        vclave = "G" & ArrRYG(3, vContar)
        Set UnGrupo = Nothing
        Set UnGrupo = CllResulta.Item(vclave)
        If Not UnGrupo Is Nothing Then
            UnGrupo.TieneInfo = True
            'UnGrupo.AcumulaVentasCosto = UnGrupo.AcumulaVentasCosto + ((CLng(ArrRYG(5, vContar)) - CLng(ArrRYG(4, vContar))) * CLng(ArrRYG(6, vContar)) * CSng(ArrRYG(8, vContar)) / CLng(ArrRYG(7, vContar)))
            UnGrupo.AcumulaVentasCosto = UnGrupo.AcumulaVentasCosto + (Abs(CLng(ArrRYG(5, vContar)) - CLng(ArrRYG(4, vContar))) * CLng(ArrRYG(6, vContar)) * CSng(ArrRYG(8, vContar)) / CLng(ArrRYG(7, vContar)))
            'UnGrupo.AcumulaVentasVenta = UnGrupo.AcumulaVentasVenta + ((CLng(ArrRYG(5, vContar)) - CLng(ArrRYG(4, vContar))) * CLng(ArrRYG(6, vContar)) * CSng(ArrRYG(9, vContar)) / CLng(ArrRYG(7, vContar)))
            If CSng(ArrRYG(9, vContar)) = 0 Then
               Campos = "NU_VALOR_DETA"
               Condi = "NU_AUTO_ORGCABE_DETA=" & ArrRYG(14, vContar) & " AND NU_AUTO_ARTI_DETA= " & ArrRYG(2, vContar)
               Dim MyArr() As Variant
               ReDim MyArr(0)
               Result = LoadData("IN_DETALLE", Campos, Condi, MyArr)
               If Result <> FAIL And Encontro Then
                   UnGrupo.AcumulaVentasVenta = UnGrupo.AcumulaVentasVenta + ((CLng(ArrRYG(5, vContar)) - CLng(ArrRYG(4, vContar))) * CLng(ArrRYG(6, vContar)) * CSng(MyArr(0)) / CLng(ArrRYG(7, vContar)))
               End If
            Else
               UnGrupo.AcumulaVentasVenta = UnGrupo.AcumulaVentasVenta + ((CLng(ArrRYG(5, vContar)) - CLng(ArrRYG(4, vContar))) * CLng(ArrRYG(6, vContar)) * CSng(ArrRYG(9, vContar)) / CLng(ArrRYG(7, vContar)))
            End If
            UnGrupo.AcumulaVentasVenta = CSng(Aplicacion.Formatear_Valor(UnGrupo.AcumulaVentasVenta)) 'JACC DEPURACION NOV 2008 'HRR M4537
        End If
    Next
    On Error GoTo 0
''''''''''''''''
    vFil = 2
    Me.FrmParam.Visible = False
    Me.wksRENYGAS.Visible = True
    Me.cmdBACK.Visible = True
    Me.cmdSAVE.Visible = True
    Me.wksRENYGAS.Cells(1 + vFil, 1).Value = "C�digo"
    Me.wksRENYGAS.Cells(2 + vFil, 1).Value = "Nombre"
    Me.wksRENYGAS.Cells(3 + vFil, 1).Value = "Inventario Inicial"
    Me.wksRENYGAS.Cells(4 + vFil, 1).Value = "Entradas Costo"
    Me.wksRENYGAS.Cells(5 + vFil, 1).Value = "Salidas Costo"
    Me.wksRENYGAS.Cells(6 + vFil, 1).Value = "Inventario Final"
    Me.wksRENYGAS.Cells(7 + vFil, 1).Value = "Promedio"
    Me.wksRENYGAS.Cells(8 + vFil, 1).Value = "Indice de Rotaci�n"
    Me.wksRENYGAS.Cells(9 + vFil, 1).Value = "Rotaci�n"
    Me.wksRENYGAS.Cells(10 + vFil, 1).Value = "Ventas Costo"
    Me.wksRENYGAS.Cells(11 + vFil, 1).Value = "Ventas P�blico"
    Me.wksRENYGAS.Cells(12 + vFil, 1).Value = "Utilidad"
    Me.wksRENYGAS.Cells(13 + vFil, 1).Value = "% de Utilidad"
'    Me.wksRENYGAS.Cells(7+vfil, 1).Value = "Entradas Venta"
'    Me.wksRENYGAS.Cells(8+vfil, 1).Value = "Salidas Venta"
    vCol = 2
    Me.wksRENYGAS.Cells(1, 1).Value = "Rentas y Gastos"
    Me.wksRENYGAS.Cells(2, 1).Value = "Del:" & Me.txtFCHASTA.Text & ", al:" & Me.txtFCDESDE.Text & ", " & Me.lstBODEGAS.SelectedItem.Text
    For vContar = 1 To CllResulta.Count
        Set UnGrupo = CllResulta.Item(vContar)
        If UnGrupo.TieneInfo Then
            Me.wksRENYGAS.Cells(1 + vFil, vCol).Value = UnGrupo.Codigo
            Me.wksRENYGAS.Cells(2 + vFil, vCol).Value = UnGrupo.Nombre
            Me.wksRENYGAS.Cells(3 + vFil, vCol).Value = UnGrupo.AcumulaDebito 'Inventario Inicial
            Me.wksRENYGAS.Cells(4 + vFil, vCol).Value = UnGrupo.AcumulaEntradaCosto 'Entradas Costo
            Me.wksRENYGAS.Cells(5 + vFil, vCol).Value = UnGrupo.AcumulaSalidaCosto 'Salidas Costo
            'Me.wksRENYGAS.Cells(6 + vFil, vCol).Value = UnGrupo.InventaFinal 'Inventario Final
            Me.wksRENYGAS.Cells(6 + vFil, vCol).Value = (UnGrupo.AcumulaDebito + UnGrupo.AcumulaEntradaCosto) - UnGrupo.AcumulaSalidaCosto 'Inventario Final
            Me.wksRENYGAS.Cells(7 + vFil, vCol).Value = UnGrupo.AcumulaCredito / CInt(Me.txtNUMES.Text) 'Promedio
            Me.wksRENYGAS.Cells(8 + vFil, vCol).Formula = "=" & Me.wksRENYGAS.Cells(5 + vFil, vCol).Address & "/" & Me.wksRENYGAS.Cells(7 + vFil, vCol).Address 'Indice de Rotaci�n
'            Me.wksRENYGAS.Cells(8+vfil, vCol).Value = 0
            Me.wksRENYGAS.Cells(9 + vFil, vCol).Formula = "=30/" & Me.wksRENYGAS.Cells(8 + vFil, vCol).Address 'Rotaci�n
            Me.wksRENYGAS.Cells(10 + vFil, vCol).Value = UnGrupo.AcumulaVentasCosto 'Ventas Costo
            Me.wksRENYGAS.Cells(11 + vFil, vCol).Value = UnGrupo.AcumulaVentasVenta 'Ventas P�blico
            Me.wksRENYGAS.Cells(12 + vFil, vCol).Formula = "=" & Me.wksRENYGAS.Cells(11 + vFil, vCol).Address & "-" & Me.wksRENYGAS.Cells(10 + vFil, vCol).Address 'Utilidad
            Me.wksRENYGAS.Cells(13 + vFil, vCol).Formula = "=" & Me.wksRENYGAS.Cells(11 + vFil, vCol).Address & "/" & Me.wksRENYGAS.Cells(10 + vFil, vCol).Address '% de Utilidad
'            Me.wksRENYGAS.Cells(9+vfil, vCol).Value = 0
            Me.wksRENYGAS.Cells(3 + vFil, vCol).NumberFormat = "#,##0"
            Me.wksRENYGAS.Cells(4 + vFil, vCol).NumberFormat = "#,##0"
            Me.wksRENYGAS.Cells(5 + vFil, vCol).NumberFormat = "#,##0"
            Me.wksRENYGAS.Cells(6 + vFil, vCol).NumberFormat = "#,##0"
            Me.wksRENYGAS.Cells(7 + vFil, vCol).NumberFormat = "#,##0"
            Me.wksRENYGAS.Cells(8 + vFil, vCol).NumberFormat = "#,##0.00"
            Me.wksRENYGAS.Cells(9 + vFil, vCol).NumberFormat = "#,##0.00"
            Me.wksRENYGAS.Cells(10 + vFil, vCol).NumberFormat = "#,##0"
            Me.wksRENYGAS.Cells(11 + vFil, vCol).NumberFormat = "#,##0"
            Me.wksRENYGAS.Cells(12 + vFil, vCol).NumberFormat = "#,##0"
            Me.wksRENYGAS.Cells(13 + vFil, vCol).NumberFormat = "0.00%"
'            Me.wksRENYGAS.Cells(7+vfil, vCol).Value = UnGrupo.AcumulaEntradaVenta
'            Me.wksRENYGAS.Cells(8+vfil, vCol).Value = UnGrupo.AcumulaSalidaVenta
            vCol = vCol + 1
        End If
    Next
    On Error GoTo 0
End Sub

Private Sub cmdSAVE_Click()
    Dim Renglon As MSComctlLib.ListItem
    For Each Renglon In Me.lstBODEGAS.ListItems
        If Renglon.Selected Then Me.dlgARCHIVO.filename = "RentasYGastos " & Renglon.Text
    Next
    If Me.dlgARCHIVO.InitDir = "" Then Me.dlgARCHIVO.InitDir = App.Path
    Me.dlgARCHIVO.Filter = "Archivos Excel (*.xls)|*.xls"
    Me.dlgARCHIVO.CancelError = True
    On Error GoTo CANCELO
    Me.dlgARCHIVO.ShowSave
    If Me.dlgARCHIVO.filename <> "" Then If MsgBox("Guardar:" & Me.dlgARCHIVO.filename, vbYesNo) = vbYes Then Me.wksRENYGAS.ActiveSheet.Export Me.dlgARCHIVO.filename, ssExportActionNone
CANCELO:
    On Error GoTo 0
End Sub

Private Sub Form_Load()
'    Call Main
    Call CenterForm(MDI_Inventarios, Me)
    Me.FrmParam.Visible = True
    Me.wksRENYGAS.Visible = False
    Me.cmdBACK.Visible = False
    Me.cmdSAVE.Visible = False
    Dueno.IniXNit (Dueno.Nit)
    Me.cboTIPO.Clear
    Me.cboTIPO.AddItem "Para la Venta"
    Me.cboTIPO.AddItem "Para El Consumo"
    NumEnCombo = Me.cboTIPO.ListIndex
    Me.wksRENYGAS.TitleBar.Caption = "Rentas y Gastos"
    If Len(FormatDateTime(Date, vbShortDate)) = 8 Then
        Me.txtFCDESDE.Mask = "##/##/##"
        Me.txtFCHASTA.Mask = "##/##/##"
    Else
        Me.txtFCDESDE.Mask = "##/##/####"
        Me.txtFCHASTA.Mask = "##/##/####"
    End If
    Me.txtFCDESDE.Text = FormatDateTime(DateAdd("d", -(Day(Date) - 1), Date), vbShortDate)
    Me.txtFCHASTA.Text = FormatDateTime(DateAdd("d", -1, DateAdd("M", 1, DateAdd("d", -(Day(Date) - 1), Date))), vbShortDate)
'    Me.chkFCRANGO.Value = False
'    Me.txtFCDESDE.Enabled = False
'    Me.txtFCHASTA.Enabled = False
    Me.lstBODEGAS.ListItems.Clear
    Me.lstBODEGAS.Checkboxes = False
    Me.lstBODEGAS.MultiSelect = True
    Me.lstBODEGAS.HideSelection = False
    Me.lstBODEGAS.ColumnHeaders.Clear
    Me.lstBODEGAS.ColumnHeaders.Add , "NOM", "Nombre de la Bodega", 0.66 * Me.lstBODEGAS.Width
    Me.lstBODEGAS.ColumnHeaders.Add , "COD", "C�digo", 0.34 * Me.lstBODEGAS.Width
    
    Me.lstGRUPOS.ListItems.Clear
    Me.lstGRUPOS.Checkboxes = False
    Me.lstGRUPOS.MultiSelect = True
    Me.lstGRUPOS.HideSelection = False
    Me.lstGRUPOS.Width = 10500
    Me.lstGRUPOS.ColumnHeaders.Clear
    Me.lstGRUPOS.ColumnHeaders.Add , "NOAR", "Nombre del Grupo", 0.4 * Me.lstGRUPOS.Width
    Me.lstGRUPOS.ColumnHeaders.Add , "COAR", "C�digo", 0.15 * Me.lstGRUPOS.Width
FALLO:
End Sub

Private Sub cboTIPO_LostFocus()
    If NumEnCombo = Me.cboTIPO.ListIndex Then Exit Sub
    Me.lstBODEGAS.ListItems.Clear
    Me.lstGRUPOS.ListItems.Clear
    Me.pgbLLEVO.Value = 0
    Me.pgbLLEVO.Visible = True
    Select Case Me.cboTIPO.ListIndex
    Case Is = -1
        vTipo = "0"
    Case Is = 0
        vTipo = "V"
    Case Is = 1
        vTipo = "C"
    End Select
    Campos = "NU_AUTO_BODE, NU_AUTO_BODE_BODE, TX_CODI_BODE, TX_NOMB_BODE, CD_CODI_CECO_BODE, TX_VENTA_BODE"
    Desde = "IN_BODEGA"
    Condi = "TX_VENTA_BODE='" & vTipo & "' ORDER BY TX_NOMB_BODE"
    ReDim ArrUXB(5, 0)
    Result = LoadMulData(Desde, Campos, Condi, ArrUXB())
    If Result = FAIL Then GoTo FALLO
    On Error GoTo SIGUI
    For CnTdr = 0 To UBound(ArrUXB, 2)
        vclave = "B" & ArrUXB(2, CnTdr)
        Me.lstBODEGAS.ListItems.Add , vclave, ArrUXB(3, CnTdr)
        Me.lstBODEGAS.ListItems(vclave).ListSubItems.Add , "COD", ArrUXB(2, CnTdr)
        Me.lstBODEGAS.ListItems(vclave).Tag = CStr(CLng(ArrUXB(0, CnTdr)))
SIGUI:
    Next
    On Error GoTo 0
    Campos = "CD_CODI_GRUP, DE_DESC_GRUP"
    
    Desde = "GRUP_ARTICULO"
    ReDim ArrUXB(2, 0)
    Result = LoadMulData(Desde, Campos, "", ArrUXB())
    If Result = FAIL Then GoTo FALLO
    If Not Encontro Then GoTo NOENC
    Me.pgbLLEVO.Min = 0
    Me.pgbLLEVO.Value = 0
    Me.pgbLLEVO.Max = UBound(ArrUXB, 2) + 1
    
    For CnTdr = 0 To UBound(ArrUXB, 2)
        Me.pgbLLEVO.Value = CnTdr
        Me.lstGRUPOS.ListItems.Add , "G" & ArrUXB(0, CnTdr), Trim(ArrUXB(1, CnTdr))
        Me.lstGRUPOS.ListItems("G" & ArrUXB(0, CnTdr)).Tag = ArrUXB(0, CnTdr)
        
        Me.lstGRUPOS.ListItems("G" & ArrUXB(0, CnTdr)).ListSubItems.Add , "COAR", Trim(ArrUXB(0, CnTdr))
    Next
NOENC:
FALLO:
    Me.pgbLLEVO.Visible = False
End Sub

Private Sub lstGRUPOS_ColumnClick(ByVal ColumnHeader As MSComctlLib.ColumnHeader)
    Me.lstGRUPOS.SortKey = ColumnHeader.Index - 1
    Me.lstGRUPOS.Sorted = True
End Sub

Private Sub txtFCDESDE_LostFocus()
    If IsDate(Me.txtFCDESDE.Text) Then Exit Sub
    Me.txtFCDESDE.Text = FormatDateTime(DateAdd("d", -(Day(Date) - 1), Date), vbShortDate)
    Me.txtFCDESDE.SetFocus
End Sub

Private Sub txtFCHASTA_LostFocus()
    'smdl m1876
    If IsDate(Me.txtFCHASTA.Text) Then
       If DateDiff("d", CDate(Me.txtFCDESDE.Text), CDate(Me.txtFCHASTA)) < 0 Then
          Call Mensaje1("La fecha final debe ser mayor a la inicial.", 3)
          Me.txtFCHASTA.SetFocus
       End If
    Else
    'smdl m1876
       Me.txtFCHASTA.Text = FormatDateTime(DateAdd("d", -1, DateAdd("M", 1, DateAdd("d", -(Day(Date) - 1), Date))), vbShortDate)
       Me.txtFCHASTA.SetFocus
    End If
End Sub



