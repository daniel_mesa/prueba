VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Object = "{C932BA88-4374-101B-A56C-00AA003668DC}#1.1#0"; "MSMASK32.OCX"
Object = "{0BA686C6-F7D3-101A-993E-0000C0EF6F5E}#1.0#0"; "THREED32.OCX"
Begin VB.Form frmPrmCOMPROBA 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Comprobantes"
   ClientHeight    =   6390
   ClientLeft      =   45
   ClientTop       =   435
   ClientWidth     =   10725
   Icon            =   "frmPrmCOMPROBA.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   6390
   ScaleWidth      =   10725
   Begin MSComctlLib.Toolbar tlbACCIONES 
      Align           =   1  'Align Top
      Height          =   660
      Left            =   0
      TabIndex        =   6
      Top             =   0
      Visible         =   0   'False
      Width           =   10725
      _ExtentX        =   18918
      _ExtentY        =   1164
      ButtonWidth     =   1799
      ButtonHeight    =   1005
      AllowCustomize  =   0   'False
      Appearance      =   1
      _Version        =   393216
      BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
         NumButtons      =   4
         BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Enabled         =   0   'False
            Caption         =   "&Adicionar"
            Key             =   "ADD"
            Object.ToolTipText     =   "Adicionar"
         EndProperty
         BeginProperty Button2 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Enabled         =   0   'False
            Caption         =   "&Borrar"
            Key             =   "DEL"
         EndProperty
         BeginProperty Button3 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Enabled         =   0   'False
            Caption         =   "&Modificar"
            Key             =   "CHA"
         EndProperty
         BeginProperty Button4 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Enabled         =   0   'False
            Caption         =   "&Cancelar"
            Key             =   "CAN"
         EndProperty
      EndProperty
      BorderStyle     =   1
   End
   Begin MSComctlLib.TreeView arbConsec 
      Height          =   4935
      Left            =   480
      TabIndex        =   0
      Top             =   960
      Width           =   4335
      _ExtentX        =   7646
      _ExtentY        =   8705
      _Version        =   393217
      HideSelection   =   0   'False
      Indentation     =   529
      LabelEdit       =   1
      LineStyle       =   1
      Style           =   7
      FullRowSelect   =   -1  'True
      Appearance      =   1
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin VB.Frame Frame1 
      Caption         =   "Informaci�on del Consecutivo"
      Height          =   4335
      Left            =   5280
      TabIndex        =   1
      Top             =   960
      Width           =   5175
      Begin MSMask.MaskEdBox txtPrefijo 
         Height          =   315
         Left            =   1680
         TabIndex        =   11
         Top             =   2185
         Width           =   855
         _ExtentX        =   1508
         _ExtentY        =   556
         _Version        =   393216
         PromptInclude   =   0   'False
         MaxLength       =   2
         PromptChar      =   "_"
      End
      Begin VB.ComboBox cboROMPE 
         Height          =   315
         Left            =   1680
         TabIndex        =   15
         Text            =   "cboROMPE"
         Top             =   3900
         Width           =   1215
      End
      Begin VB.ComboBox cboNOSI 
         Height          =   315
         Left            =   1680
         TabIndex        =   14
         Text            =   "cboNOSI"
         Top             =   3470
         Width           =   1215
      End
      Begin VB.ComboBox cboDocum 
         Height          =   315
         Left            =   1680
         TabIndex        =   10
         Text            =   "Documentos"
         Top             =   1750
         Width           =   3255
      End
      Begin VB.ComboBox cboQuien 
         Enabled         =   0   'False
         Height          =   315
         Left            =   1680
         TabIndex        =   13
         Text            =   "cboQuien"
         Top             =   3040
         Width           =   3255
      End
      Begin MSMask.MaskEdBox txtNombre 
         Height          =   795
         Left            =   1680
         TabIndex        =   9
         Top             =   855
         Width           =   3255
         _ExtentX        =   5741
         _ExtentY        =   1402
         _Version        =   393216
         PromptInclude   =   0   'False
         MaxLength       =   50
         PromptChar      =   "_"
      End
      Begin MSMask.MaskEdBox txtNumActual 
         Height          =   315
         Left            =   1680
         TabIndex        =   12
         Top             =   2615
         Width           =   1215
         _ExtentX        =   2143
         _ExtentY        =   556
         _Version        =   393216
         PromptInclude   =   0   'False
         MaxLength       =   7
         PromptChar      =   "_"
      End
      Begin MSMask.MaskEdBox txtCodigo 
         Height          =   315
         Left            =   1680
         TabIndex        =   8
         Top             =   360
         Width           =   1935
         _ExtentX        =   3413
         _ExtentY        =   556
         _Version        =   393216
         PromptInclude   =   0   'False
         MaxLength       =   10
         PromptChar      =   "_"
      End
      Begin VB.Label Label2 
         Alignment       =   1  'Right Justify
         Caption         =   "C�digo:"
         Height          =   315
         Left            =   120
         TabIndex        =   22
         Top             =   360
         Width           =   1500
      End
      Begin VB.Label Label8 
         Alignment       =   1  'Right Justify
         Caption         =   "Rompe D/dencia:"
         Height          =   315
         Left            =   120
         TabIndex        =   21
         Top             =   3900
         Width           =   1500
      End
      Begin VB.Label Label7 
         Alignment       =   1  'Right Justify
         Caption         =   "Requiere A/zaci�n:"
         Height          =   315
         Left            =   120
         TabIndex        =   20
         Top             =   3470
         Width           =   1500
      End
      Begin VB.Label Label6 
         Alignment       =   1  'Right Justify
         Caption         =   "Quien la Anula:"
         Height          =   315
         Left            =   120
         TabIndex        =   7
         Top             =   3040
         Width           =   1500
      End
      Begin VB.Label Label3 
         Alignment       =   1  'Right Justify
         Caption         =   "Tipo de Documento:"
         Height          =   315
         Left            =   120
         TabIndex        =   5
         Top             =   1750
         Width           =   1500
      End
      Begin VB.Label Label4 
         Alignment       =   1  'Right Justify
         Caption         =   "Prefijo del Consec.:"
         Height          =   315
         Left            =   120
         TabIndex        =   4
         Top             =   2180
         Width           =   1500
      End
      Begin VB.Label Label5 
         Alignment       =   1  'Right Justify
         Caption         =   "N�mero Actual:"
         Height          =   315
         Left            =   120
         TabIndex        =   3
         Top             =   2610
         Width           =   1500
      End
      Begin VB.Label Label1 
         Alignment       =   1  'Right Justify
         Caption         =   "Nombre:"
         Height          =   255
         Left            =   120
         TabIndex        =   2
         Top             =   840
         Width           =   1500
      End
   End
   Begin Threed.SSCommand btnCan 
      Height          =   735
      Left            =   9240
      TabIndex        =   19
      Top             =   5400
      Width           =   855
      _Version        =   65536
      _ExtentX        =   1508
      _ExtentY        =   1296
      _StockProps     =   78
      Caption         =   "&CANCELAR"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   6
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Enabled         =   0   'False
      Picture         =   "frmPrmCOMPROBA.frx":058A
   End
   Begin Threed.SSCommand btnAdd 
      Height          =   735
      Left            =   5640
      TabIndex        =   16
      Top             =   5400
      Width           =   855
      _Version        =   65536
      _ExtentX        =   1508
      _ExtentY        =   1296
      _StockProps     =   78
      Caption         =   "&ADICIONAR"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   6
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Enabled         =   0   'False
      Picture         =   "frmPrmCOMPROBA.frx":0C54
   End
   Begin Threed.SSCommand btnDel 
      Height          =   735
      Left            =   6840
      TabIndex        =   17
      Top             =   5400
      Width           =   855
      _Version        =   65536
      _ExtentX        =   1508
      _ExtentY        =   1296
      _StockProps     =   78
      Caption         =   "&BORRAR"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   6
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Enabled         =   0   'False
      Picture         =   "frmPrmCOMPROBA.frx":18A6
   End
   Begin Threed.SSCommand btnCha 
      Height          =   735
      Left            =   8040
      TabIndex        =   18
      Top             =   5400
      Width           =   855
      _Version        =   65536
      _ExtentX        =   1508
      _ExtentY        =   1296
      _StockProps     =   78
      Caption         =   "&MODIFICAR"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   6
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Enabled         =   0   'False
      Picture         =   "frmPrmCOMPROBA.frx":1F70
   End
End
Attribute VB_Name = "frmPrmCOMPROBA"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Dim CllDeConsec As New Collection
Public OpcCod        As String   'Opci�n de seguridad

Private Sub arbConsec_Collapse(ByVal Punto As MSComctlLib.Node)
    If Punto.Key = "CNSC" Then
'        Me.txtCodigo.Text = String(Me.txtCodigo.MaxLength, " ")
'        Me.txtNombre.Text = String(Me.txtNombre.MaxLength, " ")
'        Me.txtPrefijo.Text = String(Me.txtPrefijo.MaxLength, " ")
'        Me.txtNumActual.Text = Left(CStr(0) & String(Me.txtNumActual.MaxLength, " "), Me.txtNumActual.MaxLength)
        Me.txtCodigo.Text = ""
        Me.TxtNombre.Text = ""
        Me.txtPrefijo.Text = ""
        Me.txtNumActual.Text = CStr(0)
        Me.cboDocum.ListIndex = -1
        Me.cboQuien.ListIndex = -1
        Me.cboNOSI.ListIndex = -1
        Me.cboROMPE.ListIndex = -1
        Me.tlbACCIONES.Buttons("DEL").Enabled = False
        Me.tlbACCIONES.Buttons("CHA").Enabled = False
        Me.tlbACCIONES.Buttons("CAN").Enabled = False
        Me.btnDel.Enabled = False
        Me.btnCha.Enabled = False
        Me.btnCan.Enabled = False
        If Me.txtCodigo.Enabled Then Me.txtCodigo.Enabled = False
        If Me.TxtNombre.Enabled Then Me.TxtNombre.Enabled = False
        If Me.txtNumActual.Enabled Then Me.txtNumActual.Enabled = False
        If Me.txtPrefijo.Enabled Then Me.txtPrefijo.Enabled = False
        If Me.cboDocum.Enabled Then Me.cboDocum.Enabled = False
        If Me.cboQuien.Enabled Then Me.cboQuien.Enabled = False
        If Me.cboNOSI.Enabled Then Me.cboNOSI.Enabled = False
        If Me.cboROMPE.Enabled Then Me.cboROMPE.Enabled = False
    End If
End Sub
'NU_AUTO_ANUL_COMP
Private Sub arbConsec_NodeClick(ByVal Punto As MSComctlLib.Node)
'    Dim UnConsec As ElConsecutivo, NuMr As Integer, unNodo As Node
    Dim UnConsec As ElConsecutivo, unNodo As Node       'DEPURACION DE CODIGO
    For Each unNodo In Me.arbConsec.Nodes
        If unNodo.Bold Then unNodo.Bold = False
    Next
    Me.arbConsec.SelectedItem.Bold = True
    Me.tlbACCIONES.Buttons("DEL").Enabled = False
    Me.tlbACCIONES.Buttons("CHA").Enabled = False
    Me.tlbACCIONES.Buttons("CAN").Enabled = False
    Me.btnDel.Enabled = False
    Me.btnCha.Enabled = False
    Me.btnCan.Enabled = False
    If Punto.Key = "CNSC" Then
'        Me.txtCodigo.Text = String(Me.txtCodigo.MaxLength, " ")
'        Me.txtNombre.Text = String(Me.txtNombre.MaxLength, " ")
'        Me.txtPrefijo.Text = String(Me.txtPrefijo.MaxLength, " ")
'        Me.txtNumActual.Text = Left(CStr(0) & String(Me.txtNumActual.MaxLength, " "), Me.txtNumActual.MaxLength)
        Me.txtCodigo.Text = ""
        Me.TxtNombre.Text = ""
        Me.txtPrefijo.Text = ""
        Me.txtNumActual.Text = CStr(0)
        Me.cboDocum.ListIndex = -1
        Me.cboQuien.ListIndex = -1
        Me.cboNOSI.ListIndex = -1
        Me.cboROMPE.ListIndex = -1
        Me.tlbACCIONES.Buttons("ADD").Enabled = True
        Me.tlbACCIONES.Buttons("DEL").Enabled = False
        Me.tlbACCIONES.Buttons("CHA").Enabled = False
        Me.tlbACCIONES.Buttons("CAN").Enabled = False
        Me.btnAdd.Enabled = True
        Me.btnDel.Enabled = False
        Me.btnCha.Enabled = False
        Me.btnCan.Enabled = False
        If Me.txtCodigo.Enabled Then Me.txtCodigo.Enabled = False
        If Me.TxtNombre.Enabled Then Me.TxtNombre.Enabled = False
        If Me.txtNumActual.Enabled Then Me.txtNumActual.Enabled = False
        If Me.txtPrefijo.Enabled Then Me.txtPrefijo.Enabled = False
        If Me.cboDocum.Enabled Then Me.cboDocum.Enabled = False
        If Me.cboQuien.Enabled Then Me.cboQuien.Enabled = False
        If Me.cboNOSI.Enabled Then Me.cboNOSI.Enabled = False
        If Me.cboROMPE.Enabled Then Me.cboROMPE.Enabled = False
        Exit Sub
    End If
    Me.tlbACCIONES.Buttons("ADD").Enabled = False
    Me.tlbACCIONES.Buttons("DEL").Enabled = True
    Me.tlbACCIONES.Buttons("CHA").Enabled = True
    Me.btnAdd.Enabled = False
    Me.btnDel.Enabled = True
    Me.btnCha.Enabled = True
    If Me.txtCodigo.Enabled Then Me.txtCodigo.Enabled = False
    If Me.TxtNombre.Enabled Then Me.TxtNombre.Enabled = False
    If Me.txtNumActual.Enabled Then Me.txtNumActual.Enabled = False
    If Me.txtPrefijo.Enabled Then Me.txtPrefijo.Enabled = False
    If Me.cboDocum.Enabled Then Me.cboDocum.Enabled = False
    If Me.cboQuien.Enabled Then Me.cboQuien.Enabled = False
    If Me.cboNOSI.Enabled Then Me.cboNOSI.Enabled = False
    If Me.cboROMPE.Enabled Then Me.cboROMPE.Enabled = False
    Set UnConsec = CllDeConsec.Item(Punto.Key)
'    Me.txtCodigo.Text = Left(Trim(UnConsec.Codigo) & String(10, " "), 10)
'    Me.txtNombre = Left(Trim(UnConsec.Nombre) & String(10, " "), 10)
'    Me.txtCodigo.Text = Left(UnConsec.Codigo & String(Me.txtCodigo.MaxLength, " "), Me.txtCodigo.MaxLength)
'    Me.txtNombre.Text = Left(UnConsec.Nombre & String(Me.txtNombre.MaxLength, " "), Me.txtNombre.MaxLength)
'    Me.txtPrefijo.Text = Left(UnConsec.Prefijo & String(Me.txtPrefijo.MaxLength, " "), Me.txtPrefijo.MaxLength)
'    Me.txtNumActual.Text = Left(UnConsec.NumActual & String(Me.txtNumActual.MaxLength, " "), Me.txtNumActual.MaxLength)
'    Me.cboDocum.ListIndex = UnConsec.AutoDocu
    Me.txtCodigo.Text = Trim(UnConsec.Codigo)
    Me.TxtNombre.Text = Trim(UnConsec.Nombre)
    Me.txtPrefijo.Text = Trim(UnConsec.Prefijo)
    Me.txtNumActual.Text = UnConsec.NumActual
    Me.cboQuien.ListIndex = -1
    'Me.cboNOSI.ListIndex = IIf(UnConsec.RequereAutorizacion, 1, 0)
    'NMSR M3077
    If UnConsec.AutoDocu = BajaConsumo Then
        Me.cboNOSI.ListIndex = 0
    Else
        Me.cboNOSI.ListIndex = IIf(UnConsec.RequereAutorizacion, 1, 0)
    End If 'NMSR M3077
    Me.cboROMPE.ListIndex = IIf(UnConsec.RompeDependencia, 1, 0)
    Call BuscaEnCombo(Me.cboDocum, UnConsec.AutoDocu)
'    For NuMr = 1 To Me.cboDocum.ListCount
'         If Me.cboDocum.ItemData(NuMr - 1) = UnConsec.AutoDocu Then
'            Me.cboDocum.ListIndex = NuMr - 1
'            Exit For
'        End If
'    Next
    Call cboQuien_GotFocus
    Call BuscaEnCombo(Me.cboQuien, UnConsec.AutoAnula)
'    For NuMr = 1 To Me.cboQuien.ListCount
'        If Me.cboQuien.ItemData(NuMr - 1) = UnConsec.AutoAnula Then
'            Me.cboQuien.ListIndex = NuMr - 1
'            Exit For
'        End If
'    Next
End Sub

Private Sub btnAdd_Click()
    Dim UnConsec As New ElConsecutivo, Aviso As String, vclave As String
    Dim ArrCSC() As Variant
    If Me.arbConsec.Enabled Then Me.arbConsec.Enabled = False
    If Me.tlbACCIONES.Buttons("ADD").Caption = "&Adicionar" Then
'        Me.txtCodigo.Text = String(Me.txtCodigo.MaxLength, " ")
'        Me.txtNombre.Text = String(Me.txtNombre.MaxLength, " ")
'        Me.txtPrefijo.Text = String(Me.txtPrefijo.MaxLength, " ")
'        Me.txtNumActual.Text = Left(CStr(0) & String(Me.txtNumActual.MaxLength, " "), Me.txtNumActual.MaxLength)
        Me.txtCodigo.Text = ""
        Me.TxtNombre.Text = ""
        Me.txtPrefijo.Text = ""
        Me.txtNumActual.Text = CStr(0)
        Me.cboDocum.ListIndex = -1
        Me.cboQuien.ListIndex = -1
        Me.cboNOSI.ListIndex = -1
        Me.cboROMPE.ListIndex = -1
        Me.cboDocum.Text = "Seleccione uno"
        Me.txtCodigo.Enabled = True
        Me.TxtNombre.Enabled = True
        Me.txtNumActual.Enabled = True
        Me.txtPrefijo.Enabled = True
        Me.cboDocum.Enabled = True
        Me.cboQuien.Enabled = True
        Me.cboNOSI.Enabled = True
        Me.cboROMPE.Enabled = True
        Me.tlbACCIONES.Buttons("ADD").Caption = "&Guardar"
        Me.btnAdd.Caption = "&Guardar"
        Me.tlbACCIONES.Buttons("CAN").Enabled = True
        Me.btnCan.Enabled = True
        Me.txtCodigo.SetFocus
    Else
        Me.txtCodigo.Enabled = False
        Me.TxtNombre.Enabled = False
        Me.txtNumActual.Enabled = False
        Me.txtPrefijo.Enabled = False
        Me.cboDocum.Enabled = False
        Me.cboQuien.Enabled = False
        Me.cboNOSI.Enabled = False
        Me.cboROMPE.Enabled = False
        If MalaInformacion(Aviso) Then
            Call MsgBox("Informaci�n incompleta en el(los) siguiente(s) campos:" & Aviso, vbExclamation)
        Else
            Valores = "TX_CODI_COMP='" & Me.txtCodigo.Text & _
                "', TX_NOMB_COMP='" & Me.TxtNombre.Text & _
                "', TX_PRFJ_COMP='" & Me.txtPrefijo.Text & _
                "', NU_COMP_COMP=" & CLng(Me.txtNumActual.Text) & _
                ", NU_AUTO_DOCU_COMP=" & Me.cboDocum.ItemData(Me.cboDocum.ListIndex)
            If Me.cboQuien.ListIndex > -1 Then
                Valores = Valores & ", NU_AUTO_ANUL_COMP=" & Me.cboQuien.ItemData(Me.cboQuien.ListIndex)
            Else
                Valores = Valores & ", NU_AUTO_ANUL_COMP=0"
            End If
            If Me.cboNOSI.ListIndex > -1 Then
                Valores = Valores & ", TX_REQU_COMP='" & IIf(Me.cboNOSI.ListIndex = 0, "N", "S") & "'"
            Else
                Valores = Valores & ", TX_REQU_COMP='N'"
            End If
            If Me.cboROMPE.ListIndex > -1 Then
                Valores = Valores & ", TX_ROMP_COMP='" & IIf(Me.cboROMPE.ListIndex = 0, "N", "S") & "'"
            Else
                Valores = Valores & ", TX_ROMP_COMP='N'"
            End If
            Result = DoInsertSQL("IN_COMPROBANTE", Valores)
            If Result = FAIL Then GoTo Fallo
            Campos = "NU_AUTO_COMP, TX_CODI_COMP, TX_NOMB_COMP, TX_PRFJ_COMP, " & _
                "NU_AUTO_DOCU_COMP, NU_COMP_COMP, NU_AUTO_ANUL_COMP,TX_REQU_COMP, TX_ROMP_COMP"
            Desde = "IN_COMPROBANTE"
            Condi = "TX_CODI_COMP='" & Me.txtCodigo.Text & "'"
            ReDim ArrCSC(8)
            Result = LoadData(Desde, Campos, Condi, ArrCSC())
            If Result = FAIL Then GoTo Fallo
            If Not Encontro Then GoTo NOENC
            UnConsec.AutoDocu = ArrCSC(4)
            UnConsec.AutoNumero = ArrCSC(0)
            UnConsec.Codigo = ArrCSC(1)
            UnConsec.Nombre = ArrCSC(2)
            UnConsec.NumActual = ArrCSC(5)
            UnConsec.Prefijo = ArrCSC(3)
            UnConsec.AutoAnula = ArrCSC(6)
            If ArrCSC(7) = NUL$ Then
                UnConsec.RequereAutorizacion = "N"
            Else
                UnConsec.RequereAutorizacion = IIf(ArrCSC(7) = "N", False, True)
            End If
            If ArrCSC(8) = NUL$ Then
                UnConsec.RompeDependencia = "N"
            Else
                UnConsec.RompeDependencia = IIf(ArrCSC(8) = "N", False, True)
            End If
            CllDeConsec.Add UnConsec, "S" & ArrCSC(0)
            vclave = "S" & UnConsec.AutoNumero
            Me.arbConsec.Nodes.Add "CNSC", tvwChild, vclave, Trim(UnConsec.Nombre)
            Me.txtCodigo.Text = Left(UnConsec.Codigo & String(Me.txtCodigo.MaxLength, " "), Me.txtCodigo.MaxLength)
            Me.arbConsec.Nodes.Item(vclave).Tag = Me.txtCodigo.Text
'            Me.txtNombre.Text = Left(UnConsec.Nombre & String(Me.txtNombre.MaxLength, " "), Me.txtNombre.MaxLength)
'            Me.txtPrefijo.Text = Left(UnConsec.Prefijo & String(Me.txtPrefijo.MaxLength, " "), Me.txtPrefijo.MaxLength)
            Me.TxtNombre.Text = Trim(UnConsec.Nombre)
            Me.txtPrefijo.Text = Trim(UnConsec.Prefijo)
            Me.txtNumActual.Text = Left(CStr(UnConsec.NumActual) & String(Me.txtNumActual.MaxLength, " "), Me.txtNumActual.MaxLength)
            Me.cboQuien.ListIndex = -1
            If Me.cboQuien.ListCount > 0 And Me.cboQuien.ListCount < UnConsec.AutoAnula Then Call BuscaEnCombo(Me.cboQuien, UnConsec.AutoAnula)
            'Me.cboNOSI.ListIndex = IIf(Not UnConsec.RequereAutorizacion, 0, 1)
            'NMSR M3077
            If UnConsec.AutoDocu = BajaConsumo Then
                Me.cboNOSI.ListIndex = 0
            Else
                Me.cboNOSI.ListIndex = IIf(Not UnConsec.RequereAutorizacion, 0, 1)
            End If
            'NMSR M3077
            Me.cboROMPE.ListIndex = IIf(Not UnConsec.RompeDependencia, 0, 1)
            Set Me.arbConsec.SelectedItem = Me.arbConsec.Nodes("S" & UnConsec.AutoNumero)
            
'      'PedroJ: colocarle a TC_COMPROBANTE el numero de consecutivo del comprobante modificado
'        If Aplicacion.Interfaz_Contabilidad Then
'          Dim Comprobante As String
'          Dim vNumero As Long
'          Dim ArrComp(0) As Variant
'          Comprobante = Buscar_Comprobante_Concepto(UnConsec.AutoDocu)
'          If Comprobante <> NUL$ Then
'            Condicion = "CD_CODI_COMP=" & Comi & Comprobante & Comi
'            Result = LoadData("TC_COMPROBANTE", "NU_CONS_COMP", Condicion, ArrComp)
'            If Result <> FAIL And Encontro Then
'                 Valores = "NU_CONS_COMP=" & UnConsec.NumActual
'                Result = DoUpdate("TC_COMPROBANTE", Valores, Condicion)
'            End If
'          End If
'        End If
'      'PedroJ
            
            Set UnConsec = Nothing
            Me.arbConsec.SelectedItem.Bold = True
        End If
        Me.tlbACCIONES.Buttons("ADD").Caption = "&Adicionar"
        Me.btnAdd.Caption = "&Adicionar"
        Me.tlbACCIONES.Buttons("CAN").Enabled = False
        Me.btnCan.Enabled = False
        Me.arbConsec.Enabled = True
    End If
NOENC:
    Exit Sub
Fallo:
End Sub

Private Sub btnCha_Click()
    Dim UnConsec As New ElConsecutivo, Aviso As String
    If Me.arbConsec.Enabled Then Me.arbConsec.Enabled = False
    If Me.tlbACCIONES.Buttons("CHA").Caption = "&Modificar" Then
        Me.tlbACCIONES.Buttons("DEL").Enabled = False
        Me.btnDel.Enabled = False
        If Not Me.txtCodigo.Enabled Then Me.txtCodigo.Enabled = True
        If Not Me.TxtNombre.Enabled Then Me.TxtNombre.Enabled = True
        If Not Me.txtNumActual.Enabled Then Me.txtNumActual.Enabled = True
        If Not Me.txtPrefijo.Enabled Then Me.txtPrefijo.Enabled = True
        If Not Me.cboDocum.Enabled Then Me.cboDocum.Enabled = True
        Set UnConsec = CllDeConsec.Item(Me.arbConsec.SelectedItem.Key)
        If BuscarMvto(UnConsec, 1) = 1 Then Me.cboDocum.Enabled = False 'PedroJ
        If Not Me.cboQuien.Enabled Then Me.cboQuien.Enabled = True
        'If Not Me.cboNOSI.Enabled Then Me.cboNOSI.Enabled = True
        'NMSR M3077
        If UnConsec.AutoDocu = BajaConsumo Then
            Me.cboNOSI.ListIndex = 0
            Me.cboNOSI.Enabled = False
        Else
            If Not Me.cboNOSI.Enabled Then Me.cboNOSI.Enabled = True
        End If
        'NMSR M3077
        If Not Me.cboROMPE.Enabled Then Me.cboROMPE.Enabled = True
        Me.tlbACCIONES.Buttons("CHA").Caption = "&Guardar"
        Me.btnCha.Caption = "&Guardar"
        Me.tlbACCIONES.Buttons("CAN").Enabled = True
        Me.btnCan.Enabled = True
        Me.txtCodigo.SetFocus
    Else
        If Not Me.tlbACCIONES.Buttons("DEL").Enabled Then Me.tlbACCIONES.Buttons("DEL").Enabled = True
        Me.txtCodigo.Enabled = False
        Me.TxtNombre.Enabled = False
        Me.txtNumActual.Enabled = False
        Me.txtPrefijo.Enabled = False
        Me.cboDocum.Enabled = False
        Me.cboQuien.Enabled = False
        Me.cboNOSI.Enabled = False
        Me.cboROMPE.Enabled = False
        If MalaInformacion(Aviso) Then
            Call MsgBox("Informaci�n incompleta en el(los) siguiente(s) campos:" & Aviso, vbExclamation)
        Else
            Set UnConsec = CllDeConsec.Item(Me.arbConsec.SelectedItem.Key)
            Valores = "TX_CODI_COMP='" & Trim(Me.txtCodigo.Text) & _
                "', TX_NOMB_COMP='" & Trim(Me.TxtNombre.Text) & _
                "', TX_PRFJ_COMP='" & Trim(Me.txtPrefijo.Text) & _
                "', NU_AUTO_DOCU_COMP=" & Me.cboDocum.ItemData(Me.cboDocum.ListIndex) & _
                ", NU_COMP_COMP=" & CLng(Me.txtNumActual.Text)
            
            'PedroJ: si hay movimiento con consecutivo mayor al digitado no lo permite
            If BuscarMvto(UnConsec, 0) = 1 Then GoTo Fallo
            
            If Me.cboQuien.ListIndex > -1 Then
                Valores = Valores & ", NU_AUTO_ANUL_COMP=" & Me.cboQuien.ItemData(Me.cboQuien.ListIndex)
            Else
                Valores = Valores & ", NU_AUTO_ANUL_COMP=0"
            End If
            If Me.cboNOSI.ListIndex > -1 Then
                Valores = Valores & ", TX_REQU_COMP='" & IIf(Me.cboNOSI.ListIndex = 0, "N", "S") & "'"
            Else
                Valores = Valores & ", TX_REQU_COMP='N'"
            End If
            If Me.cboROMPE.ListIndex > -1 Then
                Valores = Valores & ", TX_ROMP_COMP='" & IIf(Me.cboROMPE.ListIndex = 0, "N", "S") & "'"
            Else
                Valores = Valores & ", TX_ROMP_COMP='N'"
            End If
            Condi = "NU_AUTO_COMP=" & CLng(Right(Me.arbConsec.SelectedItem.Key, Len(Me.arbConsec.SelectedItem.Key) - 1))
            Result = DoUpdate("IN_COMPROBANTE", Valores, Condi)
            If Result = FAIL Then GoTo Fallo
            Me.arbConsec.SelectedItem.Tag = Me.txtCodigo.Text
            UnConsec.Codigo = Trim(Me.txtCodigo.Text)
            UnConsec.Nombre = Trim(Me.TxtNombre.Text)
            UnConsec.Prefijo = Trim(Me.txtPrefijo.Text)
            UnConsec.NumActual = Me.txtNumActual.Text
            UnConsec.AutoDocu = Me.cboDocum.ItemData(Me.cboDocum.ListIndex)
            UnConsec.AutoAnula = 0
            If Me.cboQuien.ListIndex > -1 Then UnConsec.AutoAnula = Me.cboQuien.ItemData(Me.cboQuien.ListIndex)
            UnConsec.RequereAutorizacion = True
            If UnConsec.AutoDocu = BajaConsumo Then Me.cboNOSI.ListIndex = 0 'NMSR M3077
            If Me.cboNOSI.ListIndex > -1 Then UnConsec.RequereAutorizacion = IIf(Me.cboNOSI.ListIndex = 0, False, True)
            UnConsec.RompeDependencia = False
            If Me.cboROMPE.ListIndex > -1 Then UnConsec.RompeDependencia = IIf(Me.cboROMPE.ListIndex = 0, False, True)
        End If
        
'      'PedroJ: colocarle a TC_COMPROBANTE el numero de consecutivo del comprobante modificado
'        If Aplicacion.Interfaz_Contabilidad Then
'          Dim Comprobante As String
'          Dim vNumero As Long
'          Dim ArrComp(0) As Variant
'          Comprobante = Buscar_Comprobante_Concepto(UnConsec.AutoDocu)
'          Condicion = "CD_CODI_COMP=" & Comi & Comprobante & Comi
'          Result = LoadData("TC_COMPROBANTE", "NU_CONS_COMP", Condicion, ArrComp)
'          If Result <> FAIL And Encontro Then
'             Valores = "NU_CONS_COMP=" & UnConsec.NumActual
'             Result = DoUpdate("TC_COMPROBANTE", Valores, Condicion)
'          End If
'        End If
'      'PedroJ
      
        Me.tlbACCIONES.Buttons("CHA").Caption = "&Modificar"
        Me.btnCha.Caption = "&Modificar"
        Me.tlbACCIONES.Buttons("CAN").Enabled = False
        Me.btnCha.Enabled = False
        Me.arbConsec.Enabled = True
    End If
Fallo:
End Sub

Private Sub btnCan_Click()
    If Me.arbConsec.SelectedItem.Key = "CNSC" Then
        If Me.tlbACCIONES.Buttons("ADD").Caption = "&Guardar" Then Me.tlbACCIONES.Buttons("ADD").Caption = "&Adicionar"
        If Me.btnAdd.Caption = "&Guardar" Then Me.btnAdd.Caption = "&Adicionar"
    Else
        If Me.tlbACCIONES.Buttons("CHA").Caption = "&Guardar" Then Me.tlbACCIONES.Buttons("CHA").Caption = "&Modificar"
        If Me.btnCha.Caption = "&Guardar" Then Me.btnCha.Caption = "&Modificar"
        Me.tlbACCIONES.Buttons("DEL").Enabled = True
        Me.btnDel.Enabled = True
        Me.tlbACCIONES.Buttons("CHA").Enabled = False
        Me.btnCha.Enabled = False
    End If
    Call arbConsec_NodeClick(Me.arbConsec.SelectedItem)
    Me.arbConsec.Enabled = True
    Me.btnCan.Cancel = False 'GAVL M2660
End Sub

Private Sub btnDel_Click()
    
    'APGR M1978 - inicio
    Dim StCons As String
    
        Desde = "ENF_INVEMOV"
        Campos = "NU_AUTO_COMP_EINM"
        Condi = "NU_AUTO_COMP_EINM=" & CllDeConsec(Me.arbConsec.SelectedItem.Key).AutoNumero

        StCons = fnDevDato(Desde, Campos, Condi)
        If StCons <> NUL$ Then
                Call Mensaje1("El comprobante Seleccionado presenta movimientos. No se puede Borrar", 1)
    'APGR M1978 - fin
            Else
    'APGR M2455 - INICIO
                Condi = "NU_AUTO_COMP_ENCA=" & CllDeConsec(Me.arbConsec.SelectedItem.Key).AutoNumero
                StCons = fnDevDato("IN_ENCABEZADO", "NU_AUTO_COMP_ENCA", Condi)
                If StCons <> NUL$ Then
                     Call Mensaje1("El comprobante Seleccionado presenta movimientos. No se puede Borrar", 1)
                Else
                    Condi = "NU_AUTO_COMP_RBOCO=" & CllDeConsec(Me.arbConsec.SelectedItem.Key).AutoNumero
                    StCons = fnDevDato("IN_R_BODE_COMP", "NU_AUTO_COMP_RBOCO", Condi)
                    If StCons <> NUL$ Then
                        Call Mensaje1("El comprobante Seleccionado presenta movimientos. No se puede Borrar", 1)
                    Else
    'APGR M2455 - FIN
                        Desde = "IN_COMPROBANTE"
                        Condi = "NU_AUTO_COMP = " & CllDeConsec(Me.arbConsec.SelectedItem.Key).AutoNumero
                        Result = DoDelete(Desde, Condi)
                        If Result = FAIL Then Exit Sub
                        Me.arbConsec.Nodes.Remove Me.arbConsec.SelectedItem.Key
                    End If
                End If
        End If
End Sub

Private Sub cboDocum_GotFocus()
    Me.cboQuien.Enabled = False
End Sub

Private Sub cboDocum_LostFocus()
'    Me.cboROMPE.Enabled = False

    If Me.btnCan.Cancel = False Then Exit Sub 'GAVL M2660
    If Not Me.cboDocum.ListIndex > -1 Then Me.cboDocum.SetFocus: Exit Sub
    Dim Cabeza As New ElEncabezado
    Cabeza.TipDeDcmnt = Me.cboDocum.ItemData(Me.cboDocum.ListIndex)
    Cabeza.Actualiza
    Me.cboQuien.Enabled = True
'    If Cabeza.CualDepende > 0 And Cabeza.RomDepende Then
'        Me.cboROMPE.Enabled = True
'    Else
        Me.cboROMPE.ListIndex = 0
        If Cabeza.RomDepende Then Me.cboROMPE.ListIndex = 1
        Me.btnCan.Cancel = True 'GAVL M2660
'    End If
End Sub

Private Sub cboQuien_GotFocus()
'SELECT IN_COMPROBANTE.NU_AUTO_COMP, IN_COMPROBANTE.TX_CODI_COMP, IN_COMPROBANTE.TX_NOMB_COMP, IN_COMPROBANTE.TX_PRFJ_COMP, IN_COMPROBANTE.NU_AUTO_DOCU_COMP, IN_COMPROBANTE.NU_COMP_COMP
'IN_COMPROBANTE
'NU_AUTO_DOCU_COMP IN (SELECT NU_AUTO_DOCUREVE_DOCU FROM IN_DOCUMENTO WHERE NU_AUTO_DOCU=3)
    If Me.cboDocum.ListIndex < 0 Then Exit Sub
    Dim ArrCBO() As Variant, CnTr As Integer
    Campos = "NU_AUTO_COMP, TX_CODI_COMP, TX_NOMB_COMP, TX_PRFJ_COMP, " & _
        "NU_AUTO_DOCU_COMP, NU_COMP_COMP, TX_REQU_COMP"
    Desde = "IN_COMPROBANTE"
    Condi = "NU_AUTO_DOCU_COMP IN (SELECT NU_AUTO_DOCUREVE_DOCU FROM IN_DOCUMENTO WHERE NU_AUTO_DOCU=" & IIf(Me.cboDocum.ListIndex > -1, Me.cboDocum.ItemData(Me.cboDocum.ListIndex), 0) & ")"
    ReDim ArrCBO(6, 0)
    Result = LoadMulData(Desde, Campos, Condi, ArrCBO())
    Me.cboQuien.Clear
    If Result = FAIL Then GoTo Fallo
    If Not Encontro Then GoTo NOENC
    For CnTr = 0 To UBound(ArrCBO, 2)
        Me.cboQuien.AddItem ArrCBO(2, CnTr)
        Me.cboQuien.ItemData(Me.cboQuien.NewIndex) = ArrCBO(0, CnTr)
    Next
Fallo:
NOENC:
End Sub

Private Sub Form_Load()
'    Dim CnTdr As Integer, Punto As MSComctlLib.Node, vclave As String
    Dim CnTdr As Integer, vclave As String      'DEPURACION DE CODIGO
    Dim ArrCSC() As Variant
'    Call main
    Call CenterForm(MDI_Inventarios, Me)
    Me.txtCodigo.Mask = ">" & String(Me.txtCodigo.MaxLength, "a")
    Me.TxtNombre.Mask = ">" & String(Me.TxtNombre.MaxLength, "a")
    Me.txtPrefijo.Mask = ">" & String(Me.txtPrefijo.MaxLength, "a")
    Me.txtNumActual.Mask = String(Me.txtNumActual.MaxLength, "9")
'    Me.txtNombre.Text = ""
'    Me.txtNumActual.Text = CStr(0)
    Me.cboDocum.ListIndex = -1
    Me.txtCodigo.Enabled = False
    Me.TxtNombre.Enabled = False
    Me.txtNumActual.Enabled = False
    Me.txtPrefijo.Enabled = False
    Me.cboDocum.Enabled = False
    Me.cboQuien.Enabled = False
    Me.cboNOSI.Clear
    Me.cboNOSI.AddItem "NO"
    Me.cboNOSI.AddItem "SI"
    Me.cboNOSI.Enabled = False
    Me.cboROMPE.Clear
    Me.cboROMPE.AddItem "NO"
    Me.cboROMPE.AddItem "SI"
    Me.cboROMPE.Enabled = False
    Campos = "NU_AUTO_DOCU, TX_CODI_DOCU, TX_NOMB_DOCU, TX_AFEC_DOCU, " & _
        "TX_INTE_DOCU, NU_AUTO_DOCUDEPE_DOCU, NU_AUTO_DOCUREVE_DOCU, " & _
        "TX_TIPOVAL_DOCU, TX_INDE_DOCU"
    Desde = "IN_DOCUMENTO"
    Condi = ""
    ReDim ArrCSC(8, 0)
    Result = LoadMulData(Desde, Campos, Condi, ArrCSC())
    If Result = FAIL Then GoTo Fallo
    For CnTdr = 0 To UBound(ArrCSC, 2)
        Me.cboDocum.AddItem ArrCSC(2, CnTdr)
        Me.cboDocum.ItemData(Me.cboDocum.NewIndex) = ArrCSC(0, CnTdr)
    Next
    
    Campos = "NU_AUTO_COMP, TX_CODI_COMP, TX_NOMB_COMP, TX_PRFJ_COMP, " & _
        "NU_AUTO_DOCU_COMP, NU_COMP_COMP, NU_AUTO_ANUL_COMP, TX_REQU_COMP, TX_ROMP_COMP"
    Desde = "IN_COMPROBANTE ORDER BY TX_NOMB_COMP"
    Condi = ""
    ReDim ArrCSC(8, 0)
    Result = LoadMulData(Desde, Campos, Condi, ArrCSC())
    If Result = FAIL Then GoTo Fallo
    Me.arbConsec.Nodes.Add , , "CNSC", "COMPROBANTES"
    Me.arbConsec.Nodes.Item("CNSC").Tag = "[[]]"
    Me.tlbACCIONES.Buttons("ADD").Enabled = True
    Me.btnAdd.Enabled = True
    Set Me.arbConsec.SelectedItem = Me.arbConsec.Nodes("CNSC")
    If Encontro Then
        Call BorrarColeccion(CllDeConsec)
        For CnTdr = 0 To UBound(ArrCSC, 2)
            Dim UnConsec As New ElConsecutivo
            UnConsec.AutoDocu = ArrCSC(4, CnTdr)
            UnConsec.AutoNumero = ArrCSC(0, CnTdr)
            UnConsec.Codigo = ArrCSC(1, CnTdr)
            UnConsec.Nombre = ArrCSC(2, CnTdr)
            UnConsec.NumActual = ArrCSC(5, CnTdr)
            UnConsec.Prefijo = ArrCSC(3, CnTdr)
            If ArrCSC(6, CnTdr) = NUL$ Then
                UnConsec.AutoAnula = -1
            Else
                UnConsec.AutoAnula = CLng(ArrCSC(6, CnTdr))
            End If
            If ArrCSC(7, CnTdr) = NUL$ Then
                UnConsec.RequereAutorizacion = "N"
            Else
                UnConsec.RequereAutorizacion = IIf(ArrCSC(7, CnTdr) = "S", True, False)
            End If
            If ArrCSC(8, CnTdr) = NUL$ Then
                UnConsec.RompeDependencia = "N"
            Else
                UnConsec.RompeDependencia = IIf(ArrCSC(8, CnTdr) = "S", True, False)
            End If
            vclave = "S" & UnConsec.AutoNumero
            CllDeConsec.Add UnConsec, "S" & ArrCSC(0, CnTdr)
            Me.arbConsec.Nodes.Add "CNSC", tvwChild, vclave, Trim(UnConsec.Nombre)
            Me.arbConsec.Nodes.Item(vclave).Tag = Trim(UnConsec.Codigo)
            Set UnConsec = Nothing
        Next
    End If
Fallo:
End Sub

Private Sub tlbACCIONES_ButtonClick(ByVal Boton As MSComctlLib.Button)
    Select Case Boton.Key
    Case Is = "CHA"
        Call btnCha_Click
    Case Is = "ADD"
        Call btnAdd_Click
    Case Is = "DEL"
    Case Is = "CAN"
        Call btnCan_Click
    End Select
End Sub

'Private Sub Form_Unload(Cancel As Integer)
'    BD(BDCur).Close
'End Sub

'Private Sub TxtNombre_KeyPress(Tecla As Integer)
'    Dim Cara As String * 1
'    Cara = Chr(Tecla)
'    If Cara = vbBack Then Exit Sub
'    If Asc(Cara) > 96 And Asc(Tecla) < 113 Then Tecla = Asc(UCase(Chr(Tecla)))
'    If InStr(" 01234567890ABCDEFGHIJKLMNOPQRSTUVWXYZ", Chr(Tecla)) = 0 Then Tecla = 0
'End Sub

'Private Sub txtNumActual_KeyPress(Tecla As Integer)
'    Call TxtNombre_KeyPress(Tecla)
'End Sub

Private Function MalaInformacion(ByRef Cual As String) As Boolean
    Cual = ""
    MalaInformacion = False
    If Len(Trim(Me.TxtNombre)) = 0 Then Cual = Cual & IIf(Len(Cual) = 0, "", ", ") & "Nombre"
    If Len(Trim(Me.txtCodigo)) = 0 Then Cual = Cual & IIf(Len(Cual) = 0, "", ", ") & "C�digo"
    If Len(Trim(Me.txtPrefijo)) = 0 Then Cual = Cual & IIf(Len(Cual) = 0, "", ", ") & "Prefijo"
    If Not IsNumeric(Trim(Me.txtNumActual)) Then Cual = Cual & IIf(Len(Cual) = 0, "", ", ") & "N�mero actual"
    If Me.cboQuien.ListCount > 0 Then If Me.cboQuien.ListIndex < 0 Then Cual = Cual & IIf(Len(Cual) = 0, "", ", ") & "Quien anula"
    If Me.cboNOSI.ListCount > 0 Then If Me.cboNOSI.ListIndex < 0 Then Cual = Cual & IIf(Len(Cual) = 0, "", ", ") & "Requiere Autorizaci�n"
    If Me.cboROMPE.ListCount > 0 Then If Me.cboROMPE.ListIndex < 0 Then Cual = Cual & IIf(Len(Cual) = 0, "", ", ") & "Rompe Dependencia"
    If Len(Cual) > 0 Then MalaInformacion = True
End Function

Private Sub Form_KeyPress(KeyAscii As Integer)
    Call Cambiar_Enter(KeyAscii)
End Sub

Private Sub BuscaEnCombo(ByRef Cmb As VB.ComboBox, ByVal Vlr As Long)
    Dim Mero As Integer
    Cmb.ListIndex = -1
    For Mero = 1 To Cmb.ListCount
        If Cmb.ItemData(Mero - 1) = Vlr Then
            Cmb.ListIndex = Mero - 1
            Exit For
        End If
    Next
End Sub

Private Sub txtCodigo_LostFocus()
    Dim unNodo As MSComctlLib.Node
    For Each unNodo In Me.arbConsec.Nodes
        If Me.tlbACCIONES.Buttons.Item("ADD").Caption = "&Guardar" Then
            If Trim(Me.txtCodigo.Text) = unNodo.Tag Then
                Call MsgBox("El C�digo digitado ya existe, debe cambiarlo", vbInformation)
                Me.txtCodigo.SetFocus
                Exit Sub
            End If
        ElseIf Me.tlbACCIONES.Buttons.Item("CHA").Caption = "&Guardar" Then
            If Not Me.arbConsec.SelectedItem.Key = unNodo.Key Then
                If Trim(Me.txtCodigo.Text) = unNodo.Tag Then
                    Call MsgBox("El C�digo digitado ya existe, debe cambiarlo", vbInformation)
                    Me.txtCodigo.SetFocus
                    Exit Sub
                End If
            End If
        End If
    Next
End Sub

Private Function BuscarMvto(consec As ElConsecutivo, Opc As Integer) As Integer
Dim ArrComp(0) As Variant

BuscarMvto = 0
    
  Condi = "NU_AUTO_DOCU_ENCA= " & consec.AutoDocu & " AND NU_AUTO_COMP_ENCA=" & consec.AutoNumero
  Result = LoadData("IN_ENCABEZADO", "MAX(NU_COMP_ENCA)", Condi, ArrComp)
  If Result <> FAIL And Encontro Then
    If ArrComp(0) = NUL$ Then ArrComp(0) = 0
    If Opc = 0 Then
       If CLng(ArrComp(0)) > CLng(Me.txtNumActual) Then    'Si el consecutivo es menor al del mvto
          Call Mensaje1("El comprobante ya tiene mvto, no se puede disminuir el consecutivo", 1)
          BuscarMvto = 1
       End If
    ElseIf Opc = 1 Then
       'Si tiene mvto no se puede cambiar el tipo de Docum
       If ArrComp(0) > 0 Then BuscarMvto = 1
    End If
  End If
End Function

