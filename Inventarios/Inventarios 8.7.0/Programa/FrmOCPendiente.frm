VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Object = "{C932BA88-4374-101B-A56C-00AA003668DC}#1.1#0"; "MSMASK32.OCX"
Object = "{0BA686C6-F7D3-101A-993E-0000C0EF6F5E}#1.0#0"; "THREED32.OCX"
Begin VB.Form FrmOCPendiente 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Ordenes de Compra Pendientes"
   ClientHeight    =   7560
   ClientLeft      =   45
   ClientTop       =   435
   ClientWidth     =   7455
   Icon            =   "FrmOCPendiente.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   7560
   ScaleWidth      =   7455
   Begin VB.Frame FrmCosto 
      Height          =   1215
      Left            =   4800
      TabIndex        =   13
      Top             =   6240
      Width           =   1455
      Begin VB.OptionButton OptCosto 
         Caption         =   "Costeado"
         Height          =   255
         Index           =   1
         Left            =   120
         TabIndex        =   11
         Top             =   720
         Width           =   975
      End
      Begin VB.OptionButton OptCosto 
         Caption         =   "Sin costear"
         Height          =   255
         Index           =   0
         Left            =   120
         TabIndex        =   10
         Top             =   360
         Width           =   1095
      End
   End
   Begin VB.Frame FrmFechas 
      Height          =   1215
      Left            =   120
      TabIndex        =   4
      Top             =   6240
      Width           =   4575
      Begin VB.CheckBox ChkFechas 
         Caption         =   "Condicionar las fechas del reporte"
         Height          =   315
         Left            =   120
         TabIndex        =   5
         Top             =   240
         Width           =   4095
      End
      Begin MSMask.MaskEdBox MskFechFin 
         Height          =   315
         Left            =   3120
         TabIndex        =   9
         Top             =   600
         Width           =   1215
         _ExtentX        =   2143
         _ExtentY        =   556
         _Version        =   393216
         Enabled         =   0   'False
         MaxLength       =   10
         Format          =   "dd/mm/yyyy"
         Mask            =   "##/##/####"
         PromptChar      =   "_"
      End
      Begin MSMask.MaskEdBox MskFechIni 
         Height          =   315
         Left            =   1080
         TabIndex        =   7
         Top             =   600
         Width           =   1215
         _ExtentX        =   2143
         _ExtentY        =   556
         _Version        =   393216
         Enabled         =   0   'False
         MaxLength       =   10
         Format          =   "dd/mm/yyyy"
         Mask            =   "##/##/####"
         PromptChar      =   "_"
      End
      Begin VB.Label LblDesde 
         Alignment       =   1  'Right Justify
         Caption         =   "Desde:"
         Height          =   255
         Left            =   360
         TabIndex        =   6
         Top             =   600
         Visible         =   0   'False
         Width           =   615
      End
      Begin VB.Label LblHasta 
         Alignment       =   1  'Right Justify
         Caption         =   "Hasta:"
         Height          =   255
         Left            =   2400
         TabIndex        =   8
         Top             =   600
         Width           =   615
      End
   End
   Begin VB.CheckBox ChkTercero 
      Caption         =   "Todos los Terceros"
      Height          =   255
      Left            =   240
      TabIndex        =   0
      Top             =   120
      Width           =   3015
   End
   Begin MSComctlLib.ListView LstVBodega 
      Height          =   2535
      Left            =   120
      TabIndex        =   3
      Top             =   3600
      Width           =   7215
      _ExtentX        =   12726
      _ExtentY        =   4471
      View            =   3
      LabelEdit       =   1
      MultiSelect     =   -1  'True
      LabelWrap       =   -1  'True
      HideSelection   =   0   'False
      FullRowSelect   =   -1  'True
      GridLines       =   -1  'True
      _Version        =   393217
      ForeColor       =   -2147483640
      BackColor       =   -2147483643
      BorderStyle     =   1
      Appearance      =   1
      NumItems        =   0
   End
   Begin MSComctlLib.ListView LstVTercero 
      Height          =   2655
      Left            =   120
      TabIndex        =   1
      Top             =   480
      Width           =   7215
      _ExtentX        =   12726
      _ExtentY        =   4683
      View            =   3
      LabelEdit       =   1
      MultiSelect     =   -1  'True
      LabelWrap       =   -1  'True
      HideSelection   =   0   'False
      AllowReorder    =   -1  'True
      FullRowSelect   =   -1  'True
      GridLines       =   -1  'True
      _Version        =   393217
      ForeColor       =   -2147483640
      BackColor       =   -2147483643
      BorderStyle     =   1
      Appearance      =   1
      NumItems        =   2
      BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Text            =   "Nombre del Art�culo"
         Object.Width           =   7761
      EndProperty
      BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   1
         Text            =   "C�digo"
         Object.Width           =   3881
      EndProperty
   End
   Begin Threed.SSCommand SCmdOpcion 
      Height          =   735
      Left            =   6360
      TabIndex        =   12
      Top             =   6480
      Width           =   855
      _Version        =   65536
      _ExtentX        =   1508
      _ExtentY        =   1296
      _StockProps     =   78
      Caption         =   "&IMPRIMIR"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   6
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      RoundedCorners  =   0   'False
      Picture         =   "FrmOCPendiente.frx":058A
   End
   Begin VB.Label Label1 
      Caption         =   "Bodega"
      Height          =   255
      Left            =   240
      TabIndex        =   2
      Top             =   3240
      Width           =   735
   End
End
Attribute VB_Name = "FrmOCPendiente"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'---------------------------------------------------------------------------------------
' Module    : FrmOCPendiente
' DateTime  : 28/02/2008 14:50
' Author    : hector_rodriguez
' Purpose   : busca las ordenes de compra pendientes por entrada
'---------------------------------------------------------------------------------------

Option Explicit

Private Declare Sub Sleep Lib "kernel32" (ByVal dwMilliseconds As Long) 'HRR M3162

Private Sub ChkFechas_Click()
   Call Un_Enable_Fechas
End Sub

Private Sub ChkFechas_KeyPress(KeyAscii As Integer)
   Call Cambiar_Enter(KeyAscii)
   If KeyAscii = 32 Then Call Un_Enable_Fechas
End Sub

Private Sub ChkTercero_KeyPress(KeyAscii As Integer)
   Call Cambiar_Enter(KeyAscii)
End Sub

Private Sub Form_Activate()
   Me.Caption = "Ordenes de Compra Pendientes"
End Sub

Private Sub LstVBodega_KeyPress(KeyAscii As Integer)
   Call Cambiar_Enter(KeyAscii)
End Sub

'HRR M2998
Private Sub LstVTercero_ColumnClick(ByVal ColumnHeader As MSComctlLib.ColumnHeader)
    LstVTercero.SortKey = ColumnHeader.Index - 1
    LstVTercero.Sorted = True
End Sub
'HRR M2998

Private Sub LstVTercero_KeyPress(KeyAscii As Integer)
   Call Cambiar_Enter(KeyAscii)
End Sub

Private Sub MskFechFin_KeyPress(KeyAscii As Integer)
   Call Cambiar_Enter(KeyAscii)
End Sub

Private Sub MskFechIni_KeyPress(KeyAscii As Integer)
   Call Cambiar_Enter(KeyAscii)
End Sub

Private Sub OptCosto_KeyPress(Index As Integer, KeyAscii As Integer) 'JACC R2330
   Call Cambiar_Enter(KeyAscii)
End Sub

Private Sub SCmdOpcion_Click()
   Call Ordenes_Entrada_Pendientes
End Sub

Private Sub Form_Load()
   
   Dim ArTerc() As Variant
   Dim ArBodega() As Variant
   Dim InI As Integer
   
   Call CenterForm(MDI_Inventarios, Me)
   Call Leer_Permiso(Me.Name, SCmdOpcion, "I")
   Me.LstVBodega.ListItems.Clear
   Me.LstVBodega.Checkboxes = False
   Me.LstVBodega.MultiSelect = True
   Me.LstVBodega.HideSelection = False
   Me.LstVBodega.ColumnHeaders.Clear
   Me.LstVBodega.ColumnHeaders.Add , "NOM", "Nombre de la Bodega", 0.66 * Me.LstVBodega.Width
   Me.LstVBodega.ColumnHeaders.Add , "COD", "C�digo", 0.34 * Me.LstVBodega.Width
   
   Me.LstVTercero.ListItems.Clear
   Me.LstVTercero.Checkboxes = False
   Me.LstVTercero.MultiSelect = True
   Me.LstVTercero.HideSelection = False
   Me.LstVTercero.ColumnHeaders.Clear
   Me.LstVTercero.ColumnHeaders.Add , "COTE", "NIT", 0.3 * Me.LstVTercero.Width
   Me.LstVTercero.ColumnHeaders.Add , "NOTE", "Nombre", 0.6 * Me.LstVTercero.Width
   
   ReDim ArTerc(1, 0)
   Campos = "DISTINCT CD_CODI_TERC, NO_NOMB_TERC"
   Desde = "TERCERO, IN_ENCABEZADO, IN_DOCUMENTO"
   Condicion = "NU_AUTO_DOCU_ENCA = NU_AUTO_DOCU AND CD_CODI_TERC_ENCA = CD_CODI_TERC"
   Condicion = Condicion & " AND NU_AUTO_DOCU=2 ORDER BY NO_NOMB_TERC"
   Result = LoadMulData(Desde, Campos, Condicion, ArTerc)
   
   For InI = 0 To UBound(ArTerc, 2)
       Me.LstVTercero.ListItems.Add , "T" & Trim(ArTerc(0, InI)), Trim(ArTerc(0, InI))
       Me.LstVTercero.ListItems("T" & Trim(ArTerc(0, InI))).ListSubItems.Add , "NAME", Trim(ArTerc(1, InI))
   Next

   ReDim ArBodega(2, 0)
   Desde = "IN_BODEGA,IN_R_BODE_COMP,IN_COMPROBANTE"
   Campos = "DISTINCT NU_AUTO_BODE,TX_CODI_BODE, TX_NOMB_BODE"
   Condicion = "NU_AUTO_BODE=NU_AUTO_BODE_RBOCO AND NU_AUTO_COMP_RBOCO=NU_AUTO_COMP"
   'Condicion = Condicion & " AND NU_AUTO_DOCU_COMP=3" 'HRR M2996
   Condicion = Condicion & " AND NU_AUTO_DOCU_COMP=2" 'HRR M2996
   Result = LoadMulData(Desde, Campos, Condicion, ArBodega())
   For InI = 0 To UBound(ArBodega, 2)
      Me.LstVBodega.ListItems.Add , "B" & ArBodega(0, InI), ArBodega(2, InI)
      Me.LstVBodega.ListItems("B" & ArBodega(0, InI)).ListSubItems.Add , "COD", ArBodega(1, InI)
   Next
   
   MskFechIni.Text = Format(Date, "dd/mm/yyyy")
   MskFechFin.Text = MskFechIni.Text
   
   ''JACC R2330
   OptCosto(0).Value = True
   'JACC R2330
End Sub

Private Sub Un_Enable_Fechas()
   
   If ChkFechas.Value = Unchecked Then
      MskFechIni.Enabled = False
      MskFechFin.Enabled = False
   Else
      MskFechIni.Enabled = True
      MskFechFin.Enabled = True
   End If
   
End Sub


Private Sub MskFechFin_LostFocus()
    If ChkFechas.Value = Checked Then
      If ValFecha(MskFechFin, 1) <> FAIL Then
         If DateDiff("d", CDate(MskFechFin.Text), CDate(MskFechIni.Text)) > 0 Then
            Call Mensaje1("La fecha hasta no puede ser menor a la fecha desde", 3)
            MskFechFin.Text = MskFechIni.Text
         End If
      End If
   End If
End Sub

Private Sub MskFechIni_LostFocus()
   If ChkFechas.Value = Checked Then
      If ValFecha(MskFechIni, 1) <> FAIL Then
         If DateDiff("d", CDate(MskFechFin.Text), CDate(MskFechIni.Text)) > 0 Then
            Call Mensaje1("La fecha desde no puede ser mayor a la fecha hasta", 3)
            MskFechIni.Text = MskFechFin.Text
         End If
      End If
   End If
End Sub

'---------------------------------------------------------------------------------------
' Procedure : Ordenes_Entrada_Pendientes
' DateTime  : 28/02/2008 14:51
' Author    : hector_rodriguez
' Purpose   : busca las ordenes de compra pendientes por entrada
'---------------------------------------------------------------------------------------
'
Private Sub Ordenes_Entrada_Pendientes()
 
   Dim SPCmd As New ADODB.Command
   Dim StFechaIni As String
   Dim StFechaFin As String
   Dim StBodegaSel As String
   Dim StBodega As String
   Dim InI As Integer
   Dim StFecha As String
       
   On Error GoTo GoErr
    
   For InI = 1 To LstVBodega.ListItems.Count
      If LstVBodega.ListItems(InI).Selected Then
         If StBodegaSel <> NUL$ Then StBodegaSel = StBodegaSel & ","
         StBodegaSel = StBodegaSel & Mid(LstVBodega.ListItems(InI).Key, 2)
      End If
      If StBodega <> NUL$ Then StBodega = StBodega & ","
      StBodega = StBodega & Mid(LstVBodega.ListItems(InI).Key, 2)
   Next
   
   If StBodega = NUL$ Then
      Call Mensaje1("Debe existir como m�nimo una bodega ", 2)
      Exit Sub
   End If
   
   Call MouseClock
   
   If StBodegaSel <> NUL$ Then
      StBodega = StBodegaSel
   End If
   
   If ChkFechas.Value = Unchecked Then
      StFechaIni = FHFECHA(CDate("01/01/1900 00:00:00"))
      StFechaFin = FHFECHA(Now)
   Else
      StFechaIni = FHFECHA(CDate(MskFechIni.Text))
      StFechaFin = FHFECHA(CDate(MskFechFin.Text))
   End If
   
   If MotorBD = "SQL" Then
      If ExisteStoreProcedure("PA_ORDENES_PENDIENTES") Then
                   
         StFechaIni = Replace(StFechaIni, "'", "")
         StFechaFin = Replace(StFechaFin, "'", "")
         Call CreaParametrosSP(SPCmd, "Bodega", adVarChar, StBodega, adParamInput, 4000)
         Call CreaParametrosSP(SPCmd, "FechaIni", adVarChar, StFechaIni, adParamInput, 21)
         Call CreaParametrosSP(SPCmd, "FechaFin", adVarChar, StFechaFin, adParamInput, 21)
         Call CreaParametrosSP(SPCmd, "Resultado", adBigInt, True, adParamOutput)
         
         SPCmd.CommandTimeout = 0
         SPCmd.CommandType = adCmdStoredProc
         SPCmd.CommandText = "PA_ORDENES_PENDIENTES"
         SPCmd.ActiveConnection = BD(BDCurCon)
         SPCmd.Execute
         SPCmd.CommandTimeout = 600
         
         Result = IIf(SPCmd.Parameters.Item(3).Value, SUCCEED, FAIL)
         If Result = FAIL Then
            Call Mensaje1("Se presento un error en el procedimiento almacenado", 2)
         Else
            Call imprimir
         End If
         Set SPCmd = Nothing
         
      Else
         Call Mensaje1("No existe el procedimiento almacenado PA_ORDENES_PENDIENTES, comuniquese con el Administrador del Sistema.", 2)
      End If
      
   ElseIf MotorBD = "ACCESS" Then
      If ExisteTABLA("ORDEN_ENTR_PEND_TEMP") Then
         Result = ExecSQLCommand("DROP TABLE ORDEN_ENTR_PEND_TEMP")
         If Result = FAIL Then
            Call MouseNorm
            Exit Sub
         End If
      End If
      
         StFecha = "(FE_CREA_ENCA BETWEEN " & StFechaIni & " AND " & StFechaFin & ")"
         Campos = "SELECT NU_ORDEN_OEPT,NU_ARTI_OEPT,SUM(CANTENTRADA) AS NU_CANTENTR_OEPT,SUM(CANTSALIDA) AS NU_CANTSALI_OEPT INTO ORDEN_ENTR_PEND_TEMP"
         Desde = " FROM (SELECT NU_AUTO_MODCABE_DETA AS NU_ORDEN_OEPT,NU_AUTO_ARTI_DETA AS NU_ARTI_OEPT, "
         Desde = Desde & "SUM(NU_ENTRAD_DETA)AS CANTENTRADA,0 AS CANTSALIDA FROM IN_DETALLE,IN_ENCABEZADO"
         Condicion = " WHERE NU_AUTO_ORGCABE_DETA IN (SELECT NU_AUTO_ENCA FROM IN_ENCABEZADO WHERE"
         Condicion = Condicion & " NU_AUTO_DOCU_ENCA = 3 AND NU_AUTO_BODEORG_ENCA IN (" & StBodega & " )"
         Condicion = Condicion & " AND " & StFecha & "AND TX_ESTA_ENCA<>'A') AND NU_AUTO_ORGCABE_DETA<>NU_AUTO_ENCA"
         Condicion = Condicion & " AND NU_AUTO_MODCABE_DETA=NU_AUTO_ENCA AND NU_AUTO_DOCU_ENCA=2"
         Condicion = Condicion & " AND NU_AUTO_BODEORG_ENCA IN (" & StBodega & ")"
         Condicion = Condicion & " AND " & StFecha & " AND TX_ESTA_ENCA='N'"
         Condicion = Condicion & " GROUP BY NU_AUTO_MODCABE_DETA,NU_AUTO_ARTI_DETA UNION"
         'HRR M3048
         'Devolucion de entrada dependiente de orden, el sistema crea entre otros un registro con origen: devolucion, destino:orden de compra, salida:cantidad de devolucion
         Condicion = Condicion & " SELECT NU_AUTO_MODCABE_DETA AS NU_ORDEN_OEPT,NU_AUTO_ARTI_DETA AS NU_ARTI_OEPT, "
         Condicion = Condicion & "(-1)*SUM(NU_SALIDA_DETA)AS CANTENTRADA,0 AS CANTSALIDA FROM IN_DETALLE,IN_ENCABEZADO"
         Condicion = Condicion & " WHERE NU_AUTO_ORGCABE_DETA IN (SELECT NU_AUTO_ENCA FROM IN_ENCABEZADO WHERE"
         Condicion = Condicion & " NU_AUTO_DOCU_ENCA = 4 AND NU_AUTO_BODEORG_ENCA IN (" & StBodega & " )"
         Condicion = Condicion & " AND " & StFecha & "AND TX_ESTA_ENCA='N') AND NU_AUTO_ORGCABE_DETA<>NU_AUTO_ENCA"
         Condicion = Condicion & " AND NU_AUTO_MODCABE_DETA=NU_AUTO_ENCA AND NU_AUTO_DOCU_ENCA=2"
         Condicion = Condicion & " AND NU_AUTO_BODEORG_ENCA IN (" & StBodega & ")"
         Condicion = Condicion & " AND " & StFecha & " AND TX_ESTA_ENCA='N'"
         Condicion = Condicion & " GROUP BY NU_AUTO_MODCABE_DETA,NU_AUTO_ARTI_DETA UNION"
         'HRR M3048
         Condicion = Condicion & " SELECT NU_AUTO_ENCA AS NU_ORDEN_OEPT, NU_AUTO_ARTI_DETA AS NU_ARTI_OEPT, 0 AS CANTENTRADA,"
         Condicion = Condicion & " NU_SALIDA_DETA AS CANTSALIDA FROM IN_DETALLE,IN_ENCABEZADO WHERE"
         Condicion = Condicion & " NU_AUTO_MODCABE_DETA = NU_AUTO_ENCA AND NU_AUTO_ORGCABE_DETA=NU_AUTO_ENCA"
         Condicion = Condicion & " AND NU_AUTO_DOCU_ENCA=2 AND NU_AUTO_BODEORG_ENCA IN (" & StBodega & ")"
         Condicion = Condicion & " AND " & StFecha & " AND TX_ESTA_ENCA='N' ) AS T2 GROUP BY NU_ORDEN_OEPT,NU_ARTI_OEPT"
         'Condicion = Condicion & " Having Sum(CANTENTRADA) <> Sum(CANTSALIDA) ORDER BY 1,2" 'HRR M2998
         Condicion = Condicion & " Having Sum(CANTENTRADA) <> Sum(CANTSALIDA) AND Sum(CANTENTRADA) < Sum(CANTSALIDA) ORDER BY 1,2" 'HRR M2998
         Result = ExecSQLCommand(Campos & Desde & Condicion, "N")
         Sleep (1000) 'HRR M3162
         If Result <> FAIL Then Call imprimir
           
   End If
    
    Call MouseNorm
            
    Exit Sub

GoErr:
    
    Call ConvertErr
    If Not SPCmd Is Nothing Then Set SPCmd = Nothing
    Call MouseNorm

End Sub

'---------------------------------------------------------------------------------------
' Procedure : imprimir
' DateTime  : 28/02/2008 14:53
' Author    : hector_rodriguez
' Purpose   : visualiza el reporte enviando como filtro los terceros seleccionados
'---------------------------------------------------------------------------------------
'
Private Sub imprimir()
      
   Dim InI As Integer
   Dim StTercero As String
       
    On Error GoTo ERR
    
   Call Limpiar_CrysListar 'HRR M3001
   
   If ChkTercero.Value = Unchecked Then
      For InI = 1 To LstVTercero.ListItems.Count
         If LstVTercero.ListItems(InI).Selected Then
            If StTercero <> NUL$ Then StTercero = StTercero & Coma
            StTercero = StTercero & Chr(34) & LstVTercero.ListItems(InI).Text & Chr(34)
         End If
      Next
      
   End If
   
   With MDI_Inventarios.Crys_Listar

      .Formulas(0) = "ENTIDAD= " & Comi & Entidad & Comi
      .Formulas(1) = "NIT= 'Nit. " & Nit & Comi
      '.ReportFileName = App.Path & "\InfOCPendiente.rpt"
      ''JACC R2330
      If OptCosto(1).Value = True Then
         .ReportFileName = App.Path & "\infocpendientecost.rpt"
      Else
         .ReportFileName = App.Path & "\InfOCPendiente.rpt"
      End If
      'JACC R2330
      If StTercero <> NUL$ Then
         .SelectionFormula = NUL$
         .SelectionFormula = "{TERCERO.CD_CODI_TERC} IN [ " & StTercero & " ]"
      Else
         .SelectionFormula = NUL$
      End If
      
      .Destination = crptToWindow
      .WindowTitle = "Informe de ordenes de compra pendientes"
      .Password = Chr(10) & UserPwdBD
      .Action = 1
      
      .Formulas(0) = NUL$
      .Formulas(1) = NUL$
      
   End With
   Exit Sub

ERR:

   Call ConvertErr
   
End Sub
