VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "MSFLXGRD.OCX"
Begin VB.Form FrmTabCheck 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Forma de Pago"
   ClientHeight    =   4485
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   5655
   Icon            =   "FrmTabCheck.frx":0000
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   4485
   ScaleWidth      =   5655
   Begin MSComctlLib.Toolbar Tlb_Opciones 
      Align           =   1  'Align Top
      Height          =   420
      Left            =   0
      TabIndex        =   0
      Top             =   0
      Width           =   5655
      _ExtentX        =   9975
      _ExtentY        =   741
      ButtonWidth     =   609
      ButtonHeight    =   582
      Appearance      =   1
      _Version        =   393216
      BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
         NumButtons      =   7
         BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Style           =   3
         EndProperty
         BeginProperty Button2 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "G"
            Object.ToolTipText     =   "Guardar"
         EndProperty
         BeginProperty Button3 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "B"
            Object.ToolTipText     =   "Borrar"
         EndProperty
         BeginProperty Button4 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "I"
            Object.ToolTipText     =   "Imprimir"
         EndProperty
         BeginProperty Button5 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Style           =   3
         EndProperty
         BeginProperty Button6 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Enabled         =   0   'False
            Key             =   "C"
            Object.ToolTipText     =   "Cambiar C�digo"
         EndProperty
         BeginProperty Button7 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "A"
            Object.ToolTipText     =   "Ayuda"
         EndProperty
      EndProperty
   End
   Begin MSFlexGridLib.MSFlexGrid FGrdLista 
      Height          =   2535
      Left            =   120
      TabIndex        =   7
      Top             =   1800
      Width           =   5415
      _ExtentX        =   9551
      _ExtentY        =   4471
      _Version        =   393216
      Rows            =   1
      Cols            =   3
      FixedCols       =   0
      BackColorBkg    =   16777215
      AllowUserResizing=   1
   End
   Begin VB.Frame FrmDeclaracion 
      Height          =   1335
      Left            =   120
      TabIndex        =   1
      Top             =   400
      Width           =   5415
      Begin VB.CheckBox Chk_Opcion 
         Caption         =   "Opcion"
         Height          =   255
         Left            =   120
         TabIndex        =   6
         Top             =   960
         Width           =   5175
      End
      Begin VB.TextBox Txt_Codigo 
         Height          =   285
         Left            =   840
         TabIndex        =   3
         Top             =   240
         Width           =   3855
      End
      Begin VB.TextBox Txt_Desc 
         Height          =   285
         Left            =   840
         TabIndex        =   5
         Top             =   600
         Width           =   4455
      End
      Begin VB.CommandButton Cmd_Seleccion 
         Height          =   300
         Left            =   4800
         Picture         =   "FrmTabCheck.frx":058A
         Style           =   1  'Graphical
         TabIndex        =   8
         Top             =   240
         UseMaskColor    =   -1  'True
         Width           =   495
      End
      Begin VB.Label Lbl_Codigo 
         Caption         =   "&C�digo"
         Height          =   255
         Left            =   120
         TabIndex        =   2
         Top             =   240
         Width           =   615
      End
      Begin VB.Label Lbl_Nombre 
         Caption         =   "&Nombre"
         Height          =   255
         Left            =   120
         TabIndex        =   4
         Top             =   600
         Width           =   615
      End
   End
End
Attribute VB_Name = "FrmTabCheck"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Dim arrEventos() As C_Eventos
Public Titulo     As String
Public Tabla      As String
Public CampEmpr   As String
Public CampCodi   As String   'Nombre del campo del codigo
Public CampDesc   As String   'Nombre del campo de la descripcion
Public CampChec   As String   'Nombre del campo del check
Public LabelChec  As String   'Nombre del label del check
Public TipoCodi   As String   'Tipo del codigo N:Numerico, Texto
Public Informe    As String
Dim Carcomi       As String   'Caracter comilla
Dim Orden         As String   'Indica el orden
Public OpcCod     As String   'Opci�n de seguridad

Private Sub Cmd_Seleccion_Click()
Dim Codigo As String
Dim Arrsel(3, 2) As Variant

   Arrsel(0, 0) = CampCodi: Arrsel(1, 0) = "C�digo": Arrsel(2, 0) = "1000": Arrsel(3, 0) = "T"
   Arrsel(0, 1) = CampDesc: Arrsel(1, 1) = "Descripci�n": Arrsel(2, 1) = "5000": Arrsel(3, 1) = "T"
   Arrsel(0, 2) = CampChec: Arrsel(1, 2) = LabelChec: Arrsel(2, 2) = "1000": Arrsel(3, 2) = "T"
   Codigo = Seleccion_General(Tabla, NUL$, Arrsel, 0, False, Titulo)
   If Codigo <> NUL$ Then Txt_Codigo = Codigo: Call Txt_Codigo_Validate(False)
            
End Sub

Private Sub FGrdLista_Click()
   If FGrdLista.MouseRow = 0 Then
      If Orden <> 0 Then
         FGrdLista.Sort = 1
         Orden = 0
      Else
         FGrdLista.Sort = 2
         Orden = 1
      End If
   End If
End Sub

Private Sub FGrdLista_DblClick()
   If FGrdLista.Row > 0 Then
      Txt_Codigo = FGrdLista.TextMatrix(FGrdLista.Row, 0)
      Txt_Desc = FGrdLista.TextMatrix(FGrdLista.Row, 1)
      Chk_Opcion.Value = IIf(FGrdLista.TextMatrix(FGrdLista.Row, 2) = "SI", 1, 0)
      Txt_Desc.SetFocus
      Tlb_Opciones.Buttons("C").Enabled = IIf(Txt_Codigo = NUL$, False, True)
   End If
End Sub

Private Sub Form_Activate()
   FrmMenu.CodOpc = OpcCod
End Sub

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
   Select Case KeyCode
      Case vbKeyF1:
      Case vbKeyF2: If Tlb_Opciones.Buttons("G").Enabled = True Then Call Tlb_Opciones_ButtonClick(Tlb_Opciones.Buttons("G"))
      Case vbKeyF3: If Tlb_Opciones.Buttons("B").Enabled = True Then Call Tlb_Opciones_ButtonClick(Tlb_Opciones.Buttons("B"))
      Case vbKeyF11: If Tlb_Opciones.Buttons("I").Enabled = True Then Call Tlb_Opciones_ButtonClick(Tlb_Opciones.Buttons("I"))
      Case vbKeyF5:
      Case vbKeyF6: If Tlb_Opciones.Buttons("C").Enabled = True Then Call Tlb_Opciones_ButtonClick(Tlb_Opciones.Buttons("C"))
      Case vbKeyF12: Unload Me
   End Select
End Sub

Private Sub Form_Load()
   Orden = 0
   Call CenterForm(MDI_Inventarios, Me)
   Call LoadIconos(Tlb_Opciones)
   Call EventosControles(Me, arrEventos)
   Call Estado_Botones_Forma(Tlb_Opciones, OpcCod)
End Sub

Private Sub Form_QueryUnload(Cancel As Integer, UnloadMode As Integer)
Dim Estado As Integer   'Estado -1, indica que los datos estan incompletos
   Estado = 1
   If Tlb_Opciones.Buttons("G").Enabled = True Then
      If Txt_Codigo.Text <> NUL$ Then
         If WarnMsg("Desea guardar los cambios antes de salir?") Then
            Call Guardar(Estado)
            If Estado = -1 Then Cancel = True
         End If
      End If
   End If
End Sub
Private Sub Form_Unload(Cancel As Integer)
   Call DestruirControles(arrEventos)
End Sub

Private Sub Tlb_Opciones_ButtonClick(ByVal Button As MSComctlLib.Button)
   Select Case Button.Key
      Case "G": Call Guardar(0)
      Case "B": Call Borrar
      Case "I": Call Listar
      Case "C": Call Cambiar
      Case "E": Unload Me
      Case "A:"
   End Select
End Sub

Private Sub Txt_Codigo_KeyPress(KeyAscii As Integer)
   Call ValKeySinEspeciales(KeyAscii)
End Sub
'band bandera que determina desde donde es llamada la funcion
' 0 (boton guardar), 1 (Boton Salir)
Sub Guardar(ByRef Band As Integer, Optional Cambiar As String)
   ReDim Arr(0)
   DoEvents
   Call MouseClock
   
   If (Txt_Codigo = NUL$ Or Txt_Desc = NUL$) Then
      Band = -1
      Call Mensaje1("Se debe especificar los datos completos!!!...", 3)
      Call MouseNorm: Exit Sub
   End If
   
   If (BeginTran(STranIUp & Tabla) <> FAIL) Then
      Campos = CampCodi
      Condicion = CampCodi & "=" & Carcomi & Trim(Txt_Codigo) & Carcomi
      Valores = CampEmpr & "=" & Empresa & Coma
      Valores = Valores & CampCodi & "=" & Carcomi & IIf(Cambiar = NUL$, Cambiar_Comas_Comillas(Txt_Codigo), Cambiar_Comas_Comillas(Cambiar)) & Carcomi & Coma
      Valores = Valores & CampDesc & "=" & Comi & Cambiar_Comas_Comillas(Txt_Desc) & Comi & Coma
      Valores = Valores & CampChec & "=" & Comi & IIf(Chk_Opcion.Value = 1, "S", "N") & Comi
      Result = LoadData(Tabla, Campos, Condicion, Arr)
      If (Not Encontro) Then
         If Band <> 1 Then
            If Not WarnIns() Then
               Result = FAIL
            Else
               Result = DoInsertSQL(Tabla, Valores)
            End If
         Else
            Result = DoInsertSQL(Tabla, Valores)
         End If
      Else
         If Band <> 1 Then
            If Not WarnUpd() Then
               Result = FAIL
            Else
               Result = DoUpdate(Tabla, Valores, Condicion)
            End If
         Else
            Result = DoUpdate(Tabla, Valores, Condicion)
         End If
      End If
   End If
   If (Result <> FAIL) Then
      If (CommitTran() <> FAIL) Then
          Call InfoSave
          Txt_Codigo = NUL$
          Txt_Desc = NUL$
          Chk_Opcion.Value = 0
          Result = LoadfGrid(FGrdLista, Tabla & " ORDER BY " & CampDesc, CampCodi & "," & CampDesc & Coma & CampChec, "")
          Txt_Codigo.SetFocus
      Else
         Call RollBackTran
      End If
   Else
      Call RollBackTran
   End If
   Call MouseNorm
   
End Sub

Sub Borrar()
   ReDim Arr(0)
   DoEvents
   Call MouseClock
   
   If (Txt_Codigo = NUL$) Then
       Call Mensaje1("Ingrese el c�digo", 1)
       Call MouseNorm: Msglin NUL$: Exit Sub
   End If
   Campos = CampCodi
   Condicion = CampCodi & "=" & Carcomi & Txt_Codigo & Carcomi
   Result = LoadData(Tabla, Campos, Condicion, Arr)
   If Encontro Then
      If Not WarnDel() Then Call MouseNorm: Exit Sub
      If (BeginTran(STranDel & Tabla) <> FAIL) Then Result = DoDelete(Tabla, Condicion)
   End If
   If (Result <> FAIL) Then
      If (CommitTran() <> FAIL) Then
         Tlb_Opciones.Buttons("C").Enabled = False
         Txt_Codigo = NUL$
         Txt_Desc = NUL$
         Chk_Opcion.Value = 0
         Result = LoadfGrid(FGrdLista, Tabla & " ORDER BY " & CampDesc, CampCodi & "," & CampDesc, "")
         Txt_Codigo.SetFocus
      Else
         Call RollBackTran
      End If
   Else
       Call RollBackTran
   End If
   Call MouseNorm

End Sub
Sub Listar()
   Dim Forma As New FrmViewReport
   Forma.Tabla = Tabla
   Forma.Caption = Me.Informe
   Forma.Listar (CampCodi & Coma & CampDesc)
End Sub
Sub Buscar()
   ReDim Arr(1)
   If (Txt_Codigo) <> NUL$ Then
      Campos = CampDesc & Coma & CampChec
      Condicion = CampCodi & "=" & Carcomi & Txt_Codigo & Carcomi
      Result = LoadData(Tabla, Campos, Condicion, Arr)
      If Encontro Then
         Txt_Desc = Arr(0)
         Chk_Opcion.Value = IIf(Arr(1) = "S", 1, 0)
         Tlb_Opciones.Buttons("C").Enabled = True
      Else
         Txt_Desc = NUL$
         Chk_Opcion.Value = False
         Tlb_Opciones.Buttons("C").Enabled = False
      End If
   Else
      Txt_Desc = NUL$
   End If
End Sub
Sub CargarFrm()
Dim i As Integer
   Call GrdFDef(Me.FGrdLista, 0, 0, "C�digo,700,Nombre,3000,Requiere Banco,1800")
   Chk_Opcion.Caption = LabelChec
   Result = LoadfGrid(FGrdLista, Tabla & " ORDER BY " & CampDesc, CampCodi & "," & CampDesc & "," & CampChec, "")
   For i = 1 To FGrdLista.Rows - 1
       FGrdLista.TextMatrix(i, 2) = IIf(FGrdLista.TextMatrix(i, 2) = "S", "SI", "NO")
   Next i
   If TipoCodi = "N" Then
      Carcomi = ""
   Else
      Carcomi = Comi
   End If
End Sub
Private Sub Cambiar()
Dim Nuevo As String

   If Tlb_Opciones.Buttons("C").Enabled = False Then Exit Sub
   Nuevo = Cambiar_Codigo(Txt_Codigo.MaxLength, TipoCodi)
   If Nuevo <> NUL$ Then
      Call Guardar(0, Nuevo)
   End If
End Sub

Private Sub Txt_Codigo_Validate(Cancel As Boolean)
   Call Buscar
End Sub

Private Sub Txt_Desc_KeyPress(KeyAscii As Integer)
   Call ValKeyAlfaNum(KeyAscii)
End Sub
