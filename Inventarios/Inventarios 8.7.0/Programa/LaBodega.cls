VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "LaBodega"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit
Private cod As String * 10 'C�digo de la bodega
Private nom As String * 50 'Nombre de la bodega
Private num As Long 'autoN�mero de la bodega
Private pad As Long 'autoN�mero del papa de bodega
Private uso As String * 1 '(V/C) La bodega es para almacenar venta o consumo
Private stAfectaC As String * 1 '|.DR.|, R:1633

Public Property Let Codigo(Cue As String)
    cod = Cue
End Property

Public Property Get Codigo() As String
    Codigo = cod
End Property

Public Property Let Nombre(Cue As String)
    nom = Cue
End Property

Public Property Get Nombre() As String
    Nombre = nom
End Property

Public Property Let AlmacenaPara(Cue As String)
    If InStr(1, "VC", Left(Cue, 1)) > 0 Then uso = Cue
End Property

Public Property Get AlmacenaPara() As String
    AlmacenaPara = uso
End Property

Public Property Let AfectaCont(stAC As String)
    stAfectaC = stAC
End Property
Public Property Get AfectaCont() As String
    AfectaCont = stAfectaC
End Property
Public Property Let Padre(mer As Long)
    pad = mer
End Property

Public Property Get Padre() As Long
    Padre = pad
End Property

Public Property Let AutoNumerico(mer As Long)
    num = mer
End Property

Public Property Get AutoNumerico() As Long
    AutoNumerico = num
End Property

Private Sub Class_Initialize()
    uso = "V"
End Sub

Public Sub IniciaXAuto(mer As Long)
    Dim arrBDGS() As Variant
    Dim Dsd As String, Cnd As String, Cmp As String
    cod = "": nom = "": num = 0: pad = 0: uso = ""
    Dsd = "IN_BODEGA"
    Cmp = "NU_AUTO_BODE, NU_AUTO_BODE_BODE, TX_CODI_BODE, TX_NOMB_BODE, CD_CODI_CECO_BODE, TX_VENTA_BODE"
    Cnd = "NU_AUTO_BODE=" & mer
    ReDim arrBDGS(5)
    Result = LoadData(Dsd, Cmp, Cnd, arrBDGS())
    If Result = FAIL Then Exit Sub
    If Not encontro Then Exit Sub
    If Len(arrBDGS(0)) = 0 Then arrBDGS(0) = "0"
    If Len(arrBDGS(1)) = 0 Then arrBDGS(1) = "0"
    cod = arrBDGS(2): nom = arrBDGS(3): num = CLng(arrBDGS(0)): pad = CLng(arrBDGS(1)): uso = arrBDGS(5)
End Sub
