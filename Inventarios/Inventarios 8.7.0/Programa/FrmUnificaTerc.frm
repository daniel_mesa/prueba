VERSION 5.00
Object = "{0BA686C6-F7D3-101A-993E-0000C0EF6F5E}#1.0#0"; "THREED32.OCX"
Begin VB.Form FrmUnificaTerc 
   Caption         =   "Unificar Terceros"
   ClientHeight    =   1905
   ClientLeft      =   60
   ClientTop       =   450
   ClientWidth     =   6720
   Icon            =   "FrmUnificaTerc.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   ScaleHeight     =   1905
   ScaleWidth      =   6720
   StartUpPosition =   3  'Windows Default
   Begin VB.Frame Frame1 
      Height          =   1815
      Left            =   120
      TabIndex        =   0
      Top             =   0
      Width           =   6495
      Begin VB.TextBox TxtTercero 
         Height          =   285
         Index           =   0
         Left            =   1320
         MaxLength       =   20
         TabIndex        =   2
         Tag             =   " "
         Top             =   360
         Width           =   1215
      End
      Begin VB.TextBox TxtTercero 
         Height          =   285
         Index           =   1
         Left            =   1320
         MaxLength       =   20
         TabIndex        =   4
         Tag             =   " "
         Top             =   720
         Width           =   1215
      End
      Begin VB.CommandButton CmdBoton 
         Caption         =   "&CORREGIR"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   6
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   495
         Index           =   0
         Left            =   1440
         Picture         =   "FrmUnificaTerc.frx":058A
         Style           =   1  'Graphical
         TabIndex        =   5
         Top             =   1200
         Width           =   1095
      End
      Begin VB.CommandButton CmdBoton 
         Caption         =   "&SALIR"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   6
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   495
         Index           =   1
         Left            =   3240
         Picture         =   "FrmUnificaTerc.frx":0C44
         Style           =   1  'Graphical
         TabIndex        =   6
         Top             =   1200
         Width           =   1095
      End
      Begin Threed.SSCommand CmdSeleccion 
         Height          =   255
         Left            =   2520
         TabIndex        =   7
         ToolTipText     =   "Seleccion de Cuentas"
         Top             =   360
         Width           =   495
         _Version        =   65536
         _ExtentX        =   873
         _ExtentY        =   450
         _StockProps     =   78
         BevelWidth      =   0
         Outline         =   0   'False
         Picture         =   "FrmUnificaTerc.frx":0F86
      End
      Begin Threed.SSCommand CmdSeleccion1 
         Height          =   255
         Left            =   2520
         TabIndex        =   8
         ToolTipText     =   "Seleccion de Cuentas"
         Top             =   720
         Width           =   495
         _Version        =   65536
         _ExtentX        =   873
         _ExtentY        =   450
         _StockProps     =   78
         BevelWidth      =   0
         Outline         =   0   'False
         Picture         =   "FrmUnificaTerc.frx":1938
      End
      Begin VB.Label LblNIT 
         AutoSize        =   -1  'True
         Caption         =   "NIT &Anterior:"
         Height          =   195
         Index           =   0
         Left            =   240
         TabIndex        =   1
         Top             =   360
         Width           =   900
      End
      Begin VB.Label LblNIT 
         AutoSize        =   -1  'True
         Caption         =   "NIT &Nuevo:"
         Height          =   195
         Index           =   1
         Left            =   240
         TabIndex        =   3
         Top             =   720
         Width           =   840
      End
      Begin VB.Label LblTercero 
         BackColor       =   &H80000005&
         BorderStyle     =   1  'Fixed Single
         Height          =   285
         Left            =   3240
         TabIndex        =   9
         Top             =   360
         Width           =   3135
      End
      Begin VB.Label LblTercero1 
         BackColor       =   &H80000005&
         BorderStyle     =   1  'Fixed Single
         Height          =   285
         Left            =   3240
         TabIndex        =   10
         Top             =   720
         Width           =   3135
      End
   End
End
Attribute VB_Name = "FrmUnificaTerc"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'copia de formulario corrigeNit en contabilidad HRR Req.1309
Option Explicit
Dim J As Double
Public OpcCod        As String
Dim Datos()
Private Sub CmdBoton_Click(Index As Integer)
    Select Case Index
        Case 0: Call Corrige_NIT
        Case 1: Unload Me
    End Select
End Sub

'HRR Req.1309
Private Sub CmdBoton_GotFocus(Index As Integer)
If txtTercero(0) <> NUL$ And txtTercero(1) = NUL$ And Index = 0 Then txtTercero(1).SetFocus
End Sub

Private Sub CmdSeleccion_Click()
   Codigo = Seleccion("TERCERO", "NO_NOMB_TERC", "CD_CODI_TERC, NO_NOMB_TERC", "TERCEROS", "NU_ESTADO_TERC=0")
   If Codigo <> NUL$ Then txtTercero(0) = Codigo
   txtTercero(0).SetFocus
End Sub
'Hrr Req. 1309
Private Sub CmdSeleccion1_Click()
Codigo = Seleccion("TERCERO", "NO_NOMB_TERC", "CD_CODI_TERC, NO_NOMB_TERC", "TERCEROS", "NU_ESTADO_TERC=0")
   If Codigo <> NUL$ Then txtTercero(1) = Codigo
   txtTercero(1).SetFocus
End Sub

Private Sub Form_Load()
   Call CenterForm(MDI_Inventarios, Me)
End Sub


Private Sub TxtTercero_Change(Index As Integer)
    If Index = 0 Then LblTercero = NUL$
    'HRR Req. 1309
    If Index = 1 Then LblTercero1 = NUL$
End Sub
Private Sub TxtTercero_GotFocus(Index As Integer)
    txtTercero(Index).SelStart = 0
    txtTercero(Index).SelLength = Len(txtTercero(Index))
    If Index = 0 Then txtTercero(1) = NUL$
    If Index = 1 Then
       If txtTercero(0) = NUL$ Then txtTercero(0).SetFocus: Exit Sub
       LblTercero = Leer_Tercero(txtTercero(0))
       'If LblTercero = NUL$ Then TxtTercero(0).SetFocus 'antes Req 1309
       If LblTercero = NUL$ Then
          Call Mensaje1("El Nit no existe", 3)
          txtTercero(0).Text = ""
          txtTercero(0).SetFocus
       End If
    End If
End Sub
Private Sub TxtTercero_KeyDown(Index As Integer, KeyCode As Integer, Shift As Integer)
    If Index = 0 And KeyCode = vbKeyF9 Then CmdSeleccion_Click
    If Index = 1 And KeyCode = vbKeyF9 Then CmdSeleccion1_Click 'HRR Req.1309
End Sub
Private Sub TxtTercero_KeyPress(Index As Integer, KeyAscii As Integer)
    Call Cambiar_Enter(KeyAscii)
End Sub
Private Sub TxtTercero_LostFocus(Index As Integer)
     If txtTercero(Index).Text = NUL$ Then Exit Sub     'PJCA 19/08/05
    txtTercero(Index) = Trim(txtTercero(Index))
    If Index = 1 Then
        If txtTercero(0) = txtTercero(1) Then
           Call Mensaje1("El NIT no puede ser el mismo!!!", 3)
           txtTercero(1).SetFocus: Exit Sub
        Else 'HRR Req.1309
           LblTercero1 = Leer_Tercero(txtTercero(1))
           If LblTercero1 = NUL$ Then
              Call Mensaje1("El Nit no existe", 3)
              txtTercero(1).Text = ""
              txtTercero(1).SetFocus
           End If
        End If
    End If
End Sub
Private Sub Corrige_NIT()
    
    If txtTercero(0) = NUL$ Or LblTercero = NUL$ Then
       txtTercero(0).SetFocus: Exit Sub
    End If
    
    If txtTercero(1) = NUL$ Then
       txtTercero(1).SetFocus: Exit Sub
    End If
    
    If txtTercero(0) = txtTercero(1) Then
       Call Mensaje1("El NIT no puede ser el mismo!!!", 3)
       txtTercero(1).SetFocus: Exit Sub
    End If
    
    If Not WarnMsg("Esta seguro que desea unificar el NIT: " & txtTercero(0) & " por " & txtTercero(1) & " ?" & vbCr & _
                   "La unificacion afecta tanto modulos asistenciales como modulos administrativos.") Then
       txtTercero(0) = NUL$
       txtTercero(0).SetFocus: Exit Sub
    End If
    
    Call MouseClock
    Msglin "Corrigiendo NIT de Terceros"
    
    If (BeginTran(STranIUp & "TERCEROS") <> FAIL) Then
       CmdBoton(0).Enabled = False
       
       Call Busca_Tercero
             
       CmdBoton(0).Enabled = True
    End If
    If (Result <> FAIL) Then
        If (CommitTran() <> FAIL) Then
            Call Mensaje1("Termin� la correcci�n de NIT", 3)
            
            
            txtTercero(0) = NUL$
            txtTercero(0).SetFocus
        Else
            Call RollBackTran
        End If
   Else
      Call Mensaje1("No se corrigi� el NIT.", 1)
      Call RollBackTran
   End If
   Call MouseNorm
   Msglin NUL$
End Sub

'HRR Req. 1309
Private Function Buscar_Paciente(CodPac As String) As Boolean

    ReDim Datos(0)
    Condicion = "NU_HIST_PAC=" & Comi & CodPac & Comi & " AND NU_ESTA_PAC=1"
    Result = LoadData("PACIENTES", Asterisco, Condicion, Datos())
    If Result <> FAIL Then
       If Encontro Then
          Buscar_Paciente = True
       Else
          Buscar_Paciente = False
       End If
    Else
       Buscar_Paciente = False
    End If

End Function

'HRR Req. 1309
Private Function Buscar_Administradora(CodEps As String) As Boolean

    ReDim Datos(0)
    Condicion = "CD_NIT_EPS=" & Comi & CodEps & Comi
    Result = LoadData("EPS", Asterisco, Condicion, Datos())
    If Result <> FAIL Then
       If Encontro Then
          Buscar_Administradora = True
       Else
          Buscar_Administradora = False
       End If
    Else
       Buscar_Administradora = False
    End If

End Function

'HRR Req. 1309
Private Function Buscar_Empleado(CodEmpl As String) As Boolean

    ReDim Datos(0)
    Condicion = "NU_NUME_EMPL=" & Comi & CodEmpl & Comi
    Result = LoadData("EMPLEADO", Asterisco, Condicion, Datos())
    If Result <> FAIL Then
       If Encontro Then
          Buscar_Empleado = True
       Else
          Buscar_Empleado = False
       End If
    Else
       Buscar_Empleado = False
    End If

End Function

'HRR Req. 1309
Private Function Buscar_Admi_Salud(CodAdSa As String) As Boolean

    ReDim Datos(0)
    Condicion = "CD_CODI_ADSA=" & Comi & CodAdSa & Comi
    Result = LoadData("ADMI_SALUD", Asterisco, Condicion, Datos())
    If Result <> FAIL Then
       If Encontro Then
          Buscar_Admi_Salud = True
       Else
          Buscar_Admi_Salud = False
       End If
    Else
       Buscar_Admi_Salud = False
    End If

End Function

'HRR Req. 1309
Private Function Buscar_Fondo_Pensiones(CodFoPe As String) As Boolean

    ReDim Datos(0)
    Condicion = "CD_CODI_FOPE=" & Comi & CodFoPe & Comi
    Result = LoadData("FONDO_PENSIONES", Asterisco, Condicion, Datos())
    If Result <> FAIL Then
       If Encontro Then
          Buscar_Fondo_Pensiones = True
       Else
          Buscar_Fondo_Pensiones = False
       End If
    Else
       Buscar_Fondo_Pensiones = False
    End If

End Function

'HRR Req. 1309
Private Function Buscar_Arp(CodArp As String) As Boolean

    ReDim Datos(0)
    Condicion = "CD_CODI_ARP=" & Comi & CodArp & Comi
    Result = LoadData("ARP", Asterisco, Condicion, Datos())
    If Result <> FAIL Then
       If Encontro Then
          Buscar_Arp = True
       Else
          Buscar_Arp = False
       End If
    Else
       Buscar_Arp = False
    End If

End Function


'HRR Req. 1309
Private Function Buscar_Fondo_Cesantias(CodFoCe As String) As Boolean

    ReDim Datos(0)
    Condicion = "CD_CODI_FOCE=" & Comi & CodFoCe & Comi
    Result = LoadData("FONDO_CESANTIAS", Asterisco, Condicion, Datos())
    If Result <> FAIL Then
       If Encontro Then
          Buscar_Fondo_Cesantias = True
       Else
          Buscar_Fondo_Cesantias = False
       End If
    Else
       Buscar_Fondo_Cesantias = False
    End If

End Function


'HRR Req. 1309
Private Function Buscar_Caja_Compensacion(CodCaCo As String) As Boolean

    ReDim Datos(0)
    Condicion = "CD_CODI_CJCO=" & Comi & CodCaCo & Comi
    Result = LoadData("CAJA_COMPE", Asterisco, Condicion, Datos())
    If Result <> FAIL Then
       If Encontro Then
          Buscar_Caja_Compensacion = True
       Else
          Buscar_Caja_Compensacion = False
       End If
    Else
       Buscar_Caja_Compensacion = False
    End If

End Function

Private Sub Crea_Empleado(Nit_Antes As String, Nit_Nuevo As String, ByVal Tipo_IdPEmp As String)
'HRR Req.1309
'recibe el numero de identificacion y el tipo de documento de dos empleados respectivamente
'ReDim Saldo(17, 0)
'ReDim Saldo(21, 0) 'DAHV M4107 'DRMG T43659 SE DEJA EN COMENTARIO
   ReDim Saldo(23, 0) 'DRMG T43659

    Condicion = "ID_DOCU_EMPL='" & Tipo_IdPEmp & Comi & " AND NU_NUME_EMPL='" & Nit_Antes & Comi
    Result = LoadMulData("EMPLEADO", Asterisco, Condicion, Saldo)
    If Result <> FAIL Then
       If Encontro Then
          For J = 0 To UBound(Saldo(), 2)
                Valores = Comi & Tipo_IdPEmp & Comi & Coma & Comi & Nit_Nuevo & Comi & Coma
                Valores = Valores & Comi & Saldo(2, J) & Comi & Coma & Comi & Saldo(3, J) & Comi & Coma
                Valores = Valores & Comi & Saldo(4, J) & Comi & Coma & Comi & Saldo(5, J) & Comi & Coma
                Valores = Valores & Comi & Saldo(6, J) & Comi & Coma & Comi & Saldo(7, J) & Comi & Coma
                Valores = Valores & Comi & Saldo(8, J) & Comi & Coma & fecha2(Saldo(9, J)) & Coma
                Valores = Valores & Comi & Saldo(10, J) & Comi & Coma & Comi & Saldo(11, J) & Comi & Coma
                Valores = Valores & Comi & Saldo(12, J) & Comi & Coma & Comi & Saldo(13, J) & Comi & Coma
                Valores = Valores & Comi & Saldo(14, J) & Comi & Coma & Comi & Saldo(15, J) & Comi & Coma
                'Valores = Valores & Comi & Saldo(16, j) & Comi & Coma & Comi & Saldo(17, j) & Comi
                'DAHV M4107
                Valores = Valores & Comi & Saldo(16, J) & Comi & Coma & Comi & Saldo(17, J) & Comi & Coma
                Valores = Valores & Comi & Saldo(18, J) & Comi & Coma & Comi & Saldo(19, J) & Comi & Coma
                Valores = Valores & Comi & Saldo(20, J) & Comi & Coma & Comi & Saldo(21, J) & Comi
             Valores = Valores & Coma & Comi & Saldo(23, J) & Comi 'DRMG T43659 SE AGREGA DATO FALTANTE QUE CORRESPONDE A FE_RLIQU_EMPL
                
                Result = DoInsert("EMPLEADO", Valores)
                If Result = FAIL Then Exit For
          Next
       End If
    End If
End Sub

Private Sub Crea_Admi_Salud(Nit_Antes As String, Nit_Nuevo As String)
'HRR Req.1309
'recibe el numero de identificacion y el tipo de documento de dos empleados respectivamente
Dim Arr(0)
'ReDim Saldo(9, 0)
ReDim Saldo(10, 0) 'DAHV M3812

    Condicion = "CD_CODI_ADSA='" & Nit_Antes & Comi
    Result = LoadMulData("ADMI_SALUD", Asterisco, Condicion, Saldo)
    If Result <> FAIL Then
       If Encontro Then
          Condicion = "CD_CODI_ADSA='" & Nit_Nuevo & Comi
          Result = LoadData("ADMI_SALUD", Asterisco, Condicion, Arr)
          If Result <> FAIL Then
             If Arr(0) = NUL$ Then
                
                For J = 0 To UBound(Saldo(), 2)
                    If Saldo(9, J) = NUL$ Then Saldo(9, J) = 0
                    Valores = Comi & Nit_Nuevo & Comi & Coma & Comi & Saldo(1, J) & Comi & Coma
                    Valores = Valores & Comi & Saldo(2, J) & Comi & Coma & Comi & Saldo(3, J) & Comi & Coma
                    Valores = Valores & Comi & Saldo(4, J) & Comi & Coma & Comi & Saldo(5, J) & Comi & Coma
                    Valores = Valores & Comi & Saldo(6, J) & Comi & Coma & Comi & Saldo(7, J) & Comi & Coma
                    Valores = Valores & Comi & Saldo(8, J) & Comi & Coma & CInt(Saldo(9, J)) & Coma & Comi & Saldo(10, J) & Comi ' DAHV M3812
                    'Valores = Valores & Comi & Saldo(8, J) & Comi & Coma & CInt(Saldo(9, J))
                    Result = DoInsert("ADMI_SALUD", Valores)
                    If Result = FAIL Then Exit For
                Next
             End If
          End If
       End If
    End If
End Sub

Private Sub Crea_Fondo_Pensiones(Nit_Antes As String, Nit_Nuevo As String)
'HRR Req.1309
'recibe el numero de identificacion y el tipo de documento de dos empleados respectivamente
Dim Arr(0)
'ReDim Saldo(9, 0) 'DRMG T43659 SE DEJA EN COMENTARIO
   ReDim Saldo(10, 0) 'DRMG T43659

    Condicion = "CD_CODI_FOPE='" & Nit_Antes & Comi
    Result = LoadMulData("FONDO_PENSIONES", Asterisco, Condicion, Saldo)
    If Result <> FAIL Then
       If Encontro Then
          Condicion = "CD_CODI_FOPE='" & Nit_Nuevo & Comi
          Result = LoadData("FONDO_PENSIONES", Asterisco, Condicion, Arr)
          If Result <> FAIL Then
             If Arr(0) = NUL$ Then
                
                For J = 0 To UBound(Saldo(), 2)
                    If Saldo(9, J) = NUL$ Then Saldo(9, J) = 0
                    Valores = Comi & Nit_Nuevo & Comi & Coma & Comi & Saldo(1, J) & Comi & Coma
                    Valores = Valores & Comi & Saldo(2, J) & Comi & Coma & Comi & Saldo(3, J) & Comi & Coma
                    Valores = Valores & Comi & Saldo(4, J) & Comi & Coma & Comi & Saldo(5, J) & Comi & Coma
                    Valores = Valores & Comi & Saldo(6, J) & Comi & Coma & Comi & Saldo(7, J) & Comi & Coma
                    Valores = Valores & Comi & Saldo(8, J) & Comi & Coma & CInt(Saldo(9, J))
                   Valores = Valores & Coma & Comi & Saldo(10, J) & Comi 'DRMG T43659 SE AGREGA DATO FALTANTE QUE CORREPONDERIA A TX_CODIGO_FOPE
                    Result = DoInsert("FONDO_PENSIONES", Valores)
                    If Result = FAIL Then Exit For
                Next
             End If
          End If
       End If
    End If
End Sub

Private Sub Crea_Arp(Nit_Antes As String, Nit_Nuevo As String)
'HRR Req.1309
'recibe el numero de identificacion y el tipo de documento de dos empleados respectivamente
Dim Arr(0)
ReDim Saldo(9, 0)

    Condicion = "CD_CODI_ARP='" & Nit_Antes & Comi
    Result = LoadMulData("ARP", Asterisco, Condicion, Saldo)
    If Result <> FAIL Then
       If Encontro Then
          Condicion = "CD_CODI_ARP='" & Nit_Nuevo & Comi
          Result = LoadData("ARP", Asterisco, Condicion, Arr)
          If Result <> FAIL Then
             If Arr(0) = NUL$ Then
                
                For J = 0 To UBound(Saldo(), 2)
                    If Saldo(9, J) = NUL$ Then Saldo(9, J) = 0
                    Valores = Comi & Nit_Nuevo & Comi & Coma & Comi & Saldo(1, J) & Comi & Coma
                    Valores = Valores & Comi & Saldo(2, J) & Comi & Coma & Comi & Saldo(3, J) & Comi & Coma
                    Valores = Valores & Comi & Saldo(4, J) & Comi & Coma & Comi & Saldo(5, J) & Comi & Coma
                    Valores = Valores & Comi & Saldo(6, J) & Comi & Coma & Comi & Saldo(7, J) & Comi & Coma
                    Valores = Valores & Comi & Saldo(8, J) & Comi & Coma & CInt(Saldo(9, J))
                    Result = DoInsert("ARP", Valores)
                    If Result = FAIL Then Exit For
                Next
             End If
          End If
       End If
    End If
End Sub


Private Sub Crea_Fondo_Cesantias(Nit_Antes As String, Nit_Nuevo As String)
'HRR Req.1309
'recibe el numero de identificacion y el tipo de documento de dos empleados respectivamente
Dim Arr(0)
'ReDim Saldo(9, 0) 'DRMG T43659 SE DEJA EN COMENTARIO
   ReDim Saldo(10, 0) 'DRMG T43659

    Condicion = "CD_CODI_FOCE='" & Nit_Antes & Comi
    Result = LoadMulData("FONDO_CESANTIAS", Asterisco, Condicion, Saldo)
    If Result <> FAIL Then
       If Encontro Then
          Condicion = "CD_CODI_FOCE='" & Nit_Nuevo & Comi
          Result = LoadData("FONDO_CESANTIAS", Asterisco, Condicion, Arr)
          If Result <> FAIL Then
             If Arr(0) = NUL$ Then
                
                For J = 0 To UBound(Saldo(), 2)
                    If Saldo(9, J) = NUL$ Then Saldo(9, J) = 0
                    Valores = Comi & Nit_Nuevo & Comi & Coma & Comi & Saldo(1, J) & Comi & Coma
                    Valores = Valores & Comi & Saldo(2, J) & Comi & Coma & Comi & Saldo(3, J) & Comi & Coma
                    Valores = Valores & Comi & Saldo(4, J) & Comi & Coma & Comi & Saldo(5, J) & Comi & Coma
                    Valores = Valores & Comi & Saldo(6, J) & Comi & Coma & Comi & Saldo(7, J) & Comi & Coma
                    Valores = Valores & Comi & Saldo(8, J) & Comi & Coma & CInt(Saldo(9, J))
                   Valores = Valores & Coma & Comi & Saldo(10, J) & Comi 'DRMG T43659 SE AGREGA DATO FLTANTE QUE CORRESPONDERIA A TX_CODIGO_FOCE
                    Result = DoInsert("FONDO_CESANTIAS", Valores)
                    If Result = FAIL Then Exit For
                Next
             End If
          End If
       End If
    End If
End Sub

Private Sub Crea_Caja_Compensacion(Nit_Antes As String, Nit_Nuevo As String)
'HRR Req.1309
'recibe el numero de identificacion y el tipo de documento de dos empleados respectivamente
Dim Arr(0)
ReDim Saldo(9, 0)

    Condicion = "CD_CODI_CJCO='" & Nit_Antes & Comi
    Result = LoadMulData("CAJA_COMPE", Asterisco, Condicion, Saldo)
    If Result <> FAIL Then
       If Encontro Then
          Condicion = "CD_CODI_CJCO='" & Nit_Nuevo & Comi
          Result = LoadData("CAJA_COMPE", Asterisco, Condicion, Arr)
          If Result <> FAIL Then
             If Arr(0) = NUL$ Then
                
                For J = 0 To UBound(Saldo(), 2)
                    If Saldo(9, J) = NUL$ Then Saldo(9, J) = 0
                    Valores = Comi & Nit_Nuevo & Comi & Coma & Comi & Saldo(1, J) & Comi & Coma
                    Valores = Valores & Comi & Saldo(2, J) & Comi & Coma & Comi & Saldo(3, J) & Comi & Coma
                    Valores = Valores & Comi & Saldo(4, J) & Comi & Coma & Comi & Saldo(5, J) & Comi & Coma
                    Valores = Valores & Comi & Saldo(6, J) & Comi & Coma & Comi & Saldo(7, J) & Comi & Coma
                    Valores = Valores & Comi & Saldo(8, J) & Comi & Coma & CInt(Saldo(9, J))
                    Result = DoInsert("CAJA_COMPE", Valores)
                    If Result = FAIL Then Exit For
                Next
             End If
          End If
       End If
    End If
End Sub



Private Sub Busca_Tercero()


    'HRR Req. 1309
    If ExisteTABLA("EMPLEADO") Then
    
       If Buscar_Empleado(txtTercero(0)) = True Then
          If Buscar_Empleado(txtTercero(1)) = True Then
             Call Mensaje1("Se encontro movimientos en el modulo de Nomina que no permiten realizar la unificacion", 1)
             Result = FAIL
          Else
             If Not WarnMsg("Se encontro movimientos en el modulo de nomina, la unificacion afecta a dos empleados" & vbCr & _
               "Esta seguro que se realice la unificacion?", 0) Then
             Else
                Call Unifica_empleado(txtTercero(0), txtTercero(1))
             End If
          End If
       End If
    End If
    If Result <> FAIL Then
       If ExisteTABLA("PACIENTES") Then
          If Buscar_Paciente(txtTercero(0)) = True Then
             If Result <> FAIL And Buscar_Paciente(txtTercero(1)) = True Then
                If Not WarnMsg("Se encontro movimientos en modulos asistenciales, la unificacion afecta a dos pacientes" & vbCr & _
                    "Esta seguro que se realice la unificacion?", 0) Then
                Else
                   Call Unifica_Paciente(txtTercero(0), txtTercero(1))
                End If
             Else
                Call Mensaje1("No se puede realizar la unificacion porque el nit " & txtTercero(0) & " esta creado como Paciente," & vbCr & _
                                     "y el nit " & txtTercero(1) & " no esta creado como Paciente.", 1)
                Result = FAIL
             End If
          End If
       End If
    End If
    If Result <> FAIL Then
       If ExisteTABLA("EPS") Then
          If Buscar_Administradora(txtTercero(0)) = True Then
             If Buscar_Administradora(txtTercero(1)) = True Then
                If Not WarnMsg("Se encontro movimientos en modulos asistenciales, la unificacion afecta a dos EPS" & vbCr & _
                   "Esta seguro que se realice la unificacion?", 0) Then
                Else
                   Call Unifica_Eps(txtTercero(0), txtTercero(1))
                End If
             Else
                If Result <> FAIL Then
                   Call Mensaje1("No se puede realizar la unificacion porque el nit " & txtTercero(0) & " esta creado como EPS," & vbCr & _
                            "y el nit " & txtTercero(1) & " no esta creado como EPS.", 1)
                   Result = FAIL
               End If
            End If
          End If
       End If
    End If
    If Result <> FAIL Then
       If ExisteTABLA("ADMI_SALUD") Then
          If Buscar_Admi_Salud(txtTercero(0)) = True Then
             If Not WarnMsg("Se encontro movimientos en el modulo de nomina, la unificacion afecta a dos Administradoras de salud" & vbCr & _
                    "Esta seguro que se realice la unificacion?", 0) Then
             Else
                Call Unifica_Admi_Salud(txtTercero(0), txtTercero(1))
             End If
          End If
       End If
    End If
    If Result <> FAIL Then
       If ExisteTABLA("FONDO_PENSIONES") Then
          If Buscar_Fondo_Pensiones(txtTercero(0)) = True Then
             If Not WarnMsg("Se encontro movimientos en el modulo de nomina, la unificacion afecta a dos Fondos de pensiones" & vbCr & _
                   "Esta seguro que se realice la unificacion?", 0) Then
             Else
                Call Unifica_Fondo_Pensiones(txtTercero(0), txtTercero(1))
             End If
          End If
       End If
    End If
    If Result <> FAIL Then
       If ExisteTABLA("ARP") Then
          If Buscar_Arp(txtTercero(0)) = True Then
             If Not WarnMsg("Se va ha unificar dos Arp" & vbCr & _
                   "Esta seguro que se realice la unificacion?", 0) Then
             Else
                Call Unifica_Arp(txtTercero(0), txtTercero(1))
             End If
          End If
       End If
    End If
    If Result <> FAIL Then
       If ExisteTABLA("FONDO_CESANTIAS") Then
          If Buscar_Fondo_Cesantias(txtTercero(0)) = True Then
             If Not WarnMsg("Se encontro movimientos en el modulo de nomina, la unificacion afecta a dos Fondos de Cesantias" & vbCr & _
                   "Esta seguro que se realice la unificacion?", 0) Then
             Else
                Call Unifica_Fondo_Cesantias(txtTercero(0), txtTercero(1))
             End If
          End If
       End If
    End If
    If Result <> FAIL Then
       If ExisteTABLA("CAJA_COMPE") Then
          If Buscar_Caja_Compensacion(txtTercero(0)) = True Then

             If Not WarnMsg("Se encontro movimientos en el modulo de nomina, la unificacion afecta a dos Cajas de compensacion" & vbCr & _
                   "Esta seguro que se realice la unificacion?", 0) Then
             Else
                Call Unifica_Caja_Compensacion(txtTercero(0), txtTercero(1))
             End If
          End If
       End If
    End If
    If Result <> FAIL Then
       Call Corregir_Tablas(txtTercero(0), txtTercero(1))
    End If
    
    If Result <> FAIL Then Result = DoDelete("TERCERO", "CD_CODI_TERC='" & txtTercero(0) & Comi)
       
    If Result <> FAIL Then
       
       
       Valores = "FE_FECHA_REUN=" & fecha2(FechaServer()) & Coma
       Valores = Valores & " TX_NUMCON_REUN=" & Comi & NumConex & Comi & Coma
       Valores = Valores & " TX_OLDTER_REUN=" & Comi & txtTercero(0) & Comi & Coma
       Valores = Valores & " TX_NEWTER_REUN=" & Comi & txtTercero(1) & Comi
       Result = DoInsertSQL("REG_UNIFICACION", Valores)
    End If
  
End Sub
      

'*********** CAMBIOS DE NIT - CORRECCION DE TERCEROS
Private Sub Corregir_Tablas(Nit_Antes As String, Nit_Nuevo As String)
'      Dim Arr(0)       'DEPURACION DE CODIGO
      
        
        'CAMBIAR LA TABLA DE MOVIMIENTOS
   'DRMG T43373 INICIO LAS SIGUIENTES LINEAS SE DEJA EN COMENTARIO
'      Condicion = "CD_TERC_MOVI='" & Nit_Antes & Comi
'      Result = DoUpdate("MOVIMIENTOS", "CD_TERC_MOVI='" & Nit_Nuevo & Comi, Condicion)
'
'      If Result = FAIL Then Exit Sub
'
'      'TABLA CUENTA - TERCERO
'      Call Leer_Cuenta_Tercero(Nit_Antes, Nit_Nuevo)
'
'      If Result = FAIL Then Exit Sub
'
'      'TABLA CUENTA - CENTRO DE COSTO - TERCERO
'      Call Leer_Cuenta_CETE(Nit_Antes, Nit_Nuevo)
'
'      If Result = FAIL Then Exit Sub
'
'      'TABLA SALDOS INICIALES DE TERCEROS
'      Call Leer_Saldo_Tercero(Nit_Antes, Nit_Nuevo)
'
'      If Result = FAIL Then Exit Sub
'
'      'TABLA SALDOS INICIALES DE CENTRO DE COSTO - TERCEROS
'      Call Leer_Saldo_CeTe(Nit_Antes, Nit_Nuevo)
'
'      'HRR Req.1309 MODULOS ADMINISTRATIVOS
'      If Result <> FAIL Then
'         If ExisteTABLA("PARAMETROS_IMPUESTOS") = True Then Call Leer_Parametros_Impuestos(Nit_Antes, Nit_Nuevo)
'      End If
'      If Result <> FAIL Then
'         If ExisteTABLA("C_X_C") = True Then
'            If Result <> FAIL Then Result = DoUpdate("C_X_C", "CD_TERC_CXC=" & Comi & Nit_Nuevo & Comi, "CD_TERC_CXC=" & Comi & Nit_Antes & Comi)
'         End If
'      End If
'      If Result <> FAIL Then
'         If ExisteTABLA("NOTA_CREDITO") = True Then
'            If Result <> FAIL Then Result = DoUpdate("NOTA_CREDITO", "CD_TERC_NCRE=" & Comi & Nit_Nuevo & Comi, "CD_TERC_NCRE=" & Comi & Nit_Antes & Comi)
'         End If
'      End If
'      If Result <> FAIL Then
'         If ExisteTABLA("DEVOLVER") = True Then
'            If Result <> FAIL Then Result = DoUpdate("DEVOLVER", "CD_TERC_DEVD=" & Comi & Nit_Nuevo & Comi, "CD_TERC_DEVD=" & Comi & Nit_Antes & Comi)
'         End If
'      End If
'      If Result <> FAIL Then
'         If ExisteTABLA("BAJA_DEPE") = True Then
'            If Result <> FAIL Then Result = DoUpdate("BAJA_DEPE", "CD_TERC_BAJD=" & Comi & Nit_Nuevo & Comi, "CD_TERC_BAJD=" & Comi & Nit_Antes & Comi)
'         End If
'      End If
''      If Result <> FAIL Then
''         If ExisteTABLA("MOVIMIENTOS") = True Then
''            If Result <> FAIL Then Result = DoUpdate("MOVIMIENTOS", "CD_TERC_MOVI=" & Comi & Nit_Nuevo & Comi, "CD_TERC_MOVI=" & Comi & Nit_Antes & Comi)
''         End If
''      End If
'      If Result <> FAIL Then
'         If ExisteTABLA("PROTOTIPO") = True Then
'            If Result <> FAIL Then Result = DoUpdate("PROTOTIPO", "CD_TERC_PROT=" & Comi & Nit_Nuevo & Comi, "CD_TERC_PROT=" & Comi & Nit_Antes & Comi)
'         End If
'      End If
'      If Result <> FAIL Then
'         If ExisteTABLA("IN_ENCABEZADO") = True Then
'            If Result <> FAIL Then Result = DoUpdate("IN_ENCABEZADO", "CD_CODI_TERC_ENCA=" & Comi & Nit_Nuevo & Comi, "CD_CODI_TERC_ENCA=" & Comi & Nit_Antes & Comi)
'         End If
'      End If

   Dim VrArr() As Variant 'Almacena los resultados de las consultas
   Dim StTabla As String 'Almacena el nombre de la tabla
   Dim StTercero As String 'Almacena el nombre del campo del tercero
   Dim StCampos As String 'Almacena los campos que no son llave primaria
   Dim StPrimary As String 'Almacena los campos que son llave primaria
   Dim InI As Integer 'Utilziada para los recorridos del for
   Dim InCantC As Integer 'Almacena la cantidad de campos por tabla
   Dim InJ As Integer 'Utilziada para los recorridos del for
   
   If Result <> FAIL Then
      If ExisteTABLA("PARAMETROS_IMPUESTOS") = True Then Call Leer_Parametros_Impuestos(Nit_Antes, Nit_Nuevo)
   End If
   
   ReDim VrArr(1, 0)
   Campos = "T1.NOM_TABLA,T1.NPM_COLUMNA"
   Desde = "(SELECT UPPER(SYSOBJECTS.NAME) AS NOM_TABLA,UPPER(SYSCOLUMNS.NAME) AS NPM_COLUMNA,SYSCOLUMNS.COLID AS COLID"
   Desde = Desde & " FROM SYSOBJECTS INNER JOIN SYSCOLUMNS ON SYSOBJECTS.ID=SYSCOLUMNS.ID"
   Desde = Desde & " INNER JOIN SYSTYPES ON SYSCOLUMNS.XTYPE=SYSTYPES.XTYPE"
   Desde = Desde & " WHERE SYSOBJECTS.XTYPE='U'"
   Desde = Desde & " AND (UPPER(SYSCOLUMNS.NAME) LIKE UPPER('CD_TERC%')"
   Desde = Desde & " OR UPPER(SYSCOLUMNS.NAME) LIKE UPPER('CD_CODI_TERC%'))) AS T1"
   Condicion = "T1.NOM_TABLA NOT IN ('CUENTA_TERCERO','CUENTA_TERCERO_NIIF','CUENTA_CETE','CUENTA_CETE_NIIF','SALDO_TERCERO','SALDO_TERCERO_NIIF',"
   Condicion = Condicion & "'SALDO_CETE','SALDO_CETE_NIIF','IN_R_CODIGOBAR_TERCERO','IN_R_CRITERIO_TERCERO','EXTRACTO_INF','TERCERO','PARAMETROS_IMPUESTOS')"
   Condicion = Condicion & " ORDER BY T1.NOM_TABLA,T1.COLID"
   Result = LoadMulData(Desde, Campos, Condicion, VrArr)
   If Result <> FAIL And Encontro Then
      For InJ = 0 To UBound(VrArr, 2)
         If Result <> FAIL Then
            If ExisteTABLA(CStr(VrArr(0, InJ))) Then
               If ExisteCAMPO(CStr(VrArr(0, InJ)), CStr(VrArr(1, InJ))) Then
                  Result = DoUpdate(VrArr(0, InJ), VrArr(1, InJ) & "=" & Comi & Nit_Nuevo & Comi, VrArr(1, InJ) & "=" & Comi & Nit_Antes & Comi)
               End If
            End If
         End If
      Next
   End If
   
   ReDim VrArr(2, 0)
   Campos = "UPPER(SYSOBJECTS.NAME) AS NOM_TABLA,UPPER(SYSCOLUMNS.NAME) AS NOM_COLUMN,'1'"
   Desde = "SYSOBJECTS INNER JOIN SYSCOLUMNS ON SYSOBJECTS.ID=SYSCOLUMNS.ID"
   Desde = Desde & " INNER JOIN SYSTYPES ON SYSCOLUMNS.XTYPE=SYSTYPES.XTYPE"
   Desde = Desde & " WHERE SYSOBJECTS.XTYPE='U'"
   Desde = Desde & " AND UPPER(SYSOBJECTS.NAME) IN ('CUENTA_TERCERO','CUENTA_TERCERO_NIIF','CUENTA_CETE','CUENTA_CETE_NIIF','SALDO_TERCERO','SALDO_TERCERO_NIIF',"
   Desde = Desde & " 'SALDO_CETE','SALDO_CETE_NIIF','EXTRACTO_INF')"
   Desde = Desde & " AND UPPER(SYSCOLUMNS.NAME) NOT IN (SELECT UPPER(COL_NAME(IC.OBJECT_ID,IC.COLUMN_ID))"
   Desde = Desde & " FROM SYS.INDEXES I INNER JOIN SYS.INDEX_COLUMNS IC ON I.OBJECT_ID = IC.OBJECT_ID"
   Desde = Desde & " AND I.INDEX_ID=IC.INDEX_ID AND I.IS_PRIMARY_KEY=1"
   Desde = Desde & " WHERE OBJECT_NAME(ic.OBJECT_ID) IN ('CUENTA_TERCERO','CUENTA_TERCERO_NIIF','CUENTA_CETE','CUENTA_CETE_NIIF','SALDO_TERCERO','SALDO_TERCERO_NIIF',"
   Desde = Desde & " 'SALDO_CETE','SALDO_CETE_NIIF','EXTRACTO_INF'))"
   Desde = Desde & " UNION"
   Desde = Desde & " SELECT UPPER(OBJECT_NAME(ic.OBJECT_ID)) AS NOM_TABLA,UPPER(COL_NAME(IC.OBJECT_ID,IC.COLUMN_ID)) AS NOM_COLUMN,'0'"
   Desde = Desde & " FROM SYS.INDEXES I INNER JOIN SYS.INDEX_COLUMNS IC ON I.OBJECT_ID = IC.OBJECT_ID"
   Desde = Desde & " AND I.INDEX_ID=IC.INDEX_ID AND I.IS_PRIMARY_KEY=1"
   Desde = Desde & " WHERE OBJECT_NAME(ic.OBJECT_ID) IN ('CUENTA_TERCERO','CUENTA_TERCERO_NIIF','CUENTA_CETE','CUENTA_CETE_NIIF','SALDO_TERCERO','SALDO_TERCERO_NIIF',"
   Desde = Desde & " 'SALDO_CETE','SALDO_CETE_NIIF','EXTRACTO_INF')"
   Desde = Desde & " ORDER BY 1,3"
   Condicion = NUL$
   Result = LoadMulData(Desde, Campos, Condicion, VrArr)
   If Result <> FAIL And Encontro Then
      For InJ = 0 To UBound(VrArr, 2)
         If StTabla <> VrArr(0, InJ) Then
            StTabla = VrArr(0, InJ)
            StPrimary = NUL$
            StCampos = NUL$
            InCantC = 0
            For InI = InJ To UBound(VrArr, 2)
               If VrArr(0, InJ) = VrArr(0, InI) Then
                  If InStr(1, VrArr(1, InI), "CD_TERC") > 0 Or InStr(1, VrArr(1, InI), "CD_CODI_TERC") > 0 Then
                     StTercero = VrArr(1, InI)
                  Else
                     If VrArr(2, InI) = 0 Then
                        StPrimary = StPrimary & VrArr(1, InI) & Coma
                     Else
                        StCampos = StCampos & VrArr(1, InI) & Coma
                     End If
                  End If
                  InCantC = InCantC + 1
               Else
                  Exit For
               End If
            Next InI
            StPrimary = Mid(StPrimary, 1, Len(StPrimary) - 1)
            StCampos = Mid(StCampos, 1, Len(StCampos) - 1)
            InJ = InI - 1
            Call Actualizar_Saldos(StTercero, StPrimary, StTabla, StCampos, Nit_Antes, Nit_Nuevo, InCantC)
         End If
      Next
   End If
   'DRMG T43373 FIN
      If Result <> FAIL Then
         If ExisteTABLA("IN_R_CODIGOBAR_TERCERO") Then Call Leer_Codigobar_Tercero(Nit_Antes, Nit_Nuevo)
      End If
      If Result <> FAIL Then
         If ExisteTABLA("IN_R_CRITERIO_TERCERO") Then Call Leer_Criterio_Tercero(Nit_Antes, Nit_Nuevo)
      End If
   'DRMG T43373 INICIO LAS SIGUIENTES LINEAS SE DEJA EN COMENTARIO
'      If Result <> FAIL Then
'         If ExisteTABLA("IN_PROVEEDORPROYECTO") = True Then
'            If Result <> FAIL Then Result = DoUpdate("IN_PROVEEDORPROYECTO", "CD_CODI_TERC_PROPY=" & Comi & Nit_Nuevo & Comi, "CD_CODI_TERC_PROPY=" & Comi & Nit_Antes & Comi)
'         End If
'      End If
'      If Result <> FAIL Then
'         If ExisteTABLA("IN_CDBARSELE") = True Then
'            If Result <> FAIL Then Result = DoUpdate("IN_CDBARSELE", "CD_CODI_TERC_CDBSE=" & Comi & Nit_Nuevo & Comi, "CD_CODI_TERC_CDBSE=" & Comi & Nit_Antes & Comi)
'         End If
'      End If
'      If Result <> FAIL Then
'         If ExisteTABLA("MOV_CONTABLE") = True Then
'            If Result <> FAIL Then Result = DoUpdate("MOV_CONTABLE", "CD_TERC_MOVC=" & Comi & Nit_Nuevo & Comi, "CD_TERC_MOVC=" & Comi & Nit_Antes & Comi)
'         End If
'      End If
'      If Result <> FAIL Then
'         If ExisteTABLA("C_X_P") = True Then
'            If Result <> FAIL Then Result = DoUpdate("C_X_P", "CD_TERC_CXP=" & Comi & Nit_Nuevo & Comi, "CD_TERC_CXP=" & Comi & Nit_Antes & Comi)
'         End If
'      End If
'      If Result <> FAIL Then
'         If ExisteTABLA("IN_ARTICULOSELECCION") = True Then
'            If Result <> FAIL Then Result = DoUpdate("IN_ARTICULOSELECCION", "CD_CODI_TERC_ARSE=" & Comi & Nit_Nuevo & Comi, "CD_CODI_TERC_ARSE=" & Comi & Nit_Antes & Comi)
'         End If
'      End If
'      If Result <> FAIL Then
'         If ExisteTABLA("REGISTRO_PPTO") = True Then
'            If Result <> FAIL Then Result = DoUpdate("REGISTRO_PPTO", "CD_TERC_REGI=" & Comi & Nit_Nuevo & Comi, "CD_TERC_REGI=" & Comi & Nit_Antes & Comi)
'         End If
'      End If
'      If Result <> FAIL Then
'         If ExisteTABLA("OBLIGACION") = True Then
'            If Result <> FAIL Then Result = DoUpdate("OBLIGACION", "CD_TERC_OBLI=" & Comi & Nit_Nuevo & Comi, "CD_TERC_OBLI=" & Comi & Nit_Antes & Comi)
'         End If
'      End If
'      If Result <> FAIL Then
'         If ExisteTABLA("NOTA_DEBITO_P") = True Then
'            If Result <> FAIL Then Result = DoUpdate("NOTA_DEBITO_P", "CD_TERC_NDEB=" & Comi & Nit_Nuevo & Comi, "CD_TERC_NDEB=" & Comi & Nit_Antes & Comi)
'         End If
'      End If
'      If Result <> FAIL Then
'         If ExisteTABLA("OBL_RESERVA") = True Then
'            If Result <> FAIL Then Result = DoUpdate("OBL_RESERVA", "CD_TERC_OBRE=" & Comi & Nit_Nuevo & Comi, "CD_TERC_OBRE=" & Comi & Nit_Antes & Comi)
'         End If
'      End If
'      If Result <> FAIL Then
'         If ExisteTABLA("MOVBAN") = True Then
'            If Result <> FAIL Then Result = DoUpdate("MOVBAN", "CD_TERC_TEMP=" & Comi & Nit_Nuevo & Comi, "CD_TERC_TEMP=" & Comi & Nit_Antes & Comi)
'         End If
'       End If
'       If Result <> FAIL Then
'          If ExisteTABLA("COMPRO_EGRE") = True Then
'             If Result <> FAIL Then Result = DoUpdate("COMPRO_EGRE", "CD_TERC_COEG=" & Comi & Nit_Nuevo & Comi, "CD_TERC_COEG=" & Comi & Nit_Antes & Comi)
'          End If
'       End If
'       If Result <> FAIL Then
'          If ExisteTABLA("GIRO") = True Then
'             If Result <> FAIL Then Result = DoUpdate("GIRO", "CD_TERC_GIRO=" & Comi & Nit_Nuevo & Comi, "CD_TERC_GIRO=" & Comi & Nit_Antes & Comi)
'          End If
'       End If
'       If Result <> FAIL Then
'          If ExisteTABLA("EXTRACTO_INF") Then
'             If Result <> FAIL Then Call Leer_Extracto_Inf(Nit_Antes, Nit_Nuevo)
'          End If
'       End If
'       If Result <> FAIL Then
'          If ExisteTABLA("NOTA_CREDITO_P") = True Then
'             If Result <> FAIL Then Result = DoUpdate("NOTA_CREDITO_P", "CD_TERC_NCRE=" & Comi & Nit_Nuevo & Comi, "CD_TERC_NCRE=" & Comi & Nit_Antes & Comi)
'          End If
'       End If
'       If Result <> FAIL Then
'          If ExisteTABLA("RESERVAS") = True Then
'             If Result <> FAIL Then Result = DoUpdate("RESERVAS", "CD_TERC_RESE=" & Comi & Nit_Nuevo & Comi, "CD_TERC_RESE=" & Comi & Nit_Antes & Comi)
'          End If
'       End If
'       If Result <> FAIL Then
'          If ExisteTABLA("CTASXPAGAR") = True Then
'             If Result <> FAIL Then Result = DoUpdate("CTASXPAGAR", "CD_TERC_CXP=" & Comi & Nit_Nuevo & Comi, "CD_TERC_CXP=" & Comi & Nit_Antes & Comi)
'          End If
'       End If
'       If Result <> FAIL Then
'          If ExisteTABLA("PAGO_RESERVA") = True Then
'             If Result <> FAIL Then Result = DoUpdate("PAGO_RESERVA", "CD_TERC_PARE=" & Comi & Nit_Nuevo & Comi, "CD_TERC_PARE=" & Comi & Nit_Antes & Comi)
'          End If
'       End If
'       If Result <> FAIL Then
'          If ExisteTABLA("PAGOS") = True Then
'             If Result <> FAIL Then Result = DoUpdate("PAGOS", "CD_TERC_PAGO=" & Comi & Nit_Nuevo & Comi, "CD_TERC_PAGO=" & Comi & Nit_Antes & Comi)
'          End If
'        End If
'        If Result <> FAIL Then
'           If ExisteTABLA("RECIBO_CAJA") = True Then
'              If Result <> FAIL Then Result = DoUpdate("RECIBO_CAJA", "CD_TERC_RECA=" & Comi & Nit_Nuevo & Comi, "CD_TERC_RECA=" & Comi & Nit_Antes & Comi)
'           End If
'        End If
'        If Result <> FAIL Then
'           If ExisteTABLA("NOTA_DEBITO") = True Then
'              If Result <> FAIL Then Result = DoUpdate("NOTA_DEBITO", "CD_TERC_NDEB=" & Comi & Nit_Nuevo & Comi, "CD_TERC_NDEB=" & Comi & Nit_Antes & Comi)
'           End If
'        End If
'        If Result <> FAIL Then
'           If ExisteTABLA("RELA_LINEA_DEDU") = True Then
'              If Result <> FAIL Then Result = DoUpdate("RELA_LINEA_DEDU", "CD_TERC_RELD=" & Comi & Nit_Nuevo & Comi, "CD_TERC_RELD=" & Comi & Nit_Antes & Comi)
'           End If
'        End If
'        If Result <> FAIL Then
'           If ExisteTABLA("TRANSFERECIA_DETALLE") = True Then
'              If Result <> FAIL Then Result = DoUpdate("TRANSFERECIA_DETALLE", "CD_TERC_TRDE=" & Comi & Nit_Nuevo & Comi, "CD_TERC_TRDE=" & Comi & Nit_Antes & Comi)
'
'           End If
'        End If
'        If Result <> FAIL Then
'           If ExisteTABLA("REGISTRO_VF") = True Then
'              If Result <> FAIL Then Result = DoUpdate("REGISTRO_VF", "CD_TERC_REVF=" & Comi & Nit_Nuevo & Comi, "CD_TERC_REVF=" & Comi & Nit_Antes & Comi)
'           End If
'        End If
'        If Result <> FAIL Then
'           If ExisteTABLA("SERIE") = True Then
'              If Result <> FAIL Then Result = DoUpdate("SERIE", "CD_TERC_SERI=" & Comi & Nit_Nuevo & Comi, "CD_TERC_SERI=" & Comi & Nit_Antes & Comi)
'           End If
'        End If
'
'        'DSMM 28151
'        If Result <> FAIL Then
'           If ExisteTABLA("EXCLUSION_TERCEROS") = True Then
'              Result = DoUpdate("EXCLUSION_TERCEROS", "CD_CODI_TERC_EXTER=" & Comi & Nit_Nuevo & Comi, "CD_CODI_TERC_EXTER=" & Comi & Nit_Antes & Comi)
'           End If
'        End If
'        'DSMM 28151
   'DRMG T43373 FIN
    
End Sub



'Hrr Req. 1309
Private Sub Unifica_Paciente(Nit_Antes As String, Nit_Nuevo As String)
Dim Arr(0)
   'DRMG T43659 INICIO Se deja en comentario las siguientes Lineas
   Dim InI As Integer 'ES USADO PARA LOS CICLOS FOR
   Dim VrArr() As Variant 'Almacena resultados de sentencias sql
'               If Result <> FAIL Then
'                  If ExisteTABLA("DIAGNOSTICOS") = True Then
'                     Result = DoUpdate("DIAGNOSTICOS", "NU_HIST_PAC_DIAG=" & Comi & Nit_Nuevo & Comi, "NU_HIST_PAC_DIAG=" & Comi & Nit_Antes & Comi)
'                  End If
'               End If
'               If Result <> FAIL Then
'                  If ExisteTABLA("MOVI_CARGOS") = True Then
'                     Result = DoUpdate("MOVI_CARGOS", "NU_HIST_PAC_MOVI=" & Comi & Nit_Nuevo & Comi, "NU_HIST_PAC_MOVI=" & Comi & Nit_Antes & Comi)
'                  End If
'               End If
   'DRMG T43659 FIN
'               If Result <> FAIL Then
'                  If ExisteTABLA("PACIENTES") = True Then
'                     Result = Auditor("PACIENTES", TranUpd, "(UNIFICAR Tercero)-" & LastCmd) 'CA: Para tener control sobre estos cambios
'                  End If
'               End If
   'DRMG T43659 INICIO Se deja en comentario las siguientes Lineas
'               If Result <> FAIL Then
'                  If ExisteTABLA("TRIAGE") = True Then
'                     Result = DoUpdate("TRIAGE", "NU_HIST_PAC_TRIA=" & Comi & Nit_Nuevo & Comi, "NU_HIST_PAC_TRIA=" & Comi & Nit_Antes & Comi)
'                  End If
'               End If
'               If Result <> FAIL Then
'                  If ExisteTABLA("CITAS_MEDICAS") = True Then
'                     Result = DoUpdate("CITAS_MEDICAS", "NU_HIST_PAC_CIT=" & Comi & Nit_Nuevo & Comi, "NU_HIST_PAC_CIT=" & Comi & Nit_Antes & Comi)
'                  End If
'               End If
'               If Result <> FAIL Then
'                  If ExisteTABLA("REGISTRO") = True Then
'                     Result = LoadData("REGISTRO", "ID_ESTA_ASIS_REG", "NU_HIST_PAC_REG=" & Comi & Nit_Nuevo & Comi & " AND NU_ESCU_REG <> 2", Arr)
'                     If Result <> FAIL And Encontro And Arr(0) = 0 Then
'                        Call Mensaje1("No se pueden realizar los cambios porque el tercero al cual se va a unificar esta creado como paciente" _
'                        & " y tiene un registro de admisi�n abierto. Debe cerrarlo y ejecutar nuevamente la rutina.", 1)
'                        Result = FAIL
'                     End If
'                     If Result <> FAIL Then Result = DoUpdate("REGISTRO", "NU_HIST_PAC_REG=" & Comi & Nit_Nuevo & Comi, "NU_HIST_PAC_REG=" & Comi & Nit_Antes & Comi)
'                  End If
'               End If
'               If Result <> FAIL Then
'                  If ExisteTABLA("PASADO_PACI_PYP") = True Then
'                     Result = DoUpdate("PASADO_PACI_PYP", "NU_HIST_PAC_PP=" & Comi & Nit_Nuevo & Comi, "NU_HIST_PAC_PP=" & Comi & Nit_Antes & Comi)
'                  End If
'               End If
'               If Result <> FAIL Then
'                  If ExisteTABLA("MUESTRAS") = True Then
'                     Result = DoUpdate("MUESTRAS", "NU_HIST_PAC_MUE=" & Comi & Nit_Nuevo & Comi, "NU_HIST_PAC_MUE=" & Comi & Nit_Antes & Comi)
'                  End If
'               End If
'               If Result <> FAIL Then
'                  If ExisteTABLA("AUTORIZACION") = True Then
'                     Result = DoUpdate("AUTORIZACION", "NU_HIST_PAC_AUTOR=" & Comi & Nit_Nuevo & Comi, "NU_HIST_PAC_AUTOR=" & Comi & Nit_Antes & Comi)
'                  End If
'               End If
'               If Result <> FAIL Then
'                  If ExisteTABLA("HISTORIAS1") = True Then
'                     Result = DoUpdate("HISTORIAS1", "NU_NUME_HIST=" & Comi & Nit_Nuevo & Comi, "NU_NUME_HIST=" & Comi & Nit_Antes & Comi)
'                  End If
'               End If
'               If Result <> FAIL Then
'                  If ExisteTABLA("ORDENES") = True Then
'                     Result = DoUpdate("ORDENES", "NU_HIST_PAC_ORDE=" & Comi & Nit_Nuevo & Comi, "NU_HIST_PAC_ORDE=" & Comi & Nit_Antes & Comi)
'                  End If
'               End If
'               If Result <> FAIL Then
'                  If ExisteTABLA("RESULTADOS1") = True Then
'                     Result = DoUpdate("RESULTADOS1", "NU_HIST_PAC_RESU=" & Comi & Nit_Nuevo & Comi, "NU_HIST_PAC_RESU=" & Comi & Nit_Antes & Comi)
'                  End If
'               End If
'               If Result <> FAIL Then
'                  If ExisteTABLA("TRATAMIENTOS") = True Then
'                     Result = DoUpdate("TRATAMIENTOS", "NU_HIST_PAC_TRAT=" & Comi & Nit_Nuevo & Comi, "NU_HIST_PAC_TRAT=" & Comi & Nit_Antes & Comi)
'                  End If
'               End If
'               If Result <> FAIL Then
'                  If ExisteTABLA("MOVI_CARGOS_COT") = True Then
'                     Result = DoUpdate("MOVI_CARGOS_COT", "NU_HIST_PAC_MCOT=" & Comi & Nit_Nuevo & Comi, "NU_HIST_PAC_MCOT=" & Comi & Nit_Antes & Comi)
'                  End If
'               End If
'               If Result <> FAIL Then
'                  If ExisteTABLA("CONSULTAS_RIPS") = True Then
'                     Result = DoUpdate("CONSULTAS_RIPS", "NU_HIST_PAC_CONS=" & Comi & Nit_Nuevo & Comi, "NU_HIST_PAC_CONS=" & Comi & Nit_Antes & Comi)
'                  End If
'               End If
'               If Result <> FAIL Then
'                  If ExisteTABLA("PROCEDIMIENTOS_RIPS") = True Then
'                     Result = DoUpdate("PROCEDIMIENTOS_RIPS", "NU_HIST_PAC_PRRI=" & Comi & Nit_Nuevo & Comi, "NU_HIST_PAC_PRRI=" & Comi & Nit_Antes & Comi)
'                  End If
'               End If
'               If Result <> FAIL Then
'                  If ExisteTABLA("MEDICAMENTOS_RIPS") = True Then
'                     Result = DoUpdate("MEDICAMENTOS_RIPS", "NU_HIST_PAC_MERI=" & Comi & Nit_Nuevo & Comi, "NU_HIST_PAC_MERI=" & Comi & Nit_Antes & Comi)
'                  End If
'               End If
'               If Result <> FAIL Then
'                  If ExisteTABLA("SERVICIOS_RIPS") = True Then
'                     Result = DoUpdate("SERVICIOS_RIPS", "NU_HIST_PAC_SERI=" & Comi & Nit_Nuevo & Comi, "NU_HIST_PAC_SERI=" & Comi & Nit_Antes & Comi)
'                  End If
'               End If
'               If Result <> FAIL Then
'                  If ExisteTABLA("HOSPITALIZACION_RIPS") = True Then
'                     Result = DoUpdate("HOSPITALIZACION_RIPS", "NU_HIST_PAC_HOSP=" & Comi & Nit_Nuevo & Comi, "NU_HIST_PAC_HOSP=" & Comi & Nit_Antes & Comi)
'                  End If
'               End If
'               If Result <> FAIL Then
'                  If ExisteTABLA("URGENCIAS_RIPS") = True Then
'                     Result = DoUpdate("URGENCIAS_RIPS", "NU_HIST_PAC_URGE=" & Comi & Nit_Nuevo & Comi, "NU_HIST_PAC_URGE=" & Comi & Nit_Antes & Comi)
'                  End If
'               End If
'               If Result <> FAIL Then
'                  If ExisteTABLA("RECIENNACIDO_RIPS") = True Then
'                     Result = DoUpdate("RECIENNACIDO_RIPS", "NU_HIST_PAC_NACE=" & Comi & Nit_Nuevo & Comi, "NU_HIST_PAC_NACE=" & Comi & Nit_Antes & Comi)
'                  End If
'               End If
'               If Result <> FAIL Then
'                  If ExisteTABLA("MULTA") = True Then
'                     Result = DoUpdate("MULTA", "NU_HIST_PAC_MULT=" & Comi & Nit_Nuevo & Comi, "NU_HIST_PAC_MULT=" & Comi & Nit_Antes & Comi)
'                  End If
'               End If
'               If Result <> FAIL Then
'                  If ExisteTABLA("HISTORIACLINICA") = True Then
'                     Result = DoUpdate("HISTORIACLINICA", "NU_HIST_PAC_HICL=" & Comi & Nit_Nuevo & Comi, "NU_HIST_PAC_HICL=" & Comi & Nit_Antes & Comi)
'                  End If
'               End If
'               'If Result <> FAIL And NuTransCart = 1 Then Result = DoUpdate("TERCERO", "CD_CODI_TERC=" & Comi & NumHist(Opt) & Comi, "CD_CODI_TERC=" & Comi & NumHist((Opt + 1) Mod 2) & Comi)
'               If Result <> FAIL Then
'                  If ExisteTABLA("CX_SOLICITUD_SALA") = True Then
'                     Result = DoUpdate("CX_SOLICITUD_SALA", "NU_HIST_PAC_SOSA=" & Comi & Nit_Nuevo & Comi, "NU_HIST_PAC_SOSA=" & Comi & Nit_Antes & Comi)
'                  End If
'               End If
'               If Result <> FAIL Then
'                  If ExisteTABLA("TRATAMIENTO_CLINICO") = True Then
'                     Result = DoUpdate("TRATAMIENTO_CLINICO", "NU_HIST_PAC_TRCL=" & Comi & Nit_Nuevo & Comi, "NU_HIST_PAC_TRCL=" & Comi & Nit_Antes & Comi)
'                  End If
'               End If
'               If Result <> FAIL Then
'                  If ExisteTABLA("PACIENTE_IMAGEN") = True Then
'                     Result = DoDelete("PACIENTE_IMAGEN", "NU_HIST_PAC_PAIM=" & Comi & Nit_Antes & Comi)
'                     If Result <> FAIL Then Result = DoUpdate("PACIENTE_IMAGEN", "NU_HIST_PAC_PAIM=" & Comi & Nit_Nuevo & Comi, "NU_HIST_PAC_PAIM=" & Comi & Nit_Antes & Comi)
'                  End If
'               End If
'               If Result <> FAIL Then
'                  If ExisteTABLA("HIST_MEDI_FREC") = True Then
'                     Result = DoUpdate("HIST_MEDI_FREC", "NU_HIST_PAC_HMEF=" & Comi & Nit_Nuevo & Comi, "NU_HIST_PAC_HMEF=" & Comi & Nit_Antes & Comi)
'                  End If
'               End If
'               If Result <> FAIL Then
'                  If ExisteTABLA("HIST_PROC_FREC") = True Then
'                     Result = DoUpdate("HIST_PROC_FREC", "NU_HIST_PAC_HPRF=" & Comi & Nit_Nuevo & Comi, "NU_HIST_PAC_HPRF=" & Comi & Nit_Antes & Comi)
'                  End If
'               End If
'               If Result <> FAIL Then
'                  If ExisteTABLA("HIST_INDI_FREC") = True Then
'                     Result = DoUpdate("HIST_INDI_FREC", "NU_HIST_PAC_HINF=" & Comi & Nit_Nuevo & Comi, "NU_HIST_PAC_HINF=" & Comi & Nit_Antes & Comi)
'                  End If
'               End If
   If Result <> FAIL Then Result = Auditor("PACIENTES", TranUpd, "(INGRESA PAC)-" & LastCmd)
   ReDim VrArr(1, 0)
   Campos = "UPPER(SYSOBJECTS.NAME) AS NOM_TABLA,UPPER(SYSCOLUMNS.NAME) AS NPM_COLUMNA"
   Desde = "SYSOBJECTS INNER JOIN SYSCOLUMNS ON SYSOBJECTS.ID=SYSCOLUMNS.ID"
   Desde = Desde & " INNER JOIN SYSTYPES ON SYSCOLUMNS.XTYPE=SYSTYPES.XTYPE"
   Condicion = "SYSOBJECTS.XTYPE='U'"
   Condicion = Condicion & " AND (UPPER(SYSCOLUMNS.NAME) LIKE UPPER('NU_HIST_PAC%')"
   Condicion = Condicion & " OR UPPER(SYSCOLUMNS.NAME) LIKE UPPER('TX_HIST_PAC%'))"
   Condicion = Condicion & " AND UPPER(SYSOBJECTS.NAME) NOT IN ('PACIENTES','R_PAC_CONV','R_PAC_EPS','ANTECEDENTES','LOGIN_WEB')"
   Condicion = Condicion & " ORDER BY SYSOBJECTS.NAME,SYSCOLUMNS.COLID"
   Result = LoadMulData(Desde, Campos, Condicion, VrArr)
   If Result <> FAIL And Encontro Then
      For InI = 0 To UBound(VrArr, 2)
         If Result <> FAIL Then
            If ExisteTABLA(CStr(VrArr(0, InI))) Then
               If ExisteCAMPO(CStr(VrArr(0, InI)), CStr(VrArr(1, InI))) Then
                  Result = DoUpdate(VrArr(0, InI), VrArr(1, InI) & "=" & Comi & Nit_Nuevo & Comi, VrArr(1, InI) & "=" & Comi & Nit_Antes & Comi)
               End If
            End If
         End If
      Next
   End If
   If fnDevDato("LOGIN_WEB", "NU_HIST_PAC_LOWE", "NU_HIST_PAC_LOWE=" & Comi & Nit_Nuevo & Comi, True) <> NUL$ Then
      If Result <> FAIL Then Result = DoDelete("LOGIN_WEB", "NU_HIST_PAC_LOWE=" & Comi & Nit_Antes & Comi)
   Else
      If Result <> FAIL Then Result = DoUpdate("LOGIN_WEB", "NU_HIST_PAC_LOWE=" & Comi & Nit_Nuevo & Comi, "NU_HIST_PAC_LOWE=" & Comi & Nit_Antes & Comi)
   End If
   'DRMG T43659 FIN
               If Result <> FAIL Then
                  If ExisteTABLA("R_PAC_CONV") = True Then Result = DoDelete("R_PAC_CONV", "NU_HIST_PAC_RPC=" & Comi & Nit_Antes & Comi)
               End If
               If Result <> FAIL Then
                  If ExisteTABLA("R_PAC_EPS") = True Then Result = DoDelete("R_PAC_EPS", "NU_HIST_PAC_RPE=" & Comi & Nit_Antes & Comi)
               End If
               If Result <> FAIL Then
                  If ExisteTABLA("ANTECEDENTES") = True Then Result = DoDelete("ANTECEDENTES", "NU_HIST_PAC_ANTE=" & Comi & Nit_Antes & Comi)
               End If
               'DSMM T27990 Inicio
   'DRMG T43659 INICIO Se deja en comentario las siguientes Lineas
'               If Result <> FAIL Then
'                  If ExisteTABLA("ACTIVIDAD_PACIENTE_RESOL4505") = True Then
'                     Result = DoUpdate("ACTIVIDAD_PACIENTE_RESOL4505", "NU_HIST_PAC_APR=" & Comi & Nit_Nuevo & Comi, "NU_HIST_PAC_APR=" & Comi & Nit_Antes & Comi)
'                  End If
'               End If
   'DRMG T43659 FIN
                ''DSMM T27990 Fin
               If Result <> FAIL Then Result = DoDelete("PACIENTES", "NU_HIST_PAC=" & Comi & Nit_Antes & Comi)
               If Result <> FAIL Then Result = Auditor("PACIENTES", TranUpd, "(UNIFICAR PAC)-" & LastCmd)
           
End Sub


'Hrr Req. 1309
Private Sub Unifica_Eps(Nit_Antes As String, Nit_Nuevo As String)
   If ExisteTABLA("REMISION") = True Then
      Result = DoUpdate("REMISION", "CD_NIT_EPS_REMI=" & Comi & Nit_Nuevo & Comi, "CD_NIT_EPS_REMI=" & Comi & Nit_Antes & Comi)
   End If
   If Result <> FAIL Then
      If ExisteTABLA("CONVENIOS") = True Then
         Result = DoUpdate("CONVENIOS", "CD_NIT_EPS_CONV=" & Comi & Nit_Nuevo & Comi, "CD_NIT_EPS_CONV=" & Comi & Nit_Antes & Comi)
      End If
   End If
   If Result <> FAIL Then
      If ExisteTABLA("CX_SOLICITUD_SERVICIO") = True Then
         Result = DoUpdate("CX_SOLICITUD_SERVICIO", "CD_NIT_EPS_SOSE=" & Comi & Nit_Nuevo & Comi, "CD_NIT_EPS_SOSE=" & Comi & Nit_Antes & Comi)
      End If
   End If
   If Result <> FAIL Then
      If ExisteTABLA("R_REG_EPS") = True Then Call Leer_Regimen_Eps(Nit_Antes, Nit_Nuevo)
   End If
   'If Result <> FAIL Then
      'If ExisteTABLA("CUOTA_TIPOAFIL") = True Then
         'If Result <> FAIL Then Result = DoUpdate("CUOTA_TIPOAFIL", "CD_NIT_EPS_CUTA=" & Comi & Nit_Nuevo & Comi, "CD_NIT_EPS_CUTA=" & Comi & Nit_Antes & Comi)
      'End If
   'End If
   If Result <> FAIL Then
      If ExisteTABLA("CUOTA_TIPOAFIL") = True Then Call Leer_Cuota_TipoAfiliacion(Nit_Antes, Nit_Nuevo)
   End If
   If Result <> FAIL Then
      If ExisteTABLA("R_PAC_EPS") = True Then Call Leer_Paciente_Eps(Nit_Antes, Nit_Nuevo)
   End If
   If Result <> FAIL Then
      If ExisteTABLA("NOVEDADES") = True Then
         If Result <> FAIL Then Result = DoUpdate("NOVEDADES", "CD_NIT_EPS_NOVE=" & Comi & Nit_Nuevo & Comi, "CD_NIT_EPS_NOVE=" & Comi & Nit_Antes & Comi)
      End If
   End If
   'JAUM T34758 Inicio
   If Result <> FAIL Then
      Result = DoUpdate("ACTIVIDAD_PACIENTE_RESOL4505", "CD_NIT_EPS_APR = '" & Nit_Nuevo & Comi, "CD_NIT_EPS_APR = '" & Nit_Antes & Comi)
   End If
   'JAUM T34758 Fin
   'DRMG T43659 INICIO SE AGREGA TABLAS FALTANTES POR UNIFICAR PARA LA EPS
   If Result <> FAIL Then
      If ExisteTABLA("R_EPS_ESPE") = True Then
         If Result <> FAIL Then Result = DoUpdate("R_EPS_ESPE", "CD_NIT_EPS_EPES=" & Comi & Nit_Nuevo & Comi, "CD_NIT_EPS_EPES=" & Comi & Nit_Antes & Comi)
      End If
   End If
   If Result <> FAIL Then
      If ExisteTABLA("ACT_PAC_RES4505_MES") = True Then
         If Result <> FAIL Then Result = DoUpdate("ACT_PAC_RES4505_MES", "CD_NIT_EPS_APR=" & Comi & Nit_Nuevo & Comi, "CD_NIT_EPS_APR=" & Comi & Nit_Antes & Comi)
      End If
   End If
   'DRMG T43659 FIN
   If Result <> FAIL Then Result = DoDelete("EPS", "CD_NIT_EPS=" & Comi & Nit_Antes & Comi)
End Sub

'HRR Req. 1309
Private Sub Unifica_empleado(Nit_Antes As String, Nit_Nuevo As String)
                Dim ArrPEmp() As Variant 'variable que almacena el tipo de documento del primer empleado
              'empleado que se cambia su identificacion por la del segundo empleado en todos los registros
              'donde se utiliza y se elimina de la BD
                Dim ArrSEmp() As Variant 'variable que almacena el tipo de documento del segundo empleado
 
                ReDim ArrPEmp(0)
                ReDim ArrSEmp(0)
                Result = LoadData("EMPLEADO", "ID_DOCU_EMPL", "NU_NUME_EMPL=" & Comi & Nit_Antes & Comi, ArrPEmp)
                If Result <> FAIL Then Result = LoadData("EMPLEADO", "ID_DOCU_EMPL", "NU_NUME_EMPL=" & Comi & Nit_Nuevo & Comi, ArrSEmp)
                If Result <> FAIL Then
                   If Not Encontro Then
                      ArrSEmp(0) = ArrPEmp(0)
                      Call Crea_Empleado(Nit_Antes, Nit_Nuevo, ArrPEmp(0))
                   End If
                   If ExisteTABLA("AUX_PLANILLA") = True Then
                      Campos = "ID_DOCU_AUPL=" & Comi & ArrSEmp(0) & Comi & Coma & "NU_EMPL_AUPL=" & Comi & Nit_Nuevo & Comi
                      Condicion = "ID_DOCU_AUPL=" & Comi & ArrPEmp(0) & Comi & " AND NU_EMPL_AUPL=" & Comi & Nit_Antes & Comi
                      If Result <> FAIL Then Result = DoUpdate("AUX_PLANILLA", Campos, Condicion)
                   End If
                End If
                If Result <> FAIL Then
                   If ExisteTABLA("RELA_CON_ADSALUD") Then Call Leer_RAdmi_Salud(Nit_Antes, Nit_Nuevo, ArrPEmp(0), ArrSEmp(0))
                End If
                If Result <> FAIL Then
                   If ExisteTABLA("RELA_CON_ARP") Then Call Leer_RArp(Nit_Antes, Nit_Nuevo, ArrPEmp(0), ArrSEmp(0))
                End If
                If Result <> FAIL Then
                   If ExisteTABLA("RELA_CON_PENSIONES") Then Call Leer_RPensiones(Nit_Antes, Nit_Nuevo, ArrPEmp(0), ArrSEmp(0))
                End If
                If Result <> FAIL Then
                   If ExisteTABLA("RELA_CON_CESANTIAS") Then Call Leer_RCesantias(Nit_Antes, Nit_Nuevo, ArrPEmp(0), ArrSEmp(0))
                End If
                If Result <> FAIL Then
                   If ExisteTABLA("RELA_CON_CAJA") Then Call Leer_RCaja_Compe(Nit_Antes, Nit_Nuevo, ArrPEmp(0), ArrSEmp(0))
                End If
                If Result <> FAIL Then
                   If ExisteTABLA("CESANTIAS") Then Call Leer_Cesantias(Nit_Antes, Nit_Nuevo, ArrPEmp(0), ArrSEmp(0))
                End If
                If Result <> FAIL Then
                   If ExisteTABLA("PRESTAMOS") = True Then
                      Campos = "ID_EMPL_PRES=" & Comi & ArrSEmp(0) & Comi & Coma & "NU_EMPL_PRES=" & Comi & Nit_Nuevo & Comi
                      Condicion = "ID_EMPL_PRES=" & Comi & ArrPEmp(0) & Comi & " AND NU_EMPL_PRES=" & Comi & Nit_Antes & Comi
                      If Result <> FAIL Then Result = DoUpdate("PRESTAMOS", Campos, Condicion)
                   End If
                End If
                If Result <> FAIL Then
                   If ExisteTABLA("NOVDEV") Then Call Leer_NovDev(Nit_Antes, Nit_Nuevo, ArrPEmp(0), ArrSEmp(0))
                End If
                If Result <> FAIL Then
                   If ExisteTABLA("NOVDED") Then Call Leer_NovDed(Nit_Antes, Nit_Nuevo, ArrPEmp(0), ArrSEmp(0))
                End If
                If Result <> FAIL Then
                   If ExisteTABLA("NOVTIEMPO") Then Call Leer_NovTiempo(Nit_Antes, Nit_Nuevo, ArrPEmp(0), ArrSEmp(0))
                End If
                If Result <> FAIL Then
                   If ExisteTABLA("LIQUIDACION") Then Call Leer_Liquidacion(Nit_Antes, Nit_Nuevo, ArrPEmp(0), ArrSEmp(0))
                End If
                If Result <> FAIL Then
                   If ExisteTABLA("LIQU_DETA") Then Call Leer_Liqu_Deta(Nit_Antes, Nit_Nuevo, ArrPEmp(0), ArrSEmp(0))
                End If
                If Result <> FAIL Then
                   If ExisteTABLA("CONTRATOS") = True Then
                      Campos = "ID_DOCU_CONT=" & Comi & ArrSEmp(0) & Comi & Coma & "NU_EMPL_CONT=" & Comi & Nit_Nuevo & Comi
                      Condicion = "ID_DOCU_CONT=" & Comi & ArrPEmp(0) & Comi & " AND NU_EMPL_CONT=" & Comi & Nit_Antes & Comi
                      If Result <> FAIL Then Result = DoUpdate("CONTRATOS", Campos, Condicion)
                   End If
                End If
                If Result <> FAIL Then
                   If ExisteTABLA("PRIMAS") Then Call Leer_Primas(Nit_Antes, Nit_Nuevo, ArrPEmp(0), ArrSEmp(0))
                End If
                If Result <> FAIL Then
                   If ExisteTABLA("EMBARGOS") = True Then
                      Campos = "ID_EMPL_EMBA=" & Comi & ArrSEmp(0) & Comi & Coma & "NU_EMPL_EMBA=" & Comi & Nit_Nuevo & Comi
                      Condicion = "ID_EMPL_EMBA=" & Comi & ArrPEmp(0) & Comi & " AND NU_EMPL_EMBA=" & Comi & Nit_Antes & Comi
                      If Result <> FAIL Then Result = DoUpdate("EMBARGOS", Campos, Condicion)
                   End If
                End If
                If Result <> FAIL Then
                   If ExisteTABLA("DEDAUTO") Then Call Leer_Dedauto(Nit_Antes, Nit_Nuevo, ArrPEmp(0), ArrSEmp(0))
                End If
                If Result <> FAIL Then
                   If ExisteTABLA("DEVAUTO") Then Call Leer_Devauto(Nit_Antes, Nit_Nuevo, ArrPEmp(0), ArrSEmp(0))
                End If
                If Result <> FAIL Then
                   If ExisteTABLA("SALDOS_EMPL") = True Then
                      Campos = "ID_EMPL_SAEM=" & Comi & ArrSEmp(0) & Comi & Coma & "NU_EMPL_SAEM=" & Comi & Nit_Nuevo & Comi
                      Condicion = "ID_EMPL_SAEM=" & Comi & ArrPEmp(0) & Comi & " AND NU_EMPL_SAEM=" & Comi & Nit_Antes & Comi
                      If Result <> FAIL Then Result = DoUpdate("SALDOS_EMPL", Campos, Condicion)
                   End If
                End If
                If Result <> FAIL Then
                   If ExisteTABLA("OBLIGACIONES") Then Call Leer_Obligaciones(Nit_Antes, Nit_Nuevo, ArrPEmp(0), ArrSEmp(0))
                End If
                If Result <> FAIL Then
                   If ExisteTABLA("AUX_NOVEDADES") Then Call Leer_Aux_Novedades(Nit_Antes, Nit_Nuevo, ArrPEmp(0), ArrSEmp(0))
                End If
                If Result <> FAIL Then
                   If ExisteTABLA("APRODETA_TEMP") = True Then
                      Campos = "ID_EMPL_ADTE=" & Comi & ArrSEmp(0) & Comi & Coma & "NU_EMPL_ADTE=" & Comi & Nit_Nuevo & Comi
                      Condicion = "ID_EMPL_ADTE=" & Comi & ArrPEmp(0) & Comi & " AND NU_EMPL_ADTE=" & Comi & Nit_Antes & Comi
                      If Result <> FAIL Then Result = DoUpdate("APRODETA_TEMP", Campos, Condicion)
                   End If
                End If
                If Result <> FAIL Then
                   If ExisteTABLA("AUX_SALDOS") = True Then
                      Campos = "ID_EMPL_AUXL=" & Comi & ArrSEmp(0) & Comi & Coma & "NU_EMPL_AUXL=" & Comi & Nit_Nuevo & Comi
                      Condicion = "ID_EMPL_AUXL=" & Comi & ArrPEmp(0) & Comi & " AND NU_EMPL_AUXL=" & Comi & Nit_Antes & Comi
                      If Result <> FAIL Then Result = DoUpdate("AUX_SALDOS", Campos, Condicion)
                   End If
                End If
                If Result <> FAIL Then Result = DoDelete("EMPLEADO", "NU_NUME_EMPL=" & Comi & ArrPEmp(0) & Comi & " AND NU_NUME_EMPL=" & Comi & Nit_Antes & Comi)
      
      
      
End Sub


'HRR Req. 1309
Private Sub Unifica_Admi_Salud(Nit_Antes As String, Nit_Nuevo As String)
   Call Crea_Admi_Salud(Nit_Antes, Nit_Nuevo)
   If ExisteTABLA("RELA_CON_ADSALUD") = True Then
      If Result <> FAIL Then Result = DoUpdate("RELA_CON_ADSALUD", "CD_ADSA_REAS=" & Comi & Nit_Nuevo & Comi, "CD_ADSA_REAS=" & Comi & Nit_Antes & Comi)
   End If
   If ExisteTABLA("APRO_DETA") = True Then
      If Result <> FAIL Then Result = DoUpdate("APRO_DETA", "CD_ADSA_DEAP=" & Comi & Nit_Nuevo & Comi, "CD_ADSA_DEAP=" & Comi & Nit_Antes & Comi)
   End If
   If ExisteTABLA("LIQUIDACION") = True Then
      If Result <> FAIL Then Result = DoUpdate("LIQUIDACION", "CD_ADSA_LIQU=" & Comi & Nit_Nuevo & Comi, "CD_ADSA_LIQU=" & Comi & Nit_Antes & Comi)
   End If
   If Result <> FAIL Then
      If ExisteTABLA("TRANSF_NOMI") Then Call Leer_Transf_NomiPpto(Nit_Antes, Nit_Nuevo)
   End If
   If Result <> FAIL Then Result = DoDelete("ADMI_SALUD", "CD_CODI_ADSA=" & Comi & Nit_Antes & Comi)
   
   
End Sub

'HRR Req. 1309
Private Sub Unifica_Fondo_Pensiones(Nit_Antes As String, Nit_Nuevo As String)
   Call Crea_Fondo_Pensiones(Nit_Antes, Nit_Nuevo)
   If ExisteTABLA("APRO_DETA") = True Then
      If Result <> FAIL Then Result = DoUpdate("APRO_DETA", "CD_FOPE_DEAP=" & Comi & Nit_Nuevo & Comi, "CD_FOPE_DEAP=" & Comi & Nit_Antes & Comi)
   End If
   If ExisteTABLA("LIQUIDACION") = True Then
      If Result <> FAIL Then Result = DoUpdate("LIQUIDACION", "CD_FOPE_LIQU=" & Comi & Nit_Nuevo & Comi, "CD_FOPE_LIQU=" & Comi & Nit_Antes & Comi)
   End If
   If ExisteTABLA("RELA_CON_PENSIONES") = True Then
      If Result <> FAIL Then Result = DoUpdate("RELA_CON_PENSIONES", "CD_FOPE_REPE=" & Comi & Nit_Nuevo & Comi, "CD_FOPE_REPE=" & Comi & Nit_Antes & Comi)
   End If
   If Result <> FAIL Then
      If ExisteTABLA("TRANSF_NOMI") Then Call Leer_Transf_NomiPpto(Nit_Antes, Nit_Nuevo)
   End If
   If Result <> FAIL Then Result = DoDelete("FONDO_PENSIONES", "CD_CODI_FOPE=" & Comi & Nit_Antes & Comi)
End Sub

'HRR Req. 1309
Private Sub Unifica_Arp(Nit_Antes As String, Nit_Nuevo As String)
   Call Crea_Arp(Nit_Antes, Nit_Nuevo)
   If ExisteTABLA("APRO_DETA") = True Then
      If Result <> FAIL Then Result = DoUpdate("APRO_DETA", "CD_ARP_DEAP=" & Comi & Nit_Nuevo & Comi, "CD_ARP_DEAP=" & Comi & Nit_Antes & Comi)
   End If
   If ExisteTABLA("RELA_CON_ARP") = True Then
      If Result <> FAIL Then Result = DoUpdate("RELA_CON_ARP", "CD_ARP_REAP=" & Comi & Nit_Nuevo & Comi, "CD_ARP_REAP=" & Comi & Nit_Antes & Comi)
   End If
   If ExisteTABLA("RE_APRO_DETA") = True Then
      If Result <> FAIL Then Result = DoUpdate("RE_APRO_DETA", "CD_ARP_READ=" & Comi & Nit_Nuevo & Comi, "CD_ARP_READ=" & Comi & Nit_Antes & Comi)
   End If
   If ExisteTABLA("APRODETA_TEMP") = True Then
      If Result <> FAIL Then Result = DoUpdate("APRODETA_TEMP", "CD_ARP_ADTE=" & Comi & Nit_Nuevo & Comi, "CD_ARP_ADTE=" & Comi & Nit_Antes & Comi)
   End If
   If Result <> FAIL Then
      If ExisteTABLA("TRANSF_NOMI") Then Call Leer_Transf_NomiPpto(Nit_Antes, Nit_Nuevo)
   End If
   If Result <> FAIL Then Result = DoDelete("ARP", "CD_CODI_ARP=" & Comi & Nit_Antes & Comi)
End Sub

'HRR Req. 1309
Private Sub Unifica_Fondo_Cesantias(Nit_Antes As String, Nit_Nuevo As String)
   Call Crea_Fondo_Cesantias(Nit_Antes, Nit_Nuevo)
   If ExisteTABLA("LIQUIDACION") = True Then
      If Result <> FAIL Then Result = DoUpdate("LIQUIDACION", "CD_FOCE_LIQU=" & Comi & Nit_Nuevo & Comi, "CD_FOCE_LIQU=" & Comi & Nit_Antes & Comi)
   End If
   If ExisteTABLA("RELA_CON_CESANTIAS") = True Then
      If Result <> FAIL Then Result = DoUpdate("RELA_CON_CESANTIAS", "CD_FOCE_RECE=" & Comi & Nit_Nuevo & Comi, "CD_FOCE_RECE=" & Comi & Nit_Antes & Comi)
   End If
   If Result <> FAIL Then
      If ExisteTABLA("TRANSF_NOMI") Then Call Leer_Transf_NomiPpto(Nit_Antes, Nit_Nuevo)
   End If
   If Result <> FAIL Then Result = DoDelete("FONDO_CESANTIAS", "CD_CODI_FOCE=" & Comi & Nit_Antes & Comi)
End Sub

'HRR Req. 1309
Private Sub Unifica_Caja_Compensacion(Nit_Antes As String, Nit_Nuevo As String)
    Call Crea_Caja_Compensacion(Nit_Antes, Nit_Nuevo)
   If ExisteTABLA("LIQUIDACION") = True Then
      If Result <> FAIL Then Result = DoUpdate("LIQUIDACION", "CD_CJCO_LIQU=" & Comi & Nit_Nuevo & Comi, "CD_CJCO_LIQU=" & Comi & Nit_Antes & Comi)
   End If
   If ExisteTABLA("RELA_CON_CAJA") = True Then
      If Result <> FAIL Then Result = DoUpdate("RELA_CON_CAJA", "CD_CJCO_RECJ=" & Comi & Nit_Nuevo & Comi, "CD_CJCO_RECJ=" & Comi & Nit_Antes & Comi)
   End If
   If ExisteTABLA("APRO_DETA") = True Then
      If Result <> FAIL Then Result = DoUpdate("APRO_DETA", "CD_CJCO_DEAP=" & Comi & Nit_Nuevo & Comi, "CD_CJCO_DEAP=" & Comi & Nit_Antes & Comi)
   End If
   If Result <> FAIL Then
      If ExisteTABLA("TRANSF_NOMI") Then Call Leer_Transf_NomiPpto(Nit_Antes, Nit_Nuevo)
   End If
   If Result <> FAIL Then Result = DoDelete("CAJA_COMPE", "CD_CODI_CJCO=" & Comi & Nit_Antes & Comi)
End Sub


'HRR Req.1309
'recibe el numero de identificacion y el tipo de documento de un dos empleados respectivamente
Private Sub Leer_RAdmi_Salud(Nit_Antes As String, Nit_Nuevo As String, ByVal Tipo_IdPEmp As String, ByVal Tipo_IdSemp As String)
Dim Arr(0)
ReDim Saldo(4, 0)

    Condicion = "ID_DOCU_REAS='" & Tipo_IdPEmp & Comi & " AND NU_NUME_REAS='" & Nit_Antes & Comi
    Result = LoadMulData("RELA_CON_ADSALUD", Asterisco, Condicion, Saldo)
    If Result <> FAIL Then
       If Encontro Then
          Result = DoDelete("RELA_CON_ADSALUD", Condicion)
          If Result = FAIL Then Exit Sub
          For J = 0 To UBound(Saldo(), 2)
             Condicion = "ID_DOCU_REAS = " & Comi & Tipo_IdSemp & Comi
             Condicion = Condicion & " AND NU_NUME_REAS = " & Comi & Nit_Nuevo & Comi
             Condicion = Condicion & " AND FE_FECH_REAS = " & fecha2(Saldo(2, J))
             Result = LoadData("RELA_CON_ADSALUD", "ID_DOCU_REAS", Condicion, Arr())
             If Result = FAIL Then Exit For
             If Arr(0) = NUL$ Then
                Valores = Comi & Tipo_IdSemp & Comi & Coma & Comi & Nit_Nuevo & Comi & Coma
                Valores = Valores & fecha2(Saldo(2, J)) & Coma
                Valores = Valores & Comi & Saldo(3, J) & Comi & Coma & Comi & 0 & Comi
                Result = DoInsert("RELA_CON_ADSALUD", Valores)
             End If
             If Result = FAIL Then Exit For
          Next
       End If
    End If
End Sub

'HRR Req.1309
Private Sub Leer_RArp(Nit_Antes As String, Nit_Nuevo As String, ByVal Tipo_IdPEmp As String, ByVal Tipo_IdSemp As String)
Dim Arr(0)
ReDim Saldo(4, 0)

    Condicion = "ID_DOCU_REAP='" & Tipo_IdPEmp & Comi & " AND NU_NUME_REAP='" & Nit_Antes & Comi
    Result = LoadMulData("RELA_CON_ARP", Asterisco, Condicion, Saldo)
    If Result <> FAIL Then
       If Encontro Then
          Result = DoDelete("RELA_CON_ARP", Condicion)
          If Result = FAIL Then Exit Sub
          For J = 0 To UBound(Saldo(), 2)
             Condicion = "ID_DOCU_REAP = " & Comi & Tipo_IdSemp & Comi
             Condicion = Condicion & " AND NU_NUME_REAP = " & Comi & Nit_Nuevo & Comi
             Condicion = Condicion & " AND FE_FECH_REAP = " & fecha2(Saldo(2, J))
             Result = LoadData("RELA_CON_ARP", "ID_DOCU_REAP", Condicion, Arr())
             If Result = FAIL Then Exit For
             If Arr(0) = NUL$ Then
                Valores = Comi & Tipo_IdSemp & Comi & Coma & Comi & Nit_Nuevo & Comi & Coma
                Valores = Valores & fecha2(Saldo(2, J)) & Coma
                Valores = Valores & Comi & Saldo(3, J) & Comi & Coma & Comi & 0 & Comi
                Result = DoInsert("RELA_CON_ARP", Valores)
             End If
             If Result = FAIL Then Exit For
          Next
       End If
    End If
End Sub

'HRR Req.1309
Private Sub Leer_RPensiones(Nit_Antes As String, Nit_Nuevo As String, ByVal Tipo_IdPEmp As String, ByVal Tipo_IdSemp As String)
Dim Arr(0)
ReDim Saldo(4, 0)

    Condicion = "ID_DOCU_REPE='" & Tipo_IdPEmp & Comi & " AND NU_NUME_REPE='" & Nit_Antes & Comi
    Result = LoadMulData("RELA_CON_PENSIONES", Asterisco, Condicion, Saldo)
    If Result <> FAIL Then
       If Encontro Then
          Result = DoDelete("RELA_CON_PENSIONES", Condicion)
          If Result = FAIL Then Exit Sub
          For J = 0 To UBound(Saldo(), 2)
             Condicion = "ID_DOCU_REPE = " & Comi & Tipo_IdSemp & Comi
             Condicion = Condicion & " AND NU_NUME_REPE = " & Comi & Nit_Nuevo & Comi
             Condicion = Condicion & " AND FE_FECH_REPE = " & fecha2(Saldo(2, J))
             Result = LoadData("RELA_CON_PENSIONES", "ID_DOCU_REPE", Condicion, Arr())
             If Result = FAIL Then Exit For
             If Arr(0) = NUL$ Then
                Valores = Comi & Tipo_IdSemp & Comi & Coma & Comi & Nit_Nuevo & Comi & Coma
                Valores = Valores & fecha2(Saldo(2, J)) & Coma
                Valores = Valores & Comi & Saldo(3, J) & Comi & Coma & Comi & 0 & Comi
                Result = DoInsert("RELA_CON_PENSIONES", Valores)
             End If
             If Result = FAIL Then Exit For
          Next
       End If
    End If
End Sub


'HRR Req.1309
Private Sub Leer_RCesantias(Nit_Antes As String, Nit_Nuevo As String, ByVal Tipo_IdPEmp As String, ByVal Tipo_IdSemp As String)
Dim Arr(0)
ReDim Saldo(4, 0)

    Condicion = "ID_DOCU_RECE='" & Tipo_IdPEmp & Comi & " AND NU_NUME_RECE='" & Nit_Antes & Comi
    Result = LoadMulData("RELA_CON_CESANTIAS", Asterisco, Condicion, Saldo)
    If Result <> FAIL Then
       If Encontro Then
          Result = DoDelete("RELA_CON_CESANTIAS", Condicion)
          If Result = FAIL Then Exit Sub
          For J = 0 To UBound(Saldo(), 2)
             Condicion = "ID_DOCU_RECE = " & Comi & Tipo_IdSemp & Comi
             Condicion = Condicion & " AND NU_NUME_RECE = " & Comi & Nit_Nuevo & Comi
             Condicion = Condicion & " AND FE_FECH_RECE = " & fecha2(Saldo(2, J))
             Result = LoadData("RELA_CON_CESANTIAS", "ID_DOCU_RECE", Condicion, Arr())
             If Result = FAIL Then Exit For
             If Arr(0) = NUL$ Then
                Valores = Comi & Tipo_IdSemp & Comi & Coma & Comi & Nit_Nuevo & Comi & Coma
                Valores = Valores & fecha2(Saldo(2, J)) & Coma
                Valores = Valores & Comi & Saldo(3, J) & Comi & Coma & Comi & 0 & Comi
                Result = DoInsert("RELA_CON_CESANTIAS", Valores)
             End If
             If Result = FAIL Then Exit For
          Next
       End If
    End If
End Sub

'HRR Req.1309
Private Sub Leer_RCaja_Compe(Nit_Antes As String, Nit_Nuevo As String, ByVal Tipo_IdPEmp As String, ByVal Tipo_IdSemp As String)
Dim Arr(0)
ReDim Saldo(4, 0)

    Condicion = "ID_DOCU_RECJ='" & Tipo_IdPEmp & Comi & " AND NU_NUME_RECJ='" & Nit_Antes & Comi
    Result = LoadMulData("RELA_CON_CAJA", Asterisco, Condicion, Saldo)
    If Result <> FAIL Then
       If Encontro Then
          Result = DoDelete("RELA_CON_CAJA", Condicion)
          If Result = FAIL Then Exit Sub
          For J = 0 To UBound(Saldo(), 2)
             Condicion = "ID_DOCU_RECJ = " & Comi & Tipo_IdSemp & Comi
             Condicion = Condicion & " AND NU_NUME_RECJ = " & Comi & Nit_Nuevo & Comi
             Condicion = Condicion & " AND FE_FECH_RECJ = " & fecha2(Saldo(2, J))
             Condicion = Condicion & " AND ID_ESTD_RECJ='1'"
             Result = LoadData("RELA_CON_CAJA", "ID_DOCU_RECJ", Condicion, Arr())
             If Result = FAIL Then Exit For
             If Arr(0) = NUL$ Then
                Valores = Comi & Tipo_IdSemp & Comi & Coma & Comi & Nit_Nuevo & Comi & Coma
                Valores = Valores & fecha2(Saldo(2, J)) & Coma
                Valores = Valores & Comi & Saldo(3, J) & Comi & Coma & Comi & 0 & Comi
                Result = DoInsert("RELA_CON_CAJA", Valores)
             End If
             If Result = FAIL Then Exit For
          Next
       End If
    End If
End Sub

'HRR Req.1309
Private Sub Leer_Cesantias(Nit_Antes As String, Nit_Nuevo As String, ByVal Tipo_IdPEmp As String, ByVal Tipo_IdSemp As String)
Dim Arr(0)
ReDim Saldo(11, 0)

    Condicion = "ID_EMPL_CESA='" & Tipo_IdPEmp & Comi & " AND NU_EMPL_CESA='" & Nit_Antes & Comi
    Result = LoadMulData("CESANTIAS", Asterisco, Condicion, Saldo)
    If Result <> FAIL Then
       If Encontro Then
          Result = DoDelete("CESANTIAS", Condicion)
          If Result = FAIL Then Exit Sub
          For J = 0 To UBound(Saldo(), 2)
             Condicion = "ID_EMPL_CESA = " & Comi & Tipo_IdSemp & Comi
             Condicion = Condicion & " AND NU_EMPL_CESA = " & Comi & Nit_Nuevo & Comi
             Condicion = Condicion & " AND FE_PEIN_CESA = " & fecha2(Saldo(2, J))
             Result = LoadData("CESANTIAS", "ID_EMPL_CESA", Condicion, Arr())
             If Result = FAIL Then Exit For
             If Saldo(3, J) = NUL$ Then Saldo(3, J) = Saldo(2, J)
             If Saldo(4, J) = NUL$ Then Saldo(4, J) = 0
             If Saldo(5, J) = NUL$ Then Saldo(5, J) = 0
             If Saldo(6, J) = NUL$ Then Saldo(6, J) = 0
             If Saldo(7, J) = NUL$ Then Saldo(7, J) = 0
             If Saldo(8, J) = NUL$ Then Saldo(8, J) = Saldo(2, J)
             If Saldo(9, J) = NUL$ Then Saldo(9, J) = 0
             If Saldo(10, J) = NUL$ Then Saldo(10, J) = 0
             If Arr(0) = NUL$ Then
                Valores = Comi & Tipo_IdSemp & Comi & Coma & Comi & Nit_Nuevo & Comi & Coma
                Valores = Valores & fecha2(Saldo(2, J)) & Coma & fecha2(Saldo(3, J)) & Coma
                Valores = Valores & CDbl(Saldo(4, J)) & Coma & CDbl(Saldo(5, J)) & Coma & CDbl(Saldo(6, J)) & Coma
                Valores = Valores & CDbl(Saldo(7, J)) & Coma & fecha2(Saldo(8, J)) & Coma & CDbl(Saldo(9, J)) & Coma
                Valores = Valores & CDbl(Saldo(10, J))
                Result = DoInsert("CESANTIAS", Valores)
             Else
                Valores = " NU_DIAS_CESA = NU_DIAS_CESA + " & CDbl(Saldo(4, J)) & Coma & _
                           " VL_PROM_CESA = VL_PROM_CESA + " & CDbl(Saldo(5, J)) & Coma & _
                           " VL_TOTA_CESA = VL_TOTA_CESA + " & CDbl(Saldo(6, J)) & Coma & _
                           " VL_INCE_CESA = VL_INCE_CESA + " & CDbl(Saldo(7, J)) & Coma & _
                           " VL_SABA_CESA = VL_SABA_CESA + " & CDbl(Saldo(9, J)) & Coma & _
                           " VL_AUTR_CESA = VL_AUTR_CESA + " & CDbl(Saldo(10, J))
                Result = DoUpdate("CESANTIAS", Valores, Condicion)
             End If
             If Result = FAIL Then Exit For
          Next
       End If
    End If
End Sub

'HRR Req.1309
Private Sub Leer_NovDev(Nit_Antes As String, Nit_Nuevo As String, ByVal Tipo_IdPEmp As String, ByVal Tipo_IdSemp As String)
Dim Arr(0)
'ReDim Saldo(8, 0)
ReDim Saldo(11, 0)


    Condicion = "ID_EMPL_NODE='" & Tipo_IdPEmp & Comi & " AND NU_EMPL_NODE='" & Nit_Antes & Comi
    Result = LoadMulData("NOVDEV", Asterisco, Condicion, Saldo)
    If Result <> FAIL Then
       If Encontro Then
          Result = DoDelete("NOVDEV", Condicion)
          If Result = FAIL Then Exit Sub
          For J = 0 To UBound(Saldo(), 2)
             Condicion = "ID_EMPL_NODE = " & Comi & Tipo_IdSemp & Comi
             Condicion = Condicion & " AND NU_EMPL_NODE = " & Comi & Nit_Nuevo & Comi
             Condicion = Condicion & " AND NU_CONS_NODE = " & Saldo(2, J)
             Condicion = Condicion & " AND FE_FECH_NODE = " & fecha2(Saldo(3, J))
             Result = LoadData("NOVDEV", "ID_EMPL_NODE", Condicion, Arr())
             If Result = FAIL Then Exit For
             If Saldo(4, J) = NUL$ Then Saldo(4, J) = 0
             If Saldo(5, J) = NUL$ Then Saldo(5, J) = 0
             If Saldo(6, J) = NUL$ Then Saldo(6, J) = 0
             If Arr(0) = NUL$ Then
                Valores = Comi & Tipo_IdSemp & Comi & Coma & Comi & Nit_Nuevo & Comi & Coma
                Valores = Valores & CDbl(Saldo(2, J)) & Coma & fecha2(Saldo(3, J)) & Coma
                Valores = Valores & CDbl(Saldo(4, J)) & Coma & CDbl(Saldo(5, J)) & Coma & CDbl(Saldo(6, J)) & Coma
                Valores = Valores & Comi & Saldo(7, J) & Comi & Coma & Comi & Saldo(8, J) & Comi
                '---------------------------------------------------------------------------------------------------------------------------
                'DAHV M4104 - INICIO
                Valores = Valores & Coma & Comi & Saldo(9, J) & Comi & Coma & Comi & Saldo(10, J) & Comi
                Valores = Valores & Coma & Comi & Saldo(11, J) & Comi
                'DAHV M4104 - FIN
                '---------------------------------------------------------------------------------------------------------------------------
                
                
                Result = DoInsert("NOVDEV", Valores)
             Else
                Condicion = "ID_EMPL_NODE = " & Comi & Tipo_IdSemp & Comi
                Condicion = Condicion & " AND NU_EMPL_NODE = " & Comi & Nit_Nuevo & Comi
                Condicion = Condicion & " AND FE_FECH_NODE = " & Comi & Saldo(3, J) & Comi
                Result = LoadData("NOVDEV", "MAX (NU_CONS_NODE)", Condicion, Arr())
                If Result = FAIL Then Exit For
                Valores = Comi & Tipo_IdSemp & Comi & Coma & Comi & Nit_Nuevo & Comi & Coma
                Valores = Valores & (CInt(Arr(0)) + 1) & Coma & fecha2(Saldo(3, J)) & Coma
                Valores = Valores & CDbl(Saldo(4, J)) & Coma & CDbl(Saldo(5, J)) & Coma & CDbl(Saldo(6, J)) & Coma
                Valores = Valores & Comi & Saldo(7, J) & Comi & Coma & Comi & Saldo(8, J) & Comi
                '---------------------------------------------------------------------------------------------------------------------------
                'DAHV M4104 - INICIO
                Valores = Valores & Coma & Comi & Saldo(9, J) & Comi & Coma & Comi & Saldo(10, J) & Comi
                Valores = Valores & Coma & Comi & Saldo(11, J) & Comi
                'DAHV M4104 - FIN
                '---------------------------------------------------------------------------------------------------------------------------
                
                Result = DoInsert("NOVDEV", Valores)
             End If
             If Result = FAIL Then Exit For
          Next
       End If
    End If
End Sub

'HRR Req.1309
Private Sub Leer_NovDed(Nit_Antes As String, Nit_Nuevo As String, ByVal Tipo_IdPEmp As String, ByVal Tipo_IdSemp As String)
Dim Arr(0)
'ReDim Saldo(8, 0)
ReDim Saldo(9, 0)

    Condicion = "ID_EMPL_NODU='" & Tipo_IdPEmp & Comi & " AND NU_EMPL_NODU='" & Nit_Antes & Comi
    Result = LoadMulData("NOVDED", Asterisco, Condicion, Saldo)
    If Result <> FAIL Then
       If Encontro Then
          Result = DoDelete("NOVDED", Condicion)
          If Result = FAIL Then Exit Sub
          For J = 0 To UBound(Saldo(), 2)
             Condicion = "ID_EMPL_NODU = " & Comi & Tipo_IdSemp & Comi
             Condicion = Condicion & " AND NU_EMPL_NODU = " & Comi & Nit_Nuevo & Comi
             Condicion = Condicion & " AND NU_CONS_NODU = " & Saldo(2, J)
             Condicion = Condicion & " AND FE_FECH_NODU = " & fecha2(Saldo(3, J))
             Result = LoadData("NOVDED", "ID_EMPL_NODU", Condicion, Arr())
             If Result = FAIL Then Exit For
             If Saldo(4, J) = NUL$ Then Saldo(4, J) = 0
             If Saldo(5, J) = NUL$ Then Saldo(5, J) = 0
             If Saldo(6, J) = NUL$ Then Saldo(6, J) = 0
             If Arr(0) = NUL$ Then
                Valores = Comi & Tipo_IdSemp & Comi & Coma & Comi & Nit_Nuevo & Comi & Coma
                Valores = Valores & CDbl(Saldo(2, J)) & Coma & fecha2(Saldo(3, J)) & Coma
                Valores = Valores & CDbl(Saldo(4, J)) & Coma & CDbl(Saldo(5, J)) & Coma & CDbl(Saldo(6, J)) & Coma
                Valores = Valores & Comi & Saldo(7, J) & Comi & Coma & Comi & Saldo(8, J) & Comi
                ' -------------------------------------------------------------------------------------------------------------------------------------------
                'DAHV M4104 - INICIO
                Valores = Valores & Coma & Comi & Saldo(9, J) & Comi
                'DAHV M4104 - FIN
                ' -------------------------------------------------------------------------------------------------------------------------------------------
                Result = DoInsert("NOVDED", Valores)
             Else
                Condicion = "ID_EMPL_NODU = " & Comi & Tipo_IdSemp & Comi
                Condicion = Condicion & " AND NU_EMPL_NODU = " & Comi & Nit_Nuevo & Comi
                Condicion = Condicion & " AND FE_FECH_NODU = " & Comi & Saldo(3, J) & Comi
                Result = LoadData("NOVDED", "MAX (NU_CONS_NODU)", Condicion, Arr())
                If Result = FAIL Then Exit For
                Valores = Comi & Tipo_IdSemp & Comi & Coma & Comi & Nit_Nuevo & Comi & Coma
                Valores = Valores & (CInt(Arr(0)) + 1) & Coma & fecha2(Saldo(3, J)) & Coma
                Valores = Valores & CDbl(Saldo(4, J)) & Coma & CDbl(Saldo(5, J)) & Coma & CDbl(Saldo(6, J)) & Coma
                Valores = Valores & Comi & Saldo(7, J) & Comi & Coma & Comi & Saldo(8, J) & Comi
                ' -------------------------------------------------------------------------------------------------------------------------------------------
                'DAHV M4104 - INICIO
                Valores = Valores & Coma & Comi & Saldo(9, J) & Comi
                'DAHV M4104 - FIN
                ' -------------------------------------------------------------------------------------------------------------------------------------------
                Result = DoInsert("NOVDED", Valores)
             End If
             If Result = FAIL Then Exit For
          Next
       End If
    End If
End Sub

'HRR Req.1309
Private Sub Leer_NovTiempo(Nit_Antes As String, Nit_Nuevo As String, ByVal Tipo_IdPEmp As String, ByVal Tipo_IdSemp As String)
Dim Arr(0)
'ReDim Saldo(14, 0)
ReDim Saldo(16, 0)

    Condicion = "ID_EMPL_NOTI='" & Tipo_IdPEmp & Comi & " AND NU_EMPL_NOTI='" & Nit_Antes & Comi
    Result = LoadMulData("NOVTIEMPO", Asterisco, Condicion, Saldo)
    If Result <> FAIL Then
       If Encontro Then
          Result = DoDelete("NOVTIEMPO", Condicion)
          If Result = FAIL Then Exit Sub
          For J = 0 To UBound(Saldo(), 2)
             Condicion = "ID_EMPL_NOTI = " & Comi & Tipo_IdSemp & Comi
             Condicion = Condicion & " AND NU_EMPL_NOTI = " & Comi & Nit_Nuevo & Comi
             Condicion = Condicion & " AND NU_CONS_NOTI = " & Saldo(2, J)
             Condicion = Condicion & " AND FE_FECH_NOTI = " & fecha2(Saldo(3, J))
             Result = LoadData("NOVTIEMPO", "ID_EMPL_NOTI", Condicion, Arr())
             If Result = FAIL Then Exit For
             If Saldo(4, J) = NUL$ Then Saldo(4, J) = 0
             If Saldo(5, J) = NUL$ Then Saldo(5, J) = 0
             If Saldo(6, J) = NUL$ Then Saldo(6, J) = 0
             If Saldo(10, J) = NUL$ Then Saldo(10, J) = Saldo(3, J)
             If Saldo(11, J) = NUL$ Then Saldo(11, J) = Saldo(3, J)
             If Saldo(12, J) = NUL$ Then Saldo(12, J) = 0
             If Saldo(13, J) = NUL$ Then Saldo(13, J) = 0
             If Saldo(14, J) = NUL$ Then Saldo(14, J) = 0
             If Arr(0) = NUL$ Then
                Valores = Comi & Tipo_IdSemp & Comi & Coma & Comi & Nit_Nuevo & Comi & Coma
                Valores = Valores & CDbl(Saldo(2, J)) & Coma & fecha2(Saldo(3, J)) & Coma
                Valores = Valores & CDbl(Saldo(4, J)) & Coma & CDbl(Saldo(5, J)) & Coma & CDbl(Saldo(6, J)) & Coma
                Valores = Valores & Comi & Saldo(7, J) & Comi & Coma & Comi & Saldo(8, J) & Comi & Coma
                Valores = Valores & Comi & Saldo(9, J) & Comi & Coma & fecha2(Saldo(10, J)) & Coma
                Valores = Valores & fecha2(Saldo(11, J)) & Coma & CInt(Saldo(12, J)) & Coma
                Valores = Valores & CInt(Saldo(13, J)) & Coma & CInt(Saldo(14, J))
                '----------------------------------------------------------------------------------------------------------------------------
                'DAHV M4104 - INICIO
                Valores = Valores & Coma & Comi & Saldo(15, J) & Comi & Coma & Comi & Saldo(16, J) & Comi
                'DAHV M4104 - FIN
                '----------------------------------------------------------------------------------------------------------------------------
                Result = DoInsert("NOVTIEMPO", Valores)
             Else
                Condicion = "ID_EMPL_NOTI = " & Comi & Tipo_IdSemp & Comi
                Condicion = Condicion & " AND NU_EMPL_NOTI = " & Comi & Nit_Nuevo & Comi
                Condicion = Condicion & " AND FE_FECH_NOTI = " & Comi & Saldo(3, J) & Comi
                'Result = LoadData("NOVDED", "MAX (NU_CONS_NOTI)", Condicion, Arr())
                Result = LoadData("NOVTIEMPO", "MAX (NU_CONS_NOTI)", Condicion, Arr()) 'DAHV M4104
                If Result = FAIL Then Exit For
                Valores = Comi & Tipo_IdSemp & Comi & Coma & Comi & Nit_Nuevo & Comi & Coma
                Valores = Valores & (CInt(Arr(0)) + 1) & Coma & fecha2(Saldo(3, J)) & Coma
                Valores = Valores & CDbl(Saldo(4, J)) & Coma & CDbl(Saldo(5, J)) & Coma & CDbl(Saldo(6, J)) & Coma
                Valores = Valores & Comi & Saldo(7, J) & Comi & Coma & Comi & Saldo(8, J) & Comi & Coma
                Valores = Valores & Comi & Saldo(9, J) & Comi & Coma & fecha2(Saldo(10, J)) & Coma
                Valores = Valores & fecha2(Saldo(11, J)) & Coma & CInt(Saldo(12, J)) & Coma
                Valores = Valores & CInt(Saldo(13, J)) & Coma & CInt(Saldo(14, J))
                '----------------------------------------------------------------------------------------------------------------------------
                'DAHV M4104 - INICIO
                Valores = Valores & Coma & Comi & Saldo(15, J) & Comi & Coma & Comi & Saldo(16, J) & Comi
                'DAHV M4104 - FIN
                '----------------------------------------------------------------------------------------------------------------------------
                Result = DoInsert("NOVTIEMPO", Valores)
             End If
             If Result = FAIL Then Exit For
          Next
       End If
    End If
End Sub

'HRR Req.1309
Private Sub Leer_Liqu_Deta(Nit_Antes As String, Nit_Nuevo As String, ByVal Tipo_IdPEmp As String, ByVal Tipo_IdSemp As String)
Dim Arr(0)
'ReDim Saldo(11, 0) 'DRMG T43659 SE DEJA EN COMENTARIO
   ReDim Saldo(15, 0) 'DRMG T43659

    Condicion = "ID_DOCU_LIDE='" & Tipo_IdPEmp & Comi & " AND NU_EMPL_LIDE='" & Nit_Antes & Comi
    Result = LoadMulData("LIQU_DETA", Asterisco, Condicion, Saldo)
    If Result <> FAIL Then
       If Encontro Then
          Result = DoDelete("LIQU_DETA", Condicion)
          If Result = FAIL Then Exit Sub
          For J = 0 To UBound(Saldo(), 2)
             Condicion = "CD_LIQU_LIDE=" & Comi & Saldo(0, J) & Comi & " AND ID_DOCU_LIDE = " & Comi & Tipo_IdSemp & Comi
             Condicion = Condicion & " AND NU_EMPL_LIDE = " & Comi & Nit_Nuevo & Comi
             Condicion = Condicion & " AND FE_PERI_LIDE = " & fecha2(Saldo(3, J)) & " AND ID_CLAS_LIDE = " & Comi & Saldo(4, J) & Comi
             Condicion = Condicion & " AND ID_TIPO_LIDE = " & Comi & Saldo(5, J) & Comi & " AND CD_CODI_LIDE = " & Comi & Saldo(6, J) & Comi
             Result = LoadData("LIQU_DETA", "CD_LIQU_LIDE", Condicion, Arr())
             If Result = FAIL Then Exit For
             If Saldo(8, J) = NUL$ Then Saldo(8, J) = 0
             If Saldo(9, J) = NUL$ Then Saldo(9, J) = 0
             If Saldo(11, J) = NUL$ Then Saldo(11, J) = 0
             If Arr(0) = NUL$ Then
                Valores = "CD_LIQU_LIDE=" & Comi & Saldo(0, J) & Comi & Coma & "ID_DOCU_LIDE=" & Comi & Tipo_IdSemp & Comi & Coma
                Valores = Valores & "NU_EMPL_LIDE=" & Comi & Nit_Nuevo & Comi & Coma & "FE_PERI_LIDE=" & fecha2(Saldo(3, J)) & Coma
                Valores = Valores & "ID_CLAS_LIDE=" & Comi & Saldo(4, J) & Comi & Coma & "ID_TIPO_LIDE=" & Comi & Saldo(5, J) & Comi & Coma
                Valores = Valores & "CD_CODI_LIDE=" & Comi & Saldo(6, J) & Comi & Coma & "DE_DESC_LIDE=" & Comi & Saldo(7, J) & Comi & Coma
                Valores = Valores & "CT_CANT_LIDE=" & CDbl(Saldo(8, J)) & Coma & "VL_VALO_LIDE=" & CDbl(Saldo(9, J)) & Coma
                Valores = Valores & "NU_INDVAC_LIDE=" & CInt(Saldo(11, J))
                Valores = Valores & Coma & "NU_CONS_LIDE='" & Saldo(14, J) & Comi 'DRMG T43659 SE AGREHA CAMPO FALTANTE PARA INSERTAR
                Result = DoInsertSQL("LIQU_DETA", Valores)
             End If
             If Result = FAIL Then Exit For
          Next
       End If
    End If
End Sub

'HRR Req.1309
Private Sub Leer_Liquidacion(Nit_Antes As String, Nit_Nuevo As String, ByVal Tipo_IdPEmp As String, ByVal Tipo_IdSemp As String)
Dim Arr(0)
ReDim Saldo(17, 0)

    Condicion = "ID_DOCU_LIQU='" & Tipo_IdPEmp & Comi & " AND NU_EMPL_LIQU='" & Nit_Antes & Comi
    Result = LoadMulData("LIQUIDACION", Asterisco, Condicion, Saldo)
    If Result <> FAIL Then
       If Encontro Then
          Result = DoDelete("LIQUIDACION", Condicion)
          If Result = FAIL Then Exit Sub
          For J = 0 To UBound(Saldo(), 2)
             Condicion = "CD_CODI_LIQU=" & Comi & Saldo(0, J) & Comi & " AND ID_DOCU_LIQU = " & Comi & Tipo_IdSemp & Comi
             Condicion = Condicion & " AND NU_EMPL_LIQU = " & Comi & Nit_Nuevo & Comi
             Condicion = Condicion & " AND FE_PERI_LIQU = " & fecha2(Saldo(3, J))
             Result = LoadData("LIQUIDACION", "CD_CODI_LIQU", Condicion, Arr())
             If Result = FAIL Then Exit For
             If Saldo(10, J) = NUL$ Then Saldo(10, J) = 0
             If Saldo(14, J) = NUL$ Then Saldo(14, J) = 0
             If Arr(0) = NUL$ Then
                Valores = Comi & Saldo(0, J) & Comi & Coma & Comi & Tipo_IdSemp & Comi & Coma
                Valores = Valores & Comi & Nit_Nuevo & Comi & Coma & fecha2(Saldo(3, J)) & Coma
                Valores = Valores & Comi & Saldo(4, J) & Comi & Coma & Comi & Saldo(5, J) & Comi & Coma
                Valores = Valores & Comi & Saldo(6, J) & Comi & Coma & Comi & Saldo(7, J) & Comi & Coma
                Valores = Valores & Comi & Saldo(8, J) & Comi & Coma & Comi & Saldo(9, J) & Comi & Coma
                Valores = Valores & CDbl(Saldo(10, J)) & Coma & Comi & Saldo(11, J) & Comi & Coma
                Valores = Valores & Comi & Saldo(12, J) & Comi & Coma & Comi & Saldo(13, J) & Comi & Coma
                Valores = Valores & CDbl(Saldo(14, J)) & Coma & Comi & Saldo(15, J) & Comi & Coma
                 Valores = Valores & Comi & Saldo(16, J) & Comi
                 '& Coma & CDbl(Saldo(17, J))
                Result = DoInsert("LIQUIDACION", Valores)
             End If
             If Result = FAIL Then Exit For
          Next
       End If
    End If
End Sub

'HRR Req.1309
Private Sub Leer_Primas(Nit_Antes As String, Nit_Nuevo As String, ByVal Tipo_IdPEmp As String, ByVal Tipo_IdSemp As String)
Dim Arr(0)
ReDim Saldo(12, 0)

    Condicion = "ID_EMPL_PRIM='" & Tipo_IdPEmp & Comi & " AND NU_EMPL_PRIM='" & Nit_Antes & Comi
    Result = LoadMulData("PRIMAS", Asterisco, Condicion, Saldo)
    If Result <> FAIL Then
       If Encontro Then
          Result = DoDelete("PRIMAS", Condicion)
          If Result = FAIL Then Exit Sub
          For J = 0 To UBound(Saldo(), 2)
             Condicion = "ID_EMPL_PRIM=" & Comi & Tipo_IdSemp & Comi & " AND NU_EMPL_PRIM = " & Comi & Nit_Nuevo & Comi
             Condicion = Condicion & " AND FE_PERI_PRIM = " & fecha2(Saldo(2, J))
             Condicion = Condicion & " AND ID_DEVE_PRIM = " & Comi & Saldo(3, J) & Comi
             Condicion = Condicion & " AND CD_DEVE_PRIM = " & Comi & Saldo(4, J) & Comi
             Result = LoadData("PRIMAS", "ID_EMPL_PRIM", Condicion, Arr())
             If Result = FAIL Then Exit For
             If Saldo(5, J) = NUL$ Then Saldo(5, J) = 0
             If Saldo(6, J) = NUL$ Then Saldo(6, J) = 0
             If Saldo(7, J) = NUL$ Then Saldo(7, J) = 0
             If Saldo(8, J) = NUL$ Then Saldo(8, J) = 0
             If Saldo(9, J) = NUL$ Then Saldo(9, J) = 0
             If Saldo(10, J) = NUL$ Then Saldo(10, J) = 0
             If Saldo(11, J) = NUL$ Then Saldo(11, J) = 0
             If Arr(0) = NUL$ Then
                Valores = Comi & Tipo_IdSemp & Comi & Coma & Comi & Nit_Nuevo & Comi & Coma
                Valores = Valores & fecha2(Saldo(2, J)) & Coma & Comi & Saldo(3, J) & Comi & Coma
                Valores = Valores & Comi & Saldo(4, J) & Comi & Coma & CInt(Saldo(5, J)) & Coma
                Valores = Valores & CDbl(Saldo(6, J)) & Coma & CDbl(Saldo(7, J)) & Coma
                Valores = Valores & CDbl(Saldo(8, J)) & Coma & CDbl(Saldo(9, J)) & Coma
                Valores = Valores & CDbl(Saldo(10, J)) & Coma & CDbl(Saldo(11, J))
                '& Coma & CDbl(Saldo(12, J))
                Result = DoInsert("PRIMAS", Valores)
             End If
             If Result = FAIL Then Exit For
          Next
       End If
    End If
End Sub

'HRR Req.1309
Private Sub Leer_Dedauto(Nit_Antes As String, Nit_Nuevo As String, ByVal Tipo_IdPEmp As String, ByVal Tipo_IdSemp As String)
Dim Arr(0)
ReDim Saldo(7, 0)

    Condicion = "ID_EMPL_DEDA='" & Tipo_IdPEmp & Comi & " AND NU_EMPL_DEDA='" & Nit_Antes & Comi
    Result = LoadMulData("DEDAUTO", Asterisco, Condicion, Saldo)
    If Result <> FAIL Then
       If Encontro Then
          Result = DoDelete("DEDAUTO", Condicion)
          If Result = FAIL Then Exit Sub
          For J = 0 To UBound(Saldo(), 2)
             Condicion = "ID_EMPL_DEDA = " & Comi & Tipo_IdSemp & Comi
             Condicion = Condicion & " AND NU_EMPL_DEDA = " & Comi & Nit_Nuevo & Comi
             Condicion = Condicion & " AND NU_CONS_DEDA = " & Saldo(2, J)
             Result = LoadData("DEDAUTO", "ID_EMPL_DEDA", Condicion, Arr())
             If Result = FAIL Then Exit For
             If Saldo(3, J) = NUL$ Then Saldo(3, J) = 0
             If Saldo(4, J) = NUL$ Then Saldo(4, J) = 0
             If Saldo(5, J) = NUL$ Then Saldo(5, J) = 0
             If Arr(0) = NUL$ Then
                Valores = Comi & Tipo_IdSemp & Comi & Coma & Comi & Nit_Nuevo & Comi & Coma
                Valores = Valores & CDbl(Saldo(2, J)) & Coma & CDbl(Saldo(3, J)) & Coma
                Valores = Valores & CDbl(Saldo(4, J)) & Coma & CDbl(Saldo(5, J)) & Coma
                Valores = Valores & Comi & Saldo(6, J) & Comi & Coma & Comi & Saldo(7, J) & Comi
                Result = DoInsert("DEDAUTO", Valores)
             Else
                Condicion = "ID_EMPL_DEDA = " & Comi & Tipo_IdSemp & Comi
                Condicion = Condicion & " AND NU_EMPL_DEDA = " & Comi & Nit_Nuevo & Comi
                Result = LoadData("DEDAUTO", "MAX (NU_CONS_DEDA)", Condicion, Arr())
                If Result = FAIL Then Exit For
                Valores = Comi & Tipo_IdSemp & Comi & Coma & Comi & Nit_Nuevo & Comi & Coma
                Valores = Valores & (CDbl(Arr(0)) + 1) & Coma & CDbl(Saldo(3, J)) & Coma
                Valores = Valores & CDbl(Saldo(4, J)) & Coma & CDbl(Saldo(5, J)) & Coma
                Valores = Valores & Comi & Saldo(6, J) & Comi & Coma & Comi & Saldo(7, J) & Comi
                Result = DoInsert("DEDAUTO", Valores)
             End If
             If Result = FAIL Then Exit For
          Next
       End If
    End If
End Sub

'HRR Req.1309
Private Sub Leer_Devauto(Nit_Antes As String, Nit_Nuevo As String, ByVal Tipo_IdPEmp As String, ByVal Tipo_IdSemp As String)
Dim Arr(0)
ReDim Saldo(8, 0)

    Condicion = "ID_EMPL_DEVA='" & Tipo_IdPEmp & Comi & " AND NU_EMPL_DEVA='" & Nit_Antes & Comi
    Result = LoadMulData("DEVAUTO", Asterisco, Condicion, Saldo)
    If Result <> FAIL Then
       If Encontro Then
          Result = DoDelete("DEVAUTO", Condicion)
          If Result = FAIL Then Exit Sub
          For J = 0 To UBound(Saldo(), 2)
             Condicion = "ID_EMPL_DEVA = " & Comi & Tipo_IdSemp & Comi
             Condicion = Condicion & " AND NU_EMPL_DEVA = " & Comi & Nit_Nuevo & Comi
             Condicion = Condicion & " AND NU_CONS_DEVA = " & Saldo(2, J)
             Result = LoadData("DEVAUTO", "ID_EMPL_DEVA", Condicion, Arr())
             If Result = FAIL Then Exit For
             If Saldo(3, J) = NUL$ Then Saldo(3, J) = 0
             If Saldo(4, J) = NUL$ Then Saldo(4, J) = 0
             If Saldo(5, J) = NUL$ Then Saldo(5, J) = 0
             If Saldo(8, J) = NUL$ Then Saldo(8, J) = 0
             If Arr(0) = NUL$ Then
                Valores = Comi & Tipo_IdSemp & Comi & Coma & Comi & Nit_Nuevo & Comi & Coma
                Valores = Valores & CDbl(Saldo(2, J)) & Coma & CDbl(Saldo(3, J)) & Coma
                Valores = Valores & CDbl(Saldo(4, J)) & Coma & CDbl(Saldo(5, J)) & Coma
                Valores = Valores & Comi & Saldo(6, J) & Comi & Coma & Comi & Saldo(7, J) & Comi & Coma
                Valores = Valores & CInt(Saldo(8, J))
                Result = DoInsert("DEVAUTO", Valores)
             Else
                Condicion = "ID_EMPL_DEVA = " & Comi & Tipo_IdSemp & Comi
                Condicion = Condicion & " AND NU_EMPL_DEVA = " & Comi & Nit_Nuevo & Comi
                Result = LoadData("DEVAUTO", "MAX (NU_CONS_DEVA)", Condicion, Arr())
                If Result = FAIL Then Exit For
                Valores = Comi & Tipo_IdSemp & Comi & Coma & Comi & Nit_Nuevo & Comi & Coma
                Valores = Valores & (CDbl(Arr(0)) + 1) & Coma & CDbl(Saldo(3, J)) & Coma
                Valores = Valores & CDbl(Saldo(4, J)) & Coma & CDbl(Saldo(5, J)) & Coma
                Valores = Valores & Comi & Saldo(6, J) & Comi & Coma & Comi & Saldo(7, J) & Comi & Coma
                Valores = Valores & CInt(Saldo(8, J))
                Result = DoInsert("DEVAUTO", Valores)
             End If
             If Result = FAIL Then Exit For
          Next
       End If
    End If
End Sub

'HRR Req.1309
Private Sub Leer_Obligaciones(Nit_Antes As String, Nit_Nuevo As String, ByVal Tipo_IdPEmp As String, ByVal Tipo_IdSemp As String)
Dim Arr(0)
'ReDim Saldo(3, 0)
ReDim Saldo(5, 0)  'NMSR R1648-1667-1811

    Condicion = "ID_EMPL_OBLI='" & Tipo_IdPEmp & Comi & " AND NU_EMPL_OBLI='" & Nit_Antes & Comi
    Result = LoadMulData("OBLIGACIONES", Asterisco, Condicion, Saldo)
    If Result <> FAIL Then
       If Encontro Then
          Result = DoDelete("OBLIGACIONES", Condicion)
          If Result = FAIL Then Exit Sub
          For J = 0 To UBound(Saldo(), 2)
             Condicion = "ID_EMPL_OBLI = " & Comi & Tipo_IdSemp & Comi
             Condicion = Condicion & " AND NU_EMPL_OBLI = " & Comi & Nit_Nuevo & Comi
             Result = LoadData("OBLIGACIONES", "ID_EMPL_OBLI", Condicion, Arr())
             If Result = FAIL Then Exit For
             If Saldo(2, J) = NUL$ Then Saldo(2, J) = Nowserver()
             If Saldo(3, J) = NUL$ Then Saldo(3, J) = 0
             If Arr(0) = NUL$ Then
                Valores = Comi & Tipo_IdSemp & Comi & Coma & Comi & Nit_Nuevo & Comi & Coma
                Valores = Valores & fecha2(Saldo(2, J)) & Coma & CDbl(Saldo(3, J))
                'NMSR R1648-1667-1811
                Valores = Valores & Coma & Saldo(4, J) & Coma
                Valores = Valores & Comi & Saldo(5, J) & Comi 'NMSR R1648-1667-1811
                Result = DoInsert("OBLIGACIONES", Valores)
             End If
             If Result = FAIL Then Exit For
          Next
       End If
    End If
End Sub

'HRR Req.1309
Private Sub Leer_Aux_Novedades(Nit_Antes As String, Nit_Nuevo As String, ByVal Tipo_IdPEmp As String, ByVal Tipo_IdSemp As String)
Dim Arr(0)
ReDim Saldo(13, 0)

    Condicion = "ID_EMPL_AUXN='" & Tipo_IdPEmp & Comi & " AND NU_EMPL_AUXN='" & Nit_Antes & Comi
    Result = LoadMulData("AUX_NOVEDADES", Asterisco, Condicion, Saldo)
    If Result <> FAIL Then
       If Encontro Then
          Result = DoDelete("AUX_NOVEDADES", Condicion)
          If Result = FAIL Then Exit Sub
          For J = 0 To UBound(Saldo(), 2)
             Condicion = "ID_EMPL_AUXN = " & Comi & Tipo_IdSemp & Comi & " AND NU_EMPL_AUXN = " & Comi & Nit_Nuevo & Comi
             Condicion = Condicion & " AND FE_FECH_AUXN = " & fecha2(Saldo(3, J))
             Condicion = Condicion & " AND ID_CLAS_AUXN = " & Comi & Saldo(4, J) & Comi
             Result = LoadData("AUX_NOVEDADES", "ID_EMPL_AUXN", Condicion, Arr())
             If Result = FAIL Then Exit For
             If Saldo(5, J) = NUL$ Then Saldo(5, J) = 0
             If Saldo(6, J) = NUL$ Then Saldo(6, J) = 0
             If Saldo(7, J) = NUL$ Then Saldo(7, J) = 0
             If Saldo(8, J) = NUL$ Then Saldo(8, J) = Nowserver()
             If Saldo(9, J) = NUL$ Then Saldo(9, J) = Nowserver()
             If Arr(0) = NUL$ Then
                Valores = Comi & Tipo_IdSemp & Comi & Coma & Comi & Nit_Nuevo & Comi & Coma
                Valores = Valores & CDbl(Saldo(2, J)) & Coma & fecha2(Saldo(3, J)) & Coma
                Valores = Valores & Comi & Saldo(4, J) & Comi & Coma & CDbl(Saldo(5, J)) & Coma
                Valores = Valores & CDbl(Saldo(6, J)) & Coma & CDbl(Saldo(7, J)) & Coma
                Valores = Valores & fecha2(Saldo(8, J)) & Coma & fecha2(Saldo(9, J)) & Coma
                Valores = Valores & Comi & Saldo(10, J) & Comi & Coma & Comi & Saldo(11, J) & Comi & Coma
                Valores = Valores & Comi & Saldo(12, J) & Comi
                Result = DoInsert("AUX_NOVEDADES", Valores)
             Else
                Condicion = "ID_EMPL_AUXN = " & Comi & Tipo_IdSemp & Comi & " AND NU_EMPL_AUXN = " & Comi & Nit_Nuevo & Comi
                Condicion = Condicion & " AND FE_FECH_AUXN = " & fecha2(Saldo(3, J))
                Condicion = Condicion & " AND ID_CLAS_AUXN = " & Comi & Saldo(4, J) & Comi
                Result = LoadData("AUX_NOVEDADES", "MAX (NU_CONS_AUXN)", Condicion, Arr())
                If Result = FAIL Then Exit For
                Valores = Comi & Tipo_IdSemp & Comi & Coma & Comi & Nit_Nuevo & Comi & Coma
                Valores = Valores & (CDbl(Arr(0)) + 1) & Coma & fecha2(Saldo(3, J)) & Coma
                Valores = Valores & Comi & Saldo(4, J) & Comi & Coma & CDbl(Saldo(5, J)) & Coma
                Valores = Valores & CDbl(Saldo(6, J)) & Coma & CDbl(Saldo(7, J)) & Coma
                Valores = Valores & fecha2(Saldo(8, J)) & Coma & fecha2(Saldo(9, J)) & Coma
                Valores = Valores & Comi & Saldo(10, J) & Comi & Coma & Comi & Saldo(11, J) & Comi & Coma
                Valores = Valores & Comi & Saldo(12, J) & Comi
                Result = DoInsert("AUX_NOVEDADES", Valores)
             End If
             If Result = FAIL Then Exit For
          Next
       End If
    End If
End Sub

Private Sub Leer_Transf_NomiPpto(Nit_Antes As String, Nit_Nuevo As String)

Dim Arr(0)
ReDim Saldo(4, 0)

    Condicion = "CD_NIT_TRPP=" & Comi & Nit_Antes & Comi
    Result = LoadMulData("TRANSF_NOMIPPTO", Asterisco, Condicion, Saldo)
    If Result <> FAIL Then
       If Encontro Then
          Result = DoDelete("TRANSF_NOMIPPTO", Condicion)
          If Result = FAIL Then Exit Sub
          For J = 0 To UBound(Saldo(), 2)
              Condicion = "ID_CODI_TRPP = " & Saldo(0, J)
              Condicion = Condicion & " AND CD_CUEN_TRPP = " & Comi & Saldo(1, J) & Comi
              Condicion = Condicion & " AND CD_NIT_TRPP = " & Comi & Nit_Nuevo & Comi
              Condicion = Condicion & " AND CD_TIEM_TRPP = " & Comi & Saldo(3, J) & Comi
              Result = LoadData("TRANSF_NOMIPPTO", "CD_CUEN_TRPP", Condicion, Arr())
              If Result = FAIL Then Exit For
              If Arr(0) = NUL$ Then
                 Valores = Saldo(0, J) & Coma & Comi & Saldo(1, J) & Comi & Coma
                 Valores = Valores & Comi & Nit_Nuevo & Comi & Coma & Comi & Saldo(3, J) & Comi & Coma
                 Valores = Valores & Comi & Saldo(4, J) & Comi
                 Result = DoInsert("TRANSF_NOMIPPTO", Valores)
              End If
              If Result = FAIL Then Exit For
          Next
       End If
    End If


End Sub

'HRR Req.1309
Private Sub Leer_Cuota_TipoAfiliacion(Nit_Antes As String, Nit_Nuevo As String)
Dim Arr(0)
ReDim Saldo(13, 0)

    Condicion = "CD_NIT_EPS_CUTA='" & Nit_Antes & Comi
    Result = LoadMulData("CUOTA_TIPOAFIL", Asterisco, Condicion, Saldo)
    If Result <> FAIL Then
       If Encontro Then
          Result = DoDelete("CUOTA_TIPOAFIL", Condicion)
          If Result = FAIL Then Exit Sub
          For J = 0 To UBound(Saldo(), 2)
             Condicion = "CD_CODI_REG_CUTA = " & Comi & Saldo(0, J) & Comi
             Condicion = Condicion & " AND CD_NIT_EPS_CUTA = " & Comi & Nit_Nuevo & Comi
             Condicion = Condicion & " AND NU_NIVEL_CUTA = " & Saldo(12, J)
             Result = LoadData("CUOTA_TIPOAFIL", "NU_AUTO_CUTA", Condicion, Arr())
             If Result = FAIL Then Exit For
             If Saldo(2, J) = NUL$ Then Saldo(2, J) = 0
             If Saldo(3, J) = NUL$ Then Saldo(3, J) = 0
             If Saldo(4, J) = NUL$ Then Saldo(4, J) = 0
             If Saldo(5, J) = NUL$ Then Saldo(5, J) = 0
             If Saldo(6, J) = NUL$ Then Saldo(6, J) = 0
             If Saldo(7, J) = NUL$ Then Saldo(7, J) = 0
             If Saldo(8, J) = NUL$ Then Saldo(8, J) = 0
             If Saldo(9, J) = NUL$ Then Saldo(9, J) = 0
             If Saldo(10, J) = NUL$ Then Saldo(10, J) = 0
             If Saldo(11, J) = NUL$ Then Saldo(11, J) = 0
             If Saldo(12, J) = NUL$ Then Saldo(12, J) = 0
             If Arr(0) = NUL$ Then
                Valores = "CD_CODI_REG_CUTA=" & Comi & Saldo(0, J) & Comi & Coma & "CD_NIT_EPS_CUTA=" & Comi & Nit_Nuevo & Comi & Coma & "NU_CONS_CUTA=" & CInt(Saldo(2, J)) & Coma
                Valores = Valores & "VL_CONS_CUTA=" & CDbl(Saldo(3, J)) & Coma & "NU_PROC_CUTA=" & CInt(Saldo(4, J)) & Coma & "VL_PROC_CUTA=" & CDbl(Saldo(5, J)) & Coma & "NU_AYDX_CUTA=" & CInt(Saldo(6, J)) & Coma
                Valores = Valores & "VL_AYDX_CUTA=" & CDbl(Saldo(7, J)) & Coma & "NU_ELEM_CUTA=" & CInt(Saldo(8, J)) & Coma & "VL_ELEM_CUTA=" & CDbl(Saldo(9, J)) & Coma & "NU_PRQU_CUTA=" & CInt(Saldo(10, J)) & Coma
                Valores = Valores & "VL_PRQU_CUTA=" & CDbl(Saldo(11, J)) & Coma & "NU_NIVEL_CUTA=" & CInt(Saldo(12, J))
             '& Coma & saldo(13, J)
                Result = DoInsertSQL("CUOTA_TIPOAFIL", Valores)
             End If
             If Result = FAIL Then Exit For
          Next
       End If
    End If
End Sub


'HRR Req.1309
Private Sub Leer_Paciente_Eps(Nit_Antes As String, Nit_Nuevo As String)
Dim Arr(0)
ReDim Saldo(6, 0)

    Condicion = "CD_NIT_EPS_RPE='" & Nit_Antes & Comi
    Result = LoadMulData("R_PAC_EPS", Asterisco, Condicion, Saldo)
    If Result <> FAIL Then
       If Encontro Then
          Result = DoDelete("R_PAC_EPS", Condicion)
          If Result = FAIL Then Exit Sub
          For J = 0 To UBound(Saldo(), 2)
              Condicion = "NU_HIST_PAC_RPE = " & Comi & Saldo(0, J) & Comi
              Condicion = Condicion & " AND CD_NIT_EPS_RPE = " & Comi & Nit_Nuevo & Comi
              Condicion = Condicion & " AND NU_AFIL_RPE = " & Saldo(4, J)
              Result = LoadData("R_PAC_EPS", "CD_NIT_EPS_RPE", Condicion, Arr())
              If Result = FAIL Then Exit For
              If Saldo(5, J) = NUL$ Then Saldo(5, J) = 0
              If Arr(0) = NUL$ Then
                 Valores = Comi & Saldo(0, J) & Comi & Coma & _
                           Comi & Nit_Nuevo & Comi & Coma & _
                           Comi & Saldo(2, J) & Comi & Coma & _
                           Comi & Saldo(3, J) & Comi & Coma & _
                           CDbl(Saldo(4, J)) & Coma & _
                           CDbl(Saldo(5, J)) & Coma & _
                           Comi & Saldo(6, J) & Comi
                 Result = DoInsert("R_PAC_EPS", Valores)
              End If
              If Result = FAIL Then Exit For
          Next
       End If
    End If
    ReDim Saldo(4, 0)
    Condicion = "CD_NIT_EPS_RRE='" & Nit_Antes & Comi
    Result = LoadMulData("R_REG_EPS", Asterisco, Condicion, Saldo)
    If Result <> FAIL Then
       If Encontro Then Result = DoDelete("R_REG_EPS", Condicion)
    End If
End Sub


'HRR Req.1309
Private Sub Leer_Regimen_Eps(Nit_Antes As String, Nit_Nuevo As String)
Dim Arr(0)
ReDim Saldo(4, 0)

    Condicion = "CD_NIT_EPS_RRE='" & Nit_Antes & Comi
    Result = LoadMulData("R_REG_EPS", Asterisco, Condicion, Saldo)
    If Result <> FAIL Then
       If Encontro Then
          'Result = DoDelete("R_REG_EPS", Condicion)
          If Result = FAIL Then Exit Sub
          For J = 0 To UBound(Saldo(), 2)
              Condicion = "CD_CODI_REG_RRE = " & Comi & Saldo(0, J) & Comi
              Condicion = Condicion & " AND CD_NIT_EPS_RRE = " & Comi & Nit_Nuevo & Comi
              Result = LoadData("R_REG_EPS", "CD_NIT_EPS_RRE", Condicion, Arr())
              If Result = FAIL Then Exit For
              If Saldo(2, J) = NUL$ Then Saldo(2, J) = 0
              If Saldo(3, J) = NUL$ Then Saldo(3, J) = 0
              If Saldo(4, J) = NUL$ Then Saldo(4, J) = 0
              If Arr(0) = NUL$ Then
                 Valores = Comi & Saldo(0, J) & Comi & Coma & _
                           Comi & Nit_Nuevo & Comi & Coma & _
                           CDbl(Saldo(2, J)) & Coma & _
                           CDbl(Saldo(3, J)) & Coma & _
                           CDbl(Saldo(4, J))
                 Result = DoInsert("R_REG_EPS", Valores)
              End If
              If Result = FAIL Then Exit For
          Next
       End If
    End If
End Sub


'HRR Req.1309
Private Sub Leer_Parametros_Impuestos(Nit_Antes As String, Nit_Nuevo As String)
Dim Arr(0)
'ReDim Saldo(19, 0)
ReDim Saldo(20, 0) 'LDCR PNC T10675 'DSMM T27990 SE DEJA ESTA LINEA ENCOMENTARIO
'ReDim Saldo(21, 0) 'LDCR PNC T10675 'DRMG T43659 sE DEJA EN COMENTARIO
   ReDim Saldo(22, 0) 'DRMG T43659
    Condicion = "CD_CODI_TERC_PAIM='" & Nit_Antes & Comi
    Result = LoadMulData("PARAMETROS_IMPUESTOS", Asterisco, Condicion, Saldo)
    If Result <> FAIL Then
       If Encontro Then
          Result = DoDelete("PARAMETROS_IMPUESTOS", Condicion)
          If Result = FAIL Then Exit Sub
          For J = 0 To UBound(Saldo(), 2)
              Condicion = " CD_CODI_TERC_PAIM=" & Comi & Nit_Nuevo & Comi
              Result = LoadData("PARAMETROS_IMPUESTOS", "CD_CODI_TERC_PAIM", Condicion, Arr())
              If Result = FAIL Then Exit For
              If Arr(0) = NUL$ Then
                 Valores = Comi & Nit_Nuevo & Comi & Coma & _
                           Comi & Saldo(1, J) & Comi & Coma & _
                           Comi & Saldo(2, J) & Comi & Coma & _
                           Comi & Saldo(3, J) & Comi & Coma & _
                           Comi & Saldo(4, J) & Comi & Coma & _
                           Comi & Saldo(5, J) & Comi & Coma & _
                           Comi & Saldo(6, J) & Comi & Coma & _
                           Comi & Saldo(7, J) & Comi & Coma & _
                           Comi & Saldo(8, J) & Comi & Coma & _
                           Comi & Saldo(9, J) & Comi & Coma & _
                           Comi & Saldo(10, J) & Comi & Coma
                 Valores = Valores & Comi & Saldo(11, J) & Comi & Coma & _
                           Comi & Saldo(12, J) & Comi & Coma & _
                           Comi & Saldo(13, J) & Comi & Coma & _
                           Comi & Saldo(14, J) & Comi & Coma & _
                           Comi & Saldo(15, J) & Comi & Coma & _
                           Comi & Saldo(16, J) & Comi & Coma & _
                           Comi & Saldo(17, J) & Comi & Coma & _
                           Comi & Saldo(18, J) & Comi & Coma & _
                           Comi & Saldo(19, J) & Comi
                 'INICIO LDCR PNC T10675
                 Valores = Valores & Coma & Comi & Saldo(20, J) & Comi
                 'FIN LDCR PNC T10675
                 Valores = Valores & Coma & Comi & Saldo(21, J) & Comi 'DSMM T27990
                 Valores = Valores & Coma & Comi & Saldo(22, J) & Comi 'DRMG T43659 SE AGREGA CAMPO FALTANTE QUE CORRESPONDE A NU_PLAZO_CONT_PAIM
                 Result = DoInsert("PARAMETROS_IMPUESTOS", Valores)
              End If
              If Result = FAIL Then Exit For
          Next
       End If
    End If
End Sub

Private Sub Leer_Cuenta_Tercero(Nit_Antes As String, Nit_Nuevo As String)
Dim Arr(0)
'ReDim Saldo(6, 0)
ReDim Saldo(14, 0) 'LDCR T10675
    Condicion = "CD_TERC_RCT='" & Nit_Antes & Comi
    Result = LoadMulData("CUENTA_TERCERO", Asterisco, Condicion, Saldo)
    If Result <> FAIL Then
       If Encontro Then
          Result = DoDelete("CUENTA_TERCERO", Condicion)
          If Result = FAIL Then Exit Sub
          For J = 0 To UBound(Saldo(), 2)
              Condicion = " CD_CUEN_RCT=" & Comi & Saldo(0, J) & Comi & _
                          " AND CD_TERC_RCT=" & Comi & Nit_Nuevo & Comi & _
                          " AND NU_MES_RCT=" & Saldo(2, J)
              Result = LoadData("CUENTA_TERCERO", "CD_CUEN_RCT", Condicion, Arr())
              If Result = FAIL Then Exit For
              If Saldo(2, J) = NUL$ Then Saldo(2, J) = 0
              If Saldo(3, J) = NUL$ Then Saldo(3, J) = 0
              If Saldo(4, J) = NUL$ Then Saldo(4, J) = 0
              If Saldo(5, J) = NUL$ Then Saldo(5, J) = 0
              If Saldo(6, J) = NUL$ Then Saldo(6, J) = 0
              'INICIO LDCR T10675
              If Saldo(7, J) = NUL$ Then Saldo(7, J) = 0
              If Saldo(8, J) = NUL$ Then Saldo(8, J) = 0
              If Saldo(9, J) = NUL$ Then Saldo(9, J) = 0
              If Saldo(10, J) = NUL$ Then Saldo(10, J) = 0
              If Saldo(11, J) = NUL$ Then Saldo(11, J) = 0
              If Saldo(12, J) = NUL$ Then Saldo(12, J) = 0
              If Saldo(13, J) = NUL$ Then Saldo(13, J) = 0
              If Saldo(14, J) = NUL$ Then Saldo(14, J) = 0
              'FIN LDCR T10675
              ''JACC Depuracion Nov 2008
              If Saldo(3, J) <> "0" Then Saldo(3, J) = Format(Saldo(3, J), "####0.#0")
              If Saldo(4, J) <> "0" Then Saldo(4, J) = Format(Saldo(4, J), "####0.#0")
              If Saldo(5, J) <> "0" Then Saldo(5, J) = Format(Saldo(5, J), "####0.#0")
              If Saldo(6, J) <> "0" Then Saldo(6, J) = Format(Saldo(6, J), "####0.#0")
              'JACC Depuracion Nov 2008
              'INICIO LDCR T10675
              If Saldo(7, J) <> "0" Then Saldo(7, J) = Format(Saldo(7, J), "#####0.#0")
              If Saldo(8, J) <> "0" Then Saldo(8, J) = Format(Saldo(8, J), "#####0.#0")
              If Saldo(9, J) <> "0" Then Saldo(9, J) = Format(Saldo(9, J), "#####0.#0")
              If Saldo(10, J) <> "0" Then Saldo(10, J) = Format(Saldo(10, J), "#####0.#0")
              If Saldo(11, J) <> "0" Then Saldo(11, J) = Format(Saldo(11, J), "#####0.#0")
              If Saldo(12, J) <> "0" Then Saldo(12, J) = Format(Saldo(12, J), "#####0.#0")
              If Saldo(13, J) <> "0" Then Saldo(13, J) = Format(Saldo(13, J), "#####0.#0")
              If Saldo(14, J) <> "0" Then Saldo(14, J) = Format(Saldo(14, J), "#####0.#0")
              'FIN LDCR T10675
              
              If Arr(0) = NUL$ Then
                 Valores = Comi & Saldo(0, J) & Comi & Coma & _
                           Comi & Nit_Nuevo & Comi & Coma & _
                           CDbl(Saldo(2, J)) & Coma & _
                           CDbl(Saldo(3, J)) & Coma & _
                           CDbl(Saldo(4, J)) & Coma & _
                           CDbl(Saldo(5, J)) & Coma & _
                           CDbl(Saldo(6, J))
                 'INICIO LDCR T10675
                 'INICIO LDCR PNC T10675
'                 Valores = Valores & CDbl(Saldo(7, J))
'                 Valores = Valores & CDbl(Saldo(8, J))
'                 Valores = Valores & CDbl(Saldo(9, J))
'                 Valores = Valores & CDbl(Saldo(10, J))
'                 Valores = Valores & CDbl(Saldo(11, J))
'                 Valores = Valores & CDbl(Saldo(12, J))
'                 Valores = Valores & CDbl(Saldo(13, J))
'                 Valores = Valores & CDbl(Saldo(14, J))
                 Valores = Valores & Coma & CDbl(Saldo(7, J)) & Coma
                 Valores = Valores & CDbl(Saldo(8, J)) & Coma
                 Valores = Valores & CDbl(Saldo(9, J)) & Coma
                 Valores = Valores & CDbl(Saldo(10, J)) & Coma
                 Valores = Valores & CDbl(Saldo(11, J)) & Coma
                 Valores = Valores & CDbl(Saldo(12, J)) & Coma
                 Valores = Valores & CDbl(Saldo(13, J)) & Coma
                 Valores = Valores & CDbl(Saldo(14, J))
                 'FIN LDCR PNC T10675
                 'FIN LDCR T10675
                 Result = DoInsert("CUENTA_TERCERO", Valores)
              Else
                 Valores = " VL_DEBI_RCT = VL_DEBI_RCT + " & CDbl(Saldo(3, J)) & Coma & _
                           " VL_CRED_RCT = VL_CRED_RCT + " & CDbl(Saldo(4, J)) & Coma & _
                           " VL_BADB_RCT = VL_BADB_RCT + " & CDbl(Saldo(5, J)) & Coma & _
                           " VL_BACR_RCT = VL_BACR_RCT + " & CDbl(Saldo(6, J))
                 'INICIO LDCR T10675
                 'INICIO LDCR PNC T10675
'                 Valores = Valores & "VL_CREDIVA_RCT=VL_CREDIVA_RCT + " & CDbl(Saldo(7, J))
'                 Valores = Valores & "VL_CREDNIVA_RCT=VL_CREDNIVA_RCT + " & CDbl(Saldo(8, J))
'                 Valores = Valores & "VL_DEBINIVA_RCT=VL_DEBINIVA_RCT + " & CDbl(Saldo(9, J))
'                 Valores = Valores & "VL_CREDBASE_RCT=VL_CREDBASE_RCT + " & CDbl(Saldo(10, J))
'                 Valores = Valores & "VL_DEBIBASE_RCT=VL_DEBIBASE_RCT + " & CDbl(Saldo(11, J))
'                 Valores = Valores & "VL_CREDNBASE_RCT=VL_CREDNBASE_RCT + " & CDbl(Saldo(12, J))
'                 Valores = Valores & "VL_DEBINBASE_RCT=VL_DEBINBASE_RCT + " & CDbl(Saldo(13, J))
'                 Valores = Valores & "VL_DEBIIVA_RCT=VL_DEBIIVA_RCT + " & CDbl(Saldo(14, J))
                 Valores = Valores & ",VL_CREDIVA_RCT=VL_CREDIVA_RCT + " & CDbl(Saldo(7, J))
                 Valores = Valores & ",VL_CREDNIVA_RCT=VL_CREDNIVA_RCT + " & CDbl(Saldo(8, J))
                 Valores = Valores & ",VL_DEBINIVA_RCT=VL_DEBINIVA_RCT + " & CDbl(Saldo(9, J))
                 Valores = Valores & ",VL_CREDBASE_RCT=VL_CREDBASE_RCT + " & CDbl(Saldo(10, J))
                 Valores = Valores & ",VL_DEBIBASE_RCT=VL_DEBIBASE_RCT + " & CDbl(Saldo(11, J))
                 Valores = Valores & ",VL_CREDNBASE_RCT=VL_CREDNBASE_RCT + " & CDbl(Saldo(12, J))
                 Valores = Valores & ",VL_DEBINBASE_RCT=VL_DEBINBASE_RCT + " & CDbl(Saldo(13, J))
                 Valores = Valores & ",VL_DEBIIVA_RCT=VL_DEBIIVA_RCT + " & CDbl(Saldo(14, J))
                 'FIN LDCR PNC T10675
                 'FIN LDCR T10675
                 Result = DoUpdate("CUENTA_TERCERO", Valores, Condicion)
              End If
              If Result = FAIL Then Exit For
          Next
       End If
    End If
End Sub
Private Sub Leer_Cuenta_CETE(Nit_Antes As String, Nit_Nuevo As String)
Dim Arr(0)
ReDim Saldo(7, 0)

    Condicion = "CD_TERC_RCCT='" & Nit_Antes & Comi
    Result = LoadMulData("CUENTA_CETE", Asterisco, Condicion, Saldo)
    If Result <> FAIL Then
       If Encontro Then
          Result = DoDelete("CUENTA_CETE", Condicion)
          If Result = FAIL Then Exit Sub
          For J = 0 To UBound(Saldo(), 2)
              Condicion = " CD_CUEN_RCCT=" & Comi & Saldo(0, J) & Comi & _
                          " AND CD_CECO_RCCT =" & Comi & Saldo(1, J) & Comi & _
                          " AND CD_TERC_RCCT=" & Comi & Nit_Nuevo & Comi & _
                          " AND NU_MES_RCCT=" & Saldo(3, J)
              Result = LoadData("CUENTA_CETE", "CD_CUEN_RCCT", Condicion, Arr())
              If Result = FAIL Then Exit For
              If Saldo(3, J) = NUL$ Then Saldo(3, J) = 0
              If Saldo(4, J) = NUL$ Then Saldo(4, J) = 0
              If Saldo(5, J) = NUL$ Then Saldo(5, J) = 0
              If Saldo(6, J) = NUL$ Then Saldo(6, J) = 0
              If Saldo(7, J) = NUL$ Then Saldo(7, J) = 0
              
              ''JACC Depuracion Nov 2008
              If Saldo(4, J) <> "0" Then Saldo(4, J) = Format(Saldo(4, J), "####0.#0")
              If Saldo(5, J) <> "0" Then Saldo(5, J) = Format(Saldo(5, J), "####0.#0")
              If Saldo(6, J) <> "0" Then Saldo(6, J) = Format(Saldo(6, J), "####0.#0")
              If Saldo(7, J) <> "0" Then Saldo(7, J) = Format(Saldo(7, J), "####0.#0")
              'JACC Depuracion Nov 2008
              If Arr(0) = NUL$ Then
                 Valores = Comi & Saldo(0, J) & Comi & Coma & _
                           Comi & Saldo(1, J) & Comi & Coma & _
                           Comi & Nit_Nuevo & Comi & Coma & _
                           CDbl(Saldo(3, J)) & Coma & _
                           CDbl(Saldo(4, J)) & Coma & _
                           CDbl(Saldo(5, J)) & Coma & _
                           CDbl(Saldo(6, J)) & Coma & _
                           CDbl(Saldo(7, J))
                 Result = DoInsert("CUENTA_CETE", Valores)
              Else
                 Valores = " VL_DEBI_RCCT = VL_DEBI_RCCT + " & CDbl(Saldo(4, J)) & Coma & _
                           " VL_CRED_RCCT = VL_CRED_RCCT + " & CDbl(Saldo(5, J)) & Coma & _
                           " VL_BADB_RCCT = VL_BADB_RCCT + " & CDbl(Saldo(6, J)) & Coma & _
                           " VL_BACR_RCCT = VL_BACR_RCCT + " & CDbl(Saldo(7, J))
                 Result = DoUpdate("CUENTA_CETE", Valores, Condicion)
              End If
              If Result = FAIL Then Exit For
          Next
       End If
    End If
End Sub

Private Sub Leer_Saldo_Tercero(Nit_Antes As String, Nit_Nuevo As String)
Dim Arr(0)
ReDim Saldo(2, 0)

    Condicion = "CD_TERC_SALT='" & Nit_Antes & Comi
    Result = LoadMulData("SALDO_TERCERO", Asterisco, Condicion, Saldo)
    If Result <> FAIL Then
       If Encontro Then
          Result = DoDelete("SALDO_TERCERO", Condicion)
          If Result = FAIL Then Exit Sub
          For J = 0 To UBound(Saldo(), 2)
              Condicion = " CD_CUEN_SALT=" & Comi & Saldo(0, J) & Comi & _
                          " AND CD_TERC_SALT=" & Comi & Nit_Nuevo & Comi
              Result = LoadData("SALDO_TERCERO", "CD_CUEN_SALT", Condicion, Arr())
              If Result = FAIL Then Exit For
              If Saldo(2, J) = NUL$ Then Saldo(2, J) = 0
              If Saldo(2, J) <> "0" Then Saldo(2, J) = Format(Saldo(2, J), "####0.#0") 'JACC DEPURACION NOV 2008
              If Arr(0) = NUL$ Then
                 Valores = Comi & Saldo(0, J) & Comi & Coma & _
                           Comi & Nit_Nuevo & Comi & Coma & _
                           CDbl(Saldo(2, J))
                 Result = DoInsert("SALDO_TERCERO", Valores)
              Else
                 Valores = " VL_SALD_SALT = VL_SALD_SALT + " & CDbl(Saldo(2, J))
                 Result = DoUpdate("SALDO_TERCERO", Valores, Condicion)
              End If
              If Result = FAIL Then Exit For
          Next
       End If
    End If
End Sub

Private Sub Leer_Saldo_CeTe(Nit_Antes As String, Nit_Nuevo As String)
Dim Arr(0)
ReDim Saldo(3, 0)

    Condicion = "CD_TERC_SACT='" & Nit_Antes & Comi
    Result = LoadMulData("SALDO_CETE", Asterisco, Condicion, Saldo)
    If Result <> FAIL Then
       If Encontro Then
          Result = DoDelete("SALDO_CETE", Condicion)
          If Result = FAIL Then Exit Sub
          For J = 0 To UBound(Saldo(), 2)
              Condicion = " CD_CUEN_SACT=" & Comi & Saldo(0, J) & Comi & _
                          " AND CD_CECO_SACT =" & Comi & Saldo(1, J) & Comi & _
                          " AND CD_TERC_SACT=" & Comi & Nit_Nuevo & Comi
              Result = LoadData("SALDO_CETE", "CD_CUEN_SACT", Condicion, Arr())
              If Result = FAIL Then Exit For
              If Saldo(3, J) = NUL$ Then Saldo(3, J) = 0
              If Saldo(3, J) <> "0" Then Saldo(3, J) = Format(Saldo(3, J), "####0.#0") 'JACC DEPURACION NOV 2008
              If Arr(0) = NUL$ Then
                 Valores = Comi & Saldo(0, J) & Comi & Coma & _
                           Comi & Saldo(1, J) & Comi & Coma & _
                           Comi & Nit_Nuevo & Comi & Coma & _
                           CDbl(Saldo(3, J))
                 Result = DoInsert("SALDO_CETE", Valores)
              Else
                 Valores = " VL_SALD_SACT = VL_SALD_SACT + " & CDbl(Saldo(3, J))
                 Result = DoUpdate("SALDO_CETE", Valores, Condicion)
              End If
              If Result = FAIL Then Exit For
          Next
       End If
    End If
End Sub

'HRR Req.1309
Private Sub Leer_Codigobar_Tercero(Nit_Antes As String, Nit_Nuevo As String)
Dim Arr(0)
ReDim Saldo(5, 0)

    Condicion = "CD_CODI_TERC_RCBTE='" & Nit_Antes & Comi
    Result = LoadMulData("IN_R_CODIGOBAR_TERCERO", Asterisco, Condicion, Saldo)
    If Result <> FAIL Then
       If Encontro Then
          Result = DoDelete("IN_R_CODIGOBAR_TERCERO", Condicion)
          If Result = FAIL Then Exit Sub
          For J = 0 To UBound(Saldo(), 2)
              Condicion = " TX_COBA_COBA_RCBTE=" & Comi & Saldo(0, J) & Comi & _
                          " AND CD_CODI_TERC_RCBTE=" & Comi & Nit_Nuevo & Comi
              Result = LoadData("IN_R_CODIGOBAR_TERCERO", "TX_COBA_COBA_RCBTE", Condicion, Arr())
              If Result = FAIL Then Exit For
              If Saldo(2, J) = NUL$ Then Saldo(2, J) = 0
              If Saldo(3, J) = NUL$ Then Saldo(3, J) = 0
              If Saldo(4, J) = NUL$ Then Saldo(4, J) = 0
              If Saldo(5, J) = NUL$ Then Saldo(5, J) = 0
              If Arr(0) = NUL$ Then
                 Valores = Comi & Saldo(0, J) & Comi & Coma & _
                           Comi & Nit_Nuevo & Comi & Coma & _
                           CInt(Saldo(2, J)) & Coma & _
                           CInt(Saldo(3, J)) & Coma & _
                           CInt(Saldo(4, J)) & Coma & _
                           CInt(Saldo(5, J))
                 Result = DoInsert("IN_R_CODIGOBAR_TERCERO", Valores)
              Else
                 Valores = " NU_MULT_RCBTE = NU_MULT_RCBTE + " & CInt(Saldo(2, J)) & Coma & _
                           " NU_DIVI_RCBTE = NU_DIVI_RCBTE + " & CInt(Saldo(3, J)) & Coma & _
                           " NU_COST_RCBTE = NU_COST_RCBTE + " & CInt(Saldo(4, J)) & Coma & _
                           " NU_IMPU_RCBTE = NU_IMPU_RCBTE + " & CInt(Saldo(5, J))
                 Result = DoUpdate("IN_R_CODIGOBAR_TERCERO", Valores, Condicion)
              End If
              If Result = FAIL Then Exit For
          Next
       End If
    End If
End Sub

'HRR Req.1309

Private Sub Leer_Criterio_Tercero(Nit_Antes As String, Nit_Nuevo As String)
Dim Arr(0)
ReDim Saldo(3, 0)

    Condicion = "CD_CODI_TERC_RCRTE='" & Nit_Antes & Comi
    Result = LoadMulData("IN_R_CRITERIO_TERCERO", Asterisco, Condicion, Saldo)
    If Result <> FAIL Then
       If Encontro Then
          Result = DoDelete("N_R_CRITERIO_TERCERO", Condicion)
          If Result = FAIL Then Exit Sub
          For J = 0 To UBound(Saldo(), 2)
              Condicion = " NU_AUTO_CRIT_RCRTE=" & CInt(Saldo(0, J)) & _
                          " AND CD_CODI_TERC_RCRTE=" & Comi & Nit_Nuevo & Comi
              Result = LoadData("N_R_CRITERIO_TERCERO", "NU_AUTO_CRIT_RCRTE", Condicion, Arr())
              If Result = FAIL Then Exit For
              If Saldo(2, J) = NUL$ Then Saldo(2, J) = 0
              If Saldo(3, J) = NUL$ Then Saldo(3, J) = 0
              If Arr(0) = NUL$ Then
                 Valores = IIf(Saldo(0, J) = NUL$, 0, CInt(Saldo(0, J))) & Coma & _
                           Comi & Nit_Nuevo & Comi & Coma & _
                           CDbl(Saldo(2, J)) & Coma & _
                           CDbl(Saldo(3, J))
                 Result = DoInsert("N_R_CRITERIO_TERCERO", Valores)
              Else
                 Valores = " NU_CALI_RCRTE = NU_CALI_RCRTE + " & CDbl(Saldo(2, J)) & Coma & _
                           " NU_PESO_RCRTE = NU_PESO_RCRTE + " & CDbl(Saldo(3, J))
                 Result = DoUpdate("N_R_CRITERIO_TERCERO", Valores, Condicion)
              End If
              If Result = FAIL Then Exit For
          Next
       End If
    End If
End Sub

'HRR Req.1309
Private Sub Leer_Extracto_Inf(Nit_Antes As String, Nit_Nuevo As String)
Dim Arr(0)
ReDim Saldo(10, 0)

    Condicion = "CD_TERC_TEMP='" & Nit_Antes & Comi
    Result = LoadMulData("EXTRACTO_INF", Asterisco, Condicion, Saldo)
    If Result <> FAIL Then
       If Encontro Then
          Result = DoDelete("EXTRACTO_INF", Condicion)
          If Result = FAIL Then Exit Sub
          For J = 0 To UBound(Saldo(), 2)
              Condicion = " CD_TERC_TEMP=" & Comi & Nit_Nuevo & Comi & _
                          " AND DE_DOCU_TEMP=" & Comi & Saldo(1, J) & Comi & _
                          " AND CD_CONC_TEMP=" & Comi & Saldo(2, J) & Comi & _
                          " AND CD_NUME_TEMP=" & CDbl(Saldo(3, J))
              Result = LoadData("EXTRACTO_INF", "CD_TERC_TEMP", Condicion, Arr())
              If Result = FAIL Then Exit For
              If Saldo(4, J) = NUL$ Then Saldo(4, J) = Nowserver()
              If Saldo(5, J) = NUL$ Then Saldo(5, J) = 0
              If Saldo(6, J) = NUL$ Then Saldo(6, J) = 0
              If Saldo(7, J) = NUL$ Then Saldo(7, J) = Nowserver()
              If Saldo(8, J) = NUL$ Then Saldo(8, J) = 0
              If Saldo(9, J) = NUL$ Then Saldo(9, J) = 0
              If Arr(0) = NUL$ Then
                 Valores = Comi & Nit_Nuevo & Comi & Coma & _
                           Comi & Saldo(1, J) & Comi & Coma & _
                           Comi & Saldo(2, J) & Comi & Coma & _
                           CDbl(Saldo(3, J)) & Coma & _
                           fecha2(Saldo(4, J)) & Coma & _
                           CDbl(Saldo(5, J)) & Coma & _
                           CDbl(Saldo(6, J)) & Coma & _
                           fecha2(Saldo(7, J)) & Coma & _
                           CDbl(Saldo(8, J)) & Coma & _
                           CInt(Saldo(9, J)) & Coma & _
                           Comi & Saldo(10, J) & Comi
                 Result = DoInsert("EXTRACTO_INF", Valores)
              Else
                 Valores = " VL_CARG_TEMP = VL_CARG_TEMP + " & CDbl(Saldo(5, J)) & Coma & _
                           " VL_ABON_TEMP = VL_ABON_TEMP + " & CDbl(Saldo(6, J))
                 Result = DoUpdate("EXTRACTO_INF", Valores, Condicion)
              End If
              If Result = FAIL Then Exit For
          Next
       End If
    End If
End Sub

'---------------------------------------------------------------------------------------
' Procedure : Actualizar_Saldos
' DateTime  : 25/04/2018 08:51
' Author    : daniel_mesa
' Purpose   : DRMG T43373 'Actualiza los saldos de las tablas que recibe
'---------------------------------------------------------------------------------------
'
Function Actualizar_Saldos(StTercero As String, StPrimary As String, StTabla As String, StCampos As String, StNitAnt As String, StNitNue As String, InCam As Integer) As Integer
   Dim InI As Double 'Utilziada para los recorridos del for
   Dim InA As Integer 'Utilziada para los recorridos del for
   Dim VrArr() As Variant 'Almacena la informaci�n de las conmsultas
   Dim StCampP() As String 'Almacena los campos que son llave primaria
   Dim StCampR() As String 'Almacena los campos que no son llave primaria
   Dim StCampT() As String 'Almacena los campos unidos
   
   StCampP = Split(StPrimary, ",")
   StCampR = Split(StCampos, ",")
   StCampT = Split(StPrimary & "," & StCampos, ",")
   ReDim VrArr(InCam - 2, 0)
   Desde = StTabla
   Campos = StPrimary & Coma & StCampos
   Condicion = StTercero & "=" & Comi & StNitAnt & Comi
   Result = LoadMulData(Desde, Campos, Condicion, VrArr)
   If Result <> FAIL Then
      If Encontro Then
         Result = DoDelete(Desde, Condicion)
         If Result <> FAIL Then
            For InI = 0 To UBound(VrArr, 2)
               If Result <> FAIL Then
                  Condicion = NUL$
                  For InA = 0 To UBound(StCampP, 1)
                     Condicion = Condicion & IIf(Condicion = NUL$, NUL$, " AND ") & StCampP(InA) & "=" & Comi & VrArr(InA, InI) & Comi
                  Next InA
                  Condicion = Condicion & " AND " & StTercero & "=" & Comi & StNitNue & Comi
                  If fnDevDato(Desde, "COUNT(*)", Condicion, True) > "0" Then
                     Valores = NUL$
                     For InA = UBound(StCampP) + 1 To UBound(StCampT)
                        Valores = Valores & StCampT(InA) & "=" & StCampT(InA) & "+" & IIf(VrArr(InA, InI) = NUL$, 0, VrArr(InA, InI)) & Coma
                     Next InA
                     Valores = Mid(Valores, 1, Len(Valores) - 1)
                     Result = DoUpdate(Desde, Valores, Condicion)
                  Else
                     Valores = NUL$
                     For InA = 0 To InCam - 2
                        Valores = Valores & StCampT(InA) & "=" & Comi & VrArr(InA, InI) & Comi & Coma
                     Next InA
                     Valores = Valores & StTercero & "=" & Comi & StNitNue & Comi
                     Result = DoInsertSQL(Desde, Valores)
                  End If
               End If
            Next InI
         End If
      End If
   End If
   Actualizar_Saldos = Result
End Function


