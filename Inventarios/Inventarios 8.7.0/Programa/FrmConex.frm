VERSION 5.00
Begin VB.Form FrmConexion 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Conectar a ..."
   ClientHeight    =   3075
   ClientLeft      =   4185
   ClientTop       =   2970
   ClientWidth     =   4320
   Icon            =   "FrmConex.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   3075
   ScaleWidth      =   4320
   ShowInTaskbar   =   0   'False
   Begin VB.CommandButton Cmdcancelar 
      Caption         =   "&CANCELAR"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   6
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   495
      Left            =   3360
      Picture         =   "FrmConex.frx":000C
      Style           =   1  'Graphical
      TabIndex        =   6
      Top             =   1560
      Width           =   855
   End
   Begin VB.CommandButton CmdExaminar 
      Caption         =   "&EXAMINAR"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   6
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   495
      Left            =   3360
      Picture         =   "FrmConex.frx":034E
      Style           =   1  'Graphical
      TabIndex        =   5
      Top             =   840
      Width           =   855
   End
   Begin VB.TextBox TxtRed 
      Height          =   285
      Left            =   120
      TabIndex        =   2
      Top             =   2640
      Width           =   3135
   End
   Begin VB.DriveListBox Unidad 
      Height          =   315
      Left            =   120
      TabIndex        =   0
      Top             =   600
      Width           =   3135
   End
   Begin VB.DirListBox Directorio 
      Height          =   1215
      Left            =   120
      TabIndex        =   1
      Top             =   1080
      Width           =   3135
   End
   Begin VB.Label Label2 
      Caption         =   "Nombre del servidor"
      Height          =   255
      Left            =   120
      TabIndex        =   4
      Top             =   2400
      Width           =   1575
   End
   Begin VB.Label lbl 
      AutoSize        =   -1  'True
      Caption         =   "Seleccione el directorio de trabajo para realizar la conexi�n"
      Height          =   195
      Left            =   120
      TabIndex        =   3
      Top             =   240
      Width           =   4155
   End
End
Attribute VB_Name = "FrmConexion"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub CmdCancelar_Click()
   Unload Me
End Sub

Private Sub CmdExaminar_Click()
   DirDB = Directorio.Path & "\"
   Result = SetINIString("Paciente", "dirtrab", DirDB)
   'If Proyect = 1 Then
   '   DBEngine.RegisterDatabase BDDSNODBCName, "Microsoft Access Driver (*.mdb)", True, "DEFAULTDIR=" & DirDB & vbCr & "DBQ=" & BDNameStr & ".mdb"
   'End If
   Login.Caption = Me.Caption & "  [" & DirDB & BDNameStr & "]"
   Unload Me
End Sub

Private Sub Form_Load()
   Call CenterForm(MDI_Inventarios, Me)
   On Error Resume Next
   Directorio.Path = DirDB
   Unidad.Drive = Directorio.Path
End Sub

Private Sub TxtRed_GotFocus()
TxtRed.Tag = TxtRed
End Sub

Private Sub TxtRed_KeyPress(KeyAscii As Integer)
  Call Cambiar_Enter(KeyAscii)
End Sub

Private Sub TxtRed_LostFocus()
If TxtRed <> TxtRed.Tag Then
   If WarnMsg("Desea conectarse con este servidor") Then
      On Error GoTo Error
      Call MouseClock
      If Trim(TxtRed.Text) <> "" Then
        Directorio.Path = TxtRed.Text
      End If
      On Error GoTo 0
   Else
      TxtRed = TxtRed.Tag
   End If
End If
Call MouseNorm
Exit Sub
Error:
   If ERR.Number = 68 Then
      Unidad.Drive = Directorio.Path
   Else
      Call Mensaje1(ERR.Number & ": " & ERR.Description & TxtRed.Text, 1)
      
   End If
   TxtRed = TxtRed.Tag
Call MouseNorm
End Sub

Private Sub Unidad_Change()
  On Error GoTo Error
  Directorio.Path = Unidad.Drive
  On Error GoTo 0
  Exit Sub
Error:
   If ERR.Number = 68 Then
      Unidad.Drive = Directorio.Path
   Else
      Call Mensaje1(ERR.Number & ": " & ERR.Description, 1)
   End If
End Sub

