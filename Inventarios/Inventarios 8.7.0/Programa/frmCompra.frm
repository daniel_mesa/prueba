VERSION 5.00
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "MSFLXGRD.OCX"
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "mscomct2.ocx"
Object = "{C932BA88-4374-101B-A56C-00AA003668DC}#1.1#0"; "MSMASK32.OCX"
Object = "{0BA686C6-F7D3-101A-993E-0000C0EF6F5E}#1.0#0"; "THREED32.OCX"
Begin VB.Form frmFaseCompra 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Compra de art�culos"
   ClientHeight    =   6210
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   9975
   Icon            =   "frmCompra.frx":0000
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   ScaleHeight     =   6210
   ScaleWidth      =   9975
   Begin MSMask.MaskEdBox MskFechFactura 
      BeginProperty DataFormat 
         Type            =   1
         Format          =   "dd/MM/yyyy"
         HaveTrueFalseNull=   0
         FirstDayOfWeek  =   0
         FirstWeekOfYear =   0
         LCID            =   9226
         SubFormatType   =   3
      EndProperty
      Height          =   300
      Left            =   8040
      TabIndex        =   2
      Top             =   1800
      Width           =   1215
      _ExtentX        =   2143
      _ExtentY        =   529
      _Version        =   393216
      MaxLength       =   10
      Format          =   "dd/mmm/yyyy"
      Mask            =   "##/##/####"
      PromptChar      =   "_"
   End
   Begin VB.CommandButton cmdBusca 
      Enabled         =   0   'False
      Height          =   375
      Left            =   6840
      Picture         =   "frmCompra.frx":058A
      Style           =   1  'Graphical
      TabIndex        =   79
      ToolTipText     =   "Buscar."
      Top             =   420
      Width           =   375
   End
   Begin VB.TextBox txtTercero 
      Height          =   285
      Left            =   960
      TabIndex        =   77
      Top             =   110
      Width           =   1575
   End
   Begin VB.CommandButton cmdStart 
      Enabled         =   0   'False
      Height          =   375
      Left            =   7320
      Picture         =   "frmCompra.frx":09BC
      Style           =   1  'Graphical
      TabIndex        =   65
      ToolTipText     =   "Iniciar diligenciamiento."
      Top             =   420
      Width           =   400
   End
   Begin VB.CommandButton cmdClean 
      Enabled         =   0   'False
      Height          =   375
      Left            =   7320
      Picture         =   "frmCompra.frx":17AA
      Style           =   1  'Graphical
      TabIndex        =   66
      ToolTipText     =   "Nueva compra de activos."
      Top             =   60
      Width           =   400
   End
   Begin VB.CommandButton cmdClearImp 
      Enabled         =   0   'False
      Height          =   285
      Left            =   120
      Picture         =   "frmCompra.frx":1E64
      Style           =   1  'Graphical
      TabIndex        =   57
      ToolTipText     =   "Eliminar impuestos de la compra."
      Top             =   1920
      Width           =   405
   End
   Begin VB.CommandButton cmdImpuestos 
      Enabled         =   0   'False
      Height          =   285
      Left            =   120
      Picture         =   "frmCompra.frx":21B6
      Style           =   1  'Graphical
      TabIndex        =   11
      ToolTipText     =   "Agregar impuestos/descuentos a la compra."
      Top             =   1560
      Width           =   405
   End
   Begin VB.TextBox txtObser 
      Enabled         =   0   'False
      Height          =   735
      Left            =   6960
      MultiLine       =   -1  'True
      ScrollBars      =   2  'Vertical
      TabIndex        =   5
      Top             =   4560
      Width           =   2895
   End
   Begin VB.CommandButton cmdPPTO 
      Caption         =   "&Presupuesto"
      Height          =   630
      Left            =   7320
      Picture         =   "frmCompra.frx":2508
      Style           =   1  'Graphical
      TabIndex        =   12
      ToolTipText     =   "Seleccionar registro presupuestal"
      Top             =   4785
      Visible         =   0   'False
      Width           =   1095
   End
   Begin VB.CommandButton cmdXLS 
      Enabled         =   0   'False
      Height          =   505
      Left            =   120
      Picture         =   "frmCompra.frx":2BC2
      Style           =   1  'Graphical
      TabIndex        =   10
      ToolTipText     =   "Ver resumen del movimiento contable"
      Top             =   3720
      Visible         =   0   'False
      Width           =   375
   End
   Begin VB.TextBox txtDPlazo 
      Enabled         =   0   'False
      Height          =   285
      Left            =   8040
      MaxLength       =   4
      TabIndex        =   4
      Text            =   "0"
      Top             =   3000
      Width           =   495
   End
   Begin VB.ComboBox cmbAuto 
      Height          =   315
      Left            =   7920
      Style           =   1  'Simple Combo
      TabIndex        =   25
      Top             =   480
      Width           =   1335
   End
   Begin VB.TextBox txtN�mFact 
      Enabled         =   0   'False
      Height          =   285
      Left            =   8040
      MaxLength       =   20
      MultiLine       =   -1  'True
      ScrollBars      =   2  'Vertical
      TabIndex        =   1
      Top             =   1080
      Width           =   1335
   End
   Begin MSMask.MaskEdBox txtFecha 
      BeginProperty DataFormat 
         Type            =   1
         Format          =   "d/MMM/yyyy"
         HaveTrueFalseNull=   0
         FirstDayOfWeek  =   0
         FirstWeekOfYear =   0
         LCID            =   9226
         SubFormatType   =   3
      EndProperty
      Height          =   300
      Left            =   8040
      TabIndex        =   3
      Top             =   2400
      Width           =   1215
      _ExtentX        =   2143
      _ExtentY        =   529
      _Version        =   393216
      Enabled         =   0   'False
      MaxLength       =   10
      Format          =   "dd/mmm/yyyy"
      Mask            =   "##/##/####"
      PromptChar      =   "_"
   End
   Begin MSMask.MaskEdBox txtFechaV 
      BeginProperty DataFormat 
         Type            =   1
         Format          =   "d/MMM/yyyy"
         HaveTrueFalseNull=   0
         FirstDayOfWeek  =   0
         FirstWeekOfYear =   0
         LCID            =   9226
         SubFormatType   =   3
      EndProperty
      Height          =   300
      Left            =   8040
      TabIndex        =   59
      Top             =   3600
      Width           =   1215
      _ExtentX        =   2143
      _ExtentY        =   529
      _Version        =   393216
      Enabled         =   0   'False
      MaxLength       =   10
      Format          =   "dd/mmm/yyyy"
      Mask            =   "##/##/####"
      PromptChar      =   "_"
   End
   Begin VB.Frame fraValores 
      Enabled         =   0   'False
      Height          =   1695
      Left            =   240
      TabIndex        =   18
      Top             =   4440
      Width           =   6495
      Begin VB.Label lblTIOtros 
         Alignment       =   1  'Right Justify
         BackColor       =   &H80000005&
         BorderStyle     =   1  'Fixed Single
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   120
         TabIndex        =   52
         Top             =   960
         Width           =   1845
      End
      Begin VB.Label lblEtiq 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Otros"
         Height          =   195
         Index           =   20
         Left            =   2040
         TabIndex        =   51
         Top             =   960
         Width           =   375
      End
      Begin VB.Label lblTIIca 
         Alignment       =   1  'Right Justify
         BackColor       =   &H80000005&
         BorderStyle     =   1  'Fixed Single
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   120
         TabIndex        =   50
         Top             =   240
         Width           =   1845
      End
      Begin VB.Label lblEtiq 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "ICA"
         Height          =   195
         Index           =   18
         Left            =   2040
         TabIndex        =   49
         Top             =   240
         Width           =   255
      End
      Begin VB.Label lblTIRet 
         Alignment       =   1  'Right Justify
         BackColor       =   &H80000005&
         BorderStyle     =   1  'Fixed Single
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   120
         TabIndex        =   48
         Top             =   1320
         Width           =   1845
      End
      Begin VB.Label lblEtiq 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Rete. IVA"
         Height          =   195
         Index           =   17
         Left            =   2040
         TabIndex        =   47
         Top             =   600
         Width           =   690
      End
      Begin VB.Label lblEtiq 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Retenci�n"
         Height          =   195
         Index           =   16
         Left            =   2040
         TabIndex        =   46
         Top             =   1320
         Width           =   735
      End
      Begin VB.Label lblTIRIVA 
         Alignment       =   1  'Right Justify
         BackColor       =   &H80000005&
         BorderStyle     =   1  'Fixed Single
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   120
         TabIndex        =   45
         Top             =   600
         Width           =   1845
      End
      Begin VB.Label lblDesc 
         Alignment       =   1  'Right Justify
         BackColor       =   &H80000005&
         BorderStyle     =   1  'Fixed Single
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   3240
         TabIndex        =   27
         Top             =   600
         Width           =   2205
      End
      Begin VB.Label lblEtiq 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Descuentos"
         Height          =   195
         Index           =   7
         Left            =   5520
         TabIndex        =   26
         Top             =   600
         Width           =   855
      End
      Begin VB.Label lblEtiq 
         AutoSize        =   -1  'True
         Caption         =   "Total"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   9
         Left            =   5520
         TabIndex        =   24
         Top             =   1320
         Width           =   450
      End
      Begin VB.Label lblTotal 
         Alignment       =   1  'Right Justify
         BackColor       =   &H80000005&
         BorderStyle     =   1  'Fixed Single
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   3240
         TabIndex        =   23
         Top             =   1320
         Width           =   2205
      End
      Begin VB.Label lblEtiq 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Impuestos"
         Height          =   195
         Index           =   8
         Left            =   5520
         TabIndex        =   22
         Top             =   960
         Width           =   720
      End
      Begin VB.Label lblImp 
         Alignment       =   1  'Right Justify
         BackColor       =   &H80000005&
         BorderStyle     =   1  'Fixed Single
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   3240
         TabIndex        =   21
         Top             =   960
         Width           =   2205
      End
      Begin VB.Label lblEtiq 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Total bruto"
         Height          =   195
         Index           =   6
         Left            =   5520
         TabIndex        =   20
         Top             =   240
         Width           =   765
      End
      Begin VB.Label lblTotalB 
         Alignment       =   1  'Right Justify
         BackColor       =   &H80000005&
         BorderStyle     =   1  'Fixed Single
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   3240
         TabIndex        =   19
         Top             =   240
         Width           =   2205
      End
   End
   Begin VB.Frame fraImpDes 
      Height          =   1575
      Left            =   240
      TabIndex        =   31
      Top             =   4440
      Width           =   6495
      Begin VB.PictureBox Picture1 
         Appearance      =   0  'Flat
         ForeColor       =   &H80000008&
         Height          =   750
         Left            =   120
         ScaleHeight     =   720
         ScaleWidth      =   3105
         TabIndex        =   32
         Top             =   720
         Width           =   3135
         Begin VB.CommandButton cmdImpInfo 
            Enabled         =   0   'False
            Height          =   270
            Left            =   1080
            Picture         =   "frmCompra.frx":2D0C
            Style           =   1  'Graphical
            TabIndex        =   34
            ToolTipText     =   "Informaci�n de los impuestos asignados."
            Top             =   370
            Width           =   405
         End
         Begin VB.CommandButton cmdClearImpComp 
            Enabled         =   0   'False
            Height          =   270
            Left            =   1080
            Picture         =   "frmCompra.frx":3296
            Style           =   1  'Graphical
            TabIndex        =   36
            ToolTipText     =   "Eliminar impuestos a la compra."
            Top             =   120
            Width           =   405
         End
         Begin VB.CommandButton cmdImpCompr 
            Caption         =   "Impuestos"
            Height          =   525
            Left            =   120
            Picture         =   "frmCompra.frx":3820
            Style           =   1  'Graphical
            TabIndex        =   35
            ToolTipText     =   "Agregar impuestos/descuentos a la compra."
            Top             =   120
            Width           =   915
         End
         Begin VB.TextBox txtFlete 
            Height          =   285
            Left            =   1800
            TabIndex        =   33
            Tag             =   "0"
            Text            =   "0"
            Top             =   360
            Width           =   1215
         End
         Begin VB.Label lblEtiq 
            AutoSize        =   -1  'True
            BackStyle       =   0  'Transparent
            Caption         =   "Fletes:"
            Height          =   195
            Index           =   13
            Left            =   1800
            TabIndex        =   38
            Top             =   120
            Width           =   465
         End
         Begin VB.Label lblEtiq 
            Alignment       =   2  'Center
            AutoSize        =   -1  'True
            BackStyle       =   0  'Transparent
            Caption         =   "+"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   13.5
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   360
            Index           =   11
            Left            =   1545
            TabIndex        =   37
            Top             =   240
            Width           =   225
         End
      End
      Begin VB.Label lblEtiq 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Total F."
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   10
         Left            =   5280
         TabIndex        =   44
         Top             =   1350
         Width           =   675
      End
      Begin VB.Label lblImpAT 
         Alignment       =   1  'Right Justify
         BackColor       =   &H80000005&
         BorderStyle     =   1  'Fixed Single
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   3360
         TabIndex        =   43
         Top             =   960
         Width           =   1845
      End
      Begin VB.Label lblEtiq 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Impuestos al T."
         Height          =   195
         Index           =   12
         Left            =   5280
         TabIndex        =   42
         Top             =   975
         Width           =   1080
      End
      Begin VB.Label lblTotalN 
         Alignment       =   1  'Right Justify
         BackColor       =   &H80000005&
         BorderStyle     =   1  'Fixed Single
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   3360
         TabIndex        =   41
         Top             =   1320
         Width           =   1845
      End
      Begin VB.Label lblEtiq 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Descuentos al T."
         Height          =   195
         Index           =   15
         Left            =   5280
         TabIndex        =   40
         Top             =   630
         Width           =   1215
      End
      Begin VB.Label lblDescAT 
         Alignment       =   1  'Right Justify
         BackColor       =   &H80000005&
         BorderStyle     =   1  'Fixed Single
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   3360
         TabIndex        =   39
         Top             =   600
         Width           =   1845
      End
   End
   Begin VB.TextBox txtPPTO 
      Height          =   285
      Index           =   0
      Left            =   8280
      Locked          =   -1  'True
      TabIndex        =   53
      Top             =   3960
      Visible         =   0   'False
      Width           =   1095
   End
   Begin VB.TextBox txtPPTO 
      Height          =   285
      Index           =   1
      Left            =   8280
      Locked          =   -1  'True
      TabIndex        =   54
      Top             =   3960
      Visible         =   0   'False
      Width           =   1095
   End
   Begin VB.TextBox txtPPTO 
      Height          =   285
      Index           =   2
      Left            =   8280
      Locked          =   -1  'True
      TabIndex        =   55
      Top             =   4200
      Visible         =   0   'False
      Width           =   1095
   End
   Begin VB.TextBox txtPPTO 
      Height          =   285
      Index           =   3
      Left            =   8280
      Locked          =   -1  'True
      TabIndex        =   56
      Top             =   4440
      Visible         =   0   'False
      Width           =   1095
   End
   Begin MSFlexGridLib.MSFlexGrid flexEntradas 
      Height          =   1815
      Left            =   600
      TabIndex        =   0
      Top             =   840
      Width           =   7335
      _ExtentX        =   12938
      _ExtentY        =   3201
      _Version        =   393216
      Cols            =   9
      FixedCols       =   0
      Enabled         =   0   'False
      FillStyle       =   1
      Appearance      =   0
   End
   Begin MSFlexGridLib.MSFlexGrid flexArt�culos 
      Height          =   1575
      Left            =   600
      TabIndex        =   62
      Top             =   2760
      Width           =   7320
      _ExtentX        =   12912
      _ExtentY        =   2778
      _Version        =   393216
      Cols            =   6
      FixedCols       =   0
      Enabled         =   -1  'True
      FillStyle       =   1
      Appearance      =   0
   End
   Begin VB.CheckBox chkQuery 
      Caption         =   "Consultar Compra:"
      Height          =   375
      Left            =   7920
      TabIndex        =   29
      Top             =   100
      Visible         =   0   'False
      Width           =   5175
   End
   Begin Threed.SSCommand cmdGuardar 
      Height          =   735
      Left            =   6960
      TabIndex        =   6
      Top             =   5400
      Width           =   735
      _Version        =   65536
      _ExtentX        =   1296
      _ExtentY        =   1296
      _StockProps     =   78
      Caption         =   "&GUARDAR"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   6
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Enabled         =   0   'False
      Picture         =   "frmCompra.frx":3DAA
   End
   Begin Threed.SSCommand cmdAnular 
      Height          =   735
      Left            =   7680
      TabIndex        =   7
      Top             =   5400
      Width           =   735
      _Version        =   65536
      _ExtentX        =   1296
      _ExtentY        =   1296
      _StockProps     =   78
      Caption         =   "A&NULAR"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   6
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Enabled         =   0   'False
      Picture         =   "frmCompra.frx":4474
   End
   Begin Threed.SSCommand cmdImprimir 
      Height          =   735
      Left            =   8400
      TabIndex        =   8
      Top             =   5400
      Width           =   735
      _Version        =   65536
      _ExtentX        =   1296
      _ExtentY        =   1296
      _StockProps     =   78
      Caption         =   "&IMPRIMIR"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   6
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Enabled         =   0   'False
      RoundedCorners  =   0   'False
      Picture         =   "frmCompra.frx":47C6
   End
   Begin Threed.SSCommand cmdSalir 
      Height          =   735
      HelpContextID   =   120
      Left            =   9120
      TabIndex        =   9
      ToolTipText     =   "Salir"
      Top             =   5400
      Width           =   735
      _Version        =   65536
      _ExtentX        =   1296
      _ExtentY        =   1296
      _StockProps     =   78
      Caption         =   "&SALIR"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   6
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Picture         =   "frmCompra.frx":4E90
   End
   Begin Threed.SSCommand cmdOpciones 
      Height          =   255
      Index           =   0
      Left            =   5400
      TabIndex        =   63
      Tag             =   "(Insertado para compatibilidad)"
      Top             =   1800
      Visible         =   0   'False
      Width           =   855
      _Version        =   65536
      _ExtentX        =   1508
      _ExtentY        =   450
      _StockProps     =   78
      Enabled         =   0   'False
      Picture         =   "frmCompra.frx":555A
   End
   Begin Threed.SSCommand cmdSTerc 
      Height          =   285
      Left            =   2520
      TabIndex        =   64
      ToolTipText     =   "Seleccionar Tercero (F4)"
      Top             =   110
      Width           =   375
      _Version        =   65536
      _ExtentX        =   661
      _ExtentY        =   494
      _StockProps     =   78
      RoundedCorners  =   0   'False
      Outline         =   0   'False
      Picture         =   "frmCompra.frx":5576
   End
   Begin VB.CommandButton cmdXClean 
      Height          =   375
      Left            =   7320
      Picture         =   "frmCompra.frx":5AD8
      Style           =   1  'Graphical
      TabIndex        =   69
      ToolTipText     =   "Nueva compra de activos."
      Top             =   60
      Visible         =   0   'False
      Width           =   400
   End
   Begin MSMask.MaskEdBox txtFEdit 
      BeginProperty DataFormat 
         Type            =   1
         Format          =   "d/MMM/yyyy"
         HaveTrueFalseNull=   0
         FirstDayOfWeek  =   0
         FirstWeekOfYear =   0
         LCID            =   9226
         SubFormatType   =   3
      EndProperty
      Height          =   255
      Left            =   5520
      TabIndex        =   13
      Top             =   1320
      Visible         =   0   'False
      Width           =   1335
      _ExtentX        =   2355
      _ExtentY        =   450
      _Version        =   393216
      AllowPrompt     =   -1  'True
      PromptChar      =   "_"
   End
   Begin VB.Frame fraFechas 
      Height          =   735
      Left            =   1680
      TabIndex        =   70
      Top             =   360
      Width           =   5055
      Begin MSComCtl2.DTPicker dtpInicio 
         Height          =   255
         Left            =   960
         TabIndex        =   71
         Top             =   180
         Width           =   1335
         _ExtentX        =   2355
         _ExtentY        =   450
         _Version        =   393216
         Format          =   100139009
         CurrentDate     =   39189
      End
      Begin MSComCtl2.DTPicker dtpFinal 
         Height          =   255
         Left            =   3600
         TabIndex        =   72
         Top             =   180
         Width           =   1335
         _ExtentX        =   2355
         _ExtentY        =   450
         _Version        =   393216
         Format          =   100139009
         CurrentDate     =   39189
      End
      Begin VB.CheckBox chkFinal 
         Height          =   255
         Left            =   3360
         TabIndex        =   74
         Top             =   180
         Width           =   495
      End
      Begin VB.CheckBox chkInicio 
         Height          =   255
         Left            =   720
         TabIndex        =   73
         Top             =   180
         Width           =   495
      End
      Begin VB.Label lblEtiq 
         AutoSize        =   -1  'True
         Caption         =   "Hasta:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00800000&
         Height          =   195
         Index           =   21
         Left            =   2760
         TabIndex        =   76
         Top             =   195
         Width           =   480
      End
      Begin VB.Label lblEtiq 
         AutoSize        =   -1  'True
         Caption         =   "Desde:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00800000&
         Height          =   195
         Index           =   19
         Left            =   120
         TabIndex        =   75
         Top             =   195
         Width           =   510
      End
   End
   Begin VB.Image ImIn 
      Height          =   480
      Left            =   9360
      Picture         =   "frmCompra.frx":6192
      Top             =   2760
      Visible         =   0   'False
      Width           =   480
   End
   Begin VB.Image ImOut 
      Height          =   480
      Left            =   8760
      Picture         =   "frmCompra.frx":649C
      Top             =   2760
      Visible         =   0   'False
      Width           =   480
   End
   Begin VB.Label LblFechFactura 
      Caption         =   "Fecha Factura:"
      Height          =   255
      Left            =   8040
      TabIndex        =   80
      Top             =   1560
      Width           =   1575
   End
   Begin VB.Label lblNTercero 
      BackColor       =   &H80000005&
      BorderStyle     =   1  'Fixed Single
      Height          =   375
      Left            =   2880
      TabIndex        =   78
      Top             =   110
      Width           =   3825
   End
   Begin VB.Label lblEtiq 
      AutoSize        =   -1  'True
      Caption         =   "Consultar compra:"
      Height          =   195
      Index           =   5
      Left            =   7920
      TabIndex        =   68
      Top             =   160
      Visible         =   0   'False
      Width           =   1275
   End
   Begin VB.Label lblConsComp 
      Alignment       =   1  'Right Justify
      AutoSize        =   -1  'True
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   195
      Left            =   7095
      TabIndex        =   67
      Top             =   525
      Width           =   75
   End
   Begin VB.Label lblInterfases 
      Appearance      =   0  'Flat
      AutoSize        =   -1  'True
      BackColor       =   &H80000005&
      BackStyle       =   0  'Transparent
      ForeColor       =   &H80000008&
      Height          =   195
      Left            =   1905
      TabIndex        =   61
      Top             =   525
      Width           =   45
   End
   Begin VB.Label lblN 
      Alignment       =   2  'Center
      AutoSize        =   -1  'True
      Caption         =   "�"
      BeginProperty Font 
         Name            =   "Wingdings"
         Size            =   18
         Charset         =   2
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   390
      Left            =   120
      TabIndex        =   58
      ToolTipText     =   "No es posible realizar ninguna compra, con las entradas listadas."
      Top             =   960
      Visible         =   0   'False
      Width           =   465
   End
   Begin VB.Image imgCxP 
      Height          =   240
      Left            =   1155
      Picture         =   "frmCompra.frx":67A6
      ToolTipText     =   "Se guard� con interfase a CxP."
      Top             =   525
      Visible         =   0   'False
      Width           =   240
   End
   Begin VB.Image imgContab 
      Height          =   240
      Left            =   800
      Picture         =   "frmCompra.frx":68A8
      ToolTipText     =   "Se guard� con interfase a contabilidad."
      Top             =   525
      Visible         =   0   'False
      Width           =   240
   End
   Begin VB.Image imgLib 
      Height          =   240
      Index           =   2
      Left            =   5040
      Picture         =   "frmCompra.frx":69AA
      Top             =   2640
      Visible         =   0   'False
      Width           =   240
   End
   Begin VB.Label lblEtiq 
      AutoSize        =   -1  'True
      Caption         =   "Plazo:"
      Enabled         =   0   'False
      Height          =   195
      Index           =   3
      Left            =   8040
      TabIndex        =   30
      Top             =   2760
      Width           =   435
   End
   Begin VB.Label lblEtiq 
      AutoSize        =   -1  'True
      Caption         =   "Observaciones:"
      Enabled         =   0   'False
      Height          =   195
      Index           =   14
      Left            =   6960
      TabIndex        =   28
      Top             =   4320
      Width           =   1110
   End
   Begin VB.Image imgLib 
      Height          =   210
      Index           =   0
      Left            =   3600
      Picture         =   "frmCompra.frx":6AAC
      Top             =   1920
      Visible         =   0   'False
      Width           =   210
   End
   Begin VB.Label lblEtiq 
      Alignment       =   1  'Right Justify
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "Tercero:"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   195
      Index           =   4
      Left            =   180
      TabIndex        =   17
      Top             =   120
      Width           =   735
   End
   Begin VB.Label lblEtiq 
      AutoSize        =   -1  'True
      Caption         =   "N�mero de factura:"
      Enabled         =   0   'False
      Height          =   195
      Index           =   2
      Left            =   8040
      TabIndex        =   16
      Top             =   840
      Width           =   1365
   End
   Begin VB.Label lblEtiq 
      AutoSize        =   -1  'True
      Caption         =   "Vencimiento:"
      Enabled         =   0   'False
      Height          =   195
      Index           =   1
      Left            =   8040
      TabIndex        =   15
      Top             =   3360
      Width           =   915
   End
   Begin VB.Label lblEtiq 
      AutoSize        =   -1  'True
      Caption         =   "Fecha Radicacion:"
      Enabled         =   0   'False
      Height          =   195
      Index           =   0
      Left            =   8040
      TabIndex        =   14
      Top             =   2160
      Width           =   1350
   End
   Begin VB.Label lblD 
      Alignment       =   2  'Center
      AutoSize        =   -1  'True
      Caption         =   "�"
      BeginProperty Font 
         Name            =   "Wingdings"
         Size            =   36
         Charset         =   2
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H000000C0&
      Height          =   795
      Left            =   9210
      TabIndex        =   60
      ToolTipText     =   "Compra anulada."
      Top             =   250
      Visible         =   0   'False
      Width           =   495
   End
End
Attribute VB_Name = "frmFaseCompra"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Private Declare Sub GetSafeArrayPointer Lib "msvbvm60.dll" Alias "GetMem4" (pArray() As Any, ret As Long) 'JAUM T28370-R22535 Valida si un arreglo esta declarado
Public OpcCod As String   'Opci�n de seguridad
Const fExtra As Boolean = False 'Opciones extra que deben ocultarse para el usuario.
Const stVista = "VWFORM_CompraArti"
Const stVistaRel = "VWFORM_ArtEntCompr"
Const stVistaRep = "VWREP_InfCompraArticulo"
'Const stExcluCond = "NU_AUTO_ENCA Not In (Select NU_AUTO_ENCA_COEN From " & stVistaRel & " Where TX_ESTA_COMP=1)" 'JLPB T21625 SE DEJA EN COMENTARIO
Const stExcluCond = "NU_AUTO_ENCA Not In (Select NU_AUTO_ENCA_COEN From " & stVistaRel & " WITH (NOLOCK) Where TX_ESTA_COMP=1)" 'JLPB T21625
Const cObAdd As String = "| Compra No."
Const stMFormat = "$ #,#,#.00"
Dim inFCol As Integer, inFRow As Integer, flexData As MSFlexGrid
Dim boChBl As Boolean 'Indica que no se deben ignorar los cambios guardados
Dim boICont As Boolean 'Indica si hay interfaz contable (esto evita que par�metros modificados durante la edici�n sean tomados en cuenta)
Dim arrSerDescP(), arrSerDescV(), arrSerImpP(), arrSerImpV() 'Bidimensionales entradas (impuestos)
Dim arrSerCode(), arrSerIDN�m(), arrSerTope() 'Bidimensionales entradas (impuestos)
Dim arrSerIIca(), arrSerIRIva(), arrSerIReten(), arrSerIOtros() 'Unidimensional (impuestos)
Dim arrZerTope(), arrZerCode(), arrZerDescP(), arrZerDescV(), arrZerImpP(), arrZerImpV(), arrZerIDN�m(), arrZerCuen(), arrZerTipo(), arrZerVFinal() 'Matrices para impuestos heredados desde la entrada de mercanc�a.
Dim arrSerCuen(), arrSerTipo(), arrSerVFinal() 'arrSerVFinal -> Valor que el impuesto espec�fico gener�
Dim arrDescP(), arrDescV(), arrImpP() 'Unidimensionales compra
Dim arrCode(), arrIDN�m(), arrTope() 'Unidimensionales compra
Dim arrSerIVA(), arrSerTDes(), arrSerTImp(), arrSerTCost(), arrSerTBase() 'Unidimensionales entradas
'Dim dbTComDesc As Double 'HRR R1853
Dim boDescCom As Boolean 'Descuentos comerciales aplicados.
Dim fPausa As Boolean, fRIva As Boolean 'Impuestos calculados sobre el IVA (no funcionan)
'Dim btHImp, btHIC As Byte 'Se usan para mostrar s�lo una vez ciertos avisos. 'HRR R1853
Dim btModo As Byte 'Indica el modo en el que se encuentra actualmente (creando, consultando, ...)
Dim fTransit As Boolean 'Indica si se trata de una compra con movimientos transitorios previos o no.
Dim rTImDe() As Double 'Almacena el monto de impuestos/descuentos heredados por entrada.
Dim aAux() 'Usado para algunas cargas auxiliares (LoadMulData o LoadData)
'-- Matrices para movimientos contables: -- (0 Descripci�n, 1 Cuenta, 2 Monto, 3 Cr�/D�b, 4 Tercero, 5 Centro de costo ), estas matrices tienen la misma forma de la matriz arrMCuen, para facilitar luego su paso a esta.
    'BIDIMENSIONALES:
Dim aTranCnArt() 'Matriz de movimientos transitorios a revertir por art�culo.
Dim aNormCnArt() 'Matriz de movimientos normales por art�culo.
    'UNIDIMENSIONALES:
Dim aTranCnCxP() 'Matriz con el movimiento transitorio de la CxP a revertir por entrada.
Dim aNormCnCxP() 'Matriz con el movimiento normal de la CxP por compra (se suman todas las entradas y debe quedar un �tem).
'-- Matrices para movimientos contables. --
Dim lgConsec As Long        'DEPURACION DE CODIGO
Dim vContabCons, vContabComp, vcxp 'Al cargar una compra, aqu� se almacenan los valores de las interfases si existen, en caso de no existir contendran <Vac�o>.
Dim vEstado As Integer 'Estado de la compra (1 Normal, 2 Anulada)
Dim vConsec 'N�mero de la compra cargada.
Dim vFecha 'Mes de la compra cargada.
Dim fConsComp  As Boolean
Dim stCuenta As String ' guarda  la cuenta.
Dim dbVlNeto As Double
'Dim ArrVlrImpu(0, 0) As Variant, ArrVlrDesc(0, 0) As Variant 'Para llevar los valores de cada impuesto/descuentos e insertarlo en la tabla IN_R_COMP_IMDE   'PJCA 'HRR R1853
Dim AutoRet As String, AutRetIva As String, AutRetIca As String 'HRR M1698
Dim TipoRegimen As String, GranContrib As String 'HRR M1698
Dim ExentoIva As String, ExentoIca As String, ExentoFuente As String 'HRR M1698
Dim TipoPers As String 'HRR M1698
Dim DbVlrIvaEnt As Double   'REOL M1709


Dim ClssImpuDesc As ImpuDesc 'HRR R1853
Dim BoEntImpDesc As Boolean 'HRR R1853
Dim StFechaMovContable As String 'HRR R1873
'Public BoAproxCent As Boolean 'GAVL R5965  / T6197'Variable para hacer la aproximacion de las centenas en los valores de descuentos e impuestos 'JLPB T23820 SE DEJA EN COMENTARIO
Dim boConsul As Boolean 'LDCR 6401
Dim BoCruceCxP As Boolean 'JAGS R7812
Dim numAnti As String 'JAGS R7812
'Dim valanticip As Double 'JAGS R7812
Dim Mnsj As String 'JAGS R7812
Dim Arr() As Variant 'JAGS R7812
Dim matrizant As Variant 'JAGS R7812
Dim StImpCree As String 'OMOG R16934/T19113 VARIABLE QUE GUARDA LA OPCION SELECCIONADA PARA EL TERCERO EN CUANTO A "NO SUJETO PASIVO CREE O AUTORRETENEDORES DEL IMPUESTO CREE"
Dim StActi As String    'OMOG R16934/T19113 VARIABLE QUE GUARDA LA ACTIVIDAD ECONOMICA

'JAUM T28370-R22535 Inicio
Dim DbGrabado As Double 'Almacena valor grabado
Dim DbNet As Double 'Almacena valor neto
Dim DbDescu As Double 'Toma el valor total de los descuentos
Dim DbImpu As Double 'Toma el valor total de los impuestos
Dim StPres As String 'Almacena el n�mero del registro presupuestal
Dim DbObl As Double 'Almacena el n�mero de la obligaci�n
Dim DbTDCom() As Double 'Toma el valor de cada descuento parametrizado
Dim DbTDCond() As Double 'Toma el valor de cada descuento condicionado parametrizado
Dim DbTImp() As Double 'Toma el valor de cada impuestos parametrizado
'JAUM T28370-R22535 Fin
Public VrVBase As Variant 'JAUM T28644-R27752 Almacena en un vector el c�digo del impuesto y el valor base
Dim InCantEntra As Integer 'JAUM T28644-R27752 almacena la cantidad de entradas que se encuentran en la compra
Dim VrDesImp() As Variant 'JLPB T41657 'Almacena los descuwentos e impuestos realizados a cada una de las entradas

Function fnAgruparImp() As Long '|.DR.|
'Agrupa los valores de los "arreglos" arrZer(...).
'Hace uso de SQL para que la agrupaci�n sea m�s r�pida y exacta.
'La agrupaci�n se realiza por todos los elementos excepto los valors (ImpV,DescV,VFinal) que es el que se suma.
'0 Codigo, 1 ImpP, 2 ImpV, 3 DescP , 4 DescV, 5 Nombre, 6 Cuenta , 7 Tipo, 8 Tope , 9 VFinal
    
    Const cTabla As String = "tmpTAgrupar"
    Dim vResult As Integer, I&, aTemp(), StID As String
    
    ReDim aTemp(10, 0)

    If ExisteTABLA(cTabla) Then EliminarTabla (cTabla)
    
    vResult = ExecSQLCommand("Create Table " & cTabla & " (Codigo varchar (3) NULL ,ImpP float NULL ,ImpV float NULL ,DescP float NULL ,DescV float NULL ,Nombre varchar (10)  NULL , Cuenta varchar (15) NULL ,Tipo varchar (1) NULL ,Tope float NULL ,VFinal float NULL,ID varchar (1) NULL)")
        
    For I& = 0 To UBound(arrZerCode)
'        vResult = DoInsert(cTabla, "'" & arrZerCode(i) & "'," & Val(arrZerImpP(i)) & "," & Val(arrZerImpV(i)) & "," & Val(arrZerDescP(i)) & "," & Val(arrZerDescV(i)) & ",'" & arrZerIDN�m(i) & "','" & arrZerCuen(i) & "','" & arrZerTipo(i) & "'," & Val(arrZerTope(i)) & "," & Val(arrZerVFinal(i)) & "," & IIf(IsEmpty(arrZerImpP(i)), "'D'", "'I'"))
        vResult = DoInsert(cTabla, "'" & arrZerCode(I) & "'," & Val(arrZerImpP(I)) & "," & CDbl(arrZerImpV(I)) & "," & Val(arrZerDescP(I)) & "," & CDbl(arrZerDescV(I)) & ",'" & arrZerIDN�m(I) & "','" & arrZerCuen(I) & "','" & arrZerTipo(I) & "'," & CDbl(arrZerTope(I)) & "," & CDbl(arrZerVFinal(I)) & "," & IIf(IsEmpty(arrZerImpP(I)), "'D'", "'I'"))   'REOL M2465
    Next I&

        vResult = LoadMulData(cTabla & " Group By Codigo, ImpP, DescP , Nombre, Cuenta , Tipo, Tope, ID", "Codigo, ImpP, Sum(ImpV), DescP , Sum(DescV), Nombre, Cuenta , Tipo, Tope , Sum(VFinal),ID", "", aTemp)
        
        If vResult <> FAIL Then
            I = UBound(aTemp, 2)
                ReDim arrZerCode(I), arrZerImpP(I), arrZerImpV(I), arrZerDescP(I), arrZerDescV(I)
                ReDim arrZerIDN�m(I), arrZerCuen(I), arrZerTipo(I), arrZerTope(I), arrZerVFinal(I)
            For I& = 0 To UBound(aTemp, 2)
                StID = aTemp(10, I&)
                arrZerCode(I) = aTemp(0, I&)
                If StID = "I" Then
                    arrZerImpP(I) = aTemp(1, I&)
                    arrZerImpV(I) = aTemp(2, I&)
                Else
                    arrZerDescP(I) = aTemp(3, I&)
                    arrZerDescV(I) = aTemp(4, I&)
                End If
                arrZerIDN�m(I) = aTemp(5, I&)
                arrZerCuen(I) = aTemp(6, I&)
                arrZerTipo(I) = aTemp(7, I&)
                arrZerTope(I) = aTemp(8, I&)
                arrZerVFinal(I) = aTemp(9, I&)
            Next I&
            
            EliminarTabla (cTabla)
            
        End If
        
End Function

Function fnChanges() As Boolean
'Devuelve verdadero si se han realizado cambios relevantes en el documento.

On Error GoTo Salida
    
    'fnChanges = (UBound(arrSerCode) > 0) And flexEntradas.Rows > 1 'HRR R1853
    fnChanges = ClssImpuDesc.TieneDatos And flexEntradas.Rows > 1 'HRR R1853
    boChBl = True

    Exit Function
    
Salida:
    fnChanges = False
    boChBl = True
    
End Function


Function fnRowSel(lgRow As Long) As Boolean
'Devuelve si la entrada est� seleccionada.
    fnRowSel = flexEntradas.TextMatrix(lgRow, 0) = " "
End Function

Function fnRowSels() As Long
'Devuelve el n�mero de filas seleccionadas.
    Dim lgResult As Long, I&
    
    For I& = 1 To flexEntradas.Rows - 1
        If fnRowSel(I&) Then
            lgResult = lgResult + 1
        End If
    Next I&
    
        fnRowSels = lgResult
    
End Function

'---------------------------------------------------------------------------------------
' Procedure : Grabar_Cxp_Hija
' DateTime  : 23/06/2015 15:33
' Author    : juan_urrego
' Purpose   : 'JAUM T28370-R22535 Almacena en la tabla R_CXP_CXP las cuentas hijas
'---------------------------------------------------------------------------------------
'
Private Sub Grabar_Cxp_Hija(DbNumero As Double, StHijo As String, InFila As Integer)
   Valores = "CD_CONCE_CXP_RCPCP= 'CINM'" & Coma
   Valores = Valores & "CD_NUME_CXP_RCPCP=" & DbNumero & Coma
   Valores = Valores & "CD_CONCE_RCPCP=" & Comi & Cambiar_Comas_Comillas(StHijo) & Comi & Coma
   Valores = Valores & "VL_BRUT_RCPCP=" & CDbl(Format(DbGrabado, "#,###,###,###,##0.#0")) & Coma
   Valores = Valores & "VL_BRUE_RCPCP= 0" & Coma
   Valores = Valores & "VL_NETO_RCPCP=" & CDbl(Format(DbNet, "#,###,###,###,##0.#0")) & Coma
   Valores = Valores & "VL_CANC_RCPCP= 0" & Coma
   Valores = Valores & "VL_NCRE_RCPCP= 0" & Coma
   Valores = Valores & "VL_NDEB_RCPCP= 0" & Coma
   Valores = Valores & "VL_DESC_RCPCP=" & CDbl(Format(DbDescu, "#,###,###,###,##0.#0")) & Coma
   Valores = Valores & "VL_IMPU_RCPCP=" & CDbl(Format(DbImpu, "#,###,###,###,##0.#0")) & Coma
   Valores = Valores & "NU_REGI_RCPCP=" & CDbl(StPres) & Coma
   Valores = Valores & "NU_OBLI_RCPCP=" & CDbl(DbObl) & Coma
   Valores = Valores & "ID_PPTO_RCPCP= 1" & Coma
   Valores = Valores & "NU_POSI_RCPCP=" & CDbl(InFila)
   Result = DoInsertSQL("R_CXP_CXP", Valores)
   'JAUM T28836 Inicio
   Valores = "VL_IMPU_CXP = VL_IMPU_CXP + " & CDbl(Format(DbImpu, "#,###,###,###,##0.#0")) & Coma
   Valores = Valores & "VL_DESC_CXP = VL_DESC_CXP +" & CDbl(Format(DbDescu, "#,###,###,###,##0.#0"))
   Result = DoUpdate("C_X_P", Valores, "CD_NUME_CXP = " & DbNumero)
   'JAUM T28836 Fin
End Sub

Sub sbApForm()
'Copia el valor de la entrada a la columna visible con el formato de moneda configurado.

    Dim I&

    With flexEntradas
    
    For I& = 1 To .Rows - 1
            .TextMatrix(I&, 6) = Format(Val(.TextMatrix(I&, 4)), stMFormat)
    Next I&
    
    End With
                
End Sub

Sub sbAplFormat(grdTemp As MSFlexGrid, vCol As Long)
'Aplica el formato de 'moneda' a la columna especificada en la grilla especificada.
'Este procedimiento s�lo se aplica si los n�meros no tienen formato a�n.
    Dim I&
        
    With grdTemp
        For I& = 1 To .Rows - 1
            .TextMatrix(I&, vCol) = Format(Val(.TextMatrix(I&, vCol)), stMFormat)
        Next I&
    End With
    
End Sub

'HRR R1853
'Sub sbCalcImpSer() 'Realiza el c�lculo completo.
'
'        Dim i&, im&, j&, DbFinal As Double, DbInicial As Double, DbImpDesc As Double
'        Dim dbTFinal As Double, dbTInicial As Double    'DEPURACION DE CODIGO
'        Dim dbTDesc As Double, DbTImp As Double 'Totales por fila
'        Dim dbGTDesc As Double, dbGTImp As Double 'Totales por todos
'        Dim DbTIIca As Double, DbTIRet As Double, DbTIRIva As Double, dbTIOtros As Double 'Impuestos totales por fila
'        Dim dbGTIIca As Double, dbGTIRet As Double, dbGTIRIVA As Double, dbGTIOtros As Double 'Impuestos totales por todos
'        Dim dbVlrIVAEntr As Double  'almacena el valor de iva de las entradas seleccionadas cuando no hay numero de factura enla entrada
'
'        boDescCom = False: fRIva = False
'        '00 Calcular los valores de la grilla.
'        With flexEntradas
'                DbInicial = 0
'                For i& = 1 To .Rows - 1
'                    If fnRowSel(i&) Then
'                        DbInicial = DbInicial + Val(.TextMatrix(i&, 4))
'                        'If .TextMatrix(I&, 5) = NUL$ Then  'PJCA M1862 'HRR M2001
'                           dbVlrIVAEntr = dbVlrIVAEntr + Aplicacion.Formatear_Valor(Val(.TextMatrix(i&, 7)))
'                        'End If                              'PJCA M1862 'HRR M2001
'                    End If
'                Next i&
'                dbVlNeto = DbInicial - dbVlrIVAEntr
'                DbVlrIvaEnt = dbVlrIVAEntr 'REOL M1709
'
'                sbApForm
'
'                '00.a Aplicando descuentos comerciales (afectan el valor base)
'                   For j& = 0 To UBound(arrSerImpP)
'                    If Val(arrSerTipo(j&)) <> 1 And IsEmpty(arrSerImpP(j&)) Then   ' ==== Descuento ==== Es un descuento comercial (Inverso de [Not X or Not Y] es [X and Y])
'                            If Val(arrSerDescP(j&)) = 0 Then  'Valor
'                                DbImpDesc = -Val(arrSerDescV(j&))
'                            Else 'Porcentaje
''                                dbImpDesc = Round(-val(dbInicial * arrSerDescP(j&)) / 100, 4)
'                                DbImpDesc = Aplicacion.Formatear_Valor(-Val(dbVlNeto * arrSerDescP(j&)) / 100)  'REOL M1709
'                                arrSerDescV(j&) = DbImpDesc
'                            End If
'                           If Val(arrSerTope(j&)) > Val(DbInicial) Then DbImpDesc = 0  'Se aplica el "tope", no se ejecuta el impuesto si el valor no tiene el m�nimo requerido.
'                           dbTDesc = dbTDesc + DbImpDesc
'                           arrSerVFinal(j&) = -DbImpDesc 'Usado por la interfaz contable.
'                           If DbImpDesc <> 0 Then boDescCom = True 'Indica que se aplicaron impuestos comerciales.
'                           'arrSerVFinal(im&, j&) = 0 'Usado por la interfaz contable .
'                           DbInicial = DbInicial + DbImpDesc '���Estos impuestos afectan el valor base!!!
'                    End If
'                   Next j&
'                   dbTComDesc = dbTDesc
'                '00.a Fin.
'
'                DbFinal = DbInicial
'                dbTDesc = 0
'                DbTIIca = 0: DbTIRet = 0: DbTIRIva = 0: dbTIOtros = 0
'
'                '00.b Reiniciando valores:
'                arrSerIIca(0) = 0: arrSerIRIva(0) = 0
'                arrSerIReten(0) = 0: arrSerIOtros(0) = 0
'                '00.b Fin.
'
'
'                '00.c Barrido por los impuestos:
'                   For j& = 0 To UBound(arrSerImpP)
'                    If Val(arrSerTipo(j&)) = 1 Or Not IsEmpty(arrSerImpP(j&)) Then   ' Esta condici�n pretende evitar el barrido sobre descuentos comerciales (Inverso de [X and Y] es [Not X or Not Y])
'
'                        If IsEmpty(arrSerImpP(j&)) Then  '==== Descuento ====
'                            If Val(arrSerDescP(j&)) = 0 Then  'Valor
'                                DbImpDesc = -Val(arrSerDescV(j&))
'                            Else 'Porcentaje
''                                dbImpDesc = Round(-val(dbInicial * arrSerDescP(im&, j&)) / 100, 4)
'                                DbImpDesc = Aplicacion.Formatear_Valor(-Val(dbVlNeto * arrSerDescP(j&)) / 100)   'REOL M1732
'                                arrSerDescV(j&) = DbImpDesc
'                            End If
'                                If Val(arrSerTope(j&)) > Val(DbInicial) Then DbImpDesc = 0 'Se aplica el "tope", no se ejecuta el impuesto si el valor no tiene el m�nimo requerido.
'                                dbTDesc = dbTDesc + DbImpDesc
'                        Else '==== Impuesto ====
'                                If arrSerTipo(j&) = "M" Then 'No se permite ingresar IVA por lo tanto esta secci�n no funciona:
'                                    'dbImpDesc = -val(dbIVA * arrSerImpP(im&, j&)) / 100
'                                    fRIva = True
'                                    DbImpDesc = 0
'
'                                Else
'                                    'dbImpDesc = Round(-val(dbInicial * arrSerImpP(j&)) / 100, 4)
''                                    dbImpDesc = Round(-val((dbInicial - dbVlrIVAEntr) * arrSerImpP(j&)) / 100, 4)    'PJCA M1862 SE RESTA EL IVA DE LA ENTRADA CUANDO NO HAY # FACTURA
'                                    DbImpDesc = Aplicacion.Formatear_Valor(-Val((DbInicial - dbVlrIVAEntr) * arrSerImpP(j&)) / 100) 'REOL M1732
'                                    arrSerImpV(j&) = DbImpDesc
'                                    If arrSerTipo(j&) <> "R" And arrSerTipo(j&) <> "C" Then DbImpDesc = DbImpDesc * -1  'M:1285
'                                End If
'                                If Val(arrSerTope(j&)) > Val(DbInicial) Then DbImpDesc = 0  'Se aplica el "tope", no se ejecuta el impuesto si el valor no tiene el m�nimo requerido.
'                                DbTImp = DbTImp + DbImpDesc
'                                '00.c.1 Separar los impuestos:
'                                Select Case arrSerTipo(j&)
'                                Case "C":  arrSerIIca(0) = arrSerIIca(0) + DbImpDesc: DbTIIca = DbTIIca + DbImpDesc
'                                Case "R":  arrSerIRIva(0) = arrSerIRIva(0) + DbImpDesc: DbTIRIva = DbTIRIva + DbImpDesc
'                                Case "M":  arrSerIReten(0) = arrSerIReten(0) + DbImpDesc: DbTIRet = DbTIRet + DbImpDesc
'                                Case Else:  arrSerIOtros(0) = arrSerIOtros(0) + DbImpDesc: dbTIOtros = dbTIOtros + DbImpDesc
'                                End Select
'                                '00.c.1 Fin.
'                        End If
'                                arrSerVFinal(j&) = -DbImpDesc  'Usado por la interfaz contable.
'                                DbFinal = DbFinal + DbImpDesc
'                    End If
'                   Next j&
'                '00.c Fin
'                arrSerTBase(0) = DbInicial 'El valor para c�lculos internos
'                arrSerTCost(0) = DbFinal 'El valor para c�lculos internos
'                arrSerTDes(0) = dbTDesc 'El valor para c�lculos internos (este valor nunca se muestra en el formulario, s�lo aparece en la tabla)
'                arrSerTImp(0) = DbTImp 'El valor para c�lculos internos (este valor nunca se muestra en el formulario, s�lo aparece en la tabla)
'
'                '00.d S�lo las filas seleccionadas son inclu�das en la cuenta final:
'                    dbTFinal = DbFinal
'                    dbTInicial = DbInicial
'                    dbGTDesc = dbTDesc
'                    dbGTImp = DbTImp
'                    dbGTIIca = DbTIIca
'                    dbGTIRet = DbTIRet
'                    dbGTIRIVA = DbTIRIva
'                    dbGTIOtros = dbTIOtros
'                '00.d Fin.
'        End With
'        '00 Fin
'
'            '01 Mostrando valores:
''            lblTotalB = Format(dbTInicial - dbTComDesc, stMFormat): lblTotalB.Tag = dbTInicial '|.DR.| M:1731 (- dbTComDesc) un descuento comercial del cu�l s�lo debe saberse al momento de mostrar la informaci�n.
''            lblImp = Format(dbGTImp, stMFormat): lblImp.Tag = dbGTImp
''            lblDesc = Format(dbGTDesc + dbTComDesc, stMFormat): lblDesc.Tag = dbGTDesc '|.DR.| M:1731 (+ dbTComDesc) un descuento comercial del cu�l s�lo debe saberse al momento de mostrar la informaci�n.
''            lblTotal = Format(dbTFinal, stMFormat): lblTotal.Tag = dbTFinal
''            lblTIIca = Format(dbGTIIca, stMFormat): lblTIIca.Tag = dbGTIIca
''            lblTIRet = Format(dbGTIRet, stMFormat): lblTIRet.Tag = dbGTIRet
''            lblTIRIVA = Format(dbGTIRIVA, stMFormat): lblTIRIVA.Tag = dbGTIRIVA
''            lblTIOtros = Format(dbGTIOtros, stMFormat): lblTIOtros.Tag = dbGTIOtros
'            'REOL M1734
'            lblTotalB = Aplicacion.Formatear_Valor(dbTInicial - dbTComDesc):    lblTotalB.Tag = dbTInicial
'            lblImp = Aplicacion.Formatear_Valor(dbGTImp):   lblImp.Tag = dbGTImp
'            lblDesc = Aplicacion.Formatear_Valor(dbGTDesc + dbTComDesc):    lblDesc.Tag = dbGTDesc
'            lblTotal = Aplicacion.Formatear_Valor(dbTFinal):    lblTotal.Tag = dbTFinal
'            lblTIIca = Aplicacion.Formatear_Valor(dbGTIIca):    lblTIIca.Tag = dbGTIIca
'            lblTIRet = Aplicacion.Formatear_Valor(dbGTIRet):    lblTIRet.Tag = dbGTIRet
'            lblTIRIVA = Aplicacion.Formatear_Valor(dbGTIRIVA):  lblTIRIVA.Tag = dbGTIRIVA
'            lblTIOtros = Aplicacion.Formatear_Valor(dbGTIOtros):    lblTIOtros.Tag = dbGTIOtros
'            'REOL FIN
'            '01 Fin.
'
'
'        Dim dbCompIni As Double, dbCompFin As Double
'        Dim dbCTDesc As Double, dbCTImp As Double
'
'        '02 Calcular impuestos aplicados a los totales de la compra: ==== Esta secci�n esta inutilizada por ahora, ya que los controles que la afectan no est�n visibles ====
'                dbCompIni = dbTFinal
'                dbCompFin = dbCompIni
'                'dbCompFin = dbCompFin + ((dbCompIni) * txtIVA) / 100  'Calculando IVA (escrito manualmente).
'                dbCompFin = dbCompFin + Val(txtFlete.Tag)    'Agregando Fletes (escrito manualmente, se almacena en el TAG el valor "limpio" sin formato).
'
'                '02.a Barrido por los impuestos:
'                   For j& = 0 To UBound(arrCode)
'                        If IsEmpty(arrImpP(j&)) Then 'Descuento
'                            If Val(arrDescP(j&)) = 0 Then 'Valor
'                                DbImpDesc = -Val(arrDescV(j&))
'                            Else 'Porcentaje
'                                DbImpDesc = -Val(dbCompIni * arrDescP(j&)) / 100
'                            End If
'                                dbCTDesc = dbCTDesc + DbImpDesc
'                        Else 'Impuesto
'                                DbImpDesc = Val(dbCompIni * arrImpP(j&)) / 100
'                                dbCTImp = dbCTImp + DbImpDesc
'                        End If
'                        dbCompFin = dbCompFin + DbImpDesc
'                   Next j&
'                '02.a Fin
'
'                'Estos controles no son visibles por el momento, se desactivaron los controles
'                '  pero no los c�lculos:
'                lblTotalN = Format(dbCompFin, stMFormat): lblTotalN.Tag = dbCompFin
'                lblImpAT = Format(dbCTImp, stMFormat): lblImpAT.Tag = dbCTImp
'                lblDescAT = Format(dbCTDesc, stMFormat): lblDescAT.Tag = dbCTDesc
'
'        '02 Fin. ==== Secci�n inutilizada ====
'
'End Sub
'HRR R1853

'HRR R1853
Sub sbCalcImpSer() 'Asigna los valores de la ventana
   Dim I&
   Dim DbInicial As Double
   Dim dbVlrIVAEntr As Double  'almacena el valor de iva de las entradas seleccionadas cuando no hay numero de factura enla entrada
   boDescCom = False: fRIva = False
   '00 Calcular los valores de la grilla.
   With flexEntradas
      DbInicial = 0
      For I& = 1 To .Rows - 1
         If fnRowSel(I&) Then
            DbInicial = DbInicial + Val(.TextMatrix(I&, 4))
            dbVlrIVAEntr = dbVlrIVAEntr + Aplicacion.Formatear_Valor(Val(.TextMatrix(I&, 7)))
         End If
      Next I&
      'dbVlNeto = DbInicial - dbVlrIVAEntr 'JLPB T22266 SE DEJA EN COMENTARIO
      dbVlNeto = DbInicial - IIf(BoAproxCent, AproxCentena(Round(dbVlrIVAEntr)), dbVlrIVAEntr) 'JLPB T22266
      DbVlrIvaEnt = dbVlrIVAEntr
      'JAUM T29262 Inicio se deja bloque en comentario
      'If Not boIPPTO Then 'JAUM T28370-R22535
         'sbApForm
      'End If 'JAUM T28370-R22535
      'JAUM T29262 Fin
      arrSerIIca(0) = 0: arrSerIRIva(0) = 0
      arrSerIReten(0) = 0: arrSerIOtros(0) = 0
      arrSerTBase(0) = DbInicial 'El valor para c�lculos internos
      arrSerTCost(0) = DbInicial 'El valor para c�lculos internos
      arrSerTDes(0) = 0 'El valor para c�lculos internos (este valor nunca se muestra en el formulario, s�lo aparece en la tabla)
      arrSerTImp(0) = 0 'El valor para c�lculos internos (este valor nunca se muestra en el formulario, s�lo aparece en la tabla)
      'JAUM T28370-R22535 Inicio
      If boIPPTO Then
         If boICxP Then
            For I& = 1 To .Rows - 1
               .TextMatrix(I&, 8) = Format(Val(-rTImDe(I&)), stMFormat)
            Next
         Else
            For I& = 1 To .Rows - 1
               .TextMatrix(I&, 8) = Format(Val(0), stMFormat)
            Next
         End If
      Else 'JAUM T29262
         sbApForm 'JAUM T29262
      End If
      'JAUM T28370-R22535 Fin
   End With
   lblTotalB = Aplicacion.Formatear_Valor(DbInicial):    lblTotalB.Tag = DbInicial
   lblImp = Aplicacion.Formatear_Valor(0):   lblImp.Tag = 0
   lblDesc = Aplicacion.Formatear_Valor(0):    lblDesc.Tag = 0
   lblTotal = Aplicacion.Formatear_Valor(DbInicial):    lblTotal.Tag = DbInicial
   lblTIIca = Aplicacion.Formatear_Valor(0):    lblTIIca.Tag = 0
   lblTIRet = Aplicacion.Formatear_Valor(0):    lblTIRet.Tag = 0
   lblTIRIVA = Aplicacion.Formatear_Valor(0):  lblTIRIVA.Tag = 0
   lblTIOtros = Aplicacion.Formatear_Valor(0):    lblTIOtros.Tag = 0
   lblTotalN = Aplicacion.Formatear_Valor(DbInicial): lblTotalN.Tag = DbInicial
   lblImpAT = Aplicacion.Formatear_Valor(0): lblImpAT.Tag = 0
   lblDescAT = Aplicacion.Formatear_Valor(0): lblDescAT.Tag = 0
End Sub
'HRR R1853

Function fnCargar(lgConsec As Long) As Boolean
       
   Dim lgConsSer As Long
   Dim I&, J&, k&, arrXel(), arrXil(), cFAdquSer As String, cCodigo As String
   Dim dbVlrBruto As Double
   Dim VrImpDesc() As Variant 'JAUM T28370-R22535 Almacena los impuestos y descuentos que se generaron en la entrada
   Dim VrTotal() As Variant 'JAUM T28370-R22535 Almacena el valor neto de cada articulo

   'Reiniciar matrices para evitar errores con Ubound o Lbound, en caso de que este procedimiento no las "inicie":
   Dim t% 'Aunque t% representa un Cero en este caso, se deja la variable por si es necesario cambiar en un futuro los tama�os iniciales.
   Dim DbValEntrada() As Double 'JAUM T29793 Almacena el valor de la entrada cuando los articulos tienen iva
   Dim DbValIva() As Double 'JAUM T29793 Almacena el valor del iva del articulo
   Dim VrArr() As Variant 'JAUM T29815 Almacena en el arreglo la informaci�n consultada
   ReDim arrSerTDes(t%), arrSerTImp(t%), arrSerTCost(t%), arrSerIIca(t%), arrSerIRIva(t%), arrSerIReten(t%), arrSerIOtros(t%), arrSerTBase(t%)
   ReDim arrSerDescP(t%, 0), arrSerDescV(t%, 0), arrSerImpP(t%, 0), arrSerImpV(t%, 0), arrSerTipo(t%, 0), arrSerVFinal(t%, 0)
   ReDim arrSerIVA(t%), arrSerIDN�m(t%, 0), arrSerCode(t%, 0), arrSerTope(t%, 0)
   ReDim arrDescP(0), arrDescV(0), arrImpP(0)  'Informaci�n de impuestos al total de la compra
   ReDim arrCode(0), arrIDN�m(0), arrTope(0)  'Informaci�n de impuestos al total de la compra
     
   'Inicio -- Reiniciando valores de las variables para interfases --
   vContabCons = Empty
   vContabComp = Empty
   vcxp = Empty
   vEstado = 1
   vConsec = Empty
   vFecha = Empty
   'Fin -- Reiniciando valores de las variables para interfases --

   boICont = (Aplicacion.Interfaz_Contabilidad)
   boICxP = (Aplicacion.Interfaz_CxP)

   'Cargando la compra:
   'HRR R1873
   'ReDim arrXel(12, 0)
   'Result = LoadMulData("IN_COMPRA", "FE_FECH_COMP,FE_VENC_COMP,CD_CODI_TERC_COMP,TX_FAPR_COMP,TX_OBSE_COMP,VL_BRUT_COMP,VL_DESC_COMP,VL_IMPU_COMP,VL_FLET_COMP,VL_NETO_COMP,TX_ESTA_COMP, NU_CXP_COMP,NU_CONS_MOVI_COMP", "NU_AUTO_COMP=" & lgConsec, arrXel)
   'HRR R1873
    
   'HRR R1873
   ReDim arrXel(13, 0)
   Campos = "FE_FECH_COMP,FE_VENC_COMP,CD_CODI_TERC_COMP,TX_FAPR_COMP,TX_OBSE_COMP,VL_BRUT_COMP"
   Campos = Campos & Coma & "VL_DESC_COMP,VL_IMPU_COMP,VL_FLET_COMP,VL_NETO_COMP,TX_ESTA_COMP, NU_CXP_COMP"
   Campos = Campos & Coma & "NU_CONS_MOVI_COMP,FE_FACT_COMP"
   Result = LoadMulData("IN_COMPRA", Campos, "NU_AUTO_COMP=" & lgConsec, arrXel)
   'HRR R1873
   If Not Encontro Then GoTo Anular
   vConsec = lgConsec
   txtFecha = Format(arrXel(0, 0), "dd/mm/yyyy"): vFecha = arrXel(0, 0)
   txtFechaV = arrXel(1, 0)
   Me.txtDPlazo = DateDiff("d", txtFecha, txtFechaV) 'AASV R2222 COLOCA LA CANTIDAD DE DIAS DE PLAZO
   MskFechFactura = Format(arrXel(13, 0), "dd/mm/yyyy") 'HRR R1873
   txtTercero = arrXel(2, 0)
   txtN�mFact = arrXel(3, 0)
   txtObser = fnCambiarDT(CStr(arrXel(4, 0)))
   txtFlete.Tag = Val(arrXel(8, 0)): txtFlete = Format(Val(txtFlete.Tag), stMFormat)
   lblTotalN.Tag = arrXel(9, 0)
   vEstado = Val(arrXel(10, 0))
   vcxp = IIf(arrXel(11, 0) <> "", arrXel(11, 0), Empty)
'  vContabComp = IIf(arrXel(14, 0) <> "", arrXel(14, 0), Empty)
   vContabCons = IIf(arrXel(12, 0) <> "", arrXel(12, 0), Empty)
    
   'lblTotalB.Tag = arrXel(5, 0): lblTotalB = Format(val(lblTotalB.Tag), stMFormat)
   dbVlrBruto = arrXel(5, 0)
'    lblDesc.Tag = arrXel(6, 0): lblDesc = Format(val(lblDesc.Tag), stMFormat)
'    lblImp.Tag = arrXel(7, 0): lblImp = Format(val(lblImp.Tag), stMFormat)
'    lblTotal.Tag = arrXel(9, 0): lblTotal = Format(val(lblTotal.Tag), stMFormat)
    'REOL M1734
   lblDesc.Tag = arrXel(6, 0): lblDesc = Aplicacion.Formatear_Valor(Val(lblDesc.Tag))
   lblImp.Tag = arrXel(7, 0):  lblImp = Aplicacion.Formatear_Valor(Val(lblImp.Tag))
   lblTotal.Tag = arrXel(9, 0):   lblTotal = Aplicacion.Formatear_Valor(Val(lblTotal.Tag))
   'REOL FIN

   fnCargar = True
   If Result = FAIL Then GoTo Anular
    
    
   'Cargando los valores de las entradas:
   ReDim arrXel(7, 0)
   Result = LoadMulData("IN_R_COMP_ENCA,IN_COMPRA", "NU_AUTO_ENCA_COEN,0,VL_COST_COEN,VL_IMPU_COMP,VL_DESC_COMP,VL_NETO_COMP,0,NU_AUTO_COMP", "NU_AUTO_COMP_COEN=NU_AUTO_COMP And NU_AUTO_COMP_COEN=" & lgConsec, arrXel)
   
    
   With flexEntradas
      .Rows = 1
    
    
      t% = 0
      ReDim arrSerTDes(t%), arrSerTImp(t%), arrSerTCost(t%), arrSerIIca(t%), arrSerIRIva(t%), arrSerIReten(t%), arrSerIOtros(t%), arrSerTBase(t%)
      ReDim arrSerDescP(0), arrSerDescV(0), arrSerImpP(0), arrSerImpV(0), arrSerTipo(0), arrSerVFinal(0)
      ReDim arrSerIVA(0), arrSerIDN�m(0), arrSerCode(0), arrSerTope(0)
       
      For I& = 0 To UBound(arrXel, 2)
   
         cFAdquSer = fnDevDato("IN_ENCABEZADO", "FE_CREA_ENCA", "NU_AUTO_ENCA=" & arrXel(0, I&)) 'M:1335
         cCodigo = fnDevDato("IN_ENCABEZADO", "NU_COMP_ENCA", "NU_AUTO_ENCA=" & arrXel(0, I&)) 'M:1335
         .AddItem " " & vbTab & cCodigo & vbTab & arrXel(0, I&) & vbTab & cFAdquSer & vbTab & arrXel(2, I&) & vbTab & arrXel(6, I&) & vbTab & "" & vbTab & "" & vbTab & arrXel(1, I&)
         If Result = FAIL Then GoTo Anular
         lgConsSer = Val(arrXel(7, I&))
      Next I&
       
           
      ReDim arrXil(6, 0)
      Dim dbTDesc As Double
      Dim DbTIIca As Double, DbTIRet As Double, DbTIRIva As Double, dbTIOtros As Double 'Impuestos totales por fila
      Result = LoadMulData("IN_R_COMP_IMDE", "NU_POSI_CCOID,CD_CODI_IMPU_CCOID,CD_CODI_DESC_CCOID,TX_NOMB_CCOID,NU_VALO_CCOID,NU_PRAPL_CCOID,TX_TIPO_CCOID", "NU_AUTO_COMP_CCOID=" & lgConsSer, arrXil)
      If Encontro Then
         '.TextMatrix(.Rows - 1, 1) = " "
         For J& = 0 To UBound(arrXil, 2)
            k& = Val(arrXil(0, J&))
            If k& > UBound(arrSerCode) Then
               ReDim Preserve arrSerCode(k&), arrSerIDN�m(k&), arrSerImpP(k&), arrSerTope(k&), arrSerImpV(k&)
               ReDim Preserve arrSerDescV(k&), arrSerDescP(k&), arrSerTipo(k&), arrSerVFinal(k&)
            End If
                      
            arrSerTipo(k&) = arrXil(6, J&)
            arrSerIDN�m(k&) = arrXil(3, J&)
            arrSerTope(k&) = 0  '*** ��Cuidado!!, esto puede causar problemas ***
                      
            If arrXil(2, J&) = "" Then 'Impuesto
               arrSerCode(k&) = arrXil(1, J&)
               arrSerImpP(k&) = arrXil(5, J&)
               arrSerImpV(k&) = IIf(Val(arrXil(4, J&)) = 0, Empty, arrXil(4, J&))
                              
               Select Case arrSerTipo(k&)
                  Case "C":  arrSerIIca(0) = arrSerIIca(0) + arrSerImpV(k&): DbTIIca = DbTIIca + arrSerImpV(k&)
                  Case "R":  arrSerIRIva(0) = arrSerIRIva(0) + arrSerImpV(k&): DbTIRIva = DbTIRIva + arrSerImpV(k&)
                  Case "M":  arrSerIReten(0) = arrSerIReten(0) + arrSerImpV(k&): DbTIRet = DbTIRet + arrSerImpV(k&)
                  Case Else:  arrSerIOtros(0) = arrSerIOtros(0) + arrSerImpV(k&): dbTIOtros = dbTIOtros + arrSerImpV(k&)
               End Select
                              
            Else 'Descuento
               arrSerCode(k&) = arrXil(2, J&)
               arrSerDescV(k&) = IIf(Val(arrXil(4, J&)) = 0, Empty, arrXil(4, J&))
               arrSerDescP(k&) = IIf(Val(arrXil(5, J&)) = 0, Empty, arrXil(5, J&))
               dbTDesc = dbTDesc + arrSerDescV(k&)
            End If
         Next J&
      End If

'        lblTotalB.Tag = dbVlrBruto - dbTDesc: lblTotalB = Format(val(lblTotalB.Tag), stMFormat)
'        lblDesc = Format(dbTDesc, stMFormat): lblDesc.Tag = dbTDesc
'        lblTIIca = Format(dbTIIca, stMFormat): lblTIIca.Tag = dbTIIca
'        lblTIRet = Format(dbTIRet, stMFormat): lblTIRet.Tag = dbTIRet
'        lblTIRIVA = Format(dbTIRIVA, stMFormat): lblTIRIVA.Tag = dbTIRIVA
'        lblTIOtros = Format(dbTIOtros, stMFormat): lblTIOtros.Tag = dbTIOtros
        'REOL M1734
'        lblTotalB.Tag = dbVlrBruto - dbTDesc:    lblTotalB = Aplicacion.Formatear_Valor(val(lblTotalB.Tag))
      lblTotalB.Tag = dbVlrBruto: lblTotalB = Aplicacion.Formatear_Valor(Val(lblTotalB.Tag))  'REOL 1994
      'lblDesc.Tag = dbTDesc:  lblDesc = Aplicacion.Formatear_Valor(dbTDesc) 'HRR R1853
      lblTIIca.Tag = DbTIIca:  lblTIIca = Aplicacion.Formatear_Valor(DbTIIca)
      'HRR M2669
      'lblTIRet.Tag = DbTIRet:  lblTIRet = Aplicacion.Formatear_Valor(DbTIRet)
      'lblTIRIVA.Tag = DbTIRIva:  lblTIRIVA = Aplicacion.Formatear_Valor(DbTIRIva)
      lblTIRet.Tag = DbTIRIva:  lblTIRet = Aplicacion.Formatear_Valor(DbTIRIva)
      lblTIRIVA.Tag = DbTIRet: lblTIRIVA = Aplicacion.Formatear_Valor(DbTIRet)
      'HRR M2669
      lblTIOtros.Tag = dbTIOtros:  lblTIOtros = Aplicacion.Formatear_Valor(dbTIOtros)
      'REOL FIN
        
      'Agregando n�meros de factura:
      With flexEntradas
         For I& = 1 To .Rows - 1
            .TextMatrix(I&, 5) = fnDevDato("IN_PARTIPARTI", "NU_VALO_PRPR", "TX_CODI_PART_PRPR='8' And NU_AUTO_ENCA_PRPR=" & .TextMatrix(I&, 2))
         Next I&
      End With
      'JAUM T28370-R22535 Inicio
      If boIPPTO Then
         For I& = 1 To .Rows - 1
            ReDim VrImpDesc(5, 0)
            'JAUM T29793 Inicio Se deja linea en comentario
            'ReDim VrTotal(2, 0)
            ReDim VrTotal(3, 0)
            ReDim DbValEntrada(.Rows)
            ReDim DbValIva(.Rows)
            'JAUM T29793 Fin
            DbDescu = 0
            DbImpu = 0
            'Valida si se generaron impuestos antes de la compra
            Condicion = "NU_AUTO_ENCA_ENID=" & .TextMatrix(I&, 1)
            Condicion = Condicion & " AND NU_AUTO_ENCAREAL_ENID IN (" & .TextMatrix(I&, 2)
            Condicion = Condicion & ") AND (CD_CODI_DESC=CD_CODI_DESC_ENID OR CD_CODI_IMPU=CD_CODI_IMPU_ENID)"
            Desde = "IN_R_ENCA_IMDE,TC_IMPUESTOS,DESCUENTO"
            Campos = "DISTINCT CD_CODI_IMPU_ENID,CD_CODI_DESC_ENID,NU_PRAPLI_ENID,"
            Campos = Campos & "CASE WHEN CD_CODI_IMPU_ENID IS NULL THEN VL_TOPE_DESC ELSE VL_TOPE_IMPU END AS VL_TOPE,"
            Campos = Campos & "CASE WHEN CD_CODI_IMPU_ENID IS NULL THEN VL_VALO_DESC ELSE 0 END AS VL_DESC,"
            Campos = Campos & "CASE WHEN CD_CODI_IMPU_ENID IS NULL THEN NULL ELSE ID_TIPO_IMPU END AS ID_TIPO_IMPU "
            Result = LoadMulData(Desde, Campos, Condicion, VrImpDesc())
            If Result <> FAIL And Encontro Then
               Campos = "NU_ENTRAD_DETA, NU_COSTO_DETA"
               Campos = Campos & ", NU_IMPU_DETA, ((100*NU_COSTO_DETA)/(100+NU_IMPU_DETA)) AS NU_COST_SIVA" 'JAUM T29793
               Condicion = "NU_AUTO_ORGCABE_DETA = " & .TextMatrix(I&, 2)
               Result = LoadMulData("IN_DETALLE", Campos, Condicion, VrTotal())
               If Result <> FAIL Then
                  .TextMatrix(I&, 4) = "0"
                  For k& = 0 To UBound(VrTotal, 2)
                     'JAUM T29793 Inicio Se deja linea en comentario
                     '.TextMatrix(I&, 4) = .TextMatrix(I&, 4) + CDbl(VrTotal(0, k&) * VrTotal(1, k&))
                     DbValEntrada(I&) = CDbl(VrTotal(0, k&) * Aplicacion.Formatear_Valor(VrTotal(3, k&)))
                     DbValIva(I&) = (DbValEntrada(I&) * (VrTotal(2, k&) / 100))
                     If BoAproxCent Then
                        .TextMatrix(I&, 4) = .TextMatrix(I&, 4) + (DbValEntrada(I&) + AproxCentena(Round(DbValIva(I&))))
                     Else
                        .TextMatrix(I&, 4) = .TextMatrix(I&, 4) + (DbValEntrada(I&) + Aplicacion.Formatear_Valor(DbValIva(I&)))
                     End If
                     'JAUM T29793 Fin
                  Next
                  For J& = 0 To UBound(VrImpDesc, 2)
                     If VrImpDesc(0, J&) = "" Then
                        If BoAproxCent = True Then
                           'JAUM T29793 Inicio
                           If DbValEntrada(I&) <> 0 Then
                              DbDescu = DbDescu + AproxCentena(Round(Aplicacion.Formatear_Valor(DbValEntrada(I&) * (VrImpDesc(2, J&) / 100))))
                           Else
                           'JAUM T29793 Fin
                              DbDescu = DbDescu + AproxCentena(Round(Aplicacion.Formatear_Valor(.TextMatrix(I&, 4) * (VrImpDesc(2, J&) / 100))))
                           End If 'JAUM T29793
                        Else
                           'JAUM T29793 Inicio
                           If DbValEntrada(I&) <> 0 Then
                              DbDescu = DbDescu + Aplicacion.Formatear_Valor(DbValEntrada(I&) * (VrImpDesc(2, J&) / 100))
                           Else
                           'JAUM T29793 Fin
                              DbDescu = DbDescu + Aplicacion.Formatear_Valor(.TextMatrix(I&, 4) * (VrImpDesc(2, J&) / 100))
                           End If 'JAUM T29793
                        End If
                     Else
                        If BoAproxCent = True Then
                           'JAUM T29793 Inicio Se deja linea en comentario
                           'DbImpu = DbImpu + AproxCentena(Round(Aplicacion.Formatear_Valor(.TextMatrix(I&, 4) * (VrImpDesc(2, J&) / 100))))
                           If DbValEntrada(I&) <> 0 Then
                              DbImpu = DbImpu + AproxCentena(Round(Aplicacion.Formatear_Valor((DbValEntrada(I&) - DbDescu) * (VrImpDesc(2, J&) / 100))))
                           Else
                              DbImpu = DbImpu + AproxCentena(Round(Aplicacion.Formatear_Valor((.TextMatrix(I&, 4) - DbDescu) * (VrImpDesc(2, J&) / 100))))
                           End If
                           'JAUM T29793 Fin
                        Else
                           'JAUM T29793 Inicio
                           If DbValEntrada(I&) <> 0 Then
                              DbImpu = DbImpu + Aplicacion.Formatear_Valor((DbValEntrada(I&) - DbDescu) * (VrImpDesc(2, J&) / 100)) 'JAUM T28836
                           Else
                           'JAUM T29793 Fin
                              'DbImpu = DbImpu + Aplicacion.Formatear_Valor(.TextMatrix(I&, 4) * (VrImpDesc(2, J&) / 100)) 'JAUM T28836 Se deja linea en comentario
                              DbImpu = DbImpu + Aplicacion.Formatear_Valor((.TextMatrix(I&, 4) - DbDescu) * (VrImpDesc(2, J&) / 100)) 'JAUM T28836
                           End If 'JAUM T29793
                        End If
                     End If
                  Next
               End If
            Else
               For J& = 0 To UBound(arrXil, 2)
                  If arrXil(2, J&) <> "" Then
                     'JAUM T28836 Inicio
                     If arrXil(5, J&) = 0 Then
                        If BoAproxCent = True Then
                           DbDescu = DbDescu + AproxCentena(Round(Aplicacion.Formatear_Valor(arrXil(4, J&))))
                        Else
                           DbDescu = DbDescu + Aplicacion.Formatear_Valor(arrXil(4, J&))
                        End If
                     Else
                     'JAUM T28836 Fin
                        If BoAproxCent = True Then
                           DbDescu = DbDescu + AproxCentena(Round(Aplicacion.Formatear_Valor(.TextMatrix(I&, 4) * (arrXil(5, J&) / 100))))
                        Else
                           DbDescu = DbDescu + Aplicacion.Formatear_Valor(.TextMatrix(I&, 4) * (arrXil(5, J&) / 100))
                        End If
                     End If 'JAUM T28836
                  End If
               Next J&
               For J& = 0 To UBound(arrXil, 2)
                  If arrXil(1, J&) <> "" Then
                     If BoAproxCent = True Then
                        DbImpu = DbImpu + AproxCentena(Round(Aplicacion.Formatear_Valor((.TextMatrix(I&, 4) - DbDescu)) * (arrXil(5, J&) / 100)))
                     Else
                        DbImpu = DbImpu + Aplicacion.Formatear_Valor((.TextMatrix(I&, 4) - DbDescu) * (arrXil(5, J&) / 100))
                     End If
                  End If
               Next J&
            End If
            'JAUM T29815 Inicio se deja linea en comentario
            '.TextMatrix(i&, 8) = Format(Val(-DbDescu + -DbImpu), stMFormat)
            If boIPPTO Then
               ReDim VrArr(0)
               Condicion = "NU_AUTO_COMP_IMDE = " & vConsec & " AND NU_AUTO_ENCA_IMDE = " & .TextMatrix(I&, 2)
               Result = LoadData("IN_VAL_IMDE", "NU_VALOR_IMDE", Condicion, VrArr)
               If Result <> FAIL And Encontro Then
                  .TextMatrix(I&, 8) = Format(VrArr(0), stMFormat)
               End If
            End If
            'JAUM T29815 Fin
         Next I&
      End If
      'JAUM T28370-R22535 Fin
   End With

Exit Function
    
Anular:

End Function

Sub sbCVistas()
'JLPB T23820 INICIO SE DEJA EN COMENTARIO EL SIGUIENTE BLOQUE
''OMOG T17510 INICIO
'If boICxP Then
'   If ExisteCAMPO("PARAMETROS_CXP", "NU_REDIMDE_PCXP") Then
'      BoAproxCent = CBool(fnDevDato("PARAMETROS_CXP", "NU_REDIMDE_PCXP", NUL$))
'   End If
'End If
''OMOG T17510 FIN
'JLPB T23820 FIN

    Result = fnEliminarVista(stVista)
    'REOL M1643     'Se agrego NU_SALIDA_DETA a la consulta
    'REOL M1799     'Se cambia la forma en que se calcula el costo; <Cantidad x ValorSinIVA x (1+IVA)>
    'REOL M2237     'Se ajusta la consulta para que triaga valores redondeados, segun este parametrizado.
'    Result = fnCrearVista(stVista, _
 _
    "Select CD_CODI_TERC,NO_NOMB_TERC,NU_AUTO_ENCA,FE_CREA_ENCA," & _
        "Round(Sum(Round((100*NU_COSTO_DETA)/(100+NU_IMPU_DETA),2) * (1 + NU_IMPU_DETA/100) * (NU_ENTRAD_DETA-NU_SALIDA_DETA)),2) as NU_COST_TOTAL," & _
        "NU_COMP_ENCA " & _
    "From In_Encabezado, In_Detalle, Tercero " & _
    "Where CD_CODI_TERC_ENCA = CD_CODI_TERC And NU_AUTO_MODCABE_DETA = NU_AUTO_ENCA And NU_AUTO_DOCU_ENCA=3 And TX_ESTA_ENCA <> 'A' Group By CD_CODI_TERC,NO_NOMB_TERC,NU_AUTO_ENCA,NU_COMP_ENCA,FE_CREA_ENCA ", _
 _
    "Usado por la ventana de compra de art�culos, muestra los art�culos relacionados con los detalles y el tercero.")
    ReDim Arr(1)
    Result = LoadData("PARAMETROS_INVE", "ID_DECVAL_PINV,NU_DECVAL_PINV", NUL$, Arr)
    If Arr(0) = "N" Then Arr(1) = 0
    If Arr(1) = 1 Then Arr(1) = 2 'SKRV T17394
    Campos = "Select CD_CODI_TERC,NO_NOMB_TERC,NU_AUTO_ENCA,FE_CREA_ENCA,"
    'HRR T15434 Inicio
'    Campos = Campos & "Round(Sum(Round((100*NU_COSTO_DETA)/(100+NU_IMPU_DETA)," & Arr(1)
'    Campos = Campos & ") * Round((1 + NU_IMPU_DETA/100),2) * (NU_ENTRAD_DETA-NU_SALIDA_DETA))," & Arr(1)
    'HRR T15434 Fin
    'Campos = Campos & "SUM(ROUND((ROUND((100*NU_COSTO_DETA)/(100+NU_IMPU_DETA),2) * (NU_ENTRAD_DETA-NU_SALIDA_DETA))" 'SKRV T17394 se establece en comentario
    Campos = Campos & "SUM(ROUND((ROUND((100*NU_COSTO_DETA)/(100+NU_IMPU_DETA)," & Arr(1) & ") * (NU_ENTRAD_DETA-NU_SALIDA_DETA))" 'SKRV T17394
    'Campos = Campos & " + (ROUND((100*NU_COSTO_DETA)/(100+NU_IMPU_DETA),2) * (NU_IMPU_DETA/100) * (NU_ENTRAD_DETA-NU_SALIDA_DETA)),2)"'SKRV T17394 se establece en comentario
    'OMOG T17510 INICIO
    If BoAproxCent = True Then
       Campos = Campos & " + ROUND((ROUND((100*NU_COSTO_DETA)/(100+NU_IMPU_DETA)," & Arr(1) & ") * (NU_IMPU_DETA/100) * (NU_ENTRAD_DETA-NU_SALIDA_DETA)),-2)," & Arr(1) & ")"
    Else
       Campos = Campos & " + (ROUND((100*NU_COSTO_DETA)/(100+NU_IMPU_DETA)," & Arr(1) & ") * (NU_IMPU_DETA/100) * (NU_ENTRAD_DETA-NU_SALIDA_DETA))," & Arr(1) & ")" 'SKRV T17394
    End If
   'OMOG T17510 FIN
    'Campos = Campos & " + " & AproxCentena(Round( & "(ROUND((100*NU_COSTO_DETA)/(100+NU_IMPU_DETA)," & Arr(1) & ") * (NU_IMPU_DETA/100) * (NU_ENTRAD_DETA-NU_SALIDA_DETA))," & Arr(1) & ")" & )) 'SKRV T17394
    Campos = Campos & ") as NU_COST_TOTAL,NU_COMP_ENCA From In_Encabezado, In_Detalle, Tercero "
    Campos = Campos & "Where CD_CODI_TERC_ENCA = CD_CODI_TERC And NU_AUTO_MODCABE_DETA = NU_AUTO_ENCA And NU_AUTO_DOCU_ENCA=3 And TX_ESTA_ENCA <> 'A' "
    Campos = Campos & "Group By CD_CODI_TERC,NO_NOMB_TERC,NU_AUTO_ENCA,NU_COMP_ENCA,FE_CREA_ENCA"
    Result = fnCrearVista(stVista, Campos, "Usado por la ventana de compra de art�culos, muestra los art�culos relacionados con los detalles y el tercero.")
    'REOL M2237
    Debug.Print Campos
    Result = fnEliminarVista(stVistaRel)
    Result = fnCrearVista(stVistaRel, "Select NU_AUTO_ENCA_COEN,TX_ESTA_COMP From IN_COMPRA,IN_R_COMP_ENCA Where NU_AUTO_COMP=NU_AUTO_COMP_COEN", "Usado por la ventana de compra de art�culos, muestra los 'documentos' que ya se incluyeron en las compras.")
   
End Sub

Sub sbGuardar(boRealS As Boolean)
'boRealS = True -> El procedimiento es realmente para guardar.
'boRealS = False -> El procedimiento es una prueba, se realizan los procedimientos para guardar pero siempre se revierte la transacci�n.
'                    Se puede usar para obtener datos del proceso de guardado, como los movimientos contables que se hubieran hecho.
   
    Dim StValores, lgNext As Long, stExMsg As String, cObser As String, fDesbalance As Boolean, arrT(), cCond As String
    Dim I&, im&, J&, lgT As Long, lgCont As Long        'DEPURACION DE CODIGO
    Dim dbVlImpu As Double, dbVlDesc As Double
    Dim InI As Integer 'HRR R1853
    Dim InRespuesta As Integer 'AASV M3520
    Dim InFilMcuen As Integer   'LJSA M4502     N�mero de filas del que contienen informaci�n en el arreglo Mcuen
    Dim DbNeto As Double
    'JAUM T28836 Inicio
    Dim VrCxp() As Variant 'Almacena n�mero y valor de la cxp
    Dim DbDescImp As Double 'Almacena el valor del descuento o impuesto perteneciente a cada concepto hijo
    Dim dbDesc 'Almacena el valor total de los descuentos
    'JAUM T28836 Fin
    BoCruceCxP = False
    'DAHV M4136 ------------
    Dim InPos As Integer
    InPos = -1
    'DAHV M4136 ---------------
    ' DAHV M4133
    Dim InDec As Integer
    'LJSA M4730 Nota 21492 -------
    Dim BoChkAutonum As Boolean
    BoChkAutonum = False
    'LJSA M4730 Nota 21492 -------
       
    If Aplicacion.VerDecimales_Valores(InDec) Then
    Else
       InDec = 0
    End If
    ' DAHV M4133 Fin
    
    Erase VrAudMov() 'DRMG T43535 ELIMINA EL CONTENIDO DE LA MATRIZ
    
    dbVlImpu = 0: dbVlDesc = 0 'HR R1853
    
    ReDim arrMcuen(6, 1) 'Usada por 'Guardar_Movimiento_Contable'
    'JAGS R7812 INICIO
    If boICxP Then 'JAGS T8859
       If Not boIPPTO Then 'JAUM T28370-R22535
          Mnsj = "�Desea cruzar con algun anticipo la CXP?" & vbCrLf
          If MsgBox(Mnsj, vbYesNo) = vbYes Then
             Condicion = "ID_ANTI_COEG ='" & "S" & Comi & " AND ID_ESTA_COEG <> " & "2"
             Condicion = Condicion & " AND CD_TERC_COEG = '" & txtTercero & Comi
             Condicion = Condicion & " AND (VL_NETO_COEG - VL_ANTI_COEG) > 0"
             Seleccion4 "COMPRO_EGRE", "'',CD_CODI_COEG, FE_FECH_COEG,VL_NETO_COEG - VL_ANTI_COEG,'',''", "Seleccion de anticipos", Condicion 'HRR M1576
             If CanAnti = True Then Exit Sub 'JAGS T8739
             If Contador = 0 Then
                BoCruceCxP = False
             Else
                BoCruceCxP = True
             End If
                    
          End If
       End If 'JAUM T28370-R22535
    End If 'JAGS T8859
    'JAGS R7812 FIN
    'JAUM T28835 Inicio
    If boIPPTO Then
       If flexEntradas.Rows - 1 > 1 Then 'JAUM T29174
          ReDim Arr(0)
          Result = LoadData("CONCEPTO", "CD_CODI_CONC", "CD_CODI_CONC='CINM'", Arr)
          If Not Encontro Then
             'Mensaje1 "El concepto 'CINM' Compra inventario varias entra, no se encuentra creado en el m�dulo de cuentas por pagar", 2 JAUM T29173 Se deja linea en comentario
             Mensaje1 "El concepto 'CINM', no se encuentra creado en el m�dulo de cuentas por pagar", 1 'JAUM T29173
             Exit Sub
          End If
       End If 'JAUM T29174
    End If
    'JAUM T28835 Fin
    If boRealS Then H.sbW "Guardando...", 9
    If Not boRealS Then H.sbW "Verificando datos...", 6

If fnRowSels < 1 Then
    If boRealS Then H.sbMsg "No ha seleccionado entradas de art�culos,%2no se puede guardar.", cmdGuardar, 8: Exit Sub
    If Not boRealS Then H.sbMsg "No ha seleccionado entradas de art�culos.", cmdXLS, 8: Exit Sub
End If
''GAVL R5965  / T6197SENTENCIA PARA SABER SI LA OPCION ESTA ACTIVA EN CXP
'If boICxP Then
'    'If ExisteCAMPO("PARAMETROS_CXP", "UN_REDIMDE_PCXP") Then
'    If ExisteCAMPO("PARAMETROS_CXP", "NU_REDIMDE_P3CXP") Then 'GAVL T6265
'        'BoAproxCent = CBool(fnDevDato("PARAMETROS_CXP", "UN_REDIMDE_PCXP", NUL$))
'        BoAproxCent = CBool(fnDevDato("PARAMETROS_CXP", "NU_REDIMDE_PCXP", NUL$)) 'GAVL T6265
'    End If
'End If
''FIN GAVL R5965 / T6197

   'JLPB T21625 INICIO
   With flexEntradas
      If boICont And fTransit Then
         'Copiando la matriz de movimientos transitorios a revertir por art�culo.
         'Movimiento d�bito
         For I& = 0 To UBound(aTranCnArt, 2)
            lgT = UBound(arrMcuen, 2)
            arrMcuen(0, lgT) = aTranCnArt(0, I&)
            arrMcuen(1, lgT) = aTranCnArt(1, I&)
            arrMcuen(2, lgT) = aTranCnArt(2, I&)
            arrMcuen(3, lgT) = aTranCnArt(3, I&)
            arrMcuen(4, lgT) = aTranCnArt(4, I&)
            arrMcuen(5, lgT) = aTranCnArt(5, I&)
            arrMcuen(6, lgT) = aTranCnArt(6, I&)
            If fnCuenTerc(CStr(aTranCnArt(1, I&))) Then arrMcuen(4, lgT) = txtTercero
            ReDim Preserve arrMcuen(6, UBound(arrMcuen, 2) + 1)
         Next I&
            
         'Copiando la matriz de movimientos normales por art�culo.
         'Movimiento d�bito
         For I& = 0 To UBound(aNormCnArt, 2)
            lgT = UBound(arrMcuen, 2)
            arrMcuen(0, lgT) = aNormCnArt(0, I&)
            arrMcuen(1, lgT) = aNormCnArt(1, I&)
            If aNormCnArt(2, I&) = NUL$ Then aNormCnArt(2, I&) = 0
            arrMcuen(2, lgT) = Round(CDbl(aNormCnArt(2, I&)), InDec)
            arrMcuen(3, lgT) = aNormCnArt(3, I&)
            arrMcuen(4, lgT) = aNormCnArt(4, I&)
            arrMcuen(5, lgT) = aNormCnArt(5, I&)
            arrMcuen(6, lgT) = aNormCnArt(6, I&)
            If fnCuenTerc(CStr(aNormCnArt(1, I&))) Then arrMcuen(4, lgT) = txtTercero
            If IVADeducible() And aNormCnArt(7, I&) <> NUL$ Then arrMcuen(2, lgT) = arrMcuen(2, lgT) - Aplicacion.Formatear_Valor(aNormCnArt(7, I&))
            ReDim Preserve arrMcuen(6, UBound(arrMcuen, 2) + 1)
         Next I&
            
         'Cta para Impuesto de la Entrada cuando es IVA Deducible    'REOL M1709
         If IVADeducible() And DbVlrIvaEnt <> 0 Then
            lgT = UBound(arrMcuen, 2)
            arrMcuen(0, lgT) = "(Impuesto)"
            arrMcuen(1, lgT) = Buscar_Cuentas_Concepto("CPI", 1)
            arrMcuen(2, lgT) = DbVlrIvaEnt
            arrMcuen(3, lgT) = "D"
            arrMcuen(4, lgT) = txtTercero
            arrMcuen(6, lgT) = Aplicacion.Formatear_Valor(dbVlNeto)
            ReDim Preserve arrMcuen(6, UBound(arrMcuen, 2) + 1)
         End If
            
         'Copiando la matriz con el movimiento transitorio de la CxP a revertir por entrada.
         'MOVIMIENTO DEBITO
         For I& = 0 To UBound(aTranCnCxP, 2)
            lgT = UBound(arrMcuen, 2)
            arrMcuen(0, lgT) = aTranCnCxP(0, I&)
            arrMcuen(1, lgT) = aTranCnCxP(1, I&)
            arrMcuen(2, lgT) = aTranCnCxP(2, I&)
            arrMcuen(3, lgT) = aTranCnCxP(3, I&)
            arrMcuen(4, lgT) = aTranCnCxP(4, I&)
            arrMcuen(5, lgT) = aTranCnCxP(5, I&)
            arrMcuen(6, lgT) = aTranCnCxP(6, I&)
            If fnCuenTerc(CStr(aTranCnCxP(1, I&))) Then arrMcuen(4, lgT) = txtTercero
            ReDim Preserve arrMcuen(6, UBound(arrMcuen, 2) + 1)
         Next I&
            
         'MOVIMIENTO CREDITO
         For I& = 0 To UBound(aNormCnCxP, 2)
            lgT = UBound(arrMcuen, 2)
            arrMcuen(0, lgT) = aNormCnCxP(0, I&)
            arrMcuen(1, lgT) = IIf(aNormCnCxP(1, I&) <> NUL$, aNormCnCxP(1, I&), stCuenta)
            arrMcuen(2, lgT) = aNormCnCxP(2, I&)
            arrMcuen(3, lgT) = aNormCnCxP(3, I&)
            arrMcuen(3, lgT) = IIf(aNormCnCxP(3, I&) <> NUL$, aNormCnCxP(3, I&), "C")
            arrMcuen(4, lgT) = aNormCnCxP(4, I&)
            arrMcuen(5, lgT) = fnNCeCo(CStr(aNormCnCxP(1, I&)))
            arrMcuen(6, lgT) = aNormCnCxP(6, I&)
            If fnCuenTerc(CStr(aNormCnCxP(1, I&))) Then arrMcuen(4, lgT) = txtTercero
            ReDim Preserve arrMcuen(6, UBound(arrMcuen, 2) + 1)
            InPos = lgT
         Next I&
      End If
      If boICxP And (Not BoEntImpDesc) And boICont Then
         For InI = 0 To ClssImpuDesc.SizeDesc
            If ClssImpuDesc.ElemDesc(InI, 0) <> NUL$ Then
               If Trim(ClssImpuDesc.ElemDesc(InI, 5)) = "" Then stExMsg = "%2Si asigna la cuenta, deber� cambiar el concepto de impuestos para leer los nuevos cambios.": Mensaje1 "La cuenta del descuento, c�digo -> " & ClssImpuDesc.ElemDesc(InI, 0) & ", no ha sido parametrizada.", 2: GoTo Anular
               lgT = UBound(arrMcuen, 2)
               arrMcuen(0, lgT) = "(Descuento)"
               arrMcuen(1, lgT) = ClssImpuDesc.ElemDesc(InI, 5)
               arrMcuen(2, lgT) = ClssImpuDesc.ElemDesc(InI, 4)
               arrMcuen(3, lgT) = "C"
               arrMcuen(4, lgT) = txtTercero
               arrMcuen(6, lgT) = Aplicacion.Formatear_Valor(dbVlNeto)
               dbVlDesc = dbVlDesc + Aplicacion.Formatear_Valor(ClssImpuDesc.ElemDesc(InI, 4))
               ReDim Preserve arrMcuen(6, UBound(arrMcuen, 2) + 1)
            End If
         Next
         'Movimiento cr�dito
         For InI = 0 To ClssImpuDesc.SizeImp
            If ClssImpuDesc.ElemImp(InI, 0) <> NUL$ Then
               If Trim(ClssImpuDesc.ElemImp(InI, 5)) = "" Then stExMsg = "%2Si asigna la cuenta, deber� cambiar el concepto de impuestos-Descuentos para leer los nuevos cambios.": Mensaje1 "La cuenta del impuesto, c�digo -> " & ClssImpuDesc.ElemImp(InI, 0) & ", no ha sido parametrizada.", 2: GoTo Anular
               lgT = UBound(arrMcuen, 2)
               arrMcuen(0, lgT) = "(Impuesto)"
               arrMcuen(1, lgT) = ClssImpuDesc.ElemImp(InI, 5)
               arrMcuen(2, lgT) = ClssImpuDesc.ElemImp(InI, 4)
               arrMcuen(3, lgT) = "C"
               arrMcuen(4, lgT) = txtTercero
               If ClssImpuDesc.ElemImp(InI, 6) = "M" Then
                  arrMcuen(6, lgT) = Aplicacion.Formatear_Valor(DbVlrIvaEnt)
               Else
                  arrMcuen(6, lgT) = Aplicacion.Formatear_Valor(dbVlNeto)
                  If Buscar_desc_impuesto(ClssImpuDesc.ElemImp(InI, 0)) <> NUL$ And TipoPers = "0" Then
                     arrMcuen(6, lgT) = Aplicacion.Formatear_Valor(dbVlNeto - Valor_Descuento(Buscar_desc_impuesto(ClssImpuDesc.ElemImp(InI, 0)), dbVlNeto))
                  Else
                     'JAUM T28644-R27752 Inicio
                     If fnDevDato("TC_IMPUESTOS", "TX_RETEFU_IMPU", "CD_CODI_IMPU = '" & ClssImpuDesc.ElemImp(InI, 0) & Comi, True) = True Then
                        For I& = 0 To UBound(VrVBase, 2)
                           If ClssImpuDesc.ElemImp(InI, 0) = VrVBase(0, I&) Then
                              arrMcuen(2, lgT) = Aplicacion.Formatear_Valor(VrVBase(1, I&))
                              arrMcuen(6, lgT) = Aplicacion.Formatear_Valor(VrVBase(2, I&))
                              If ClssImpuDesc.ElemImp(InI, 4) = 0 Then dbVlImpu = dbVlImpu + CDbl(VrVBase(1, I&))
                              Exit For
                           End If
                        Next
                     Else
                     'JAUM T28644-R27752 Fin
                        arrMcuen(6, lgT) = Aplicacion.Formatear_Valor(dbVlNeto)
                     End If 'JAUM T28644-R27752
                  End If
               End If
               dbVlImpu = dbVlImpu + Aplicacion.Formatear_Valor(ClssImpuDesc.ElemImp(InI, 4))
               ReDim Preserve arrMcuen(6, UBound(arrMcuen, 2) + 1)
            End If
         Next
         'Movimiento cr�dito
         If (Not BoEntImpDesc) And boICxP And (Not fTransit) Then
            If (dbVlImpu + dbVlDesc) <> 0 Then
               lgT = UBound(arrMcuen, 2)
               arrMcuen(0, lgT) = "(CXP)"
               arrMcuen(1, lgT) = Aplicacion.PinvCuentaCxP
               arrMcuen(2, lgT) = dbVlImpu + dbVlDesc
               arrMcuen(3, lgT) = "D"
               arrMcuen(4, lgT) = txtTercero
               arrMcuen(6, lgT) = Aplicacion.Formatear_Valor(dbVlNeto)
               ReDim Preserve arrMcuen(6, UBound(arrMcuen, 2) + 1)
            End If
         End If
         If ((InPos <> -1)) Then
            If (arrMcuen(2, InPos) <> NUL$) Then
               arrMcuen(2, InPos) = arrMcuen(2, InPos) - (dbVlImpu + dbVlDesc)
            End If
         End If
      End If
   End With
   Dim stCodComp As String, dbBalance As Double
   If boICont And boRealS Then
      stExMsg = "%2Error al realizar los movimientos contables."
      dbBalance = fnBalance()
      If dbBalance <> 0 Then
         fDesbalance = True
         H.Hide
         If Not boRealS Then GoTo Pr�ximo
         MsgBox "Desbalance contable, los " & IIf(dbBalance > 0, "d�bitos", "cr�ditos") & " se superan en: " & vbCrLf & vbCrLf & dbBalance & vbCrLf & "Revise la parametrizaci�n de las cuentas", vbExclamation, "Interfaz contable"
      Else
         fDesbalance = False
      End If
      limpia_matriz Mcuen, 2000, 6
      InFilMcuen = 1
      For I& = 1 To UBound(arrMcuen, 2)
         Mcuen(InFilMcuen, 1) = arrMcuen(1, I&)
         Mcuen(InFilMcuen, 2) = IIf(arrMcuen(2, I&) <> NUL$, arrMcuen(2, I&), 0)     'LJSA M4502 Nota 20091
         Mcuen(InFilMcuen, 3) = arrMcuen(3, I&)
         Mcuen(InFilMcuen, 4) = arrMcuen(4, I&)
         Mcuen(InFilMcuen, 5) = arrMcuen(5, I&)
         Mcuen(InFilMcuen, 6) = arrMcuen(6, I&)
         InFilMcuen = InFilMcuen + 1
      Next I&
      continuar = False
      If fTransit Then muestra_cuenta "S", Me
      If Aplicacion.PinvImpComp And (Not fTransit) And ClssImpuDesc.TieneDatos Then
         muestra_cuenta "S", Me
      Else
         If Not fTransit Then continuar = True
      End If
      If Not continuar Then stExMsg = "%2Operaci�n cancelada por el usuario.": GoTo Anular
   End If
   'JLPB T21625 FIN

If (BeginTran(STranIns & "IN_COMPRA") <> FAIL) Then
    
    'Guardando la compra:
    StValores = "FE_FECH_COMP=" & fnFFechaHora(Nowserver)
    StValores = StValores & Coma & "FE_VENC_COMP=" & fnFFechaHora(txtFechaV)
    StValores = StValores & Coma & "CD_CODI_TERC_COMP='" & txtTercero & "'"
    'StValores = StValores & Coma & "TX_FAPR_COMP='" & txtN�mFact & "'"
    StValores = StValores & Coma & "TX_FAPR_COMP='" & UCase(txtN�mFact) & "'" 'JACC R2221
    StValores = StValores & Coma & "TX_OBSE_COMP='" & fnCambiarAT(txtObser) & "'"
'    stValores = stValores & Coma & "VL_BRUT_COMP=" & lblTotalB.Tag
    StValores = StValores & Coma & "VL_BRUT_COMP=" & CDbl(lblTotalB)  'REOL M1709
    StValores = StValores & Coma & "VL_DESC_COMP=" & -lblDesc.Tag
    StValores = StValores & Coma & "VL_IMPU_COMP=" & -lblImp.Tag
    StValores = StValores & Coma & "VL_FLET_COMP=" & txtFlete.Tag
    StValores = StValores & Coma & "VL_NETO_COMP=" & lblTotalN.Tag
    StValores = StValores & Coma & "TX_ESTA_COMP=1"
    StValores = StValores & Coma & "FE_FACT_COMP=" & fnFFechaHora(MskFechFactura) 'HRR R1873
    Result = DoInsertSQL("IN_COMPRA", StValores)
    If Result = FAIL Then GoTo Anular
    If Result <> FAIL Then BoChkAutonum = True      'LJSA M4730 Nota 21492
    lgConsec = GetMaxCod("IN_COMPRA", "NU_AUTO_COMP", NUL$)
    
    'Guardando los valores de In_Encabezado:
    With flexEntradas
    
    'Actualizando las observaciones de IN_ENCABEZADO:
    
    For I& = 1 To .Rows - 1
       If fnRowSel(I&) Then
           im& = I& - 1
           
          stExMsg = "%2Error actualizando observaciones en IN_ENCABEZADO."
          cObser = fnDevDato("IN_ENCABEZADO", "TX_OBSE_ENCA", "NU_AUTO_ENCA=" & .TextMatrix(I&, 2))
          cObser = Cambiar_Comas_Comillas(cObser) 'LDCR T10658
          cObser = cObser & " " & cObAdd & " " & lgConsec
          If DoUpdate("IN_ENCABEZADO", "TX_OBSE_ENCA='" & cObser & "'", "NU_AUTO_ENCA=" & .TextMatrix(I&, 2)) = FAIL Then GoTo Anular
              
           stExMsg = "%2Error relacionando la compra con los documentos."
           
           StValores = "NU_AUTO_COMP_COEN=" & lgConsec
           StValores = StValores & Coma & "NU_AUTO_ENCA_COEN=" & .TextMatrix(I&, 2)
           StValores = StValores & Coma & "VL_COST_COEN=" & .TextMatrix(I&, 4)
           
           Result = DoInsertSQL("IN_R_COMP_ENCA", StValores)
           If Result = FAIL Then GoTo Anular
               'HRR R1853
   '            For j& = 0 To UBound(arrSerCode)
   '                stValores = "NU_AUTO_COMP_CCOID=" & lgConsec
   '                stValores = stValores & Coma & "TX_NOMB_CCOID='" & arrSerIDN�m(j&) & "'"
   '                stValores = stValores & Coma & "NU_POSI_CCOID='" & j& & "'"
   '
   '                If Not IsEmpty(arrSerImpP(j&)) Then
   '                    stValores = stValores & Coma & "CD_CODI_IMPU_CCOID='" & arrSerCode(j&) & "'"
   '                    stValores = stValores & Coma & "TX_TIPO_CCOID='" & arrSerTipo(j&) & "'"
   '                    'stValores = stValores & Coma & "NU_VALO_CCOID=Null" 'arrSerImpV
   '                    stValores = stValores & Coma & "NU_VALO_CCOID=" & Val(arrSerImpV(j&))
   '                    stValores = stValores & Coma & "NU_PRAPL_CCOID=" & Val(arrSerImpP(j&))
   '                Else
   '                    stValores = stValores & Coma & "CD_CODI_DESC_CCOID=" & Val(arrSerCode(j&))
   '                    stValores = stValores & Coma & "TX_TIPO_CCOID='" & arrSerTipo(j&) & "'"
   '                    stValores = stValores & Coma & "NU_VALO_CCOID=" & Val(arrSerDescV(j&))
   '                    stValores = stValores & Coma & "NU_PRAPL_CCOID=" & Val(arrSerDescP(j&))
   '                End If
   '
   '                If Not IsEmpty(arrSerCode(j&)) Then
   '                    Result = DoInsertSQL("IN_R_COMP_IMDE", stValores)
   '                    If Result = FAIL Then GoTo Anular
   '                End If
   '            Next j&
               'HRR R1853
       End If
    Next I&
    'JAUM T29815 Inicio Almacena en la tabla IN_VAL_IMDE el total de los impuestos y descuentos
    For I& = 1 To .Rows - 1
       If boIPPTO Then
          Valores = "NU_AUTO_COMP_IMDE = " & lgConsec & ", NU_AUTO_ENCA_IMDE = " & .TextMatrix(I&, 2)
          Valores = Valores & ", NU_VALOR_IMDE = " & CDbl(.TextMatrix(I&, 8))
          Valores = Valores & ", NU_PPTO_IMDE = 1"
          Result = DoInsertSQL("IN_VAL_IMDE", Valores)
          If Result = FAIL Then GoTo Anular
       Else
          Valores = "NU_AUTO_COMP_IMDE = " & lgConsec & ", NU_AUTO_ENCA_IMDE = " & .TextMatrix(I&, 2)
          Valores = Valores & ", NU_VALOR_IMDE = 0" & ", NU_PPTO_IMDE = 0"
          Result = DoInsertSQL("IN_VAL_IMDE", Valores)
          If Result = FAIL Then GoTo Anular
       End If
    Next
    'JAUM T29815 Fin
    'HRR R1853
      'impuestos - descuentos aplicados a la entrada
      If boICxP And (Not BoEntImpDesc) Then 'SI la entrada no tenia impuestos - descuentos y la interfaz con cxp estaba activa
         For InI = 0 To ClssImpuDesc.SizeDesc
            If ClssImpuDesc.ElemDesc(InI, 0) <> NUL$ Then
               Valores = "NU_AUTO_COMP_CCOID=" & lgConsec
               Valores = Valores & Coma & "TX_NOMB_CCOID=" & Comi & ClssImpuDesc.ElemDesc(InI, 1) & Comi
               Valores = Valores & Coma & "NU_POSI_CCOID='" & J& & "'"
               Valores = Valores & Coma & "CD_CODI_DESC_CCOID=" & Comi & ClssImpuDesc.ElemDesc(InI, 0) & Comi
               Valores = Valores & Coma & "TX_TIPO_CCOID=" & Comi & ClssImpuDesc.ElemDesc(InI, 6) & Comi
               'Valores = Valores & Coma & "NU_VALO_CCOID=" & ClssImpuDesc.ElemDesc(Ini, 4) JAUM T28370-R22535 Se deja linea en comentario
               Valores = Valores & Coma & "NU_VALO_CCOID=" & CDbl(ClssImpuDesc.ElemDesc(InI, 4)) 'JAUM T28370-R22535
               Valores = Valores & Coma & "NU_PRAPL_CCOID=" & ClssImpuDesc.ElemDesc(InI, 2)
               Result = DoInsertSQL("IN_R_COMP_IMDE", Valores)
               If Result = FAIL Then GoTo Anular
            End If
         Next
         
         For InI = 0 To ClssImpuDesc.SizeImp
            If ClssImpuDesc.ElemImp(InI, 0) <> NUL$ Then
               Valores = "NU_AUTO_COMP_CCOID=" & lgConsec
               'Valores = Valores & Coma & "TX_NOMB_CCOID=" & Comi & ClssImpuDesc.ElemImp(Ini, 1) & Comi JAUM T28370-R22535 Se deja linea en comentario
               Valores = Valores & Coma & "TX_NOMB_CCOID=" & Comi & Cambiar_Comas_Comillas(ClssImpuDesc.ElemImp(InI, 1)) & Comi 'JAUM T28370-R22535
               Valores = Valores & Coma & "NU_POSI_CCOID='" & J& & "'"
               Valores = Valores & Coma & "CD_CODI_IMPU_CCOID=" & Comi & ClssImpuDesc.ElemImp(InI, 0) & Comi
               Valores = Valores & Coma & "TX_TIPO_CCOID=" & Comi & ClssImpuDesc.ElemImp(InI, 6) & Comi
               'Valores = Valores & Coma & "NU_VALO_CCOID=" & ClssImpuDesc.ElemImp(Ini, 4) JAUM T28370-R22535 Se deja linea en comentario
               Valores = Valores & Coma & "NU_VALO_CCOID=" & CDbl(ClssImpuDesc.ElemImp(InI, 4)) 'JAUM T28370-R22535
               Valores = Valores & Coma & "NU_PRAPL_CCOID=" & ClssImpuDesc.ElemImp(InI, 2)
               Result = DoInsertSQL("IN_R_COMP_IMDE", Valores)
               If Result = FAIL Then GoTo Anular
            End If
         Next
         
      End If
      'HRR R1853
    'JLPB T21625 INICIO EL SIGUIENTE BLOQUE SE DEJA EN COMENTARIO
'        '00 ============== INTERFASE CONTABLE ==============
'        If boICont And fTransit Then '�Se realiza transferencia a contabilidad? ======= INTERFASE CONTABLE
'
'            'Copiando la matriz de movimientos transitorios a revertir por art�culo.
'            For i& = 0 To UBound(aTranCnArt, 2)
'            lgT = UBound(arrMcuen, 2) 'Movimiento d�bito
'                arrMcuen(0, lgT) = aTranCnArt(0, i&)
'                arrMcuen(1, lgT) = aTranCnArt(1, i&)
'                arrMcuen(2, lgT) = aTranCnArt(2, i&)
'                arrMcuen(3, lgT) = aTranCnArt(3, i&)
'                arrMcuen(4, lgT) = aTranCnArt(4, i&)
'                arrMcuen(5, lgT) = aTranCnArt(5, i&)
'                arrMcuen(6, lgT) = aTranCnArt(6, i&)
'                If fnCuenTerc(CStr(aTranCnArt(1, i&))) Then arrMcuen(4, lgT) = txtTercero '|.DR.|, M:1449
'                ReDim Preserve arrMcuen(6, UBound(arrMcuen, 2) + 1) 'El �ltimo arreglo quedar� vacio, es usado por el procedimiento para detectar el final.
'            Next i&
'
'            'Copiando la matriz de movimientos normales por art�culo.
'            For i& = 0 To UBound(aNormCnArt, 2)
'            lgT = UBound(arrMcuen, 2) 'Movimiento d�bito
'                arrMcuen(0, lgT) = aNormCnArt(0, i&)
'                arrMcuen(1, lgT) = aNormCnArt(1, i&)
'                'arrMcuen(2, lgT) = Aplicacion.Formatear_Valor(aNormCnArt(2, i&))
'                If aNormCnArt(2, i&) = NUL$ Then aNormCnArt(2, i&) = 0 'AASV M5112
'                arrMcuen(2, lgT) = Round(CDbl(aNormCnArt(2, i&)), InDec) 'DAHV M4133 'HRR M4839'AASV M4839 Nota: 26106 Se devuelven los cambios
'                'If aNormCnArt(7, I&) = NUL$ Then aNormCnArt(7, I&) = 0 'HRR M4839'AASV M4839 Nota: 26106 Se devuelven los cambios
'               'arrMcuen(2, lgT) = Round(CDbl(aNormCnArt(2, I&)), InDec) + Round(CDbl(aNormCnArt(7, I&)), InDec) 'HRR M4839 'AASV M4839 Nota: 26106 Se devuelven los cambios
'                arrMcuen(3, lgT) = aNormCnArt(3, i&)
'                arrMcuen(4, lgT) = aNormCnArt(4, i&)
'                arrMcuen(5, lgT) = aNormCnArt(5, i&)
'                arrMcuen(6, lgT) = aNormCnArt(6, i&)
'                If fnCuenTerc(CStr(aNormCnArt(1, i&))) Then arrMcuen(4, lgT) = txtTercero '|.DR.|, M:1449
'                If IVADeducible() And aNormCnArt(7, i&) <> NUL$ Then arrMcuen(2, lgT) = arrMcuen(2, lgT) - Aplicacion.Formatear_Valor(aNormCnArt(7, i&)) 'HRR R1853 'menos valor de iva del articulo
'                ReDim Preserve arrMcuen(6, UBound(arrMcuen, 2) + 1) 'El �ltimo arreglo quedar� vacio, es usado por el procedimiento para detectar el final.
'            Next i&
'
'            'If IVADeducible() And DbVlrIvaEnt <> 0 Then arrMcuen(2, lgT) = arrMcuen(2, lgT) - DbVlrIvaEnt   'REOL M1709 'HRR R1853 ESTO SE REALIZA PARA CADA ARTICULO EN EL LOOP ANTERIOR
'            'Cta para Impuesto de la Entrada cuando es IVA Deducible    'REOL M1709
'            If IVADeducible() And DbVlrIvaEnt <> 0 Then
'                lgT = UBound(arrMcuen, 2)
'                arrMcuen(0, lgT) = "(Impuesto)"
'                arrMcuen(1, lgT) = Buscar_Cuentas_Concepto("CPI", 1)
'                arrMcuen(2, lgT) = DbVlrIvaEnt
'                arrMcuen(3, lgT) = "D"
'                arrMcuen(4, lgT) = txtTercero
'                arrMcuen(6, lgT) = Aplicacion.Formatear_Valor(dbVlNeto)
'                ReDim Preserve arrMcuen(6, UBound(arrMcuen, 2) + 1)
'                End If
'            ''REOL''
'
'            'Copiando la matriz con el movimiento transitorio de la CxP a revertir por entrada.
'            For i& = 0 To UBound(aTranCnCxP, 2)
'            lgT = UBound(arrMcuen, 2) 'Movimiento d�bito
'                arrMcuen(0, lgT) = aTranCnCxP(0, i&)
'                arrMcuen(1, lgT) = aTranCnCxP(1, i&)
'                arrMcuen(2, lgT) = aTranCnCxP(2, i&)
'                arrMcuen(3, lgT) = aTranCnCxP(3, i&)
'                arrMcuen(4, lgT) = aTranCnCxP(4, i&)
'                arrMcuen(5, lgT) = aTranCnCxP(5, i&)
'                arrMcuen(6, lgT) = aTranCnCxP(6, i&)
'                If fnCuenTerc(CStr(aTranCnCxP(1, i&))) Then arrMcuen(4, lgT) = txtTercero '|.DR.|, M:1449
'                ReDim Preserve arrMcuen(6, UBound(arrMcuen, 2) + 1) 'El �ltimo arreglo quedar� vacio, es usado por el procedimiento para detectar el final.
'            Next i&
'
'            'NYCM M2769
''            'Copiando la matriz con el movimiento normal de la CxP por compra (se suman todas las entradas y debe quedar un �tem).
'''           aNormCnCxP(2, 0) = val(lblTotal.Tag)
''            'aNormCnCxP(2, 0) = CDbl(lblTotal)   'REOL M1709 'HRR R1853 Esto se soluciona generando un movimiento contable debitando la cuenta cxp x el valor de los impuestos aplicados en la compra
''            'se realiza mas abajo con el req R1853
''            lgT = UBound(arrMcuen, 2) 'Movimiento credito
''            arrMcuen(0, lgT) = aNormCnCxP(0, 0)
''            arrMcuen(1, lgT) = aNormCnCxP(1, 0)
''            arrMcuen(2, lgT) = aNormCnCxP(2, 0)
''            arrMcuen(3, lgT) = aNormCnCxP(3, 0)
''            arrMcuen(4, lgT) = aNormCnCxP(4, 0)
''            arrMcuen(5, lgT) = aNormCnCxP(5, 0)
''            arrMcuen(6, lgT) = aNormCnCxP(6, 0)
''            If fnCuenTerc(CStr(aNormCnCxP(1, 0))) Then arrMcuen(4, lgT) = txtTercero '|.DR.|, M:1449
'''                arrMcuen(2, lgT) = arrMcuen(2, lgT) - CDbl(lblImp)  'REOL M1709
''            ReDim Preserve arrMcuen(6, UBound(arrMcuen, 2) + 1) 'El �ltimo arreglo quedar� vacio, es usado por el procedimiento para detectar el final.
'            'NYCM M2769
'
'            'DAHV M4136 ------------------INICIO ------------------------------
''            For i& = 0 To UBound(aNormCnCxP, 2)
''                lgT = UBound(arrMcuen, 2) 'Movimiento credito
''                arrMcuen(0, lgT) = aNormCnCxP(0, i&)
''                'arrMcuen(1, lgT) = aNormCnCxP(1, i&)
''                arrMcuen(1, lgT) = aNormCnCxP(1, i&)
''                arrMcuen(1, lgT) = IIf(aNormCnCxP(1, i&) <> NUL$, aNormCnCxP(1, i&), stCuenta)   'LJSA M4502 Nota 20091
''                'arrMcuen(2, lgT) = aNormCnCxP(2, i&)
''                'arrMcuen(2, lgT) = lblTotal 'DAHV M3890 Se debe Manejar es el valor neto de la compra no el valor bruto
''                If ClssImpuDesc.ElemDesc(0, 0) <> NUL$ And ClssImpuDesc.ElemImp(0, 0) <> NUL$ Then 'DAHV M4133
''                  'arrMcuen(2, lgT) = lblTotal
''                  arrMcuen(2, lgT) = aNormCnCxP(2, i&)  'DAHV M4136
''                Else
''                  'arrMcuen(2, lgT) = aNormCnCxP(2, i&)
''                  arrMcuen(2, lgT) = lblTotal 'DAHV M4136
''                End If
''                arrMcuen(3, lgT) = aNormCnCxP(3, i&)
''                arrMcuen(3, lgT) = IIf(aNormCnCxP(3, i&) <> NUL$, aNormCnCxP(3, i&), "C")   'LJSA M4502    Nota 20091
''                arrMcuen(4, lgT) = aNormCnCxP(4, i&)
''                'arrMcuen(5, lgT) = aNormCnCxP(5, 0)
''                arrMcuen(5, lgT) = fnNCeCo(CStr(aNormCnCxP(1, i&)))  'DAHV M4133
''                arrMcuen(6, lgT) = aNormCnCxP(6, i&)
''                If fnCuenTerc(CStr(aNormCnCxP(1, i&))) Then arrMcuen(4, lgT) = txtTercero '|.DR.|, M:1449
'''                arrMcuen(2, lgT) = arrMcuen(2, lgT) - CDbl(lblImp)  'REOL M1709
''            ReDim Preserve arrMcuen(6, UBound(arrMcuen, 2) + 1) 'El �ltimo arreglo quedar� vacio, es usado por el procedimiento para detectar el final.
''            Next i&
'
'
'            'MOVIMIENTO CREDITO
'
'            For i& = 0 To UBound(aNormCnCxP, 2)
'                lgT = UBound(arrMcuen, 2)
'                arrMcuen(0, lgT) = aNormCnCxP(0, i&)
'                arrMcuen(1, lgT) = IIf(aNormCnCxP(1, i&) <> NUL$, aNormCnCxP(1, i&), stCuenta)
'                arrMcuen(2, lgT) = aNormCnCxP(2, i&)
'                arrMcuen(3, lgT) = aNormCnCxP(3, i&)
'                arrMcuen(3, lgT) = IIf(aNormCnCxP(3, i&) <> NUL$, aNormCnCxP(3, i&), "C")
'                arrMcuen(4, lgT) = aNormCnCxP(4, i&)
'                arrMcuen(5, lgT) = fnNCeCo(CStr(aNormCnCxP(1, i&)))
'                arrMcuen(6, lgT) = aNormCnCxP(6, i&)
'                If fnCuenTerc(CStr(aNormCnCxP(1, i&))) Then arrMcuen(4, lgT) = txtTercero
'                ReDim Preserve arrMcuen(6, UBound(arrMcuen, 2) + 1) 'El �ltimo arreglo quedar� vacio, es usado por el procedimiento para detectar el final.
'                InPos = lgT
'            Next i&
'            'DAHV M4136 ------------- FIN -----------------------------------
'
'            '/------------------
'        End If
'        '00 Fin. ============== INTERFASE CONTABLE ==============
'
'                    'HRR R1853
'                    '01 Impuestos y descuentos ============== INTERFASE CONTABLE ==============
''                    If boICont And fTransit Then
''
''                        For j& = 0 To UBound(arrSerCuen)
''                        If Val(arrSerVFinal(j&)) <> 0 Then
''                            If Trim(arrSerCuen(j&)) = "" Then stExMsg = "%2Si asigna la cuenta, deber� reasignar los impuestos para leer los nuevos cambios.": Mensaje1 "La cuenta del " & IIf(arrSerTipo(j&) = "I", "impuesto", "descuento") & ", c�digo -> " & arrSerCode(j&) & ", no ha sido parametrizada.", 2: GoTo Anular
''                            If Val(arrSerVFinal(j&)) <> 0 Then
''                               If arrSerTipo(j&) = "I" And Not IVADeducible() Then 'REOL M1709
''                                  arrMcuen(2, lgT) = arrMcuen(2, lgT) - arrSerVFinal(j&)        'REOL M1709
''                               Else
''                                  lgT = UBound(arrMcuen, 2) 'Movimiento cr�dito
''                                  arrMcuen(0, lgT) = IIf(IsEmpty(arrSerImpP(j&)), "(Descuento)", "(Impuesto)")
''                                  arrMcuen(1, lgT) = arrSerCuen(j&)
'''                                  arrMcuen(2, lgT) = arrSerVFinal(j&)
''                                  arrMcuen(2, lgT) = IIf(arrSerVFinal(j&) > 0, arrSerVFinal(j&), arrSerVFinal(j&) * -1)     'REOL M1709
'''                                  arrMcuen(3, lgT) = "C"
''                                 arrMcuen(3, lgT) = IIf(arrSerTipo(j&) = "O", "D", "C") 'REOL M1709
''                                  arrMcuen(4, lgT) = txtTercero
''                                  'arrMcuen(6, lgT) = arrSerVFinal(j&)
''                                  arrMcuen(6, lgT) = Aplicacion.Formatear_Valor(dbVlNeto) 'PJCA M1862
''                                ReDim Preserve arrMcuen(6, UBound(arrMcuen, 2) + 1) 'El �ltimo arreglo quedar� vacio, es usado por el procedimiento para detectar el final.
''                               End If
''                            End If
''                        End If
''                        Next j&
''                     End If
'                      'HRR R1853
'
'                     'HRR R1853
'                        If boICxP And (Not BoEntImpDesc) And boICont Then 'SI la entrada no tenia impuestos - descuentos y la interfaz con cxp estaba activa
'                           For Ini = 0 To ClssImpuDesc.SizeDesc
'                              If ClssImpuDesc.ElemDesc(Ini, 0) <> NUL$ Then
'                                  If Trim(ClssImpuDesc.ElemDesc(Ini, 5)) = "" Then stExMsg = "%2Si asigna la cuenta, deber� cambiar el concepto de impuestos para leer los nuevos cambios.": Mensaje1 "La cuenta del descuento, c�digo -> " & ClssImpuDesc.ElemDesc(Ini, 0) & ", no ha sido parametrizada.", 2: GoTo Anular
'                                  lgT = UBound(arrMcuen, 2) 'Movimiento cr�dito
'                                  arrMcuen(0, lgT) = "(Descuento)"
'                                  arrMcuen(1, lgT) = ClssImpuDesc.ElemDesc(Ini, 5) 'CUENTA
'                                  arrMcuen(2, lgT) = ClssImpuDesc.ElemDesc(Ini, 4)     'VALOR
'                                  arrMcuen(3, lgT) = "C"
'                                  arrMcuen(4, lgT) = txtTercero
'                                  arrMcuen(6, lgT) = Aplicacion.Formatear_Valor(dbVlNeto)
'                                  dbVlDesc = dbVlDesc + Aplicacion.Formatear_Valor(ClssImpuDesc.ElemDesc(Ini, 4))
'                                  ReDim Preserve arrMcuen(6, UBound(arrMcuen, 2) + 1) 'El �ltimo arreglo quedar� vacio, es usado por el procedimiento para detectar el final.
'                               End If
'                            Next
'                            For Ini = 0 To ClssImpuDesc.SizeImp
'                              If ClssImpuDesc.ElemImp(Ini, 0) <> NUL$ Then
'                                  If Trim(ClssImpuDesc.ElemImp(Ini, 5)) = "" Then stExMsg = "%2Si asigna la cuenta, deber� cambiar el concepto de impuestos-Descuentos para leer los nuevos cambios.": Mensaje1 "La cuenta del impuesto, c�digo -> " & ClssImpuDesc.ElemImp(Ini, 0) & ", no ha sido parametrizada.", 2: GoTo Anular
'                                  lgT = UBound(arrMcuen, 2) 'Movimiento cr�dito
'                                  arrMcuen(0, lgT) = "(Impuesto)"
'                                  arrMcuen(1, lgT) = ClssImpuDesc.ElemImp(Ini, 5) 'CUENTA
'                                  arrMcuen(2, lgT) = ClssImpuDesc.ElemImp(Ini, 4)     'VALOR
'                                  arrMcuen(3, lgT) = "C"
'                                  arrMcuen(4, lgT) = txtTercero
'                                  If ClssImpuDesc.ElemImp(Ini, 6) = "M" Then 'Iva retenido
'                                    arrMcuen(6, lgT) = Aplicacion.Formatear_Valor(DbVlrIvaEnt) 'Valor base
'                                  Else
'                                    arrMcuen(6, lgT) = Aplicacion.Formatear_Valor(dbVlNeto) 'Valor base
'
'                                     ''JACC R2345
'                                    If Buscar_desc_impuesto(ClssImpuDesc.ElemImp(Ini, 0)) <> NUL$ And TipoPers = "0" Then
'                                       arrMcuen(6, lgT) = Aplicacion.Formatear_Valor(dbVlNeto - Valor_Descuento(Buscar_desc_impuesto(ClssImpuDesc.ElemImp(Ini, 0)), dbVlNeto))
'                                    Else
'                                       arrMcuen(6, lgT) = Aplicacion.Formatear_Valor(dbVlNeto) 'Valor base
'                                    End If
'                                    'JACC R2345
'
'
'                                  End If
'                                  dbVlImpu = dbVlImpu + Aplicacion.Formatear_Valor(ClssImpuDesc.ElemImp(Ini, 4))
'                                  ReDim Preserve arrMcuen(6, UBound(arrMcuen, 2) + 1) 'El �ltimo arreglo quedar� vacio, es usado por el procedimiento para detectar el final.
'                               End If
'                            Next
'
'                            'LJSA M4502-----------------------Nota 20091
'                             'If boICxP And (Not BoEntImpDesc) And boICont And (Not fTransit) Then
'                             '   For i& = 0 To UBound(aNormCnCxP, 2)
'                             '       lgT = UBound(arrMcuen, 2) 'Movimiento credito
'                             '       arrMcuen(0, lgT) = aNormCnCxP(0, i&)
'                             '       arrMcuen(1, lgT) = IIf(aNormCnCxP(1, i&) <> NUL$, aNormCnCxP(1, i&), stCuenta)
'                             '       If ClssImpuDesc.ElemDesc(0, 0) <> NUL$ And ClssImpuDesc.ElemImp(0, 0) <> NUL$ Then
'                             '           arrMcuen(2, lgT) = aNormCnCxP(2, i&)
'                             '       Else
'                             '           arrMcuen(2, lgT) = lblTotal
'                             '       End If
'                             '       arrMcuen(3, lgT) = IIf(aNormCnCxP(3, i&) <> NUL$, aNormCnCxP(3, i&), "C")
'                             '       arrMcuen(4, lgT) = aNormCnCxP(4, i&)
'                             '       arrMcuen(5, lgT) = fnNCeCo(CStr(aNormCnCxP(1, i&)))  'DAHV M4133
'                             '       arrMcuen(6, lgT) = aNormCnCxP(6, i&)
'                             '       If fnCuenTerc(CStr(aNormCnCxP(1, i&))) Then arrMcuen(4, lgT) = txtTercero
'                             '       ReDim Preserve arrMcuen(6, UBound(arrMcuen, 2) + 1) 'El �ltimo arreglo quedar� vacio, es usado por el procedimiento para detectar el final.
'                             '   Next i&
'
'                             '   For i& = 0 To UBound(aNormCnArt, 2)
'                             '       lgT = UBound(arrMcuen, 2) 'Movimiento d�bito
'                             '       arrMcuen(0, lgT) = aNormCnArt(0, i&)
'                             '       arrMcuen(1, lgT) = aNormCnArt(1, i&)
'                             '       arrMcuen(2, lgT) = Round(CDbl(aNormCnArt(2, i&)), InDec)
'                             '       arrMcuen(3, lgT) = aNormCnArt(3, i&)
'                             '       arrMcuen(4, lgT) = aNormCnArt(4, i&)
'                             '       arrMcuen(5, lgT) = aNormCnArt(5, i&)
'                             '       arrMcuen(6, lgT) = aNormCnArt(6, i&)
'                             '      If fnCuenTerc(CStr(aNormCnArt(1, i&))) Then arrMcuen(4, lgT) = txtTercero
'                             '      If IVADeducible() And aNormCnArt(7, i&) <> NUL$ Then arrMcuen(2, lgT) = arrMcuen(2, lgT) - Aplicacion.Formatear_Valor(aNormCnArt(7, i&)) 'HRR R1853 'menos valor de iva del articulo
'                             '       ReDim Preserve arrMcuen(6, UBound(arrMcuen, 2) + 1) 'El �ltimo arreglo quedar� vacio, es usado por el procedimiento para detectar el final.
'                             '   Next i&
'                            'End If
'                            'LJSA M4502----------------------Nota 20091
'
'
'                            'LJSA M4502 - Se habilita c�digo seg�n Nota 20091 -
'
'                            'DAHV M3890 - Se comentarea este codigo debido a que se realiza cambio en el momento de procesar el valor
'                            ' de la cuenta por pagar por lo cual no se debe realizar las partidas debito de forma explicita
'
'
''                            If (Not BoEntImpDesc) And boICxP Then 'no se agrego impuestos - descuentos en la entrada - tiene activa la interfaz con cxp
'                            If (Not BoEntImpDesc) And boICxP And (Not fTransit) Then 'no se agrego impuestos - descuentos en la entrada - tiene activa la interfaz con cxp
'                               If (dbVlImpu + dbVlDesc) <> 0 Then
'                                 lgT = UBound(arrMcuen, 2) 'Movimiento cr�dito
'                                 arrMcuen(0, lgT) = "(CXP)"
'                                 arrMcuen(1, lgT) = Aplicacion.PinvCuentaCxP 'CUENTA
'                                 arrMcuen(2, lgT) = dbVlImpu + dbVlDesc   'VALOR
'                                 arrMcuen(3, lgT) = "D"
'                                 arrMcuen(4, lgT) = txtTercero
'                                 arrMcuen(6, lgT) = Aplicacion.Formatear_Valor(dbVlNeto) 'Valor base
'                                 ReDim Preserve arrMcuen(6, UBound(arrMcuen, 2) + 1) 'El �ltimo arreglo quedar� vacio, es usado por el procedimiento para detectar el final.
'                              End If
'                            End If
'
'                           'DAHV M4136 ----
'                           If ((InPos <> -1)) Then
'                              If (arrMcuen(2, InPos) <> NUL$) Then
'                                    arrMcuen(2, InPos) = arrMcuen(2, InPos) - (dbVlImpu + dbVlDesc)
'                              End If
'                           End If
'                           'DAHV M4136 ----
'
'                       End If
'                        'HRR R1853
'
'                        'LJSA M4502 - Se habilita c�digo seg�n Nota 20091 -
'
'                    '01 Fin. ============== INTERFASE CONTABLE ==============
'
'
'
'
   'JLPB TT21625 FIN
    End With
   'JLPB TT21625 INICIO EL SIGUIENTE BLOQUE SE DEJA EN COMENTARIO
'' ============== INTERFASE CONTABLE ==============
''02 Realizando movimientos contables:
'        Dim stCodComp As String, dbBalance As Double
'        If boICont And boRealS Then '�Se realiza transferencia a contabilidad?
'            stExMsg = "%2Error al realizar los movimientos contables."
'            dbBalance = fnBalance() 'Coprobando que el balance est� correcto.
'            If dbBalance <> 0 Then
'                fDesbalance = True
'                H.Hide
'                If Not boRealS Then GoTo Pr�ximo
'                'MsgBox "Desbalance contable, los " & IIf(dbBalance > 0, "d�bitos", "cr�ditos") & " se superan en: " & vbCrLf & vbCrLf & dbBalance, vbExclamation, "Interfaz contable"
'                MsgBox "Desbalance contable, los " & IIf(dbBalance > 0, "d�bitos", "cr�ditos") & " se superan en: " & vbCrLf & vbCrLf & dbBalance & vbCrLf & "Revise la parametrizaci�n de las cuentas", vbExclamation, "Interfaz contable"
''                    H.sbW "Creando documento de Excel...", 4
''                    sbResumenMov
''                    H.Hide
''                GoTo Anular '***
'            Else
'                fDesbalance = False
'            End If
'
'            'Copiando la matriz global de movimientos a la matriz compatible con Guardar_movimiento_contable.
'            limpia_matriz Mcuen, 2000, 6
'            InFilMcuen = 1      'LJSA M4502
'            For i& = 1 To UBound(arrMcuen, 2)
'                'If ((arrMcuen(1, i&) <> NUL$) Or (arrMcuen(2, i&) <> NUL$) Or (arrMcuen(3, i&) <> NUL$) Or (arrMcuen(4, i&) <> NUL$) Or (arrMcuen(5, i&) <> NUL$) Or (arrMcuen(6, i&) <> NUL$)) Then     'LJSA M4502 Nota 20091
'                    'Mcuen(i&, 1) = arrMcuen(1, i&)
'                    'Mcuen(i&, 2) = arrMcuen(2, i&)
'                    'Mcuen(i&, 3) = arrMcuen(3, i&)
'                    'Mcuen(i&, 4) = arrMcuen(4, i&)
'                    'Mcuen(i&, 5) = arrMcuen(5, i&)
'                    'Mcuen(i&, 6) = arrMcuen(6, i&)
'                    'LJSA M4502---------
'                    Mcuen(InFilMcuen, 1) = arrMcuen(1, i&)
'                    'Mcuen(InFilMcuen, 2) = arrMcuen(2, i&)
'                    Mcuen(InFilMcuen, 2) = IIf(arrMcuen(2, i&) <> NUL$, arrMcuen(2, i&), 0)     'LJSA M4502 Nota 20091
'                    Mcuen(InFilMcuen, 3) = arrMcuen(3, i&)
'                    Mcuen(InFilMcuen, 4) = arrMcuen(4, i&)
'                    Mcuen(InFilMcuen, 5) = arrMcuen(5, i&)
'                    Mcuen(InFilMcuen, 6) = arrMcuen(6, i&)
'                    InFilMcuen = InFilMcuen + 1
'                    'LJSA M4502---------
'                'End If      'LJSA M4502 Nota 20091
'            Next i&
'
'            'continuar = Not fTransit 'Si no hay cuentas transitorias por revertir, no debe mostrar la ventana de contabilidad 'HRR M2669
'            continuar = False 'HRR M2669
'            'NMSR M3421
'            If fTransit Then muestra_cuenta "S", Me 'Si no hay cuentas transitorias por revertir, no debe mostrar la ventana de contabilidad
'            'If Aplicacion.PinvImpComp And (Not fTransit) And ClssImpuDesc.TieneDatos Then muestra_cuenta "S", Me 'HRR M2644
'            'If Not Aplicacion.PinvImpComp And (Not fTransit) Then continuar = True  'REOL M2727
'            'NMSR M3421
'            If Aplicacion.PinvImpComp And (Not fTransit) And ClssImpuDesc.TieneDatos Then
'                muestra_cuenta "S", Me
'            Else
'                If Not fTransit Then continuar = True
'            End If 'NMSR M3421
'            If Not continuar Then stExMsg = "%2Operaci�n cancelada por el usuario.": GoTo Anular
'
   If boICont And boRealS Then
   'JLPB TT21625 FIN
            stCodComp = "CPI" 'Establecer c�digo del comprobante
            'If Trim(stCodComp) = "" Then Mensaje1 "No ha parametrizado el comprobante de compra, en par�metros de la aplicaci�n.", 2: GoTo Anular
            lgNext = 1 + Val(fnDevDato("TC_COMPROBANTE", "NU_CONS_COMP", "CD_CODI_COMP='" & stCodComp & "'"))
            
            'Guardar_Movimiento_Contable "CINV", txtFecha, stCodComp, lgNext, "COMPRA No. " & lgConsec, "" 'HRR R1853
            'Guardar_Movimiento_Contable ClssImpuDesc.concepto, txtFecha, stCodComp, lgNext, "COMPRA No. " & lgConsec, ""  'HRR R1853 'HRR R1873
'AASV R2238 INICIO SE COLOCA EN COMENTARIO
'            'HRR R1873
'             If Mcuen(1, 1) = NUL$ Then
'               Mcuen(1, 1) = Buscar_Cuentas_Concepto("CPI", 1)
'               If Mcuen(1, 1) = NUL$ Then Mensaje1 "La cuenta iva del comprobante CPI no ha sido parametrizada.", 2: GoTo Anular
'                Mcuen(1, 2) = 0
'                Mcuen(1, 3) = "C"
'                Mcuen(1, 4) = txtTercero
'                Mcuen(1, 6) = 0
'                'Guardar_Movimiento_Contable ClssImpuDesc.concepto, StFechaMovContable, stCodComp, lgNext, "No se genero movimiento contable a la COMPRA No. " & lgConsec & " porque en la entrada se realizo el movimiento correspondiente.", "" 'HRR R1873
'                'Guardar_Movimiento_Contable ClssImpuDesc.concepto, StFechaMovContable, stCodComp, lgNext, "No se genero movimiento contable a la FACTURA No." & (txtN�mFact) & "" & "COMPRA No. " & lgConsec & " porque en la entrada se realizo el movimiento correspondiente.", "" 'AASV R1323
'                Guardar_Movimiento_Contable ClssImpuDesc.concepto, StFechaMovContable, stCodComp, lgNext, "No se genero movimiento contable a la FACTURA No." & (txtN�mFact) & " " & "COMPRA No. " & lgConsec & " porque en la entrada se realizo el movimiento correspondiente.", "" 'AASV M3485
'            Else
'                 'Guardar_Movimiento_Contable ClssImpuDesc.concepto, StFechaMovContable, stCodComp, lgNext, "COMPRA No. " & lgConsec, ""
'                 'Guardar_Movimiento_Contable ClssImpuDesc.concepto, StFechaMovContable, stCodComp, lgNext, "FACTURA No." & (txtN�mFact) & "" & "COMPRA No. " & lgConsec, "" 'AASV R1323
'                 Guardar_Movimiento_Contable ClssImpuDesc.concepto, StFechaMovContable, stCodComp, lgNext, "FACTURA No." & (txtN�mFact) & " " & "COMPRA No. " & lgConsec, "" 'AASV R1323
'            End If
'            'HRR R1873
            
'AASV R2238 FIN
'AASV M4610 INICIO
            If lgNext <> lgConsec Then lgNext = lgConsec 'AASV M4242
            'If lgNext < lgConsec Then lgNext = lgConsec 'AASV M5699 Se cambia la validacion para que no genere error en la tabla movimientos cuando el comprobante tenga un consecutivo mayor que la compra.
            'If Mcuen(1, 1) <> NUL$ Then Guardar_Movimiento_Contable ClssImpuDesc.concepto, StFechaMovContable, stCodComp, lgNext, "FACTURA No." & (txtN�mFact) & " " & "COMPRA No. " & lgConsec, ""
            'If Mcuen(1, 1) <> NUL$ Then Guardar_Movimiento_Contable ClssImpuDesc.concepto, StFechaMovContable, stCodComp, lgNext, "FACTURA No." & UCase(txtN�mFact) & " " & "COMPRA No. " & lgConsec, "" 'JACC R2221'SKRV T24261/R22366 comentario
            'If Mcuen(1, 1) <> NUL$ Then Guardar_Movimiento_Contable ClssImpuDesc.concepto, StFechaMovContable, stCodComp, lgNext, "FACTURA No." & UCase(txtN�mFact) & " " & "COMPRA No. " & lgConsec, "", , , "0609", cmbAuto 'SKRV T24261/R22366 SKRV T24951 COMENTARIO
            If Mcuen(1, 1) <> NUL$ Then Guardar_Movimiento_Contable ClssImpuDesc.concepto, StFechaMovContable, stCodComp, lgNext, "FACTURA No." & UCase(txtN�mFact) & " " & "COMPRA No. " & lgConsec, "", , , fnDevDato("MODU_MOV_NIIF", "NU_CODMO_MODU", "TX_DESCM_MODU = 'Compra Inventarios'", True), cmbAuto 'SKRV T24951
'AASV M4610 FIN
            If Result <> FAIL Then Result = DoUpdate("TC_COMPROBANTE", "NU_CONS_COMP=" & lgNext, "CD_CODI_COMP='" & stCodComp & "'")
            If Result = FAIL Then GoTo Anular
            lgCont = lgNext
            
            'Guardando en la tabla In_Compra, los datos del movimiento contable:
            stExMsg = "%2Error al actualizar un registro en la tabla IN_COMPRA (interfase contable)."
            StValores = "NU_CONS_MOVI_COMP=" & lgNext
            If DoUpdate("IN_COMPRA", StValores, "NU_AUTO_COMP=" & lgConsec) = FAIL Then GoTo Anular
        'AASV M4826 Inicio
        Else
            stCodComp = "CPI" 'Establecer c�digo del comprobante
            lgNext = 1 + Val(fnDevDato("TC_COMPROBANTE", "NU_CONS_COMP", "CD_CODI_COMP='" & stCodComp & "'"))
            If lgNext <> lgConsec Then lgNext = lgConsec 'AASV M4242
            If Result <> FAIL Then Result = DoUpdate("TC_COMPROBANTE", "NU_CONS_COMP=" & lgNext, "CD_CODI_COMP='" & stCodComp & "'")
            If Result = FAIL Then GoTo Anular  'AASV M4826 Nota:22288
        'AASV M4826 Fin
        End If
   'DRMG T43535 INICIO 'SE AGREGA AUDITORIA DE MOVIMIENTOS
   If Result <> FAIL Then
      Result = Almacenar_Auditoria_Mov(CDbl(lgConsec), stCodComp, CDbl(lgNext), CDbl(AutonumUser))
      If Result = FAIL Then Call Mensaje1("Fallo al insertar en la tabla [IN_AUDMOVCONT]", 1): GoTo Anular
   End If
   'DRMG T43535 FIN
'02 Fin.
' ============== INTERFASE CONTABLE ==============

' ============== CUENTAS POR PAGAR ==============
'03
    Dim vUno As MSComctlLib.ListItem, vNumero As Long
    Dim arrCPT(0), vAutoDocu As Long
    Dim LnAutoDocuMul() As Long 'JAUM T28370-R22535 Almacena el c�digo cuando es por cuentas multiples
    If boICxP Then

        vAutoDocu = Val(flexEntradas.TextMatrix(1, 2)) '*** �Tomar el primer documento para asociar los datos de presupuesto? ��ugh!!
        vNumero = Buscar_Numero_Teso(CompraDElementos)
        
        '03.a Graba_cxp toma los valores directamente de los controles, entonces se modifican sus valores
        '    en el caso de impuestos heredados desde la entrada (matrices arrZer...).
        If Not fTransit Then 'Si no se hicieron movimientos transitorios, es posible que existan impuestos aplicados desde la entrada.
            Dim vDescT As Double, vImpT As Double
          
                For J& = 0 To UBound(arrZerCode)
                    If Not IsEmpty(arrZerImpP(J&)) Then
'                        vImpT = vImpT + Val(arrZerImpV(J&))
                        vImpT = vImpT + arrZerImpV(J&)     'REOL M2465
                    Else
'                        vDescT = vDescT + Val(arrZerDescV(J&))
                        vDescT = vDescT + arrZerDescV(J&)     'REOL M2465
                    End If
                Next J&
               
            'HRR R1853
            'lblImp.Tag = vImpT
            'lblDesc.Tag = vDescT
            'lblTotalB.Tag = Val(lblTotal.Tag) + rTImDe(0)
            'lblTotal.Tag = Val(lblTotalB.Tag) - vImpT - vDescT
            'HRR R1853
            'HRR R1853
            lblImp.Tag = lblImp.Tag + vImpT
            lblDesc.Tag = lblDesc.Tag + vDescT
            lblTotalB.Tag = Val(lblTotalB.Tag) + rTImDe(0) 'rTImDe(0) es igual a vImpT+vDesct
            'HRR R1853
            
                
        End If
        '03.a
        
        If BoFacCXP Then GoTo Anular   'NMSR R1924
        
        'Call Graba_cxp(fnFFechaHora(txtFecha), fnFFechaHora(txtFechaV), "CINV", vNumero, vAutoDocu) 'HRR R1853
        'HRR R1853
        'JAUM T28370-R22535 Inicio
        With flexEntradas
           If .Rows - 1 <> 1 Then
              If boIPPTO Then 'JAUM T28835
                 ReDim LnAutoDocuMul(.Rows - 1)
                 For I = 1 To .Rows - 1
                    LnAutoDocuMul(I) = Val(.TextMatrix(I, 2))
                 Next
                 'Call Graba_Cxp_Mul(fnFFechaHora(txtFecha), fnFFechaHora(txtFechaV), ClssImpuDesc.concepto, vNumero, LnAutoDocuMul()) 'JAUM T29758 Se deja linea en comentario
                 Call Graba_Cxp_Mul(txtFecha, fnFFechaHora(txtFechaV), ClssImpuDesc.concepto, vNumero, LnAutoDocuMul()) 'JAUM T29758
              'JAUM T28835 Inicio
              Else
                 'Call Graba_cxp(fnFFechaHora(txtFecha), fnFFechaHora(txtFechaV), ClssImpuDesc.concepto, vNumero, vAutoDocu) 'JAUM T29758 Se deja linea en comentario
                 Call Graba_cxp(txtFecha, fnFFechaHora(txtFechaV), ClssImpuDesc.concepto, vNumero, vAutoDocu) 'JAUM T29758
              End If
              'JAUM T28835 Fin
           Else
        'JAUM T28370-R22535 Fin
              'Call Graba_cxp(fnFFechaHora(txtFecha), fnFFechaHora(txtFechaV), ClssImpuDesc.concepto, vNumero, vAutoDocu) 'JAUM T29758 Se deja linea en comentario
              Call Graba_cxp(txtFecha, fnFFechaHora(txtFechaV), ClssImpuDesc.concepto, vNumero, vAutoDocu) 'JAUM T29758
           End If 'JAUM T28370-R22535
        End With 'JAUM T28370-R22535
        'HRR R1853
        'JAGS R7812
    If BoCruceCxP = True Then
        DbNeto = CDbl(lblTotal)
        For I = 1 To Contador
                      
                      
                    If DbNeto <> 0 And DbNeto < ValAnti(I, 0) And CDbl(lblTotal) = DbNeto Then
                    DbNeto = CDbl(lblTotal)
                    End If
                      
                    
                    
            'If dbNeto > ValAnti(i, 0) And dbNeto <> 0 Then 'HRR R1605
            If DbNeto >= ValAnti(I, 0) And DbNeto <> 0 Then 'JAGS T10540
                
                    ReDim Arr(3) 'NMSR M1813
            '        Result = LoadData("COMPRO_EGRE", "VL_NETO_COEG,ID_PPTO_COEG,NU_REGI_COEG", "CD_CODI_COEG=" & CDbl(GrdAnticipo.TextMatrix(i, 0)), Arr())
                     
                    Result = LoadData("COMPRO_EGRE", "VL_NETO_COEG,ID_PPTO_COEG,NU_REGI_COEG,NU_GIRO_COEG", "CD_CODI_COEG=" & ValAnti(I, 1), Arr()) 'NMSR M1813
                    If (Result <> False) Then
                       If Encontro Then
                          Valores = "CD_COEG_CECP = " & ValAnti(I, 1)
                          Valores = Valores & Coma & "CD_CONC_CECP = " & Comi & Cambiar_Comas_Comillas(ClssImpuDesc.concepto) & Comi
                          Valores = Valores & Coma & "CD_NUME_CECP = " & vNumero
                          'JLPB T42274 INICIO Se deja en comentario las siguientes lineas
                          'Valores = Valores & Coma & "VL_SALD_CECP = " & CDbl(lblTotal)
                          'Valores = Valores & Coma & "VL_CANC_CECP = " & CDbl(arr(0))
                          Valores = Valores & Coma & "VL_SALD_CECP = " & CDbl(ValAnti(I, 0))
                          Valores = Valores & Coma & "VL_CANC_CECP = " & CDbl(ValAnti(I, 0))
                          'JLPB T42274 FIN
                          Valores = Valores & Coma & "VL_DEIM_CECP = " & "0"
                          'Valores = Valores & Coma & "VL_GIRA_CECP = " & CDbl(arr(0)) 'JLPB T42274 Se deja en comentario
                          Valores = Valores & Coma & "VL_GIRA_CECP = " & CDbl(ValAnti(I, 0)) 'JLPB T42274
                          'NMSR M1813
                          Valores = Valores & Coma & "NU_GIRO_CECP=" & CDbl(Arr(3))
                          Valores = Valores & Coma & "ID_PPTO_CECP=" & CDbl(Arr(1))
                          'NMSR M1813
                          'Result = DoInsertSQL("R_COEG_CXP (" & Campos & ")", Valores)
                          Result = DoInsertSQL("R_COEG_CXP", Valores)
                          If Result <> FAIL Then
                            'Valores = "VL_ANTI_COEG=VL_ANTI_COEG +" & CDbl(TxtNeto)
                            Valores = "VL_ANTI_COEG=" & CDbl(Arr(0)) 'JAGS R7812
                            Result = DoUpdate("COMPRO_EGRE", Valores, "CD_CODI_COEG=" & ValAnti(I, 1))
                          End If
                            Valores = "ID_ANTI_COEG=" & Comi & "N" & Comi
                            Condicion = "CD_CODI_COEG=" & ValAnti(I, 1) & " AND VL_NETO_COEG <= VL_ANTI_COEG"
                            Result = DoUpdate("COMPRO_EGRE", Valores, Condicion)
                        
                      End If
                   End If
                
            End If
            ReDim Arr(2)
            If DbNeto < ValAnti(I, 0) And DbNeto <> 0 Then
                
                    Result = LoadData("COMPRO_EGRE", "VL_NETO_COEG,ID_PPTO_COEG,NU_GIRO_COEG", "CD_CODI_COEG=" & ValAnti(I, 1), Arr())
                    If (Result <> False) Then
                       If Encontro Then
                          Valores = "CD_COEG_CECP = " & ValAnti(I, 1)
                          Valores = Valores & Coma & "CD_CONC_CECP = " & Comi & Cambiar_Comas_Comillas(ClssImpuDesc.concepto) & Comi
                          Valores = Valores & Coma & "CD_NUME_CECP = " & vNumero
                          'Valores = Valores & Coma & "VL_SALD_CECP = " & CDbl(arr(0)) 'JLPB T42274 Se deja en comentario
                          Valores = Valores & Coma & "VL_SALD_CECP = " & CDbl(ValAnti(I, 0)) 'JLPB T42274
                          Valores = Valores & Coma & "VL_CANC_CECP = " & DbNeto
                          Valores = Valores & Coma & "VL_DEIM_CECP = " & "0"
                          'Valores = Valores & Coma & "VL_GIRA_CECP = " & CDbl(arr(0)) 'JLPB T42274 Se deja en comentario
                          Valores = Valores & Coma & "VL_GIRA_CECP = " & DbNeto 'JLPB T42274
                          'NMSR M1813
                          Valores = Valores & Coma & "NU_GIRO_CECP=" & CDbl(Arr(2))
                          Valores = Valores & Coma & "ID_PPTO_CECP=" & CDbl(Arr(1))
                          'NMSR M1813
                          'Result = DoInsertSQL("R_COEG_CXP (" & Campos & ")", Valores)
                          Result = DoInsertSQL("R_COEG_CXP", Valores)
                          If Result <> FAIL Then
                             
                                Valores = "VL_ANTI_COEG=VL_ANTI_COEG +" & DbNeto
                             
                             Result = DoUpdate("COMPRO_EGRE", Valores, "CD_CODI_COEG=" & ValAnti(I, 1))
                           End If
                           Valores = "ID_ANTI_COEG=" & Comi & "N" & Comi
                           Condicion = "CD_CODI_COEG=" & ValAnti(I, 1) & " AND VL_NETO_COEG <= VL_ANTI_COEG" 'JAGS R8530
                           If Result <> FAIL Then Result = DoUpdate("COMPRO_EGRE", Valores, Condicion)

                         End If
                     End If
                    
            End If
            If DbNeto > ValAnti(I, 0) Then
                DbNeto = DbNeto - ValAnti(I, 0)
            Else
                DbNeto = 0
            End If
               
        Next I
    End If
            'JAGS R7812
        With flexEntradas
                Desde = "IN_ENCABEZADO"
                Valores = "TX_CXPE_ENCA='S' " & Coma & " NU_CXP_ENCA=" & Comi & vNumero & Comi
            For I& = 1 To .Rows - 1
                If .TextMatrix(I&, 0) <> "" Then
                    Condi = "NU_AUTO_ENCA=" & .TextMatrix(I&, 2)
                    Result = DoUpdate(Desde, Valores, Condi)
                End If
            Next I&
        End With
        
'HRR R1853
'             dbVlImpu = 0: dbVlDesc = 0
'            '03.b |.DR.|, R:1033-1651
'            If fTransit Then 'Guardar impuestos aplicados en la compra:
'                For j& = 0 To UBound(arrSerCode)
'                    If Not IsEmpty(arrSerImpP(j&)) Then
'                        stValores = "CD_CONC_CPIM='CINV'"
'                        stValores = stValores & Coma & "CD_NUME_CPIM=" & vNumero
'                        stValores = stValores & Coma & "CD_IMPU_CPIM='" & arrSerCode(j&) & "'"
'                        stValores = stValores & Coma & "VL_IMPU_CPIM=" & Val(arrSerImpV(j&))
'                        stValores = stValores & Coma & "PR_IMPU_CPIM=" & Val(arrSerImpP(j&))
'                        dbVlImpu = dbVlImpu + Val(arrSerImpV(j&))       'PJCA M1944
'
'                    Else
'                        stValores = "CD_CONC_CPDE='CINV'"
'                        stValores = stValores & Coma & "CD_NUME_CPDE=" & vNumero
'                        stValores = stValores & Coma & "CD_DESC_CPDE='" & arrSerCode(j&) & "'"
'                        stValores = stValores & Coma & "VL_DESC_CPDE=" & Val(arrSerVFinal(j&)) '|.DR.|, Se ha reemplazado arrSerDescV por arrSerVFinal, el primero ten�a el valor de parametrizaci�n del descuento, el segundo contiene el valor que gener� realmente (por ejemplo en el caso de los porcentajes).
'                        stValores = stValores & Coma & "PR_DESC_CPDE=" & Val(arrSerDescP(j&))
'                        dbVlDesc = dbVlDesc + Val(arrSerVFinal(j&))     'PJCA M1944
'                    End If
'
'                    If Not IsEmpty(arrSerCode(j&)) Then
'                        If Not IsEmpty(arrSerImpP(j&)) Then
'                            Result = DoInsertSQL("R_CXP_IMPU", stValores)
'                            If Result = FAIL Then GoTo Anular
'                        Else
'                            Result = DoInsertSQL("R_CXP_DESC", stValores)
'                            If Result = FAIL Then GoTo Anular
'                        End If
'                    End If
'                Next j&
'            Else 'Guardar impuestos heredados desde la entrada de mercanc�a:
'                ReDim arrT(0, 0)
'                fnAgruparImp 'Agrupa los impuestos y descuentos para evitar que se repitan los c�digos.
'                For j& = 0 To UBound(arrZerCode)
'                    If Not IsEmpty(arrZerImpP(j&)) Then
'                        stValores = "CD_CONC_CPIM='CINV'"
'                        stValores = stValores & Coma & "CD_NUME_CPIM=" & vNumero
'                        stValores = stValores & Coma & "CD_IMPU_CPIM='" & arrZerCode(j&) & "'"
'                        stValores = stValores & Coma & "VL_IMPU_CPIM=" & Val(arrZerImpV(j&))
'                        stValores = stValores & Coma & "PR_IMPU_CPIM=" & Val(arrZerImpP(j&))
'                        dbVlImpu = dbVlImpu + Val(arrZerImpV(j&))       'PJCA M1944
'                    Else
'                        '!! *** �Si no hay c�digo asignarle el primero que aparezca? |.DR.|, R:1033-1651
'                        If Trim(arrZerCode(j&)) = "" Then
'                            cCond = fnStValores(arrT, 0)
'                            If Not IsEmpty(arrT(0, 0)) Then ReDim arrT(0, UBound(arrT, 2) + 1)
'                            If cCond <> "()" Then cCond = "CD_CODI_DESC Not In " & cCond Else cCond = ""
'                            arrZerCode(j&) = fnDevDato("DESCUENTO", "CD_CODI_DESC", cCond, True)
'                            arrT(0, UBound(arrT, 2)) = "'" & arrZerCode(j&) & "'"
'                        End If
'                        '!! *** ������!!!!!! ***
'                        stValores = "CD_CONC_CPDE='CINV'"
'                        stValores = stValores & Coma & "CD_NUME_CPDE=" & vNumero
'                        stValores = stValores & Coma & "CD_DESC_CPDE='" & arrZerCode(j&) & "'"
'                        stValores = stValores & Coma & "VL_DESC_CPDE=" & Val(arrZerDescV(j&))
'                        stValores = stValores & Coma & "PR_DESC_CPDE=" & Val(arrZerDescP(j&))
'                        dbVlDesc = dbVlDesc + Val(arrZerDescV(j&))      'PJCA M1944
'                    End If
'
'                    If Not IsEmpty(arrZerCode(j&)) Then
'                        If Not IsEmpty(arrZerImpP(j&)) Then
'                            Result = DoInsertSQL("R_CXP_IMPU", stValores)
'                            If Result = FAIL Then GoTo Anular
'                        Else
'                            Result = DoInsertSQL("R_CXP_DESC", stValores)
'                            If Result = FAIL Then GoTo Anular
'                        End If
'                    End If
'                Next j&
'
'
'            End If
'            '03.b
'HRR R1853

         'HRR R1853
         If (Not BoEntImpDesc) Then 'SI la entrada no tenia impuestos - descuentos y la interfaz con cxp estaba activa
            'JAUM T28370-R22535 Inicio
            With flexEntradas
               If .Rows - 1 <> 1 Then
                  If boIPPTO Then 'JAUM T28835
                     'JAUM T28836 Inicio
                     ReDim VrCxp(1, 0)
                     Result = LoadMulData("R_CXP_CXP", "CD_NUME_RCPCP, VL_BRUT_RCPCP", "CD_NUME_CXP_RCPCP = " & vNumero, VrCxp)
                     If Result <> FAIL And Encontro Then
                        For I& = 0 To UBound(VrCxp, 2)
                           dbDesc = 0
                     'JAUM T28836 Fin
                           'JLPB T41657 INICIO Se deja en comentario las siguientes lineas
                           'For InI = 0 To ClssImpuDesc.SizeDesc
                           '   If ClssImpuDesc.ElemDesc(InI, 0) <> NUL$ Then
                           '      'JAUM T28836 Inicio
                           '      If ClssImpuDesc.ElemDesc(InI, 2) = 0 Then
                           '         DbDescImp = Aplicacion.Formatear_Valor(Val(CDbl(ClssImpuDesc.ElemDesc(InI, 4))))
                           '      Else
                           '         DbDescImp = Aplicacion.Formatear_Valor(Val((VrCxp(1, I&) * (ClssImpuDesc.ElemDesc(InI, 2) / 100))))
                           '      End If
                           '      dbDesc = dbDesc + DbDescImp
                           '      'JAUM T28836 Fin
                           '      Valores = "CD_CONC_RCPDE = " & Comi & ClssImpuDesc.concepto & Comi
                           '      'Valores = Valores & Coma & "CD_NUME_RCPDE=" & vNumero 'JAUM T28836 Se deja linea en comentario
                           '      Valores = Valores & Coma & "CD_NUME_RCPDE=" & VrCxp(0, I&) 'JAUM T28836
                           '      Valores = Valores & Coma & "CD_DESC_RCPDE=" & Comi & ClssImpuDesc.ElemDesc(InI, 0) & Comi
                           '      'Valores = Valores & Coma & "VL_DESC_RCPDE=" & CDbl(ClssImpuDesc.ElemDesc(InI, 4)) JAUM T28836 Se deja linea en comentario
                           '      Valores = Valores & Coma & "VL_DESC_RCPDE=" & DbDescImp 'JAUM T28836
                           '      Valores = Valores & Coma & "PR_DESC_RCPDE=" & ClssImpuDesc.ElemDesc(InI, 2)
                           '      Result = DoInsertSQL("R_CPCP_DESC", Valores)
                           '      If Result = FAIL Then GoTo Anular
                           '   End If
                           'Next
                           'For InI = 0 To ClssImpuDesc.SizeImp
                           '   If ClssImpuDesc.ElemImp(InI, 0) <> NUL$ Then
                           '      'JAUM T28836 Inicio
                           '      If ClssImpuDesc.ElemImp(InI, 6) = "C" Or ClssImpuDesc.ElemImp(InI, 6) = "R" Then
                           '         DbDescImp = Aplicacion.Formatear_Valor(Val(CDbl(VrCxp(1, I&) - dbDesc) * ClssImpuDesc.ElemImp(InI, 2) / 100))
                           '      ElseIf ClssImpuDesc.ElemImp(InI, 6) = "M" Then
                           '         DbDescImp = Aplicacion.Formatear_Valor(Val((VrCxp(1, I&)) * ClssImpuDesc.ElemImp(InI, 2) / 100))
                           '      End If
                           '      'JAUM T28836 Fin
                           '      Valores = "CD_CONC_RCPIM=" & Comi & ClssImpuDesc.concepto & Comi
                           '      'Valores = Valores & Coma & "CD_NUME_RCPIM=" & vNumero JAUM T28836 Se deja linea en comentario
                           '      Valores = Valores & Coma & "CD_NUME_RCPIM=" & VrCxp(0, I&) 'JAUM T28836
                           '      Valores = Valores & Coma & "CD_IMPU_RCPIM=" & Comi & ClssImpuDesc.ElemImp(InI, 0) & Comi
                           '      'Valores = Valores & Coma & "VL_IMPU_RCPIM=" & CDbl(ClssImpuDesc.ElemImp(InI, 4)) JAUM T28836 Se deja linea en comentario
                           '      Valores = Valores & Coma & "VL_IMPU_RCPIM=" & DbDescImp 'JAUM T28836
                           '      Valores = Valores & Coma & "PR_IMPU_RCPIM=" & ClssImpuDesc.ElemImp(InI, 2)
                           '      Result = DoInsertSQL("R_CPCP_IMPU", Valores)
                           '      If Result = FAIL Then GoTo Anular
                           '   End If
                           'Next
                           For InI = 0 To UBound(VrDesImp, 2)
                              If VrDesImp(0, InI) <> NUL$ Then
                                 If VrDesImp(7, InI) = "D" & I& + 1 Then
                                    If VrDesImp(5, InI) <> 0 Then
                                       DbDescImp = VrDesImp(5, InI)
                                       dbDesc = dbDesc + DbDescImp
                                       Valores = "CD_CONC_RCPDE = " & Comi & ClssImpuDesc.concepto & Comi
                                       Valores = Valores & Coma & "CD_NUME_RCPDE=" & VrCxp(0, I&)
                                       Valores = Valores & Coma & "CD_DESC_RCPDE=" & Comi & VrDesImp(0, InI) & Comi
                                       Valores = Valores & Coma & "VL_DESC_RCPDE=" & DbDescImp
                                       Valores = Valores & Coma & "PR_DESC_RCPDE=" & VrDesImp(2, InI)
                                       Result = DoInsertSQL("R_CPCP_DESC", Valores)
                                       If Result = FAIL Then GoTo Anular
                                    End If
                                 ElseIf VrDesImp(7, InI) = "I" & I& + 1 Then
                                    If VrDesImp(5, InI) <> 0 Then
                                       DbDescImp = VrDesImp(5, InI)
                                       Valores = "CD_CONC_RCPIM=" & Comi & ClssImpuDesc.concepto & Comi
                                       Valores = Valores & Coma & "CD_NUME_RCPIM=" & VrCxp(0, I&)
                                       Valores = Valores & Coma & "CD_IMPU_RCPIM=" & Comi & VrDesImp(0, InI) & Comi
                                       Valores = Valores & Coma & "VL_IMPU_RCPIM=" & DbDescImp
                                       Valores = Valores & Coma & "PR_IMPU_RCPIM=" & VrDesImp(2, InI)
                                       Result = DoInsertSQL("R_CPCP_IMPU", Valores)
                                       If Result = FAIL Then GoTo Anular
                                    End If
                                 End If
                              End If
                           Next
                           'JLPB T41657 FIN
                        Next 'JAUM T28836
                     End If 'JAUM R28836
                  'JAUM T28835 Inicio
                  Else
                     GoTo Siguiente
                  End If
                  'JAUM T28835 Fin
               Else
Siguiente: 'JAUM T28835
            'JAUM T28370-R22535 Fin
                  For InI = 0 To ClssImpuDesc.SizeDesc
                     If ClssImpuDesc.ElemDesc(InI, 0) <> NUL$ Then
                        Valores = "CD_CONC_CPDE=" & Comi & ClssImpuDesc.concepto & Comi
                        Valores = Valores & Coma & "CD_NUME_CPDE=" & vNumero
                        Valores = Valores & Coma & "CD_DESC_CPDE=" & Comi & ClssImpuDesc.ElemDesc(InI, 0) & Comi
                        'Valores = Valores & Coma & "VL_DESC_CPDE=" & ClssImpuDesc.ElemDesc(Ini, 4) JAUM T28370-R22535 Se deja linea en comentario
                        Valores = Valores & Coma & "VL_DESC_CPDE=" & CDbl(ClssImpuDesc.ElemDesc(InI, 4)) 'JAUM T28370-R22535
                        Valores = Valores & Coma & "PR_DESC_CPDE=" & ClssImpuDesc.ElemDesc(InI, 2)
                        
                        Result = DoInsertSQL("R_CXP_DESC", Valores)
                        If Result = FAIL Then GoTo Anular
                     End If
                  Next
         
                  For InI = 0 To ClssImpuDesc.SizeImp
                     If ClssImpuDesc.ElemImp(InI, 0) <> NUL$ Then
                        Valores = "CD_CONC_CPIM=" & Comi & ClssImpuDesc.concepto & Comi
                        Valores = Valores & Coma & "CD_NUME_CPIM=" & vNumero
                        Valores = Valores & Coma & "CD_IMPU_CPIM=" & Comi & ClssImpuDesc.ElemImp(InI, 0) & Comi
                        'Valores = Valores & Coma & "VL_IMPU_CPIM=" & ClssImpuDesc.ElemImp(Ini, 4) JAUM T28370-R22535 Se deja linea en comentario
                        Valores = Valores & Coma & "VL_IMPU_CPIM=" & CDbl(ClssImpuDesc.ElemImp(InI, 4)) 'JAUM T28370-R22535
                        Valores = Valores & Coma & "PR_IMPU_CPIM=" & ClssImpuDesc.ElemImp(InI, 2)
                        
                        Result = DoInsertSQL("R_CXP_IMPU", Valores)
                        If Result = FAIL Then GoTo Anular
                     End If
                  Next
               End If 'JAUM T28370-R22535
            End With 'JAUM T28370-R22535
         Else 'Guardar impuestos heredados desde la entrada de mercanc�a:
            ReDim arrT(0, 0)
            fnAgruparImp 'Agrupa los impuestos y descuentos para evitar que se repitan los c�digos.
            'JAUM T28370-R22535 Inicio
            With flexEntradas
               If .Rows - 1 <> 1 Then
                  If boIPPTO Then 'JAUM T28835
                     For J& = 0 To UBound(arrZerCode)
                        If Not IsEmpty(arrZerImpP(J&)) Then
                           StValores = "CD_CONC_RCPIM=" & Comi & ClssImpuDesc.concepto & Comi
                           StValores = StValores & Coma & "CD_NUME_RCPIM=" & vNumero
                           StValores = StValores & Coma & "CD_IMPU_RCPIM='" & arrZerCode(J&) & "'"
                           StValores = StValores & Coma & "VL_IMPU_RCPIM=" & Val(arrZerImpV(J&))
                           StValores = StValores & Coma & "PR_IMPU_RCPIM=" & Val(arrZerImpP(J&))
                        Else
                           StValores = "CD_CONC_RCPDE=" & Comi & ClssImpuDesc.concepto & Comi
                           StValores = StValores & Coma & "CD_NUME_RCPDE=" & vNumero
                           StValores = StValores & Coma & "CD_DESC_RCPDE='" & arrZerCode(J&) & "'"
                           StValores = StValores & Coma & "VL_DESC_RCPDE=" & Val(arrZerDescV(J&))
                           StValores = StValores & Coma & "PR_DESC_RCPDE=" & Val(arrZerDescP(J&))
                        End If
                        If Not IsEmpty(arrZerCode(J&)) Then
                           If Not IsEmpty(arrZerImpP(J&)) Then
                              Result = DoInsertSQL("R_CPCP_IMPU", StValores)
                              If Result = FAIL Then GoTo Anular
                           Else
                              Result = DoInsertSQL("R_CPCP_DESC", StValores)
                              If Result = FAIL Then GoTo Anular
                           End If
                        End If
                     Next J&
                  'JAUM T28835 Inicio
                  Else
                     GoTo Siguiente2
                  End If
                  'JAUM T28835 Fin
               Else
Siguiente2: 'JAUM T28835
            'JAUM T28370-R22535 Fin
                  For J& = 0 To UBound(arrZerCode)
                     If Not IsEmpty(arrZerImpP(J&)) Then
                        StValores = "CD_CONC_CPIM=" & Comi & ClssImpuDesc.concepto & Comi
                        StValores = StValores & Coma & "CD_NUME_CPIM=" & vNumero
                        StValores = StValores & Coma & "CD_IMPU_CPIM='" & arrZerCode(J&) & "'"
                        StValores = StValores & Coma & "VL_IMPU_CPIM=" & Val(arrZerImpV(J&))
                        StValores = StValores & Coma & "PR_IMPU_CPIM=" & Val(arrZerImpP(J&))
                  
                     Else
                        '!! *** �Si no hay c�digo asignarle el primero que aparezca? |.DR.|, R:1033-1651
                        If Trim(arrZerCode(J&)) = "" Then
                           cCond = fnStValores(arrT, 0)
                           If Not IsEmpty(arrT(0, 0)) Then ReDim arrT(0, UBound(arrT, 2) + 1)
                           If cCond <> "()" Then cCond = "CD_CODI_DESC Not In " & cCond Else cCond = ""
                           arrZerCode(J&) = fnDevDato("DESCUENTO", "CD_CODI_DESC", cCond, True)
                           arrT(0, UBound(arrT, 2)) = "'" & arrZerCode(J&) & "'"
                        End If
                        '!! *** ������!!!!!! ***
                        StValores = "CD_CONC_CPDE=" & Comi & ClssImpuDesc.concepto & Comi
                        StValores = StValores & Coma & "CD_NUME_CPDE=" & vNumero
                        StValores = StValores & Coma & "CD_DESC_CPDE='" & arrZerCode(J&) & "'"
                        StValores = StValores & Coma & "VL_DESC_CPDE=" & Val(arrZerDescV(J&))
                        StValores = StValores & Coma & "PR_DESC_CPDE=" & Val(arrZerDescP(J&))
                  
                     End If

                     If Not IsEmpty(arrZerCode(J&)) Then
                        If Not IsEmpty(arrZerImpP(J&)) Then
                           Result = DoInsertSQL("R_CXP_IMPU", StValores)
                           If Result = FAIL Then GoTo Anular
                        Else
                           Result = DoInsertSQL("R_CXP_DESC", StValores)
                           If Result = FAIL Then GoTo Anular
                        End If
                     End If
                  Next J&
               End If 'JAUM T28370-R22535
            End With 'JAUM T28370-R22535
         End If
         
         
         'HRR R1853


         'HRR M2001
           ReDim Arr(0)
           If DbVlrIvaEnt <> 0 Then
               Result = LoadData("TC_IMPUESTOS", "TOP 1 CD_CODI_IMPU", "ID_TIPO_IMPU='I' AND PR_PORC_IMPU=16", Arr)
               If Result <> FAIL And Encontro Then
                  'JAUM T28370-R22535 Inicio
                  With flexEntradas
                     If .Rows - 1 <> 1 Then
                        If boIPPTO Then 'JAUM T28835
                           StValores = "CD_CONC_RCPIM=" & Comi & ClssImpuDesc.concepto & Comi
                           StValores = StValores & Coma & "CD_NUME_RCPIM=" & vNumero
                           StValores = StValores & Coma & "CD_IMPU_RCPIM='" & Arr(0) & Comi
                           StValores = StValores & Coma & "VL_IMPU_RCPIM=" & DbVlrIvaEnt
                           StValores = StValores & Coma & "PR_IMPU_RCPIM= 16"
                           Result = DoInsertSQL("R_CPCP_IMPU", StValores)
                           If Result = FAIL Then GoTo Anular
                        'JAUM T28835 Inicio
                        Else
                           GoTo Siguiente3
                        End If
                        'JAUM T28835 Fin
                     Else
Siguiente3: 'JAUM T28835
                  'JAUM T28370-R22535 Fin
                        StValores = "CD_CONC_CPIM=" & Comi & ClssImpuDesc.concepto & Comi
                        StValores = StValores & Coma & "CD_NUME_CPIM=" & vNumero
                        StValores = StValores & Coma & "CD_IMPU_CPIM='" & Arr(0) & Comi
                        StValores = StValores & Coma & "VL_IMPU_CPIM=" & DbVlrIvaEnt
                        StValores = StValores & Coma & "PR_IMPU_CPIM=16"
                        Result = DoInsertSQL("R_CXP_IMPU", StValores)
                        If Result = FAIL Then GoTo Anular
                     End If 'JAUM T28370-R22535
                  End With 'JAUM T28370-R22535
               End If
            End If
         'HRR M2001

        'HRR H1853
        'Result = DoUpdate("C_X_P", "VL_DESC_CXP=" & dbVlDesc, "CD_NUME_CXP=" & vNumero)     'PJCA M1944
        'Result = DoUpdate("C_X_P", "VL_IMPU_CXP=" & dbVlImpu, "CD_NUME_CXP=" & vNumero)     'PJCA M1944
        'HRR H1853
         
        'Guardando en la tabla In_Compra, los datos del movimiento contable:
        If Result <> FAIL Then
            stExMsg = "%2Error al actualizar un registro en la tabla IN_COMPRA (interfase C_X_P)."
            StValores = "NU_CXP_COMP=" & vNumero
            If DoUpdate("IN_COMPRA", StValores, "NU_AUTO_COMP=" & lgConsec) = FAIL Then GoTo Anular
        End If

        If Result <> FAIL And boRealS Then Call MsgBox("Se gener� la Cuenta X Pagar # " & Right(String(7, "0") & vNumero, 7), vbInformation)
    End If
'03 Fin.
' ============== CUENTAS POR PAGAR ==============
Else
    stExMsg = "%2Error al crear la transacci�n"
    GoTo Anular
End If
'If Result = FAIL Then GoTo Anular 'AASV M4826 Nota:22224 'AASV M4826 Nota:22288 Se coloca en comentario
Pr�ximo:
    H.Hide
    If Not boRealS Then
        RollBackTran
        
        'LJSA M4730 Nota 21492 -------  Deja la semilla del autonum�rico de IN_COMPRA en el valor en el que se encontraba ----
        If MotorBD = "SQL" And BoChkAutonum = True Then
            Result = ExecSQLCommand("DBCC CHECKIDENT (IN_COMPRA,RESEED," & lgConsec - 1 & ")")
        End If
        'LJSA M4730 Nota 21492 -------  Deja la semilla del autonum�rico de IN_COMPRA en el valor en el que se encontraba ----
            
        H.sbW "Creando documento de Excel...", 4
        sbResumenMov
        H.Hide
        Exit Sub
    End If
    
       If (CommitTran() <> FAIL) Then
          boChBl = False
          H.H
          Mensaje1 "Se gener� la compra No. " & lgConsec, 3
          
          ClssImpuDesc.TieneDatos = False 'HRR  M2644
          Call cmdClean_Click
          'h.sbMsg "La compra se guard� correctamente (# " & lgConsec & ").", txtTercero, 1
          btModo = icNinguno
          
          'AASV R1883
           If Aplicacion.PinvImpAntSCon Then
              'StRespuesta = MsgBox("Desea imprimir el documento?", vbYesNo + vbInformation)
              InRespuesta = MsgBox("Desea imprimir el documento?", vbYesNo + vbInformation) 'AASV M3520
              'If StRespuesta = vbYes Then
              If InRespuesta = vbYes Then 'AASV M3520
                'Documento.Encabezado.EsNuevoDocumento = False
                 vConsec = lgConsec 'DAHV  M3629
                 Call CmdImprimir_Click
              End If
           End If
          'AASV R1883
       Else
          GoTo Anular
       End If

    Exit Sub
    
Anular:
    Call RollBackTran
    'LJSA M4730 Nota 21492 -------  Deja la semilla del autonum�rico de IN_COMPRA en el valor en el que se encontraba ----
    If MotorBD = "SQL" And BoChkAutonum = True Then

        Result = ExecSQLCommand("DBCC CHECKIDENT (IN_COMPRA,RESEED," & lgConsec - 1 & ")")
    End If
    'LJSA M4730 Nota 21492 -------  Deja la semilla del autonum�rico de IN_COMPRA en el valor en el que se encontraba ----
    H.Hide
'    If boRealS Then h.sbMsg "La compra no se guard� correctamente." & stExMsg, cmdGuardar, 2, 2
    If Not boRealS Then H.sbMsg "Las validaciones de la compra han fallado." & stExMsg, cmdXLS, 2, 2

End Sub

Sub sbLimpForm(boQ As Boolean)

flexEntradas.Rows = 1: flexEntradas.Rows = 2
    txtTercero.Enabled = boQ
    cmdSTerc.Enabled = boQ
    cmbAuto.Enabled = True
    txtDPlazo.Enabled = False
    cmdStart.Enabled = False: cmdClean.Enabled = False
    txtFecha.Enabled = False: txtFechaV.Enabled = False: txtN�mFact.Enabled = False
    MskFechFactura.Enabled = False 'HRR R1873
    lblEtiq(0).Enabled = False: lblEtiq(1).Enabled = False: lblEtiq(2).Enabled = False: lblEtiq(3).Enabled = False:   lblEtiq(14).Enabled = False:    imgContab.Visible = False: imgCxP.Visible = False: lblInterfases.Visible = False
    cmdImpuestos.Enabled = False: cmdClearImp.Enabled = False: cmdXLS.Enabled = False: cmdPPTO.Enabled = False: lblN.Visible = False: lblD.Visible = False: lblD.Visible = False
    cmdGuardar.Enabled = False: cmdAnular.Enabled = False: cmdImprimir.Enabled = False
    fraValores.Enabled = False
    txtObser.Enabled = False
    cmdClearImpComp.Enabled = False
    cmdImpInfo.Enabled = False
    fraFechas.Visible = True
    flexArt�culos.Rows = 1
    lblDesc = "": lblImp = "": lblImpAT = "":  lblNTercero = "": lblConsComp = ""
    lblTotal = "": lblTotalB = "": lblTotalN = "":  txtObser = "": txtFlete = ""
    
    txtTercero = "": lblNTercero = ""
    lblDescAT = ""
    txtFecha = Format(dServ, "dd/mm/yyyy")
    txtFechaV = txtFecha
    MskFechFactura = txtFechaV 'HRR R1873
    Me.txtDPlazo = 0 'AASV R2222
    
    'HRR R1853
    lblTIIca = NUL$
    lblTIRIVA = NUL$
    lblTIOtros = NUL$
    lblTIRet = NUL$
    'HRR R1853
    sbListas
    boConsul = False 'LDCR 6401

End Sub

Sub sbReDraw()
'Actualiza las im�genes que deben estar visibles en la grilla

    Dim I&
    
    With flexEntradas
        For I& = 1 To flexEntradas.Rows - 1
            If .TextMatrix(I&, 0) = "" Then
                .Row = I&: .Col = 0
                Set .CellPicture = Nothing
            Else
                .Row = I&: .Col = 0
                Set .CellPicture = imgLib(0).Picture
                .CellPictureAlignment = 3
            End If
    

        Next I&
    End With

End Sub

Sub sbStartEdit(flexDat As MSFlexGrid, lgRow As Long, lgCol As Long, lgColor As Long)

    Set flexData = flexDat
    With flexData
        .Col = lgCol
        .Row = lgRow
        .CellForeColor = lgColor
        txtFEdit.Top = .RowPos(lgRow) + .Top: txtFEdit.Left = .ColPos(lgCol) + .Left
        txtFEdit.Width = .ColWidth(lgCol): txtFEdit.Height = .RowHeight(lgRow)
        txtFEdit.Text = .Text: txtFEdit.SelStart = 0
        txtFEdit.SelLength = 32000: txtFEdit.Visible = True
        txtFEdit.ZOrder 0: txtFEdit.SetFocus
    End With

End Sub

Sub sbStartEnabled()

    txtTercero.Enabled = False
    cmbAuto.Enabled = False
    cmdSTerc.Enabled = False
    cmdStart.Enabled = False
    cmdImpuestos.Enabled = fTransit And boICxP 'DARS, M:1748
    cmdClearImp.Enabled = fTransit And boICxP 'DARS, M:1748
    cmdClean.Enabled = True
    boChBl = True
    txtObser.Enabled = True
    txtFecha.Enabled = True
    txtFechaV.Enabled = True
    txtN�mFact.Enabled = True
    lblEtiq(0).Enabled = True
    lblEtiq(1).Enabled = True
    lblEtiq(2).Enabled = True
    lblEtiq(3).Enabled = True
    lblEtiq(14).Enabled = True
    fraValores.Enabled = True
    txtDPlazo.Enabled = True
    txtObser = ""
    cmdBusca.Enabled = False    'REOL M2075
    MskFechFactura.Enabled = True 'HRR R1873
    
End Sub

Sub sbTransEnt()

Dim I&, fType As Boolean
        
        With flexEntradas
            For I& = 1 To .Rows - 1
                    .Row = I&: .Col = 0
                    If .Text = "  " Then .Text = "": Set .CellPicture = Nothing
            Next I&
        
        
            For I& = 1 To .Rows - 1
                If .TextMatrix(I&, 0) = " " And Trim(.TextMatrix(I&, 5)) = "" Then
                    fType = False: GoTo Cambiar
                ElseIf .TextMatrix(I&, 0) = " " Then
                    fType = True: GoTo Cambiar
                End If
            Next I&
            
            Exit Sub
                    
Cambiar:
            fTransit = Not fType 'Se usa durante los procesos para saber si se realizan movimientos contables "contra" transitorios.
            For I& = 1 To .Rows - 1
                If Not fType Xor Trim(.TextMatrix(I&, 5)) = "" Then
                        .Row = I&: .Col = 0
                        If .Text <> "  " Then
                        Set .CellPicture = imgLib(2).Picture
                        .CellPictureAlignment = 3
                        .Text = "  "
                    End If
                End If
            Next I&
        End With
End Sub

'HRR R1853
'Sub sbVacImp()
''lgFila debe traer el valor de la fila menos uno (.Row - 1).
'Dim J&
'
'For J& = 0 To UBound(arrSerCode)
'    arrSerTope(J&) = Empty
'    arrSerCode(J&) = Empty
'    arrSerDescP(J&) = Empty
'    arrSerDescV(J&) = Empty
'    arrSerImpP(J&) = Empty
'    arrSerIDN�m(J&) = Empty
'    arrSerCuen(J&) = Empty
'    arrSerTipo(J&) = Empty
'Next J&
'
'End Sub
'HRR R1853

Sub sbZonaEdicion(boEstado As Boolean)
    flexEntradas.Enabled = boEstado
    cmdImpuestos.Enabled = fTransit And boEstado And boICxP 'DARS, M:1748
    cmdClearImp.Enabled = fTransit And boEstado And boICxP 'DARS, M:1748
    txtFecha.Enabled = boEstado
    MskFechFactura.Enabled = boEstado 'HRR R1873
End Sub

Private Sub chkQuery_Click()
        
    Static fContra As Boolean
    
    If fContra Then Exit Sub
    fContra = True
    
    If chkQuery.value Then
        If fnChanges Then If Not WarnMsg("�Desea perder los cambios?") Then chkQuery.value = 0: fContra = False: Exit Sub
        'HRR M2644
        ClssImpuDesc.TieneDatos = False
        'HRR M2644
        sbLimpForm False
        fConsComp = True
        cmdClean.Visible = False '|.DR.|, M:1393
        cmdXClean.Visible = True '|.DR.|, M:1393
        fraFechas.Visible = False
    Else
        fConsComp = False
        sbLimpForm True
        txtTercero.SetFocus
        cmdClean.Visible = True '|.DR.|, M:1393
        cmdXClean.Visible = False '|.DR.|, M:1393
        fraFechas.Visible = True
    End If
        
        flexArt�culos.Rows = 1
    
    fContra = False
        
End Sub

Private Sub cmbAuto_Click()
   Dim I&

   I& = Val(cmbAuto)
   sbLimpForm False
   If Not fnCargar(I&) Then
      lblConsComp = ""
      H.sbMsg "No se encontr� el consecutivo " & I& & ".", cmbAuto, 8
      btModo = icNinguno
        
   Else
      H.H 'smdl m1679, el control utiliza .h para ocultar.
      fraFechas.Visible = False
      lblConsComp = "Compra No. " & I&
      sbReDraw
      'sbCalcImpSer
      sbApForm
      cmdAnular.Enabled = (vEstado = 1)
      cmdImprimir.Enabled = True
      flexEntradas.Enabled = True
      lblNTercero.Caption = fnNTerc(txtTercero) & " "
      btModo = icConsultando
      cmbAuto.Text = I&
      boConsul = True 'LDCR T6401
      txtN�mFact.Enabled = True
   End If

   'Indicadores visuales de interfases (estas im�genes adem�s contienen instrucciones en el evento 'MouseMove'):
   imgContab.Visible = Not IsEmpty(vContabCons)
   imgCxP.Visible = Not IsEmpty(vcxp)
   lblInterfases.Visible = imgContab.Visible Or imgCxP.Visible: lblInterfases = " <Mueva el puntero sobre los iconos> "

   '�Estado anulado?
   lblD.Visible = (vEstado <> 1)
    
End Sub

Private Sub cmbAuto_GotFocus()
    If fPausa Then fPausa = False: Exit Sub
    chkQuery.value = 1
    chkQuery_Click
    If Not fConsComp And chkQuery.Visible Then chkQuery.SetFocus: Exit Sub
    cmbAuto.SelStart = 0
    cmbAuto.SelLength = 32000
End Sub

Private Sub cmbAuto_KeyUp(KeyCode As Integer, Shift As Integer)
    If KeyCode = 13 Then cmbAuto_Click
End Sub
'JAGS T8696 INICIO
'Sub anular_anticipo()
Sub anular_anticipo(concepto As String) 'JAGS T9827
    ReDim ValCoeg(1, 0)
    ReDim Arr(0)
    ReDim ValMovRc(3)
    Dim I, J As Integer
    Dim rc As Double
    Dim Mens As String
    'Condicion = "CD_CONC_CECP = " & Comi & Cambiar_Comas_Comillas(ClssImpuDesc.Concepto) & Comi
    Condicion = "CD_CONC_CECP = " & Comi & concepto & Comi 'JAGS T9827
    Condicion = Condicion & " AND CD_NUME_CECP = " & vcxp
    Result = LoadMulData("R_COEG_CXP", "CD_COEG_CECP,VL_CANC_CECP", Condicion, ValCoeg)
        'actualiza los anticipos y elimina la relacion de la cxp y los comprobantes de egreso
        
        For I = 0 To UBound(ValCoeg, 2)
           If Result <> FAIL Then
              If ValCoeg(1, I) = NUL$ Then ValCoeg(1, I) = 0
              Valores = "VL_ANTI_COEG=VL_ANTI_COEG-" & ValCoeg(1, I) & Coma
              Valores = Valores & "ID_ANTI_COEG=" & Comi & "S" & Comi
              Result = DoUpdate("COMPRO_EGRE", Valores, "CD_CODI_COEG=" & CDbl(ValCoeg(0, I)))
              If Result <> FAIL Then
                 Condicion = "CD_CONC_CECP = " & Comi & Cambiar_Comas_Comillas(ClssImpuDesc.concepto) & Comi
                 Condicion = Condicion & " AND CD_NUME_CECP = " & vcxp
                 Condicion = Condicion & " AND CD_COEG_CECP = " & CDbl(ValCoeg(0, I))
                 Result = DoDelete("R_COEG_CXP", Condicion)
              End If
           End If
        Next
    
        
    'NU_EGRE_CXP = -1 Cuando a la cxp se cruza con anticipos.
    If Result <> FAIL Then
       Result = DoUpdate("C_X_P", "NU_EGRE_CXP=0", "CD_NUME_CXP=" & vcxp)
    End If
End Sub
'JAGS T8696 INICIO


Private Sub cmdAnular_Click()
   'Dim InRespuesta As Integer 'AASV M3520
   Dim stExMsg As String, cObser As String, vInst As Integer, I&
   Dim DbMes As Double 'HRR R1873
    
   If Not WarnMsg("�Est� seguro que desea anular esta compra?") Then Exit Sub
   H.sbW "Anulando compra...", 10
       
   boICont = (Aplicacion.Interfaz_Contabilidad)
'AASV M4807 Nota:22222 Inicio Se coloca en comentario
'    'AASV M4807 Inicio
'    If boICont And Not IsEmpty(vContabCons) Then
'      If Valida_Cierre_Contable(Month(Nowserver)) Then Exit Sub
'    End If
'    'AASV M4807 Fin
'AASV M4807 Nota:22222 Fin
   If (BeginTran(STranIns & "IN_COMPRA") <> FAIL) Then
    
      '00 Validaciones iniciales:
      'JAGS T8696 SE AGREGA VALIDACION PARA Q SE CUMPLA CUANDO EL COMPROBANTE DE EGRESO NO SEA DE TIPO ANTICIPO
      If boICxP Then
         ReDim Arr(0)
         Desde = "R_COEG_CXP"
         Campos = "CD_COEG_CECP"
         Condicion = "CD_NUME_CECP=" & vcxp
         Result = LoadData(Desde, Campos, Condicion, Arr())
         If Encontro Then
            If Val(fnDevDato("COMPRO_EGRE", "VL_ANTI_COEG", "CD_CODI_COEG=" & Arr(0))) = 0 Then
            'JAGS T8696
               If Val(fnDevDato("C_X_P", "VL_CANC_CXP", "CD_NUME_CXP=" & vcxp)) > 0 Then Mensaje1 "Se han hecho egresos a la CxP, no es posible anular.", 2:   GoTo cancelar
            End If 'JAGS T8696
         End If 'JAGS T8696
      End If
    
      If Not boICxP And Not IsEmpty(vcxp) Then Mensaje1 "La interfase a cuentas por pagar no est� activa, la compra se guard� con esta interfase.", 2:   GoTo cancelar
      If Not boICont And Not IsEmpty(vContabCons) Then Mensaje1 "La interfase a contabilidad no est� activa, la compra se guard� con esta interfase.", 2:   GoTo cancelar
  
      If Not IsEmpty(vcxp) Then
         If Val(fnDevDato("C_X_P", "VL_NDEB_CXP", "CD_NUME_CXP=" & vcxp, True)) <> 0 Then
            Mensaje1 "Existe una nota d�bito asociada a la C_x_P que se gener� con esta compra, debe anularse primero.", 2
            GoTo cancelar
         End If
    
         If Val(fnDevDato("C_X_P", "VL_NCRE_CXP", "CD_NUME_CXP=" & vcxp, True)) <> 0 Then
            Mensaje1 "Existe una nota cr�dito asociada a la C_x_P que se gener� con esta compra, debe anularse primero.", 2
            GoTo cancelar
         End If
      End If
    '00 Fin.
'AASV R2238 INICIO SE COLOCA EN COMENTARIO
'    'HRR R1873
'AASV M4807 Nota:22222 Inicio Se quita comentario
      If boICont And Not IsEmpty(vContabCons) Then
         DbMes = Find_MesMovContable("CPI", CLng(vContabCons))
'AASV M4807 Nota:22284 Inicio
'         If DbMes = 0 Then
''            Call Mensaje1("No se encontro el mes del movimiento contable.", 2)
'            GoTo Cancelar
'         End If
'AASV M4807 Nota:22284 Fin
      End If
'AASV M4807 Nota:22222 Fin
'
'    'HRR R1873
'AASV R2238 FIN
    
      'Anulando la compra del art�culo:
      'AASV R2241 INICIO
      'If DoUpdate("IN_COMPRA", "TX_ESTA_COMP=2", "NU_AUTO_COMP=" & vConsec) = FAIL Then GoTo Cancelar
      Valores = "TX_ESTA_COMP=2"
      Valores = Valores & Coma & "NU_CONE_COMP = " & NumConex 'AASV R2241
      If DoUpdate("IN_COMPRA", Valores, "NU_AUTO_COMP=" & vConsec) = FAIL Then GoTo cancelar
      'AASV R2241 FIN
      'Anulando cuenta por pagar:
      'AASV M4858 Inicio
      'If boICxP And Not IsEmpty(vCxP) Then If DoUpdate("C_X_P", "ID_ESTA_CXP=2", "CD_NUME_CXP=" & vCxP) = FAIL Then GoTo Cancelar
      If boICxP And Not IsEmpty(vcxp) Then
         Valores = "ID_ESTA_CXP=2"
         Valores = Valores & Coma & "TX_NOUSAN_CXP = '" & NombreUsuario(UserId) & Comi
         'Valores = Valores & Coma & "FE_FEUSAN_CXP = " & FFechaCon(Nowserver) 'JAUM T29758 Se deja linea en comentario
         Valores = Valores & Coma & "FE_FEUSAN_CXP = " & FHFECHA(Format(Nowserver, FormatF & " HH:MM")) 'JAUM T29758
         If DoUpdate("C_X_P", Valores, "CD_NUME_CXP=" & vcxp) = FAIL Then
            GoTo cancelar
         End If
         'JAGS T8696
         ReDim Arr(1)
         Desde = "R_COEG_CXP"
         'Campos = "CD_COEG_CECP"
         Campos = "CD_COEG_CECP,CD_CONC_CECP" 'JAGS T9827
         Condicion = "CD_NUME_CECP=" & vcxp
         Result = LoadData(Desde, Campos, Condicion, Arr())
         If Encontro Then
            'Call anular_anticipo(Arr(1))
            Call anular_anticipo(CStr(Arr(1))) 'JAGS T9827
         End If
         'JAGS T8696
      End If
      'AASV M4858 Fin
      'Anulando comprobante contable:
      'If boICont And Not IsEmpty(vContabCons) Then
      If boICont And Not IsEmpty(vContabCons) And DbMes <> 0 Then 'AASV M4807 Nota:22284
         'If Valida_Cierre_Contable(Month(vFecha)) Then GoTo Cancelar 'HRR R1873
         If Valida_Cierre_Contable(DbMes) Then GoTo cancelar 'HRR R1873
         'SKRV T25681 INICIO Las siguientes 2 lineas se establecen en comentario
         'If Eliminar_Comprobante("CPI", CLng(vContabCons)) = FAIL Then GoTo cancelar
         'If Eliminar_Comprobante_New_NIIF("CPI", CLng(vContabCons)) = FAIL Then GoTo cancelar 'JLPB T25784-R25328
         If Eliminar_Comprobante("CPI", CLng(vContabCons), fnDevDato("MODU_MOV_NIIF", "NU_CODMO_MODU", "TX_DESCM_MODU = 'Compra Inventarios' AND NU_CODMO_MODU LIKE '06%' ", True), CStr(vConsec)) = FAIL Then GoTo cancelar
         If BoMovNiif = True Then 'JAUM T32800-R31099
            If Eliminar_Comprobante_New_NIIF("CPI", CLng(vContabCons), , fnDevDato("MODU_MOV_NIIF", "NU_CODMO_MODU", "TX_DESCM_MODU = 'Compra Inventarios' AND NU_CODMO_MODU LIKE '06%' ", True), CStr(vConsec)) = FAIL Then GoTo cancelar
         End If 'JAUM T32800-R31099
         'SKRV T25681 FIN
      End If



      'Eliminando observaciones (***):
      With flexEntradas
         stExMsg = "%2Error al actualizar las observaciones de las entradas."
         For I& = 1 To .Rows - 1
            cObser = fnDevDato("IN_ENCABEZADO", "TX_OBSE_ENCA", "NU_AUTO_ENCA=" & .TextMatrix(I&, 2))
            vInst = InStr(1, cObser, cObAdd)
            If vInst > 0 Then cObser = Mid(cObser, 1, vInst - 1)
            If DoUpdate("IN_ENCABEZADO", "TX_OBSE_ENCA='" & Trim(cObser) & "'", "NU_AUTO_ENCA=" & .TextMatrix(I&, 2)) = FAIL Then GoTo cancelar
         Next I&
      End With

   Else
      stExMsg = "%2Error al crear la transacci�n."
      GoTo cancelar
   End If

   H.Hide
        
   If (CommitTran() <> FAIL) Then
      chkQuery.value = 0
      chkQuery_Click
      boChBl = False 'No debe preguntar por cambios guardados
      Call cmdClean_Click
      H.sbMsg "La compra se anul� correctamente.", txtTercero, 3
      btModo = icNinguno
      'AASV M3520 Se deja la funcionalidad solo para cuando se guarde el documento
      'AASV R1883
      'If Aplicacion.PinvImpAntSCon Then
      '  InRespuesta = MsgBox("Desea imprimir el documento?", vbYesNo + vbInformation)
      '  If InRespuesta = vbYes Then
      'Documento.Encabezado.EsNuevoDocumento = False
      '      Call CmdImprimir_Click
      '  End If
      'End If
      'AASV R1883
          
   Else
      GoTo cancelar
   End If

   Exit Sub
    
cancelar:
   Call RollBackTran
   H.Hide
   H.sbMsg "La anulaci�n no se complet�." & stExMsg, cmdAnular, 2, 2

End Sub

'HRR R1873
Private Function Find_MesMovContable(ByVal StComprobante As String, ByVal LnConsecutivo As Long) As Double
   
   ReDim Arr(0)
   Condicion = " NU_COMP_MOVI='" & StComprobante & Comi & " AND NU_CONS_MOVI = " & LnConsecutivo
   Result = LoadData("MOVIMIENTOS", "TOP 1 NU_MES_MOVI", Condicion, Arr)
   If Result <> FAIL And Encontro Then
      Find_MesMovContable = Arr(0)
   Else
      Find_MesMovContable = 0
   End If

End Function
'HRR R1873

Private Sub cmdClean_Click()
    Dim CantE As String 'M:1339
    CantE = txtTercero  'M:1339
   
    If fnChanges Then If Not WarnMsg("�Desea perder los cambios?") Then Exit Sub
    ClssImpuDesc.TieneDatos = False 'HRR M2644
    sbLimpForm True
    txtTercero.SetFocus
    chkInicio.value = 0
    btModo = icNinguno
    boChBl = False
    fraFechas.Visible = True
    
    '01 Retomando valores que se ten�an antes de limpiar. M:1339
    txtTercero = CantE
    TxtTercero_LostFocus
    If chkQuery.Visible Then chkQuery.SetFocus
    '01 Fin.
    ReDim VrVBase(0, 0) 'JAUM T28644-R27752
    Unload frmImDe  'PJCA M1864
End Sub

'Eliminar los impuestos asginados:
Private Sub cmdClearImp_Click()

   If Not WarnMsg("�Desea eliminar los impuestos/descuentos aplicados?") Then Exit Sub

'HRR R1853
'    Dim i&, im&, J&, t%
'
'    With flexEntradas
'
'                t% = UBound(arrSerDescP, 1) 'S�lo se cambia una de las dimensiones de la matriz, aqu� se registra el n�mero de "Filas" que tiene la matriz actualmente, cada una representa una entrada de la grilla
'
'
'                    'A cada fila se le aplican los impuestos seleccionados, olvid�ndose de anteriores selecciones si se hicieron.
'                    For J& = 0 To UBound(arrSerTope) 'Y un bucle por cada uno de los impuestos que seleccion� el usuario
'                        If UBound(arrSerTope) < J& Then ReDim Preserve arrSerTope(t%, J&), arrSerCode(t%, J&), arrSerDescP(t%, J&), arrSerDescV(t%, J&), arrSerImpP(t%, J&), arrSerImpV(t%, J&), arrSerIDN�m(t%, J&)
'                        arrSerCode(J&) = Empty
'                        arrSerDescP(J&) = Empty
'                        arrSerDescV(J&) = Empty
'                        arrSerImpP(J&) = Empty
'                        arrSerTope(J&) = Empty
'                        arrSerIDN�m(J&) = Empty
'                    Next J&
'    End With
'HRR R1853
   'HRR R1853
   ReDim ArImp(6, 0)
   ReDim ArDesc(6, 0)
   Call ClssImpuDesc.Impuestos(ArImp)
   Call ClssImpuDesc.Descuentos(ArDesc)
   ClssImpuDesc.TieneDatos = False
   ClssImpuDesc.concepto = "CINV"
   'JAUM T28370-R22535 Inicio
   With flexEntradas
      If .Rows - 1 <> 1 Then
         Call Calcular_Mul
      Else
   'JAUM T28370-R22535 Fin
         Call Calcular
      End If 'JAUM T28370-R22535
   End With 'JAUM T28370-R22535
   'HRR R1853
   
    
   H.sbMsg "Se han eliminado los impuestos/descuentos%2.", flexEntradas, 1
    
   'Unload frmImDe 'HRR R1853
   'sbCalcImpSer 'Recalcular valores de las entradas. 'HRR R1853
        
End Sub

'HRR R1853
'Private Sub cmdClearImpComp_Click()
''Eliminar impuestos aplicados a la compra.
'
'    If Not WarnMsg("�Desea eliminar los impuestos/descuentos aplicados al total?") Then Exit Sub
'
'    Dim J&
'
'                    For J& = 0 To UBound(arrTope)
'                        arrCode(J&) = Empty
'                        arrDescP(J&) = Empty
'                        arrDescV(J&) = Empty
'                        arrImpP(J&) = Empty
'                        arrIDN�m(J&) = Empty
'                        arrTope(J&) = Empty
'                    Next J&
'
'    cmdClearImpComp.Enabled = False
'    cmdImpInfo.Enabled = False
'    h.sbMsg "Se han eliminado los impuestos/descuentos%2a la compra.", cmdImpCompr, 1
'    sbCalcImpSer
'
'End Sub
'HRR R1853

Private Sub cmdGuardar_Click()
Dim Mnsj As String
    
    '00 Validaciones:
        'If Not IsLong(val(txtN�mFact)) Then Mensaje1 "El n�mero de factura se sale de los l�mites (Int32).", 2: h.sbMsg "El n�mero de factura no puede ser %2almacenado en un tipo Int32.", txtN�mFact, 2, 2: Exit Sub
        If Trim(txtN�mFact) = "0" Or Trim(txtN�mFact) = "" Then Mensaje1 "Debe escribir un n�mero de factura.", 2: H.sbMsg "El n�mero de factura no puede ser%2alfanum�rico.", txtN�mFact, 2, 2: Exit Sub
        
        If fTransit And Not boICont Then 'Si se realizaron movimientos transitorios y no existe transferencia contable, no se puede realizar la compra.
            Mensaje1 "No se puede formalizar la compra si no existe la interfase contable, estas entradas probablemente generaron movimientos transitorios.", 2
            Exit Sub
        End If
        
        'HRR R1873
        'If MskFechFactura > txtFecha Then
        If Format(CDate(MskFechFactura), "yyyy/mm/dd") > Format(CDate(txtFecha), "yyyy/mm/dd") Then 'NMSR M3313
           Call Mensaje1("La fecha de la factura no puede ser  mayor a la fecha de radicacion.", 2): Exit Sub
        End If
        'HRR R1873
        
        'If boICont Then If Valida_Cierre_Contable(Month(txtFecha)) Then h.sbMsg "El mes '" & Format(txtFecha, "mmmm") & "' se encuentra cerrado en contabilidad.", cmdGuardar, 2, 2: Exit Sub 'HRR R1873
        'HRR R1873
        'AASV M4812 Inicio
        'If boICont Then
        If boICont And fTransit Then
        'AASV M4812 Fin
           If Aplicacion.PinvLastMesOpen Then
               Call Fech_MovContable
               If StFechaMovContable = NUL$ Then
                  Call Mensaje1("No se encontro el mes activo en contabilidad", 2): Exit Sub
               End If
           Else
               If Valida_Cierre_Contable(Month(txtFecha)) Then Exit Sub 'Call Mensaje1("El mes '" & Format(txtFecha, "mmmm") & "' se encuentra cerrado en contabilidad.", 2): Exit Sub
               StFechaMovContable = txtFecha
           End If
        'AASV M4812 Inicio
        Else
           StFechaMovContable = txtFecha
        'AASV M4812 Fin
        End If
        'HRR R1873
        
        If boICxP Then
            'AASV R2222 INICIO
            'If txtDPlazo = 0 Then
            If txtDPlazo = "0" Then 'AASV M4743
               If Not WarnMsg("Se est� realizando un proceso de compra con d�as de plazo cero (0)" & vbCrLf & vbTab & vbTab & " �Desea continuar?") Then Exit Sub
            End If
            'AASV R2222 FIN
            ReDim aAux(0): Result = LoadData("PARAMETROS_CXP", "FE_CIER_APLI", NUL$, aAux())
            If Month(aAux(0)) + (Year(aAux(0)) * 12) >= Month(txtFecha) + (Year(txtFecha) * 12) Then Call Mensaje1("El mes especificado en fecha de radicacion no puede usarse en CxP.", 2): Exit Sub
        End If
    '00 Fin
    
    If BoFacCompra Then Exit Sub    'NMSR R1924
    
         If Not WarnMsg("�Est� seguro que desea guardar la compra?") Then Exit Sub
         sbGuardar True
   

    
End Sub

'HRR R1873
Private Sub Fech_MovContable()
   
   
   Dim InMes As String
   ReDim Arr(0)
   'DRMG T46151 INICIO SE DEJA EN COMENTARIO LAS SIGIENTES 2 LINEAS
   'Condicion = "VL_SAIN_SALD=1 AND CD_CUEN_SALD='ZZZZZZZZZZZZZZZ'"
   'Result = LoadData("SALDOS", "MAX(NU_MES_SALD)", Condicion, Arr)
   Condicion = "VL_SAIN_SALD=0 AND CD_CUEN_SALD='ZZZZZZZZZZZZZZZ'"
   Result = LoadData("SALDOS", "MIN(NU_MES_SALD)", Condicion, Arr)
   'DRMG T46151 FIN
   If Result <> FAIL And Encontro Then
      'DRMG T46151 INICIO SE DEJA EN COMENTARIO LAS SIGIENTES 2 LINEAS
      'If Arr(0) <> NUL$ Then
         'InMes = (Arr(0) Mod 12) + 1
       If Arr(0) <> NUL$ And Arr(0) <= 12 Then
         InMes = Arr(0)
      'DRMG T46151 FIN
         StFechaMovContable = DatePart("d", MskFechFactura) & "/" & IIf(Len(InMes) = 1, "0" + InMes, InMes) & "/" & DatePart("yyyy", MskFechFactura)
         StFechaMovContable = Format(StFechaMovContable, FormatF)
      Else
         StFechaMovContable = NUL$
      End If
   End If
   
End Sub
'HRR R1873

'El concepto al cual env�a la cxp en tesorer�a es CINV, por lo cual debe estar creado con anticipaci�n.
Private Sub Graba_cxp(ByVal fechag As Variant, ByVal fechav As Variant, Cepto As String, Umero As Long, vADocu As Long)

   'vADocu *** �Tomar el primer documento para asociar los datos de presupuesto? ��ugh!!
   Dim Compro As String
   Dim DbNeto As Double 'JAGS T8530
   Dim I As Integer 'JAGS T8530
    
   Valores = "CD_CONCE_CXP=" & Comi & Cepto & Comi & Coma
   Valores = Valores & "CD_COUSRE_CXP=" & Comi & Cambiar_Comas_Comillas(UserId) & Comi & Coma  'EACT 18098 , 13715
   Valores = Valores & "CD_NUME_CXP=" & Umero & Coma
   'Valores = Valores & "FE_FECH_CXP=" & fechag & Coma 'JAUM T29758 Se deja linea en comentario
   Valores = Valores & "FE_FECH_CXP=" & FHFECHA(CStr(fechag) & " " & Format(Nowserver, "HH:MM")) & Coma 'JAUM T29758
   Valores = Valores & "FE_VENC_CXP=" & fechav & Coma
   Valores = Valores & "CD_TERC_CXP=" & Comi & txtTercero & Comi & Coma
   'Valores = Valores & "NU_FAPR_CXP=" & Val(txtN�mFact) & Coma
   Valores = Valores & "NU_FAPR_CXP=" & Comi & UCase(txtN�mFact) & Comi & Coma  'JACC R2221
   'Valores = Valores & "VL_BRUT_CXP=" & val(lblTotalB.Tag) - dbTComDesc & Coma 'HRR M2001
   'Valores = Valores & "VL_BRUT_CXP=" & Val(lblTotalB.Tag) - dbTComDesc - DbVlrIvaEnt & Coma 'HRR M2001 'HRR R1853
   Valores = Valores & "VL_BRUT_CXP=" & Val(lblTotalB.Tag) - DbVlrIvaEnt & Coma 'HRR M2001
   Valores = Valores & "VL_BRUE_CXP=0" & Coma
   Valores = Valores & "VL_NETO_CXP=" & Val(lblTotal.Tag) & Coma
   Valores = Valores & "VL_NCRE_CXP=0" & Coma
   Valores = Valores & "VL_NDEB_CXP=0" & Coma
   If BoCruceCxP = True Then
      Valores = Valores & "NU_EGRE_CXP=-1" & Coma 'JAGS T8735
   Else
      Valores = Valores & "NU_EGRE_CXP=0" & Coma
   End If
   Valores = Valores & "DE_OBSE_CXP=" & Comi & Cambiar_Comas_Comillas(txtObser) & Comi & Coma
   ''Fletes y retencion
   Valores = Valores & "VL_FLET_CXP=0" & Coma
   Valores = Valores & "VL_RETE_CXP=0" & Coma
   ''Descuentos e Impuestos
   'Valores = Valores & "VL_DESC_CXP=" & Val(lblDesc.Tag) - dbTComDesc & Coma 'HRR R1853
   'Valores = Valores & "VL_DESC_CXP=" & -Val(lblDesc.Tag) & Coma 'HRR R1853 'GAVL R5965  / T6197SE PONE EN COMENTARIO POR RQM 5965
   'Valores = Valores & "VL_IMPU_CXP=" & -val(lblImp.Tag) & Coma '|.DR.|, M:1747 'HRR M2001
   'Valores = Valores & "VL_IMPU_CXP=" & -Val(lblImp.Tag) + DbVlrIvaEnt & Coma '|.DR.|, M:1747 'HRR M2001 'GAVL R5965  / T6197SE PONE EN COMENTARIO POR RQM 5965
   
   'INICIO GAVL R5965 /T6197 se trata de aproximar las centenas de lso valores tanto de descuento como de impuesto si la opcion de CXP esta activa
   If BoAproxCent = True Then
'       Valores = Valores & "VL_DESC_CXP=" & AproxCentena(-Val(lblDesc.Tag)) & Coma
'       Valores = Valores & "VL_IMPU_CXP=" & AproxCentena(-Val(lblImp.Tag) + DbVlrIvaEnt) & Coma
      Valores = Valores & "VL_DESC_CXP=" & AproxCentena(Round(-Val(lblDesc.Tag))) & Coma 'GAVL T6265
      'Valores = Valores & "VL_IMPU_CXP=" & AproxCentena(Round(-Val(lblImp.Tag) + DbVlrIvaEnt)) & Coma 'GAVL T6265 'OMOG T19943 SE ESTABLECE EN COMENTARIO
      Valores = Valores & "VL_IMPU_CXP=" & AproxCentena(Round(-Val(lblImp.Tag))) & Coma 'OMOG T19943
   Else
      Valores = Valores & "VL_DESC_CXP=" & -Val(lblDesc.Tag) & Coma
      Valores = Valores & "VL_IMPU_CXP=" & -Val(lblImp.Tag) + DbVlrIvaEnt & Coma
   End If
   'FIN GAVL R5965 /T6197
   
   ''Monto Escrito
   Valores = Valores & "DE_MONT_CXP=" & Comi & Monto_Escrito(Val(lblTotal.Tag)) & Comi & Coma
   ''Estado
   Valores = Valores & "ID_ESTA_CXP=1" & Coma
   If Aplicacion.Interfaz_Presupuesto Then
      ReDim Arr(1, 0)
      Condicion = "NU_AUTO_ENCA_ENPP=" & vADocu
      Result = LoadMulData("IN_R_ENCA_PPTO", "NU_CONS_PTAL_ENPP,NU_TIPO_PTAL_ENPP", Condicion, Arr)
      If Result <> FAIL And Encontro Then
         Dim J As Double
         Valores = Valores & "ID_PPTO_CXP=1" & Coma
         For J = 0 To UBound(Arr, 2)
            If Arr(1, J) = 2 Then Valores = Valores & "NU_REGI_CXP=" & Arr(0, J) & Coma  'Asocia el RP
            'If Arr(1, J) = 3 Then Valores = Valores & "NU_OBLI_CXP=" & Arr(0, J)         'Asocio la Obliagcion
            If Arr(1, J) = 3 Then Valores = Valores & "NU_OBLI_CXP=" & Arr(0, J) & Coma        'Asocio la Obliagcion 'JAGS T8710 PNC
         Next J
      End If
   Else
      'Valores = Valores & "ID_PPTO_CXP=0"
      Valores = Valores & "ID_PPTO_CXP=0" & Coma 'JAGS T8530
   End If
    
   'JAGS T8530
   DbNeto = 0
   
   For I = 1 To Contador
      DbNeto = DbNeto + ValAnti(I, 0)
   Next I
   
   If BoCruceCxP = True Then
      If CDbl(lblTotal) > DbNeto Then
            
         Valores = Valores & "VL_CANC_CXP=" & DbNeto
      Else
         Valores = Valores & "VL_CANC_CXP=" & CDbl(lblTotal)
      End If
   Else
      Valores = Valores & "VL_CANC_CXP=0"
   End If
    
   'JAGS T8530

   Debug.Print Valores
   Result = DoInsertSQL("C_X_P", Valores)
   If Result <> FAIL Then Call TrasladoMovimiento("R_TRASMOV_CXP", Umero, "CXP")    'JACC M4759
   ''Actualiza el consecutivo del concepto
   If Result <> FAIL Then Result = DoUpdate("CONCEPTO", "NU_CONS_CONC=" & Umero, "ID_TIPO_CONC=7 And NU_CONS_CONC <" & Umero)
   If (Result <> FAIL) Then    'Busca el comprobante contable de la CxP
      ReDim Arrc(0)
      Result = LoadData("CONCEPTO", "CD_COMP_CONC", "ID_TIPO_CONC=7", Arrc())
      If Result <> FAIL Then
         If Encontro Then
            Compro = Arrc(0)
            Condicion = "CD_CODI_COMP=" & Comi & Compro & Comi & " AND NU_CONS_COMP <" & Umero
            Result = DoUpdate("TC_COMPROBANTE", "NU_CONS_COMP=" & Umero, Condicion)
         Else
            Call Mensaje1("No se encontro el comprobante contable de las CxP", 2)
         End If
      End If
   End If
   'If Result <> FAIL Then Result = DoUpdate("CONCEPTO", "NU_CONS_CONC=" & Umero, "NU_CONS_CONC <" & Umero)
   If Result <> FAIL Then Result = DoUpdate("PARAMETROS_CXP", "NU_CXPA_APLI=" & Umero, NUL$)
   If Result <> FAIL Then Unload frmImDe  'PJCA M1864
End Sub

'---------------------------------------------------------------------------------------
' Procedure : Graba_cxp_Mul
' DateTime  : 23/06/2015 09:08
' Author    : juan_urrego
' Purpose   : JAUM T28370-R22535 Almacena en el modulo de CxP cuando tiene multiples entradas
'---------------------------------------------------------------------------------------
'
Private Sub Graba_Cxp_Mul(ByVal fechag As Variant, ByVal fechav As Variant, Cepto As String, Numero As Long, LnAutoDocuMul() As Long)

   Dim StCompro As String 'C�digo comprobante contable
   Dim DbNeto As Double 'Valor Neto
   Dim InI As Integer 'Contador
   Dim InJ As Integer 'Contador
   Dim LnRetorno As Long 'Envia cero (0) cuando no se ha dimensionado un arreglo
   Valores = "CD_CONCE_CXP= 'CINM' , "
   Valores = Valores & "CD_COUSRE_CXP=" & Comi & Cambiar_Comas_Comillas(UserId) & Comi & Coma
   Valores = Valores & "CD_NUME_CXP=" & Numero & Coma
   'Valores = Valores & "FE_FECH_CXP=" & fechag & Coma 'JAUM T29758 Se deja linea en comentario
   Valores = Valores & "FE_FECH_CXP=" & FHFECHA(fechag & " " & Format(Nowserver, "HH:MM")) & Coma 'JAUM T29758
   Valores = Valores & "FE_VENC_CXP=" & fechav & Coma
   Valores = Valores & "CD_TERC_CXP=" & Comi & txtTercero & Comi & Coma
   Valores = Valores & "NU_FAPR_CXP=" & Comi & UCase(txtN�mFact) & Comi & Coma
   Valores = Valores & "VL_BRUT_CXP=" & Val(lblTotalB.Tag) - DbVlrIvaEnt & Coma
   Valores = Valores & "VL_BRUE_CXP=0" & Coma
   Valores = Valores & "VL_NETO_CXP=" & Val(lblTotal.Tag) & Coma
   Valores = Valores & "VL_NCRE_CXP=0" & Coma
   Valores = Valores & "VL_NDEB_CXP=0" & Coma
   If BoCruceCxP = True Then
      Valores = Valores & "NU_EGRE_CXP=-1" & Coma
   Else
      Valores = Valores & "NU_EGRE_CXP=0" & Coma
   End If
   Valores = Valores & "DE_OBSE_CXP=" & Comi & Cambiar_Comas_Comillas(txtObser) & Comi & Coma
   ''Fletes y retencion
   Valores = Valores & "VL_FLET_CXP=0" & Coma
   Valores = Valores & "VL_RETE_CXP=0" & Coma
   ''Descuentos e Impuestos
   Valores = Valores & "VL_DESC_CXP=0" & Coma
   Valores = Valores & "VL_IMPU_CXP=0" & Coma
   ''Monto Escrito
   Valores = Valores & "DE_MONT_CXP=" & Comi & Monto_Escrito(Val(lblTotal.Tag)) & Comi & Coma
   ''Estado
   Valores = Valores & "ID_ESTA_CXP=1" & Coma
   If Aplicacion.Interfaz_Presupuesto Then
      Valores = Valores & "ID_PPTO_CXP= 1" & Coma
      Valores = Valores & "NU_REGI_CXP= 0" & Coma
      Valores = Valores & "NU_OBLI_CXP= 0" & Coma
   Else
      Valores = Valores & "ID_PPTO_CXP=0" & Coma
      Valores = Valores & "NU_REGI_CXP= 0" & Coma
      Valores = Valores & "NU_OBLI_CXP= 0" & Coma
   End If
   DbNeto = 0
   For InI = 1 To Contador
      DbNeto = DbNeto + ValAnti(InI, 0)
   Next InI
   If BoCruceCxP = True Then
      If CDbl(lblTotal) > DbNeto Then
         Valores = Valores & "VL_CANC_CXP=" & DbNeto
      Else
         Valores = Valores & "VL_CANC_CXP=" & CDbl(lblTotal)
      End If
   Else
      Valores = Valores & "VL_CANC_CXP=0"
   End If
   Debug.Print Valores
   Result = DoInsertSQL("C_X_P", Valores)
   If Result <> FAIL Then Call TrasladoMovimiento("R_TRASMOV_CXP", Numero, "CXP")
   'Graba las CxP hijas
   With flexEntradas
      GetSafeArrayPointer DbTDCond(), LnRetorno
      If LnRetorno = 0 Then
         ReDim DbTDCond(.Rows - 1)
      End If
      For InI = 1 To .Rows - 1
         DbGrabado = CDbl(.TextMatrix(InI, 6))
         If boIPPTO Then DbNet = DbGrabado + CDbl(.TextMatrix(InI, 8)) Else DbNet = DbGrabado
         DbDescu = CDbl(-DbTDCom(InI)) + CDbl(-DbTDCond(InI))
         DbImpu = CDbl(-DbTImp(InI))
         ReDim Arr(1, 0)
         Condicion = "NU_AUTO_ENCA_ENPP=" & LnAutoDocuMul(InI)
         Result = LoadMulData("IN_R_ENCA_PPTO", "NU_CONS_PTAL_ENPP,NU_TIPO_PTAL_ENPP", Condicion, Arr)
         If Encontro Then
            For InJ = 0 To UBound(Arr, 2)
               If Arr(1, InJ) = 2 Then StPres = Arr(0, InJ)
               If Arr(1, InJ) = 3 Then DbObl = Arr(0, InJ)
            Next
         Else
            StPres = 0: DbObl = 0
         End If
         Call Grabar_Cxp_Hija(CDbl(Numero), Cepto, InI)
      Next
   End With
   ''Actualiza el consecutivo del concepto
   If Result <> FAIL Then Result = DoUpdate("CONCEPTO", "NU_CONS_CONC=" & Numero, "ID_TIPO_CONC=7 And NU_CONS_CONC <" & Numero)
   If (Result <> FAIL) Then    'Busca el comprobante contable de la CxP
      ReDim Arrc(0)
      Result = LoadData("CONCEPTO", "CD_COMP_CONC", "ID_TIPO_CONC=7", Arrc())
      If Result <> FAIL Then
         If Encontro Then
            StCompro = Arrc(0)
            Condicion = "CD_CODI_COMP=" & Comi & StCompro & Comi & " AND NU_CONS_COMP <" & Numero
            Result = DoUpdate("TC_COMPROBANTE", "NU_CONS_COMP=" & Numero, Condicion)
         Else
            Call Mensaje1("No se encontro el comprobante contable de las CxP", 2)
         End If
      End If
   End If
   If Result <> FAIL Then Result = DoUpdate("PARAMETROS_CXP", "NU_CXPA_APLI=" & Numero, NUL$)
   If Result <> FAIL Then Unload frmImDe
End Sub

'NMSR R1924
'---------------------------------------------------------------------------------------
' Procedure : BoFacCompra
' DateTime  : 04/03/2008 14:43
' Author    : natalia_silva
' Purpose   : Retorna True si hay alguna compra con la Facturar y Tercero con que se va a hacer la compra.
'---------------------------------------------------------------------------------------
Function BoFacCompra() As Boolean
Dim ArFact() As Variant
Dim InI As Integer
Dim StCompras As String

ReDim ArFact(0, 0)

    BoFacCompra = False
    
    Condicion = "CD_CODI_TERC_COMP=" & Comi & txtTercero & Comi
    'Condicion = Condicion & " AND TX_FAPR_COMP=" & Val(txtN�mFact)
    'Condicion = Condicion & " AND TX_FAPR_COMP=" & Comi & txtN�mFact & Comi 'NMSR M3033
    Condicion = Condicion & " AND TX_FAPR_COMP=" & Comi & UCase(txtN�mFact) & Comi 'JACC R2221
    Condicion = Condicion & " AND TX_ESTA_COMP<>2" 'NMSR M3055
    'Result = LoadMulData("IN_COMPRA", "NU_AUTO_COMP", Condicion, ArFact) 'JLPB T21625 SE DEJA EN COMENTARIO
    Result = LoadMulData("IN_COMPRA WITH (NOLOCK)", "NU_AUTO_COMP", Condicion, ArFact) 'JLPB T21625
    If Result <> FAIL And Encontro Then
        StCompras = "El N�mero de Factura ingresado  para el tercero"
        StCompras = StCompras & " ya se encuentra en la Compra : N�"
        For InI = 0 To UBound(ArFact, 2)
            StCompras = StCompras & ArFact(0, InI) & Coma
        Next
         StCompras = Mid(StCompras, 1, Len(StCompras) - 1)
        Call Mensaje1(StCompras, 1)
        BoFacCompra = True
    End If
End Function 'NMS R1924

'---------------------------------------------------------------------------------------
' Procedure : BoFacCxP
' DateTime  : 05/03/2008 14:12
' Author    : natalia_silva
' Purpose   : Retorna True si hay alguna CXP con la Facturar y Tercero con que se va a hacer la compra.
'---------------------------------------------------------------------------------------
'
Function BoFacCXP() As Boolean
Dim ArCXP() As Variant
Dim InI As Integer
Dim StCXP As String

ReDim ArCXP(0, 0)

    BoFacCXP = False
    
    Condicion = "CD_TERC_CXP=" & Comi & txtTercero & Comi
    'Condicion = Condicion & " AND NU_FAPR_CXP=" & Val(txtN�mFact)
    Condicion = Condicion & " AND NU_FAPR_CXP= '" & UCase(txtN�mFact) & Comi  'JACC R2221
    Condicion = Condicion & " AND ID_ESTA_CXP<>2" 'NMSR M3055
    Result = LoadMulData("C_X_P", "CD_NUME_CXP", Condicion, ArCXP)
    If Result <> FAIL And Encontro Then
        StCXP = "El N�mero de Factura ingresado  para el tercero"
        StCXP = StCXP & " ya se encuentra en la CXP : N� "
        For InI = 0 To UBound(ArCXP, 2)
            StCXP = StCXP & ArCXP(0, InI) & Coma
        Next
        StCXP = Mid(StCXP, 1, Len(StCXP) - 1)
        
        Call Mensaje1(StCXP, 1)
        BoFacCXP = True
    End If
End Function 'NMS R1924

'HRR R1853
'Private Sub cmdImpCompr_Click()
'
'    Dim arrData(), boResult As Boolean
'    boResult = frmImDe.fnSelDescImp(arrData(), 0) 'Esta funci�n devuelve una matriz con 2 "columnas", la primera con el indicador impuesto/descuento, y la segunda con el c�digo.
'
'    If boResult Then
'
'        Dim J&, arrImpX(), arrDesX(), stCond
'        ReDim arrImpX(3, 0), arrDesX(4, 0)
'
'            For J& = 0 To UBound(arrData(), 2) 'Un bucle por cada uno de los impuestos que seleccion� el usuario
'                If UBound(arrTope, 1) < J& Then ReDim Preserve arrTope(J&), arrCode(J&), arrDescP(J&), arrDescV(J&), arrImpP(J&), arrIDN�m(J&)
'                stCond = "'" & arrData(1, J&) & "'"
'                arrCode(J&) = arrData(1, J&)
'                arrDescP(J&) = Empty
'                arrDescV(J&) = Empty
'                arrImpP(J&) = Empty
'                If arrData(0, J&) = "I" Then
'                        Result = LoadMulData("TC_IMPUESTOS", "CD_CODI_IMPU,DE_NOMB_IMPU,PR_PORC_IMPU,VL_TOPE_IMPU", "CD_CODI_IMPU=" & stCond, arrImpX)
'                        arrIDN�m(J&) = arrImpX(1, 0)
'                        arrImpP(J&) = arrImpX(2, 0)
'                        arrTope(J&) = arrImpX(3, 0)
'                Else
'                        Result = LoadMulData("DESCUENTO", "CD_CODI_DESC,DE_NOMB_DESC,PR_PORC_DESC,VL_TOPE_DESC,VL_VALO_DESC", "CD_CODI_DESC=" & stCond, arrDesX)
'                        arrIDN�m(J&) = arrDesX(1, 0)
'                        If Val(arrDesX(2, 0)) <> 0 Then arrDescP(J&) = arrDesX(2, 0) Else arrDescV(J&) = arrDesX(4, 0)   'Si no tiene porcentaje, entonces se inserta el valor
'                        arrTope(J&) = arrDesX(3, 0)
'                End If
'            Next J&
'
'        cmdClearImpComp.Enabled = True
'        cmdImpInfo.Enabled = True
'        sbCalcImpSer 'Recalcular valores.
'
'    End If
'End Sub
'HRR R1853

'HRR R1853
'Private Sub cmdImpInfo_Click()
'    '00 Informaci�n de los impuestos en la fila seleccionada:
'    Dim StInfo As String, i&
'
'        For i& = 0 To UBound(arrCode)
'            If Not IsEmpty(arrIDN�m(i&)) Then
'                StInfo = StInfo & arrCode(i&) & vbTab & arrIDN�m(i&) & vbTab
'                If Not IsEmpty(arrDescP(i&)) Then
'                    StInfo = StInfo & arrDescP(i&) & " %" & vbCrLf
'                ElseIf Not IsEmpty(arrDescV(i&)) Then
'                    StInfo = StInfo & "$ " & arrDescV(i&) & vbCrLf
'                Else
'                    StInfo = StInfo & arrImpP(i&) & " %" & vbCrLf
'                End If
'            End If
'        Next i&
'
'        If Trim(StInfo) <> "" Then
'            Mensaje1 StInfo, 3
'        Else
'            Mensaje1 "No hay impuestos/descuentos asignados a la compra.", 2
'        End If
'
'    '00 Fin.
'End Sub
'HRR R1853

Private Sub CmdImprimir_Click()
            
    H.sbW "Generando reporte...", 6

  sbCVistasRep " And NU_AUTO_COMP=" & vConsec
  'sbCVistasRep " And NU_AUTO_COMP=" & vConsec & " AND dbo.ARTICULO.NU_AUTO_ARTI = dbo.IN_KARDEX.NU_AUTO_ARTI_KARD " 'JAGS T9275 PNC

  ReDim Arr(7)
  MDI_Inventarios.Crys_Listar.Formulas(1) = "FECHAIMP='" & Format(Nowserver, "dddddd") & "'"

  Call Listar1("InfCompraINV.rpt", NUL$, NUL$, 0, "Compra de art�culos", MDI_Inventarios)
  
    H.Hide
    
End Sub


Sub sbCVistasRep(stAddCond As String)
'Crear las vistas necesarias para el reporte:
    Result = fnEliminarVista(stVistaRep)
    'HRR T12052 Inicio
    Result = fnCrearVista(stVistaRep, _
 _
    "Select *  " & _
    "From TERCERO,ARTICULO,IN_ENCABEZADO,IN_DETALLE,IN_COMPRA,IN_R_COMP_ENCA " & _
    "Where NU_AUTO_COMP_COEN=NU_AUTO_COMP And NU_AUTO_ENCA_COEN=NU_AUTO_ENCA And CD_CODI_TERC_ENCA=CD_CODI_TERC " & _
    "And NU_AUTO_ORGCABE_DETA=NU_AUTO_ENCA AND NU_AUTO_MODCABE_DETA=NU_AUTO_ENCA " & _
    "AND NU_AUTO_ARTI_DETA=NU_AUTO_ARTI " & stAddCond, _
 _
    "Usado por el informe (InfCompraINV) contiene los detalles de la compra.")
'    'JAGS T9275 INICIO
'    Result = fnCrearVista(stVistaRep, _
' _
'    "Select *  " & _
'    "From TERCERO,ARTICULO,IN_ENCABEZADO,IN_DETALLE,IN_COMPRA,IN_R_COMP_ENCA " & _
'    "Where NU_AUTO_COMP_COEN=NU_AUTO_COMP And NU_AUTO_ENCA_COEN=NU_AUTO_ENCA And CD_CODI_TERC_ENCA=CD_CODI_TERC And NU_AUTO_ORGCABE_DETA=NU_AUTO_ENCA And NU_AUTO_ARTI_DETA=NU_AUTO_ARTI " & stAddCond, _
' _
'    "Usado por el informe (InfCompraINV) contiene los detalles de la compra.")
    'HRR T12052 Fin
    
'    Result = fnCrearVista(stVistaRep, _
' _
'    "Select *  " & _
'    "From TERCERO,ARTICULO,IN_ENCABEZADO,IN_DETALLE,IN_COMPRA,IN_R_COMP_ENCA,IN_KARDEX,IN_KARDEXLOTE,IN_LOTE " & _
'    "Where NU_AUTO_COMP_COEN=NU_AUTO_COMP And NU_AUTO_ENCA_COEN=NU_AUTO_ENCA And CD_CODI_TERC_ENCA=CD_CODI_TERC And NU_AUTO_ORGCABE_DETA=NU_AUTO_ENCA And NU_AUTO_ARTI_DETA=NU_AUTO_ARTI" & stAddCond, _
' _
'    "Usado por el informe (InfCompraINV) contiene los detalles de la compra.") 'JAGS T9275 PNC
    

    
    'JAGS T9275 FIN
    
    If Result = FAIL Then Mensaje1 "No se pudo crear la 'vista/consulta' necesaria para generar el reporte.", 2

End Sub
Private Sub cmdImpuestos_Click()

   Call Show_DescuImpu 'HRR R1853
'HRR R1853
'        Dim arrData(), boResult As Boolean
'
'        'HRR M1698
'        frmImDe.AutoRet = AutoRet
'        frmImDe.AutRetIca = AutRetIca
'        frmImDe.AutRetIva = AutRetIva
'        frmImDe.ExentoFuente = ExentoFuente
'        frmImDe.ExentoIca = ExentoIca
'        frmImDe.ExentoIva = ExentoIva
'        frmImDe.TipoPers = TipoPers
'        frmImDe.TipoRegimen = TipoRegimen
'        frmImDe.GranContrib = GranContrib
'        'HRR M1698
'        'boResult = frmImDe.fnSelDescImp(arrData()) 'Esta funci�n devuelve una matriz con 2 "columnas", la primera con el indicador impuesto/descuento, y la segunda con el c�digo.
'         boResult = frmImDe.fnSelDescImp(arrData(), dbVlNeto)  'PJCA M1862
'
'    If boResult Then
'
'        Dim i&, J&, t%, arrImpX(), arrDesX(), stCond, Inicio&  'inicio& se usa para evitar llamar sentencias SQL nuevamente, esta variable almacena la primera entrada que recibio los datos y luego "todos se copian de esa".
'        ReDim arrImpX(5, 0), arrDesX(6, 0)
'
'        With flexEntradas
'
'                    t% = UBound(arrSerDescP, 1) 'S�lo se cambia una de las dimensiones de la matriz, aqu� se registra el n�mero de "Filas" que tiene la matriz actualmente, cada una representa una entrada de la grilla
'                    Inicio& = .Row  'Almacenar el �ndice del primer elemento que recibir� los datos.
'
'                    sbVacImp
'
'                        'A cada fila se le aplican los impuestos seleccionados, olvid�ndose de anteriores selecciones si se hicieron.
'                        For J& = 0 To UBound(arrData(), 2) 'Y un bucle por cada uno de los impuestos que seleccion� el usuario
'                            If UBound(arrSerTope) < J& Then ReDim Preserve arrSerTope(J&), arrSerCode(J&), arrSerDescP(J&), arrSerDescV(J&), arrSerImpP(J&), arrSerImpV(J&), arrSerIDN�m(J&), arrSerCuen(J&), arrSerTipo(J&), arrSerVFinal(J&)
'                            stCond = "'" & arrData(1, J&) & "'"
'                            arrSerCode(J&) = arrData(1, J&)
'                            arrSerDescP(J&) = Empty: arrSerDescV(J&) = Empty: arrSerImpP(J&) = Empty: arrSerCuen(J&) = Empty: arrSerTipo(J&) = Empty
'                            If arrData(0, J&) = "I" Then '======== IMPUESTO ========
'                                    Result = LoadMulData("TC_IMPUESTOS", "CD_CODI_IMPU,DE_NOMB_IMPU,PR_PORC_IMPU,VL_TOPE_IMPU,CD_CUEN_IMPU,ID_TIPO_IMPU", "CD_CODI_IMPU=" & stCond, arrImpX)
'                                    arrSerIDN�m(J&) = arrImpX(1, 0)
'                                    arrSerImpP(J&) = arrImpX(2, 0)
'                                    arrSerTope(J&) = arrImpX(3, 0)
'                                    arrSerCuen(J&) = arrImpX(4, 0)
'                                    arrSerTipo(J&) = arrImpX(5, 0)
'                            Else '======== DESCUENTO ========
'                                    Result = LoadMulData("DESCUENTO", "CD_CODI_DESC,DE_NOMB_DESC,PR_PORC_DESC,VL_TOPE_DESC,VL_VALO_DESC,CD_CUEN_DESC,TX_TIPO_DESC", "CD_CODI_DESC=" & stCond, arrDesX)
'                                    arrSerIDN�m(J&) = arrDesX(1, 0)
'                                    If Val(arrDesX(2, 0)) <> 0 Then arrSerDescP(J&) = arrDesX(2, 0) Else arrSerDescV(J&) = arrDesX(4, 0)   'Si no tiene porcentaje, entonces se inserta el valor
'                                    arrSerTope(J&) = arrDesX(3, 0)
'                                    arrSerCuen(J&) = arrDesX(5, 0)
'                                    arrSerTipo(J&) = arrDesX(6, 0)
'                            End If
'                        Next J&
'
'
'
'        End With
'
'        sbCalcImpSer 'Recalcular valores de las entradas.
'        If boDescCom Then
'            'If btHIC = 0 Then h.sbMsg "Los descuentos comerciales afectan directamente el valor base y no se reflejan en los dem�s valores.", flexEntradas
'            btHIC = 1
'        ElseIf fRIva Then
'            h.sbMsg "Seleccion� impuestos que se calculan sobre el IVA, no ser�n tenidos en cuenta.", flexEntradas
'        Else
'            btHImp = 1
'        End If
'
'    End If
'HRR R1853

End Sub

'HRR R1853
Private Sub Show_DescuImpu()

Dim ArDesc() As Variant
Dim ArImp() As Variant

Dim InI As Integer
  
   ReDim ArDesc(8, ClssImpuDesc.SizeDesc)
   
   For InI = 0 To ClssImpuDesc.SizeDesc
         
         ArDesc(0, InI) = ClssImpuDesc.ElemDesc(InI, 6) 'tipo
         ArDesc(1, InI) = ClssImpuDesc.ElemDesc(InI, 0) 'codigo
         ArDesc(2, InI) = ClssImpuDesc.ElemDesc(InI, 1) 'nombre
         'ArDesc(3, Ini) = ClssImpuDesc.ElemDesc(Ini, 4) 'valor
         'INICIO GAVL T6265
         If BoAproxCent = True Then
           ArDesc(3, InI) = AproxCentena(Round(ClssImpuDesc.ElemDesc(InI, 4))) 'valor
         Else
           ArDesc(3, InI) = ClssImpuDesc.ElemDesc(InI, 4) 'valor
         End If
         'FIN  GAVL T6265
         ArDesc(4, InI) = "R"
         ArDesc(5, InI) = ClssImpuDesc.ElemDesc(InI, 2) '%
         ArDesc(6, InI) = "D"
         ArDesc(7, InI) = ClssImpuDesc.ElemDesc(InI, 3) 'tope
         ArDesc(8, InI) = ClssImpuDesc.ElemDesc(InI, 5) 'cuenta
         
      
   Next
   
   'ReDim ArImp(9, ClssImpuDesc.SizeImp) 'JAUM T28644-R27752 Se deja linea en comentario
   ReDim ArImp(10, ClssImpuDesc.SizeImp) ''JAUM T28644-R27752
   
   For InI = 0 To ClssImpuDesc.SizeImp
            
      ArImp(0, InI) = NUL$
      ArImp(1, InI) = ClssImpuDesc.ElemImp(InI, 0) 'codigo
      ArImp(2, InI) = ClssImpuDesc.ElemImp(InI, 1) 'nombre
      'ArImp(3, Ini) = ClssImpuDesc.ElemImp(Ini, 4) 'valor
      
      'INICIO GAVL T6265
         If BoAproxCent = True Then
           ArImp(3, InI) = AproxCentena(Round(ClssImpuDesc.ElemImp(InI, 4))) 'valor
         Else
           ArImp(3, InI) = ClssImpuDesc.ElemImp(InI, 4) 'valor
         End If
      'FIN  GAVL T6265
      
      ArImp(4, InI) = "R"
      ArImp(5, InI) = ClssImpuDesc.ElemImp(InI, 2) '%
      ArImp(6, InI) = "I"
      ArImp(7, InI) = ClssImpuDesc.ElemImp(InI, 3) 'tope
      ArImp(8, InI) = ClssImpuDesc.ElemImp(InI, 5) 'cuenta
      ArImp(9, InI) = ClssImpuDesc.ElemImp(InI, 6) 'tipo
      ArImp(10, InI) = fnDevDato("TC_IMPUESTOS", "TX_RETEFU_IMPU", "CD_CODI_IMPU = '" & ClssImpuDesc.ElemImp(InI, 0) & Comi, True) 'JAUM T28644-R27752
   Next
         
   'FrmDesImp.BoArbol = False ' PARA QUE DESHABILITE LAS OPCIONES DEL FORMULARIO IMDES
   
   FrmDesImp.AutoRet = AutoRet
   FrmDesImp.AutRetIca = AutRetIca
   FrmDesImp.AutRetIva = AutRetIva
   FrmDesImp.ExentoFuente = ExentoFuente
   FrmDesImp.ExentoIca = ExentoIca
   FrmDesImp.ExentoIva = ExentoIva
   FrmDesImp.TipoPers = TipoPers
   FrmDesImp.TipoRegimen = TipoRegimen
   FrmDesImp.GranContrib = GranContrib
   FrmDesImp.DbBase = dbVlNeto
   FrmDesImp.DbIva = DbVlrIvaEnt
   FrmDesImp.BoArbol = False
   
   
   'Call FrmDesImp.Cargar_ImpDesc(ArDesc, ArImp, ClssImpuDesc) 'HRR CAMBIO JAUM T28644-R27752 Se deja linea en comentario
   Call FrmDesImp.Cargar_ImpDesc(ArDesc, ArImp, ClssImpuDesc, VrVBase, InCantEntra) 'JAUM T28644-R27752
     
   'Set ClssImpuDesc.RefImpuDesc = FrmDesImp.ClssImpDes 'HRR CAMBIO
   
        
   FrmDesImp.Show 1
   VrVBase = frmFaseCompra.VrVBase 'JAUM T28644-R27752 T29609
   'JAUM T28370-R22535 Inicio
   With flexEntradas
      If .Rows - 1 <> 1 Then
         Call Calcular_Mul
      Else
   'JAUM T28370-R22535 Fin
         Call Calcular
      End If 'JAUM T28370-R22535
   End With 'JAUM T28370-R22535
End Sub
'HRR R1853

Private Sub CmdPpto_Click()
    If Val(lblTotal.Tag) = 0 Then H.sbMsg "�El total de la compra es cero!", cmdPPTO, 3: sbVFlash 0, False, lblTotal, lblEtiq(9): Exit Sub
    FrmPresu.Desde_Forma = Me
    'PptoArti = Articulos_Presupuesto(flexEntradas, "NU_COMP_CUEN,NU_CDPE_CUEN", 15, 0) 'REOL M807
    Call FrmPresu.CargarDatos(Val(lblTotal.Tag), txtFecha, "", 0, True)
    FrmPresu.Show 1
    If Trim(txtPPTO(0)) <> "" Then sbZonaEdicion False
End Sub

Private Sub CmdSalir_Click()
    Unload frmImDe
    Unload Me
End Sub

Private Sub cmdStart_Click()

    
    Dim I&, t%, aEnt(), cCond As String, ArrX(0)
    Dim InDec As Integer 'HRR M2669
    Dim StSubConsulta As String 'AASV M5907
    Dim StSubConsultaD As String 'AASV M5907
   Dim InI As Integer 'JLPB T35969 Utilziada para el contador del for
   'JLPB T22266 INICIO
   Dim VrArr() As Variant
   Dim DbIva As Double
   Dim DbTotal As Double
   DbIva = 0
   DbTotal = 0
   'JLPB T22266 FIN
   Dim DbTDescu As Double 'JAUM T28836 Almacena el total de los decuentos por cada entrada
    
    BoEntImpDesc = False 'HRR R1853
    
    boICont = (Aplicacion.Interfaz_Contabilidad)
    boICxP = (Aplicacion.Interfaz_CxP)
    t% = fnRowSels - 1 'Tama�o para informaci�n de cada entrada
    If t% < 0 Then Mensaje1 ("No ha seleccionado las entradas"), 2: Exit Sub
    
    sbStartEnabled 'Desactivar controles que no son editables al iniciar la creaci�n.

    cmdGuardar.Enabled = True: cmdXLS.Enabled = True: cmdPPTO.Enabled = True

'-- Eliminando filas que no se seleccionaron. --
    With flexEntradas
DeNuevo:
        For I& = 1 To .Rows - 1
            If .TextMatrix(I&, 0) <> " " Then .RemoveItem I&: GoTo DeNuevo
        Next I&
                
        ReDim aEnt(0, flexEntradas.Rows - 2) 'aEnt es usado por otras operaciones fuera de este bloque, se aprovecha aqu� este bucle para obtener la informaci�n. La bidimensionalidad de la matriz, permite la compatibilidad con cierta funci�n.
        For I& = 1 To .Rows - 1
            aEnt(0, I& - 1) = .TextMatrix(I&, 2)
        Next I&
        
    End With
'-- Eliminando filas que no se seleccionaron. --

'-- Estableciendo si en realidad se realizaron movimientos transitorios:
    If fTransit And Not boICont Then
        cCond = "NU_COMP_CONTA_ENCA = NU_CONS_MOVC And NU_AUTO_ENCA In " & fnStValores(aEnt, 0)
        Result = LoadData("MOV_CONTABLE, IN_ENCABEZADO", "*", cCond, ArrX())
        fTransit = Result <> FAIL And Encontro
    End If
'-- Estableciendo si en realidad se realizaron movimientos transitorios.
    'AASV M5907 Inicio
    StSubConsulta = "( SELECT SUM(A.VL_VALO_MOVC) AS VALDEV ,CD_CUEN_MOVC AS CUENTA FROM MOV_CONTABLE A, IN_ENCABEZADO B"
    StSubConsulta = StSubConsulta & " Where B.NU_COMP_CONTA_ENCA = A.NU_CONS_MOVC And A.ID_NATU_MOVC= 'C' And A.CD_CONC_MOVC='DVEN' And B.NU_AUTO_ENCA In "
    StSubConsulta = StSubConsulta & " (SELECT DISTINCT NU_AUTO_ORGCABE_DETA FROM IN_DETALLE WHERE NU_AUTO_MODCABE_DETA IN " & fnStValores(aEnt, 0) & " )And B.TX_ESTA_ENCA <> 'A' GROUP BY CD_CUEN_MOVC)AS DEVOLUCION"
    StSubConsulta = StSubConsulta & " ON MOV_CONTABLE.CD_CUEN_MOVC = DEVOLUCION.CUENTA"
    
    StSubConsultaD = "( SELECT SUM(A.VL_VALO_MOVC) AS VALDEV ,CD_CUEN_MOVC AS CUENTA FROM MOV_CONTABLE A, IN_ENCABEZADO B"
    StSubConsultaD = StSubConsultaD & " Where B.NU_COMP_CONTA_ENCA = A.NU_CONS_MOVC And A.ID_NATU_MOVC= 'D' And A.CD_CONC_MOVC='DVEN' And B.NU_AUTO_ENCA In "
    StSubConsultaD = StSubConsultaD & " (SELECT DISTINCT NU_AUTO_ORGCABE_DETA FROM IN_DETALLE WHERE NU_AUTO_MODCABE_DETA IN " & fnStValores(aEnt, 0) & " )And B.TX_ESTA_ENCA <> 'A' GROUP BY CD_CUEN_MOVC)AS DEVOLUCION"
    StSubConsultaD = StSubConsultaD & " ON MOV_CONTABLE.CD_CUEN_MOVC = DEVOLUCION.CUENTA"
    
    'AASV M5907 Fin

'-- Estableciendo los movimientos contables "contra" transitorios y normales:
If fTransit And boICont Then
    'HRR M2669
    If Aplicacion.VerDecimales_Valores(InDec) Then
    Else
       InDec = 0
    End If
    'HRR M2669
    stCuenta = fnDevDato("PARAMETROS_INVE", "CD_CXP_APLI") 'smdl M1717
    ReDim aTranCnArt(6, 0), aTranCnCxP(6, 0), aNormCnArt(7, 0), aNormCnCxP(6, 0)
    ReDim VrArr(5, 0) 'JLPB T22266
    'AASV M5907 Inicio
    'cCond = "NU_COMP_CONTA_ENCA = NU_CONS_MOVC And ID_NATU_MOVC='D' And CD_CONC_MOVC='ENTR' And  NU_AUTO_ENCA In " & fnStValores(aEnt, 0)
    'Result = LoadMulData("MOV_CONTABLE, IN_ENCABEZADO", "'(Movimiento transitorio del art�culo)',CD_CUEN_MOVC,VL_VALO_MOVC,'C',CD_TERC_MOVC,CD_CECO_MOVC,VL_BASE_MOVC", cCond, aTranCnArt())
    
    cCond = "ID_NATU_MOVC='D' And CD_CONC_MOVC='ENTR' And  NU_AUTO_ENCA In " & fnStValores(aEnt, 0)
    Campos = "'(Movimiento transitorio del art�culo)',CD_CUEN_MOVC,"
    'Campos = Campos & " (VL_VALO_MOVC - ISNULL( DEVOLUCION.VALDEV,0))"
    Campos = Campos & " SUM(VL_VALO_MOVC) - ISNULL( DEVOLUCION.VALDEV,0)" 'APGR T9087
    Campos = Campos & ",'C',CD_TERC_MOVC,CD_CECO_MOVC,VL_BASE_MOVC"
    Desde = " MOV_CONTABLE INNER JOIN IN_ENCABEZADO ON MOV_CONTABLE.NU_CONS_MOVC = IN_ENCABEZADO.NU_COMP_CONTA_ENCA LEFT OUTER JOIN "
    cCond = cCond & " GROUP BY CD_CUEN_MOVC, CD_TERC_MOVC,CD_CECO_MOVC,VL_BASE_MOVC,DEVOLUCION.VALDEV" 'APGR T9087
    Result = LoadMulData(Desde & StSubConsulta, Campos, cCond, aTranCnArt())
    'AASV M5907 Fin
'    cCond = "NU_AUTO_ORGCABE_DETA=NU_AUTO_MODCABE_DETA And CD_CODI_GRUP = CD_GRUP_ARTI And NU_AUTO_ARTI = NU_AUTO_ARTI_DETA And NU_AUTO_ENCA = NU_AUTO_ORGCABE_DETA AND CD_CODI_GRUP=CD_CODI_GRUP_RGB AND NU_AUTO_BODE_RGB=NU_AUTO_BODEORG_ENCA And NU_AUTO_ENCA In " & fnStValores(aEnt, 0)
'    Result = LoadMulData("Articulo, GRUP_ARTICULO, IN_R_GRUP_BODE, IN_ENCABEZADO, IN_DETALLE", "'(Movimiento normal del art�culo)', CD_ENTR_RGB,NU_COSTO_DETA * NU_ENTRAD_DETA,'D',CD_CODI_TERC_ENCA,'',0", cCond, aNormCnArt())
'    cCond = "CD_CODI_GRUP = CD_GRUP_ARTI And NU_AUTO_ARTI = NU_AUTO_ARTI_DETA And NU_AUTO_ENCA = NU_AUTO_MODCABE_DETA AND CD_CODI_GRUP=CD_CODI_GRUP_RGB AND NU_AUTO_BODE_RGB=NU_AUTO_BODEORG_ENCA And NU_AUTO_ENCA In " & fnStValores(aEnt, 0) & " Group By CD_ENTR_RGB,CD_CODI_TERC_ENCA"  'REOL M1643 'JLPB T22266 SE DEJA EN COMENTARIO
    cCond = "CD_CODI_GRUP = CD_GRUP_ARTI And NU_AUTO_ARTI = NU_AUTO_ARTI_DETA And NU_AUTO_ENCA = NU_AUTO_MODCABE_DETA AND CD_CODI_GRUP=CD_CODI_GRUP_RGB AND NU_AUTO_BODE_RGB=NU_AUTO_BODEORG_ENCA And NU_AUTO_ENCA In " & fnStValores(aEnt, 0) 'JLPB T22266
'    Result = LoadMulData("Articulo, GRUP_ARTICULO, IN_R_GRUP_BODE, IN_ENCABEZADO, IN_DETALLE", "'(Movimiento normal del art�culo)', CD_ENTR_RGB,SUM(NU_COSTO_DETA * (NU_ENTRAD_DETA - NU_SALIDA_DETA)),'D',CD_CODI_TERC_ENCA,'',0", cCond, aNormCnArt())    'REOL M1643
    'Result = LoadMulData("Articulo, GRUP_ARTICULO, IN_R_GRUP_BODE, IN_ENCABEZADO, IN_DETALLE", "'(Movimiento normal del art�culo)', CD_ENTR_RGB,SUM((Round((100*NU_COSTO_DETA)/(100+NU_IMPU_DETA),2) * (NU_ENTRAD_DETA - NU_SALIDA_DETA)) * (1+NU_IMPU_DETA/100)),'D',CD_CODI_TERC_ENCA,'',0", cCond, aNormCnArt())    'REOL M1799 'HRR R1853
    'HRR R1853
    'HRR M2669
'    Campos = "'(Movimiento normal del art�culo)', CD_ENTR_RGB,"
'    Campos = Campos & "SUM((Round((100*NU_COSTO_DETA)/(100+NU_IMPU_DETA),2) * (NU_ENTRAD_DETA - NU_SALIDA_DETA)) * (1+(NU_IMPU_DETA/100)))," 'costo total con iva 'nu_costo_deta=costo unitario + (costo unitario * % de iva)
'    Campos = Campos & "'D',CD_CODI_TERC_ENCA,'',0"
'    Campos = Campos & Coma & "SUM((Round((100*NU_COSTO_DETA)/(100+NU_IMPU_DETA),2) *(NU_ENTRAD_DETA - NU_SALIDA_DETA)*(NU_IMPU_DETA/100)))" 'costo de iva
    'HRR M2669
    'DAHV M4136 ---------------------------
'    'HRR M2669
'    Campos = "'(Movimiento normal del art�culo)', CD_ENTR_RGB,"
'    'Campos = Campos & "SUM((Round((100*NU_COSTO_DETA)/(100+NU_IMPU_DETA)," & InDec & ") * (NU_ENTRAD_DETA - NU_SALIDA_DETA)) * (1+(NU_IMPU_DETA/100)))," 'costo total con iva 'nu_costo_deta=costo unitario + (costo unitario * % de iva)
'    Campos = Campos & " ROUND(SUM((100*NU_COSTO_DETA)/(100+NU_IMPU_DETA) * (NU_ENTRAD_DETA - NU_SALIDA_DETA)* (1+(NU_IMPU_DETA/100))), " & InDec & ")," 'costo total con iva 'nu_costo_deta=costo unitario + (costo unitario * % de iva) 'DAHV M4133
'    Campos = Campos & "'D',CD_CODI_TERC_ENCA,'',0"
'    'Campos = Campos & Coma & "SUM((Round((100*NU_COSTO_DETA)/(100+NU_IMPU_DETA)," & InDec & ") *(NU_ENTRAD_DETA - NU_SALIDA_DETA)*(NU_IMPU_DETA/100)))" 'costo de iva
'    Campos = Campos & Coma & "SUM((Round((100*NU_COSTO_DETA)/(100+NU_IMPU_DETA)," & InDec & ") * Round((NU_ENTRAD_DETA - NU_SALIDA_DETA)*(NU_IMPU_DETA/100), " & InDec & ")))" 'costo de iva DAHV M4133
'    'HRR M2669
'    'HRR R1853
'    Result = LoadMulData("Articulo, GRUP_ARTICULO, IN_R_GRUP_BODE, IN_ENCABEZADO, IN_DETALLE", Campos, cCond, aNormCnArt())    'REOL M1799 'HRR R1853
    
    
    Campos = "'(Movimiento normal del art�culo)', CD_ENTR_RGB,"
    'Campos = Campos & " ROUND(SUM(ROUND((100*NU_COSTO_DETA)/(100+NU_IMPU_DETA), " & InDec & ") * (NU_ENTRAD_DETA - NU_SALIDA_DETA)* ROUND((1+(NU_IMPU_DETA/100)), " & InDec & " )), " & InDec & ")," 'costo total con iva 'nu_costo_deta=costo unitario + (costo unitario * % de iva) 'DAHV M4133 HRR M4839 Se deja como estaba
    'Campos = Campos & " ROUND(SUM(ROUND((100*NU_COSTO_DETA)/(100+NU_IMPU_DETA), " & InDec & ") * (NU_ENTRAD_DETA - NU_SALIDA_DETA)* ROUND((1+(NU_IMPU_DETA/100)) , " & InDec & " )), " & InDec & ") +  (SUM((Round((100*NU_COSTO_DETA)/(100+NU_IMPU_DETA)," & InDec & ") * Round((NU_ENTRAD_DETA - NU_SALIDA_DETA)*(NU_IMPU_DETA/100), " & InDec & "))))," 'AASV M4839 Se suma el valor del impuesto ya que no lo tenia en cuenta'costo total con iva 'nu_costo_deta=costo unitario + (costo unitario * % de iva) 'DAHV M4133
    'Campos = Campos & " ROUND(SUM(ROUND((100*NU_COSTO_DETA)/(100+NU_IMPU_DETA), " & InDec & ") * (NU_ENTRAD_DETA - NU_SALIDA_DETA)* (1+(NU_IMPU_DETA/100))), " & InDec & ")," 'costo total con iva 'nu_costo_deta=costo unitario + (costo unitario * % de iva) 'DAHV M4133 'AASV M4839 Nota: 26106 Se quita el redondeo de los impuestos ya que esto es lo que esta generando el desbance
    'AASV M5907 Inicio
    'Campos = Campos & " ((SUM(NU_COSTO_DETA)/SUM(NU_ENTRAD_DETA)))* ((SUM(NU_ENTRAD_DETA))-(SUM(NU_SALIDA_DETA)))," 'costo total con iva 'nu_costo_deta=costo unitario + (costo unitario * % de iva) 'DAHV M4133 'AASV M4839 Nota: 26106 Se quita el redondeo de los impuestos ya que esto es lo que esta generando el desbance
    'Campos = Campos & " ((SUM((NU_COSTO_DETA) * NU_ENTRAD_DETA)) - (SUM((NU_COSTO_DETA)*(NU_SALIDA_DETA))))," 'costo total con iva 'nu_costo_deta=costo unitario + (costo unitario * % de iva) 'DAHV M4133 'AASV M4839 Nota: 26106 Se quita el redondeo de los impuestos ya que esto es lo que esta generando el desbance 'OMOG T19943 SE ESTABLECE EN COMENTARIO Y SE INGRESA LA LINEA AL IF SIGUIENTE
    'JLPB T22266 INICIO EL SIGUIENTE BLOQUE SE DEJA EN COMENTARIO
'    'OMOG T19943 INICIO
'    If BoAproxCent = True Then
'       Campos = Campos & " ROUND(((SUM(ROUND((NU_COSTO_DETA)*(NU_ENTRAD_DETA),2)))-(SUM(ROUND((NU_COSTO_DETA)*(NU_SALIDA_DETA),2)))),2)-"
'       Campos = Campos & "ROUND(SUM(ROUND(((100*NU_COSTO_DETA)/(100+NU_IMPU_DETA)), 2)*(NU_ENTRAD_DETA - NU_SALIDA_DETA)*(NU_IMPU_DETA/100)), 2)+"
'       Campos = Campos & "ROUND(SUM(ROUND(((100*NU_COSTO_DETA)/(100+NU_IMPU_DETA)), 2)*(NU_ENTRAD_DETA - NU_SALIDA_DETA)*(NU_IMPU_DETA/100)), -2),"
'    Else
'       Campos = Campos & " ((SUM((NU_COSTO_DETA) * NU_ENTRAD_DETA)) - (SUM((NU_COSTO_DETA)*(NU_SALIDA_DETA))))," 'costo total con iva 'nu_costo_deta=costo unitario + (costo unitario * % de iva) 'DAHV M4133 'AASV M4839 Nota: 26106 Se quita el redondeo de los impuestos ya que esto es lo que esta generando el desbance
'    End If
'    'OMOG T19943 FIN
    'AASV M5907 Fin
'    Campos = Campos & "'D',CD_CODI_TERC_ENCA,'',0"
    'Campos = Campos & Coma & "SUM((Round((100*NU_COSTO_DETA)/(100+NU_IMPU_DETA)," & InDec & ") * Round((NU_ENTRAD_DETA - NU_SALIDA_DETA)*(NU_IMPU_DETA/100), " & InDec & ")))" 'costo de iva DAHV M4133
    'JLPB T22266 INICIO LA DOS SIGUIENTES LIENAS SE DEJAN EN COMENTARIO
    'Campos = Campos & Coma & "Round(SUM(Round(((100*NU_COSTO_DETA)/(100+NU_IMPU_DETA)), " & InDec & ") * (NU_ENTRAD_DETA - NU_SALIDA_DETA)*(NU_IMPU_DETA/100)), " & InDec & ")" 'costo de iva DAHV M4133 'AASV M4839 Nota: 26106 Se quita el redondeo de los impuestos ya que esto es lo que esta generando el desbance
    'Result = LoadMulData("Articulo, GRUP_ARTICULO, IN_R_GRUP_BODE, IN_ENCABEZADO, IN_DETALLE", Campos, cCond, aNormCnArt())    'REOL M1799 'HRR R1853
    Campos = Campos & "CD_CODI_TERC_ENCA,((100*NU_COSTO_DETA)/(100+NU_IMPU_DETA)) AS NU_COST_SIVA,(NU_ENTRAD_DETA-NU_SALIDA_DETA),NU_IMPU_DETA"
    cCond = cCond & " ORDER BY 2,3" 'JLPB T35969
    Result = LoadMulData("Articulo, GRUP_ARTICULO, IN_R_GRUP_BODE, IN_ENCABEZADO, IN_DETALLE", Campos, cCond, VrArr())
    If Result <> FAIL And Encontro Then
       For I& = 0 To UBound(VrArr, 2)
          VrArr(3, I&) = Aplicacion.Formatear_Valor(CDbl(VrArr(3, I&)))
          'DbIVA = DbIVA + (((VrArr(3, I&) * VrArr(4, I&)) * VrArr(5, I&)) / 100) 'JLPB T35969 Se deja en comentario
          DbIva = (((VrArr(3, I&) * VrArr(4, I&)) * VrArr(5, I&)) / 100) 'JLPB T35969
          If BoAproxCent Then DbIva = AproxCentena(Round(DbIva)) Else DbIva = Aplicacion.Formatear_Valor(DbIva)
          'JLPB T35969 INICIO Se deja en comentario
          'DbTotal = DbTotal + (VrArr(3, I&) * VrArr(4, I&))
          DbTotal = 0
          DbTotal = (VrArr(3, I&) * VrArr(4, I&))
          DbTotal = DbTotal + DbIva
          If I& = 0 Then
             aNormCnArt(0, I&) = VrArr(0, I&)
             aNormCnArt(1, I&) = VrArr(1, I&)
             aNormCnArt(2, I&) = DbTotal
             aNormCnArt(3, I&) = "D"
             aNormCnArt(4, I&) = VrArr(2, I&)
             aNormCnArt(5, I&) = ""
             aNormCnArt(6, I&) = 0
             aNormCnArt(7, I&) = DbIva
          Else
             For InI = 0 To I&
                If VrArr(1, InI) = VrArr(1, I&) Then
                   Exit For
                End If
             Next
             If InI < I& Then
                aNormCnArt(2, InI) = aNormCnArt(2, InI) + DbTotal
                aNormCnArt(7, InI) = aNormCnArt(7, InI) + DbIva
             Else
                ReDim Preserve aNormCnArt(7, I&)
                aNormCnArt(0, I&) = VrArr(0, I&)
                aNormCnArt(1, I&) = VrArr(1, I&)
                aNormCnArt(2, I&) = DbTotal
                aNormCnArt(3, I&) = "D"
                aNormCnArt(4, I&) = VrArr(2, I&)
                aNormCnArt(5, I&) = ""
                aNormCnArt(6, I&) = 0
                aNormCnArt(7, I&) = DbIva
             End If
          End If
          'JLPB T35969 FIN
       Next
       'JLPB T35969 INICIO Se deja en comentario las siguientes lineas
       'DbTotal = DbTotal + DbIVA
       'For I& = 0 To 0
       '   aNormCnArt(0, I&) = VrArr(0, I&)
       '   aNormCnArt(1, I&) = VrArr(1, I&)
       '   aNormCnArt(2, I&) = DbTotal
       '   aNormCnArt(3, I&) = "D"
       '   aNormCnArt(4, I&) = VrArr(2, I&)
       '   aNormCnArt(5, I&) = ""
       '   aNormCnArt(6, I&) = 0
       '   aNormCnArt(7, I&) = DbIVA
       'Next
       'JLPB T35969 FIN
    End If
    'JLPB T22266 FIN
    'DAHV M4136 -----------------------------

    
    'AASV M5907 Inicio
    'Result = LoadMulData("MOV_CONTABLE, IN_ENCABEZADO", "'(Movimiento transitorio de CxP)',CD_CUEN_MOVC,VL_VALO_MOVC,'D',CD_TERC_MOVC,CD_CECO_MOVC,VL_BASE_MOVC", cCond, aTranCnCxP())
    'cCond = "NU_COMP_CONTA_ENCA = NU_CONS_MOVC And ID_NATU_MOVC='C' And CD_CONC_MOVC='ENTR' And NU_AUTO_ENCA In " & fnStValores(aEnt, 0)
    cCond = " ID_NATU_MOVC='C' And CD_CONC_MOVC='ENTR' And NU_AUTO_ENCA In " & fnStValores(aEnt, 0)
    Campos = "'(Movimiento transitorio de CxP)',CD_CUEN_MOVC,"
    'Campos = Campos & " (VL_VALO_MOVC - ISNULL( DEVOLUCION.VALDEV,0))"
    Campos = Campos & " SUM(VL_VALO_MOVC) - ISNULL( DEVOLUCION.VALDEV,0)" 'APGR T9087
    Campos = Campos & ",'D',CD_TERC_MOVC,CD_CECO_MOVC,VL_BASE_MOVC"
    Desde = " MOV_CONTABLE INNER JOIN IN_ENCABEZADO ON MOV_CONTABLE.NU_CONS_MOVC = IN_ENCABEZADO.NU_COMP_CONTA_ENCA LEFT OUTER JOIN "
    cCond = cCond & " GROUP BY CD_CUEN_MOVC, CD_TERC_MOVC,CD_CECO_MOVC,VL_BASE_MOVC,DEVOLUCION.VALDEV" 'APGR T9087
    Result = LoadMulData(Desde & StSubConsultaD, Campos, cCond, aTranCnCxP())
    'AASV M5907 Fin
    'Result = LoadMulData("MOV_CONTABLE, IN_ENCABEZADO", "Top 1 '(Movimiento normal de CxP)'," & fnDevDato("PARAMETROS_INVE", "CD_CXP_APLI") & ",VL_VALO_MOVC,'C',CD_TERC_MOVC,CD_CECO_MOVC,VL_BASE_MOVC", cCond, aNormCnCxP()) 'SMDL M1683 SE ELIMA COMA QUE SE ENCONTRABA EN UN CAMPO.
    'Result = LoadMulData("MOV_CONTABLE, IN_ENCABEZADO", "Top 1 '(Movimiento normal de CxP)'," & IIf(stCuenta <> NUL$, stCuenta, "' '") & ",VL_VALO_MOVC,'C',CD_TERC_MOVC,CD_CECO_MOVC,VL_BASE_MOVC", cCond, aNormCnCxP()) 'SMDL M1683,m1717 se cambia por iif stcuenta 'NYCM M2769
    'Result = LoadMulData("MOV_CONTABLE, IN_ENCABEZADO", "'(Movimiento normal de CxP)'," & IIf(stCuenta <> NUL$, stCuenta, "' '") & ",VL_VALO_MOVC,'C',CD_TERC_MOVC,CD_CECO_MOVC,VL_BASE_MOVC", cCond, aNormCnCxP()) 'NYCM M2769
    
    '----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
      'DAHV M4133 - Inicio
       
       'Campos = "'(Movimiento normal de CxP)'," & IIf(stCuenta <> NUL$, stCuenta, "' '") & ","
       'Campos = "'(Movimiento normal de CxP)'," & Comi & IIf(stCuenta <> NUL$, stCuenta, "' '") & Comi & ","
       Campos = "'(Movimiento normal de CxP)'," & Comi & IIf(stCuenta <> NUL$, stCuenta & Comi, "  '") & "," 'LDCR T5612
       'AASV M5907 Inicio
       'Campos = Campos & " ROUND(VL_VALO_MOVC, " & InDec & ") AS VL_VALO_MOVC, "
       'Campos = Campos & " ROUND((VL_VALO_MOVC - ISNULL( DEVOLUCION.VALDEV,0)), " & InDec & ") AS VL_VALO_MOVC, "
       Campos = Campos & " ROUND(SUM(VL_VALO_MOVC) - ISNULL( DEVOLUCION.VALDEV,0), 2)  AS VL_VALO_MOVC, " 'APGR T9087
       'AASV M5907 Fin
       Campos = Campos & " 'C',CD_TERC_MOVC,CD_CECO_MOVC,VL_BASE_MOVC"
                     
       'Result = LoadMulData("MOV_CONTABLE, IN_ENCABEZADO ", Campos, cCond, aNormCnCxP())
       'Result = LoadMulData("MOV_CONTABLE, IN_ENCABEZADO ," & StSubConsultaD, Campos, cCond, aNormCnCxP())
       Result = LoadMulData(Desde & StSubConsultaD, Campos, cCond, aNormCnCxP())
      'DAHV M4133 - Fin
       
    '----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    
End If
'-- Estableciendo los movimientos contables "contra" transitorios y normales.

'-- Verificando si las facturas tienen diferentes n�meros:
If Not fTransit Then
        
    Dim cTest As String
    
    With flexEntradas
            cTest = .TextMatrix(1, 5)
        For I& = 2 To .Rows - 1
            If cTest <> .TextMatrix(I&, 5) Then
                Mensaje1 "Las entradas tienen diferentes n�meros de factura, verifique el n�mero de factura que se asignar� a la compra.", 2
            End If
        Next I&
    End With
End If
'-- Verificando si las facturas tienen diferentes n�meros.


'Reiniciar matrices con los detalles de los impuestos:
ReDim arrSerTDes(t%), arrSerTImp(t%), arrSerTCost(t%), arrSerTBase(t%)
ReDim arrSerCD�b(t%), arrSerCCr�(t%) 'Cuentas d�bito y cr�dito.
ReDim arrSerIIca(0), arrSerIRIva(0), arrSerIReten(0), arrSerIOtros(0)

ReDim arrSerDescP(0), arrSerDescV(0), arrSerImpP(0), arrSerImpV(0)
ReDim arrSerIDN�m(0), arrSerCode(0), arrSerTope(0)
ReDim arrSerCuen(0), arrSerTipo(0), arrSerVFinal(0)

ReDim arrZerDescP(0), arrZerDescV(0), arrZerImpP(0), arrZerImpV(0)
ReDim arrZerIDN�m(0), arrZerCode(0), arrZerTope(0)
ReDim arrZerCuen(0), arrZerTipo(0), arrZerVFinal(0)

ReDim arrDescP(0), arrDescV(0), arrImpP(0)  'Informaci�n de impuestos al total de la compra
ReDim arrCode(0), arrIDN�m(0), arrTope(0)  'Informaci�n de impuestos al total de la compra

'-- Verificando si existen impuestos aplicados previamente |.DR.|, R:1033-1651
If Not fTransit And boICxP Then
    Dim xArr(), J&, k&

    'ReDim xArr(3, 0) 'JLPB T15675 SE DEJA EN COMENTARIO
    ReDim xArr(6, 0) 'JLPB T15675
 
    With flexEntradas
        ReDim rTImDe(.Rows - 1) '(Indice 0 contiene suma de los dem�s!)
        'JAUM T28370-R22535 Inicio
        If .Rows - 1 <> 1 Then
           ReDim DbTDCom(.Rows - 1)
           ReDim DbTImp(.Rows - 1)
        End If
        'JAUM T28370-R22535 Fin
        For I& = 1 To .Rows - 1
           DbTDescu = 0 'JAUM T28836
            'NYCM M2940----------------------
            Condicion = "NU_AUTO_ENCA_ENID=" & .TextMatrix(I&, 1)
            Condicion = Condicion & " AND NU_AUTO_ENCAREAL_ENID IN " & fnStValores(aEnt, 0)
            
            'Result = LoadMulData("IN_R_ENCA_IMDE", "CD_CODI_IMPU_ENID,CD_CODI_DESC_ENID,NU_VALO_ENID,NU_PRAPLI_ENID", "NU_AUTO_ENCA_ENID=" & .TextMatrix(i&, 1), xArr())
            'JLPB T15675 INICIO LA SIGUIENTE LINEA SE DEJA EN COMENTARIO
            'Result = LoadMulData("IN_R_ENCA_IMDE", "CD_CODI_IMPU_ENID,CD_CODI_DESC_ENID,NU_VALO_ENID,NU_PRAPLI_ENID", Condicion, xArr())
            Desde = "IN_R_ENCA_IMDE,TC_IMPUESTOS,DESCUENTO"
            Campos = "DISTINCT CD_CODI_IMPU_ENID,CD_CODI_DESC_ENID,NU_VALO_ENID,NU_PRAPLI_ENID,"
            Campos = Campos & "CASE WHEN CD_CODI_IMPU_ENID IS NULL THEN VL_TOPE_DESC ELSE VL_TOPE_IMPU END AS VL_TOPE,"
            Campos = Campos & "CASE WHEN CD_CODI_IMPU_ENID IS NULL THEN VL_VALO_DESC ELSE 0 END AS VL_DESC,"
            Campos = Campos & "CASE WHEN CD_CODI_IMPU_ENID IS NULL THEN NULL ELSE ID_TIPO_IMPU END AS ID_TIPO_IMPU "
            Condicion = Condicion & "AND (CD_CODI_DESC=CD_CODI_DESC_ENID OR CD_CODI_IMPU=CD_CODI_IMPU_ENID)"
            Result = LoadMulData(Desde, Campos, Condicion, xArr())
            'JLPB T15675 FIN
            '/-------------------------------
            rTImDe(I) = 0 'Se calcular� el total de impuestos aplicados sobre la entrada
            
            If Result <> FAIL And Encontro Then
               For J& = 0 To UBound(xArr, 2)
                  If UBound(arrZerTope) < k& Then ReDim Preserve arrZerTope(k&), arrZerCode(k&), arrZerDescP(k&), arrZerDescV(k&), arrZerImpP(k&), arrZerImpV(k&), arrZerIDN�m(k&), arrZerCuen(k&), arrZerTipo(k&), arrZerVFinal(k&)
                  arrZerDescP(k&) = Empty: arrZerDescV(k&) = Empty: arrZerImpP(k&) = Empty: arrZerCuen(k&) = Empty: arrZerTipo(k&) = Empty
                  If xArr(0, J&) <> "" Then 'Impuesto
                     If xArr(4, J&) < (.TextMatrix(I&, 4) - .TextMatrix(I&, 7)) Then
                        arrZerCode(k&) = xArr(0, J&)
'                       arrZerImpV(k&) = xArr(2, j&)   'REOL M2237
                        'JLPB T15675 INICIO SE DEJA EN COMENTARIO LA SIGUIENTE LINEA
                        'arrZerImpV(k&) = Aplicacion.Formatear_Valor(xArr(2, J&))
                        If xArr(6, J&) <> "M" Then
                           'arrZerImpV(k&) = Aplicacion.Formatear_Valor(((.TextMatrix(I&, 4) - .TextMatrix(I&, 7)) * xArr(3, J&)) / 100) 'JAUM T28836 Se deja linea en comentario
                           arrZerImpV(k&) = Aplicacion.Formatear_Valor(((.TextMatrix(I&, 4) - .TextMatrix(I&, 7) - DbTDescu) * xArr(3, J&)) / 100) 'JAUM T2883io
                        Else
                           arrZerImpV(k&) = Aplicacion.Formatear_Valor((.TextMatrix(I&, 7) * xArr(3, J&)) / 100)
                        End If
                        If BoAproxCent Then arrZerImpV(k&) = AproxCentena(Round(arrZerImpV(k&)))
                        'JLPB T15675 FIN
                        arrZerImpP(k&) = xArr(3, J&)
                        rTImDe(I) = rTImDe(I) + arrZerImpV(k&)
                        'JAUM T28370-R22535 Inicio
                        If .Rows - 1 <> 1 Then
                           DbTImp(I) = DbTImp(I) + arrZerImpV(k&)
                        End If
                        'JAUM T28370-R22535 Fin
                        k& = k& + 1
                     End If
                  Else 'Descuento
                     If xArr(4, J&) < (.TextMatrix(I&, 4) - .TextMatrix(I&, 7)) Then
                        arrZerCode(k&) = xArr(1, J&)
'                       arrZerDescV(k&) = xArr(2, j&)  'REOL M2237
                        'JLPB T15675 INICIO SE DEJA EN COMENTARIO LA SIGUIENTE LINEA
                        'arrZerDescV(k&) = Aplicacion.Formatear_Valor(xArr(2, J&))
                        If xArr(3, J&) <> 0 Then
                           arrZerDescV(k&) = Aplicacion.Formatear_Valor(((.TextMatrix(I&, 4) - .TextMatrix(I&, 7)) * xArr(3, J&)) / 100)
                        Else
                           arrZerDescV(k&) = xArr(5, J&)
                        End If
                        If BoAproxCent Then arrZerDescV(k&) = AproxCentena(Round(arrZerDescV(k&)))
                        'JLPB T15675 FIN
                        arrZerDescP(k&) = xArr(3, J&)
                        rTImDe(I) = rTImDe(I) + arrZerDescV(k&)
                        DbTDescu = DbTDescu + arrZerDescV(k&) 'JAUM T28836
                        'JAUM T28370-R22535 Inicio
                        If .Rows - 1 <> 1 Then
                           DbTDCom(I) = DbTDCom(I) + arrZerDescV(k&)
                        End If
                        'JAUM T28370-R22535 Fin
                        k& = k& + 1
                     End If
                  End If
               Next J&
            End If
            
            'LJSA M4502 ------------ Consulta valores de los movimientos CxP y Articulos ---------------Nota 20091
            
            'stCuenta = fnDevDato("PARAMETROS_INVE", "CD_CXP_APLI") 'smdl M1717
            'ReDim aNormCnArt(7, 0), aNormCnCxP(6, 0)
            '
            'cCond = "CD_CODI_GRUP = CD_GRUP_ARTI And NU_AUTO_ARTI = NU_AUTO_ARTI_DETA And NU_AUTO_ENCA = NU_AUTO_MODCABE_DETA AND CD_CODI_GRUP=CD_CODI_GRUP_RGB AND NU_AUTO_BODE_RGB=NU_AUTO_BODEORG_ENCA And NU_AUTO_ENCA In " & fnStValores(aEnt, 0) & " Group By CD_ENTR_RGB,CD_CODI_TERC_ENCA"  'REOL M1643
            'Campos = "'(Movimiento normal del art�culo)', CD_ENTR_RGB,"
            'Campos = Campos & " ROUND(SUM((100*NU_COSTO_DETA)/(100+NU_IMPU_DETA) * (NU_ENTRAD_DETA - NU_SALIDA_DETA)* (1+(NU_IMPU_DETA/100))), " & InDec & ")," 'costo total con iva 'nu_costo_deta=costo unitario + (costo unitario * % de iva) 'DAHV M4133
            'Campos = Campos & "'D',CD_CODI_TERC_ENCA,'',0"
            'Campos = Campos & Coma & "SUM((Round((100*NU_COSTO_DETA)/(100+NU_IMPU_DETA)," & InDec & ") * Round((NU_ENTRAD_DETA - NU_SALIDA_DETA)*(NU_IMPU_DETA/100), " & InDec & ")))" 'costo de iva DAHV M4133
            '
            'Result = LoadMulData("Articulo, GRUP_ARTICULO, IN_R_GRUP_BODE, IN_ENCABEZADO, IN_DETALLE", Campos, cCond, aNormCnArt())
            '
            '
            'cCond = "NU_COMP_CONTA_ENCA = NU_CONS_MOVC And ID_NATU_MOVC='C' And CD_CONC_MOVC='ENTR' And NU_AUTO_ENCA In " & fnStValores(aEnt, 0)
            'Campos = "'(Movimiento normal de CxP)'," & Comi & IIf(stCuenta <> NUL$, stCuenta, "' '") & Comi & ","
            'Campos = Campos & " ROUND(VL_VALO_MOVC, " & InDec & ") AS VL_VALO_MOVC, "
            'Campos = Campos & " 'C',CD_TERC_MOVC,CD_CECO_MOVC,VL_BASE_MOVC"
            '
            'Result = LoadMulData("MOV_CONTABLE, IN_ENCABEZADO", Campos, cCond, aNormCnCxP())
       
            'LJSA M4502 ------------ Consulta valores de los movimientos CxP y Articulos ---------------Nota 20091
            
                        rTImDe(0) = rTImDe(0) + rTImDe(I)
                'AA Estableciendo el nuevo valor de la entrada despu�s de la aplicaci�n de impuestos heredados.
'                    .TextMatrix(I&, 4) = val(.TextMatrix(I&, 4)) - rTImDe(I)   'REOL M2237
                    .TextMatrix(I&, 4) = Aplicacion.Formatear_Valor(Val(.TextMatrix(I&, 4))) - rTImDe(I)
                    If rTImDe(I) <> 0 Then sbGridColor flexEntradas, RGB(230, 230, 255), I&
                'AA Fin.
        
        Next I&
    End With

End If
'-- Verificando si existen impuestos aplicados previamente |.DR.|, R:1033-1651

'00 Cargar c�digos de las cuentas:
cmdXLS.Visible = boICont And fExtra
'cmdPPTO.Left = 6960 + (360 * Abs(boICont))
'cmdPPTO.Visible = boIPPTO
'If boICont Then
'    h.sbW "Verificando cuentas contables de las entradas...", 5
'With flexEntradas
'    For I& = 1 To .Rows - 1
'        arrSerCD�b(I& - 1) = fnDevDato(stVistCxS, "NU_CUDB_GRCE", "CD_CODI_SERI='" & .TextMatrix(I&, 2) & "' And CD_ARTI_SERI='" & .TextMatrix(I&, 8) & "' And CD_CECO_SERI='" & .TextMatrix(I&, 9) & "'", True)
'        arrSerCCr�(I& - 1) = fnDevDato(stVistCxS, "NU_CUCR_GRCE", "CD_CODI_SERI='" & .TextMatrix(I&, 2) & "' And CD_ARTI_SERI='" & .TextMatrix(I&, 8) & "' And CD_CECO_SERI='" & .TextMatrix(I&, 9) & "'", True)
'        If Trim(arrSerCD�b(I& - 1)) = "" Or Trim(arrSerCCr�(I& - 1)) = "" Then 'Verificar si las cuentas estan parametrizadas.
'            .Col = 0: .Row = I&
'            Set .CellPicture = imgLib(2).Picture
'            .CellPictureAlignment = 3
'            arrSerCD�b(I& - 1) = Empty: arrSerCCr�(I& - 1) = Empty
'        End If
'    Next I&
'End With
'    h.Hide
'End If
    btModo = icCreando
'00 Fin.
    txtN�mFact = flexEntradas.TextMatrix(1, 5) 'N�mero de factura predeterminado
    
    
    'sbCalcImpSer 'HRR R1853
    'HRR R1853
    'implementado inicialmente por HRR R1853
    'Se aplico impuestos descuentos a la entrada
'    If arrZerCode(0) <> Null Or Not IsEmpty(arrZerCode(0)) Then
'      sbCalcImpSer
'      BoEntImpDesc = True
'      cmdImpuestos.Enabled = False
'      cmdClearImp.Enabled = False
'    Else
'      BoEntImpDesc = False
'      If boICxP Then
'         cmdImpuestos.Enabled = True
'         cmdClearImp.Enabled = True
'      Else
'         cmdImpuestos.Enabled = False 'no se habilita x el concepto de inv esta en cxp
'      End If
     'HASTA AQUI HRR R1853
'    End If
     'HRR M2550
     InCantEntra = flexEntradas.Rows - 1 'JAUM T28644-R27752
     If boICxP Then
        cmdImpuestos.Enabled = True
        cmdClearImp.Enabled = True
     Else
        cmdImpuestos.Enabled = False
        cmdClearImp.Enabled = False
     End If
     If Aplicacion.PinvImpComp Then
         BoEntImpDesc = False
     Else
        If fTransit Then
           BoEntImpDesc = False
        Else 'Tiene factura
           cmdImpuestos.Enabled = False
           cmdClearImp.Enabled = False
           If arrZerCode(0) <> Null Or Not IsEmpty(arrZerCode(0)) Then
               BoEntImpDesc = True
           Else
               BoEntImpDesc = False
           End If
        End If
     End If
     'HRR M2550
     'HRR R1853
   
    If fTransit And Not boICont Then lblN.Visible = True: sbVFlash 0, False, lblN
    fraFechas.Visible = False
   'HRR R1853
   'HRR M2550
   If cmdImpuestos.Enabled = False Then
      ReDim ArImp(6, 0)
      ReDim ArDesc(6, 0)
      Call ClssImpuDesc.Descuentos(ArDesc)
      Call ClssImpuDesc.Impuestos(ArImp)
      ClssImpuDesc.TieneDatos = False
      ClssImpuDesc.concepto = "CINV"
      Call sbCalcImpSer
   Else
      'HRR M2644
       Call ClssImpuDesc.Buscar_Descu
       Call ClssImpuDesc.Buscar_Impu
      'HRR M2644
      'JAUM T28370-R22535 Inicio
      With flexEntradas
         If .Rows - 1 <> 1 Then
            Call Calcular_Mul
         Else
      'JAUM T28370-R22535 Fin
            Call Calcular
         End If 'JAUM T28370-R22535
      End With 'JAUM T28370-R22535
   End If
   'HRR M2550
   'HRR R1853
End Sub


'HRR R1853
Sub Calcular() 'Realiza el c�lculo completo.
         Dim DbInicial As Double
         Dim DbIvaEnt As Double
         Dim DbimpDesc As Double
         Dim DbTDCom As Double
         Dim DbTDCond As Double
         Dim DbFinal As Double
         Dim DbTImp As Double
         Dim DbTIIca As Double
         Dim DbTIRet As Double
         Dim DbTIRIva As Double
         
         Dim InJ As Integer
         Dim InI As Integer
         Dim InX As Integer 'JAUM T28644-R27752 Contador
         InJ = 0
         DbInicial = 0
         DbIvaEnt = 0
         DbTDCom = 0
         DbTDCond = 0
         DbTImp = 0
         DbTIIca = 0
         DbTIRet = 0
         DbTIRIva = 0
         ReDim ArDescComp(6, 0)
         ReDim ArImpComp(6, 0)
        'Calcular los valores de la grilla.
        With flexEntradas
            
            For InI = 1 To .Rows - 1
                If fnRowSel(CLng(InI)) Then
                    DbInicial = DbInicial + Val(.TextMatrix(InI, 4))
                    DbIvaEnt = DbIvaEnt + Aplicacion.Formatear_Valor(Val(.TextMatrix(InI, 7)))
                End If
            Next
            'JLPB T22266 INICIO LAS DOS SIGUIENTES LINEAS SE DEJA EN COMENTARIO
            'dbVlNeto = DbInicial - DbIvaEnt
            'DbVlrIvaEnt = DbIvaEnt
            dbVlNeto = DbInicial - IIf(BoAproxCent, AproxCentena(Round(DbIvaEnt)), DbIvaEnt)
            DbVlrIvaEnt = IIf(BoAproxCent, AproxCentena(Round(DbIvaEnt)), DbIvaEnt)
            'JLPB T22266 FIN
            sbApForm
            
            
            For InI = 0 To ClssImpuDesc.SizeDesc
                                                
               
               If Val(ClssImpuDesc.ElemDesc(InI, 3)) <= Val(dbVlNeto) Then 'Se aplica el "tope", no se aplica si el valor no tiene el m�nimo requerido.
                  DbimpDesc = 0
                  If Val(ClssImpuDesc.ElemDesc(InI, 2)) = 0 Then 'Valor
                     If ClssImpuDesc.ElemDesc(InI, 4) = NUL$ Then Call ClssImpuDesc.SetElemDesc(InI, 4, 0)
                     'DbImpDesc = Aplicacion.Formatear_Valor(ClssImpuDesc.ElemDesc(Ini, 4))
                     
                     'INICIO GAVL T6265
                     If BoAproxCent = True Then
                       DbimpDesc = AproxCentena(Round(Aplicacion.Formatear_Valor(ClssImpuDesc.ElemDesc(InI, 4))))
                     Else
                       DbimpDesc = Aplicacion.Formatear_Valor(ClssImpuDesc.ElemDesc(InI, 4))
                     End If
                     'FIN  GAVL T6265
                     
                  Else 'Porcentaje
                     'DbImpDesc = Aplicacion.Formatear_Valor(Val(dbVlNeto * ClssImpuDesc.ElemDesc(Ini, 2)) / 100) 'REOL M1709
                     
                     'INICIO GAVL T6265
                     If BoAproxCent = True Then
                       DbimpDesc = AproxCentena(Round(Aplicacion.Formatear_Valor(Val(dbVlNeto * ClssImpuDesc.ElemDesc(InI, 2)) / 100)))
                     Else
                       DbimpDesc = Aplicacion.Formatear_Valor(Val(dbVlNeto * ClssImpuDesc.ElemDesc(InI, 2)) / 100)
                     End If
                     'FIN  GAVL T6265
                     
                  End If
                  If Val(ClssImpuDesc.ElemDesc(InI, 6)) <> 1 Then   'descuento comercial
                     DbTDCom = DbTDCom + DbimpDesc
                  Else 'descuento condicional
                     DbTDCond = DbTDCond + DbimpDesc
                  End If
                  If InJ <> 0 Then
                     ReDim Preserve ArDescComp(6, InJ)
                  End If
                  ArDescComp(0, InJ) = ClssImpuDesc.ElemDesc(InI, 0)
                  ArDescComp(1, InJ) = ClssImpuDesc.ElemDesc(InI, 1)
                  ArDescComp(2, InJ) = ClssImpuDesc.ElemDesc(InI, 2)
                  ArDescComp(3, InJ) = ClssImpuDesc.ElemDesc(InI, 3)
                  ArDescComp(4, InJ) = DbimpDesc 'ArDescConc(4, InI)
                  ArDescComp(5, InJ) = ClssImpuDesc.ElemDesc(InI, 5)
                  ArDescComp(6, InJ) = ClssImpuDesc.ElemDesc(InI, 6)
                  InJ = InJ + 1
               End If
               
            Next
                                 
             
            DbFinal = DbInicial - DbTDCom - DbTDCond
            
            Call ClssImpuDesc.Descuentos(ArDescComp)
            InJ = 0
            'impuestos:
            For InI = 0 To ClssImpuDesc.SizeImp
               DbimpDesc = 0
               If ClssImpuDesc.ElemImp(InI, 6) = "C" Then
                  If ClssImpuDesc.ElemImp(InI, 3) < dbVlNeto Then
                     'DbImpDesc = Aplicacion.Formatear_Valor(Val((dbVlNeto - DbTDCom) * ClssImpuDesc.ElemImp(Ini, 2)) / 100)
                     
                     'INICIO GAVL T6265
                     If BoAproxCent = True Then
                       DbimpDesc = AproxCentena(Round(Aplicacion.Formatear_Valor(Val((dbVlNeto - DbTDCom) * ClssImpuDesc.ElemImp(InI, 2)) / 100)))
                     Else
                       DbimpDesc = Aplicacion.Formatear_Valor(Val((dbVlNeto - DbTDCom) * ClssImpuDesc.ElemImp(InI, 2)) / 100)
                     End If
                     'FIN  GAVL T6265
                     
                     DbTIIca = DbTIIca + DbimpDesc
                  End If
               ElseIf ClssImpuDesc.ElemImp(InI, 6) = "M" Then
                  If ClssImpuDesc.ElemImp(InI, 3) < DbIvaEnt Then
                     'DbImpDesc = Aplicacion.Formatear_Valor((DbIvaEnt * ClssImpuDesc.ElemImp(Ini, 2)) / 100)
                     
                     'INICIO GAVL T6265
                     If BoAproxCent = True Then
                        'JLPB T21873 INICIO LA SIGUIENTE LINEA SE DEJA EN COMENTARIO
                        'DbimpDesc = AproxCentena(Round(DbimpDesc = Aplicacion.Formatear_Valor((DbIvaEnt * ClssImpuDesc.ElemImp(Ini, 2)) / 100)))
                        DbimpDesc = AproxCentena(Round(Aplicacion.Formatear_Valor((DbIvaEnt * ClssImpuDesc.ElemImp(InI, 2)) / 100)))
                        'JLPB T21873 FIN LA SIGUIENTE LINEA SE DEJA EN COMENTARIO
                     Else
                        DbimpDesc = Aplicacion.Formatear_Valor((DbIvaEnt * ClssImpuDesc.ElemImp(InI, 2)) / 100)
                     End If
                     'FIN  GAVL T6265
                     
                     DbTIRIva = DbTIRIva + DbimpDesc
                  End If
               ElseIf ClssImpuDesc.ElemImp(InI, 6) = "R" Then
                  If ClssImpuDesc.ElemImp(InI, 3) < dbVlNeto Then
                  
                     'DbImpDesc = Aplicacion.Formatear_Valor(Val((dbVlNeto - DbTDCom) * ClssImpuDesc.ElemImp(Ini, 2)) / 100)
                     ''JACC R2345
                     If Buscar_desc_impuesto(ClssImpuDesc.ElemImp(InI, 0)) <> NUL$ And TipoPers = "0" Then
                        'DbImpDesc = Aplicacion.Formatear_Valor(Val((dbVlNeto - DbTDCom - Valor_Descuento(Buscar_desc_impuesto(ClssImpuDesc.ElemImp(Ini, 0)), (dbVlNeto - DbTDCom))) * ClssImpuDesc.ElemImp(Ini, 2)) / 100)
                        
                        'INICIO GAVL T6265
                        If BoAproxCent = True Then
                          DbimpDesc = AproxCentena(Round(Aplicacion.Formatear_Valor(Val((dbVlNeto - DbTDCom - Valor_Descuento(Buscar_desc_impuesto(ClssImpuDesc.ElemImp(InI, 0)), (dbVlNeto - DbTDCom))) * ClssImpuDesc.ElemImp(InI, 2)) / 100)))
                        Else
                           DbimpDesc = Aplicacion.Formatear_Valor(Val((dbVlNeto - DbTDCom - Valor_Descuento(Buscar_desc_impuesto(ClssImpuDesc.ElemImp(InI, 0)), (dbVlNeto - DbTDCom))) * ClssImpuDesc.ElemImp(InI, 2)) / 100)
                        End If
                        'FIN  GAVL T6265
                        
                        
                     Else
                        'DbImpDesc = Aplicacion.Formatear_Valor(Val((dbVlNeto - DbTDCom) * ClssImpuDesc.ElemImp(Ini, 2)) / 100)
                        'JAUM T28644-R27752 Inicio
                        If fnDevDato("TC_IMPUESTOS", "TX_RETEFU_IMPU", "CD_CODI_IMPU ='" & ClssImpuDesc.ElemImp(InI, 0) & Comi, True) = True Then
                           For InX = 0 To UBound(VrVBase, 2)
                              If ClssImpuDesc.ElemImp(InI, 0) = VrVBase(0, InX) Then
                                 DbimpDesc = VrVBase(1, InX)
                                 Exit For
                              End If
                           Next
                           If BoAproxCent = True Then
                              DbimpDesc = AproxCentena(Round(Aplicacion.Formatear_Valor(Val(DbimpDesc))))
                           Else
                              DbimpDesc = Aplicacion.Formatear_Valor(Val(DbimpDesc))
                           End If
                        Else
                        'JAUM T28644-R27752 Fin
                           'INICIO GAVL T6265
                           If BoAproxCent = True Then
                              DbimpDesc = AproxCentena(Round(Aplicacion.Formatear_Valor(Val((dbVlNeto - DbTDCom) * ClssImpuDesc.ElemImp(InI, 2)) / 100)))
                           Else
                              DbimpDesc = Aplicacion.Formatear_Valor(Val((dbVlNeto - DbTDCom) * ClssImpuDesc.ElemImp(InI, 2)) / 100)
                           End If
                           'FIN  GAVL T6265
                        End If 'JAUM T28644-R27752
                     End If
                     'JACC R2345
                     DbTIRet = DbTIRet + DbimpDesc
                  End If
               End If
               DbTImp = DbTImp + DbimpDesc
               If InJ <> 0 Then
                  ReDim Preserve ArImpComp(6, InJ)
               End If
               ArImpComp(0, InJ) = ClssImpuDesc.ElemImp(InI, 0)
               ArImpComp(1, InJ) = ClssImpuDesc.ElemImp(InI, 1)
               ArImpComp(2, InJ) = ClssImpuDesc.ElemImp(InI, 2)
               ArImpComp(3, InJ) = ClssImpuDesc.ElemImp(InI, 3)
               ArImpComp(4, InJ) = DbimpDesc
               ArImpComp(5, InJ) = ClssImpuDesc.ElemImp(InI, 5)
               ArImpComp(6, InJ) = ClssImpuDesc.ElemImp(InI, 6)
               InJ = InJ + 1
            Next

            DbFinal = DbFinal - DbTImp
                                  
            Call ClssImpuDesc.Impuestos(ArImpComp)
            'JAUM T28370-R22535 Inicio
            If boIPPTO Then
               '.TextMatrix(1, 8) = Format(Val(-DbTDCom + -DbTImp), stMFormat) JAUM T29815 Se deja linea en comentario
               .TextMatrix(1, 8) = Format(Val(-(DbTDCom + DbTDCond) + -DbTImp), stMFormat) 'JAUM T29815
            End If
            'JAUM T28370-R22535 Fin
        End With
            lblTotalB = Aplicacion.Formatear_Valor(DbInicial):    lblTotalB.Tag = DbInicial
            lblImp = Aplicacion.Formatear_Valor(-DbTImp):   lblImp.Tag = DbTImp
            lblDesc = Aplicacion.Formatear_Valor(-(DbTDCom + DbTDCond)):    lblDesc.Tag = (DbTDCom + DbTDCond)
            lblTotal = Aplicacion.Formatear_Valor(DbFinal):    lblTotal.Tag = DbFinal
            lblTIIca = Aplicacion.Formatear_Valor(-DbTIIca):    lblTIIca.Tag = DbTIIca
            lblTIRet = Aplicacion.Formatear_Valor(-DbTIRet):    lblTIRet.Tag = DbTIRet
            lblTIRIVA = Aplicacion.Formatear_Valor(-DbTIRIva):  lblTIRIVA.Tag = DbTIRIva
            lblTIOtros = Aplicacion.Formatear_Valor(0):    lblTIOtros.Tag = 0
         
         
        Dim dbCompIni As Double, dbCompFin As Double
        Dim dbCTDesc As Double, dbCTImp As Double
        
        '02 Calcular impuestos aplicados a los totales de la compra: ==== Esta secci�n esta inutilizada por ahora, ya que los controles que la afectan no est�n visibles ====
                dbCompIni = DbFinal
                dbCompFin = dbCompIni
                dbCompFin = dbCompFin + Val(txtFlete.Tag)    'Agregando Fletes (escrito manualmente, se almacena en el TAG el valor "limpio" sin formato).
                
                
                'Estos controles no son visibles por el momento, se desactivaron los controles
                '  pero no los c�lculos:
                lblTotalN = Format(dbCompFin, stMFormat): lblTotalN.Tag = dbCompFin
                lblImpAT = Format(dbCTImp, stMFormat): lblImpAT.Tag = dbCTImp
                lblDescAT = Format(dbCTDesc, stMFormat): lblDescAT.Tag = dbCTDesc
        
End Sub
'HRR R1853

'---------------------------------------------------------------------------------------
' Procedure : Calcular_Mul
' DateTime  : 22/06/2015 08:59
' Author    : juan_urrego
' Purpose   : T28370-R22535 Realiza el c�lculo completo cuando se seleccionan mas de dos entradas
'---------------------------------------------------------------------------------------
'
Sub Calcular_Mul()
   Dim DbInicial() As Double 'Almacena el valor inicial de las entradas
   Dim DbIvaEnt() As Double 'Almacena el total iva
   Dim DbimpDesc As Double 'Almacena el valor del impuesto o descuento
   Dim DbFinal() As Double 'Almacena la diferencia entre el valor inicial, impuestos y descuentos
   Dim DbTIIca As Double 'Almacena los impuestos de tipo ICA
   Dim DbTIRet As Double 'Almacena los impuestos de tipo Retencion
   Dim DbTIRIva As Double 'Almacena los impuestos de tipo Reteiva
   Dim InJ As Integer 'Contador
   Dim InI As Integer 'Contador
   Dim InX As Integer 'Contador
   Dim dbVlNetoMul() As Double 'Diferencia entre el valor inicial y el iva
   Dim DbVlrIvaEntMul() As Double 'Almacena el iva de cada una de las entradas
   Dim DbValorDes As Double 'JAUM T31312 Almacena el valor del descuento
   InJ = 0
   DbTIIca = 0
   DbTIRet = 0
   DbTIRIva = 0
   ReDim ArDescComp(6, 0)
   ReDim VrDesImp(7, 0) 'JLPB T41657
   ReDim ArImpComp(6, 0)
   'Calcular los valores de la grilla.
   With flexEntradas
      ReDim DbInicial(.Rows - 1)
      ReDim DbIvaEnt(.Rows - 1)
      ReDim dbVlNetoMul(.Rows - 1)
      ReDim DbVlrIvaEntMul(.Rows - 1)
      ReDim DbTDCom(.Rows - 1)
      ReDim DbTImp(.Rows - 1)
      ReDim DbFinal(.Rows - 1)
      ReDim DbTDCond(.Rows - 1)
      For InI = 1 To .Rows - 1
         If fnRowSel(CLng(InI)) Then
            DbInicial(InI) = Val(.TextMatrix(InI, 4))
            DbInicial(0) = DbInicial(0) + Val(.TextMatrix(InI, 4))
            DbIvaEnt(InI) = Aplicacion.Formatear_Valor(Val(.TextMatrix(InI, 7)))
            DbIvaEnt(0) = DbIvaEnt(0) + Aplicacion.Formatear_Valor(Val(.TextMatrix(InI, 7)))
         End If
      Next
      For InX = 1 To .Rows - 1
         dbVlNetoMul(InX) = DbInicial(InX) - IIf(BoAproxCent, AproxCentena(Round(DbIvaEnt(InX))), DbIvaEnt(InX))
         DbVlrIvaEntMul(InX) = IIf(BoAproxCent, AproxCentena(Round(DbIvaEnt(InX))), DbIvaEnt(InX))
         sbApForm
         For InI = 0 To ClssImpuDesc.SizeDesc
            If Val(ClssImpuDesc.ElemDesc(InI, 3)) <= Val(dbVlNetoMul(InX)) Then 'Se aplica el "tope", no se aplica si el valor no tiene el m�nimo requerido.
               DbimpDesc = 0
               If Val(ClssImpuDesc.ElemDesc(InI, 2)) = 0 Then 'Valor
                  If ClssImpuDesc.ElemDesc(InI, 4) = NUL$ Then Call ClssImpuDesc.SetElemDesc(InI, 4, 0)
                  'JAUM T31312 Inicio
                  If ClssImpuDesc.ElemDesc(InI, 0) <> NUL$ Then
                     DbValorDes = fnDevDato("DESCUENTO", "VL_VALO_DESC", "CD_CODI_DESC = '" & ClssImpuDesc.ElemDesc(InI, 0) & Comi, True)
                     If Aplicacion.Formatear_Valor(DbValorDes) <> Aplicacion.Formatear_Valor(ClssImpuDesc.ElemDesc(InI, 4)) Then
                        If BoAproxCent = True Then
                           DbimpDesc = AproxCentena(Round(Aplicacion.Formatear_Valor(DbValorDes)))
                        Else
                           DbimpDesc = Aplicacion.Formatear_Valor(DbValorDes)
                        End If
                        GoTo Siguiente
                     End If
                  End If
                  'JAUM T31312 Fin
                  If BoAproxCent = True Then
                     DbimpDesc = AproxCentena(Round(Aplicacion.Formatear_Valor(ClssImpuDesc.ElemDesc(InI, 4))))
                  Else
                     DbimpDesc = Aplicacion.Formatear_Valor(ClssImpuDesc.ElemDesc(InI, 4))
                  End If
Siguiente: 'JAUM T31312
               Else 'Porcentaje
                  If BoAproxCent = True Then
                     DbimpDesc = AproxCentena(Round(Aplicacion.Formatear_Valor(Val(dbVlNetoMul(InX) * ClssImpuDesc.ElemDesc(InI, 2)) / 100)))
                  Else
                     DbimpDesc = Aplicacion.Formatear_Valor(Val(dbVlNetoMul(InX) * ClssImpuDesc.ElemDesc(InI, 2)) / 100)
                  End If
               End If
               If Val(ClssImpuDesc.ElemDesc(InI, 6)) <> 1 Then   'descuento comercial
                  DbTDCom(InX) = DbTDCom(InX) + DbimpDesc
               Else 'descuento condicional
                  DbTDCond(InX) = DbTDCond(InX) + DbimpDesc
               End If
               'If InJ <> 0 Then JAUM T31312
               If InJ <> 0 And InX = 1 Then 'JAUM T31312
                  ReDim Preserve ArDescComp(6, InJ)
               End If
               'JAUM T31312 Inicio
               If ClssImpuDesc.ElemDesc(InI, 0) <> NUL$ And ArDescComp(0, InJ) = ClssImpuDesc.ElemDesc(InI, 0) Then
                  ArDescComp(4, InJ) = ArDescComp(4, InJ) + DbimpDesc
                  InJ = InJ + 1
               Else
               'JAUM T31312 Fin
                  ArDescComp(0, InJ) = ClssImpuDesc.ElemDesc(InI, 0)
                  ArDescComp(1, InJ) = ClssImpuDesc.ElemDesc(InI, 1)
                  ArDescComp(2, InJ) = ClssImpuDesc.ElemDesc(InI, 2)
                  ArDescComp(3, InJ) = ClssImpuDesc.ElemDesc(InI, 3)
                  ArDescComp(4, InJ) = DbimpDesc
                  ArDescComp(5, InJ) = ClssImpuDesc.ElemDesc(InI, 5)
                  ArDescComp(6, InJ) = ClssImpuDesc.ElemDesc(InI, 6)
                  InJ = InJ + 1
               End If 'JAUm T31312
               'JLPB T41657 INICIO
               ReDim Preserve VrDesImp(7, UBound(VrDesImp, 2) + 1)
               VrDesImp(0, UBound(VrDesImp, 2)) = ClssImpuDesc.ElemDesc(InI, 0)
               VrDesImp(1, UBound(VrDesImp, 2)) = ClssImpuDesc.ElemDesc(InI, 1)
               VrDesImp(2, UBound(VrDesImp, 2)) = ClssImpuDesc.ElemDesc(InI, 2)
               VrDesImp(3, UBound(VrDesImp, 2)) = ClssImpuDesc.ElemDesc(InI, 3)
               VrDesImp(4, UBound(VrDesImp, 2)) = ClssImpuDesc.ElemDesc(InI, 6)
               VrDesImp(5, UBound(VrDesImp, 2)) = DbimpDesc
               VrDesImp(6, UBound(VrDesImp, 2)) = dbVlNetoMul(InX)
               VrDesImp(7, UBound(VrDesImp, 2)) = "D" & InX
               'JLPB T41657 FIN
            End If
         Next
         DbFinal(InX) = DbInicial(InX) - DbTDCom(InX) - DbTDCond(InX)
         Call ClssImpuDesc.Descuentos(ArDescComp) 'JAUM T31312
         InJ = 0
         'impuestos:
         For InI = 0 To ClssImpuDesc.SizeImp
            DbimpDesc = 0
            If ClssImpuDesc.ElemImp(InI, 6) = "C" Then
               If ClssImpuDesc.ElemImp(InI, 3) < dbVlNetoMul(InX) Then
                  If BoAproxCent = True Then
                     DbimpDesc = AproxCentena(Round(Aplicacion.Formatear_Valor(Val((dbVlNetoMul(InX) - DbTDCom(InX)) * ClssImpuDesc.ElemImp(InI, 2)) / 100)))
                  Else
                     DbimpDesc = Aplicacion.Formatear_Valor(Val((dbVlNetoMul(InX) - DbTDCom(InX)) * ClssImpuDesc.ElemImp(InI, 2)) / 100)
                  End If
                  DbTIIca = DbTIIca + DbimpDesc
               End If
            ElseIf ClssImpuDesc.ElemImp(InI, 6) = "M" Then
               If ClssImpuDesc.ElemImp(InI, 3) < DbIvaEnt(InX) Then
                  If BoAproxCent = True Then
                     DbimpDesc = AproxCentena(Round(Aplicacion.Formatear_Valor((DbIvaEnt(InX) * ClssImpuDesc.ElemImp(InI, 2)) / 100)))
                  Else
                     DbimpDesc = Aplicacion.Formatear_Valor((DbIvaEnt(InX) * ClssImpuDesc.ElemImp(InI, 2)) / 100)
                  End If
                  DbTIRIva = DbTIRIva + DbimpDesc
               End If
            ElseIf ClssImpuDesc.ElemImp(InI, 6) = "R" Then
               If ClssImpuDesc.ElemImp(InI, 3) < dbVlNetoMul(InX) Then
                  If Buscar_desc_impuesto(ClssImpuDesc.ElemImp(InI, 0)) <> NUL$ And TipoPers = "0" Then
                     If BoAproxCent = True Then
                        DbimpDesc = AproxCentena(Round(Aplicacion.Formatear_Valor(Val((dbVlNetoMul(InX) - DbTDCom(InX) - Valor_Descuento(Buscar_desc_impuesto(ClssImpuDesc.ElemImp(InI, 0)), (dbVlNetoMul(InX) - DbTDCom(InX)))) * ClssImpuDesc.ElemImp(InI, 2)) / 100)))
                     Else
                        DbimpDesc = Aplicacion.Formatear_Valor(Val((dbVlNetoMul(InX) - DbTDCom(InX) - Valor_Descuento(Buscar_desc_impuesto(ClssImpuDesc.ElemImp(InI, 0)), (dbVlNetoMul(InX) - DbTDCom(InX)))) * ClssImpuDesc.ElemImp(InI, 2)) / 100)
                     End If
                  Else
                     If BoAproxCent = True Then
                        DbimpDesc = AproxCentena(Round(Aplicacion.Formatear_Valor(Val((dbVlNetoMul(InX) - DbTDCom(InX)) * ClssImpuDesc.ElemImp(InI, 2)) / 100)))
                     Else
                        DbimpDesc = Aplicacion.Formatear_Valor(Val((dbVlNetoMul(InX) - DbTDCom(InX)) * ClssImpuDesc.ElemImp(InI, 2)) / 100)
                     End If
                  End If
                  DbTIRet = DbTIRet + DbimpDesc
               End If
            End If
            DbTImp(InX) = DbTImp(InX) + DbimpDesc
            'If InJ <> 0 Then JAUM T31312 Se deja linea en comentario
            If InJ <> 0 And InX = 1 Then 'JAUM T31312
               ReDim Preserve ArImpComp(6, InJ)
            End If
            'JAUM T31312 Inicio
            If ClssImpuDesc.ElemImp(InI, 0) <> NUL$ And ArImpComp(0, InJ) = ClssImpuDesc.ElemImp(InI, 0) Then
               ArImpComp(4, InJ) = ArImpComp(4, InJ) + DbimpDesc
               InJ = InJ + 1
            Else
            'JAUM T31312 Fin
               ArImpComp(0, InJ) = ClssImpuDesc.ElemImp(InI, 0)
               ArImpComp(1, InJ) = ClssImpuDesc.ElemImp(InI, 1)
               ArImpComp(2, InJ) = ClssImpuDesc.ElemImp(InI, 2)
               ArImpComp(3, InJ) = ClssImpuDesc.ElemImp(InI, 3)
               ArImpComp(4, InJ) = DbimpDesc
               ArImpComp(5, InJ) = ClssImpuDesc.ElemImp(InI, 5)
               ArImpComp(6, InJ) = ClssImpuDesc.ElemImp(InI, 6)
               InJ = InJ + 1
            End If 'JAUM T31312
            'JLPB T41657 INICIO
            ReDim Preserve VrDesImp(7, UBound(VrDesImp, 2) + 1)
            VrDesImp(0, UBound(VrDesImp, 2)) = ClssImpuDesc.ElemImp(InI, 0)
            VrDesImp(1, UBound(VrDesImp, 2)) = ClssImpuDesc.ElemImp(InI, 1)
            VrDesImp(2, UBound(VrDesImp, 2)) = ClssImpuDesc.ElemImp(InI, 2)
            VrDesImp(3, UBound(VrDesImp, 2)) = ClssImpuDesc.ElemImp(InI, 3)
            VrDesImp(4, UBound(VrDesImp, 2)) = ClssImpuDesc.ElemImp(InI, 6)
            VrDesImp(5, UBound(VrDesImp, 2)) = DbimpDesc
            VrDesImp(6, UBound(VrDesImp, 2)) = dbVlNetoMul(InX)
            VrDesImp(7, UBound(VrDesImp, 2)) = "I" & InX
            'JLPB T41657 FIN
         Next
         InJ = 0 'JAUM T31312
         DbFinal(InX) = DbFinal(InX) - DbTImp(InX)
         Call ClssImpuDesc.Impuestos(ArImpComp) 'JAUM T31312
         If boIPPTO Then
            '.TextMatrix(InX, 8) = Format(Val(-DbTDCom(InX) + -DbTImp(InX)), stMFormat) JAUM T29815 Se deja linea en comentario
            .TextMatrix(InX, 8) = Format(Val(-(DbTDCom(InX) + DbTDCond(InX)) + -DbTImp(InX)), stMFormat) 'JAUM T29815
         End If
         DbTImp(0) = DbTImp(0) + DbTImp(InX)
         DbTDCom(0) = DbTDCom(0) + DbTDCom(InX)
         DbTDCond(0) = DbTDCond(0) + DbTDCond(InX)
         DbFinal(0) = DbFinal(0) + DbFinal(InX)
         dbVlNetoMul(0) = dbVlNetoMul(0) + dbVlNetoMul(InX)
         DbVlrIvaEntMul(0) = DbVlrIvaEntMul(0) + DbVlrIvaEntMul(InX)
      Next
   End With
   dbVlNeto = dbVlNetoMul(0)
   DbVlrIvaEnt = DbVlrIvaEntMul(0)
   lblTotalB = Aplicacion.Formatear_Valor(CDbl(DbInicial(0))):  lblTotalB.Tag = DbInicial(0)
   lblImp = Aplicacion.Formatear_Valor(-DbTImp(0)):   lblImp.Tag = DbTImp(0)
   lblDesc = Aplicacion.Formatear_Valor(-(DbTDCom(0) + DbTDCond(0))):    lblDesc.Tag = (DbTDCom(0) + DbTDCond(0))
   lblTotal = Aplicacion.Formatear_Valor(DbFinal(0)):    lblTotal.Tag = DbFinal(0)
   lblTIIca = Aplicacion.Formatear_Valor(-DbTIIca):    lblTIIca.Tag = DbTIIca
   lblTIRet = Aplicacion.Formatear_Valor(-DbTIRet):    lblTIRet.Tag = DbTIRet
   lblTIRIVA = Aplicacion.Formatear_Valor(-DbTIRIva):  lblTIRIVA.Tag = DbTIRIva
   lblTIOtros = Aplicacion.Formatear_Valor(0):    lblTIOtros.Tag = 0
   Dim dbCompIni As Double, dbCompFin As Double
   Dim dbCTDesc As Double, dbCTImp As Double
   dbCompIni = DbFinal(0)
   dbCompFin = dbCompIni
   dbCompFin = dbCompFin + Val(txtFlete.Tag)    'Agregando Fletes (escrito manualmente, se almacena en el TAG el valor "limpio" sin formato).
   'Estos controles no son visibles por el momento, se desactivaron los controles
   '  pero no los c�lculos:
   lblTotalN = Format(dbCompFin, stMFormat): lblTotalN.Tag = dbCompFin
   lblImpAT = Format(dbCTImp, stMFormat): lblImpAT.Tag = dbCTImp
   lblDescAT = Format(dbCTDesc, stMFormat): lblDescAT.Tag = dbCTDesc
        
End Sub

Private Sub cmdSTerc_Click()
    Dim StTerc As String
    If Not frmSelect.fnSeleccionar("TERCERO", "CD_CODI_TERC,NO_NOMB_TERC", , , "Terceros") Then Exit Sub
    StTerc = aSelectC(0)
    If StTerc <> "" Then
        txtTercero = StTerc
        TxtTercero_LostFocus
    End If
    
End Sub

Private Sub cmdXClean_Click()
    chkQuery.value = 0
    chkQuery_Click
End Sub

Private Sub cmdXLS_Click()
    sbGuardar False
End Sub

Private Sub dtpFinal_GotFocus()
    chkFinal.value = 1
End Sub


Private Sub dtpInicio_GotFocus()
    chkInicio.value = 1
End Sub








Private Sub flexEntradas_Click()
    Dim I As Integer
    'JLPB T23820 INICIO SE DEJA EN COMENTARIO EL SIGUIENTE BLOQUE
'    Dim BoAproxCent As Boolean  'OMOG T17510 VARIABLE QUE SIRVE PARA VISUALIZAR SI ESTA SELECCIONADA LA OPCION DE APROXIMAR
'                                'CENTENAS EN IMPUESTOS Y DESCUENTOS EN CXP
'
''OMOG T17510 INICIO
'If boICxP Then
'   If ExisteCAMPO("PARAMETROS_CXP", "NU_REDIMDE_PCXP") Then
'      BoAproxCent = CBool(fnDevDato("PARAMETROS_CXP", "NU_REDIMDE_PCXP", NUL$))
'   End If
'End If
''OMOG T17510 FIN
   'JLPB T23820 FIN

        If flexEntradas.TextMatrix(1, 1) = NUL$ Then Exit Sub 'NYCM M2316
        
        With flexEntradas
            If .Col = 0 And .Row > 0 And btModo = icSeleccionando Then

                If .Text = " " Then
                    .Col = 0 'Al arrastrar puede aplicarlo a columnas no deseadas.
                    Set .CellPicture = Nothing
                    .Text = ""
                ElseIf .Text = "" Then
                    .Col = 0 'Al arrastrar puede aplicarlo a columnas no deseadas.
                    Set .CellPicture = imgLib(0).Picture
                    .CellPictureAlignment = 3
                    .Text = " "
                End If
                
                sbTransEnt 'Verifica cuales entradas puede seleccionar el usuario.
                
            ElseIf .Row > 0 Then
                'Result = LoadfGrid(flexArt�culos, "In_Detalle, Articulo", "CD_CODI_ARTI,NO_NOMB_ARTI, NU_COSTO_DETA , NU_ENTRAD_DETA , NU_ENTRAD_DETA * NU_COSTO_DETA", "NU_AUTO_MODCABE_DETA=" & .TextMatrix(.Row, 2) & " AND NU_AUTO_ARTI=NU_AUTO_ARTI_DETA ORDER BY NO_NOMB_ARTI")
                'LJSA M4432
                'Entradas
                'AASV M5438 Inicio
                ReDim Arr(1)
                Result = LoadData("PARAMETROS_INVE", "ID_DECVAL_PINV,NU_DECVAL_PINV", NUL$, Arr)
                If Arr(0) = "N" Then Arr(1) = 0
                'SKRV T17394 INICIO
                If Arr(1) = 1 Then Arr(1) = 2
                Desde = " In_Detalle, Articulo"
                'JLPB T22266 INICIO EL SIGUIENTE BLOQUE SE DEJA EN COEMNTARIO
'                Campos = " '',CD_CODI_ARTI,NO_NOMB_ARTI, NU_COSTO_DETA , NU_ENTRAD_DETA ,"
'                'Campos = Campos & " ROUND((ROUND((100*NU_COSTO_DETA)/(100+NU_IMPU_DETA)," & Arr(1) & ")" 'JLPB T15675 SE DEJA EN COMENTARIO
'                Campos = Campos & " ROUND((((100*NU_COSTO_DETA)/(100+NU_IMPU_DETA))" 'JLPB T15675
'                'OMOG T17510 INICIO
'                If BoAproxCent = True Then
'                   'JLPB T15675 INICIO LAS DOS SIGUIENTES LINEAS SE DEJA EN COMENTARIO
'                   'Campos = Campos & "  * (NU_ENTRAD_DETA-NU_SALIDA_DETA))+ ROUND((ROUND((100*NU_COSTO_DETA)/(100+NU_IMPU_DETA)," & Arr(1) & ")"
'                   'Campos = Campos & "  * (NU_IMPU_DETA/100) * (NU_ENTRAD_DETA-NU_SALIDA_DETA)),-2)," & Arr(1) & ")"
'                   Campos = Campos & "  * (NU_ENTRAD_DETA-NU_SALIDA_DETA))+(((100*NU_COSTO_DETA)/(100+NU_IMPU_DETA))"
'                   Campos = Campos & "  * ((NU_IMPU_DETA/100) * (NU_ENTRAD_DETA-NU_SALIDA_DETA)))," & Arr(1) & ")"
'                   'JLPB T15675 FIN
'                Else
'                'OMOG T17510 FIN
'                   'JLPB T15675 INICIO LAS DOS SIGUIENTES LINEAS SE DEJA EN COMENTARIO
'                   'Campos = Campos & "  * (NU_ENTRAD_DETA-NU_SALIDA_DETA))+ (ROUND((100*NU_COSTO_DETA)/(100+NU_IMPU_DETA)," & Arr(1) & ")"
'                   'Campos = Campos & "  * (NU_IMPU_DETA/100) * (NU_ENTRAD_DETA-NU_SALIDA_DETA))," & Arr(1) & ")"
'                   Campos = Campos & "  * (NU_ENTRAD_DETA-NU_SALIDA_DETA))+(((100*NU_COSTO_DETA)/(100+NU_IMPU_DETA))"
'                   Campos = Campos & "  * ((NU_IMPU_DETA/100) * (NU_ENTRAD_DETA-NU_SALIDA_DETA)))," & Arr(1) & ")"
'                   'JLPB T15675 FIN
'                End If 'OMOG T17510
                Campos = " '',CD_CODI_ARTI,NO_NOMB_ARTI,NU_COSTO_DETA,NU_ENTRAD_DETA,"
                Campos = Campos & "((100*NU_COSTO_DETA)/(100+NU_IMPU_DETA)) AS NU_COST_SIVA,NU_IMPU_DETA"
                'JLPB T22266 FIN
                Condicion = "NU_AUTO_MODCABE_DETA=" & .TextMatrix(.Row, 2) & " AND NU_AUTO_ARTI=NU_AUTO_ARTI_DETA And NU_ENTRAD_DETA <> 0 ORDER BY NO_NOMB_ARTI"
                'Result = LoadfGrid(flexArt�culos, "In_Detalle, Articulo", "'',CD_CODI_ARTI,NO_NOMB_ARTI, NU_COSTO_DETA , NU_ENTRAD_DETA , NU_ENTRAD_DETA * NU_COSTO_DETA", "NU_AUTO_MODCABE_DETA=" & .TextMatrix(.Row, 2) & " AND NU_AUTO_ARTI=NU_AUTO_ARTI_DETA And NU_ENTRAD_DETA <> 0 ORDER BY NO_NOMB_ARTI")
                'Result = LoadfGrid(flexArt�culos, "In_Detalle, Articulo", "'',CD_CODI_ARTI,NO_NOMB_ARTI, NU_COSTO_DETA , NU_ENTRAD_DETA , ROUND(NU_ENTRAD_DETA * NU_COSTO_DETA," & Arr(1) & ") ", "NU_AUTO_MODCABE_DETA=" & .TextMatrix(.Row, 2) & " AND NU_AUTO_ARTI=NU_AUTO_ARTI_DETA And NU_ENTRAD_DETA <> 0 ORDER BY NO_NOMB_ARTI")
                Debug.Print Campos
                Debug.Print Condicion
                'JLPB T22266 INICIO LA SIGUIENTE LINEA SE DEJA EN COMENTARIO
'                Result = LoadfGrid(flexArt�culos, Desde, Campos, Condicion)
                Result = CargarGridEntradas(flexArt�culos, Desde, Campos, Condicion, False)
                'JLPB T22266 FIN
                'SKRV T17394 FIN
                'AASV M5438 Fin
                For I = 1 To flexArt�culos.Rows - 1
                    flexArt�culos.Row = I
                    Set flexArt�culos.CellPicture = ImIn.Picture
                Next I
                'Devoluciones
                'AASV M5438 Inicio
                'Result = LoadfGrid(flexArt�culos, "In_Detalle, Articulo", "'',CD_CODI_ARTI,NO_NOMB_ARTI, NU_COSTO_DETA , NU_SALIDA_DETA , NU_SALIDA_DETA * NU_COSTO_DETA", "NU_AUTO_MODCABE_DETA=" & .TextMatrix(.Row, 2) & " AND NU_AUTO_ARTI=NU_AUTO_ARTI_DETA And NU_SALIDA_DETA <> 0 ORDER BY NO_NOMB_ARTI", True)
                Result = LoadfGrid(flexArt�culos, "In_Detalle, Articulo", "'',CD_CODI_ARTI,NO_NOMB_ARTI, NU_COSTO_DETA , NU_SALIDA_DETA , ROUND(NU_SALIDA_DETA * NU_COSTO_DETA," & Arr(1) & ")", "NU_AUTO_MODCABE_DETA=" & .TextMatrix(.Row, 2) & " AND NU_AUTO_ARTI=NU_AUTO_ARTI_DETA And NU_SALIDA_DETA <> 0 ORDER BY NO_NOMB_ARTI", True)
                'AASV M5438 Fin
                For I = I To flexArt�culos.Rows - 1
                    flexArt�culos.Row = I
                    Set flexArt�culos.CellPicture = ImOut.Picture
                Next I
                'LJSA M4432
                'sbAplFormat flexArt�culos, 2
                'sbAplFormat flexArt�culos, 4
                sbAplFormat flexArt�culos, 3
                sbAplFormat flexArt�culos, 5
            End If
            
            If .Col = 5 And Not cmdStart.Enabled And btModo = icCreando Then
                'sbStartEdit flexEntradas, flexEntradas.Row, 5, RGB(0, 0, 200) * No se aplica IVA a una entrada completa, cada art�culo contiene ese impuesto aplicado en la 'Entrada de Mercancia'.
            End If
        End With
        
End Sub

Private Sub flexEntradas_DblClick()
                
        If flexEntradas.Row <> 0 Then
        
        '00 Informaci�n de los impuestos en la fila seleccionada:
        Dim StInfo As String, stLine As String, I&, t$, jm&
        
        With flexEntradas
            If .Col < 3 And Not cmdStart.Enabled Then
                jm& = .Row - 1
                For I& = 0 To UBound(arrSerCode)
                    If Not IsEmpty(arrSerIDN�m(I&)) Then
                        t$ = "[Des] "
                        If Not IsEmpty(arrSerDescP(I&)) Then
                            stLine = arrSerDescP(I&) & " %" & vbCrLf
                        ElseIf Not IsEmpty(arrSerDescV(I&)) Then
                            stLine = "$ " & arrSerDescV(I&) & vbCrLf
                        Else
                            t$ = "[Imp] "
                            stLine = arrSerImpP(I&) & " %" & vbCrLf
                        End If
                        
                        StInfo = StInfo & t$ & arrSerCode(I&) & vbTab & arrSerIDN�m(I&) & vbTab & stLine

                    End If
                Next I&
            
               If Trim(StInfo) <> "" Then
                   Mensaje1 StInfo, 3
               Else
                   Mensaje1 "No hay impuestos/descuentos asignados.", 2
               End If
            
            
            End If
        End With
        '00 Fin.
     End If
End Sub


Private Sub flexEntradas_KeyUp(KeyCode As Integer, Shift As Integer)
        If KeyCode = 113 Then flexEntradas_Click
        If KeyCode = 32 Then flexEntradas.Col = 0: flexEntradas_Click
End Sub

Private Sub flexEntradas_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
'AASV R2239 INICIO
'Dim InPos As Integer 'AASV M4704
With flexEntradas
'   'InPos = Round((y - 60) / 255) 'INDICA LA FILA POR LA CUAL PASA EL MOUSE
'   'If InPos > .Rows - 1 Then Exit Sub
'   '.Row = InPos                  'ASIGNA LA POSICION A EVALUAR
'   'If x < 220 Then               'EVALUA SOLO LA PRIMERA COLUMNA
'      'If .TextMatrix(.Row, 5) = NUL$ Then ' SI NO TIENE FACTURA
'         .ToolTipText = " Entrada sin factura"
'      Else
'         '.ToolTipText = " Entrada con n�mero de factura " & .TextMatrix(.Row, 5)
'      End If
'   Else
'      .ToolTipText = ""
'   End If
   'AASV M4704
   If .MouseRow > 0 And .MouseCol < 1 Then 'SI NO ES LA PRIMERA FILA Y ES LA PRIMERA COLUMNA
      If .TextMatrix(.MouseRow, 5) = NUL$ Then ' SI NO TIENE FACTURA
         '.ToolTipText = " Entrada sin factura"
         .ToolTipText = " Entrada sin n�mero de factura" 'AASV M4755
      Else
         .ToolTipText = " Entrada con n�mero de factura " & .TextMatrix(.MouseRow, 5)
      End If
   Else
      .ToolTipText = ""
      Exit Sub
   End If
   'AASV M4704
End With
'AASV R2239 FIN
End Sub

Private Sub Form_Deactivate()
        fPausa = True
End Sub

Private Sub Form_KeyPress(KeyAscii As Integer)
    Select Case Me.ActiveControl
        Case txtObser, cmbAuto
        Case Else
            Call Cambiar_Enter(KeyAscii)
    End Select
End Sub

Sub sbListas()

Dim ArrX(), I&
ReDim ArrX(0, 0)

    'Result = LoadMulData("IN_COMPRA Order By NU_AUTO_COMP Desc", "Top 20 NU_AUTO_COMP", NUL, ArrX()) 'JLPB T21625 SE DEJA EN COMENTARIO
    Result = LoadMulData("IN_COMPRA WITH (NOLOCK) Order By NU_AUTO_COMP Desc", "Top 20 NU_AUTO_COMP", NUL, ArrX()) 'JLPB T21625
    cmbAuto.Clear

    For I& = 0 To UBound(ArrX, 2)
        Me.cmbAuto.AddItem ArrX(0, I&)
    Next I&
    
    cmbAuto.Text = Val(cmbAuto.List(0)) + 1
    
End Sub

Private Sub Form_KeyUp(KeyCode As Integer, Shift As Integer)
        If KeyCode = 115 Then cmdSTerc_Click
End Sub

Private Sub Form_Load()
   boICxP = (Aplicacion.Interfaz_CxP) 'OMOG T17510
   sbCVistas
   sbListas
   chkQuery.Visible = fExtra
   lblEtiq(5).Visible = Not fExtra
   dtpInicio = dServ
   dtpFinal = dServ
   CenterForm MDI_Inventarios, Me
   'GrdFDef flexEntradas, 0, 0, "S,250,N�mero de entrada,1500,C�digo,1,Fecha de adquisici�n,1500,Valor de la adquicisi�n,1,Factura,900,Valor Final,1455"
   'GrdFDef flexEntradas, 0, 0, "S,250,N�mero de entrada,1500,C�digo,1,Fecha de adquisici�n,1500,Valor de la adquicisi�n,1,Factura,900,Valor Final,1455,IVA,1"
   'GrdFDef flexEntradas, 0, 0, "S,250,N�mero de entrada,1500,C�digo,1,Fecha de adquisici�n,1500,Valor de la adquicisi�n,1,Factura,1400,Valor Final,1455,IVA,1" 'JACC M596
   'JAUM T28370-R22535 Inicio
   boIPPTO = (Aplicacion.Interfaz_Presupuesto)
   If boIPPTO Then
      GrdFDef flexEntradas, 0, 0, "S,250,N�mero de entrada,1500,C�digo,1,Fecha de adquisici�n,1500,Valor de la adquicisi�n,1,Factura,2500,Valor Final,1455,IVA,1,Desc/Imp,1455"
   Else
   'JAUM T28370-R22535 Fin
      GrdFDef flexEntradas, 0, 0, "S,250,N�mero de entrada,1500,C�digo,1,Fecha de adquisici�n,1500,Valor de la adquicisi�n,1,Factura,2500,Valor Final,1455,IVA,1" 'LDCR T7346
   End If 'JAUM T28370-R22535
   'GrdFDef Me.flexArt�culos, 0, 0, "C�digo art�culo,1000,Nombre del art�culo,3200,Precio unitario,1300,Cantidad,400,Total,1300"
   GrdFDef Me.flexArt�culos, 0, 0, ",350,C�digo art�culo,1200,Nombre del art�culo,3200,Precio unitario,1300,Cantidad,750,Total,1300"        'LJSA M4432
   txtFecha.Text = Format(Nowserver, "dd/mm/yyyy")
   txtFechaV = txtFecha
   MskFechFactura = txtFechaV 'HRR R1873
   Set ClssImpuDesc = New ImpuDesc 'HRR R1853
   'LJSA M4534 -- Interfaces conbilidad & CxP ----
   boICont = (Aplicacion.Interfaz_Contabilidad)
   'boICxP = (Aplicacion.Interfaz_CxP) 'OMOG T17510 SE ESTABLECE EN COMENTARIO
   'LJSA M4534 -- Interfaces conbilidad & CxP ----
   ReDim VrVBase(0, 0) 'JAUM T28644-R27752
End Sub



Private Sub Form_Unload(Cancel As Integer)

    'If fnChanges Then If Not WarnMsg("�Desea perder los cambios?") Then Cancel = -1 'HRR R1853
    'HRR R1853
    If fnChanges Then
      If Not WarnMsg("�Desea perder los cambios?") Then
         Cancel = -1
      Else
         Set ClssImpuDesc = Nothing
      End If
   Else
      Set ClssImpuDesc = Nothing
   End If
    'HRR R1853
    
End Sub

Private Sub imgContab_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    lblInterfases = " Comprobante CPI, No. " & vContabCons & " "
End Sub


Private Sub imgCxP_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
    lblInterfases = " Cuenta por pagar No. " & vcxp & " "
End Sub

'HRR R1873
Private Sub MskFechFactura_LostFocus()
   ValFecha MskFechFactura, 0
End Sub
'HRR R1873

Private Sub txtDPlazo_KeyPress(KeyAscii As Integer)
    Call Cambiar_Enter(KeyAscii)
End Sub

Private Sub txtDPlazo_LostFocus()
        txtDPlazo = Abs(Val(txtDPlazo))
        txtFechaV = DateAdd("d", txtDPlazo, txtFecha)
End Sub


Private Sub txtFecha_LostFocus()
    ValFecha txtFecha, 0
    'If Not IsDate(txtFecha) Then Mensaje1 "Fecha incorrecta.", 2: txtFecha = Format(Nowserver, "dd/mm/yyyy")
End Sub


Private Sub txtFechaV_LostFocus()
    'ValFecha txtFechaV, 0 'AFMG T22157 SE DEJA EN COMENTARIO
    ValFecha txtFechaV, 2 'AFMG T22157
    'If Not IsDate(txtFechaV) Then Mensaje1 "Fecha incorrecta.", 2: txtFechaV = Format(Nowserver, "dd/mm/yyyy")
    txtDPlazo = DateDiff("d", txtFecha, txtFechaV)
End Sub

Private Sub txtFEdit_GotFocus()
    inFCol = flexData.Col
    inFRow = flexData.Row
End Sub

Private Sub txtFEdit_KeyUp(KeyCode As Integer, Shift As Integer)
        
    If KeyCode = 13 Then
        flexEntradas.Text = txtFEdit.Text
        txtFEdit_LostFocus
    End If
        
End Sub

Private Sub txtFEdit_LostFocus()
    txtFEdit.Visible = False
    flexData.TextMatrix(inFRow, inFCol) = Val(txtFEdit)
    'sbCalcImpSer 'Realizar c�lculos de los art�culos 'HRR R1853
End Sub

'HRR R1853
'Private Sub txtFlete_LostFocus()
'    txtFlete.Tag = Val(txtFlete)
'    txtFlete = Format(Val(txtFlete), stMFormat)
'    sbCalcImpSer
'End Sub
'HRR R1853


Private Sub txtN�mFact_KeyPress(KeyAscii As Integer)
    'Call ValKeyNum(KeyAscii)
    If boConsul = True Then KeyAscii = 0 'LDCR 6401
    Call ValKeyAlfaNumSinCarConGuion(KeyAscii) 'JACC R2221
    Call Cambiar_Enter(KeyAscii)
End Sub

Private Sub txtN�mFact_LostFocus()
        'If InStr(1, txtN�mFact, ".") Then Mensaje1 "El n�mero de factura debe ser un n�mero entero.", 2 'M:1406
        'txtN�mFact = Int(Val(txtN�mFact))
        txtN�mFact = UCase(txtN�mFact) 'JACC R2221
End Sub

Private Sub txtObser_LostFocus()
        txtObser = UCase(txtObser) 'DoInsertSQL aplica 'Ucase', el usuario sabr� que la informaci�n de las min�sculas se perder�.
End Sub

Private Sub TxtTercero_GotFocus()
        
    'Validando fechas:
        If chkInicio.value And chkFinal.value Then
            If DateDiff("d", dtpInicio, dtpFinal) < 0 Then
                Mensaje1 "Este intervalo de fechas no generar� resultados.", 2
            End If
        End If
    'Fin.
    
    txtTercero.SelStart = 0
    txtTercero.SelLength = 32000
        
End Sub

Private Sub txtTercero_KeyUp(KeyCode As Integer, Shift As Integer)
        If KeyCode = 114 Then cmdSTerc_Click
End Sub

Private Sub TxtTercero_LostFocus()
        
    Static stAnte As String
    'Dim stNTer(0) 'HRR R1853
    Dim boRetr As Boolean, boMsg As Boolean
    Dim VrArr() As Variant 'HRR M1698
    Dim BoEncontro As Boolean 'HRR R1853
            
    'HRR M1698
'    Result = LoadData("TERCERO", "NO_NOMB_TERC", "CD_CODI_TERC='" & txtTercero & "'", stNTer())
'    lblNTercero.Caption = stNTer(0) & " "
'    boRetr = stAnte <> txtTercero.Text And Trim(txtTercero) <> ""
'    If Not Encontro And boRetr Then h.sbMsg "El tercero no existe.", txtTercero, 2: boMsg = True
    'HRR M1698
    
    'HRR M1698
    'ReDim VrArr(9) 'OMOG R16934/T19113 SE ESTABLECE EN COMENTARIO
    ReDim VrArr(11)  'OMOG R16934/T19113
        Campos = "NO_NOMB_TERC,ID_TIPO_TERC,ID_TIPO_REGI_PAIM,ID_TIPO_CONTR_PAIM,"
        Campos = Campos & "ID_AUTO_RETIVA_PAIM,ID_AUTO_RETICA_PAIM,ID_AUTO_RETREN_PAIM"
        Campos = Campos & ",ID_EXERET_FUEN_PAIM,ID_EXERET_ICA_PAIM,ID_EXERET_IVA_PAIM"
        Campos = Campos & ",TX_RESO_CREE_PAIM,DE_ACTI_TERC" 'OMOG R16934/T19113
        Desde = "TERCERO,PARAMETROS_IMPUESTOS "
        Condicion = "CD_CODI_TERC=CD_CODI_TERC_PAIM"
        Condicion = Condicion & " AND CD_CODI_TERC=" & Comi & txtTercero & Comi & " and (NU_ESTADO_TERC <> 1)"
   
   Result = LoadData(Desde, Campos, Condicion, VrArr())
   If (Result <> False) Then
     If Encontro Then
        
        TipoPers = VrArr(1)
         TipoRegimen = VrArr(2)
         GranContrib = VrArr(3)
         AutRetIva = VrArr(4)
         AutRetIca = VrArr(5)
         AutoRet = VrArr(6)
         ExentoFuente = VrArr(7)
         ExentoIca = VrArr(8)
         ExentoIva = VrArr(9)
         StImpCree = VrArr(10) 'OMOG R16934/T19113
         StActi = VrArr(11) 'OMOG R16934/T19113
         cmdBusca.Enabled = True    'REOL M2075
     Else
        TipoPers = NUL$
        AutoRet = NUL$
        AutRetIva = NUL$
        AutRetIca = NUL$
        TipoRegimen = NUL$
        GranContrib = NUL$
        ExentoFuente = NUL$
        ExentoIca = NUL$
        ExentoIva = NUL$
        cmdBusca.Enabled = False    'REOL M2075
     End If
   End If
   'HRR R1853
   BoEncontro = Encontro
   If boICxP Then
      ClssImpuDesc.TipoPersona = TipoPers
      ClssImpuDesc.AutoRetenedor = AutoRet
      ClssImpuDesc.AutoRetenedorIva = AutRetIva
      ClssImpuDesc.AutoRetenedorIca = AutRetIca
      ClssImpuDesc.PTipoRegimen = TipoRegimen
      ClssImpuDesc.GranContribuyente = GranContrib
      ClssImpuDesc.PExentoFuente = ExentoFuente
      ClssImpuDesc.PExentoIva = ExentoIva
      ClssImpuDesc.PExentoIca = ExentoIca
      ClssImpuDesc.concepto = "CINV"
      'HRR M2644
      'ClssImpuDesc.Buscar_Descu
      'ClssImpuDesc.Buscar_Impu
      'HRR M2644
   End If
   Encontro = BoEncontro
   'HRR R1853
   
   
   lblNTercero.Caption = VrArr(0) & " "
   boRetr = stAnte <> txtTercero.Text And Trim(txtTercero) <> ""
   If Not Encontro And boRetr Then H.sbMsg "El tercero no existe.", txtTercero, 2: boMsg = True

'HRR M1698
        'JLPB T23820 INICIO SE DEJA EN COMENTARIO EL SIGUIENTE BLOQUE
'          'GAVL T6265 SENTENCIA PARA SABER SI LA OPCION ESTA ACTIVA EN CXP
'        If boICxP Then
'            If ExisteCAMPO("PARAMETROS_CXP", "NU_REDIMDE_PCXP") Then '
'                BoAproxCent = CBool(fnDevDato("PARAMETROS_CXP", "NU_REDIMDE_PCXP", NUL$))
'            End If
'        End If
'        'FIN GAVL T6265
        'JLPB T23820 FIN

End Sub

Private Sub cmdBusca_Click()    'REOL M2075
   Static stAnte As String
   Dim boRetr As Boolean, boMsg As Boolean, I&
   Dim StFletePart As String
   Dim InDec As Integer 'HRR M2669
   Dim StSql As String 'JLPB T15675
   'HRR M2669
   cmdBusca.Enabled = False 'JAUM T35451
   If Aplicacion.VerDecimales_Valores(InDec) Then
   Else
      InDec = 0
   End If
   'HRR M2669
   btModo = icNinguno
   flexEntradas.Rows = 1
   'JLPB T15675 INICIO
   'JLPB T22266 INICIO LA SIGUIENTE LINEA SE DEJA EN COEMNTARIO
   'StSql = "Select CD_CODI_TERC,NO_NOMB_TERC,NU_AUTO_ENCA,FE_CREA_ENCA," _
   & " SUM(ROUND((((100*NU_COSTO_DETA)/(100+NU_IMPU_DETA))  * (NU_ENTRAD_DETA-NU_SALIDA_DETA))" _
   & " +(((100*NU_COSTO_DETA)/(100+NU_IMPU_DETA))  * ((NU_IMPU_DETA/100) *" _
   & " (NU_ENTRAD_DETA-NU_SALIDA_DETA))),2)) as NU_COST_TOTAL, NU_COMP_ENCA" _
   & " From In_Encabezado, In_Detalle, Tercero Where CD_CODI_TERC_ENCA = CD_CODI_TERC And" _
   & " NU_AUTO_MODCABE_DETA = NU_AUTO_ENCA And NU_AUTO_DOCU_ENCA=3 And TX_ESTA_ENCA <> 'A'" _
   & " Group By CD_CODI_TERC,NO_NOMB_TERC, NU_AUTO_ENCA,NU_COMP_ENCA,FE_CREA_ENCA"
   'JLPB T15675 INICIO EL SIGUIENTE BLOQUE SE DEJA EN COMENTARIO
'   StSql = "SELECT CD_CODI_TERC,NO_NOMB_TERC,NU_AUTO_ENCA,FE_CREA_ENCA," _
   & " ((100*NU_COSTO_DETA)/(100+NU_IMPU_DETA)) AS NU_COST_SIVA," _
   & " NU_IMPU_DETA,(NU_ENTRAD_DETA-NU_SALIDA_DETA) as NU_CANTIDAD," _
   & " NU_COMP_ENCA" _
   & " FROM IN_ENCABEZADO WITH(NOLOCK), IN_DETALLE WITH(NOLOCK), TERCERO WITH(NOLOCK) Where CD_CODI_TERC_ENCA = CD_CODI_TERC AND" _
   & " NU_AUTO_MODCABE_DETA = NU_AUTO_ENCA AND NU_AUTO_DOCU_ENCA=3 AND TX_ESTA_ENCA <> 'A'" _
   & " GROUP BY CD_CODI_TERC,NO_NOMB_TERC, NU_AUTO_ENCA,NU_COMP_ENCA,FE_CREA_ENCA," _
   & " NU_COSTO_DETA,NU_IMPU_DETA,NU_ENTRAD_DETA,NU_SALIDA_DETA"
   'JAUM T25233 INICIO EL SIGUIENTE BLOQUE SE DEJA EN COMENTARIO
'   StSql = "SELECT CD_CODI_TERC,NO_NOMB_TERC,NU_AUTO_ENCA,FE_CREA_ENCA," _
'   & " ((100*NU_COSTO_DETA)/(100+NU_IMPU_DETA)) AS NU_COST_SIVA," _
'   & " NU_IMPU_DETA,(NU_ENTRAD_DETA-NU_SALIDA_DETA) as NU_CANTIDAD," _
'   & " NU_COMP_ENCA" _
'   & ", NU_AUTO_ARTI_DETA" _
'   & " FROM IN_ENCABEZADO WITH(NOLOCK), IN_DETALLE WITH(NOLOCK), TERCERO WITH(NOLOCK) Where CD_CODI_TERC_ENCA = CD_CODI_TERC AND" _
'   & " NU_AUTO_MODCABE_DETA = NU_AUTO_ENCA AND NU_AUTO_DOCU_ENCA=3 AND TX_ESTA_ENCA <> 'A'" _
'   & " GROUP BY CD_CODI_TERC,NO_NOMB_TERC, NU_AUTO_ENCA,NU_COMP_ENCA,FE_CREA_ENCA," _
'   & " NU_COSTO_DETA,NU_IMPU_DETA,NU_ENTRAD_DETA,NU_SALIDA_DETA" _
'   & ", NU_AUTO_ARTI_DETA"
   'JLPB T15675 FIN
   'JLPB T22266 FIN
   StSql = "SELECT CD_CODI_TERC,NO_NOMB_TERC,NU_AUTO_ENCA,FE_CREA_ENCA," _
   & " ((100*NU_COSTO_DETA)/(100+NU_IMPU_DETA)) AS NU_COST_SIVA," _
   & " NU_IMPU_DETA,(sum(NU_ENTRAD_DETA)-sum(NU_SALIDA_DETA)) as NU_CANTIDAD," _
   & " NU_COMP_ENCA" _
   & ", NU_AUTO_ARTI_DETA" _
   & " FROM IN_ENCABEZADO WITH(NOLOCK), IN_DETALLE WITH(NOLOCK), TERCERO WITH(NOLOCK) Where CD_CODI_TERC_ENCA = CD_CODI_TERC AND" _
   & " NU_AUTO_MODCABE_DETA = NU_AUTO_ENCA AND NU_AUTO_DOCU_ENCA=3 AND TX_ESTA_ENCA <> 'A'" _
   & " GROUP BY CD_CODI_TERC,NO_NOMB_TERC, NU_AUTO_ENCA,NU_COMP_ENCA,FE_CREA_ENCA," _
   & " NU_COSTO_DETA,NU_IMPU_DETA,NU_ENTRAD_DETA,NU_SALIDA_DETA" _
   & ", NU_AUTO_ARTI_DETA"
   'JAUM T25233 FIN
   If fnExisteVista("VWFORM_CompraArti") Then
      Call ExecSQLCommand("DROP VIEW VWFORM_CompraArti")
   End If
   Result = fnCrearVista("VWFORM_CompraArti", StSql, "Usado por la ventana de compra de art�culos, muestra los art�culos relacionados con los detalles y el tercero.")
   'JLPB T15675 FIN
   'Cargando entradas relacionadas con el tercero:
   'Result = LoadfGrid(flexEntradas, stVista, "'', NU_COMP_ENCA ,NU_AUTO_ENCA,FE_CREA_ENCA,NU_COST_TOTAL,'',''", stExcluCond & " And CD_CODI_TERC='" & txtTercero & "'" & IIf(chkInicio.Value, " And FE_CREA_ENCA >= " & fnFechaCon(dtpInicio), "") & IIf(chkFinal.Value, " And FE_CREA_ENCA <= " & fnFechaCon(dtpFinal), "") & " Order By FE_CREA_ENCA")
   'Result = LoadfGrid(flexEntradas, stVista, "'', NU_COMP_ENCA ,NU_AUTO_ENCA,FE_CREA_ENCA,NU_COST_TOTAL,'','',''", stExcluCond & " And CD_CODI_TERC='" & txtTercero & "'" & IIf(chkInicio.Value, " And FE_CREA_ENCA >= " & fnFechaCon(dtpInicio), "") & IIf(chkFinal.Value, " And FE_CREA_ENCA <= " & fnFechaCon(dtpFinal), "") & " Order By FE_CREA_ENCA") 'JLPB T21625 SE DEJA EN COMENTARIO
   'Result = LoadfGrid(flexEntradas, stVista & " WITH (NOLOCK)", "'', NU_COMP_ENCA ,NU_AUTO_ENCA,FE_CREA_ENCA,NU_COST_TOTAL,'','',''", stExcluCond & " And CD_CODI_TERC='" & txtTercero & "'" & IIf(chkInicio.Value, " And FE_CREA_ENCA >= " & fnFechaCon(dtpInicio), "") & IIf(chkFinal.Value, " And FE_CREA_ENCA <= " & fnFechaCon(dtpFinal), "") & " Order By FE_CREA_ENCA") 'JLPB T21625 'JLPB T22266 SE DEJA EN COMENTARIO
   'Result = CargarGridEntradas(flexEntradas, stVista & " WITH (NOLOCK)", "'', NU_COMP_ENCA ,NU_AUTO_ENCA,FE_CREA_ENCA,NU_COST_SIVA,NU_IMPU_DETA,NU_CANTIDAD,'','',''", stExcluCond & " And CD_CODI_TERC='" & txtTercero & "'" & IIf(chkInicio.Value, " And FE_CREA_ENCA >= " & fnFechaCon(dtpInicio), "") & IIf(chkFinal.Value, " And FE_CREA_ENCA <= " & fnFechaCon(dtpFinal), "") & " Order By FE_CREA_ENCA", True) 'JLPB T22266 'JAUM T25233
   Result = CargarGridEntradas(flexEntradas, stVista & " WITH (NOLOCK)", "'', NU_COMP_ENCA ,NU_AUTO_ENCA,FE_CREA_ENCA,NU_COST_SIVA,NU_IMPU_DETA,NU_CANTIDAD,'','',''", stExcluCond & " And CD_CODI_TERC='" & txtTercero & "'" & IIf(chkInicio.value, " And FE_CREA_ENCA >= " & fnFechaCon(dtpInicio), "") & IIf(chkFinal.value, " And FE_CREA_ENCA <= " & fnFechaCon(dtpFinal), "") & " Order By FE_CREA_ENCA, NU_COMP_ENCA", True) 'JAUM T25233
   Debug.Print stVista
   flexEntradas.Enabled = flexEntradas.Rows > 1
   flexArt�culos.Rows = 1
   cmdStart.Enabled = flexEntradas.Rows > 1
   If Not boMsg And Not flexEntradas.Enabled And boRetr Then H.sbMsg "Este tercero no tiene entradas de art�culos sin comprar." & IIf(chkInicio.value + chkFinal.value > 0, "%2Verifique los intervalos de fecha.", ""), txtTercero, 3
   If flexEntradas.Enabled Then
      'HRR R1853
      ClssImpuDesc.concepto = "CINV"
      ClssImpuDesc.ImpuestoCree = StImpCree 'OMOG R16934/T19113
      ClssImpuDesc.ActividadEconomica = StActi 'OMOG R16934/T19113
      Call ClssImpuDesc.Buscar_Descu
      Call ClssImpuDesc.Buscar_Impu
      'HRR R1853
      With flexEntradas
         For I& = 1 To .Rows - 1 'Agregando n�meros de factura:
            Call MouseClock
            'PJCA M1805 Tener en cuenta los Fletes
            StFletePart = fnDevDato("IN_PARTIPARTI", "NU_VALO_PRPR", "TX_CODI_PART_PRPR='7' And NU_AUTO_ENCA_PRPR=" & .TextMatrix(I&, 2), True)
            If StFletePart <> "1" Then  'Pagar flete a Proveedor
               ReDim Arr(0)
               Result = LoadData("IN_PARTIPARTI", "NU_VALO_PRPR", "TX_CODI_PART_PRPR='5' And NU_AUTO_ENCA_PRPR=" & .TextMatrix(I&, 2), Arr)
               If Arr(0) = NUL$ Then Arr(0) = 0
               .TextMatrix(I&, 4) = CDbl(.TextMatrix(I&, 4)) + Arr(0)
            End If
            'Descuento Adicional
            ReDim Arr(0)
            Result = LoadData("IN_PARTIPARTI", "NU_VALO_PRPR", "TX_CODI_PART_PRPR='6' And NU_AUTO_ENCA_PRPR=" & .TextMatrix(I&, 2), Arr)
            If Arr(0) = NUL$ Then Arr(0) = 0
            .TextMatrix(I&, 4) = CDbl(.TextMatrix(I&, 4)) - Arr(0)
            'PJCA M1805
            .TextMatrix(I&, 5) = fnDevDato("IN_PARTIPARTI", "NU_VALO_PRPR", "TX_CODI_PART_PRPR='8' And NU_AUTO_ENCA_PRPR=" & .TextMatrix(I&, 2), True)
            '.TextMatrix(i&, 7) = fnDevDato("IN_DETALLE", "SUM(NU_ENTRAD_DETA * (NU_COSTO_DETA - (100*NU_COSTO_DETA)/(100+NU_IMPU_DETA)))", "NU_AUTO_ORGCABE_DETA=" & .TextMatrix(i&, 2))    'PJCA M1862
            'REOL M1643
            'Campos = "SUM(NU_ENTRAD_DETA * (NU_COSTO_DETA - (100*NU_COSTO_DETA)/(100+NU_IMPU_DETA)))-SUM(NU_SALIDA_DETA * (NU_COSTO_DETA - (100*NU_COSTO_DETA)/(100+NU_IMPU_DETA)))"
            'Campos = "SUM(DISTINCT NU_ENTRAD_DETA * (NU_COSTO_DETA - (100*NU_COSTO_DETA)/(100+NU_IMPU_DETA)))-SUM(NU_SALIDA_DETA * (NU_COSTO_DETA - (100*NU_COSTO_DETA)/(100+NU_IMPU_DETA)))"   'REOL M2027
            'HRR M2669
            'Campos = "SUM(DISTINCT NU_ENTRAD_DETA * Round((100*NU_COSTO_DETA)/(100+NU_IMPU_DETA),2)* (NU_IMPU_DETA/100))" & _
            '"-SUM(NU_SALIDA_DETA * Round((100*NU_COSTO_DETA)/(100+NU_IMPU_DETA),2)* (NU_IMPU_DETA/100))"   'REOL M1799
            'Condi = "NU_AUTO_ORGCABE_DETA=" & .TextMatrix(i&, 2) & " OR NU_AUTO_MODCABE_DETA=" & .TextMatrix(i&, 2)
            '.TextMatrix(i&, 7) = fnDevDato("IN_DETALLE", Campos, Condi, True)
            'HRR M2669
            'LJSA M4666 -- Se pone en comentario --
            '        'HRR M2669
            '        Campos = "SUM(DISTINCT NU_ENTRAD_DETA * Round((100*NU_COSTO_DETA)/(100+NU_IMPU_DETA)," & InDec & ")* (NU_IMPU_DETA/100))" & _
            '                    "-SUM(NU_SALIDA_DETA * Round((100*NU_COSTO_DETA)/(100+NU_IMPU_DETA)," & InDec & ")* (NU_IMPU_DETA/100))"   'REOL M1799
            '        Condi = "NU_AUTO_ORGCABE_DETA=" & .TextMatrix(i&, 2) & " OR NU_AUTO_MODCABE_DETA=" & .TextMatrix(i&, 2)
            '        .TextMatrix(i&, 7) = fnDevDato("IN_DETALLE", Campos, Condi, True)
            '        'HRR M2669
            'LJSA M4666 -- Se pone en comentario --
            'AASV M4839 Nota:26106 Inicio Se modifica la forma de hacer la consulta ya que genera desbalance
            'LJSA M4666
            'Campos = "Round(SUM(NU_ENTRAD_DETA * Round((100*NU_COSTO_DETA)/(100+NU_IMPU_DETA)," & InDec & ")* (NU_IMPU_DETA/100))" & _
            '"-SUM(NU_SALIDA_DETA * Round((100*NU_COSTO_DETA)/(100+NU_IMPU_DETA)," & InDec & ")* (NU_IMPU_DETA/100)), " & InDec & ")"   'REOL M1799
            'Condi = "NU_AUTO_ORGCABE_DETA=" & .TextMatrix(I&, 2) & " And NU_AUTO_MODCABE_DETA=" & .TextMatrix(I&, 2)
            '.TextMatrix(I&, 7) = fnDevDato("IN_DETALLE", Campos, Condi, True)
            'LJSA M4666
            'Campos = "SUM(Round(Round(((100*NU_COSTO_DETA)/(100+NU_IMPU_DETA))," & InDec & ") * (NU_ENTRAD_DETA - NU_SALIDA_DETA)*(CASE NU_IMPU_DETA WHEN 0 THEN 1 ELSE (NU_IMPU_DETA/100) END) ," & InDec & "))"
            'Campos = "SUM(Round(Round(((100*NU_COSTO_DETA)/(100+NU_IMPU_DETA))," & InDec & ") * (NU_ENTRAD_DETA - NU_SALIDA_DETA)*(CASE NU_IMPU_DETA WHEN 0 THEN 0 ELSE (NU_IMPU_DETA/100) END) ," & InDec & "))" 'JACC CPO ACTUALIZACION  05/01/2010 'JLPB T15675 SE DEJA EN COMENTARIO
            'JLPB T22266 INICIO EL SIGUIENTE BLOQUE SE DEJA EN COMENTARIO
            'Campos = "SUM(ROUND(((((100*NU_COSTO_DETA)/(100+NU_IMPU_DETA))*(NU_ENTRAD_DETA-NU_SALIDA_DETA))*NU_IMPU_DETA)/100," & InDec & "))" 'JLPB T15675
            'Condi = "NU_AUTO_ORGCABE_DETA=" & .TextMatrix(I&, 2) & " And NU_AUTO_MODCABE_DETA=" & .TextMatrix(I&, 2)
            'Condi = " NU_AUTO_MODCABE_DETA=" & .TextMatrix(I&, 2) 'AASV M5907   Se quita la condicion del NU_AUTO_ORGCABE_DETA para que traiga los documentos relacionados
            '.TextMatrix(I&, 7) = fnDevDato("IN_DETALLE", Campos, Condi, True)
            'JLPB T22266 FIN
            'AASV M4839 Nota:26106 Fin
            'FIN REOL
         Next I&
         Call MouseNorm
         sbApForm
         btModo = icSeleccionando
      End With
   End If
   stAnte = txtTercero.Text
   cmdBusca.Enabled = True 'JAUM T35451
End Sub

'---------------------------------------------------------------------------------------
' Procedure : Buscar_desc_impuesto
' DateTime  : 15/09/2009 16:18
' Author    : jeison_camacho
' Purpose   : Buscar codigo descuento asociado al impuesto
'---------------------------------------------------------------------------------------
' JACC R2345
Function Buscar_desc_impuesto(ByVal StCodImpu As String) As String
   ReDim ArrDesImp(0)
   Condicion = "CD_CODI_IMPU = " & Comi & StCodImpu & Comi
   Result = LoadData("TC_IMPUESTOS", "TX_CODI_DESC_IMPU ", Condicion, ArrDesImp)
   If Result <> FAIL And Encontro And ArrDesImp(0) <> NUL$ Then
      Buscar_desc_impuesto = ArrDesImp(0)
   Else
      Buscar_desc_impuesto = NUL$
   End If
End Function

'---------------------------------------------------------------------------------------
' Procedure : CargarGridEntradas
' DateTime  : 16/07/2014 09:44 AM
' Author    : JAIRO LEONARDO PARRA BUITRAGO
' Purpose   : LISTAR LOS REGISTROS DE LAS ENTRADAS EN EL GRID 'JLPB T22266
'---------------------------------------------------------------------------------------
Function CargarGridEntradas(Grid As MSFlexGrid, ByVal TableName As String, ByVal Fields As String, ByVal Condi As Variant, Optional boAdd As Boolean) As Integer
   Dim I As Integer
   Dim J As Integer
   Dim N As Integer
   Dim l As Integer
   Dim S As String
   Dim DbCosSIVA As Double
   Dim DbIva As Double
   Dim DbTotal As Double
   Dim DbNuComp As Double

   DoEvents
  
   If Not boAdd Then Grid.Rows = 2
   Encontro = False
   If (DoSelect(TableName, Fields, Condi) <> FAIL) Then
      If (ResultQuery()) Then
         N = 0
         l = Grid.Cols - 1
         I = QueryNumCols()
         If (I < (l - N + 1)) Then
            l = N + I - 1
            Call Mensaje1("N�mero de columnas incorrecto", 2)
         End If
         Call MouseClock
         If Not boAdd Then I = Grid.FixedRows - 1 Else I = Grid.Rows - 1
         Do While (NextRowQuery())
            S = NUL$
            For J = N To l
               If boAdd Then
                  If J = 4 Then
                     DbCosSIVA = Aplicacion.Formatear_Valor(DataQuery(J - N + 1))
                     DbIva = (((DbCosSIVA * DataQuery(J - N + 3)) * DataQuery(J - N + 2)) / 100)
                     If BoAproxCent Then DbIva = AproxCentena(Round(DbIva)) Else DbIva = Aplicacion.Formatear_Valor(DbIva)
                     DbTotal = (DbCosSIVA * DataQuery(J - N + 3)) + DbIva
                     Codigo = DbTotal
                     S = S + CStr(DbTotal)
                  Else
                     If J > 4 Then
                        If J = 7 Then
                           Codigo = DbIva
                           S = S + CStr(DbIva)
                        Else
                           Codigo = NUL$
                           S = S + NUL$
                        End If
                     Else
                        Codigo = DataQuery(J - N + 1)
                        S = S + DataQuery(J - N + 1)
                     End If
                  End If
               Else
                  If J = 5 Then
                      DbCosSIVA = Aplicacion.Formatear_Valor(DataQuery(J - N + 1))
                      DbIva = (((DbCosSIVA * DataQuery(J - N + 0)) * DataQuery(J - N + 2)) / 100)
                      If BoAproxCent Then DbIva = AproxCentena(Round(DbIva)) Else DbIva = Aplicacion.Formatear_Valor(DbIva)
                      DbTotal = (DbCosSIVA * DataQuery(J - N + 0)) + DbIva
                      Codigo = DbTotal
                      S = S + CStr(DbTotal)
                  Else
                     Codigo = DataQuery(J - N + 1)
                     S = S + DataQuery(J - N + 1)
                  End If
               End If
               If J = 1 And boAdd Then DbNuComp = DataQuery(J - N + 1)
               If (J < l) Then 'No ha sido la ultima columna
                  S = S & Chr$(9)
               End If
            Next J
            I = I + 1
            'Se ha llegado al m�ximo soportado por Visual Basic para un grid?
            If (I = 2000) And Grid.Name <> "FGrdPrecios" Then 'JACC M3472
               Call MouseNorm
               Call Mensaje1("Indique parametros mas precisos para seleccionar", 3)
               Exit Do
            End If
            If boAdd Then
               If Grid.TextMatrix(I - 1, 1) <> NUL$ And IsNumeric(Grid.TextMatrix(I - 1, 1)) Then
                  If Grid.TextMatrix(I - 1, 1) = DbNuComp Then
                     Grid.TextMatrix(I - 1, 4) = Grid.TextMatrix(I - 1, 4) + DbTotal
                     Grid.TextMatrix(I - 1, 7) = Grid.TextMatrix(I - 1, 7) + DbIva
                     I = I - 1
                  Else
                     Grid.AddItem S, I
                  End If
               Else
                  Grid.AddItem S, I
               End If
            Else
               Grid.AddItem S, I
            End If
         Loop
         Encontro = (I >= 0)
         Call MouseNorm
         On Error Resume Next
         If Not boAdd Then Grid.RemoveItem Grid.Rows - 1
      End If
      Result = SUCCEED
   Else
      Result = FAIL
   End If
   If Encontro = False And Not boAdd Then Grid.Rows = 1
   Call FreeBufs(Result)
   Grid.Visible = True
   CargarGridEntradas = Result
End Function

'---------------------------------------------------------------------------------------
' Procedure : Almacenar_Auditoria_Mov
' DateTime  : 28/05/2018 14:35
' Author    : daniel_mesa
' Purpose   : DRMG T43535 Almacena la auditoria de los movimientos enviados a contabilidad, los que genera el sistema y los que se envian despues de que usuarios los acepte
'---------------------------------------------------------------------------------------
'
Private Function Almacenar_Auditoria_Mov(DbAutEnc As Double, StComp As String, DbConsMov As Double, InAutUsu As Integer) As Boolean
   Dim InI As Integer 'Utilizada para los recorridos del for
   Dim InA As Integer 'Utilizada para los recorridos del for
   Dim LnRetorno As Long 'Envia cero (0) cuando no se ha dimensionado un arreglo
   
   GetSafeArrayPointer VrAudMov, LnRetorno
   If LnRetorno <> 0 Then
      For InA = 0 To UBound(VrAudMov, 2)
         For InI = 0 To UBound(VrAudMov, 3)
            If VrAudMov(8, InA, InI) <> NUL$ Then
               If Result <> FAIL Then
                  Valores = "NU_AUTENCA_AUDT=" & DbAutEnc & Coma
                  Valores = Valores & "TX_COMP_AUDT='" & StComp & Comi & Coma
                  Valores = Valores & "NU_CONMOV_AUDT=" & DbConsMov & Coma
                  Valores = Valores & "TX_CUENTA_AUDT='" & VrAudMov(0, InA, InI) & Comi & Coma
                  Valores = Valores & "TX_TERC_AUDT='" & VrAudMov(1, InA, InI) & Comi & Coma
                  Valores = Valores & "TX_CC_AUDT='" & VrAudMov(2, InA, InI) & Comi & Coma
                  Valores = Valores & "TX_NATU_AUDT='" & VrAudMov(3, InA, InI) & Comi & Coma
                  Valores = Valores & "NU_VALOR_AUDT=" & VrAudMov(4, InA, InI) & Coma
                  Valores = Valores & "NU_VALORB_AUDT=" & VrAudMov(5, InA, InI) & Coma
                  Valores = Valores & "NU_VALORIVA_AUDT=" & VrAudMov(6, InA, InI) & Coma
                  Valores = Valores & "NU_AUTUSU_AUDT=" & InAutUsu & Coma
                  Valores = Valores & "NU_IMPT_AUDT=" & VrAudMov(7, InA, InI) & Coma
                  Valores = Valores & "NU_ORIALM_AUDT=" & VrAudMov(8, InA, InI)
                  Result = DoInsertSQL("IN_AUDMOVCONT", Valores)
               End If
            End If
         Next
      Next
   End If
   Almacenar_Auditoria_Mov = Result
End Function

