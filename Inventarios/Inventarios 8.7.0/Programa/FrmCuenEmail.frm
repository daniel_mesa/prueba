VERSION 5.00
Object = "{0BA686C6-F7D3-101A-993E-0000C0EF6F5E}#1.0#0"; "THREED32.OCX"
Begin VB.Form FrmCuenEmail 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "CUENTA DE CORREO"
   ClientHeight    =   3780
   ClientLeft      =   45
   ClientTop       =   375
   ClientWidth     =   5310
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   3780
   ScaleWidth      =   5310
   StartUpPosition =   3  'Windows Default
   Begin VB.Frame FrmCorreo 
      Caption         =   "Mensaje"
      Height          =   3735
      Left            =   80
      TabIndex        =   0
      Top             =   0
      Width           =   5175
      Begin VB.CheckBox ChkSSL 
         Caption         =   "Seg. SSL"
         Height          =   255
         Left            =   2760
         TabIndex        =   15
         Top             =   720
         Width           =   975
      End
      Begin VB.TextBox TxtSMTP 
         Height          =   285
         Left            =   1440
         MaxLength       =   255
         TabIndex        =   1
         Top             =   360
         Width           =   2295
      End
      Begin VB.TextBox TxtEmail 
         Height          =   285
         Left            =   1440
         MaxLength       =   50
         TabIndex        =   3
         Top             =   1080
         Width           =   2295
      End
      Begin VB.TextBox TxtAsunto 
         Height          =   285
         Left            =   1440
         MaxLength       =   60
         TabIndex        =   5
         Top             =   2040
         Width           =   3615
      End
      Begin VB.TextBox TxtDetalle 
         Height          =   975
         Left            =   1440
         MaxLength       =   500
         MultiLine       =   -1  'True
         TabIndex        =   6
         Top             =   2520
         Width           =   3615
      End
      Begin VB.TextBox TxtClave 
         Height          =   285
         IMEMode         =   3  'DISABLE
         Left            =   1440
         MaxLength       =   30
         PasswordChar    =   "*"
         TabIndex        =   4
         Top             =   1560
         Width           =   2295
      End
      Begin VB.TextBox TxtPuerto 
         Height          =   285
         Left            =   1440
         MaxLength       =   5
         TabIndex        =   2
         Top             =   720
         Width           =   735
      End
      Begin Threed.SSCommand ScmdSalir 
         Height          =   735
         Left            =   3960
         TabIndex        =   8
         Top             =   960
         Width           =   735
         _Version        =   65536
         _ExtentX        =   1296
         _ExtentY        =   1296
         _StockProps     =   78
         Caption         =   "&Salir"
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   6.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Picture         =   "FrmCuenEmail.frx":0000
      End
      Begin Threed.SSCommand ScmdGuardar 
         Height          =   735
         Left            =   3960
         TabIndex        =   7
         Top             =   240
         Width           =   735
         _Version        =   65536
         _ExtentX        =   1296
         _ExtentY        =   1296
         _StockProps     =   78
         Caption         =   "&Guardar"
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   6.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Picture         =   "FrmCuenEmail.frx":06CA
      End
      Begin VB.Label LblClave 
         Caption         =   "Clave:"
         Height          =   255
         Left            =   120
         TabIndex        =   14
         Top             =   1560
         Width           =   855
      End
      Begin VB.Label LblSmtp 
         Caption         =   "Servidopr SMTP:"
         Height          =   255
         Left            =   120
         TabIndex        =   13
         Top             =   360
         Width           =   1335
      End
      Begin VB.Label LblEmail 
         Caption         =   "Correo:"
         Height          =   255
         Left            =   120
         TabIndex        =   12
         Top             =   1080
         Width           =   615
      End
      Begin VB.Label LlAsunto 
         Caption         =   "Asunto:"
         Height          =   255
         Left            =   120
         TabIndex        =   11
         Top             =   2040
         Width           =   615
      End
      Begin VB.Label LblDetalle 
         Caption         =   "Detalle:"
         Height          =   255
         Left            =   120
         TabIndex        =   10
         Top             =   2520
         Width           =   615
      End
      Begin VB.Label LblPuerto 
         Caption         =   "Puerto:"
         Height          =   255
         Left            =   120
         TabIndex        =   9
         Top             =   720
         Width           =   1215
      End
   End
End
Attribute VB_Name = "FrmCuenEmail"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'---------------------------------------------------------------------------------------
' Module    : FrmCuenEmail
' DateTime  : 25/09/2018 16:19
' Author    : daniel_mesa
' Purpose   : DRMG T44728-R41288 Formulario que gurda y actualiza los paramentros de la SMTP
'---------------------------------------------------------------------------------------

Option Explicit

Dim VrArr() As Variant 'Almacena los resultados de consultas sql

'---------------------------------------------------------------------------------------
' Procedure : Form_Load
' DateTime  : 25/09/2018 16:19
' Author    : daniel_mesa
' Purpose   : 'DRMG T44728-R41288 centra el formulario y carga la informacion
'---------------------------------------------------------------------------------------
'
Private Sub Form_Load()
   Call CenterForm(MDI_Inventarios, Me)
   Call CargarEmail
End Sub

'---------------------------------------------------------------------------------------
' Procedure : ScmdGuardar_Click
' DateTime  : 25/09/2018 16:19
' Author    : daniel_mesa
' Purpose   : 'DRMG T44728-R41288 Guarda o Acutualiza la informacion del correo para la factura electronica
'---------------------------------------------------------------------------------------
'
Private Sub ScmdGuardar_Click()

   If TxtSMTP = NUL$ Then
      Call Mensaje1("Digite el SMTP.", 3)
      Exit Sub 'DRMG T45138
   End If
   
   If TxtPuerto = NUL$ Then
      Call Mensaje1("Digite el Puerto.", 3)
      Exit Sub 'DRMG T45138
   End If
   
   If TxtEmail = NUL$ Then
      Call Mensaje1("Digite el correo.", 3)
      Exit Sub 'DRMG T45138
   End If
   
   If TxtClave = NUL$ Then
      Call Mensaje1("Digite la contraseña.", 3)
      Exit Sub 'DRMG T45138
   End If
   
   If TxtAsunto = NUL$ Then
      Call Mensaje1("Digite el asunto.", 3)
      Exit Sub 'DRMG T45138
   End If
   
   Valores = "TX_SMPT_DEF='" & TxtSMTP & Comi
   Valores = Valores & Coma & "TX_CORREO_DEF='" & TxtEmail & Comi
   Valores = Valores & Coma & "TX_CLAVE_DEF='" & TxtClave & Comi
   Valores = Valores & Coma & "TX_ASUNTO_DEF='" & TxtAsunto & Comi
   Valores = Valores & Coma & "TX_DETA_DEF='" & TxtDetalle & Comi
   Valores = Valores & Coma & "TX_MODULO_DEF='1'"
   Valores = Valores & Coma & "TX_PUERTO_DEF='" & TxtPuerto & Comi
   Valores = Valores & Coma & "NU_SSL_DEF=" & ChkSSL.value 'DRMG T45220
   Condicion = "TX_MODULO_DEF='1'"
   
   ReDim VrArr(0)
   Result = LoadData("DATOS_EMAIL_FAC", "TX_MODULO_DEF", Condicion, VrArr())
   If Result <> FAIL Then
      If Not Encontro Then
         Result = DoInsertSQL("DATOS_EMAIL_FAC", Valores)
      Else
         Result = DoUpdate("DATOS_EMAIL_FAC", Valores, Condicion)
      End If
   End If
   
   If Result <> FAIL Then
      Call Mensaje1("Se ha guardado correctamente la información", 3)
   End If
   Call CargarEmail
   
End Sub

'---------------------------------------------------------------------------------------
' Procedure : CargarEmail
' DateTime  : 25/09/2018 16:19
' Author    : daniel_mesa
' Purpose   : 'DRMG T44728-R41288 Cargala informacion del correo de la factura electronica
'---------------------------------------------------------------------------------------
'
Private Function CargarEmail()
   'ReDim VrArr(5) 'DRMG T45220 SE DEJA EN COMENTARIO
   ReDim VrArr(6) 'DRMG T45220
   Valores = "TX_SMPT_DEF"
   Valores = Valores & Coma & "TX_CORREO_DEF"
   Valores = Valores & Coma & "TX_CLAVE_DEF"
   Valores = Valores & Coma & "TX_ASUNTO_DEF"
   Valores = Valores & Coma & "TX_DETA_DEF"
   Valores = Valores & Coma & "TX_PUERTO_DEF"
   Valores = Valores & Coma & "NU_SSL_DEF" 'DRMG T45220
   Condicion = "TX_MODULO_DEF='1'"
   Result = LoadData("DATOS_EMAIL_FAC", Valores, Condicion, VrArr())
   
   If Result <> FAIL Then
      If Encontro Then
         TxtSMTP.Text = VrArr(0)
         TxtEmail.Text = VrArr(1)
         TxtClave.Text = CStr(VrArr(2))
         TxtAsunto.Text = VrArr(3)
         TxtDetalle.Text = VrArr(4)
         TxtPuerto.Text = VrArr(5)
         ChkSSL.value = IIf(VrArr(6) = True, 1, 0)  'DRMG T45220
      End If
   End If
   
End Function

'---------------------------------------------------------------------------------------
' Procedure : ScmdGuardar_KeyPress
' DateTime  : 25/09/2018 16:19
' Author    : daniel_mesa
' Purpose   : DRMG T44728-R41288 Pasa el Enter como tap
'---------------------------------------------------------------------------------------
'
Private Sub ScmdGuardar_KeyPress(KeyAscii As Integer)
   Call Cambiar_Enter(KeyAscii)
End Sub

'---------------------------------------------------------------------------------------
' Procedure : ScmdSalir_Click
' DateTime  : 25/09/2018 16:32
' Author    : daniel_mesa
' Purpose   : DRMG T44728-R41288  CIERRA LA VENTANA
'---------------------------------------------------------------------------------------
'
Private Sub ScmdSalir_Click()
   Unload Me
End Sub

'---------------------------------------------------------------------------------------
' Procedure : TxtAsunto_KeyPress
' DateTime  : 25/09/2018 16:19
' Author    : daniel_mesa
' Purpose   : DRMG T44728-R41288 Pasa el Enter como tap
'---------------------------------------------------------------------------------------
'
Private Sub TxtAsunto_KeyPress(KeyAscii As Integer)
   Call Cambiar_Enter(KeyAscii)
End Sub

'---------------------------------------------------------------------------------------
' Procedure : TxtClave_KeyPress
' DateTime  : 25/09/2018 16:19
' Author    : daniel_mesa
' Purpose   : DRMG T44728-R41288 Pasa el Enter como tap
'---------------------------------------------------------------------------------------
'
Private Sub TxtClave_KeyPress(KeyAscii As Integer)
   Call Cambiar_Enter(KeyAscii)
End Sub

'---------------------------------------------------------------------------------------
' Procedure : TxtDetalle_KeyPress
' DateTime  : 25/09/2018 16:19
' Author    : daniel_mesa
' Purpose   : DRMG T44728-R41288 Pasa el Enter como tap
'---------------------------------------------------------------------------------------
'
Private Sub TxtDetalle_KeyPress(KeyAscii As Integer)
   Call Cambiar_Enter(KeyAscii)
End Sub

'---------------------------------------------------------------------------------------
' Procedure : TxtEmail_KeyPress
' DateTime  : 25/09/2018 16:19
' Author    : daniel_mesa
' Purpose   : DRMG T44728-R41288 Pasa el Enter como tap
'---------------------------------------------------------------------------------------
'
Private Sub TxtEmail_KeyPress(KeyAscii As Integer)
   Call Cambiar_Enter(KeyAscii)
End Sub

'---------------------------------------------------------------------------------------
' Procedure : TxtPuerto_Change
' DateTime  : 25/09/2018 16:19
' Author    : daniel_mesa
' Purpose   : DRMG T44883-R41288 Pasa el Enter como tap
'---------------------------------------------------------------------------------------
'
Private Sub TxtPuerto_KeyPress(KeyAscii As Integer)
   Call ValKeyNum(KeyAscii) 'DRMG T45138
   Call Cambiar_Enter(KeyAscii)
End Sub

'---------------------------------------------------------------------------------------
' Procedure : TxtSMTP_KeyPress
' DateTime  : 25/09/2018 16:19
' Author    : daniel_mesa
' Purpose   : DRMG T44728-R41288 Pasa el Enter como tap
'---------------------------------------------------------------------------------------
'
Private Sub TxtSMTP_KeyPress(KeyAscii As Integer)
   Call Cambiar_Enter(KeyAscii)
End Sub

