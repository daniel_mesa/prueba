Attribute VB_Name = "NotaContable"
Option Explicit

'Parametros de Entrada
'Tipo de Nota:    Tipo de nota que genera NC - Nota credito
'Cuota:           Retorna el valor del numero de cuota al que aplica el movimiento
'Estado:          C-cancelado,S-sin facturar, A-anulado
'autonumeracion   Autonumerico tipo de numeracioin
'contrato         Autonumerico del contrato
'NroMovimiento    Numero de movimiento generado
'arrCuotasNota:   columan 0,numero cuota, columna 1, valor cuota, 2 autonumerico cuota
'opcion           0: Solo lo que envia en el arreglo
'                 9: lo que envia en el arrego mas apot_contrato
'                 10, solo mueve las cuentas del contrato c,
'valor :          valor con el que se mueven las cuentas del contrato , valido solo para la opcion 10
'condselafi: condicion de selecion de afiliados
'*******************************************************
'Funcion en Reconstruccion
'Optional InfPersona:      Contiene el arreglo con los valores de la nota por cuota y persona
'
Public Function Generar_Notas(ByVal TipoNota As Documento, ByRef Cuota As Integer, ByRef EstNotaDebito As Numeracion, ByVal Secuencia As Long, ByVal Contrato As Long, ByVal Estado As String, ByVal NroMovimiento As Long, ByRef arrCuotasNota() As Variant, ByVal condselafi As String, ByVal TipoValor As String, ByVal FDesde As String, ByVal FHasta As String, ByVal Opcion As Integer, ByRef arrComprobantes() As Comprobante, Optional valordoc As Double = 0, Optional valoraux As Double = 0, Optional Inf As Variant) As Integer
Dim TN               As String
Dim i                As Integer
Dim AutNumProducto   As Long
Dim arrfac()         As Variant  'Arreglo con informacion de factura
Dim arrcuotas()      As Variant  'Arreglo con informacion sobre cuotas
Dim valor            As Double   'valor de la desafiliacion
Dim autofactura      As Long
Dim nronota          As String
Dim Interfase        As Byte
Dim nroFactura       As String   'numero de factura, sobre el que se genera la nota
Dim cancelada        As Boolean  'true si la factura relacionada de la nota esta cancelada
         
ReDim arrfac(0)

   'carga el valor de la cuota
   Generar_Notas = FAIL
   If TipoNota = factura Then
      Call Mensaje1("Documento no permitido", 3)
      Exit Function
   End If
   
   Interfase = CargarParametro("12")
   
   TN = IIf(TipoNota = notacredito, "NC", "ND")
   
   ReDim arr(0)
   
   'Busca el producto parametrizado
   AutNumProducto = CargarParametro("06")
   If AutNumProducto = -1 Then
      Call Mensaje1("No se encuentra parametrizado el producto de Medicina." & vbCr & "Consulte la opci�n de parametros", 3)
      Exit Function
   End If
   
   'If (Opcion = 0 Or Opcion = 9 Or Opcion = 7 Or Opcion = 6) And arrCuotasNota(0, 0) <> "" Then
   If (Opcion = 0 Or Opcion = 9 Or Opcion = 7) And arrCuotasNota(0, 0) <> "" Then
      
      For i = 0 To UBound(arrCuotasNota, 2)
         cancelada = False
         
         'Busca el autonumerico de la factura a la que afecta
         'Result = Buscar_Factura_Cuota(Contrato, CInt(arrCuotasNota(0, i)), AutoFactura)
         'If Result = FAIL Then Exit Function
         
         If Result <> FAIL Then Result = Consultar_NroFactura(Contrato, arrCuotasNota(0, i), autofactura, nroFactura, cancelada)
         If Result = FAIL Then Exit Function
         
         If autofactura = 0 Then
            Call Mensaje1("No se encontro la factura de la cuota " & CInt(arrCuotasNota(0, i)) & " para hacer la nota", 3)
            Exit Function
         End If
         
         nronota = LeerNroFactura(EstNotaDebito, Secuencia)
         If nronota = "" Then
            Call Mensaje1("No se encontro el consecutivo de facturacion", 3)
            Result = FAIL: Exit For
         End If
   
         If Result = FAIL Then Exit Function
         valor = CDbl(CStr(arrCuotasNota(3, i)))
         
         'If Result <> FAIL Then Result = Consultar_NroFactura(Contrato, arrCuotasNota(0, i), nrodctorel, cancelada)
      
         ReDim arr(16)
         'Carga los los datos del contrato
         Desde = "CONTRATO,CUOTA_CONTRATO,NEGOCIO"
         Campos = "NU_AUTO_CONT,NU_AUTO_CUOT,FE_VENC_CUOT,0,NU_AUTO_TERC_CONT,TX_TIAF_NEGO,NU_AUTO_NEGO_CONT,NU_AUTO_EMPR_CONT,FE_INIC_CONT,FE_FINA_CONT,NU_AUTO_FOPA_CUOT,NU_AUTO_PEPA_CUOT,NU_AUTO_BANC_CUOT,TX_CUEN_CUOT,NU_AUTO_TERC_CUOT,FE_INIC_CUOT,FE_VENC_CUOT"
         Condicion = "NU_AUTO_NEGO_CONT=NU_AUTO_NEGO_CONT"
         Condicion = Condicion & " AND NU_AUTO_CONT=NU_AUTO_CONT_CUOT"
         Condicion = Condicion & " AND NU_AUTO_CONT=" & Contrato
         Condicion = Condicion & " AND NU_NUME_CUOT=" & val(arrCuotasNota(0, i))
         Result = LoadData(Desde, Campos, Condicion, arr)
         If Result = FAIL Or Not Encontro Then Exit Function
      
         'Graba en el tipo de movimiento
         '-----------------------------------------------------------------------------
         Valores = "TX_TIPO_TIMO=" & Comi & TN & Comi & Coma
         Valores = Valores & "NU_AUTO_NUDO_TIMO=" & EstNotaDebito.autonumerico & Coma
         Valores = Valores & "TX_FACT_TIMO=" & Comi & EstNotaDebito.Prefijo & nronota & Comi & Coma
         'Cuando la factura esta sin cancelar la nota se guarda con el estado cancelado y se afecta el saldo de esta
         Valores = Valores & "TX_ESTA_TIMO=" & Comi & IIf(cancelada = False, "C", "S") & Comi & Coma
         Valores = Valores & "FE_GENE_TIMO=" & FFechaIns(FechaServer) & Coma
         Valores = Valores & "FE_VENC_TIMO=" & FFechaIns(FechaServer) & Coma
         Valores = Valores & "NU_VALO_TIMO=" & valor & Coma
         'si la factura no esta cancelada el saldo de la nota es 0 porque se lleva al saldo de la factura
         Valores = Valores & "NU_SALD_TIMO=" & IIf(cancelada = False, 0, valor) & Coma
         Valores = Valores & "NU_AUTO_TERC_TIMO=" & CLng(arr(14)) & Coma
         Valores = Valores & "NU_AUTO_NEGO_TIMO=" & CLng(arr(6)) & Coma
         Valores = Valores & "NU_AUTO_SUCU_TIMO=" & CLng(arr(7)) & Coma
         Valores = Valores & "FE_INVI_TIMO=" & FFechaIns(arr(8)) & Coma
         Valores = Valores & "FE_FIVI_TIMO=" & FFechaIns(arr(9)) & Coma
         Valores = Valores & "FE_INPE_TIMO=" & FFechaIns(arr(15)) & Coma
         Valores = Valores & "FE_FIPE_TIMO=" & FFechaIns(arr(16)) & Coma
         Valores = Valores & "NU_AUTO_FOPA_TIMO=" & CLng(arr(10)) & Coma
         Valores = Valores & "NU_AUTO_PEPA_TIMO=" & CLng(arr(11)) & Coma
         Valores = Valores & "NU_AUTO_BANC_TIMO=NULL" & Coma
         Valores = Valores & "TX_CUEN_TIMO=NULL" & Coma
         Valores = Valores & "NU_AUTO_CONT_TIMO=" & arr(0) & Coma
         Valores = Valores & "NU_AUTO_DIRE_TIMO=NULL" & Coma
         Valores = Valores & "TX_RUTI_TIMO=" & Comi & "N" & Comi & Coma
         Valores = Valores & "NU_AUTO_TIMO_TIMO=" & autofactura & Coma
         Valores = Valores & "NU_AUTO_CONE_TIMO=" & NumConex & Coma
         Valores = Valores & "FE_EMIS_TIMO=" & FFechaIns(FechaServer)
         Result = DoInsertSQL("TIPO_MOVIMIENTO", Valores)
         If Result = FAIL Then Exit For
   
         'actualiza el saldo de la factura
         ReDim arrfac(2)
         Condicion = "NU_AUTO_TIMO=" & autofactura
         Result = LoadData("TIPO_MOVIMIENTO", "NU_SALD_TIMO,NU_NODB_TIMO,NU_NOCR_TIMO", Condicion, arrfac)
         If Result = FAIL Or Not Encontro Then Result = FAIL: Exit For
         
         If Result <> FAIL Then
            If TN = "ND" Then
               Valores = "NU_SALD_TIMO= " & CDbl(arrfac(0)) + valor & Coma
               Valores = Valores & "NU_NODB_TIMO=" & CDbl(arrfac(1)) + valor
               If Result <> FAIL Then Result = DoUpdate("TIPO_MOVIMIENTO", Valores, Condicion)
               If Result = FAIL Then Exit For
               If CountRecords = 0 Then Call Mensaje1("No se pudo actualizar el saldo de la factura", 3): Result = FAIL: Exit For
            ElseIf TN = "NC" And cancelada = False Then
               If CDbl(arrfac(0)) - valor >= 0 Then
                  Valores = "NU_SALD_TIMO= " & CDbl(arrfac(0)) - valor & Coma
                  Valores = Valores & "NU_NOCR_TIMO=" & CDbl(arrfac(2)) + valor
               Else
                  Call Mensaje1("La factura " & nroFactura & " no tiene saldo", 1)
                  Result = FAIL
               End If
               If Result <> FAIL Then Result = DoUpdate("TIPO_MOVIMIENTO", Valores, Condicion)
               If Result = FAIL Then Exit For
               If CountRecords = 0 Then Call Mensaje1("No se pudo actualizar el saldo de la factura", 3): Result = FAIL: Exit For
            End If
            
         End If
         
         'Consulta el autonumerico asignado
         ReDim arrfac(0)
         Campos = "NU_AUTO_TIMO"
         Condicion = "TX_FACT_TIMO=" & Comi & EstNotaDebito.Prefijo & nronota & Comi
         Condicion = Condicion & " AND NU_AUTO_NUDO_TIMO=" & EstNotaDebito.autonumerico
         Result = LoadData("TIPO_MOVIMIENTO", Campos, Condicion, arrfac)
         If Result = FAIL Or Not Encontro Then Exit For
   
         'Graba la relacion entre el tipo de movimiento y la cuota
         Valores = "NU_AUTO_CUOT_CUTI=" & CLng(arr(1)) & Coma
         Valores = Valores & "NU_AUTO_TIMO_CUTI=" & CLng(arrfac(0)) & Coma
         Valores = Valores & "NU_VALO_CUTI=" & 0
         Result = DoInsertSQL("R_CUOTA_TIPO_MOVIMIENTO", Valores)
         If Result = FAIL Then Exit For
   
         'Graba en el detalle del tipo de movimiento
         Valores = "NU_AUTO_TIMO_TMDE=" & arrfac(0) & Coma
         Valores = Valores & "NU_AUTO_PRAD_TMDE=" & AutNumProducto & Coma
         Valores = Valores & "NU_DEBI_TMDE=" & 0 & Coma
         Valores = Valores & "NU_CRED_TMDE=" & 0 & Coma
         Valores = Valores & "NU_CANT_TMDE=" & 1 & Coma
         Valores = Valores & "TX_INDI_TMDE=" & Comi & "S" & Comi & Coma
         Valores = Valores & "TX_TIPO_TMDE=" & Comi & "A" & Comi & Coma
         Result = DoInsertSQL("TIPO_MOVIMIENTO_DETALLE", Valores)
         If Result = FAIL Then Exit For
         
         'Graba el documento de acuerdo al movimiento de contrato generado
         Valores = "NU_NUME_MOCO_MCTM=" & NroMovimiento & Coma
         Valores = Valores & "NU_AUTO_TIMO_MCTM=" & CLng(arrfac(0))
         Result = DoInsertSQL("R_MOV_CON_TIPO_MOV", Valores)
         If Result = FAIL Then Exit For
         
         'Graba los afiliados correspondientes
         Result = Grabar_Afiliados(CLng(arrfac(0)), Contrato, condselafi, TipoValor, FDesde, FHasta, valoraux, CInt(arrCuotasNota(0, i)), Inf)
         
         'Genera la nota correspondiente
         If Result <> FAIL And Interfase = 1 Then Result = Apot_Notas(CLng(arrfac(0)), TN, nroFactura, cancelada, arrComprobantes)
         If Result = FAIL Then Exit For
         
      Next i
   End If
   
   If (Opcion = 7) And Result <> FAIL Then
      If Interfase = 1 And valordoc <> 0 Then Result = Apot_Contrato(Contrato, "+", valordoc, FechaServer, "M", NroMovimiento)
   ElseIf Opcion = 6 Then
      If Interfase = 1 And valordoc <> 0 Then Result = Apot_Contrato(Contrato, "-", valordoc, FechaServer, "M", NroMovimiento)
   End If
   
   Generar_Notas = Result

End Function
'Invoca el formulario de seleccion de documentos contables
Public Function InvocarNumeracion(ByVal TipoDocumento As Documento, ByRef autoNumeracion As Long, ByRef NumeroDocumento As String, ByRef Secuencia As Long, ByVal AutoSucursal) As Integer
   With FrmSelNum
      .TipoDocumento = TipoDocumento
      .Sucursal = AutoSucursal
      Call MouseNorm
      .Show 1
      If .Seleccion = True Then
         autoNumeracion = .Informacion(0)
         NumeroDocumento = .Informacion(1)
         Secuencia = .Informacion(2)
         InvocarNumeracion = SUCCEED
      Else
         InvocarNumeracion = FAIL
      End If
   End With
   Call MouseClock
End Function

'Auto_Contrato        => Autonumerico del contrato
'Auto_Persona_Natural => Autonumerico persona natural
'Fh_Desde             => Fecha inicio
'Fh_Hasta             => Fecha hasta
'arrNC                => Cuota - Valor cuota de la nota credito
'clase                => 1 toma el valor de meses segun la cuota
Public Function Calcular_valor_NC_X_Persona(ByRef Auto_Contrato As Long, ByRef Auto_Persona_Natural As Long, ByRef Fh_Desde As String, ByRef Fh_Hasta As String, ByRef arrNC() As Variant, ByVal Clase As Byte) As Integer
   Dim i     As Integer
   Dim valor As Double
   Dim dias  As Integer
   
   ReDim arr(6, i)
   ReDim arrNC(6, 0)
   i = 0
   
   Calcular_valor_NC_X_Persona = FAIL
   valor = 0
   Campos = "NU_NUME_CUOT, FE_INIC_CUOT, FE_VENC_CUOT, NU_NETO_COCU,NU_AUTO_CUOT,FE_INIC_COCU,FE_FINA_COCU"
   Desde = "CONTRATO_DETALLE_CUOTA, CUOTA_CONTRATO"
   Condicion = "NU_AUTO_CONT_COCU = NU_AUTO_CONT_CUOT"
   Condicion = Condicion & " AND NU_AUTO_CONT_COCU = " & Auto_Contrato
   Condicion = Condicion & " AND NU_AUTO_PENA_COCU  = " & Auto_Persona_Natural
   Condicion = Condicion & " AND " & SParA & SParA
   Condicion = Condicion & "     FE_VENC_CUOT <= " & FFechaCon(Fh_Desde)
   Condicion = Condicion & " AND FE_INIC_CUOT >= " & FFechaCon(Fh_Desde)
   Condicion = Condicion & " AND FE_VENC_CUOT <= " & FFechaCon(Fh_Hasta) & SParC
   Condicion = Condicion & " OR "
   Condicion = Condicion & SParA
   Condicion = Condicion & "     FE_INIC_CUOT <= " & FFechaCon(Fh_Hasta)
   Condicion = Condicion & " AND FE_VENC_CUOT >= " & FFechaCon(Fh_Desde) & SParC
   Condicion = Condicion & " AND FE_VENC_CUOT <= " & FFechaCon(Fh_Hasta) & SParC
   
   'adicion hecha debido al mvto de inclusion
   Condicion = Condicion & " AND " & SParA & SParA
   Condicion = Condicion & "     FE_INIC_CUOT<=FE_INIC_COCU"
   Condicion = Condicion & " AND FE_VENC_CUOT>=FE_FINA_COCU" & SParC
   Condicion = Condicion & " OR "
   Condicion = Condicion & SParA
   Condicion = Condicion & "     FE_INIC_COCU<=FE_INIC_CUOT"
   Condicion = Condicion & " AND FE_FINA_COCU>=FE_VENC_CUOT" & SParC & SParC
   
   Result = LoadMulData(Desde, Campos, Condicion, arr)
   If Result = FAIL Then Exit Function
      
   If Encontro Then
      For i = 0 To UBound(arr, 2)
         If i > 0 Then ReDim Preserve arrNC(6, i)
         Dim meses As Long
         'meses = DateDiff("M", Fh_Desde, Fh_Hasta)
         'If meses = 0 Then meses = 1
         meses = Cantidad_Meses(Fh_Desde, Fh_Hasta)
         
         'si tiene igual numero de dias
         If CDate(arr(1, i)) = Fh_Desde And Clase = 0 Then
            dias = meses * 30
         ElseIf Clase = 1 Then
            'meses = Abs(DateDiff("m", CDate(arr(1, i)), CDate(arr(2, i))))
            'If meses = 0 Then meses = 1
            meses = Cantidad_Meses(CDate(arr(1, i)), CDate(arr(2, i)))
            dias = meses * 30
         ElseIf Clase = 2 Then   'prorrateado
            'If CDate(arr(1, i)) < CDate(arr(5, i)) And CDate(arr(6, i)) <= CDate(arr(2, i)) Then
               dias = Abs(DateDiff("d", CDate(arr(5, i)), CDate(arr(2, i)))) + 1 - Dias_Adicionales(CDate(arr(5, i)), CDate(arr(2, i)))
               If dias = 1 Then dias = 0
            'Else
               'meses = Abs(DateDiff("m", CDate(arr(1, i)), CDate(arr(2, i))))
               'If meses = 0 Then meses = 1
            '   meses = Cantidad_Meses(CDate(arr(1, i)), CDate(arr(2, i)))
            '   dias = meses * 30
            'End If
         ElseIf Clase = 10 Then
         
         Else
            dias = Abs(DateDiff("d", CDate(arr(1, i)), Fh_Desde))
         End If
         valor = Format(((arr(3, i) / 30) * dias), "###,##0.00")
         arrNC(0, i) = arr(0, i)
         arrNC(1, i) = CDate(arr(1, i))   'inicion
         arrNC(2, i) = CDate(arr(2, i))   'fin
         arrNC(3, i) = valor     'Valor
         arrNC(4, i) = CLng(arr(4, i))
      Next i
   End If
   Calcular_valor_NC_X_Persona = Result
End Function

Public Function Calcular_valor_NC_X_Nucleo(ByRef Auto_Contrato As Long, ByRef Nucleo As Long, ByRef arrNucleo() As Variant, Optional Fh_Desde As String, Optional Fh_Hasta As String) As Boolean
   Dim i         As Integer
   Dim j         As Integer
   Dim arrNC()   As Variant
   Dim arr()     As Variant
   
   j = 0
   i = 0
   ReDim arr(0, i)
   ReDim arrNucleo(4, 0)
   
   Campos = "DISTINCT NU_AUTO_PENA_COPN"
   Desde = "R_CONTRATO_AFILIADO"
   Condicion = "NU_AUTO_CONT_COPN = " & Auto_Contrato
   Condicion = Condicion & " AND NU_GRUP_COPN = " & Nucleo
   Result = LoadMulData(Desde, Campos, Condicion, arr)
   If Result = FAIL Then
      Calcular_valor_NC_X_Nucleo = False
      Exit Function
   End If
   
   If Encontro Then
   For i = 0 To UBound(arr, 2)
      Call Calcular_valor_NC_X_Persona(Auto_Contrato, CLng(arr(0, i)), Fh_Desde, Fh_Hasta, arrNC, 0)
      'Suma cuota por nucleo
      If i = 0 Then
         For j = 0 To UBound(arrNC, 2)
            If j > 0 Then ReDim Preserve arrNucleo(4, j)
            arrNucleo(0, j) = arrNC(0, i) 'Cuota
            arrNucleo(1, i) = CDate(arrNC(2, i))   'inicio
            arrNucleo(2, i) = CDate(arrNC(2, i))   'fin
            arrNucleo(3, j) = arrNC(3, i) 'Valor
            arrNucleo(4, i) = CLng(arrNC(4, i)) 'autonumerico cuota
         Next j
      Else
         For j = 0 To UBound(arrNC, 2)
            arrNucleo(3, j) = arrNucleo(3, j) + arrNC(3, j) ' valor
         Next j
      End If
   Next i
   End If
   Calcular_valor_NC_X_Nucleo = True
End Function


Public Function Cuotas_Facturadas(ByVal Contrato As Long, ByVal CuotaInicial As Integer, ByRef arrcuotas() As Variant) As Integer
Dim i As Integer
Dim arr() As Variant
ReDim arrcuotas(4, 0)
   Condicion = "NU_AUTO_CONT_CUOT=" & Contrato
   Condicion = Condicion & " AND NU_NUME_CUOT>=" & CuotaInicial
   Condicion = Condicion & " AND NU_AUTO_CUOT=NU_AUTO_CUOT_CUTI"
   Condicion = Condicion & " AND NU_AUTO_TIMO_CUTI=NU_AUTO_TIMO"
   Condicion = Condicion & " AND TX_ESTA_CUOT='F'"    'facturadas
   Condicion = Condicion & " AND TX_TIPO_TIMO='FA'"   'factura
   Condicion = Condicion & " AND TX_ESTA_TIMO<>'A'"   'no este anulada
   Campos = "DISTINCT NU_NUME_CUOT,FE_INIC_CUOT,FE_VENC_CUOT,0,NU_AUTO_CUOT"
   Result = LoadMulData("TIPO_MOVIMIENTO,R_CUOTA_TIPO_MOVIMIENTO,CUOTA_CONTRATO", Campos, Condicion, arrcuotas)
   If Result <> FAIL Then
      If Encontro Then
         For i = 0 To UBound(arrcuotas, 2)
            Call Valor_Cuota_Contrato(Contrato, arrcuotas(4, i), arr)
            arrcuotas(3, i) = arr(3) 'NETO
         Next i
      End If
   End If
   Cuotas_Facturadas = Result
End Function

'Consulta el numero de la factura de una cuota, y su estado
Public Function Consultar_NroFactura(ByVal Contrato As Long, ByVal Cuota As Integer, ByRef autofactura As Long, ByRef nroFactura As String, ByRef cancelada As Boolean) As Integer
ReDim arr(5)

   Condicion = "NU_AUTO_CONT_CUOT=" & Contrato
   Condicion = Condicion & " AND NU_NUME_CUOT=" & Cuota
   Condicion = Condicion & " AND NU_AUTO_CUOT=NU_AUTO_CUOT_CUTI"
   Condicion = Condicion & " AND NU_AUTO_TIMO_CUTI=NU_AUTO_TIMO"
   Condicion = Condicion & " AND TX_ESTA_CUOT='F'"    'facturadas
   Condicion = Condicion & " AND TX_TIPO_TIMO='FA'"   'factura
   Condicion = Condicion & " AND TX_ESTA_TIMO<>'A'"   'no este anulada
   Campos = "NU_AUTO_TIMO,TX_FACT_TIMO,TX_ESTA_TIMO,NU_SALD_TIMO,TX_RUTI_TIMO,TX_NUDOCS_TIMO"
   Result = LoadData("TIPO_MOVIMIENTO,R_CUOTA_TIPO_MOVIMIENTO,CUOTA_CONTRATO", Campos, Condicion, arr)
   If Result <> FAIL And Encontro Then
      autofactura = arr(0)
      nroFactura = arr(1)
      If arr(2) = "C" And CDbl(arr(3)) = 0 Then
         cancelada = True
      Else
         cancelada = False
      End If
      'Cuando la factura es migrada retonra el numero asignado en apoteosys
      If arr(4) = "S" Then nroFactura = arr(5)
   Else
      Call Mensaje1("No se encontro numero de factura", 3)
      Result = FAIL
   End If
   Consultar_NroFactura = Result
End Function

'TipoValor=
'           0. si lo toma el valor de cuota_contrato_detalle
'           1. si lo toma de valor nota credito
'Public Function Grabar_Afiliados(ByVal TipoMovimiento As Long, ByVal AutNumContrato As Long, ByVal condaux As String, ByVal TipoValor As String, ByVal FDesde As String, ByVal FHasta As String, ByVal Otrovalor As Double) As Integer
'Dim llave   As String
'Dim i       As Integer
'Dim rst     As New ADODB.Recordset
'Dim valor   As Double
'
'   Desde = "R_CONTRATO_AFILIADO,PERSONA_NATURAL,TERCERO"
'   Condicion = "NU_AUTO_PENA_COPN=NU_AUTO_PENA"
'   Condicion = Condicion & " AND NU_AUTO_TERC_PENA=NU_AUTO_TERC"
'   Condicion = Condicion & " AND NU_AUTO_CONT_COPN=" & AutNumContrato
'   If condaux <> NUL$ Then Condicion = Condicion & " AND " & condaux
'   Condicion = Condicion & " ORDER BY NU_GRUP_COPN, TX_TIPO_COPN DESC"
'
'   Campos = "TX_TIPO_COPN,TX_NUID_TERC,NU_AUTO_COPN,NU_AUTO_PARE_COPN,NU_AUTO_PLAN_COPN,NU_AUTO_PENA_COPN"
'   rst.Open "SELECT " & Campos & " FROM " & Desde & " WHERE " & Condicion, BD(BDCurCon), adOpenStatic
'   'rst.MoveFirst
'
'   While Not rst.EOF
'      Dim arrNC() As Variant
'      llave = rst.Fields(1)
'      If TipoValor = "0" Then
'         ReDim arr(1)
'         Condicion = "NU_AUTO_CONT_CUOT=" & AutNumContrato
'         Condicion = Condicion & " AND NU_NUME_CUOT=" & FDesde
'         Result = LoadData("CUOTA_CONTRATO", "MIN(FE_INIC_CUOT),MAX(FE_VENC_CUOT)", Condicion, arr)
'         If Result = FAIL Then Grabar_Afiliados = FAIL: Exit Function
'
'         If Encontro Then
'            'Result = Valor_Cuota_Persona(AutNumContrato, rst.Fields(5), arrNC, CDate(arr(0)))
'            Result = Calcular_valor_NC_X_Persona(AutNumContrato, rst.Fields(5), CDate(arr(0)), CDate(arr(1)), arrNC, 0)
'            If Result = FAIL Then Grabar_Afiliados = FAIL: Exit Function
'            valor = arrNC(3, 0)
'         End If
'
'      ElseIf TipoValor = "1" Then
'         Result = Calcular_valor_NC_X_Persona(AutNumContrato, rst.Fields(5), FDesde, FHasta, arrNC, 0)
'         If Result = FAIL Then Grabar_Afiliados = FAIL: Exit Function
'         valor = arrNC(3, 0)
'
'      ElseIf TipoValor = "3" Then
'         'Result = Calcular_valor_NC_X_Persona(AutNumContrato, rst.Fields(5), FDesde, FHasta, arrNC, 0)
'         'If Result = FAIL Then Grabar_Afiliados = FAIL: Exit Function
'         valor = Otrovalor
'      'modificado enero 15
'      ElseIf TipoValor = "4" Then
'         'ojo hace lo mismo que 1
'         Result = Calcular_valor_NC_X_Persona(AutNumContrato, rst.Fields(5), FDesde, FHasta, arrNC, 0)
'         If Result = FAIL Then Grabar_Afiliados = FAIL: Exit Function
'         valor = arrNC(3, 0)
'      End If
'
'      Valores = "NU_AUTO_TIMO_TMAF=" & TipoMovimiento & Coma
'      Valores = Valores & "NU_AUTO_COPN_TMAF=" & rst.Fields(2) & Coma
'      Valores = Valores & "TX_LLAV_TMAF=" & Comi & llave & Comi & Coma
'      Valores = Valores & "NU_VALO_CUOT_TMAF=" & valor & Coma
'      Valores = Valores & "NU_AUTO_PARE_TMAF=" & rst.Fields(3) & Coma
'      Valores = Valores & "NU_AUTO_PLAN_TMAF=" & rst.Fields(4) & Coma
'      Valores = Valores & "TX_TIPO_TMAF=" & Comi & rst.Fields(0) & Comi
'      Result = DoInsertSQL("TIPO_MOVIMIENTO_AFILIADO", Valores)
'      If Result = FAIL Then Grabar_Afiliados = FAIL: Exit Function
'      rst.MoveNext
'   Wend
'   rst.Close
'
'   Grabar_Afiliados = Result
'
'End Function
Public Function Grabar_Afiliados(ByVal TipoMovimiento As Long, ByVal AutNumContrato As Long, ByVal condaux As String, ByVal TipoValor As String, ByVal FDesde As String, ByVal FHasta As String, ByVal Otrovalor As Double, ByVal Cuota As Integer, Optional InfPersonas) As Integer
Dim llave   As String
Dim i       As Integer
Dim rst     As New ADODB.Recordset
Dim valor   As Double
Dim prorrateado As Boolean
Dim Numero As Byte


   ReDim arr(0)
   Condicion = "NU_AUTO_CONT=" & AutNumContrato
   Condicion = Condicion & " AND NU_AUTO_NEGO_CONT=NU_AUTO_NEGO"
   Result = LoadData("CONTRATO,NEGOCIO", "TX_FAPR_NEGO", Condicion, arr)
   If Result = FAIL Or Not Encontro Then Exit Function
   'Numero = IIf(arr(0) = "1", 2, 0)
   Numero = 2

   Desde = "R_CONTRATO_AFILIADO,PERSONA_NATURAL,TERCERO"
   Condicion = "NU_AUTO_PENA_COPN=NU_AUTO_PENA"
   Condicion = Condicion & " AND NU_AUTO_TERC_PENA=NU_AUTO_TERC"
   Condicion = Condicion & " AND NU_AUTO_CONT_COPN=" & AutNumContrato
   If condaux <> NUL$ Then Condicion = Condicion & " AND " & condaux
   Condicion = Condicion & " ORDER BY NU_GRUP_COPN, TX_TIPO_COPN DESC"

   Campos = "TX_TIPO_COPN,TX_NUID_TERC,NU_AUTO_COPN,NU_AUTO_PARE_COPN,NU_AUTO_PLAN_COPN,NU_AUTO_PENA_COPN"
   rst.Open "SELECT " & Campos & " FROM " & Desde & " WHERE " & Condicion, BD(BDCurCon), adOpenStatic
   'rst.MoveFirst

   While Not rst.EOF
      Dim arrNC() As Variant
      llave = rst.Fields(1)
      If TipoValor = "0" Then
         ReDim arr(1)
         Condicion = "NU_AUTO_CONT_CUOT=" & AutNumContrato
         Condicion = Condicion & " AND NU_NUME_CUOT=" & FDesde
         Result = LoadData("CUOTA_CONTRATO", "MIN(FE_INIC_CUOT),MAX(FE_VENC_CUOT)", Condicion, arr)
         If Result = FAIL Then Grabar_Afiliados = FAIL: Exit Function

         If Encontro Then
            'Result = Valor_Cuota_Persona(AutNumContrato, rst.Fields(5), arrNC, CDate(arr(0)))
            'Result = Calcular_valor_NC_X_Persona(AutNumContrato, rst.Fields(5), CDate(arr(0)), CDate(arr(1)), arrNC, 0)
            Result = Calcular_valor_NC_X_Persona(AutNumContrato, rst.Fields(5), CDate(arr(0)), CDate(arr(1)), arrNC, Numero)
            If Result = FAIL Then Grabar_Afiliados = FAIL: Exit Function
            valor = arrNC(3, 0)
         End If

      ElseIf TipoValor = "1" Then
         'Result = Calcular_valor_NC_X_Persona(AutNumContrato, rst.Fields(5), FDesde, FHasta, arrNC, 0)
         Result = Calcular_valor_NC_X_Persona(AutNumContrato, rst.Fields(5), FDesde, FHasta, arrNC, Numero)
         If Result = FAIL Then Grabar_Afiliados = FAIL: Exit Function
         valor = arrNC(3, 0)

      ElseIf TipoValor = "3" Then
         'Result = Calcular_valor_NC_X_Persona(AutNumContrato, rst.Fields(5), FDesde, FHasta, arrNC, 0)
         'If Result = FAIL Then Grabar_Afiliados = FAIL: Exit Function
         valor = Otrovalor
      'modificado enero 15
      ElseIf TipoValor = "4" Then
         'ojo hace lo mismo que 1
         'Result = Calcular_valor_NC_X_Persona(AutNumContrato, rst.Fields(5), FDesde, FHasta, arrNC, 0)
         Result = Calcular_valor_NC_X_Persona(AutNumContrato, rst.Fields(5), FDesde, FHasta, arrNC, Numero)
         If Result = FAIL Then Grabar_Afiliados = FAIL: Exit Function
         valor = arrNC(3, 0)
      ElseIf TipoValor = "10" Then
         For i = 0 To UBound(InfPersonas, 2)
            If rst.Fields(5).Value = val(InfPersonas(0, i)) And Cuota = val(InfPersonas(1, i)) Then
               valor = CDbl(CStr(InfPersonas(3, i)))
               Exit For
            End If
         Next i
      End If

      Valores = "NU_AUTO_TIMO_TMAF=" & TipoMovimiento & Coma
      Valores = Valores & "NU_AUTO_COPN_TMAF=" & rst.Fields(2) & Coma
      Valores = Valores & "TX_LLAV_TMAF=" & Comi & llave & Comi & Coma
      Valores = Valores & "NU_VALO_CUOT_TMAF=" & valor & Coma
      Valores = Valores & "NU_AUTO_PARE_TMAF=" & rst.Fields(3) & Coma
      Valores = Valores & "NU_AUTO_PLAN_TMAF=" & rst.Fields(4) & Coma
      Valores = Valores & "TX_TIPO_TMAF=" & Comi & rst.Fields(0) & Comi
      Result = DoInsertSQL("TIPO_MOVIMIENTO_AFILIADO", Valores)
      If Result = FAIL Then Grabar_Afiliados = FAIL: Exit Function
      rst.MoveNext
   Wend
   rst.Close

   Grabar_Afiliados = Result

End Function

'Esta funcion elmina las comprobantes generados en apoteosys
'
Public Function Eliminar_Comprobantes(ByRef arrComprobantes() As Comprobante) As Integer
Dim i As Long
   
   Eliminar_Comprobantes = FAIL
   Result = SUCCEED
   For i = 0 To UBound(arrComprobantes)
      If arrComprobantes(i).Contabilidad <> NUL$ Then
         Result = Apot_EliminarComprobante(arrComprobantes(i))
         If Result = FAIL Then Exit Function
      End If
   Next i
   Eliminar_Comprobantes = SUCCEED
End Function
