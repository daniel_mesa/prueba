VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "ElArticulo"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit

Private num As Long 'Autonum�rico de art�culo
Private cod As String * 16 'Codigo del art�culo
Private VlotProd As String * 16 'Lote de producci�n
Private nom As String * 50 'nombre del art�culo
Private clv As String 'Clave del art�culo
'Private can As Long 'Cantidad
Private can As Double 'JACC M2967
Private mlt As Integer 'multiplicador
'Private dvd As Integer 'divisor

Private dvd As Double 'divisor 'JACC M5937

Private und As Long 'Autonum�rico Unidad de medida de inventario
Private dsc As String * 50 'Descripcion unidad de inventario art�culo
'Private dcm As Long 'Autonum�rico del documento que se esta cargando       'DEPURACION DE CODIGO
Private pra As String * 1 'C=CONSUMO, V=VENTA. Es el art�culo para venta o consumo
Private Vcm As Boolean 'El articulo maneja lotes de vencimiento
Private ent As Boolean 'El articulo maneja lotes de vencimiento
Private fCm As Long 'La fecha de vencimiento del lote
Private fNt As Long 'la fecha de entrada del lote
Private cos As Double 'Costo promedio del articulo
Private UlCos As Double 'Ultimo costo del articulo
'HRR M2968
'Private pdc As Single 'Precio de compra
'Private vta As Single 'Precio de venta del articulo
'HRR M2968
'HRR M2968
Private pdc As Double 'Precio de compra
Private vta As Double 'Precio de venta del articulo
'HRR M2968
Private vAutoListaPrecio As Long 'Autonumerico de la lista de precios
Private Iva As Single 'Porcentaje de IVA
Private VCodIva As String 'codigo porcentaje de IVA
Private Utl As Single 'Porcentaje de Utilidad
Private Consumo As Single 'Consumo promedio
Private UDescargue As Long 'Autonum�rico de la unidad de descargue del inventario
Private Acti As String * 1 'Indica si el art�culo esta activo. '0' Activo, '1' Inactivo 'GMS R1636
Private RegInvi As String * 50 'N�mero del registro invima del art�culo 'NMSR R1690
'PR_IMPU_ARTI
Private sXb As Collection 'Colecci�n de saldos x bodega
Private uVb As Collection 'Colecci�n de unidades de venta x bodega
Private sLd As Collection 'Colecci�n de SALIDAS x bodega
Private nTr As Collection 'Colecci�n ENTRADAS x bodega
Private cDb As Collection 'Colecci�n de c�digos de barra
Private lTs As Collection 'Colecci�n de Lotes X Bodega
Private pRv As Collection 'Colecci�n de proveedores
Private InDesc As Single 'Porcentaje de Descuento


'num, cod, nom, clv, mlt, dvd, und
'sXb, uVb, sLd, nTr, cDb
Public Property Get UnidadConteo() As Long
   UnidadConteo = und
End Property

Public Property Get UnidadDescargue() As Long
   UnidadDescargue = UDescargue
End Property

Public Property Let UnidadDescargue(num As Long)
   UDescargue = num
End Property

Public Property Let Clave(cla As String)
    clv = cla
End Property

Public Property Get Clave() As String
    Clave = clv
End Property

Public Property Let LoteProduccion(cla As String)
    VlotProd = cla
End Property

Public Property Get LoteProduccion() As String
    LoteProduccion = VlotProd
End Property

Public Property Get Para() As String
    Para = pra
End Property

Public Property Get CodigoImpuesto() As String
    CodigoImpuesto = VCodIva
End Property

Public Property Let FechaVence(dia As Date)
    fCm = dia
End Property

Public Property Let FechaEntrada(dia As Date)
    fNt = dia
End Property

Public Property Get FechaVence() As Date
    FechaVence = fCm
End Property

Public Property Get FechaEntrada() As Date
    FechaEntrada = fNt
End Property

Public Property Get LoteVence() As Boolean
    LoteVence = Vcm
End Property

Public Property Get LoteEntrada() As Boolean
    LoteEntrada = ent
End Property

'HRR M2968
'Public Property Get PrecioDeCompra() As Single
'    PrecioDeCompra = pdc
'End Property

'Public Property Let PrecioDeCompra(cla As Single)
'    pdc = cla
'End Property
'HRR M2968

'HRR M2968
Public Property Get PrecioDeCompra() As Double
    PrecioDeCompra = pdc
End Property

Public Property Let PrecioDeCompra(cla As Double)
    pdc = cla
End Property

'HRR M2968


Public Property Let NumInventario(cla As Long)
    und = cla
End Property

Public Property Get NumInventario() As Long
    NumInventario = und
End Property

Public Property Get Numero() As Long
    Numero = num
End Property

Public Property Get CostoPromedio() As Double
    CostoPromedio = cos
End Property

Public Property Let CostoPromedio(cnt As Double)
    cos = cnt
End Property

Public Property Get UltimoCosto() As Double
   UltimoCosto = UlCos
End Property

Public Property Let PorcentajeUtilidad(num As Single)
    Utl = num
End Property

Public Property Get PorcentajeUtilidad() As Single
    PorcentajeUtilidad = Utl
End Property

Public Property Let PorcentajeIVA(num As Single)
    Iva = num
End Property

Public Property Get PorcentajeIVA() As Single
    PorcentajeIVA = Iva
End Property

'HRR 2968
'Public Property Let ValorVENTA(cnt As Single)
'    vta = cnt
'End Property
'
'Public Property Get ValorVENTA() As Single
'    ValorVENTA = vta
'End Property
'HRR 2968

'HRR 2968
Public Property Let ValorVENTA(cnt As Double)
    vta = cnt
End Property

Public Property Get ValorVENTA() As Double
    ValorVENTA = vta
End Property
'HRR 2968

Public Property Get Activo() As String 'GMS R1636
   Activo = Acti
End Property

Public Property Get RegInvima() As String 'NMSR R1690
   RegInvima = RegInvi
End Property
'DEGP T43448 INICIO
Public Property Let PorcentajeDESCUENTO(num As Single)
    InDesc = num
End Property

Public Property Get PorcentajeDESCUENTO() As Single
    PorcentajeDESCUENTO = InDesc
End Property
'DEGP T43448 FIN

Public Function PrecioXLista(ByVal AutLisPr As Long, ByVal fecha As Date) As Double
    Dim Cmp As String, Dsd As String, Cdc As String
    Dim ArrCLS() As Variant
    Cmp = "NU_PRECIO_RLIA"
    Dsd = "IN_R_LIST_ARTI"
    Cdc = "NU_AUTO_LIPR_RLIA=" & AutLisPr & " AND NU_AUTO_ARTI_RLIA=" & num
    Cdc = Cdc & " AND FE_DESDE_RLIA<=" & FFechaCon(fecha) & " ORDER BY FE_DESDE_RLIA DESC"
    ReDim ArrCLS(0)
    Result = LoadData(Dsd, Cmp, Cdc, ArrCLS())
    If Result <> FAIL And Encontro Then
      PrecioXLista = CDbl(ArrCLS(0))
    Else
      PrecioXLista = 0
    End If
End Function

Public Property Get Codigo() As String
    Codigo = cod
End Property

Public Property Let Codigo(Cue As String)
    cod = Cue
End Property

Public Property Let NomInventario(Txt As String)
    dsc = Txt
End Property

Public Property Get NomInventario() As String
    NomInventario = dsc
End Property

Public Property Get Nombre() As String
    Nombre = nom
End Property

'Public Property Let Cantidad(cnt As Long)
Public Property Let Cantidad(cnt As Double) 'JACC M2967
    can = cnt
End Property

'Public Property Get Cantidad() As Long
Public Property Get Cantidad() As Double 'JACC M2967
    Cantidad = can
End Property

Public Property Let Multi(cnt As Integer)
    If cnt > 0 Then mlt = cnt
End Property

Public Property Get Multi() As Integer
    Multi = mlt
End Property

Public Property Let AutoListaPrecio(cnt As Integer)
    vAutoListaPrecio = cnt
End Property

Public Property Get AutoListaPrecio() As Integer
    AutoListaPrecio = vAutoListaPrecio
End Property

'Public Property Let Divide(cnt As Integer)
Public Property Let Divide(cnt As Double) 'JACC M5937
    If cnt > 0 Then dvd = cnt
End Property

'Public Property Get Divide() As Integer
Public Property Get Divide() As Double 'JACC M5937
    Divide = dvd
End Property

Public Property Get ConsumoPromedio() As Single
    ConsumoPromedio = Consumo
End Property

Public Function EnExistencia() As Double
    Dim SLXBd As InfXBodega
'    Call INTCllSaldosXBodega
    For Each SLXBd In sXb
        EnExistencia = EnExistencia + (SLXBd.Saldo.Numerador / SLXBd.Saldo.Denominador)
    Next
End Function

Public Function SaldoXBodega(ega As Long) As InfXBodega
    Dim Cmp As String, Dsd As String, Cdc As String
    Dim ArrCLS() As Variant
    Dim SLXBd As InfXBodega, ARtrnr As New InfXBodega
    ARtrnr.Bodega = ega
    Cmp = "NU_STMAX_LIBA, NU_STMIN_LIBA, NU_REPOS_LIBA, NU_MXDES_LIBA, " & _
        "TX_CODI_BODE, TX_NOMB_BODE, NU_MXDES_LIBA"
    Dsd = "IN_LIMITES_BODE_ARTI, IN_BODEGA"
    Cdc = "NU_AUTO_BODE_LIBA=NU_AUTO_BODE"
    Cdc = Cdc & " AND NU_AUTO_BODE_LIBA=" & ega & " AND NU_AUTO_ARTI_LIBA=" & num
    ReDim ArrCLS(6)
    Result = LoadData(Dsd, Cmp, Cdc, ArrCLS())
    If Result = FAIL Then Exit Function
    If Encontro Then
        If Len(ArrCLS(0)) = 0 Then ArrCLS(0) = "0"
        If Len(ArrCLS(1)) = 0 Then ArrCLS(1) = "0"
        If Len(ArrCLS(2)) = 0 Then ArrCLS(2) = "0"
        If Len(ArrCLS(6)) = 0 Then ArrCLS(6) = "0"
        ARtrnr.Codigo = ArrCLS(4)
        ARtrnr.Nombre = ArrCLS(5)
        ARtrnr.Maximo = CLng(ArrCLS(0))
        ARtrnr.Minimo = CLng(ArrCLS(1))
        ARtrnr.Reposicion = CLng(ArrCLS(2))
        ARtrnr.MaxDespachar = CLng(ArrCLS(6))
    End If
    For Each SLXBd In sXb
        If SLXBd.Bodega = ega Then
            Set ARtrnr.Saldo = SLXBd.Saldo
'            ARtrnr.Numerador = SLXBd.Numerador
'            ARtrnr.Denominador = SLXBd.Denominador
            Set SaldoXBodega = ARtrnr
            Exit Function
        End If
    Next
End Function

Public Function EntradasXBodega(ega As Long) As InfXBodega
'    Dim Cmp As String, Dsd As String, Cdc As String        'DEPURACION DE CODIGO
'    Dim ArrCLS() As Variant                                        'DEPURACION DE CODIGO
    Dim ENXBd As InfXBodega, ARtrnr As New InfXBodega
    ARtrnr.Bodega = ega
    For Each ENXBd In nTr
        If ENXBd.Bodega = ega Then
            Set ARtrnr.Entradas = ENXBd.Entradas
'            ARtrnr.Numerador = ENXBd.Numerador
'            ARtrnr.Denominador = ENXBd.Denominador
            Set EntradasXBodega = ARtrnr
            Exit Function
        End If
    Next
End Function

Public Function SalidasXBodega(ega As Long) As InfXBodega
    Dim Cmp As String, Dsd As String, Cdc As String     'DEPURACION DE CODIGO
    Dim ArrCLS() As Variant                                     'DEPURACION DE CODIGO
    Dim SLXBd As InfXBodega, ARtrnr As New InfXBodega
    ARtrnr.Bodega = ega
    For Each SLXBd In sLd
        If SLXBd.Bodega = ega Then
            Set ARtrnr.Salidas = SLXBd.Salidas
'            ARtrnr.Numerador = SLXBd.Numerador
'            ARtrnr.Denominador = SLXBd.Denominador
            Set SalidasXBodega = ARtrnr
            Exit Function
        End If
    Next
End Function

Public Function UnidadXBodega(ega As Long) As LaUndVenta
    Dim UdXBd As New LaUndVenta
    Dim TmPBd As LaUndVenta
    UdXBd.Bodega = ega
    UdXBd.NumeroArticulo = num
    UdXBd.INIXNumero
    Set UnidadXBodega = UdXBd
    For Each TmPBd In uVb
        If TmPBd.Bodega = ega Then
            Set UnidadXBodega = TmPBd
            Exit Function
        End If
    Next
End Function

Public Function IniciaXCodigo(ByVal cdg As String) As ResultConsulta
    Dim Cmp As String, Dsd As String, Cdc As String
    Dim ArrCLS() As Variant
    'GMS R1636
    'Cmp = "NU_AUTO_ARTI, CD_CODI_ARTI, NO_NOMB_ARTI, NU_AUTO_UNVE_ARTI, TX_VENCE_ARTI, " & _
    '    "TX_PARA_ARTI, TX_ENTRA_ARTI, TX_CODI_UNVE, TX_NOMB_UNVE, VL_COPR_ARTI," & _
    '    "PR_PORC_IMPU,VL_ULCO_ARTI,NU_UTIL_ARTI, NU_SUMO_ARTI, CD_IMPU_ARTI_TCIM"
    
'    Cmp = "NU_AUTO_ARTI, CD_CODI_ARTI, NO_NOMB_ARTI, NU_AUTO_UNVE_ARTI, TX_VENCE_ARTI, " & _
'        "TX_PARA_ARTI, TX_ENTRA_ARTI, TX_CODI_UNVE, TX_NOMB_UNVE, VL_COPR_ARTI," & _
'        "PR_PORC_IMPU,VL_ULCO_ARTI,NU_UTIL_ARTI, NU_SUMO_ARTI, CD_IMPU_ARTI_TCIM,TX_ESTA_ARTI"
    'NMSR R1690
    Cmp = "NU_AUTO_ARTI, CD_CODI_ARTI, NO_NOMB_ARTI, NU_AUTO_UNVE_ARTI, TX_VENCE_ARTI, " & _
        "TX_PARA_ARTI, TX_ENTRA_ARTI, TX_CODI_UNVE, TX_NOMB_UNVE, VL_COPR_ARTI," & _
        "PR_PORC_IMPU,VL_ULCO_ARTI,NU_UTIL_ARTI, NU_SUMO_ARTI, CD_IMPU_ARTI_TCIM,TX_ESTA_ARTI,TX_INVIMA_ARTI"
    'NMSR R1690
    
    Dsd = "ARTICULO, TC_IMPUESTOS, IN_UNDVENTA"
    Cdc = "CD_IMPU_ARTI_TCIM=CD_CODI_IMPU AND ARTICULO.NU_AUTO_UNVE_ARTI=IN_UNDVENTA.NU_AUTO_UNVE AND ARTICULO.CD_CODI_ARTI='" & cdg & "'"
    'ReDim ArrCLS(14)
    'ReDim ArrCLS(15) 'GMS R1636
    ReDim ArrCLS(16) 'NMSR R1690
    
    Result = LoadData(Dsd, Cmp, Cdc, ArrCLS())
    If Result = FAIL Then GoTo FALLO
    If Not Encontro Then GoTo NOENC
    num = ArrCLS(0) 'RST("NU_AUTO_ARTI")
    cod = cdg
    nom = ArrCLS(2) 'RST("NO_NOMB_ART")
    pra = ArrCLS(5) 'RST("TX_PARA_ARTI")
    Vcm = IIf(UCase(ArrCLS(4)) = "S", True, False) 'RST("TX_VENCE_ARTI")
    fCm = IIf(Vcm, FechaServer(), fCm) 'La fecha de vencimiento del lote
    ent = IIf(UCase(ArrCLS(6)) = "S", True, False) 'RST("TX_ENTRA_ARTI")
    fNt = IIf(ent, FechaServer(), fNt) 'La fecha de vencimiento del lote
    und = ArrCLS(3) 'RST("NU_AUTO_UNVE_ARTI")
    dsc = ArrCLS(8) 'RST("TX_NOMB_UNVE")
    If ArrCLS(9) = NUL$ Then ArrCLS(9) = "0"
    cos = CDbl(ArrCLS(9)) 'RST("VL_COPR_ARTI")
    cos = CDbl(Aplicacion.Formatear_Valor(cos)) 'JACC DEPURACION NOV 2008 'HRR M4537
    pdc = ArrCLS(9) 'RST("VL_COPR_ARTI")
    pdc = CDbl(Aplicacion.Formatear_Valor(pdc))  'JACC DEPURACION NOV 2008 'HRR M4537
    If ArrCLS(10) = NUL$ Then ArrCLS(10) = "0"
    Iva = CSng(ArrCLS(10)) 'RST("PR_PORC_IMPU")
    If ArrCLS(12) = NUL$ Then ArrCLS(12) = "0"
    Utl = CSng(ArrCLS(12)) 'RST("NU_UTIL_ARTI")
    If ArrCLS(11) = NUL$ Then ArrCLS(11) = "0"
    UlCos = CDbl(ArrCLS(11)) 'RST("VL_ULCO_ARTI")
    If ArrCLS(13) = NUL$ Then ArrCLS(13) = "0"
    Consumo = CSng(ArrCLS(13)) 'RST("NU_SUMO_ARTI")
    VCodIva = ArrCLS(14) 'RST("CD_IMPU_ARTI_TCIM")
    Acti = ArrCLS(15) 'GMS R1636
    RegInvi = ArrCLS(16) 'NMSR R1690
    IniciaXCodigo = Encontroinfo
    If Not vAutoListaPrecio > -1 Then Exit Function
    vta = PrecioXLista(vAutoListaPrecio, FechaServer())
    Exit Function
FALLO:
   IniciaXCodigo = Errorinfo
   Exit Function
NOENC:
   IniciaXCodigo = NoEncontroinfo
   Exit Function
End Function

Public Sub IniciaXNumero(ByVal nmr As Long, Optional StBodg As String = NUL$, Optional StNum As String = NUL$, Optional InDocu As Integer)
    Dim Cmp As String, Dsd As String, Cdc As String
    Dim ArrCLS() As Variant
    'GMS R1636
    'CMP = "NU_AUTO_ARTI, CD_CODI_ARTI, NO_NOMB_ARTI, NU_AUTO_UNVE_ARTI, TX_VENCE_ARTI, " & _
    '    "TX_PARA_ARTI, TX_ENTRA_ARTI, TX_CODI_UNVE, TX_NOMB_UNVE, VL_COPR_ARTI," & _
    '    "PR_PORC_IMPU,VL_ULCO_ARTI,NU_UTIL_ARTI, NU_SUMO_ARTI,CD_IMPU_ARTI_TCIM"
    
'    Cmp = "NU_AUTO_ARTI, CD_CODI_ARTI, NO_NOMB_ARTI, NU_AUTO_UNVE_ARTI, TX_VENCE_ARTI, " & _
'        "TX_PARA_ARTI, TX_ENTRA_ARTI, TX_CODI_UNVE, TX_NOMB_UNVE, VL_COPR_ARTI," & _
'        "PR_PORC_IMPU,VL_ULCO_ARTI,NU_UTIL_ARTI, NU_SUMO_ARTI,CD_IMPU_ARTI_TCIM,TX_ESTA_ARTI"
    'JLPB T25225 INICIO
    If StBodg <> NUL$ And StNum <> NUL$ And InDocu = DevolucionEntrada Then
       Cmp = "NU_AUTO_ARTI,CD_CODI_ARTI,NO_NOMB_ARTI,NU_AUTO_UNVE_ARTI,TX_VENCE_ARTI,TX_PARA_ARTI,TX_ENTRA_ARTI,"
       Cmp = Cmp & "TX_CODI_UNVE,TX_NOMB_UNVE,VL_COPR_ARTI,PR_PORC_IMPU,NU_COSTO_DETA,NU_UTIL_ARTI,"
       Cmp = Cmp & "NU_SUMO_ARTI,CD_IMPU_ARTI_TCIM,TX_ESTA_ARTI,TX_INVIMA_ARTI"
       Dsd = "ARTICULO INNER JOIN TC_IMPUESTOS ON CD_IMPU_ARTI_TCIM=CD_CODI_IMPU"
       Dsd = Dsd & " INNER JOIN IN_UNDVENTA ON NU_AUTO_UNVE_ARTI=NU_AUTO_UNVE"
       Dsd = Dsd & " INNER JOIN IN_DETALLE ON NU_AUTO_ARTI=NU_AUTO_ARTI_DETA"
       Dsd = Dsd & " AND NU_AUTO_MODCABE_DETA=NU_AUTO_ORGCABE_DETA"
       Cdc = " NU_AUTO_ARTI=" & nmr & " AND NU_AUTO_ORGCABE_DETA=" & CDbl(StNum)
       Cdc = Cdc & " AND NU_AUTO_BODE_DETA=" & CInt(StBodg)
    Else
    'JLPB T25225 FIN
      'NMSR R1690
      Cmp = "NU_AUTO_ARTI, CD_CODI_ARTI, NO_NOMB_ARTI, NU_AUTO_UNVE_ARTI, TX_VENCE_ARTI, " & _
          "TX_PARA_ARTI, TX_ENTRA_ARTI, TX_CODI_UNVE, TX_NOMB_UNVE, VL_COPR_ARTI," & _
          "PR_PORC_IMPU,VL_ULCO_ARTI,NU_UTIL_ARTI, NU_SUMO_ARTI,CD_IMPU_ARTI_TCIM,TX_ESTA_ARTI,TX_INVIMA_ARTI"
      'NMSR R1690
      Dsd = "ARTICULO, TC_IMPUESTOS, IN_UNDVENTA"
      Cdc = "CD_IMPU_ARTI_TCIM=CD_CODI_IMPU AND NU_AUTO_UNVE_ARTI=NU_AUTO_UNVE AND NU_AUTO_ARTI=" & nmr
    End If 'JLPB T25225
    'ReDim ArrCLS(14)
    'ReDim ArrCLS(15) 'GMS R1636
    ReDim ArrCLS(16) 'NMSR R1690
    
    Result = LoadData(Dsd, Cmp, Cdc, ArrCLS())
    If Result = FAIL Then GoTo FALLO
    If Not Encontro Then GoTo NOENC
    num = nmr
    cod = ArrCLS(1) 'RST("CD_CODI_ARTI")
    nom = ArrCLS(2) 'RST("NO_NOMB_ART")
    pra = ArrCLS(5) 'RST("TX_PARA_ARTI")
    Vcm = IIf(UCase(ArrCLS(4)) = "S", True, False) 'RST("TX_VENCE_ARTI")
    fCm = IIf(Vcm, FechaServer(), fCm) 'La fecha de vencimiento del lote
    ent = IIf(UCase(ArrCLS(6)) = "S", True, False) 'RST("TX_ENTRA_ARTI")
    fNt = IIf(ent, FechaServer(), fNt) 'La fecha de vencimiento del lote
    und = ArrCLS(3) 'RST("NU_AUTO_UNVE_ARTI")
    dsc = ArrCLS(8) 'RST("TX_NOMB_UNVE")
    If ArrCLS(9) = NUL$ Then ArrCLS(9) = "0"
    cos = ArrCLS(9) 'RST("VL_COPR_ARTI")
    If ArrCLS(10) = NUL$ Then ArrCLS(10) = "0"
    Iva = ArrCLS(10) 'RST("PR_PORC_IMPU")
    If ArrCLS(12) = NUL$ Then ArrCLS(12) = "0"
    Utl = CSng(ArrCLS(12)) 'RST("NU_UTIL_ARTI")
    If ArrCLS(11) = NUL$ Then ArrCLS(11) = "0"
    UlCos = ArrCLS(11) 'RST("VL_ULCO_ARTI")
    If ArrCLS(13) = NUL$ Then ArrCLS(13) = "0"
    Consumo = CSng(ArrCLS(13)) 'RST("NU_SUMO_ARTI")
    VCodIva = ArrCLS(14) 'RST("NU_SUMO_ARTI")
    Acti = ArrCLS(15) 'GMS R1636
    RegInvi = ArrCLS(16) 'NMSR R1690
    If Not vAutoListaPrecio > -1 Then Exit Sub
    vta = PrecioXLista(vAutoListaPrecio, FechaServer())
    Exit Sub
NOENC:
FALLO:
End Sub

Public Function IniciaXBarra(Cue As String) As ResultConsulta
    Dim Cmp As String, Dsd As String, Cdc As String
    Dim ArrCLS() As Variant
'    Cmp = "NU_AUTO_ARTI_COBA, TX_COBA_COBA, CD_CODI_TERC_COBA"
    Cmp = "NU_AUTO_ARTI_COBA, TX_COBA_COBA"
    Dsd = "IN_CODIGOBAR"
    Cdc = "TX_COBA_COBA='" & Trim(Cue) & "'"
    ReDim ArrCLS(1)
    Result = LoadData(Dsd, Cmp, Cdc, ArrCLS())
    If Result = FAIL Then GoTo FALLO
    If Not Encontro Then GoTo NOENC
    num = ArrCLS(0) 'RST("NU_AUTO_ARTI_COBA")
    Call IniciaXNumero(num)
    IniciaXBarra = Encontroinfo
    Exit Function
NOENC:
   IniciaXBarra = NoEncontroinfo
   Exit Function
FALLO:
   IniciaXBarra = Errorinfo
   Exit Function
End Function

Private Sub Class_Initialize()
    Set sXb = New Collection
    Set uVb = New Collection
    Set sLd = New Collection
    Set nTr = New Collection
    Set cDb = New Collection
    Set lTs = New Collection
    Set pRv = New Collection
    vAutoListaPrecio = -1
    fNt = CDate(2)
    fCm = CDate(2)
    mlt = 1
    dvd = 1
    pra = "V"
    Vcm = False
    ent = False
End Sub

Private Sub Class_Terminate()
    Set sXb = Nothing
    Set uVb = Nothing
    Set sLd = Nothing
    Set nTr = Nothing
    Set cDb = Nothing
    Set lTs = Nothing
End Sub

Public Sub INTCllUnidadesXBodega()
    Dim Cmp As String, Dsd As String, Cdc As String
    Dim ArrCLS() As Variant, CnTdr As Integer
    Cmp = "NU_AUTO_BODE_RBAU, NU_AUTO_UNVE_RBAU, TX_CODI_UNVE, TX_NOMB_UNVE, NU_MULT_UNVE, " & _
        "NU_DIVI_UNVE, NU_AUTO_ARTI, CD_CODI_ARTI, NO_NOMB_ARTI, NU_AUTO_UNVE"
    Dsd = "IN_R_BODE_ARTI_UNDVENTA, IN_UNDVENTA, ARTICULO"
    Cdc = "NU_AUTO_ARTI=NU_AUTO_ARTI_RBAU AND NU_AUTO_UNVE=NU_AUTO_UNVE_RBAU AND NU_AUTO_ARTI_RBAU=" & num
    Call BorrarColeccion(uVb)
    ReDim ArrCLS(9, 0)
    Result = LoadMulData(Dsd, Cmp, Cdc, ArrCLS())
    If Result = FAIL Then GoTo FALLO
    If Not Encontro Then GoTo NOENC
    For CnTdr = 0 To UBound(ArrCLS, 2)
        Dim cua As New LaUndVenta
        cua.Bodega = ArrCLS(0, CnTdr) 'RST("NU_AUTO_BODE_RBAU")
        cua.CodigoArticulo = ArrCLS(7, CnTdr) 'RST("CD_CODI_ARTI")
        cua.NombreArticulo = ArrCLS(8, CnTdr) 'RST("NO_NOMB_ART")
        cua.NumeroArticulo = num
        cua.AutoNumerico = ArrCLS(9, CnTdr) 'RST("NU_AUTO_UNVE")
        cua.CodigoDeUnidad = ArrCLS(2, CnTdr) 'RST("TX_CODI_UNVE")
        cua.NombreDeUnidad = ArrCLS(3, CnTdr) 'RST("TX_NOMB_UNVE")
        cua.Multiplicador = ArrCLS(4, CnTdr) 'RST("NU_MULT_UNVE")
        cua.Divisor = ArrCLS(5, CnTdr) 'RST("NU_DIVI_UNVE")
        uVb.Add cua, CStr(cua.Bodega)
        Set cua = Nothing
    Next
    Exit Sub
NOENC:
FALLO:
End Sub

Public Function CargarUnidadXBodega(ByVal AutoBode As Long) As ResultConsulta
    Dim Cmp As String, Dsd As String, Cdc As String
'    Dim ArrCLS() As Variant, CnTdr As Integer
    Dim ArrCLS() As Variant     'DEPURACION DE CODIGO
    Dim UndVen As New LaUndVenta
    Cmp = "NU_AUTO_UNVE,TX_CODI_UNVE,TX_NOMB_UNVE,NU_MULT_UNVE,NU_DIVI_UNVE"
    Dsd = "IN_UNDVENTA,IN_R_BODE_ARTI_UNDVENTA"
    Cdc = "NU_AUTO_UNVE=NU_AUTO_UNVE_RBAU AND NU_AUTO_BODE_RBAU=" & AutoBode & " AND NU_AUTO_ARTI_RBAU=" & num
    Call BorrarColeccion(uVb)
    ReDim ArrCLS(4)
    Result = LoadData(Dsd, Cmp, Cdc, ArrCLS())
    If Result = FAIL Then GoTo FALLO
    If Not Encontro Then
'      UDescargue = und
      UndVen.NumeroArticulo = num
      UndVen.INIXNumero
      UndVen.Bodega = AutoBode
    Else
      UndVen.Bodega = AutoBode
      UndVen.CodigoArticulo = cod
      UndVen.NombreArticulo = nom
      UndVen.NumeroArticulo = num
      UndVen.CodigoDeUnidad = ArrCLS(1)
      UndVen.NombreDeUnidad = ArrCLS(2)
      UndVen.Multiplicador = ArrCLS(3)
      UndVen.Divisor = ArrCLS(4)
      UndVen.AutoNumerico = CLng(ArrCLS(0))
    End If
    uVb.Add UndVen, CStr(UndVen.Bodega)
    Set UndVen = Nothing
    CargarUnidadXBodega = Encontroinfo
    Exit Function
NOENC:
    CargarUnidadXBodega = NoEncontroinfo
    Exit Function
FALLO:
   CargarUnidadXBodega = Errorinfo
   Exit Function
End Function


Public Sub INTCllLotesXBodega()
    Dim Cmp As String, Dsd As String, Cdc As String
    Dim ArrCLS() As Variant, CnTdr As Integer
'num, cod, nom, clv, mlt, dvd, und
'SELECT NU_AUTO_LOTE, NU_AUTO_ARTI_LOTE, FE_FECHA_LOTE, TX_TIPO_LOTE FROM IN_LOTE
'WHERE NU_AUTO_ARTI_LOTE=" & num & " AND FE_FECHA_LOTE=" & fcm & " AND TX_TIPO_LOTE="E"
    
    Cmp = "NU_AUTO_BODE_RBAU, NU_AUTO_UNVE_RBAU, TX_CODI_UNVE, TX_NOMB_UNVE, NU_MULT_UNVE, " & _
        "NU_DIVI_UNVE, NU_AUTO_ARTI, CD_CODI_ARTI, NO_NOMB_ARTI"
    Dsd = "IN_R_BODE_ARTI_UNDVENTA, IN_UNDVENTA, ARTICULO"
    Cdc = "NU_AUTO_ARTI=NU_AUTO_ARTI_RBAU AND NU_AUTO_UNVE=NU_AUTO_UNVE_RBAU AND NU_AUTO_ARTI_RBAU=" & num
    Call BorrarColeccion(lTs)
    ReDim ArrCLS(8, 0)
    Result = LoadMulData(Dsd, Cmp, Cdc, ArrCLS())
    If Result = FAIL Then GoTo FALLO
    If Not Encontro Then GoTo NOENC
    For CnTdr = 0 To UBound(ArrCLS, 2)
        Dim cua As New LoteDeArticulo
        cua.Bodega = ArrCLS(0, CnTdr) 'RST("NU_AUTO_BODE_RBAU")
        cua.CodigoArticulo = ArrCLS(7, CnTdr) 'RST("CD_CODI_ARTI")
        cua.NombreArticulo = ArrCLS(8, CnTdr) 'RST("NO_NOMB_ART")
        cua.NumeroArticulo = num
'        cua.NombreDeUnidad = ArrCLS(3, CnTdr) 'RST("TX_NOMB_UNVE")
        cua.Multiplicador = ArrCLS(4, CnTdr) 'RST("NU_MULT_UNVE")
        cua.Divisor = ArrCLS(5, CnTdr) 'RST("NU_DIVI_UNVE")
        uVb.Add cua, CStr(cua.Bodega)
        Set cua = Nothing
    Next
    Exit Sub
NOENC:
FALLO:
End Sub

'Public Sub INTCllSaldosXBodega(Optional Corte As Date)
'Public Sub INTCllSaldosXBodega()    'DEPURACION DE CODIGO
Public Sub INTCllSaldosXBodega(Optional lnVentana As Long) 'JACC M6675
    Dim Cmp As String, Dsd As String, Cdc As String
    Dim ArrCLS() As Variant, CnTdr As Integer
    Cmp = "NU_AUTO_BODE_KARD, NU_ACTUNMR_KARD, NU_ACTUDNM_KARD, NU_AUTO_ARTI, " & _
        "CD_CODI_ARTI, NO_NOMB_ARTI, FE_FECH_KARD"
    Dsd = "IN_KARDEX, ARTICULO"
'    Condi = "NU_AUTO_ARTI = NU_AUTO_ARTI_KARD AND NU_APUN_KARD=0 AND NU_AUTO_ARTI_KARD=" & num & " AND FE_PERI_KARD=" & FFechaCon(CStr(prd))
    Cdc = "NU_AUTO_ARTI = NU_AUTO_ARTI_KARD AND NU_APUN_KARD=0 AND NU_AUTO_ARTI_KARD=" & num
    Call BorrarColeccion(sXb) 'sXb, uVb, sLd, nTr, cDb
    ReDim ArrCLS(6, 0)
    Result = LoadMulData(Dsd, Cmp, Cdc, ArrCLS())
    If Result = FAIL Then GoTo CONTI
    If Not Encontro Then GoTo CONTI
    On Error Resume Next
    For CnTdr = 0 To UBound(ArrCLS, 2)
        If ArrCLS(1, CnTdr) <> 0 Then 'RST("NU_ACTUNMR_KARD")
            Dim cua As New InfXBodega
            cua.Bodega = ArrCLS(0, CnTdr) 'RST("NU_AUTO_BODE_KARD")
            cua.Codigo = ArrCLS(4, CnTdr) 'RST("CD_CODI_ARTI")
            cua.Nombre = ArrCLS(5, CnTdr) 'RST("NO_NOMB_ART")
            If Len(ArrCLS(1, CnTdr)) = 0 Then ArrCLS(1, CnTdr) = "0"
            If Len(ArrCLS(2, CnTdr)) = 0 Then ArrCLS(2, CnTdr) = "1"
            cua.Saldo.Numerador = CLng(ArrCLS(1, CnTdr)) 'RST("NU_ACTUNMR_KARD")
            cua.Saldo.Denominador = CLng(ArrCLS(2, CnTdr)) 'RST("NU_ACTUDNM_KARD")
            On Error Resume Next
            sXb.Add cua, CStr(cua.Bodega)
            Set cua = Nothing
        End If
        ''JACC M6675
        If lnVentana = entrada And ArrCLS(1, CnTdr) = 0 Then
            Dim cuab As New InfXBodega
            cuab.Bodega = ArrCLS(0, CnTdr) 'RST("NU_AUTO_BODE_KARD")
            cuab.Codigo = ArrCLS(4, CnTdr) 'RST("CD_CODI_ARTI")
            cuab.Nombre = ArrCLS(5, CnTdr) 'RST("NO_NOMB_ART")
            If Len(ArrCLS(1, CnTdr)) = 0 Then ArrCLS(1, CnTdr) = "0"
            If Len(ArrCLS(2, CnTdr)) = 0 Then ArrCLS(2, CnTdr) = "1"
            cuab.Saldo.Numerador = CLng(ArrCLS(1, CnTdr)) 'RST("NU_ACTUNMR_KARD")
            cuab.Saldo.Denominador = CLng(ArrCLS(2, CnTdr)) 'RST("NU_ACTUDNM_KARD")
            On Error Resume Next
            sXb.Add cuab, CStr(cuab.Bodega)
            Set cuab = Nothing
        
        End If
        'JACC M6675
    Next
    On Error GoTo 0
CONTI:
NOENC:
FALLO:
End Sub

'Public Sub INTCllSalidasXBodega(Optional Corte As Date)
Public Sub INTCllSalidasXBodega()   'DEPURACION DE CODIGO
    Dim Cmp As String, Dsd As String, Cdc As String
    Dim ArrCLS() As Variant, CnTdr As Integer
    Cmp = "NU_AUTO_BODE_KARD, NU_SALINMR_KARD, NU_SALIDNM_KARD, NU_AUTO_ARTI, " & _
        "CD_CODI_ARTI, NO_NOMB_ARTI, FE_FECH_KARD"
    Dsd = "IN_KARDEX, ARTICULO"
'    Condi = "NU_AUTO_ARTI = NU_AUTO_ARTI_KARD AND NU_APUN_KARD=0 AND NU_AUTO_ARTI_KARD=" & num & " AND FE_PERI_KARD=" & FFechaCon(CStr(prd))
    Cdc = "NU_AUTO_ARTI = NU_AUTO_ARTI_KARD AND NU_APUN_KARD=0 AND NU_AUTO_ARTI_KARD=" & num
    Call BorrarColeccion(sLd) 'sXb, uVb, sLd, nTr, cDb
    ReDim ArrCLS(6, 0)
    Result = LoadMulData(Dsd, Cmp, Cdc, ArrCLS())
    If Result = FAIL Then GoTo CONTI
    If Not Encontro Then GoTo CONTI
    On Error Resume Next
    For CnTdr = 0 To UBound(ArrCLS, 2)
        If ArrCLS(1, CnTdr) <> 0 And CDate(ArrCLS(6, CnTdr)) >= DateAdd("d", 1 - Day(FechaServer), FechaServer) Then 'RST("NU_ACTUNMR_KARD")
            Dim cua As New InfXBodega
            cua.Bodega = ArrCLS(0, CnTdr) 'RST("NU_AUTO_BODE_KARD")
            cua.Codigo = ArrCLS(4, CnTdr) 'RST("CD_CODI_ARTI")
            cua.Nombre = ArrCLS(5, CnTdr) 'RST("NO_NOMB_ART")
            If Len(ArrCLS(1, CnTdr)) = 0 Then ArrCLS(1, CnTdr) = "0"
            If Len(ArrCLS(2, CnTdr)) = 0 Then ArrCLS(2, CnTdr) = "1"
            cua.Salidas.Numerador = CLng(ArrCLS(1, CnTdr)) 'RST("NU_ACTUNMR_KARD")
            cua.Salidas.Denominador = CLng(ArrCLS(2, CnTdr)) 'RST("NU_ACTUDNM_KARD")
            On Error Resume Next
            sLd.Add cua, CStr(cua.Bodega)
            Set cua = Nothing
        End If
    Next
    On Error GoTo 0
CONTI:
NOENC:
FALLO:
End Sub

'Public Sub INTCllEntradasXBodega(Optional Corte As Date)
Public Sub INTCllEntradasXBodega()      'DEPURACION DE CODIGO
    Dim Cmp As String, Dsd As String, Cdc As String
    Dim ArrCLS() As Variant, CnTdr As Integer
    Cmp = "NU_AUTO_BODE_KARD, NU_ENTRNMR_KARD, NU_ENTRDNM_KARD, NU_AUTO_ARTI, " & _
        "CD_CODI_ARTI, NO_NOMB_ARTI, FE_FECH_KARD"
    Dsd = "IN_KARDEX, ARTICULO"
'    Condi = "NU_AUTO_ARTI = NU_AUTO_ARTI_KARD AND NU_APUN_KARD=0 AND NU_AUTO_ARTI_KARD=" & num & " AND FE_PERI_KARD=" & FFechaCon(CStr(prd))
    Cdc = "NU_AUTO_ARTI = NU_AUTO_ARTI_KARD AND NU_APUN_KARD=0 AND NU_AUTO_ARTI_KARD=" & num
    Call BorrarColeccion(nTr) 'sXb, uVb, sLd, nTr, cDb
    ReDim ArrCLS(6, 0)
    Result = LoadMulData(Dsd, Cmp, Cdc, ArrCLS())
    If Result = FAIL Then GoTo CONTI
    If Not Encontro Then GoTo CONTI
    On Error Resume Next
    For CnTdr = 0 To UBound(ArrCLS, 2)
        If ArrCLS(1, CnTdr) <> 0 And CDate(ArrCLS(6, CnTdr)) >= DateAdd("d", 1 - Day(FechaServer), FechaServer) Then 'RST("NU_ACTUNMR_KARD")
            Dim cua As New InfXBodega
            cua.Bodega = ArrCLS(0, CnTdr) 'RST("NU_AUTO_BODE_KARD")
            cua.Codigo = ArrCLS(4, CnTdr) 'RST("CD_CODI_ARTI")
            cua.Nombre = ArrCLS(5, CnTdr) 'RST("NO_NOMB_ART")
            If Len(ArrCLS(1, CnTdr)) = 0 Then ArrCLS(1, CnTdr) = "0"
            If Len(ArrCLS(2, CnTdr)) = 0 Then ArrCLS(2, CnTdr) = "1"
            cua.Entradas.Numerador = CLng(ArrCLS(1, CnTdr)) 'RST("NU_ACTUNMR_KARD")
            cua.Entradas.Denominador = CLng(ArrCLS(2, CnTdr)) 'RST("NU_ACTUDNM_KARD")
            On Error Resume Next
            nTr.Add cua, CStr(cua.Bodega)
            Set cua = Nothing
        End If
    Next
    On Error GoTo 0
CONTI:
NOENC:
FALLO:
End Sub

Public Sub INTCllCodigosDBarra()
    Dim Cmp As String, Dsd As String, Cdc As String
    Dim ArrCLS() As Variant, vContar As Integer
'    Cmp = "NU_AUTO_ARTI_COBA, TX_COBA_COBA, CD_CODI_TERC_COBA"
'IN_CODIGOBAR(NU_AUTO_ARTI_COBA,TX_COBA_COBA,TX_REGI_COBA,TX_CIAL_COBA)
    Cmp = "NU_AUTO_ARTI_COBA,TX_COBA_COBA,TX_REGI_COBA,TX_CIAL_COBA,TX_LABO_COBA"
    Dsd = "IN_CODIGOBAR"
    Cdc = "NU_AUTO_ARTI_COBA=" & num
    Call BorrarColeccion(cDb)
    ReDim ArrCLS(4, 0)
    Result = LoadMulData(Dsd, Cmp, Cdc, ArrCLS())
    If Result = FAIL Then Exit Sub
    If Not Encontro Then Exit Sub
    For vContar = 0 To UBound(ArrCLS, 2)
        Dim cua As New CdgBarra
        cua.AutoArticulo = ArrCLS(0, vContar) 'RST("NU_AUTO_ARTI_COBA")
        cua.Codigo = ArrCLS(1, vContar) 'RST("TX_COBA_COBA")
        cua.Registro = ArrCLS(2, vContar) 'RST("TX_REGI_COBA")
        cua.Comercial = ArrCLS(3, vContar) 'RST("TX_CIAL_COBA")
        cua.Laboratorio = ArrCLS(4, vContar) 'RST("TX_LABO_COBA")
        cDb.Add cua
        Set cua = Nothing
    Next
End Sub

Public Sub INTCllProveedoresXArticulo()
    Dim Cmp As String, Dsd As String, Cdc As String
    Dim ArrCLS() As Variant, vContar As Integer
    Dim vCodigoBarra As CdgBarra, vCuerda As String
''IN_R_CODIGOBAR_TERCERO(TX_COBA_COBA_RCBTE,CD_CODI_TERC_RCBTE,NU_MULT_RCBTE,NU_DIVI_RCBTE,NU_COST_RCBTE,NU_IMPU_RCBTE)

    Call INTCllCodigosDBarra
    Cmp = "TX_COBA_COBA_RCBTE,CD_CODI_TERC_RCBTE,NU_MULT_RCBTE,NU_DIVI_RCBTE,NU_COST_RCBTE,NU_IMPU_RCBTE"
    Dsd = "IN_CODIGOBAR"
    Cdc = "NU_AUTO_ARTI_COBA=" & num
    Call BorrarColeccion(pRv)
    Call INTCllCodigosDBarra
    For Each vCodigoBarra In cDb
        vCuerda = vCuerda & IIf(Len(vCuerda) = 0, "", ",") & Comi & Trim(vCodigoBarra.Codigo) & Comi
    Next
    If Len(vCuerda) = 0 Then Exit Sub
    vCuerda = "(" & vCuerda & ")"
    Cmp = "TX_COBA_COBA_RCBTE,CD_CODI_TERC_RCBTE,NU_MULT_RCBTE,NU_DIVI_RCBTE,NU_COST_RCBTE,NU_IMPU_RCBTE"
    Dsd = "IN_R_CODIGOBAR_TERCERO"
    Cdc = "TX_COBA_COBA_RCBTE IN " & vCuerda
    ReDim ArrCLS(5, 0)
    Result = LoadMulData(Dsd, Cmp, Cdc, ArrCLS())
    If Result = FAIL Then Exit Sub
    If Not Encontro Then Exit Sub
    For vContar = 0 To UBound(ArrCLS, 2)
        Dim cua As New ElProveedor
        If Len(ArrCLS(2, vContar)) = 0 Then ArrCLS(2, vContar) = "0"
        If Len(ArrCLS(3, vContar)) = 0 Then ArrCLS(3, vContar) = "1"
        If Len(ArrCLS(4, vContar)) = 0 Then ArrCLS(4, vContar) = "0"
        If Len(ArrCLS(5, vContar)) = 0 Then ArrCLS(4, vContar) = "0"
        cua.CodigoDeBarra = ArrCLS(0, vContar) 'RST("TX_COBA_COBA_RCBTE")
        cua.CodigoTercero = ArrCLS(1, vContar) 'RST("CD_CODI_TERC_RCBTE")
        cua.Multiplicador = CLng(ArrCLS(2, vContar)) 'RST("NU_MULT_RCBTE")
        cua.Divisor = CLng(ArrCLS(3, vContar)) 'RST("NU_DIVI_RCBTE")
        cua.Costo = CSng(ArrCLS(4, vContar)) 'RST("NU_COST_RCBTE")
        cua.Impuesto = CSng(ArrCLS(5, vContar)) 'RST("NU_IMPU_RCBTE")
        pRv.Add cua, "C" & vContar
        Set cua = Nothing
    Next
End Sub

Public Function GetCllDeProveedores() As Collection
    Set GetCllDeProveedores = pRv
End Function

Public Function GetCllDeCDBarra() As Collection
    Set GetCllDeCDBarra = cDb
End Function

'---------------------------------------------------------------------------------------
' Procedure : BuscarExistencia
' DateTime  : 18/01/2019 17:19
' Author    : daniel_mesa
' Purpose   : DRMG T40674 TRAE LA CANTIDAD DE EXISTENCIA DE UN ARTICULO EN TODAS LAS BODEGAS
'---------------------------------------------------------------------------------------
'
Public Function BuscarExistencia(DbAutoNum As Double) As Double
   Dim StBodegas As String 'Almacena los codigos de las bodegas
   Dim DbCant As Double 'ALMACENA LA CANTIDAD DEL ARTICULO
   Dim VrArr() As Variant 'ALMACENA LOS RESULTADOS EN BD
   Dim DbI As Double 'SE UTILIZA PARA CICLOS FOR


   ReDim VrArr(0, 0)
   Result = LoadMulData("IN_BODEGA ORDER BY NU_AUTO_BODE", "NU_AUTO_BODE", NUL$, VrArr)
   If Result <> FAIL And Encontro Then
      StBodegas = "("
      For DbI = 0 To UBound(VrArr, 2)
         StBodegas = StBodegas & VrArr(0, DbI)
         If DbI <> UBound(VrArr, 2) Then StBodegas = StBodegas & ","
      Next DbI
      StBodegas = StBodegas & ")"
   End If

   Campos = "NU_ACTUNMR_KARD, NU_ACTUDNM_KARD"
   Desde = "IN_KARDEX, ARTICULO, IN_BODEGA, IN_UNDVENTA"

   Condi = "NU_AUTO_KARD IN (SELECT MAX(NU_AUTO_KARD)"    'Kardex por Bodegas
   Condi = Condi & " From IN_KARDEX "
   Condi = Condi & " Where (NU_AUTO_ARTI_KARD = " & DbAutoNum & ")"
   If Len(StBodegas) > 0 Then Condi = Condi & " And NU_AUTO_BODE_KARD IN " & StBodegas
   Condi = Condi & " And (NU_AUTO_KARD>=0) GROUP BY NU_AUTO_BODE_KARD, NU_AUTO_ARTI_KARD)"
   Condi = Condi & " AND NU_ACTUNMR_KARD<>0 AND NU_AUTO_ARTI_KARD=NU_AUTO_ARTI"
   Condi = Condi & " AND NU_AUTO_BODE_KARD=NU_AUTO_BODE AND NU_AUTO_UNVE_ARTI=NU_AUTO_UNVE"
   Condi = Condi & " ORDER BY NU_AUTO_ARTI"

   ReDim VrArr(1, 0)
   DbCant = 0
   Result = LoadMulData(Desde, Campos, Condi, VrArr)
   If Result <> FAIL And Encontro Then
      For DbI = 0 To UBound(VrArr, 2)
         If VrArr(1, DbI) <> 0 Then
            DbCant = DbCant + VrArr(0, DbI) / VrArr(1, DbI)
         End If
      Next DbI
   Else
      DbCant = 0
   End If

   BuscarExistencia = DbCant

End Function
