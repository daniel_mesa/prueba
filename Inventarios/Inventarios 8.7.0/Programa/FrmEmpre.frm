VERSION 5.00
Object = "{C932BA88-4374-101B-A56C-00AA003668DC}#1.1#0"; "MSMASK32.OCX"
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Object = "{0BA686C6-F7D3-101A-993E-0000C0EF6F5E}#1.0#0"; "THREED32.OCX"
Begin VB.Form FrmEmpresa 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Personalizaci�n de la Empresa"
   ClientHeight    =   8265
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   5925
   HelpContextID   =   83
   Icon            =   "FrmEmpre.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   ScaleHeight     =   8265
   ScaleWidth      =   5925
   Begin TabDlg.SSTab SSTab1 
      Height          =   7335
      Left            =   0
      TabIndex        =   44
      Top             =   0
      Width           =   5925
      _ExtentX        =   10451
      _ExtentY        =   12938
      _Version        =   393216
      TabHeight       =   520
      TabCaption(0)   =   "Datos Generales"
      TabPicture(0)   =   "FrmEmpre.frx":058A
      Tab(0).ControlEnabled=   -1  'True
      Tab(0).Control(0)=   "Label12"
      Tab(0).Control(0).Enabled=   0   'False
      Tab(0).Control(1)=   "Lbl_Paciente(11)"
      Tab(0).Control(1).Enabled=   0   'False
      Tab(0).Control(2)=   "Lbl_Paciente(12)"
      Tab(0).Control(2).Enabled=   0   'False
      Tab(0).Control(3)=   "Label11"
      Tab(0).Control(3).Enabled=   0   'False
      Tab(0).Control(4)=   "Label7"
      Tab(0).Control(4).Enabled=   0   'False
      Tab(0).Control(5)=   "Label14"
      Tab(0).Control(5).Enabled=   0   'False
      Tab(0).Control(6)=   "Label13"
      Tab(0).Control(6).Enabled=   0   'False
      Tab(0).Control(7)=   "Label9"
      Tab(0).Control(7).Enabled=   0   'False
      Tab(0).Control(8)=   "Label8"
      Tab(0).Control(8).Enabled=   0   'False
      Tab(0).Control(9)=   "Label2"
      Tab(0).Control(9).Enabled=   0   'False
      Tab(0).Control(10)=   "Label1(0)"
      Tab(0).Control(10).Enabled=   0   'False
      Tab(0).Control(11)=   "Label1(1)"
      Tab(0).Control(11).Enabled=   0   'False
      Tab(0).Control(12)=   "Label1(3)"
      Tab(0).Control(12).Enabled=   0   'False
      Tab(0).Control(13)=   "Label1(4)"
      Tab(0).Control(13).Enabled=   0   'False
      Tab(0).Control(14)=   "Label1(5)"
      Tab(0).Control(14).Enabled=   0   'False
      Tab(0).Control(15)=   "Label1(8)"
      Tab(0).Control(15).Enabled=   0   'False
      Tab(0).Control(16)=   "Label1(10)"
      Tab(0).Control(16).Enabled=   0   'False
      Tab(0).Control(17)=   "Label1(9)"
      Tab(0).Control(17).Enabled=   0   'False
      Tab(0).Control(18)=   "Label15"
      Tab(0).Control(18).Enabled=   0   'False
      Tab(0).Control(19)=   "CmdSeleccion"
      Tab(0).Control(19).Enabled=   0   'False
      Tab(0).Control(20)=   "MskFechaCreacion"
      Tab(0).Control(20).Enabled=   0   'False
      Tab(0).Control(21)=   "TxtCorreo"
      Tab(0).Control(21).Enabled=   0   'False
      Tab(0).Control(22)=   "CboDpto"
      Tab(0).Control(22).Enabled=   0   'False
      Tab(0).Control(23)=   "CboCiudad"
      Tab(0).Control(23).Enabled=   0   'False
      Tab(0).Control(24)=   "TxtIdRepresentante"
      Tab(0).Control(24).Enabled=   0   'False
      Tab(0).Control(25)=   "TxtRepresentante"
      Tab(0).Control(25).Enabled=   0   'False
      Tab(0).Control(26)=   "CboActoAdm"
      Tab(0).Control(26).Enabled=   0   'False
      Tab(0).Control(27)=   "CboClaseEnt"
      Tab(0).Control(27).Enabled=   0   'False
      Tab(0).Control(28)=   "TxtDigVerif"
      Tab(0).Control(28).Enabled=   0   'False
      Tab(0).Control(29)=   "TxtCodUnion"
      Tab(0).Control(29).Enabled=   0   'False
      Tab(0).Control(30)=   "CboTipoIdent"
      Tab(0).Control(30).Enabled=   0   'False
      Tab(0).Control(31)=   "TxtEmpresa(0)"
      Tab(0).Control(31).Enabled=   0   'False
      Tab(0).Control(32)=   "TxtEmpresa(1)"
      Tab(0).Control(32).Enabled=   0   'False
      Tab(0).Control(33)=   "TxtEmpresa(3)"
      Tab(0).Control(33).Enabled=   0   'False
      Tab(0).Control(34)=   "TxtEmpresa(4)"
      Tab(0).Control(34).Enabled=   0   'False
      Tab(0).Control(35)=   "TxtEmpresa(5)"
      Tab(0).Control(35).Enabled=   0   'False
      Tab(0).Control(36)=   "CboCeco"
      Tab(0).Control(36).Enabled=   0   'False
      Tab(0).Control(37)=   "CboTipoE"
      Tab(0).Control(37).Enabled=   0   'False
      Tab(0).Control(38)=   "CboActividad"
      Tab(0).Control(38).Enabled=   0   'False
      Tab(0).Control(39)=   "TxtIndicativo"
      Tab(0).Control(39).Enabled=   0   'False
      Tab(0).ControlCount=   40
      TabCaption(1)   =   "Datos Tributarios"
      TabPicture(1)   =   "FrmEmpre.frx":05A6
      Tab(1).ControlEnabled=   0   'False
      Tab(1).Control(0)=   "LblPersona"
      Tab(1).Control(0).Enabled=   0   'False
      Tab(1).Control(1)=   "Label3"
      Tab(1).Control(1).Enabled=   0   'False
      Tab(1).Control(2)=   "CboTipoPer"
      Tab(1).Control(2).Enabled=   0   'False
      Tab(1).Control(3)=   "CboRegimenIVA"
      Tab(1).Control(3).Enabled=   0   'False
      Tab(1).Control(4)=   "Frame1"
      Tab(1).Control(4).Enabled=   0   'False
      Tab(1).Control(5)=   "Frame2"
      Tab(1).Control(5).Enabled=   0   'False
      Tab(1).Control(6)=   "Frame3"
      Tab(1).Control(6).Enabled=   0   'False
      Tab(1).Control(7)=   "Frame4"
      Tab(1).Control(7).Enabled=   0   'False
      Tab(1).Control(8)=   "ChkIVADed"
      Tab(1).Control(8).Enabled=   0   'False
      Tab(1).Control(9)=   "FrmIVAXART"
      Tab(1).Control(9).Enabled=   0   'False
      Tab(1).Control(10)=   "FrmDatos"
      Tab(1).Control(10).Enabled=   0   'False
      Tab(1).ControlCount=   11
      TabCaption(2)   =   "Fac. Electr�nica"
      TabPicture(2)   =   "FrmEmpre.frx":05C2
      Tab(2).ControlEnabled=   0   'False
      Tab(2).Control(0)=   "SSTabFael"
      Tab(2).Control(0).Enabled=   0   'False
      Tab(2).Control(1)=   "FrmRangAutor"
      Tab(2).Control(1).Enabled=   0   'False
      Tab(2).ControlCount=   2
      Begin VB.Frame FrmRangAutor 
         Caption         =   "Rangos Autorizados"
         Height          =   2415
         Left            =   -74920
         TabIndex        =   84
         Top             =   480
         Width           =   5770
         Begin Threed.SSCommand ScmdNewRang 
            Height          =   495
            Left            =   5160
            TabIndex        =   110
            ToolTipText     =   "Guardar Nuevo Rango"
            Top             =   1440
            Width           =   495
            _Version        =   65536
            _ExtentX        =   873
            _ExtentY        =   873
            _StockProps     =   78
            Picture         =   "FrmEmpre.frx":05DE
         End
         Begin Threed.SSCommand ScmdAct 
            Height          =   495
            Left            =   4560
            TabIndex        =   109
            ToolTipText     =   "Actualizar"
            Top             =   1440
            Width           =   495
            _Version        =   65536
            _ExtentX        =   873
            _ExtentY        =   873
            _StockProps     =   78
            Picture         =   "FrmEmpre.frx":0CA8
         End
         Begin VB.TextBox TxtAutori 
            Height          =   285
            Left            =   1080
            MaxLength       =   30
            TabIndex        =   88
            Top             =   960
            Width           =   4575
         End
         Begin VB.TextBox TxtConFact 
            Height          =   285
            Left            =   2400
            TabIndex        =   92
            Top             =   1680
            Width           =   1095
         End
         Begin VB.TextBox TxtCTecni 
            Enabled         =   0   'False
            Height          =   285
            Left            =   1440
            MaxLength       =   50
            TabIndex        =   89
            Top             =   2400
            Visible         =   0   'False
            Width           =   4215
         End
         Begin VB.TextBox TxtPreFijo 
            Height          =   285
            Left            =   4920
            MaxLength       =   4
            TabIndex        =   87
            Top             =   360
            Width           =   735
         End
         Begin VB.TextBox TxtFactFin 
            Height          =   285
            Left            =   3240
            MaxLength       =   10
            TabIndex        =   86
            Top             =   360
            Width           =   1095
         End
         Begin VB.TextBox TxtFactIni 
            Height          =   285
            Left            =   1080
            MaxLength       =   10
            TabIndex        =   85
            Top             =   360
            Width           =   1095
         End
         Begin MSMask.MaskEdBox MskFechAut 
            Height          =   285
            Left            =   1080
            TabIndex        =   90
            ToolTipText     =   "d�a/mes/a�o"
            Top             =   1440
            Width           =   1170
            _ExtentX        =   2064
            _ExtentY        =   503
            _Version        =   393216
            MaxLength       =   10
            Format          =   "dd/mm/yyyy"
            Mask            =   "##/##/####"
            PromptChar      =   " "
         End
         Begin MSMask.MaskEdBox MskFechFin 
            Height          =   285
            Left            =   1080
            TabIndex        =   91
            ToolTipText     =   "d�a/mes/a�o"
            Top             =   1920
            Width           =   1170
            _ExtentX        =   2064
            _ExtentY        =   503
            _Version        =   393216
            MaxLength       =   10
            Format          =   "dd/mm/yyyy"
            Mask            =   "##/##/####"
            PromptChar      =   " "
         End
         Begin VB.Label LblFechFin 
            Caption         =   "Fecha Fin:"
            Height          =   255
            Left            =   120
            TabIndex        =   118
            Top             =   1920
            Width           =   855
         End
         Begin VB.Label LblFechAut 
            Caption         =   "Feca Aut:"
            Height          =   255
            Left            =   120
            TabIndex        =   117
            Top             =   1440
            Width           =   735
         End
         Begin VB.Label LblConFact 
            Caption         =   "Cons. Factura:"
            Height          =   255
            Left            =   2400
            TabIndex        =   116
            Top             =   1440
            Width           =   1095
         End
         Begin VB.Label LblCTecnico 
            Caption         =   "Clave T�cnica:"
            Height          =   255
            Left            =   120
            TabIndex        =   115
            Top             =   2400
            Visible         =   0   'False
            Width           =   1185
         End
         Begin VB.Label LblAutori 
            Caption         =   "No. Autorizaci�n:"
            Height          =   495
            Left            =   120
            TabIndex        =   114
            Top             =   840
            Width           =   975
         End
         Begin VB.Label LblPrefijo 
            Caption         =   "Prefijo:"
            Height          =   255
            Left            =   4390
            TabIndex        =   113
            Top             =   360
            Width           =   495
         End
         Begin VB.Label LblFactFin 
            Caption         =   "Factura Fin:"
            Height          =   255
            Left            =   2360
            TabIndex        =   112
            Top             =   360
            Width           =   855
         End
         Begin VB.Label LblFactIni 
            Caption         =   "Factura Ini:"
            Height          =   255
            Left            =   120
            TabIndex        =   111
            Top             =   360
            Width           =   855
         End
      End
      Begin VB.Frame FrmDatos 
         Caption         =   "NIIF"
         Height          =   945
         Left            =   -74880
         TabIndex        =   82
         Top             =   3120
         Width           =   5730
         Begin VB.CheckBox ChkGenMovNiif 
            Alignment       =   1  'Right Justify
            Caption         =   "&Genera Movimiento NIIF"
            Height          =   195
            Left            =   120
            TabIndex        =   34
            Top             =   240
            Width           =   2055
         End
         Begin VB.OptionButton OptVMercado 
            Caption         =   "Valor del Mercado"
            Height          =   255
            Left            =   2760
            TabIndex        =   36
            Top             =   600
            Width           =   1695
         End
         Begin VB.OptionButton OptUCosto 
            Caption         =   "Ultimo Costo"
            Height          =   255
            Left            =   1320
            TabIndex        =   35
            Top             =   600
            Value           =   -1  'True
            Width           =   1695
         End
         Begin VB.Label LblCostoNIIF 
            Caption         =   "Costo NIIF:"
            Height          =   255
            Left            =   120
            TabIndex        =   83
            Top             =   600
            Width           =   855
         End
      End
      Begin VB.Frame FrmIVAXART 
         Caption         =   "Por Articulo"
         Enabled         =   0   'False
         Height          =   495
         Left            =   -71280
         TabIndex        =   23
         Top             =   840
         Width           =   2055
         Begin VB.OptionButton OptIvaSi 
            Caption         =   "No"
            Height          =   195
            Index           =   1
            Left            =   1320
            TabIndex        =   25
            Top             =   240
            Width           =   615
         End
         Begin VB.OptionButton OptIvaSi 
            Caption         =   "Si"
            Height          =   195
            Index           =   0
            Left            =   720
            TabIndex        =   24
            Top             =   240
            Value           =   -1  'True
            Width           =   495
         End
      End
      Begin VB.TextBox TxtIndicativo 
         Height          =   315
         Left            =   960
         MaxLength       =   3
         TabIndex        =   14
         Top             =   5520
         Width           =   495
      End
      Begin VB.ComboBox CboActividad 
         Height          =   315
         Left            =   960
         Style           =   2  'Dropdown List
         TabIndex        =   19
         Top             =   6840
         Width           =   4815
      End
      Begin VB.CheckBox ChkIVADed 
         Alignment       =   1  'Right Justify
         Caption         =   "&IVA Deducible"
         Height          =   255
         Left            =   -71280
         TabIndex        =   22
         Top             =   480
         Width           =   1455
      End
      Begin VB.Frame Frame4 
         Caption         =   "&Agente Retenci�n de RENTA"
         Height          =   615
         Left            =   -74880
         TabIndex        =   76
         Top             =   6120
         Visible         =   0   'False
         Width           =   5730
         Begin VB.CheckBox ChkRegSimplif 
            Alignment       =   1  'Right Justify
            Caption         =   "&Regimen SIMPLIFICADO"
            Height          =   255
            Index           =   2
            Left            =   3120
            TabIndex        =   78
            Top             =   250
            Width           =   2175
         End
         Begin VB.CheckBox ChkRegComun 
            Alignment       =   1  'Right Justify
            Caption         =   "&Regimen COMUN"
            Height          =   255
            Index           =   2
            Left            =   720
            TabIndex        =   77
            Top             =   250
            Width           =   1815
         End
      End
      Begin VB.Frame Frame3 
         Caption         =   "&Agente Retenci�n de ICA"
         Height          =   615
         Left            =   -74880
         TabIndex        =   73
         Top             =   5400
         Visible         =   0   'False
         Width           =   5730
         Begin VB.CheckBox ChkRegSimplif 
            Alignment       =   1  'Right Justify
            Caption         =   "&Regimen SIMPLIFICADO"
            Height          =   255
            Index           =   1
            Left            =   3120
            TabIndex        =   75
            Top             =   250
            Width           =   2175
         End
         Begin VB.CheckBox ChkRegComun 
            Alignment       =   1  'Right Justify
            Caption         =   "&Regimen COMUN"
            Height          =   255
            Index           =   1
            Left            =   720
            TabIndex        =   74
            Top             =   240
            Width           =   1815
         End
      End
      Begin VB.Frame Frame2 
         Caption         =   "&Agente Retenci�n de IVA"
         Height          =   615
         Left            =   -74880
         TabIndex        =   70
         Top             =   4680
         Visible         =   0   'False
         Width           =   5730
         Begin VB.CheckBox ChkRegComun 
            Alignment       =   1  'Right Justify
            Caption         =   "&Regimen COMUN"
            Height          =   255
            Index           =   0
            Left            =   720
            TabIndex        =   72
            Top             =   250
            Width           =   1815
         End
         Begin VB.CheckBox ChkRegSimplif 
            Alignment       =   1  'Right Justify
            Caption         =   "&Regimen SIMPLIFICADO"
            Height          =   255
            Index           =   0
            Left            =   3120
            TabIndex        =   71
            Top             =   250
            Width           =   2175
         End
      End
      Begin VB.Frame Frame1 
         Height          =   1815
         Left            =   -74880
         TabIndex        =   26
         Top             =   1320
         Width           =   5730
         Begin VB.TextBox TxtResRENTA 
            Height          =   285
            Left            =   4300
            MaxLength       =   15
            TabIndex        =   33
            Top             =   1440
            Width           =   1335
         End
         Begin VB.TextBox TxtResICA 
            Height          =   285
            Left            =   4300
            MaxLength       =   15
            TabIndex        =   31
            Top             =   1020
            Width           =   1335
         End
         Begin VB.TextBox TxtResIVA 
            Height          =   285
            Left            =   4305
            MaxLength       =   15
            TabIndex        =   29
            Top             =   580
            Width           =   1335
         End
         Begin VB.CheckBox ChkAutoRetRenta 
            Alignment       =   1  'Right Justify
            Caption         =   "&AutoRetenedor de RENTA"
            Height          =   375
            Left            =   120
            TabIndex        =   32
            Top             =   1320
            Width           =   1935
         End
         Begin VB.CheckBox ChkAutoRetIca 
            Alignment       =   1  'Right Justify
            Caption         =   "&AutoRetenedor de ICA"
            Height          =   255
            Left            =   120
            TabIndex        =   30
            Top             =   960
            Width           =   1935
         End
         Begin VB.CheckBox ChkAutoRetIva 
            Alignment       =   1  'Right Justify
            Caption         =   "&AutoRetenedor de IVA"
            Height          =   255
            Left            =   120
            TabIndex        =   28
            Top             =   600
            Width           =   1935
         End
         Begin VB.OptionButton OptNo 
            Caption         =   "No"
            Height          =   255
            Left            =   2400
            TabIndex        =   66
            Top             =   240
            Value           =   -1  'True
            Width           =   615
         End
         Begin VB.OptionButton OptSi 
            Caption         =   "Si"
            Height          =   255
            Left            =   1800
            TabIndex        =   27
            Top             =   240
            Width           =   495
         End
         Begin VB.Label Label4 
            Caption         =   "&Res. Autoretendor de RENTA"
            Height          =   435
            Index           =   3
            Left            =   2280
            TabIndex        =   69
            Top             =   1320
            Width           =   1515
         End
         Begin VB.Label Label4 
            AutoSize        =   -1  'True
            Caption         =   "&Res. Autoretendor de ICA"
            Height          =   195
            Index           =   2
            Left            =   2280
            TabIndex        =   68
            Top             =   960
            Width           =   1815
         End
         Begin VB.Label Label4 
            AutoSize        =   -1  'True
            Caption         =   "&Res. Autoretendor de IVA"
            Height          =   195
            Index           =   1
            Left            =   2280
            TabIndex        =   67
            Top             =   600
            Width           =   1815
         End
         Begin VB.Label Label4 
            AutoSize        =   -1  'True
            Caption         =   "&Gran Contribuyente :"
            Height          =   195
            Index           =   0
            Left            =   120
            TabIndex        =   65
            Top             =   240
            Width           =   1455
         End
      End
      Begin VB.ComboBox CboRegimenIVA 
         Height          =   315
         ItemData        =   "FrmEmpre.frx":18FA
         Left            =   -73560
         List            =   "FrmEmpre.frx":1904
         TabIndex        =   21
         Top             =   960
         Width           =   1575
      End
      Begin VB.ComboBox CboTipoPer 
         Height          =   315
         ItemData        =   "FrmEmpre.frx":191D
         Left            =   -73560
         List            =   "FrmEmpre.frx":1927
         TabIndex        =   20
         Top             =   480
         Width           =   1575
      End
      Begin VB.ComboBox CboTipoE 
         Height          =   315
         ItemData        =   "FrmEmpre.frx":193E
         Left            =   3360
         List            =   "FrmEmpre.frx":194B
         Style           =   2  'Dropdown List
         TabIndex        =   5
         Top             =   1320
         Width           =   2055
      End
      Begin VB.ComboBox CboCeco 
         Height          =   315
         Left            =   960
         Style           =   2  'Dropdown List
         TabIndex        =   18
         Top             =   6360
         Width           =   4815
      End
      Begin VB.TextBox TxtEmpresa 
         Height          =   285
         Index           =   5
         Left            =   120
         MaxLength       =   40
         TabIndex        =   11
         Top             =   4440
         Width           =   5535
      End
      Begin VB.TextBox TxtEmpresa 
         Height          =   285
         Index           =   4
         Left            =   4200
         MaxLength       =   10
         TabIndex        =   16
         Top             =   5520
         Width           =   1575
      End
      Begin VB.TextBox TxtEmpresa 
         Height          =   285
         Index           =   3
         Left            =   2280
         MaxLength       =   10
         TabIndex        =   15
         Top             =   5520
         Width           =   1455
      End
      Begin VB.TextBox TxtEmpresa 
         Height          =   285
         Index           =   1
         Left            =   1200
         MaxLength       =   16
         TabIndex        =   2
         Top             =   720
         Width           =   1695
      End
      Begin VB.TextBox TxtEmpresa 
         Height          =   315
         Index           =   0
         Left            =   120
         MaxLength       =   60
         TabIndex        =   8
         Top             =   2640
         Width           =   5580
      End
      Begin VB.ComboBox CboTipoIdent 
         Height          =   315
         ItemData        =   "FrmEmpre.frx":196F
         Left            =   120
         List            =   "FrmEmpre.frx":197F
         Style           =   2  'Dropdown List
         TabIndex        =   1
         Top             =   705
         Width           =   975
      End
      Begin VB.TextBox TxtCodUnion 
         Height          =   315
         Left            =   3360
         MaxLength       =   10
         TabIndex        =   3
         Top             =   720
         Width           =   2055
      End
      Begin VB.TextBox TxtDigVerif 
         Height          =   315
         Left            =   2880
         Locked          =   -1  'True
         MaxLength       =   1
         TabIndex        =   45
         Top             =   720
         Width           =   270
      End
      Begin VB.ComboBox CboClaseEnt 
         Height          =   315
         ItemData        =   "FrmEmpre.frx":1993
         Left            =   120
         List            =   "FrmEmpre.frx":19A5
         Style           =   2  'Dropdown List
         TabIndex        =   4
         Top             =   1320
         Width           =   3135
      End
      Begin VB.ComboBox CboActoAdm 
         Height          =   315
         ItemData        =   "FrmEmpre.frx":19DD
         Left            =   120
         List            =   "FrmEmpre.frx":19F8
         Style           =   2  'Dropdown List
         TabIndex        =   6
         Top             =   1935
         Width           =   3135
      End
      Begin VB.TextBox TxtRepresentante 
         Height          =   315
         Left            =   120
         MaxLength       =   60
         TabIndex        =   10
         Top             =   3795
         Width           =   5580
      End
      Begin VB.TextBox TxtIdRepresentante 
         Height          =   315
         Left            =   120
         MaxLength       =   16
         TabIndex        =   9
         Top             =   3240
         Width           =   5580
      End
      Begin VB.ComboBox CboCiudad 
         Height          =   315
         ItemData        =   "FrmEmpre.frx":1A37
         Left            =   3000
         List            =   "FrmEmpre.frx":1A39
         Style           =   2  'Dropdown List
         TabIndex        =   13
         Tag             =   "                 "
         Top             =   5040
         Width           =   2775
      End
      Begin VB.ComboBox CboDpto 
         Height          =   315
         ItemData        =   "FrmEmpre.frx":1A3B
         Left            =   120
         List            =   "FrmEmpre.frx":1A3D
         Style           =   2  'Dropdown List
         TabIndex        =   12
         Tag             =   "                 "
         Top             =   5040
         Width           =   2775
      End
      Begin VB.TextBox TxtCorreo 
         Height          =   315
         Left            =   960
         MaxLength       =   30
         TabIndex        =   17
         Top             =   5880
         Width           =   4815
      End
      Begin MSMask.MaskEdBox MskFechaCreacion 
         Height          =   285
         Left            =   3480
         TabIndex        =   7
         ToolTipText     =   "d�a/mes/a�o"
         Top             =   1935
         Width           =   1170
         _ExtentX        =   2064
         _ExtentY        =   503
         _Version        =   393216
         MaxLength       =   10
         Format          =   "dd/mm/yyyy"
         Mask            =   "##/##/####"
         PromptChar      =   " "
      End
      Begin Threed.SSCommand CmdSeleccion 
         Height          =   255
         Left            =   5400
         TabIndex        =   80
         ToolTipText     =   "Seleccion de Cuentas"
         Top             =   6840
         Visible         =   0   'False
         Width           =   495
         _Version        =   65536
         _ExtentX        =   873
         _ExtentY        =   450
         _StockProps     =   78
         BevelWidth      =   0
         Outline         =   0   'False
         Picture         =   "FrmEmpre.frx":1A3F
      End
      Begin TabDlg.SSTab SSTabFael 
         Height          =   4215
         Left            =   -74880
         TabIndex        =   119
         Top             =   3000
         Width           =   5655
         _ExtentX        =   9975
         _ExtentY        =   7435
         _Version        =   393216
         Tabs            =   4
         TabsPerRow      =   4
         TabHeight       =   520
         TabCaption(0)   =   "Ubicac�on"
         TabPicture(0)   =   "FrmEmpre.frx":23F1
         Tab(0).ControlEnabled=   -1  'True
         Tab(0).Control(0)=   "FrmUrl"
         Tab(0).Control(0).Enabled=   0   'False
         Tab(0).ControlCount=   1
         TabCaption(1)   =   "Encabezado"
         TabPicture(1)   =   "FrmEmpre.frx":240D
         Tab(1).ControlEnabled=   0   'False
         Tab(1).Control(0)=   "FrmPersonalizacion"
         Tab(1).ControlCount=   1
         TabCaption(2)   =   "Software"
         TabPicture(2)   =   "FrmEmpre.frx":2429
         Tab(2).ControlEnabled=   0   'False
         Tab(2).Control(0)=   "FrmUsu"
         Tab(2).ControlCount=   1
         TabCaption(3)   =   "URL"
         TabPicture(3)   =   "FrmEmpre.frx":2445
         Tab(3).ControlEnabled=   0   'False
         Tab(3).Control(0)=   "FrmDian"
         Tab(3).Control(0).Enabled=   0   'False
         Tab(3).ControlCount=   1
         Begin VB.Frame FrmDian 
            Height          =   3735
            Left            =   -74880
            TabIndex        =   132
            Top             =   360
            Width           =   5415
            Begin VB.TextBox TxtUrlEnv 
               Height          =   285
               Left            =   120
               TabIndex        =   106
               Top             =   480
               Width           =   5055
            End
            Begin VB.TextBox TxtUrlCons 
               Height          =   285
               Left            =   120
               TabIndex        =   107
               Top             =   1200
               Width           =   5055
            End
            Begin VB.Label LblUrlEnv 
               Caption         =   "URL Envio:"
               Height          =   255
               Left            =   120
               TabIndex        =   134
               Top             =   240
               Width           =   975
            End
            Begin VB.Label LblUrlCons 
               Caption         =   "URL Consulta:"
               Height          =   255
               Left            =   120
               TabIndex        =   133
               Top             =   960
               Width           =   1095
            End
         End
         Begin VB.Frame FrmUsu 
            Height          =   3735
            Left            =   -74880
            TabIndex        =   129
            Top             =   360
            Width           =   5415
            Begin VB.TextBox TxtCont 
               Height          =   285
               IMEMode         =   3  'DISABLE
               Left            =   120
               MaxLength       =   100
               PasswordChar    =   "*"
               TabIndex        =   104
               Top             =   1920
               Width           =   5055
            End
            Begin VB.CheckBox ChkVer 
               Height          =   195
               Index           =   1
               Left            =   120
               TabIndex        =   105
               Top             =   2280
               Width           =   255
            End
            Begin VB.TextBox TxtPin 
               Height          =   285
               IMEMode         =   3  'DISABLE
               Left            =   120
               MaxLength       =   100
               TabIndex        =   103
               Top             =   1200
               Width           =   5055
            End
            Begin VB.TextBox TxtIdSoft 
               Height          =   285
               Left            =   120
               MaxLength       =   100
               TabIndex        =   102
               Top             =   480
               Width           =   5055
            End
            Begin VB.Label LblCon2 
               Caption         =   "Contrase�a Software:"
               Height          =   255
               Left            =   120
               TabIndex        =   136
               Top             =   1680
               Width           =   1815
            End
            Begin VB.Label LblVer 
               Caption         =   "Mostrar contrase�a"
               Height          =   255
               Index           =   1
               Left            =   360
               TabIndex        =   135
               Top             =   2280
               Width           =   1455
            End
            Begin VB.Label LblCon 
               Caption         =   "PIN Software:"
               Height          =   255
               Left            =   120
               TabIndex        =   131
               Top             =   960
               Width           =   1215
            End
            Begin VB.Label LblUsu 
               Caption         =   "Id Software:"
               Height          =   255
               Left            =   120
               TabIndex        =   130
               Top             =   240
               Width           =   1215
            End
         End
         Begin VB.Frame FrmPersonalizacion 
            Caption         =   "Personalizacion"
            Height          =   3735
            Left            =   -74880
            TabIndex        =   125
            Top             =   360
            Width           =   5415
            Begin VB.TextBox TxtEncabez 
               Height          =   735
               Left            =   120
               MaxLength       =   255
               MultiLine       =   -1  'True
               ScrollBars      =   2  'Vertical
               TabIndex        =   97
               Top             =   600
               Width           =   3015
            End
            Begin VB.TextBox TxtPie 
               Height          =   855
               Index           =   0
               Left            =   120
               MaxLength       =   1000
               MultiLine       =   -1  'True
               ScrollBars      =   2  'Vertical
               TabIndex        =   98
               Top             =   1680
               Width           =   3015
            End
            Begin VB.TextBox TxtPie 
               Height          =   855
               Index           =   1
               Left            =   120
               MaxLength       =   1000
               MultiLine       =   -1  'True
               ScrollBars      =   2  'Vertical
               TabIndex        =   99
               Top             =   2640
               Width           =   3015
            End
            Begin VB.Frame FrmLogo 
               Caption         =   "Logo"
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   2655
               Left            =   3240
               TabIndex        =   126
               Top             =   120
               Width           =   2055
               Begin Threed.SSCommand ScmdLogo 
                  Height          =   375
                  Index           =   0
                  Left            =   120
                  TabIndex        =   100
                  ToolTipText     =   "Guardar Nuevo Rango"
                  Top             =   2160
                  Width           =   855
                  _Version        =   65536
                  _ExtentX        =   1508
                  _ExtentY        =   661
                  _StockProps     =   78
                  Caption         =   "Examinar"
                  Picture         =   "FrmEmpre.frx":2461
               End
               Begin Threed.SSCommand ScmdLogo 
                  Height          =   375
                  Index           =   1
                  Left            =   1080
                  TabIndex        =   101
                  ToolTipText     =   "Guardar Nuevo Rango"
                  Top             =   2160
                  Width           =   855
                  _Version        =   65536
                  _ExtentX        =   1508
                  _ExtentY        =   661
                  _StockProps     =   78
                  Caption         =   "Limpiar"
                  Picture         =   "FrmEmpre.frx":247D
               End
               Begin VB.Image ImgLogo 
                  BorderStyle     =   1  'Fixed Single
                  Height          =   1815
                  Left            =   120
                  OLEDragMode     =   1  'Automatic
                  OLEDropMode     =   2  'Automatic
                  Stretch         =   -1  'True
                  Top             =   240
                  Width           =   1815
               End
            End
            Begin VB.Label LblEncabez 
               Caption         =   "Encabezado:"
               Height          =   255
               Left            =   120
               TabIndex        =   128
               Top             =   360
               Width           =   975
            End
            Begin VB.Label LblPie 
               Caption         =   "Pie de p�gina:"
               Height          =   255
               Left            =   120
               TabIndex        =   127
               Top             =   1440
               Width           =   1095
            End
         End
         Begin VB.Frame FrmUrl 
            Caption         =   "Parametrizar ubicaci�n"
            Height          =   3615
            Left            =   120
            TabIndex        =   120
            Top             =   480
            Width           =   5415
            Begin VB.CheckBox ChkVer 
               Height          =   195
               Index           =   0
               Left            =   3600
               TabIndex        =   96
               Top             =   1680
               Width           =   255
            End
            Begin VB.TextBox TxtPassword 
               Height          =   285
               IMEMode         =   3  'DISABLE
               Left            =   2160
               MaxLength       =   50
               PasswordChar    =   "*"
               TabIndex        =   95
               Top             =   1680
               Width           =   1335
            End
            Begin VB.TextBox TxtUrl 
               Height          =   285
               Index           =   1
               Left            =   120
               TabIndex        =   94
               Top             =   1200
               Width           =   5175
            End
            Begin VB.TextBox TxtUrl 
               Height          =   285
               Index           =   0
               Left            =   120
               TabIndex        =   93
               Top             =   600
               Width           =   5175
            End
            Begin VB.Label LblVer 
               Caption         =   "Mostrar contrase�a"
               Height          =   255
               Index           =   0
               Left            =   3840
               TabIndex        =   124
               Top             =   1680
               Width           =   1455
            End
            Begin VB.Label LblPassword 
               Caption         =   "Contrase�a del Certificado:"
               Height          =   255
               Left            =   120
               TabIndex        =   123
               Top             =   1680
               Width           =   2055
            End
            Begin VB.Label LnlCert 
               Caption         =   "Certificado URL:"
               Height          =   255
               Left            =   120
               TabIndex        =   122
               Top             =   960
               Width           =   1215
            End
            Begin VB.Label LnlFactElect 
               Caption         =   " Facturacion Electronica:"
               Height          =   255
               Left            =   120
               TabIndex        =   121
               Top             =   360
               Width           =   2175
            End
         End
      End
      Begin VB.Label Label15 
         Caption         =   "Indicativo:"
         Height          =   255
         Left            =   120
         TabIndex        =   81
         Top             =   5520
         Width           =   735
      End
      Begin VB.Label Label1 
         Caption         =   "Actividad Economica"
         Height          =   435
         Index           =   9
         Left            =   120
         TabIndex        =   79
         Top             =   6720
         Width           =   945
      End
      Begin VB.Label Label3 
         AutoSize        =   -1  'True
         Caption         =   "&Regimen de IVA"
         Height          =   195
         Left            =   -74880
         TabIndex        =   64
         Top             =   960
         Width           =   1155
      End
      Begin VB.Label LblPersona 
         AutoSize        =   -1  'True
         Caption         =   "&Tipo Persona"
         Height          =   195
         Left            =   -74880
         TabIndex        =   63
         Top             =   480
         Width           =   945
      End
      Begin VB.Label Label1 
         AutoSize        =   -1  'True
         Caption         =   "&Tipo Empresa:"
         Height          =   195
         Index           =   10
         Left            =   3360
         TabIndex        =   62
         Top             =   1080
         Width           =   1020
      End
      Begin VB.Label Label1 
         Caption         =   "C&entro de Costo"
         Height          =   435
         Index           =   8
         Left            =   120
         TabIndex        =   61
         Top             =   6240
         Width           =   705
      End
      Begin VB.Label Label1 
         AutoSize        =   -1  'True
         Caption         =   "&Direcci�n:"
         Height          =   195
         Index           =   5
         Left            =   120
         TabIndex        =   60
         Top             =   4200
         Width           =   720
      End
      Begin VB.Label Label1 
         AutoSize        =   -1  'True
         Caption         =   "&Fax"
         Height          =   195
         Index           =   4
         Left            =   3840
         TabIndex        =   59
         Top             =   5520
         Width           =   255
      End
      Begin VB.Label Label1 
         AutoSize        =   -1  'True
         Caption         =   "&Tel�fono"
         Height          =   195
         Index           =   3
         Left            =   1560
         TabIndex        =   58
         Top             =   5520
         Width           =   630
      End
      Begin VB.Label Label1 
         AutoSize        =   -1  'True
         Caption         =   "&Numero"
         Height          =   195
         Index           =   1
         Left            =   1200
         TabIndex        =   57
         Top             =   480
         Width           =   555
      End
      Begin VB.Label Label1 
         Caption         =   "Nombre Entidad"
         Height          =   195
         Index           =   0
         Left            =   120
         TabIndex        =   56
         Top             =   2400
         Width           =   1215
      End
      Begin VB.Label Label2 
         AutoSize        =   -1  'True
         Caption         =   "Tipo Doc."
         Height          =   195
         Left            =   120
         TabIndex        =   55
         Top             =   480
         Width           =   705
      End
      Begin VB.Label Label8 
         Alignment       =   2  'Center
         Caption         =   "C�digo Uni�n"
         Height          =   255
         Left            =   3360
         TabIndex        =   54
         Top             =   480
         Width           =   975
      End
      Begin VB.Label Label9 
         Caption         =   "Clase Entidad"
         Height          =   255
         Left            =   120
         TabIndex        =   53
         Top             =   1080
         Width           =   1095
      End
      Begin VB.Label Label13 
         Caption         =   "Acto Administrativo"
         Height          =   255
         Left            =   120
         TabIndex        =   52
         Top             =   1680
         Width           =   1455
      End
      Begin VB.Label Label14 
         Caption         =   "Fecha Creaci�n"
         Height          =   255
         Left            =   3480
         TabIndex        =   51
         Top             =   1680
         Width           =   1215
      End
      Begin VB.Label Label7 
         Caption         =   "Nombre  Representante"
         Height          =   315
         Left            =   120
         TabIndex        =   50
         Top             =   3585
         Width           =   1770
      End
      Begin VB.Label Label11 
         Caption         =   "Id. Representante"
         Height          =   255
         Left            =   120
         TabIndex        =   49
         Top             =   3000
         Width           =   1335
      End
      Begin VB.Label Lbl_Paciente 
         Caption         =   "Ciudad "
         Height          =   195
         Index           =   12
         Left            =   3000
         TabIndex        =   48
         Tag             =   "Cdad Proced:"
         Top             =   4800
         Width           =   900
      End
      Begin VB.Label Lbl_Paciente 
         BackColor       =   &H00C0C0C0&
         BackStyle       =   0  'Transparent
         Caption         =   "Departamento"
         Height          =   195
         Index           =   11
         Left            =   120
         TabIndex        =   47
         Top             =   4800
         Width           =   1065
      End
      Begin VB.Label Label12 
         Caption         =   "E-mail"
         Height          =   255
         Left            =   120
         TabIndex        =   46
         Top             =   5910
         Width           =   975
      End
   End
   Begin VB.TextBox TxtEmpresa 
      Height          =   285
      Index           =   2
      Left            =   5280
      MaxLength       =   3
      TabIndex        =   40
      Top             =   7800
      Visible         =   0   'False
      Width           =   615
   End
   Begin VB.TextBox TxtEmpresa 
      Height          =   285
      Index           =   6
      Left            =   720
      MaxLength       =   20
      TabIndex        =   39
      Top             =   7800
      Visible         =   0   'False
      Width           =   975
   End
   Begin VB.TextBox TxtEmpresa 
      Height          =   285
      Index           =   7
      Left            =   720
      MaxLength       =   20
      TabIndex        =   0
      Top             =   8040
      Visible         =   0   'False
      Width           =   1095
   End
   Begin Threed.SSCommand Cmd_Botones 
      Height          =   705
      HelpContextID   =   120
      Index           =   1
      Left            =   3360
      TabIndex        =   38
      ToolTipText     =   "Salir"
      Top             =   7440
      Width           =   735
      _Version        =   65536
      _ExtentX        =   1296
      _ExtentY        =   1244
      _StockProps     =   78
      Caption         =   "&SALIR"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   6
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Picture         =   "FrmEmpre.frx":2499
   End
   Begin Threed.SSCommand Cmd_Botones 
      Height          =   705
      HelpContextID   =   120
      Index           =   0
      Left            =   1680
      TabIndex        =   37
      ToolTipText     =   "Guardar"
      Top             =   7440
      Width           =   735
      _Version        =   65536
      _ExtentX        =   1296
      _ExtentY        =   1244
      _StockProps     =   78
      Caption         =   "&GUARDAR"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   6
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Picture         =   "FrmEmpre.frx":2B63
   End
   Begin Threed.SSCommand Cmd_Botones 
      Height          =   705
      HelpContextID   =   120
      Index           =   2
      Left            =   2520
      TabIndex        =   108
      ToolTipText     =   "Salir"
      Top             =   7440
      Width           =   735
      _Version        =   65536
      _ExtentX        =   1296
      _ExtentY        =   1244
      _StockProps     =   78
      Caption         =   "&EMAIL"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   6
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Picture         =   "FrmEmpre.frx":322D
   End
   Begin VB.Label Label1 
      AutoSize        =   -1  'True
      Caption         =   "&Identificaci�n:"
      Height          =   195
      Index           =   2
      Left            =   4560
      TabIndex        =   43
      Top             =   7800
      Visible         =   0   'False
      Width           =   870
   End
   Begin VB.Label Label1 
      AutoSize        =   -1  'True
      Caption         =   "&Ciudad:"
      Height          =   195
      Index           =   6
      Left            =   120
      TabIndex        =   42
      Top             =   7800
      Visible         =   0   'False
      Width           =   540
   End
   Begin VB.Label Label1 
      AutoSize        =   -1  'True
      Caption         =   "De&partamento:"
      Height          =   195
      Index           =   7
      Left            =   120
      TabIndex        =   41
      Top             =   8160
      Visible         =   0   'False
      Width           =   1050
   End
End
Attribute VB_Name = "FrmEmpresa"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Dim CodCeco()
Dim a�o As Integer
Dim EncontroT As Integer
Dim Cod_Dpto() As Variant, Cod_Muni() As Variant, Cod_ActEcon() As Variant
Public OpcCod        As String   'Opci�n de seguridad
Dim VrArr() As Variant 'DRMG T44728-R41288 almacena los resultados de consultas sql
Dim BoFaVe As Boolean 'DRMG T44728-R41288 almacena true o false
Dim ObForm As Object 'DRMG T44728-R41288 objeto que permitira abrir mas de una ves una misma ventana

Private Sub CboActividad_GotFocus()
    CboActividad.Width = 4415
    CmdSeleccion.Visible = True
End Sub

Private Sub CboActividad_KeyPress(KeyAscii As Integer)
   Call Cambiar_Enter(KeyAscii) 'smdl m1952
End Sub

Private Sub CboActoAdm_KeyPress(KeyAscii As Integer)
   Call Cambiar_Enter(KeyAscii) 'smdl m1952
End Sub

Private Sub CboCeco_KeyPress(KeyAscii As Integer)
    Call Cambiar_Enter(KeyAscii)
End Sub

Private Sub CboCiudad_KeyPress(KeyAscii As Integer)
   Call Cambiar_Enter(KeyAscii) 'smdl m1952
End Sub

Private Sub CboClaseEnt_KeyPress(KeyAscii As Integer)
   Call Cambiar_Enter(KeyAscii) 'smdl m1952
End Sub

Private Sub CboDpto_KeyPress(KeyAscii As Integer)
   Call Cambiar_Enter(KeyAscii) 'smdl m1952
End Sub

Private Sub CboRegimenIVA_KeyPress(KeyAscii As Integer)
   Call Cambiar_Enter(KeyAscii) 'smdl m1952
End Sub

Private Sub CboTipoE_KeyPress(KeyAscii As Integer)
    Call Cambiar_Enter(KeyAscii)
End Sub
Private Sub CboTipoIdent_KeyPress(KeyAscii As Integer)
   Call Cambiar_Enter(KeyAscii) 'smdl m1952
End Sub

Private Sub CboTipoPer_KeyPress(KeyAscii As Integer)
   Call Cambiar_Enter(KeyAscii) 'smdl m1952
End Sub

Private Sub ChkAutoRetIca_KeyPress(KeyAscii As Integer)
   Call Cambiar_Enter(KeyAscii) 'smdl m1952
End Sub

Private Sub ChkAutoRetIva_KeyPress(KeyAscii As Integer)
   Call Cambiar_Enter(KeyAscii) 'smdl m1952
End Sub

Private Sub ChkAutoRetRenta_KeyPress(KeyAscii As Integer)
   Call Cambiar_Enter(KeyAscii) 'smdl m1952
End Sub

'---------------------------------------------------------------------------------------
' Procedure : ChkGenMovNiif_Click
' DateTime  : 11/02/2016 08:57
' Author    : juan_urrego
' Purpose   : T32800-31099 oculta el frame de costos niif si se encuentra desmarcado
'---------------------------------------------------------------------------------------
'
Private Sub ChkGenMovNiif_Click()
   If ChkGenMovNiif.value = 0 Then
      'JAUM T33370 Inicio se deja linea en comentario
      'FrmDatos.Visible = False
      FrmDatos.Height = 600
      LblCostoNIIF.Visible = False
      OptUCosto.Visible = False
      OptVMercado.Visible = False
      'JAUM T33370 Fin
   Else
      'JAUM T33370 Inicio se deja linea en comentario
      'FrmDatos.Visible = True
      FrmDatos.Height = 945
      LblCostoNIIF.Visible = True
      OptUCosto.Visible = True
      OptVMercado.Visible = True
      'JAUM T33370 Fin
   End If
End Sub

'---------------------------------------------------------------------------------------
' Procedure : ChkGenMovNiif_KeyPress
' DateTime  : 08/02/2016 15:00
' Author    : juan_urrego
' Purpose   : T32800-R31099 Cambia por Enter por TAB
'---------------------------------------------------------------------------------------
'
Private Sub ChkGenMovNiif_KeyPress(KeyAscii As Integer)
   Call Cambiar_Enter(KeyAscii)
End Sub


Private Sub ChkIVADed_Click()
' DAHV T6105 - INICIO
If ChkIvaDed.value = 1 Then
    FrmIVAXART.Enabled = True
Else
    FrmIVAXART.Enabled = False
    OptIvaSi(1).value = True
End If
' DAHV T6105 - INICIO
End Sub

Private Sub ChkIvaDed_KeyPress(KeyAscii As Integer)
   Call Cambiar_Enter(KeyAscii) 'smdl m1952
   'DAHV T6105 -  INICIO
   If ChkIvaDed.value = 1 Then
        FrmIVAXART.Enabled = True
   Else
        FrmIVAXART.Enabled = False
   End If
   'DAHV T6105 -  FIN
End Sub


'---------------------------------------------------------------------------------------
' Procedure : ChkVer_Click
' DateTime  : 24/07/2018 18:31
' Author    : daniel_mesa
' Purpose   : DRMG T44016 MUESTRA LA CONTRASE�A DEL CERTIFICADO
'---------------------------------------------------------------------------------------
'
'DRMG T45150 INICIO SE DEJA EN COMENTARIO LAS SIGUIENTES DOS PRIMERAS LINEAS
'Private Sub ChkVer_Click()
   'If ChkVer.Value = 1 Then
Private Sub ChkVer_Click(Index As Integer)
   
   If ChkVer(1).value = 1 Then
      TxtCont.PasswordChar = NUL$
   Else
      TxtCont.PasswordChar = "*"
   End If
   
   If ChkVer(0).value = 1 Then
'DRMG T45150 FIN
      TxtPassword.PasswordChar = NUL$
   Else
      TxtPassword.PasswordChar = "*"
   End If
End Sub

Private Sub Cmd_Botones_Click(Index As Integer)
''JACC RESOLUCION 1474
'JACC R2369 -INICIO
' Dim StMens
' StMens = "Al cambiar la clase entidad cambia la estructura del nivel de las cuentas, esto"
' StMens = StMens & vbCrLf & "puede ocasionar da�os en los movimientos contables, �Desea continuar?"
' 'JACC R2369 - FIN
'    Select Case Index
'        Case 0:
'                  'JACC R2369 - INICIO
'                  If (((BoTipoEmpre = False) And (CboClaseEnt.ListIndex = 3)) Or ((BoTipoEmpre = True) And (CboClaseEnt.ListIndex <> 3))) Then
'                          If MsgBox(StMens, vbQuestion + vbYesNo, "Inventarios- Personalizaci�n de la empresa") = vbNo Then
'                              Exit Sub
'                          End If
'                  End If
                  'JACC R2369 - FIN
'JACC RESOLUCION 1474
    Select Case Index
        Case 0:

                  Call Grabar
        Case 1: Unload Me
        Case 2: FrmCuenEmail.Show vbModal 'DRMG T44728-R41288
    End Select
End Sub

Private Sub Cmd_Botones_KeyPress(Index As Integer, KeyAscii As Integer)
   Call Cambiar_Enter(KeyAscii) 'smdl m1952
End Sub

Private Sub CmdSeleccion_Click()
Dim I As Double
      'Codigo = Seleccion("ACT_ECONOMICA", "DE_DESC_ACEC", "CD_CODI_ACEC, DE_DESC_ACEC", "ACTIVIDADES ECONOMICAS", NUL$)
      Codigo = Seleccion("ACT_ECONOMICA", "DE_DESC_ACEC", "CD_CODI_ACEC, DE_DESC_ACEC", "ACTIVIDADES ECONOMICAS", "NU_ESTADO=1") 'JJRG 15628
      If Codigo <> NUL$ Then
        I = FindInArr(Cod_ActEcon, Codigo)
        CboActividad.ListIndex = I
      End If
      'TxtEmpresa(8).SetFocus
      CmdSeleccion.Visible = False
      CboActividad.Width = 4815
End Sub

Private Sub Form_Load()
    Call CenterForm(MDI_Inventarios, Me)
    Result = loadctrl("CENTRO_COSTO ", "NO_NOMB_CECO", "CD_CODI_CECO", CboCeco, CodCeco(), "NU_ESTADO_CECO<>1 ORDER BY NO_NOMB_CECO")
    EncontroT = Encontro
    If (Result = FAIL) Then
      Call Mensaje1("ERROR CARGANDO TABLA [CENTRO_COSTO]", 1)
      Exit Sub
    End If
    
    Result = loadctrl("DEPARTAMENTOS order by NO_NOMB_DPTO", "NO_NOMB_DPTO", "CD_CODI_DPTO", CboDpto, Cod_Dpto(), NUL$)
    If (Result = FAIL) Then
         Call Mensaje1("ERROR CARGANDO TABLA [DEPARTAMENTOS]", 1)
        Exit Sub
    End If
    
    'Result = loadctrl("ACT_ECONOMICA ORDER BY DE_DESC_ACEC", "DE_DESC_ACEC", "CD_CODI_ACEC", CboActividad, Cod_ActEcon(), NUL$)
    'Result = loadctrl("ACT_ECONOMICA ORDER BY DE_DESC_ACEC", "DE_DESC_ACEC", "CD_CODI_ACEC", CboActividad, Cod_ActEcon(), "NU_ESTADO=1") 'JJRG 15628 'ALGM 15885
    Result = loadctrl("ACT_ECONOMICA", "DE_DESC_ACEC", "CD_CODI_ACEC", CboActividad, Cod_ActEcon(), "NU_ESTADO=1 ORDER BY DE_DESC_ACEC") 'ALGM 15885
    If (Result = FAIL) Then
       Call Mensaje1("Error cargando la tabla [ACT_ECONOMICA]", 1)
       Exit Sub
    End If
    
    'Cmd_Botones(0).Enabled = Leer_Permiso("04002", "ID_CREA_PERM")
    a�o = CInt(Format(Date, "yyyy"))
    Call Cargar_Datos
    
   'DRMG T44728-R41288 INICIO
   BoFaVe = False
   'ReDim VrArr(18) 'DRMG T45150 SE DEJA EN COMENTARIO
   ReDim VrArr(19) 'DRMG T45150
   Valores = "NU_MODULO_FAEL,NU_FACINI_FAEL,NU_FACFIN_FAEL,TX_PREFI_FAEL,TX_AUTORI_FAEL"
   Valores = Valores & Coma & "TX_CTEC_FAEL,FE_AUTORI_FAEL,FE_FINAUT_FAEL,TX_URL_FAEL"
   Valores = Valores & Coma & "TX_URLCER_FAEL,TX_PSSWRD_FAEL,NU_CONFAC_FAEL"
   Valores = Valores & Coma & "TX_ENCA_FAEL,TX_PIEUNO_FAEL,TX_PIEDOS_FAEL"
   Valores = Valores & Coma & "TX_IDSOFT_FAEL,TX_PIN_FAEL,TX_URLENV_FAEL,TX_URLCON_FAEL"
   Valores = Valores & Coma & "TX_SFPSW_FAEL" 'DRMG T45150
   Condicion = "NU_MODULO_FAEL=1 AND NU_ESTADO_FAEL=0"
   Result = LoadData("FACTURA_ELECTRONICA", Valores, Condicion, VrArr)
   If Result <> FAIL Then
      If Encontro Then
         Call BUSCAR_FACTELECT(VrArr())
      Else
         MskFechAut.Text = Format(Date, "dd/mm/yyyy")
         MskFechFin.Text = MskFechAut.Text
         TxtFactIni.Text = 1
         TxtConFact = 0
         BoFaVe = True 'DRMG T45218
      End If
      'DRMG T45218 INICIO
      ReDim VrArr(19)
      Valores = Replace(Valores, "FAEL", "AUFA")
      Condicion = "NU_MODULO_AUFA=1"
      Result = LoadData("AUTO_FAEL", Valores, Condicion, VrArr())
      If Result <> FAIL And Encontro Then
         Call Buscar_Fael_Temp(VrArr())
      End If
      'DRMG T45218 FIN
   End If
   'DRMG T44728-R41288 FIN
End Sub

Private Sub Form_Unload(Cancel As Integer)
    'Call Carga_Datos_Empresa
   If BoFaVe Then Call Grabar_Auto_Fael 'DRMG T45218
End Sub

Private Sub MskFechaCreacion_KeyPress(KeyAscii As Integer)
   Call Cambiar_Enter(KeyAscii) 'smdl m1952
End Sub

'---------------------------------------------------------------------------------------
' Procedure : MskFechAut_LostFocus
' DateTime  : 17/05/2018 10:45
' Author    : daniel_mesa
' Purpose   : DRMG T44728-R41288  validacion de la fecha inicial debe ser menor a la fecha final
'---------------------------------------------------------------------------------------
'
Private Sub MskFechAut_LostFocus()
   If Not IsDate(MskFechFin.Text) Then
     Call Mensaje1("Esta fecha no existe. Por favor corrijala", 3)
     MskFechAut.SetFocus
     MskFechAut = Format(FechaServer, "dd/mm/yyyy")
     MskFechAut.SetFocus
   Else
      Call ValiFeInMenorFeFin
   End If
End Sub

'---------------------------------------------------------------------------------------
' Procedure : MskFechFin_LostFocus
' DateTime  : 15/05/2018 11:31
' Author    : daniel_mesa
' Purpose   : DRMG T44728-R41288  validacion de que la fecha final debe ser mayor a la autorizada
'---------------------------------------------------------------------------------------
'
Private Sub MskFechFin_LostFocus()
   If Not IsDate(MskFechFin.Text) Then
     Call Mensaje1("Esta fecha no existe. Por favor corrijala", 3)
     MskFechFin.SetFocus
     MskFechFin = Format(FechaServer, "dd/mm/yyyy")
     MskFechFin.SetFocus
   Else
      Call ValiFeInMenorFeFin
   End If
End Sub

Private Sub OptNo_KeyPress(KeyAscii As Integer)
   Call Cambiar_Enter(KeyAscii) 'smdl m1952
End Sub

Private Sub OptSi_KeyPress(KeyAscii As Integer)
   Call Cambiar_Enter(KeyAscii) 'smdl m1952
End Sub

Private Sub OptUCosto_KeyPress(KeyAscii As Integer)
   Call Cambiar_Enter(KeyAscii) 'SKRV T24261/R22366
End Sub

Private Sub OptVMercado_KeyPress(KeyAscii As Integer)
   Call Cambiar_Enter(KeyAscii) 'SKRV T24261/R22366
End Sub


'---------------------------------------------------------------------------------------
' Procedure : ScmdAct_Click
' DateTime  : 17/05/2018 10:48
' Author    : daniel_mesa
' Purpose   : DRMG T44728-R41288 hailita la seccion de Rangos Autorizados para poder actualizar la informacion
' solo si el rango puesto no exista facturas creadas
'---------------------------------------------------------------------------------------
'
Private Sub ScmdAct_Click()
   ReDim VrArr(0)
   'DRMG T45124 INICIO ' LAS SIGUIENTES DOS LINEAS SE DEJA EN COMENTARIO
   'Condicion = "NU_CNSELT_RFVE>=" & TxtFactIni & " and NU_CNSELT_RFVE<=" & TxtFactFin.Text
   'Result = LoadData("R_FACTURA_VENT_ELECT", "NU_CNSELT_RFVE", Condicion, VrArr())
   Condicion = "NU_CNFAVE_FAFA>=" & TxtFactIni & " and NU_CNFAVE_FAFA<=" & TxtFactFin.Text
   Result = LoadData("R_FAVE_FAEL", "NU_CNFAVE_FAFA", Condicion, VrArr())
   'DRMG T45124 FIN
   If Result = FAIL Then Exit Sub
   If Encontro Then
      Call Mensaje1("En el Rango establecido ya existen Facturas creadas, no es posible actualizar la informaci�n", 3)
   Else
      TxtFactIni.Enabled = True
      TxtFactFin.Enabled = True
      TxtPreFijo.Enabled = True
      TxtAutori.Enabled = True
      'TxtCTecni.Enabled = True 'DRMG T45866 SE DEJA EN COMENTARIO
      MskFechAut.Enabled = True
      MskFechFin.Enabled = True
   End If
End Sub

'---------------------------------------------------------------------------------------
' Procedure : ScmdNewRang_Click
' DateTime  : 17/05/2018 10:49
' Author    : daniel_mesa
' Purpose   : DRMG T44728-R41288 Guarda los nuevos reguistros autorizados
'---------------------------------------------------------------------------------------
'
Private Sub ScmdNewRang_Click()
   MskFechAut.Text = Format(Date, "dd/mm/yyyy")
   MskFechFin.Text = MskFechAut.Text
   ReDim Arr(0)
   Condicion = "NU_MODULO_FAEL=1 AND NU_ESTADO_FAEL=0"
   Result = LoadData("FACTURA_ELECTRONICA", "NU_CONFAC_FAEL", Condicion, Arr)
   
   If Result <> FAIL And Encontro Then
      TxtFactIni.Text = Arr(0) + 1: TxtConFact = Arr(0)
   Else
      Exit Sub
   End If
   TxtFactFin.Text = NUL$
   
   TxtFactIni.Enabled = True
   TxtFactFin.Enabled = True
   TxtPreFijo.Enabled = True
   TxtAutori.Enabled = True
   'TxtCTecni.Enabled = True 'DRMG T45866 SE DEJA EN COMENTARIO
   MskFechAut.Enabled = True
   MskFechFin.Enabled = True
   TxtConFact.Enabled = True
   BoFaVe = True
   TxtPreFijo.Text = NUL$
   TxtAutori.Text = NUL$
   'TxtCTecni.Text = NUL$ 'DRMG T45866 SE DEJA EN COMENTARIO

End Sub

'---------------------------------------------------------------------------------------
' Procedure : TxtAutori_KeyPress
' DateTime  : 17/05/2018 11:05
' Author    : daniel_mesa
' Purpose   : DRMG T44728-R41288 Valida que solo el campo reciba caracteres alfannumericos
'---------------------------------------------------------------------------------------
'
Private Sub TxtAutori_KeyPress(KeyAscii As Integer)
   Call ValKeyAlfaNumSinCar(KeyAscii)
   Call Cambiar_Enter(KeyAscii)
End Sub

Private Sub TxtCodUnion_KeyPress(KeyAscii As Integer)
   Call Cambiar_Enter(KeyAscii) 'smdl m1952
End Sub

'---------------------------------------------------------------------------------------
' Procedure : TxtConFact_KeyPress
' DateTime  : 25/09/2018 16:54
' Author    : daniel_mesa
' Purpose   : DRMG T44728-R41288 Valida que solo se ingrese caracteres numericos
'---------------------------------------------------------------------------------------
'
Private Sub TxtConFact_KeyPress(KeyAscii As Integer)
   Call ValKeyNum(KeyAscii)
   Call Cambiar_Enter(KeyAscii)
End Sub

'---------------------------------------------------------------------------------------
' Procedure : TxtConFact_LostFocus
' DateTime  : 25/09/2018 17:02
' Author    : daniel_mesa
' Purpose   : DRMG T44728-R41288 Valida si ya existen facturas con ese consecutivo
'---------------------------------------------------------------------------------------
'
Private Sub TxtConFact_LostFocus()
   If TxtConFact.Text = NUL$ Then Exit Sub
   ReDim VrArr(0)
   'DRMG T45124 INICIO ' LAS SIGUIENTES DOS LINEAS SE DEJA EN COMENTARIO
   'Condicion = "NU_CNSELT_RFVE= " & CDbl(TxtConFact.Text) + 1
   'Result = LoadData("R_FACTURA_VENT_ELECT", "NU_CNSELT_RFVE", Condicion, VrArr())
   Condicion = "NU_AUTO_FAFA= " & CDbl(TxtConFact.Text) + 1
   Result = LoadData("R_FAVE_FAEL", "NU_AUTO_FAFA", Condicion, VrArr())
   'DRMG T45124 FIN
   If Result <> FAIL And Encontro Then
      Call Mensaje1("Ya Existe el consecutivo " & (CDbl(TxtConFact.Text) + 1) & ", ingrese uno que no exista", 3)
      TxtConFact.Text = NUL$
      TxtConFact.SetFocus
      Call MouseNorm: Exit Sub
   Else 'DRMG T45218
      If BoFaVe Then Call Grabar_Auto_Fael 'DRMG T45218
   End If
End Sub

Private Sub TxtCorreo_KeyPress(KeyAscii As Integer)
   Call Cambiar_Enter(KeyAscii) 'smdl m1952
End Sub

'---------------------------------------------------------------------------------------
' Procedure : TxtCTecni_KeyPress
' DateTime  : 17/05/2018 11:06
' Author    : daniel_mesa
' Purpose   : DRMG T44728-R41288
'---------------------------------------------------------------------------------------
'
'DRMG T45866 INICIO SE DEJA EN COMENTARIO LAS SIGUIENTES 4 LINEAS
'Private Sub TxtCTecni_KeyPress(KeyAscii As Integer)
'   Call ValKeyAlfaNumSinCar(KeyAscii)
'   Call Cambiar_Enter(KeyAscii)
'End Sub
'DRMG T45866 FIN

Private Sub TxtEmpresa_GotFocus(Index As Integer)
    TxtEmpresa(Index).SelStart = 0
    TxtEmpresa(Index).SelLength = Len(TxtEmpresa(Index))
End Sub
Private Sub TxtEmpresa_KeyPress(Index As Integer, KeyAscii As Integer)
    Call ValKeyAlfaNum(KeyAscii)
    Call Cambiar_Enter(KeyAscii)
End Sub
Private Sub Cargar_Datos()
Dim I As Double

    'ReDim Arr(15)
    ReDim Arr(16) 'JACC M4202
    Campos = "CD_NIT_ENTI,ID_TIPO_IDEN_ENTI,NO_NOMB_ENTI,CD_CODI_PSS_ENTI,CD_TIPO_ENTI,CD_NIT_REPR_ENTI"
    Campos = Campos & ",NO_NOMB_REPR_ENTI,DE_DIRE_ENTI,CD_CODI_DPTO_ENTI,CD_CODI_MUNI_ENTI,DE_TELE_ENTI"
    Campos = Campos & ",DE_FAX_ENTI,DE_EMAIL_ENTI,CD_CODI_ACTO_ENTI,FE_FECH_CREA_ENTI,CD_CODI_CGN_ENTI"
    Campos = Campos & ",TX_INDICATIVO_ENTI"
    Result = LoadData("ENTIDAD", Campos, NUL$, Arr)
    If Result <> FAIL Then
        CboTipoIdent.ListIndex = FindInCtrl(CboTipoIdent, Arr(1))
        TxtEmpresa(1) = Arr(0)
        TxtDigVerif = DigitoNIT(Arr(0))
        If Arr(4) <> NUL$ Then CboClaseEnt.ListIndex = Arr(4)    'Clase entidad
        If Arr(13) <> NUL$ Then CboActoAdm.ListIndex = Arr(13)
        TxtCodUnion = Arr(15)
        If Arr(14) <> NUL$ Then MskFechaCreacion = Format(Arr(14), "DD/MM/YYYY")
        TxtEmpresa(0) = Arr(2)
        TxtIdRepresentante = Arr(5)
        TxtRepresentante = Arr(6)
        TxtEmpresa(5) = Arr(7)
        If Arr(8) <> NUL$ Then CboDpto.ListIndex = FindInArr(Cod_Dpto(), Arr(8))
        Call CboDpto_LostFocus
        If Arr(9) <> NUL$ Then CboCiudad.ListIndex = FindInArr(Cod_Muni(), Arr(9))
        TxtEmpresa(3) = Arr(10)
        TxtEmpresa(4) = Arr(11)
        TxtCorreo = Arr(12)
        TxtIndicativo = Arr(16) 'JACC M4202
        ReDim Arr(2)
        Result = LoadData("TC_CONTROL", "ID_IDE2_CONT,ID_CONT_CONT,CD_ACEC_CONT", NUL$, Arr)
        If Result <> FAIL Then
            If Arr(0) <> NUL$ Then  'Centro de costo
              If EncontroT Then
                I = FindInArr(CodCeco, Arr(0))
                If CboCeco.ListCount > 0 Then CboCeco.ListIndex = I
              Else
                Call Mensaje1("No existen Centros de Costo, es necesario crear una dependencia.", 1)
              End If
            Else
              On Error GoTo siga
              CboCeco.ListIndex = 0
siga:
            End If
            
            If Arr(1) <> NUL$ Then  'Tipo Empresa
               CboTipoE.ListIndex = IIf(Arr(1) = "C", 0, IIf(Arr(1) = "E", 1, 2))
            Else
              CboTipoE.ListIndex = 0
            End If
            
            If IsNumeric(Arr(2)) Then
               If Arr(2) <> NUL$ Then
                 I = FindInArr(Cod_ActEcon, Arr(2))
                 CboActividad.ListIndex = I
                 CboActividad.ToolTipText = CboActividad.List(CboActividad.ListIndex)
               End If
            End If
            
        End If
        EncontroT = Encontro
        Call Cargar_Datos_Tributarios
        Encontro = EncontroT
    End If
End Sub
Private Sub Grabar()

    DoEvents
    Call MouseClock
    Msglin "Ingresando informaci�n al Archivo de CONTROL"
    
    If TxtEmpresa(0) = NUL$ Or TxtEmpresa(1) = NUL$ Then
       Call Mensaje1("Se debe especificar el nombre y el nit de la empresa!!!", 3)
       TxtEmpresa(0).SetFocus
       Call MouseNorm: Exit Sub
    End If
    
    If CboCeco.ListIndex = -1 Then
       Call Mensaje1("Seleccione el Centro de Costo!!!", 3)
       CboCeco.SetFocus
       Call MouseNorm: Exit Sub
    End If
    
    If CboTipoE.ListIndex = -1 Then
       Call Mensaje1("Seleccione el Tipo de Empresa!!!", 3)
       CboTipoE.SetFocus
       Call MouseNorm: Exit Sub
    End If
    
    If CboDpto.ListIndex <> -1 And CboCiudad.ListIndex = -1 Then
      Call Mensaje1("Debe seleccionar la Ciudad donde se encuentra ubicada la Entidad.", 3)
      Call MouseNorm: Exit Sub
    End If
    
    If CboClaseEnt.ListIndex = -1 Then
      Call Mensaje1("Debe seleccionar el Tipo de Entidad.", 3)
      Call MouseNorm: Exit Sub
    End If
   
    If TxtIdRepresentante = "" Or TxtRepresentante = "" Then
      Call Mensaje1("Digite los datos del Representante Legal.", 3)
      Call MouseNorm: Exit Sub
    End If
    
    If CboActividad.ListIndex = -1 Then
       Call Mensaje1("Seleccione la Actividad Ec�nomica!!!", 3)
       CboActividad.SetFocus
       Call MouseNorm: Exit Sub
    End If
    
    If CboTipoPer.ListIndex = -1 Then
      Call Mensaje1("Seleccione el Tipo de Persona de la Empresa.", 3)
      Call MouseNorm: Exit Sub
    End If
    
    If CboRegimenIVA.ListIndex = -1 Then
      Call Mensaje1("Seleccione el Tipo de Regimen de IVA.", 3)
      Call MouseNorm: Exit Sub
    End If
    
    ReDim VrArr(0) 'DRMG T45216
    Result = LoadData("ENTIDAD", "CD_NIT_ENTI", NUL$, VrArr()) 'DRMG T45216
    If (BeginTran(STranIUp & "ENTIDAD") <> FAIL) Then
     'If (Not Encontro) Then SE DEJA EN COMENTARIO
     If Result <> FAIL And (Not Encontro) Then 'DRMG T45216
        Valores = "CD_NIT_ENTI=" & Comi & TxtEmpresa(1) & Comi & Coma
        Valores = Valores & "NO_NOMB_ENTI=" & Comi & TxtEmpresa(0) & Comi & Coma
        Valores = Valores & "ID_TIPO_IDEN_ENTI=" & Comi & CboTipoIdent.List(CboTipoIdent.ListIndex) & Comi & Coma
        Valores = Valores & "CD_CODI_CGN_ENTI=" & Comi & TxtCodUnion & Comi & Coma
        Valores = Valores & "CD_TIPO_ENTI=" & Comi & CboClaseEnt.ListIndex & Comi & Coma
        Valores = Valores & "CD_NIT_REPR_ENTI=" & Comi & TxtIdRepresentante & Comi & Coma
        Valores = Valores & "NO_NOMB_REPR_ENTI=" & Comi & TxtRepresentante & Comi & Coma
        Valores = Valores & "DE_DIRE_ENTI=" & IIf(TxtEmpresa(5) = "", "Null", Comi & TxtEmpresa(5) & Comi) & Coma
        Valores = Valores & "CD_CODI_DPTO_ENTI=" & Comi & Cod_Dpto(CboDpto.ListIndex) & Comi & Coma
        Valores = Valores & "CD_CODI_MUNI_ENTI=" & Comi & Cod_Muni(CboCiudad.ListIndex) & Comi & Coma
        Valores = Valores & "DE_TELE_ENTI=" & IIf(TxtEmpresa(3) = "", "Null", Comi & TxtEmpresa(3) & Comi) & Coma
        Valores = Valores & "DE_FAX_ENTI=" & IIf(TxtEmpresa(4) = "", "Null", Comi & TxtEmpresa(4) & Comi) & Coma
        Valores = Valores & "DE_EMAIL_ENTI=" & IIf(TxtCorreo = "", "Null", Comi & TxtCorreo & Comi) & Coma
        Valores = Valores & "CD_CODI_ACTO_ENTI=" & Comi & CboActoAdm.ListIndex & Comi & Coma
        Valores = Valores & "FE_FECH_CREA_ENTI=" & FFechaIns(CDate(Hoy)) 'REOL MANTIS: 0000560
        Valores = Valores & Coma & "TX_INDICATIVO_ENTI=" & Comi & TxtIndicativo & Comi  'JACC M4204
        Valores = Valores & ", NU_MOVNIIF_ENTI = " & ChkGenMovNiif.value 'JAUM T32800-R31099
        Result = DoInsertSQL("ENTIDAD", Valores)
        If Result <> FAIL Then
          ''JACC RESOLUCION 1474
          ''JACC R2176 OE 'JACC R2369 Se habilita el codigo
'          If CboClaseEnt.ListIndex = 3 Then
'              BoTipoEmpre = True
'          Else
'              BoTipoEmpre = False
'          End If
          'JACC R2176 OE 'JACC R2369
          'JACC RESOLUCION 1474
          BoTipoEmpre = False 'JACC Devolver cambios 'JACC RESOLUCION 1474
          Valores = "CD_CODI_TERC_PAIM =" & Comi & TxtEmpresa(0) & Comi & Coma
          Valores = Valores & "ID_TIPO_TERC_PAIM =" & Comi & CboTipoPer.ListIndex & Comi & Coma
          Valores = Valores & "ID_TIPO_REGI_PAIM =" & Comi & CboRegimenIVA.ListIndex & Comi & Coma
          Valores = Valores & "ID_TIPO_CONTR_PAIM =" & Comi & IIf(OptSi.value = True, "S", "N") & Comi & Coma
          Valores = Valores & "ID_AUTO_RETIVA_PAIM =" & Comi & ChkAutoRetIva.value & Comi & Coma
          Valores = Valores & "ID_AUTO_RETICA_PAIM =" & Comi & ChkAutoRetIca.value & Comi & Coma
          Valores = Valores & "ID_AUTO_RETREN_PAIM =" & Comi & ChkAutoRetRenta.value & Comi & Coma
          Valores = Valores & "TX_RESO_ARETIVA_PAIM =" & Comi & Me.TxtResIVA & Comi & Coma
          Valores = Valores & "TX_RESO_ARETICA_PAIM =" & Comi & TxtResICA & Comi & Coma
          Valores = Valores & "TX_RESO_ARETREN_PAIM =" & Comi & TxtResRENTA & Comi & Coma
          Valores = Valores & "ID_AREIVA_REGCOM_PAIM =" & Comi & ChkRegComun(0).value & Comi & Coma
          Valores = Valores & "ID_AREIVA_REGSIM_PAIM =" & Comi & ChkRegSimplif(0).value & Comi & Coma
          Valores = Valores & "ID_AREICA_REGCOM_PAIM =" & Comi & ChkRegComun(1).value & Comi & Coma
          Valores = Valores & "ID_AREICA_REGSIM_PAIM =" & Comi & ChkRegSimplif(1).value & Comi & Coma
          Valores = Valores & "ID_AREREN_REGCOM_PAIM =" & Comi & ChkRegComun(2).value & Comi & Coma
          Valores = Valores & "ID_AREREN_REGSIM_PAIM =" & Comi & ChkRegSimplif(2).value & Comi & Coma
          Valores = Valores & "ID_IVADEDU_PAIM =" & Comi & ChkIvaDed.value & Comi
          ' DAHV T6105 - INICIO
          ' Proceso de actualizaci�n para verificar si se hace deducible por art�culo o no
          If ChkIvaDed.value = 1 Then
                If OptIvaSi(0).value = True Then
                    Valores = Valores & Coma & " TX_IVADEDAR_PAIM = 'S'"
                ElseIf OptIvaSi(1).value = True Then
                    Valores = Valores & Coma & " TX_IVADEDAR_PAIM = 'N'"
                End If
          Else
                Valores = Valores & Coma & " TX_IVADEDAR_PAIM = 'N'"
          End If
          ' DAHV T6105 - FIN
          
          Result = DoInsertSQL("PARAMETROS_IMPUESTOS", Valores)
          Call Costo_Niif 'SKRV T24261/R22366
          
          'DAHV T6226 - INICIO
          Result = Auditor("PARAMETROS_IMPUESTOS", TranIns, LastCmd)
          'DAHV T6226 - INICIO
          
          
          If Result <> FAIL Then
            Valores = "FE_PERI_FISC = " & Comi & a�o & Comi & Coma
            Valores = Valores & "NU_PATR_CONT = " & Comi & "0" & Comi & Coma
            Valores = Valores & "DE_JEFE_FINA = " & Comi & NUL$ & Comi & Coma
            Valores = Valores & "DE_REVI_FISC = " & Comi & NUL$ & Comi & Coma
            Valores = Valores & "NU_TARJ_REFI = " & Comi & NUL$ & Comi & Coma
            Valores = Valores & "NU_CEDU_REFI = " & Comi & NUL$ & Comi & Coma
            Valores = Valores & "DE_REPR_LEGA = " & Comi & NUL$ & Comi & Coma
            Valores = Valores & "NU_TARJ_REPR = " & Comi & NUL$ & Comi & Coma
            Valores = Valores & "NU_CEDU_REPR = " & Comi & NUL$ & Comi & Coma
            Valores = Valores & "DE_CONT_GENE = " & Comi & NUL$ & Comi & Coma
            Valores = Valores & "NU_TARJ_CONT = " & Comi & NUL$ & Comi & Coma
            Valores = Valores & "NU_CEDU_CONT = " & Comi & NUL$ & Comi & Coma
            Valores = Valores & "DE_ORDE_GAST = " & Comi & NUL$ & Comi & Coma
            Valores = Valores & "NU_CEDU_ORDE = " & Comi & "0" & Comi & Coma
            Valores = Valores & "ID_APLI_HABI = " & Comi & NUL$ & Comi & Coma
            Valores = Valores & "ID_ANOS_BISI = " & Comi & NUL$ & Comi & Coma
            Valores = Valores & "VL_SALA_MINI = " & Comi & "0" & Comi & Coma
            Valores = Valores & "ID_LICE_USO = " & Comi & NUL$ & Comi & Coma
            Valores = Valores & "ID_VENC_DEMO = " & Comi & NUL$ & Comi & Coma
            Valores = Valores & "FE_VENC_DEMO = " & Comi & FechaLic & Comi & Coma
            Valores = Valores & "FE_INST_APLI = " & Comi & FechaLic & Comi & Coma
            Valores = Valores & "ID_GRAN_CONT = " & Comi & "N" & Comi & Coma
            Valores = Valores & "ID_CONT_CONT = " & Comi & "C" & Comi & Coma
            Valores = Valores & "ID_IDE2_CONT = " & Comi & CodCeco(CboCeco.ListIndex) & Comi & Coma
            Valores = Valores & "ID_TIEN_CONT = " & Comi & "0" & Comi & Coma
            Valores = Valores & "CD_ACEC_CONT = " & Comi & Cod_ActEcon(CboActividad.ListIndex) & Comi & Coma
            Valores = Valores & "DE_EMPR_CONT = ' '"
            Result = DoInsertSQL("TC_CONTROL", Valores)
            If (Result <> FAIL) Then
               Entidad = TxtEmpresa(0)
               Nit = TxtEmpresa(1)
               'Id_Empresa = TxtEmpresa(2)
               Result = Auditor("TC_CONTROL", TranIns, LastCmd)
            End If
          End If
        End If
     Else
       If WarnMsg("Desea Modificar la Informaci�n de la Empresa ? ", 256) Then
          Valores = "CD_NIT_ENTI=" & Comi & TxtEmpresa(1) & Comi & Coma
          Valores = Valores & "NO_NOMB_ENTI=" & Comi & TxtEmpresa(0) & Comi & Coma
          Valores = Valores & "ID_TIPO_IDEN_ENTI=" & Comi & CboTipoIdent.List(CboTipoIdent.ListIndex) & Comi & Coma
          Valores = Valores & "CD_CODI_CGN_ENTI=" & Comi & TxtCodUnion & Comi & Coma
          Valores = Valores & "CD_TIPO_ENTI=" & Comi & CboClaseEnt.ListIndex & Comi & Coma
          Valores = Valores & "CD_NIT_REPR_ENTI=" & Comi & TxtIdRepresentante & Comi & Coma
          Valores = Valores & "NO_NOMB_REPR_ENTI=" & Comi & TxtRepresentante & Comi & Coma
          Valores = Valores & "DE_DIRE_ENTI=" & IIf(TxtEmpresa(5) = "", "Null", Comi & TxtEmpresa(5) & Comi) & Coma
          Valores = Valores & "CD_CODI_DPTO_ENTI=" & Comi & Cod_Dpto(CboDpto.ListIndex) & Comi & Coma
          Valores = Valores & "CD_CODI_MUNI_ENTI=" & Comi & Cod_Muni(CboCiudad.ListIndex) & Comi & Coma
          Valores = Valores & "DE_TELE_ENTI=" & IIf(TxtEmpresa(3) = "", "Null", Comi & TxtEmpresa(3) & Comi) & Coma
          Valores = Valores & "DE_FAX_ENTI=" & IIf(TxtEmpresa(4) = "", "Null", Comi & TxtEmpresa(4) & Comi) & Coma
          Valores = Valores & "DE_EMAIL_ENTI=" & IIf(TxtCorreo = "", "Null", Comi & TxtCorreo & Comi) & Coma
          Valores = Valores & "CD_CODI_ACTO_ENTI=" & Comi & CboActoAdm.ListIndex & Comi & Coma
          Valores = Valores & "FE_FECH_CREA_ENTI=" & FFechaIns(CDate(MskFechaCreacion)) 'REOL MANTIS: 0000560
          Valores = Valores & Coma & "TX_INDICATIVO_ENTI=" & Comi & TxtIndicativo & Comi 'JACC M4204
          Valores = Valores & ", NU_MOVNIIF_ENTI = " & ChkGenMovNiif.value 'JAUM T32800-R31099
          Result = DoUpdate("ENTIDAD", Valores, NUL$)
          If Result <> FAIL Then
            ''JACC RESOLUCION 1474
            ''JACC R2176 OE 'JACC R2369 Se habilita el codigo
'            If CboClaseEnt.ListIndex = 3 Then
'                BoTipoEmpre = True
'            Else
'                BoTipoEmpre = False
'            End If
            'JACC R2176 OE 'JACC R2369
            'JACC RSOLUCION 1474
            BoTipoEmpre = False  'JACC Devolver cambios 'JACC RSOLUCION 1474
            Valores = "ID_TIPO_TERC_PAIM =" & Comi & CboTipoPer.ListIndex & Comi & Coma
            Valores = Valores & "ID_TIPO_REGI_PAIM =" & Comi & CboRegimenIVA.ListIndex & Comi & Coma
            Valores = Valores & "ID_TIPO_CONTR_PAIM =" & Comi & IIf(OptSi.value = True, "S", "N") & Comi & Coma
            Valores = Valores & "ID_AUTO_RETIVA_PAIM =" & Comi & ChkAutoRetIva.value & Comi & Coma
            Valores = Valores & "ID_AUTO_RETICA_PAIM =" & Comi & ChkAutoRetIca.value & Comi & Coma
            Valores = Valores & "ID_AUTO_RETREN_PAIM =" & Comi & ChkAutoRetRenta.value & Comi & Coma
            Valores = Valores & "TX_RESO_ARETIVA_PAIM =" & Comi & Me.TxtResIVA & Comi & Coma
            Valores = Valores & "TX_RESO_ARETICA_PAIM =" & Comi & TxtResICA & Comi & Coma
            Valores = Valores & "TX_RESO_ARETREN_PAIM =" & Comi & TxtResRENTA & Comi & Coma
            Valores = Valores & "ID_AREIVA_REGCOM_PAIM =" & Comi & ChkRegComun(0).value & Comi & Coma
            Valores = Valores & "ID_AREIVA_REGSIM_PAIM =" & Comi & ChkRegSimplif(0).value & Comi & Coma
            Valores = Valores & "ID_AREICA_REGCOM_PAIM =" & Comi & ChkRegComun(1).value & Comi & Coma
            Valores = Valores & "ID_AREICA_REGSIM_PAIM =" & Comi & ChkRegSimplif(1).value & Comi & Coma
            Valores = Valores & "ID_AREREN_REGCOM_PAIM =" & Comi & ChkRegComun(2).value & Comi & Coma
            Valores = Valores & "ID_AREREN_REGSIM_PAIM =" & Comi & ChkRegSimplif(2).value & Comi & Coma
            Valores = Valores & "ID_IVADEDU_PAIM=" & Comi & ChkIvaDed.value & Comi
            
            ' DAHV T6105 - INICIO
            ' Proceso de actualizaci�n para verificar si se hace deducible por art�culo o no
            If ChkIvaDed.value = 1 Then
                  If OptIvaSi(0).value = True Then
                      Valores = Valores & Coma & " TX_IVADEDAR_PAIM = 'S'"
                  ElseIf OptIvaSi(1).value = True Then
                      Valores = Valores & Coma & " TX_IVADEDAR_PAIM = 'N'"
                  End If
            Else
                  Valores = Valores & Coma & " TX_IVADEDAR_PAIM = 'N'"
             End If
            ' DAHV T6105 - FIN
            
            
            ReDim Arr(0)
            Condi = "CD_CODI_TERC_PAIM=" & Comi & TxtEmpresa(1) & Comi
            Result = LoadData("PARAMETROS_IMPUESTOS", "COUNT(*)", Condi, Arr)
            If Result <> FAIL Then
              If Arr(0) > 0 Then
                 Result = DoUpdate("PARAMETROS_IMPUESTOS", Valores, Condi)
                 'DAHV T6226 - INICIO
                 Result = Auditor("PARAMETROS_IMPUESTOS", TranUpd, LastCmd)
                'DAHV T6226 - INICIO
              Else
                 Valores = Valores & Coma & "CD_CODI_TERC_PAIM =" & Comi & TxtEmpresa(1) & Comi
                 Result = DoInsertSQL("PARAMETROS_IMPUESTOS", Valores)
                 'DAHV T6226 - INICIO
                  Result = Auditor("PARAMETROS_IMPUESTOS", TranIns, LastCmd)
                 'DAHV T6226 - INICIO
              End If
            End If
            Call Costo_Niif 'SKRV T24261/R22366
            If Result <> FAIL Then
                Valores = "ID_IDE2_CONT = " & Comi & CodCeco(CboCeco.ListIndex) & Comi & Coma
                Valores = Valores & "ID_CONT_CONT = " & IIf(CboTipoE.ListIndex = 0, "'C'", IIf(CboTipoE.ListIndex = 1, "'E'", "'O'")) & Coma
                Valores = Valores & "CD_ACEC_CONT = " & Comi & Cod_ActEcon(CboActividad.ListIndex) & Comi
                Result = DoUpdate("TC_CONTROL", Valores, NUL$)
                If Result <> FAIL Then
                   Entidad = TxtEmpresa(0)
                   Nit = TxtEmpresa(1)
                   'Id_Empresa = TxtEmpresa(2)
                   Result = Auditor("TC_CONTROL", TranUpd, LastCmd)
                End If
            End If
          End If
       End If
     End If
    End If
    
   'DRMG T44728-R41288 INICIO
   If WarnMsg("Desea guardar informacion de Factura Electr�nica?.") Then
       Result = Factura_Electronica
       If Result <> FAIL Then Call Mensaje1("Se han almacenado correctamente los datos de la factura electr�nica", 3) 'DRMG T45135
   End If
   'DRMG T44728-R41288 FIN
    
    If (Result <> FAIL) Then
      If (CommitTran() <> FAIL) Then
         Call CargarParametrosAplicacion 'JAUM T32800-R31099
         Unload Me
      Else
         Call RollBackTran
      End If
    Else
      Call RollBackTran
    End If
    Call MouseNorm
    Msglin NUL$
End Sub

Private Sub CboDpto_LostFocus()
If CboDpto.ListIndex <> -1 Then
    Result = loadctrl("MUNICIPIOS ", "NO_NOMB_MUNI", "CD_CODI_MUNI", CboCiudad, Cod_Muni(), "CD_CODI_DPTO_MUNI='" & Cod_Dpto(CboDpto.ListIndex) & "' order by NO_NOMB_MUNI")
    If (Result = FAIL) Then
         Call Mensaje1("ERROR CARGANDO TABLA [MUNICIPIOS]", 1)
         Exit Sub
    End If
End If
End Sub

Private Sub Cargar_Datos_Tributarios()
   'DAHV T6105 - INICIO
   'ReDim Arr(15)
   ReDim Arr(16)
   'DAHV T6105 - FIN
   Campos = "ID_TIPO_TERC_PAIM,ID_TIPO_REGI_PAIM,ID_TIPO_CONTR_PAIM,ID_AUTO_RETIVA_PAIM,"
   Campos = Campos & "ID_AUTO_RETICA_PAIM,ID_AUTO_RETREN_PAIM,TX_RESO_ARETIVA_PAIM,TX_RESO_ARETICA_PAIM,"
   Campos = Campos & "TX_RESO_ARETREN_PAIM,ID_AREIVA_REGCOM_PAIM,ID_AREIVA_REGSIM_PAIM,ID_AREICA_REGCOM_PAIM,"
   Campos = Campos & "ID_AREICA_REGSIM_PAIM,ID_AREREN_REGCOM_PAIM,ID_AREREN_REGSIM_PAIM,ID_IVADEDU_PAIM"
   'DAHV T6105 - INICIO
   Campos = Campos & " , TX_IVADEDAR_PAIM"
    
   'DAHV T6105 - FIN
   Condicion = "CD_CODI_TERC_PAIM=" & Comi & TxtEmpresa(1) & Comi
   Result = LoadData("PARAMETROS_IMPUESTOS", Campos, Condicion, Arr)
   If Result <> FAIL Then
      If Arr(0) <> NUL$ Then CboTipoPer.ListIndex = Arr(0)
      If Arr(1) <> NUL$ Then CboRegimenIVA.ListIndex = Arr(1)
      If Arr(2) = "S" Then OptSi.value = True Else OptNo.value = True
      ChkAutoRetIva.value = IIf(Arr(3) <> NUL$, Arr(3), 0)
      ChkAutoRetIca.value = IIf(Arr(4) <> NUL$, Arr(4), 0)
      ChkAutoRetRenta.value = IIf(Arr(5) <> NUL$, Arr(5), 0)
      TxtResIVA = Arr(6)
      TxtResICA = Arr(7)
      TxtResRENTA = Arr(8)
      ChkRegComun(0).value = IIf(Arr(9) <> NUL$, Arr(9), 0)
      ChkRegSimplif(0).value = IIf(Arr(10) <> NUL$, Arr(10), 0)
      ChkRegComun(1).value = IIf(Arr(11) <> NUL$, Arr(11), 0)
      ChkRegSimplif(1).value = IIf(Arr(12) <> NUL$, Arr(12), 0)
      ChkRegComun(2).value = IIf(Arr(13) <> NUL$, Arr(13), 0)
      ChkRegSimplif(2).value = IIf(Arr(14) <> NUL$, Arr(14), 0)
      ChkIvaDed.value = IIf(Arr(15) <> NUL$, Arr(15), 0)
      ' DAHV T6105 - INICIO
      If Arr(15) = NUL$ Or Arr(16) = "N" Then
         OptIvaSi(1).value = True
      ElseIf Arr(15) = "S" Then
         OptIvaSi(0).value = True
      End If
   ' DAHV T6105 - FIN
        
   End If
   'JAUM T32800-R31099 Inicio
   If fnDevDato("ENTIDAD", "NU_MOVNIIF_ENTI", NUL$, True) Then
      ChkGenMovNiif.value = 1
   Else
      ChkGenMovNiif.value = 0
   End If
   'DRMG T44413-R44232 INICIO
   'SI EN CONTABILIDAD NIIF O PACIENTESTES ESTA MARCADA LA OPCION 'SOLO AFECTAR NIIF'
   'NO SE VUSUALIZARA EL CHEK DE GENERAR MOVIMIENTO NIIF
   If fnDevDato("ENTIDAD", "NU_NIIF_ENTI", NUL$, True) Then
      ChkGenMovNiif.value = 1
      ChkGenMovNiif.Enabled = False
   End If
   'DRMG T44413-R44232 FIN
   'JAUM T33370 Inicio se deja linea en comentario
   'If ChkGenMovNiif.Value = 0 Then FrmDatos.Visible = False
   If ChkGenMovNiif.value = 0 Then
      FrmDatos.Height = 600
      LblCostoNIIF.Visible = False
      OptUCosto.Visible = False
      OptVMercado.Visible = False
   End If
   'JAUM T33370 Fin
   'JAUM T32800-R31099 Fin
   'SKRV T24261/R22366 INICIO
   'Campos = fnDevDato("PARAM_NIIF", "NU_COSTONIIF_PANI", "NU_CODMOD_PANI=6") 'SKRV T24951 COMENTARIO
   Campos = fnDevDato("PARAM_NIIF", "NU_COSTONIIF_PANI", "NU_CODMOD_PANI=6", True) 'SKRV T24951
   
   If Campos <> NUL$ Then
      Select Case Campos
         Case "Falso": OptUCosto.value = True
         Case "Verdadero": OptVMercado.value = True
      End Select
   End If
   'SKRV T24261/R22366 FIN
End Sub

Private Sub TxtEmpresa_Validate(Index As Integer, Cancel As Boolean)
    If TxtEmpresa(1) <> NUL$ Then TxtDigVerif = DigitoNIT(CStr(TxtEmpresa(1)))
End Sub



'---------------------------------------------------------------------------------------
' Procedure : TxtFactFin_KeyPress
' DateTime  : 15/05/2018 11:49
' Author    : daniel_mesa
' Purpose   : DRMG T44728-R41288 Valida que solo se ingrese caracteres numericos
'---------------------------------------------------------------------------------------
'
Private Sub TxtFactFin_KeyPress(KeyAscii As Integer)
   Call ValKeyNum(KeyAscii)
   Call Cambiar_Enter(KeyAscii)
End Sub

'---------------------------------------------------------------------------------------
' Procedure : TxtFactFin_LostFocus
' DateTime  : 15/05/2018 12:38
' Author    : daniel_mesa
' Purpose   : DRMG T44728-R41288 valida si la factura final ya existe
'---------------------------------------------------------------------------------------
'
Private Sub TxtFactFin_LostFocus()
   If TxtFactFin.Text = NUL$ Then Exit Sub
   ReDim VrArr(0)
   'DRMG T45124 INICIO ' LAS SIGUIENTES DOS LINEAS SE DEJA EN COMENTARIO
   'Condicion = "NU_CNSELT_RFVE= " & TxtFactFin.Text
   'Result = LoadData("R_FACTURA_VENT_ELECT", "NU_CNSELT_RFVE", Condicion, VrArr())
   Condicion = "NU_CNFAVE_FAFA= " & TxtFactFin.Text
   Result = LoadData("R_FAVE_FAEL", "NU_CNFAVE_FAFA", Condicion, VrArr())
   'DRMG T45124 FIN
   If Result <> FAIL And Encontro Then
      Call Mensaje1("El Rango de la Factura Final ya Existe, Se recomienda Seleccionar otra que no exista", 3)
      TxtFactFin.Text = NUL$
      TxtFactFin.SetFocus
      Call MouseNorm: Exit Sub
   Else 'DRMG T45218
      If BoFaVe Then Call Grabar_Auto_Fael 'DRMG T45218
   End If
End Sub

'---------------------------------------------------------------------------------------
' Procedure : TxtFactIni_KeyPress
' DateTime  : 17/05/2018 10:51
' Author    : daniel_mesa
' Purpose   : DRMG T44728-R41288 Valida que solo se ingrese caracteres numericos
'---------------------------------------------------------------------------------------
'
Private Sub TxtFactIni_KeyPress(KeyAscii As Integer)
   Call ValKeyNum(KeyAscii)
   Call Cambiar_Enter(KeyAscii)
End Sub

'---------------------------------------------------------------------------------------
' Procedure : TxtFactIni_LostFocus
' DateTime  : 17/05/2018 12:18
' Author    : daniel_mesa
' Purpose   : DRMG T44728-R41288 valida si la factura inicial ya existe con ese consecutivo
'---------------------------------------------------------------------------------------
'
Private Sub TxtFactIni_LostFocus()
   If TxtFactIni.Text = NUL$ Then Exit Sub
   ReDim VrArr(0)
   'DRMG T45124 INICIO ' LAS SIGUIENTES DOS LINEAS SE DEJA EN COMENTARIO
   'Condicion = "NU_CNSELT_RFVE= " & TxtFactIni.Text
   'Result = LoadData("R_FACTURA_VENT_ELECT", "NU_CNSELT_RFVE", Condicion, VrArr())
   Condicion = "NU_CNFAVE_FAFA= " & TxtFactIni.Text
   Result = LoadData("R_FAVE_FAEL", "NU_CNFAVE_FAFA", Condicion, VrArr())
   'DRMG T45124 FIN
   If Result <> FAIL And Encontro Then
      Call Mensaje1("El Rango de la Factura Inicial ya Existe, Se recomienda Seleccionar otra que no exista", 3)
      TxtFactIni.Text = NUL$
      TxtFactIni.SetFocus
      Call MouseNorm: Exit Sub
   Else 'DRMG T45218
      If BoFaVe Then Call Grabar_Auto_Fael 'DRMG T45218
   End If
End Sub

'---------------------------------------------------------------------------------------
' Procedure : TxtFirElect_KeyPress
' DateTime  : 17/05/2018 12:28
' Author    : daniel_mesa
' Purpose   : DRMG T44728-41288 VALIDA QUE SOLO ASECTE CARACTERES ALFANUMERICOS
'---------------------------------------------------------------------------------------
'
Private Sub TxtFirElect_KeyPress(KeyAscii As Integer)
   Call Cambiar_Enter(KeyAscii)
End Sub

Private Sub TxtIdRepresentante_KeyPress(KeyAscii As Integer)
   Call Cambiar_Enter(KeyAscii) 'smdl m1952
End Sub
Private Sub TxtIndicativo_KeyPress(KeyAscii As Integer) 'JACC M4314
    Call Cambiar_Enter(KeyAscii)
    Call ValKeyNum(KeyAscii)
End Sub

'---------------------------------------------------------------------------------------
' Procedure : TxtPassword_KeyPress
' DateTime  : 13/06/2018 17:27
' Author    : daniel_mesa
' Purpose   : DRMG T44728-R41288 cambia enter como tab
'---------------------------------------------------------------------------------------
'
Private Sub TxtPassword_KeyPress(KeyAscii As Integer)
   Call Cambiar_Enter(KeyAscii)
End Sub

'---------------------------------------------------------------------------------------
' Procedure : TxtPreFijo_KeyPress
' DateTime  : 17/05/2018 10:55
' Author    : daniel_mesa
' Purpose   : DRMG T44728-41288 VALIDA QUE SOLO ASECTE CARACTERES ALFANUMERICOS
'---------------------------------------------------------------------------------------
'
Private Sub TxtPreFijo_KeyPress(KeyAscii As Integer)
   Call ValKeyAlfaNumSinCar(KeyAscii)
   Call Cambiar_Enter(KeyAscii)
End Sub

Private Sub TxtRepresentante_KeyPress(KeyAscii As Integer)
   Call Cambiar_Enter(KeyAscii) 'smdl m1952
End Sub
Private Sub TxtResICA_KeyPress(KeyAscii As Integer)
   Call Cambiar_Enter(KeyAscii) 'smdl m1952
End Sub

Private Sub TxtResIVA_KeyPress(KeyAscii As Integer)
   Call Cambiar_Enter(KeyAscii) 'smdl m1952
End Sub
Private Sub TxtResRENTA_KeyPress(KeyAscii As Integer)
   Call Cambiar_Enter(KeyAscii) 'smdl m1952
End Sub


'---------------------------------------------------------------------------------------
' Procedure : Costo_Niif
' DateTime  : 15/10/2014 10:34
' Author    : sinthia_rodriguez
' Purpose   : T24261/R22366
'---------------------------------------------------------------------------------------
'
Private Sub Costo_Niif()
   Dim StVal As String
   Dim InCost As Integer
   
   If OptUCosto.value = True Then InCost = 0
   If OptVMercado.value = True Then InCost = 1
   
   Valores = "NU_COSTONIIF_PANI=" & InCost
   Condicion = " NU_CODMOD_PANI=6"
   Result = DoUpdate("PARAM_NIIF", Valores, Condicion)
   
   If Result <> FAIL Then Result = Auditor("PARAM_NIIF", TranUpd, LastCmd)
End Sub

'---------------------------------------------------------------------------------------
' Procedure : Factura_Electronica
' DateTime  : 17/05/2018 10:40
' Author    : daniel_mesa
' Purpose   : DRMG T44728-R41288 guarda la informacion de Factura electronica
'---------------------------------------------------------------------------------------
'
Private Function Factura_Electronica()

   If TxtFactIni.Text = NUL$ Then
      Call Mensaje1("Falta ingresar la factura inicial", 3)
      TxtFactIni.SetFocus
      Call MouseNorm
      Factura_Electronica = FAIL
      Exit Function
   End If
   If TxtFactFin.Text = NUL$ Then
      Call Mensaje1("Falta ingresar la factura final", 3)
      TxtFactFin.SetFocus
      Call MouseNorm
      Factura_Electronica = FAIL
      Exit Function
   End If
   
   If Not CDbl(TxtFactIni.Text) < CDbl(TxtFactFin.Text) Then
      Call Mensaje1("El Rango de la factura inicial debe ser menor a la factura final", 3)
      TxtFactIni.Text = NUL$
      TxtFactIni.SetFocus
      Call MouseNorm
      Factura_Electronica = FAIL
      Exit Function
   End If
   If CDbl(TxtFactIni.Text) > CDbl(TxtFactFin.Text) Then
      Call Mensaje1("El rango de la factura final debe ser mayor que la factura inical", 3)
      TxtFactFin.Text = NUL$
      TxtFactFin.SetFocus
      Call MouseNorm
      Factura_Electronica = FAIL
      Exit Function
   End If
   
   If Not CDbl(TxtConFact.Text) >= (CDbl(TxtFactIni.Text) - 1) Then
      Call Mensaje1("El consecutivo debe ser mayor a la factura inicial.", 3)
      TxtConFact.Text = NUL$
      TxtConFact.SetFocus
      Call MouseNorm
      Factura_Electronica = FAIL
      Exit Function
   End If
   
   If Not CDbl(TxtConFact.Text) <= CDbl(TxtFactFin.Text) Then
      Call Mensaje1("El consecutivo debe ser menor a la factura final.", 3)
      TxtConFact.Text = NUL$
      TxtConFact.SetFocus
      Call MouseNorm
      Factura_Electronica = FAIL
      Exit Function
   End If
   
   ReDim VrArr(0)
   'DRMG T45124 INICIO ' LAS SIGUIENTES DOS LINEAS SE DEJA EN COMENTARIO
   'Condicion = "NU_CNSELT_RFVE<=" & TxtFactIni.Text & " and NU_CNSELT_RFVE>=" & TxtFactFin.Text
   'Result = LoadData("R_FACTURA_VENT_ELECT", "NU_CNSELT_RFVE", Condicion, VrArr())
   Condicion = "NU_CNFAVE_FAFA<=" & TxtFactIni.Text & " and NU_CNFAVE_FAFA>=" & TxtFactFin.Text
   Result = LoadData("R_FAVE_FAEL", "NU_CNFAVE_FAFA", Condicion, VrArr())
   'DRMG T45124 FIN
   If Result <> FAIL And Encontro Then
      Call Mensaje1("El Numero de la Factura ya Existe en Base de Datos, Porfavor un Rango Diferente", 3)
      TxtFactIni.SetFocus
      Call MouseNorm
      Factura_Electronica = FAIL
      Exit Function
   End If
   
   If TxtAutori.Text = NUL$ Then
      Call Mensaje1("Falta ingresar el No. autorizaci�n", 3)
      TxtAutori.SetFocus
      Call MouseNorm
      Factura_Electronica = FAIL
      Exit Function
   End If
   
   'DRMG T45866 INICIO SE DEJA EN COMENTARIO EL SIGUIENTE BLOQUE
'   If TxtCTecni.Text = NUL$ Then
'      Call Mensaje1("Falta Ingresar la clave t�cnica", 3)
'      TxtCTecni.SetFocus
'      Call MouseNorm
'      Factura_Electronica = FAIL
'      Exit Function
'   End If
   'DRMG T45866 FIN
   If MskFechAut.Text = NUL$ Then
      Call Mensaje1("Falta Ingresar la Fecha Autorizada", 3)
      MskFechAut.SetFocus
      Call MouseNorm
      Factura_Electronica = FAIL
      Exit Function
   End If
   If MskFechFin.Text = NUL$ Then
      Call Mensaje1("Falta Ingresar la Fecha Final", 3)
      MskFechFin.SetFocus
      Call MouseNorm
      Factura_Electronica = FAIL
      Exit Function
   End If
   If MskFechFin.Text = MskFechAut.Text Then
      Call Mensaje1("La fecha de autorizaci�n y la fecha final no pueden ser iguales", 3)
      MskFechAut.SetFocus
      Call MouseNorm
      Factura_Electronica = FAIL
      Exit Function
   End If
   
   
   If TxtUrl(0).Text = NUL$ Then
      Call Mensaje1("Falta ingresar la ruta de la factura electr�nica", 3)
      SSTabFael.Tab = 0
      TxtUrl(0).SetFocus
      Call MouseNorm
      Factura_Electronica = FAIL
      Exit Function
   End If
   
   If TxtUrl(1).Text = NUL$ Then
      Call Mensaje1("Falta ingresar la ruta del certificado digital", 3)
      SSTabFael.Tab = 0
      TxtUrl(1).SetFocus
      Call MouseNorm
      Factura_Electronica = FAIL
      Exit Function
   End If
   
   If TxtPassword.Text = NUL$ Then
      Call Mensaje1("Falta ingresar la contrase�a del certificado", 3)
      SSTabFael.Tab = 0
      TxtPassword.SetFocus
      Call MouseNorm
      Factura_Electronica = FAIL
      Exit Function
   End If
   
   If TxtIdSoft.Text = NUL$ Then
      'Call Mensaje1("Falta ingresar el usuario de la DIAN", 3) 'DRMG T45135 SE DEJA EN COMENTARIO
      Call Mensaje1("Falta ingresar el Id de Software", 3) 'DRMG T45135
      SSTabFael.Tab = 2
      TxtIdSoft.SetFocus
      Call MouseNorm
      Factura_Electronica = FAIL
      Exit Function
   End If
   If TxtPin.Text = NUL$ Then
      'Call Mensaje1("Falta ingresar la contrase�a de la DIAN", 3) 'DRMG T45135 SE DEJA EN COMENTARIO
      Call Mensaje1("Falta ingresar el Pin", 3) 'DRMG T45135
      SSTabFael.Tab = 2
      TxtPin.SetFocus
      Call MouseNorm
      Factura_Electronica = FAIL
      Exit Function
   End If
   'DRMG T45150 INICIO
   If TxtCont.Text = NUL$ Then
      Call Mensaje1("Falta ingresar la contrase�a software", 3)
      SSTabFael.Tab = 2
      TxtCont.SetFocus
      Call MouseNorm
      Exit Function
   End If
   'DRMG T45150 FIN
   If TxtUrlEnv.Text = NUL$ Then
      Call Mensaje1("Falta ingresar la URL de envio", 3)
      SSTabFael.Tab = 3
      TxtUrlEnv.SetFocus
      Call MouseNorm
      Factura_Electronica = FAIL
      Exit Function
   End If
   If TxtUrlCons.Text = NUL$ Then
      'Call Mensaje1("Falta ingresar la URL de la consulta", 3) 'DRMG T45135 SE DEJA EN COMENTARIO
      Call Mensaje1("Falta ingresar la URL de consulta", 3) 'DRMG T45135
      SSTabFael.Tab = 3
      TxtUrlCons.SetFocus
      Call MouseNorm
      Factura_Electronica = FAIL
      Exit Function
   End If
   

   Valores = "NU_MODULO_FAEL=1"
   Valores = Valores & Coma & "NU_FACINI_FAEL=" & TxtFactIni.Text
   Valores = Valores & Coma & "NU_FACFIN_FAEL=" & TxtFactFin.Text
   Valores = Valores & Coma & "TX_PREFI_FAEL='" & TxtPreFijo.Text & Comi
   Valores = Valores & Coma & "TX_AUTORI_FAEL='" & TxtAutori.Text & Comi
   'Valores = Valores & Coma & "TX_CTEC_FAEL='" & TxtCTecni.Text & Comi 'DRMG T45866 SE DEJA EN COMENTARIO
   Valores = Valores & Coma & "FE_AUTORI_FAEL='" & Format(MskFechAut.Text, UCase(FormatF)) & Comi
   Valores = Valores & Coma & "FE_FINAUT_FAEL='" & Format(MskFechFin.Text, UCase(FormatF)) & Comi
   Valores = Valores & Coma & "TX_URL_FAEL='" & TxtUrl(0).Text & Comi
   Valores = Valores & Coma & "TX_URLCER_FAEL='" & TxtUrl(1).Text & Comi
   'Valores = Valores & Coma & "TX_PSSWRD_FAEL='" & Trim(TxtPassword.Text) & Comi 'DRMG T45448 SE DEJA EN COMENTARIO
   Valores = Valores & Coma & "TX_PSSWRD_FAEL='" & Cambiar_Comas_Comillas(Trim(TxtPassword.Text)) & Comi 'DRMG T45448
   'DRMG T45216 INICIO SE DEJA EN COMENTARIO LAS SIGUIENTES 3 LINEAS
   'Valores = Valores & Coma & "TX_ENCA_FAEL='" & TxtEncabez.Text & Comi
   'Valores = Valores & Coma & "TX_PIEUNO_FAEL='" & TxtPie(0).Text & Comi
   'Valores = Valores & Coma & "TX_PIEDOS_FAEL='" & TxtPie(1).Text & Comi
   Valores = Valores & Coma & "TX_ENCA_FAEL='" & Cambiar_Comas_Comillas(TxtEncabez.Text) & Comi
   Valores = Valores & Coma & "TX_PIEUNO_FAEL='" & Cambiar_Comas_Comillas(TxtPie(0).Text) & Comi
   Valores = Valores & Coma & "TX_PIEDOS_FAEL='" & Cambiar_Comas_Comillas(TxtPie(1).Text) & Comi
   'DRMG T45216 FIN
   Valores = Valores & Coma & "NU_CONFAC_FAEL=" & TxtConFact.Text
   Valores = Valores & Coma & "TX_IDSOFT_FAEL='" & TxtIdSoft.Text & Comi
   'Valores = Valores & Coma & "TX_PIN_FAEL='" & TxtPin.Text & Comi 'DRMG T45448 SE DEJA EN COMENTARIO
   Valores = Valores & Coma & "TX_PIN_FAEL='" & Cambiar_Comas_Comillas(TxtPin.Text) & Comi 'DRMG T45448
   Valores = Valores & Coma & "TX_URLENV_FAEL='" & TxtUrlEnv.Text & Comi
   Valores = Valores & Coma & "TX_URLCON_FAEL='" & TxtUrlCons.Text & Comi
   'Valores = Valores & Coma & "TX_SFPSW_FAEL='" & TxtCont.Text & Comi 'DRMG T45150 'DRMG T45448 SE DEJA EN COMENTARIO
   Valores = Valores & Coma & "TX_SFPSW_FAEL='" & Cambiar_Comas_Comillas(TxtCont.Text) & Comi 'DRMG T45448
   
   ReDim VrArr(0)
   Condicion = "NU_MODULO_FAEL=1"
   Condicion = Condicion & " AND NU_ESTADO_FAEL=0"
   Result = LoadData("FACTURA_ELECTRONICA", "NU_MODULO_FAEL", Condicion, VrArr())
   If Result <> FAIL Then
      If Not Encontro Then
         Valores = Valores & Coma & "NU_ESTADO_FAEL=0"
         Result = DoInsertSQL("FACTURA_ELECTRONICA", Valores)
      Else
         If BoFaVe Then
            Result = DoUpdate("FACTURA_ELECTRONICA", "NU_ESTADO_FAEL=1", Condicion)
            If Result <> FAIL Then
               Valores = Valores & Coma & "NU_ESTADO_FAEL=0"
               Result = DoInsertSQL("FACTURA_ELECTRONICA", Valores)
            End If
         Else
            Result = DoUpdate("FACTURA_ELECTRONICA", Valores, Condicion)
         End If
      End If
   End If
   
   If Result <> FAIL Then Result = Auditor("FACTURA_ELECTRONICA", TranIns, LastCmd)
   
   If MotorBD = "SQL" Then
      If ImgLogo.Picture <> 0 Then
         Result = AdjuntarImagen("FACTURA_ELECTRONICA", "IM_LOGO_FAEL", ImgLogo, Condicion)
      Else
         Result = DoUpdate("FACTURA_ELECTRONICA", "IM_LOGO_FAEL='" & Comi, Condicion)
      End If
   End If
   
   If Result = FAIL Then Exit Function
   If TxtFactIni.Text = NUL$ Then
      Condicion = "NU_MODULO_FAEL=1"
      Condicion = Condicion & " AND NU_ESTADO_FAEL=0"
      Valores = "NU_CONFAC_FAEL=" & TxtFactIni.Text - 1
      If Result <> FAIL Then Result = DoUpdate("FACTURA_ELECTRONICA", Valores, Condicion)
   End If
   
   
   If Result <> FAIL Then
      'ReDim VrArr(18) 'DRMG T45150 SE DEJA EN COMENTARIO
      ReDim VrArr(19) 'DRMG T45150
      Valores = "NU_MODULO_FAEL,NU_FACINI_FAEL,NU_FACFIN_FAEL,TX_PREFI_FAEL,TX_AUTORI_FAEL"
      Valores = Valores & Coma & "TX_CTEC_FAEL,FE_AUTORI_FAEL,FE_FINAUT_FAEL,TX_URL_FAEL"
      Valores = Valores & Coma & "TX_URLCER_FAEL,TX_PSSWRD_FAEL,NU_CONFAC_FAEL"
      Valores = Valores & Coma & "TX_ENCA_FAEL,TX_PIEUNO_FAEL,TX_PIEDOS_FAEL"
      Valores = Valores & Coma & "TX_IDSOFT_FAEL,TX_PIN_FAEL,TX_URLENV_FAEL,TX_URLCON_FAEL"
      Valores = Valores & Coma & "TX_SFPSW_FAEL" 'DRMG T45150
      Condicion = "NU_MODULO_FAEL=1 AND NU_ESTADO_FAEL=0"
      Result = LoadData("FACTURA_ELECTRONICA", Valores, Condicion, VrArr())
      If Result <> FAIL Then
         If Encontro Then
            Call BUSCAR_FACTELECT(VrArr())
            Result = DoDelete("AUTO_FAEL", "NU_MODULO_AUFA=1") 'DRMG T4521/
            BoFaVe = False 'DRMG T45218
         End If
      End If
   End If
   
   Factura_Electronica = Result
   
End Function

'---------------------------------------------------------------------------------------
' Procedure : BUSCAR_FACTELECT
' DateTime  : 17/05/2018 10:29
' Author    : daniel_mesa
' Purpose   : DRMG T44728-R41288 'deshabilita y coloca la informacion encontrada en los campos de facturacion electronica
'---------------------------------------------------------------------------------------
'
Private Function BUSCAR_FACTELECT(VrFac() As Variant)

   TxtFactIni.Text = VrFac(1)
   TxtFactFin.Text = VrFac(2)
   TxtPreFijo.Text = VrFac(3)
   TxtAutori.Text = VrFac(4)
   'TxtCTecni.Text = VrFac(5) 'DRMG T45866 SE DEJA EN COMENTARIO
   MskFechAut.Text = Format(VrFac(6), "DD/MM/YYYY")
   MskFechFin.Text = Format(VrFac(7), "DD/MM/YYYY")
   TxtUrl(0).Text = VrFac(8)
   TxtUrl(1).Text = VrFac(9)
   TxtPassword.Text = CStr(VrFac(10))
   TxtConFact = VrFac(11)
   TxtEncabez = VrFac(12)
   TxtPie(0) = VrFac(13)
   TxtPie(1) = VrFac(14)
   TxtIdSoft = VrFac(15)
   TxtPin = VrFac(16)
   TxtUrlEnv = VrFac(17)
   TxtUrlCons = VrFac(18)
   TxtCont = VrFac(19) 'DRMG T45150
   
   TxtFactIni.Enabled = False
   TxtFactFin.Enabled = False
   TxtPreFijo.Enabled = False
   TxtAutori.Enabled = False
   'TxtCTecni.Enabled = False 'DRMG T45866 SE DEJA EN COMENTARIO
   MskFechAut.Enabled = False
   MskFechFin.Enabled = False
   TxtConFact.Enabled = False
   
   ImgLogo.Tag = False
   Condicion = "NU_MODULO_FAEL=1 AND NU_ESTADO_FAEL=0"
   If MotorBD = "SQL" Then Call LeerImagen("FACTURA_ELECTRONICA", "IM_LOGO_FAEL", ImgLogo, Condicion)
   
End Function

'---------------------------------------------------------------------------------------
' Procedure : ValiFeInMenorFeFin
' DateTime  : 17/05/2018 10:44
' Author    : daniel_mesa
' Purpose   : DRMG T44728-R41288 valida que la fecha Autorizada no sea mayor que la final
'---------------------------------------------------------------------------------------
'
Sub ValiFeInMenorFeFin()
   If DateDiff("yyyy", MskFechAut, MskFechFin) < 0 Then Call Mensaje1("La fecha Autorizada no puede ser mayor a la fecha final", 3): MskFechFin = Format(FechaServer, "dd/mm/yyyy"): MskFechAut.SetFocus: Exit Sub
   If DateDiff("m", MskFechAut, MskFechFin) < 0 And DateDiff("yyyy", MskFechAut, MskFechFin) <= 0 Then Call Mensaje1("La fecha Autorizada no puede ser mayor a la fecha final", 3): MskFechFin = Format(FechaServer, "dd/mm/yyyy"): MskFechAut.SetFocus: Exit Sub
   If DateDiff("d", MskFechAut, MskFechFin) < 0 Then Call Mensaje1("La fecha Autorizada no puede ser mayor a la fecha final", 3): MskFechFin = Format(FechaServer, "dd/mm/yyyy"): MskFechAut.SetFocus
End Sub

'---------------------------------------------------------------------------------------
' Procedure : TxtUrl_KeyPress
' DateTime  : 13/06/2018 17:26
' Author    : daniel_mesa
' Purpose   : DRMG T44728-R41288 Cambia enter como tab
'---------------------------------------------------------------------------------------
'
Private Sub TxtUrl_KeyPress(Index As Integer, KeyAscii As Integer)
   Call Cambiar_Enter(KeyAscii)
End Sub

'---------------------------------------------------------------------------------------
' Procedure : ScmdLogo_Click
' DateTime  : 04/07/2018 10:50
' Author    : daniel_mesa
' Purpose   : DRMG T44728-R41288 EXAMINA UN NUEVO LOGO
'---------------------------------------------------------------------------------------
'
Private Sub ScmdLogo_Click(Index As Integer)
   If MotorBD <> "SQL" Then Exit Sub
      
   On Error GoTo ERR
   Select Case Index
      Case 0: MDI_Inventarios.CMDialog1.Filter = "Pictures (*.bmp;*.jpg;*.gif)|*.bmp;*.jpg;*.gif"
         MDI_Inventarios.CMDialog1.ShowOpen
         If MDI_Inventarios.CMDialog1.filename <> "" Then
            ImgLogo.Picture = LoadPicture(MDI_Inventarios.CMDialog1.filename)
            ImgLogo.Tag = True
         End If
      Case 1: ImgLogo.Picture = LoadPicture()
         ImgLogo.Tag = True
   End Select
   Exit Sub
    
ERR:
   Call Mensaje1("La imagen no es valida", 2)
End Sub

'---------------------------------------------------------------------------------------
' Procedure : TxtUrl_GotFocus
' DateTime  : 27/09/2018 09:45
' Author    : daniel_mesa
' Purpose   : DRMG T44883-R41288 AL TENER EL FOCO LO MOSTRAR EN EL SSTAB CORRESPONDIENTE
'---------------------------------------------------------------------------------------
'
Private Sub TxtUrl_GotFocus(Index As Integer)
   SSTabFael.Tab = 0
   SendKeys "{F4}", True
End Sub

'---------------------------------------------------------------------------------------
' Procedure : TxtUrlEnv_GotFocus
' DateTime  : 27/09/2018 09:51
' Author    : daniel_mesa
' Purpose   : DRMG T44883-R41288 AL TENER EL FOCO LO MOSTRAR EN EL SSTAB CORRESPONDIENTE
'---------------------------------------------------------------------------------------
'
Private Sub TxtUrlEnv_GotFocus()
   SSTabFael.Tab = 3
   SendKeys "{F4}", True
End Sub

'---------------------------------------------------------------------------------------
' Procedure : TxtIdSoft_GotFocus
' DateTime  : 27/09/2018 09:49
' Author    : daniel_mesa
' Purpose   : DRMG T44883-R41288 AL TENER EL FOCO LO MOSTRAR EN EL SSTAB CORRESPONDIENTE
'---------------------------------------------------------------------------------------
'
Private Sub TxtIdSoft_GotFocus()
   SSTabFael.Tab = 2
   SendKeys "{F4}", True
End Sub

'---------------------------------------------------------------------------------------
' Procedure : TxtIdSoft_KeyPress
' DateTime  : 27/09/2018 10:07
' Author    : daniel_mesa
' Purpose   : DRMG T44883-R41288 Cambia enter como tab
'---------------------------------------------------------------------------------------
'
Private Sub TxtIdSoft_KeyPress(KeyAscii As Integer)
   Call Cambiar_Enter(KeyAscii)
End Sub

'---------------------------------------------------------------------------------------
' Procedure : TxtPin_KeyPress
' DateTime  : 27/09/2018 10:08
' Author    : daniel_mesa
' Purpose   : DRMG T44883-R41288 Cambia enter como tab
'---------------------------------------------------------------------------------------
'
Private Sub TxtPin_KeyPress(KeyAscii As Integer)
   Call Cambiar_Enter(KeyAscii)
End Sub

'---------------------------------------------------------------------------------------
' Procedure : ChkVer_KeyPress
' DateTime  : 27/09/2018 10:08
' Author    : daniel_mesa
' Purpose   : DRMG T44883-R41288 Cambia enter como tab
'---------------------------------------------------------------------------------------
'
'Private Sub ChkVer_KeyPress(KeyAscii As Integer) 'DRMG T45150 SE DEJA EN COMENTARIO
Private Sub ChkVer_KeyPress(Index As Integer, KeyAscii As Integer) 'DRMG T45150
   Call Cambiar_Enter(KeyAscii)
End Sub

'---------------------------------------------------------------------------------------
' Procedure : TxtUrlEnv_KeyPress
' DateTime  : 27/09/2018 10:08
' Author    : daniel_mesa
' Purpose   : DRMG T44883-R41288 Cambia enter como tab
'---------------------------------------------------------------------------------------
'
Private Sub TxtUrlEnv_KeyPress(KeyAscii As Integer)
   Call Cambiar_Enter(KeyAscii)
End Sub

'---------------------------------------------------------------------------------------
' Procedure : TxtUrlCons_KeyPress
' DateTime  : 27/09/2018 10:09
' Author    : daniel_mesa
' Purpose   : DRMG T44883-R41288 Cambia enter como tab
'---------------------------------------------------------------------------------------
'
Private Sub TxtUrlCons_KeyPress(KeyAscii As Integer)
   Call Cambiar_Enter(KeyAscii)
End Sub

'---------------------------------------------------------------------------------------
' Procedure : TxtEncabez_KeyPress
' DateTime  : 27/09/2018 11:47
' Author    : daniel_mesa
' Purpose   : DRMG T44883-R41288 Cambia enter como tab
'---------------------------------------------------------------------------------------
'
Private Sub TxtEncabez_KeyPress(KeyAscii As Integer)
   'Call ValKeyAlfaNum(KeyAscii) 'DRMG T45216 SE DEJA EN COMENTARIO
   Call Cambiar_Enter(KeyAscii)
End Sub

'---------------------------------------------------------------------------------------
' Procedure : TxtPie_KeyPress
' DateTime  : 27/09/2018 11:48
' Author    : daniel_mesa
' Purpose   : DRMG T44883-R41288 Cambia enter como tab
'---------------------------------------------------------------------------------------
'
Private Sub TxtPie_KeyPress(Index As Integer, KeyAscii As Integer)
   'Call ValKeyAlfaNum(KeyAscii) 'DRMG T45216 SE DEJA EN COMENTARIO
   Call Cambiar_Enter(KeyAscii)
End Sub

'---------------------------------------------------------------------------------------
' Procedure : ScmdLogo_KeyPress
' DateTime  : 16/07/2018 11:33
' Author    : daniel_mesa
' Purpose   : DRMG T44883-R41288 Hace pasar el enter como tabulador
'---------------------------------------------------------------------------------------
'
Private Sub ScmdLogo_KeyPress(Index As Integer, KeyAscii As Integer)
   Call Cambiar_Enter(KeyAscii)
End Sub

'---------------------------------------------------------------------------------------
' Procedure : TxtCont_KeyPress
' DateTime  : 10/10/2018 16:59
' Author    : daniel_mesa
' Purpose   : DRMG T45150
'---------------------------------------------------------------------------------------
'
Private Sub TxtCont_KeyPress(KeyAscii As Integer)
   Call Cambiar_Enter(KeyAscii)
End Sub

'---------------------------------------------------------------------------------------
' Procedure : Grabar_Auto_Fael
' DateTime  : 24/10/2018 17:46
' Author    : daniel_mesa
' Purpose   : DRMG T45218 ALMACENA LA INFORMACION EN LA TABLA SECUNDARISA AUTO_FAEL
'---------------------------------------------------------------------------------------
'
Sub Grabar_Auto_Fael()

   Valores = "NU_MODULO_AUFA=1"
   Valores = Valores & Coma & "NU_FACINI_AUFA=" & IIf(TxtFactIni.Text = NUL$, 0, TxtFactIni)
   Valores = Valores & Coma & "NU_FACFIN_AUFA=" & IIf(TxtFactFin.Text = NUL$, 0, TxtFactFin)
   Valores = Valores & Coma & "TX_PREFI_AUFA='" & TxtPreFijo.Text & Comi
   Valores = Valores & Coma & "TX_AUTORI_AUFA='" & TxtAutori.Text & Comi
   'Valores = Valores & Coma & "TX_CTEC_AUFA='" & TxtCTecni.Text & Comi 'DRMG T45866 SE DEJA EN COMENTARIO
   Valores = Valores & Coma & "FE_AUTORI_AUFA='" & Format(MskFechAut.Text, UCase(FormatF)) & Comi
   Valores = Valores & Coma & "FE_FINAUT_AUFA='" & Format(MskFechFin.Text, UCase(FormatF)) & Comi
   Valores = Valores & Coma & "TX_URL_AUFA='" & TxtUrl(0).Text & Comi
   Valores = Valores & Coma & "TX_URLCER_AUFA='" & TxtUrl(1).Text & Comi
   'Valores = Valores & Coma & "TX_PSSWRD_AUFA='" & Trim(TxtPassword.Text) & Comi 'DRMG T45448 SE DEJA EN COMENTARIO
   Valores = Valores & Coma & "TX_PSSWRD_AUFA='" & Cambiar_Comas_Comillas(Trim(TxtPassword.Text)) & Comi 'DRMG T45448
   Valores = Valores & Coma & "TX_ENCA_AUFA='" & Cambiar_Comas_Comillas(TxtEncabez.Text) & Comi
   Valores = Valores & Coma & "TX_PIEUNO_AUFA='" & Cambiar_Comas_Comillas(TxtPie(0).Text) & Comi
   Valores = Valores & Coma & "TX_PIEDOS_AUFA='" & Cambiar_Comas_Comillas(TxtPie(1).Text) & Comi
   Valores = Valores & Coma & "NU_CONFAC_AUFA=" & IIf(TxtConFact.Text = NUL$, 0, TxtConFact.Text)
   Valores = Valores & Coma & "TX_IDSOFT_AUFA='" & TxtIdSoft.Text & Comi
   'Valores = Valores & Coma & "TX_PIN_AUFA='" & TxtPin.Text & Comi 'DRMG T45448 SE DEJA EN COMENTARIO
   Valores = Valores & Coma & "TX_PIN_AUFA='" & Cambiar_Comas_Comillas(TxtPin.Text) & Comi 'DRMG T45448
   Valores = Valores & Coma & "TX_URLENV_AUFA='" & TxtUrlEnv.Text & Comi
   Valores = Valores & Coma & "TX_URLCON_AUFA='" & TxtUrlCons.Text & Comi
   'Valores = Valores & Coma & "TX_SFPSW_AUFA='" & TxtCont.Text & Comi 'DRMG T45448 SE DEJA EN COMENTARIO
   Valores = Valores & Coma & "TX_SFPSW_AUFA='" & Cambiar_Comas_Comillas(TxtCont.Text) & Comi 'DRMG T45448
      
   ReDim VrArr(0)
   Condicion = "NU_MODULO_AUFA=1"
   Result = LoadData("AUTO_FAEL", "NU_MODULO_AUFA", Condicion, VrArr())
   If Result <> FAIL Then
      If Not Encontro Then
         Result = DoInsertSQL("AUTO_FAEL", Valores)
      Else
         Result = DoUpdate("AUTO_FAEL", Valores, Condicion)
      End If
   End If
   
   If MotorBD = "SQL" Then
      If ImgLogo.Picture <> 0 Then
         Result = AdjuntarImagen("AUTO_FAEL", "IM_LOGO_AUFA", ImgLogo, Condicion)
      Else
         Result = DoUpdate("AUTO_FAEL", "IM_LOGO_AUFA='" & Comi, Condicion)
      End If
   End If
   
End Sub

'---------------------------------------------------------------------------------------
' Procedure : Buscar_Fael_Temp
' DateTime  : 24/10/2018 18:08
' Author    : daniel_mesa
' Purpose   : DRMG T45218 coloca los resultados de la consulta en el FORMULARIO para facrura electronica
'---------------------------------------------------------------------------------------
'
Sub Buscar_Fael_Temp(VrTemp() As Variant)
   BoFaVe = True

   TxtFactIni.Text = VrTemp(1)
   TxtFactFin.Text = VrTemp(2)
   TxtPreFijo.Text = VrTemp(3)
   TxtAutori.Text = VrTemp(4)
   'TxtCTecni.Text = VrTemp(5) 'DRMG T45866 SE DEJA EN COMENTARIO
   MskFechAut.Text = Format(VrTemp(6), "DD/MM/YYYY")
   MskFechFin.Text = Format(VrTemp(7), "DD/MM/YYYY")
   TxtUrl(0).Text = VrTemp(8)
   TxtUrl(1).Text = VrTemp(9)
   TxtPassword.Text = CStr(VrTemp(10))
   TxtConFact = VrTemp(11)
   TxtEncabez = VrTemp(12)
   TxtPie(0) = VrTemp(13)
   TxtPie(1) = VrTemp(14)
   TxtIdSoft = VrTemp(15)
   TxtPin = VrTemp(16)
   TxtUrlEnv = VrTemp(17)
   TxtUrlCons = VrTemp(18)
   TxtCont = VrTemp(19)
   
   TxtFactIni.Enabled = True
   TxtFactFin.Enabled = True
   TxtPreFijo.Enabled = True
   TxtAutori.Enabled = True
   'TxtCTecni.Enabled = True 'DRMG T45866 SE DEJA EN COMENTARIO
   MskFechAut.Enabled = True
   MskFechFin.Enabled = True
   TxtConFact.Enabled = True
   
   ImgLogo.Tag = False
   Condicion = "NU_MODULO_AUFA=1"
   If MotorBD = "SQL" Then Call LeerImagen("AUTO_FAEL", "IM_LOGO_AUFA", ImgLogo, Condicion)
End Sub

'---------------------------------------------------------------------------------------
' Procedure : TxtUrl_LostFocus
' DateTime  : 24/10/2018 17:17
' Author    : daniel_mesa
' Purpose   : DRMG T45218 SI ES UN NUEVO RANGO O ES EL PRIMER REGUISTRO SE LLAMARA LA FUNCION Grabar_Auto_Fael
'---------------------------------------------------------------------------------------
'
Private Sub TxtUrl_LostFocus(Index As Integer)
   If BoFaVe Then Call Grabar_Auto_Fael
End Sub

'---------------------------------------------------------------------------------------
' Procedure : TxtPassword_LostFocus
' DateTime  : 24/10/2018 17:17
' Author    : daniel_mesa
' Purpose   : DRMG T45218 SI ES UN NUEVO RANGO O ES EL PRIMER REGUISTRO SE LLAMARA LA FUNCION Grabar_Auto_Fael
'---------------------------------------------------------------------------------------
'
Private Sub TxtPassword_LostFocus()
   If BoFaVe Then Call Grabar_Auto_Fael
End Sub

'---------------------------------------------------------------------------------------
' Procedure : TxtEncabez_LostFocus
' DateTime  : 24/10/2018 17:17
' Author    : daniel_mesa
' Purpose   : DRMG T45218 SI ES UN NUEVO RANGO O ES EL PRIMER REGUISTRO SE LLAMARA LA FUNCION Grabar_Auto_Fael
'---------------------------------------------------------------------------------------
'
Private Sub TxtEncabez_LostFocus()
   If BoFaVe Then Call Grabar_Auto_Fael
End Sub

'---------------------------------------------------------------------------------------
' Procedure : TxtPie_LostFocus
' DateTime  : 24/10/2018 17:17
' Author    : daniel_mesa
' Purpose   : DRMG T45218 SI ES UN NUEVO RANGO O ES EL PRIMER REGUISTRO SE LLAMARA LA FUNCION Grabar_Auto_Fael
'---------------------------------------------------------------------------------------
'
Private Sub TxtPie_LostFocus(Index As Integer)
   If BoFaVe Then Call Grabar_Auto_Fael
End Sub

'---------------------------------------------------------------------------------------
' Procedure : TxtIdSoft_LostFocus
' DateTime  : 24/10/2018 17:17
' Author    : daniel_mesa
' Purpose   : DRMG T45218 SI ES UN NUEVO RANGO O ES EL PRIMER REGUISTRO SE LLAMARA LA FUNCION Grabar_Auto_Fael
'---------------------------------------------------------------------------------------
'
Private Sub TxtIdSoft_LostFocus()
   If BoFaVe Then Call Grabar_Auto_Fael
End Sub

'---------------------------------------------------------------------------------------
' Procedure : TxtPin_LostFocus
' DateTime  : 24/10/2018 17:17
' Author    : daniel_mesa
' Purpose   : DRMG T45218 SI ES UN NUEVO RANGO O ES EL PRIMER REGUISTRO SE LLAMARA LA FUNCION Grabar_Auto_Fael
'---------------------------------------------------------------------------------------
'
Private Sub TxtPin_LostFocus()
   If BoFaVe Then Call Grabar_Auto_Fael
End Sub

'---------------------------------------------------------------------------------------
' Procedure : TxtCont_LostFocus
' DateTime  : 24/10/2018 17:17
' Author    : daniel_mesa
' Purpose   : DRMG T45218 SI ES UN NUEVO RANGO O ES EL PRIMER REGUISTRO SE LLAMARA LA FUNCION Grabar_Auto_Fael
'---------------------------------------------------------------------------------------
'
Private Sub TxtCont_LostFocus()
   If BoFaVe Then Call Grabar_Auto_Fael
End Sub

'---------------------------------------------------------------------------------------
' Procedure : TxtUrlEnv_LostFocus
' DateTime  : 24/10/2018 17:17
' Author    : daniel_mesa
' Purpose   : DRMG T45218 SI ES UN NUEVO RANGO O ES EL PRIMER REGUISTRO SE LLAMARA LA FUNCION Grabar_Auto_Fael
'---------------------------------------------------------------------------------------
'
Private Sub TxtUrlEnv_LostFocus()
   If BoFaVe Then Call Grabar_Auto_Fael
End Sub
'---------------------------------------------------------------------------------------
' Procedure : TxtUrlCons_LostFocus
' DateTime  : 24/10/2018 17:15
' Author    : daniel_mesa
' Purpose   : DRMG T45218 SI ES UN NUEVO RANGO O ES EL PRIMER REGUISTRO SE LLAMARA LA FUNCION Grabar_Auto_Fael
'---------------------------------------------------------------------------------------
'
Private Sub TxtUrlCons_LostFocus()
   If BoFaVe Then Call Grabar_Auto_Fael
End Sub

'---------------------------------------------------------------------------------------
' Procedure : ScmdLogo_LostFocus
' DateTime  : 24/10/2018 17:42
' Author    : daniel_mesa
' Purpose   : DRMG T45218 SI ES UN NUEVO RANGO O ES EL PRIMER REGUISTRO SE LLAMARA LA FUNCION Grabar_Auto_Fael
'---------------------------------------------------------------------------------------
'
Private Sub ScmdLogo_LostFocus(Index As Integer)
   If BoFaVe Then Call Grabar_Auto_Fael
End Sub

'---------------------------------------------------------------------------------------
' Procedure : TxtPreFijo_LostFocus
' DateTime  : 24/10/2018 18:04
' Author    : daniel_mesa
' Purpose   : DRMG T45218 SI ES UN NUEVO RANGO O ES EL PRIMER REGUISTRO SE LLAMARA LA FUNCION Grabar_Auto_Fael
'---------------------------------------------------------------------------------------
'
Private Sub TxtPreFijo_LostFocus()
   If BoFaVe Then Call Grabar_Auto_Fael
End Sub

'---------------------------------------------------------------------------------------
' Procedure : TxtAutori_LostFocus
' DateTime  : 24/10/2018 18:04
' Author    : daniel_mesa
' Purpose   : DRMG T45218 SI ES UN NUEVO RANGO O ES EL PRIMER REGUISTRO SE LLAMARA LA FUNCION Grabar_Auto_Fael
'---------------------------------------------------------------------------------------
'
Private Sub TxtAutori_LostFocus()
   If BoFaVe Then Call Grabar_Auto_Fael
End Sub

'---------------------------------------------------------------------------------------
' Procedure : TxtCTecni_LostFocus
' DateTime  : 24/10/2018 18:05
' Author    : daniel_mesa
' Purpose   : DRMG T45218 ES UN NUEVO RANGO O ES EL PRIMER REGUISTRO SE LLAMARA LA FUNCION Grabar_Auto_Fael
'---------------------------------------------------------------------------------------
'
'DRMG T45866 INICIO SE DEJA EN COMENTARIO LAS SIGUIENTES 3 LINEAS
'Private Sub TxtCTecni_LostFocus()
'   If BoFaVe Then Call Grabar_Auto_Fael
'End Sub
'DRMG T45866 FIN SE DEJA EN COMENTARIO


