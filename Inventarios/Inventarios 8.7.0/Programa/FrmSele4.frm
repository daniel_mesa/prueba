VERSION 5.00
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "MSFLXGRD.OCX"
Object = "{0BA686C6-F7D3-101A-993E-0000C0EF6F5E}#1.0#0"; "THREED32.OCX"
Begin VB.Form FrmSeleccion4 
   BorderStyle     =   1  'Fixed Single
   ClientHeight    =   4740
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   6585
   ClipControls    =   0   'False
   Icon            =   "FrmSele4.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   4740
   ScaleWidth      =   6585
   StartUpPosition =   2  'CenterScreen
   Begin VB.ComboBox TxtDesde 
      Height          =   315
      ItemData        =   "FrmSele4.frx":058A
      Left            =   1200
      List            =   "FrmSele4.frx":05A0
      TabIndex        =   0
      Text            =   "Iniciales"
      Top             =   120
      Width           =   3015
   End
   Begin Threed.SSCommand SscMarcar 
      Height          =   495
      Left            =   5520
      TabIndex        =   1
      ToolTipText     =   "Marcar o Desmarcar todo"
      Top             =   0
      Width           =   615
      _Version        =   65536
      _ExtentX        =   1085
      _ExtentY        =   873
      _StockProps     =   78
      Caption         =   "TODO"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   6
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Picture         =   "FrmSele4.frx":05CC
   End
   Begin MSFlexGridLib.MSFlexGrid Grd_Selecciones 
      Height          =   3255
      Left            =   0
      TabIndex        =   2
      Top             =   600
      Width           =   6495
      _ExtentX        =   11456
      _ExtentY        =   5741
      _Version        =   393216
      Cols            =   5
      FixedCols       =   0
      ForeColorSel    =   -2147483635
      BackColorBkg    =   12632256
      FocusRect       =   2
      MergeCells      =   2
   End
   Begin Threed.SSCommand SscAceptar 
      Height          =   495
      Left            =   1560
      TabIndex        =   3
      Top             =   4080
      Width           =   1335
      _Version        =   65536
      _ExtentX        =   2355
      _ExtentY        =   873
      _StockProps     =   78
      Caption         =   "&ACEPTAR"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   6
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Picture         =   "FrmSele4.frx":08EE
   End
   Begin Threed.SSCommand SscCancelar 
      Height          =   495
      Left            =   3480
      TabIndex        =   4
      Top             =   4080
      Width           =   1335
      _Version        =   65536
      _ExtentX        =   2355
      _ExtentY        =   873
      _StockProps     =   78
      Caption         =   "&CANCELAR"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   6
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Picture         =   "FrmSele4.frx":0C40
   End
   Begin VB.Label LblRango 
      AutoSize        =   -1  'True
      Caption         =   "Desde : "
      Height          =   195
      Left            =   360
      TabIndex        =   5
      Top             =   120
      Width           =   600
   End
End
Attribute VB_Name = "FrmSeleccion4"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Dim todo As Boolean
Private vLblTabla As String
Private vLblCampos As String
Private vLblCondicion As String
Private vLblTitulo As String
'Private vFilaGrilla As Integer     'DEPURACION DE CODIGO
Private vColCodigo As String
Private vStrCodigo As String
Private vEsTodo As Boolean


Public Property Let Lbl_Tabla(Cue As String)
    vLblTabla = Cue
End Property

Public Property Let Lbl_Campos(Cue As String)
    vLblCampos = Cue
End Property

Public Property Let Lbl_Condicion(Cue As String)
    vLblCondicion = Cue
End Property

Public Property Let Lbl_Titulo(Cue As String)
    vLblTitulo = Cue
End Property
'Private Sub Grd_selecciones_DblClick()
Private Sub Grd_selecciones_Click() 'JAGS T8744
    'JAGS T7812
    If vLblTitulo = "Seleccion de anticipos" Then
    Grd_Selecciones.Col = 4
    Else
    Grd_Selecciones.Col = 3
    End If
    'JAGS T7812
    
    If Grd_Selecciones.TextMatrix(Grd_Selecciones.Row, 1) <> "" And Grd_Selecciones.Row <> 0 Then
        If Grd_Selecciones.TextMatrix(Grd_Selecciones.Row, 0) = "0" Then
            Grd_Selecciones.TextMatrix(Grd_Selecciones.Row, 0) = "1"
            Grd_Selecciones.CellPictureAlignment = 3
            Set Grd_Selecciones.CellPicture = MDI_Inventarios.ImgLstInventar.ListImages(1).Picture
        Else
            Grd_Selecciones.TextMatrix(Grd_Selecciones.Row, 0) = "0"
            Grd_Selecciones.CellPictureAlignment = 3
            Set Grd_Selecciones.CellPicture = LoadPicture()
        End If
    End If
End Sub

Private Sub Form_Load()
    'JAGS T7812
    If vLblTitulo = "Seleccion de anticipos" Then
    Call Titulos_Grid1
    Else
    Call Titulos_Grid
    End If
    'JAGS T7812
    limpia_matriz Mselec2, 1000, 6
End Sub

Private Sub Grd_Selecciones_EnterCell()
    If vEsTodo Then Exit Sub
    Dim Lumn As Byte, CnTr As Byte, UnBdg As ListItem, LaSlctd As String
    Dim ParcialEntradas As Single, ParcialSalidas As Single
    
    With Me.Grd_Selecciones
        If .Row < 1 Then Exit Sub
        If .TextMatrix(.Row, vColCodigo) = vStrCodigo Then Exit Sub
        vStrCodigo = .TextMatrix(.Row, vColCodigo)
''        Me.Text1.Text = vStrCodigo
'        vArticulo.IniciaXCodigo vStrCodigo
    End With
End Sub

Private Sub SscAceptar_Click()
    Dim aux As Integer
    Dim aux2 As Integer
    Dim i As Integer
    
    aux = 1
    aux2 = 1
    Grd_Selecciones.Row = 0
    ReDim ValAnti(Grd_Selecciones.Rows - 1, 1)
    Contador = 0
    
    While aux <= Grd_Selecciones.Rows - 1
        Grd_Selecciones.Row = Grd_Selecciones.Row + 1
        Grd_Selecciones.Col = 0
        If Grd_Selecciones.Text = "1" Then
            If Grd_Selecciones.TextMatrix(aux, 1) <> "" Then
            Contador = Contador + 1 'JAGS R7812
            End If
            If Grd_Selecciones.TextMatrix(aux, 1) <> "" Then
                Mselec2(aux2, 1) = Grd_Selecciones.TextMatrix(aux, 2)
                Mselec2(aux2, 2) = Grd_Selecciones.TextMatrix(aux, 1)
                'JAGS R7812
                If vLblTitulo = "Seleccion de anticipos" Then
                ValAnti(aux2, 0) = Grd_Selecciones.TextMatrix(aux, 3)
                ValAnti(aux2, 1) = Grd_Selecciones.TextMatrix(aux, 1)
                End If
                'JAGS R7812
                aux2 = aux2 + 1
            End If
        End If
        aux = aux + 1
    Wend
    Unload Me
    CanAnti = False
End Sub

Private Sub SscCancelar_Click()
    Unload Me
    CanAnti = True
End Sub

Private Sub SscMarcar_Click()
    Dim aux As Long
    vEsTodo = True
    aux = 1
    While aux <= Grd_Selecciones.Rows - 1
        'Grd_Selecciones.Col = 3
        Grd_Selecciones.Col = 4 'JAGS T8740
        Grd_Selecciones.Row = aux
        If Grd_Selecciones.TextMatrix(aux, 1) <> "" Then
            If todo = False Then
                Grd_Selecciones.TextMatrix(aux, 0) = "1"
                Grd_Selecciones.CellPictureAlignment = 3
                Set Grd_Selecciones.CellPicture = MDI_Inventarios.ImgLstInventar.ListImages(1).Picture
            Else
                Grd_Selecciones.TextMatrix(aux, 0) = "0"
                Grd_Selecciones.CellPictureAlignment = 3
                Set Grd_Selecciones.CellPicture = LoadPicture()
            End If
        End If
        aux = aux + 1
    Wend
    If todo = False Then
        todo = True
    Else
        todo = False
    End If
    vEsTodo = False
End Sub

Private Sub TxtDesde_KeyPress(KeyAscii As Integer)
  Call Cambiar_Enter(KeyAscii)
End Sub

Private Sub TxtDesde_LostFocus()
    Dim aux As Integer
    Dim Pos As Long
    Grd_Selecciones.Clear
    Grd_Selecciones.Rows = 2
    'JAGS R7812
    If vLblTitulo = "Seleccion de anticipos" Then
    Call Titulos_Grid1
    Else
    'JAGS R7812
    Call Titulos_Grid
    End If 'JAGS R7812
    DoEvents
    ''''''
    Pos = InStr(vLblCampos, ",") + 1
    Codigo = Mid(vLblCampos, Pos, Len(vLblCampos))
    Codigo = Mid(Codigo, 1, InStr(Codigo, ",") - 1)
    ''''''
    If Trim(TxtDesde) = "*" Or Trim(TxtDesde) = "%" Then TxtDesde = CaractLike
    Condicion = Codigo & " Like '" & TxtDesde & CaractLike & Comi & IIf(vLblCondicion = NUL$, NUL$, " AND " & vLblCondicion) & " ORDER BY " & Codigo
    
    If TxtDesde.Text <> "" Then
        Result = LoadfGrid(Grd_Selecciones, vLblTabla, vLblCampos, Condicion)
        For aux = 1 To Grd_Selecciones.Rows - 1 Step 1
            'Grd_Selecciones.TextMatrix(aux, 3) = Format(Grd_Selecciones.TextMatrix(aux, 3), "############0.##00")
            Grd_Selecciones.TextMatrix(aux, 3) = Format(Grd_Selecciones.TextMatrix(aux, 3), "#,###,###,###,##0.#0") 'JAGS T8694
        Next
    End If
salto:
    aux = 1
    Grd_Selecciones.Row = 0
    While aux <= Grd_Selecciones.Rows - 1
        Grd_Selecciones.Row = Grd_Selecciones.Row + 1
        Grd_Selecciones.Col = 0
        Grd_Selecciones.Text = "0"
        aux = aux + 1
    Wend
End Sub

Private Sub Form_Activate()
    If todo = False Then
        TxtDesde.SetFocus
    Else
        TxtDesde.Text = CaractLike
        TxtDesde_LostFocus
    End If
End Sub

Private Sub TxtDesde_GotFocus()
    TxtDesde.SelStart = 0
    TxtDesde.SelLength = Len(TxtDesde.Text)
End Sub

Private Sub Titulos_Grid()
    Grd_Selecciones.ColWidth(0) = 0
    Grd_Selecciones.ColWidth(1) = 3800
    Grd_Selecciones.ColWidth(2) = 1800
'    Grd_Selecciones.ColWidth(3) = 1400
'    Grd_Selecciones.ColWidth(4) = 1600
'    Grd_Selecciones.ColWidth(5) = 1600
'    Grd_Selecciones.ColWidth(6) = 0
    Grd_Selecciones.ColWidth(3) = 600
    Grd_Selecciones.ColWidth(4) = 0
    Grd_Selecciones.Row = 0
    vColCodigo = 2
    
    Grd_Selecciones.Col = 2
    Grd_Selecciones.CellAlignment = 4
    Grd_Selecciones.Text = "NIT"
    Grd_Selecciones.Col = 1
    Grd_Selecciones.CellAlignment = 4
    Grd_Selecciones.Text = "Nombre"
'    Grd_Selecciones.Col = 3
'    Grd_Selecciones.CellAlignment = 4
'    Grd_Selecciones.Text = "Costo Promedio"
'    Grd_Selecciones.Col = 4
'    Grd_Selecciones.CellAlignment = 4
'    Grd_Selecciones.Text = "Unidad de Medida"
'    Grd_Selecciones.Col = 5
'    Grd_Selecciones.CellAlignment = 4
'    Grd_Selecciones.Text = "Concentraci�n"
    Grd_Selecciones.Col = 3
    Grd_Selecciones.CellAlignment = 4
    Grd_Selecciones.Text = "Marcar"
    Grd_Selecciones.Row = 1
    todo = False
End Sub
'JAGS R7812 INICIO
Private Sub Titulos_Grid1()
 Grd_Selecciones.ColWidth(0) = 0
 Grd_Selecciones.ColWidth(1) = 1700
 Grd_Selecciones.ColWidth(2) = 1700
 Grd_Selecciones.ColWidth(3) = 1700
 Grd_Selecciones.ColWidth(4) = 700
 vColCodigo = 3
 Grd_Selecciones.Row = 0
 Grd_Selecciones.Col = 1
 Grd_Selecciones.CellAlignment = 4
 Grd_Selecciones.Text = "N�mero"
 Grd_Selecciones.Col = 2
 Grd_Selecciones.CellAlignment = 4
 Grd_Selecciones.Text = "Fecha"
 Grd_Selecciones.Col = 3
 Grd_Selecciones.CellAlignment = 4
 Grd_Selecciones.Text = "Valor"
 Grd_Selecciones.Col = 4
 Grd_Selecciones.CellAlignment = 4
 Grd_Selecciones.Text = "Marcar"
 Grd_Selecciones.Row = 1
 todo = False
End Sub 'JAGS R7812 FIN
'JAGS T8744
Private Sub Grd_Selecciones_KeyPress(KeyAscii As Integer)
   
   If KeyAscii = 32 Then 'Barra espaciadora
      Call Grd_selecciones_Click
   End If
   
End Sub 'JAGS T8744
'JAGS T8743
Private Sub Form_QueryUnload(Cancel As Integer, unloadmode As Integer)
    CanAnti = True
End Sub
