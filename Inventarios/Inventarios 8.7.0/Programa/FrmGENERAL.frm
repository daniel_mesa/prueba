VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.1#0"; "Mscomctl.ocx"
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "MSFLXGRD.OCX"
Object = "{0BA686C6-F7D3-101A-993E-0000C0EF6F5E}#1.0#0"; "THREED32.OCX"
Object = "{6B7E6392-850A-101B-AFC0-4210102A8DA7}#1.3#0"; "COMCTL32.OCX"
Object = "{65E121D4-0C60-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCHRT20.OCX"
Begin VB.Form FrmGENERAL 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "FrmGENERAL"
   ClientHeight    =   8460
   ClientLeft      =   45
   ClientTop       =   510
   ClientWidth     =   10605
   Icon            =   "FrmGENERAL.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   ScaleHeight     =   8460
   ScaleWidth      =   10605
   Begin VB.PictureBox PbCordigoQr 
      Height          =   3855
      Left            =   11040
      ScaleHeight     =   3795
      ScaleWidth      =   3795
      TabIndex        =   260
      Top             =   5520
      Visible         =   0   'False
      Width           =   3855
   End
   Begin VB.PictureBox PbLogo 
      Height          =   2535
      Left            =   11040
      ScaleHeight     =   2475
      ScaleWidth      =   2595
      TabIndex        =   259
      Top             =   2760
      Width           =   2655
   End
   Begin VB.Frame frmINVENTARIOS 
      Height          =   8415
      Left            =   0
      TabIndex        =   117
      Top             =   0
      Width           =   10575
      Begin VB.Frame FrmFactElect 
         BackColor       =   &H8000000A&
         Enabled         =   0   'False
         Height          =   1095
         Left            =   120
         TabIndex        =   261
         Top             =   6720
         Visible         =   0   'False
         Width           =   5895
         Begin Threed.SSCommand ScmdFacEL 
            Height          =   735
            Index           =   0
            Left            =   120
            TabIndex        =   151
            Top             =   240
            Width           =   1215
            _Version        =   65536
            _ExtentX        =   2143
            _ExtentY        =   1296
            _StockProps     =   78
            Caption         =   "&Generar XML"
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   6.75
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Picture         =   "FrmGENERAL.frx":058A
         End
         Begin Threed.SSCommand ScmdFacEL 
            Height          =   735
            Index           =   1
            Left            =   1440
            TabIndex        =   152
            Top             =   240
            Width           =   1335
            _Version        =   65536
            _ExtentX        =   2355
            _ExtentY        =   1296
            _StockProps     =   78
            Caption         =   "&Fact. Elect."
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   6.75
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Picture         =   "FrmGENERAL.frx":0978
         End
         Begin Threed.SSCommand ScmdFacEL 
            Height          =   735
            Index           =   2
            Left            =   2880
            TabIndex        =   153
            Top             =   240
            Width           =   1335
            _Version        =   65536
            _ExtentX        =   2355
            _ExtentY        =   1296
            _StockProps     =   78
            Caption         =   "&Enviar F.E."
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   6.75
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Picture         =   "FrmGENERAL.frx":1D7A
         End
         Begin Threed.SSCommand ScmdFacEL 
            Height          =   735
            Index           =   3
            Left            =   4320
            TabIndex        =   262
            Top             =   240
            Width           =   1455
            _Version        =   65536
            _ExtentX        =   2566
            _ExtentY        =   1296
            _StockProps     =   78
            Caption         =   "&Consultar Estado"
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   6.75
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Picture         =   "FrmGENERAL.frx":29CE
         End
      End
      Begin VB.Frame fraTOTALES 
         Enabled         =   0   'False
         Height          =   1215
         Left            =   6480
         TabIndex        =   166
         Top             =   6700
         Width           =   3975
         Begin VB.TextBox txtTOTAL 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00D6FEFE&
            Height          =   285
            Left            =   1560
            TabIndex        =   169
            Top             =   840
            Width           =   1935
         End
         Begin VB.TextBox txtIMPUE 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00D6FEFE&
            Height          =   285
            Left            =   1560
            TabIndex        =   168
            Top             =   510
            Width           =   1935
         End
         Begin VB.TextBox txtSUBTO 
            Alignment       =   1  'Right Justify
            BackColor       =   &H00D6FEFE&
            Height          =   285
            Left            =   1560
            TabIndex        =   167
            Top             =   190
            Width           =   1935
         End
         Begin VB.Label Label3 
            Alignment       =   1  'Right Justify
            Caption         =   "Total:"
            Height          =   255
            Left            =   120
            TabIndex        =   172
            Top             =   900
            Width           =   1215
         End
         Begin VB.Label Label2 
            Alignment       =   1  'Right Justify
            Caption         =   "Impuesto:"
            Height          =   255
            Index           =   1
            Left            =   120
            TabIndex        =   171
            Top             =   585
            Width           =   1215
         End
         Begin VB.Label Label1 
            Alignment       =   1  'Right Justify
            Caption         =   "Sub Total:"
            Height          =   255
            Index           =   1
            Left            =   120
            TabIndex        =   170
            Top             =   240
            Width           =   1215
         End
      End
      Begin VB.Frame FraPie 
         BackColor       =   &H8000000A&
         Caption         =   "Observaciones"
         Height          =   1335
         Left            =   120
         TabIndex        =   165
         Top             =   5280
         Width           =   6135
         Begin VB.TextBox txtDocum 
            Height          =   975
            Index           =   9
            Left            =   120
            MaxLength       =   200
            MultiLine       =   -1  'True
            ScrollBars      =   2  'Vertical
            TabIndex        =   181
            Top             =   240
            Visible         =   0   'False
            Width           =   5860
         End
         Begin VB.TextBox txtDocum 
            Height          =   975
            Index           =   8
            Left            =   120
            MaxLength       =   200
            MultiLine       =   -1  'True
            ScrollBars      =   2  'Vertical
            TabIndex        =   180
            Top             =   240
            Width           =   5860
         End
      End
      Begin VB.Frame FraArti 
         BackColor       =   &H8000000A&
         Caption         =   "Art�culos"
         Enabled         =   0   'False
         Height          =   3495
         Left            =   120
         TabIndex        =   155
         Top             =   1680
         Width           =   8775
         Begin VB.Frame fraBRR 
            BorderStyle     =   0  'None
            Height          =   600
            Left            =   7920
            TabIndex        =   157
            Top             =   1200
            Width           =   825
            Begin VB.OptionButton opcBOSEL 
               Caption         =   "Sel"
               Height          =   240
               Left            =   100
               TabIndex        =   160
               Top             =   120
               Value           =   -1  'True
               Width           =   650
            End
            Begin VB.OptionButton opcBONO 
               Caption         =   "No"
               Height          =   240
               Left            =   100
               TabIndex        =   159
               Top             =   330
               Width           =   650
            End
         End
         Begin MSFlexGridLib.MSFlexGrid GrdArticulos 
            Height          =   3135
            Left            =   120
            TabIndex        =   161
            Top             =   240
            Width           =   7815
            _ExtentX        =   13785
            _ExtentY        =   5530
            _Version        =   393216
            Cols            =   5
            FixedCols       =   0
            ForeColorSel    =   -2147483635
            FocusRect       =   2
            MergeCells      =   1
         End
         Begin Threed.SSCommand cmdIconos 
            Height          =   465
            HelpContextID   =   40
            Index           =   1
            Left            =   8160
            TabIndex        =   162
            ToolTipText     =   "Agregar Art�culos"
            Top             =   360
            Width           =   495
            _Version        =   65536
            _ExtentX        =   873
            _ExtentY        =   820
            _StockProps     =   78
            ForeColor       =   16777215
            Picture         =   "FrmGENERAL.frx":3620
         End
         Begin Threed.SSCommand cmdIconos 
            Height          =   465
            HelpContextID   =   40
            Index           =   2
            Left            =   8160
            TabIndex        =   163
            ToolTipText     =   "Quitar Art�culos"
            Top             =   1920
            Width           =   495
            _Version        =   65536
            _ExtentX        =   873
            _ExtentY        =   820
            _StockProps     =   78
            ForeColor       =   16777215
            Picture         =   "FrmGENERAL.frx":3CEA
         End
         Begin Threed.SSCommand cmdIconos 
            Height          =   465
            HelpContextID   =   40
            Index           =   3
            Left            =   8160
            TabIndex        =   164
            ToolTipText     =   "Exportar, importar Articulos"
            Top             =   2760
            Width           =   495
            _Version        =   65536
            _ExtentX        =   873
            _ExtentY        =   820
            _StockProps     =   78
            ForeColor       =   16777215
            AutoSize        =   1
            Picture         =   "FrmGENERAL.frx":403C
         End
      End
      Begin VB.Frame FraOpciones 
         BackColor       =   &H8000000A&
         Height          =   1500
         Left            =   6480
         TabIndex        =   144
         Top             =   5280
         Width           =   3975
         Begin Threed.SSCommand cmdOpciones 
            Height          =   720
            Index           =   2
            Left            =   2040
            TabIndex        =   146
            Top             =   690
            Width           =   855
            _Version        =   65536
            _ExtentX        =   1508
            _ExtentY        =   1270
            _StockProps     =   78
            Caption         =   "&IMPRIMIR"
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   6
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            RoundedCorners  =   0   'False
            Picture         =   "FrmGENERAL.frx":49BE
         End
         Begin Threed.SSCommand cmdOpciones 
            Height          =   720
            Index           =   1
            Left            =   1080
            TabIndex        =   148
            Top             =   690
            Width           =   855
            _Version        =   65536
            _ExtentX        =   1508
            _ExtentY        =   1270
            _StockProps     =   78
            Caption         =   "A&NULAR"
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   6
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Picture         =   "FrmGENERAL.frx":5088
         End
         Begin Threed.SSCommand cmdOpciones 
            Height          =   720
            Index           =   0
            Left            =   120
            TabIndex        =   149
            Top             =   690
            Width           =   855
            _Version        =   65536
            _ExtentX        =   1508
            _ExtentY        =   1270
            _StockProps     =   78
            Caption         =   "&GUARDAR"
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   6
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Picture         =   "FrmGENERAL.frx":5752
         End
         Begin Threed.SSCommand cmdOpciones 
            Height          =   720
            Index           =   3
            Left            =   3000
            TabIndex        =   150
            ToolTipText     =   "Limpia Pantalla"
            Top             =   690
            Width           =   855
            _Version        =   65536
            _ExtentX        =   1508
            _ExtentY        =   1270
            _StockProps     =   78
            Caption         =   "&LIMPIAR"
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   6
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            AutoSize        =   1
            Picture         =   "FrmGENERAL.frx":5E1C
         End
         Begin Threed.SSCommand CmdCuentas 
            Height          =   495
            Left            =   2520
            TabIndex        =   182
            ToolTipText     =   "Registrar Movimiento Contable"
            Top             =   170
            Width           =   1335
            _Version        =   65536
            _ExtentX        =   2355
            _ExtentY        =   873
            _StockProps     =   78
            Caption         =   "&CONTABILIDAD"
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   6
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Picture         =   "FrmGENERAL.frx":64E6
         End
         Begin Threed.SSCommand CmdPpto 
            Height          =   495
            Left            =   960
            TabIndex        =   227
            ToolTipText     =   "Registrar Movimiento Presupuestal"
            Top             =   170
            Width           =   1335
            _Version        =   65536
            _ExtentX        =   2355
            _ExtentY        =   873
            _StockProps     =   78
            Caption         =   "&PRESUPUESTO"
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   6
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Picture         =   "FrmGENERAL.frx":6838
         End
      End
      Begin VB.Frame FraGeneral 
         Height          =   1575
         Left            =   120
         TabIndex        =   122
         Top             =   120
         Width           =   10335
         Begin VB.TextBox TxtFactElt 
            BackColor       =   &H00D6FEFE&
            CausesValidation=   0   'False
            Enabled         =   0   'False
            ForeColor       =   &H00000000&
            Height          =   285
            Left            =   6550
            TabIndex        =   258
            Top             =   480
            Width           =   1215
         End
         Begin VB.ComboBox cboLisPrecio 
            BackColor       =   &H00D6FEFE&
            CausesValidation=   0   'False
            ForeColor       =   &H00000000&
            Height          =   315
            Left            =   4605
            Style           =   2  'Dropdown List
            TabIndex        =   142
            Top             =   960
            Visible         =   0   'False
            Width           =   2900
         End
         Begin VB.TextBox txtDocum 
            BackColor       =   &H00D6FEFE&
            CausesValidation=   0   'False
            ForeColor       =   &H00000000&
            Height          =   285
            Index           =   21
            Left            =   9000
            MaxLength       =   2
            TabIndex        =   154
            Top             =   480
            Width           =   375
         End
         Begin VB.TextBox txtDocum 
            BackColor       =   &H00D6FEFE&
            ForeColor       =   &H00000000&
            Height          =   285
            Index           =   22
            Left            =   9360
            MaxLength       =   2
            TabIndex        =   156
            Top             =   480
            Width           =   375
         End
         Begin VB.TextBox txtDocum 
            BackColor       =   &H00D6FEFE&
            ForeColor       =   &H00000000&
            Height          =   285
            Index           =   23
            Left            =   9720
            MaxLength       =   4
            TabIndex        =   158
            Top             =   480
            Width           =   495
         End
         Begin VB.TextBox txtDocum 
            BackColor       =   &H00D6FEFE&
            Enabled         =   0   'False
            ForeColor       =   &H00000000&
            Height          =   285
            Index           =   6
            Left            =   2640
            MaxLength       =   40
            TabIndex        =   138
            Top             =   800
            Width           =   4755
         End
         Begin VB.ComboBox cboIndependiente 
            BackColor       =   &H00D6FEFE&
            CausesValidation=   0   'False
            ForeColor       =   &H00000000&
            Height          =   315
            ItemData        =   "FrmGENERAL.frx":6B8A
            Left            =   8640
            List            =   "FrmGENERAL.frx":6B8C
            Style           =   2  'Dropdown List
            TabIndex        =   143
            Top             =   1150
            Width           =   975
         End
         Begin VB.TextBox txtDocum 
            BackColor       =   &H00D6FEFE&
            CausesValidation=   0   'False
            ForeColor       =   &H00000000&
            Height          =   285
            Index           =   5
            Left            =   1080
            MaxLength       =   11
            TabIndex        =   135
            Top             =   800
            Width           =   1095
         End
         Begin VB.ComboBox cboBodDestino 
            BackColor       =   &H00D6FEFE&
            CausesValidation=   0   'False
            ForeColor       =   &H00000000&
            Height          =   315
            Left            =   4600
            TabIndex        =   147
            Top             =   1150
            Width           =   2900
         End
         Begin VB.ComboBox cboBodega 
            BackColor       =   &H00D6FEFE&
            ForeColor       =   &H00000000&
            Height          =   315
            Left            =   1050
            TabIndex        =   141
            Top             =   1150
            Width           =   2900
         End
         Begin VB.ComboBox cboConsecutivo 
            BackColor       =   &H00D6FEFE&
            CausesValidation=   0   'False
            ForeColor       =   &H00000000&
            Height          =   315
            Left            =   1080
            Style           =   2  'Dropdown List
            TabIndex        =   126
            Top             =   360
            Width           =   3135
         End
         Begin VB.TextBox txtNumero 
            BackColor       =   &H00D6FEFE&
            CausesValidation=   0   'False
            ForeColor       =   &H00000000&
            Height          =   285
            Left            =   6550
            MaxLength       =   8
            TabIndex        =   127
            Top             =   150
            Width           =   855
         End
         Begin VB.TextBox txtDocum 
            BackColor       =   &H00D6FEFE&
            Enabled         =   0   'False
            ForeColor       =   &H00000000&
            Height          =   285
            Index           =   3
            Left            =   9720
            MaxLength       =   4
            TabIndex        =   132
            Top             =   180
            Width           =   495
         End
         Begin VB.TextBox txtDocum 
            BackColor       =   &H00D6FEFE&
            Enabled         =   0   'False
            ForeColor       =   &H00000000&
            Height          =   285
            Index           =   2
            Left            =   9360
            MaxLength       =   2
            TabIndex        =   129
            Top             =   180
            Width           =   375
         End
         Begin VB.TextBox txtDocum 
            BackColor       =   &H00D6FEFE&
            CausesValidation=   0   'False
            Enabled         =   0   'False
            ForeColor       =   &H00000000&
            Height          =   285
            Index           =   1
            Left            =   9000
            MaxLength       =   2
            TabIndex        =   128
            Top             =   180
            Width           =   375
         End
         Begin VB.Frame frmCUAL 
            Height          =   600
            Left            =   4300
            TabIndex        =   123
            Top             =   120
            Width           =   1440
            Begin VB.OptionButton opcACT 
               Caption         =   "Actual"
               Height          =   240
               Left            =   120
               TabIndex        =   125
               Top             =   330
               Width           =   1000
            End
            Begin VB.OptionButton opcORG 
               Caption         =   "Original"
               Height          =   240
               Left            =   120
               TabIndex        =   124
               Top             =   120
               Value           =   -1  'True
               Width           =   1000
            End
         End
         Begin Threed.SSCommand cmdIrAlCuerpo 
            Height          =   600
            Left            =   9720
            TabIndex        =   145
            ToolTipText     =   "Ir al cuerpo del documento"
            Top             =   960
            Width           =   600
            _Version        =   65536
            _ExtentX        =   1058
            _ExtentY        =   1058
            _StockProps     =   78
            ForeColor       =   16777215
            AutoSize        =   1
            Picture         =   "FrmGENERAL.frx":6B8E
         End
         Begin Threed.SSCommand CmdSelecTercero 
            Height          =   375
            Left            =   2040
            TabIndex        =   130
            ToolTipText     =   "Selecci�n de Terceros"
            Top             =   750
            Width           =   615
            _Version        =   65536
            _ExtentX        =   1085
            _ExtentY        =   661
            _StockProps     =   78
            BevelWidth      =   0
            RoundedCorners  =   0   'False
            Outline         =   0   'False
            Picture         =   "FrmGENERAL.frx":798C
         End
         Begin VB.Label LblFactElt 
            Caption         =   "Factura:"
            Height          =   255
            Left            =   5880
            TabIndex        =   257
            Top             =   480
            Width           =   615
         End
         Begin VB.Image imgWait 
            Height          =   480
            Left            =   9780
            Picture         =   "FrmGENERAL.frx":833E
            Top             =   1080
            Width           =   480
         End
         Begin VB.Image imgNoCont 
            Height          =   240
            Left            =   120
            Picture         =   "FrmGENERAL.frx":8C08
            ToolTipText     =   "No se afectar� contabilidad."
            Top             =   1150
            Visible         =   0   'False
            Width           =   240
         End
         Begin VB.Label lblLisPrecio 
            Alignment       =   1  'Right Justify
            Caption         =   "Lista de Precio:"
            Height          =   435
            Left            =   3840
            TabIndex        =   186
            Top             =   1080
            Visible         =   0   'False
            Width           =   750
         End
         Begin VB.Label LblFecha 
            Alignment       =   1  'Right Justify
            Caption         =   "Vigente Hasta:"
            Height          =   255
            Index           =   3
            Left            =   7680
            TabIndex        =   179
            Top             =   480
            Width           =   1215
         End
         Begin VB.Label lblINDEPE 
            Alignment       =   1  'Right Justify
            Caption         =   "Independiente:"
            Height          =   255
            Left            =   7485
            TabIndex        =   140
            Top             =   1150
            Width           =   1125
         End
         Begin VB.Label LblProveedor 
            Alignment       =   1  'Right Justify
            Caption         =   "Tercero:"
            Height          =   255
            Left            =   60
            TabIndex        =   139
            Top             =   800
            Width           =   1005
         End
         Begin VB.Label lblDESTINO 
            Alignment       =   1  'Right Justify
            Caption         =   "Destino:"
            Height          =   255
            Left            =   3950
            TabIndex        =   137
            Top             =   1150
            Width           =   630
         End
         Begin VB.Label lblBODEGA 
            Alignment       =   1  'Right Justify
            Caption         =   "Bodega:"
            Height          =   255
            Left            =   60
            TabIndex        =   136
            Top             =   1150
            Width           =   1005
         End
         Begin VB.Label Label8 
            Alignment       =   1  'Right Justify
            Caption         =   "Comprobante:"
            Height          =   195
            Left            =   60
            TabIndex        =   134
            Top             =   360
            Width           =   1005
         End
         Begin VB.Label LblNumero 
            Alignment       =   1  'Right Justify
            Caption         =   "N�mero:"
            Height          =   255
            Left            =   5805
            TabIndex        =   133
            Top             =   150
            Width           =   630
         End
         Begin VB.Label LblFecha 
            Alignment       =   1  'Right Justify
            Caption         =   "Fecha C/bante:"
            Height          =   255
            Index           =   0
            Left            =   7680
            TabIndex        =   131
            Top             =   240
            Width           =   1215
         End
      End
      Begin VB.Frame FraRequi 
         BackColor       =   &H8000000A&
         Caption         =   "Documentos"
         Height          =   3495
         Left            =   9000
         TabIndex        =   118
         Top             =   1680
         Visible         =   0   'False
         Width           =   1455
         Begin VB.CheckBox ChkRequ 
            BackColor       =   &H8000000A&
            Caption         =   "Requisici�n"
            Height          =   255
            Left            =   120
            TabIndex        =   256
            Top             =   2520
            Visible         =   0   'False
            Width           =   1215
         End
         Begin VB.ListBox lstDocsPadre 
            Height          =   2205
            Left            =   120
            MultiSelect     =   2  'Extended
            TabIndex        =   119
            Top             =   240
            Width           =   1095
         End
         Begin Threed.SSCommand cmdDocsPadre 
            Height          =   465
            HelpContextID   =   40
            Index           =   0
            Left            =   120
            TabIndex        =   120
            ToolTipText     =   "Agregar"
            Top             =   2880
            Width           =   495
            _Version        =   65536
            _ExtentX        =   873
            _ExtentY        =   820
            _StockProps     =   78
            ForeColor       =   -2147483640
            Picture         =   "FrmGENERAL.frx":8F4A
         End
         Begin Threed.SSCommand cmdDocsPadre 
            Height          =   465
            HelpContextID   =   40
            Index           =   1
            Left            =   720
            TabIndex        =   121
            ToolTipText     =   "Quitar"
            Top             =   2880
            Width           =   495
            _Version        =   65536
            _ExtentX        =   873
            _ExtentY        =   820
            _StockProps     =   78
            ForeColor       =   -2147483640
            Picture         =   "FrmGENERAL.frx":9614
         End
      End
      Begin MSComctlLib.ListView lstSALXBOD 
         Height          =   2805
         Left            =   120
         TabIndex        =   173
         Top             =   5175
         Visible         =   0   'False
         Width           =   6135
         _ExtentX        =   10821
         _ExtentY        =   4948
         View            =   3
         LabelEdit       =   1
         LabelWrap       =   -1  'True
         HideSelection   =   0   'False
         Checkboxes      =   -1  'True
         FullRowSelect   =   -1  'True
         GridLines       =   -1  'True
         _Version        =   393217
         ForeColor       =   -2147483640
         BackColor       =   -2147483643
         BorderStyle     =   1
         Appearance      =   1
         NumItems        =   0
      End
      Begin MSComctlLib.ProgressBar proARTICULOS 
         Height          =   300
         Left            =   6480
         TabIndex        =   174
         Top             =   7200
         Width           =   3975
         _ExtentX        =   7011
         _ExtentY        =   529
         _Version        =   393216
         Appearance      =   1
      End
      Begin MSChart20Lib.MSChart chaSALDOS 
         Height          =   2805
         Left            =   120
         OleObjectBlob   =   "FrmGENERAL.frx":9966
         TabIndex        =   175
         Top             =   5175
         Width           =   6135
      End
      Begin ComctlLib.StatusBar staINFORMA 
         Height          =   285
         Left            =   240
         TabIndex        =   176
         Top             =   8040
         Width           =   9975
         _ExtentX        =   17595
         _ExtentY        =   503
         SimpleText      =   ""
         _Version        =   327682
         BeginProperty Panels {0713E89E-850A-101B-AFC0-4210102A8DA7} 
            NumPanels       =   9
            BeginProperty Panel1 {0713E89F-850A-101B-AFC0-4210102A8DA7} 
               AutoSize        =   2
               Object.Width           =   1402
               MinWidth        =   1411
               Text            =   "Nuevo"
               TextSave        =   "Nuevo"
               Key             =   "NUEVO"
               Object.Tag             =   ""
            EndProperty
            BeginProperty Panel2 {0713E89F-850A-101B-AFC0-4210102A8DA7} 
               Alignment       =   2
               Bevel           =   0
               Object.Width           =   1764
               MinWidth        =   1764
               Text            =   "Costo Total:"
               TextSave        =   "Costo Total:"
               Key             =   "lblCOS"
               Object.Tag             =   ""
            EndProperty
            BeginProperty Panel3 {0713E89F-850A-101B-AFC0-4210102A8DA7} 
               TextSave        =   ""
               Key             =   "COSTO"
               Object.Tag             =   ""
            EndProperty
            BeginProperty Panel4 {0713E89F-850A-101B-AFC0-4210102A8DA7} 
               Alignment       =   2
               Bevel           =   0
               Object.Width           =   2117
               MinWidth        =   2117
               Text            =   "Costo Rengl�n:"
               TextSave        =   "Costo Rengl�n:"
               Key             =   "lblPAR"
               Object.Tag             =   ""
            EndProperty
            BeginProperty Panel5 {0713E89F-850A-101B-AFC0-4210102A8DA7} 
               TextSave        =   ""
               Key             =   "PARCIAL"
               Object.Tag             =   ""
            EndProperty
            BeginProperty Panel6 {0713E89F-850A-101B-AFC0-4210102A8DA7} 
               Alignment       =   2
               Bevel           =   0
               Object.Width           =   529
               MinWidth        =   529
               Text            =   "#"
               TextSave        =   "#"
               Object.Tag             =   ""
            EndProperty
            BeginProperty Panel7 {0713E89F-850A-101B-AFC0-4210102A8DA7} 
               Object.Width           =   1235
               MinWidth        =   1235
               TextSave        =   ""
               Key             =   "RNGLNS"
               Object.Tag             =   ""
            EndProperty
            BeginProperty Panel8 {0713E89F-850A-101B-AFC0-4210102A8DA7} 
               Alignment       =   2
               Bevel           =   2
               Text            =   "Saldos X Bodega"
               TextSave        =   "Saldos X Bodega"
               Key             =   "SALBOD"
               Object.Tag             =   ""
            EndProperty
            BeginProperty Panel9 {0713E89F-850A-101B-AFC0-4210102A8DA7} 
               TextSave        =   ""
               Key             =   "SLDSEL"
               Object.Tag             =   ""
            EndProperty
         EndProperty
      End
   End
   Begin MSFlexGridLib.MSFlexGrid GrdArti 
      Height          =   2055
      Left            =   1200
      TabIndex        =   1
      Top             =   3960
      Visible         =   0   'False
      Width           =   5775
      _ExtentX        =   10186
      _ExtentY        =   3625
      _Version        =   393216
      Cols            =   10
      FixedCols       =   0
      ForeColorSel    =   -2147483635
      FocusRect       =   2
      MergeCells      =   1
   End
   Begin VB.Frame FrmDEVOLFACTURA 
      Caption         =   "DevolFactura"
      Height          =   2775
      Left            =   4440
      TabIndex        =   94
      Top             =   1560
      Visible         =   0   'False
      Width           =   5925
      Begin VB.CommandButton cmdREGRESAR 
         Caption         =   "Regresar"
         Height          =   375
         Index           =   12
         Left            =   1680
         TabIndex        =   112
         Top             =   2280
         Width           =   975
      End
      Begin VB.CommandButton cmdPROCEDER 
         Caption         =   "Continuar"
         Height          =   375
         Index           =   12
         Left            =   3360
         TabIndex        =   111
         Top             =   2280
         Width           =   975
      End
      Begin VB.TextBox TxtDevo 
         BackColor       =   &H80000004&
         Enabled         =   0   'False
         Height          =   285
         Index           =   9
         Left            =   4440
         MaxLength       =   5
         TabIndex        =   97
         Top             =   1320
         Width           =   615
      End
      Begin VB.TextBox TxtDevo 
         BackColor       =   &H80000004&
         Enabled         =   0   'False
         Height          =   285
         Index           =   10
         Left            =   4440
         MaxLength       =   5
         TabIndex        =   99
         Top             =   1800
         Width           =   615
      End
      Begin VB.TextBox TxtDevo 
         Enabled         =   0   'False
         Height          =   285
         Index           =   15
         Left            =   3720
         MaxLength       =   16
         TabIndex        =   93
         Top             =   840
         Width           =   1575
      End
      Begin VB.TextBox TxtDevo 
         Enabled         =   0   'False
         Height          =   285
         Index           =   16
         Left            =   960
         MaxLength       =   16
         TabIndex        =   95
         Top             =   1200
         Width           =   1575
      End
      Begin VB.TextBox TxtDevo 
         Enabled         =   0   'False
         Height          =   285
         Index           =   12
         Left            =   960
         MaxLength       =   16
         TabIndex        =   87
         Top             =   480
         Width           =   1575
      End
      Begin VB.TextBox TxtDevo 
         Enabled         =   0   'False
         Height          =   285
         Index           =   13
         Left            =   3720
         MaxLength       =   16
         TabIndex        =   89
         Top             =   480
         Width           =   1575
      End
      Begin VB.TextBox TxtDevo 
         Enabled         =   0   'False
         Height          =   285
         Index           =   14
         Left            =   960
         MaxLength       =   16
         TabIndex        =   91
         Top             =   840
         Width           =   1575
      End
      Begin VB.Label Label25 
         Caption         =   "% Descuento :"
         Height          =   255
         Left            =   3360
         TabIndex        =   96
         Top             =   1320
         Width           =   1095
      End
      Begin VB.Label Label24 
         Caption         =   "% Ret Fuente :"
         Height          =   255
         Left            =   3360
         TabIndex        =   98
         Top             =   1800
         Width           =   1095
      End
      Begin VB.Label LblRete 
         AutoSize        =   -1  'True
         Caption         =   "Ret Fuente :"
         Height          =   195
         Index           =   3
         Left            =   2640
         TabIndex        =   92
         Top             =   840
         Width           =   885
      End
      Begin VB.Label Label14 
         Caption         =   "Bruto :"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   240
         TabIndex        =   86
         Top             =   480
         Width           =   495
      End
      Begin VB.Label Label13 
         Caption         =   "Neto :"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   240
         TabIndex        =   15
         Top             =   1200
         Width           =   495
      End
      Begin VB.Label Label12 
         Caption         =   "Descuentos :"
         Height          =   255
         Left            =   2640
         TabIndex        =   88
         Top             =   480
         Width           =   975
      End
      Begin VB.Label LblImpu 
         Caption         =   "Iva :"
         Height          =   255
         Index           =   3
         Left            =   240
         TabIndex        =   90
         Top             =   840
         Width           =   375
      End
   End
   Begin VB.Frame FrmFACTURA 
      Caption         =   "Factura"
      Height          =   3240
      Left            =   4440
      TabIndex        =   2
      Top             =   3000
      Visible         =   0   'False
      Width           =   5925
      Begin VB.CommandButton cmdPROCEDER 
         Caption         =   "Continuar"
         Height          =   375
         Index           =   11
         Left            =   3480
         TabIndex        =   116
         Top             =   2640
         Width           =   975
      End
      Begin VB.CommandButton cmdREGRESAR 
         Caption         =   "Regresar"
         Height          =   375
         Index           =   11
         Left            =   1680
         TabIndex        =   115
         Top             =   2640
         Width           =   975
      End
      Begin VB.TextBox TxtVenta 
         Height          =   285
         Index           =   12
         Left            =   1920
         MaxLength       =   5
         TabIndex        =   83
         Top             =   2040
         Width           =   615
      End
      Begin VB.TextBox TxtVenta 
         Height          =   285
         Index           =   13
         Left            =   4200
         MaxLength       =   5
         TabIndex        =   85
         Top             =   2040
         Width           =   615
      End
      Begin VB.TextBox TxtVenta 
         Height          =   285
         Index           =   4
         Left            =   1440
         MaxLength       =   3
         TabIndex        =   75
         Text            =   "0"
         Top             =   1560
         Width           =   375
      End
      Begin VB.TextBox TxtVenta 
         Enabled         =   0   'False
         Height          =   285
         Index           =   5
         Left            =   4320
         MaxLength       =   2
         TabIndex        =   77
         Top             =   1560
         Width           =   375
      End
      Begin VB.TextBox TxtVenta 
         Enabled         =   0   'False
         Height          =   285
         Index           =   6
         Left            =   4680
         MaxLength       =   2
         TabIndex        =   79
         Top             =   1560
         Width           =   375
      End
      Begin VB.TextBox TxtVenta 
         Enabled         =   0   'False
         Height          =   285
         Index           =   7
         Left            =   5040
         MaxLength       =   4
         TabIndex        =   81
         Top             =   1560
         Width           =   495
      End
      Begin VB.TextBox TxtVenta 
         Enabled         =   0   'False
         Height          =   285
         Index           =   17
         Left            =   960
         MaxLength       =   16
         TabIndex        =   63
         Top             =   720
         Width           =   1575
      End
      Begin VB.TextBox TxtVenta 
         Enabled         =   0   'False
         Height          =   285
         Index           =   16
         Left            =   3960
         MaxLength       =   16
         TabIndex        =   59
         Top             =   360
         Width           =   1575
      End
      Begin VB.TextBox TxtVenta 
         Height          =   285
         Index           =   19
         Left            =   960
         MaxLength       =   16
         TabIndex        =   71
         Top             =   1080
         Width           =   1575
      End
      Begin VB.TextBox TxtVenta 
         Enabled         =   0   'False
         Height          =   285
         Index           =   15
         Left            =   960
         MaxLength       =   16
         TabIndex        =   55
         Top             =   360
         Visible         =   0   'False
         Width           =   1575
      End
      Begin VB.TextBox TxtVenta 
         Enabled         =   0   'False
         Height          =   285
         Index           =   20
         Left            =   3960
         MaxLength       =   16
         TabIndex        =   73
         Top             =   1080
         Width           =   1575
      End
      Begin VB.TextBox TxtVenta 
         Enabled         =   0   'False
         Height          =   285
         Index           =   18
         Left            =   3960
         MaxLength       =   16
         TabIndex        =   67
         Top             =   720
         Width           =   1575
      End
      Begin VB.Label Label23 
         Caption         =   "% Descuento :"
         Height          =   255
         Left            =   840
         TabIndex        =   82
         Top             =   2040
         Width           =   1215
      End
      Begin VB.Label Label22 
         Caption         =   "% Ret Fuente :"
         Height          =   255
         Left            =   3000
         TabIndex        =   84
         Top             =   2040
         Width           =   1095
      End
      Begin VB.Label LblPlazo 
         Caption         =   "D�as de Plazo :"
         Height          =   255
         Left            =   240
         TabIndex        =   74
         Top             =   1560
         Width           =   1095
      End
      Begin VB.Label LblFecha 
         Caption         =   "Fecha de Vencimiento :"
         Height          =   255
         Index           =   1
         Left            =   2520
         TabIndex        =   76
         Top             =   1560
         Width           =   1695
      End
      Begin VB.Label LblImpu 
         Caption         =   "Iva :"
         Height          =   255
         Index           =   2
         Left            =   240
         TabIndex        =   61
         Top             =   720
         Width           =   375
      End
      Begin VB.Label Label11 
         Caption         =   "Descuentos :"
         Height          =   255
         Left            =   2760
         TabIndex        =   57
         Top             =   360
         Width           =   975
      End
      Begin VB.Label Label10 
         Caption         =   "Fletes :"
         Height          =   255
         Left            =   240
         TabIndex        =   69
         Top             =   1080
         Width           =   615
      End
      Begin VB.Label Label9 
         Caption         =   "Neto :"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   2760
         TabIndex        =   72
         Top             =   1080
         Width           =   495
      End
      Begin VB.Label Label7 
         Caption         =   "Bruto :"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   240
         TabIndex        =   53
         Top             =   360
         Width           =   495
      End
      Begin VB.Label LblRete 
         Caption         =   "Rete. Fuente :"
         Height          =   255
         Index           =   2
         Left            =   2760
         TabIndex        =   65
         Top             =   720
         Width           =   1095
      End
   End
   Begin VB.Frame frmCOMPRAS 
      Caption         =   "Compras"
      Height          =   3615
      Left            =   4200
      TabIndex        =   36
      Top             =   360
      Visible         =   0   'False
      Width           =   5925
      Begin VB.TextBox TxtCompra 
         Enabled         =   0   'False
         Height          =   285
         Index           =   5
         Left            =   2520
         MaxLength       =   2
         TabIndex        =   107
         Top             =   3000
         Width           =   375
      End
      Begin VB.TextBox TxtCompra 
         Enabled         =   0   'False
         Height          =   285
         Index           =   6
         Left            =   2880
         MaxLength       =   2
         TabIndex        =   106
         Top             =   3000
         Width           =   375
      End
      Begin VB.TextBox TxtCompra 
         Enabled         =   0   'False
         Height          =   285
         Index           =   7
         Left            =   3240
         MaxLength       =   4
         TabIndex        =   105
         Top             =   3000
         Width           =   495
      End
      Begin VB.TextBox TxtCompra 
         Height          =   285
         Index           =   0
         Left            =   4200
         MaxLength       =   8
         TabIndex        =   102
         Top             =   2400
         Width           =   855
      End
      Begin VB.TextBox TxtCompra 
         Height          =   285
         Index           =   4
         Left            =   2040
         MaxLength       =   3
         TabIndex        =   101
         Text            =   "0"
         Top             =   2400
         Width           =   375
      End
      Begin VB.TextBox TxtCompra 
         Enabled         =   0   'False
         Height          =   285
         Index           =   14
         Left            =   1080
         MaxLength       =   16
         TabIndex        =   52
         Top             =   600
         Width           =   1575
      End
      Begin VB.TextBox TxtCompra 
         Enabled         =   0   'False
         Height          =   285
         Index           =   13
         Left            =   3960
         MaxLength       =   16
         TabIndex        =   50
         Top             =   240
         Width           =   1575
      End
      Begin VB.TextBox TxtCompra 
         Enabled         =   0   'False
         Height          =   285
         Index           =   16
         Left            =   1080
         MaxLength       =   16
         TabIndex        =   48
         Top             =   1320
         Width           =   1575
      End
      Begin VB.TextBox TxtCompra 
         Enabled         =   0   'False
         Height          =   285
         Index           =   12
         Left            =   1080
         MaxLength       =   16
         TabIndex        =   46
         Top             =   240
         Width           =   1575
      End
      Begin VB.TextBox TxtCompra 
         Enabled         =   0   'False
         Height          =   285
         Index           =   17
         Left            =   3960
         MaxLength       =   16
         TabIndex        =   44
         Top             =   1680
         Width           =   1575
      End
      Begin VB.TextBox TxtCompra 
         Enabled         =   0   'False
         Height          =   285
         Index           =   15
         Left            =   3960
         MaxLength       =   16
         TabIndex        =   42
         Top             =   600
         Width           =   1575
      End
      Begin VB.TextBox TxtCompra 
         Enabled         =   0   'False
         Height          =   285
         Index           =   18
         Left            =   1080
         MaxLength       =   16
         TabIndex        =   40
         Top             =   960
         Width           =   1575
      End
      Begin VB.TextBox TxtCompra 
         Enabled         =   0   'False
         Height          =   285
         Index           =   19
         Left            =   3960
         MaxLength       =   16
         TabIndex        =   38
         Top             =   960
         Width           =   1575
      End
      Begin VB.TextBox TxtCompra 
         Enabled         =   0   'False
         Height          =   285
         Index           =   20
         Left            =   3960
         MaxLength       =   16
         TabIndex        =   37
         Top             =   1320
         Width           =   1575
      End
      Begin VB.TextBox TxtCompra 
         Height          =   1605
         Index           =   11
         Left            =   2040
         MaxLength       =   250
         MultiLine       =   -1  'True
         TabIndex        =   183
         Top             =   480
         Width           =   2445
      End
      Begin VB.Label LblFecha 
         Caption         =   "Fecha de Vencimiento :"
         Height          =   255
         Index           =   2
         Left            =   720
         TabIndex        =   108
         Top             =   3000
         Width           =   1695
      End
      Begin VB.Label Label27 
         Caption         =   "No Factura :"
         Height          =   255
         Left            =   3240
         TabIndex        =   104
         Top             =   2400
         Width           =   975
      End
      Begin VB.Label Label26 
         Caption         =   "D�as de Plazo :"
         Height          =   255
         Left            =   840
         TabIndex        =   103
         Top             =   2400
         Width           =   1095
      End
      Begin VB.Label LblImpu 
         Caption         =   "Iva :"
         Height          =   255
         Index           =   4
         Left            =   360
         TabIndex        =   70
         Top             =   600
         Width           =   375
      End
      Begin VB.Label Label21 
         Caption         =   "Descuentos :"
         Height          =   255
         Left            =   2880
         TabIndex        =   68
         Top             =   240
         Width           =   975
      End
      Begin VB.Label Label20 
         Caption         =   "Fletes :"
         Height          =   255
         Left            =   360
         TabIndex        =   66
         Top             =   1320
         Width           =   615
      End
      Begin VB.Label Label19 
         Caption         =   "Neto :"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   2880
         TabIndex        =   64
         Top             =   1680
         Width           =   495
      End
      Begin VB.Label Label18 
         Caption         =   "Bruto :"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   360
         TabIndex        =   62
         Top             =   240
         Width           =   495
      End
      Begin VB.Label LblRete 
         Caption         =   "Rete. Fuente :"
         Height          =   255
         Index           =   4
         Left            =   2880
         TabIndex        =   60
         Top             =   600
         Width           =   1095
      End
      Begin VB.Label Label1 
         Caption         =   "ReteICA:"
         Height          =   255
         Index           =   0
         Left            =   2880
         TabIndex        =   58
         Top             =   960
         Width           =   1095
      End
      Begin VB.Label Label2 
         AutoSize        =   -1  'True
         Caption         =   "Rete IVA "
         Height          =   195
         Index           =   2
         Left            =   360
         TabIndex        =   56
         Top             =   960
         Width           =   690
      End
      Begin VB.Label Label17 
         Caption         =   "Descuento Adicional CXP"
         Height          =   375
         Left            =   2880
         TabIndex        =   54
         Top             =   1320
         Width           =   975
      End
   End
   Begin VB.Frame frmBAJA 
      Caption         =   "Baja"
      Height          =   2775
      Left            =   1320
      TabIndex        =   22
      Top             =   1680
      Visible         =   0   'False
      Width           =   6375
      Begin VB.ComboBox cmbDEPENDENCIA 
         Height          =   315
         Left            =   1560
         Style           =   2  'Dropdown List
         TabIndex        =   185
         Top             =   1320
         Width           =   4455
      End
      Begin VB.TextBox TxtBajaD 
         Height          =   285
         Index           =   0
         Left            =   2760
         MaxLength       =   11
         TabIndex        =   184
         Top             =   1320
         Visible         =   0   'False
         Width           =   3375
      End
      Begin VB.CommandButton cmdREGRESAR 
         Caption         =   "Regresar"
         Height          =   375
         Index           =   8
         Left            =   1920
         TabIndex        =   110
         Top             =   2160
         Width           =   975
      End
      Begin VB.CommandButton cmdPROCEDER 
         Caption         =   "Continuar"
         Enabled         =   0   'False
         Height          =   375
         Index           =   8
         Left            =   3600
         TabIndex        =   100
         Top             =   2160
         Width           =   975
      End
      Begin VB.TextBox TxtBajaD 
         Height          =   285
         Index           =   4
         Left            =   1320
         MaxLength       =   11
         TabIndex        =   78
         Top             =   1320
         Visible         =   0   'False
         Width           =   855
      End
      Begin VB.OptionButton OptSalida 
         Caption         =   "Baja"
         Height          =   255
         Index           =   0
         Left            =   960
         TabIndex        =   29
         Top             =   240
         Value           =   -1  'True
         Visible         =   0   'False
         Width           =   735
      End
      Begin VB.OptionButton OptSalida 
         Caption         =   "Venta"
         Height          =   255
         Index           =   1
         Left            =   1920
         TabIndex        =   27
         Top             =   240
         Visible         =   0   'False
         Width           =   735
      End
      Begin VB.TextBox TxtBajaD 
         Enabled         =   0   'False
         Height          =   285
         Index           =   9
         Left            =   2520
         MaxLength       =   16
         TabIndex        =   23
         Top             =   600
         Width           =   1695
      End
      Begin Threed.SSCommand CmdSelec 
         Height          =   375
         Index           =   0
         Left            =   2160
         TabIndex        =   109
         ToolTipText     =   "Selecci�n de Dependencias"
         Top             =   1320
         Visible         =   0   'False
         Width           =   615
         _Version        =   65536
         _ExtentX        =   1085
         _ExtentY        =   661
         _StockProps     =   78
         BevelWidth      =   0
         RoundedCorners  =   0   'False
         Outline         =   0   'False
         Picture         =   "FrmGENERAL.frx":B39E
      End
      Begin VB.Label LblDependencia 
         Caption         =   "Dependencia :"
         Height          =   255
         Left            =   360
         TabIndex        =   80
         Top             =   1320
         Width           =   1095
      End
      Begin VB.Label Label16 
         AutoSize        =   -1  'True
         Caption         =   "Valor:"
         Height          =   195
         Left            =   1920
         TabIndex        =   25
         Top             =   600
         Width           =   405
      End
   End
   Begin VB.Frame frmSALIDA 
      Caption         =   "Salida"
      Height          =   1695
      Left            =   6720
      TabIndex        =   17
      Top             =   120
      Visible         =   0   'False
      Width           =   3615
      Begin VB.CommandButton cmdPROCEDER 
         Caption         =   "Continuar"
         Height          =   375
         Index           =   19
         Left            =   2160
         TabIndex        =   178
         Top             =   1080
         Width           =   975
      End
      Begin VB.CommandButton cmdREGRESAR 
         Caption         =   "Regresar"
         Height          =   375
         Index           =   19
         Left            =   360
         TabIndex        =   177
         Top             =   1080
         Width           =   975
      End
      Begin VB.TextBox TxtSaliA 
         Enabled         =   0   'False
         Height          =   285
         Index           =   7
         Left            =   1440
         MaxLength       =   16
         TabIndex        =   19
         Top             =   360
         Width           =   1575
      End
      Begin VB.Label Label15 
         AutoSize        =   -1  'True
         Caption         =   "Valor:"
         Height          =   195
         Left            =   840
         TabIndex        =   20
         Top             =   360
         Width           =   405
      End
   End
   Begin VB.Frame frmOTROSCARGOS 
      Caption         =   "Otros Cargos x Facturar"
      Height          =   4335
      Left            =   960
      TabIndex        =   199
      Top             =   1920
      Visible         =   0   'False
      Width           =   8295
      Begin VB.CommandButton cmdOtrosCargos 
         Caption         =   "&Forma de Pago"
         Height          =   495
         Left            =   4680
         TabIndex        =   212
         Top             =   3600
         Width           =   1695
      End
      Begin MSFlexGridLib.MSFlexGrid grdOtrosCargos 
         Height          =   1935
         Left            =   120
         TabIndex        =   203
         Top             =   1440
         Width           =   4095
         _ExtentX        =   7223
         _ExtentY        =   3413
         _Version        =   393216
         Cols            =   4
         FixedCols       =   0
         ForeColorSel    =   -2147483635
         FocusRect       =   2
         MergeCells      =   1
      End
      Begin VB.Frame Frame1 
         Height          =   2055
         Left            =   4560
         TabIndex        =   204
         Top             =   1320
         Width           =   3615
         Begin VB.TextBox txtPorceCargo 
            Alignment       =   1  'Right Justify
            Height          =   375
            Left            =   2040
            TabIndex        =   208
            Text            =   "0.00"
            Top             =   960
            Width           =   1215
         End
         Begin VB.TextBox txtValorCargo 
            Alignment       =   1  'Right Justify
            Height          =   375
            Left            =   1440
            TabIndex        =   210
            Text            =   "$ 0"
            Top             =   1560
            Width           =   1815
         End
         Begin VB.ComboBox cboOtrosCargos 
            Height          =   315
            Left            =   1200
            TabIndex        =   205
            Top             =   360
            Width           =   2295
         End
         Begin VB.Label Label2 
            Alignment       =   1  'Right Justify
            Caption         =   "Porcentaje:"
            Height          =   375
            Index           =   6
            Left            =   960
            TabIndex        =   206
            Top             =   960
            Width           =   975
         End
         Begin VB.Label Label2 
            Alignment       =   1  'Right Justify
            Caption         =   "Valor:"
            Height          =   375
            Index           =   3
            Left            =   360
            TabIndex        =   209
            Top             =   1560
            Width           =   975
         End
         Begin VB.Label Label28 
            Alignment       =   1  'Right Justify
            Caption         =   "Cargo:"
            Height          =   375
            Left            =   240
            TabIndex        =   207
            Top             =   360
            Width           =   855
         End
      End
      Begin VB.Frame Frame2 
         Height          =   855
         Left            =   1680
         TabIndex        =   200
         Top             =   360
         Width           =   4935
         Begin VB.TextBox txtValorOtrosCargos 
            Alignment       =   1  'Right Justify
            Enabled         =   0   'False
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   13.5
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   495
            Left            =   1440
            TabIndex        =   201
            Text            =   "$ 0"
            Top             =   240
            Width           =   3140
         End
         Begin VB.Label Label1 
            Alignment       =   1  'Right Justify
            Caption         =   "Valor a Pagar:"
            Height          =   375
            Index           =   2
            Left            =   120
            TabIndex        =   202
            Top             =   360
            Width           =   1095
         End
      End
      Begin VB.CommandButton cmdOtrosCancelar 
         Caption         =   "&Cancelar"
         Height          =   495
         Left            =   2400
         TabIndex        =   211
         Top             =   3600
         Width           =   1695
      End
   End
   Begin VB.Frame frmFORMAPAGO 
      Caption         =   "Forma de Pago"
      Height          =   4335
      Left            =   960
      TabIndex        =   187
      Top             =   1680
      Visible         =   0   'False
      Width           =   8295
      Begin VB.CommandButton cmdFacturar 
         Caption         =   "&Facturar"
         Height          =   495
         Left            =   4680
         TabIndex        =   198
         Top             =   3600
         Width           =   1695
      End
      Begin VB.CommandButton cmdCancelar 
         Caption         =   "&Cancelar"
         Height          =   495
         Left            =   2400
         TabIndex        =   197
         Top             =   3600
         Width           =   1695
      End
      Begin VB.Frame Frame4 
         Height          =   855
         Left            =   1680
         TabIndex        =   194
         Top             =   240
         Width           =   4935
         Begin VB.TextBox txtValorFactura 
            Alignment       =   1  'Right Justify
            Enabled         =   0   'False
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   13.5
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   495
            Left            =   1560
            TabIndex        =   195
            Text            =   "$ 0"
            Top             =   240
            Width           =   3140
         End
         Begin VB.Label Label1 
            Alignment       =   1  'Right Justify
            Caption         =   "Valor a Pagar:"
            Height          =   375
            Index           =   3
            Left            =   240
            TabIndex        =   196
            Top             =   360
            Width           =   1215
         End
      End
      Begin VB.Frame Frame3 
         Height          =   2055
         Left            =   4560
         TabIndex        =   188
         Top             =   1320
         Width           =   3615
         Begin VB.ComboBox cboFormas 
            Height          =   315
            Left            =   1200
            TabIndex        =   189
            Top             =   360
            Width           =   2295
         End
         Begin VB.TextBox txtValorRenglon 
            Alignment       =   1  'Right Justify
            Height          =   375
            Left            =   1680
            TabIndex        =   190
            Text            =   "$ 0"
            Top             =   1200
            Width           =   1815
         End
         Begin VB.Label Label29 
            Alignment       =   1  'Right Justify
            Caption         =   "Forma:"
            Height          =   375
            Left            =   120
            TabIndex        =   193
            Top             =   360
            Width           =   855
         End
         Begin VB.Label Label2 
            Alignment       =   1  'Right Justify
            Caption         =   "Valor Parcial:"
            Height          =   375
            Index           =   4
            Left            =   240
            TabIndex        =   192
            Top             =   1200
            Width           =   1095
         End
      End
      Begin MSFlexGridLib.MSFlexGrid grdFormaPago 
         Height          =   1935
         Left            =   120
         TabIndex        =   191
         Top             =   1440
         Width           =   4095
         _ExtentX        =   7223
         _ExtentY        =   3413
         _Version        =   393216
         Cols            =   3
         FixedCols       =   0
         ForeColorSel    =   -2147483635
         FocusRect       =   2
         MergeCells      =   1
      End
   End
   Begin VB.Frame frmOTROSDESCUENTOS 
      Caption         =   "Otros Descuentos"
      Height          =   4335
      Left            =   960
      TabIndex        =   213
      Top             =   2880
      Visible         =   0   'False
      Width           =   8295
      Begin VB.Frame Frame6 
         Height          =   2055
         Left            =   4560
         TabIndex        =   217
         Top             =   1320
         Width           =   3615
         Begin VB.TextBox txtPorceDescuento 
            Alignment       =   1  'Right Justify
            Height          =   375
            Left            =   2040
            TabIndex        =   220
            Text            =   "0.00"
            Top             =   960
            Width           =   1215
         End
         Begin VB.TextBox txtValorDescuento 
            Alignment       =   1  'Right Justify
            Height          =   375
            Left            =   1440
            TabIndex        =   222
            Text            =   "$ 0"
            Top             =   1560
            Width           =   1815
         End
         Begin VB.ComboBox cboOtrosDescuentos 
            Height          =   315
            Left            =   1200
            TabIndex        =   218
            Top             =   360
            Width           =   2295
         End
         Begin VB.Label Label2 
            Alignment       =   1  'Right Justify
            Caption         =   "Valor:"
            Height          =   375
            Index           =   5
            Left            =   480
            TabIndex        =   221
            Top             =   1560
            Width           =   855
         End
         Begin VB.Label Label30 
            Alignment       =   1  'Right Justify
            Caption         =   "Descuento:"
            Height          =   375
            Left            =   120
            TabIndex        =   219
            Top             =   360
            Width           =   855
         End
         Begin VB.Label Label2 
            Alignment       =   1  'Right Justify
            Caption         =   "Porcentaje:"
            Height          =   375
            Index           =   7
            Left            =   960
            TabIndex        =   223
            Top             =   960
            Width           =   975
         End
      End
      Begin VB.Frame Frame5 
         Height          =   855
         Left            =   1680
         TabIndex        =   214
         Top             =   360
         Width           =   4935
         Begin VB.TextBox txtValorOtrosDescuentos 
            Alignment       =   1  'Right Justify
            Enabled         =   0   'False
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   13.5
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   495
            Left            =   1560
            TabIndex        =   215
            Text            =   "$ 0"
            Top             =   240
            Width           =   3140
         End
         Begin VB.Label Label1 
            Alignment       =   1  'Right Justify
            Caption         =   "Valor a Pagar:"
            Height          =   375
            Index           =   4
            Left            =   120
            TabIndex        =   216
            Top             =   360
            Width           =   1215
         End
      End
      Begin VB.CommandButton cmdDescuentosCancelar 
         Caption         =   "&Cancelar"
         Height          =   495
         Left            =   2400
         TabIndex        =   224
         Top             =   3600
         Width           =   1695
      End
      Begin VB.CommandButton cmdOtrosDescuentos 
         Caption         =   "&Otros Cargos"
         Height          =   495
         Left            =   4680
         TabIndex        =   225
         Top             =   3600
         Width           =   1695
      End
      Begin MSFlexGridLib.MSFlexGrid grdOtrosDescuentos 
         Height          =   1935
         Left            =   120
         TabIndex        =   226
         Top             =   1440
         Width           =   4095
         _ExtentX        =   7223
         _ExtentY        =   3413
         _Version        =   393216
         Cols            =   4
         FixedCols       =   0
         ForeColorSel    =   -2147483635
         FocusRect       =   2
         MergeCells      =   1
      End
   End
   Begin VB.Frame FrmPpto 
      Caption         =   "Ppto"
      Height          =   1815
      Left            =   120
      TabIndex        =   228
      Top             =   120
      Visible         =   0   'False
      Width           =   3975
      Begin VB.TextBox TxtPpto 
         Height          =   285
         Index           =   3
         Left            =   1560
         TabIndex        =   251
         Top             =   1320
         Width           =   1335
      End
      Begin VB.TextBox TxtPpto 
         Height          =   285
         Index           =   2
         Left            =   1560
         TabIndex        =   233
         Top             =   960
         Width           =   1335
      End
      Begin VB.TextBox TxtPpto 
         Height          =   285
         Index           =   1
         Left            =   1560
         TabIndex        =   231
         Top             =   600
         Width           =   1335
      End
      Begin VB.TextBox TxtPpto 
         Height          =   285
         Index           =   0
         Left            =   1560
         TabIndex        =   229
         Top             =   240
         Width           =   1335
      End
      Begin VB.Label LblPpto 
         Caption         =   "Valor Doc Ptal"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   3
         Left            =   120
         TabIndex        =   252
         Top             =   1320
         Width           =   1335
      End
      Begin VB.Label LblPpto 
         Caption         =   "Valor"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   2
         Left            =   120
         TabIndex        =   234
         Top             =   960
         Width           =   1335
      End
      Begin VB.Label LblPpto 
         Caption         =   "Dependencia"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   1
         Left            =   120
         TabIndex        =   232
         Top             =   600
         Width           =   1335
      End
      Begin VB.Label LblPpto 
         Caption         =   "Consecutivo"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   0
         Left            =   120
         TabIndex        =   230
         Top             =   240
         Width           =   1095
      End
   End
   Begin VB.Frame FrmENTRADA 
      Caption         =   "Entrada"
      Height          =   3615
      Left            =   240
      TabIndex        =   0
      Top             =   4080
      Visible         =   0   'False
      Width           =   10000
      Begin VB.CommandButton cmdREGRESAR 
         Caption         =   "Regresar"
         Height          =   375
         Index           =   3
         Left            =   3720
         TabIndex        =   114
         Top             =   3000
         Width           =   975
      End
      Begin VB.CommandButton cmdPROCEDER 
         Caption         =   "Continuar"
         Height          =   375
         Index           =   3
         Left            =   5400
         TabIndex        =   113
         Top             =   3000
         Width           =   975
      End
      Begin VB.TextBox TxtCompra 
         Height          =   285
         Index           =   10
         Left            =   2040
         MaxLength       =   20
         MultiLine       =   -1  'True
         ScrollBars      =   2  'Vertical
         TabIndex        =   33
         Top             =   1560
         Width           =   1600
      End
      Begin VB.OptionButton OptProveedor 
         Caption         =   "Proveedor"
         Height          =   255
         Left            =   1560
         TabIndex        =   31
         Top             =   1080
         Value           =   -1  'True
         Width           =   1095
      End
      Begin VB.OptionButton OptParticular 
         Caption         =   "Particular"
         Height          =   255
         Left            =   2760
         TabIndex        =   32
         Top             =   1080
         Width           =   1095
      End
      Begin VB.TextBox TxtEntra 
         Enabled         =   0   'False
         Height          =   285
         Index           =   13
         Left            =   7680
         MaxLength       =   16
         TabIndex        =   41
         Top             =   1440
         Width           =   1575
      End
      Begin VB.TextBox TxtEntra 
         Enabled         =   0   'False
         Height          =   285
         Index           =   10
         Left            =   5040
         MaxLength       =   16
         TabIndex        =   34
         Top             =   1080
         Width           =   1575
      End
      Begin VB.TextBox TxtEntra 
         Enabled         =   0   'False
         Height          =   285
         Index           =   14
         Left            =   5040
         MaxLength       =   16
         TabIndex        =   47
         Top             =   2160
         Width           =   1575
      End
      Begin VB.TextBox TxtEntra 
         Enabled         =   0   'False
         Height          =   285
         Index           =   11
         Left            =   7680
         MaxLength       =   16
         TabIndex        =   35
         Top             =   1080
         Width           =   1575
      End
      Begin VB.TextBox TxtEntra 
         Enabled         =   0   'False
         Height          =   285
         Index           =   12
         Left            =   5040
         MaxLength       =   16
         TabIndex        =   39
         Top             =   1440
         Width           =   1575
      End
      Begin VB.TextBox TxtEntra 
         Enabled         =   0   'False
         Height          =   285
         Index           =   18
         Left            =   5040
         MaxLength       =   16
         TabIndex        =   43
         Top             =   1800
         Width           =   1575
      End
      Begin VB.TextBox TxtEntra 
         Enabled         =   0   'False
         Height          =   285
         Index           =   19
         Left            =   7680
         MaxLength       =   16
         TabIndex        =   45
         Top             =   1800
         Width           =   1575
      End
      Begin VB.TextBox TxtEntra 
         Enabled         =   0   'False
         Height          =   285
         Index           =   20
         Left            =   7680
         MaxLength       =   16
         TabIndex        =   49
         Top             =   2160
         Width           =   1575
      End
      Begin VB.TextBox TxtEntra 
         Height          =   285
         Index           =   7
         Left            =   2160
         Locked          =   -1  'True
         MaxLength       =   7
         TabIndex        =   24
         Top             =   360
         Width           =   615
      End
      Begin VB.TextBox TxtEntra 
         Height          =   285
         Index           =   8
         Left            =   4500
         Locked          =   -1  'True
         MaxLength       =   7
         TabIndex        =   26
         Top             =   360
         Width           =   615
      End
      Begin VB.TextBox TxtEntra 
         Height          =   285
         Index           =   16
         Left            =   6720
         Locked          =   -1  'True
         MaxLength       =   7
         TabIndex        =   28
         Top             =   360
         Width           =   615
      End
      Begin VB.TextBox TxtEntra 
         Height          =   285
         Index           =   17
         Left            =   8715
         Locked          =   -1  'True
         MaxLength       =   7
         TabIndex        =   30
         Top             =   360
         Width           =   615
      End
      Begin VB.TextBox TxtEntra 
         Enabled         =   0   'False
         Height          =   285
         Index           =   15
         Left            =   7680
         MaxLength       =   16
         TabIndex        =   51
         Top             =   2640
         Width           =   1575
      End
      Begin VB.Label lblIndi 
         Alignment       =   2  'Center
         AutoSize        =   -1  'True
         BackColor       =   &H00FFFFFF&
         BackStyle       =   0  'Transparent
         Caption         =   "O"
         BeginProperty Font 
            Name            =   "Wingdings"
            Size            =   9
            Charset         =   2
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00000040&
         Height          =   210
         Index           =   1
         Left            =   3960
         TabIndex        =   254
         ToolTipText     =   "El n�mero de factura activar� la opci�n de adicionar impuestos."
         Top             =   1560
         Width           =   180
      End
      Begin VB.Label LblNuFac 
         Caption         =   "No Factura :"
         Height          =   255
         Left            =   1080
         TabIndex        =   8
         Top             =   1560
         Width           =   975
      End
      Begin VB.Label Label6 
         AutoSize        =   -1  'True
         Caption         =   "Pagar Fletes a:"
         Height          =   195
         Left            =   360
         TabIndex        =   7
         Top             =   1080
         Width           =   1065
      End
      Begin VB.Label LblRete 
         Caption         =   "Rete. Fuente :"
         Height          =   255
         Index           =   0
         Left            =   6720
         TabIndex        =   12
         Top             =   1440
         Width           =   1095
      End
      Begin VB.Label LblBruto 
         Caption         =   "Bruto :"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   4320
         TabIndex        =   9
         Top             =   1080
         Width           =   495
      End
      Begin VB.Label LblNeto 
         Caption         =   "Neto :"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   6720
         TabIndex        =   21
         Top             =   2640
         Width           =   495
      End
      Begin VB.Label LblFletes 
         Caption         =   "Fletes "
         Height          =   255
         Left            =   4320
         TabIndex        =   16
         Top             =   2160
         Width           =   615
      End
      Begin VB.Label LblDescu 
         Caption         =   "Descuentos "
         Height          =   255
         Left            =   6720
         TabIndex        =   10
         Top             =   1080
         Width           =   975
      End
      Begin VB.Label LblImpu 
         Caption         =   "IVA "
         Height          =   255
         Index           =   0
         Left            =   4320
         TabIndex        =   11
         Top             =   1440
         Width           =   375
      End
      Begin VB.Label LblImpu 
         AutoSize        =   -1  'True
         Caption         =   "Rete IVA"
         Height          =   195
         Index           =   1
         Left            =   4320
         TabIndex        =   13
         Top             =   1800
         Width           =   645
      End
      Begin VB.Label LblRete 
         Caption         =   "Rete ICA"
         Height          =   255
         Index           =   1
         Left            =   6720
         TabIndex        =   14
         Top             =   1800
         Width           =   1095
      End
      Begin VB.Label Label5 
         Caption         =   "Descuento Adicional CXP"
         Height          =   375
         Left            =   6720
         TabIndex        =   18
         Top             =   2160
         Width           =   975
      End
      Begin VB.Label LblDescuento 
         Caption         =   "% Descuento :"
         Height          =   255
         Left            =   840
         TabIndex        =   3
         Top             =   360
         Width           =   1095
      End
      Begin VB.Label LblPrete 
         Caption         =   "% Ret Fuente :"
         Height          =   255
         Left            =   3420
         TabIndex        =   4
         Top             =   360
         Width           =   1095
      End
      Begin VB.Label Label4 
         Caption         =   "% Rete IVA:"
         Height          =   255
         Left            =   5490
         TabIndex        =   5
         Top             =   360
         Width           =   1095
      End
      Begin VB.Label Label2 
         Caption         =   "% Rete ICA:"
         Height          =   255
         Index           =   0
         Left            =   7755
         TabIndex        =   6
         Top             =   360
         Width           =   1095
      End
   End
   Begin VB.Frame FrmImpuestos 
      Height          =   4095
      Left            =   240
      TabIndex        =   235
      Top             =   0
      Visible         =   0   'False
      Width           =   9975
      Begin VB.ListBox lstDesc 
         Height          =   450
         Left            =   6720
         TabIndex        =   250
         Top             =   600
         Visible         =   0   'False
         Width           =   1695
      End
      Begin VB.ListBox lstConcepto 
         Height          =   255
         Left            =   8040
         TabIndex        =   248
         Top             =   240
         Visible         =   0   'False
         Width           =   1335
      End
      Begin VB.PictureBox picHelp 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         BackColor       =   &H80000005&
         BorderStyle     =   0  'None
         ForeColor       =   &H80000008&
         Height          =   120
         Index           =   0
         Left            =   7750
         ScaleHeight     =   120
         ScaleWidth      =   120
         TabIndex        =   247
         Tag             =   $"FrmGENERAL.frx":B7F0
         ToolTipText     =   "Ayuda"
         Top             =   240
         Width           =   120
      End
      Begin VB.ComboBox cmbConcepto 
         Height          =   315
         Left            =   3600
         Style           =   2  'Dropdown List
         TabIndex        =   245
         Top             =   240
         Width           =   4095
      End
      Begin VB.Frame FraFondo 
         Height          =   3375
         Left            =   3600
         TabIndex        =   240
         Top             =   600
         Width           =   6260
         Begin VB.ListBox lstImpu 
            Height          =   450
            Left            =   1320
            TabIndex        =   249
            Top             =   0
            Visible         =   0   'False
            Width           =   1695
         End
         Begin MSFlexGridLib.MSFlexGrid GrdDeIm 
            Height          =   3000
            Left            =   90
            TabIndex        =   243
            Top             =   240
            Width           =   6100
            _ExtentX        =   10769
            _ExtentY        =   5292
            _Version        =   393216
            Rows            =   1
            Cols            =   9
            FixedCols       =   0
            AllowUserResizing=   1
            FormatString    =   "| |             Descripcion            |       %    |          Valor        |  Suma(S) - Resta(R)"
         End
         Begin VB.TextBox TxtVlBase 
            Height          =   285
            Left            =   1200
            Locked          =   -1  'True
            TabIndex        =   241
            Top             =   2580
            Visible         =   0   'False
            Width           =   1455
         End
         Begin VB.CommandButton CmdTerminar 
            Caption         =   "&Terminar"
            Height          =   255
            Left            =   4920
            TabIndex        =   242
            Top             =   2640
            Visible         =   0   'False
            Width           =   1215
         End
         Begin VB.Label LbBase 
            Caption         =   "Valor Base"
            Height          =   255
            Left            =   240
            TabIndex        =   244
            Top             =   2595
            Visible         =   0   'False
            Width           =   855
         End
      End
      Begin VB.CommandButton Cmd_agregar 
         Caption         =   ">>"
         Enabled         =   0   'False
         Height          =   255
         Left            =   3180
         TabIndex        =   239
         Top             =   1800
         Width           =   375
      End
      Begin VB.CommandButton Cmd_Quitar 
         Caption         =   "<<"
         Height          =   255
         Left            =   3180
         TabIndex        =   238
         Top             =   2280
         Width           =   375
      End
      Begin VB.Frame FrmLista 
         Caption         =   "Lista"
         Height          =   3375
         Left            =   120
         TabIndex        =   236
         Top             =   600
         Width           =   3015
         Begin MSComctlLib.TreeView TrvLista 
            Height          =   3015
            Left            =   75
            TabIndex        =   237
            Top             =   240
            Width           =   2850
            _ExtentX        =   5027
            _ExtentY        =   5318
            _Version        =   393217
            Indentation     =   176
            LineStyle       =   1
            Style           =   7
            HotTracking     =   -1  'True
            Appearance      =   1
         End
      End
      Begin VB.Label lblNoCont 
         BackStyle       =   0  'Transparent
         Caption         =   "La bodega actual no afecta contabilidad."
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   495
         Left            =   120
         TabIndex        =   255
         Top             =   180
         Width           =   2055
      End
      Begin VB.Label lblIndi 
         Alignment       =   2  'Center
         AutoSize        =   -1  'True
         BackColor       =   &H00FFFFFF&
         BackStyle       =   0  'Transparent
         Caption         =   "O"
         BeginProperty Font 
            Name            =   "Wingdings"
            Size            =   9
            Charset         =   2
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00000040&
         Height          =   210
         Index           =   0
         Left            =   3255
         TabIndex        =   253
         ToolTipText     =   "Para activar esta opci�n debe escribir un n�mero de factura."
         Top             =   1500
         Width           =   180
      End
      Begin VB.Label lblEtiq 
         Alignment       =   1  'Right Justify
         AutoSize        =   -1  'True
         Caption         =   "Filtrar por concepto"
         Height          =   195
         Left            =   2160
         TabIndex        =   246
         Top             =   240
         Width           =   1365
      End
   End
   Begin VB.Image ImgLogo 
      BorderStyle     =   1  'Fixed Single
      Height          =   2295
      Left            =   11040
      OLEDragMode     =   1  'Automatic
      OLEDropMode     =   2  'Automatic
      Stretch         =   -1  'True
      Top             =   240
      Width           =   1815
   End
End
Attribute VB_Name = "FrmGENERAL"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
'Dim Tabla As String
'Dim Gol, Bad As Boolean
'Variables para controlar el foco del grid de Art�culos
Public OpcCod        As String   'Opci�n de seguridad
Private NumeroCompro As Long
'Variables de interface
'Public SiMuestra    As Boolean
Dim ValReg          As Double
Dim Ppto            As Boolean
Dim ArtiPpto()
Dim ArtiFlete       As String
Dim DescArtiF       As String
'Dim stBodOrig() As String * 1, stBodDest() As String * 1 '|.DR.|, R:1633       'DEPURACION DE CODIGO

Dim arrDPDCS() As Variant 'arreglo para las dependencias
Private comprobante As String
Private vGrabaBaja As Boolean

'Dim Conse_CXC       As Long
'Dim Conse_CXP       As Long
'Private blnContab       As Boolean 'tiene interface contabilidad
'Private blnXPagar       As Boolean 'tiene interface cuentas x pagar
'Private blnXCobrar      As Boolean 'tiene interface cuentas x cobrar
Private blnDesdeCompras As Boolean 'tiene interface cuentas x cobrar
Private vCbCell         As Boolean
Private LaPuedoCerrar As Boolean

Private focol As Long 'n�mero de columna de la grilla que tiene el foco
Private fofil As Long 'n�mero de fila de la grilla que tiene el foco
Private AuTiDoc As Long 'Autoum�rico en la tabla IN_DOCUMENTO
Private Mensaje As String  'Nombre del documento que se esta procesando
Private EsNuevo As Boolean 'Bandera para saber si el documento es nuevo
Private EsAnul  As Boolean 'Bandera para saber si el documento esta anulado
Private SeActi  As Boolean 'Bandera para saber si el documento ya se activo
Private FiActiv As Long 'numero de la fila de la grilla activa
Private FechUlt As Long 'Fecha de creaci�n del �ltimo documento de ese tipo
Private FechVgn As Long 'Fecha de vigencia
Private FechCie As Long 'Fecha de creaci�n del �ltimo documento de ese tipo
Private DefDGrl As String 'cuerda que tiene los parametros para l�a cracion de la grilla
Private CntGrll As Variant 'Contenido de la celda activa de la grilla de articulos

Private vGriAutoArticulo        As Byte 'N�mero de columna de la grilla que contiene el autonum�rico del art�culo
Private vMarcar                 As Byte 'N�mero de columna de la grilla para el chulo
Private vGriCodigoArticulo      As Byte 'N�mero de columna de la grilla que contiene el c�digo del art�culo
Private vGriNombreArticulo      As Byte 'N�mero de columna de la grilla que contiene el nombre del art�culo
Private vGriAutoUnidad          As Byte 'N�mero de columna de la grilla que contiene el Auto de la unidad de distribuci�n
Private vGriNombreUnidad        As Byte 'N�mero de columna de la grilla que contiene el nombre de la unidad de distribuci�n
Private vGriMultiplicador       As Byte 'N�mero de columna de la grilla que contiene el Muntiplicador de la unidad de distribuci�n
Private vGriDivisor             As Byte 'N�mero de columna de la grilla que contiene el Divisor de la unidad de distribuci�n
Private vGriUltimoCosto         As Byte 'N�mero de columna de la grilla que contiene el �ltimo costo
Private vGriCantidad            As Byte 'N�mero de columna de la grilla que contiene la cantidad
Private vGriCostoSinIVA         As Byte 'N�mero de columna de la grilla que contiene el Costo unitario sin IVA
Private vGriPorceIVA            As Byte 'N�mero de columna de la grilla que contiene el Porcentaje de IVA
Private vGriCostoUnidad         As Byte 'N�mero de columna de la grilla que contiene el Costo unitario con IVA
Private vGriCostoTotal          As Byte 'N�mero de columna de la grilla que contiene el Costo total
Private vGriVentaSinIVA         As Byte 'N�mero de columna de la grilla que contiene el Costo total
'Private vGriPorceIVAVenta       As Byte 'N�mero de columna de la grilla que contiene el Costo total
Private vGriVentaTotal          As Byte 'N�mero de columna de la grilla que contiene el Costo total
Private vGriDescPorcentual      As Byte 'N�mero de columna de la grilla para el descuento porcentual
Private vGriDescAbsoluto        As Byte 'N�mero de columna de la grilla para el descuento absoluto
Private vGriVenUndConDes        As Byte 'N�mero de columna de la grilla para el descuento absoluto
Private vGriVenTotConDes        As Byte 'N�mero de columna de la grilla para el descuento absoluto
Private vGriFecVence            As Byte 'N�mero de columna de la grilla que contiene la fecha de vancimiento
Private vGriFecEntrada          As Byte 'N�mero de columna de la grilla que contiene la fecha de entrada
Private vGriCantidadFija        As Byte 'N�mero de columna de la grilla que contiene el Cantidad del docomento original
Private vGriAutoDocumento       As Byte 'N�mero de columna de la grilla que contiene el Auto num�rico del Doc original
Private vSeleccionada           As Byte 'N�mero de columna de la grilla 0 si no tiene chulo,0 si no
Private vLoteProduccion         As Byte 'N�mero de columna de la grilla para el lote de produccion
Private vColCodigoImpuesto As Byte 'N�mero de columna de la grilla para el lote de produccion
Private vCodigoFormaCredito As Integer 'codigo de la forma_pago es credito para generar la CXC
Private vCodigoFormaEfectivo As Integer 'codigo de la forma_pago es credito para generar la CXC

'HRR Req1553
Private vGriCostoSinIvaFija As Byte 'N�mero de columna de la grilla que contiene el Costo sin iva del docomento original
Private vGriPorceIvaFija As Byte 'N�mero de columna de la grilla que contiene el Porcentaje de iva del docomento original
Private vGriCostoUnidadFija As Byte 'N�mero de columna de la grilla que contiene el Costo Unitario del docomento original
'Req.1553

Private vProXLts As Boolean
Private vSeGraba As Boolean

''presupesto
Private vGiroIng As Long

Private WithEvents WinDoc               As WinDocumento
Attribute WinDoc.VB_VarHelpID = -1
Private WithEvents Documento            As ElDocumento
Attribute Documento.VB_VarHelpID = -1
''////////////////////////////
Dim itmCuentasIVA() As Variant
''////////////////////////////
Private vTotalFactura As Currency
Private vSeFactura As Boolean
Dim itmFormas() As Variant, itmCuentasFormas() As Variant
''////////////////////////////
''////////////////////////////
'Private vTotalOtrosCargos As Single
Private vSeCarga As Boolean
Dim itmOtrosCargos() As Variant, itmCuentasOtrosCargos() As Variant
''////////////////////////////
''////////////////////////////
'Private vTotalOtrosCargos As Single
Private vSeDescuenta As Boolean
Dim itmOtrosDescuentos() As Variant, itmCuentasOtrosDescuentos() As Variant, itemValoTopeDescuento() As Variant
''////////////////////////////

Dim boAfectC As Boolean, stDesc As String '|.DR.|, R:1633

'PedroJ
Dim Contrato  As String, CompPpto As String
Dim RegPPto As Double, AutoDocCargado As Double
Dim Imput As Integer
Dim ArrCxP() As Variant
Dim DatPpto As Boolean

Dim AutoRet As String, AutRetIva As String, AutRetIca As String         'Req 1308-934-1067
Dim TipoRegimen As String, GranContrib As String                        'Req 1308-934-1067
Dim ExentoIva As String, ExentoIca As String, ExentoFuente As String    'Req 1308-934-1067
Dim TipoPers As String                                                  'Req 1308-934-1067
Dim BandLoad As Integer
Dim Desco As Boolean 'REOL920
Dim indi As Variant 'SMDL M1143
Dim SumaCosto   As Double  'REOL M1642
Dim StDoc As String 'HRR M1818
Dim DbTotIva As Double 'HRR M1818
Dim LnAntoEncaEnid As Long 'GMS M3559
Dim InBanCont As Integer 'CARV M5084
Dim CodOpc As String 'APGR T1643
Dim BoResult As Boolean  'GAVL T4959 'Variable para manejar  la condicion de la  fecha de la orden para los registro pptales
'Dim BoAproxCent As Boolean  'GAVL T6423 'JLPB T23820 SE DEJA EN COMENTARIO
'SKRV T15377 INICIO
Dim StRegEmpnavi As String
Dim StOpcCxpnavi As String
'SKRV T15377 FIN
Dim StImpCree As String 'OMOG R16934/T19113 VARIABLE QUE GUARDA LA OPCION SELECCIONADA PARA EL TERCERO EN CUANTO A "NO SUJETO PASIVO CREE O AUTORRETENEDORES DEL IMPUESTO CREE"
Dim StActi As String    'OMOG R16934/T19113 VARIABLE QUE GUARDA LA ACTIVIDAD ECONOMICA
Dim DbImpuesto As Double 'OMOG T15675 VARIABLE QUE TRAE EL VALOR DEL IMPUESTO CUANDO SE VA A REALIZAR UNA DEVOLUCION ENTRADA
Dim DbDescuento As Double 'EACT T15675 VARIABLE QUE TRAE EL VALOR DEL DESCUENTO CUANDO SE VA A REALIZAR UNA DEVOLUCION ENTRADA
'JLPB T15675 INICIO
Dim ArDescComp() As Variant 'ALMACENA LOS DESCUENTOS
Dim ArImpComp() As Variant 'ALMACENA LOS IMPUESTOS
Dim InPosc As Integer
'JLPB T37799 INICIO Se deja en comentario las siguientes lineas
'Dim InCantT As Integer 'ALMACENA LA CANTIDAD DE ARTICULOS CON LOS CUALES SE REALIZO LA ENTRADA
'Dim InCantD As Integer 'ALMACENA LA CANTIDAD DE ARTICULOS DEVUELTOS
'Dim InCantET As Integer 'ALMACENA LA CANTIDAD DE ARTICULOS RESTANTES
Dim DbCantT As Double 'ALMACENA LA CANTIDAD DE ARTICULOS CON LOS CUALES SE REALIZO LA ENTRADA
Dim DbCantD As Double 'ALMACENA LA CANTIDAD DE ARTICULOS DEVUELTOS
Dim DbCantET As Double 'ALMACENA LA CANTIDAD DE ARTICULOS RESTANTES
'JLPB T37799 FIN
'JLPB T15675 FIN
'JLPB T26186 inicio
Dim StCodMod As String 'ALMACENA EL CODIGO DEL MODULO DEL DOCUMENTO GENERADO
Dim StConsMov As String 'ALMACENA EL NUMERO DEL MOVIMIENTO GENERADO
'JLPB T26186 fin
'Dim TxPosicion As String 'JAUM T28370-R22535 Almacena la posicion en la cual esta la CxP hija JAUM T29173 Se deja linea en comentario
Dim StPosicion As String 'JAUM T29173 Almacena la posicion en la cual esta la CxP hija
'DRMG T44728-R44128 INICIO
Dim VrArr() As Variant 'ALMACENA LOS RESULTADOS SE CONSULTAS SQL
Dim cQrCode As ClsQrCode
Dim StRutaPDF As String 'Almacena la ruta del PDF
'Public WithEvents poSendMail As vbSendMail.clsSendMail 'Objeto de la clase clsSendMail para generar el envio del correo 'DRMG T45220 SE DEJA EN COMENTARIO
'DRMG T44728-R44128 FIN

'---------------------------------------------------------------------------------------
' Procedure : Actualiza_CxP_Mul
' DateTime  : 03/07/2015 11:35
' Author    : juan_urrego
' Purpose   : T28370-R22535 Actualiza la CxP cuando es por concepto multiple
'---------------------------------------------------------------------------------------
'
'Private Function Actualiza_CxP_Mul(ByVal LnNumero As Long, DbNumCxP As Double, DbValCxP As Double, Optional DbVNDeb As Double, Optional BoTComp As Boolean) As Integer 'JAUM T28836 Se deja linea en comentario
Private Function Actualiza_CxP_Mul(ByVal LnNumero As Long, DbNumCxP As Double, DbValCxP As Double, Optional DbVNDeb As Double, Optional BoTComp As Boolean, Optional BoMSaldo As Boolean) As Integer 'JAUM T28836
   Dim VrHijas() As Variant  'Almacena el concepto numero y valor de las cuentas hijas
   Dim InI, InJ As Integer  'Contador
   Dim VrTotal() As Variant 'Almacena el valor total que se guardara por cada una de las entradas
   'JAUM T28836 Inicio
   Dim VrDescImp() As Variant 'Almacena los descuentos e impuestos
   Dim dbDesc  As Double 'Almacena el valor total de los decuentos
   Dim DbImp As Double 'Almacena el valor total de los impuestos
   Dim InA As Integer 'Contador
   Dim DbTotal As Double 'Almacena el valor total
   'JAUM T28836 Fin
   'Crea la relaci�n entre la nota cr�dito y las CXP
   Valores = "CD_NDEB_NDCP = " & LnNumero
   Valores = Valores & Coma & "CD_CONC_NDCP = 'CINM'"
   Valores = Valores & Coma & "CD_NUME_NDCP = " & CDbl(DbNumCxP)
   Valores = Valores & Coma & "VL_SALD_NDCP = " & CDbl(DbValCxP)
   If BoTComp Then
      Valores = Valores & Coma & "VL_NDEB_NDCP = " & CDbl(DbVNDeb)
   Else
      If DbImpuesto > 0 Or DbDescuento > 0 Then
         Valores = Valores & Coma & "VL_NDEB_NDCP = " & CDbl(Me.TxtCompra(17).Text) - DbImpuesto - DbDescuento
      Else
         Valores = Valores & Coma & "VL_NDEB_NDCP = " & CDbl(Me.TxtCompra(17).Text)
      End If
   End If
   Result = DoInsertSQL("R_NDEB_CXP", Valores)
   'JAUM T28836 Inicio se deja bloque en comentario
   'If Result <> FAIL Then
      'Actualiza el valor de las notas d�bito de la CXP
      'Condicion = "CD_CONCE_CXP='CINM' AND CD_NUME_CXP=" & CDbl(DbNumCxP)
      'If BoTComp Then
         'Result = DoUpdate("C_X_P", "VL_NDEB_CXP=VL_NDEB_CXP + " & CDbl(DbVNDeb), Condicion)
      'Else
         'If DbImpuesto > 0 Or DbDescuento = 0 Then
            'Result = DoUpdate("C_X_P", "VL_NDEB_CXP=VL_NDEB_CXP + " & CDbl(Me.TxtCompra(17).Text - DbImpuesto - DbDescuento), Condicion) 'EACT T15675
         'Else
            'Result = DoUpdate("C_X_P", "VL_NDEB_CXP=VL_NDEB_CXP + " & CDbl(Me.TxtCompra(17).Text), Condicion)
         'End If
      'End If
   'End If
   'JAUM T28836 Fin
   'Suma el valor de los articulos que pertenece a cada una de las entradas
   'ReDim VrTotal(0) JAUM T28836 Se deja linea en comentario
   ReDim VrTotal(2, 0) 'JAUM T28836
   With GrdArticulos
      InJ = 0
      If .Rows <> 2 Then
         For InI = 1 To .Rows - 1
            If InJ <> 0 Then
               'If VrTotal(InJ - 1) <> "" Then ReDim Preserve VrTotal(InJ) JAUM T28836 Se deja linea en comentario
               If VrTotal(0, InJ - 1) <> "" Then ReDim Preserve VrTotal(2, InJ) 'JAUM T28836
            End If
            'JAUM T28836 Inicio se deja linea en comentario
            'VrTotal(InJ) = CDbl(.TextMatrix(InI, 15))
            VrTotal(0, InJ) = CDbl(.TextMatrix(InI, 15))
            VrTotal(1, InJ) = InJ
            VrTotal(2, InJ) = .TextMatrix(InI, 24)
            'JAUM T28836 Fin
            If InI <> .Rows - 1 Then
               Do While .TextMatrix(InI, 24) = .TextMatrix(InI + 1, 24)
                  'VrTotal(InJ) = VrTotal(InJ) + CDbl(.TextMatrix(InI + 1, 15)) JAUM T28836 Se deja linea en comentario
                  VrTotal(0, InJ) = VrTotal(0, InJ) + CDbl(.TextMatrix(InI + 1, 15)) 'JAUM T28836
                  InI = InI + 1
                  If InI = .Rows - 1 Then Exit Do
               Loop
            End If
            InJ = InJ + 1
         Next
      Else
         'JAUM T29255 Inicio se deja linea en comentario
         'VrTotal(InJ) = CDbl(.TextMatrix(1, 15))
         VrTotal(0, InJ) = CDbl(.TextMatrix(1, 15))
         VrTotal(1, InJ) = InJ
         VrTotal(2, InJ) = .TextMatrix(1, 24)
         'JAUM T29255 Fin
      End If
   End With
   'JAUM T28836 Inicio
   For InJ = 0 To UBound(VrTotal, 2)
      ReDim VrDescImp(5, 0)
      dbDesc = 0
      DbImp = 0
      Desde = "DESCUENTO INNER JOIN R_CPCP_DESC ON CD_CODI_DESC=CD_DESC_RCPDE INNER JOIN R_CXP_CXP ON CD_NUME_RCPCP=CD_NUME_RCPDE"
      Campos = "CD_CODI_DESC,PR_PORC_DESC,VL_TOPE_DESC,CD_CUEN_DESC,VL_VALO_DESC,TX_TIPO_DESC"
      Condicion = "CD_NUME_CXP_RCPCP = " & DbNumCxP & " AND NU_POSI_RCPCP = " & VrTotal(1, InJ) + 1
      Condicion = Condicion & " UNION SELECT CD_CODI_DESC,PR_PORC_DESC,VL_TOPE_DESC,CD_CUEN_DESC,VL_VALO_DESC,TX_TIPO_DESC "
      Condicion = Condicion & "FROM IN_R_ENCA_IMDE INNER JOIN DESCUENTO ON CD_CODI_DESC_ENID=CD_CODI_DESC "
      Condicion = Condicion & "WHERE NU_AUTO_ENCAREAL_ENID = " & VrTotal(2, InJ)
      Result = LoadMulData(Desde, Campos, Condicion, VrDescImp)
      If Result <> FAIL And Encontro Then
         For InI = 0 To UBound(VrDescImp, 2)
            If CDbl(VrDescImp(2, InI)) < CDbl(VrTotal(0, InJ)) Then
               If Val(VrDescImp(1, InI)) = 0 Then
                  If BoAproxCent Then
                     dbDesc = dbDesc + AproxCentena(Round(VrDescImp(4, InI)))
                  Else
                     dbDesc = dbDesc + Aplicacion.Formatear_Valor(Val(VrDescImp(4, InI)))
                  End If
               Else
                  If BoAproxCent Then
                     dbDesc = dbDesc + AproxCentena(Round(VrTotal(0, InJ) * VrDescImp(1, InI)) / 100)
                  Else
                     dbDesc = dbDesc + Aplicacion.Formatear_Valor(Val((VrTotal(0, InJ) * (VrDescImp(1, InI) / 100))))
                  End If
               End If
            End If
         Next
      End If
      Desde = "TC_IMPUESTOS INNER JOIN R_CPCP_IMPU ON CD_CODI_IMPU=CD_IMPU_RCPIM INNER JOIN R_CXP_CXP ON CD_NUME_RCPCP=CD_NUME_RCPIM"
      Campos = "CD_CODI_IMPU,PR_PORC_IMPU,VL_TOPE_IMPU,CD_CUEN_IMPU,ID_TIPO_IMPU"
      Condicion = "CD_NUME_CXP_RCPCP = " & DbNumCxP & " AND ID_TIPO_IMPU<>'I' AND NU_POSI_RCPCP = " & VrTotal(1, InJ) + 1
      Condicion = Condicion & " UNION SELECT CD_CODI_IMPU,PR_PORC_IMPU,VL_TOPE_IMPU,CD_CUEN_IMPU,ID_TIPO_IMPU "
      Condicion = Condicion & "FROM IN_R_ENCA_IMDE INNER JOIN TC_IMPUESTOS ON CD_CODI_IMPU_ENID=CD_CODI_IMPU "
      Condicion = Condicion & "WHERE NU_AUTO_ENCAREAL_ENID = " & VrTotal(2, InJ)
      Result = LoadMulData(Desde, Campos, Condicion, VrDescImp)
      If Result <> FAIL And Encontro Then
         For InI = 0 To UBound(VrDescImp, 2)
            If CDbl(VrDescImp(2, InI)) < CDbl(VrTotal(0, InJ)) Then
               If VrDescImp(4, InI) = "C" Then
                  If BoAproxCent Then
                     DbImp = DbImp + AproxCentena(Round((VrTotal(0, InJ) - dbDesc) * VrDescImp(1, InI)) / 100)
                  Else
                     DbImp = DbImp + Aplicacion.Formatear_Valor(Val((VrTotal(0, InJ) - dbDesc) * (VrDescImp(1, InI) / 100)))
                  End If
               ElseIf VrDescImp(4, InI) = "M" Then
                  If BoAproxCent Then
                     DbImp = DbImp + AproxCentena(Round(VrTotal(0, InJ) * VrDescImp(1, InI)) / 100)
                  Else
                     DbImp = DbImp + Aplicacion.Formatear_Valor(Val((VrTotal(0, InJ) * (VrDescImp(1, InI) / 100))))
                  End If
               ElseIf VrDescImp(4, InI) = "R" Then
                  If BoAproxCent Then
                     DbImp = DbImp + AproxCentena(Round((VrTotal(0, InJ) - dbDesc) * VrDescImp(1, InI)) / 100)
                  Else
                     DbImp = DbImp + Aplicacion.Formatear_Valor(Val((VrTotal(0, InJ) - dbDesc) * (VrDescImp(1, InI) / 100)))
                  End If
               End If
            End If
         Next
      End If
      
      VrTotal(0, InJ) = VrTotal(0, InJ) - dbDesc - DbImp
   Next
   'JAUM T28836 Fin
   ReDim VrHijas(0, 0)
   'TxPosicion = "" JAUM T29173 Se deja linea en comentario
   StPosicion = "" 'JAUM T29173
   Condicion = "NU_CXP_ENCA = " & DbNumCxP
   Result = LoadMulData("IN_ENCABEZADO", "NU_AUTO_ENCA", Condicion, VrHijas)
   If Result <> FAIL And Encontro Then
      For InI = 1 To GrdArticulos.Rows - 1
         For InJ = 0 To UBound(VrHijas, 2)
            If Format(GrdArticulos.TextMatrix(InI, 24), "##") = VrHijas(0, InJ) Then
               If InI = 1 Then
                  'TxPosicion = InJ + 1 JAUM T29173 Se deja linea en comentario
                  StPosicion = InJ + 1 'JAUM T29173
                  If InI <> GrdArticulos.Rows - 1 Then
                     Do While GrdArticulos.TextMatrix(InI, 24) = GrdArticulos.TextMatrix(InI + 1, 24)
                        InI = InI + 1
                        If InI = GrdArticulos.Rows - 1 Then Exit Do
                     Loop
                  End If
                  GoTo Siguiente
               Else
                  'TxPosicion = TxPosicion & Coma & InJ + 1 JAUM T29173 Se deja linea en comentario
                  StPosicion = StPosicion & Coma & InJ + 1 'JAUM T29173
                  If InI <> GrdArticulos.Rows - 1 Then
                     Do While GrdArticulos.TextMatrix(InI, 24) = GrdArticulos.TextMatrix(InI + 1, 24)
                        InI = InI + 1
                        If InI = GrdArticulos.Rows - 1 Then Exit Do
                     Loop
                  End If
                  GoTo Siguiente
               End If
            End If
         Next
Siguiente:
      Next
   End If
   'Actualiza las CxP hijas
   'reDim VrHijas(3, 0) JAUM T28836 Se deja linea en comentario
   ReDim VrHijas(6, 0) 'JAUM T28836
   'Condicion = "CD_NUME_CXP_RCPCP = " & DbNumCxP & " AND NU_POSI_RCPCP in (" & TxPosicion & ")" JAUM T29173 Se deja linea en comentario
   Condicion = "CD_NUME_CXP_RCPCP = " & DbNumCxP & " AND NU_POSI_RCPCP in (" & StPosicion & ")" 'JAUM T29173
   'JAUM T28836 Inicio se deja linea en comentario
   'Result = LoadMulData("R_CXP_CXP", "CD_CONCE_RCPCP, CD_NUME_RCPCP, VL_NETO_RCPCP", Condicion, VrHijas)
   Campos = "CD_CONCE_RCPCP, CD_NUME_RCPCP, VL_NETO_RCPCP, VL_NDEB_RCPCP, VL_DESC_RCPCP, VL_IMPU_RCPCP"
   Result = LoadMulData("R_CXP_CXP", Campos, Condicion, VrHijas)
   If BoMSaldo Then
      DbTotal = 0
      For InI = 0 To UBound(VrTotal, 2)
         DbTotal = DbTotal + VrTotal(0, InI)
      Next
      If Aplicacion.Formatear_Valor(DbVNDeb) > Aplicacion.Formatear_Valor(DbTotal) Then
         VrTotal(0, 0) = VrTotal(0, 0) + (DbVNDeb - DbTotal)
         VrTotal(0, 0) = Format(VrTotal(0, 0), "00.00")
      Else
         VrTotal(0, 0) = VrTotal(0, 0) + (DbTotal - DbVNDeb)
         VrTotal(0, 0) = Format(VrTotal(0, 0), "00.00")
      End If
   Else
      DbVNDeb = 0
   End If
   DbValCxP = 0
   'JAUM T28836 Fin
   For InI = 0 To UBound(VrHijas, 2)
      'JAUM T28836 Inicio
      If InI <> 0 Then ReDim Preserve VrHijas(6, InI)
      VrHijas(6, InI) = VrHijas(2, InI) - VrHijas(3, InI) - VrHijas(4, InI) - VrHijas(5, InI)
      Valores = "CD_NDEB_NDCPH = " & LnNumero & Coma
      Valores = Valores & " CD_CONC_NDCPH = '" & VrHijas(0, InI) & Comi & Coma
      Valores = Valores & " CD_NUME_NDCPH = " & VrHijas(1, InI) & Coma
      Valores = Valores & " CD_CXPPAD_NDCPH = " & DbNumCxP & Coma
      'JAUM T28836 Inicio se dejan lineas en comentario
      'Valores = Valores & " VL_SALD_NDCPH = " & VrHijas(2, InI) & Coma
      'Valores = Valores & " VL_NDEB_NDCPH= " & VrTotal(InI)
      Valores = Valores & " VL_SALD_NDCPH = " & VrHijas(6, InI) & Coma
      Valores = Valores & " VL_NDEB_NDCPH= " & VrTotal(0, InI)
      If Not BoMSaldo Then DbVNDeb = DbVNDeb + CDbl(VrTotal(0, InI))
      DbValCxP = DbValCxP + CDbl(VrHijas(6, InI))
      'JAUM T28836 Fin
      Result = DoInsertSQL("R_NDEB_CPCP", Valores)
      If Result <> FAIL Then
         Condicion = "CD_NUME_RCPCP = " & VrHijas(1, InI) & " AND CD_NUME_CXP_RCPCP = " & DbNumCxP
         'Valores = "VL_NDEB_RCPCP = VL_NDEB_RCPCP + " & VrTotal(InI) JAUM T28836 Se deja linea en comentario
         Valores = "VL_NDEB_RCPCP = VL_NDEB_RCPCP + " & VrTotal(0, InI) 'JAUM T28836
         Result = DoUpdate("R_CXP_CXP", Valores, Condicion)
         If Result = FAIL Then Exit For
      Else
         Exit For
      End If
   Next
   'JAUM T28836 Inicio
   If Result <> FAIL Then
      Valores = "VL_SALD_NDCP = " & DbValCxP & " ,VL_NDEB_NDCP = " & DbVNDeb
      Condicion = "CD_NDEB_NDCP = " & LnNumero
      Condicion = Condicion & " AND CD_CONC_NDCP = 'CINM'"
      Condicion = Condicion & " AND CD_NUME_NDCP = " & CDbl(DbNumCxP)
      Result = DoUpdate("R_NDEB_CXP", Valores, Condicion)
      If Result <> FAIL Then
         Valores = "VL_VALO_NDEB = " & DbVNDeb
         Valores = Valores & Coma & "DE_MONT_NDEB = " & Comi & Monto_Escrito(CStr(DbVNDeb)) & Comi
         Condicion = "CD_CODI_NDEB = " & LnNumero
         Result = DoUpdate("NOTA_DEBITO_P", Valores, Condicion)
      End If
      If Result <> FAIL Then
         'Actualiza el valor de las notas d�bito de la CXP
         Condicion = "CD_CONCE_CXP='CINM' AND CD_NUME_CXP=" & CDbl(DbNumCxP)
         If BoTComp Then
            Result = DoUpdate("C_X_P", "VL_NDEB_CXP=VL_NDEB_CXP + " & CDbl(DbVNDeb), Condicion)
         Else
            If DbImpuesto > 0 Or DbDescuento = 0 Then
               Result = DoUpdate("C_X_P", "VL_NDEB_CXP=VL_NDEB_CXP + " & CDbl(Me.TxtCompra(17).Text - DbImpuesto - DbDescuento), Condicion) 'EACT T15675
            Else
               Result = DoUpdate("C_X_P", "VL_NDEB_CXP=VL_NDEB_CXP + " & CDbl(Me.TxtCompra(17).Text), Condicion)
            End If
         End If
      End If
   End If
   'JAUM T28836 Fin
   Actualiza_CxP_Mul = Result
End Function

Function fnBodegaCont(StAutoBodega As String) As Boolean '|.DR.|
'Retorna verdadero si la bodega debe hacer transferencia a contabilidad.
'Retorna falso si la bodega no debe hacer transferencia o si el c�digo no se encontr�
'El par�metro debe ser el autonum�rico de la bodega.

Dim arrT(0)
    
    Condicion = "NU_AUTO_BODE=" & StAutoBodega
    Result = LoadData("IN_BODEGA", "TX_CONTA_BODE", Condicion, arrT())
    fnBodegaCont = (arrT(0) = "S") And (StAutoBodega <> "") And Encontro And Result <> FAIL

End Function

Private Sub cboBodDestino_LostFocus()
    cboBodega_LostFocus '|.DR.|, R:1633
    'AASV M6021 Inicio
    If cboBodDestino.ListIndex <> -1 And cboBodega.ListIndex <> -1 And Documento.Encabezado.TipDeDcmnt = Traslado Then
        If Not Validar_Bodega_VentaConsumo(cboBodDestino.ItemData(cboBodDestino.ListIndex), cboBodega.ItemData(cboBodega.ListIndex)) Then
            Call Mensaje1("No se pueden hacer traslados entre bodegas con diferente tipo de uso, seleccione otra bodega destino", 3)
            cboBodDestino.ListIndex = -1
            cboBodDestino.SetFocus
        End If
    End If
    'AASV M6021 Fin
End Sub
'---------------------------------------------------------------------------------------
' Procedure : Validar_Bodega_VentaConsumo
' DateTime  : 01/12/2009 11:03
' Author    : albert_silva
' Purpose   : M6021 Valida que las bodegas sean del mismo tipo (Consumo, Venta)
'---------------------------------------------------------------------------------------
'
Function Validar_Bodega_VentaConsumo(StAutoBodegaD As String, StAutoBodegaO As String) As Boolean
Dim ArBD(0) As Variant
Dim ArBO(0) As Variant
    Condicion = "NU_AUTO_BODE=" & StAutoBodegaO
    Result = LoadData("IN_BODEGA", "TX_VENTA_BODE", Condicion, ArBO())
    If Result <> FAIL And Encontro Then
        Condicion = "NU_AUTO_BODE=" & StAutoBodegaD
        Result = LoadData("IN_BODEGA", "TX_VENTA_BODE", Condicion, ArBD())
        If Result <> FAIL And Encontro Then
            If ArBO(0) = ArBD(0) Then Validar_Bodega_VentaConsumo = True
        End If
    End If
    
End Function

Private Sub cboBodega_LostFocus()
    '00 |.DR.|, R:1633
    If cboBodDestino.Visible And cboBodDestino.ListIndex > -1 Then
        'If cboBodDestino.ListIndex = -1 Then Exit Sub 'JACC R2353
        If cboBodega.ListIndex = -1 Then Exit Sub 'JACC R2353
        imgNoCont.Visible = Not (fnBodegaCont(cboBodDestino.ItemData(cboBodDestino.ListIndex)) And fnBodegaCont(cboBodega.ItemData(cboBodega.ListIndex))) And Aplicacion.Interfaz_Contabilidad
    ElseIf cboBodega.ListIndex > -1 Then
        imgNoCont.Visible = Not fnBodegaCont(cboBodega.ItemData(cboBodega.ListIndex)) And Aplicacion.Interfaz_Contabilidad
    Else
        imgNoCont.Visible = False
    End If
        FrmImpuestos.Enabled = Not imgNoCont.Visible
        lblNoCont.Visible = imgNoCont.Visible
        If lblNoCont.Visible Then Mensaje1 "La bodega '" & cboBodega & "', no afecta contabilidad (movimientos contables en 0).", 2
    '00 Fin
End Sub

Private Sub chaSALDOS_SeriesSelected(Series As Integer, MouseFlags As Integer, Cancel As Integer)
    Me.chaSALDOS.Column = Series
    Me.chaSALDOS.Row = 1
    Me.staINFORMA.Panels("SALBOD").Text = Me.chaSALDOS.ColumnLabel
    Me.staINFORMA.Panels("SLDSEL").Text = Aplicacion.Formatear_Cantidad(Me.chaSALDOS.Data)
    Me.chaSALDOS.DataGrid.RowLabel(1, 1) = Trim(Me.GrdArticulos.TextMatrix(Me.FilaDeGrillaActiva, Me.ColNombreArticulo))
    Me.chaSALDOS.DataGrid.RowLabel(1, 2) = Me.staINFORMA.Panels("SALBOD").Text & ":" & Me.staINFORMA.Panels("SLDSEL").Text
End Sub

Private Sub cmbConcepto_Click()
        If cmbConcepto.ListIndex = cmbConcepto.ListCount - 1 Then cmbConcepto.ListIndex = -1 '|.DR.|, R:1631
        TxtEntra(7) = 0: TxtEntra(8) = 0: TxtEntra(16) = 0: TxtEntra(17) = 0 '|.DR.|, M:1091
        Call ImpuestosDescuentos '|.DR.|, R:1631
End Sub

Private Sub cmbDEPENDENCIA_LostFocus()
    Me.cmdPROCEDER.Item(8).Enabled = False
    If Not Me.cmbDEPENDENCIA.ListIndex >= 0 Then Exit Sub
    Me.cmdPROCEDER.Item(8).Enabled = True
    Me.TxtBajaD(4).Text = arrDPDCS(Me.cmbDEPENDENCIA.ListIndex)
End Sub

Private Sub Cmd_agregar_Click()
Dim texto As String
Dim cadena As String
Dim Cadena2 As String
Dim aux As String

   With GrdDeIm
      Call MouseClock
'      If Not TrvLista.SelectedItem.Parent Is Nothing Then
      If Instanciado(TrvLista.SelectedItem) = False Then MouseNorm: Exit Sub 'hmc 11\11\2003
      If Instanciado(TrvLista.SelectedItem.Parent) Then 'hmc 29\10\2003
         texto = TrvLista.SelectedItem.Key
         cadena = Left(texto, IIf(InStr(1, texto, "|") - 1 < 1, 0, InStr(1, texto, "|") - 1))
         If TrvLista.SelectedItem.Parent = "Descuentos" Then
            If BuscarItem(cadena, 0) = False Then
               cadena = Right(texto, Len(texto) - InStr(1, texto, "|"))
               If Mid(TrvLista.SelectedItem.Tag, 1, 1) = "P" Then
                  aux = Replace(TrvLista.SelectedItem.Tag, "P|", "")
                  aux = Mid(aux, 1, IIf(InStr(1, aux, "|") - 1 < 0, Len(aux), InStr(1, aux, "|") - 1))
                  Cadena2 = Left(TrvLista.SelectedItem.Key, InStr(1, TrvLista.SelectedItem.Key, "|") - 1) 'codigo
                  Cadena2 = Cadena2 & vbTab & "D"
                  Cadena2 = Cadena2 & vbTab & cadena 'Descripcion
                  Cadena2 = Cadena2 & vbTab & aux 'porcentaje
                  Cadena2 = Cadena2 & vbTab & Round(((CDbl(TxtEntra(10)) * aux) / 100), 2)  'valor 'DARS, M:1821
                  Cadena2 = Cadena2 & vbTab & "R" 'Suma - Resta
                  Cadena2 = Cadena2 & vbTab & "P" 'Tipo
                  aux = Replace(TrvLista.SelectedItem.Tag, "P|", "")
                  If InStr(1, aux, "|") = 0 Then
                    aux = 0
                  Else
                    aux = Mid(aux, InStr(1, aux, "|") + 1, Len(aux))
                  End If
                  Cadena2 = Cadena2 & vbTab & DecDouble(aux) 'Tope
                  If Round(CDbl(aux)) > Round(CDbl(TxtEntra(10))) Then Call Mensaje1(" El Descuento seleccionado aplica a partir de $" & Round(CDbl(aux)) & ". Revise el valor de la cuenta", 3): Call MouseNorm: Exit Sub 'smdl m1663
                  Cadena2 = Cadena2 & vbTab & "D"
                  .AddItem Cadena2
'                  .TextMatrix(.Rows - 1, 4) = Format(GrdDeIm.TextMatrix(GrdDeIm.Rows - 1, 4), "#,###.#000") '|.DR.|, M:1650
                  .TextMatrix(.Rows - 1, 4) = Aplicacion.Formatear_Valor(GrdDeIm.TextMatrix(GrdDeIm.Rows - 1, 4))   'REOL M2258
                  
                  If BoAproxCent = True Then .TextMatrix(.Rows - 1, 4) = AproxCentena(Round(.TextMatrix(.Rows - 1, 4))) 'GAVL T6423
                  
                  TxtEntra(7) = Val(TxtEntra(7)) + Val(GrdDeIm.TextMatrix(GrdDeIm.Rows - 1, 3))
               Else
                  aux = Replace(TrvLista.SelectedItem.Tag, "V|", "")
                  aux = Mid(aux, 1, IIf(InStr(1, aux, "|") - 1 < 0, Len(aux), InStr(1, aux, "|") - 1))
                  Cadena2 = Left(TrvLista.SelectedItem.Key, InStr(1, TrvLista.SelectedItem.Key, "|") - 1) 'codigo
                  Cadena2 = Cadena2 & vbTab & "D"
                  Cadena2 = Cadena2 & vbTab & cadena 'Descripcion
                  Cadena2 = Cadena2 & vbTab & "0" 'porcentaje
                  Cadena2 = Cadena2 & vbTab & aux 'valor
                  Cadena2 = Cadena2 & vbTab & "R" 'Suma - Resta
                  Cadena2 = Cadena2 & vbTab & "V" 'Tipo
                  aux = Replace(TrvLista.SelectedItem.Tag, "V|", "")
                  If InStr(1, aux, "|") = 0 Then
                    aux = 0
                  Else
                    aux = Mid(aux, InStr(1, aux, "|") + 1, Len(aux))
                  End If
                  Cadena2 = Cadena2 & vbTab & DecDouble(aux) 'Tope
                  If Round(CDbl(aux)) > Round(CDbl(TxtEntra(10))) Then Call Mensaje1(" El Descuento seleccionado aplica a partir de $" & Round(CDbl(aux)) & ". Revise el valor de la cuenta", 3): Call MouseNorm: Exit Sub 'smdl m1663
                  Cadena2 = Cadena2 & vbTab & "D"
                  .AddItem Cadena2
                  .TextMatrix(.Rows - 1, 4) = Format(GrdDeIm.TextMatrix(GrdDeIm.Rows - 1, 4), "#,###.#000") '|.DR.|, M:1650
                  
                  If BoAproxCent = True Then .TextMatrix(.Rows - 1, 4) = AproxCentena(Round(.TextMatrix(.Rows - 1, 4))) 'GAVL T6423
                  
                  'TxtEntra(11) = CDbl(TxtEntra(11)) + CDbl(GrdDeIm.TextMatrix(GrdDeIm.Rows - 1, 4))
'                  TxtEntra(7) = Format(CDec(TxtEntra(7)) + (CDec((GrdDeIm.TextMatrix(GrdDeIm.Rows - 1, 4))) * 100) / CDec((TxtEntra(10))), "#,###.#000") 'smdl m1752
                  TxtEntra(7) = Aplicacion.Formatear_Valor(CDec(TxtEntra(7)) + (CDec((GrdDeIm.TextMatrix(GrdDeIm.Rows - 1, 4))) * 100) / CDec((TxtEntra(10))))      'REOL M2258
               End If
            End If
            
         Else
            If BuscarItem(cadena, 1) = False Then
               aux = Mid(TrvLista.SelectedItem.Tag, 1, InStr(1, TrvLista.SelectedItem.Tag, "|") - 1)
               cadena = Right(texto, Len(texto) - InStr(1, texto, "|"))
               Cadena2 = vbTab & Left(TrvLista.SelectedItem.Key, InStr(1, TrvLista.SelectedItem.Key, "|") - 1) 'codigo
               Cadena2 = Cadena2 & vbTab & cadena 'descripcion
               Cadena2 = Cadena2 & vbTab & aux 'Porcentaje
               If Mid(texto, 1, 1) = "M" Then
                  Cadena2 = Cadena2 & vbTab & (CDbl(txtIMPUE) * aux) / 100  'valor
               Else
'                  Cadena2 = Cadena2 & vbTab & (CDbl(TxtEntra(10)) * aux) / 100 'valor
                  Cadena2 = Cadena2 & vbTab & (CDbl(TxtEntra(10) - CDbl(TxtEntra(11))) * aux) / 100 'valor 'REOL1298  Bruto - Descuento
               End If
               Cadena2 = Cadena2 & vbTab & "R" 'Suma - Resta
               Cadena2 = Cadena2 & vbTab & "P" 'Tipo
               aux = Mid(TrvLista.SelectedItem.Tag, InStr(1, TrvLista.SelectedItem.Tag, "|") + 1, Len(TrvLista.SelectedItem.Tag))
               Cadena2 = Cadena2 & vbTab & DecDouble(aux) 'tope
               If Round(CDbl(aux)) > Round(CDbl(TxtEntra(10))) Then Call Mensaje1(" El impuesto seleccionado aplica a partir de $" & Round(CDbl(aux)) & ". Revise el valor de la cuenta", 3): Call MouseNorm: Exit Sub 'smdl m1663
               Cadena2 = Cadena2 & vbTab & "I"
               .AddItem Cadena2
'               .TextMatrix(.Rows - 1, 4) = Format(GrdDeIm.TextMatrix(GrdDeIm.Rows - 1, 4), "#,###.#000") '|.DR.|, M:1650
               .TextMatrix(.Rows - 1, 4) = Aplicacion.Formatear_Valor(GrdDeIm.TextMatrix(GrdDeIm.Rows - 1, 4))      'REOL M2258
               
               If BoAproxCent = True Then .TextMatrix(.Rows - 1, 4) = AproxCentena(Round(.TextMatrix(.Rows - 1, 4))) 'GAVL T6423
               
               If Mid(texto, 1, 1) = "R" Then TxtEntra(8) = Val(TxtEntra(8)) + Val(GrdDeIm.TextMatrix(GrdDeIm.Rows - 1, 3))
               If Mid(texto, 1, 1) = "C" Then TxtEntra(17) = Val(TxtEntra(17)) + Val(GrdDeIm.TextMatrix(GrdDeIm.Rows - 1, 3))
               If Mid(texto, 1, 1) = "M" Then TxtEntra(16) = Val(TxtEntra(16)) + Val(GrdDeIm.TextMatrix(GrdDeIm.Rows - 1, 3))
            End If
         End If
      End If
      Call MouseNorm
   End With
   
  Dim i As Variant
'  If GrdDeIm.Rows > 1 Then
'      For i = 1 To GrdDeIm.Rows - 1
'         GrdDeIm.Row = i
'         GrdDeIm.Col = 3 ' Para que revise el valor base
'         GrdDeIm.Col = 4
'         GrdDeIm.Col = 3 ' Para que revise el valor base
'      Next
'  End If
   
  Call calcular_entra(True)
  
  'PJCA M1787: Se debe revisar los impuestos ya aplicados para restar los descuentos comerciales que se apliquen despues
  Dim vrMiValor As Double
  If GrdDeIm.Rows > 1 Then
      For i = 1 To GrdDeIm.Rows - 1
        If GrdDeIm.TextMatrix(i, 8) = "I" Then
            If GrdDeIm.TextMatrix(i, 4) = NUL$ Then GrdDeIm.TextMatrix(i, 4) = 0
            If Not IsNumeric(GrdDeIm.TextMatrix(i, 4)) Then GrdDeIm.TextMatrix(i, 4) = 0
            If CDbl(TxtEntra(10)) = 0 Then
               vrMiValor = 0
            Else
               If Mid(GrdDeIm.TextMatrix(i, 1), 1, 1) = "M" Then    'Impuestos tipo ReteIVA
                  vrMiValor = (GrdDeIm.TextMatrix(i, 3) * CDbl(txtIMPUE)) / 100
               Else
                  'vrMiValor = (GrdDeIm.TextMatrix(i, 3) * (CDbl(TxtEntra(10)) - CDbl(TxtEntra(11)))) / 100
                  Dim Costo As Double                                                                       'PJCA M1787
                  Costo = CDbl(TxtEntra(10)) - CDbl(TxtEntra(11))                                            'PJCA M1787
                  vrMiValor = Aplicacion.Formatear_Valor((CSng(GrdDeIm.TextMatrix(i, 3)) * Costo) / 100)    'PJCA M1787
               End If
            End If
            GrdDeIm.TextMatrix(i, 4) = Aplicacion.Formatear_Valor(vrMiValor)
            'GrdDeIm.TextMatrix(i, 4) = Format(GrdDeIm.TextMatrix(i, 4), "#,###.#0")
            If BoAproxCent = True Then GrdDeIm.TextMatrix(i, 4) = AproxCentena(Round(GrdDeIm.TextMatrix(i, 4))) 'GAVL T6423
        End If
      Next
  End If
  'PJCA M1787
  
End Sub

Private Sub Cmd_Quitar_Click()
Dim i
Dim texto As String
   
   If GrdDeIm.Rows - 1 < GrdDeIm.Row Then Exit Sub '|.DR.|
   texto = GrdDeIm.TextMatrix(GrdDeIm.Row, 1)
   'smdl m1725 se adiciona la comparacion si es porcentaje o valor para que borre el dto.
   'If Mid(texto, 1, 1) = "D"  Then TxtEntra(7) = val(TxtEntra(7)) - val(GrdDeIm.TextMatrix(GrdDeIm.Row, 3))
   If Mid(texto, 1, 1) = "D" And GrdDeIm.TextMatrix(GrdDeIm.Row, 6) = "P" Then TxtEntra(7) = Val(TxtEntra(7)) - Val(GrdDeIm.TextMatrix(GrdDeIm.Row, 3))
   If Mid(texto, 1, 1) = "D" And GrdDeIm.TextMatrix(GrdDeIm.Row, 6) = "V" Then TxtEntra(7) = Format(CDec(TxtEntra(7)) - (CDec((GrdDeIm.TextMatrix(GrdDeIm.Rows - 1, 4))) * 100) / CDec((TxtEntra(10))), "#,###.#000") 'smdl m1752 borre el dto por valor.
   'smdl m1725
   If Mid(texto, 1, 1) = "R" Then TxtEntra(8) = Val(TxtEntra(8)) - Val(GrdDeIm.TextMatrix(GrdDeIm.Row, 3))
   If Mid(texto, 1, 1) = "C" Then TxtEntra(17) = Val(TxtEntra(17)) - Val(GrdDeIm.TextMatrix(GrdDeIm.Row, 3))
   If Mid(texto, 1, 1) = "M" Then TxtEntra(16) = Val(TxtEntra(16)) - Val(GrdDeIm.TextMatrix(GrdDeIm.Row, 3))
   
   If GrdDeIm.Rows = 2 Then
      For i = 0 To GrdDeIm.Cols - 1
         GrdDeIm.TextMatrix(1, i) = ""
      Next i
      GrdDeIm.Rows = 1
   ElseIf GrdDeIm.Rows > 1 Then
      GrdDeIm.RemoveItem (GrdDeIm.Row)
   End If
   Call calcular_entra(True)
   
  'PJCA M1787: Se debe revisar los impuestos ya aplicados para restar los descuentos comerciales que se apliquen despues
  Dim vrMiValor As Variant
  If GrdDeIm.Rows > 1 Then
      For i = 1 To GrdDeIm.Rows - 1
        If GrdDeIm.TextMatrix(i, 8) = "I" Then
            If GrdDeIm.TextMatrix(i, 4) = NUL$ Then GrdDeIm.TextMatrix(i, 4) = 0
            If Not IsNumeric(GrdDeIm.TextMatrix(i, 4)) Then GrdDeIm.TextMatrix(i, 4) = 0
            If CDbl(TxtEntra(10)) = 0 Then
               vrMiValor = 0
            Else
               If Mid(GrdDeIm.TextMatrix(i, 1), 1, 1) = "M" Then    'Impuestos tipo ReteIVA
                  vrMiValor = (GrdDeIm.TextMatrix(i, 3) * CDbl(txtIMPUE)) / 100
               Else
                  vrMiValor = (GrdDeIm.TextMatrix(i, 3) * (CDbl(TxtEntra(10)) - CDbl(TxtEntra(11)))) / 100
               End If
            End If
            If BoAproxCent Then vrMiValor = AproxCentena(Round(vrMiValor)) 'JLPB T15675
            GrdDeIm.TextMatrix(i, 4) = vrMiValor
            GrdDeIm.TextMatrix(i, 4) = Format(GrdDeIm.TextMatrix(i, 4), "#,###.#0")
        End If
      Next
  End If
  'PJCA M1787
   
End Sub

Private Sub cmdIconos_Click(Index As Integer)
'    Dim Cuer As String, CnTr As Integer, Dicc As Boolean
    Dim Cuer As String, CnTr As Integer     'DEPURACION DE CODIGO
    Select Case Index
    Case 0
    Case 1: If Documento.Encabezado.EsNuevoDocumento And Not Me.FraRequi.Visible Then WinDoc.AgregarArticulos
            'PJCA M1206
               Me.FilaDeGrillaActiva = GrdArticulos.Rows - 1 'HRR DEPURACION
              If Aplicacion.Interfaz_Presupuesto Then
                 If TxtPpto(0) <> NUL$ And TxtPpto(1) <> NUL$ And TxtPpto(3) <> NUL$ Then
                    TxtPpto(0) = NUL$: TxtPpto(1) = NUL$: TxtPpto(3) = NUL$
                    Call Mensaje1("Revise nuevamente el movimiento Presupuestal", 2)
                 End If
              End If
            'PJCA M1206
    Case 2
        If Documento.Encabezado.EsNuevoDocumento Then
            CnTr = GrdArticulos.Rows - 1
            If Me.opcBOSEL.value Then
                While (CnTr > 0 And GrdArticulos.Rows > 1)
                    If Me.GrdArticulos.TextMatrix(CnTr, vSeleccionada) = "1" Then
                        WinDoc.EliminarFila (CnTr)
                    End If
                    CnTr = CnTr - 1
                Wend
            ElseIf Me.opcBONO.value Then
                While (CnTr > 0 And GrdArticulos.Rows > 1)
                    If Not Me.GrdArticulos.TextMatrix(CnTr, vSeleccionada) = "1" Then
                        WinDoc.EliminarFila (CnTr)
                    End If
                    CnTr = CnTr - 1
                Wend
            End If
'            If Me.opcBONO.Value And Me.GrdArticulos.TextMatrix(Cntr, vSeleccionada) = "0" Then WinDoc.EliminarFila (Cntr)
'            WinDoc.EliminarFila (Me.FilaActiva)
            If Not GrdArticulos.Rows > 2 Then GrdArticulos.Rows = 2
            'GrdArticulos.Row = 1 'EACT T20042 SE ESTABLECE EN COMENTARIO
            GrdArticulos.Row = 0 'EACT T20042
            'Me.FilaActiva = GrdArticulos.Row
            Me.FilaDeGrillaActiva = GrdArticulos.Row 'DAHV M4220
        End If
    Case 3
        Set FrmEXPOIMPO.ElDocumento = Documento
        Set FrmEXPOIMPO.FgridArtic = Me.GrdArticulos
        WinDoc.ProcesarEventosGrilla = False
        Cuer = ""
        FrmEXPOIMPO.PosicionesEnGrilla = Cuer & Me.ColCodigoArticulo & Cuer & _
            Coma & Cuer & Me.ColCantidad & Cuer & _
            Coma & Cuer & Me.ColCostoUnidad & Cuer
         FrmEXPOIMPO.UtilizArticulo = WinDoc.TipoDeBodega 'AASV M5606
        Set FrmEXPOIMPO.ElFormulario = Me
        FrmEXPOIMPO.Show vbModal
        WinDoc.ProcesarEventosGrilla = True
        ' DAHV T1607 - INICIO
        ' Se adiciona esta l�nea de c�digo pasra permitir que se ejecute el evento RowColChange.
        ' que genera el mensaje de existencias de art�culo.
         Me.GrdArticulos.Row = 0
        ' DAHV T1607 - FIN
        If (Documento.Encabezado.TipDeDcmnt = FacturaVenta) Or (Documento.Encabezado.TipDeDcmnt = DevolucionFacturaVenta) Or (Documento.Encabezado.TipDeDcmnt = AnulaFacturaVenta) Then
            Call WinDoc_CambioRenglonGrilla(0, WinDoc.Calcular_Documento(Me.ColCantidad, Me.ColVenUndConDes))
        Else
'            Call WinDoc_CambioRenglonGrilla(0, WinDoc.Calcular_Documento(Me.ColCantidad, Me.ColCostoUnidad))
            Call WinDoc_CambioRenglonGrilla(0, WinDoc.Calcular_Documento(Me.ColCantidad, Me.ColCostoSinIVA))        'REOL M2460
        End If
    End Select
End Sub

Private Sub cmdDocsPadre_Click(Index As Integer)
Dim aux As Integer
Dim resp As Long 'jjrg 15597
Dim InI As Integer 'SKRV T28409 Contador para ciclos
    Select Case Index
    Case 0
        Me.fraTOTALES.Visible = False
        'WinDoc.SeleccionarDocumentoPadre (Not Documento.Encabezado.EsInterno), , boFact '|.DR.|, M:1372
        
        'jjrg 15597/15603/15595/15601
        If CodOpc = "033012" Or CodOpc = "033018" Or CodOpc = "033024" Or CodOpc = "033006" Then 'Anulacion requisicion
           ' resp = MsgBox("La Anulacion se va a generar de manera independiente", vbYesNo + vbInformation, "Inventarios") 'AFMG T15595 , AFMG T15597, AFMG T15601, AFMG T15603  SE ELIMINA MENSAJE
                   If (Documento.Encabezado.TipDeDcmnt = FacturaVenta) Or (Documento.Encabezado.TipDeDcmnt = DevolucionFacturaVenta) Or (Documento.Encabezado.TipDeDcmnt = AnulaFacturaVenta) Then
            'Call WinDoc_CambioRenglonGrilla(0, WinDoc.Calcular_Documento(Me.ColCantidad, Me.ColVenUndConDes))
            Call WinDoc_CambioRenglonGrilla(0, WinDoc.Calcular_Documento(Me.ColCantidad, Me.ColCostoSinIVA))
        Else
            'Call WinDoc_CambioRenglonGrilla(0, WinDoc.Calcular_Documento(Me.ColCantidad, Me.ColCostoUnidad))
            Call WinDoc_CambioRenglonGrilla(0, WinDoc.Calcular_Documento(Me.ColCantidad, Me.ColCostoSinIVA))
            If (Documento.Encabezado.TipDeDcmnt = Entrada) Or (Documento.Encabezado.TipDeDcmnt = Despacho) Or (Documento.Encabezado.TipDeDcmnt = BajaConsumo) Or (Documento.Encabezado.TipDeDcmnt = Salida) Then
                GrdArticulos.Col = 10: GrdArticulos.Col = 11
            End If
        End If
           ' If resp = vbYes Then 'AFMG T15595 , AFMG T15597, AFMG T15601, AFMG T15603 SE ELIMINA MENSAJE
             '  Exit Sub 'AFMG T15595 , AFMG T15597, AFMG T15601, AFMG T15603 SE ELIMINA MENSAJE
            'End If 'AFMG T15595 , AFMG T15597, AFMG T15601, AFMG T15603 SE SE ELIMINA MENSAJE
        End If
        'jjrg 15597
            
        
        WinDoc.SeleccionarDocumentoPadre (Not Documento.Encabezado.EsInterno), , boFact, CInt(Me.ChkRequ.value) 'JACC R1428-1871-2143-2364
'        If Aplicacion.Interfaz_Contabilidad Then Call CargaValoresParticulatres
        ''JACC R1428-1871-2143-2369
        If BoSeleccion And ChkRequ.Visible Then
           ChkRequ.Enabled = False
        End If
        'JACC R1428-1871-2143-2369
        AutoDocCargado = Documento.Encabezado.AutoDelDocCARGADO
        Call CargaValoresParticulatres
        Me.fraTOTALES.Visible = True
        If (Documento.Encabezado.TipDeDcmnt = FacturaVenta) Or (Documento.Encabezado.TipDeDcmnt = DevolucionFacturaVenta) Or (Documento.Encabezado.TipDeDcmnt = AnulaFacturaVenta) Then
            'Call WinDoc_CambioRenglonGrilla(0, WinDoc.Calcular_Documento(Me.ColCantidad, Me.ColVenUndConDes))
            Call WinDoc_CambioRenglonGrilla(0, WinDoc.Calcular_Documento(Me.ColCantidad, Me.ColCostoSinIVA))
        Else
            'Call WinDoc_CambioRenglonGrilla(0, WinDoc.Calcular_Documento(Me.ColCantidad, Me.ColCostoUnidad))
            Call WinDoc_CambioRenglonGrilla(0, WinDoc.Calcular_Documento(Me.ColCantidad, Me.ColCostoSinIVA))
            If (Documento.Encabezado.TipDeDcmnt = Entrada) Or (Documento.Encabezado.TipDeDcmnt = Despacho) Or (Documento.Encabezado.TipDeDcmnt = BajaConsumo) Or (Documento.Encabezado.TipDeDcmnt = Salida) Then
                GrdArticulos.Col = 10: GrdArticulos.Col = 11
            End If
        End If
'        WinDoc.SeleccionarDocumentoPadre ((Not Documento.Encabezado.EsIndependiente) And (Not Documento.Encabezado.RomDepende))
       'PedroJ
          If Aplicacion.Interfaz_Presupuesto Then
            ReDim Arr(0)
            Condicion = "NU_AUTO_ENCA_ENPP=" & AutoDocCargado & " AND NU_TIPO_PTAL_ENPP=2"  'Leer el RP
            Result = LoadData("IN_R_ENCA_PPTO", "NU_CONS_PTAL_ENPP", Condicion, Arr)
            If Result <> FAIL And Encontro Then
                Me.TxtPpto(0) = Arr(0)
                DatPpto = True
            End If
          End If
        'PedroJ
    Case 1 'Elimina los items seleccionados
        'de la lista de requisiciones
        aux = 1
        Do While aux <= lstDocsPadre.ListCount
           If lstDocsPadre.Selected(aux - 1) = True Then
              'SKRV T28409 INICIO
              For InI = 0 To UBound(VaAnulDocs, 2)
                 If VaAnulDocs(1, InI) = lstDocsPadre.List(aux - 1) Then
                    VaAnulDocs(1, InI) = NUL$
                    VaAnulDocs(0, InI) = NUL$
                    Exit For
                 End If
              Next InI
              'SKRV T28409 FIN
              lstDocsPadre.RemoveItem (aux - 1)
              aux = 1
           Else
              aux = aux + 1
           End If
        Loop
        ''JACC R1428-1871-2143-2369
        If lstDocsPadre.ListCount = 0 Then
          If ChkRequ.Visible Then ChkRequ.Enabled = True
        End If
        'JACC R1428-1871-2143-2369
'               Call Agregar_Arti_Requi
    End Select
   BoSeleccion = False 'AASV M3694
End Sub

Private Sub IrAlCuerpo()
    Dim Cuerda As String
    Dim arrTDA(0) As Variant
    If Me.cboConsecutivo.ListIndex < 0 Then Cuerda = Cuerda & IIf(Len(Cuerda) > 0, ", ", "") & "Consecutivo"
    If Documento.Encabezado.EsNuevoDocumento = True Then 'JACC M2930
        If Len(Trim(Me.txtDocum(5).Text)) = 0 Then Cuerda = Cuerda & IIf(Len(Cuerda) > 0, ", ", "") & "Tercero"
    End If
    If Me.cboBodega.ListIndex < 0 Then Cuerda = Cuerda & IIf(Len(Cuerda) > 0, ", ", "") & "Bodega de origen"
    If Documento.Encabezado.EsNuevoDocumento And Documento.Encabezado.TipDeDcmnt = FacturaVenta Then If Me.cboLisPrecio.ListIndex < 0 Then Cuerda = Cuerda & IIf(Len(Cuerda) > 0, ", ", "") & "Lista de precio"
    If Documento.Encabezado.EsInterno Then If Me.cboBodDestino.ListIndex < 0 Then Cuerda = Cuerda & IIf(Len(Cuerda) > 0, ", ", "") & "Bodega de destino"
    If Not IsDate(txtDocum(21) & "/" & txtDocum(22) & "/" & txtDocum(23)) Then Cuerda = Cuerda & IIf(Len(Cuerda) > 0, ", ", "") & "Fecha de vigencia"
    If Len(Cuerda) > 0 Then MsgBox ("Falta informaci�n en:" & Cuerda): Exit Sub
    Me.FraGeneral.Enabled = False
    If Me.cboLisPrecio.ListIndex > -1 Then Documento.AutoListaPrecio = Me.cboLisPrecio.ItemData(Me.cboLisPrecio.ListIndex)
    Documento.Encabezado.EsIndependiente = True
    If Not Documento.Encabezado.EsNuevoDocumento Then Documento.Encabezado.EsIndependiente = False
    
    Me.CmdCuentas.Visible = (Aplicacion.Interfaz_Contabilidad Or Aplicacion.Interfaz_CxP) And (Documento.Encabezado.TipDeDcmnt = FacturaVenta Or Documento.Encabezado.TipDeDcmnt = Entrada)    'PedroJ
    If Me.cboIndependiente.ListIndex = 0 And Documento.Encabezado.EsNuevoDocumento Then Me.FraRequi.Visible = True
    If Me.FraRequi.Visible Then Documento.Encabezado.EsIndependiente = False
    Me.FraArti.Enabled = True
    Me.FraPie.Enabled = True
    Me.FraOpciones.Enabled = True
    Documento.Encabezado.NumeroCOMPROBANTE = Me.cboConsecutivo.ItemData(Me.cboConsecutivo.ListIndex)
    Documento.Encabezado.BodegaORIGEN = Me.cboBodega.ItemData(Me.cboBodega.ListIndex)
    Result = LoadData("IN_BODEGA", "TX_VENTA_BODE", "NU_AUTO_BODE=" & Documento.Encabezado.BodegaORIGEN, arrTDA())
    WinDoc.TipoDeBodega = CStr(arrTDA(0))
    Call INTCllBodegas(WinDoc.ClldeBodegas, WinDoc.TipoDeBodega)
    Documento.Encabezado.Tercero.IniXNit (Me.txtDocum(5))
    Documento.Encabezado.FechaVigencia = CDate(txtDocum(21) & "/" & txtDocum(22) & "/" & txtDocum(23))
    Me.staINFORMA.Visible = True
    Me.FraArti.BackColor = &H8000000F
    Me.FraPie.BackColor = &H8000000F
    Me.FraRequi.BackColor = &H8000000F
    Me.FraOpciones.BackColor = &H8000000F
    Me.FraGeneral.BackColor = &H8000000A
    If (Documento.Encabezado.TipDeDcmnt = FacturaVenta) Or (Documento.Encabezado.TipDeDcmnt = DevolucionFacturaVenta) Then
''////////////
        ReDim itmCuentasIVA(4, 0)
        Result = LoadMulData("TC_IMPUESTOS", "CD_CODI_IMPU, CD_CUEN_IMPU, 0, DE_NOMB_IMPU, PR_PORC_IMPU", "ID_TIPO_IMPU='I'", itmCuentasIVA)
        ReDim itmCuentasFormas(2, 0)
        Result = LoadMulData("FORMA_PAGO", "NU_NUME_FOPA, CD_CUEN_FOPA, DE_DESC_FOPA", "", itmCuentasFormas)
        ReDim itmCuentasOtrosCargos(3, 0)
        Result = LoadMulData("TC_IMPUESTOS", "CD_CODI_IMPU, CD_CUEN_IMPU, DE_NOMB_IMPU, PR_PORC_IMPU", "ID_TIPO_IMPU='O'", itmCuentasOtrosCargos)
        ReDim itmCuentasOtrosDescuentos(3, 0)
        Result = LoadMulData("DESCUENTO", "CD_CODI_DESC, CD_CUEN_DESC, DE_NOMB_DESC, PR_PORC_DESC", "", itmCuentasOtrosDescuentos)
        
        If Documento.Encabezado.EsNuevoDocumento Then Me.grdFormaPago.Rows = 1
        If Documento.Encabezado.EsNuevoDocumento Then Me.grdOtrosCargos.Rows = 1
        If Documento.Encabezado.EsNuevoDocumento Then Me.grdOtrosDescuentos.Rows = 1
''////////////
    End If
        Select Case Documento.Encabezado.TipDeDcmnt
        Case Entrada, ODCompra
            'Me.DefinicionDeGrilla = "Autoart,+0,Marcar,800," & _
                "Codigo,1900,Lote Producci�n,1900," & _
                "Nombre,3500,AutoUndMedida,+0," & _
                "Und Medida,1000," & _
                "Multiplicador,+0," & _
                "Divisor,+1," & _
                "Ultimo Costo,+1500," & _
                "Cantidad,+800," & _
                "Sin IVA,+1500," & _
                "Venta Sin IVA,+0," & _
                "IVA,+800," & _
                "Costo Unitario,+1500," & _
                "Costo Total,+1500," & _
                "Venta Sin DESC,+0," & _
                "% Descuento,+0," & _
                "Valor Descuento,+0," & _
                "Venta Unitario,+0," & _
                "Venta Total,+0," & _
                "Fecha Vencimiento,1200," & _
                "Fecha Entrada,1200," & _
                "Control C/dad,+0," & _
                "Pertenece A,+0," & _
                "Select,+0," & _
                "COD Impuesto,+0," 'DRMG T45696 SE DEJA EN COMENTARIO
           Me.DefinicionDeGrilla = "Autoart,+0,Marcar,800," & _
              "Codigo,1900,Lote Producci�n,1900," & _
              "Nombre,3500,AutoUndMedida,+0," & _
              "Und Medida,1000," & _
              "Multiplicador,+0," & _
              "Divisor,+1," & _
              "Ultimo Costo,+1500," & _
              "Cantidad,+950," & _
              "Sin IVA,+1500," & _
              "Venta Sin IVA,+0," & _
              "IVA,+800," & _
              "Costo Unitario,+1500," & _
              "Costo Total,+1500," & _
              "Venta Sin DESC,+0," & _
              "% Descuento,+0," & _
              "Valor Descuento,+0," & _
              "Venta Unitario,+0," & _
              "Venta Total,+0," & _
              "Fecha Vencimiento,1200," & _
              "Fecha Entrada,1200," & _
              "Control C/dad,+0," & _
              "Pertenece A,+0," & _
              "Select,+0," & _
              "COD Impuesto,+0," 'DRMG T45696
                'HRR Req1553
                Me.DefinicionDeGrilla = Me.DefinicionDeGrilla & _
                "Control CostSinIva,+0," & _
                "Control PorceIva,+0," & _
                "Control CostUnidad,+0"
                cambioCelda = False
                'Req1553
                
        Case FacturaVenta, DevolucionFacturaVenta
            'Me.DefinicionDeGrilla = "Autoart,+0,Marcar,800," & _
                "Codigo,1900,Lote Producci�n,0," & _
                "Nombre,3500,AutoUndMedida,+0," & _
                "Und Medida,1000," & _
                "Multiplicador,+0," & _
                "Divisor,+0," & _
                "Ultimo Costo,+0," & _
                "Cantidad,+800," & _
                "Sin IVA,+0," & _
                "Venta Sin IVA,+1500," & _
                "IVA,+800," & _
                "Costo Unitario,+0," & _
                "Costo Total,+0," & _
                "Venta Sin DESC,+1500," & _
                "% Descuento,+800," & _
                "Valor Descuento,+1500," & _
                "Venta Unitario,+1500," & _
                "Venta Total,+1500," & _
                "Fecha Vencimiento,0," & _
                "Fecha Entrada,0," & _
                "Control C/dad,+0," & _
                "Pertenece A,+0," & _
                "Select,+0," & _
                "COD Impuesto,+0," 'DRMG T45696 SE DEJA EN COMENTARIO
           Me.DefinicionDeGrilla = "Autoart,+0,Marcar,800," & _
              "Codigo,1900,Lote Producci�n,0," & _
              "Nombre,3500,AutoUndMedida,+0," & _
              "Und Medida,1000," & _
              "Multiplicador,+0," & _
              "Divisor,+0," & _
              "Ultimo Costo,+0," & _
              "Cantidad,+950," & _
              "Sin IVA,+0," & _
              "Venta Sin IVA,+1500," & _
              "IVA,+800," & _
              "Costo Unitario,+0," & _
              "Costo Total,+0," & _
              "Venta Sin DESC,+1500," & _
              "% Descuento,+800," & _
              "Valor Descuento,+1500," & _
              "Venta Unitario,+1500," & _
              "Venta Total,+1500," & _
              "Fecha Vencimiento,0," & _
              "Fecha Entrada,0," & _
              "Control C/dad,+0," & _
              "Pertenece A,+0," & _
              "Select,+0," & _
              "COD Impuesto,+0," 'DRMG T45696
                'HRR Req1553 Esto no aplica para este formulario pero como utilizan las mismas funciones
                'hay que definirlos
                Me.DefinicionDeGrilla = Me.DefinicionDeGrilla & _
                "Control CostSinIva,+0," & _
                "Control PorceIva,+0," & _
                "Control CostUnidad,+0"
                cambioCelda = False
                'Req1553
''
        Case Else
            'Me.DefinicionDeGrilla = "Autoart,+0,Marcar,800," & _
                "Codigo,1900,Lote Producci�n,0," & _
                "Nombre,3500,AutoUndMedida,+0," & _
                "Und Medida,1000," & _
                "Multiplicador,+0," & _
                "Divisor,+0," & _
                "Ultimo Costo,+0," & _
                "Cantidad,+800," & _
                "Costo Sin IVA,+0," & _
                "Venta Sin IVA,+1500," & _
                "IVA,+800," & _
                "Costo Unitario,+1500," & _
                "Costo Total,+1500," & _
                "Venta Sin DESC,+0," & _
                "% Descuento,+0," & _
                "Valor Descuento,+0," & _
                "Venta Unitario,+0," & _
                "Venta Total,+0," & _
                "Fecha Vencimiento,0," & _
                "Fecha Entrada,0," & _
                "Control C/dad,+0," & _
                "Pertenece A,+0," & _
                "Select,+0," & _
                "COD Impuesto,+0," 'DRMG T45696 SE DEJA EN COMENTARIO
           Me.DefinicionDeGrilla = "Autoart,+0,Marcar,800," & _
              "Codigo,1900,Lote Producci�n,0," & _
              "Nombre,3500,AutoUndMedida,+0," & _
              "Und Medida,1000," & _
              "Multiplicador,+0," & _
              "Divisor,+0," & _
              "Ultimo Costo,+0," & _
              "Cantidad,+950," & _
              "Costo Sin IVA,+0," & _
              "Venta Sin IVA,+1500," & _
              "IVA,+800," & _
              "Costo Unitario,+1500," & _
              "Costo Total,+1500," & _
              "Venta Sin DESC,+0," & _
              "% Descuento,+0," & _
              "Valor Descuento,+0," & _
              "Venta Unitario,+0," & _
              "Venta Total,+0," & _
              "Fecha Vencimiento,0," & _
              "Fecha Entrada,0," & _
              "Control C/dad,+0," & _
              "Pertenece A,+0," & _
              "Select,+0," & _
              "COD Impuesto,+0," 'DRMG T45696
                'HRR Req1553 Esto no aplica para este formulario pero como utilizan las mismas funciones
                'hay que definirlos
                Me.DefinicionDeGrilla = Me.DefinicionDeGrilla & _
                "Control CostSinIva,+0," & _
                "Control PorceIva,+0," & _
                "Control CostUnidad,+0"
                cambioCelda = False
                'Req1553
        End Select
    Call GrdFDef(GrdArticulos, 0, 0, Me.DefinicionDeGrilla)
End Sub

Private Sub cmdIrAlCuerpo_Click()
    NoHacerNada = True
    BuscarTercero 'JACC M2930
    Call IrAlCuerpo
    NoHacerNada = False
End Sub

Private Sub CmdPpto_Click()
     
    'HRR M1818
    If (Documento.Encabezado.TipDeDcmnt = Entrada And (Not Documento.Encabezado.EsIndependiente) And Documento.Encabezado.EsNuevoDocumento) Then
      sbPresupuesto_Entrada (NUL$)
      Exit Sub
    End If
    'HRR M1818
    If Val(txtTOTAL) = 0 Then Exit Sub
    FrmPresu.Desde_Forma = Me
'    PptoArti = Articulos_Presupuesto(GrdArticulos, "NU_COMP_CUEN,NU_CDPE_CUEN", 15, 0) 'REOL M807
    'PptoArti = Articulos_Presupuesto(GrdArticulos, 15, 0)       'DEPURACION DE CODIGO
    'NMSR M3544
    'If Documento.Encabezado.TipDeDcmnt = ODCompra Then
    If ((Documento.Encabezado.TipDeDcmnt = ODCompra) Or (Documento.Encabezado.TipDeDcmnt = Entrada)) Then 'DAHV M4460
        'ArtiPpto = Articulos_Presupuesto(GrdArticulos, 15, 0, , , ODCompra) 'DAHV M4460
        'ArtiPpto = Articulos_Presupuesto(GrdArticulos, 15, 0, , , Documento.Encabezado.TipDeDcmnt) 'HRR M5080
        'ArtiPpto = Articulos_Presupuesto(GrdArticulos, 15, 0, , Documento.Encabezado.TipDeDcmnt) 'HRR M5080
        ''JACC M6427
        'If Documento.Encabezado.TipDeDcmnt = Entrada And (TipoRegimen & TipoPers) = "10" Then
        If Documento.Encabezado.TipDeDcmnt = Entrada Then
            If Not IsNumeric(Me.TxtEntra(18)) Then Me.TxtEntra(18) = 0
            If Not cmdOpciones(0).Enabled Then
                If CDbl(Me.TxtEntra(18)) = 0 Then
                   If IsNumeric(Me.TxtEntra(16)) Then
                      Me.TxtEntra(18) = (CDbl(Me.txtIMPUE.Text) * CDbl(Me.TxtEntra(16))) / 100
                   End If
                End If
            End If
          'ArtiPpto = Articulos_Presupuesto(GrdArticulos, 15, 0, , Documento.Encabezado.TipDeDcmnt, CDbl(Me.TxtEntra(18)))       'HRR M5080
          'JACC M6527
          If Not IsNumeric(Me.TxtEntra(16)) Then Me.TxtEntra(16) = 0
          'ArtiPpto = Articulos_Presupuesto(GrdArticulos, 15, 0, , Documento.Encabezado.TipDeDcmnt, CDbl(Me.TxtEntra(16))) 'LDCR R5027/T6173
          ArtiPpto = Articulos_Presupuesto(GrdArticulos, 15, 0, , Documento.Encabezado.TipDeDcmnt, CDbl(Me.TxtEntra(16)), Documento.Encabezado.Tercero.Nit) 'LDCR R5027/T6173
          'JACC M6527
        Else
           ArtiPpto = Articulos_Presupuesto(GrdArticulos, 15, 0, , Documento.Encabezado.TipDeDcmnt) 'HRR M5080
        End If
        'JACC M6427
        
    Else
        PptoArti = Articulos_Presupuesto(GrdArticulos, 15, 0)
    End If
    'NMSR M3544
        
         'If lstDocsPadre.Visible = False Then            'Si es un documento independiente 'HRR R1700
         If lstDocsPadre.Visible = False Or BoConsMov Then 'HRR R1700 Si es un documento independiente o se consulta el movimiento
            If Me.cmdOpciones(0).Enabled = True Then     'Si es nuevo documento
               'Call FrmPresu.CargarDatos(txtTOTAL, txtDocum(1) & "/" & Me.txtDocum(2) & "/" & Me.txtDocum(3), Documento.Encabezado.TipDeDcmnt, 0, True)
               'JACC M6427
               'If Documento.Encabezado.TipDeDcmnt = Entrada And (TipoRegimen & TipoPers) = "10" Then
               If Documento.Encabezado.TipDeDcmnt = Entrada Then
                  If Not IsNumeric(Me.TxtEntra(18)) Then Me.TxtEntra(18) = 0
                  'Call FrmPresu.CargarDatos(CDbl(txtTOTAL) + CDbl(Me.TxtEntra(18)), txtDocum(1) & "/" & Me.txtDocum(2) & "/" & Me.txtDocum(3), Documento.Encabezado.TipDeDcmnt, 0, True) 'JLPB T30751 Se deja en comentario
                  Call FrmPresu.CargarDatos(CDbl(txtTOTAL), txtDocum(1) & "/" & Me.txtDocum(2) & "/" & Me.txtDocum(3), Documento.Encabezado.TipDeDcmnt, 0, True) 'JLPB T30751
               Else
                  Call FrmPresu.CargarDatos(txtTOTAL, txtDocum(1) & "/" & Me.txtDocum(2) & "/" & Me.txtDocum(3), Documento.Encabezado.TipDeDcmnt, 0, True)
               End If
               'JACC M6427
               
            Else                                         'si esta consultando uno guardado
               If Me.TxtPpto(0) = 0 Then Call Mensaje1("No se encontraron datos Pptales", 3): Exit Sub
               Call FrmPresu.CargarDatos(txtTOTAL, txtDocum(1) & "/" & Me.txtDocum(2) & "/" & Me.txtDocum(3), Documento.Encabezado.TipDeDcmnt, 2, True)
            End If
         Else                                            'Si es un documento dependiente
              Call FrmPresu.CargarDatos(txtTOTAL, txtDocum(1) & "/" & Me.txtDocum(2) & "/" & Me.txtDocum(3), Documento.Encabezado.TipDeDcmnt, 1, False)
         End If
    
    FrmPresu.Show 1
    If Screen.ActiveForm.Name = "FrmPresu" Then 'JAGS T8404
    'INICIO GAVL T5359
    If FrmPresu.Txtpres = "" Then
        Result = FAIL
    End If
    End If 'JAGS T8404
    'FIN GAVL T5359
End Sub

Private Sub cmdPROCEDER_Click(Index As Integer)
    'DAHV M4138 - INICIO
    Dim Mensaje As String
    Mensaje = "El valor neto de la compra no puede tener un saldo Negativo."
    Mensaje = Mensaje & Chr(13) & "Por favor revise nuevamente los valores Ingresados"
    'DAHV M4138 - FIN
    
    If Me.frmBAJA.Visible Then Me.frmBAJA.Visible = False
    If Me.frmCOMPRAS.Visible Then Me.frmCOMPRAS.Visible = False
    If Me.FrmDEVOLFACTURA.Visible Then Me.FrmDEVOLFACTURA.Visible = False
    
    'If Me.FrmENTRADA.Visible Then Me.FrmENTRADA.Visible = False
    
    'DAHV M4138 - INICIO
      If Me.FrmENTRADA.Visible Then
        If CDbl(TxtEntra(15).Text) < 0 Then
          Call Mensaje1(Mensaje, 3)
          Exit Sub
        End If
        Me.FrmENTRADA.Visible = False
      End If
    'DAHV M4138 - FIN
    
    If Me.FrmFACTURA.Visible Then Me.FrmFACTURA.Visible = False
    If Me.frmFORMAPAGO.Visible Then Me.frmFORMAPAGO.Visible = False
    If Me.frmOTROSCARGOS.Visible Then Me.frmOTROSCARGOS.Visible = False
    If Me.frmOTROSDESCUENTOS.Visible Then Me.frmOTROSDESCUENTOS.Visible = False
    Me.frmINVENTARIOS.Visible = True

'PedroJ se pone en comentario para que no muestre la ventana de ctas contables 2 veces
    Select Case Index
'    Case Is = CompraDElementos
'        VerMovimientoContable CompraDElementos
    Case Is = Entrada
'        VerMovimientoContable Documento.Encabezado.TipDeDcmnt
    Case Is = DevolucionEntrada
'        VerMovimientoContable Documento.Encabezado.TipDeDcmnt
    Case Is = Salida 'DevolucionEntrada, AprovechaDonacion
'        VerMovimientoContable Documento.Encabezado.TipDeDcmnt
         VerMovimientoContable Documento.Encabezado.TipDeDcmnt 'HRR R1801
    Case Is = AprovechaDonacion
'        VerMovimientoContable Documento.Encabezado.TipDeDcmnt
    Case Is = FacturaVenta
'        VerMovimientoContable Documento.Encabezado.TipDeDcmnt
    Case Is = DevolucionFacturaVenta
'        VerMovimientoContable Documento.Encabezado.TipDeDcmnt
    Case Is = BajaConsumo
        VerMovimientoContable Documento.Encabezado.TipDeDcmnt
    Case Is = DevolucionBaja
'        VerMovimientoContable Documento.Encabezado.TipDeDcmnt
    End Select
    LaPuedoCerrar = True
End Sub

Private Sub cmdREGRESAR_Click(Index As Integer)
    If Me.frmBAJA.Visible Then Me.frmBAJA.Visible = False
    If Me.frmCOMPRAS.Visible Then Me.frmCOMPRAS.Visible = False
    If Me.FrmDEVOLFACTURA.Visible Then Me.FrmDEVOLFACTURA.Visible = False
    If Me.FrmENTRADA.Visible Then Me.FrmENTRADA.Visible = False
    If Me.FrmFACTURA.Visible Then Me.FrmFACTURA.Visible = False
    If Me.frmFORMAPAGO.Visible Then Me.frmFORMAPAGO.Visible = False
    If Me.frmOTROSCARGOS.Visible Then Me.frmOTROSCARGOS.Visible = False
    If Me.frmOTROSDESCUENTOS.Visible Then Me.frmOTROSDESCUENTOS.Visible = False
    If Not Me.fraTOTALES.Visible Then Me.fraTOTALES.Visible = True
    Me.frmINVENTARIOS.Visible = True
    vSeFactura = False
    vSeGraba = False
    LaPuedoCerrar = True
    If vGrabaBaja Then
        vGrabaBaja = False
        Call RollBackTran
    End If
    
    If cmdREGRESAR(3).Enabled = True Then 'smdl m1822
        'Borrar los valores de los impuestos y descuentos
        TxtEntra(7) = 0: TxtEntra(8) = 0: TxtEntra(11) = 0: TxtEntra(16) = 0: TxtEntra(17) = 0: TxtEntra(14) = 0: TxtEntra(20) = 0 'smdl m1753 se agrego los index 14 y 20
        TxtCompra(10) = 0: TxtEntra(14).Enabled = False: TxtEntra(20).Enabled = False 'smdl m1775
        GrdDeIm.Rows = 1  'smdl m1752 se adiciono limpiar la grilla de dtos y impuestos.
        Cmd_agregar.Enabled = False 'smdl 1752
    End If
End Sub

Private Sub CmdSelec_Click(Index As Integer)
    Select Case Index
    Case 0: Codigo = NUL$
            Codigo = Seleccion("CENTRO_COSTO", "NO_NOMB_CECO", "CD_CODI_CECO,NO_NOMB_CECO", "DEPENDENCIAS", NUL$)
            If Codigo <> NUL$ Then Me.TxtBajaD(4).Text = Codigo
            Me.TxtBajaD(4).SetFocus
    End Select
End Sub

Private Sub CmdSelecTercero_Click()
    'DAHV - M5468 Se devuelven los cambios realizados por Carlos
    'If txtNumero.Tag = txtNumero.Text Then 'CARV M5468 se agrego que solo seleccione si es el consecutivo indicado
        Codigo = NUL$
        Codigo = Seleccion("TERCERO", "NO_NOMB_TERC", "CD_CODI_TERC,NO_NOMB_TERC", "TERCEROS", NUL$)
        If Codigo <> NUL$ Then txtDocum(5) = Codigo
        txtDocum(5).SetFocus
    'End If
End Sub

Private Sub Documento_ArticuloProcesado(ByVal EnCual As Integer, ByVal Maxi As Integer)
    If Me.proARTICULOS.Max <> Maxi Then Me.proARTICULOS.Max = Maxi
    Me.proARTICULOS.value = EnCual
    Me.proARTICULOS.Refresh
End Sub

Private Sub Form_KeyPress(Tecla As Integer)
    If Tecla = vbCr Then Tecla = vbTab
End Sub

Private Sub Form_Activate()
    If blnDesdeCompras Then Exit Sub
    Dim ArrEDC(3) As Variant
    ReDim VaAnulDocs(1, 0) 'SKRV T28409
    If Not Me.SeActivoFormulario Then
        Me.staINFORMA.Visible = False
        Me.SeActivoFormulario = True
        Documento.Encabezado.EsNuevoDocumento = True
        
        Me.FraPie.Enabled = False
        Me.FraPie.Visible = True
        Me.chaSALDOS.Visible = False
        Me.staINFORMA.Panels("SALBOD").Text = "Saldos X Bodega" 'Saldos X Bodega
        Me.FraOpciones.Enabled = False
        Documento.Encabezado.TipDeDcmnt = Me.AutoTipoDocumento
        If Documento.Encabezado.TipDeDcmnt = ODCompra Then ChkRequ.Visible = True   'JACC R1428-1871-2143-2364
        Call GrdFDef(GrdArticulos, 0, 0, Me.DefinicionDeGrilla)
        Call GrdFDef(Me.grdFormaPago, 0, 0, "C�digo,0,Nombre,2500,Valor,+1200")
        'DRMG T45221-R41288 INICIO SE DEJA EN COMENTARIO LAS SIGUIENTES 2 LINEAS
        'Call GrdFDef(Me.grdOtrosCargos, 0, 0, "C�digo,0,Nombre,2500,Valor,+1200,Porcentaje,+0")
        'Call GrdFDef(Me.grdOtrosDescuentos, 0, 0, "C�digo,0,Nombre,2500,Valor,+1200,Porcentaje,+0")
        Call GrdFDef(Me.grdOtrosCargos, 0, 0, "C�digo,0,Nombre,2500,Valor,+1200,Porcentaje,+0,BASE,+0")
        Call GrdFDef(Me.grdOtrosDescuentos, 0, 0, "C�digo,0,Nombre,2500,Valor,+1200,Porcentaje,+0,BASE,+0")
        'DRMG T45221-R41288 FIN
        Documento.Encabezado.Actualiza
        Me.Caption = Documento.Encabezado.NombreDocumento
        Me.FechaCierre = Documento.Encabezado.FechaCierre
'        Me.UltimaFechaDocumento = UltimaFechaCreacionDocumento(Documento.Encabezado.TipDeDcmnt, Me.FechaCierre)    'DEPURACION DE CODIGO
        Me.UltimaFechaDocumento = UltimaFechaCreacionDocumento()
        Me.FechaVigencia = DateAdd("m", 1, CDate(Me.UltimaFechaDocumento))
        Me.txtDocum(1).Text = Right(String(2, "0") & Trim(Day(Me.UltimaFechaDocumento)), 2)
        Me.txtDocum(2).Text = Right(String(2, "0") & Trim(Month(Me.UltimaFechaDocumento)), 2)
        Me.txtDocum(3).Text = Right(String(4, "0") & Trim(Year(Me.UltimaFechaDocumento)), 4)
        Me.txtDocum(21).Text = Right(String(2, "0") & Trim(Day(Me.FechaVigencia)), 2)
        Me.txtDocum(22).Text = Right(String(2, "0") & Trim(Month(Me.FechaVigencia)), 2)
        Me.txtDocum(23).Text = Right(String(4, "0") & Trim(Year(Me.FechaVigencia)), 4)
        Me.txtDocum(5).Text = Documento.Encabezado.Tercero.Nit
        Documento.Encabezado.Tercero.IniXNit
        Me.txtDocum(6).Text = Documento.Encabezado.Tercero.Nombre
        
    'Req 1112-1308
        If Trim(txtDocum(5).Text) <> NUL$ Then
            ReDim Arr(9)
            Campos = "NO_NOMB_TERC,ID_TIPO_TERC,ID_TIPO_REGI_PAIM,ID_TIPO_CONTR_PAIM,"
            Campos = Campos & "ID_AUTO_RETIVA_PAIM,ID_AUTO_RETICA_PAIM,ID_AUTO_RETREN_PAIM"
            Campos = Campos & ",ID_EXERET_FUEN_PAIM,ID_EXERET_ICA_PAIM,ID_EXERET_IVA_PAIM"
            Desde = "TERCERO,PARAMETROS_IMPUESTOS "
            Condicion = "CD_CODI_TERC=" & Comi & Cambiar_Comas_Comillas(txtDocum(5).Text) & Comi & " AND (NU_ESTADO_TERC <> 1)"
            Condi = "CD_CODI_TERC=CD_CODI_TERC_PAIM AND "
            Condicion = Condi & Condicion
            Result = LoadData(Desde, Campos, Condicion, Arr())
            If (Result <> False) Then
              If Encontro Then
                    TipoPers = Arr(1)
                    TipoRegimen = Arr(2)
                    GranContrib = Arr(3)
                    AutRetIva = Arr(4)
                    AutRetIca = Arr(5)
                    AutoRet = Arr(6)
                    ExentoFuente = Arr(7)
                    ExentoIca = Arr(8)
                    ExentoIva = Arr(9)
              End If
            End If
        End If
    'Req 1112-1308
    
    
        Call Fecha_Docu
        Me.FilaDeGrillaActiva = GrdArticulos.Rows - 1
        Me.ColumnaDeGrillaActiva = 2
        Set WinDoc.ElDocumento = Documento
        Set WinDoc.EsteFormulario = Me
'        Call CargarClassWinDoc(WinDoc, cboConsecutivo, cboBodega, cboBodDestino, _
            GrdArticulos, txtNumero, Me.AutoTipoDocumento, lstDocsPadre, cboIndependiente)
        'DEPURACION DE CODIGO
        Call CargarClassWinDoc(WinDoc, cboConsecutivo, cboBodega, cboBodDestino, _
            GrdArticulos, txtNumero, lstDocsPadre, cboIndependiente)
'        AutonumPerfil = 1
        WinDoc.CargarCombos
        Me.cboLisPrecio.Visible = False
        Me.lblLisPrecio.Visible = False
        If (Documento.Encabezado.TipDeDcmnt = FacturaVenta) Or (Documento.Encabezado.TipDeDcmnt = DevolucionFacturaVenta) Then
            Me.cboLisPrecio.Visible = True
            Me.cboLisPrecio.Top = Me.cboBodDestino.Top
            Me.lblLisPrecio.Visible = True
            Me.lblLisPrecio.Top = Me.lblDESTINO.Top
        End If
        WinDoc.ProcesarEventosGrilla = True
        Me.txtDocum(8).ToolTipText = "Digite las Observaciones del Comprobante"
        If Documento.Encabezado.TipDeDcmnt = ListaPreciosCotiza Then Me.txtDocum(8).ToolTipText = "Presione doble click para digitar las Condiciones Comerciales"
        
       'PedroJ
       If (Aplicacion.Interfaz_Presupuesto) And (Documento.Encabezado.TipDeDcmnt = ODCompra Or Documento.Encabezado.TipDeDcmnt = Entrada) Then
         ReDim Arr(2)
         Result = LoadData("PARAMETROS_INVE", "CD_ORDE_APLI, CD_CONT_APLI, CD_ARFL_APLI", NUL$, Arr())
         OrdeSel = IIf(Arr(0) = NUL$, 0, Arr(0))
         Contrato = Arr(1)
         Me.CmdPpto.Visible = True
       Else
         Me.CmdPpto.Visible = False
       End If
        
       If Aplicacion.Interfaz_Contabilidad Then
         Me.CmdCuentas.Visible = True
       Else
         Me.CmdCuentas.Visible = False
       End If
    
    End If
    opcUsu = fnDevDato("OPCION", "TX_CODI_OPCI", "TX_DESC_OPCI = '" & Me.Caption & Comi) 'JAUM T28559
    BoConsMov = False 'HRR R1700
    LnAntoEncaEnid = 0 'GMS M3559
    
    'DRMG T44728-R41288 INICIO
    If Not (Documento.Encabezado.TipDeDcmnt = FacturaVenta) Then
       TxtFactElt.Visible = False
       LblFactElt.Visible = False
       txtNumero.Top = 360
       LblNumero.Top = 360
    Else
       FrmFactElect.Visible = True
       Condicion = "NU_MODULO_FAEL=1"
       Condicion = Condicion & " AND NU_ESTADO_FAEL=0"
       ReDim VrArr(0)
       Result = LoadData("FACTURA_ELECTRONICA", "NU_CONFAC_FAEL", Condicion, VrArr)
       If Encontro And Result <> FAIL Then
          TxtFactElt.Visible = True
          LblFactElt.Visible = True
          TxtFactElt = VrArr(0) + 1
       Else
          TxtFactElt.Visible = False
          LblFactElt.Visible = False
          txtNumero.Top = 360
          LblNumero.Top = 360
       End If
    End If
    'DRMG T44728-R41288 FIN
    
End Sub

Sub sbCListas()

        '01 |.DR.|, Agregar todos los conceptos de movimiento tipo "inventarios".
Dim ArrX(), i&, J&, A$
ReDim ArrX(1, 0)

    Condicion = "ID_TIPO_CONC=12"
    Result = LoadMulData("Concepto", "CD_CODI_CONC,DE_DESC_CONC", Condicion, ArrX())
    
    cmbConcepto.Clear
    lstConcepto.Clear
    
    For i& = 0 To UBound(ArrX, 2)
        lstConcepto.AddItem ArrX(0, i&)
        cmbConcepto.AddItem ArrX(1, i&)
    Next i&
        
        cmbConcepto.AddItem " - (Anular filtro) - "
        
        '01 Fin.
        
        '02 |.DR.|, Agregar los impuestos con los que est� relacionado cada Concepto.
        ReDim ArrX(0, 0)
    
                lstImpu.Clear
            For i& = 0 To lstConcepto.ListCount - 1
            Condicion = "CD_CONC_COIM='" & lstConcepto.List(i&) & "'"
            Result = LoadMulData("R_CONC_IMPU", "CD_IMPU_COIM", Condicion, ArrX())
                A$ = ""
                For J& = 0 To UBound(ArrX, 2)
                    A$ = A$ & "'" & ArrX(0, J&) & "',"
                Next J&
                    If Encontro And Result <> FAIL Then
                        A$ = Mid(A$, 1, Len(A$) - 1)
                        A$ = "(" & A$ & ")"
                    Else
                        A$ = "(Null)"
                    End If
                    lstImpu.AddItem A$
            Next i&
        '02 Fin.

        '03 |.DR.|, Agregar los descuentos con los que est� relacionado cada Concepto.
        ReDim ArrX(0, 0)
    
                lstDesc.Clear
            For i& = 0 To lstConcepto.ListCount - 1
            Condicion = "CD_CONC_CODE='" & lstConcepto.List(i&) & "'"
            Result = LoadMulData("R_CONC_DESC", "CD_DESC_CODE", Condicion, ArrX())
                A$ = ""
                For J& = 0 To UBound(ArrX, 2)
                    A$ = A$ & "'" & ArrX(0, J&) & "',"
                Next J&
                    If Encontro And Result <> FAIL Then
                        A$ = Mid(A$, 1, Len(A$) - 1)
                        A$ = "(" & A$ & ")"
                    Else
                        A$ = "(Null)"
                    End If
                    lstDesc.AddItem A$
            Next i&
        '03 Fin.


End Sub
Private Sub Form_Load()
Dim tItem As ListItem 'REOL MANTIS:0000569

     
    'Result = loadctrl("CENTRO_COSTO ORDER BY NO_NOMB_CECO", "NO_NOMB_CECO", "CD_CODI_CECO", cmbDEPENDENCIA, arrDPDCS(), NUL$)
    boICxP = (Aplicacion.Interfaz_CxP)
    Result = loadctrl("CENTRO_COSTO", "NO_NOMB_CECO", "CD_CODI_CECO", cmbDEPENDENCIA, arrDPDCS(), "NU_ESTADO_CECO <> 1  ORDER BY NO_NOMB_CECO") 'JACC M7052
    LaPuedoCerrar = True
    Set WinDoc = New WinDocumento
    Set Documento = New ElDocumento
    Call CenterForm(MDI_Inventarios, Me)
    Call CargRecursos '|.DR.|
    Call sbCListas '|.DR.|, R:1631
    Me.lstSALXBOD.ListItems.Clear
    Me.lstSALXBOD.ColumnHeaders.Clear
    Me.lstSALXBOD.ColumnHeaders.Add , "COD", "C�digo", 0.2 * Me.lstSALXBOD.Width
    Me.lstSALXBOD.ColumnHeaders.Add , "NOM", "Nombre", 0.5 * Me.lstSALXBOD.Width
    Me.lstSALXBOD.ColumnHeaders.Add , "SLD", "Saldo", 0.16 * Me.lstSALXBOD.Width
    Me.lstSALXBOD.ColumnHeaders("SLD").Alignment = lvwColumnRight
    Me.lstSALXBOD.ColumnHeaders.Add , "MIN", "M�nimo", 0.16 * Me.lstSALXBOD.Width
    Me.lstSALXBOD.ColumnHeaders("MIN").Alignment = lvwColumnRight
    Me.lstSALXBOD.ColumnHeaders.Add , "REP", "Reposi", 0.16 * Me.lstSALXBOD.Width
    Me.lstSALXBOD.ColumnHeaders("REP").Alignment = lvwColumnRight
    Me.lstSALXBOD.ColumnHeaders.Add , "MAX", "M�ximo", 0.16 * Me.lstSALXBOD.Width
    Me.lstSALXBOD.ColumnHeaders("MAX").Alignment = lvwColumnRight
    Me.lstSALXBOD.ColumnHeaders.Add , "DES", "MxDesp", 0.16 * Me.lstSALXBOD.Width
    Me.lstSALXBOD.ColumnHeaders("DES").Alignment = lvwColumnRight
    Me.lstSALXBOD.ColumnHeaders.Add , "ENT", "Entradas", 0.16 * Me.lstSALXBOD.Width
    Me.lstSALXBOD.ColumnHeaders("ENT").Alignment = lvwColumnRight
    Me.lstSALXBOD.ColumnHeaders.Add , "SAL", "Salidas", 0.16 * Me.lstSALXBOD.Width
    Me.lstSALXBOD.ColumnHeaders("SAL").Alignment = lvwColumnRight
    
    Set tItem = MDI_Inventarios.Lstw_Formas.ListItems("L" & FrmMenu.CodOpc)
    If Mid(tItem.Text, 2, 1) = "N" Then cmdOpciones(1).Enabled = False Else cmdOpciones(1).Enabled = True 'REOL MANTIS:0000569
    BoConsMov = False 'HRR R1700
    CodOpc = FrmMenu.CodOpc 'APGR T1643
    
    'JLPB T23820 INICIO SE DEJA EN COMENTARIO
'    'GAVL T6423 SENTENCIA PARA SABER SI LA OPCION ESTA ACTIVA EN CXP
'    If boICxP Then
'        If ExisteCAMPO("PARAMETROS_CXP", "NU_REDIMDE_PCXP") Then '
'            BoAproxCent = CBool(fnDevDato("PARAMETROS_CXP", "NU_REDIMDE_PCXP", NUL$))
'        End If
'    End If
'    'FIN GAVL T6423
    'JLPB T23820 FIN
    
    'orden de compra
    'jjrg 14834
    If CodOpc = "0303" Then
       'txtDocum(9).MaxLength = 400
       txtDocum(8).MaxLength = 400
    Else
       'txtDocum(9).MaxLength = 200
       txtDocum(8).MaxLength = 200
    End If
    'SKRV T15377 INICIO
    Condi = "CD_CODI_TERC_PAIM =(SELECT CD_NIT_ENTI  FROM ENTIDAD)"
    StRegEmpnavi = fnDevDato("PARAMETROS_IMPUESTOS", "ID_TIPO_REGI_PAIM", Condi, True)
      'SKRV T16945 Se agrega validacion de interfaz a cxp INICIO
      If Interfaz_CxP() = True Then
         StOpcCxpnavi = fnDevDato("PARAMETROS_CXP", "TX_RIVARS_PCXP", NUL$, True)
      End If
      'SKRV T16945 FIN
    'SKRV T15377 FIN
    
End Sub

Sub CargRecursos() '|.DR.|
 picHelp(0).Picture = MDI_Inventarios.imgH.Picture
End Sub

Private Sub cmdOpciones_Click(Index As Integer)
    Dim vValorParcial As Currency, vValorAcumula As Currency
    Dim Formula As String, vNumero As Long, vCon As Long
    Dim Reporte As String, ArrCPB() As Variant, ArrPPTO() As Variant
    Dim FechaEntera As Long, vContable As Long, vNumeroActual As Long
    Dim Dueno As New ElTercero, UnConsecutivo As New ElConsecutivo
    Dim No_Recargar As Double
    Dim tItem As ListItem 'REOL MANTIS:0000569
    Dim SQL As String 'Consulta General
    Dim J&  '|.DR.|, R:1633
    Dim AutoDocCargado As Double    'PJCA M1069
    Dim FechaDocCargado As String   'PJCA M1069
    'Dim StRespuesta As Integer 'AASV R1883
    Dim InRespuesta As Integer 'AASV M3520
    Dim InI As Integer 'AASV M3873
    Dim StMensaje As String   'DAHV M4140
    'DAHV M4046 Inicio
    Dim InDec As Integer
    Cue_Ter = txtDocum(5).Text  'CARV R2272
    Dim IntDocu As Integer 'JACC M5623
    Dim Incont As Integer  'LDCR T6699
    Dim StEntrada As String 'SKRV T15948 trae el consecutovo de la entrada
    Dim VrArr() As Variant 'DRMG T44728-R41288 almacena resultados de consultas sql
    Dim StPrefijo As Variant 'DRMG T44728-R41288 'almacena el prefijo junto con el consecutivo

    If Aplicacion.VerDecimales_Valores(InDec) Then
    Else
       InDec = 0
    End If
    'DAHV M4046 FIn
    
   'DRMG T44728-R41288 INICIO
   If (Documento.Encabezado.TipDeDcmnt = FacturaVenta) And Index = 0 Then
      ReDim VrArr(0)
      'DRMG T45124 INICIO ' LAS SIGUIENTES TRES LINEAS SE DEJA EN COMENTARIO
      'Valores = "NU_AUTO_RFVE"
      'Condicion = "NU_COMVET_RFVE=" & txtNumero
      'Condicion = Condicion & " AND NU_DOCCOP_RFVE=" & FacturaVenta
      'Result = LoadData("R_FACTURA_VENT_ELECT", Valores, Condicion, VrArr())
      Valores = "NU_AUTO_FAFA"
      Condicion = "NU_NUCOMP_FAFA=" & txtNumero
      Condicion = Condicion & " AND NU_DOCU_FAFA=" & FacturaVenta
      Condicion = Condicion & " AND NU_COMP_FAFA=" & Documento.Encabezado.NumeroCOMPROBANTE
      Result = LoadData("R_FAVE_FAEL", Valores, Condicion, VrArr())
      'DRMG T45124 FIN
      If Result <> FAIL Then
         If Not Encontro Then
            ReDim VrArr(4)
            Valores = "NU_FACINI_FAEL,NU_FACFIN_FAEL,NU_CONFAC_FAEL,FE_AUTORI_FAEL,FE_FINAUT_FAEL"
            Condicion = "NU_MODULO_FAEL=1"
            Condicion = Condicion & " AND NU_ESTADO_FAEL=0"
            Result = LoadData("FACTURA_ELECTRONICA", Valores, Condicion, VrArr())
            If Result <> FAIL Then
               If Encontro Then
                  If CDbl(VrArr(2) + 1) < VrArr(0) Or CDbl(VrArr(2) + 1) > VrArr(1) Then
                     Call Mensaje1("El n�mero de factura no se encuentra dentro del rango autorizado", 3)
                     Exit Sub
                  End If
                  If CDate(Format(Date, "dd/mm/yyyy")) < CDate(Format(VrArr(3), "dd/mm/yyyy")) Then
                     Call Mensaje1("La Fecha de factura es menor del rango autorizado", 3)
                     Exit Sub
                  End If
                  If CDate(Format(Date, "dd/mm/yyyy")) > CDate(Format(VrArr(4), "dd/mm/yyyy")) Then
                     Call Mensaje1("La Fecha de factura es mayor al rango autorizado", 3)
                     Exit Sub
                  End If
               End If
            End If
         End If
      End If
   End If
   'DRMG T44728-R41288 FIN
    
    Result = loadctrl("CENTRO_COSTO", "NO_NOMB_CECO", "CD_CODI_CECO", cmbDEPENDENCIA, arrDPDCS(), "NU_ESTADO_CECO <> 1  ORDER BY NO_NOMB_CECO") 'APGR T1678- 7052 PNC

    InBanCont = 0 'CARV M5084
    
    '00 |.DR.|, R:1633
    If cboBodDestino.Visible And cboBodDestino.ListIndex > -1 Then
        boAfectC = Not (Not (fnBodegaCont(cboBodDestino.ItemData(cboBodDestino.ListIndex)) And fnBodegaCont(cboBodega.ItemData(cboBodega.ListIndex))) And Aplicacion.Interfaz_Contabilidad)
        If Not boAfectC And (Index = 0 Or Index = 1) Then
            If MsgBox("No se realizar� el movimiento en contabilidad, una o las 2 bodegas no admiten movimientos en contabilidad." & vbCrLf & "�Desea continuar?", vbYesNo + vbDefaultButton2 + vbQuestion, "Inventarios") <> vbYes Then Exit Sub
        End If
    ElseIf cboBodega.ListIndex > -1 Then
        boAfectC = fnBodegaCont(cboBodega.ItemData(cboBodega.ListIndex)) And Aplicacion.Interfaz_Contabilidad
    End If
    '00 Fin.
     
    'Impide el uso de guardar o anular si la interfaz con contabilidad est� activa y el mes est� bloqueado
    If Aplicacion.Interfaz_Contabilidad And (Index = 0 Or Index = 1) Then   'Solo cuando sea Guardar o Anular
        Select Case Documento.Encabezado.TipDeDcmnt
            Case Is = Entrada, Despacho, Traslado, BajaConsumo, DevolucionBaja, Salida, FacturaVenta, DevolucionFacturaVenta & _
                      DevolucionEntrada, DevolucionDespacho, AprovechaDonacion      'M899
                      'If Valida_Cierre_Contable(Month(Nowserver)) Then Exit Sub  'OMOG T18556 SE ESTABLECE EN COMENTARIO
                      If Valida_Cierre_Contable(txtDocum(2)) Then Exit Sub 'OMOG T18556 FIN
        End Select
    End If
    
    'PJCA M1881
    If Aplicacion.Interfaz_Contabilidad Or Aplicacion.Interfaz_Presupuesto Then
        If fnValidarCierreAnual Then Exit Sub
    End If
    'PJCA M1881
    
    
    'DAHV M7054 INICIO
    If Not ExisteTABLA("IN_R_ENCA_DEPE") Then
            Result = CrearTabla("IN_R_ENCA_DEPE", "NU_ENCA_RENDE, 4, 0, 0")
            If Result <> FAIL Then Result = CrearCampo("IN_R_ENCA_DEPE", "TX_CECO_RENDE", "10", 11, 1, False)
            If Result <> FAIL Then Result = CrearCampo("IN_R_ENCA_DEPE", "NU_AUTO_RENDE", "4", 0, 0, True)
    End If
    'DAHV M7054 FIN
    
    
    
    Dueno.IniXNit
    If Me.cboConsecutivo.ListIndex = -1 Then Exit Sub
    Documento.Encabezado.NumeroCOMPROBANTE = Me.cboConsecutivo.ItemData(Me.cboConsecutivo.ListIndex)
    Select Case Index
    Case Is = 0


       '-------------------------------------------------------------------------------------------------------------------------------
        'DAHV M4140 - Inicio
        If fnCantCero(GrdArticulos) = True Then
            StMensaje = "Se esta intentando grabar un movimiento, con art�culos relacionados que no"
            StMensaje = StMensaje & Chr(13) & "tienen cantidad. �Desea continuar con el proceso?"
            
            If MsgBox(StMensaje, vbYesNo + vbDefaultButton2 + vbQuestion, "Inventarios") <> vbYes Then
                 Call Mensaje1("No se guardo el documento.", 3)
                 Exit Sub
            End If
                
        End If
    
        'DAHV M4140 - Fin
        '-------------------------------------------------------------------------------------------------------------------------------

'         'AASV M3873
'         Select Case Documento.Encabezado.TipDeDcmnt
'            Case Is = BajaConsumo
'                    For Ini = 1 To GrdArticulos.Rows - 2
'                       If GrdArticulos.TextMatrix(Ini, 10) = 0 Then
'                          If MsgBox("Existen Art�culos con cantidad 0." & vbCrLf & "�Desea continuar?", vbYesNo + vbDefaultButton2 + vbQuestion, "Inventarios") = vbYes Then
'                              Exit For
'                          Else
'                              Call Mensaje1("No se pudo guardar el documento.", 3)
'                              Exit Sub
'                           End If
'                        End If
'                     Next Ini
'         End Select
'         'AASV M3873
         
         
        Me.fraTOTALES.Visible = False
        If IsNumeric(Me.txtTOTAL.Text) Then
            If Documento.Encabezado.EsNuevoDocumento And Me.GrdArticulos.Rows > 1 And (CDbl(Me.txtTOTAL.Text) > 0 Or Val(GrdArticulos.TextMatrix(GrdArticulos.Row, 10)) > 0) Then 'REOL682 smdl m1529
                If Documento.Encabezado.TipDeDcmnt = FacturaVenta Then
                    If Not vSeFactura Then
                        vSeGraba = True
                        Call CmdCuentas_Click
        'LJSA M3965------------------------------------------
'                        If Bandera = 1 Then
'                            Bandera = 0
                        If InBanCont = 1 Then 'CARV M5084
                            InBanCont = 0 'CARV M5084
                            Me.fraTOTALES.Visible = True
                            Call Mensaje1("No se pudo guardar el documento.", 3)
                            Exit Sub
                        End If
        'LJSA M3965------------------------------------------
                    End If
                Else
                    vSeFactura = True
                End If
                If Not vSeFactura Then GoTo SLRDLTRNS
                vSeFactura = False
                
               If Not (Documento.Encabezado.TipDeDcmnt = Entrada And (Not Documento.Encabezado.EsIndependiente) And Documento.Encabezado.EsNuevoDocumento) Then 'HRR M1818
                   'If Documento.Encabezado.TipDeDcmnt <> bajaconsumo Then 'HRR M3197 Unicamente la condicion no el cuerpo
                   If (Documento.Encabezado.TipDeDcmnt = Entrada) Or (Documento.Encabezado.TipDeDcmnt = ODCompra) Then 'PJCA M3234
                     'PedroJ
                     If Aplicacion.Interfaz_Presupuesto Then
                         If Me.lstDocsPadre.Visible = True Then CmdPpto_Click
                         If Result = FAIL Then Me.fraTOTALES.Visible = True: Exit Sub     'REOL M2387
                         If Valida_PPto = 0 Then Me.fraTOTALES.Visible = True: GoTo FLLCMMTRS
                     End If
                  End If 'HRR M3197
               End If 'HRR M1818
             
                If Aplicacion.Interfaz_Contabilidad Then
                  If Documento.Encabezado.TipDeDcmnt = Entrada Then
                    If Len(Trim(TxtCompra(10))) = 0 Then
                        If Not WarnMsg("No se ha especificado el n�mero de factura, se realizar�n los movimientos contables a cuentas transitorias." & vbCrLf & "�Desea guardar?") Then
                            Me.fraTOTALES.Visible = True
                            Exit Sub
                         End If
                    ElseIf TxtCompra(10) = "0" Then
                        If Not WarnMsg("No se ha especificado el n�mero de factura, se realizar�n los movimientos contables a cuentas transitorias." & vbCrLf & " �Desea guardar?") Then
                            Me.fraTOTALES.Visible = True
                            Exit Sub
                        End If
                    End If
                  End If
                End If
                'PedroJ
                
                'HRR M1818
               If Documento.Encabezado.TipDeDcmnt = Entrada And (Not Documento.Encabezado.EsIndependiente) And Documento.Encabezado.EsNuevoDocumento Then
                  Call fnGuardar_Entrada
                  Exit Sub
               End If
               'HRR M1818
                
                'HRR M3229
'                If BeginTran(STranIUp & "KARDEX") = FAIL Then GoTo FLLBGNTRN
'                Me.TxtPpto(2) = CDbl(txtTOTAL)      'PedroJ
'                NumeroCompro = WinDoc.Guardar(False)
'
'               'REOL174 Guarda el porcentaje de descuento en la BD.
'               If Documento.Encabezado.TipDeDcmnt = FacturaVenta Then
'                If grdOtrosDescuentos.Row > 0 Then
'                   If grdOtrosDescuentos.TextMatrix(grdOtrosDescuentos.Row, 3) = NUL$ Then grdOtrosDescuentos.TextMatrix(grdOtrosDescuentos.Row, 3) = 0
'                   If grdOtrosDescuentos.TextMatrix(grdOtrosDescuentos.Row, 0) = NUL$ Then grdOtrosDescuentos.TextMatrix(grdOtrosDescuentos.Row, 0) = 0
'                   Valores = "NU_AUTO_ENCA_PRPR=" & Documento.Encabezado.AutoDelDocCARGADO & Coma
'                   Valores = Valores & "NU_VALO_PRPR=" & CDbl(grdOtrosDescuentos.TextMatrix(grdOtrosDescuentos.Row, 3)) & Coma
'                   Valores = Valores & "TX_CODI_PART_PRPR=" & Comi & grdOtrosDescuentos.TextMatrix(grdOtrosDescuentos.Row, 0) & Comi '& Coma
'                   Result = DoInsertSQL("IN_PARTIPARTI", Valores)
'                   If Result = FAIL Then Call Mensaje1("Fallo al guardar el Descuento", 1): GoTo FLLCMMTRS
'                End If
'               End If
'               'REOL174
'
'                Me.fraTOTALES.Visible = True
'                If Not NumeroCompro > 0 Then GoTo FLLCMMTRS
                'HRR M3229
                
                'HRR M3229
                'JLPB T15675 INICIO SE DEJA EN COMENTARIO LA SIGUIENTE LINEA
                'Me.txtPPTO(2) = CDbl(txtTOTAL)      'PedroJ
                If Not IsNumeric(Me.TxtEntra(18)) Then Me.TxtEntra(18) = 0
                Me.TxtPpto(2) = CDbl(txtTOTAL) - CDbl(Me.TxtEntra(18))
                'JLPB T15675 FIN
                
                'JACC M6427
                'If Documento.Encabezado.TipDeDcmnt = Entrada And (TipoRegimen & TipoPers) = "10" Then
                If Documento.Encabezado.TipDeDcmnt = Entrada Then
                  If Not IsNumeric(Me.TxtEntra(18)) Then Me.TxtEntra(18) = 0
                  Me.TxtPpto(2) = CDbl(Me.TxtPpto(2)) + CDbl(Me.TxtEntra(18))
                End If
                'JACC M6427
                
                Select Case Documento.Encabezado.TipDeDcmnt
                    Case Is = Entrada, DevolucionEntrada, AprovechaDonacion, FacturaVenta, DevolucionFacturaVenta, Traslado, Despacho, DevolucionDespacho, Salida, BajaConsumo, DevolucionBaja
                       DbImpuesto = 0 'OMOG T15675
                       DbDescuento = 0 'EACT T15675
                       NumeroCompro = WinDoc.Guardar(False, False)
                       DbImpuesto = WinDoc.DbImpuesto 'OMOG T15675
                       DbDescuento = WinDoc.DbDescuento 'EACT T15675
                      'NumeroCompro = WinDoc.Guardar(False, False, "0", cboBodega.ItemData(cboBodega.ListIndex)) 'APGR M1845
                      If NumeroCompro > 0 Then txtNumero.Text = NumeroCompro 'AASV M5669
                      Me.fraTOTALES.Visible = True
                      If NumeroCompro = -1 Then GoTo FLLCMMTRS
                   Case Else
                     
                End Select
                
                'HRR M3229
                
                If Aplicacion.Interfaz_Contabilidad Then
                    continuar = False
                     
                    
                    'Se requiere calcular los montos contables antes, por eso se "extraen" de los bloques Select - End Select de "m�s abajo".
                    Select Case Documento.Encabezado.TipDeDcmnt
                        Case Entrada, DevolucionEntrada
                        
                            Call CargaGrillaInterface(Me)
                            '-----------------------------------------------------------------
                            'GMS M3559. Esto solo aplica para devoluciones de entrada
                            If Documento.Encabezado.TipDeDcmnt = DevolucionEntrada Then
                                LnAntoEncaEnid = txtNumero: txtNumero = Documento.Encabezado.ConsecComprobante
                                'Trae los impuestos del comprobante
                                Call ImpuestosDescuentos
                                'Recalcula los valores
                                Call calcular_entra(True)
                                'Retorna el Nro. del Comprobante anterior
                                txtNumero = LnAntoEncaEnid
                            Else
                                Call calcular_entra(True): LnAntoEncaEnid = 0
                            End If
                            '-----------------------------------------------------------------
                        Case AprovechaDonacion
                            boFact = True '|.DR.| En este tipo de movimiento la ausencia de la factura no tiene nada que ver, el movimiento debe hacerse normalmente. M:1307
                            Call CargaGrillaInterface(Me)
                            Call calcular_entra(True)
                        'HRR R1801
                        'Case Salida
                        '    Call calcular_salidaA
                        'HRR R1801
                        Case Traslado, Despacho, DevolucionDespacho
                            
                            Call calcular_Traslado
                            
                         'HRR M3229
                         Case FacturaVenta, DevolucionFacturaVenta
                               Call calcular_venta
                              'APGR T1565 PNC - INICIO
'                              Select Case Documento.Encabezado.TipDeDcmnt
'                              Case FacturaVenta
'                                   Call calcular_venta
'                              Case DevolucionFacturaVenta
'                                    If lstDocsPadre.Visible = True And lstDocsPadre.ListCount > 0 Then
'                                        Call calcular_venta
'                                    Else
'                                        Call Mensaje1("No se puede generar la devoluci�n de factura sin tener un documento", 3): GoTo FLLBGNTRN
'                                    End If
'                              End Select
                              'APGR T1565 PNC - FIN
                         'HRR M3229
                    End Select
                    
                        '01 |.DR.|, R:1633
                        stDesc = WinDoc.ElConsecutivo.Nombre
                        Select Case Documento.Encabezado.TipDeDcmnt
                        'Case Is = Entrada, DevolucionEntrada, Salida, TRASLADO, Despacho, DevolucionDespacho 'HRR R1801
                        ' DAHV T6622 - INICIO
                        ' Case Is = Entrada, DevolucionEntrada, Traslado, Despacho, DevolucionDespacho 'HRR R1801
                          Case Is = Entrada, DevolucionEntrada, Traslado, Despacho, DevolucionDespacho, AprovechaDonacion
                        ' DAHV T6622 - FIN
                           stDesc = IIf(boAfectC, WinDoc.ElConsecutivo.Nombre, "LA BODEGA NO AFECTA CONT.") '|.DR.|, R:1633
                           If Not boAfectC Then
                                
                             'APGR T8174 PNC- INICIO
                             continuar = WarnMsg("Los movimientos contables se realizar�n con valor cero." & vbCrLf & "�Desea continuar?")
                             
                             If continuar = True Then
                             'APGR T8174 PNC- FIN
                                'INICIO LDCR T6699
                                For Incont = LBound(McuenD) To UBound(McuenD)
                                      Mcuen(Incont, 1) = McuenD(Incont, 1)
                                      Mcuen(Incont, 2) = McuenD(Incont, 2)
                                      Mcuen(Incont, 3) = McuenD(Incont, 3)
                                      Mcuen(Incont, 4) = McuenD(Incont, 4)
                                      Mcuen(Incont, 5) = McuenD(Incont, 5)
                                      Mcuen(Incont, 6) = McuenD(Incont, 6)
                                Next
                                'FIN LDCR T6699
                                 
                                  For J& = LBound(Mcuen) To UBound(Mcuen)
                                      If Mcuen(J&, 1) <> "" Then
                                          Mcuen(J&, 2) = "0"
                                          McuenD(J&, 2) = "0"
                                   'APGR T8174 PNC- Se deja mensaje (devuelve cambios)
                                     'APGR T8174 - Se deja en comentario ELSE, dado que con la variable boAfectC se valida si afecta contabilidad,
                                      'como no afecta contabilidad no se debe realizar parametrizacion contable por lo cualno debe visualizarse el mensaje.
                                      'INICIO LDCR T6699 SE COLOCA ELSE
                                      Else
                                          continuar = True
                                          If Mcuen(J&, 1) = "" And (Mcuen(J&, 2) <> "" And Mcuen(J&, 3) <> "") Then
                                               continuar = False
                                               Mensaje1 "No se ha realizado toda la parametrizaci�n contable", 3
                                               Exit For
                                          End If
                                      End If
                                     'APGR T8174 - fin
                                  'FIN LDCR T6699
                                  Next J&
                             'APGR T8174 PNC - INICIO
                             End If
                               'INICIO LDCR T6699
                               'APGR T8174 PNC - Se deja en comentario porque se sube el codigo.
                               'continuar = WarnMsg("Los movimientos contables se realizar�n con valor cero." & vbCrLf & "�Desea continuar?")
'                               If continuar = True Then
'                                    continuar = WarnMsg("Los movimientos contables se realizar�n con valor cero." & vbCrLf & "�Desea continuar?")
'                               End If
                               'FIN LDCR T6699
                           Else
                               'INICIO LDCR T10119
'                               If Documento.Encabezado.TipDeDcmnt = Entrada And ArrIVA(0) > 0 Then
'                                Result = FrmCuentas.ValIva(True, True)
'                               Else
'                                Result = FrmCuentas.ValIva(False, False)
'                               End If
                               'FIN LDCR T10119
                               muestra_cuenta "S", Me
                           End If
                        'HRR M3229
                        ' DAHV T6622 - INICIO
                        ' Case Is = AprovechaDonacion, FacturaVenta, DevolucionFacturaVenta
                          Case Is = FacturaVenta, DevolucionFacturaVenta
                        ' DAHV T6622 - FIN
                           muestra_cuenta "S", Me
                        'HRR M3229
                        
                        End Select
                        '01 Fin.
                    
                End If 'HRR M3229
                
                                    
               'HRR M3229
               Select Case Documento.Encabezado.TipDeDcmnt
               Case Is = Entrada, DevolucionEntrada, AprovechaDonacion, FacturaVenta, DevolucionFacturaVenta, Traslado, Despacho, DevolucionDespacho
               
                   If Aplicacion.Interfaz_Contabilidad Then If Not continuar Then GoTo FLLBGNTRN
                   
                   If BeginTran(STranIUp & "KARDEX") = FAIL Then GoTo FLLBGNTRN
                    NumeroCompro = WinDoc.Guardar(False, True)
                   'NumeroCompro = WinDoc.Guardar(False, True, "0", cboBodega.ItemData(cboBodega.ListIndex)) 'APGR M1845
                   If NumeroCompro > 0 Then txtNumero.Text = NumeroCompro 'AASV M5669
                  
                  'REOL174 Guarda el porcentaje de descuento en la BD.
                  If Documento.Encabezado.TipDeDcmnt = FacturaVenta Then
                     If grdOtrosDescuentos.Row > 0 Then
                        'JLPB T41054 INICIO Se deja en comentario
                        'If grdOtrosDescuentos.TextMatrix(grdOtrosDescuentos.Row, 3) = NUL$ Then grdOtrosDescuentos.TextMatrix(grdOtrosDescuentos.Row, 3) = 0
                        'If grdOtrosDescuentos.TextMatrix(grdOtrosDescuentos.Row, 0) = NUL$ Then grdOtrosDescuentos.TextMatrix(grdOtrosDescuentos.Row, 0) = 0
                        'Valores = "NU_AUTO_ENCA_PRPR=" & Documento.Encabezado.AutoDelDocCARGADO & Coma
                        'Valores = Valores & "NU_VALO_PRPR=" & CDbl(grdOtrosDescuentos.TextMatrix(grdOtrosDescuentos.Row, 3)) & Coma
                        'Valores = Valores & "TX_CODI_PART_PRPR=" & Comi & grdOtrosDescuentos.TextMatrix(grdOtrosDescuentos.Row, 0) & Comi '& Coma
                        'Result = DoInsertSQL("IN_PARTIPARTI", Valores)
                        For InI = 1 To grdOtrosDescuentos.Row
                           If Result <> FAIL Then
                              If grdOtrosDescuentos.TextMatrix(InI, 3) = NUL$ Then grdOtrosDescuentos.TextMatrix(InI, 3) = 0
                              If grdOtrosDescuentos.TextMatrix(InI, 0) = NUL$ Then grdOtrosDescuentos.TextMatrix(InI, 0) = 0
                              Valores = "NU_AUTO_ENCA_PRPR=" & Documento.Encabezado.AutoDelDocCARGADO & Coma
                              Valores = Valores & "NU_VALO_PRPR=" & CDbl(grdOtrosDescuentos.TextMatrix(InI, 3)) & Coma
                              Valores = Valores & "TX_CODI_PART_PRPR=" & Comi & grdOtrosDescuentos.TextMatrix(InI, 0) & Comi '& Coma
                              Result = DoInsertSQL("IN_PARTIPARTI", Valores)
                           End If
                        Next
                        'JLPB T41054 FIN
                        If Result = FAIL Then Call Mensaje1("Fallo al guardar el Descuento", 1): GoTo FLLCMMTRS
                     End If
                  End If
                  'REOL174
                  If Not NumeroCompro > 0 Then GoTo FLLCMMTRS
                  
               Case Is = Salida, BajaConsumo, DevolucionBaja
                  If Not Aplicacion.Interfaz_Contabilidad Then
                     If BeginTran(STranIUp & "KARDEX") = FAIL Then GoTo FLLBGNTRN
                     NumeroCompro = WinDoc.Guardar(False, True)
                     If NumeroCompro > 0 Then txtNumero.Text = NumeroCompro 'AASV M5669
                      
                     Me.fraTOTALES.Visible = True
                   
                      If Not NumeroCompro > 0 Then GoTo FLLCMMTRS
                  End If
               Case Else
                  
                  If BeginTran(STranIUp & "KARDEX") = FAIL Then GoTo FLLBGNTRN
                  'JAUM T28560 Inicio
                  If Documento.Encabezado.TipDeDcmnt = AnulaODCompra Or Documento.Encabezado.TipDeDcmnt = AnulaEntrada Then
                     AutoDocCargado = Documento.Encabezado.AutoDelDocCARGADO
                  End If
                  'JAUM T28560 Fin
                  NumeroCompro = WinDoc.Guardar(False, True)
                  If NumeroCompro > 0 Then txtNumero.Text = NumeroCompro 'AASV M5669
                   
                   Me.fraTOTALES.Visible = True
                   
                   If Not NumeroCompro > 0 Then GoTo FLLCMMTRS
                   
               End Select
               
               'HRR M3229
                    
         If Aplicacion.Interfaz_Contabilidad Then 'HRR M3229
            Select Case Documento.Encabezado.TipDeDcmnt
                     Case Is = Entrada, DevolucionEntrada
'                        Call CargaGrillaInterface(Me)
'                        Call calcular_entra(True)
                        
                        'If Not continuar Then GoTo FLLBGNTRN 'HRR M3229
                        
    '                    Call Guardar_Movimiento_Contable("ENTR", fechag, Buscar_Comprobante_Concepto(0), TxtEntra(0), Mid("ENTRADA A ALMACEN " & TxtEntra(9), 1, 250), NUL$)
                        If Documento.Encabezado.TipDeDcmnt = Entrada Then
                            comprobante = Buscar_Comprobante_Concepto(Entrada)
                            vNumero = Buscar_Numero_Comprobante(comprobante)
                            'Call Guardar_Movimiento_Contable("ENTR", Documento.Encabezado.FechaDocumento, Comprobante, vNumero, Left(Trim(stDesc) & Right(String(7, "0") & NumeroCompro, 7) & ". " & Documento.Encabezado.Observaciones, 250), NUL$)
                            'Call Guardar_Movimiento_Contable("ENTR", Documento.Encabezado.FechaDocumento, Comprobante, vNumero, "FACTURA No." & (Me.TxtCompra(10).Text) & "" & Left(Trim(stDesc) & Right(String(7, "0") & NumeroCompro, 7) & ". " & Documento.Encabezado.Observaciones, 250), NUL$) 'AASV R1323
                            'Call Guardar_Movimiento_Contable("ENTR", Documento.Encabezado.FechaDocumento, Comprobante, vNumero, "FACTURA No." & (Me.TxtCompra(10).Text) & " " & Left(Trim(stDesc) & Right(String(7, "0") & NumeroCompro, 7) & ". " & Documento.Encabezado.Observaciones, 250), NUL$) 'AASV M3485
                            'INICIO LDCR 2da PARTE R9033/T9930
                            'Call Guardar_Movimiento_Contable("ENTR", Documento.Encabezado.FechaDocumento, Comprobante, vNumero, "FACTURA No." & UCase(Me.TxtCompra(10).Text) & " " & Left(Trim(stDesc) & Right(String(7, "0") & NumeroCompro, 7) & ". " & Documento.Encabezado.Observaciones, 250), NUL$) 'JACC R2221
                            'Call Guardar_Movimiento_Contable("ENTR", Documento.Encabezado.FechaDocumento, Comprobante, vNumero, "FACTURA No." & UCase(Me.TxtCompra(10).Text) & " " & Left(Trim(stDesc) & Right(String(7, "0") & NumeroCompro, 7) & ". " & Documento.Encabezado.Observaciones, 250), NUL$, Documento.Encabezado.BodegaORIGEN, Documento.Encabezado.TipDeDcmnt) 'JACC R2221'SKRV T24261/R22366 comentario
                            'Call Guardar_Movimiento_Contable("ENTR", Documento.Encabezado.FechaDocumento, Comprobante, vNumero, "FACTURA No." & UCase(Me.TxtCompra(10).Text) & " " & Left(Trim(stDesc) & Right(String(7, "0") & NumeroCompro, 7) & ". " & Documento.Encabezado.Observaciones, 250), NUL$, Documento.Encabezado.BodegaORIGEN, Documento.Encabezado.TipDeDcmnt, "0601", txtNumero) 'SKRV T24261/R22366 SKRV T24951 COMENTARIO
                            Call Guardar_Movimiento_Contable("ENTR", Documento.Encabezado.FechaDocumento, comprobante, vNumero, "FACTURA No." & UCase(Me.TxtCompra(10).Text) & " " & Left(Trim(stDesc) & Right(String(7, "0") & NumeroCompro, 7) & ". " & Documento.Encabezado.Observaciones, 250), NUL$, Documento.Encabezado.BodegaORIGEN, Documento.Encabezado.TipDeDcmnt, fnDevDato("MODU_MOV_NIIF", "NU_CODMO_MODU", "TX_DESCM_MODU = 'Entrada de Almac�n'", True), txtNumero) 'SKRV T24951
                            'FINICIO LDCR 2da PARTE R9033/T9930

                            If Result <> FAIL Then Result = DoUpdate("IN_ENCABEZADO", "NU_COMP_CONTA_ENCA=" & vNumero, "NU_AUTO_COMP_ENCA=" & Documento.Encabezado.NumeroCOMPROBANTE & " AND NU_COMP_ENCA=" & NumeroCompro)
                            If Result <> FAIL Then
                                'AASV M5669 Nota: 26367 Inicio
                                'Result = DoDelete("IN_R_ENCA_IMDE", "NU_AUTO_ENCA_ENID=" & CDbl(txtNumero))
                                Result = DoDelete("IN_R_ENCA_IMDE", "NU_AUTO_ENCA_ENID=" & CDbl(txtNumero), "N") 'AASV M5855 Se adiciona parametro para que no valide siel usuario tiene permiso de borrado
                                If Result <> FAIL Then
                                'AASV M5669 Nota: 26367 Fin
                                    Call Guardar_ImpuestoDescuento
                                End If 'AASV M5669 Nota: 26367
                                
                                If Result = FAIL Then GoTo FLLCMMTRS
                            End If
                            If Result = FAIL Then GoTo FLLCMMTRS
                        End If
                        If Documento.Encabezado.TipDeDcmnt = DevolucionEntrada Then
                            comprobante = Buscar_Comprobante_Concepto(DevolucionEntrada)
                            vNumero = Buscar_Numero_Comprobante(comprobante)
                            'Call Guardar_Movimiento_Contable("DVEN", Documento.Encabezado.FechaDocumento, Comprobante, vNumero, Left(Trim(stDesc) & Right(String(7, "0") & NumeroCompro, 7) & ". " & Documento.Encabezado.Observaciones, 250), NUL$) 'SKRV T24261/R22366 comentario
                            'Call Guardar_Movimiento_Contable("DVEN", Documento.Encabezado.FechaDocumento, Comprobante, vNumero, Left(Trim(stDesc) & Right(String(7, "0") & NumeroCompro, 7) & ". " & Documento.Encabezado.Observaciones, 250), NUL$, , , "0604", txtNumero) 'SKRV T24261/R22366 SKRV T24951 COMENTARIO
                            Call Guardar_Movimiento_Contable("DVEN", Documento.Encabezado.FechaDocumento, comprobante, vNumero, Left(Trim(stDesc) & Right(String(7, "0") & NumeroCompro, 7) & ". " & Documento.Encabezado.Observaciones, 250), NUL$, , , fnDevDato("MODU_MOV_NIIF", "NU_CODMO_MODU", "TX_DESCM_MODU = 'Devoluci�n a Proveedor'", True), txtNumero) 'SKRV T24951
                            If Result = FAIL Then GoTo FLLCMMTRS
                            Result = DoUpdate("IN_ENCABEZADO", "NU_COMP_CONTA_ENCA=" & vNumero, "NU_AUTO_COMP_ENCA=" & Documento.Encabezado.NumeroCOMPROBANTE & " AND NU_COMP_ENCA=" & NumeroCompro)
                        End If
                        If Aplicacion.Interfaz_CxP And Documento.Encabezado.TipDeDcmnt = DevolucionEntrada Then
                            Me.TxtCompra(12).Text = Me.TxtEntra(10).Text
                            'Me.TxtCompra(14).Text = Me.TxtEntra(12).Text
                            Me.TxtCompra(14).Text = Aplicacion.Formatear_Valor(CDbl(Me.TxtEntra(12).Text)) 'SKRV T 15434
                            Me.TxtCompra(18).Text = Me.TxtEntra(18).Text
                            Me.TxtCompra(16).Text = Me.TxtEntra(14).Text
                            Me.TxtCompra(10).Text = Me.TxtEntra(10).Text
                            Me.TxtCompra(13).Text = Me.TxtEntra(11).Text
                            Me.TxtCompra(15).Text = Me.TxtEntra(13).Text
                            Me.TxtCompra(19).Text = Me.TxtEntra(19).Text
                            Me.TxtCompra(20).Text = Me.TxtEntra(20).Text
                            Me.TxtCompra(17).Text = Me.TxtEntra(15).Text
                            Me.TxtCompra(11).Text = "DEVOLUCION A PROVEEDOR" 'Buscar_Numero_Teso
'                            Call Graba_NotaDeb(Documento.Encabezado.FechaDocumento, Documento.Encabezado.FechaVencimiento, "NDEP", Buscar_Numero_Teso(DevolucionEntrada))
                            Call Graba_NotaDeb(Documento.Encabezado.FechaDocumento, Buscar_Numero_Teso(DevolucionEntrada))      'DEPURACION DE CODIGO
                            If Result = FAIL Then GoTo FLLBGNTRN
                        End If

                        Call GuardaValoresParticulatres 'HRR M1819
                        'If Documento.Encabezado.TipDeDcmnt = DevolucionEntrada Then Call GuardaValoresParticulatres 'HRR M1819
                        
                    Case Is = AprovechaDonacion
'                        Call CargaGrillaInterface(Me)
'                        Call calcular_entra(True)
                        'muestra_cuenta "S", Me 'HRR M3229
                        'If Not continuar Then GoTo FLLBGNTRN 'HRR M3229
                        comprobante = Buscar_Comprobante_Concepto(AprovechaDonacion)
                        vNumero = Buscar_Numero_Comprobante(comprobante)
                        'SKRV T24951 INICIO
                        'Call Guardar_Movimiento_Contable("APDN", Documento.Encabezado.FechaDocumento, Comprobante, vNumero, Left(Trim(WinDoc.ElConsecutivo.Nombre) & Right(String(7, "0") & NumeroCompro, 7) & ". " & Documento.Encabezado.Observaciones, 250), NUL$) SE ESTABLECE EN COMENTARIO
                        Call Guardar_Movimiento_Contable("APDN", Documento.Encabezado.FechaDocumento, comprobante, vNumero, Left(Trim(WinDoc.ElConsecutivo.Nombre) & Right(String(7, "0") & NumeroCompro, 7) & ". " & Documento.Encabezado.Observaciones, 250), NUL$, , , fnDevDato("MODU_MOV_NIIF", "NU_CODMO_MODU", "TX_DESCM_MODU = 'Aprovechamientos/Donaciones'", True), txtNumero)
                        'SKRV T24951 FIN
                        If Result = FAIL Then GoTo FLLCMMTRS 'DAHV M3534
                        Result = DoUpdate("IN_ENCABEZADO", "NU_COMP_CONTA_ENCA=" & vNumero, "NU_AUTO_COMP_ENCA=" & Documento.Encabezado.NumeroCOMPROBANTE & " AND NU_COMP_ENCA=" & NumeroCompro)
                        
                    Case Is = Salida
'                        Call calcular_salidaA
                        'muestra_cuenta "S", Me 'HRR M1468
                        
                        'HRR R1801
'                        If Not continuar Then GoTo FLLBGNTRN
'                        Comprobante = Buscar_Comprobante_Concepto(Salida)
'                        vNumero = Buscar_Numero_Comprobante(Comprobante)
'                        Call Guardar_Movimiento_Contable("SALI", Documento.Encabezado.FechaDocumento, Comprobante, vNumero, Left(Trim(stDesc) & Right(String(7, "0") & NumeroCompro, 7) & ". " & Documento.Encabezado.Observaciones, 250), NUL$)
'                        If Result = FAIL Then GoTo FLLBGNTRN 'HRR M1468
'                        Result = DoUpdate("IN_ENCABEZADO", "NU_COMP_CONTA_ENCA=" & vNumero, "NU_AUTO_COMP_ENCA=" & Documento.Encabezado.NumeroCOMPROBANTE & " AND NU_COMP_ENCA=" & NumeroCompro)
                        'HRR R1801
                        
                        'HRR R1801
                        calcular_salidaD
                        Me.frmINVENTARIOS.Visible = False
                        Call CentraMarco(Me.frmBAJA)
                        Me.frmBAJA.Caption = "Salida"
                        Me.frmBAJA.Visible = True
                        vGrabaBaja = True
                        Exit Sub
                        'HRR R1801
                        
                    Case Is = FacturaVenta
                        'HRR M3229
                        'Call calcular_venta
                        'muestra_cuenta "S", Me
                        'If Not continuar Then GoTo FLLBGNTRN
                        'HRR M3229
                        
                        comprobante = Buscar_Comprobante_Concepto(FacturaVenta)
                        vNumero = Buscar_Numero_Comprobante(comprobante)
                        'Call Guardar_Movimiento_Contable("VENT", Documento.Encabezado.FechaDocumento, Comprobante, vNumero, Left(Trim(WinDoc.ElConsecutivo.Nombre) & Right(String(7, "0") & NumeroCompro, 7) & ". " & Documento.Encabezado.Observaciones, 250), NUL$) 'SKRV T24261/R22366 comentario
                        'Call Guardar_Movimiento_Contable("VENT", Documento.Encabezado.FechaDocumento, Comprobante, vNumero, Left(Trim(WinDoc.ElConsecutivo.Nombre) & Right(String(7, "0") & NumeroCompro, 7) & ". " & Documento.Encabezado.Observaciones, 250), NUL$, , , "0603", txtNumero) 'SKRV T24261/R22366 SKRV T24951 COMENTARIO
                        Call Guardar_Movimiento_Contable("VENT", Documento.Encabezado.FechaDocumento, comprobante, vNumero, Left(Trim(WinDoc.ElConsecutivo.Nombre) & Right(String(7, "0") & NumeroCompro, 7) & ". " & Documento.Encabezado.Observaciones, 250), NUL$, , , fnDevDato("MODU_MOV_NIIF", "NU_CODMO_MODU", "TX_DESCM_MODU = 'Venta de Inventario'", True), txtNumero) 'SKRV T24951
                        If Result = FAIL Then GoTo FLLCMMTRS 'DAHV M3534
                        Result = DoUpdate("IN_ENCABEZADO", "NU_COMP_CONTA_ENCA=" & vNumero, "NU_AUTO_COMP_ENCA=" & Documento.Encabezado.NumeroCOMPROBANTE & " AND NU_COMP_ENCA=" & NumeroCompro)
                        Call ValorCreditoFactura
                        'If IsNumeric(Me.TxtVenta(15).Text) Then
                        If IsNumeric(Me.TxtVenta(15).Text) And fnDevDato("CONCEPTO", "CD_CODI_CONC", "CD_CODI_CONC='VINV'", True) <> NUL$ Then 'CARV M5445 se agrago la validacion para confirmar la existencia del concepto de codigo VINV ya que es obligatorio para la relacion entre las tablas concepto y cxc
                            If Aplicacion.Interfaz_CxC And CCur(TxtVenta(15).Text) > 0 Then
    '                        Conse_CXC = Buscar_Numero_Concepto("VINV") 'Buscar_Numero_Teso
    '                        Call Graba_cxc(Documento.Encabezado.FechaDocumento, Documento.Encabezado.FechaVencimiento, "VINV", Buscar_Numero_Concepto("VINV"))
                                Call Graba_cxc(Documento.Encabezado.FechaDocumento, Documento.Encabezado.FechaVencimiento, "VINV", Buscar_Numero_Teso(FacturaVenta))
                            End If
                            If Aplicacion.Interfaz_Presupuesto And CCur(TxtVenta(15).Text) > 0 Then
                                vGiroIng = Leer_Consecutivo_Ppto("NU_CONS_GIRI") + 1
                                Call Grabar_Presupuesto(0)
                                If Result = FAIL Then GoTo FLLCMMTRS
                            End If
    'ENA, ENTRADA DE ALMACEN, 3
    'SLD, SALIDA DE BODEGA, 19
    'VTI, VENTA DE MERCANCIA, 11
    'BJP, BAJA DE PACIENTES, 8
    'DDC, DEVOLUCION DE CLIENTE, 12
    'DAP, DEVOLUCION A PROVEEDOR, 4
    'APD, APROVECHAMIENTOS/DONACIONES, 26
    'AJI, AJUSTES X INFLACION, 31
    'FVT, FACTURA DE VENTA, 11
    '    NoDefinido = 0
    '    Cotizacion = 1
    '    AnulaCotizacion = 20
    '    ODCompra = 2
    '                    Else
                            If Aplicacion.Interfaz_Presupuesto And (CCur(Me.txtValorFactura.Text) - CCur(Me.TxtVenta(15).Text)) > 0 Then
                                vGiroIng = Leer_Consecutivo_Ppto("NU_CONS_GIRI") + 1
                                Call Grabar_Presupuesto(1)
                                If Result = FAIL Then GoTo FLLCMMTRS
                            End If
                        End If
                        Call GuardaValoresParticulatres
                    Case Is = DevolucionFacturaVenta
                        'HRR M3229
                        'Call calcular_venta ''calcular_devo
                        'muestra_cuenta "S", Me
                        'If Not continuar Then GoTo FLLBGNTRN
                        'HRR M3229
                        
                        comprobante = Buscar_Comprobante_Concepto(DevolucionFacturaVenta)
                        vNumero = Buscar_Numero_Comprobante(comprobante)
                        'Call Guardar_Movimiento_Contable("DEVC", Documento.Encabezado.FechaDocumento, Comprobante, vNumero, Left(Trim(WinDoc.ElConsecutivo.Nombre) & Right(String(7, "0") & NumeroCompro, 7) & ". " & Documento.Encabezado.Observaciones, 250), NUL$) 'SKRV T24261/R22366 comentario
                        'Call Guardar_Movimiento_Contable("DEVC", Documento.Encabezado.FechaDocumento, Comprobante, vNumero, Left(Trim(WinDoc.ElConsecutivo.Nombre) & Right(String(7, "0") & NumeroCompro, 7) & ". " & Documento.Encabezado.Observaciones, 250), NUL$, , , "0605", txtNumero) 'SKRV T24261/R22366 SKRV T24951 COMENTARIO
                        Call Guardar_Movimiento_Contable("DEVC", Documento.Encabezado.FechaDocumento, comprobante, vNumero, Left(Trim(WinDoc.ElConsecutivo.Nombre) & Right(String(7, "0") & NumeroCompro, 7) & ". " & Documento.Encabezado.Observaciones, 250), NUL$, , , fnDevDato("MODU_MOV_NIIF", "NU_CODMO_MODU", "TX_DESCM_MODU = 'Devoluci�n de Clientes'", True), txtNumero) 'SKRV T24951
                        If Result = FAIL Then GoTo FLLCMMTRS 'DAHV M3534
                        Result = DoUpdate("IN_ENCABEZADO", "NU_COMP_CONTA_ENCA=" & vNumero, "NU_AUTO_COMP_ENCA=" & Documento.Encabezado.NumeroCOMPROBANTE & " AND NU_COMP_ENCA=" & NumeroCompro)
                        Call ValorCreditoFactura
                       'REOL M2189
'                        If IsNumeric(Me.TxtVenta(15).Text) Then
'                                Call Graba_cxc(Documento.Encabezado.FechaDocumento, Documento.Encabezado.FechaVencimiento, "NCRE", Buscar_Numero_Concepto(DevolucionFacturaVenta))
                        If Aplicacion.Interfaz_CxC And IsNumeric(Me.TxtVenta(15).Text) > 0 Then
                            Call Graba_cxc(Documento.Encabezado.FechaDocumento, Documento.Encabezado.FechaVencimiento, "NCRE", Buscar_Numero_Teso(DevolucionFacturaVenta))
                        End If
                       'REOL M2189
                        If Aplicacion.Interfaz_Presupuesto And CCur(TxtVenta(15).Text) > 0 Then
                            vGiroIng = Leer_Consecutivo_Ppto("NU_CONS_GIRI") + 1
                            Call Grabar_Presupuesto(5)
                        End If
                       'REOL M2189
'                        If Aplicacion.Interfaz_CxC Then
'                            Call Graba_cxc(Documento.Encabezado.FechaDocumento, Documento.Encabezado.FechaVencimiento, "NCRE", Buscar_Numero_Concepto(DevolucionFacturaVenta))
'                        End If
                       'REOL M2189
                    Case Is = BajaConsumo, DevolucionBaja
                        calcular_salidaD
                        Me.frmINVENTARIOS.Visible = False
                        Call CentraMarco(Me.frmBAJA)
                        Me.frmBAJA.Visible = True
                        vGrabaBaja = True
                        Exit Sub
                    Case Is = Traslado, Despacho
'                        Call calcular_Traslado
                        'muestra_cuenta "S", Me 'HRR M1468
                        'If Not continuar Then GoTo FLLBGNTRN 'HRR M3229
                        If Documento.Encabezado.TipDeDcmnt = Despacho Then
                            'Result = DoUpdate("IN_COMPROBANTE", "NU_COMP_COMP=" & NumeroCompro, "NU_AUTO_COMP=" & Me.cboConsecutivo.ItemData(Me.cboConsecutivo.ListIndex))   'HRR M3229
                            comprobante = Buscar_Comprobante_Concepto(Despacho)
                            vNumero = Buscar_Numero_Comprobante(comprobante)
                            'Call Guardar_Movimiento_Contable("DSP", Documento.Encabezado.FechaDocumento, Comprobante, vNumero, Left(Trim(stDesc) & Right(String(7, "0") & NumeroCompro, 7) & ". " & Documento.Encabezado.Observaciones, 250), NUL$) 'SKRV T24261/R22366 comentario
                            'Call Guardar_Movimiento_Contable("DSP", Documento.Encabezado.FechaDocumento, Comprobante, vNumero, Left(Trim(stDesc) & Right(String(7, "0") & NumeroCompro, 7) & ". " & Documento.Encabezado.Observaciones, 250), NUL$, , , "0607", txtNumero) 'SKRV T24261/R22366 SKRV T24951 COMENTARIO
                            'Call Guardar_Movimiento_Contable("DSP", Documento.Encabezado.FechaDocumento, comprobante, vNumero, Left(Trim(stDesc) & Right(String(7, "0") & NumeroCompro, 7) & ". " & Documento.Encabezado.Observaciones, 250), NUL$, , , fnDevDato("MODU_MOV_NIIF", "NU_CODMO_MODU", "TX_DESCM_MODU = 'Despacho de Inventario'", True), txtNumero) 'SKRV T24951 'JLPB T25784-R25328 SE DEJA EN COMENTARIO
                            Call Guardar_Movimiento_Contable("DSP", Documento.Encabezado.FechaDocumento, comprobante, vNumero, Left(Trim(stDesc) & Right(String(7, "0") & NumeroCompro, 7) & ". " & Documento.Encabezado.Observaciones, 250), NUL$, , Documento.Encabezado.TipDeDcmnt, fnDevDato("MODU_MOV_NIIF", "NU_CODMO_MODU", "TX_DESCM_MODU = 'Despacho de Inventario'", True), txtNumero) 'JLPB T25784-R25328
                            If Result = FAIL Then GoTo FLLCMMTRS 'DAHV M3534
                            Result = DoUpdate("IN_ENCABEZADO", "NU_COMP_CONTA_ENCA=" & vNumero, "NU_AUTO_COMP_ENCA=" & Documento.Encabezado.NumeroCOMPROBANTE & " AND NU_COMP_ENCA=" & NumeroCompro)
                        End If
                        If Documento.Encabezado.TipDeDcmnt = Traslado Then
                            comprobante = Buscar_Comprobante_Concepto(Traslado)
                            vNumero = Buscar_Numero_Comprobante(comprobante)
                            'Call Guardar_Movimiento_Contable("TRS", Documento.Encabezado.FechaDocumento, Comprobante, vNumero, Left(Trim(stDesc) & Right(String(7, "0") & NumeroCompro, 7) & ". " & Documento.Encabezado.Observaciones, 250), NUL$) 'SKRV T24261/R22366 comentario
                            'Call Guardar_Movimiento_Contable("TRS", Documento.Encabezado.FechaDocumento, Comprobante, vNumero, Left(Trim(stDesc) & Right(String(7, "0") & NumeroCompro, 7) & ". " & Documento.Encabezado.Observaciones, 250), NUL$, , , "0606", txtNumero) 'SKRV T24261/R22366 SKRV T24951 COMENTARIO
                            'Call Guardar_Movimiento_Contable("TRS", Documento.Encabezado.FechaDocumento, comprobante, vNumero, Left(Trim(stDesc) & Right(String(7, "0") & NumeroCompro, 7) & ". " & Documento.Encabezado.Observaciones, 250), NUL$, , , fnDevDato("MODU_MOV_NIIF", "NU_CODMO_MODU", "TX_DESCM_MODU = 'Traslados entre Bodegas'", True), txtNumero) 'SKRV T24951 'JLPB T25784-R25328 SE DEJA EN COMENTARIO
                            Call Guardar_Movimiento_Contable("TRS", Documento.Encabezado.FechaDocumento, comprobante, vNumero, Left(Trim(stDesc) & Right(String(7, "0") & NumeroCompro, 7) & ". " & Documento.Encabezado.Observaciones, 250), NUL$, , Documento.Encabezado.TipDeDcmnt, fnDevDato("MODU_MOV_NIIF", "NU_CODMO_MODU", "TX_DESCM_MODU = 'Traslados entre Bodegas'", True), txtNumero) 'JLPB T25784-R25328
                            If Result = FAIL Then GoTo FLLCMMTRS 'DAHV M3534
                            Result = DoUpdate("IN_ENCABEZADO", "NU_COMP_CONTA_ENCA=" & vNumero, "NU_AUTO_COMP_ENCA=" & Documento.Encabezado.NumeroCOMPROBANTE & " AND NU_COMP_ENCA=" & NumeroCompro)
                        End If
                    Case Is = DevolucionDespacho
'                        Call calcular_Traslado
                        'muestra_cuenta "S", Me 'HRR M1468
                        'If Not continuar Then GoTo FLLBGNTRN 'HRR M3229
                        If Documento.Encabezado.TipDeDcmnt = DevolucionDespacho Then
                            'Result = DoUpdate("IN_COMPROBANTE", "NU_COMP_COMP=" & NumeroCompro, "NU_AUTO_COMP=" & Me.cboConsecutivo.ItemData(Me.cboConsecutivo.ListIndex))   'HRR M3229
                            comprobante = Buscar_Comprobante_Concepto(DevolucionDespacho)
                            vNumero = Buscar_Numero_Comprobante(comprobante)
                            'Call Guardar_Movimiento_Contable("DDP", Documento.Encabezado.FechaDocumento, Comprobante, vNumero, Left(Trim(stDesc) & Right(String(7, "0") & NumeroCompro, 7) & ". " & Documento.Encabezado.Observaciones, 250), NUL$) 'SKRV T24261/R22366 comentario
                            'Call Guardar_Movimiento_Contable("DDP", Documento.Encabezado.FechaDocumento, Comprobante, vNumero, Left(Trim(stDesc) & Right(String(7, "0") & NumeroCompro, 7) & ". " & Documento.Encabezado.Observaciones, 250), NUL$, , , "0608", txtNumero) 'SKRV T24261/R22366 SKRV T24951 COMENTARIO
                            'Call Guardar_Movimiento_Contable("DDP", Documento.Encabezado.FechaDocumento, comprobante, vNumero, Left(Trim(stDesc) & Right(String(7, "0") & NumeroCompro, 7) & ". " & Documento.Encabezado.Observaciones, 250), NUL$, , , fnDevDato("MODU_MOV_NIIF", "NU_CODMO_MODU", "TX_DESCM_MODU = 'Devoluci�n de Despacho'", True), txtNumero) 'SKRV T24951 'JLPB T25784-R25328 SE DEJA EN COMENTARIO
                            Call Guardar_Movimiento_Contable("DDP", Documento.Encabezado.FechaDocumento, comprobante, vNumero, Left(Trim(stDesc) & Right(String(7, "0") & NumeroCompro, 7) & ". " & Documento.Encabezado.Observaciones, 250), NUL$, , Documento.Encabezado.TipDeDcmnt, fnDevDato("MODU_MOV_NIIF", "NU_CODMO_MODU", "TX_DESCM_MODU = 'Devoluci�n de Despacho'", True), txtNumero) 'JLPB T25784-R25328
                            If Result = FAIL Then GoTo FLLCMMTRS 'DAHV M3534
                            Result = DoUpdate("IN_ENCABEZADO", "NU_COMP_CONTA_ENCA=" & vNumero, "NU_AUTO_COMP_ENCA=" & Documento.Encabezado.NumeroCOMPROBANTE & " AND NU_COMP_ENCA=" & NumeroCompro)
                        End If
                        If Documento.Encabezado.TipDeDcmnt = Traslado Then
                            comprobante = Buscar_Comprobante_Concepto(Traslado)
                            vNumero = Buscar_Numero_Comprobante(comprobante)
                            'Call Guardar_Movimiento_Contable("TRS", Documento.Encabezado.FechaDocumento, comprobante, vNumero, Left(Trim(stDesc) & Right(String(7, "0") & NumeroCompro, 7) & ". " & Documento.Encabezado.Observaciones, 250), NUL$) 'SKRV T26242 COMENTARIO
                            Call Guardar_Movimiento_Contable("TRS", Documento.Encabezado.FechaDocumento, comprobante, vNumero, Left(Trim(stDesc) & Right(String(7, "0") & NumeroCompro, 7) & ". " & Documento.Encabezado.Observaciones, 250), NUL$, fnDevDato("MODU_MOV_NIIF", "NU_CODMO_MODU", "TX_DESCM_MODU = 'Devoluci�n de Despacho' AND NU_CODMO_MODU like '06%'", True), txtNumero) 'SKRV T26242
                            If Result = FAIL Then GoTo FLLCMMTRS 'DAHV M3534
                            Result = DoUpdate("IN_ENCABEZADO", "NU_COMP_CONTA_ENCA=" & vNumero, "NU_AUTO_COMP_ENCA=" & Documento.Encabezado.NumeroCOMPROBANTE & " AND NU_COMP_ENCA=" & NumeroCompro)
                        End If
                    'SKRV T28409 INICIO
                    Case Is = AnulaEntrada
                       Condicion = ""
                       For InI = 0 To UBound(VaAnulDocs, 2)
                          If VaAnulDocs(0, InI) <> NUL$ Then
                             Condicion = " NU_AUTO_COMP = NU_AUTO_COMP_ENCA AND NU_AUTO_DOCU = NU_AUTO_DOCU_COMP AND NU_AUTO_ENCA = " & VaAnulDocs(0, InI)
                             Desde = "IN_DOCUMENTO, IN_COMPROBANTE, IN_ENCABEZADO"
                             vNumero = fnDevDato(Desde, "NU_COMP_CONTA_ENCA", Condicion, True)
                             'comprobante = Buscar_Comprobante_Concepto(Entrada) 'JLPB T41459-R37519 'DRMG T43370 Se deja en comentario
                             Call Eliminar_Comprobante(Buscar_Comprobante_Concepto(Entrada), vNumero, fnDevDato("MODU_MOV_NIIF", "NU_CODMO_MODU", "TX_DESCM_MODU = 'Entrada de Almac�n' AND NU_CODMO_MODU LIKE '06%' ", True), fnDevDato("IN_COMPROBANTE", "NU_COMP_COMP+1", "NU_AUTO_COMP=" & Documento.Encabezado.NumeroCOMPROBANTE, True))
                             StCodMod = fnDevDato("MODU_MOV_NIIF", "NU_CODMO_MODU", "TX_DESCM_MODU = 'Entrada de Almac�n' AND NU_CODMO_MODU LIKE '06%' ", True)
                             StConsMov = fnDevDato("IN_COMPROBANTE", "NU_COMP_COMP+1", "NU_AUTO_COMP=" & Documento.Encabezado.NumeroCOMPROBANTE, True)
                             If BoMovNiif = True Then 'JAUM T32800-R31099
                                Call Eliminar_Comprobante_New_NIIF(Buscar_Comprobante_Concepto(Entrada), vNumero, , StCodMod, StConsMov)
                             End If 'JAUM T32800-R31099
                          End If
                       Next InI
                       ReDim Arr(0)
                    'SKRV T28409 FIN
               'JAUM T28561 Inicio
               Case Is = AnulaDespacho
                  vContable = Documento.Encabezado.NumeroContable
                  Condicion = "TX_DESCM_MODU = 'Despacho de Inventario' AND NU_CODMO_MODU LIKE '06%' "
                  StCodMod = fnDevDato("MODU_MOV_NIIF", "NU_CODMO_MODU", Condicion, True)
                  Condicion = "NU_AUTO_COMP=" & Documento.Encabezado.NumeroCOMPROBANTE
                  StConsMov = fnDevDato("IN_COMPROBANTE", "NU_COMP_COMP+1", Condicion, True)
                  'comprobante = Buscar_Comprobante_Concepto(Despacho) 'JLPB T41459-R37519 'DRMG T43370 Se deja en comentario
                  Call Eliminar_Comprobante(Buscar_Comprobante_Concepto(Despacho), vContable, StCodMod, StConsMov)
                  If BoMovNiif = True Then
                     Call Eliminar_Comprobante_New_NIIF(Buscar_Comprobante_Concepto(Despacho), vContable, , StCodMod, StConsMov)
                  End If
               Case Is = AnulaBaja
                  vContable = Documento.Encabezado.NumeroContable
                  Condicion = "TX_DESCM_MODU = 'Salidas o Bajas de Almac�n' AND NU_CODMO_MODU LIKE '06%' "
                  StCodMod = fnDevDato("MODU_MOV_NIIF", "NU_CODMO_MODU", Condicion, True)
                  Condicion = "NU_AUTO_COMP=" & Documento.Encabezado.NumeroCOMPROBANTE
                  StConsMov = fnDevDato("IN_COMPROBANTE", "NU_COMP_COMP+1", Condicion, True)
                  'comprobante = Buscar_Comprobante_Concepto(BajaConsumo) 'JLPB T41459-R37519 'DRMG T43370 Se deja en comentario
                  Call Eliminar_Comprobante(Buscar_Comprobante_Concepto(BajaConsumo), vContable, StCodMod, StConsMov)
                  If BoMovNiif = True Then
                     Call Eliminar_Comprobante_New_NIIF(Buscar_Comprobante_Concepto(BajaConsumo), vContable, , StCodMod, StConsMov)
                  End If
               Case Is = AnulaTraslado
                  vContable = Documento.Encabezado.NumeroContable
                  Condicion = "TX_DESCM_MODU = 'Traslados entre Bodegas' AND NU_CODMO_MODU LIKE '06%' "
                  StCodMod = fnDevDato("MODU_MOV_NIIF", "NU_CODMO_MODU", Condicion, True)
                  Condicion = "NU_AUTO_COMP=" & Documento.Encabezado.NumeroCOMPROBANTE
                  StConsMov = fnDevDato("IN_COMPROBANTE", "NU_COMP_COMP + 1", Condicion, True)
                  'comprobante = Buscar_Comprobante_Concepto(Traslado) 'JLPB T41459-R37519 'DRMG T43370 Se deja en comentario
                  Call Eliminar_Comprobante(Buscar_Comprobante_Concepto(Traslado), vContable, StCodMod, StConsMov)
                  If BoMovNiif = True Then
                     Call Eliminar_Comprobante_New_NIIF(Buscar_Comprobante_Concepto(Traslado), vContable, , , StConsMov)
                  End If
               Case Is = AnulaSalida
                  vContable = Documento.Encabezado.NumeroContable
                  Condicion = "TX_DESCM_MODU = 'Salidas de Dependencia' AND NU_CODMO_MODU LIKE '06%' "
                  StCodMod = fnDevDato("MODU_MOV_NIIF", "NU_CODMO_MODU", Condicion, True)
                  Condicion = "NU_AUTO_COMP=" & Documento.Encabezado.NumeroCOMPROBANTE
                  StConsMov = fnDevDato("IN_COMPROBANTE", "NU_COMP_COMP+1", Condicion, True)
                  'comprobante = Buscar_Comprobante_Concepto(Salida) 'JLPB T41459-R37519 'DRMG T43370 Se deja en comentario
                  Call Eliminar_Comprobante(Buscar_Comprobante_Concepto(Salida), vContable, StCodMod, StConsMov)
                  If BoMovNiif = True Then
                     Call Eliminar_Comprobante_New_NIIF(Buscar_Comprobante_Concepto(Salida), vContable, , StCodMod, StConsMov)
                  End If
               Case Is = AnulaFacturaVenta
                  vContable = Documento.Encabezado.NumeroContable
                  Condicion = "TX_DESCM_MODU = 'Venta de Inventario' AND NU_CODMO_MODU LIKE '06%' "
                  StCodMod = fnDevDato("MODU_MOV_NIIF", "NU_CODMO_MODU", Condicion, True)
                  Condicion = "NU_AUTO_COMP=" & Documento.Encabezado.NumeroCOMPROBANTE
                  StConsMov = fnDevDato("IN_COMPROBANTE", "NU_COMP_COMP+1", Condicion, True)
                  'comprobante = Buscar_Comprobante_Concepto(FacturaVenta) 'JLPB T41459-R37519 'DRMG T43370 Se deja en comentario
                  Call Eliminar_Comprobante(Buscar_Comprobante_Concepto(FacturaVenta), vContable, StCodMod, StConsMov)
                  If BoMovNiif = True Then
                     Call Eliminar_Comprobante_New_NIIF(Buscar_Comprobante_Concepto(FacturaVenta), vContable, , StCodMod, StConsMov)
                  End If
               'JAUM T28561 Fin
            End Select
         Else
                   ''JACC M5449 Codigo copiado de GuardaValoresParticulatres
                    Select Case Documento.Encabezado.TipDeDcmnt
                    Case Is = FacturaVenta
                        Dim vCon1 As Integer, vVal As Variant, vPos As Integer
                        For vCon1 = 1 To Me.grdFormaPago.Rows - 1
                            vVal = 0: vPos = -1
                            If IsNumeric(Me.grdFormaPago.TextMatrix(vCon1, 2)) Then
                               vVal = CCur(Me.grdFormaPago.TextMatrix(vCon1, 2))
                            End If
                            vPos = FindInArrM(itmCuentasFormas, Me.grdFormaPago.TextMatrix(vCon1, 0)) 'aqui
                            If vVal > 0 And vPos > -1 Then
                                  Call InsertaValoresParticulares(itmCuentasFormas(0, vPos), vVal, "F")
                            End If
                         Next
                         '----
                         For vCon1 = 0 To UBound(itmCuentasIVA, 2)
                                  If CSng(itmCuentasIVA(2, vCon1)) > 0 Then
                                      Call InsertaValoresParticulares(itmCuentasIVA(0, vCon1), itmCuentasIVA(2, vCon1), "I")
                                  End If
                          Next
                          For vCon1 = 1 To Me.grdOtrosCargos.Rows - 1
                              vVal = 0: vPos = -1
                              If IsNumeric(Me.grdOtrosCargos.TextMatrix(vCon1, 2)) Then
                                  vVal = CCur(Me.grdOtrosCargos.TextMatrix(vCon1, 2))
                              End If
                              vPos = FindInArrM(itmCuentasOtrosCargos, Me.grdOtrosCargos.TextMatrix(vCon1, 0))
                              If vVal > 0 And vPos > -1 Then
                                  Call InsertaValoresParticulares(itmCuentasOtrosCargos(0, vPos), vVal, "O")
                              End If
                          Next
                          For vCon1 = 1 To Me.grdOtrosDescuentos.Rows - 1
                              vVal = 0: vPos = -1
                              If IsNumeric(Me.grdOtrosDescuentos.TextMatrix(vCon1, 2)) Then
                                  vVal = CCur(Me.grdOtrosDescuentos.TextMatrix(vCon1, 2))
                              End If
                              vPos = FindInArrM(itmCuentasOtrosDescuentos, Me.grdOtrosDescuentos.TextMatrix(vCon1, 0))
                              If vVal > 0 And vPos > -1 Then
                                  Call InsertaValoresParticulares(itmCuentasOtrosDescuentos(0, vPos), vVal, "D")
                              End If
                          Next
                    End Select
                   'JACC M5449
                End If
                
            'PedroJ
                If Aplicacion.Interfaz_Presupuesto And Result <> FAIL Then
                    continuar = False
                    Select Case Documento.Encabezado.TipDeDcmnt
                      Case Is = ODCompra
'                         ArtiPpto = Articulos_Presupuesto(GrdArticulos, "NU_COMP_CUEN,NU_CDPE_CUEN", 15, 0)
                         'ArtiPpto = Articulos_Presupuesto(GrdArticulos, 15, 0)      'DEPURACION DE CODIGO
                         'ArtiPpto = Articulos_Presupuesto(GrdArticulos, 15, 0, , , 2) 'NMSR M3544 'HRR M5080
                         ArtiPpto = Articulos_Presupuesto(GrdArticulos, 15, 0, , 2) 'NMSR M3544 'HRR M5080
                         If ArtiPpto(0, 0) = NUL$ Then Call Mensaje1("Fallo Interfase a Ppto", 1): GoTo FLLCMMTRS
                         TxtPpto(3) = Aplicacion.Formatear_Valor(TxtPpto(3)) 'HRR M2968
                         'If TotArtiPpto > Val(TxtPpto(3)) Then Call Mensaje1("El valor de la Orden de Compra es mayor que el valor del CDP", 1): GoTo FLLCMMTRS     'PJCA M1115 'HRR M2968
                         If CDbl(Trim(TotArtiPpto)) > CDbl(TxtPpto(3)) Then Call Mensaje1("El valor de la Orden de Compra es mayor que el valor del CDP", 1): GoTo FLLCMMTRS     'PJCA M1115 'HRR M2968
                         Valores = "NU_AUTO_ENCA_ENPP=" & Documento.Encabezado.AutoDelDocCARGADO & Coma
                         Valores = Valores & "NU_CONS_PTAL_ENPP=" & Trim(Me.TxtPpto(0)) & Coma
                         Valores = Valores & "NU_TIPO_PTAL_ENPP=1"  'ES UN CDP
                         Result = DoInsertSQL("IN_R_ENCA_PPTO", Valores)
                         If Result = FAIL Then Call Mensaje1("Fallo Interfase a Ppto", 1): GoTo FLLCMMTRS
                         RegPPto = Leer_Consecutivo_Ppto("NU_CONS_REGI") + 1
                         Result = DoUpdate("CONSECUTIVOS", "NU_CONS_REGI=" & RegPPto, NUL$)
                         If (Result <> FAIL) Then Call Actualizar_Ppto
                         If (Result <> FAIL) Then
                            Valores = "NU_AUTO_ENCA_ENPP=" & Documento.Encabezado.AutoDelDocCARGADO & Coma
                            Valores = Valores & "NU_CONS_PTAL_ENPP=" & RegPPto & Coma
                            Valores = Valores & "NU_TIPO_PTAL_ENPP=2"  'ES UN RP
                            Result = DoInsertSQL("IN_R_ENCA_PPTO", Valores)
                            If Result = FAIL Then Call Mensaje1("Fallo Interfase a Ppto", 1): GoTo FLLCMMTRS
                         End If
                       Case Is = Entrada
'                         ArtiPpto = Articulos_Presupuesto(GrdArticulos, "NU_COMP_CUEN,NU_CDPE_CUEN", 15, 0)
                         'ArtiPpto = Articulos_Presupuesto(GrdArticulos, 15, 0)      'DEPURACION DE CODIGO
                         'ArtiPpto = Articulos_Presupuesto(GrdArticulos, 15, 0, , , 3) 'DAHV M4460 'HRR M5080
                         'ArtiPpto = Articulos_Presupuesto(GrdArticulos, 15, 0, , 3) 'DAHV M4460 'HRR M5080
                         'ArtiPpto = Articulos_Presupuesto(GrdArticulos, 15, 0, , 3, CDbl(Me.TxtEntra(18))) 'JACC M6427
                         'JACC M6527
                           If Not IsNumeric(Me.TxtEntra(16)) Then Me.TxtEntra(16) = 0
                           'ArtiPpto = Articulos_Presupuesto(GrdArticulos, 15, 0, , 3, CDbl(Me.TxtEntra(16))) 'LDCR R5027/T6173
                           ArtiPpto = Articulos_Presupuesto(GrdArticulos, 15, 0, , 3, CDbl(Me.TxtEntra(16)), Documento.Encabezado.Tercero.Nit) 'LDCR R5027/T6173
                          'JACC M6527
                         If ArtiPpto(0, 0) = NUL$ Then Call Mensaje1("Fallo al buscar los Articulos Pptales del Grupo de Articulos", 1): GoTo FLLCMMTRS
                         TxtPpto(3).Text = Aplicacion.Formatear_Valor(TxtPpto(3)) 'NYCM M2968
                         
                         'If TotArtiPpto > Val(TxtPpto(3)) Then Call Mensaje1("El valor de la Entrada es mayor que el valor del RP", 1): GoTo FLLCMMTRS       'PJCA M1115
                         'If TotArtiPpto > CDbl(TxtPpto(3)) Then Call Mensaje1("El valor de la Entrada es mayor que el valor del RP", 1): GoTo FLLCMMTRS       'PJCA M3212
                         If CDbl(Trim(TotArtiPpto)) > CDbl(TxtPpto(3)) Then Call Mensaje1("El valor de la Entrada es mayor que el valor del RP", 1): GoTo FLLCMMTRS       'HRR M2968
                         
                         Valores = "NU_AUTO_ENCA_ENPP=" & Documento.Encabezado.AutoDelDocCARGADO & Coma
                         Valores = Valores & "NU_CONS_PTAL_ENPP=" & Trim(Me.TxtPpto(0)) & Coma
                         Valores = Valores & "NU_TIPO_PTAL_ENPP=2"  'ES UNA OBLIGACION
                         Result = DoInsertSQL("IN_R_ENCA_PPTO", Valores)
                         If Result = FAIL Then Call Mensaje1("Fallo al insertar en la tabla [IN_R_ENCA_PPTO]", 1): GoTo FLLCMMTRS
                         RegPPto = Leer_Consecutivo_Ppto("NU_CONS_OBLI") + 1
                         Result = DoUpdate("CONSECUTIVOS", "NU_CONS_OBLI=" & RegPPto, NUL$)
                         If (Result <> FAIL) Then Call Actualizar_Ppto
                         If BoResult = True Then GoTo FLLCMMTRS 'GAVL T4959
                         If (Result <> FAIL) Then
                            Valores = "NU_AUTO_ENCA_ENPP=" & Documento.Encabezado.AutoDelDocCARGADO & Coma
                            Valores = Valores & "NU_CONS_PTAL_ENPP=" & RegPPto & Coma
                            Valores = Valores & "NU_TIPO_PTAL_ENPP=3"  'ES UNA OBLIGACION
                            Result = DoInsertSQL("IN_R_ENCA_PPTO", Valores)
                            If Result = FAIL Then Call Mensaje1("Fallo al insertar en la tabla [IN_R_ENCA_PPTO]", 1): GoTo FLLCMMTRS
                         End If
                       'Case Is = CompraDElementos
                       
                       'SKRV T15948 INICIO
                       Case Is = DevolucionEntrada
                           Dim cont As Integer
                           For cont = 0 To Me.lstDocsPadre.ListCount - 1
                              ReDim ArrPPTO(0)
                              Desde = " IN_ENCABEZADO "
                              Campos = " NU_AUTO_ENCA "
                              Condicion = " NU_COMP_ENCA = " & lstDocsPadre.List(cont)
                              Condicion = Condicion & " AND NU_AUTO_BODEORG_ENCA = " & Me.cboBodega.ItemData(Me.cboBodega.ListIndex)
                              Condicion = Condicion & " AND CD_CODI_TERC_ENCA = '" & Cue_Ter & "'"
                              Result = LoadData(Desde, Campos, Condicion, ArrPPTO())
                              If Result <> FAIL And Encontro Then
                                 StEntrada = ArrPPTO(0)
                                 ReDim ArrPPTO(1)
                                 Desde = " OBLIGACION INNER JOIN IN_R_ENCA_PPTO ON OBLIGACION.NU_CONS_OBLI = IN_R_ENCA_PPTO.NU_CONS_PTAL_ENPP"
                                 Campos = " NU_CONS_PTAL_ENPP, VL_VALO_OBLI"
                                 Condicion = " NU_AUTO_ENCA_ENPP = " & StEntrada
                                 Condicion = Condicion & " AND NU_TIPO_PTAL_ENPP = 3"
                                 Result = LoadData(Desde, Campos, Condicion, ArrPPTO())
                                 If Result <> FAIL And Encontro Then
                                    If CDbl(ArrPPTO(1)) = CDbl(GrdArticulos.TextMatrix(cont + 1, 15)) Then
                                       Call Saldar_Obligacion(CInt(ArrPPTO(0)))
                                    Else
                                       Call Mensaje1("Se esta haciendo una devolucion parcial, el movimiento no afectara la obligacion " & ArrPPTO(0), 2)
                                    End If
                                 End If
                              End If
                           Next
                       'SKRV T15948 FIN
                       'JAUM T28560 Inicio
                       Case Is = AnulaEntrada
                          ReDim ArrPPTO(0)
                          FechaDocCargado = FechaServer
                          Condi = "NU_AUTO_ENCA_ENPP=" & AutoDocCargado
                          Condi = Condi & " AND NU_TIPO_PTAL_ENPP=3"
                          Result = LoadData("IN_R_ENCA_PPTO", "NU_CONS_PTAL_ENPP", Condi, ArrPPTO)
                          If Result <> FAIL And Encontro Then
                             Call Anulacion_Documentos_Ppto(6, CDbl(ArrPPTO(0)), True, FechaDocCargado)
                          Else
                             Call Mensaje1("No se encontro la Obligaci�n asociada", 2)
                          End If
                       Case Is = AnulaODCompra
                          ReDim ArrPPTO(0)
                          FechaDocCargado = FechaServer
                          Condi = "NU_AUTO_ENCA_ENPP=" & AutoDocCargado
                          Condi = Condi & " AND NU_TIPO_PTAL_ENPP = 2"
                          Result = LoadData("IN_R_ENCA_PPTO", "NU_CONS_PTAL_ENPP", Condi, ArrPPTO)
                          If Result <> FAIL And Encontro Then
                             Call Anulacion_Documentos_Ppto(5, CDbl(ArrPPTO(0)), True, FechaDocCargado)
                          Else
                             Call Mensaje1("No se encontro el Registro Pptal asociado", 2)
                          End If
                       'JAUM T28560 in
                    End Select
                End If
            
            'PedroJ
                'DRMG T43370 INICIO Se crea case para que solo se permita guardar el reguistro de auditoria
                'a los comprobantes distintos a anulados
                Select Case Documento.Encabezado.TipDeDcmnt
                   Case Is = AnulaEntrada, AnulaDespacho, AnulaBaja, AnulaTraslado, _
                      AnulaSalida, AnulaFacturaVenta, AnulaAprovecha, AnulaODCompra
                      Result = SUCCEED
                   Case Else
                   'DRMG T43370 FIN
                      Result = Almacenar_Auditoria_Mov(CDbl(fnDevDato("IN_ENCABEZADO", "NU_AUTO_ENCA", "NU_COMP_ENCA=" & NumeroCompro & " AND NU_AUTO_DOCU_ENCA=" & Documento.Encabezado.TipDeDcmnt & " AND NU_AUTO_COMP_ENCA=" & Documento.Encabezado.NumeroCOMPROBANTE)), comprobante, CDbl(vNumero), CInt(Documento.Encabezado.AutoDelUSUARIO)) 'JLPB T41459-R37519
                      If Result = FAIL Then Call Mensaje1("Fallo al insertar en la tabla [IN_AUDMOVCONT]", 1): GoTo FLLCMMTRS 'JLPB T41459-R37519
                End Select 'DRMG T43370
                If CommitTran = FAIL Then GoTo FLLCMMTRS
                'AASV R1883
                If Aplicacion.PinvImpAntSCon Then
                   'StRespuesta = MsgBox("Desea imprimir el documento?", vbYesNo + vbInformation)
                   InRespuesta = MsgBox("Desea imprimir el documento?", vbYesNo + vbInformation) 'AASV M3520
                   'If StRespuesta = vbYes Then
                   If InRespuesta = vbYes Then 'AASV M3520
                      Documento.Encabezado.EsNuevoDocumento = False
                      Call cmdOpciones_Click(2)
                   End If
                End If
                'AASV R1883
                
                'DRMG T44728-R41288 INICIO
                If Documento.Encabezado.TipDeDcmnt = FacturaVenta Then
                   ReDim VrArr(0)
                   Valores = "NU_AUTO_FAFA"
                   Condicion = "NU_NUCOMP_FAFA=" & NumeroCompro
                   Condicion = Condicion & " AND NU_DOCU_FAFA=" & FacturaVenta
                   Condicion = Condicion & " AND NU_COMP_FAFA=" & Documento.Encabezado.NumeroCOMPROBANTE
                   Result = LoadData("R_FAVE_FAEL", Valores, Condicion, VrArr())
                   If Result <> FAIL Then
                      If Not Encontro Then
                         ReDim VrArr(1)
                         Valores = "TX_PREFI_FAEL,NU_CONFAC_FAEL"
                         Condicion = "NU_MODULO_FAEL=1"
                         Condicion = Condicion & " AND NU_ESTADO_FAEL=0"
                         Result = LoadData("FACTURA_ELECTRONICA", Valores, Condicion, VrArr())
                         If Result <> FAIL Then
                            If Encontro Then
                               Valores = "NU_DOCU_FAFA=" & FacturaVenta
                               Valores = Valores & Coma & "NU_NUCOMP_FAFA=" & NumeroCompro
                               Valores = Valores & Coma & "NU_CNFAVE_FAFA=" & CDbl(VrArr(1) + 1)
                               Valores = Valores & Coma & "NU_COMP_FAFA=" & Documento.Encabezado.NumeroCOMPROBANTE
                               Valores = Valores & Coma & "TX_PREFI_FAFA='" & VrArr(0) & Comi
                               'Valores = Valores & Coma & "TX_HORA_FAFA='" & Format(Nowserver, "hh:mm") & Comi 'DRMG T45162-R41288 SE DEJA EN COMENTARIO
                               Valores = Valores & Coma & "TX_HORA_FAFA='" & Format(Nowserver, "hh:mm:ss") & Comi 'DRMG T45162-R41288
                               Result = DoInsertSQL("R_FAVE_FAEL", Valores)
                               If Result <> FAIL Then
                                  Call Fave_Impu_Desc_Fael
                                  If Result <> FAIL Then
                                     If ByFael = 1 Then 'DRMG T45277-R41288
                                        Call ParametrosXML(TxtFactElt.Text)
                                        Call Enviar_PDF_XML
                                     'DRMG T45277-R41288 INICIO
                                     Else
                                        Call Guardar_email(TxtFactElt, 0, "No se a generado por licencia", 0)
                                     End If
                                     'DRMG T45277-R41288 FIN
                                     If Result <> FAIL Then
                                        Condicion = "NU_MODULO_FAEL=1"
                                        Condicion = Condicion & " AND NU_ESTADO_FAEL=0"
                                        Result = DoUpdate("FACTURA_ELECTRONICA", "NU_CONFAC_FAEL=" & VrArr(1) + 1, Condicion)
                                        If Result <> FAIL Then TxtFactElt.Text = CDbl(VrArr(1) + 1)
                                     End If
                                  End If
                               End If
                            End If
                         End If
                      End If
                   End If
                End If
                'DRMG T44728-R41288 FIN
                
                'AASV Bloqueos Rosario 04/02/2009 INICIO
                'If NumeroCompro <> FAIL Then Call cmdOpciones_Click(3)
                If NumeroCompro <> FAIL Then
                   Call cmdOpciones_Click(3)
                   Call Mensaje1("Se gener� el documento n�mero " & NumeroCompro, 3)
                End If
                'AASV Bloqueos Rosario 04/02/2009 FIN
                GoTo SLRDLTRNS
                 
            End If
        End If
        Me.fraTOTALES.Visible = True
FLLBGNTRN:
FLLCMMTRS:
        Call RollBackTran
        'AASV Bloqueos Rosario 05/02/2009 INICIO
        If StDescripError <> NUL$ Then
           Call Mensaje1(StDescripError, 3)
           StDescripError = NUL$
        End If
        'AASV Bloqueos Rosario 05/02/2009 FIN
        BoResult = False 'GAVL T4959
SLRDLTRNS:
        If (Documento.Encabezado.TipDeDcmnt = FacturaVenta) Or (Documento.Encabezado.TipDeDcmnt = DevolucionFacturaVenta) Or (Documento.Encabezado.TipDeDcmnt = AnulaFacturaVenta) Then
            Call WinDoc_CambioRenglonGrilla(0, WinDoc.Calcular_Documento(Me.ColCantidad, Me.ColVenUndConDes))
        Else
'            Call WinDoc_CambioRenglonGrilla(0, WinDoc.Calcular_Documento(Me.ColCantidad, Me.ColCostoUnidad))
            If continuar Then Call WinDoc_CambioRenglonGrilla(0, WinDoc.Calcular_Documento(Me.ColCantidad, Me.ColCostoUnidad))  'REOL M2085
        End If
       
    Case Is = 1

        If Documento.Encabezado.EsNuevoDocumento Then Exit Sub
        vContable = Documento.Encabezado.NumeroContable
        FechaEntera = Documento.Encabezado.FechaDocumento
         If Documento.Encabezado.EstadoDocumento = "N" Then
            If BuscarDependientes = 1 Then Exit Sub
            'DRMG T45983 INICIO
            If ExisteTABLA("ENF_INVEMOV") Then
               If ConsultaPacientes Then
                  StMensaje = "El documento fue creado desde interfaces, si lo anula no podr� gestionar las ordenes en el m�dulo de pacientes."
                  StMensaje = StMensaje & vbCrLf & "Desea continuar?"
                  If Not WarnMsg(StMensaje) Then
                     Exit Sub
                  Else
                     Campos = "TX_NOMB_DOCU"
                     Desde = "IN_DOCUMENTO"
                     Condi = "NU_AUTO_DOCU= " & Documento.Encabezado.TipDeDcmnt
                     LastCmd = "Anulaci�n del movimiento " & fnDevDato(Desde, Campos, Condi)
                     
                     Campos = "TX_NOMB_COMP"
                     Desde = "IN_COMPROBANTE"
                     Condi = "NU_AUTO_COMP= " & Documento.Encabezado.NumeroCOMPROBANTE
                     LastCmd = LastCmd & " del comprobante " & fnDevDato(Desde, Campos, Condi)
                     LastCmd = LastCmd & " con el consecutivo " & txtNumero
                     
                     Result = Auditor("IN_ENCABEZADO", TranUpd, LastCmd)
                  End If
               End If
            End If
            'DRMG T45983 FIN
            Select Case Documento.Encabezado.TipDeDcmnt
            Case Is = Cotizacion
                Documento.Encabezado.TipDeDcmnt = AnulaCotizacion
            Case Is = ODCompra
                Documento.Encabezado.TipDeDcmnt = AnulaODCompra
            Case Is = Entrada
                Documento.Encabezado.TipDeDcmnt = AnulaEntrada
            Case Is = AprovechaDonacion
                Documento.Encabezado.TipDeDcmnt = AnulaAprovecha
            Case Is = Requisicion
                Documento.Encabezado.TipDeDcmnt = AnulaRequisicion
            Case Is = Despacho
                Documento.Encabezado.TipDeDcmnt = AnulaDespacho
            Case Is = BajaConsumo
                Documento.Encabezado.TipDeDcmnt = AnulaBaja
            Case Is = Salida
                Documento.Encabezado.TipDeDcmnt = AnulaSalida
            Case Is = FacturaVenta
                UnConsecutivo.Actualiza Documento.Encabezado.NumeroCOMPROBANTE
                vNumeroActual = CLng(Me.txtNumero.Text)
                Documento.Encabezado.TipDeDcmnt = AnulaFacturaVenta
            Case Is = Traslado
                Documento.Encabezado.TipDeDcmnt = AnulaTraslado
            End Select
            Me.cmdOpciones(1).Enabled = False
            Documento.Encabezado.Actualiza
            Campos = "NU_AUTO_COMP,TX_NOMB_COMP"
            Desde = "IN_COMPROBANTE"
            Condi = "NU_AUTO_DOCU_COMP=" & Documento.Encabezado.TipDeDcmnt
            ReDim ArrCPB(1)
            Result = LoadData(Desde, Campos, Condi, ArrCPB())
            If Result <> FAIL Then
                If Encontro Then
                    If BeginTran(STranIUp & "KARDEX") = FAIL Then GoTo FAILGNTRN 'APGR T2674
                    Documento.Encabezado.NumeroCOMPROBANTE = CLng(ArrCPB(0)) 'NU_AUTO_COMP

                    If Aplicacion.Interfaz_Contabilidad Then
                        Select Case Documento.Encabezado.TipDeDcmnt
                        Case Is = AnulaEntrada
                            'SKRV T25681 INICIO Las siguientes dos lineas quedan en comentario
                            'Call Eliminar_Comprobante(Buscar_Comprobante_Concepto(Entrada), vContable)
                            'Call Eliminar_Comprobante_New_NIIF(Buscar_Comprobante_Concepto(Entrada), vContable) 'JLPB T25784-R25328
                            'comprobante = Buscar_Comprobante_Concepto(Entrada) 'JLPB T41459-R37519 'DRMG T43370 Se deja en comentario
                            Call Eliminar_Comprobante(Buscar_Comprobante_Concepto(Entrada), vContable, fnDevDato("MODU_MOV_NIIF", "NU_CODMO_MODU", "TX_DESCM_MODU = 'Entrada de Almac�n' AND NU_CODMO_MODU LIKE '06%' ", True), fnDevDato("IN_COMPROBANTE", "NU_COMP_COMP+1", "NU_AUTO_COMP=" & Documento.Encabezado.NumeroCOMPROBANTE, True))
                            StCodMod = fnDevDato("MODU_MOV_NIIF", "NU_CODMO_MODU", "TX_DESCM_MODU = 'Entrada de Almac�n' AND NU_CODMO_MODU LIKE '06%' ", True)
                            StConsMov = fnDevDato("IN_COMPROBANTE", "NU_COMP_COMP+1", "NU_AUTO_COMP=" & Documento.Encabezado.NumeroCOMPROBANTE, True)
                            If BoMovNiif = True Then 'JAUM T32800-R31099
                               Call Eliminar_Comprobante_New_NIIF(Buscar_Comprobante_Concepto(Entrada), vContable, , StCodMod, StConsMov)
                            End If 'JAUM T32800-R31099
                            'SKRV T25681 FIN
                        Case Is = AnulaSalida
                            'SKRV T25681 INICIO Las siguientes dos lineas quedan en comentario
                            'Call Eliminar_Comprobante(Buscar_Comprobante_Concepto(Salida), vContable)
                            'Call Eliminar_Comprobante_New_NIIF(Buscar_Comprobante_Concepto(Salida), vContable) 'JLPB T25784-R25328
                            'comprobante = Buscar_Comprobante_Concepto(Salida) 'JLPB T41459-R37519 'DRMG T43370 Se deja en comentario
                            Call Eliminar_Comprobante(Buscar_Comprobante_Concepto(Salida), vContable, fnDevDato("MODU_MOV_NIIF", "NU_CODMO_MODU", "TX_DESCM_MODU = 'Salidas de Dependencia' AND NU_CODMO_MODU LIKE '06%' ", True), fnDevDato("IN_COMPROBANTE", "NU_COMP_COMP+1", "NU_AUTO_COMP=" & Documento.Encabezado.NumeroCOMPROBANTE, True))
                            StCodMod = fnDevDato("MODU_MOV_NIIF", "NU_CODMO_MODU", "TX_DESCM_MODU = 'Salidas de Dependencia' AND NU_CODMO_MODU LIKE '06%' ", True)
                            StConsMov = fnDevDato("IN_COMPROBANTE", "NU_COMP_COMP+1", "NU_AUTO_COMP=" & Documento.Encabezado.NumeroCOMPROBANTE, True)
                            If BoMovNiif = True Then 'JAUM T32800-R31099
                               Call Eliminar_Comprobante_New_NIIF(Buscar_Comprobante_Concepto(Salida), vContable, , StCodMod, StConsMov)
                            End If 'JAUM T32800-R31099
                            'SKRV T25681 FIN
                        Case Is = AnulaFacturaVenta
                            'SKRV T25681 INICIO Las siguientes dos lineas quedan en comentario
                            'Call Eliminar_Comprobante(Buscar_Comprobante_Concepto(FacturaVenta), vContable)
                            'Call Eliminar_Comprobante_New_NIIF(Buscar_Comprobante_Concepto(FacturaVenta), vContable) 'JLPB T25784-R25328
                            'comprobante = Buscar_Comprobante_Concepto(FacturaVenta) 'JLPB T41459-R37519 'DRMG T43370 Se deja en comentario
                            Call Eliminar_Comprobante(Buscar_Comprobante_Concepto(FacturaVenta), vContable, fnDevDato("MODU_MOV_NIIF", "NU_CODMO_MODU", "TX_DESCM_MODU = 'Venta de Inventario' AND NU_CODMO_MODU LIKE '06%' ", True), fnDevDato("IN_COMPROBANTE", "NU_COMP_COMP+1", "NU_AUTO_COMP=" & Documento.Encabezado.NumeroCOMPROBANTE, True))
                            StCodMod = fnDevDato("MODU_MOV_NIIF", "NU_CODMO_MODU", "TX_DESCM_MODU = 'Venta de Inventario' AND NU_CODMO_MODU LIKE '06%' ", True)
                            StConsMov = fnDevDato("IN_COMPROBANTE", "NU_COMP_COMP+1", "NU_AUTO_COMP=" & Documento.Encabezado.NumeroCOMPROBANTE, True)
                            If BoMovNiif = True Then 'JAUM T32800-R31099
                               Call Eliminar_Comprobante_New_NIIF(Buscar_Comprobante_Concepto(FacturaVenta), vContable, , StCodMod, StConsMov)
                            End If 'JAUM T32800-R31099
                            'SKRV T25681 FIN
                        Case Is = AnulaBaja
                            'Call Eliminar_Comprobante(Buscar_Comprobante_Concepto(AnulaBaja), vContable)
                            'SKRV T25681 INICIO Las siguientes dos lineas quedan en comentario
                            'Call Eliminar_Comprobante(Buscar_Comprobante_Concepto(BajaConsumo), vContable)  'PJCA M1036'SKRV T25681 COMENTARIO
                            'Call Eliminar_Comprobante_New_NIIF(Buscar_Comprobante_Concepto(BajaConsumo), vContable) 'JLPB T25784-R25328
                            'comprobante = Buscar_Comprobante_Concepto(BajaConsumo) 'JLPB T41459-R37519 'DRMG T43370 Se deja en comentario
                            Call Eliminar_Comprobante(Buscar_Comprobante_Concepto(BajaConsumo), vContable, fnDevDato("MODU_MOV_NIIF", "NU_CODMO_MODU", "TX_DESCM_MODU = 'Salidas o Bajas de Almac�n' AND NU_CODMO_MODU LIKE '06%' ", True), fnDevDato("IN_COMPROBANTE", "NU_COMP_COMP+1", "NU_AUTO_COMP=" & Documento.Encabezado.NumeroCOMPROBANTE, True))
                            StCodMod = fnDevDato("MODU_MOV_NIIF", "NU_CODMO_MODU", "TX_DESCM_MODU = 'Salidas o Bajas de Almac�n' AND NU_CODMO_MODU LIKE '06%' ", True)
                            StConsMov = fnDevDato("IN_COMPROBANTE", "NU_COMP_COMP+1", "NU_AUTO_COMP=" & Documento.Encabezado.NumeroCOMPROBANTE, True)
                            Call Eliminar_Comprobante(Buscar_Comprobante_Concepto(BajaConsumo), vContable, StCodMod, StConsMov) 'JLPB T28485
                            If BoMovNiif = True Then 'JAUM T32800-R31099
                               Call Eliminar_Comprobante_New_NIIF(Buscar_Comprobante_Concepto(BajaConsumo), vContable, , StCodMod, StConsMov)
                            End If 'JAUM T32800-R31099
                            'SKRV T25681 FIN
                        Case Is = AnulaDespacho
                            'Call Eliminar_Comprobante(Buscar_Comprobante_Concepto(Despacho), vContable)'SKRV T25681 COMENTARIO
                            'comprobante = Buscar_Comprobante_Concepto(Despacho) 'JLPB T41459-R37519 'DRMG T43370 Se deja en comentario
                            Call Eliminar_Comprobante(Buscar_Comprobante_Concepto(Despacho), vContable, fnDevDato("MODU_MOV_NIIF", "NU_CODMO_MODU", "TX_DESCM_MODU = 'Despacho de Inventario' AND NU_CODMO_MODU LIKE '06%' ", True), fnDevDato("IN_COMPROBANTE", "NU_COMP_COMP+1", "NU_AUTO_COMP=" & Documento.Encabezado.NumeroCOMPROBANTE, True)) 'SKRV T25681
                            'JLPB T26186 INICIO
                            'StCodMod = fnDevDato("MODU_MOV_NIIF", "NU_CODMO_MODU", "TX_DESCM_MODU = 'Devoluci�n de Despacho' AND NU_CODMO_MODU LIKE '06%' ", True) JAUM T26639 Se deja linea en comentario
                            StCodMod = fnDevDato("MODU_MOV_NIIF", "NU_CODMO_MODU", "TX_DESCM_MODU = 'Despacho de Inventario' AND NU_CODMO_MODU LIKE '06%' ", True) 'JAUM T26639
                            StConsMov = fnDevDato("IN_COMPROBANTE", "NU_COMP_COMP+1", "NU_AUTO_COMP=" & Documento.Encabezado.NumeroCOMPROBANTE, True)
                            If BoMovNiif = True Then 'JAUM T32800-R31099
                               Call Eliminar_Comprobante_New_NIIF(Buscar_Comprobante_Concepto(Despacho), vContable, , StCodMod, StConsMov)
                            End If 'JAUM T32800-R31099
                            'JLPB T26186 FIN
                        Case Is = AnulaTraslado
                            'Call Eliminar_Comprobante(Buscar_Comprobante_Concepto(Traslado), vContable)'SKRV T25681 COMENTARIO
                            'comprobante = Buscar_Comprobante_Concepto(Traslado) 'JLPB T41459-R37519 'DRMG T43370 Se deja en comentario
                            Call Eliminar_Comprobante(Buscar_Comprobante_Concepto(Traslado), vContable, fnDevDato("MODU_MOV_NIIF", "NU_CODMO_MODU", "TX_DESCM_MODU = 'Traslados entre Bodegas' AND NU_CODMO_MODU LIKE '06%' ", True), fnDevDato("IN_COMPROBANTE", "NU_COMP_COMP+1", "NU_AUTO_COMP=" & Documento.Encabezado.NumeroCOMPROBANTE, True)) 'SKRV T25681
                            'JLPB T26186 INICIO
                            StConsMov = fnDevDato("IN_COMPROBANTE", "NU_COMP_COMP+1", "NU_AUTO_COMP=" & Documento.Encabezado.NumeroCOMPROBANTE, True)
                            If BoMovNiif = True Then 'JAUM T32800-R31099
                               Call Eliminar_Comprobante_New_NIIF(Buscar_Comprobante_Concepto(Traslado), vContable, , , StConsMov)
                            End If 'JAUM T32800-R31099
                            'JLPB T26186 FIN
                        'NMSR M3533
                        Case Is = AnulaAprovecha
                            'SKRV T25681 INICIO Las siguientes dos lineas quedan en comentario
                            'Call Eliminar_Comprobante(Buscar_Comprobante_Concepto(AprovechaDonacion), vContable)'SKRV T25681 COMENTARIO
                            'Call Eliminar_Comprobante_New_NIIF(Buscar_Comprobante_Concepto(AprovechaDonacion), vContable) 'JLPB T25784-R25328
                            'comprobante = Buscar_Comprobante_Concepto(AprovechaDonacion) 'JLPB T41459-R37519 'DRMG T43370 Se deja en comentario
                            Call Eliminar_Comprobante(Buscar_Comprobante_Concepto(AprovechaDonacion), vContable, fnDevDato("MODU_MOV_NIIF", "NU_CODMO_MODU", "TX_DESCM_MODU = 'Aprovechamientos/Donaciones' AND NU_CODMO_MODU LIKE '06%' ", True), fnDevDato("IN_COMPROBANTE", "NU_COMP_COMP+1", "NU_AUTO_COMP=" & Documento.Encabezado.NumeroCOMPROBANTE, True)) 'SKRV T25681
                            StCodMod = fnDevDato("MODU_MOV_NIIF", "NU_CODMO_MODU", "TX_DESCM_MODU = 'Aprovechamientos/Donaciones' AND NU_CODMO_MODU LIKE '06%' ", True)
                            StConsMov = fnDevDato("IN_COMPROBANTE", "NU_COMP_COMP+1", "NU_AUTO_COMP=" & Documento.Encabezado.NumeroCOMPROBANTE, True)
                            If BoMovNiif = True Then 'JAUM T32800-R31099
                               Call Eliminar_Comprobante_New_NIIF(Buscar_Comprobante_Concepto(AprovechaDonacion), vContable, , StCodMod, StConsMov)
                            End If 'JAUM T32800-R31099
                            'SKRV T25681 FIN
                        'NMSR M3533
                        End Select
                        If Result = FAIL Then GoTo FAILGNTRN 'APGR T2674
                    End If
                    
                    '/////////////////
'                    GrabarDocumento (NumeroCompro)
                    'If BeginTran(STranIUp & "KARDEX") = FAIL Then GoTo FAILGNTRN 'APGR T2674 - Se sube l�nea de codigo para que comienza la trasaccion antes de ejecutar interfaz a contabilidad.
                    Me.fraTOTALES.Visible = False
                    Me.fraTOTALES.Visible = True
                    AutoDocCargado = Documento.Encabezado.AutoDelDocCARGADO
                    Documento.Encabezado.FechaDocumento = FechaServer   'REOL M2607
                    Documento.Encabezado.FechaVigencia = FechaServer    'REOL M2607
                    FechaDocCargado = Documento.Encabezado.FechaDocumento
                    'If Result <> 0 Then NumeroCompro = WinDoc.Guardar(False) 'HRR M3229
                    If Result <> 0 Then NumeroCompro = WinDoc.Guardar(False, True) 'HRR M3229
                    If NumeroCompro > 0 Then txtNumero.Text = NumeroCompro 'AASV M5669
                    IntDocu = Documento.Encabezado.TipDeDcmnt  'JACC M5623
                    If Not NumeroCompro > 0 Then GoTo FAILMMTRS
                    
                    If Aplicacion.Interfaz_Presupuesto Then
                        Select Case Documento.Encabezado.TipDeDcmnt
                            Case Is = AnulaFacturaVenta 'NU_CONS_GIRI, CD_CODIPAG_GIRI (UnConsecutivo.Prefijo & NumeroCompro)
                                Desde = "GIROING"
                                Campos = "NU_CONS_GIRI"
                                Condi = "CD_CODI_PAC_GIRI=" & Comi & UnConsecutivo.Prefijo & vNumeroActual & Comi
                                ReDim ArrPPTO(1, 0)
                                Result = LoadMulData(Desde, Campos, Condi, ArrPPTO)
                                If Result <> FAIL Then
                                    For vCon = 0 To UBound(ArrPPTO, 2)
                                        'Call Anulacion_Documentos_Ppto(8, CDbl(ArrPPTO(vCon, 0)), False, Documento.Encabezado.FechaDocumento)   'PedroJ
                                        Call Anulacion_Documentos_Ppto(8, CDbl(ArrPPTO(vCon, 0)), True, FechaDocCargado)   'PJCA M1069
                                    Next
                                End If
                            'Case Is = ODCompra
                            Case Is = AnulaODCompra     'PJCA M1069
                                ReDim ArrPPTO(0)
                                'Condi = "NU_AUTO_ENCA_ENPP=" & Documento.Encabezado.AutoDelDocCARGADO
                                Condi = "NU_AUTO_ENCA_ENPP=" & AutoDocCargado
                                Condi = Condi & " AND NU_TIPO_PTAL_ENPP=2"
                                Result = LoadData("IN_R_ENCA_PPTO", "NU_CONS_PTAL_ENPP", Condi, ArrPPTO)
                                If Result <> FAIL And Encontro Then
                                        'Call Anulacion_Documentos_Ppto(5, CDbl(ArrPPTO(0)), False, Documento.Encabezado.FechaDocumento)   'PedroJ
                                        Call Anulacion_Documentos_Ppto(5, CDbl(ArrPPTO(0)), True, FechaDocCargado)   'PJCA M1069
                                Else
                                    Call Mensaje1("No se encontro el Registro Pptal asociado", 2)
                                End If
                            'Case Is = Entrada
                            Case Is = AnulaEntrada      'PJCA M1069
                                ReDim ArrPPTO(0)
                                'Condi = "NU_AUTO_ENCA_ENPP=" & Documento.Encabezado.AutoDelDocCARGADO
                                Condi = "NU_AUTO_ENCA_ENPP=" & AutoDocCargado
                                Condi = Condi & " AND NU_TIPO_PTAL_ENPP=3"
                                Result = LoadData("IN_R_ENCA_PPTO", "NU_CONS_PTAL_ENPP", Condi, ArrPPTO)
                                If Result <> FAIL And Encontro Then
                                        'Call Anulacion_Documentos_Ppto(6, CDbl(ArrPPTO(0)), False, Documento.Encabezado.FechaDocumento)   'PedroJ
                                        Call Anulacion_Documentos_Ppto(6, CDbl(ArrPPTO(0)), True, FechaDocCargado)   'PJCA M1069
                                Else
                                    Call Mensaje1("No se encontro la Obligaci�n asociada", 2)
                                End If
                            End Select
                    End If
                    
                'PedroJ Def 4223
                    If Aplicacion.Interfaz_CxC Then
                        Select Case Documento.Encabezado.TipDeDcmnt
                        Case Is = AnulaFacturaVenta
                            Desde = "C_X_C"
                            Condi = "CD_CONCE_CXC='VINV' AND NU_FACL_CXC=" & vNumeroActual
                            ReDim ArrPPTO(0, 0)
                            Result = LoadMulData(Desde, "CD_NUME_CXC", Condi, ArrPPTO)
                            If Result <> FAIL And Encontro Then
                                Call Anula_CxC(CDbl(ArrPPTO(vCon, 0)), CDbl(vNumeroActual))
                                If Result = FAIL Then Call Mensaje1("Error anulando la CxC", 3): GoTo FAILMMTRS
                            End If
                        End Select
                    End If
                'PedroJ Def 4223
                    
                    If CommitTran = FAIL Then GoTo FAILMMTRS
                    'AASV Bloqueos Rosario 04/02/2009 INCIO
                    'If NumeroCompro <> FAIL Then Call cmdOpciones_Click(3)
                     If NumeroCompro <> FAIL Then
                        Call cmdOpciones_Click(3)
                        Call Mensaje1("Se gener� el documento n�mero " & NumeroCompro, 3)
                     End If
                     'AASV Bloqueos Rosario 04/02/2009 FIN
                    'If OpcCod = 316 Then '**** INICIO CARV M4973
                    If Documento.Encabezado.TipDeDcmnt = AnulaTraslado Then
                       Documento.Encabezado.TipDeDcmnt = 10
                    End If '**** FIN CARV M4973
                    GoTo EXITLTRNS
                    Me.fraTOTALES.Visible = True
FAILGNTRN:
FAILMMTRS:
                    Call RollBackTran
                    No_Recargar = 1
                    ''JACC M5623
                    If IntDocu = AnulaEntrada Then
                      Call Mensaje1("No se puede anular la entrada de mercancia", 3)
                      StDescripError = NUL$
                    End If
                    'JACC M5623
                    'GSCD T45416 INICIO
                    If IntDocu = AnulaAprovecha Then
                       If StDescripError <> NUL$ Then
                          Call Mensaje1(StDescripError, 3)
                          StDescripError = NUL$
                       End If
                    End If
                    'GSCD T45416 FIN
EXITLTRNS:
'/////////////////
                Else
                    MsgBox ("No existe Comprobante para procesar la transacci�n")
                End If
            End If
            Select Case Documento.Encabezado.TipDeDcmnt
            Case Is = AnulaCotizacion
                Documento.Encabezado.TipDeDcmnt = Cotizacion
            Case Is = AnulaODCompra
                Documento.Encabezado.TipDeDcmnt = ODCompra
            Case Is = AnulaEntrada
                Documento.Encabezado.TipDeDcmnt = Entrada
            Case Is = AnulaAprovecha
                Documento.Encabezado.TipDeDcmnt = AprovechaDonacion
            Case Is = AnulaRequisicion
                Documento.Encabezado.TipDeDcmnt = Requisicion
            Case Is = AnulaDespacho
                Documento.Encabezado.TipDeDcmnt = Despacho
            Case Is = AnulaBaja
                Documento.Encabezado.TipDeDcmnt = BajaConsumo
            Case Is = AnulaSalida
                Documento.Encabezado.TipDeDcmnt = Salida
            Case Is = AnulaFacturaVenta
                Documento.Encabezado.TipDeDcmnt = FacturaVenta
            End Select
            Documento.Encabezado.Actualiza
            'WinDoc.CargarCombos
            'If No_Recargar = 1 Then WinDoc.CargarCombos
            If No_Recargar <> 1 Then WinDoc.CargarCombos 'DAHV M4235
            If No_Recargar = 1 Then cmdOpciones_Click (3) 'JACC M4237
        Else
            Call MsgBox("El documento no puede ser anulado", vbInformation)
        End If
    Case Is = 2
        If Documento.Encabezado.EsNuevoDocumento Then Exit Sub
         Call Limpiar_CrysListar
        Select Case Documento.Encabezado.TipDeDcmnt
        
        Case Is = AnulaBaja, AnulaDespacho, AnulaEntrada, AnulaAprovecha, AnulaFacturaVenta, AnulaODCompra, AnulaRequisicion, AnulaSalida, AnulaCotizacion, AnulaTraslado
'            Formula = "{IN_ENCABEZADO.NU_AUTO_DOCU_ENCA}=" & Documento.Encabezado.TipDeDcmnt & _
'                " AND {IN_ENCABEZADO.NU_COMP_ENCA}=" & CLng(Me.txtNumero) & _
'                " AND {IN_DETALLE.NU_AUTO_BODE_DETA}=" & Me.cboBodega.ItemData(Me.cboBodega.ListIndex)
            'REOL518
            SQL = "SELECT NU_ENTRAD_DETA,NU_SALIDA_DETA,NU_MULT_DETA,NU_DIVI_DETA,NU_COSTO_DETA," & _
                "NU_IMPU_DETA,TX_NOMB_BODE,NU_COMP_ENCA,FE_CREA_ENCA,TX_OBSE_ENCA,CD_CODI_ARTI," & _
                "NO_NOMB_ARTI,CD_CODI_TERC,NO_NOMB_TERC,TX_NOMB_COMP,TX_DESC_USUA,TX_NOMB_UNVE "
            'SQL = SQL & Coma & "TX_ESTA_ENCA,NU_CONE_ENCA " 'AASV R2241
            SQL = SQL & Coma & " TX_ESTA_ENCA, TX_NOUSAN_ENCA,FE_FEANUL_ENCA " 'AASV M4858
            SQL = SQL & "INTO ANULA_TEMP "
            SQL = SQL & "FROM IN_DETALLE,IN_BODEGA,IN_ENCABEZADO,ARTICULO,TERCERO,IN_COMPROBANTE,USUARIO,IN_UNDVENTA "
            SQL = SQL & "WHERE NU_AUTO_BODE_DETA = NU_AUTO_BODE AND NU_AUTO_ORGCABE_DETA = NU_AUTO_ENCA AND "
            SQL = SQL & "NU_AUTO_ARTI_DETA = NU_AUTO_ARTI AND NU_AUTO_COMP_ENCA = NU_AUTO_COMP AND "
            SQL = SQL & "NU_AUTO_USUA_ENCA = NU_AUTO_USUA AND NU_AUTO_UNVE_ARTI = NU_AUTO_UNVE AND "
            SQL = SQL & "CD_CODI_TERC_ENCA = CD_CODI_TERC AND "
            'If Documento.Encabezado.TipDeDcmnt = AnulaEntrada Then SQL = SQL & "NU_AUTO_MODCABE_DETA = NU_AUTO_ENCA AND " 'HRR M2424
            MDI_Inventarios.Crys_Listar.Formulas(11) = "Movasoc = " & Movimiento_Asociado(txtNumero, CLng(Documento.Encabezado.TipDeDcmnt)) 'AASV M4681
            If MDI_Inventarios.Crys_Listar.Formulas(11) = "Movasoc = 0" Then MDI_Inventarios.Crys_Listar.Formulas(11) = "" 'AASV M4968
            'HRR M2424
            Select Case Documento.Encabezado.TipDeDcmnt
               Case AnulaEntrada, AnulaODCompra, AnulaRequisicion, AnulaCotizacion
                     SQL = SQL & "NU_AUTO_MODCABE_DETA = NU_AUTO_ENCA AND "
               Case Else
            End Select
            'HRR M2424
            SQL = SQL & "NU_AUTO_DOCU_ENCA=" & Documento.Encabezado.TipDeDcmnt
            SQL = SQL & " AND NU_COMP_ENCA=" & CLng(Me.txtNumero)
            SQL = SQL & " AND NU_AUTO_BODE_DETA=" & Me.cboBodega.ItemData(Me.cboBodega.ListIndex)
                
            If ExisteTABLA("ANULA_TEMP") Then Result = EliminarTabla("ANULA_TEMP")
            Result = ExecSQL(SQL)
            '''''
        Case DevolucionEntrada, DevolucionAprovecha, DevolucionDespacho, DevolucionBaja, DevolucionSalida, DevolucionFacturaVenta
            Formula = "{IN_DETALLE.NU_AUTO_ORGCABE_DETA}={IN_ENCABEZADO.NU_AUTO_ENCA} AND " & _
                "{IN_DETALLE.NU_AUTO_MODCABE_DETA}={IN_ENCABEZADO_1.NU_AUTO_ENCA} AND " & _
                "{IN_DETALLE.NU_AUTO_BODE_DETA}=" & Me.cboBodega.ItemData(Me.cboBodega.ListIndex) & " AND " & _
                "{IN_ENCABEZADO.NU_AUTO_DOCU_ENCA}=" & Documento.Encabezado.TipDeDcmnt & " AND " & _
                "{IN_ENCABEZADO.NU_COMP_ENCA}=" & CLng(Me.txtNumero)
             MDI_Inventarios.Crys_Listar.Formulas(11) = "Movasoc = " & Movimiento_Asociado(txtNumero, CLng(Documento.Encabezado.TipDeDcmnt)) 'AASV M4680
             'If MDI_Inventarios.Crys_Listar.Formulas(11) = "Movasoc = 0" Then MDI_Inventarios.Crys_Listar.Formulas(11) = "" 'AASV M4968 'JLPB T29892 Se deja en comentario
             If MDI_Inventarios.Crys_Listar.Formulas(11) = "Movasoc = 0" Then MDI_Inventarios.Crys_Listar.Formulas(11) = "Movasoc=" 'JLPB T29892
        Case Entrada, ODCompra
        ''''''''''REOL(57-166)
            'Dim SQL As String 'Consulta General
            SQL = "SELECT NU_COMP_ENCA,FE_CREA_ENCA,TX_OBSE_ENCA,TX_NOMB_COMP,TX_DESC_USUA,TX_NOMB_BODE,"
            SQL = SQL & "TX_CODI_BODE,NU_COSTO_DETA,NU_IMPU_DETA,NU_AUTO_DETA,NU_AUTO_ORGCABE_DETA,"
            SQL = SQL & "NU_ENTRAD_DETA,NU_SALIDA_DETA,NU_MULT_DETA,NU_DIVI_DETA,CD_CODI_TERC,NO_NOMB_TERC,"
            SQL = SQL & "CD_CODI_ARTI, NO_NOMB_ARTI,TX_NOMB_UNVE,NU_AUTO_DOCU_ENCA,NU_COMP_CONTA_ENCA " '|.DR.|, Se agreg� NU_AUTO_DOCU_ENCA y NU_COMP_CONTA_ENCA. R:1402
            'SQL = SQL & Coma & " NU_CONE_ENCA, TX_ESTA_ENCA " 'AASV R2241
            SQL = SQL & Coma & " TX_ESTA_ENCA, TX_NOUSAN_ENCA,FE_FEANUL_ENCA " 'AASV M4858
            SQL = SQL & "INTO ENTRAORDEN_TEMP "
            SQL = SQL & "FROM IN_ENCABEZADO,USUARIO,IN_DETALLE,IN_COMPROBANTE,ARTICULO,TERCERO,IN_UNDVENTA,IN_BODEGA "
            SQL = SQL & "WHERE NU_AUTO_USUA_ENCA = NU_AUTO_USUA AND NU_AUTO_ENCA = NU_AUTO_ORGCABE_DETA AND "
            SQL = SQL & "NU_AUTO_COMP_ENCA = NU_AUTO_COMP AND NU_AUTO_ARTI_DETA = NU_AUTO_ARTI AND "
            SQL = SQL & "NU_AUTO_BODE_DETA = NU_AUTO_BODE AND CD_CODI_TERC_ENCA = CD_CODI_TERC AND "
            If Documento.Encabezado.TipDeDcmnt = 2 Then 'REOL68 'VERIFICA LAS OC CON COTIZACION"
               SQL = SQL & "NU_AUTO_UNVE_ARTI = NU_AUTO_UNVE AND NU_AUTO_ENCA = NU_AUTO_ORGCABE_DETA AND "
               SQL = SQL & "NU_ENTRAD_DETA = 0 AND "
            Else
               SQL = SQL & "NU_AUTO_UNVE_ARTI = NU_AUTO_UNVE AND NU_AUTO_ENCA = NU_AUTO_MODCABE_DETA AND "
            End If
            SQL = SQL & "NU_AUTO_BODE_DETA = " & Me.cboBodega.ItemData(Me.cboBodega.ListIndex) & " AND "
            SQL = SQL & "NU_AUTO_DOCU_ENCA=" & Documento.Encabezado.TipDeDcmnt & " AND "
            SQL = SQL & "NU_COMP_ENCA=" & CLng(Me.txtNumero.Text) & " AND "
            SQL = SQL & "NU_AUTO_COMP_ENCA=" & Documento.Encabezado.NumeroCOMPROBANTE
            
            SQL = SQL & " ORDER BY NO_NOMB_ARTI ASC,NU_AUTO_ORGCABE_DETA ASC,NU_AUTO_DETA ASC"
            
            If ExisteTABLA("ENTRAORDEN_TEMP") Then Result = EliminarTabla("ENTRAORDEN_TEMP")
            Result = ExecSQL(SQL)
            
            'Formula = "{IN_ENCABEZADO.NU_AUTO_ENCA} = {IN_DETALLE.NU_AUTO_ORGCABE_DETA} AND " & _
            '    "{IN_DETALLE.NU_AUTO_MODCABE_DETA} = {IN_DETALLE_1.NU_AUTO_ORGCABE_DETA} AND " & _
            '    "{IN_DETALLE.NU_AUTO_MODCABE_DETA} = {IN_DETALLE_1.NU_AUTO_MODCABE_DETA} AND " & _
            '    "{IN_DETALLE.NU_AUTO_ARTI_DETA}={IN_DETALLE_1.NU_AUTO_ARTI_DETA} AND " & _
            '    "{IN_DETALLE.NU_AUTO_BODE_DETA}=" & Me.cboBodega.ItemData(Me.cboBodega.ListIndex) & " AND " & _
            '    "{IN_ENCABEZADO.NU_AUTO_DOCU_ENCA}=" & Documento.Encabezado.TipDeDcmnt & " AND " & _
            '    "{IN_ENCABEZADO.NU_COMP_ENCA}=" & CLng(Me.txtNumero.Text)
            'Formula = Formula & " AND {IN_ENCABEZADO.NU_AUTO_COMP_ENCA}=" & Documento.Encabezado.NumeroCOMPROBANTE  'PedroJ Def 3560
        ''''''''''REOL(57-166)
        Case Else
            Formula = "{IN_DETALLE.NU_AUTO_ORGCABE_DETA}={IN_ENCABEZADO.NU_AUTO_ENCA} AND " & _
                "{IN_DETALLE.NU_AUTO_MODCABE_DETA}={IN_ENCABEZADO_1.NU_AUTO_ENCA} AND " & _
                "{IN_DETALLE.NU_AUTO_BODE_DETA}=" & Me.cboBodega.ItemData(Me.cboBodega.ListIndex) & " AND " & _
                "{IN_ENCABEZADO.NU_AUTO_DOCU_ENCA}=" & Documento.Encabezado.TipDeDcmnt & " AND " & _
                "{IN_ENCABEZADO.NU_COMP_ENCA}=" & CLng(Me.txtNumero.Text) & _
                " AND {IN_ENCABEZADO.NU_AUTO_COMP_ENCA}=" & CLng(Documento.Encabezado.NumeroCOMPROBANTE) 'AASV M4041
        End Select

        MDI_Inventarios.Crys_Listar.Formulas(10) = "ENTIDAD=" & Comi & Trim(Dueno.Nombre) & ", NIT:" & Trim(Dueno.Nit) & Comi
        Reporte = "infDCANL_COS.rpt"
        
        Select Case Documento.Encabezado.TipDeDcmnt
        Case Is = Cotizacion
            Reporte = "infDCSOL_COS.rpt"
        Case Is = ODCompra
            Reporte = "infDCODC_COS.rpt"
            MDI_Inventarios.Crys_Listar.Formulas(11) = "Movasoc = " & Movimiento_Asociado(txtNumero, CLng(Documento.Encabezado.TipDeDcmnt)) 'AASV M4679
            If MDI_Inventarios.Crys_Listar.Formulas(11) = "Movasoc = 0" Then MDI_Inventarios.Crys_Listar.Formulas(11) = "" 'AASV M4968
            MDI_Inventarios.Crys_Listar.Formulas(12) = "DblTotal = '" & txtTOTAL & "'" 'SKRV T14531
        Case Is = Entrada ', AprovechaDonacion
            Reporte = "infDCENT_COS.rpt"
            MDI_Inventarios.Crys_Listar.Formulas(12) = "DblTotal = '" & txtTOTAL & "'" 'SKRV T14531
        Case Is = DevolucionEntrada
            Reporte = "infDCDVE_COS.rpt"
        Case Is = Requisicion
            Reporte = "infDCREQ_COS.rpt"
        'REOL471
        Case Is = Despacho
            'SQL = "SELECT DISTINCT NU_ENTRAD_DETA,NU_SALIDA_DETA,NU_MULT_DETA,NU_DIVI_DETA,NU_COSTO_DETA,"
            'DAHV M3904 Se ingresa el nu_auto_enca para poder obtener la relaci�n exacta de los productos que estan ingresados en el despacho
            SQL = "SELECT DISTINCT NU_AUTO_ENCA,NU_ENTRAD_DETA,NU_SALIDA_DETA,NU_MULT_DETA,NU_DIVI_DETA,NU_COSTO_DETA,"
            'DAHV M3904 Poder obtener el costo real del producto
            'SQL = SQL & "ROUND(ROUND(ROUND((100*NU_COSTO_DETA/(100+ NU_IMPU_DETA)),0) * (1 + NU_IMPU_DETA/100),2) * NU_SALIDA_DETA,0) COSTO,"
            'DAHV M4046 Se realiza ajuste para que se trabaje con decimales pero con precisi�n de dos decimales
            SQL = SQL & "ROUND(ROUND(ROUND((100*NU_COSTO_DETA/(100+ NU_IMPU_DETA))," & InDec & ") * (1 + NU_IMPU_DETA/100)," & InDec & ") * NU_SALIDA_DETA," & InDec & ") COSTO,"
            SQL = SQL & "CD_CODI_ARTI,NO_NOMB_ARTI,NU_COMP_ENCA,FE_CREA_ENCA,TX_NOMB_UNVE,TX_NOMB_BODE,"
            SQL = SQL & "TX_DESC_USUA,TX_NOMB_COMP,NU_AUTO_BODEDST_ENCA "
            SQL = SQL & ",NU_AUTO_DETA " 'NMSR M3416
            SQL = SQL & "INTO DESPA_TEMP "
            'SQL = SQL & "FROM IN_DETALLE,IN_ENCABEZADO,IN_COMPROBANTE,IN_BODEGA,IN_UNDVENTA,IN_DOCUMENTO,USUARIO,ARTICULO "
            If MotorBD = "SQL" Then 'NMSR M3714
                'SQL = SQL & "FROM IN_ENCABEZADO LEFT JOIN USUARIO ON (NU_AUTO_USUA_ENCA=NU_AUTO_USUA),IN_DETALLE,IN_COMPROBANTE,IN_BODEGA,IN_UNDVENTA,IN_DOCUMENTO,ARTICULO "  'NYCM M2999 'HRR DEPURACION
                SQL = SQL & "FROM IN_ENCABEZADO LEFT JOIN USUARIO ON (NU_AUTO_USUA_ENCA=NU_AUTO_USUA),IN_DETALLE,IN_COMPROBANTE,IN_BODEGA,IN_UNDVENTA,ARTICULO "  'NYCM M2999 'HRR DEPURACION
                SQL = SQL & "WHERE NU_AUTO_ORGCABE_DETA=NU_AUTO_ENCA AND NU_AUTO_ARTI_DETA=NU_AUTO_ARTI AND "
                SQL = SQL & "NU_AUTO_UNVE_ARTI=NU_AUTO_UNVE AND NU_AUTO_BODEORG_ENCA=NU_AUTO_BODE AND "
                'SQL = SQL & "NU_AUTO_COMP_ENCA=NU_AUTO_COMP AND NU_AUTO_USUA_ENCA=NU_AUTO_USUA AND "
                SQL = SQL & "NU_AUTO_COMP_ENCA=NU_AUTO_COMP AND " 'NYCM M2999
                SQL = SQL & "NU_AUTO_BODE_DETA=" & Me.cboBodega.ItemData(Me.cboBodega.ListIndex) & " AND "
                SQL = SQL & "NU_AUTO_DOCU_ENCA=" & Documento.Encabezado.TipDeDcmnt & " AND "
                SQL = SQL & "NU_COMP_ENCA=" & CLng(Me.txtNumero.Text)
                SQL = SQL & "AND NU_AUTO_COMP_ENCA=" & Documento.Encabezado.NumeroCOMPROBANTE 'APGR T6899
            'NMSR M3714
            Else
                SQL = SQL & "FROM IN_UNDVENTA INNER JOIN (IN_BODEGA INNER JOIN (IN_COMPROBANTE INNER JOIN "
                SQL = SQL & "(ARTICULO INNER JOIN ((IN_ENCABEZADO  LEFT JOIN USUARIO ON "
                SQL = SQL & "IN_ENCABEZADO.NU_AUTO_USUA_ENCA = USUARIO.NU_AUTO_USUA) INNER JOIN "
                SQL = SQL & "IN_DETALLE ON IN_ENCABEZADO.NU_AUTO_ENCA = IN_DETALLE.NU_AUTO_ORGCABE_DETA) "
                SQL = SQL & "ON ARTICULO.NU_AUTO_ARTI = IN_DETALLE.NU_AUTO_ARTI_DETA) ON "
                SQL = SQL & " IN_COMPROBANTE.NU_AUTO_COMP = IN_ENCABEZADO.NU_AUTO_COMP_ENCA) ON "
                SQL = SQL & "IN_BODEGA.NU_AUTO_BODE = IN_ENCABEZADO.NU_AUTO_BODEORG_ENCA) ON "
                SQL = SQL & "IN_UNDVENTA.NU_AUTO_UNVE = ARTICULO.NU_AUTO_UNVE_ARTI"
                SQL = SQL & " WHERE NU_AUTO_BODE_DETA=" & Me.cboBodega.ItemData(Me.cboBodega.ListIndex) & " AND "
                SQL = SQL & "NU_AUTO_DOCU_ENCA=" & Documento.Encabezado.TipDeDcmnt & " AND "
                SQL = SQL & "NU_COMP_ENCA=" & CLng(Me.txtNumero.Text)
            End If
            'NMSR M3714
            SQL = SQL & " ORDER BY NO_NOMB_ARTI ASC "
            If ExisteTABLA("DESPA_TEMP") Then Result = EliminarTabla("DESPA_TEMP")
            Result = ExecSQL(SQL)
            
            'Formula = Formula & " AND (" & "{IN_DOCUMENTO_1.NU_AUTO_DOCU}=" & Despacho & _
            '    " OR {IN_DOCUMENTO_1.NU_AUTO_DOCU}=" & DevolucionDespacho & ")"
            'Formula = Formula & " AND " & "{IN_ENCABEZADO.NU_AUTO_COMP_ENCA}=" & Documento.Encabezado.NumeroCOMPROBANTE     'PedroJ Def 3981
             
             Formula = ""
            'AASV R2236 OB INICIO
            'MDI_Inventarios.Crys_Listar.Formulas(11) = "Movasoc = " & Movimiento_Asociado(txtNumero, CLng(Documento.Encabezado.TipDeDcmnt)) 'AASV R2236 OB 'APGR T6899 PNC
            MDI_Inventarios.Crys_Listar.Formulas(11) = "Movasoc = " & Movimiento_Asociado(txtNumero, CLng(Documento.Encabezado.TipDeDcmnt), CLng(Documento.Encabezado.NumeroCOMPROBANTE)) 'APGR T6899 PNC
            If MDI_Inventarios.Crys_Listar.Formulas(11) = "Movasoc = 0" Then MDI_Inventarios.Crys_Listar.Formulas(11) = ""
            'AASV R2236 OB FIN
            Reporte = "infDCDES_COS.rpt"
        Case Is = DevolucionDespacho
            Reporte = "infDCDVD_COS.rpt"
        Case Is = BajaConsumo
            Formula = Formula & " AND (" & "{IN_DOCUMENTO_1.NU_AUTO_DOCU}=" & BajaConsumo & _
                " OR {IN_DOCUMENTO_1.NU_AUTO_DOCU}=" & DevolucionBaja & _
                " OR {IN_DOCUMENTO_1.NU_AUTO_DOCU}=" & AnulaBaja & ")"
            Formula = Formula & " AND {IN_ENCABEZADO.NU_AUTO_COMP_ENCA}=" & Documento.Encabezado.NumeroCOMPROBANTE      'PedroJ Def 2392
            'AASV R2236 OB INICIO
            MDI_Inventarios.Crys_Listar.Formulas(11) = "Movasoc = " & Movimiento_Asociado(txtNumero, CLng(Documento.Encabezado.TipDeDcmnt))
            If MDI_Inventarios.Crys_Listar.Formulas(11) = "Movasoc = 0" Then MDI_Inventarios.Crys_Listar.Formulas(11) = ""
            'AASV R2236 OB FIN
            Reporte = "infDCBAJ_COS.rpt"
        Case Is = DevolucionBaja
            Reporte = "infDCDVB_COS.rpt"
        Case Is = FacturaVenta
            'DEGP T43448 INICIO SE DEJA EN COMENTARIO LA SIGUIENTE L�NEA
            'vValorParcial = 0: vValorAcumula = CCur(Me.txtTOTAL.Text)
            vValorParcial = 0: vValorAcumula = 0
            For Incont = 1 To GrdArticulos.Rows - 1
               vValorAcumula = vValorAcumula + (GrdArticulos.TextMatrix(Incont, 19) * GrdArticulos.TextMatrix(Incont, 10))
            Next
            'DEGP T43448 FIN
            MDI_Inventarios.Crys_Listar.Formulas(11) = "ValorBruto=" & vValorAcumula
            vValorParcial = SumaOtrosCargos()
            MDI_Inventarios.Crys_Listar.Formulas(12) = "OtrosCargos=" & vValorParcial
            vValorAcumula = vValorAcumula + vValorParcial
            vValorParcial = SumaOtrosDescuentos()
            vValorAcumula = vValorAcumula - IIf(UCase(Me.staINFORMA.Panels("NUEVO").Text) = UCase("Nuevo"), vValorParcial, 0) 'JLPB T41054
            MDI_Inventarios.Crys_Listar.Formulas(13) = "OtrosDescuentos=" & vValorParcial
            'vValorAcumula = vValorAcumula - vValorParcial REOL174 El Total ya tiene el descuento
            '-------------------------------------------------------------------------------------------------------------------------------
            'DAHV M3995 - INICIO
            'MDI_Inventarios.Crys_Listar.Formulas(14) = "ValorNumeros=" & vValorAcumula
            'MDI_Inventarios.Crys_Listar.Formulas(15) = "ValorLetras=" & Comi & Monto_Escrito(vValorAcumula) & Comi
            'JAGS T10025 INICIO
            'MDI_Inventarios.Crys_Listar.Formulas(14) = "ValorNumeros=" & vValorAcumula - vValorParcial
            MDI_Inventarios.Crys_Listar.Formulas(14) = "ValorNumeros=" & vValorAcumula
            'MDI_Inventarios.Crys_Listar.Formulas(15) = "ValorLetras=" & Comi & Monto_Escrito(vValorAcumula - vValorParcial) & Comi
            MDI_Inventarios.Crys_Listar.Formulas(15) = "ValorLetras=" & Comi & Monto_Escrito(vValorAcumula) & Comi
            'JAGS T10025 FIN
            'DAHV M3995 - FIN
            '---------------------------------------------------------------------------------------------------------------------------------
            'REOL174
            MDI_Inventarios.Crys_Listar.Formulas(16) = "Descuento=0"
            MDI_Inventarios.Crys_Listar.Formulas(16) = "Descuento=" & GrdArticulos.TextMatrix(1, 17)
            Reporte = "infDCFDV_COS.rpt"
            MDI_Inventarios.Crys_Listar.Formulas(17) = "Movasoc = " & Movimiento_Asociado(txtNumero, CLng(Documento.Encabezado.TipDeDcmnt)) 'AASV M4680 Nota:22217
            'If MDI_Inventarios.Crys_Listar.Formulas(17) = "Movasoc = 0" Then MDI_Inventarios.Crys_Listar.Formulas(11) = "" 'AASV M4968 'HRR M4968
            If MDI_Inventarios.Crys_Listar.Formulas(17) = "Movasoc = 0" Then MDI_Inventarios.Crys_Listar.Formulas(17) = "" 'AASV M4968 'HRR M4968
            'DRMG T44728-R41288 INICIO SE ENVIA EL PREFIJO
            Condicion = "NU_NUCOMP_FAFA=" & txtNumero
            Condicion = Condicion & " AND NU_DOCU_FAFA=" & FacturaVenta
            Condicion = Condicion & " AND NU_COMP_FAFA=" & Documento.Encabezado.NumeroCOMPROBANTE
            Campos = "TX_PREFI_FAFA + '-' + CONVERT(VARCHAR,NU_CNFAVE_FAFA)"
            StPrefijo = fnDevDato("R_FAVE_FAEL", Campos, Condicion, True)
            MDI_Inventarios.Crys_Listar.Formulas(18) = "PREFIJ0='" & StPrefijo & Comi
            
            ReDim VrArr(2)
            Campos = "TX_ENCA_FAEL,TX_PIEUNO_FAEL,TX_PIEDOS_FAEL"
            Condicion = "NU_MODULO_FAEL=1"
            Condicion = Condicion & " AND NU_ESTADO_FAEL=0"
            Result = LoadData("FACTURA_ELECTRONICA", Campos, Condicion, VrArr())
            If Result <> FAIL Then
               If Encontro Then
                  MDI_Inventarios.Crys_Listar.Formulas(19) = "ENCABEZADO='" & VrArr(0) & Comi
                  MDI_Inventarios.Crys_Listar.Formulas(20) = "PIE1='" & VrArr(1) & Comi
                  MDI_Inventarios.Crys_Listar.Formulas(21) = "PIE2='" & VrArr(2) & Comi
               End If
            End If
            
            If Cargar_Logo Then MDI_Inventarios.Crys_Listar.Formulas(22) = "LOGO='" & 1 & Comi
            'DRMG T44728-R41288 FIN
        Case Is = DevolucionFacturaVenta
            vValorParcial = 0: vValorAcumula = CCur(Me.txtTOTAL.Text)
            MDI_Inventarios.Crys_Listar.Formulas(11) = "ValorBruto=" & vValorAcumula
            vValorParcial = SumaOtrosCargos()
            MDI_Inventarios.Crys_Listar.Formulas(12) = "OtrosCargos=" & vValorParcial
            vValorAcumula = vValorAcumula + vValorParcial
            vValorParcial = SumaOtrosDescuentos()
            MDI_Inventarios.Crys_Listar.Formulas(13) = "OtrosDescuentos=" & vValorParcial
            vValorAcumula = vValorAcumula - vValorParcial
            MDI_Inventarios.Crys_Listar.Formulas(14) = "ValorNumeros=" & vValorAcumula
            MDI_Inventarios.Crys_Listar.Formulas(15) = "ValorLetras=" & Comi & Monto_Escrito(vValorAcumula) & Comi
            MDI_Inventarios.Crys_Listar.Formulas(16) = "Movasoc = " & Movimiento_Asociado(txtNumero, CLng(Documento.Encabezado.TipDeDcmnt))
            'If MDI_Inventarios.Crys_Listar.Formulas(16) = "Movasoc = 0" Then MDI_Inventarios.Crys_Listar.Formulas(11) = "" 'AASV M4968 'HRR M4968
            If MDI_Inventarios.Crys_Listar.Formulas(16) = "Movasoc = 0" Then MDI_Inventarios.Crys_Listar.Formulas(16) = "" 'AASV M4968 'HRR M4968
            Reporte = "infDCDVF_COS.rpt"
        Case Is = Traslado
            Reporte = "infDCTRA_COS.rpt"
        'REOL919
        Case Is = Salida
                Reporte = "infDCANL_COS1.rpt"
        Case Is = AprovechaDonacion
                Reporte = "infDCENT_COS1.rpt"
        End Select
        Call ListarD(Reporte, Formula, crptToWindow, Documento.Encabezado.NombreDocumento, MDI_Inventarios, True)
        Documento.Encabezado.NumeroImpresiones
        MDI_Inventarios.Crys_Listar.Formulas(10) = ""
        MDI_Inventarios.Crys_Listar.Formulas(11) = ""
        MDI_Inventarios.Crys_Listar.Formulas(12) = ""
        MDI_Inventarios.Crys_Listar.Formulas(13) = ""
        MDI_Inventarios.Crys_Listar.Formulas(14) = ""
        MDI_Inventarios.Crys_Listar.Formulas(15) = ""
        MDI_Inventarios.Crys_Listar.Formulas(16) = ""
    Case Is = 3
         ''JACC R1428-1871-2143-2364
         ChkRequ.value = 0
         If ChkRequ.Visible Then ChkRequ.Enabled = True
         'JACC R1428-1871-2143-2364
         StDoc = NUL$ 'HRR M1819
        boFact = False
        Me.imgNoCont.Visible = False
        Me.FrmImpuestos.Enabled = Not imgNoCont.Visible
        lblNoCont.Visible = imgNoCont.Visible
        Me.opcORG.value = True
        Me.opcACT.value = False
        Me.chaSALDOS.Visible = False
        Me.lstSALXBOD.Visible = False
        Me.FraPie.Visible = True
       'DRMG T44728-R41288 INICION
       If Documento.Encabezado.TipDeDcmnt = FacturaVenta Then
           Me.FrmFactElect.Visible = True
       End If
       'DRMG T44728-R41288 FIN

        Me.GrdArticulos.Rows = 2
        
        Me.cboConsecutivo.ListIndex = -1
        Me.cboBodega.ListIndex = -1
        Me.cboBodDestino.ListIndex = -1
        Me.txtDocum(1) = Format(Me.UltimaFechaDocumento, "dd")
        Me.txtDocum(2) = Format(Me.UltimaFechaDocumento, "mm")
        Me.txtDocum(3) = Format(Me.UltimaFechaDocumento, "yyyy")
        Me.txtDocum(21) = Format(Me.FechaVigencia, "dd")
        Me.txtDocum(22) = Format(Me.FechaVigencia, "mm")
        Me.txtDocum(23) = Format(Me.FechaVigencia, "yyyy")
        Me.txtDocum(5).Text = ""
        Me.txtDocum(6).Text = ""
        Me.txtDocum(8).Text = ""
        Me.txtDocum(9).Text = ""
        Me.lstDocsPadre.Clear

        ''Para posici�n inicial en el grid
        Me.FilaDeGrillaActiva = GrdArticulos.Rows - 1
        Me.ColumnaDeGrillaActiva = 2
        WinDoc.LimpiaLineaGrilla 1, Me.ColCodigoArticulo, ""
        Documento.Encabezado.EsNuevoDocumento = True
        Me.EstaAnulado = False
        Me.FraArti.Enabled = False
        Me.FraRequi.Visible = False
        Me.FraGeneral.Enabled = True
        Me.CmdCuentas.Visible = Aplicacion.Interfaz_Contabilidad
        If Me.cboConsecutivo.Enabled Then
            Me.cboConsecutivo.SetFocus
        Else
            If Me.cboConsecutivo.ListCount > 0 Then Me.cboConsecutivo.ListIndex = 0
        End If
        Me.FraOpciones.Enabled = False
        Me.FraArti.BackColor = &H8000000A
        Me.FraPie.BackColor = &H8000000A
        Me.FraRequi.BackColor = &H8000000A
        Me.FraOpciones.BackColor = &H8000000A
        Me.FraGeneral.BackColor = &H8000000F
        'DRMG T44728-R41288 INICION
        Me.FrmFactElect.BackColor = &H8000000A
        Me.FrmFactElect.Enabled = False
        'DRMG T44728-R41288 FIN
        
        'WinDoc.CargarCombos 'DAHV M4235
        Me.txtDocum(5).Text = Dueno.Nit
        Me.txtDocum(6).Text = Dueno.Nombre
        Me.staINFORMA.Panels("NUEVO").Text = "NUEVO"
        Me.staINFORMA.Panels("COSTO").Text = ""
        Me.staINFORMA.Panels("PARCIAL").Text = ""
        Me.staINFORMA.Panels("SALBOD").Text = "Saldos X Bodega"
        Me.staINFORMA.Visible = False
        Me.txtSUBTO = 0     'PedroJ
        Me.txtIMPUE = 0
        Me.txtTOTAL = 0
        Me.TxtCompra(10) = 0        'PedroJ
        Dim i As Double
        For i = 7 To 20
           If i <> 9 Then Me.TxtEntra(i) = 0
        Next i
        
        TrvLista.Nodes.Clear
        TrvLista.Enabled = True
        FrmImpuestos.Enabled = True
        'Cmd_agregar.Enabled = True
        Cmd_agregar.Enabled = False 'smdl 1682 - se cambia true por false al limpiar la ventana
        Cmd_Quitar.Enabled = True
        cmdREGRESAR(3).Enabled = True 'smdl m1822
        GrdDeIm.Rows = 1
        Set Dueno = Nothing
        
        If Me.cmdOpciones(0).Enabled = False Then Me.cmdOpciones(0).Enabled = True  'PedroJ
        If Me.cmdOpciones(1).Enabled = False Then Me.cmdOpciones(1).Enabled = True  'PedroJ
        
        'REOL MANTIS:0000569
        'Set tItem = MDI_Inventarios.Lstw_Formas.ListItems("L" & FrmMenu.CodOpc) APGR T1643
        Set tItem = MDI_Inventarios.Lstw_Formas.ListItems("L" & CodOpc) 'APGR T1643
        If Mid(tItem.Text, 2, 1) = "N" Then cmdOpciones(1).Enabled = False
        
        TxtPpto(0) = NUL$: TxtPpto(1) = NUL$: TxtPpto(3) = NUL$         'PJCA M1206
        'HRR R1700
        cmdDocsPadre(0).Enabled = True
        cmdDocsPadre(1).Enabled = True
        BoConsMov = False
        'HRR R1700
        Documento.CargaDocumentORIGINAL (0)     'REOL M2255
        LnAntoEncaEnid = 0 'GMS M3559
        
       'DRMG T44728-R41288 INICIO
       If Not (Documento.Encabezado.TipDeDcmnt = FacturaVenta) Or LblFactElt.Visible = False Then
          TxtFactElt.Visible = False
          LblFactElt.Visible = False
          txtNumero.Top = 360
          LblNumero.Top = 360
       Else
          TxtFactElt.Visible = True
          LblFactElt.Visible = True
          Condicion = "NU_MODULO_FAEL=1"
          Condicion = Condicion & " AND NU_ESTADO_FAEL=0"
          TxtFactElt = fnDevDato("FACTURA_ELECTRONICA", "NU_CONFAC_FAEL", Condicion)
          TxtFactElt = IIf(TxtFactElt <> NUL$, (CDbl(TxtFactElt) + 1), NUL$)
       End If
       'DRMG T44728-R41288 FIN
       
    End Select
      
End Sub

Private Sub cmdOpciones_GotFocus(Index As Integer)
 Select Case Index
    Case 0: Msglin "Pulse este icono para GrabarDocumento"
    Case 1: Msglin "Pulse este icono para anular"
    Case 2: Msglin "Pulse este icono para imprimir"
    Case 3: Msglin "Pulse este icono limpiar y empezar de nuevo"
  End Select
End Sub

Private Sub BuscarDocumento()
    Dim ArrTemp()
    Dim UnRsltd As Long
    Dim InI As Integer 'HRR R1700
    Dim LnDocDepe As Long 'HRR R1700
    Dim StNombForm As String
    StNombForm = Caption 'EACT 18441
    If cboConsecutivo.ListIndex = -1 Then Exit Sub
    Call Documento.REMALLArticulo
    Documento.Encabezado.NumeroCOMPROBANTE = cboConsecutivo.ItemData(cboConsecutivo.ListIndex)
    If Me.opcACT.value Then
        UnRsltd = Documento.CargaDocumentoACTUAL(CLng(Me.txtNumero.Text))
    ElseIf Me.opcORG.value Then
        'UnRsltd = Documento.CargaDocumentORIGINAL(CLng(Me.txtNumero.Text)) 'EACT 18441
        UnRsltd = Documento.CargaDocumentORIGINAL(CLng(Me.txtNumero.Text), StNombForm) 'EACT 18441
    End If
    ''JACC R1428-1871-2143-2364
    'Busca si tiene documentos dependientes y si son de requicision
    If Documento.Encabezado.TipDeDcmnt = ODCompra Then
         ChkRequ.Enabled = False
         Campos = "NU_AUTO_DOCU_ENCA"
         Desde = "IN_R_ENCA_ENCA,IN_ENCABEZADO"
         Condi = "NU_AUTO_ENCA=NU_ENCDEP_RENEN"
         Condi = Condi & " AND NU_ENCCMP_RENEN=(SELECT NU_AUTO_ENCA FROM IN_DOCUMENTO, IN_COMPROBANTE, IN_ENCABEZADO "
         Condi = Condi & " WHERE NU_AUTO_COMP = NU_AUTO_COMP_ENCA AND NU_AUTO_DOCU = NU_AUTO_DOCU_COMP" & _
                 " AND NU_AUTO_COMP_ENCA=" & Documento.Encabezado.NumeroCOMPROBANTE & " AND NU_COMP_ENCA=" & CLng(Me.txtNumero.Text)
         Condi = Condi & " )"
         ReDim ArrDOCO(0)
         Result = LoadData(Desde, Campos, Condi, ArrDOCO)
         If Result <> FAIL And Encontro Then
            If ArrDOCO(0) = Requisicion Then
              ChkRequ.value = 1
            Else
              ChkRequ.value = 0
            End If
         Else
             ChkRequ.value = 0
         End If
    End If
    'JACC R1428-1871-2143-2364
    
    
    
    If UnRsltd = ResultConsulta.Encontroinfo Then
        Call CargaValoresParticulatres
        Select Case Me.AutoTipoDocumento
        Case Is = Entrada
''/////////
            Me.DefinicionDeGrilla = "Autoart,+0,Marcar,800," & _
                "Codigo,1900," & _
                "Lote Producci�n,0," & _
                "Nombre,3500," & _
                "AutoUndMedida,+0," & _
                "Und Medida,1000," & _
                "Multiplicador,+0," & _
                "Divisor,+0," & _
                "Ultimo Costo,+0," & _
                "Cantidad,+800," & _
                "Costo Sin IVA,+1500," & _
                "Venta Sin IVA,+0," & _
                "IVA,+800," & _
                "Costo Unitario,+1500," & _
                "Costo Total,+1500," & _
                "Venta Sin DESC,+0," & _
                "% Descuento,+0," & _
                "Valor Descuento,+0," & _
                "Venta Unitario,+0," & _
                "Venta Total,+0," & _
                "Fecha Vencimiento,1200," & _
                "Fecha Entrada,1200," & _
                "Control C/dad,+0," & _
                "Pertenece A,+0," & _
                "Select,+0,"
                
                'HRR Req1553
                Me.DefinicionDeGrilla = Me.DefinicionDeGrilla & _
                "COD Impuesto,+0," & _
                "Control CostSinIva,+0," & _
                "Control PorceIva,+0," & _
                "Control CostUnidad,+0"
                cambioCelda = False
                'Req1553
''/////////
        End Select
        Call GrdFDef(GrdArticulos, 0, 0, Me.DefinicionDeGrilla)
'        Me.cboIndependiente.Visible = True: Me.lblINDEPE.Visible = True
        Me.cboIndependiente.ListIndex = 0
        Documento.Encabezado.EsNuevoDocumento = False
        Me.staINFORMA.Panels("NUEVO").Text = "CREADO"
'        Me.UltimaFechaDocumento = CLng(Documento.Encabezado.FechaDocumento)
        Me.txtDocum(1) = Format(Documento.Encabezado.FechaDocumento, "dd")
        Me.txtDocum(2) = Format(Documento.Encabezado.FechaDocumento, "mm")
        Me.txtDocum(3) = Format(Documento.Encabezado.FechaDocumento, "yyyy")
        Me.txtDocum(21) = Format(Documento.Encabezado.FechaVigencia, "dd")
        Me.txtDocum(22) = Format(Documento.Encabezado.FechaVigencia, "mm")
        Me.txtDocum(23) = Format(Documento.Encabezado.FechaVigencia, "yyyy")
    
        Me.txtDocum(5) = Documento.Encabezado.Tercero.Nit
        ''JACC M6427
'        If Documento.Encabezado.TipDeDcmnt = Entrada Then
'            ReDim Arrter(1)
'            Result = LoadData("Tercero, PARAMETROS_IMPUESTOS", "ID_TIPO_TERC,ID_TIPO_REGI_PAIM", "CD_CODI_TERC=CD_CODI_TERC_PAIM AND CD_CODI_TERC='" & Me.txtDocum(5) & Comi, Arrter)
'            If Result <> FAIL And Encontro Then
'              TipoRegimen = Arrter(1)
'              TipoPers = Arrter(0)
'            End If
'        End If
        'JACC M6427
        txtDocum(6) = Documento.Encabezado.Tercero.Nombre
        txtDocum(8) = Documento.Encabezado.Observaciones
        txtDocum(9) = Documento.Encabezado.CondicionesComerciales
        txtNumero.Tag = Documento.Encabezado.AutoDelDocCARGADO
        ReDim ArrTemp(0)
        Result = LoadData("IN_BODEGA", "TX_NOMB_BODE", "NU_AUTO_BODE=" & Documento.Encabezado.BodegaORIGEN, ArrTemp())
        Me.cboBodega.Clear
        If Result <> FAIL And Encontro Then
            Me.cboBodega.AddItem ArrTemp(0)
            Me.cboBodega.ItemData(Me.cboBodega.NewIndex) = Documento.Encabezado.BodegaORIGEN
            Me.cboBodega.ListIndex = Me.cboBodega.NewIndex
        End If
        Result = LoadData("IN_BODEGA", "TX_NOMB_BODE", "NU_AUTO_BODE=" & Documento.Encabezado.BodegaDESTINO, ArrTemp())
        Me.cboBodDestino.Clear
        If Result <> FAIL And Encontro Then
            Me.cboBodDestino.AddItem ArrTemp(0)
            Me.cboBodDestino.ItemData(Me.cboBodDestino.NewIndex) = Documento.Encabezado.BodegaDESTINO
            Me.cboBodDestino.ListIndex = Me.cboBodDestino.NewIndex
        End If
        GrdArticulos.Rows = 1
        Me.fraTOTALES.Visible = False
        NoHacerNada = True
        Call Documento.LlenaFgridConArticulos(GrdArticulos, Me, Documento.Encabezado.AutoDelDocCARGADO)
        Me.fraTOTALES.Visible = True
        If Documento.Encabezado.EstadoDocumento = "A" Then
            Me.EstaAnulado = True
            Call MsgBox("Documento ANULADO", vbInformation)
        ElseIf Documento.Encabezado.EstadoDocumento = "M" Then
            Call MsgBox("Documento MODIFICADO", vbInformation)
        End If
        If (Documento.Encabezado.TipDeDcmnt = FacturaVenta) Or (Documento.Encabezado.TipDeDcmnt = DevolucionFacturaVenta) Then
'            Me.staINFORMA.Panels("COSTO").Text = Aplicacion.Formatear_Valor(WinDoc.Calcular_Documento(Me.ColCantidad, Me.ColVenUndConDes))
            Me.staINFORMA.Panels("COSTO").Text = Aplicacion.Formatear_Valor(WinDoc.Calcular_Documento(Me.ColCantidad, Me.ColVentaSinIVA))   'REOL M2521/M2520
        Else
            'Me.staINFORMA.Panels("COSTO").Text = Aplicacion.Formatear_Valor(WinDoc.Calcular_Documento(Me.ColCantidad, Me.ColCostoUnidad))
            Me.staINFORMA.Panels("COSTO").Text = Aplicacion.Formatear_Valor(WinDoc.Calcular_Documento(Me.ColCantidad, Me.ColCostoSinIVA))
        End If
        NoHacerNada = False
        Call IrAlCuerpo
        Me.cmdOpciones(0).Enabled = False
        Select Case Documento.Encabezado.TipDeDcmnt
            Case Is = DevolucionBaja: Me.cmdOpciones(1).Enabled = False
            Case Is = DevolucionEntrada: Me.cmdOpciones(1).Enabled = False 'REOL M1078
            Case Is = DevolucionDespacho: Me.cmdOpciones(1).Enabled = False
            Case Is = DevolucionSalida: Me.cmdOpciones(1).Enabled = False
            Case Is = DevolucionFacturaVenta: Me.cmdOpciones(1).Enabled = False
            Case Is = DevolucionAprovecha: Me.cmdOpciones(1).Enabled = False
        End Select
       'REOL M2606
        ReDim Arr(0)
        Condi = "NU_AUTO_DOCU = NU_AUTO_DOCU_COMP AND NU_AUTO_COMP = " & Documento.Encabezado.NumeroCOMPROBANTE
        Result = LoadData("IN_DOCUMENTO, IN_COMPROBANTE", "NU_AUTO_DOCU", Condi, Arr)
        If Result <> FAIL And Encontro Then
            Select Case Arr(0)
                Case Is = AnulaCotizacion: cmdOpciones(1).Enabled = False
                Case Is = AnulaODCompra: cmdOpciones(1).Enabled = False
                Case Is = AnulaEntrada: cmdOpciones(1).Enabled = False
                Case Is = AnulaRequisicion: cmdOpciones(1).Enabled = False
                Case Is = AnulaDespacho: cmdOpciones(1).Enabled = False
                Case Is = AnulaTraslado: cmdOpciones(1).Enabled = False
                Case Is = AnulaBaja: cmdOpciones(1).Enabled = False
                Case Is = AnulaSalida: cmdOpciones(1).Enabled = False
                Case Is = AnulaFacturaVenta: cmdOpciones(1).Enabled = False
                Case Is = AnulaAprovecha: cmdOpciones(1).Enabled = False
            End Select
        End If
       'FIN REOL
       'PedroJ
        If Aplicacion.Interfaz_Presupuesto And (Documento.Encabezado.TipDeDcmnt = ODCompra Or Documento.Encabezado.TipDeDcmnt = Entrada) Then
          ReDim Arr(0)
          Select Case Documento.Encabezado.TipDeDcmnt
            Case Is = ODCompra: Condicion = " NU_TIPO_PTAL_ENPP=1"  'Leer el CDP
            Case Is = Entrada: Condicion = " NU_TIPO_PTAL_ENPP=2"   'Leer el RP
          End Select
          Condicion = Condicion & " AND NU_AUTO_ENCA_ENPP=" & Documento.Encabezado.AutoDelDocCARGADO
          Result = LoadData("IN_R_ENCA_PPTO", "NU_CONS_PTAL_ENPP", Condicion, Arr)
          If Result <> FAIL And Encontro Then Me.TxtPpto(0) = Arr(0) Else Me.TxtPpto(0) = 0
        End If
        'PedroJ
       'REOL174 Carga la grilla con el VALOR de descuento
        ' DAHV R2221 - INICIO
        ' Como la tabla IN_PARTIPARTI almacena diferentes valores toca realizar validaci�n
        ' al codigo del concepto
        'ReDim Arr(0)
        'Result = LoadData("IN_PARTIPARTI", "NU_VALO_PRPR", "NU_AUTO_ENCA_PRPR=" & Documento.Encabezado.AutoDelDocCARGADO, Arr)
        ReDim Arr(1)
        Result = LoadData("IN_PARTIPARTI", "NU_VALO_PRPR,TX_CODI_PART_PRPR", "NU_AUTO_ENCA_PRPR=" & Documento.Encabezado.AutoDelDocCARGADO, Arr)
        'DAHV R2221 - FIN
        
'        If Result <> FAIL And Encontro Then
        If Result <> FAIL And Encontro And GrdArticulos.Rows > 1 Then   'REOL M2187
           grdOtrosDescuentos.Rows = grdOtrosDescuentos.Rows + 1
           'grdOtrosDescuentos.TextMatrix(1, 2) = GrdArticulos.TextMatrix(1, 16) / 100 * Arr(0)
           'JACC R2221
           'DAHV R2221 -  INICIO
           'Que siga manteniendo el funcionamiento hasta el momento
           'If IsNumeric(Arr(0)) Then
           If (IsNumeric(Arr(0)) And Val(Arr(1)) <> 8) Then
           'DAHV R2221 -  FIN
             grdOtrosDescuentos.TextMatrix(1, 2) = GrdArticulos.TextMatrix(1, 16) / 100 * Arr(0)
           End If
           'JACC R2221
        End If
       'REOL174
       
       'HRR R1700
       LnDocDepe = Documento.Encabezado.DocumentoDep
       If LnDocDepe <> 0 Then
         Me.FraRequi.Visible = True
         cmdDocsPadre(0).Enabled = False
         cmdDocsPadre(1).Enabled = False
         Call Me.lstDocsPadre.AddItem(LnDocDepe)
         BoConsMov = True
       End If

       'HRR R1700
       
       'DRMG T44728-R41288 INICIO
       If (Documento.Encabezado.TipDeDcmnt = FacturaVenta) Then
          ReDim VrArr(0)
          Valores = "NU_CNFAVE_FAFA"
          Condicion = "NU_NUCOMP_FAFA=" & txtNumero
          Condicion = Condicion & " AND NU_DOCU_FAFA=" & FacturaVenta
          Condicion = Condicion & " AND NU_COMP_FAFA=" & Documento.Encabezado.NumeroCOMPROBANTE
          Result = LoadData("R_FAVE_FAEL", Valores, Condicion, VrArr())
          If Result <> FAIL Then
             If Encontro Then
                TxtFactElt.Text = VrArr(0)
                ReDim VrArr(0)
                'DRMG T45136 INICIO SE DJEA EN COMENTARIO LAS SIGUIENTES 3 LINEAS
'                Valores = "NU_ESTA_FAEL"
'                Valores = Valores & Coma & "NU_ESTXML_FAEL"
'                Condicion = "NU_MODULO_FAEL=1 AND NU_NUMFAC_FAEL=" & TxtFactElt
                Valores = "NU_ESTA_ENFA"
                Condicion = "NU_MODULO_ENFA=1 AND NU_NUMFAC_ENFA=" & TxtFactElt
                'DRMG T45136 FIN
                If ByFael = 1 Then 'DRMG T45277-R41288
                   Result = LoadData("ENVIO_FAEL", Valores, Condicion, VrArr())
                   If Encontro Then
                      Me.FrmFactElect.BackColor = &H8000000F
                      FrmFactElect.Enabled = True
                      If VrArr(0) = True And VrArr(0) = True Then ScmdFacEL(2).Enabled = False
                   Else
                      Me.FrmFactElect.BackColor = &H8000000F
                      FrmFactElect.Enabled = True
                      ScmdFacEL(0).Enabled = True
                      ScmdFacEL(2).Enabled = True
                   End If
                'DRMG T45277-R41288 INICIO
                Else
                   FrmFactElect.Enabled = False
                   Me.FrmFactElect.BackColor = &H8000000A
                End If
                'DRMG T45277-R41288 FIN
             Else
                TxtFactElt.Text = NUL$
                FrmFactElect.Enabled = False
                Me.FrmFactElect.BackColor = &H8000000A
             End If
          Else
             TxtFactElt.Text = NUL$
             FrmFactElect.Enabled = False
             Me.FrmFactElect.BackColor = &H8000000A
          End If
       End If
       'DRMG T44728-R41288 FIN
    Else
        Select Case Documento.Encabezado.TipDeDcmnt
        
        Case Is = AnulaBaja, AnulaDespacho, AnulaEntrada, AnulaFacturaVenta, AnulaODCompra, AnulaRequisicion, AnulaSalida, AnulaCotizacion, AnulaTraslado
            ' DAHV - M5468  - INICIO
            If Me.txtNumero.Enabled = True Then
                Me.txtNumero.SetFocus
            End If
            ' DAHV - M5468  - FIN
        Case Else
            Call Fecha_Docu
        End Select
    End If
End Sub

'DEPURACION DE CODIGO
'Private Sub GrabarDocumentoOBSOLETE(ByRef Comproba As Long)
'   ReDim Arr(0)
'   Dim fechag As Variant
'
'   DoEvents
'
'   Call MouseClock
'   Msglin "Ingresando informaci�n del " & Documento.Encabezado.NombreDocumento
'
'   If txtNumero = NUL$ Then
'      Call Mensaje1("Se deben especificar los datos completos del " & Documento.Encabezado.NombreDocumento, 3)
'      Call MouseNorm: Exit Sub
'   End If
'
''   Call Calcular_Documento
'   If GrdArticulos.TextMatrix(1, Me.ColAutoArticulo) = NUL$ Then
'      Call Mensaje1(Documento.Encabezado.NombreDocumento & " sin Art�culos", 3)
'      Call MouseNorm: Exit Sub
'   End If
'
'   If txtDocum(1) = NUL$ Or txtDocum(2) = NUL$ Or txtDocum(3) = NUL$ Then
'      If Buscar_Fecha_Cierre(txtDocum(1), txtDocum(2), txtDocum(3)) = False Then
'         Call cmdOpciones_Click(3)
'      End If
'   End If
'   fechag = txtDocum(1) & "/" & txtDocum(2) & "/" & txtDocum(3)
'
'   If IsDate(fechag) = False Then
'      Call Mensaje1("Revise la fecha", 3)
'      Call MouseNorm: Exit Sub
'   End If
'   Documento.Encabezado.FechaDocumento = fechag
'   Me.fraTOTALES.Visible = False
'   Comproba = WinDoc.Guardar(True)
'   Me.fraTOTALES.Visible = True
'   If Comproba <> FAIL Then Call cmdOpciones_Click(3)
'   Call MouseNorm
'   Msglin NUL$
'End Sub

'DEPURACION DE CODIGO
'Private Sub AnularDocumento()
'   DoEvents
'
'   Call MouseClock
'
'   Msglin "Anulando un " & Documento.Encabezado.NombreDocumento
'   If (txtNumero = NUL$) Then
'       Call Mensaje1("Ingrese el N�mero del " & Documento.Encabezado.NombreDocumento, 1)
'       Call MouseNorm: Msglin NUL$: Exit Sub
'   End If
'   'valida la fecha de cierre
'   If Valid_Fecha_Cie(CDate(Me.txtDocum(1) & "/" & Me.txtDocum(2) & "/" & Me.txtDocum(3))) Then Call MouseNorm: Exit Sub
'   Call MouseNorm
'   Msglin NUL$
'End Sub

Private Sub Form_Unload(Cancel As Integer)
    If Not LaPuedoCerrar Then Cancel = -1: cmdREGRESAR_Click (Cancel) ' smdl m1680, se adiciona la instruccion cmdREGRESAR_Click (Cancel)
End Sub


Private Sub GrdDeIm_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = 46 Then GrdDeIm.Text = ""
End Sub

Private Sub GrdDeIm_KeyPress(KeyAscii As Integer)
    Select Case GrdDeIm.Col
       Case 3:
               If GrdDeIm.TextMatrix(GrdDeIm.Row, 8) = "D" Then
                  If GrdDeIm.TextMatrix(GrdDeIm.Row, 6) = "P" Then Call ValidarPuntoFGrd(GrdDeIm, GrdDeIm.Col, KeyAscii, 6) 'PORCENTAJE
                  If KeyAscii = 13 Then
                     GrdDeIm.Col = 4: GrdDeIm.Col = 3
                  End If
               ElseIf GrdDeIm.TextMatrix(GrdDeIm.Row, 8) = "I" Then
                  If GrdDeIm.TextMatrix(GrdDeIm.Row, 6) = "V" Or GrdDeIm.TextMatrix(GrdDeIm.Row, 6) = "P" Then Call ValidarPuntoFGrd(GrdDeIm, GrdDeIm.Col, KeyAscii, 10) 'VALOR O PORCENTAJE
                  If KeyAscii = 13 Then
                     GrdDeIm.Col = 4: GrdDeIm.Col = 3
                  End If
               End If
       Case 4:
               If GrdDeIm.TextMatrix(GrdDeIm.Row, 8) = "D" Then
                  If GrdDeIm.TextMatrix(GrdDeIm.Row, 6) = "V" Or GrdDeIm.TextMatrix(GrdDeIm.Row, 6) = "P" Then Call ValidarPuntoFGrd(GrdDeIm, GrdDeIm.Col, KeyAscii, 10) 'VALOR O PORCENTAJE
                  If KeyAscii = 13 Then
                     GrdDeIm.Col = 4: GrdDeIm.Col = 3
                  End If
               ElseIf GrdDeIm.TextMatrix(GrdDeIm.Row, 8) = "I" Then
                  If GrdDeIm.TextMatrix(GrdDeIm.Row, 6) = "V" Or GrdDeIm.TextMatrix(GrdDeIm.Row, 6) = "P" Then Call ValidarPuntoFGrd(GrdDeIm, GrdDeIm.Col, KeyAscii, 10) 'VALOR O PORCENTAJE
                  If KeyAscii = 13 Then
                     GrdDeIm.Col = 4: GrdDeIm.Col = 3
                  End If
               End If
       Case 5:
               If GrdDeIm.TextMatrix(GrdDeIm.Row, 8) = "I" Then
                 If UCase(Chr(KeyAscii)) = "R" Or UCase(Chr(KeyAscii)) = "S" Then
                    GrdDeIm.Text = UCase(Chr(KeyAscii))
                 End If
              End If
    End Select
End Sub

Private Sub GrdDeIm_LeaveCell()
    If BandLoad = True Or cmdOpciones(0).Enabled = False Then Exit Sub
    Dim MiValor As Variant
    Select Case GrdDeIm.Col
       Case 3:
               If GrdDeIm.TextMatrix(GrdDeIm.Row, 8) = "D" Then
                 If GrdDeIm.TextMatrix(GrdDeIm.Row, 6) = "V" Then Exit Sub
                 If GrdDeIm.Text = NUL$ Then GrdDeIm.Text = 0
                 If Not IsNumeric(GrdDeIm.Text) Then GrdDeIm.Text = 0
                 If CDbl(TxtEntra(10)) = 0 Then
                    MiValor = 0
                 Else
                    MiValor = ((GrdDeIm.TextMatrix(GrdDeIm.Row, 3) * CDbl(TxtEntra(10))) / 100)
                 End If
                 If BoAproxCent Then MiValor = AproxCentena(Round(MiValor)) 'JLPB T15675
                 GrdDeIm.TextMatrix(GrdDeIm.Row, 4) = MiValor
                 GrdDeIm.TextMatrix(GrdDeIm.Row, 4) = Format(GrdDeIm.TextMatrix(GrdDeIm.Row, 4), "#,###.#0")
               ElseIf GrdDeIm.TextMatrix(GrdDeIm.Row, 8) = "I" Then
                 If GrdDeIm.Text = NUL$ Then GrdDeIm.Text = 0
                 If Not IsNumeric(GrdDeIm.Text) Then GrdDeIm.Text = 0
                 If CDbl(TxtEntra(10)) = 0 Then
                    MiValor = 0
                 Else
                    'MiValor = ((GrdDeIm.TextMatrix(GrdDeIm.Row, 3) * CDbl(TxtEntra(10))) / 100)
                    MiValor = ((GrdDeIm.TextMatrix(GrdDeIm.Row, 3) * (CDbl(TxtEntra(10)) - CDbl(TxtEntra(11)))) / 100)
                 End If
                 If BoAproxCent Then MiValor = AproxCentena(Round(MiValor)) 'JLPB T15675
                 GrdDeIm.TextMatrix(GrdDeIm.Row, 4) = MiValor
                 GrdDeIm.TextMatrix(GrdDeIm.Row, 4) = Format(GrdDeIm.TextMatrix(GrdDeIm.Row, 4), "#,###.#0")
               End If
       Case 4:  'If GrdDeIm.TextMatrix(GrdDeIm.Row, 6) = "P" Then Exit Sub
               If GrdDeIm.TextMatrix(GrdDeIm.Row, 8) = "D" Then
                 If GrdDeIm.TextMatrix(GrdDeIm.Row, 6) = "P" Then
                    If GrdDeIm.Text = NUL$ Then GrdDeIm.Text = 0
                    If Not IsNumeric(GrdDeIm.Text) Then GrdDeIm.Text = 0
                    If CDbl(TxtEntra(10)) = 0 Then
                       MiValor = 0
                    Else
                       MiValor = ((GrdDeIm.TextMatrix(GrdDeIm.Row, 4) * 100) / CDbl(TxtEntra(10)))
                    End If
                    'GrdDeIm.TextMatrix(GrdDeIm.Row, 3) = MiValor
                    GrdDeIm.TextMatrix(GrdDeIm.Row, 3) = Format(MiValor, "#0.####")
                 Else
                    If GrdDeIm.Text = NUL$ Then GrdDeIm.Text = 0
                    If Not IsNumeric(GrdDeIm.Text) Then GrdDeIm.Text = 0
                 End If
                 GrdDeIm.TextMatrix(GrdDeIm.Row, 4) = Format(GrdDeIm.TextMatrix(GrdDeIm.Row, 4), "#,###.#0")
               ElseIf GrdDeIm.TextMatrix(GrdDeIm.Row, 8) = "I" Then
                    If GrdDeIm.Text = NUL$ Then GrdDeIm.Text = 0
                    If Not IsNumeric(GrdDeIm.Text) Then GrdDeIm.Text = 0
                    If CDbl(TxtEntra(10)) = 0 Then
                       MiValor = 0
                    Else
                       MiValor = ((GrdDeIm.TextMatrix(GrdDeIm.Row, 4) * 100) / CDbl(TxtEntra(10)))
                    End If
                    GrdDeIm.TextMatrix(GrdDeIm.Row, 3) = Format(MiValor, "#0.####")
                    GrdDeIm.TextMatrix(GrdDeIm.Row, 4) = Format(GrdDeIm.TextMatrix(GrdDeIm.Row, 4), "#,###.#0")
               End If
    End Select
End Sub

Private Sub OptParticular_Click() 'SMDL M1775
    If Documento.Encabezado.TipDeDcmnt = Entrada Then
        'If Val(Me.TxtCompra(10)) <> 0 And boFact = True Then
        If Me.TxtCompra(10) <> "0" And boFact = True Then 'JACC R2221
           If OptParticular.value = True Then
              TxtEntra(14).Enabled = True: TxtEntra(20).Enabled = False
            End If
        End If
    End If
End Sub

Private Sub OptProveedor_Click() 'SMDL M1775
    If Documento.Encabezado.TipDeDcmnt = Entrada Then
        'If Val(Me.TxtCompra(10)) <> 0 And boFact = True Then
        If Me.TxtCompra(10) <> "0" And boFact = True Then 'JACC R2221
           If OptProveedor.value = True Then
              TxtEntra(14).Enabled = True: TxtEntra(20).Enabled = True
           End If
        End If
    End If
End Sub

Private Sub picHelp_Click(Index As Integer)
    H.sbMsg picHelp(Index).Tag, picHelp(Index)
End Sub

Private Sub staINFORMA_PanelClick(ByVal Panel As ComctlLib.Panel)
       Select Case Panel.Key
    Case Is = "lblCOS"
        If (Documento.Encabezado.TipDeDcmnt = FacturaVenta) Or (Documento.Encabezado.TipDeDcmnt = DevolucionFacturaVenta) Then
            Me.staINFORMA.Panels("COSTO").Text = Aplicacion.Formatear_Valor(WinDoc.Calcular_Documento(Me.ColCantidad, Me.ColVenUndConDes))
        Else
            Me.staINFORMA.Panels("COSTO").Text = Aplicacion.Formatear_Valor(WinDoc.Calcular_Documento(Me.ColCantidad, Me.ColCostoUnidad))
        End If
    Case Is = "lblPAR"
        If (Documento.Encabezado.TipDeDcmnt = FacturaVenta) Or (Documento.Encabezado.TipDeDcmnt = DevolucionFacturaVenta) Then
            Me.staINFORMA.Panels("PARCIAL").Text = Aplicacion.Formatear_Valor(WinDoc.Calcular_Renglon(Me.FilaDeGrillaActiva, Me.ColCantidad, Me.ColVenUndConDes, Me.ColMultiplicador, Me.ColDivisor))
        Else ''ColVenUndConDes
            Me.staINFORMA.Panels("PARCIAL").Text = Aplicacion.Formatear_Valor(WinDoc.Calcular_Renglon(Me.FilaDeGrillaActiva, Me.ColCantidad, Me.ColCostoUnidad, Me.ColMultiplicador, Me.ColDivisor))
        End If
    Case Is = "SALBOD"
        If Me.FraPie.Visible Then
            Me.staINFORMA.Panels("SALBOD").Text = "Observaciones"
            Me.FraPie.Visible = False
            Me.lstSALXBOD.Visible = False
            Me.chaSALDOS.Visible = True
            'DRMG T44728-R41288 INICION
            If Documento.Encabezado.TipDeDcmnt = FacturaVenta Then
               Me.FrmFactElect.Visible = False
            End If
            'DRMG T44728-R41288 FIN
            'Call WinDoc.vFGrid_RowColChange
            WinDoc.CargaPanelSaldos
        ElseIf Me.chaSALDOS.Visible Then
            Me.staINFORMA.Panels("SALBOD").Text = "Saldos X Bodega"
            Me.chaSALDOS.Visible = False
            Me.FraPie.Visible = False
            Me.lstSALXBOD.Visible = True
            'DRMG T44728-R41288 INICION
            If Documento.Encabezado.TipDeDcmnt = FacturaVenta Then
               Me.FrmFactElect.Visible = False
            End If
            'DRMG T44728-R41288 FIN
        ElseIf Me.lstSALXBOD.Visible Then
            Me.staINFORMA.Panels("SALBOD").Text = "Saldos X Bodega"
            Me.lstSALXBOD.Visible = False
            Me.chaSALDOS.Visible = False
            Me.FraPie.Visible = True
            'DRMG T44728-R41288 INICION
            If Documento.Encabezado.TipDeDcmnt = FacturaVenta Then
               Me.FrmFactElect.Visible = True
            End If
            'DRMG T44728-R41288 FIN
        End If
    End Select
End Sub

Private Sub TxtCompra_KeyPress(Index As Integer, KeyAscii As Integer)
  'If Index = 10 Then Call ValKeyNum(KeyAscii)
  If Index = 10 Then Call ValKeyAlfaNumSinCarConGuion(KeyAscii) 'JACC R2221
End Sub

Private Sub TxtCompra_LostFocus(Index As Integer)
  Select Case Index
    Case 10:
            'If Val(Me.TxtCompra(10)) = 0 Then '|.DR.|, R:979-1295-1632
            If Me.TxtCompra(10) = "0" Then 'JACC R2221
                GrdDeIm.Rows = 1
                boFact = False
                Cmd_agregar.Enabled = False: lblIndi(0).Visible = True: lblIndi(1).Visible = True
                'smdl m1775
                TxtEntra(14).Text = 0: TxtEntra(20).Text = 0
                TxtEntra(14).Enabled = False: TxtEntra(20).Enabled = False
                'smdl m1775
                Exit Sub
            Else
                boFact = True
                Cmd_agregar.Enabled = True: lblIndi(0).Visible = False: lblIndi(1).Visible = False
                'smdl m1775
                If OptProveedor.value = True Then
                   TxtEntra(14).Enabled = True: TxtEntra(20).Enabled = True
                Else
                   TxtEntra(14).Enabled = True
                End If
                'smdl m1775
            End If
            ReDim Arr(0, 0)
            Condicion = "NU_AUTO_ENCA_PRPR=NU_AUTO_ENCA"
            Condicion = Condicion & " AND NU_AUTO_DOCU_ENCA=" & Documento.Encabezado.TipDeDcmnt
            Condicion = Condicion & " AND TX_CODI_PART_PRPR='8'"  'Tipo de valor: # de factura
            Condicion = Condicion & " AND CD_CODI_TERC_ENCA=" & Comi & Trim(Me.txtDocum(5)) & Comi
            Result = LoadMulData("IN_PARTIPARTI,IN_ENCABEZADO", "NU_VALO_PRPR", Condicion, Arr)
            If Result <> FAIL And Encontro Then
               Dim i As Double
               For i = 0 To UBound(Arr, 2)
                  'If Val(Me.TxtCompra(10)) = CDbl(Arr(0, i)) Then
                  If UCase(Me.TxtCompra(10)) = UCase(Arr(0, i)) Then 'JACC R2221
                    Call Mensaje1("Ya hay una " & Documento.Encabezado.NombreDocumento & " con ese numero de factura", 3)
                  End If
               Next i
            End If
  End Select
End Sub

Private Sub TxtCompra_MouseUp(Index As Integer, Button As Integer, Shift As Integer, X As Single, Y As Single) 'JACC M5926
  Select Case Index
    Case 10
          TxtCompra(10).Text = ValAlfaNumConGuion(TxtCompra(10).Text)
  End Select
End Sub

Private Sub txtDocum_DblClick(Index As Integer)
    Select Case Index
    Case Is = 8
        If Not Documento.Encabezado.TipDeDcmnt = ListaPreciosCotiza Then Exit Sub
        Me.txtDocum(8).Visible = False
        Me.FraPie.Height = Me.FraPie.Height * 2
        Me.FraPie.Caption = "Condiciones Comerciales"
        Me.txtDocum(9).Height = Me.txtDocum(9).Height * 2
        Me.txtDocum(9).Visible = True
    Case Is = 9
        Me.txtDocum(9).Height = Me.txtDocum(9).Height / 2
        Me.txtDocum(9).Visible = False
        Me.FraPie.Caption = "Observaciones"
        Me.FraPie.Height = Me.FraPie.Height / 2
        Me.txtDocum(8).Visible = True
    End Select
End Sub

Private Sub txtDocum_GotFocus(Index As Integer)
  txtDocum(Index).SelStart = 0
  txtDocum(Index).SelLength = Len(txtDocum(Index).Text)
  Select Case Index
    Case 0: Msglin "Digite el N�mero del " & Documento.Encabezado.NombreDocumento
    Case 1: Msglin "Digite la Fecha del " & Documento.Encabezado.NombreDocumento
            If txtNumero = NUL$ Then txtNumero.SetFocus: Exit Sub
    Case 4: Msglin "Digite la Bodega del " & Documento.Encabezado.NombreDocumento
    Case 9: Msglin "Digite las Observaciones del " & Documento.Encabezado.NombreDocumento
  End Select
End Sub

Private Sub txtDocum_KeyDown(Index As Integer, KeyCode As Integer, Shift As Integer)
    Select Case Index
    Case 0: If KeyCode = vbKeyDown Then txtDocum(1).SetFocus: Exit Sub
    Case 1:
        If KeyCode = vbKeyUp Then
            If txtNumero.Enabled = True Then
               txtNumero.SetFocus
            End If
            Exit Sub
        End If
        If KeyCode = vbKeyDown Then
'            If txtDocum(4).Enabled = True Then
'               txtDocum(4).SetFocus
'            End If
            Exit Sub
        End If
    Case 4:
        If KeyCode = vbKeyUp Then
            If txtDocum(1).Enabled = True Then
               txtDocum(1).SetFocus
            End If
            Exit Sub
        End If
        If KeyCode = vbKeyDown Then
            cmdDocsPadre(0).SetFocus
            Exit Sub
        End If
    End Select
End Sub

Private Sub txtDocum_KeyPress(Index As Integer, KeyAscii As Integer)
 Select Case Index
   Case 0, 1:
          Call ValKeyNum(KeyAscii)
          If KeyAscii = vbKeyReturn Then
             If Index = 0 Then
                txtDocum(1).SetFocus
             End If
             If Index = 1 Then
                txtDocum(4).SetFocus
             End If
          End If
    Case 5:
        Call Cambiar_Enter(KeyAscii)
   Case 4, 6:
          Call ValKeyAlfaNum(KeyAscii)
          If KeyAscii = vbKeyReturn Then
             If Index = 4 Then
                cmdDocsPadre(0).SetFocus
             End If
          End If
 End Select
End Sub

Private Sub txtDocum_LostFocus(Index As Integer)
     Msglin NUL$
     Select Case Index
        Case Is = 0
            If txtNumero <> NUL$ Then BuscarDocumento
        Case Is = 1
            If Documento.Encabezado.EsNuevoDocumento Then
                If txtDocum(1) = NUL$ And txtNumero <> NUL$ Then txtDocum(1) = Format(Me.UltimaFechaDocumento, "dd")
                If Not IsDate(txtDocum(1) & "/" & txtDocum(2) & "/" & txtDocum(3)) Then
                   Call Mensaje1("Fecha no V�lida", 3)
                   txtDocum(1) = Format(Me.UltimaFechaDocumento, "dd")
                   txtDocum(1).SetFocus
                   Exit Sub
                End If
                If Me.UltimaFechaDocumento > 0 Then
                   If CDate(Me.UltimaFechaDocumento) > CDate(txtDocum(1) & "/" & txtDocum(2) & "/" & txtDocum(3)) Then
                        Call Mensaje1("No puede devolver la fecha del documento", 2)
                        txtDocum(1) = Format(Me.UltimaFechaDocumento, "dd")
                        txtDocum(1).SetFocus
                   End If
                End If
            End If
        Case Is = 5
            'If txtDocum(5) <> NUL$ Then BuscarTercero 'DRMG T44728-R44232 SE DEJA EN COMENTARIO
            If Trim(txtDocum(5)) <> NUL$ Then BuscarTercero 'DRMG T44728-R44232
        Case Is = 21, 22, 23
            If Not IsDate(txtDocum(21) & "/" & txtDocum(22) & "/" & txtDocum(23)) Then
                Call MsgBox("La fecha de vigencia no es valida", vbInformation)
                'DAHV M3595
                txtDocum(21) = txtDocum(1)
                txtDocum(22) = txtDocum(2)
                txtDocum(23) = txtDocum(3)
                txtDocum(Index).SetFocus
            ElseIf CDate(txtDocum(21) & "/" & txtDocum(22) & "/" & txtDocum(23)) < Documento.Encabezado.FechaDocumento Then
                Call MsgBox("La fecha de vigencia no puede ser menor a:" & FormatDateTime(Documento.Encabezado.FechaDocumento, vbShortDate))
            Else
                Documento.Encabezado.FechaVigencia = CDate(txtDocum(21) & "/" & txtDocum(22) & "/" & txtDocum(23))
            End If
     End Select
End Sub

Private Sub GrdArticulos_GotFocus()
    On Error GoTo salto
    GrdArticulos.Row = Me.FilaDeGrillaActiva
    'GrdArticulos.col = me.ColumnaDeGrillaActiva
    If GrdArticulos.Row >= 6 Then GrdArticulos.TopRow = GrdArticulos.Row
salto:
    On Error GoTo 0
End Sub

Private Sub GrdArticulos_LostFocus()
'If EncontroT = False Then Calcular_Documento
''El foco del grid de Art�culos est� en la
    Me.FilaActiva = GrdArticulos.Row
 ''El foco del grid de Art�culos est� en la
 ''primera casilla por defecto
Me.FilaDeGrillaActiva = GrdArticulos.Rows - 1
Me.ColumnaDeGrillaActiva = 2
End Sub

Private Sub Fecha_Docu()
Dim FC As String
    If Me.UltimaFechaDocumento > 0 Then
       FC = txtDocum(1) & "/" & txtDocum(2) & "/" & txtDocum(3)
       If IsDate(FC) Then
          If CDate(FC) < CDate(Me.UltimaFechaDocumento) Then
             txtDocum(1) = Format(Me.UltimaFechaDocumento, "dd")
             txtDocum(2) = Format(Me.UltimaFechaDocumento, "mm")
             txtDocum(3) = Format(Me.UltimaFechaDocumento, "yyyy")
          End If
       Else
          txtDocum(1) = Format(Me.UltimaFechaDocumento, "dd")
          txtDocum(2) = Format(Me.UltimaFechaDocumento, "mm")
          txtDocum(3) = Format(Me.UltimaFechaDocumento, "yyyy")
       End If
    End If
End Sub

Private Sub BuscarTercero()
   'ReDim Arr(9), APCT(5, 0) 'OMOG R16934/T19113 SE ESTABLECE EN COMENTARIO
   'JLPB T42130 INICIO Se deja en comentario la siguiente linea
   'ReDim arr(11), APCT(5, 0)  'OMOG R16934/T19113
   Dim VrArr() As Variant 'Almacena los registros devueltos por una consulta
   ReDim Arr(11)
   'JLPB T42130 FIN
   
   Campos = "NO_NOMB_TERC,ID_TIPO_TERC,ID_TIPO_REGI_PAIM,ID_TIPO_CONTR_PAIM,"
   Campos = Campos & "ID_AUTO_RETIVA_PAIM,ID_AUTO_RETICA_PAIM,ID_AUTO_RETREN_PAIM"
   Campos = Campos & ",ID_EXERET_FUEN_PAIM,ID_EXERET_ICA_PAIM,ID_EXERET_IVA_PAIM"
   Campos = Campos & ",TX_RESO_CREE_PAIM,DE_ACTI_TERC" 'OMOG R16934/T19113
   Desde = "TERCERO,PARAMETROS_IMPUESTOS "
   Condicion = "CD_CODI_TERC=" & Comi & Cambiar_Comas_Comillas(Me.txtDocum(5).Text) & Comi & " AND (NU_ESTADO_TERC <> 1)"
   Condi = "CD_CODI_TERC=CD_CODI_TERC_PAIM AND "
   Condicion = Condi & Condicion
   Result = LoadData(Desde, Campos, Condicion, Arr())
   If (Result <> False) Then
      If Encontro Then
         Me.txtDocum(6) = Arr(0)
         TipoPers = Arr(1)
         TipoRegimen = Arr(2)
         GranContrib = Arr(3)
         AutRetIva = Arr(4)
         AutRetIca = Arr(5)
         AutoRet = Arr(6)
         ExentoFuente = Arr(7)
         ExentoIca = Arr(8)
         ExentoIva = Arr(9)
         If Arr(10) = NUL$ Then Arr(10) = "0" 'OMOG T19883
         StImpCree = Arr(10) 'OMOG R16934/T19113
         StActi = Arr(11) 'OMOG R16934/T19113
      ElseIf Documento.Encabezado.TipDeDcmnt = FacturaVenta Then
         'JLPB T42130 INICIO Se deja en comentario las siguientes lineas
         'Campos = "NU_DOCU_PAC, NU_HIST_PAC, DE_PRAP_PAC, DE_SGAP_PAC, NO_NOMB_PAC, NO_SGNO_PAC"
         'Desde = "PACIENTES"
         'Condi = "NU_DOCU_PAC='" & Cambiar_Comas_Comillas(Me.txtDocum(5).Text) & "'"
         'Result = LoadMulData(Desde, Campos, Condi, APCT())
         'If (Result <> False) Then
         '   If Encontro Then
         '      'Result = DoInsertSQL("TERCERO", "CD_CODI_TERC='" & Cambiar_Comas_Comillas(Me.txtDocum(5).Text) & "', NO_NOMB_TERC='" & Apct(2, 0) & " " & Apct(3, 0) & " " & Apct(4, 0) & " " & Apct(5, 0) & "'") 'HRR R1717
         '      'HRR R1717
         '      Valores = "CD_CODI_TERC='" & Cambiar_Comas_Comillas(Me.txtDocum(5).Text) & "'"
         '      Valores = Valores & ",NO_NOMB_TERC='" & APCT(2, 0) & " " & APCT(3, 0) & " " & APCT(4, 0) & " " & APCT(5, 0) & "'"
         '      Valores = Valores & ",TX_PNOM_TERC='" & APCT(4, 0) & Comi
         '      Valores = Valores & ",TX_SNOM_TERC='" & APCT(5, 0) & Comi
         '      Valores = Valores & ",TX_PAPE_TERC='" & APCT(2, 0) & Comi
         '      Valores = Valores & ",TX_SAPE_TERC='" & APCT(3, 0) & Comi
         '      Valores = Valores & ",NU_ESTADO_TERC= 0" 'AASV M3912
         '      Result = DoInsertSQL("TERCERO", Valores)
         '      'HRR R171
         ReDim VrArr(9)
         Campos = "NU_DOCU_PAC,NO_NOMB_PAC,NO_SGNO_PAC,DE_PRAP_PAC,DE_SGAP_PAC,"
         Campos = Campos & "DE_DIRE_PAC,DE_TELE_PAC,DE_EMAIL_PAC,CD_CODI_DPTO_PAC,CD_CODI_MUNI_PAC"
         Desde = "PACIENTES"
         Condicion = "NU_DOCU_PAC='" & Cambiar_Comas_Comillas(Me.txtDocum(5).Text) & Comi
         Result = LoadData(Desde, Campos, Condicion, VrArr)
         If Result <> FAIL Then
            If Encontro Then
               If fnDevDato("TERCERO", "CD_CODI_TERC", "CD_CODI_TERC='" & VrArr(0) & Comi, True) <> NUL$ Then GoTo SEGUIR
               Desde = "TERCERO"
               Valores = "CD_CODI_TERC='" & VrArr(0) & Comi & Coma
               Valores = Valores & "NO_NOMB_TERC='" & Cambiar_Comas_Comillas(CStr(VrArr(3) & " " & VrArr(4) & " " & VrArr(1) & " " & VrArr(2))) & Comi & Coma
               Valores = Valores & "TX_PNOM_TERC='" & Cambiar_Comas_Comillas(CStr(VrArr(1))) & Comi & Coma
               Valores = Valores & "TX_SNOM_TERC='" & Cambiar_Comas_Comillas(CStr(VrArr(2))) & Comi & Coma
               Valores = Valores & "TX_PAPE_TERC='" & Cambiar_Comas_Comillas(CStr(VrArr(3))) & Comi & Coma
               Valores = Valores & "TX_SAPE_TERC='" & Cambiar_Comas_Comillas(CStr(VrArr(4))) & Comi & Coma
               Valores = Valores & "DE_DIRE_TERC='" & Cambiar_Comas_Comillas(CStr(VrArr(5))) & Comi & Coma
               Valores = Valores & "NU_TELE_TERC='" & Cambiar_Comas_Comillas(CStr(VrArr(6))) & Comi & Coma
               Valores = Valores & "TX_MAIL_TERC='" & Cambiar_Comas_Comillas(CStr(VrArr(7))) & Comi & Coma
               Valores = Valores & "DE_DPTO_TERC='" & VrArr(8) & Comi & Coma
               Valores = Valores & "DE_CIUD_TERC='" & VrArr(9) & Comi & Coma
               Valores = Valores & "NU_ESTADO_TERC=0"
               Result = DoInsertSQL(Desde, Valores)
               If Result <> FAIL Then
SEGUIR:
                  If fnDevDato("PARAMETROS_IMPUESTOS", "CD_CODI_TERC_PAIM", "CD_CODI_TERC_PAIM ='" & Cambiar_Comas_Comillas(Me.txtDocum(5).Text) & Comi, True) <> NUL$ Then GoTo SALIR
         'JLPB T42130 FIN
                  'AASV M3912
                  Valores = "CD_CODI_TERC_PAIM ='" & Cambiar_Comas_Comillas(Me.txtDocum(5).Text) & "'"
                  Valores = Valores & Coma & "ID_TIPO_TERC_PAIM ='0'"
                  Valores = Valores & Coma & "ID_TIPO_REGI_PAIM ='1'"
                  Valores = Valores & Coma & "ID_TIPO_CONTR_PAIM ='0'"
                  Valores = Valores & Coma & "ID_AUTO_RETIVA_PAIM ='0'"
                  Valores = Valores & Coma & "ID_AUTO_RETICA_PAIM ='0'"
                  Valores = Valores & Coma & "ID_AUTO_RETREN_PAIM ='0'"
                  Valores = Valores & Coma & "TX_RESO_ARETIVA_PAIM =''"
                  Valores = Valores & Coma & "TX_RESO_ARETICA_PAIM =''"
                  Valores = Valores & Coma & "TX_RESO_ARETREN_PAIM =''"
                  Valores = Valores & Coma & "ID_AREIVA_REGCOM_PAIM ='0'"
                  Valores = Valores & Coma & "ID_AREIVA_REGSIM_PAIM ='0'"
                  Valores = Valores & Coma & "ID_AREICA_REGCOM_PAIM ='0'"
                  Valores = Valores & Coma & "ID_AREICA_REGSIM_PAIM ='0'"
                  Valores = Valores & Coma & "ID_AREREN_REGCOM_PAIM ='0'"
                  Valores = Valores & Coma & "ID_AREREN_REGSIM_PAIM ='0'"
                  Valores = Valores & Coma & "ID_EXERET_FUEN_PAIM='0'"
                  Valores = Valores & Coma & "ID_EXERET_ICA_PAIM='0'"
                  Valores = Valores & Coma & "ID_EXERET_IVA_PAIM='0'"
                  Result = DoInsertSQL("PARAMETROS_IMPUESTOS", Valores)
                  'AASV M3912
               End If 'JLPB T42130
SALIR: 'JLPB T42130
               Call txtDocum_LostFocus(5)
            Else
               FrmTerceros.txtTercero(0).Text = Me.txtDocum(5).Text
               Me.txtDocum(5).Text = ""
               FrmTerceros.Show
            End If
         End If
      Else
         Me.txtDocum(5) = NUL$
         Me.txtDocum(6) = NUL$
         TipoPers = NUL$
         TipoRegimen = NUL$
         GranContrib = NUL$
         AutRetIva = NUL$
         AutRetIca = NUL$
         AutoRet = NUL$
         ExentoFuente = NUL$
         ExentoIca = NUL$
         ExentoIva = NUL$
         If Me.txtDocum(5).Enabled Then Me.txtDocum(5).SetFocus
      End If
   End If
End Sub
Private Sub TxtEntra_LostFocus(Index As Integer) 'smdl m1753
Select Case Index
   Case 14, 20:
      If Documento.Encabezado.TipDeDcmnt = Entrada Then Call calcular_entra(True)
End Select
End Sub

Private Sub TxtNumero_GotFocus()
    txtNumero.Tag = txtNumero.Text 'AASV M5651
    Msglin "Digite el N�mero de la " & Documento.Encabezado.NombreDocumento
End Sub

Private Sub txtNumero_KeyPress(KeyAscii As Integer)
    Call ValKeyNum(KeyAscii)    'REOL M2071
End Sub

Private Sub TxtNumero_LostFocus()
    'AASV M5651 Inicio
    If txtNumero.Tag <> "" Then 'GAVL T5611 Se valida primero el comprobante este ingresado para llenar el tag
        If IsNumeric(txtNumero.Text) Then
            If CDbl(txtNumero.Tag) < CDbl(txtNumero.Text) Then txtNumero.Text = txtNumero.Tag
        Else
            Exit Sub
        End If
        'AASV M5651 Fin
    Else
        Call Mensaje1("Debe seleccionar el comprobante", 3) 'GAVL T5611
    End If
    'DAHV - M5468 Se devuelven los cambios realizados por Carlos
    If txtNumero <> NUL$ Then BuscarDocumento
    'If txtNumero <> NUL$ And CDbl(txtNumero.Tag) > CDbl(txtNumero.Text) Then BuscarDocumento 'CARV M5468 Se agrego que solo busque el documento si es menor al consecutivo
    
        
End Sub

Function Cambiar_Comas_Comillas(cadena As String) As String
   cadena = Trim$(cadena)
   cadena = Replace(cadena, "'", "^")
   cadena = Replace(cadena, ",", "*")
   Cambiar_Comas_Comillas = cadena
End Function
'blnDesdeCompras
Public Property Let EsDesdeCompras(lgi As Boolean)
    blnDesdeCompras = lgi
End Property

Public Property Get EsDesdeCompras() As Boolean
    EsDesdeCompras = blnDesdeCompras
End Property

Public Property Let AutoTipoDocumento(num As Long)
    AuTiDoc = num
End Property

Public Property Get AutoTipoDocumento() As Long
    AutoTipoDocumento = AuTiDoc
End Property

Public Property Let SeActivoFormulario(lgi As Boolean)
    SeActi = lgi
End Property

Public Property Get SeActivoFormulario() As Boolean
    SeActivoFormulario = SeActi
End Property

Public Property Let EstaAnulado(lgi As Boolean)
    EsAnul = lgi
End Property

Public Property Get EstaAnulado() As Boolean
    EstaAnulado = EsAnul
End Property

Public Property Let FilaActiva(num As Long)
    FiActiv = num
End Property

Public Property Get FilaActiva() As Long
    FilaActiva = FiActiv
End Property

Public Property Let FilaDeGrillaActiva(num As Long)
    fofil = num
End Property

Public Property Get FilaDeGrillaActiva() As Long
    FilaDeGrillaActiva = fofil
End Property

Public Property Let ColumnaDeGrillaActiva(num As Long)
    focol = num
End Property

Public Property Get ColumnaDeGrillaActiva() As Long
    ColumnaDeGrillaActiva = focol
End Property

Public Property Let UltimaFechaDocumento(Cue As Long)
    FechUlt = Cue
End Property

Public Property Get UltimaFechaDocumento() As Long
    UltimaFechaDocumento = FechUlt
End Property

Public Property Let FechaVigencia(Cue As Long)
    FechVgn = Cue
End Property

Public Property Get FechaVigencia() As Long
    FechaVigencia = FechVgn
End Property

Public Property Let FechaCierre(num As Long)
    FechCie = num
End Property

Public Property Get FechaCierre() As Long
    FechaCierre = FechCie
End Property

Public Property Let DefinicionDeGrilla(Cue As String)
    DefDGrl = Cue
End Property

Public Property Get DefinicionDeGrilla() As String
    DefinicionDeGrilla = DefDGrl
End Property

Public Property Let ElContenidoDeCelda(Cue As String)
    CntGrll = Cue
End Property

Public Property Get ElContenidoDeCelda() As String
    ElContenidoDeCelda = CntGrll
End Property

Public Property Let ColAutoArticulo(num As Byte)
    vGriAutoArticulo = num
End Property

Public Property Get ColAutoArticulo() As Byte
    ColAutoArticulo = vGriAutoArticulo
End Property

Public Property Let ColCodigoArticulo(num As Byte)
    vGriCodigoArticulo = num
End Property

Public Property Get ColCodigoArticulo() As Byte
    ColCodigoArticulo = vGriCodigoArticulo
End Property

Public Property Let ColNombreArticulo(num As Byte)
    vGriNombreArticulo = num
End Property

Public Property Get ColNombreArticulo() As Byte
    ColNombreArticulo = vGriNombreArticulo
End Property

Public Property Let ColAutoUnidad(num As Byte)
    vGriAutoUnidad = num
End Property

Public Property Get ColAutoUnidad() As Byte
    ColAutoUnidad = vGriAutoUnidad
End Property

Public Property Let ColVentaSinIVA(num As Byte)
    vGriVentaSinIVA = num
End Property

Public Property Get ColVentaSinIVA() As Byte
    ColVentaSinIVA = vGriVentaSinIVA
End Property

Public Property Let ColVenUndConDes(num As Byte)
    vGriVenUndConDes = num
End Property

Public Property Get ColVenUndConDes() As Byte
    ColVenUndConDes = vGriVenUndConDes
End Property

Public Property Let ColVenTotConDes(num As Byte)
    vGriVenTotConDes = num
End Property

Public Property Get ColVenTotConDes() As Byte
    ColVenTotConDes = vGriVenTotConDes
End Property

Public Property Let ColDescPorcentual(num As Byte)
    vGriDescPorcentual = num
End Property

Public Property Get ColDescPorcentual() As Byte
    ColDescPorcentual = vGriDescPorcentual
End Property

Public Property Let ColDescAbsoluto(num As Byte)
    vGriDescAbsoluto = num
End Property

Public Property Get ColDescAbsoluto() As Byte
    ColDescAbsoluto = vGriDescAbsoluto
End Property

Public Property Let ColVentaTotal(num As Byte)
    vGriVentaTotal = num
End Property

Public Property Get ColVentaTotal() As Byte
    ColVentaTotal = vGriVentaTotal
End Property

Public Property Let ColNombreUnidad(num As Byte)
    vGriNombreUnidad = num
End Property

Public Property Get ColNombreUnidad() As Byte
    ColNombreUnidad = vGriNombreUnidad
End Property

Public Property Let ColMultiplicador(num As Byte)
    vGriMultiplicador = num
End Property

Public Property Get ColMultiplicador() As Byte
    ColMultiplicador = vGriMultiplicador
End Property

Public Property Let ColDivisor(num As Byte)
    vGriDivisor = num
End Property

Public Property Get ColDivisor() As Byte
    ColDivisor = vGriDivisor
End Property

Public Property Let ColCantidad(num As Byte)
    vGriCantidad = num
End Property

Public Property Get ColCantidad() As Byte
    ColCantidad = vGriCantidad
End Property

Public Property Let ColCostoUnidad(num As Byte)
    vGriCostoUnidad = num
End Property

Public Property Get ColCostoUnidad() As Byte
    ColCostoUnidad = vGriCostoUnidad
End Property

Public Property Let ColCostoTotal(num As Byte)
    vGriCostoTotal = num
End Property

Public Property Get ColCostoTotal() As Byte
    ColCostoTotal = vGriCostoTotal
End Property

Public Property Let ColFecVence(num As Byte)
    vGriFecVence = num
End Property

Public Property Get ColFecVence() As Byte
    ColFecVence = vGriFecVence
End Property

Public Property Let ColFecEntrada(num As Byte)
    vGriFecEntrada = num
End Property

Public Property Get ColFecEntrada() As Byte
    ColFecEntrada = vGriFecEntrada
End Property

Public Property Let ColCantidadFija(num As Byte)
    vGriCantidadFija = num
End Property

Public Property Get ColCantidadFija() As Byte
    ColCantidadFija = vGriCantidadFija
End Property

Public Property Let ColAutoDocumento(num As Byte)
    vGriAutoDocumento = num
End Property

Public Property Get ColAutoDocumento() As Byte
    ColAutoDocumento = vGriAutoDocumento
End Property

Public Property Let ColUltimoCosto(num As Byte)
    vGriUltimoCosto = num
End Property

Public Property Get ColUltimoCosto() As Byte
    ColUltimoCosto = vGriUltimoCosto
End Property

Public Property Let ColCostoSinIVA(num As Byte)
    vGriCostoSinIVA = num
End Property

Public Property Get ColCostoSinIVA() As Byte
    ColCostoSinIVA = vGriCostoSinIVA
End Property

Public Property Let ColPorceIVA(num As Byte)
    vGriPorceIVA = num
End Property

Public Property Get ColPorceIVA() As Byte
    ColPorceIVA = vGriPorceIVA
End Property

'HRR Req1553
Public Property Let ColPorceIvaFija(num As Byte)
    vGriPorceIvaFija = num
End Property

'HRR Req1553
Public Property Get ColPorceIvaFija() As Byte
    ColPorceIvaFija = vGriPorceIvaFija
End Property

'HRR Req1553
Public Property Let ColCostoSinIvaFija(num As Byte)
    vGriCostoSinIvaFija = num
End Property

'HRR Req1553
Public Property Get ColCostoSinIvaFija() As Byte
    ColCostoSinIvaFija = vGriCostoSinIvaFija
End Property

'HRR Req1553
Public Property Let ColCostoUnidadFija(num As Byte)
    vGriCostoUnidadFija = num
End Property

'HRR Req1553
Public Property Get ColCostoUnidadFija() As Byte
    ColCostoUnidadFija = vGriCostoUnidadFija
End Property

Public Property Let Seleccionada(num As Byte)
    vSeleccionada = num
End Property

Public Property Get Seleccionada() As Byte
    Seleccionada = vSeleccionada
End Property

Public Property Let CodigoFormaEfectivo(num As Integer)
    vCodigoFormaEfectivo = num
End Property

Public Property Let CodigoFormaCredito(num As Integer)
    vCodigoFormaCredito = num
End Property

Public Property Let ColCodigoImpuesto(num As Byte)
    vColCodigoImpuesto = num
End Property

Public Property Get ColCodigoImpuesto() As Byte
    ColCodigoImpuesto = vColCodigoImpuesto
End Property

Public Property Let Marcar(num As Byte)
    vMarcar = num
End Property

Public Property Get Marcar() As Byte
    Marcar = vMarcar
End Property

Public Property Let LoteProduccion(num As Byte)
    vLoteProduccion = num
End Property

Public Property Get LoteProduccion() As Byte
    LoteProduccion = vLoteProduccion
End Property

Public Property Let ProcesaEntradaXlotes(bol As Boolean)
    vProXLts = bol
End Property

Public Property Get ProcesaEntradaXlotes() As Boolean
    ProcesaEntradaXlotes = vProXLts
End Property

Public Property Let CambieAlgoEnCeldaDeGrilla(bol As Boolean)
    vCbCell = bol
End Property

Public Property Get CambieAlgoEnCeldaDeGrilla() As Boolean
    CambieAlgoEnCeldaDeGrilla = vCbCell
End Property

'Private Sub CargarClassWinDoc(ByRef WinDoc As WinDocumento, _
        ByRef cboConsecutivo As VB.ComboBox, ByRef cboBodega As VB.ComboBox, _
        ByRef CboBodegaDest As VB.ComboBox, ByRef Fgrid As MSFlexGrid, _
        ByRef txtNumero As VB.TextBox, ByVal TipoDoc As TipoDocumento, _
        ByRef LstDocuPadre As VB.ListBox, ByRef cboIndpn As VB.ComboBox)
'DEPURACION DE CODIGO
Private Sub CargarClassWinDoc(ByRef WinDoc As WinDocumento, _
        ByRef cboConsecutivo As VB.ComboBox, ByRef cboBodega As VB.ComboBox, _
        ByRef CboBodegaDest As VB.ComboBox, ByRef Fgrid As MSFlexGrid, ByRef txtNumero As VB.TextBox, _
        ByRef LstDocuPadre As VB.ListBox, ByRef cboIndpn As VB.ComboBox)
    Set WinDoc.cboConsecutivo = cboConsecutivo
    Set WinDoc.CboBodegaOrigen = cboBodega
    Set WinDoc.CboBodegaDestino = CboBodegaDest
    Set WinDoc.cboIndependiente = cboIndpn
    Set WinDoc.CboListaPrecio = cboLisPrecio
    Set WinDoc.FgridArtic = Fgrid
    Set WinDoc.txtNumero = txtNumero
    Set WinDoc.LstDocuPadre = LstDocuPadre
End Sub

Private Sub txtPorceCargo_KeyPress(KeyAscii As Integer)
    Call Cambiar_Enter(KeyAscii)
End Sub

Private Sub txtPorceCargo_LostFocus()
    If Not IsNumeric(Me.txtPorceCargo.Text) Then Me.txtPorceCargo.Text = "0"
    Me.txtPorceCargo.Text = FormatNumber(Me.txtPorceCargo.Text)
End Sub

Private Sub txtPorceDescuento_KeyPress(KeyAscii As Integer)
    Call Cambiar_Enter(KeyAscii)
End Sub

Private Sub txtPorceDescuento_LostFocus()
    If Not IsNumeric(Me.txtPorceDescuento.Text) Then Me.txtPorceDescuento.Text = "0"
    Me.txtPorceDescuento.Text = FormatNumber(Me.txtPorceDescuento.Text)
End Sub

Private Sub WinDoc_CambioComprobante(ByVal RompeDepe As Boolean)
    Me.cboIndependiente.Enabled = True
    Me.cboIndependiente.Visible = False 'AFMG T15595 SE RESTALBLECE A COMO ESTABA EN UN PRINCIPIO
     'Me.cboIndependiente.Visible = True 'AFMG T15595 SE PUEDE OBSERVAR EL COMBO DONDE IDENTIFICA SI ES INDEPENDIENTE O NO
    Me.lblINDEPE.Visible = False 'AFMG T15595 SE RESTALBLECE A COMO ESTABA EN UN PRINCIPIO
   
    Me.cboIndependiente.ListIndex = 0
    If Not WinDoc.ElConsecutivo.RompeDependencia Then Exit Sub
'    If Documento.Encabezado.CualDepende = 0 Then
    If Documento.Encabezado.CualDepende = 0 And Documento.Encabezado.TipDeDcmnt <> BajaConsumo Then 'NMSR R1883
        Me.cboIndependiente.ListIndex = 1
        Me.cboIndependiente.Enabled = False
        Me.cboIndependiente.Visible = True
    Else
        Me.cboIndependiente.Visible = True
    End If
    Me.lblINDEPE.Visible = True
End Sub

Private Sub WinDoc_CambioRenglonGrilla(ByVal Fila As Double, ByVal TOTAL As Double)


    'Me.txtSUBTO.Text = Aplicacion.Formatear_Valor(TOTAL - IIf((Documento.Encabezado.TipDeDcmnt = FacturaVenta) Or (Documento.Encabezado.TipDeDcmnt = DevolucionFacturaVenta) Or (Documento.Encabezado.TipDeDcmnt = AnulaFacturaVenta), WinDoc.ImpuestoDeVenta, WinDoc.ImpuestoDeCompra))
    Me.txtSUBTO.Text = Aplicacion.Formatear_Valor(TOTAL)
    'Me.txtIMPUE.Text = Format(IIf((Documento.Encabezado.TipDeDcmnt = FacturaVenta) Or (Documento.Encabezado.TipDeDcmnt = DevolucionFacturaVenta) Or (Documento.Encabezado.TipDeDcmnt = AnulaFacturaVenta), WinDoc.ImpuestoDeVenta, WinDoc.ImpuestoDeCompra), "#,##0")
    Me.txtIMPUE.Text = Aplicacion.Formatear_Valor(IIf((Documento.Encabezado.TipDeDcmnt = FacturaVenta) Or (Documento.Encabezado.TipDeDcmnt = DevolucionFacturaVenta) Or (Documento.Encabezado.TipDeDcmnt = AnulaFacturaVenta), WinDoc.ImpuestoDeVenta, WinDoc.ImpuestoDeCompra))
    
    'INICIO GAVL T6423
    If BoAproxCent = True Then
        Me.txtIMPUE.Text = AproxCentena(Round(Me.txtIMPUE.Text))
        'TOTAL = AproxCentena(Round(TOTAL)) 'LDCR T6797 SE COLOCA EN COMENTARIO
        txtIMPUE = AproxCentena(Round(txtIMPUE))
    End If
    'FIN  GAVL T6423
    
    Me.txtTOTAL.Text = Aplicacion.Formatear_Valor(TOTAL)
    'Me.txtTOTAL.Text = Aplicacion.Formatear_Valor(TOTAL + IIf((Documento.Encabezado.TipDeDcmnt = FacturaVenta) Or (Documento.Encabezado.TipDeDcmnt = DevolucionFacturaVenta) Or (Documento.Encabezado.TipDeDcmnt = AnulaFacturaVenta), WinDoc.ImpuestoDeVenta, WinDoc.ImpuestoDeCompra))

    'Si es el docuemnto es distinto a cualqueir mvto de venta (factura,anulacion,devolucion), le suma el valor del impuesto
    
    'INICIO GAVL T6423
    If BoAproxCent = True Then
        'INICIO LDCR T6797
        If (Documento.Encabezado.TipDeDcmnt <> FacturaVenta) And (Documento.Encabezado.TipDeDcmnt <> DevolucionFacturaVenta) And (Documento.Encabezado.TipDeDcmnt <> AnulaFacturaVenta) Then Me.txtTOTAL.Text = Aplicacion.Formatear_Valor(TOTAL + AproxCentena(Round(WinDoc.ImpuestoDeCompra))) 'JAGS T10733
        'If (Documento.Encabezado.TipDeDcmnt <> FacturaVenta) And (Documento.Encabezado.TipDeDcmnt <> DevolucionFacturaVenta) And (Documento.Encabezado.TipDeDcmnt <> AnulaFacturaVenta) Then Me.txtTOTAL.Text = Aplicacion.Formatear_Valor(TOTAL + WinDoc.ImpuestoDeCompra)
        'FIN LDCR T6797
      Else
        If (Documento.Encabezado.TipDeDcmnt <> FacturaVenta) And (Documento.Encabezado.TipDeDcmnt <> DevolucionFacturaVenta) And (Documento.Encabezado.TipDeDcmnt <> AnulaFacturaVenta) Then Me.txtTOTAL.Text = Aplicacion.Formatear_Valor(TOTAL + WinDoc.ImpuestoDeCompra)
    End If
    'FIN GAVL T6423
    'If TOTAL1 = 0 Then 'APGR T8058
        If (Documento.Encabezado.TipDeDcmnt = FacturaVenta) Or (Documento.Encabezado.TipDeDcmnt = DevolucionFacturaVenta) Or _
            (Documento.Encabezado.TipDeDcmnt = AnulaFacturaVenta) Then Me.txtTOTAL.Text = Aplicacion.Formatear_Valor(TOTAL + txtIMPUE) 'REOL M2521/M2520
    'Else
'        Me.txtTOTAL.Text = Aplicacion.Formatear_Valor(TOTAL1) 'APGR T8058
'    End If
    
    If Documento.Encabezado.TipDeDcmnt = FacturaVenta Then Me.txtTOTAL.Text = Aplicacion.Formatear_Valor(Me.txtTOTAL.Text - WinDoc.DbDescfac) 'DRMG T43448
    
    Me.staINFORMA.Panels("COSTO").Text = Aplicacion.Formatear_Valor(TOTAL)
    Me.staINFORMA.Panels("PARCIAL").Text = Aplicacion.Formatear_Valor(Fila)
    Me.staINFORMA.Panels("RNGLNS").Text = Aplicacion.Formatear_Cantidad(WinDoc.NumeroArticulosGrilla)
End Sub


Private Sub WinDoc_ExisteBodegaDestino(ByVal NumItems As Long)
    Me.cboBodDestino.Visible = False
    Me.cboBodDestino.Enabled = False
    Me.lblDESTINO.Visible = False
    If Not NumItems > 0 Then Exit Sub
    Me.lblDESTINO.Visible = True
    Me.cboBodDestino.Visible = True
    Me.cboBodDestino.Enabled = True
    If Not Me.cboBodDestino.ListCount = 1 Then Exit Sub
    If Documento.Encabezado.BodegaDESTINO <> 0 And Me.cboBodDestino.ListCount = 1 Then Exit Sub 'DRMG T42400
    Me.cboBodDestino.ListIndex = 0
    Me.cboBodDestino.Enabled = False
End Sub

Private Sub CmdCuentas_Click()
    Me.cmdPROCEDER.Item(8).Enabled = False
    If Not IsNumeric(Me.txtTOTAL.Text) Then Exit Sub
    'If Not CDbl(Me.txtTOTAL.Text) > 0 Then Exit Sub 'smdl m1529
    'If Not CDbl(Me.txtTOTAL.Text) > 0 Then Bandera = 1: Exit Sub 'LJSA M3965
    If Not CDbl(Me.txtTOTAL.Text) > 0 Then InBanCont = 1: Exit Sub 'CARV M5084
    If Not Me.GrdArti.Rows > 1 Then Exit Sub
    LaPuedoCerrar = False
    Me.frmINVENTARIOS.Visible = False
    Select Case Documento.Encabezado.TipDeDcmnt
    Case Is = Entrada, DevolucionEntrada, AprovechaDonacion
        Call CargaGrillaInterface(Me)
        Call calcular_entra(True)
        If Not Documento.Encabezado.TipDeDcmnt = AprovechaDonacion Then
            'Call CentraMarco(Me.FrmENTRADA)
            FrmENTRADA.Left = (Me.Width - FrmENTRADA.Width) / 2
            '--------------------------------------------------------------------
            'DAHV M4133 - INICIO
               If (TxtCompra(10).Text = NUL$) Or (TxtCompra(10).Text = "0") Then
                    TxtEntra(14).Enabled = False
                    TxtEntra(14).Text = "0"
                    TxtEntra(20).Enabled = False
                    TxtEntra(20).Text = "0"
               Else
                    TxtEntra(14).Enabled = True
                    TxtEntra(20).Enabled = True
               End If
            'DAHV M4133 - INICIO
            '-------------------------------------------------------------------
            
            'Call ImpuestosDescuentos 'HRR R1853
            'HRR R1853
            If Not (Aplicacion.PinvImpComp And Documento.Encabezado.TipDeDcmnt = Entrada) Then
               Call ImpuestosDescuentos
               Me.FrmENTRADA.Top = 4080
               LblDescuento.Visible = True
               TxtEntra(7).Visible = True
               LblPrete.Visible = True
               TxtEntra(8).Visible = True
               Label4.Visible = True
               TxtEntra(16).Visible = True
               Label2(0).Visible = True
               TxtEntra(17).Visible = True
            Else
               'Me.FrmENTRADA.Top = 100 'HRR Revision entrega
               Me.FrmENTRADA.Top = (Me.Height - Me.FrmENTRADA.Height) / 2 'HRR Revision entrega
               LblDescuento.Visible = False
               TxtEntra(7).Visible = False
               LblPrete.Visible = False
               TxtEntra(8).Visible = False
               Label4.Visible = False
               TxtEntra(16).Visible = False
               Label2(0).Visible = False
               TxtEntra(17).Visible = False
               '----------------------------------------------------------------------------------------------------
               'DAHV M4137 - INICIO
               
               Call Aplicacion.CargarParametrosAplicacion
               
               If Aplicacion.PinvImpComp = True Then
                 TxtCompra(10).Enabled = False
               Else
                 TxtCompra(10).Enabled = True
               End If
    

               'DAHV M4137 - FIN
               '----------------------------------------------------------------------------------------------------
               
            End If
            'HRR R1853
            
            Me.FrmENTRADA.Visible = True
            FrmImpuestos.Left = (Me.Width - FrmImpuestos.Width) / 2
            'Me.FrmImpuestos.Visible = True 'HRR R1853
            'HRR R1853
            If Not (Aplicacion.PinvImpComp And Documento.Encabezado.TipDeDcmnt = Entrada) Then
               Me.FrmImpuestos.Visible = True
            Else
               Me.FrmImpuestos.Visible = False
            End If
            'HRR R1853
        Else
            LaPuedoCerrar = True
        End If
'        Call CargaGrillaInterface(Me)
'        Call calcular_entra(True)
'    Case Is = DevolucionEntrada
'        Call cmdPROCEDER_Click(DevolucionEntrada)
        If Documento.Encabezado.TipDeDcmnt = DevolucionEntrada Then Call cmdPROCEDER_Click(DevolucionEntrada)
        If Documento.Encabezado.TipDeDcmnt = AprovechaDonacion Then Call cmdPROCEDER_Click(AprovechaDonacion)
    Case Is = Salida
      'HRR R1801
'        Call calcular_salidaA
'        Me.frmSALIDA.Visible = True
'        Call CentraMarco(Me.frmSALIDA)
      'HRR R1801
      
      'HRR R1801
      Call calcular_salidaD
      Me.frmBAJA.Caption = "Salida"
      Call CentraMarco(Me.frmBAJA)
      Me.frmBAJA.Visible = True
      'HRR R1801
    Case Is = FacturaVenta
        Call calcular_venta
        Me.txtValorOtrosDescuentos.Text = FormatCurrency(CCur(Me.txtTOTAL.Text), 2)
        vTotalFactura = CCur(Me.txtTOTAL.Text)
        Call otrosDescuentosForm_Load
        Call CentraMarco(Me.frmOTROSDESCUENTOS)
        Me.frmOTROSDESCUENTOS.Visible = True
    Case Is = DevolucionFacturaVenta
'        Call calcular_devo
'        Call cmdPROCEDER_Click(DevolucionFacturaVenta)
        Call calcular_venta
        Me.txtValorOtrosDescuentos.Text = FormatCurrency(CCur(Me.txtTOTAL.Text), 2)
        vTotalFactura = CCur(Me.txtTOTAL.Text)
        Call otrosDescuentosForm_Load
        Call CentraMarco(Me.frmOTROSDESCUENTOS)
        Me.frmOTROSDESCUENTOS.Visible = True
    Case Is = BajaConsumo, DevolucionBaja
        Call calcular_salidaD
        Call CentraMarco(Me.frmBAJA)
        Me.frmBAJA.Visible = True
'        LaPuedoCerrar = True
'        If Documento.Encabezado.TipDeDcmnt = BajaConsumo Then Call cmdPROCEDER_Click(BajaConsumo)
'        If Documento.Encabezado.TipDeDcmnt = DevolucionBaja Then Call cmdPROCEDER_Click(DevolucionBaja)
    Case Else
        LaPuedoCerrar = True
        Me.frmINVENTARIOS.Visible = True
    End Select
End Sub
   
Private Sub VerMovimientoContable(ParaCual As Integer)
   Dim vNumero As Long     'PJCA M1036
   Dim J& '|.DR.|, R:1633
   Dim InRespuesta As Integer
   Dim StDocu As String 'JLPB T28493 'Variable que almacena los numeros de los docuementos generados
   Dim StArr() As String 'DRMG T36958 Almacena la cuenttas contables
   Dim InI As Integer 'DRMG T36958 variable que funciona como contador par un ciclo for
    
   'PASO A CONTA
   Select Case ParaCual
   Case Is = CompraDElementos
   Case Is = Entrada, DevolucionEntrada, AprovechaDonacion
      'VerMovimientoContable Documento.Encabezado.TipDeDcmnt
      'SiMuestra = True
      If Documento.Encabezado.EsNuevoDocumento Then
         Call CargaGrillaInterface(Me)
         Call calcular_entra(True)
         muestra_cuenta "S", Me
      Else
         'Trae cuentas grabadas
         If ParaCual = Entrada Then Call Mostrar_Movimiento_Contable("ENTR", Me.txtNumero)
         If ParaCual = DevolucionEntrada Then Call Mostrar_Movimiento_Contable("DVEN", Me.txtNumero)
         If ParaCual = AprovechaDonacion Then Call Mostrar_Movimiento_Contable("APDN", Me.txtNumero)
         muestra_cuenta "N", Me
      End If
   'HRR R1801
   'Case Is = Salida
      ''SiMuestra = True
      'If Documento.Encabezado.EsNuevoDocumento Then
         'Call calcular_salidaA
         'muestra_cuenta "S", Me
      'Else
         ''Trae cuentas grabadas
         'Call Mostrar_Movimiento_Contable("SALI", Me.txtNumero)
         'muestra_cuenta "N", Me
      'End If
   'HRR R1801
   Case Is = FacturaVenta
      'SiMuestra = True
      If Documento.Encabezado.EsNuevoDocumento Then
         Call calcular_venta
         muestra_cuenta "S", Me
      Else
         'Trae cuentas grabadas
         Call Mostrar_Movimiento_Contable("VENT", Me.txtNumero)
         muestra_cuenta "N", Me
      End If
   Case Is = DevolucionFacturaVenta
      If Documento.Encabezado.EsNuevoDocumento Then
         Call calcular_venta ''calcular_devo
         muestra_cuenta "S", Me
      Else
         'Trae cuentas grabadas
         Call Mostrar_Movimiento_Contable("DEVC", Me.txtNumero)
         muestra_cuenta "N", Me
      End If
   'Case Is = BajaConsumo, DevolucionBaja 'HRR R1801
   Case Is = BajaConsumo, DevolucionBaja, Salida 'HRR R1801
      If Documento.Encabezado.EsNuevoDocumento Then
         Call calcular_salidaD
         '01 |.DR.|, R:1633
         stDesc = IIf(boAfectC, WinDoc.ElConsecutivo.Nombre, "LA BODEGA NO AFECTA CONT.") '|.DR.|, R:1633
         If Not boAfectC Then
            For J& = LBound(Mcuen) To UBound(Mcuen)
               If Mcuen(J&, 1) <> "" Then
                  Mcuen(J&, 2) = "0"
                  McuenD(J&, 2) = "0"
               End If
            Next J&
            continuar = WarnMsg("Los movimientos contables se realizar�n con valor cero." & vbCrLf & "�Desea continuar?")
         Else
            muestra_cuenta "S", Me
         End If
         '01 Fin.
         If vGrabaBaja Then
            vGrabaBaja = False
            If continuar Then
               comprobante = Buscar_Comprobante_Concepto(Documento.Encabezado.TipDeDcmnt)  'PedroJ
               If comprobante = NUL$ Then
                  Call Mensaje1("Falta el parametrizar el comprobante contable", 2)
               Else
                  ''HRR M3229
                  'If ParaCual = BajaConsumo Then
                     'Result = DoUpdate("IN_COMPROBANTE", "NU_COMP_COMP=" & NumeroCompro, "NU_AUTO_COMP=" & Documento.Encabezado.NumeroCOMPROBANTE)
                  'End If
                  ''HRR M3229
                  'HRR M3229
                  continuar = False
                  If BeginTran(STranIUp & "KARDEX") = FAIL Then GoTo FLLBGNTRN
                  StDescripError = NUL$ 'JACC M5619
                  'JLPB T28493 INICIO SE DEJA EN COMENTARIO LA SIGUIENTE LINEA
                  'NumeroCompro = WinDoc.Guardar(False, True)
                  StDocu = NUL$
                  NumeroCompro = WinDoc.Guardar(False, True, NUL$, StDocu)
                  If StDocu <> NUL$ Then StDocu = Mid(StDocu, 1, Len(StDocu) - 1)
                  If InStr(1, StDocu, ",") = 0 Then
                     StDocu = Right(String(7, "0") & StDocu, 7)
                  End If
                  'JLPB T28493 FIN
                  If NumeroCompro > 0 Then txtNumero.Text = NumeroCompro 'AASV M5669
                  If Not NumeroCompro > 0 Then GoTo FLLCMMTRS 'HRR M3229
                  'HRR M3229
                  vNumero = Buscar_Numero_Comprobante(comprobante)           'PJCA M1036
                         
                  'HRR R1801
                  ''Call Guardar_Movimiento_Contable("BAJA", Documento.Encabezado.FechaDocumento, Comprobante, Buscar_Numero_Comprobante(Comprobante), Left(Trim(WinDoc.ElConsecutivo.Nombre) & Right(String(7, "0") & NumeroCompro, 7) & ". " & Documento.Encabezado.Observaciones, 250), NUL$)  'PJCA M1036
                  ''REOL M2259
                  'If ParaCual = BajaConsumo Then
                     'Call Guardar_Movimiento_Contable("BAJA", Documento.Encabezado.FechaDocumento, Comprobante, vNumero, Left(Trim(stDesc) & Right(String(7, "0") & NumeroCompro, 7) & ". " & Documento.Encabezado.Observaciones, 250), NUL$)  'PJCA M1036
                  'Else
                     'Call Guardar_Movimiento_Contable("DBJ", Documento.Encabezado.FechaDocumento, Comprobante, vNumero, Left(Trim(stDesc) & Right(String(7, "0") & NumeroCompro, 7) & ". " & Documento.Encabezado.Observaciones, 250), NUL$)
                  'End If
                  ''REOL M2259
                  'HRR R1801
                  'HRR R1801
                  Select Case ParaCual
                     Case BajaConsumo
                        'SKRV T24951 INICIO
                        'Call Guardar_Movimiento_Contable("BAJA", Documento.Encabezado.FechaDocumento, Comprobante, vNumero, Left(Trim(stDesc) & Right(String(7, "0") & NumeroCompro, 7) & ". " & Documento.Encabezado.Observaciones, 250), NUL$)'SE ESTABLECE EN COMENTARIO
                        'Call Guardar_Movimiento_Contable("BAJA", Documento.Encabezado.FechaDocumento, comprobante, vNumero, Left(Trim(stDesc) & Right(String(7, "0") & NumeroCompro, 7) & ". " & Documento.Encabezado.Observaciones, 250), NUL$, , , fnDevDato("MODU_MOV_NIIF", "NU_CODMO_MODU", "TX_DESCM_MODU = 'Salidas o Bajas de Almac�n'", True), txtNumero) 'JLPB T28493 SE DEJA EN COMENTARIO
                        Call Guardar_Movimiento_Contable("BAJA", Documento.Encabezado.FechaDocumento, comprobante, vNumero, Left(Trim(stDesc) & StDocu & ". " & Documento.Encabezado.Observaciones, 250), NUL$, , , fnDevDato("MODU_MOV_NIIF", "NU_CODMO_MODU", "TX_DESCM_MODU = 'Salidas o Bajas de Almac�n'", True), txtNumero) 'JLPB T28493
                        'SKRV T24951 FIN
                     Case DevolucionBaja
                        'SKRV T24951 INICIO
                        'Call Guardar_Movimiento_Contable("DBJ", Documento.Encabezado.FechaDocumento, Comprobante, vNumero, Left(Trim(stDesc) & Right(String(7, "0") & NumeroCompro, 7) & ". " & Documento.Encabezado.Observaciones, 250), NUL$)'SE ESTABLECE EN COMENTARIO
                        Call Guardar_Movimiento_Contable("DBJ", Documento.Encabezado.FechaDocumento, comprobante, vNumero, Left(Trim(stDesc) & Right(String(7, "0") & NumeroCompro, 7) & ". " & Documento.Encabezado.Observaciones, 250), NUL$, , , fnDevDato("MODU_MOV_NIIF", "NU_CODMO_MODU", "TX_DESCM_MODU = 'Devoluci�n Baja de Consumo'", True), txtNumero)
                        'SKRV T24951 FIN
                     Case Salida
                        'Call Guardar_Movimiento_Contable("SALI", Documento.Encabezado.FechaDocumento, Comprobante, vNumero, Left(Trim(stDesc) & Right(String(7, "0") & NumeroCompro, 7) & ". " & Documento.Encabezado.Observaciones, 250), NUL$) 'SKRV T24261/R22366 comentario
                        'Call Guardar_Movimiento_Contable("SALI", Documento.Encabezado.FechaDocumento, Comprobante, vNumero, Left(Trim(stDesc) & Right(String(7, "0") & NumeroCompro, 7) & ". " & Documento.Encabezado.Observaciones, 250), NUL$, , , "0602", txtNumero) 'SKRV T24261/R22366 SKRV T24951 COMENTARIO
                        Call Guardar_Movimiento_Contable("SALI", Documento.Encabezado.FechaDocumento, comprobante, vNumero, Left(Trim(stDesc) & Right(String(7, "0") & NumeroCompro, 7) & ". " & Documento.Encabezado.Observaciones, 250), NUL$, , , fnDevDato("MODU_MOV_NIIF", "NU_CODMO_MODU", "TX_DESCM_MODU = 'Salidas de Dependencia'", True), txtNumero) 'SKRV T24951 COMENTARIO
                     Case Else
                  End Select
                  'HRR R1801
                  'DRMG T36958 INICIO
                  'If Result <> FAIL Then Result = DoUpdate("IN_ENCABEZADO", "NU_COMP_CONTA_ENCA=" & vNumero, "NU_AUTO_COMP_ENCA=" & Documento.Encabezado.NumeroCOMPROBANTE & " AND NU_COMP_ENCA=" & Me.txtNumero)                                                'PJCA M1036
                  StArr = Split(StDocu, ",")
                  If UBound(StArr) >= 1 Then
                     For InI = 0 To UBound(StArr)
                        If Result <> FAIL Then Result = DoUpdate("IN_ENCABEZADO", "NU_COMP_CONTA_ENCA=" & vNumero, "NU_AUTO_COMP_ENCA=" & Documento.Encabezado.NumeroCOMPROBANTE & " AND NU_COMP_ENCA=" & StArr(InI))
                     Next
                  ElseIf Result <> FAIL Then Result = DoUpdate("IN_ENCABEZADO", "NU_COMP_CONTA_ENCA=" & vNumero, "NU_AUTO_COMP_ENCA=" & Documento.Encabezado.NumeroCOMPROBANTE & " AND NU_COMP_ENCA=" & Me.txtNumero)
                  End If
                  'DRMG T36958 FIN
                  'DAHV M7054 - INICIO
                  If Result <> FAIL Then
                     If ParaCual = 8 Then
                        Valores = "NU_ENCA_RENDE = " & Val(Documento.AutoDocGenerado)
                        Valores = Valores & Coma & "TX_CECO_RENDE = " & Comi & CStr(TxtBajaD(4).Text) & Comi
                        Result = DoInsertSQL("IN_R_ENCA_DEPE", Valores)
                     End If
                  End If
                  Result = Almacenar_Auditoria_Mov(CDbl(fnDevDato("IN_ENCABEZADO", "NU_AUTO_ENCA", "NU_COMP_ENCA=" & NumeroCompro & " AND NU_AUTO_DOCU_ENCA=" & Documento.Encabezado.TipDeDcmnt & " AND NU_AUTO_COMP_ENCA=" & Documento.Encabezado.NumeroCOMPROBANTE)), comprobante, CDbl(vNumero), CInt(Documento.Encabezado.AutoDelUSUARIO)) 'JLPB T41459-R37519
                  'DAHV M7054 - FIN
                  If Result <> FAIL Then
                     Call CommitTran
                     'AASV Bloqueos Rosario 04/02/2009 INICIO
                     If NumeroCompro <> FAIL Then
                        Call Mensaje1("Se gener� el documento n�mero " & NumeroCompro, 3)
                     End If
                     'AASV Bloqueos Rosario 04/02/2009 FIN
                     'DAHV M3627 - M3628
                     'AASV R1883
                     If Aplicacion.PinvImpAntSCon Then
                        InRespuesta = MsgBox("Desea imprimir el documento?", vbYesNo + vbInformation) 'AASV M3520
                        If InRespuesta = vbYes Then 'AASV M3520
                           Documento.Encabezado.EsNuevoDocumento = False
                           Call cmdOpciones_Click(2)
                        End If
                     End If
                     Call cmdOpciones_Click(3)
                  Else
                     Call RollBackTran
                     'AASV Bloqueos Rosario 05/02/2009 INICIO
                     If StDescripError <> NUL$ Then
                        Call Mensaje1(StDescripError, 3)
                        StDescripError = NUL$
                     End If
                     'AASV Bloqueos Rosario 04/02/2009 FIN
                  End If
               End If
               'HRR M3229
               'Else
                  'Call RollBackTran
               'HRR M3229
            End If
         End If
      Else
         'Trae cuentas grabadas
         'HRR R1801
         'Call Mostrar_Movimiento_Contable("BAJA", Me.txtNumero)
         'muestra_cuenta "N", Me
         'HRR R1801
         'HRR R1801
         Select Case ParaCual
            Case BajaConsumo
               Call Mostrar_Movimiento_Contable("BAJA", Me.txtNumero)
               muestra_cuenta "N", Me
            Case DevolucionBaja
               Call Mostrar_Movimiento_Contable("DBJ", Me.txtNumero)
               muestra_cuenta "N", Me
            Case Salida
               Call Mostrar_Movimiento_Contable("SALI", Me.txtNumero)
               muestra_cuenta "N", Me
            Case Else
         End Select
         'HRR R1801
      End If
   End Select
   ''''''''''''''
   'HRR M3229
   Exit Sub
FLLBGNTRN:
FLLCMMTRS:
   Call RollBackTran
   ''JACC M5619
   'If Documento.Encabezado.TipDeDcmnt = Salida Then 'JLPB T28401 SE DEJA EN COMENTARIO
   If Documento.Encabezado.TipDeDcmnt = Salida Or Documento.Encabezado.TipDeDcmnt = BajaConsumo Then 'JLPB T28401
   'If Documento.Encabezado.TipDeDcmnt = Salida Or Documento.Encabezado.TipDeDcmnt = BajaConsumo Then 'JACC M6568 se devuelve el cambio
      If StDescripError <> NUL$ Then
         Call Mensaje1(StDescripError, 3)
         StDescripError = NUL$
      End If
   End If
   'JACC M5619
SLRDLTRNS:
   If (Documento.Encabezado.TipDeDcmnt = FacturaVenta) Or (Documento.Encabezado.TipDeDcmnt = DevolucionFacturaVenta) Or (Documento.Encabezado.TipDeDcmnt = AnulaFacturaVenta) Then
      Call WinDoc_CambioRenglonGrilla(0, WinDoc.Calcular_Documento(Me.ColCantidad, Me.ColVenUndConDes))
   Else
      'Call WinDoc_CambioRenglonGrilla(0, WinDoc.Calcular_Documento(Me.ColCantidad, Me.ColCostoUnidad))
      If continuar Then Call WinDoc_CambioRenglonGrilla(0, WinDoc.Calcular_Documento(Me.ColCantidad, Me.ColCostoUnidad))  'REOL M2085
   End If
'HRR M3229
End Sub

Private Sub calcular_entra(xLote As Boolean)
    Dim columna As Long 'Para que conserve la posici�n
    Dim Fila As Long 'Para que conserve la posici�n
    '''''''''''
    Dim bruto, neto As Double
    Dim Impuesto As Double
    Dim retef As Double, ReteIVA As Double, ReteICA As Double
    Dim descu As Double
    Dim descu2 As Double
    Dim fletes As Double
    Dim fletes2 As Double
    Dim desitem As Double
    Dim desitema As Double
    Dim desitemv As Double
    Dim Costo As Double
    Dim items As Integer
    Dim aux As Integer
    Dim SumCosto As Double
    Dim TotFletes As Double
    Dim DbIva As Double 'HRR M1818
    Dim InI As Integer 'HRR M1818
    ' DAHV T6254 -  INICIO
    ' SE DEVUELVEN LOS CAMBIOS PORQUE NO SE DETERMINAN AUN FUNCIONALES.
    'INICIO LDCR T6797
'    Dim dbImpVisuaL As Double
'    Dim dbdescVisuaL As Double
'    Dim dbretefVisuaL  As Double
'    Dim dbReteIVAVisuaL  As Double
'    Dim dbReteICAVisuaL  As Double
    'FIN LDCR T6797
    ' DAHV T6254 -  FIN
    
    'INICIO  LDCR R5027/T6173
'    Dim stTpoReg As String
'    stTpoReg = fnDevDato("PARAMETROS_IMPUESTOS", "ID_TIPO_REGI_PAIM", "CD_CODI_TERC_PAIM='" & Trim(Documento.Encabezado.Tercero.Nit) & Comi)
    'FIN  LDCR R5027/T6173
    ' DAHV T6254 -  FIN
    
    'PASO A CONTA
    Dim valo_cont(1 To 6) As Variant
'/////////////
    If Not xLote Then CargaGrillaInterface Me
'/////////////
    columna = GrdArti.Col
    Fila = GrdArti.Row
    VBase = 0
    '''
    '''Suma todos los descuentos
    descu2 = 0
    aux = 1
    Do While aux <= 20
        If Mdesc(aux, 2) = "" Then
           aux = 21
        Else
           If Trim(Mdesc(aux, 2)) = NUL$ Then Mdesc(aux, 2) = 0
           descu2 = descu2 + CDbl(Mdesc(aux, 2))
        End If
        aux = aux + 1
    Loop
    ''''''''
    If GrdArti.TextMatrix(GrdArti.Rows - 1, 1) <> "" Then
        items = GrdArti.Rows - 1
    Else
        items = GrdArti.Rows - 2
    End If
    '''Divide el valor de los descuentos en todos los art�culos
    If items <> 0 Then
        desitemv = descu2 / items
    End If
    ''''''''''''
    ''Divide el valor de los fletes en cada item
    If TxtEntra(14).Text = "" Then
        TxtEntra(14).Text = "0"
    End If
    
    If Trim(TxtEntra(14).Text) = NUL$ Then TxtEntra(14).Text = 0
    fletes = CDbl(TxtEntra(14).Text)
    If items <> 0 Then fletes2 = fletes / items: fletes2 = Aplicacion.Formatear_Valor(fletes2)
    
    ''''''''''''
    GrdArti.Row = 0
    desitema = 0
    bruto = 0
    SumCosto = 0
    
    ReDim ArrIVA(GrdArti.Rows - 1)
    'ReDim ArrTipoIVA(2, GrdArti.Rows - 1)  'LDCR 2da PARTE R9033/T9930
    If Not IsNumeric(TxtEntra(7).Text) Then TxtEntra(7).Text = "0"
    On Error GoTo salto
    While GrdArti.Row <= GrdArti.Rows - 1
        GrdArti.Row = GrdArti.Row + 1
        If GrdArti.TextMatrix(GrdArti.Row, 1) <> NUL$ Then
            If GrdArti.TextMatrix(GrdArti.Row, 2) = "" Or IsNumeric(GrdArti.TextMatrix(GrdArti.Row, 2)) = False Then
              GrdArti.TextMatrix(GrdArti.Row, 2) = "0"
            End If
            If GrdArti.TextMatrix(GrdArti.Row, 3) = "" Or IsNumeric(GrdArti.TextMatrix(GrdArti.Row, 3)) = False Then
              GrdArti.TextMatrix(GrdArti.Row, 3) = "0"
            End If
            If GrdArti.TextMatrix(GrdArti.Row, 5) = "" Or IsNumeric(GrdArti.TextMatrix(GrdArti.Row, 5)) = False Then
              GrdArti.TextMatrix(GrdArti.Row, 5) = "0"
            End If
            'GrdArti.TextMatrix(GrdArti.Row, 4) = CDbl(GrdArti.TextMatrix(GrdArti.Row, 2)) * CDbl(GrdArti.TextMatrix(GrdArti.Row, 3)) 'HRR M1818
            'DAHV Se devuelven los cambios
            GrdArti.TextMatrix(GrdArti.Row, 4) = Aplicacion.Formatear_Valor(CDbl(GrdArti.TextMatrix(GrdArti.Row, 2)) * CDbl(GrdArti.TextMatrix(GrdArti.Row, 3))) 'HRR M1818 HRR M5080
            'GrdArti.TextMatrix(GrdArti.Row, 4) = Aplicacion.Formatear_Valor(CDbl(GrdArti.TextMatrix(GrdArti.Row, 2)) * CDbl(GrdArti.TextMatrix(GrdArti.Row, 3)) * CDbl(GrdArti.TextMatrix(GrdArti.Row, 8)) / CDbl(GrdArti.TextMatrix(GrdArti.Row, 9))) 'HRR M1818 HRR M5080
            'DAHV Se devuelven los cambios
            'DAHV Se devuelven los cambios
            Costo = CDbl(GrdArti.TextMatrix(GrdArti.Row, 2)) * CDbl(GrdArti.TextMatrix(GrdArti.Row, 3)) 'HRR M5080
             
            'Costo = CDbl(GrdArti.TextMatrix(GrdArti.Row, 2)) * CDbl(GrdArti.TextMatrix(GrdArti.Row, 3)) * CDbl(GrdArti.TextMatrix(GrdArti.Row, 8)) / CDbl(GrdArti.TextMatrix(GrdArti.Row, 9)) 'HRR M5080
            'DAHV Se devuelven los cambios
            Costo = Aplicacion.Formatear_Valor(Costo) 'HRR M1818
                       
            'desitem = Round((CSng(TxtEntra(7).Text) * Costo) / 100)
            
            'desitem = Aplicacion.Formatear_Valor((CSng(TxtEntra(7).Text) * Costo) / 100) 'HRR M1818
            desitem = (CDbl(TxtEntra(7).Text) * Costo) / 100 'HRR M1818
            desitem = Aplicacion.Formatear_Valor(desitem) 'HRR M1818
            'desitema = Round(desitema + desitem)
            desitema = Aplicacion.Formatear_Valor(desitema + desitem)
            SumCosto = SumCosto + (Costo - desitem - desitemv)
            SumaCosto = SumCosto    'REOL M1642
'            Costo = (((Costo - desitem - desitemv) * CDbl(GrdArti.TextMatrix(GrdArti.Row, 5))) / 100)
            'Costo = (((Costo - desitemv) * CDbl(GrdArti.TextMatrix(GrdArti.Row, 5))) / 100) 'REOL M1298 'HRR M1818
            'JLPB T22240 INICIO
            If BoAproxCent Then
               DbIva = AproxCentena(Round((((Costo - desitemv) * CDbl(GrdArti.TextMatrix(GrdArti.Row, 5))) / 100)))
            Else
            'JLPB T22240 FIN
               DbIva = (((Costo - desitemv) * CDbl(GrdArti.TextMatrix(GrdArti.Row, 5))) / 100)
            End If 'JLPB T22240
            'ArrIVA(GrdArti.Row - 1) = Costo 'HRR M1818
            DbIva = Aplicacion.Formatear_Valor(DbIva) 'HRR M1818
                        
            ArrIVA(GrdArti.Row - 1) = DbIva 'HRR M1818
            'INICIO LDCR 2da PARTE R9033/T9930
'            ArrTipoIVA(0, GrdArti.Row - 1) = DbIVA
'            ArrTipoIVA(1, GrdArti.Row - 1) = CDbl(GrdArti.TextMatrix(GrdArti.Row, 4))
'            ArrTipoIVA(2, GrdArti.Row - 1) = GrdArti.TextMatrix(GrdArti.Row, 0)
            'FIN LDCR 2da PARTE R9033/T9930
            
     ' SILVIA : SE LE AGREGO LA VARIABLE QUE LE RESTA EL DESCUENTO
            'VBase = VBase + CDbl(IIf(GrdArti.TextMatrix(GrdArti.Row, 5) <> 0, Val(GrdArti.TextMatrix(GrdArti.Row, 4)) - Val(desitem), 0)) 'HRR M1818
            VBase = VBase + CDbl(IIf(GrdArti.TextMatrix(GrdArti.Row, 5) <> 0, CDbl(GrdArti.TextMatrix(GrdArti.Row, 4)) - CDbl(desitem), 0)) 'HRR M1818
      
            'Costo = Round(Costo)
            'Costo = Aplicacion.Formatear_Valor(Costo) 'HRR M1818 Esto es iva
      ''Si la empresa es del estado el iva hace parte del costo
            'HRR M1818
'            If Empresa_Comercial = False Then
'                GrdArti.TextMatrix(GrdArti.Row, 4) = Costo + fletes2 + (CDbl(GrdArti.TextMatrix(GrdArti.Row, 2)) * CDbl(GrdArti.TextMatrix(GrdArti.Row, 3))) - desitem - desitemv
'            Else
'                GrdArti.TextMatrix(GrdArti.Row, 4) = fletes2 + (CDbl(GrdArti.TextMatrix(GrdArti.Row, 2)) * CDbl(GrdArti.TextMatrix(GrdArti.Row, 3))) - desitem - desitemv
'            End If
            ''''''''''''
'            If fletes > 0 Then
'              TotFletes = TotFletes + GrdArti.TextMatrix(GrdArti.Row, 4) - ((CDbl(GrdArti.TextMatrix(GrdArti.Row, 2)) * CDbl(GrdArti.TextMatrix(GrdArti.Row, 3))) - desitem - desitemv)
'              If (GrdArti.Row = GrdArti.Rows - 1) And TotFletes <> fletes Then
'                 GrdArti.TextMatrix(GrdArti.Row, 4) = GrdArti.TextMatrix(GrdArti.Row, 4) + (fletes - TotFletes)
'              End If
'            End If
            'HRR M1818
            If Empresa_Comercial = False Then
                GrdArti.TextMatrix(GrdArti.Row, 4) = DbIva + fletes2 + Costo - desitem - desitemv
            Else
                GrdArti.TextMatrix(GrdArti.Row, 4) = fletes2 + Costo - desitem - desitemv
            End If
            ''''''''''''
            If fletes > 0 Then
              TotFletes = TotFletes + GrdArti.TextMatrix(GrdArti.Row, 4) - (Costo - desitem - desitemv)
              If (GrdArti.Row = GrdArti.Rows - 1) And TotFletes <> fletes Then
                 GrdArti.TextMatrix(GrdArti.Row, 4) = GrdArti.TextMatrix(GrdArti.Row, 4) + (fletes - TotFletes)
              End If
            End If
            'HRR M1818
            
            GrdArti.TextMatrix(GrdArti.Row, 4) = Aplicacion.Formatear_Valor(GrdArti.TextMatrix(GrdArti.Row, 4))
            'GrdArti.TextMatrix(GrdArti.Row, 6) = COSTO 'HRR M1818
            GrdArti.TextMatrix(GrdArti.Row, 6) = DbIva 'HRR M1818
            'DAHV Se devuelven los cambios
             GrdArti.TextMatrix(GrdArti.Row, 8) = GrdArti.TextMatrix(GrdArti.Row, 4) - fletes2 'HRR M5080 Se coloca en comentario
             GrdArti.TextMatrix(GrdArti.Row, 8) = Aplicacion.Formatear_Valor(GrdArti.TextMatrix(GrdArti.Row, 8)) 'HRR M5080 Se coloca en comentario
            'DAHV Se devuelven los cambios
        End If
    Wend
salto:
    On Error GoTo 0
    'Impuesto = Round(sumar_items(GrdArti, 6))
    Impuesto = Aplicacion.Formatear_Valor(sumar_items(GrdArti, 6))
    ''Si la empresa es del estado el iva hace parte del costo
    If Empresa_Comercial = False Then
        bruto = sumar_items(GrdArti, 4) - Impuesto - fletes + descu2 + desitema
        'bruto = bruto - impuesto - fletes
    Else
        bruto = sumar_items(GrdArti, 4) - fletes + descu2 + desitema
        'bruto = bruto - fletes
    End If
    '''''''''''''
    If Not IsNumeric(TxtEntra(7).Text) Then TxtEntra(7) = 0
    If Not IsNumeric(TxtEntra(8).Text) Then TxtEntra(8) = 0
    If Not IsNumeric(TxtEntra(16).Text) Then TxtEntra(16) = 0
    If Not IsNumeric(TxtEntra(17).Text) Then TxtEntra(17) = 0
    If Not IsNumeric(TxtEntra(20).Text) Then TxtEntra(20) = 0

   'HRR M1818
'    descu = Aplicacion.Formatear_Valor((CSng(TxtEntra(7).Text) * bruto) / 100)
'    retef = Aplicacion.Formatear_Valor((CSng(TxtEntra(8).Text) * SumCosto) / 100)
'    ReteIVA = Aplicacion.Formatear_Valor((CSng(TxtEntra(16).Text) * Impuesto) / 100)
'    ReteICA = Aplicacion.Formatear_Valor((CSng(TxtEntra(17).Text) * SumCosto) / 100)
'    neto = bruto - descu - descu2 - ReteIVA - ReteICA - CDbl(TxtEntra(20))
    'HRR M1818
    
    'HRR M1818
    descu = desitema
    retef = 0
    ReteIVA = 0
    ReteICA = 0
      With GrdDeIm
         For InI = 1 To .Rows - 1
            'If Mid(.TextMatrix(Ini, 1), 1, 1) = "R" Then
            'If Mid(.TextMatrix(Ini, 1), 1, 1) = "R" Or LnAntoEncaEnid > 0 Then 'GMS M3559 'JLPB T15675 SE DEJA EN COMENTARIO
            If Mid(.TextMatrix(InI, 1), 1, 1) = "R" Then  'JLPB T15675
               If Not IsNumeric(.TextMatrix(InI, 3)) Then .TextMatrix(InI, 3) = 0   'JACC M3614   32283
               retef = retef + Aplicacion.Formatear_Valor(IIf(.TextMatrix(InI, 3) = NUL$, 0, (bruto - descu) * CDbl(.TextMatrix(InI, 3)) / 100))
            ElseIf Mid(.TextMatrix(InI, 1), 1, 1) = "M" Then
               If Not IsNumeric(.TextMatrix(InI, 3)) Then .TextMatrix(InI, 3) = 0   'JACC M3614   32283
               ReteIVA = ReteIVA + Aplicacion.Formatear_Valor(IIf(.TextMatrix(InI, 3) = NUL$, 0, Impuesto * CDbl(.TextMatrix(InI, 3)) / 100))
            ElseIf Mid(.TextMatrix(InI, 1), 1, 1) = "C" Then
               If Not IsNumeric(.TextMatrix(InI, 3)) Then .TextMatrix(InI, 3) = 0   'JACC M3614   32283
               ReteICA = ReteICA + Aplicacion.Formatear_Valor(IIf(.TextMatrix(InI, 3) = NUL$, 0, (bruto - descu) * CDbl(.TextMatrix(InI, 3)) / 100))
            End If
         Next
      End With
        
    
    'DAHV T7478 - INICIO
    If BoAproxCent = True Then
        descu = AproxCentena(Round(descu))
        descu2 = AproxCentena(Round(descu2))
        retef = AproxCentena(Round(retef))
        ReteIVA = AproxCentena(Round(ReteIVA))
        ReteICA = AproxCentena(Round(ReteICA))
        Impuesto = AproxCentena(Round(Impuesto))
    End If
    'DAHV T7478 - FIN
    
    
    neto = bruto - descu - descu2 - ReteIVA - ReteICA - CDbl(TxtEntra(20))
    DbTotIva = Impuesto
    'HRR M1818

    'Si el proveedor cobra los fletes o son por aparte
    If OptProveedor.value = True Then
        neto = neto + Impuesto + fletes - retef
    Else
        neto = neto + Impuesto - retef
    End If
    
    descu = descu + descu2
    

    'DAHV T7478 - INICIO
'    'INICIO GAVL T6423
'    If BoAproxCent = True Then
'  'INICIO LDCR T6797
'        descu = AproxCentena(Round(descu))
'        retef = AproxCentena(Round(retef))
'        ReteIVA = AproxCentena(Round(ReteIVA))
'        ReteICA = AproxCentena(Round(ReteICA))
'        Impuesto = AproxCentena(Round(Impuesto))
' '       neto = AproxCentena(Round(neto))
'
''        dbdescVisuaL = AproxCentena(Round(descu))
''        dbImpVisuaL = AproxCentena(Round(Impuesto))
''        dbretefVisuaL = AproxCentena(Round(retef))
''        dbReteIVAVisuaL = AproxCentena(Round(ReteIVA))
''        dbReteICAVisuaL = AproxCentena(Round(ReteICA))
''        neto = AproxCentena(Round(neto)) 'LDCR T6797 SE COLOCA EN COMENTARIO
'        'FIN LDCR T6797
'    End If
'    'FIN GAVL T6423
    'DAHV T7478 - FIN
    
     'INICIO LDCR T6797
    TxtEntra(10).Text = Aplicacion.Formatear_Valor(bruto)
    TxtEntra(11).Text = Aplicacion.Formatear_Valor(descu)
    TxtEntra(12).Text = Aplicacion.Formatear_Valor(Impuesto)
    TxtEntra(13).Text = Aplicacion.Formatear_Valor(retef)
    TxtEntra(14).Text = Aplicacion.Formatear_Valor(fletes)
    TxtEntra(15).Text = Aplicacion.Formatear_Valor(neto)
    TxtEntra(18).Text = Aplicacion.Formatear_Valor(ReteIVA)
    TxtEntra(19).Text = Aplicacion.Formatear_Valor(ReteICA)
    TxtEntra(20).Text = Aplicacion.Formatear_Valor(TxtEntra(20).Text)
    
'    TxtEntra(10).Text = Aplicacion.Formatear_Valor(bruto)
'    TxtEntra(11).Text = Aplicacion.Formatear_Valor(dbdescVisuaL)
'    TxtEntra(12).Text = Aplicacion.Formatear_Valor(dbImpVisuaL)
'    TxtEntra(13).Text = Aplicacion.Formatear_Valor(dbretefVisuaL)
'    TxtEntra(14).Text = Aplicacion.Formatear_Valor(fletes)
'    TxtEntra(15).Text = Aplicacion.Formatear_Valor(neto)
'    TxtEntra(18).Text = Aplicacion.Formatear_Valor(dbReteIVAVisuaL)
'    TxtEntra(19).Text = Aplicacion.Formatear_Valor(dbReteICAVisuaL)
'    TxtEntra(20).Text = Aplicacion.Formatear_Valor(TxtEntra(20).Text)
    'FIN LDCR T6797
 
    ''PASO A CONTA
    valo_cont(1) = bruto
    valo_cont(2) = descu
    valo_cont(3) = Impuesto
    valo_cont(4) = retef
    valo_cont(5) = fletes
    valo_cont(6) = neto
    'If Aplicacion.Interfaz_Contabilidad Then Call Interface_ContableEntrada(Documento.Encabezado.TipDeDcmnt)
    If Aplicacion.Interfaz_Contabilidad Then Call Interface_ContableEntrada(Documento.Encabezado.TipDeDcmnt, Documento.Encabezado.BodegaORIGEN) 'Req 1552
    '''''''''''''''''
    ValReg = bruto - descu + Impuesto + fletes
    On Error GoTo TRMNR
    If Me.GrdArti.Rows < 2 Then Me.GrdArti.Rows = 2
    'If Ppto And GrdArti.TextMatrix(1, 0) <> NUL$ Then 'HRR M5080
    If Aplicacion.Interfaz_Presupuesto And GrdArti.TextMatrix(1, 0) <> NUL$ Then 'HRR M5080
'        ArtiPpto = Articulos_Presupuesto(GrdArti, "NU_COMP_CUEN,NU_CDPE_CUEN", 8, 0)
        'ArtiPpto = Articulos_Presupuesto(GrdArti, 8, 0)     'DEPURACION DE CODIGO
        'ArtiPpto = Articulos_Presupuesto(GrdArti, 8, 0, , , 3)  'DEPURACION DE CODIGO 'HRR M5080
        'ArtiPpto = Articulos_Presupuesto(GrdArticulos, 8, 0, , 3)  'HRR M5080
        ''JACC M6427
        'If Documento.Encabezado.TipDeDcmnt = Entrada And (TipoRegimen & TipoPers) = "10" Then
        If Documento.Encabezado.TipDeDcmnt = Entrada Then
           'ArtiPpto = Articulos_Presupuesto(GrdArticulos, 8, 0, , 3, CDbl(Me.TxtEntra(18)))
           'JACC M6527
             If Not IsNumeric(Me.TxtEntra(16)) Then Me.TxtEntra(16) = 0
             'ArtiPpto = Articulos_Presupuesto(GrdArticulos, 8, 0, , 3, CDbl(Me.TxtEntra(16)))
             ArtiPpto = Articulos_Presupuesto(GrdArticulos, 8, 0, , 3, CDbl(Me.TxtEntra(16)), Documento.Encabezado.Tercero.Nit) 'LDCR R5027/T6173
           'JACC M6527
        Else
          ArtiPpto = Articulos_Presupuesto(GrdArticulos, 8, 0, , 3)
        End If
        'JACC M6427
        
        
        If ArtiFlete <> NUL$ And CDbl(TxtEntra(14)) > 0 Then
            ReDim Preserve ArtiPpto(3, UBound(ArtiPpto(), 2) + 1)
            ArtiPpto(0, UBound(ArtiPpto(), 2)) = ArtiFlete
            ArtiPpto(3, UBound(ArtiPpto(), 2)) = CCur(TxtEntra(14).Text)
        End If
    End If
TRMNR:
    On Error GoTo 0
    GrdArti.Col = columna  ''Recupera la posici�n
    If Fila > 0 And Fila < GrdArti.Rows Then GrdArti.Row = Fila ''Recupera la posici�n
'    GrdArti.Row = Fila  ''Recupera la posici�n
End Sub

''HRR R1801
'Private Sub calcular_salidaA()
'    Dim columna As Long 'Para que conserve la posici�n
'    Dim Fila As Long 'Para que conserve la posici�n
'    Dim valor As Double
'    '''''''''''
'    'PASO A CONTA
'    Dim valo_cont(1 To 2) As Variant
'    ''''''''''
'    columna = GrdArti.Col
'    Fila = GrdArti.Row
'    '''
'    GrdArti.Row = 0
''/////////////
'    CargaGrillaInterface Me
''/////////////
'    On Error GoTo salto
'    While GrdArti.Row <= GrdArti.Rows - 1
'        GrdArti.Row = GrdArti.Row + 1
'        If GrdArti.TextMatrix(GrdArti.Row, 1) <> NUL$ Then
'            If GrdArti.TextMatrix(GrdArti.Row, 2) = "" Or IsNumeric(GrdArti.TextMatrix(GrdArti.Row, 2)) = False Then
'                GrdArti.TextMatrix(GrdArti.Row, 2) = "0"
'            End If
'            If GrdArti.TextMatrix(GrdArti.Row, 3) = "" Or IsNumeric(GrdArti.TextMatrix(GrdArti.Row, 3)) = False Then
'                GrdArti.TextMatrix(GrdArti.Row, 3) = "0"
'            End If
'            'Precio total
'            valor = CDbl(GrdArti.TextMatrix(GrdArti.Row, 2)) * CDbl(GrdArti.TextMatrix(GrdArti.Row, 3))
'            GrdArti.TextMatrix(GrdArti.Row, 4) = Aplicacion.Formatear_Valor(valor)
'        End If
'    Wend
'salto:
'    On Error GoTo 0
'    TxtSaliA(7).Text = Aplicacion.Formatear_Valor(sumar_items(GrdArti, 4))
'    '''
'    ''PASO A CONTA
'    valo_cont(1) = TxtSaliA(7).Text
'    valo_cont(2) = TxtSaliA(7).Text
'    'If Aplicacion.Interfaz_Contabilidad Then Call Interface_ContableSalida 'HRR R1801
'    '''''''''''''''''
'    GrdArti.Col = columna  ''Recupera la posici�n
'    If Fila > 0 And Fila < GrdArti.Rows Then GrdArti.Row = Fila ''Recupera la posici�n
''    GrdArti.Row = Fila  ''Recupera la posici�n
'End Sub
'HRR R1801
Private Sub calcular_venta()
    Dim columna As Long 'Para que conserve la posici�n
    Dim Fila As Long 'Para que conserve la posici�n
    '''''''''''
    Dim bruto As Double
    Dim descu As Double
    Dim descu2 As Double
    Dim desitem As Double
    Dim desitemv As Double
    Dim Impuesto As Double
    Dim retef As Double
    Dim fletes As Double
    Dim neto As Double
    Dim Valor As Double
    Dim items As Integer
    Dim aux As Integer
    
    ''''''''''
    'PASO A CONTA
    Dim valo_cont(1 To 8) As Variant
    ''''''''''
    columna = GrdArti.Col
    Fila = GrdArti.Row
'/////////////
    CargaGrillaInterface Me, True
'/////////////
    VBase = 0
    '''
    '''Suma todos los descuentos
    descu2 = 0
    aux = 1
    Do While aux <= 20
     If Mdesc(aux, 2) = "" Then
       aux = 21
     Else
       descu2 = descu2 + CDbl(Mdesc(aux, 2))
     End If
     aux = aux + 1
    Loop
    ''''''''
    If GrdArti.TextMatrix(GrdArti.Rows - 1, 1) <> "" Then
      items = GrdArti.Rows - 1
    Else
      items = GrdArti.Rows - 2
    End If
    '''Divide el valor de los descuentos en todos los art�culos
    If items <> 0 Then
      desitemv = descu2 / items
    End If
    ''''''''''''
    If TxtVenta(19).Text = "" Then
      TxtVenta(19).Text = "0"
    End If
    fletes = CDbl(TxtVenta(19).Text)
    
    GrdArti.Row = 0
    If Not IsNumeric(TxtVenta(12).Text) Then TxtVenta(12).Text = "0"
    On Error GoTo salto
    While GrdArti.Row <= GrdArti.Rows - 1
        GrdArti.Row = GrdArti.Row + 1
        If GrdArti.TextMatrix(GrdArti.Row, 1) <> NUL$ Then
            If GrdArti.TextMatrix(GrdArti.Row, 2) = "" Or IsNumeric(GrdArti.TextMatrix(GrdArti.Row, 2)) = False Then
                GrdArti.TextMatrix(GrdArti.Row, 2) = "0"
            End If
            If GrdArti.TextMatrix(GrdArti.Row, 3) = "" Or IsNumeric(GrdArti.TextMatrix(GrdArti.Row, 3)) = False Then
                GrdArti.TextMatrix(GrdArti.Row, 3) = "0"
            End If
            If GrdArti.TextMatrix(GrdArti.Row, 5) = "" Or IsNumeric(GrdArti.TextMatrix(GrdArti.Row, 5)) = False Then
                GrdArti.TextMatrix(GrdArti.Row, 5) = "0"
            End If
            Valor = CDbl(GrdArti.TextMatrix(GrdArti.Row, 2)) * CDbl(GrdArti.TextMatrix(GrdArti.Row, 3))
            'JLPB T41942 INICIO
            If WinDoc.ElConsecutivo.AutoDocu = 11 Then
               If Valor < WinDoc.Calcular_Renglon(GrdArti.Row, Me.ColCantidad, Me.ColVentaSinIVA, Me.ColMultiplicador, Me.ColDivisor) Then
                  Valor = WinDoc.Calcular_Renglon(GrdArti.Row, Me.ColCantidad, Me.ColVentaSinIVA, Me.ColMultiplicador, Me.ColDivisor)
               End If
            End If
            'JLPB T41942 FIN
            GrdArti.TextMatrix(GrdArti.Row, 4) = Format(Valor, "############0.#0")
            desitem = (CSng(TxtVenta(12).Text) * Valor) / 100
            Valor = (((Valor - desitem - desitemv) * CDbl(GrdArti.TextMatrix(GrdArti.Row, 5)) / 100))
            GrdArti.TextMatrix(GrdArti.Row, 6) = Valor
            'DAHV Se devuelven los cambios
            Valor = CDbl(GrdArti.TextMatrix(GrdArti.Row, 2)) * CDbl(GrdArti.TextMatrix(GrdArti.Row, 3)) 'HRR M5080 Se coloca en comentario
            'JLPB T41942 INICIO
            If WinDoc.ElConsecutivo.AutoDocu = 11 Then
               If Valor < WinDoc.Calcular_Renglon(GrdArti.Row, Me.ColCantidad, Me.ColVentaSinIVA, Me.ColMultiplicador, Me.ColDivisor) Then
                  Valor = WinDoc.Calcular_Renglon(GrdArti.Row, Me.ColCantidad, Me.ColVentaSinIVA, Me.ColMultiplicador, Me.ColDivisor)
               End If
            End If
            'JLPB T41942 FIN
            Valor = Valor - desitem - desitemv + CDbl(GrdArti.TextMatrix(GrdArti.Row, 6)) 'HRR M5080 Se coloca en comentario
            GrdArti.TextMatrix(GrdArti.Row, 9) = Format(Valor, "############0.##00") 'HRR M5080 Se coloca en comentario
            'DAHV Se devuelven los cambios
            'SILVIA : SE LE AGREGO LA VARIABLE QUE LE RESTA EL DESCUENTO
            VBase = VBase + CDbl(IIf(GrdArti.TextMatrix(GrdArti.Row, 5) <> 0, Val(GrdArti.TextMatrix(GrdArti.Row, 4)) - Val(desitem), 0))
        End If
    Wend
salto:
    On Error GoTo 0
    bruto = sumar_items(GrdArti, 4)
    'DAHV T8058 - INICIO
    If bruto <> IIf(txtSUBTO.Text <> NUL$, CDbl(txtSUBTO.Text), 0) Then
        bruto = IIf(txtSUBTO.Text <> NUL$, CDbl(txtSUBTO.Text), 0)
    End If
    
    
    'DAHV T8058 - FIN
    
    If Not IsNumeric(TxtVenta(12).Text) Then TxtVenta(12).Text = "0"
    If Not IsNumeric(TxtVenta(13).Text) Then TxtVenta(13).Text = "0"
    descu = Round((CSng(TxtVenta(12).Text) * bruto) / 100)
    descu2 = Round(descu2)
    descu = Round(descu + descu2)
    Impuesto = Round(sumar_items(GrdArti, 6))
    retef = Round((CSng(TxtVenta(13).Text) * bruto) / 100)
    neto = bruto - descu + Impuesto - retef + fletes
    TxtVenta(15).Text = FormatNumber(bruto, 2)
    TxtVenta(16).Text = FormatNumber(descu, 2)
    TxtVenta(17).Text = FormatNumber(Impuesto, 2)
    TxtVenta(18).Text = FormatNumber(retef, 2)
    TxtVenta(19).Text = FormatNumber(fletes, 2)
    TxtVenta(20).Text = FormatNumber(neto, 2)
    '''
    ''PASO A CONTA
    valo_cont(1) = bruto
    valo_cont(2) = descu
    valo_cont(3) = Impuesto
    valo_cont(4) = retef
    valo_cont(5) = fletes
    valo_cont(6) = neto
    valo_cont(7) = calcular_costo(GrdArti, 2, 7)
    valo_cont(8) = valo_cont(7)

    'If Aplicacion.Interfaz_Contabilidad Then Call Interface_ContableVenta(Documento.Encabezado.TipDeDcmnt)
    If Aplicacion.Interfaz_Contabilidad Then Call Interface_ContableVenta(Documento.Encabezado.TipDeDcmnt, Documento.Encabezado.BodegaORIGEN)
    '''''''''''''''''
    If GrdArti.Rows > 1 Then
        If Aplicacion.Interfaz_Presupuesto And GrdArti.TextMatrix(1, 0) <> NUL$ Then
'           MatPpto = Articulos_Presupuesto(GrdArti, "NU_COMP_CUEN,NU_CDPE_CUEN", 9, 1)
           'MatPpto = Articulos_Presupuesto(GrdArti, 9, 1)      'DEPURACION DE CODIGO
           'MatPpto = Articulos_Presupuesto(GrdArti, 9, 1, True) 'JACC M3206 'HRR M5080
           MatPpto = Articulos_Presupuesto(GrdArticulos, 9, 1)  'JACC M3206 'HRR M5080
           If ArtiFlete <> NUL$ And CDbl(TxtVenta(19).Text) > 0 Then
              ReDim Preserve MatPpto(3, UBound(MatPpto(), 2) + 1)
              MatPpto(0, UBound(MatPpto(), 2)) = ArtiFlete
              MatPpto(1, UBound(MatPpto(), 2)) = DescArtiF
              MatPpto(2, UBound(MatPpto(), 2)) = CCur(TxtVenta(19).Text)
              MatPpto(3, UBound(MatPpto(), 2)) = CCur(TxtVenta(19).Text)
           End If
        End If
    End If
    GrdArti.Col = columna  ''Recupera la posici�n
    If Fila > 0 And Fila < GrdArti.Rows Then GrdArti.Row = Fila ''Recupera la posici�n
End Sub

Private Sub calcular_salidaD()
    Dim columna As Long 'Para que conserve la posici�n
    Dim Fila As Long 'Para que conserve la posici�n
    Dim Valor As Double
    '''''''''''
    'PASO A CONTA
    Dim valo_cont(1 To 2) As Variant
    Dim valo_cont2(1 To 4) As Variant
    ''''''''''
    columna = GrdArti.Col
    Fila = GrdArti.Row
    '''
'/////////////
    CargaGrillaInterface Me
'/////////////
    GrdArti.Row = 0
    On Error GoTo salto
    While GrdArti.Row <= GrdArti.Rows - 1
        GrdArti.Row = GrdArti.Row + 1
        If GrdArti.TextMatrix(GrdArti.Row, 1) <> NUL$ Then
            If GrdArti.TextMatrix(GrdArti.Row, 2) = "" Or IsNumeric(GrdArti.TextMatrix(GrdArti.Row, 2)) = False Then
                GrdArti.TextMatrix(GrdArti.Row, 2) = "0"
            End If
            If GrdArti.TextMatrix(GrdArti.Row, 3) = "" Or IsNumeric(GrdArti.TextMatrix(GrdArti.Row, 3)) = False Then
                GrdArti.TextMatrix(GrdArti.Row, 3) = "0"
            End If
            'Precio total
            'DAHV Se devuelven los cambios
             Valor = CDbl(GrdArti.TextMatrix(GrdArti.Row, 2)) * CDbl(GrdArti.TextMatrix(GrdArti.Row, 3)) 'HRR M5080
            'valor = CDbl(GrdArti.TextMatrix(GrdArti.Row, 2)) * CDbl(GrdArti.TextMatrix(GrdArti.Row, 3)) * CDbl(GrdArti.TextMatrix(GrdArti.Row, 8)) / CDbl(GrdArti.TextMatrix(GrdArti.Row, 9)) 'HRR M5080
            'DAHV Se devuelven los cambios
            GrdArti.TextMatrix(GrdArti.Row, 4) = Aplicacion.Formatear_Valor(Valor)
            'Costo unitario
            GrdArti.TextMatrix(GrdArti.Row, 5) = Aplicacion.Formatear_Valor(Buscar_Valor_Arti(GrdArti.TextMatrix(GrdArti.Row, 0), "VL_COPR_ARTI"))
            'Costo Total
            'DAHV Se devuelven los cambios
             Valor = CDbl(GrdArti.TextMatrix(GrdArti.Row, 2)) * CDbl(GrdArti.TextMatrix(GrdArti.Row, 5)) 'HRR M5080
            'valor = CDbl(GrdArti.TextMatrix(GrdArti.Row, 2)) * CDbl(GrdArti.TextMatrix(GrdArti.Row, 5)) * CDbl(GrdArti.TextMatrix(GrdArti.Row, 8)) / CDbl(GrdArti.TextMatrix(GrdArti.Row, 9)) 'HRR M5080
            'DAHV Se devuelven los cambios
            GrdArti.TextMatrix(GrdArti.Row, 6) = Aplicacion.Formatear_Valor(Valor)
        End If
    Wend
salto:
    On Error GoTo 0
    TxtBajaD(9).Text = Aplicacion.Formatear_Valor(sumar_items(GrdArti, 4))
    '''
    ''PASO A CONTA
    If Not IsNumeric(TxtBajaD(9).Text) Then TxtBajaD(9).Text = "0"
    If OptSalida(1).value = True Then
        valo_cont2(1) = TxtBajaD(9).Text
        valo_cont2(2) = TxtBajaD(9).Text
        valo_cont2(3) = sumar_items(GrdArti, 6)
        valo_cont2(4) = valo_cont2(3)
    Else
        valo_cont(1) = TxtBajaD(9).Text
        valo_cont(2) = TxtBajaD(9).Text
    End If
    If Aplicacion.Interfaz_Contabilidad Then Call Interface_ContableBajaConsumo(Documento.Encabezado.TipDeDcmnt)
    '''''''''''''''''
    GrdArti.Col = columna  ''Recupera la posici�n
    If Fila > 0 And Fila < GrdArti.Rows Then GrdArti.Row = Fila ''Recupera la posici�n
'    GrdArti.Row = Fila  ''Recupera la posici�n
End Sub

'DEPURACION DE CODIGO
'Private Sub OLDcalcular_devo()
'    Dim columna As Long 'Para que conserve la posici�n
'    Dim Fila As Long 'Para que conserve la posici�n
'    '''''''''''
'    Dim bruto As Double
'    Dim descu As Double
'    Dim descu2 As Double
'    Dim desitem As Double
'    Dim desitemv As Double
'    Dim Impuesto As Double
'    Dim retef As Double
'    Dim neto As Double
'    Dim valor As Double
'    Dim items As Integer
'    Dim aux As Integer
'    ''''''''''
'    'PASO A CONTA
'    Dim valo_cont(1 To 7) As Variant
'    ''''''''''
'    columna = GrdArti.Col
'    Fila = GrdArti.Row
''/////////////
'    CargaGrillaInterface Me
''/////////////
'    '''
'    '''Suma todos los descuentos
'    descu2 = 0
'    aux = 1
'    Do While aux <= 20
'     If Mdesc(aux, 2) = "" Then
'       aux = 21
'     Else
'       descu2 = descu2 + CDbl(Mdesc(aux, 2))
'     End If
'     aux = aux + 1
'    Loop
'    ''''''''
'    If GrdArti.TextMatrix(GrdArti.Rows - 1, 1) <> "" Then
'      items = GrdArti.Rows - 1
'    Else
'      items = GrdArti.Rows - 2
'    End If
'    '''Divide el valor de los descuentos en todos los art�culos
'    If items <> 0 Then
'      desitemv = descu2 / items
'    End If
'    ''''''''''''
'    GrdArti.Row = 0
'    If Not IsNumeric(TxtDevo(9).Text) Then TxtDevo(9).Text = "0"
'    On Error GoTo salto
'    While GrdArti.Row <= GrdArti.Rows - 1
'        GrdArti.Row = GrdArti.Row + 1
'        If GrdArti.TextMatrix(GrdArti.Row, 1) <> NUL$ Then
'            If GrdArti.TextMatrix(GrdArti.Row, 2) = "" Or IsNumeric(GrdArti.TextMatrix(GrdArti.Row, 2)) = False Then
'                GrdArti.TextMatrix(GrdArti.Row, 2) = "0"
'            End If
'            If GrdArti.TextMatrix(GrdArti.Row, 3) = "" Or IsNumeric(GrdArti.TextMatrix(GrdArti.Row, 3)) = False Then
'                GrdArti.TextMatrix(GrdArti.Row, 3) = "0"
'            End If
'            If GrdArti.TextMatrix(GrdArti.Row, 5) = "" Or IsNumeric(GrdArti.TextMatrix(GrdArti.Row, 5)) = False Then
'                GrdArti.TextMatrix(GrdArti.Row, 5) = "0"
'            End If
'            valor = CDbl(GrdArti.TextMatrix(GrdArti.Row, 2)) * CDbl(GrdArti.TextMatrix(GrdArti.Row, 3))
'            GrdArti.TextMatrix(GrdArti.Row, 4) = Aplicacion.Formatear_Valor(valor)
'            desitem = (CSng(TxtDevo(9).Text) * valor) / 100
'            valor = Round((((valor - desitem - desitemv) * CDbl(GrdArti.TextMatrix(GrdArti.Row, 5))) / 100))
'            GrdArti.TextMatrix(GrdArti.Row, 6) = valor
'            valor = CDbl(GrdArti.TextMatrix(GrdArti.Row, 2)) * CDbl(GrdArti.TextMatrix(GrdArti.Row, 3))
'            valor = Round((((valor - desitem - desitemv) * CDbl(GrdArti.TextMatrix(GrdArti.Row, 5))) / 100)) + (valor - desitem - desitemv)
'            GrdArti.TextMatrix(GrdArti.Row, 9) = Aplicacion.Formatear_Valor(valor)
'        End If
'    Wend
'salto:
'    On Error GoTo 0
'    bruto = sumar_items(GrdArti, 4)
'    If Not IsNumeric(TxtDevo(9).Text) Then TxtDevo(9).Text = "0"
'    If Not IsNumeric(TxtDevo(10).Text) Then TxtDevo(10).Text = "0"
'    descu = Round((CSng(TxtDevo(9).Text) * bruto) / 100)
'    descu = Round(descu + descu2)
'    Impuesto = Round(sumar_items(GrdArti, 6))
'    retef = Round((CSng(TxtDevo(10).Text) * bruto) / 100)
'    neto = bruto - descu + Impuesto - retef
'    TxtDevo(12).Text = Aplicacion.Formatear_Valor(bruto)
'    TxtDevo(13).Text = Aplicacion.Formatear_Valor(descu)
'    TxtDevo(14).Text = Aplicacion.Formatear_Valor(Impuesto)
'    TxtDevo(15).Text = Aplicacion.Formatear_Valor(retef)
'    TxtDevo(16).Text = Aplicacion.Formatear_Valor(neto)
'    '''
'    ''PASO A CONTA
'    valo_cont(1) = bruto
'    valo_cont(2) = descu
'    valo_cont(3) = Impuesto
'    valo_cont(4) = retef
'    valo_cont(5) = neto
'    valo_cont(6) = calcular_costo(GrdArti, 2, 7)
'    valo_cont(7) = valo_cont(6)
'    If Aplicacion.Interfaz_Contabilidad Then Call OLDInterface_ContableDevolucion(Documento.Encabezado.TipDeDcmnt)
'    '''''''''''''''''
'    If Ppto And GrdArti.TextMatrix(1, 0) <> NUL$ Then
''       MatPpto = Articulos_Presupuesto(GrdArti, "NU_COMP_CUEN,NU_CDPE_CUEN", 9, 1)
'        MatPpto = Articulos_Presupuesto(GrdArti, 9, 1)      'DEPURACION DE CODIGO
'    End If
'    GrdArti.Col = columna  ''Recupera la posici�n
'    If Fila > 0 And Fila < GrdArti.Rows Then GrdArti.Row = Fila ''Recupera la posici�n
''    GrdArti.Row = Fila  ''Recupera la posici�n
'End Sub

Public Sub Interface_ContableEntrada(ByVal intTipo As Integer, ByVal IntBodega As Integer)
    Dim Arr(4)
    Dim CtasG(4)
    Dim i As Integer
    'ReDim CtasConta(3, 0)
    ReDim CtasConta(5, 0) 'LDCR T10191
    'CtasConta = Cuentas_Articulos(GrdArti, "CD_ENTR_GRUP", "D")
    CtasConta = Cuentas_Articulos(GrdArti, IIf(boFact, "CD_ENTR_RGB", "CD_TRAN_RGB"), "D", IntBodega)
    Arr(0) = TxtEntra(12): Arr(1) = TxtEntra(18)
    Arr(2) = TxtEntra(19): Arr(3) = TxtEntra(13)
    
    Result = LoadData("PARAMETROS_INVE", "CD_DECO_APLI,CD_FLET_APLI,CD_DEAD_APLI," & IIf(boFact, "CD_CXP_APLI", "CD_CXPT_APLI") & ",CD_DONA_APLI", NUL$, CtasG())
    If Not IsNumeric(TxtEntra(11)) Then TxtEntra(11) = 0
    If CDbl(TxtEntra(11)) > 0 Then
        i = UBound(CtasConta, 2) + 1
        'ReDim Preserve CtasConta(3, I)
        ReDim Preserve CtasConta(5, i) 'LDCR T10396
        CtasConta(0, i) = CtasG(0)
        CtasConta(1, i) = TxtEntra(11)
        CtasConta(2, i) = 0
        CtasConta(3, i) = "C"
    End If
    'CtasConta = Cuentas_Movimiento(intTipo, Arr(), CDbl(TxtEntra(10)) - TxtEntra(11))
    'CtasConta = Cuentas_Movimiento(intTipo, Arr(), CDbl(TxtEntra(10)) - TxtEntra(11), GrdDeIm, GrdArti, IntBodega) 'HRR M1818
    'HRR M1818
    If intTipo = Entrada And (Not Documento.Encabezado.EsIndependiente) Then
      CtasConta = Cuentas_Movimiento_Entrada(intTipo, Arr(), CDbl(TxtEntra(10)) - TxtEntra(11), GrdDeIm, GrdArti, IntBodega)
    Else
      CtasConta = Cuentas_Movimiento(intTipo, Arr(), CDbl(TxtEntra(10)) - TxtEntra(11), GrdDeIm, GrdArti, IntBodega)
    End If
    'HRR M1818
    If Not IsNumeric(TxtEntra(14)) Then TxtEntra(14) = 0
    If CDbl(TxtEntra(14)) > 0 Then
        i = UBound(CtasConta, 2) + 1
        'ReDim Preserve CtasConta(3, I)
        ReDim Preserve CtasConta(5, i) 'LDCR T10191
        CtasConta(0, i) = CtasG(1)
        CtasConta(1, i) = TxtEntra(14)
        CtasConta(2, i) = 0
        CtasConta(3, i) = "D"
        'smdl m1751
        If OptParticular.value = True Then
           i = UBound(CtasConta, 2) + 1
           'ReDim Preserve CtasConta(3, I)
           ReDim Preserve CtasConta(5, i) 'LDCR T10191
           CtasConta(0, i) = ""
           CtasConta(1, i) = TxtEntra(14)
           CtasConta(2, i) = 0
           CtasConta(3, i) = "C"
        End If
        'smdl m1751
    End If
    If CDbl(TxtEntra(20)) > 0 Then
        i = UBound(CtasConta, 2) + 1
        'ReDim Preserve CtasConta(3, I)
        ReDim Preserve CtasConta(5, i) 'LDCR T10191
        CtasConta(0, i) = CtasG(2)
        CtasConta(1, i) = TxtEntra(20)
        CtasConta(2, i) = 0
        CtasConta(3, i) = "C"
    End If
    If CDbl(TxtEntra(15)) > 0 Then
        i = UBound(CtasConta(), 2) + 1
        'ReDim Preserve CtasConta(3, I)
        ReDim Preserve CtasConta(5, i) 'LDCR T10191
        CtasConta(0, i) = IIf(intTipo = AprovechaDonacion, CtasG(4), CtasG(3))
        CtasConta(1, i) = TxtEntra(15)
        CtasConta(2, i) = 0
        CtasConta(3, i) = "C"
    End If
    
    'JLPB T15675 INICIO
    If Screen.ActiveForm.Caption = "DEVOLUCION ENTRADA" Then
       ReDim ArrCxP(0)
       Dim DbDXP As Double
       Result = LoadData("IN_ENCABEZADO", "NU_CXP_ENCA", "NU_AUTO_ENCA=" & AutoDocCargado, ArrCxP)
       If Result <> FAIL And Encontro Then
          If ArrCxP(0) = NUL$ Then GoTo SEGUIR
          DbDXP = ArrCxP(0)
          Result = LoadData("IN_COMPRA", "NU_AUTO_COMP", "NU_CXP_COMP=" & DbDXP, ArrCxP)
          If Result <> FAIL And Encontro Then
             Call DescuImpues(CDbl(DbDXP))
             Dim InI As Integer
             Dim InA As Integer
             InA = UBound(CtasConta(), 2)
             InPosc = UBound(CtasConta(), 2) + 1
             If ArDescComp(1, 0) = NUL$ Then GoTo SEGUIR1
             For InI = 0 To UBound(ArDescComp, 2)
                i = UBound(CtasConta(), 2) + 1
                ReDim Preserve CtasConta(5, i)
                CtasConta(0, i) = ArDescComp(3, InI)
                CtasConta(1, i) = -Val(ArDescComp(4, InI))
                CtasConta(2, i) = 0
                CtasConta(3, i) = "C"
             Next
SEGUIR1:
             If ArImpComp(1, 0) = NUL$ Then GoTo SEGUIR2:
             For InI = 0 To UBound(ArImpComp, 2)
                i = UBound(CtasConta(), 2) + 1
                ReDim Preserve CtasConta(5, i)
                CtasConta(0, i) = ArImpComp(3, InI)
                CtasConta(1, i) = -Val(ArImpComp(4, InI))
                CtasConta(2, i) = 0
                CtasConta(3, i) = "C"
             Next
SEGUIR2:
             For InI = InA + 1 To UBound(CtasConta(), 2)
                CtasConta(1, InA) = CtasConta(1, InA) - CtasConta(1, InI)
             Next

          End If
       End If
    End If
SEGUIR:
   'JLPB T15675 FIN
    
'    Call Cuentas_Contables_Movi(0, CtasConta, txtDocum(5), NUL$)
    If intTipo = DevolucionEntrada Then
        For i = 0 To UBound(CtasConta, 2)
            If CtasConta(3, i) = "D" Then
                CtasConta(3, i) = "C"
            Else
                CtasConta(3, i) = "D"
            End If
        Next
        'JLPB T15675 INICIO EL SIGUIENTE BLOQUE SE DEJA EN COMENTARIO
'         If Not BoConsMov Then 'HRR R1700
'            'PJCA M1077 Cargar los impuestos y descuentos de las entradas a devolver
'            For I = 0 To Me.lstDocsPadre.ListCount - 1
'                Call ImpDesc_DevEntrada(Me.lstDocsPadre.List(I))
'            Next
'            'PJCA M1077
'        End If 'HRR R1700
        'JLPB T15675 FIN
        
    End If
'    Call Cuentas_Contables_Movi(0, CtasConta, Documento.Encabezado.Tercero.Nit, NUL$)
    Call Cuentas_Contables_Movi(CtasConta, Documento.Encabezado.Tercero.Nit, NUL$, "ENTRA")     'DEPURACION DE CODIGO
'    SiMuestra = False
End Sub

'HRR R1801
'Private Sub Interface_ContableSalida()
''Dim Arr(3)     'DEPURACION DE CODIGO
''Dim CtasG(2)   'DEPURACION DE CODIGO
''Dim i As Integer   'DEPURACION DE CODIGO
'ReDim CtasConta(3, 0)
'    'CtasConta = Cuentas_Articulos_Costo(GrdArti, "CD_SALI_GRUP", "CD_GAST_GRUP", "C", Documento.Encabezado.BodegaORIGEN)
'    CtasConta = Cuentas_Articulos_Costo(GrdArti, "CD_SALI_RGB", "CD_GAST_RGB", "C", Documento.Encabezado.BodegaORIGEN)
''    Call Cuentas_Contables_Movi(7, CtasConta, TxtSaliA(4), NUL$)
''    Call Cuentas_Contables_Movi(7, CtasConta, Documento.Encabezado.Tercero.Nit, NUL$)
'    Call Cuentas_Contables_Movi(CtasConta, Documento.Encabezado.Tercero.Nit, NUL$)      'DEPURACION DE CODIGO
'End Sub
'HRR R1801

'DEPURACION DE CODIGO
'Private Sub OLDInterface_ContableVenta(ByVal intTipo As Integer)
'Dim Arr(3)
'Dim CtasG(2)
'Dim i As Integer
'
'ReDim CtasConta(3, 0)
'    'CtasConta = Cuentas_Articulos(GrdArti, "CD_INGR_GRUP", "C")
'    Result = LoadData("PARAMETROS_INVE", "CD_DEVE_APLI,CD_FLET_APLI,CD_CXC_APLI", NUL$, CtasG())
'    If Not IsNumeric(TxtVenta(16)) Then TxtVenta(16) = 0
'    If CDbl(TxtVenta(16)) > 0 Then
'        i = UBound(CtasConta(), 2) + 1
'        ReDim Preserve CtasConta(3, i)
'        CtasConta(0, i) = CtasG(0) 'SE CAMBIO ESTE SUBINDICE QUE ERA UNO PARA QUE LA CUENTA SE ENVIE A LA CUENTA CORRECTA.
'        CtasConta(1, i) = TxtVenta(16)
'        CtasConta(2, i) = 0
'        CtasConta(3, i) = "D"
'    End If
'    Arr(0) = TxtVenta(17): Arr(1) = 0
'    Arr(2) = 0: Arr(3) = TxtVenta(18)
'    CtasConta = Cuentas_Factura(intTipo, Arr(), CCur(TxtVenta(15).Text))
'    If CDbl(TxtVenta(19)) > 0 Then
'        i = UBound(CtasConta(), 2) + 1
'        ReDim Preserve CtasConta(3, i)
'        CtasConta(0, i) = CtasG(1)
'        CtasConta(1, i) = TxtVenta(19).Text
'        CtasConta(2, i) = 0
'        CtasConta(3, i) = "C"
'    End If
'    If CDbl(TxtVenta(20)) > 0 Then
'        i = UBound(CtasConta(), 2) + 1
'        ReDim Preserve CtasConta(3, i)
'        CtasConta(0, i) = CtasG(2)
'        CtasConta(1, i) = TxtVenta(20).Text
'        CtasConta(2, i) = 0
'        CtasConta(3, i) = "D"
'    End If
'    'CtasConta = Cuentas_Articulos_Costo(GrdArti, "CD_SALI_GRUP", "CD_COST_GRUP", "C") 'For I = 0 To UBound(CtasConta(), 2)
''    Call Cuentas_Contables_Movi(2, CtasConta, TxtVenta(8), NUL$)
''    Call Cuentas_Contables_Movi(2, CtasConta, Documento.Encabezado.Tercero.Nit, NUL$)
'    Call Cuentas_Contables_Movi(CtasConta, Documento.Encabezado.Tercero.Nit, NUL$)      'DEPURACION DE CODIGO
''    SiMuestra = False
'End Sub

'Private Sub Interface_ContableVenta(ByVal intTipo As Integer)
Private Sub Interface_ContableVenta(ByVal intTipo As Integer, ByVal IntBodega As Integer)
   'Dim Arr(3)     'DEPURACION DE CODIGO
   'Dim CtasG(2)   'DEPURACION DE CODIGO
   Dim i As Integer, vCon As Integer, vVal As Currency
   'DAHV T6264 - INICIO
   Dim Incont As Integer
   Dim StIvaPorArt As String
   Dim StIvaArt As String
   Dim InI As Integer 'JLPB T41942 'Almacena la ultima posici�n de las cuentas de los articulos
   Dim DbValorArt As Double 'JLPB T41942 'Almacena el total del valor de los articulos por grupo
   'JAUM T39942 Inicio
   Dim VrIva() As Variant 'Almacena cada uno de los valores de los iva aplicados a los articulos
   Dim InJ As Integer 'Contador
   ReDim VrIva(2, 0)
   'JAUM T39942 Fin
   StIvaPorArt = ""
   StIvaPorArt = fnDevDato("PARAMETROS_IMPUESTOS, ENTIDAD", "TX_IVADEDAR_PAIM", "CD_CODI_TERC_PAIM = CD_NIT_ENTI", True)
    
   'DAHV T6264 - FIN
    
    
   'ReDim CtasConta(3, 0)
   ReDim CtasConta(5, 0) 'LDCR T10396
   'CtasConta = Cuentas_Articulos(GrdArti, "CD_INGR_GRUP", IIf((intTipo = FacturaVenta), "C", "D"))
   CtasConta = Cuentas_Articulos(GrdArti, "CD_INGR_RGB", IIf((intTipo = FacturaVenta), "C", "D"), IntBodega)
   InI = UBound(CtasConta, 2) 'JLPB T41942
   
   ' DAHV T1565 - INICIO
   ' If intTipo = DevolucionFacturaVenta Then grdFormaPago.TextMatrix(1, 2) = FormatCurrency(CtasConta(1, I))    'REOL M2228
   If intTipo = DevolucionFacturaVenta Then
      If grdFormaPago.Rows = 1 Then
         grdFormaPago.Rows = 2
         grdFormaPago.TextMatrix(1, 2) = FormatCurrency(CtasConta(1, i))
      Else
         grdFormaPago.TextMatrix(1, 2) = FormatCurrency(CtasConta(1, i))
      End If
   End If
   'DAHV T1565 - INICIO
    
   'If intTipo = DevolucionFacturaVenta Then grdFormaPago.TextMatrix(0, 2) = FormatCurrency(CtasConta(1, I))    'GAVL  M1565
   'APGR T1565 PNC - INICIO SE DEVUELVE CAMBIOS HECHOS POR GAVL
       'INICIO  GAVL M1565
   'If lstDocsPadre.Visible = True And lstDocsPadre.ListCount > 0 Then
   '  If intTipo = DevolucionFacturaVenta Then grdFormaPago.TextMatrix(1, 2) = FormatCurrency(CtasConta(1, I))
   'Else
   '  Call Mensaje1("No se puede generar la devolucion de factura sin tener un documento", 3)
   'End If
   'FIN GAVL M1565
   'APGR T1565 PNC - FIN
   If Me.GrdArticulos.Rows > 1 Then
      For vCon = 0 To UBound(itmCuentasIVA, 2)
         itmCuentasIVA(2, vCon) = 0
      Next
      For vCon = 1 To Me.GrdArticulos.Rows - 1
         If IsNumeric(Me.GrdArticulos.TextMatrix(vCon, Me.ColCantidad)) _
         And IsNumeric(Me.GrdArticulos.TextMatrix(vCon, Me.ColVenUndConDes)) _
         And IsNumeric(Me.GrdArticulos.TextMatrix(vCon, Me.ColPorceIVA)) _
         And IsNumeric(Me.GrdArticulos.TextMatrix(vCon, Me.ColDivisor)) _
         And IsNumeric(Me.GrdArticulos.TextMatrix(vCon, Me.ColMultiplicador)) Then
            'vVal = CCur(CInt(Me.GrdArticulos.TextMatrix(vCon, Me.ColCantidad)) * CInt(Me.GrdArticulos.TextMatrix(vCon, Me.ColMultiplicador)) * CCur(Me.GrdArticulos.TextMatrix(vCon, Me.ColVenUndConDes)) / CInt(Me.GrdArticulos.TextMatrix(vCon, Me.ColDivisor))) - CCur(100 * CInt(Me.GrdArticulos.TextMatrix(vCon, Me.ColCantidad)) * CInt(Me.GrdArticulos.TextMatrix(vCon, Me.ColMultiplicador)) * CCur(Me.GrdArticulos.TextMatrix(vCon, Me.ColVenUndConDes)) / (CInt(Me.GrdArticulos.TextMatrix(vCon, Me.ColDivisor)) * (100 + CCur(Me.GrdArticulos.TextMatrix(vCon, Me.ColPorceIVA)))))
            'DAHV M3930 Estaba calculando el valor del impuesto pero sobre el valor de venta con el descuento,
            'se adiciona la condici�n para que el cambio se afecte siempre y cuando el tipo de documento sea Factura de Venta.
            If intTipo = FacturaVenta Then
               'DAHV Se devuelven los cambios
               'vVal = CCur(CInt(Me.GrdArticulos.TextMatrix(vCon, Me.ColCantidad)) * CInt(Me.GrdArticulos.TextMatrix(vCon, Me.ColMultiplicador)) * CCur(Me.GrdArticulos.TextMatrix(vCon, Me.ColVentaTotal)) / CInt(Me.GrdArticulos.TextMatrix(vCon, Me.ColDivisor))) - CCur(100 * CInt(Me.GrdArticulos.TextMatrix(vCon, Me.ColCantidad)) * CInt(Me.GrdArticulos.TextMatrix(vCon, Me.ColMultiplicador)) * CCur(Me.GrdArticulos.TextMatrix(vCon, Me.ColVentaTotal)) / (CInt(Me.GrdArticulos.TextMatrix(vCon, Me.ColDivisor)) * (100 + CCur(Me.GrdArticulos.TextMatrix(vCon, Me.ColPorceIVA))))) 'APGR T4022 -Se deja en comentario
               'vVal = CCur(CDbl(Me.GrdArticulos.TextMatrix(vCon, Me.ColCantidad)) * CDbl(Me.GrdArticulos.TextMatrix(vCon, Me.ColMultiplicador)) * CCur(Me.GrdArticulos.TextMatrix(vCon, Me.ColVentaTotal)) / CDbl(Me.GrdArticulos.TextMatrix(vCon, Me.ColDivisor))) - CCur(100 * CDbl(Me.GrdArticulos.TextMatrix(vCon, Me.ColCantidad)) * CDbl(Me.GrdArticulos.TextMatrix(vCon, Me.ColMultiplicador)) * CCur(Me.GrdArticulos.TextMatrix(vCon, Me.ColVentaTotal)) / (CDbl(Me.GrdArticulos.TextMatrix(vCon, Me.ColDivisor)) * (100 + CCur(Me.GrdArticulos.TextMatrix(vCon, Me.ColPorceIVA))))) 'HRR M5080
               'vVal = CCur(CDbl(Me.GrdArticulos.TextMatrix(vCon, Me.ColCantidad)) * CDbl(Me.GrdArticulos.TextMatrix(vCon, Me.ColMultiplicador)) * CCur(Me.GrdArticulos.TextMatrix(vCon, Me.ColVentaTotal)) / CDbl(Me.GrdArticulos.TextMatrix(vCon, Me.ColDivisor))) - CCur(100 * CDbl(Me.GrdArticulos.TextMatrix(vCon, Me.ColCantidad)) * CDbl(Me.GrdArticulos.TextMatrix(vCon, Me.ColMultiplicador)) * CCur(Me.GrdArticulos.TextMatrix(vCon, Me.ColVentaTotal)) / (CDbl(Me.GrdArticulos.TextMatrix(vCon, Me.ColDivisor)) * (100 + CCur(Me.GrdArticulos.TextMatrix(vCon, Me.ColPorceIVA))))) 'APGR T4022 'JAUM T39942 Se deja linea en comentario segun indicacion de directora de proyecto debido a que en la factura de venta no se debe tener en cuenta la unidosis y que la operacion genera desbalance contable
               vVal = CCur(CDbl(Me.GrdArticulos.TextMatrix(vCon, Me.ColCantidad)) * CDbl(Me.GrdArticulos.TextMatrix(vCon, Me.ColVentaSinIVA)) * CDbl(Me.GrdArticulos.TextMatrix(vCon, Me.ColPorceIVA) / 100)) 'JAUM T39942
               'DAHV Se devuelven los cambios
            Else
               vVal = CCur(CInt(Me.GrdArticulos.TextMatrix(vCon, Me.ColCantidad)) * CInt(Me.GrdArticulos.TextMatrix(vCon, Me.ColMultiplicador)) * CCur(Me.GrdArticulos.TextMatrix(vCon, Me.ColVenUndConDes)) / CInt(Me.GrdArticulos.TextMatrix(vCon, Me.ColDivisor))) - CCur(100 * CInt(Me.GrdArticulos.TextMatrix(vCon, Me.ColCantidad)) * CInt(Me.GrdArticulos.TextMatrix(vCon, Me.ColMultiplicador)) * CCur(Me.GrdArticulos.TextMatrix(vCon, Me.ColVenUndConDes)) / (CInt(Me.GrdArticulos.TextMatrix(vCon, Me.ColDivisor)) * (100 + CCur(Me.GrdArticulos.TextMatrix(vCon, Me.ColPorceIVA)))))
            End If
            If vVal > 0 Then
               i = FindInArrM(itmCuentasIVA, Me.GrdArticulos.TextMatrix(vCon, Me.ColCodigoImpuesto))
               If i > -1 Then itmCuentasIVA(2, i) = itmCuentasIVA(2, i) + vVal
               'JAUM T39942 Inicio
               If intTipo = FacturaVenta Then
                  If i > -1 Then
                     If InJ <> 0 Then ReDim Preserve VrIva(2, InJ)
                     VrIva(0, InJ) = GrdArti.TextMatrix(vCon, 0)
                     VrIva(1, InJ) = itmCuentasIVA(0, i)
                     VrIva(2, InJ) = Formatear_Valor(vVal)
                     InJ = InJ + 1
                  End If
               End If
               'JAUM T39942 Fin
            End If
         End If
      Next
   End If
   'For vCon = 0 To UBound(itmCuentasIVA, 2)
   '    If IsNumeric(itmCuentasIVA(2, vCon)) Then
   '        vVal = CCur(itmCuentasIVA(2, vCon))
   '        If vVal > 0 Then
   '            I = UBound(CtasConta(), 2) + 1
   '            ReDim Preserve CtasConta(3, I)
   '            CtasConta(0, I) = itmCuentasIVA(1, vCon)
   '            CtasConta(1, I) = FormatCurrency(vVal, 2)
   '            CtasConta(2, I) = 0
   '            CtasConta(3, I) = IIf((intTipo = FacturaVenta), "C", "D")
   '        End If
   '    End If
   'Next
   'SMD M1658
    
   For vCon = 0 To UBound(itmCuentasIVA, 2)
      If IsNumeric(itmCuentasIVA(2, vCon)) Then
         vVal = CCur(itmCuentasIVA(2, vCon))
         If vVal > 0 Then
            'DAHV T6264 - INICIO
            'If IVADeducible() Then
            '   I = UBound(CtasConta(), 2) + 1
            '   ReDim Preserve CtasConta(3, I)
            '   CtasConta(0, I) = itmCuentasIVA(1, vCon)
            '   CtasConta(1, I) = FormatCurrency(vVal, 2)
            '   CtasConta(2, I) = 0
            '   CtasConta(3, I) = IIf((intTipo = FacturaVenta), "C", "D")
            'Else
            '   I = UBound(CtasConta(), 2)
            '   CtasConta(1, I) = Round((CtasConta(1, I) + vVal)) ' smdl m1779 CDbl por round
            'End If
            If IVADeducible() Then
               If StIvaPorArt = "S" Then
                  With GrdArti
                     For Incont = 1 To .Rows - 1
                        StIvaArt = fnDevDato("ARTICULO", "TX_IVADED_ARTI", "CD_CODI_ARTI=" & Comi & .TextMatrix(Incont, 0) & Comi, True)
                        If StIvaArt = "1" Then
                           i = UBound(CtasConta(), 2) + 1
                           'ReDim Preserve CtasConta(3, I)
                           ReDim Preserve CtasConta(5, i) 'LDCR T10396
                           CtasConta(0, i) = itmCuentasIVA(1, vCon)
                           'JAUM T39942 Inicio
                           If intTipo = FacturaVenta Then
                              If UBound(VrIva, 2) > 0 Then
                                 For InJ = 0 To UBound(VrIva, 2)
                                    If VrIva(0, InJ) = .TextMatrix(Incont, 0) And VrIva(1, InJ) = itmCuentasIVA(0, vCon) Then
                                       CtasConta(1, i) = VrIva(2, InJ)
                                       Exit For
                                    End If
                                 Next
                              Else
                                 CtasConta(1, i) = FormatCurrency(vVal, 2)
                              End If
                           Else
                           'JAUM T39942 Fin
                              CtasConta(1, i) = FormatCurrency(vVal, 2)
                           End If 'JAUM T39942
                           CtasConta(2, i) = 0
                           CtasConta(3, i) = IIf((intTipo = FacturaVenta), "C", "D")
                        Else
                           i = UBound(CtasConta(), 2)
                           CtasConta(1, i) = Round((CtasConta(1, i) + vVal))
                        End If
                     Next Incont
                  End With
               Else
                  i = UBound(CtasConta(), 2) + 1
                  'ReDim Preserve CtasConta(3, I)
                  ReDim Preserve CtasConta(5, i) 'LDCR T10396
                  CtasConta(0, i) = itmCuentasIVA(1, vCon)
                  CtasConta(1, i) = FormatCurrency(vVal, 2)
                  CtasConta(2, i) = 0
                  CtasConta(3, i) = IIf((intTipo = FacturaVenta), "C", "D")
               End If
            Else
               i = UBound(CtasConta(), 2)
               CtasConta(1, i) = Round((CtasConta(1, i) + vVal))
            End If
            'DAHV T6264 - FIN
         End If
      End If
   Next
   'SMD M1658
   If Me.grdOtrosDescuentos.Rows > 1 Then
      For vCon = 1 To Me.grdOtrosDescuentos.Rows - 1
         If IsNumeric(Me.grdOtrosDescuentos.TextMatrix(vCon, 2)) Then
            vVal = CCur(Me.grdOtrosDescuentos.TextMatrix(vCon, 2))
            If vVal > 0 Then
               i = UBound(CtasConta(), 2) + 1
               'ReDim Preserve CtasConta(3, I)
               ReDim Preserve CtasConta(5, i) 'LDCR T10396
               'If Me.grdOtrosDescuentos.Rows = -1 Then 'SMDL M1144, 1143
               indi = FindInArrM(itmCuentasOtrosDescuentos, Me.grdOtrosDescuentos.TextMatrix(vCon, 0))
               If indi <> -1 Then
                  CtasConta(0, i) = itmCuentasOtrosDescuentos(1, indi) 'SMDL M1144
               End If
               CtasConta(1, i) = FormatCurrency(CCur(Me.grdOtrosDescuentos.TextMatrix(vCon, 2)), 2)
               CtasConta(2, i) = 0
               CtasConta(3, i) = IIf((intTipo = FacturaVenta), "D", "C")
               'End If
            End If
         End If
      Next
   End If
   'arr(0) = TxtVenta(17): arr(1) = 0
   'arr(2) = 0: arr(3) = TxtVenta(18)
   'CtasConta = Cuentas_Factura(intTipo, arr(), CDbl(TxtVenta(15)))
   If Me.grdOtrosCargos.Rows > 1 Then
      For vCon = 1 To Me.grdOtrosCargos.Rows - 1
         If IsNumeric(Me.grdOtrosCargos.TextMatrix(vCon, 2)) Then
            vVal = CCur(Me.grdOtrosCargos.TextMatrix(vCon, 2))
            If vVal > 0 Then
               i = UBound(CtasConta(), 2) + 1
               'ReDim Preserve CtasConta(3, I)
               ReDim Preserve CtasConta(5, i) 'LDCR T10396
               CtasConta(0, i) = itmCuentasOtrosCargos(1, FindInArrM(itmCuentasOtrosCargos, Me.grdOtrosCargos.TextMatrix(vCon, 0)))
               CtasConta(1, i) = FormatCurrency(CCur(Me.grdOtrosCargos.TextMatrix(vCon, 2)), 2)
               CtasConta(2, i) = 0
               CtasConta(3, i) = IIf((intTipo = FacturaVenta), "C", "D")
            End If
         End If
      Next
   End If
   Me.TxtVenta(15).Text = "0"
   Me.TxtVenta(20).Text = "0"
   If Me.grdFormaPago.Rows > 1 Then
      For vCon = 1 To Me.grdFormaPago.Rows - 1
         If IsNumeric(Me.grdFormaPago.TextMatrix(vCon, 2)) Then
            vVal = CCur(Me.grdFormaPago.TextMatrix(vCon, 2))
            If vVal > 0 Then
               i = UBound(CtasConta(), 2) + 1
               'ReDim Preserve CtasConta(3, I)
               ReDim Preserve CtasConta(5, i) 'LDCR T10396
               'CtasConta(0, i) = itmCuentasFormas(1, FindInArrM(itmCuentasFormas, Me.grdFormaPago.TextMatrix(vCon, 0)))
               'CtasConta(0, i) = BuscarCuenta("PARAMETROS_INVE", "CD_CXC_APLI", "")        'SMD M1658 'HRR M3150
               If Me.grdFormaPago.TextMatrix(vCon, 0) <> NUL$ Then
                   CtasConta(0, i) = itmCuentasFormas(1, FindInArrM(itmCuentasFormas, Me.grdFormaPago.TextMatrix(vCon, 0))) 'HRR M3150
               Else
                   CtasConta(0, i) = NUL$
               End If
               vVal = CCur(Me.grdFormaPago.TextMatrix(vCon, 2))
               CtasConta(1, i) = FormatCurrency(vVal, 2)
               If IsNumeric(Me.grdFormaPago.TextMatrix(vCon, 0)) Then
                   If CInt(Me.grdFormaPago.TextMatrix(vCon, 0)) = vCodigoFormaCredito Then Me.TxtVenta(20).Text = CCur(Me.TxtVenta(20).Text) + vVal
               End If
               CtasConta(2, i) = 0
               CtasConta(3, i) = IIf((intTipo = FacturaVenta), "D", "C")
            End If
         End If
      Next
   End If
   'GSCD T45839 INICIO
   If intTipo = FacturaVenta Then
      If WinDoc.DbDescfac > 0 Then
         i = UBound(CtasConta(), 2) + 1
         ReDim Preserve CtasConta(5, i)
         CtasConta(0, i) = fnDevDato("PARAMETROS_INVE", "CD_DEVE_APLI", NUL$, True)
         CtasConta(1, i) = WinDoc.DbDescfac
         CtasConta(2, i) = 0
         CtasConta(3, i) = "D"
      End If
   End If
   'GSCD T45839 FIN
   Me.TxtVenta(15).Text = Me.TxtVenta(20).Text
   'CtasConta = Cuentas_Articulos_Costo(GrdArti, "CD_SALI_GRUP", "CD_COST_GRUP", IIf((intTipo = FacturaVenta), "C", "D")) 'For I = 0 To UBound(CtasConta(), 2)
   'DAHV T8058 - INICIO
   For i = 0 To UBound(CtasConta(), 2) - 1
      If Abs(CDbl(txtSUBTO.Text) - CtasConta(1, i)) = 1 Then
         CtasConta(1, i) = CDbl(txtSUBTO.Text)
      End If
   Next i
   'DAHV T8058 - FIN
   'JLPB T41942 INICIO
   DbValorArt = 0
   For i = 0 To InI
      DbValorArt = DbValorArt + CtasConta(1, i)
   Next
   If WinDoc.ElConsecutivo.AutoDocu = 11 Then
      If DbValorArt < WinDoc.Calcular_Documento(Me.ColCantidad, Me.ColVentaSinIVA) Then
         DbValorArt = WinDoc.Calcular_Documento(Me.ColCantidad, Me.ColVentaSinIVA) - DbValorArt
         CtasConta(1, 0) = CtasConta(1, 0) + DbValorArt
      End If
   End If
   'JLPB T41942 FIN
   CtasConta = Cuentas_Articulos_Costo(GrdArti, "CD_SALI_RGB", "CD_COST_RGB", IIf((intTipo = FacturaVenta), "C", "D"), IntBodega)
   'COSTO "D", INVENTARIO "C"
   'Call Cuentas_Contables_Movi(2, CtasConta, TxtVenta(8), NUL$)
   'Call Cuentas_Contables_Movi(2, CtasConta, Documento.Encabezado.Tercero.Nit, NUL$)
   Call Cuentas_Contables_Movi(CtasConta, Documento.Encabezado.Tercero.Nit, NUL$)      'DEPURACION DE CODIGO
   'SiMuestra = False
End Sub

Private Sub OLDInterface_ContableDevolucion(ByVal intTipo As Integer)
Dim Arr(3)
Dim CtasG(1)
Dim i As Integer
'ReDim CtasConta(3, 0)
ReDim CtasConta(5, 0) 'LDCR T10396
    'CtasConta = Cuentas_Articulos(GrdArti, "CD_INGR_GRUP", "D")
    Result = LoadData("PARAMETROS_INVE", "CD_DEVE_APLI,CD_CXC_APLI", NUL$, CtasG())
    If Not IsNumeric(TxtDevo(13)) Then TxtDevo(13) = 0
    If CDbl(TxtDevo(13)) > 0 Then 'descuentos
        i = UBound(CtasConta(), 2) + 1
        'ReDim Preserve CtasConta(3, I)
        ReDim Preserve CtasConta(5, i) 'LDCR T10396
        CtasConta(0, i) = CtasG(0)
        CtasConta(1, i) = TxtDevo(13)
        CtasConta(2, i) = 0
        CtasConta(3, i) = "C"
    End If
    Arr(0) = TxtDevo(14) 'iva
    Arr(1) = 0
    Arr(2) = 0
    Arr(3) = TxtDevo(15) 'retefuente
    CtasConta = Cuentas_Movimiento(intTipo, Arr(), CDbl(TxtDevo(12)))
    If CDbl(TxtDevo(16)) > 0 Then
        i = UBound(CtasConta(), 2) + 1
        'ReDim Preserve CtasConta(3, I)
        ReDim Preserve CtasConta(5, i) 'LDCR T10396
        CtasConta(0, i) = CtasG(1)
        CtasConta(1, i) = TxtDevo(16)
        CtasConta(2, i) = 0
        CtasConta(3, i) = "C"
    End If
    'CtasConta = Cuentas_Articulos_Costo(GrdArti, "CD_SALI_GRUP", "CD_COST_GRUP", "D") 'For I = 0 To UBound(CtasConta(), 2)
'    Call Cuentas_Contables_Movi(3, CtasConta, TxtDevo(5), NUL$)
'    Call Cuentas_Contables_Movi(3, CtasConta, Documento.Encabezado.Tercero.Nit, NUL$)
    Call Cuentas_Contables_Movi(CtasConta, Documento.Encabezado.Tercero.Nit, NUL$)        'DEPURACION DE CODIGO
'    SiMuestra = False
End Sub

Private Sub Interface_ContableBajaConsumo(ByVal intTipo As Integer)
Dim Arr() As Variant
'Dim CtasG(2)   'DEPURACION DE CODIGO
'Dim i As Integer, vDpndc As String     'DEPURACION DE CODIGO
Dim i As Integer
'ReDim CtasConta(3, 0)
ReDim CtasConta(5, 0) 'LDCR T10396
    ReDim Arr(3)
    'CtasConta = Cuentas_Articulos_Dependencia(GrdArti, "CD_SALI_RGD", "CD_COST_RGD", "C", Me.TxtBajaD(4).Text) 'HRR R1801
    'HRR R1801
    Select Case intTipo
      Case Salida
         CtasConta = Cuentas_Articulos_Dependencia(GrdArti, "CD_SALI_RGD", "CD_GAST_RGD", "C", Me.TxtBajaD(4).Text)
      Case Else
         CtasConta = Cuentas_Articulos_Dependencia(GrdArti, "CD_SALI_RGD", "CD_COST_RGD", "C", Me.TxtBajaD(4).Text)
      
    End Select
    'HRR R1801
    
'//////////
    If intTipo = DevolucionBaja Then
        For i = 0 To UBound(CtasConta, 2)
            If CtasConta(3, i) = "D" Then
                CtasConta(3, i) = "C"
            Else
                CtasConta(3, i) = "D"
            End If
        Next
    End If
'///////////
'Call Cuentas_Contables_Movi(5, CtasConta, Documento.Encabezado.Tercero.Nit, TxtBajaD(4))
Call Cuentas_Contables_Movi(CtasConta, Documento.Encabezado.Tercero.Nit, TxtBajaD(4))       'DEPURACION DE CODIGO
End Sub

Private Sub CentraMarco(ByRef Mar As VB.Frame)
    Mar.Top = (Me.Height - Mar.Height) / 2
    Mar.Left = (Me.Width - Mar.Width) / 2
End Sub

Private Sub GuardaValoresParticulatres()
'    Dim Fec As String, vCon As Integer, vVal As Variant, vPos As Integer   'DEPURACION DE CODIGO
    Dim vCon As Integer, vVal As Variant, vPos As Integer
    Select Case Documento.Encabezado.TipDeDcmnt
    Case Is = Entrada
'Tabla: IN_PARTICULAR
'1            PORDES       PORCENTAJE DE DESCUENTO                            3
'2            RETFUE       PORCENTAJE DE RETENCION EN LA FUENTE               3
'3            RETIVA       PORCENTAJE DE RETENCION DE IVA                     3
'4            RETICA       PORCENTAJE DE RETENCION DE ICA                     3
'5            FLETES       COSTO DE LOS FLETES                                3
'6            DESADI       PORCENTAJE DE DESCUENTO ADICIONAL                  3
'7            PROPAR       PAGA FLETES PROVEDOR/PARTICULAR                    3
'8            FACTUR       NUMERO DE FACTURA                                  3
        If IsNumeric(Me.TxtEntra(7).Text) Then Call InsertaValoresParticulares("1", CCur(Me.TxtEntra(7).Text), "A")
        If IsNumeric(Me.TxtEntra(8).Text) Then Call InsertaValoresParticulares("2", CCur(Me.TxtEntra(8).Text), "A")
        If IsNumeric(Me.TxtEntra(16).Text) Then Call InsertaValoresParticulares("3", CCur(Me.TxtEntra(16).Text), "A")
        If IsNumeric(Me.TxtEntra(17).Text) Then Call InsertaValoresParticulares("4", CCur(Me.TxtEntra(17).Text), "A")
        If IsNumeric(Me.TxtEntra(14).Text) Then Call InsertaValoresParticulares("5", CCur(Me.TxtEntra(14).Text), "A")
        If IsNumeric(Me.TxtEntra(20).Text) Then Call InsertaValoresParticulares("6", CCur(Me.TxtEntra(20).Text), "A")
        If Me.OptParticular.value Then Call InsertaValoresParticulares("7", 1, "A")
        'If IsNumeric(Me.TxtCompra(10).Text) Then Call InsertaValoresParticulares("8", CCur(Me.TxtCompra(10).Text), "A")
        If Me.TxtCompra(10).Text <> NUL$ Then Call InsertaValoresParticularestr("8", CStr(Me.TxtCompra(10).Text), "A") 'JACC R2221
        
    Case Is = FacturaVenta
        For vCon = 0 To UBound(itmCuentasIVA, 2)
                If CSng(itmCuentasIVA(2, vCon)) > 0 Then
                    Call InsertaValoresParticulares(itmCuentasIVA(0, vCon), itmCuentasIVA(2, vCon), "I")
                End If
        Next

        For vCon = 1 To Me.grdFormaPago.Rows - 1
            vVal = 0: vPos = -1
            If IsNumeric(Me.grdFormaPago.TextMatrix(vCon, 2)) Then
                vVal = CCur(Me.grdFormaPago.TextMatrix(vCon, 2))
            End If
            vPos = FindInArrM(itmCuentasFormas, Me.grdFormaPago.TextMatrix(vCon, 0)) 'aqui
            If vVal > 0 And vPos > -1 Then
                Call InsertaValoresParticulares(itmCuentasFormas(0, vPos), vVal, "F")
            End If
        Next

        For vCon = 1 To Me.grdOtrosCargos.Rows - 1
            vVal = 0: vPos = -1
            If IsNumeric(Me.grdOtrosCargos.TextMatrix(vCon, 2)) Then
                vVal = CCur(Me.grdOtrosCargos.TextMatrix(vCon, 2))
            End If
            vPos = FindInArrM(itmCuentasOtrosCargos, Me.grdOtrosCargos.TextMatrix(vCon, 0))
            If vVal > 0 And vPos > -1 Then
                Call InsertaValoresParticulares(itmCuentasOtrosCargos(0, vPos), vVal, "O")
            End If
        Next

        For vCon = 1 To Me.grdOtrosDescuentos.Rows - 1
            vVal = 0: vPos = -1
            If IsNumeric(Me.grdOtrosDescuentos.TextMatrix(vCon, 2)) Then
                vVal = CCur(Me.grdOtrosDescuentos.TextMatrix(vCon, 2))
            End If
            vPos = FindInArrM(itmCuentasOtrosDescuentos, Me.grdOtrosDescuentos.TextMatrix(vCon, 0))
            If vVal > 0 And vPos > -1 Then
                Call InsertaValoresParticulares(itmCuentasOtrosDescuentos(0, vPos), vVal, "D")
            End If
        Next
    End Select
End Sub

Private Sub CargaValoresParticulatres()
    Dim ArrPRT()
    Dim vQaz As Integer, vCon As Integer, vPos As Integer, vEnc As Boolean
    If (Documento.Encabezado.TipDeDcmnt = FacturaVenta) Or (Documento.Encabezado.TipDeDcmnt = DevolucionFacturaVenta) Then
''////////////
        ReDim itmCuentasIVA(4, 0)
        Result = LoadMulData("TC_IMPUESTOS", "CD_CODI_IMPU, CD_CUEN_IMPU, 0, DE_NOMB_IMPU, PR_PORC_IMPU", "ID_TIPO_IMPU='I'", itmCuentasIVA)
        ReDim itmCuentasFormas(2, 0)
        Result = LoadMulData("FORMA_PAGO", "NU_NUME_FOPA, CD_CUEN_FOPA, DE_DESC_FOPA", "", itmCuentasFormas)
        ReDim itmCuentasOtrosCargos(3, 0)
        Result = LoadMulData("TC_IMPUESTOS", "CD_CODI_IMPU, CD_CUEN_IMPU, DE_NOMB_IMPU, PR_PORC_IMPU", "ID_TIPO_IMPU='O'", itmCuentasOtrosCargos)
        ReDim itmCuentasOtrosDescuentos(3, 0)
        Result = LoadMulData("DESCUENTO", "CD_CODI_DESC, CD_CUEN_DESC, DE_NOMB_DESC, PR_PORC_DESC", "", itmCuentasOtrosDescuentos)
        
        Me.grdFormaPago.Rows = 1
        Me.grdOtrosCargos.Rows = 1
        Me.grdOtrosDescuentos.Rows = 1
''////////////
    End If
    Campos = "TX_CODI_PART_PRPR, NU_VALO_PRPR, TX_CUAL_PRPR"
    Desde = "IN_PARTIPARTI"
    Condi = "NU_AUTO_ENCA_PRPR=" & Documento.Encabezado.AutoDelDocCARGADO
'    Condi = Condi & " AND TX_CUAL_PART='A'"
    ReDim ArrPRT(2, 0)
    Result = LoadMulData(Desde, Campos, Condi, ArrPRT())
    If Result = FAIL Then Exit Sub
    If Not Encontro Then Exit Sub
    
    Select Case Documento.Encabezado.TipDeDcmnt
    Case Is = Entrada, DevolucionEntrada
        For vQaz = 0 To UBound(ArrPRT, 2)
            If ArrPRT(2, vQaz) = "A" Then
                Select Case ArrPRT(0, vQaz)
                Case Is = "1"
'                    Me.TxtEntra(7).Text = Formatear_Porcentaje(ArrPRT(1, vQaz))    'REOL M2235
                    Me.TxtEntra(7).Text = ArrPRT(1, vQaz)
                Case Is = "2"
'                    Me.TxtEntra(8).Text = Formatear_Porcentaje(ArrPRT(1, vQaz))    'REOL M2235
                    Me.TxtEntra(8).Text = ArrPRT(1, vQaz)
                Case Is = "3"
'                    Me.TxtEntra(16).Text = Formatear_Porcentaje(ArrPRT(1, vQaz))   'REOL M2235
                    Me.TxtEntra(16).Text = ArrPRT(1, vQaz)
                Case Is = "4"
'                    Me.TxtEntra(17).Text = Formatear_Porcentaje(ArrPRT(1, vQaz))   'REOL M2235
                    Me.TxtEntra(17).Text = ArrPRT(1, vQaz)
                Case Is = "5"
                    If (Documento.Encabezado.TipDeDcmnt <> DevolucionEntrada) Then
                        Me.TxtEntra(14).Text = Formatear_Valor(ArrPRT(1, vQaz))
                    End If
                Case Is = "6"
                    Me.TxtEntra(20).Text = Formatear_Porcentaje(ArrPRT(1, vQaz))
                Case Is = "7"
                    Me.OptParticular.value = (Not CInt(ArrPRT(1, vQaz)) = 0)
                Case Is = "8"
                   ' Me.TxtCompra(10).Text = Formatear_Cantidad(ArrPRT(1, vQaz))
                   Me.TxtCompra(10).Text = ArrPRT(1, vQaz)  'JACC R2221
                End Select
            End If
        Next
    Case Is = FacturaVenta, DevolucionFacturaVenta
        For vQaz = 0 To UBound(ArrPRT, 2)
            If Not ArrPRT(2, vQaz) = "A" Then
                vCon = 1
                vEnc = False
                    Select Case ArrPRT(2, vQaz)
                    Case "I"
                        vPos = FindInArrM(itmCuentasIVA, ArrPRT(0, vQaz), 0)
                        vCon = 1
                    Case "F"
                        vPos = FindInArrM(itmCuentasFormas, ArrPRT(0, vQaz), 0)
                        vCon = 2
                    Case "O"
                        vPos = FindInArrM(itmCuentasOtrosCargos, ArrPRT(0, vQaz), 0)
                        vCon = 3
                    Case "D"
                        vPos = FindInArrM(itmCuentasOtrosDescuentos, ArrPRT(0, vQaz), 0)
                        vCon = 4
                    End Select
                    If vPos > -1 Then vEnc = True
                
                If vEnc And (Not vCon > 4) And vPos > -1 Then
                    Select Case vCon
                    Case 1
                    Case 2
                        With Me.grdFormaPago
                            .Rows = .Rows + 1
                            .Row = .Rows - 1
                            .TextMatrix(.Row, 0) = itmCuentasFormas(0, vPos)
                            .TextMatrix(.Row, 1) = itmCuentasFormas(2, vPos)
                            .TextMatrix(.Row, 2) = FormatCurrency(CSng(ArrPRT(1, vQaz)), 2)
                        End With
                    Case 3
                        With Me.grdOtrosCargos
                            .Rows = .Rows + 1
                            .Row = .Rows - 1
                            .TextMatrix(.Row, 0) = itmCuentasOtrosCargos(0, vPos)
                            .TextMatrix(.Row, 1) = itmCuentasOtrosCargos(2, vPos)
                            .TextMatrix(.Row, 2) = FormatCurrency(CSng(ArrPRT(1, vQaz)), 2)
                        End With
                    Case 4
                        With Me.grdOtrosDescuentos
                            .Rows = .Rows + 1
                            .Row = .Rows - 1
                            .TextMatrix(.Row, 0) = itmCuentasOtrosDescuentos(0, vPos)
                            .TextMatrix(.Row, 1) = itmCuentasOtrosDescuentos(2, vPos)
                            .TextMatrix(.Row, 2) = FormatCurrency(CSng(ArrPRT(1, vQaz)), 2)
                        End With
                    End Select
                End If
            End If
        Next
'''////////////
    Case Is = DevolucionFacturaVenta
    End Select
End Sub

Private Sub InsertaValoresParticulares(ByVal tipo As String, ByVal Valo As Currency, Cual As String)
    Dim Cuer As String
    If Valo <> 0 Then
        Cuer = "NU_AUTO_ENCA_PRPR='" & Documento.Encabezado.AutoDelDocCARGADO & "', TX_CODI_PART_PRPR='" & tipo & "', NU_VALO_PRPR='" & Valo & "', TX_CUAL_PRPR='" & Cual & "'"
        Result = DoInsertSQL("IN_PARTIPARTI", Cuer)
    End If
End Sub
'---------------------------------------------------------------------------------------
' Procedure : InsertaValoresParticularestr
' DateTime  : 26/10/2009 16:53
' Author    : jeison_camacho
' Purpose   :
'---------------------------------------------------------------------------------------
' JACC R2221
Private Sub InsertaValoresParticularestr(ByVal tipo As String, ByVal Valo As String, Cual As String)
    Dim Cuer As String
    If Valo <> "0" And Valo <> NUL$ Then
        Cuer = "NU_AUTO_ENCA_PRPR='" & Documento.Encabezado.AutoDelDocCARGADO & "', TX_CODI_PART_PRPR='" & tipo & "', NU_VALO_PRPR='" & Valo & "', TX_CUAL_PRPR='" & Cual & "'"
        Result = DoInsertSQL("IN_PARTIPARTI", Cuer)
    End If
End Sub

'DEPURACION DE CODIGO
'El concepto al cual env�a la cxp en tesorer�a es CINV, por lo cual debe estar creado con anticipaci�n.
'Private Sub Graba_cxp(ByVal fechag As Variant, ByVal fechav As Variant, Cepto As String, Umero As Long)
'    Valores = "CD_CONCE_CXP=" & Comi & Cepto & Comi & Coma
'    Valores = Valores & "CD_NUME_CXP=" & Umero & Coma
'    Valores = Valores & "FE_FECH_CXP=" & Comi & fechag & Comi & Coma
'    Valores = Valores & "FE_VENC_CXP=" & Comi & fechav & Comi & Coma
'    Valores = Valores & "CD_TERC_CXP=" & Comi & Cambiar_Comas_Comillas(Me.txtDocum(5).Text) & Comi & Coma
'    Valores = Valores & "NU_FAPR_CXP=" & CDbl(Me.TxtCompra(10).Text) & Coma
'    Valores = Valores & "VL_BRUT_CXP=" & CDbl(Me.TxtCompra(12).Text) & Coma
'    Valores = Valores & "VL_BRUE_CXP=0" & Coma
'    Valores = Valores & "VL_NETO_CXP=0" & CDbl(Me.TxtCompra(17).Text) & Coma
'    Valores = Valores & "VL_CANC_CXP=0" & Coma
'    Valores = Valores & "VL_NCRE_CXP=0" & Coma
'    Valores = Valores & "VL_NDEB_CXP=0" & Coma
'    Valores = Valores & "NU_EGRE_CXP=0" & Coma
'    Valores = Valores & "DE_OBSE_CXP=" & Comi & Cambiar_Comas_Comillas(Me.TxtCompra(11).Text) & Comi & Coma
'    ''Fletes y retencion
'    Valores = Valores & "VL_FLET_CXP=" & CDbl(Me.TxtCompra(16).Text) & Coma
'    Valores = Valores & "VL_RETE_CXP=" & CDbl(Me.TxtCompra(15).Text) & Coma
'    ''Descuentos e Impuestos
'    Valores = Valores & "VL_DESC_CXP=" & CDbl(Me.TxtCompra(13).Text) & Coma
'    Valores = Valores & "VL_IMPU_CXP=" & CDbl(Me.TxtCompra(14).Text) & Coma
'    ''Monto Escrito
'    Valores = Valores & "DE_MONT_CXP=" & Comi & Monto_Escrito(CDbl(Me.TxtCompra(17).Text)) & Comi & Coma
'    ''Estado
'    Valores = Valores & "ID_ESTA_CXP=1" & Coma
'    Valores = Valores & "ID_PPTO_CXP=0" ' Realizado para que funcione la interface con cxpagar giu
'    Debug.Print Valores
'    Result = DoInsertSQL("C_X_P", Valores)
'    ''Actualiza el consecutivo del concepto
'    If Result <> FAIL Then Result = DoUpdate("CONCEPTO", "NU_CONS_CONC=" & Umero, "ID_TIPO_CONC=7 And NU_CONS_CONC <" & Umero)
'    If Result <> FAIL Then Result = DoUpdate("PARAMETROS_CXP", "NU_CXPA_APLI=" & Umero, NUL$)
'End Sub

Private Sub Graba_cxc(ByVal fechag As Variant, ByVal fechav As Variant, Cepto, Umero)
   Dim UnConsecutivo As New ElConsecutivo
    
   ''LJSA M4110 --------------------------------------------------------------------------------------------
   'Dim Arr() As Variant   'LJSA M4110
   'Dim Arr2() As Variant   'LJSA M4110
   'Dim StMsg As String               'LJSA M4110
   'Dim I As Integer                      'LJSA M4110
   ''Actualiza la cxc en valor nota credito
   'Condicion = "CD_CONCE_CXC=" & Comi & Cepto & Comi
   'Condicion = Condicion & " And CD_NUME_CXC=" & Umero
   'ReDim Arr(1)
   'Result = LoadData("C_X_C", "VL_NCRE_CXC, ( VL_NETO_CXC - VL_CANC_CXC - VL_NCRE_CXC + VL_NDEB_CXC )", Condicion, Arr())
   'If (Result <> False) Then
   '     If Encontro Then
   '         ReDim Arr2(1, 0)
   '         Result = LoadMulData("R_RECA_CXC", "CD_RECA_RECC,VL_SALD_RECC - VL_CANC_RECC", "CD_NUME_RECC = " & Comi & Umero & Comi, Arr2)
   '         If Result And Encontro Then
   '             StMsg = "Existen recibos de caja asociados a la Cuenta por Cobrar N� " & Umero
   '             StMsg = StMsg & vbCrLf & vbCrLf
   '             For I = 0 To UBound(Arr2, 2)
   '                 StMsg = StMsg & "- Recibo de Caja N� " & Arr2(0, I) & "                Saldo: $ " & Arr2(1, I)
   '                 StMsg = StMsg & vbCrLf
   '                 Arr(0) = CDbl(Arr(0)) + CDbl(Arr2(1, I))
   '             Next I
   '             StMsg = StMsg & vbCrLf & "---------------------------"
   '             StMsg = StMsg & vbCrLf & "TOTAL: $ " & Arr(0) & vbCrLf
   '             StMsg = StMsg & vbCrLf & "Desea continuar con la devoluc�n de la factura?"
   '             If MsgBox(StMsg, vbQuestion + vbYesNo, "Acceso") = vbNo Then Call RollBackTran: Exit Sub
   '         Else
   '         Arr(0) = Arr(1)
   '     End If
   ' If Arr(0) = NUL$ Then Arr(0) = 0
   ' Valores = " VL_NCRE_CXC=" & Comi & Arr(0) & Comi
   ' Result = DoUpdate("C_X_C", Valores, Condicion)
   ' End If
  'End If
''LJSA M4110 --------------------------------------------------------------------------------------------
   UnConsecutivo.Actualiza Documento.Encabezado.NumeroCOMPROBANTE
'    Valores = "CD_CONCE_CXC=" & Comi & "VINV" & Comi & Coma
'    Valores = Valores & "CD_NUME_CXC=" & Conse_CXC & Coma
   Valores = "CD_CONCE_CXC=" & Comi & Cepto & Comi & Coma
   Valores = Valores & "CD_NUME_CXC=" & Umero & Coma
   'DAHV T6196 - INICIO
   'Valores = Valores & "FE_FECH_CXC=" & Comi & fechag & Comi & Coma
   'Valores = Valores & "FE_VENC_CXC=" & Comi & fechav & Comi & Coma
   'Valores = Valores & "FE_FECH_CXC=" & FFechaIns(CStr(fechag)) & Coma 'JAUM T29758 Se deja linea en comentario
   Valores = Valores & "FE_FECH_CXC=" & FHFECHA(CStr(fechag) & " " & Format(Nowserver, "HH:MM:SS")) & Coma 'JAUM T29758
   Valores = Valores & "FE_VENC_CXC=" & FFechaIns(CStr(fechav)) & Coma
   'DAHV T6196 - FIN
   Valores = Valores & "CD_TERC_CXC=" & Comi & Cambiar_Comas_Comillas(Documento.Encabezado.Tercero.Nit) & Comi & Coma
'    Valores = Valores & "NU_FACL_CXC=" & CDbl(TxtVenta(0)) & Coma
   Valores = Valores & "NU_FACL_CXC=" & CLng(NumeroCompro) & Coma
   Valores = Valores & "VL_BRUT_CXC=" & CCur(TxtVenta(15).Text) & Coma
   Valores = Valores & "VL_BRUE_CXC=0" & Coma
   'Valores = Valores & "VL_NETO_CXC=" & CCur(TxtVenta(20).Text) & Coma
   Valores = Valores & "VL_NETO_CXC=" & CCur(TxtVenta(15).Text) & Coma     'PedroJ Def 4224
   Valores = Valores & "VL_CANC_CXC=0" & Coma
   Valores = Valores & "VL_NCRE_CXC=0" & Coma
   Valores = Valores & "VL_NDEB_CXC=0" & Coma
   Valores = Valores & "NU_RECA_CXC=0" & Coma
   Valores = Valores & "DE_OBSE_CXC=" & Comi & "II_FACTURA DE VENTA " & UnConsecutivo.Prefijo & "-" & NumeroCompro & "/" & Cambiar_Comas_Comillas(Documento.Encabezado.Tercero.Nit) & Comi & Coma
   ''Fletes y retencion
   Valores = Valores & "VL_FLET_CXC=" & CDbl(TxtVenta(19).Text) & Coma
   Valores = Valores & "VL_RETE_CXC=" & CDbl(TxtVenta(18).Text) & Coma
   ''Descuentos e Impuestos
   Valores = Valores & "VL_DESC_CXC=" & CDbl(TxtVenta(16).Text) & Coma
   Valores = Valores & "VL_IMPU_CXC=" & CDbl(TxtVenta(17).Text) & Coma
   ''Monto Escrito
   Valores = Valores & "DE_MONT_CXC=" & Comi & Monto_Escrito(CDbl(TxtVenta(15).Text)) & Comi & Coma
   ''Estado
   Valores = Valores & "ID_ESTA_CXC=1" & Coma
   'Valores = Valores & "ID_PPTO_CXC=0"
   Result = DoInsertSQL("C_X_C", Valores)
   
   'INICIO LDCR R9067/T9396
   'Campos = "NU_NUMEFACT_MCXC = " & CLng(NumeroCompro) EACT T20268 SE ESTABELCE EN COMENTARIO
   'Campos = Campos & ",FE_FECHACAMB_MCXC=" & FFechaIns(Hoy())  EACT T20268 SE ESTABLECE EN COMENTARIO
   'Campos = "FE_FECHACAMB_MCXC = " & FFechaIns(Hoy()) 'EACT T20268 'JAUM T29758 Se deja linea en comentario
   Campos = "FE_FECHACAMB_MCXC = " & FHFECHA(Nowserver) 'JAUM T29758
   Campos = Campos & ",CD_NUME_MCXC=" & Umero
   Campos = Campos & ",NU_TIPOMOV_MCXC=0"
   'Campos = Campos & ",NU_ESTCXC_MCXC=1" EACT T20268 SE ESTABLECE EN COMENTARIO
   If Result <> FAIL Then Result = DoInsertSQL("MovCxC", Campos)
   'INICIO LDCR R9067/T9396
   
   If (Result <> FAIL) Then Call TrasladoMovimiento("R_TRASMOV_CXC", Umero, "CXC") 'JACC M4759
   If Result <> FAIL Then Call Mensaje1("Se genero la CxC No." & Umero, 3)
   ''Actualiza el consecutivo del concepto
'    Result = DoUpdate("CONCEPTO", "NU_CONS_CONC=" & Umero, "ID_TIPO_CONC=0 And NU_CONS_CONC <" & Umero)
   'Result = DoUpdate("CONCEPTO", "NU_CONS_CONC=" & Umero, "NU_CONS_CONC <" & Umero) 'OMOG T18089
   If Result <> FAIL Then Result = DoUpdate("PARAMETROS_CXC", "NU_CXCO_APLI=" & Umero, NUL$)
    
End Sub

Private Sub Grabar_Presupuesto(vTipo As Byte)
    Dim J As Long, vPorXTipo As Double
    Me.TxtVenta(20).Text = "0"
    For J = 0 To UBound(MatPpto(), 2)
        Me.TxtVenta(20).Text = CCur(Me.TxtVenta(20).Text) + CCur(MatPpto(3, J))
    Next
    If IsNumeric(Me.TxtVenta(15).Text) And IsNumeric(Me.TxtVenta(20).Text) Then
        If CCur(Me.TxtVenta(20).Text) > 0 Then
            Select Case vTipo
            Case Is = 0
                vPorXTipo = CCur(Me.TxtVenta(15).Text) / CCur(Me.TxtVenta(20).Text)
            Case Is = 1
                vPorXTipo = 1 - (CCur(Me.TxtVenta(15).Text) / CCur(Me.TxtVenta(20).Text))
            Case Is = 5
                vPorXTipo = 1
            End Select
        End If
    End If
    Dim Val As Double, UnConsecutivo As New ElConsecutivo
    UnConsecutivo.Actualiza Documento.Encabezado.NumeroCOMPROBANTE
'    Val = CDbl(TxtVenta(15)) - CDbl(TxtVenta(16)) + CDbl(TxtVenta(17)) + CDbl(TxtVenta(19))
'              "FE_FECH_GIRI=" & fecha(Me.TxtVenta(1).Text & "/" & Me.TxtVenta(2).Text & "/" & Me.TxtVenta(3).Text) & Coma & _
'              "DE_DESC_GIRI=" & Comi & "II-VENTA DE INVENTARIOS No." & TxtVenta(0).Text & Comi & Coma & _
'Anula_Giro_Ingresos, Depesel, OrdeSel

    Valores = "NU_CONS_GIRI=" & vGiroIng & Coma & _
              "FE_PERI_GIRI=" & Year(Documento.Encabezado.FechaDocumento) & Coma & _
              "FE_FECH_GIRI=" & FFechaIns(Documento.Encabezado.FechaDocumento) & Coma & _
              "CD_DEPE_GIRI=" & Comi & Aplicacion.Ppto_Dependencia & Comi & Coma & _
              "DE_DESC_GIRI=" & Comi & "II-VENTA DE INVENTARIOS " & UnConsecutivo.Prefijo & "-" & NumeroCompro & Comi & Coma & _
              "CD_ORDE_GIRI=" & Aplicacion.Ppto_Ordenador & Coma & _
              "ID_TIPO_GIRI=" & vTipo & Coma & _
              "VL_VALO_GIRI=" & vPorXTipo * CCur(Me.TxtVenta(20).Text) & Coma & _
              "CD_CODI_PAC_GIRI=" & Comi & UnConsecutivo.Prefijo & NumeroCompro & Comi & Coma & _
              "ID_ESTA_GIRI='1'"
   Result = DoInsertSQL("GIROING", Valores)
   
   If Result <> FAIL Then
      For J = 0 To UBound(MatPpto(), 2)
          If MatPpto(0, J) <> NUL$ Then
             Valores = "NU_GIRO_GIAR=" & vGiroIng & Coma & _
                       "CD_ARTI_GIAR=" & Comi & MatPpto(0, J) & Comi & Coma & _
                       "VL_GIRO_GIAR=" & vPorXTipo * CCur(MatPpto(3, J)) & Coma & _
                       "ID_NOAF_GIAR=0"
             Result = DoInsertSQL("GIROING_ARTICULO", Valores)
             
             If (Result <> FAIL) Then Call Actualiza_Ingresos(CByte(vTipo), CStr(MatPpto(0, J)), CDbl(MatPpto(3, J)), CByte(Month(Documento.Encabezado.FechaDocumento)))
             If (Result <> FAIL) Then Result = DoUpdate("CONSECUTIVOS", "NU_CONS_GIRI = NU_CONS_GIRI + 1", NUL$)
             If (Result = FAIL) Then Exit For
          End If
      Next
   End If
'   If Result <> FAIL Then Call Mensaje1("Giro de Ingresos No." & vGiroIng, 2)
End Sub

'DEPURACION DE CODIGO
'Private Sub Anula_Giro_Ingresos(Numero As Double, ActConta As Boolean, NumDocAn As Double, FechaD As String)
'    ReDim Datos(2, 0)
'    Dim ArrD(1)
'    ReDim Arr(1)
'    Dim campo As String
'    Dim CampoC As String
'    Dim CompPpto As String
'    Dim Imput As Integer, i As Integer
'    Dim val As Double
'
'    CompPpto = Leer_Comprobante("NU_COMP_ANUL")
'
'    Result = DoUpdate("GIROING", "ID_ESTA_GIRI = '2', FE_ANUL_GIRI = " & FFechaIns(FechaD), "NU_CONS_GIRI = " & Numero)
'
'    If Result <> FAIL Then
'       Result = LoadData("GIROING", "CD_DEPE_GIRI, ID_TIPO_GIRI", "NU_CONS_GIRI=" & Numero, ArrD())
'       Select Case CByte(ArrD(1))
'            Case 0: CampoC = "NU_EFEC_CUEN": campo = "VL_CAAN_PERI"
'            Case 1: CampoC = "NU_EFEC_CUEN": campo = "VL_EFAN_PERI"
'            Case 2: CampoC = "NU_PAPE_CUEN": campo = "VL_RPAN_PERI"
'            Case 3: CampoC = "NU_OTCA_CUEN": campo = "VL_OCAN_PERI"
'            Case 4: CampoC = "NU_RECO_CUEN": campo = "VL_RCAN_PERI"
'       End Select
'
'       Campos = "CD_ARTI_GIAR, VL_GIRO_GIAR, ID_NOAF_GIAR"
'       Result = LoadMulData("GIROING_ARTICULO", Campos, "NU_GIRO_GIAR = " & Numero, Datos())
'
'       If Result <> FAIL Then
'          Imput = 1
'          For i = 0 To UBound(Datos(), 2)
'              If Datos(1, i) = NUL$ Then Datos(1, i) = 0
'              val = CDbl(Datos(1, i))
'              If Result <> FAIL Then Call Actualiza_Ingresos_Mes(CStr(Datos(0, i)), campo, val, Format(Hoy(), "mm"))
'
'              If Result <> FAIL Then Call Actualiza_Ingresos_Dep(CStr(Datos(0, i)), campo, val, Format(Hoy(), "mm"), CStr(ArrD(0)))
'
'              If CByte(ArrD(1)) > 0 And ActConta Then
'                Result = LoadData("CUENTAS_INGRESOS", "NU_EFEC_CUEN, NU_XEJE_CUEN", "CD_ARTI_CUEN =" & Comi & CStr(Datos(0, i)) & Comi, Arr())
'                If Arr(0) <> NUL$ And Arr(1) <> NUL$ Then
'                    If Result <> FAIL Then Call Actualiza_Contabilidad(CStr(Arr(0)), "D", CStr(ArrD(0)), NUL$, val, NumDocAn, FechaD, CompPpto, "ANULACION GIRO INGRESOS No. " & Numero, Imput)
'                    If Result <> FAIL Then Imput = Imput + 1
'                    If Result <> FAIL Then Call Actualiza_Contabilidad(CStr(Arr(1)), "C", CStr(ArrD(0)), NUL$, val, NumDocAn, FechaD, CompPpto, "ANULACION GIRO INGRESOS No. " & Numero, Imput)
'                    If Result <> FAIL Then Imput = Imput + 1
'                End If
'              End If
'              If Result = FAIL Then Exit For
'          Next i
'       End If
'    End If
'End Sub

Private Sub ValorCreditoFactura()

    Dim vVal As Currency, vCon As Integer
    Me.TxtVenta(15).Text = "0"
    Me.TxtVenta(20).Text = "0"
    If Me.grdFormaPago.Rows > 1 Then
        For vCon = 1 To Me.grdFormaPago.Rows - 1
            If IsNumeric(Me.grdFormaPago.TextMatrix(vCon, 2)) Then
                vVal = CCur(Me.grdFormaPago.TextMatrix(vCon, 2))
                If vVal > 0 Then
                    Me.TxtVenta(20).Text = CCur(Me.TxtVenta(20).Text) + vVal
                    'INICIO LDCR T1565
                    'If CInt(Me.grdFormaPago.TextMatrix(vCon, 0)) = vCodigoFormaCredito Then Me.TxtVenta(15).Text = CCur(Me.TxtVenta(15).Text) + vVal
                    If Me.grdFormaPago.TextMatrix(vCon, 0) = CStr(vCodigoFormaCredito) Then Me.TxtVenta(15).Text = CCur(Me.TxtVenta(15).Text) + vVal
                    'FIN LDCR T1565
                End If
            End If
        Next
    End If

End Sub

''//////////////////////
Private Sub CmdCancelar_Click()
    Call cmdREGRESAR_Click(-1)
End Sub

Private Sub cmdFacturar_Click()
    If Not CCur(Me.txtValorFactura.Text) = SumaFormas() Then
        Me.cboFormas.SetFocus
'        Me.cmdFacturar.Enabled = False
        Exit Sub
    End If
    If Me.frmFORMAPAGO.Visible Then Me.frmFORMAPAGO.Visible = False
    Me.frmINVENTARIOS.Visible = True
    vSeFactura = True
    LaPuedoCerrar = True
    If vSeGraba Then
        vSeGraba = False
        Call cmdOpciones_Click(0)
    Else
        Call cmdPROCEDER_Click(FacturaVenta)
    End If
'    Me.Hide
End Sub

Private Sub cmdFacturar_LostFocus()
    If Not CCur(Me.txtValorFactura.Text) = SumaFormas() Then Me.cmdFacturar.Enabled = False
End Sub

Private Sub formapagoForm_Load()

    vSeFactura = False
    Me.txtValorFactura.Text = FormatCurrency(vTotalFactura, 2)
    Me.txtValorRenglon.Text = FormatCurrency(vTotalFactura, 2)
    Result = loadctrl("FORMA_PAGO", "DE_DESC_FOPA", "NU_NUME_FOPA", Me.cboFormas, itmFormas, "")
    If Documento.Encabezado.EsNuevoDocumento Then Me.grdFormaPago.Rows = 1
'    If Documento.Encabezado.TipDeDcmnt = FacturaVenta Then Me.grdFormaPago.Rows = 1
'    If Documento.Encabezado.TipDeDcmnt = FacturaVenta Then If Documento.Encabezado.EsNuevoDocumento Then Call InsertaForma(CStr(vCodigoFormaEfectivo), vTotalFactura)
   If Documento.Encabezado.TipDeDcmnt = FacturaVenta Then If Documento.Encabezado.EsNuevoDocumento Then Call InsertaForma(CStr(vCodigoFormaEfectivo))   'DEPURACION DE CODIGO

End Sub

'Private Sub InsertaForma(Optional vTipo As String = "", Optional vValor As Currency = 0)   'DEPURACION DE CODIGO
Private Sub InsertaForma(Optional vTipo As String = "")
    Dim vFil As Integer
    If Len(vTipo) > 0 Then Me.cboFormas.ListIndex = FindInArr(itmFormas, vTipo)
    If Not Me.cboFormas.ListIndex > -1 Then Exit Sub
    If Not CCur(Me.txtValorRenglon.Text) > 0 Then
        For vFil = 1 To Me.grdFormaPago.Rows - 1
            If Me.grdFormaPago.TextMatrix(vFil, 0) = itmFormas(Me.cboFormas.ListIndex) Then
                If Me.grdFormaPago.Rows > 2 Then
                    Me.grdFormaPago.RemoveItem vFil
                    Exit For
                Else
                    Me.grdFormaPago.Rows = 1
                End If
            End If
        Next
        Exit Sub
    Else
        For vFil = 1 To Me.grdFormaPago.Rows - 1
            If Me.grdFormaPago.TextMatrix(vFil, 0) = itmFormas(Me.cboFormas.ListIndex) Then
                Me.grdFormaPago.TextMatrix(vFil, 1) = Me.cboFormas.List(Me.cboFormas.ListIndex)
                Me.grdFormaPago.TextMatrix(vFil, 2) = FormatCurrency(CCur(Me.txtValorRenglon.Text), 2)
                Exit Sub
            End If
        Next
    End If
    Me.grdFormaPago.Rows = Me.grdFormaPago.Rows + 1
    Me.grdFormaPago.Row = Me.grdFormaPago.Rows - 1
    Me.grdFormaPago.TextMatrix(Me.grdFormaPago.Row, 0) = itmFormas(Me.cboFormas.ListIndex)
    Me.grdFormaPago.TextMatrix(Me.grdFormaPago.Row, 1) = Me.cboFormas.List(Me.cboFormas.ListIndex)
    Me.grdFormaPago.TextMatrix(Me.grdFormaPago.Row, 2) = FormatCurrency(CCur(Me.txtValorRenglon.Text), 2)
End Sub

Private Function SumaFormas() As Currency
    Dim vFil As Integer
    For vFil = 1 To Me.grdFormaPago.Rows - 1
        SumaFormas = SumaFormas + CCur(Me.grdFormaPago.TextMatrix(vFil, 2))
    Next
End Function

Private Sub txtValorRenglon_GotFocus()
    Me.txtValorRenglon.Text = FormatCurrency(CCur(Me.txtValorFactura.Text) - SumaFormas(), 2)
End Sub

Private Sub txtValorRenglon_KeyPress(KeyAscii As Integer)
    Call Cambiar_Enter(KeyAscii)
End Sub

Private Sub txtValorRenglon_LostFocus()
    If Not IsNumeric(Me.txtValorRenglon.Text) Then Me.txtValorRenglon.Text = "0"
    Me.txtValorRenglon.Text = FormatCurrency(IIf(CCur(Me.txtValorRenglon.Text) > 0, CCur(Me.txtValorRenglon.Text), 0), 2)
    Call InsertaForma
    If vTotalFactura = SumaFormas() Then Me.cmdFacturar.Enabled = True
End Sub
''//////////////////////
''//////////////////////
Private Sub cmdOtrosCancelar_Click()
    Call cmdREGRESAR_Click(-1)
End Sub

Private Sub cboOtrosCargos_LostFocus()
    Dim vPos As Integer
    If Me.cboOtrosCargos.ListIndex < 0 Then Exit Sub
    vPos = FindInArrM(itmCuentasOtrosCargos, itmOtrosCargos(Me.cboOtrosCargos.ListIndex), 0)
    If vPos < 0 Then Exit Sub
    Me.txtPorceCargo.Text = FormatNumber(itmCuentasOtrosCargos(3, vPos), 2)
End Sub

Private Sub cmdOtrosCargos_Click()
    vSeCarga = True
    Me.txtValorFactura.Text = FormatCurrency(CCur(Me.txtValorOtrosCargos.Text), 2)
    vTotalFactura = CCur(Me.txtValorOtrosCargos.Text)
    Call formapagoForm_Load
    Me.frmOTROSCARGOS.Visible = False
    Me.frmFORMAPAGO.Visible = True
    Call CentraMarco(Me.frmFORMAPAGO)
'    Me.Hide
End Sub

Private Sub otroscargosForm_Load()
''SELECT CD_CODI_IMPU, DE_NOMB_IMPU FROM TC_IMPUESTOS WHERE ID_TIPO_IMPU='O'
    vSeCarga = False
'    Me.txtValorOtrosCargos = FormatCurrency(vTotalOtrosCargos, 2)
'    Me.txtValorCargo = FormatCurrency(vTotalOtrosCargos, 2)
    Result = loadctrl("TC_IMPUESTOS", "DE_NOMB_IMPU", "CD_CODI_IMPU", Me.cboOtrosCargos, itmOtrosCargos, "ID_TIPO_IMPU='O'")
    If Documento.Encabezado.EsNuevoDocumento Then Me.grdOtrosCargos.Rows = 1
'    If Documento.Encabezado.TipDeDcmnt = FacturaVenta Then Me.grdOtrosCargos.Rows = 1
'    Call InsertaOtroCargo("0", vTotalOtrosCargos)
End Sub

'Private Sub InsertaOtroCargo(Optional vTipo As String = "", Optional vValor As Currency = 0)
Private Sub InsertaOtroCargo(Optional vTipo As String = "")     'DEPURACION DE CODIGO
    Dim vFil As Integer
    If Len(vTipo) > 0 Then Me.cboOtrosCargos.ListIndex = FindInArr(itmOtrosCargos, vTipo)
    If Not Me.cboOtrosCargos.ListIndex > -1 Then Exit Sub
    If Not CCur(Me.txtValorCargo.Text) > 0 Then
        For vFil = Me.grdOtrosCargos.Rows - 1 To 1 Step -1
            If Me.grdOtrosCargos.TextMatrix(vFil, 0) = itmOtrosCargos(Me.cboOtrosCargos.ListIndex) Then
                If Me.grdOtrosCargos.Rows > 2 Then
                    Me.grdOtrosCargos.RemoveItem vFil
                Else
                    Me.grdOtrosCargos.Rows = 1
                End If
            End If
        Next
        Exit Sub
    Else
        For vFil = 1 To Me.grdOtrosCargos.Rows - 1
            If Me.grdOtrosCargos.TextMatrix(vFil, 0) = itmOtrosCargos(Me.cboOtrosCargos.ListIndex) Then
                Me.grdOtrosCargos.TextMatrix(vFil, 1) = Me.cboOtrosCargos.List(Me.cboOtrosCargos.ListIndex)
                Me.grdOtrosCargos.TextMatrix(vFil, 2) = FormatCurrency(CCur(Me.txtValorCargo.Text), 2)
                Me.grdOtrosCargos.TextMatrix(vFil, 3) = FormatNumber(CCur(Me.txtPorceCargo.Text), 2)
                Me.grdOtrosCargos.TextMatrix(vFil, 4) = FormatNumber(CCur(vTotalFactura), 2) 'DRMG T45221-R41288
                Exit Sub
            End If
        Next
    End If
    Me.grdOtrosCargos.Rows = Me.grdOtrosCargos.Rows + 1
    Me.grdOtrosCargos.Row = Me.grdOtrosCargos.Rows - 1
    Me.grdOtrosCargos.TextMatrix(Me.grdOtrosCargos.Row, 0) = itmOtrosCargos(Me.cboOtrosCargos.ListIndex)
    Me.grdOtrosCargos.TextMatrix(Me.grdOtrosCargos.Row, 1) = Me.cboOtrosCargos.List(Me.cboOtrosCargos.ListIndex)
    Me.grdOtrosCargos.TextMatrix(Me.grdOtrosCargos.Row, 2) = FormatCurrency(CCur(Me.txtValorCargo.Text), 2)
    Me.grdOtrosCargos.TextMatrix(Me.grdOtrosCargos.Row, 3) = FormatNumber(CCur(Me.txtPorceCargo.Text), 2)
    Me.grdOtrosCargos.TextMatrix(Me.grdOtrosCargos.Row, 3) = FormatNumber(CCur(vTotalFactura), 2) 'DRMG T45221-R41288
End Sub

Private Function SumaOtrosCargos() As Currency
    Dim vFil As Integer
    For vFil = 1 To Me.grdOtrosCargos.Rows - 1
        SumaOtrosCargos = SumaOtrosCargos + CCur(Me.grdOtrosCargos.TextMatrix(vFil, 2))
    Next
End Function

Private Sub txtValorCargo_GotFocus()
'    Me.txtValorCargo.Text = FormatCurrency(Me.txtValorOtrosCargos - SumaOtrosCargos(), 2)
    Me.txtValorCargo.Text = FormatCurrency(0, 2)
    If IsNumeric(Me.txtPorceCargo.Text) Then
        If CCur(Me.txtPorceCargo.Text) > 0 Then Me.txtValorCargo.Text = FormatCurrency(CCur(Me.txtPorceCargo.Text) * vTotalFactura / 100, 2)
    End If
End Sub

Private Sub txtValorCargo_KeyPress(KeyAscii As Integer)
    Call Cambiar_Enter(KeyAscii)
End Sub

Private Sub txtValorCargo_LostFocus()
    If Not IsNumeric(Me.txtValorCargo.Text) Then Me.txtValorCargo.Text = "0"
    Me.txtValorCargo.Text = FormatCurrency(IIf(Len(Me.txtValorCargo.Text) > 0, CCur(Me.txtValorCargo.Text), 0), 2)
    Me.txtPorceCargo.Text = FormatNumber(IIf(vTotalFactura > 0, 100 * CCur(Me.txtValorCargo.Text) / vTotalFactura, 0), 2)
    Call InsertaOtroCargo
    Me.txtValorOtrosCargos.Text = FormatCurrency(vTotalFactura + SumaOtrosCargos(), 2)
End Sub
''//////////////////////
''//////////////////////
Private Sub cmdDescuentosCancelar_Click()
'    Me.frmINVENTARIOS.Visible = True
    Call cmdREGRESAR_Click(-1)
End Sub

Private Sub cboOtrosDescuentos_LostFocus()
    Dim vPos As Integer
    If Me.cboOtrosDescuentos.ListIndex < 0 Then Exit Sub
    vPos = FindInArrM(itmCuentasOtrosDescuentos, itmOtrosDescuentos(Me.cboOtrosDescuentos.ListIndex), 0)
    If vPos < 0 Then Exit Sub
    Me.txtPorceDescuento.Text = FormatNumber(itmCuentasOtrosDescuentos(3, vPos))
End Sub

Private Sub cmdOtrosDescuentos_Click()
'    If Not CSng(Me.txtValorOtrosDescuentos.Text) = SumaOtrosDescuentos() Then
'        Me.cmdOtrosDescuentos.Enabled = False
'        Exit Sub
'    End If
    vSeDescuenta = True
    Me.txtValorOtrosCargos.Text = FormatCurrency(CCur(Me.txtValorOtrosDescuentos.Text), 2)
    vTotalFactura = CCur(Me.txtValorOtrosDescuentos.Text)
    Call otroscargosForm_Load
    Me.frmOTROSDESCUENTOS.Visible = False
    Me.frmOTROSCARGOS.Visible = True
    Call CentraMarco(Me.frmOTROSCARGOS)
'    Me.Hide
End Sub

Private Sub otrosDescuentosForm_Load()
    Dim vCon As Integer, ArrOtrDes() As Variant
''SELECT CD_CODI_IMPU, DE_NOMB_IMPU, PR_PORC_IMPU FROM TC_IMPUESTOS WHERE ID_TIPO_IMPU='O'
'DESCUENTO, CD_CODI_DESC, DE_NOMB_DESC
    vSeDescuenta = False
'    Me.txtValorOtrosDescuentos = FormatCurrency(vTotalOtrosDescuentos, 2)
'    Me.txtValorDescuento = FormatCurrency(vTotalOtrosDescuentos, 2)
    'GAPM M6404 INICIO
    If Documento.Encabezado.TipDeDcmnt = FacturaVenta Then
        Condicion = "TX_TIPDESC_DESC='1' OR TX_TIPDESC_DESC='3'"
    End If
    Result = loadctrl("DESCUENTO", "DE_NOMB_DESC", "CD_CODI_DESC", Me.cboOtrosDescuentos, itmOtrosDescuentos, Condicion)
    'Result = loadctrl("DESCUENTO", "DE_NOMB_DESC", "CD_CODI_DESC", Me.cboOtrosDescuentos, itmOtrosDescuentos, "")
    'GAPM M6404 FIN
    If Encontro Then    'REOL M2195
       For vCon = 0 To UBound(itmOtrosDescuentos, 1)
           ReDim ArrOtrDes(0)
           Result = LoadData("DESCUENTO", "VL_TOPE_DESC", "CD_CODI_DESC=" & Comi & itmOtrosDescuentos(vCon) & Comi, ArrOtrDes)
           If Result <> FAIL Then
               ReDim Preserve itemValoTopeDescuento(vCon)
               itemValoTopeDescuento(vCon) = CCur(ArrOtrDes(0))
           End If
       Next
    End If
    If Documento.Encabezado.TipDeDcmnt = FacturaVenta Then Me.grdOtrosDescuentos.Rows = 1
'    Call InsertaOtroDescuento("0", vTotalOtrosDescuentos)
End Sub

'Private Sub InsertaOtroDescuento(Optional vTipo As String = "", Optional vValor As Single = 0)     'DEPURACION DE CODIGO
Private Sub InsertaOtroDescuento(Optional vTipo As String = "")
    Dim vFil As Integer
    If Len(vTipo) > 0 Then Me.cboOtrosDescuentos.ListIndex = FindInArr(itmOtrosDescuentos, vTipo)
    If Not Me.cboOtrosDescuentos.ListIndex > -1 Then Exit Sub
    If Not CCur(Me.txtValorDescuento.Text) > 0 Then
        For vFil = Me.grdOtrosDescuentos.Rows - 1 To 1 Step -1
            If Me.grdOtrosDescuentos.TextMatrix(vFil, 0) = itmOtrosDescuentos(Me.cboOtrosDescuentos.ListIndex) Then
                If Me.grdOtrosDescuentos.Rows > 2 Then
                    Me.grdOtrosDescuentos.RemoveItem vFil
                Else
                    Me.grdOtrosDescuentos.Rows = 1
                End If
            End If
        Next
        Exit Sub
    ElseIf CCur(vTotalFactura) >= CCur(itemValoTopeDescuento(Me.cboOtrosDescuentos.ListIndex)) Then
        For vFil = 1 To Me.grdOtrosDescuentos.Rows - 1
            If Me.grdOtrosDescuentos.TextMatrix(vFil, 0) = itmOtrosDescuentos(Me.cboOtrosDescuentos.ListIndex) Then
                Me.grdOtrosDescuentos.TextMatrix(vFil, 1) = Me.cboOtrosDescuentos.List(Me.cboOtrosDescuentos.ListIndex)
                Me.grdOtrosDescuentos.TextMatrix(vFil, 2) = FormatCurrency(CCur(Me.txtValorDescuento.Text), 2)
                Me.grdOtrosDescuentos.TextMatrix(vFil, 3) = FormatCurrency(CCur(Me.txtPorceDescuento.Text), 2)
                Me.grdOtrosDescuentos.TextMatrix(vFil, 4) = vTotalFactura 'DRMG T45221-R41288
                Exit Sub
            End If
        Next
        Me.grdOtrosDescuentos.Rows = Me.grdOtrosDescuentos.Rows + 1
        Me.grdOtrosDescuentos.Row = Me.grdOtrosDescuentos.Rows - 1
        Me.grdOtrosDescuentos.TextMatrix(Me.grdOtrosDescuentos.Row, 0) = itmOtrosDescuentos(Me.cboOtrosDescuentos.ListIndex)
        Me.grdOtrosDescuentos.TextMatrix(Me.grdOtrosDescuentos.Row, 1) = Me.cboOtrosDescuentos.List(Me.cboOtrosDescuentos.ListIndex)
        Me.grdOtrosDescuentos.TextMatrix(Me.grdOtrosDescuentos.Row, 2) = FormatCurrency(CCur(Me.txtValorDescuento.Text), 2)
        Me.grdOtrosDescuentos.TextMatrix(Me.grdOtrosDescuentos.Row, 3) = FormatCurrency(CCur(Me.txtPorceDescuento.Text), 2)
        Me.grdOtrosDescuentos.TextMatrix(Me.grdOtrosDescuentos.Row, 4) = vTotalFactura 'DRMG T45221-R41288
    Else
        Call MsgBox("El valor de la factura debe ser superior o igual a " & FormatCurrency(CCur(itemValoTopeDescuento(Me.cboOtrosDescuentos.ListIndex)), 2) & ", para poder aplicar este descuento", vbInformation)
    End If
End Sub

Private Function SumaOtrosDescuentos() As Single
   Dim vFil As Integer
   Dim DbPorc As Double 'JLPB T41054
   If Desco Then
      For vFil = 1 To Me.grdOtrosDescuentos.Rows - 1
         If Me.grdOtrosDescuentos.TextMatrix(vFil, 2) = NUL$ Then Me.grdOtrosDescuentos.TextMatrix(vFil, 2) = "0" 'REOL174
         SumaOtrosDescuentos = SumaOtrosDescuentos + CCur(Me.grdOtrosDescuentos.TextMatrix(vFil, 2))
      Next
   Else
      ' DAHV M6606
      ''JACC M6606
      'If Documento.Encabezado.TipDeDcmnt = FacturaVenta Then
      '  If Not Documento.Encabezado.EsNuevoDocumento Then
      '    ReDim ArDes(0)
      '    Result = LoadData("IN_PARTIPARTI", "SUM(CASE ISNUMERIC (NU_VALO_PRPR) WHEN 1 THEN NU_VALO_PRPR ELSE 0 END)", "TX_CUAL_PRPR = 'D' AND NU_AUTO_ENCA_PRPR = " & Documento.Encabezado.AutoDelDocCARGADO, ArDes)
      '    If Result <> FAIL And ArDes(0) <> NUL$ Then
      '        SumaOtrosDescuentos = CDbl(ArDes(0))
      '        Exit Function
      '    End If
      '  End If
      'End If
      'JACC M6606
      'REOL174
      'JLPB T41054 INICIO
      Campos = "ISNULL(SUM(CAST(NU_VALO_PRPR AS FLOAT)),0)"
      Desde = "IN_PARTIPARTI"
      Condicion = "NU_AUTO_ENCA_PRPR=" & Documento.Encabezado.AutoDelDocCARGADO
      Condicion = Condicion & " AND TX_CUAL_PRPR IS NULL"
      DbPorc = fnDevDato(Desde, Campos, Condicion, True)
      'JLPB T41054 FIN
      For vFil = 1 To GrdArticulos.Rows - 1
         If GrdArticulos.TextMatrix(vFil, 2) = NUL$ Then GrdArticulos.TextMatrix(vFil, 2) = "0"
         'SumaOtrosDescuentos = SumaOtrosDescuentos + (GrdArticulos.TextMatrix(vFil, 18) * GrdArticulos.TextMatrix(vFil, 10)) 'JLPB T41054 Se deja en comentario
         SumaOtrosDescuentos = SumaOtrosDescuentos + (((GrdArticulos.TextMatrix(vFil, 16) * DbPorc) / 100) * GrdArticulos.TextMatrix(vFil, 10)) 'JLPB T41054
      Next
   End If
   Desco = False
End Function

Private Sub txtValorDescuento_GotFocus()
'    Me.txtValorDescuento.Text = FormatCurrency(Me.txtValorOtrosDescuentos - SumaOtrosDescuentos(), 2)
    Me.txtValorDescuento.Text = FormatCurrency(0, 2)
    If IsNumeric(Me.txtPorceDescuento.Text) Then
        If CCur(Me.txtPorceDescuento.Text) > 0 Then Me.txtValorDescuento.Text = FormatCurrency(CCur(Me.txtPorceDescuento.Text) * vTotalFactura / 100, 2)
    End If
End Sub

Private Sub txtValorDescuento_KeyPress(KeyAscii As Integer)
    Call Cambiar_Enter(KeyAscii)
End Sub

Private Sub txtValorDescuento_LostFocus()
    If Not IsNumeric(Me.txtValorDescuento.Text) Then Me.txtValorDescuento.Text = "0"
    Me.txtValorDescuento.Text = FormatCurrency(IIf(CCur(Me.txtValorDescuento.Text) > 0, CCur(Me.txtValorDescuento.Text), 0), 2)
    Me.txtPorceDescuento.Text = FormatNumber(IIf(vTotalFactura > 0, 100 * CCur(Me.txtValorDescuento.Text) / vTotalFactura, 0), 2)
    Call InsertaOtroDescuento
    Desco = True 'REOL920
    Me.txtValorOtrosDescuentos.Text = FormatCurrency(vTotalFactura - SumaOtrosDescuentos(), 2)
End Sub
''//////////////////////


'PedroJ Def 4223
Private Sub Anula_CxC(CxC As Double, Factura As Double)
   Condi = "CD_CONCE_CXC='VINV' AND NU_FACL_CXC=" & Factura & " AND CD_NUME_CXC=" & CxC
   'Campos = "ID_ESTA_CXC=2" & Coma & " FE_ANUL_CXC=" & FFechaIns(Hoy()) 'JAUM T29758 Se deja linea en comentario
   Campos = "ID_ESTA_CXC=2" & Coma & " FE_ANUL_CXC=" & FHFECHA(Format(Nowserver, FormatF & " HH:MM:SS")) 'JAUM T29758
   Campos = Campos & Coma & "TX_NOUSAN_CXC = '" & NombreUsuario(UserId) & Comi 'AASV M4858
   Result = DoUpdate("C_X_C", Campos, Condi)
   
   'INICIO LDCR R9067/T9396
   'Campos = "NU_NUMEFACT_MCXC = " & Factura  EACT T20268 SE ESTABLECE EN COMENTARIO
   'Campos = Campos & ",FE_FECHACAMB_MCXC=" & FFechaIns(Hoy()) EACT T20268 SE ESTABLECE EN COMENTARIO
   'Campos = "FE_FECHACAMB_MCXC = " & FFechaIns(Hoy()) 'EACT T20268 'JAUM T29758 Se deja linea en comentario
   Campos = "FE_FECHACAMB_MCXC = " & FHFECHA(Format(Nowserver, FormatF & " HH:MM:SS")) 'JAUM T29758
   Campos = Campos & ",CD_NUME_MCXC=" & CxC
   Campos = Campos & ",NU_TIPOMOV_MCXC=0"
   'Campos = Campos & ",NU_ESTCXC_MCXC=2" EACT T20268 SE ESTABLECE EN COMENTARIO
   If Result <> FAIL Then Result = DoInsertSQL("MovCxC", Campos)
   'FIN LDCR R9067/T9396
   
   If Result <> FAIL Then Call Mensaje1("Se anulo la CxC No " & CxC, 2)
End Sub
'PedroJ Def 4223

'PedroJ: Interfase a Ppto de gastos
Private Sub Actualizar_Ppto()
'Dim Imput As Integer
Dim X As Double
Dim Incont As Integer 'DAHV M4217
Dim BoValida As Boolean
Dim StDiaVig As String 'GAVL T4266
Dim StFecha As Date  'GAVL T4266
Dim StFechaV As Date 'GAVL T4266
Dim ArrFech() As Variant 'GAVL T4959
'SKRV T15377 INICIO
Dim BoBandera As Boolean
If TipoRegimen = "1" And StRegEmpnavi = "0" And StOpcCxpnavi = "1" Then
    BoBandera = False
Else
    BoBandera = True
End If
'SKRV T15377 FIN
  Select Case Documento.Encabezado.TipDeDcmnt
    Case Is = ODCompra
    'crea el registro pptal
      CompPpto = Leer_Comprobante("NU_COMP_REGI")
      Imput = 1
      Valores = "NU_CONS_REGI=" & RegPPto & Coma
      Valores = Valores & "FE_PERI_REGI=" & CDbl(Year(Documento.Encabezado.FechaDocumento)) & Coma
      Valores = Valores & "FE_FECH_REGI=" & FFechaIns(Documento.Encabezado.FechaDocumento) & Coma
      Valores = Valores & "CD_DEPE_REGI=" & Comi & Me.TxtPpto(1) & Comi & Coma
      Valores = Valores & "NU_CERT_REGI=" & CDbl(Me.TxtPpto(0)) & Coma
      Valores = Valores & "CD_TERC_REGI=" & Comi & Trim(Me.txtDocum(5)) & Comi & Coma
      Valores = Valores & "DE_DESC_REGI='ORDEN DE COMPRA DE INVENTARIOS No. " & CDbl(NumeroCompro) & Comi & Coma
      'Valores = Valores & "CD_ORDE_REGI=" & OrdeSel & Coma 'JAUM T33493-R32409 Se deja linea en comentario
      'Valores = Valores & "CD_ORDE_REGI = '" & OrdeSel & "', " 'JAUM T33493-R32409 'JAUM T33864 se deja linea en comentario
      Valores = Valores & "CD_ORDE_REGI = '" & OrdeSel & Comi & Coma 'JAUM T33864
      Valores = Valores & "VL_VALO_REGI=" & CDbl(Me.TxtPpto(2)) & Coma
      Valores = Valores & "VL_COMP_REGI=0" & Coma
      Valores = Valores & "VL_NOUS_REGI=0" & Coma
      Valores = Valores & "CD_TICO_REGI=" & Comi & Contrato & Comi & Coma
      Valores = Valores & "NU_CONT_REGI=" & CDbl(NumeroCompro) & Coma
      'Valores = Valores & "ID_ESTA_REGI='1'"
      Valores = Valores & "ID_ESTA_REGI= " & Comi & "1" & Comi & Coma 'GAVL R2470 04/01/2011
      'INICIO GAVL R2470 03/01/2011
      'Valores = Valores & "NU_DIVI_REGI= " & Comi & "0" & Comi & Coma
      'INCIO GAVL T4266
      StFecha = (Documento.Encabezado.FechaDocumento)
      StFechaV = (txtDocum(21) & "/" & txtDocum(22) & "/" & txtDocum(23))
      StDiaVig = DateDiff("d", StFecha, StFechaV, vbMonday)
      Valores = Valores & "NU_DIVI_REGI= " & Comi & StDiaVig & Comi & Coma
      'FIN GAVL T4266
      Valores = Valores & "FE_EXPI_REGI=" & FFechaIns(txtDocum(21) & "/" & txtDocum(22) & "/" & txtDocum(23)) & ""
      'FIN GAVL R2470 03/01/2011
      Result = DoInsertSQL("REGISTRO_PPTO", Valores)
      If Result <> FAIL Then
         Condicion = "NU_CONS_CERT=" & CDbl(Me.TxtPpto(0))
         Result = DoUpdate("CERTIFICADO", "VL_COMP_CERT=VL_COMP_CERT+" & CDbl(Me.TxtPpto(2)), Condicion)
         If Result = FAIL Then Exit Sub
         
         For X = 0 To UBound(MatPpto(), 2)
            If MatPpto(0, X) <> NUL$ Then
               Valores = "NU_REGI_REAR=" & RegPPto & Coma
               Valores = Valores & "CD_ARTI_REAR=" & Comi & MatPpto(0, X) & Comi & Coma
'               Valores = Valores & "VL_REGI_REAR=" & PptoArti(3, x) & Coma

'              'REOL M2552
'               If UBound(PptoArti, 2) = X Then
'                Valores = Valores & "VL_REGI_REAR=" & PptoArti(3, X) & Coma
'               Else
'                Valores = Valores & "VL_REGI_REAR= 0" & Coma
'               End If
              'FIN REOL
              
              'PJCA M3212
               If MatPpto(3, X) <> 0 Then
                 'If PptoArti(3, x) = NUL$ Then PptoArti(3, x) = 0
                 'Valores = Valores & "VL_REGI_REAR=" & PptoArti(3, x) & Coma
                '-----------------------------------------------------------------------------------------
                'DAHV M4217 - INICIO
                    BoValida = False
                    For Incont = 0 To UBound(PptoArti(), 2)
                       If MatPpto(0, X) = PptoArti(0, Incont) Then
                           If PptoArti(3, Incont) = NUL$ Then PptoArti(3, Incont) = 0
                           BoValida = True
                           Exit For
                       End If
                    Next
                    
                 If BoValida = False Then
                    Valores = Valores & "VL_REGI_REAR=0" & Coma
                 Else
                    Valores = Valores & "VL_REGI_REAR=" & PptoArti(3, Incont) & Coma 'OMOG T17510 SE VUELVE A DEJAR COMO ESTABA
                    'Valores = Valores & "VL_REGI_REAR=" & MatPpto(3, x) & Coma 'OMOG T17510 SE ESTABLECE EN COMENTARIO YA QUE NO ERA NECESARIO
                 End If
                'DAHV M4217 - FIN
                 '-----------------------------------------------------------------------------------------
                
               Else
                 Valores = Valores & "VL_REGI_REAR=0" & Coma
               End If
              'PJCA M3212
               
               Valores = Valores & "VL_CDPD_REAR=" & MatPpto(2, X) & Coma
               Valores = Valores & "VL_COMP_REAR=0,"
               Valores = Valores & "VL_NOUS_REAR=0"
               Result = DoInsertSQL("REG_ARTICULO", Valores)
               If Result = FAIL Then Exit For
               Condicion = "NU_CERT_CEAR=" & CDbl(Me.TxtPpto(0)) & " AND CD_ARTI_CEAR=" & Comi & MatPpto(0, X) & Comi
               Result = DoUpdate("CDP_ARTICULO", "VL_COMP_CEAR = VL_COMP_CEAR + " & CDbl(MatPpto(3, X)), Condicion)
               If Result = FAIL Then Exit For
               If Result <> FAIL Then Call Actualiza_Gastos("VL_REGI_PERI", CStr(MatPpto(0, X)), CDbl(MatPpto(3, X)), CByte(Month(Documento.Encabezado.FechaDocumento)))
               'If Result <> FAIL Then Call Cuentas_Contable_Ppto(CStr(MatPpto(0, x)), CDbl(MatPpto(3, x)))
'               If Result <> FAIL Then Call Cuentas_Contable_Ppto(CStr(MatPpto(0, x)), CDbl(MatPpto(3, x)), 1)   'PJCA M1069
               'If Result <> FAIL Then Call Cuentas_Contable_Ppto(CStr(MatPpto(0, x)), 1)    'PJCA M1069     'DEPURACION DE CODIGO'INICIO LDCR R9384/T9676 SE COLOCA EN COMENTARIO POR REQ.
               If Result = FAIL Then Exit For
            End If
         Next
         Valores = "NU_REGI_REFE=" & RegPPto & Coma
         Valores = Valores & "FE_FEPA_REFE=" & FFechaIns(Documento.Encabezado.FechaDocumento) & Coma
         Valores = Valores & "VL_PAGO_REFE=" & CDbl(Me.TxtPpto(2))
         If Result <> FAIL Then Result = DoInsertSQL("REGISTRO_FECHA", Valores)
         
         If Result <> FAIL Then Call Mensaje1("Registro Presupuestal No." & RegPPto, 2)
      End If
    
    Case Is = Entrada
        
        'INICIO GAVL T4959
        ReDim ArrFech(0)
        Result = LoadData("REGISTRO_PPTO", "FE_EXPI_REGI", "NU_CONS_REGI= " & Me.TxtPpto(0), ArrFech())
        If DateDiff("d", CDate((Documento.Encabezado.FechaDocumento)), CDate(ArrFech(0))) < 0 Then
          MsgBox "La fecha del registro presupuestal ya caduc�: " & Trim(ArrFech(0)), vbInformation
          BoResult = True
          Exit Sub
        End If
        'FIN GAVL T4959
        'JLPB T30752 INICIO Se deja en comentario las siguientes lineas
        ''SKRV T15377 INICIO
        'If BoBandera = True Then
        '    Me.TxtPpto(2) = Me.TxtPpto(2) - TxtEntra(18)
        'End If
        ''SKRV T15377 FIN
        'JLPB T30752 FIN
         CompPpto = Leer_Comprobante("NU_COMP_OBLI")
         'JAUM T33493-R32409 Inicio Se deja bloque en comentario
         'Valores = "NU_CONS_OBLI=" & RegPPto & Coma & _
         "FE_PERI_OBLI=" & CDbl(Year(Documento.Encabezado.FechaDocumento)) & Coma & _
         "FE_FECH_OBLI=" & FFechaIns(Documento.Encabezado.FechaDocumento) & Coma & _
         "CD_DEPE_OBLI=" & Comi & Me.TxtPpto(1) & Comi & Coma & _
         "NU_REGI_OBLI=" & CDbl(Me.TxtPpto(0)) & Coma & _
         "CD_TERC_OBLI=" & Comi & Trim(Me.txtDocum(5)) & Comi & Coma & _
         "DE_DESC_OBLI=" & Comi & "ENTRADA ALMACEN No." & CDbl(NumeroCompro) & Comi & Coma & _
         "CD_ORDE_OBLI=" & OrdeSel & Coma & _
         "VL_VALO_OBLI=" & CDbl(Me.TxtPpto(2)) & Coma & _
         "ID_ESTA_OBLI='1', VL_PAGO_OBLI=0"
         'JAUM T33864 Inicio se deja bloque en comentario
         'Valores = "NU_CONS_OBLI = " & RegPPto & ", FE_PERI_OBLI = " & CDbl(Year(Documento.Encabezado.FechaDocumento))
         'Valores = Valores & ", FE_FECH_OBLI = " & FFechaIns(Documento.Encabezado.FechaDocumento) & ", "
         'Valores = Valores & "CD_DEPE_OBLI = '" & Me.TxtPpto(1) & "', NU_REGI_OBLI = " & CDbl(Me.TxtPpto(0)) & ", "
         'Valores = Valores & "CD_TERC_OBLI = '" & Trim(Me.txtDocum(5)) & "', "
         'Valores = Valores & "DE_DESC_OBLI = 'ENTRADA ALMACEN No." & CDbl(NumeroCompro) & "', "
         'Valores = Valores & "CD_ORDE_OBLI = '" & OrdeSel & "', VL_VALO_OBLI = " & CDbl(Me.TxtPpto(2)) & ", "
         'Valores = Valores & "ID_ESTA_OBLI = '1', VL_PAGO_OBLI = 0"
         Valores = "NU_CONS_OBLI = " & RegPPto & Coma & " FE_PERI_OBLI = " & CDbl(Year(Documento.Encabezado.FechaDocumento))
         Valores = Valores & Coma & " FE_FECH_OBLI = " & FFechaIns(Documento.Encabezado.FechaDocumento) & Coma
         Valores = Valores & "CD_DEPE_OBLI = " & Comi & Me.TxtPpto(1) & Comi & Coma & " NU_REGI_OBLI = " & CDbl(Me.TxtPpto(0)) & Coma
         Valores = Valores & "CD_TERC_OBLI = " & Comi & Trim(Me.txtDocum(5)) & Comi & Coma
         Valores = Valores & "DE_DESC_OBLI = " & Comi & "ENTRADA ALMACEN No." & CDbl(NumeroCompro) & Comi & Coma
         Valores = Valores & "CD_ORDE_OBLI = " & Comi & OrdeSel & Comi & Coma & " VL_VALO_OBLI = " & CDbl(Me.TxtPpto(2)) & Coma
         Valores = Valores & "ID_ESTA_OBLI = '1', VL_PAGO_OBLI = 0"
         'JAUM T33864 Fin
         'JAUM T33493-R32409 Fin
         Result = DoInsertSQL("OBLIGACION", Valores)
         
         If (Result <> FAIL) Then Result = DoUpdate("REGISTRO_PPTO", "VL_COMP_REGI = VL_COMP_REGI + " & CDbl(Me.TxtPpto(2)), "NU_CONS_REGI = " & CDbl(Me.TxtPpto(0)))
         If Result <> FAIL Then
            Imput = Imput + 1
            For X = 0 To UBound(MatPpto(), 2)
                If MatPpto(0, X) <> NUL$ Then
                   If MatPpto(3, X) = NUL$ Then MatPpto(3, X) = 0
                   If CDbl(MatPpto(3, X)) > 0 Then
                        '-----------------------------------------------------------------------------------------
                        'DAHV M4217 - INICIO
                        
                        
'                     Valores = "NU_OBLI_OBAR=" & RegPPto & Coma & _
'                               "CD_ARTI_OBAR=" & Comi & MatPpto(0, x) & Comi & Coma & _
'                               "VL_OBLI_OBAR=" & CDbl(PptoArti(3, x)) & Coma & _
'                               "VL_REGD_OBAR=" & CDbl(MatPpto(2, x)) & Coma & _
'                               "VL_PAGO_OBAR=0"

                        
                            BoValida = False
                            For Incont = 0 To UBound(PptoArti(), 2)
                               If MatPpto(0, X) = PptoArti(0, Incont) Then
                                   If PptoArti(3, Incont) = NUL$ Then PptoArti(3, Incont) = 0
                                   BoValida = True
                                   Exit For
                               End If
                            Next
                            
                         If BoValida = False Then
                                Valores = "NU_OBLI_OBAR=" & RegPPto & Coma & _
                                               "CD_ARTI_OBAR=" & Comi & MatPpto(0, X) & Comi & Coma & _
                                               "VL_OBLI_OBAR=" & CDbl(0) & Coma & _
                                               "VL_REGD_OBAR=" & CDbl(MatPpto(2, X)) & Coma & _
                                               "VL_PAGO_OBAR=0"
                         Else
                                Valores = "NU_OBLI_OBAR=" & RegPPto & Coma & _
                                                "CD_ARTI_OBAR=" & Comi & MatPpto(0, X) & Comi & Coma & _
                                                "VL_OBLI_OBAR=" & CDbl(PptoArti(3, Incont)) & Coma & _
                                                "VL_REGD_OBAR=" & CDbl(MatPpto(2, X)) & Coma & _
                                                "VL_PAGO_OBAR=0"

                         End If
                        'DAHV M4217 - FIN
                         '-----------------------------------------------------------------------------------------
                
                   
                   

                     Result = DoInsertSQL("OBL_ARTICULO", Valores)
                     If Result = FAIL Then Exit For
                     Condicion = "NU_REGI_REAR = " & CDbl(Me.TxtPpto(0)) & " AND CD_ARTI_REAR = " & Comi & MatPpto(0, X) & Comi
                     Result = DoUpdate("REG_ARTICULO", "VL_COMP_REAR = VL_COMP_REAR + " & CDbl(MatPpto(3, X)), Condicion)
                     If Result = FAIL Then Exit For
                     If Result <> FAIL Then Call Actualiza_Gastos("VL_OBLI_PERI", CStr(MatPpto(0, X)), CDbl(MatPpto(3, X)), CByte(Month(Documento.Encabezado.FechaDocumento)))
                     'If Result <> FAIL Then Call Cuentas_Contable_Ppto(CStr(MatPpto(0, x)), CDbl(MatPpto(3, x)))
'                     If Result <> FAIL Then Call Cuentas_Contable_Ppto(CStr(MatPpto(0, x)), CDbl(MatPpto(3, x)), 2)  'PJCA M1069
                    'If Result <> FAIL Then Call Cuentas_Contable_Ppto(CStr(MatPpto(0, x)), 2)   'PJCA M1069     'DEPURACION DE CODIGO 'INICIO LDCR R9384/T9676 SE COLOCA EN COMENTARIO POR REQ.
                   End If
                   If Result = FAIL Then Exit For
                End If
            Next X
         End If
         If (Result <> FAIL) Then Call Mensaje1("No. Obligaci�n: " & RegPPto, 2)
   End Select

End Sub
'INICIO LDCR R9384/T9676 SE COLOCA EN COMENTARIO POR REQ.
'Private Sub Cuentas_Contable_Ppto(Articulo As String, valor As Double)
'Tipo: 1 Registro / 2 obligaci�n
'Private Sub Cuentas_Contable_Ppto(Articulo As String, valor As Double, tipo As Integer)   'PJCA M1069
'Private Sub Cuentas_Contable_Ppto(Articulo As String, tipo As Integer)    'PJCA M1069   'DEPURACION DE CODIGO
''Dim i As Byte      'DEPURACION DE CODIGO
'Dim ArrP(1)
'    If CompPpto = NUL$ Then Exit Sub
'    'Result = LoadData("CUENTAS_GASTOS", "NU_COMP_CUEN, NU_CDPE_CUEN", "CD_ARTI_CUEN =" & Comi & Articulo & Comi, ArrP())
'    'If tipo = 1 Then Result = LoadData("CUENTAS_GASTOS", "NU_COMP_CUEN, NU_CDPE_CUEN", "CD_ARTI_CUEN =" & Comi & Articulo & Comi, ArrP())   'PJCA M1069 'HRR R1678-1681
'    If tipo = 1 Then Result = LoadData("CUENTAS_GASTOS", "NU_COMP_CUEN, NU_XEJE_CUEN", "CD_ARTI_CUEN =" & Comi & Articulo & Comi, ArrP())   'PJCA M1069 'HRR R1678-1681
'    If tipo = 2 Then Result = LoadData("CUENTAS_GASTOS", "NU_OBLI_CUEN, NU_COMP_CUEN", "CD_ARTI_CUEN =" & Comi & Articulo & Comi, ArrP())   'PJCA M1069
'    If ArrP(0) <> NUL$ And ArrP(1) <> NUL$ Then
'        If Result <> FAIL Then Call Actualiza_Contabilidad(CStr(ArrP(0)), "D", Depesel, Trim(Me.txtDocum(5)), CDbl(Me.TxtPpto(2)), RegPPto, Documento.Encabezado.FechaDocumento, CompPpto, "REGISTRO PPTAL (INVENTARIOS) ARTICULO: " + Articulo, Imput)
'        If Result <> FAIL Then Imput = Imput + 1
'        If Result <> FAIL Then Call Actualiza_Contabilidad(CStr(ArrP(1)), "C", Depesel, Trim(Me.txtDocum(5)), CDbl(Me.TxtPpto(2)), RegPPto, Documento.Encabezado.FechaDocumento, CompPpto, "REGISTRO PPTAL (INVENTARIOS) ARTICULO: " + Articulo, Imput)
'        If Result <> FAIL Then Imput = Imput + 1
'    Else
'        Call Mensaje1("No se han especificado cuentas al art�culo: " & Articulo, 2)
'        Result = FAIL
'        Exit Sub
'    End If
'End Sub
'FIN LDCR R9384/T9676 SE COLOCA EN COMENTARIO POR REQ.


Private Function Valida_PPto() As Integer
Valida_PPto = 1
  
  Select Case Documento.Encabezado.TipDeDcmnt
   Case Is = ODCompra
        If Me.TxtPpto(0) = NUL$ Then Me.TxtPpto(0) = 0
        If Me.TxtPpto(1) = NUL$ Then Me.TxtPpto(1) = 0
        If Contrato = NUL$ Then
           Call Mensaje1("Debe especificar el Contrato por parametrizaci�n", 3): Call MouseNorm
           Valida_PPto = 0: Exit Function
        End If
        If Me.TxtPpto(0) = 0 Then
           Call Mensaje1("Debe especificar el CDP", 3): Call MouseNorm
           Valida_PPto = 0: Exit Function
        End If
        If Me.TxtPpto(0) = 0 Then
           Call Mensaje1("Debe especificar la dependencia", 3): Call MouseNorm
           Valida_PPto = 0: Exit Function
        End If
        If MatPpto(0, 0) = NUL$ Then
           Call Mensaje1("No se especificaron articulos presupuestales", 3)
           Call MouseNorm: Valida_PPto = 0: Exit Function
        End If
   Case Is = Entrada
        If Me.TxtPpto(0) = NUL$ Then Me.TxtPpto(0) = 0
        If Me.TxtPpto(0) = 0 Then
          Call Mensaje1("Debe especificar el Registro o Reserva", 2): Call MouseNorm
          Valida_PPto = 0: Exit Function
        End If
        If MatPpto(0, 0) = NUL$ Then
          Call Mensaje1("No se especificaron rubros presupuestales", 2): Call MouseNorm
          Valida_PPto = 0: Exit Function
        End If
   End Select
End Function
'Pedro

'Private Sub Graba_NotaDeb(ByVal fechag As Variant, ByVal fechav As Variant, Cepto As String, Umero As Long)
Private Sub Graba_NotaDeb(ByVal fechag As Variant, Umero As Long)   'DEPURACION DE CODIGO
   'Dim Factura As Double, Compro As Double
   'Dim Factura As Double           'PJCA M1114        'DEPURACION DE CODIGO
   Dim Compro As String            'PJCA M1114
   Dim CXP As Double, Saldo As Double
   'JLPB T15675 INICIO
   Dim BoTComp As Boolean 'GUARDA TRUE SI LA ENTRADA TIENE COMPRA RELACIONADA
   Dim InI As Integer
   Dim InB As Integer
   Dim DbVTNDeb As Double 'GUARDA EL VALOR DE LA NOTA DEBITO
   Dim DbVDesImp As Double 'GUARDA EL TOTAL DE LOS IMPUESTOS Y DESCUENTOS
   Dim DbImpu As Double 'ALMACENA EL IVA DE CADA REGISTRO
   'JAUM T28836 Inicio
   Dim VrPosi() As Variant 'Almacena la posici�n para traer los impuestos y descuentos de los comprobantes hijos
   Dim VrImp() As Variant 'Almacena los impuestos de cada concepto hijo
   Dim VrDesc() As Variant 'Almacena los descuentos de cada concepto hijo
   Dim InX As Integer 'Contador
   Dim DbSalCxP As Double 'Almacena el saldo de la CxP
   'JAUM T28836 Fin
   BoTComp = False
   DbVTNDeb = 0
   DbVDesImp = 0
   DbImpu = 0
   InB = 1
   'JLPB T15675 FIN
   ReDim ArrCxP(0)
   Result = LoadData("IN_ENCABEZADO", "NU_CXP_ENCA", "NU_AUTO_ENCA=" & AutoDocCargado, ArrCxP)
   If Result <> FAIL And Encontro Then
      If ArrCxP(0) = NUL$ Then Exit Sub
      CXP = CDbl(ArrCxP(0))
   End If
   ReDim ArrCxP(0)
   'Condicion = "CD_CONCE_CXP='CINV'" & " AND CD_TERC_CXP=" & Comi & Trim(Me.txtDocum(5)) & Comi JAUM T28836 Se deja linea en comentario
   Condicion = "CD_TERC_CXP=" & Comi & Trim(Me.txtDocum(5)) & Comi 'JAUM T28836
   Condicion = Condicion & " AND CD_NUME_CXP=" & CXP
   'Result = LoadData("C_X_P", "( VL_NETO_CXP - VL_CANC_CXP + VL_NCRE_CXP - VL_NDEB_CXP )", Condicion, ArrCxP) 'OMOG T19943 SE ESTABLECE EN COMENTARIO
   'Result = LoadData("C_X_P", "( VL_NETO_CXP - VL_CANC_CXP + VL_NCRE_CXP - VL_NDEB_CXP - CASE WHEN VL_IMPU_CXP<0 THEN VL_IMPU_CXP ELSE 0 END)", Condicion, ArrCxP) 'OMOG T19943 'EACT T15675 SE ESTABLECE EN COMENTARIO
   Result = LoadData("C_X_P", "( VL_NETO_CXP - VL_CANC_CXP + VL_NCRE_CXP - VL_NDEB_CXP - CASE WHEN VL_IMPU_CXP<0 THEN VL_IMPU_CXP ELSE 0 END- CASE WHEN VL_DESC_CXP<0 THEN VL_DESC_CXP ELSE 0 END)", Condicion, ArrCxP) 'EACT T15675
   If Result <> FAIL And Encontro Then
      If ArrCxP(0) = NUL$ Then ArrCxP(0) = 0
         Saldo = CDbl(ArrCxP(0))
         Saldo = Aplicacion.Formatear_Valor(Saldo)
      End If
      'JLPB T15675 INICIO
      'JAUM T28836 Inicio se deja bloque en comentario
      'JAUM T28370-R22535 Inicio
      'If Not Encontro Then
      'Campos = "( VL_NETO_CXP - VL_CANC_CXP + VL_NCRE_CXP - VL_NDEB_CXP - "
      'Campos = Campos & "CASE WHEN VL_IMPU_CXP < 0 THEN VL_IMPU_CXP ELSE 0 END - CASE WHEN VL_DESC_CXP < 0 THEN VL_DESC_CXP ELSE 0 END)"
      'Campos = "( VL_NETO_CXP - VL_CANC_CXP + VL_NCRE_CXP - VL_NDEB_CXP + "
      'Condicion = "CD_CONCE_CXP='CINM'" & " AND CD_TERC_CXP=" & Comi & Trim(Me.txtDocum(5)) & Comi
      'Condicion = Condicion & " AND CD_NUME_CXP=" & CXP
      'Result = LoadData("C_X_P", Campos, Condicion, ArrCxP)
      'If Result <> FAIL And Encontro Then
      '   If ArrCxP(0) = NUL$ Then ArrCxP(0) = 0
      '   Saldo = Aplicacion.Formatear_Valor(CDbl(ArrCxP(0)))
      'End If
      'End If
      'JAUM T28370-R22535 Fin
      'JAUM T28836 Fin
   ReDim ArrCxP(0)
   Result = LoadData("IN_COMPRA", "NU_AUTO_COMP", "NU_CXP_COMP=" & CXP, ArrCxP)
   If Result <> FAIL And Encontro Then
      BoTComp = True
      For InI = InPosc To UBound(CtasConta, 2)
         DbVDesImp = DbVDesImp + CtasConta(1, InI)
      Next
      For InI = 1 To GrdArticulos.Rows - 1
         DbImpu = (GrdArticulos.TextMatrix(InI, 10) * GrdArticulos.TextMatrix(InI, 11))
         DbImpu = (DbImpu * GrdArticulos.TextMatrix(InI, 13)) / 100
         If BoAproxCent Then DbImpu = AproxCentena(Round(DbImpu))
         DbImpu = DbImpu + (GrdArticulos.TextMatrix(InI, 10) * GrdArticulos.TextMatrix(InI, 11))
         DbVTNDeb = DbVTNDeb + DbImpu
      Next
      'JAUM T28836 Inicio Se deja bloque en comentario
      'JAUM T28370-R22535 Inicio
      'ReDim arr(0)
      'Desde = "C_X_P, CONCEPTO"
      'Condicion = "CD_CONCE_CXP = CD_CODI_CONC AND CD_NUME_CXP = '" & CXP & Comi
      'Result = LoadData(Desde, "TX_MULT_CONC", Condicion, arr)
      'If arr(0) = "S" Then
         'ReDim ArrCxP(0, 0)
         'Condicion = "NU_CXP_ENCA = " & CXP
         'Result = LoadMulData("IN_ENCABEZADO", "NU_AUTO_ENCA", Condicion, ArrCxP)
         'If Result <> FAIL And Encontro Then
            'For InI = 1 To GrdArticulos.Rows - 1
               'For InB = 0 To UBound(ArrCxP, 2)
                  'If Format(GrdArticulos.TextMatrix(InI, 24), "##") = ArrCxP(0, InB) Then
                     'If InI = 1 Then
                        'TxPosicion = InB + 1
                        'If InI <> GrdArticulos.Rows - 1 Then
                           'Do While GrdArticulos.TextMatrix(InI, 24) = GrdArticulos.TextMatrix(InI + 1, 24)
                              'InI = InI + 1
                              'If InI = GrdArticulos.Rows - 1 Then Exit Do
                           'Loop
                        'End If
                        'GoTo Siguiente
                     'Else
                        'TxPosicion = TxPosicion & Coma & InB + 1
                        'If InI <> GrdArticulos.Rows - 1 Then
                           'Do While GrdArticulos.TextMatrix(InI, 24) = GrdArticulos.TextMatrix(InI + 1, 24)
                              'InI = InI + 1
                              'If InI = GrdArticulos.Rows - 1 Then Exit Do
                           'Loop
                        'End If
                        'GoTo Siguiente
                     'End If
                  'End If
               'Next
'Siguiente:
            'Next
         'End If
         'ReDim ArrCxP(0)
         'Condicion = "CD_NUME_CXP_RCPCP = " & CXP & " AND NU_POSI_RCPCP IN (" & TxPosicion & ")"
         'Result = LoadData("R_CXP_CXP", "SUM((VL_NETO_RCPCP - VL_CANC_RCPCP + VL_NCRE_RCPCP - VL_NDEB_RCPCP))", Condicion, ArrCxP)
         'If InCantT = (InCantD + InCantET) Or (DbVTNDeb - DbVDesImp) > ArrCxP(0) Then
            'DbVTNDeb = ArrCxP(0)
         'Else
            'DbVTNDeb = DbVTNDeb - DbVDesImp
         'End If
      'Else
      'JAUM T28370-R22535 Fin
      'JAUM T28836 Fin
      DbVDesImp = DbVDesImp + (CDbl(TxtEntra(11)) + CDbl(TxtEntra(13)) + CDbl(TxtEntra(18)) + CDbl(TxtEntra(19))) 'JLPB T42097
      ReDim ArrCxP(0)
      Result = LoadData("C_X_P", "(VL_NETO_CXP - VL_CANC_CXP + VL_NCRE_CXP - VL_NDEB_CXP)", "CD_NUME_CXP=" & CXP, ArrCxP)
      'If InCantT = (InCantD + InCantET) Or (DbVTNDeb - DbVDesImp) > ArrCxP(0) Then 'JLPB T37799 Se deja en comentario
      If DbCantT = (DbCantD + DbCantET) Or (DbVTNDeb - DbVDesImp) > ArrCxP(0) Then 'JLPB T37799
         DbVTNDeb = ArrCxP(0)
      Else
         DbVTNDeb = DbVTNDeb - DbVDesImp
      End If
      DbSalCxP = ArrCxP(0) 'JAUM T28836
      'End If 'JAUM T28370-R22535 JAUM T28836 Se deja linea en comentario
   End If
   If DbVTNDeb = 0 Then Exit Sub
   'JLPB T15675 FIN
   If Saldo = 0 Then
      Call Mensaje1("La CxP " & CXP & " ya esta cancelada, no se puede Devolver la Entrada", 2)
      Result = FAIL: Exit Sub
   End If
   'JLPB T15675 INICIO
   If BoTComp Then
      'If (Saldo - DbVDesImp) < Aplicacion.Formatear_Valor(CDbl(Me.TxtCompra(17).Text) - DbVDesImp) Then JAUM T28836 Se deja linea en comentario
      If (Saldo - DbVDesImp) < (CDbl(Me.TxtCompra(17).Text) - DbVDesImp) Then 'JAUM T28836
         Call Mensaje1("El saldo de la CxP " & CXP & " es menor al valor de la devoluci�n", 2)
         Result = FAIL: Exit Sub
      End If
   Else
   'JLPB T15675 FIN
      If Saldo < Aplicacion.Formatear_Valor(CDbl(Me.TxtCompra(17).Text)) Then 'OMOG T15675 SE ESTABLECE EN COMENTARIO' EACT T15675 SE DESCOMENTAREA
      'If (Saldo + DbImpuesto + 1) < Aplicacion.Formatear_Valor(CDbl(Me.TxtCompra(17).Text)) Then 'OMOG T15675 'EACT T15675 S ESTABLECE EN COMENTARIO
         Call Mensaje1("El saldo de la CxP " & CXP & " es menor al valor de la devoluci�n", 2)
         Result = FAIL: Exit Sub
      End If
   End If 'JLPB T15675
   Valores = "CD_CODI_NDEB = " & Umero
   'Valores = Valores & Coma & "FE_FECH_NDEB = " & Comi & fechag & Comi
   'Valores = Valores & Coma & "FE_FECH_NDEB = " & FFechaCon(fechag) 'AASV M4858 Depuracion 'EACT T19065
   'Valores = Valores & Coma & "FE_FECH_NDEB = '" & Format(FechaServer, "yyyymmdd") & Comi 'EACT T19065 'JAUM T29758 Se deja linea en comentario
   Valores = Valores & Coma & "FE_FECH_NDEB = '" & Format(Nowserver, FormatF & " HH:MM") & Comi 'JAUM T29758
   Valores = Valores & Coma & "CD_COUSRE_NDEB = " & Comi & Cambiar_Comas_Comillas(UserId) & Comi 'EACT T19065
   Valores = Valores & Coma & "CD_CECO_NDEB = " & Comi & Aplicacion.CxP_Dependencia & Comi  'DepeCxPsel
   Valores = Valores & Coma & "CD_TERC_NDEB = " & Comi & Cambiar_Comas_Comillas(Trim(Me.txtDocum(5))) & Comi
   'Valores = Valores & Coma & "VL_VALO_NDEB = " & CDbl(Me.TxtCompra(17).Text) 'OMOG T15675 SE ESTABLECE EN COMENTARIO
   'OMOG T15675 INICIO
   'If DbImpuesto > 0 Then EACT T15675 SE ESTABLECE EN COMENTARI0
   'JLPB T15675 INICIO
   If BoTComp Then
      Valores = Valores & Coma & "VL_VALO_NDEB = " & CDbl(DbVTNDeb)
   Else
   'JLPB T15675 FIN
      If DbImpuesto > 0 Or DbDescuento > 0 Then 'EACT T15675
         'Valores = Valores & Coma & "VL_VALO_NDEB = " & CDbl(Me.TxtCompra(17).Text) - DbImpuesto 'EACT T15675 SE ESTABLECE EN COMENTARIO
         Valores = Valores & Coma & "VL_VALO_NDEB = " & CDbl(Me.TxtCompra(17).Text) - DbImpuesto - DbDescuento 'EACT T15675
      Else
         Valores = Valores & Coma & "VL_VALO_NDEB = " & CDbl(Me.TxtCompra(17).Text)
      End If
   End If 'JLPB T15675
   'OMOG T15675 FIN
   Valores = Valores & Coma & "ID_ESTA_NDEB = " & "1"  'Estado
   Valores = Valores & Coma & "DE_OBSE_NDEB = " & Comi & "DEVOLUCION A PROVEEDOR No." & Me.txtNumero & Comi
   'Valores = Valores & Coma & "DE_MONT_NDEB = " & Comi & Monto_Escrito(CStr(Me.TxtCompra(17).Text)) & Comi 'OMOG T15675 SE ESTABLECE EN COMENTARIO
   'OMOG T15675 INICIO
   'If DbImpuesto > 0 Then 'EACT T15675 SE ESTABLECE EN COMENTARIO
   'JLPB T15675 INICIO
   If BoTComp Then
      Valores = Valores & Coma & "DE_MONT_NDEB = " & Comi & Monto_Escrito(CStr(DbVTNDeb)) & Comi
   Else
   'JLPB T15675 FIN
      If DbImpuesto > 0 Or DbDescuento > 0 Then 'EACT T15675
         'Valores = Valores & Coma & "DE_MONT_NDEB = " & Comi & Monto_Escrito(CStr(Me.TxtCompra(17).Text) - DbImpuesto) & Comi 'EACT T15675 SE ESTABLECEEN COMENTARIO
         Valores = Valores & Coma & "DE_MONT_NDEB = " & Comi & Monto_Escrito(CStr(Me.TxtCompra(17).Text) - DbImpuesto - DbDescuento) & Comi 'EACT T15675
      Else
         Valores = Valores & Coma & "DE_MONT_NDEB = " & Comi & Monto_Escrito(CStr(Me.TxtCompra(17).Text)) & Comi
      End If
   End If 'JLPB T15675
   'OMOG T15675 FIN
   Result = DoInsertSQL("NOTA_DEBITO_P", Valores)
   If Result <> FAIL Then Call TrasladoMovimiento("R_TRASMOV_NDP", Umero, "CXP")     'JACC M4759
   If (Result <> FAIL) Then    'Actualiza el consecutivo
      ReDim ArrCxP(0)         'comprobante del concepto Nota debito CxP
      Result = LoadData("CONCEPTO", "CD_COMP_CONC", "CD_CODI_CONC='NDEP'", ArrCxP)
      If Result <> FAIL And Encontro Then
         If ArrCxP(0) <> NUL$ Then Result = DoUpdate("TC_COMPROBANTE", "NU_CONS_COMP=" & Umero, "CD_CODI_COMP=" & Comi & ArrCxP(0) & Comi)              'Actualiza el consecutivo
      End If
   End If
   'JAUM T28370-R22535 Inicio
   ReDim Arr(0)
   Desde = "C_X_P, CONCEPTO"
   Condicion = "CD_CONCE_CXP = CD_CODI_CONC AND CD_NUME_CXP = '" & CXP & Comi
   Result = LoadData(Desde, "TX_MULT_CONC", Condicion, Arr)
   If Arr(0) = "S" Then
      'If Result <> FAIL Then Result = Actualiza_CxP_Mul(Umero, CXP, Saldo, DbVTNDeb, BoTComp) 'JAUM T28836 Se deja linea en comentario
      'If Result <> FAIL Then Result = Actualiza_CxP_Mul(Umero, CXP, Saldo, DbVTNDeb, BoTComp, (InCantT = (InCantD + InCantET) Or (DbVTNDeb - DbVDesImp) > DbSalCxP)) 'JLPB T37799 Se deja en comentario
      If Result <> FAIL Then Result = Actualiza_CxP_Mul(Umero, CXP, Saldo, DbVTNDeb, BoTComp, (DbCantT = (DbCantD + DbCantET) Or (DbVTNDeb - DbVDesImp) > DbSalCxP)) 'JLPB T37799
   Else
   'JAUM T28370-R22535 Fin
      'If (Result <> FAIL) Then Result = Actualiza_CxP(Umero, CXP, Saldo) ''Graba las CXP 'JLPB T15675 SE DEJA EN COMENTARIO
      If (Result <> FAIL) Then Result = Actualiza_CxP(Umero, CXP, Saldo, DbVTNDeb, BoTComp) 'JLPB T15675
   End If 'JAUM T28370-R22535
   If (Result <> FAIL) Then    'Busca el comprobante contable de la Nota debito
      ReDim Arr(0)
      Result = LoadData("CONCEPTO", "CD_COMP_CONC", "CD_CODI_CONC='NDEP'", Arr())
      If Result <> FAIL Then
         If Encontro Then Compro = Arr(0) Else Call Mensaje1("No se encontro el comprbante contable de la Nota Debito a CxP", 2)
      End If
   End If
   'If (Result <> FAIL) And CmdCuentas.Enabled = True Then Call Guardar_Movimiento_Contable("NDEP", fechag, Compro, Umero, Mid("NOTA DEBITO A CXP " & CxP, 1, 250), NUL$) 'Afecta la contabilidad  'PJCA M1077
   If (Result <> FAIL) Then Call Mensaje1("Se genero la Nota Debito a CxP No. " & Umero, 3)
End Sub

'Private Function Actualiza_CxP(ByVal Numero As Long, NumCxP As Double, ValCxP As Double) As Integer 'JLPB T15675 SE DEJA EN COMENTARIO
Private Function Actualiza_CxP(ByVal Numero As Long, NumCxP As Double, ValCxP As Double, Optional DbVNDeb As Double, Optional BoTComp As Boolean) As Integer 'JLPB T15675
'Dim i As Integer       'DEPURACION DE CODIGO
ReDim Arr(0)

   ''Crea la relaci�n entre la nota cr�dito y las CXP
   Valores = "CD_NDEB_NDCP = " & Numero
   'SRM T36017 INICIO se deja la siguiente linea en comentario.
   'Valores = Valores & Coma & "CD_CONC_NDCP = 'CINV'"
   Result = LoadData("C_X_P", "CD_CONCE_CXP", "CD_NUME_CXP=" & CDbl(NumCxP), Arr)
   Valores = Valores & Coma & "CD_CONC_NDCP =" & Comi & Arr(0) & Comi
   'SRM T36017 FIN
   Valores = Valores & Coma & "CD_NUME_NDCP = " & CDbl(NumCxP)
   Valores = Valores & Coma & "VL_SALD_NDCP = " & CDbl(ValCxP)
   'Valores = Valores & Coma & "VL_NDEB_NDCP = " & CDbl(Me.TxtCompra(17).Text) 'OMOG T15675 SE ESTABLECE EN COMENTARIO
   'OMOG T15675 INICIO
   'EACT T15675 INICIO
   'If DbImpuesto > 0 Then
   'JLPB T15675 INICIO
   If BoTComp Then
      Valores = Valores & Coma & "VL_NDEB_NDCP = " & CDbl(DbVNDeb)
   Else
   'JLPB T15675 FIN
      If DbImpuesto > 0 Or DbDescuento > 0 Then
         'Valores = Valores & Coma & "VL_NDEB_NDCP = " & CDbl(Me.TxtCompra(17).Text) - DbImpuesto - DbTotImp
         Valores = Valores & Coma & "VL_NDEB_NDCP = " & CDbl(Me.TxtCompra(17).Text) - DbImpuesto - DbDescuento
     'FIN INICIO
      Else
         Valores = Valores & Coma & "VL_NDEB_NDCP = " & CDbl(Me.TxtCompra(17).Text)
      End If
   End If 'JLPB T15675
   'OMOG T15675 FIN
   Result = DoInsertSQL("R_NDEB_CXP", Valores)
   If Result <> FAIL Then
   
     '''Actualiza el valor de las notas d�bito de la CXP
     Condicion = "CD_CONCE_CXP='CINV' AND CD_NUME_CXP=" & CDbl(NumCxP)
     'Result = DoUpdate("C_X_P", "VL_NDEB_CXP=VL_NDEB_CXP + " & CDbl(Me.TxtCompra(17).Text), Condicion) 'OMOG T15675 SE ESTABLECE EN COMENTARIO
     'OMOG T15675 INICIO
     'If DbImpuesto > 0 Then EACT 15675 SE DEJA EN COMENTARIO
        'Result = DoUpdate("C_X_P", "VL_NDEB_CXP=VL_NDEB_CXP + " & CDbl(Me.TxtCompra(17).Text - DbImpuesto), Condicion) 'EACT T15675 SE DEJA EN COMENTARIO
      'JLPB T15675 INICIO
      If BoTComp Then
         Result = DoUpdate("C_X_P", "VL_NDEB_CXP=VL_NDEB_CXP + " & CDbl(DbVNDeb), Condicion)
      Else
      'JLPB T15675 FIN
         If DbImpuesto > 0 Or DbDescuento = 0 Then 'EACT T15675
            Result = DoUpdate("C_X_P", "VL_NDEB_CXP=VL_NDEB_CXP + " & CDbl(Me.TxtCompra(17).Text - DbImpuesto - DbDescuento), Condicion) 'EACT T15675
         Else
            Result = DoUpdate("C_X_P", "VL_NDEB_CXP=VL_NDEB_CXP + " & CDbl(Me.TxtCompra(17).Text), Condicion)
         End If
      End If 'JLPB T15675
     'OMOG T15675 FIN
   End If
Actualiza_CxP = Result
End Function

'Busca los movimientos existentes (entradas, compras) hechos usando el documento que se intenta anular
'si existe algun movimiento no deja anular
Private Function BuscarDependientes() As Integer
    
    BuscarDependientes = 0
    
    Select Case Documento.Encabezado.TipDeDcmnt
       Case Is = ODCompra
            ReDim Arr(1)
            Campos = "NU_AUTO_ENCA,NU_COMP_ENCA"
            Desde = "IN_ENCABEZADO, IN_DETALLE"
            Condicion = "NU_AUTO_ORGCABE_DETA=NU_AUTO_ENCA "
            Condicion = Condicion & " AND NU_AUTO_DOCU_ENCA=3"      'Tipo Doc = Entrada
            Condicion = Condicion & " AND NU_AUTO_MODCABE_DETA=" & Documento.Encabezado.AutoDelDocCARGADO
            Condicion = Condicion & " AND TX_ESTA_ENCA<>'A'"
            Result = LoadData(Desde, Campos, Condicion, Arr)
            If Result <> FAIL Then
               If Encontro Then
                  Call Mensaje1("No se puede anular la Orden de Compra (" & Me.txtNumero & ") porque ya fue utilizada", 2)
                  BuscarDependientes = 1
               End If
            End If
       Case Is = Entrada
            ReDim Arr(0)
            
            'HRR M2068
'            Campos = "NU_CXP_ENCA"
'            Condicion = " NU_AUTO_DOCU_ENCA=3"                      'Tipo Doc = Entrada
'            Condicion = Condicion & " AND NU_AUTO_ENCA=" & Documento.Encabezado.AutoDelDocCARGADO
'            Condicion = Condicion & " AND TX_ESTA_ENCA<>'A'"
'            Result = LoadData("IN_ENCABEZADO", Campos, Condicion, Arr)
'            If Result <> FAIL And Encontro Then
'               If Arr(0) <> NUL$ Then
'                  Call Mensaje1("No se puede anular la Entrada (" & Me.txtNumero & ") porque ya tiene una Compra asociada." & vbCr & _
'                                "Realice el proceso Devoluci�n de Entrada", 3)
'                  BuscarDependientes = 1
'               End If
'            End If
            'HRR M2068
            
            'HRR M2068
            'Si la entrada esta relacionada con una compra y la compra esta anulada
            Campos = "NU_AUTO_COMP"
            Condicion = "NU_AUTO_COMP=NU_AUTO_COMP_COEN"
            Condicion = Condicion & " AND NU_AUTO_ENCA_COEN = " & Documento.Encabezado.AutoDelDocCARGADO
            Condicion = Condicion & " AND TX_ESTA_COMP=1"
            Result = LoadData("IN_R_COMP_ENCA, IN_COMPRA", Campos, Condicion, Arr)
            If Result <> FAIL And Encontro Then
                     Call Mensaje1("No se puede anular la Entrada (" & Me.txtNumero & ") porque esta asociada con la compra." & Arr(0) & vbCr & _
                                "Realice el proceso Devoluci�n de Entrada", 3)
                     BuscarDependientes = 1
            End If
            'HRR M2068
       'SKRV T27136 INICIO
       Case Is = Cotizacion
          ReDim Arr(0)
          Desde = "IN_ENCABEZADO , IN_DETALLE"
          Campos = "IN_ENCABEZADO.NU_COMP_ENCA"
          Condicion = "NU_AUTO_ORGCABE_DETA=NU_AUTO_ENCA"
          Condicion = Condicion & " AND NU_AUTO_MODCABE_DETA=" & Documento.Encabezado.AutoDelDocCARGADO
          Condicion = Condicion & " AND NU_AUTO_COMP_ENCA<>" & Documento.Encabezado.NumeroCOMPROBANTE
          Condicion = Condicion & " AND TX_ESTA_ENCA <> 'A'"
          Result = LoadData(Desde, Campos, Condicion, Arr)
          If Result <> FAIL And Encontro Then
             Call MsgBox("No se puede anular la cotizaci�n (" & Me.txtNumero & ") porque est� asociada con la orden de compra N� " & Arr(0), vbCritical, App.Title)
             BuscarDependientes = 1
          End If
       'SKRV T27136 FIN
    End Select
End Function

Private Function ImpuestosDescuentos()
Dim i As Integer, J As Integer
'Dim BandLoad   As Integer      'DEPURACION DE CODIGO
Dim BandReg    As Integer
Dim Nodoa      As Integer
Dim Nodob      As Integer
'Dim concepto   As String       'DEPURACION DE CODIGO
'Dim CodDes()   As Variant      'DEPURACION DE CODIGO
Dim stDescCondi As String, stImpuCondi As String '|.DR.|, R:1631
Dim stDCondi As String, stICondi As String '|.DR.|, R:1631
'Dim nodo As Node       'DEPURACION DE CODIGO
Dim MiValor As Variant, ArrI() As Variant, arropc() As Variant
Dim DbRIva As Double 'REOL M2258
Dim DbRIca As Double 'REOL M2258
Dim DbRete As Double 'REOL M2258
Dim dbDesc As Double 'REOL M2258
Dim VrCodI() As Variant 'REOL M2258

ReDim Arr(4, 0)
    
    '00 |.DR.|, R:1631
    If cmbConcepto.ListIndex > -1 Then
        stDescCondi = " And CD_CODI_DESC In " & lstDesc.List(cmbConcepto.ListIndex)
        stImpuCondi = " And CD_CODI_IMPU In " & lstImpu.List(cmbConcepto.ListIndex)
        'stDCondi = " CD_CODI_DESC In " & lstDesc.List(cmbConcepto.ListIndex)
        stDCondi = " CD_CODI_DESC In " & lstDesc.List(cmbConcepto.ListIndex) & " AND "
        stICondi = " CD_CODI_IMPU In " & lstImpu.List(cmbConcepto.ListIndex)
        
    End If
    '00 Fin.
    
   TrvLista.Nodes.Clear
    'TxtEntra(7) = 0: TxtEntra(8) = 0: TxtEntra(16) = 0: TxtEntra(17) = 0 '|.DR.|, M:1091
    Call calcular_entra(True) '|.DR.|, M:1091
   Nodoa = 1  ' Carga los descuentos y los impuestos en el arbol
   With TrvLista
        'Carga los descuentos
        '--------------------
        .Nodes.Add , , "Des", "Descuentos"
        'GAPM M6403 INICIO
        If Documento.Encabezado.TipDeDcmnt = Entrada Then
            stDCondi = stDCondi & "(TX_TIPDESC_DESC='2' OR TX_TIPDESC_DESC='3')"
        End If
        'GAPM M6403 FIN
        
        Result = LoadMulData("DESCUENTO", "CD_CODI_DESC,DE_NOMB_DESC,PR_PORC_DESC,VL_VALO_DESC,VL_TOPE_DESC", stDCondi, Arr)
        
        If Result <> FAIL Then
           If Encontro Then
              For i = 0 To UBound(Arr, 2)
                 .Nodes.Add "Des", tvwChild, Arr(0, i) & "|" & Arr(1, i), Arr(0, i) & " " & Arr(1, i)
                 If Arr(2, i) = NUL$ Then Arr(2, i) = 0
                 If Arr(3, i) = NUL$ Then Arr(3, i) = 0
                 If DecDouble(CStr(Arr(2, i))) <> 0 Then
                    .Nodes.Item(.Nodes.Count).Tag = "P" & "|" & CDbl(CStr(Arr(2, i))) & "|" & CDbl(CStr(Arr(4, i))) 'smdl m1752 se cambia DecDouble por CDbl
                 Else
                    .Nodes.Item(.Nodes.Count).Tag = "V" & "|" & CDbl(CStr(Arr(3, i))) & "|" & CDbl(CStr(Arr(4, i))) 'smdl m1752 se cambia DecDouble por CDbl
                 End If
              Next i
           End If
        End If
        'Carga los impuestos
        '--------------------
        .Nodes.Add , , "Imp", "Impuestos"
        Nodob = .Nodes.Count
        
        'Condicion = "ID_TIPO_IMPU <> 'I'  " & stImpuCondi '|.DR.|, R:1631
        Condicion = "ID_TIPO_IMPU NOT IN('I','O')  " & stImpuCondi '|.DR.|, R:1631 , smdl m1699 Se adiciona 'O' para que no tome otros impuestos.
        If AutoRet = "1" Or ExentoFuente = "1" Then Condicion = Condicion & IIf(Condicion <> NUL$, " AND ", "") & " ID_TIPO_IMPU <> 'R'"
        If AutRetIva = "1" Or ExentoIva = "1" Then Condicion = Condicion & IIf(Condicion <> NUL$, " AND ", "") & " ID_TIPO_IMPU <> 'M'"
        If AutRetIca = "1" Or ExentoIca = "1" Then Condicion = Condicion & IIf(Condicion <> NUL$, " AND ", "") & " ID_TIPO_IMPU <> 'C'"
'        Result = FiltrarImpuestos("CD_CODI_IMPU", "TC_IMPUESTOS", Condicion, TipoPers, TipoRegimen, GranContrib, NUL$, NUL$, NUL$, ArrI)   'DEPURACION DE CODIGO
        Result = FiltrarImpuestos("CD_CODI_IMPU", "TC_IMPUESTOS", Condicion, TipoPers, TipoRegimen, GranContrib, ArrI)
        If Result <> FAIL Then
            For J = 0 To UBound(ArrI())
                Condicion = "CD_CODI_IMPU=" & Comi & ArrI(J) & Comi
                Condicion = Condicion & stImpuCondi '|.DR.|, R:1631
                'ReDim Arr(5, 0) 'OMOG R16934/T19113 SE ESTABLECE EN COMENTARIO
                ReDim Arr(6, 0) 'OMOG R16934/T19113
                'Result = LoadMulData("TC_IMPUESTOS", "CD_CODI_IMPU,DE_NOMB_IMPU,PR_PORC_IMPU,0,VL_TOPE_IMPU,ID_TIPO_IMPU", Condicion, Arr) 'OMOG R16934/T19113 SE ESTABLECE EN COMENTARIO
                Result = LoadMulData("TC_IMPUESTOS LEFT JOIN IMP_ACTECO_PORC ON (CD_CODI_IMPU=NU_NUME_IMP AND NU_NUME_ACTECO='" & StActi & "')", "CD_CODI_IMPU,DE_NOMB_IMPU,PR_PORC_IMPU,0,VL_TOPE_IMPU,ID_TIPO_IMPU,CANT_PORC_IMAC", Condicion, Arr)  'OMOG R16934/T19113
                If Result <> FAIL Then
                   If Encontro Then
                      For i = 0 To UBound(Arr, 2)
                         '.Nodes.Add "Imp", tvwChild, "I" & Arr(0, I) & "|" & Arr(1, I), Arr(0, I) & " " & Arr(1, I) 'Dayan,Def 4771
                         
                         'OMOG R16934/T19113 INICIO SE ESTABLECE EN COMENTARIO LAS SIGUIENTES 3 LINEAS Y SE AGREGAN 11 LINEAS
'                         .Nodes.Add "Imp", tvwChild, Arr(5, 0) & Arr(0, i) & "|" & Arr(1, i), Arr(0, i) & " " & Arr(1, i) 'Dayan,Def 4771
'                         If Arr(2, i) = NUL$ Then Arr(2, i) = 0
'                         .Nodes.Item(.Nodes.Count).Tag = Arr(2, i) & "|" & CDbl(CStr(Arr(4, i))) 'smdl 1725 se cambia DecDouble por CDbl
                          If Arr(6, i) = NUL$ Then
                             .Nodes.Add "Imp", tvwChild, Arr(5, 0) & Arr(0, i) & "|" & Arr(1, i), Arr(0, i) & " " & Arr(1, i)
                             If Arr(2, i) = NUL$ Then Arr(2, i) = 0
                             .Nodes.Item(.Nodes.Count).Tag = Arr(2, i) & "|" & CDbl(CStr(Arr(4, i)))
                          Else
                             If StImpCree <> 1 Then
                                .Nodes.Add "Imp", tvwChild, Arr(5, 0) & Arr(0, i) & "|" & Arr(1, i), Arr(0, i) & " " & Arr(1, i)
                                If Arr(6, i) = NUL$ Then Arr(6, i) = 0
                                .Nodes.Item(.Nodes.Count).Tag = Arr(6, i) & "|" & CDbl(CStr(Arr(4, i)))
                             End If
                          End If
                          'OMOG R16934/T19113 FIN
                          
                      Next i
                   End If
                End If
            Next J
         End If
   End With
   
'Busca los Imp/desc ya guardados
   With GrdDeIm
        'GrdDeIm.Rows = 1 smdl m1725
        ReDim ArrI(6, 0)
        
        'JLPB T15675 INICIO LA SIGUIENTE LINEA SE DEJA EN COMENTARIO
        'Campos = "TX_NOMB_ENID,NU_POSI_ENID,NU_VALO_ENID,TX_TIPO_ENID,CD_CODI_IMPU_ENID,CD_CODI_DESC_ENID,NU_PRAPLI_ENID"
        Campos = "TX_NOMB_ENID,NU_POSI_ENID,NU_VALO_ENID,TX_TIPO_ENID,(ID_TIPO_IMPU+CD_CODI_IMPU_ENID),CD_CODI_DESC_ENID,NU_PRAPLI_ENID"
        Desde = "IN_R_ENCA_IMDE INNER JOIN TC_IMPUESTOS ON CD_CODI_IMPU=CD_CODI_IMPU_ENID"
        'JLPB T15675 FIN
        'Condicion = "NU_AUTO_ENCA_ENID=" & txtNumero & " ORDER BY 2"
        'NYCM M2940----------
        Condicion = "NU_AUTO_ENCA_ENID=" & txtNumero & " "
        Condicion = Condicion & " AND NU_AUTO_ENCAREAL_ENID=" & Documento.Encabezado.AutoDelDocCARGADO & " ORDER BY 2"
        'Result = LoadMulData("IN_R_ENCA_IMDE", Campos, Condicion, ArrI) 'JLPB T15675 SE DEJA EN COMENTARIO
        Result = LoadMulData(Desde, Campos, Condicion, ArrI) 'JLPB T15675
        '/--------------------
        If Result <> FAIL And Encontro Then
           GrdDeIm.Rows = 1 'smdl m1725
           ReDim StSum(1, UBound(ArrI, 2))  'REOL M2258
           For i = 0 To UBound(ArrI, 2)
             If ArrI(4, i) <> NUL$ Then  'IMPUESTO
                 ReDim arropc(0)
                 'Condicion = "CD_CODI_IMPU = " & Comi & ArrI(4, i) & Comi 'JLPB T15675 SE DEJA EN COMENTARIO
                 Condicion = "CD_CODI_IMPU = " & Comi & Mid(ArrI(4, i), 2) & Comi 'JLPB T15675
                 Condicion = Condicion & stImpuCondi '|.DR.|, R:1631
                 Result = LoadData("TC_IMPUESTOS", "VL_TOPE_IMPU", Condicion, arropc)
                   If CDbl(TxtEntra(10)) = 0 Then
                      MiValor = 0
                   Else
                      MiValor = Format(((ArrI(2, i) * 100) / CDbl(TxtEntra(10))), "###0.0###")
                   End If
                   'JLPB T15675 INICIO
                   If Mid(ArrI(4, i), 1, 1) <> "M" Then
                      'ArrI(2, I) = (TxtEntra(10) * ArrI(6, I)) / 100 'JAUM T28836 Se deja linea en comentario
                      ArrI(2, i) = ((TxtEntra(10) - TxtEntra(11)) * ArrI(6, i)) / 100 'JAUM T28836
                   Else
                      ArrI(2, i) = (TxtEntra(12) * ArrI(6, i)) / 100
                   End If
                   'JLPB T15675 FIN
                   'SKRV T14531 INICIO
                     If BoAproxCent = True Then
                       ArrI(2, i) = AproxCentena(Round(ArrI(2, i)))
                     End If
                   'SKRV T14531 FINAL
                  '.AddItem vbTab & ArrI(4, i) & vbTab & ArrI(0, i) & vbTab & MiValor & vbTab & ArrI(2, i) & vbTab & ArrI(3, i) & vbTab & "P"    'PJCA M1418 No mostrar los valores recalculados sino mostrar los valores como vienen de la tabla
                  .AddItem vbTab & ArrI(4, i) & vbTab & ArrI(0, i) & vbTab & ArrI(6, i) & vbTab & ArrI(2, i) & vbTab & ArrI(3, i) & vbTab & "P"
'                  .TextMatrix(GrdDeIm.Rows - 1, 4) = Format(GrdDeIm.TextMatrix(GrdDeIm.Rows - 1, 4), "#,###.#0")
                  .TextMatrix(GrdDeIm.Rows - 1, 4) = Aplicacion.Formatear_Valor(GrdDeIm.TextMatrix(GrdDeIm.Rows - 1, 4))    'REOL M2258
                  .TextMatrix(GrdDeIm.Rows - 1, 7) = arropc(0)
                  .TextMatrix(GrdDeIm.Rows - 1, 8) = "I"
                 'REOL M2258
                  ReDim VrCodI(0)
                  Condicion = "CD_CODI_IMPU_ENID = CD_CODI_IMPU AND NU_AUTO_ENCA_ENID=" & txtNumero
                  Condicion = Condicion & " AND NU_AUTO_ENCAREAL_ENID=" & Documento.Encabezado.AutoDelDocCARGADO 'NYCM M2940
                  'Condicion = Condicion & " AND CD_CODI_IMPU = '" & ArrI(4, i) & Comi 'JLPB T15675 SE DEJA EN COMENTARIO
                  Condicion = Condicion & " AND CD_CODI_IMPU = '" & Mid(ArrI(4, i), 2) & Comi 'JLPB T15675
                  Result = LoadData("IN_R_ENCA_IMDE,TC_IMPUESTOS", "ID_TIPO_IMPU", Condicion, VrCodI)
                  Select Case VrCodI(0)
                    Case "R": DbRete = DbRete + ArrI(2, i)
                    'Case "I": DbRIva = DbRIva + ArrI(2, I) 'JLPB T22240 SE DEJA EN COMENTARIO
                    Case "M": DbRIva = DbRIva + ArrI(2, i) 'JLPB T22240
                    Case "C": DbRIca = DbRIca + ArrI(2, i)
                  End Select
                 'REOL M2258
             ElseIf ArrI(5, i) <> NUL$ Then 'DESCUENTO
                  ReDim arropc(2)
                  Condicion = "CD_CODI_DESC=" & Comi & ArrI(5, i) & Comi
                  Condicion = Condicion & stDescCondi '|.DR.|, R:1631
                  Result = LoadData("DESCUENTO", "PR_PORC_DESC,VL_VALO_DESC,VL_TOPE_DESC", Condicion, arropc)
                  If DecDouble(CStr(arropc(0))) <> 0 Then
                     If CDbl(TxtEntra(10)) = 0 Then
                        MiValor = 0
                     Else
                        MiValor = Format(((ArrI(2, i) * 100) / CDbl(TxtEntra(10))), "###0.0###")
                     End If
                     '.AddItem vbTab & ArrI(5, i) & vbTab & ArrI(0, i) & vbTab & MiValor & vbTab & ArrI(2, i) & vbTab & ArrI(3, i)      'PJCA M1418 No mostrar los valores recalculados sino mostrar los valores como vienen de la tabla
                     .AddItem vbTab & ArrI(5, i) & vbTab & ArrI(0, i) & vbTab & ArrI(6, i) & vbTab & ArrI(2, i) & vbTab & ArrI(3, i)
                     .TextMatrix(GrdDeIm.Rows - 1, 6) = "P"
                     .TextMatrix(GrdDeIm.Rows - 1, 7) = arropc(2)
                     .TextMatrix(GrdDeIm.Rows - 1, 8) = "D"
                  End If
                  If DecDouble(CStr(arropc(1))) <> 0 Then
                     .AddItem vbTab & ArrI(5, i) & vbTab & ArrI(0, i) & vbTab & MiValor & vbTab & ArrI(2, i) & vbTab & ArrI(3, i)
                     .TextMatrix(GrdDeIm.Rows - 1, 6) = "V"
                     .TextMatrix(GrdDeIm.Rows - 1, 7) = arropc(2)
                     .TextMatrix(GrdDeIm.Rows - 1, 8) = "D"
                  End If
'                  .TextMatrix(GrdDeIm.Rows - 1, 4) = Format(GrdDeIm.TextMatrix(GrdDeIm.Rows - 1, 4), "#,###.#0")
                  .TextMatrix(GrdDeIm.Rows - 1, 4) = Aplicacion.Formatear_Valor(GrdDeIm.TextMatrix(GrdDeIm.Rows - 1, 4))    'REOL M2258
                  BandReg = 1
                 
                  dbDesc = dbDesc + ArrI(2, i)      'REOL M2258

             End If
           Next i
          'REOL M2258
           If DbRIva > 0 Then TxtEntra(15) = Aplicacion.Formatear_Valor(CDbl(TxtEntra(15)) + CDbl(TxtEntra(18)) - DbRIva): TxtEntra(18) = Aplicacion.Formatear_Valor(DbRIva)
           If DbRIca > 0 Then TxtEntra(15) = Aplicacion.Formatear_Valor(CDbl(TxtEntra(15)) + CDbl(TxtEntra(19)) - DbRIca): TxtEntra(19) = Aplicacion.Formatear_Valor(DbRIca)
           If DbRete > 0 Then TxtEntra(15) = Aplicacion.Formatear_Valor(CDbl(TxtEntra(15)) + CDbl(TxtEntra(13)) - DbRete): TxtEntra(13) = Aplicacion.Formatear_Valor(DbRete)
           If dbDesc > 0 Then TxtEntra(15) = Aplicacion.Formatear_Valor(CDbl(TxtEntra(15)) + CDbl(TxtEntra(11)) - dbDesc): TxtEntra(11) = Aplicacion.Formatear_Valor(dbDesc)
          'REOL M2258
        End If
   End With
   
   If cmdOpciones(0).Enabled = False Then
      TrvLista.Enabled = False
      Cmd_agregar.Enabled = False
      Cmd_Quitar.Enabled = False
      cmdREGRESAR(3).Enabled = False 'smdl m1822
   End If

  GrdDeIm.ColWidth(7) = 0
  GrdDeIm.ColWidth(8) = 0

End Function
         

Private Function Instanciado(objeto As Object) As Boolean
Dim S As String
On Error GoTo Error
   S = objeto.Tag
   Instanciado = True
Exit Function
Error:
      If ERR.Number = 91 Then
         Instanciado = False
      Else
         Resume Next
      End If
End Function

'Verifica si un elemento que se vaya a agregar ya exite en la lista
Private Function BuscarItem(ByVal concepto As String, ByVal columna As Integer) As Integer
Dim i As Integer
   BuscarItem = False
   With GrdDeIm
      For i = 1 To .Rows - 1
         If .TextMatrix(i, columna) = concepto Then
            .Row = i
            .Col = 1
            .SetFocus
            BuscarItem = True
            Call Mensaje1("Este concepto ya esta seleccionado", 3)
            Exit For
         End If
      Next i
   End With
End Function

Private Function Guardar_ImpuestoDescuento()
Dim f, Maximo As Integer
Dim dbValor As Double

'HRR M1818
'Dim StDocDep As String 'HRR M1819
'Dim DbAutoEnca As Double 'HRR M1819
'Dim DbDocDep As Double 'HRR M1819
'Dim DbCompEnca As Double 'HRR M1819
'Dim InI As Integer 'HRR M1819
'Dim DbAutoDoc As Double 'HRR M1819
'HRR M1818

'HRR M1818 SE DEJA COMO ESTABA ANTES
'HRR M1819

   If Documento.Encabezado.TipDeDcmnt <> Entrada Or (Documento.Encabezado.TipDeDcmnt = Entrada And Documento.Encabezado.EsIndependiente) Then 'HRR M1818
   
      Maximo = GetMaxCod("IN_R_ENCA_IMDE", "NU_POSI_ENID", "NU_AUTO_ENCA_ENID=" & CDbl(txtNumero))
   
      With GrdDeIm
        For f = 1 To .Rows - 1
           Valores = "NU_AUTO_ENCA_ENID=" & CDbl(txtNumero) & Coma
           Valores = Valores & "NU_POSI_ENID=" & Maximo + 1 & Coma
           Valores = Valores & "TX_NOMB_ENID=" & Comi & Cambiar_Comas_Comillas(CStr(.TextMatrix(f, 2))) & Comi & Coma
           Valores = Valores & "NU_VALO_ENID=" & CDbl(IIf(.TextMatrix(f, 4) = NUL$, 0, .TextMatrix(f, 4))) & Coma
           Valores = Valores & "TX_TIPO_ENID=" & Comi & .TextMatrix(f, 5) & Comi & Coma
           Valores = Valores & "NU_PRAPLI_ENID=" & CDbl(.TextMatrix(f, 3)) & Coma
   
           If Mid(.TextMatrix(f, 1), 1, 1) = "D" Then
               'Valores = Valores & "CD_CODI_DESC_ENID=" & Comi & .TextMatrix(f, 0) & Comi
               Valores = Valores & "CD_CODI_DESC_ENID=" & Comi & .TextMatrix(f, 0) & Comi & Coma 'NYCM M2940
           Else
               'Valores = Valores & "CD_CODI_IMPU_ENID=" & Comi & Mid(.TextMatrix(f, 1), 2, Len(.TextMatrix(f, 1))) & Comi
               Valores = Valores & "CD_CODI_IMPU_ENID=" & Comi & Mid(.TextMatrix(f, 1), 2, Len(.TextMatrix(f, 1))) & Comi & Coma 'NYCM M2940
           End If
           Valores = Valores & "NU_AUTO_ENCAREAL_ENID=" & Documento.Encabezado.AutoDelDocCARGADO 'NYCM M2940
           If Not (.TextMatrix(f, 0) = NUL$ And .TextMatrix(f, 1) = NUL$ And .TextMatrix(f, 3) = NUL$) Then
             Result = DoInsertSQL("IN_R_ENCA_IMDE", Valores)
           End If
           If Result = FAIL Then Exit For
        Next f
      End With
   

'HRR M1819

'HRR M1818 SE DEJA COMO ESTABA ANTES
''HRR M1819
'   If Me.cboIndependiente.ListIndex = 1 Then
'         Maximo = GetMaxCod("IN_R_ENCA_IMDE", "NU_POSI_ENID", "NU_AUTO_ENCA_ENID=" & CDbl(txtNumero))
'
'         With GrdDeIm
'           For f = 1 To .Rows - 1
'              Valores = "NU_AUTO_ENCA_ENID=" & CDbl(txtNumero) & Coma
'              Valores = Valores & "NU_POSI_ENID=" & Maximo + 1 & Coma
'              Valores = Valores & "TX_NOMB_ENID=" & Comi & Cambiar_Comas_Comillas(CStr(.TextMatrix(f, 2))) & Comi & Coma
'              Valores = Valores & "NU_VALO_ENID=" & CDbl(IIf(.TextMatrix(f, 4) = NUL$, 0, .TextMatrix(f, 4))) & Coma
'              Valores = Valores & "TX_TIPO_ENID=" & Comi & .TextMatrix(f, 5) & Comi & Coma
'              Valores = Valores & "NU_PRAPLI_ENID=" & CDbl(.TextMatrix(f, 3)) & Coma
'
'              If Mid(.TextMatrix(f, 1), 1, 1) = "D" Then
'                  'Valores = Valores & "CD_CODI_DESC_ENID=" & Comi & .TextMatrix(f, 0) & Comi
'                  Valores = Valores & "CD_CODI_DESC_ENID=" & Comi & .TextMatrix(f, 0) & Comi & Coma 'NYCM M2940
'              Else
'                  'Valores = Valores & "CD_CODI_IMPU_ENID=" & Comi & Mid(.TextMatrix(f, 1), 2, Len(.TextMatrix(f, 1))) & Comi
'                  Valores = Valores & "CD_CODI_IMPU_ENID=" & Comi & Mid(.TextMatrix(f, 1), 2, Len(.TextMatrix(f, 1))) & Comi & Coma 'NYCM M2940
'              End If
'              Valores = Valores & "NU_AUTO_ENCAREAL_ENID=" & Documento.Encabezado.AutoDelDocCARGADO 'NYCM M2940
'              If Not (.TextMatrix(f, 0) = NUL$ And .TextMatrix(f, 1) = NUL$ And .TextMatrix(f, 3) = NUL$) Then
'                Result = DoInsertSQL("IN_R_ENCA_IMDE", Valores)
'              End If
'              If Result = FAIL Then Exit For
'           Next f
'           If Result <> FAIL Then Call GuardaValoresParticulatres
'
'         End With
'
'   Else
'      DbAutoDoc = Documento.Encabezado.AutoDelDocCARGADO
'      With GrdDeIm
'      ReDim Arr(0)
'      For InI = 1 To Me.GrdArticulos.Rows - 1
'         DbDocDep = Me.GrdArticulos.TextMatrix(InI, Me.ColAutoDocumento)
'        If InStr(1, StDocDep, CStr(DbDocDep)) = 0 Then
'            If StDocDep <> NUL$ Then StDocDep = StDocDep & ","
'            StDocDep = StDocDep & CStr(DbDocDep)
'
'            If DbDocDep = 0 Then Result = FAIL: Exit For
'            DbAutoEnca = GetMaxCod("IN_R_ENCA_ENCA", "NU_ENCCMP_RENEN", "NU_ENCDEP_RENEN=" & DbDocDep)
'
'            If DbAutoEnca = 0 Then Result = FAIL: Exit For
'
'            Result = LoadData("IN_ENCABEZADO", "NU_COMP_ENCA", "NU_AUTO_ENCA=" & DbAutoEnca, Arr)
'
'            If Result <> FAIL And Encontro And IsNumeric(Arr(0)) Then
'               If Arr(0) <> NUL$ Then
'                  DbCompEnca = CInt(Arr(0))
'               Else
'                  Result = FAIL
'                  Exit For
'               End If
'            Else
'               Result = FAIL
'               Exit For
'            End If
'
'            Maximo = GetMaxCod("IN_R_ENCA_IMDE", "NU_POSI_ENID", "NU_AUTO_ENCA_ENID=" & DbCompEnca)
'
'            For f = 1 To .Rows - 1
'               Valores = "NU_AUTO_ENCA_ENID=" & DbCompEnca & Coma
'               Valores = Valores & "NU_POSI_ENID=" & Maximo + 1 & Coma
'               Valores = Valores & "TX_NOMB_ENID=" & Comi & Cambiar_Comas_Comillas(CStr(.TextMatrix(f, 2))) & Comi & Coma
'               Valores = Valores & "NU_VALO_ENID=" & CDbl(IIf(.TextMatrix(f, 4) = NUL$, 0, .TextMatrix(f, 4))) & Coma
'               Valores = Valores & "TX_TIPO_ENID=" & Comi & .TextMatrix(f, 5) & Comi & Coma
'               Valores = Valores & "NU_PRAPLI_ENID=" & CDbl(.TextMatrix(f, 3)) & Coma
'
'               If Mid(.TextMatrix(f, 1), 1, 1) = "D" Then
'                   'Valores = Valores & "CD_CODI_DESC_ENID=" & Comi & .TextMatrix(f, 0) & Comi
'                   Valores = Valores & "CD_CODI_DESC_ENID=" & Comi & .TextMatrix(f, 0) & Comi & Coma 'NYCM M2940
'               Else
'                   'Valores = Valores & "CD_CODI_IMPU_ENID=" & Comi & Mid(.TextMatrix(f, 1), 2, Len(.TextMatrix(f, 1))) & Comi
'                   Valores = Valores & "CD_CODI_IMPU_ENID=" & Comi & Mid(.TextMatrix(f, 1), 2, Len(.TextMatrix(f, 1))) & Comi & Coma 'NYCM M2940
'               End If
'               Valores = Valores & "NU_AUTO_ENCAREAL_ENID=" & DbAutoEnca
'               If Not (.TextMatrix(f, 0) = NUL$ And .TextMatrix(f, 1) = NUL$ And .TextMatrix(f, 3) = NUL$) Then
'                 Result = DoInsertSQL("IN_R_ENCA_IMDE", Valores)
'               End If
'               If Result = FAIL Then Exit For
'            Next f
'            Documento.Encabezado.AutoDelDocCARGADO = DbAutoEnca
'            If Result <> FAIL Then Call GuardaValoresParticulatres
'
'            If Result = FAIL Then Exit For
'        End If
'      Next
'
'      End With
'
'      Documento.Encabezado.AutoDelDocCARGADO = DbAutoDoc
'   End If
'
''HRR M1819
'HRR M1818

  'HRR M1818 Si es entrada y es dependiente
  Else
      Maximo = GetMaxCod("IN_R_ENCA_IMDE", "NU_POSI_ENID", "NU_AUTO_ENCA_ENID=" & CDbl(txtNumero))
   
      With GrdDeIm
        For f = 1 To .Rows - 1
           Valores = "NU_AUTO_ENCA_ENID=" & CDbl(txtNumero) & Coma
           Valores = Valores & "NU_POSI_ENID=" & Maximo + 1 & Coma
           Valores = Valores & "TX_NOMB_ENID=" & Comi & Cambiar_Comas_Comillas(CStr(.TextMatrix(f, 2))) & Comi & Coma
           If .TextMatrix(f, 8) = "I" Then
               If Mid(.TextMatrix(f, 1), 1, 1) = "M" Then    'Impuestos tipo ReteIVA
                  dbValor = IIf(.TextMatrix(f, 3) = NUL$, 0, (CDbl(.TextMatrix(f, 3)) * DbTotIva) / 100)
               Else
                   dbValor = IIf(.TextMatrix(f, 3) = NUL$, 0, ((CDbl(Me.TxtEntra(10).Text) - CDbl(Me.TxtEntra(11).Text)) * CDbl(.TextMatrix(f, 3))) / 100)
               End If
            Else
                  If .TextMatrix(f, 6) = "P" Then
                     dbValor = IIf(.TextMatrix(f, 3) = NUL$, 0, CDbl(Me.TxtEntra(10).Text) * CDbl(.TextMatrix(f, 3)) / 100)
                  Else
                     dbValor = IIf(.TextMatrix(f, 4) = NUL$, 0, CDbl(.TextMatrix(f, 4)))
                  End If
            End If
           Valores = Valores & "NU_VALO_ENID=" & dbValor & Coma
           Valores = Valores & "TX_TIPO_ENID=" & Comi & .TextMatrix(f, 5) & Comi & Coma
           Valores = Valores & "NU_PRAPLI_ENID=" & CDbl(.TextMatrix(f, 3)) & Coma
   
           If Mid(.TextMatrix(f, 1), 1, 1) = "D" Then
               'Valores = Valores & "CD_CODI_DESC_ENID=" & Comi & .TextMatrix(f, 0) & Comi
               Valores = Valores & "CD_CODI_DESC_ENID=" & Comi & .TextMatrix(f, 0) & Comi & Coma 'NYCM M2940
           Else
               'Valores = Valores & "CD_CODI_IMPU_ENID=" & Comi & Mid(.TextMatrix(f, 1), 2, Len(.TextMatrix(f, 1))) & Comi
               Valores = Valores & "CD_CODI_IMPU_ENID=" & Comi & Mid(.TextMatrix(f, 1), 2, Len(.TextMatrix(f, 1))) & Comi & Coma 'NYCM M2940
           End If
           Valores = Valores & "NU_AUTO_ENCAREAL_ENID=" & Documento.Encabezado.AutoDelDocCARGADO 'NYCM M2940
           If Not (.TextMatrix(f, 0) = NUL$ And .TextMatrix(f, 1) = NUL$ And .TextMatrix(f, 3) = NUL$) Then
             Result = DoInsertSQL("IN_R_ENCA_IMDE", Valores)
           End If
           If Result = FAIL Then Exit For
        Next f
      End With
      
  End If
  'HRR M1818
   
End Function

Private Sub calcular_Traslado()
    Dim columna As Long 'Para que conserve la posici�n
    Dim Fila As Long 'Para que conserve la posici�n
    Dim Valor As Double
    '''''''''''
    'PASO A CONTA
    Dim valo_cont(1 To 2) As Variant
    ''''''''''
    columna = GrdArti.Col
    Fila = GrdArti.Row
    '''
    GrdArti.Row = 0
'/////////////
    CargaGrillaInterface Me
'/////////////
    
    GrdArti.Row = 0
    On Error GoTo salto
    'While GrdArti.Row <= GrdArti.Rows - 1
    While GrdArti.Row < GrdArti.Rows - 1
        GrdArti.Row = GrdArti.Row + 1
        If GrdArti.TextMatrix(GrdArti.Row, 1) <> NUL$ Then
            If GrdArti.TextMatrix(GrdArti.Row, 2) = "" Or IsNumeric(GrdArti.TextMatrix(GrdArti.Row, 2)) = False Then
                GrdArti.TextMatrix(GrdArti.Row, 2) = "0"
            End If
            If GrdArti.TextMatrix(GrdArti.Row, 3) = "" Or IsNumeric(GrdArti.TextMatrix(GrdArti.Row, 3)) = False Then
                GrdArti.TextMatrix(GrdArti.Row, 3) = "0"
            End If
            'Precio total
            Valor = CDbl(GrdArti.TextMatrix(GrdArti.Row, 2)) * CDbl(GrdArti.TextMatrix(GrdArti.Row, 3))
            GrdArti.TextMatrix(GrdArti.Row, 4) = Aplicacion.Formatear_Valor(Valor)
        End If
    Wend
salto:
    On Error GoTo 0
    TxtSaliA(7).Text = Aplicacion.Formatear_Valor(sumar_items(GrdArti, 4))
    '''
    ''PASO A CONTA
    valo_cont(1) = TxtSaliA(7).Text
    valo_cont(2) = TxtSaliA(7).Text
    If Aplicacion.Interfaz_Contabilidad Then Call Interface_ContableTraslado(Documento.Encabezado.TipDeDcmnt)
    '''''''''''''''''
    GrdArti.Col = columna  ''Recupera la posici�n
    If Fila > 0 And Fila < GrdArti.Rows Then GrdArti.Row = Fila ''Recupera la posici�n
End Sub

Private Sub Interface_ContableTraslado(TipoDoc As Integer)

    CtasConta = Cuentas_ArticulosTraslado(GrdArti, "CD_SALI_RGB", "CD_ENTR_RGB", "C", "D", Documento.Encabezado.BodegaORIGEN, Documento.Encabezado.BodegaDESTINO)
   'Call Cuentas_Contables_Movi(TipoDoc, CtasConta, Documento.Encabezado.Tercero.Nit, NUL$)
    Call Cuentas_Contables_Movi(CtasConta, Documento.Encabezado.Tercero.Nit, NUL$)      'DEPURACION DE CODIGO
End Sub

'PJCA M1077
'Calcula los impuestos y desceuntos de la entrada, cuando se esta haciendo la devoluci�n de entrada Dependiente
Private Function ImpDesc_DevEntrada(NumEntrada As Double)
Dim ArrI() As Variant
Dim i As Integer, k As Integer, J As Integer
Dim Cta As String

'    ReDim ArrI(2, 0)
'    ReDim ArrI(3, 0)    'REOL M1642
    ReDim ArrI(4, 0)    'REOL M2388
'    Campos = "CD_CODI_IMPU_ENID,CD_CODI_DESC_ENID,NU_VALO_ENID"
    Campos = "CD_CODI_IMPU_ENID,CD_CODI_DESC_ENID,NU_VALO_ENID,NU_PRAPLI_ENID"  'REOL M1642
    Campos = "CD_CODI_IMPU_ENID,CD_CODI_DESC_ENID,NU_VALO_ENID,NU_PRAPLI_ENID,ID_TIPO_IMPU"   'REOL M2388
'    Condicion = "NU_AUTO_ENCA_ENID=" & NumEntrada & " ORDER BY 2"
    'Condicion = "CD_CODI_IMPU = CD_CODI_IMPU_ENID AND NU_AUTO_ENCA_ENID=" & NumEntrada & " ORDER BY 2"   'REOL M2388
    Condicion = "CD_CODI_IMPU = CD_CODI_IMPU_ENID AND NU_AUTO_ENCA_ENID=" & NumEntrada & " " 'NYCM M2940
    Condicion = Condicion & "AND NU_AUTO_ENCAREAL_ENID=" & Documento.Encabezado.AutoDelDocCARGADO & " ORDER BY 2" 'NYCM M2940

'    Result = LoadMulData("IN_R_ENCA_IMDE", Campos, Condicion, ArrI)
    Result = LoadMulData("IN_R_ENCA_IMDE,TC_IMPUESTOS", Campos, Condicion, ArrI)    'REOL M2388
    If Result <> FAIL And Encontro Then
       For i = 0 To UBound(ArrI, 2)
            k = UBound(CtasConta(), 2) + 1
            'ReDim Preserve CtasConta(3, k)
            ReDim Preserve CtasConta(5, k) 'LDCR T10396
            If ArrI(0, i) <> NUL$ Then     'Impuestos
               Cta = BuscarCuenta("TC_IMPUESTOS", "CD_CUEN_IMPU", "CD_CODI_IMPU=" & Comi & ArrI(0, i) & Comi)
               J = FindInArr(CtasConta(), Cta)
               If J <> -1 Then k = J
               CtasConta(0, k) = Cta
'               CtasConta(1, k) = ArrI(2, I)
               'REOL M2388
               If ArrI(4, i) <> "M" Then
                  CtasConta(1, k) = Aplicacion.Formatear_Valor((CSng(ArrI(3, i)) * SumaCosto) / 100)     'REOL M1642
               Else
'                  CtasConta(1, k) = Aplicacion.Formatear_Valor((CSng(ArrI(3, i)) * (SumaCosto * 0.16)) / 100)
                  'CtasConta(1, k) = Aplicacion.Formatear_Valor((CSng(ArrI(3, i)) * Aplicacion.Formatear_Valor(TxtEntra(12))) / 100) 'REOL M2388
                  CtasConta(1, k) = Aplicacion.Formatear_Valor(CSng(ArrI(3, i)) * CDbl(TxtEntra(12)) / 100) 'REOL M2388'SKRV T 15434
               End If
               'REOL M2388
               If BoAproxCent Then CtasConta(1, k) = AproxCentena(Round(CtasConta(1, k)))
               CtasConta(2, k) = CDbl(TxtEntra(10)) - TxtEntra(11)
               CtasConta(3, k) = "D"
    'REOL M1643
'            ElseIf ArrI(1, i) <> NUL$ Then 'Descuentos
'               CtasConta(0, k) = BuscarCuenta("DESCUENTO", "CD_CUEN_DESC", "CD_CODI_DESC=" & Comi & ArrI(1, i) & Comi)
''               CtasConta(1, k) = ArrI(2, I)
'               CtasConta(1, k) = Aplicacion.Formatear_Valor((CSng(ArrI(3, i)) * SumaCosto) / 100)     'REOL M1642
'               CtasConta(2, k) = CDbl(TxtEntra(10)) - TxtEntra(11)
'               CtasConta(3, k) = "D"
            End If
       Next i
    End If
End Function
'PJCA M1077

'HRR M1818
'---------------------------------------------------------------------------------------
' Procedure : Cuentas_Movimiento_Entrada
' DateTime  : 22/04/2008 14:52
' Author    : hector_rodriguez
' Purpose   : Carga el arreglo CtasConta (movimiento contable) con los valores de impuestos aplicados a la entrada
'---------------------------------------------------------------------------------------
'
Function Cuentas_Movimiento_Entrada(Movimiento As Integer, ARRValor() As Variant, bruto As Double, Optional GrdImDe As MSFlexGrid, Optional GrdArti As MSFlexGrid, Optional Bodega As Integer) As Variant
    Dim i As Long
    Dim k As Double, J As Double
    Dim CtaGrupo As String
    Dim Impuesto As Double 'JAGS T10733
    Impuesto = 0 'JAGS T10733
    ReDim Arr(5)
    Result = LoadData("CONCEPTOT", Asterisco, "NU_MOVI_CONC=" & Movimiento, Arr())
    If Arr(0) <> NUL$ Then
        i = UBound(CtasConta, 2) + 1
        'iva
        If ARRValor(0) > 0 Then
'            ReDim Preserve CtasConta(3, I)
'            CtasConta(0, I) = Arr(2)
'            CtasConta(1, I) = ARRValor(0)
'            CtasConta(2, I) = VBase
'            CtasConta(3, I) = "D"
'            I = I + 1
            If IVADeducible() Then                  'Req 1112-983-1035
                'ReDim Preserve CtasConta(3, I)
                ReDim Preserve CtasConta(5, i) 'LDCR T10396
                CtasConta(0, i) = Arr(2)
                CtasConta(1, i) = ARRValor(0)
                CtasConta(2, i) = VBase
                CtasConta(3, i) = "D"
                'INICIO LDCR T10191
                CtasConta(4, i) = ARRValor(0)
                CtasConta(5, i) = "DEDUCIBLE"
                'FIN LDCR T10191
                i = i + 1
            Else
                'Req 1112-983-1035
                With GrdArti
                  For J = 1 To .Rows - 1
                     'CtaGrupo = BuscarCuenta("ARTICULO,GRUP_ARTICULO", "CD_ENTR_GRUP", "CD_CODI_GRUP=CD_GRUP_ARTI AND CD_CODI_ARTI=" & Comi & .TextMatrix(J, 0) & Comi)
                     Condicion = "CD_CODI_GRUP=CD_CODI_GRUP_RGB AND CD_CODI_GRUP=CD_GRUP_ARTI AND CD_CODI_ARTI=" & Comi & .TextMatrix(J, 0) & Comi
                     Condicion = Condicion & " AND NU_AUTO_BODE_RGB=" & Bodega
                     CtaGrupo = BuscarCuenta("ARTICULO,GRUP_ARTICULO,IN_R_GRUP_BODE", IIf(boFact, "CD_ENTR_RGB", "CD_TRAN_RGB"), Condicion)
                     k = FindInArrM(CtasConta, CtaGrupo, 0)
                     If ArrIVA(J - 1) = NUL$ Then ArrIVA(J - 1) = 0
                     'JAGS T10733 inicio
                     Impuesto = Impuesto + ArrIVA(J - 1)
                     CtasConta(1, k) = CDbl(CtasConta(1, k)) + IIf(BoAproxCent, AproxCentena(Round(ArrIVA(J - 1))), ArrIVA(J - 1)) 'JLPB T22240
                     'CtasConta(1, k) = CDbl(CtasConta(1, k)) + ArrIVA(J - 1)
                     'CtasConta(4, k) = ArrIVA(J - 1)
                     'CtasConta(5, k) = "NO DEDUCIBLE"
                     'JAGS T10733 fin
                  Next J
                  'JAGS T10733 inicio
                    If BoAproxCent Then Impuesto = AproxCentena(Round(Impuesto))
                    'CtasConta(1, k) = CDbl(CtasConta(1, k)) + Impuesto 'JLPB T22240 SE DEJA EN COMENTARIO
                    CtasConta(4, 0) = Impuesto
                    CtasConta(5, 0) = "NO DEDUCIBLE"
                    'JAGS T10733 fin
                End With
            End If
        End If
        'retefte
        If ARRValor(3) > 0 Then
'            ReDim Preserve CtasConta(3, I)
'            CtasConta(0, I) = Arr(5)
'            CtasConta(1, I) = ARRValor(3)
'            CtasConta(2, I) = bruto
'            CtasConta(3, I) = "C"
'            I = I + 1
            With GrdImDe
              For k = 1 To .Rows - 1
                If Mid(.TextMatrix(k, 1), 1, 1) = "R" Then
                  'ReDim Preserve CtasConta(3, I)
                  ReDim Preserve CtasConta(5, i) 'LDCR T10396
                  CtasConta(0, i) = BuscarCuenta("TC_IMPUESTOS", "CD_CUEN_IMPU", "CD_CODI_IMPU=" & Comi & Mid(.TextMatrix(k, 1), 2, Len(.TextMatrix(k, 1))) & Comi)
                  'CtasConta(1, I) = ARRValor(3)
                  'CtasConta(1, I) = .TextMatrix(k, 4)     'M0924
                  CtasConta(1, i) = IIf(.TextMatrix(k, 3) = NUL$, 0, (CDbl(Me.TxtEntra(10).Text) - CDbl(Me.TxtEntra(11).Text)) * CDbl(.TextMatrix(k, 3)) / 100)
                  If BoAproxCent = True Then CtasConta(1, i) = AproxCentena(Round(CtasConta(1, i))) 'JAGS T10733
                  
                  CtasConta(2, i) = bruto
                  CtasConta(3, i) = "C"
                  i = i + 1
                End If
              Next k
            End With
        End If
        'reteiva
        If ARRValor(1) > 0 Then
'            ReDim Preserve CtasConta(3, I)
'            CtasConta(0, I) = Arr(3)
'            CtasConta(1, I) = ARRValor(1)
'            CtasConta(2, I) = ARRValor(0)
'            CtasConta(3, I) = "C"
'            I = I + 1
          With GrdImDe
            For k = 1 To .Rows - 1
              If Mid(.TextMatrix(k, 1), 1, 1) = "M" Then
                'ReDim Preserve CtasConta(3, I)
                ReDim Preserve CtasConta(5, i) 'LDCR T10396
                CtasConta(0, i) = BuscarCuenta("TC_IMPUESTOS", "CD_CUEN_IMPU", "CD_CODI_IMPU=" & Comi & Mid(.TextMatrix(k, 1), 2, Len(.TextMatrix(k, 1))) & Comi)
                'CtasConta(1, I) = ARRValor(1)
                'CtasConta(1, I) = .TextMatrix(k, 4)     'M0924
                CtasConta(1, i) = IIf(.TextMatrix(k, 3) = NUL$, 0, DbTotIva * CDbl(.TextMatrix(k, 3)) / 100) 'HRR M1818
                If BoAproxCent Then CtasConta(1, i) = AproxCentena(Round(CtasConta(1, i))) 'JLPB T22240
                CtasConta(2, i) = ARRValor(0)
                CtasConta(3, i) = "C"
                i = i + 1
              End If
            Next k
          End With
        End If
            'reteica
        If ARRValor(2) > 0 Then
'            ReDim Preserve CtasConta(3, I)
'            CtasConta(0, I) = Arr(4)
'            CtasConta(1, I) = ARRValor(2)
'            CtasConta(2, I) = bruto
'            CtasConta(3, I) = "C"
'            I = I + 1
          With GrdImDe
            For k = 1 To .Rows - 1
              If Mid(.TextMatrix(k, 1), 1, 1) = "C" Then
                'ReDim Preserve CtasConta(3, I)
                ReDim Preserve CtasConta(5, i) 'LDCR T10396
                CtasConta(0, i) = BuscarCuenta("TC_IMPUESTOS", "CD_CUEN_IMPU", "CD_CODI_IMPU=" & Comi & Mid(.TextMatrix(k, 1), 2, Len(.TextMatrix(k, 1))) & Comi)
                'CtasConta(1, I) = ARRValor(2)
                'CtasConta(1, I) = .TextMatrix(k, 4)     'M0924
                CtasConta(1, i) = IIf(.TextMatrix(k, 3) = NUL$, 0, (CDbl(Me.TxtEntra(10).Text) - CDbl(Me.TxtEntra(11).Text)) * CDbl(.TextMatrix(k, 3)) / 100)
                'SKRV T14531 INICIO
                If BoAproxCent = True Then
                  CtasConta(1, i) = AproxCentena(Round(CtasConta(1, i)))
                End If
                'SKRV T14531 FINAL
                CtasConta(2, i) = bruto
                CtasConta(3, i) = "C"
                i = i + 1
              End If
            Next k
          End With
        End If
    Else
        Call Mensaje1("No se ha realizado la parametrizaci�n contable", 3)
    End If
    Cuentas_Movimiento_Entrada = CtasConta()
End Function
'HRR M1818

'HRR M1818
'---------------------------------------------------------------------------------------
' Procedure : fnGuardar_Entrada
' DateTime  : 22/04/2008 14:55
' Author    : hector_rodriguez
' Purpose   : Guarda una entrada que dependa de una o mas ordenes
'---------------------------------------------------------------------------------------
'
Private Sub fnGuardar_Entrada()
   Dim ArDoc() As String
   Dim StDocAux As String
   Dim InI, InRespuesta As Integer
   Dim J&
   Dim vNumero As Long

         

        'HRR M3229
         'Me.TxtPpto(2) = CDbl(txtTOTAL)      'PedroJ 'hrr m1818 se pasa mas abajo
          
         NumeroCompro = WinDoc.Guardar(False, False, StDocAux) 'HRR M1818
         If NumeroCompro > 0 Then txtNumero.Text = NumeroCompro 'AASV M5669
         
         Me.fraTOTALES.Visible = True
         If NumeroCompro = -1 Then GoTo FLLCMMTRS
          'HRR M3229
         
         'HRR M1818
         ArDoc = Split(StDocAux, ",")
         For InI = 0 To UBound(ArDoc)
            If InStr(StDoc, ArDoc(InI)) = 0 Then
               
               'HRR M1818
               
               If Aplicacion.Interfaz_Presupuesto Then
                  Call CargaGrillaInterface(Me, False, ArDoc(InI))
                  Call calcular_entra(True)
                  'Me.TxtPpto(2).Text = Me.TxtEntra(10).Text
                   'DAHV T3910 - INICIO
                   'Me.txtPPTO(2).Text = Me.TxtEntra(15).Text 'DAHV M4460
                   If Me.TxtEntra(10).Text = NUL$ Then Me.TxtEntra(10).Text = "0"
                   If Me.TxtEntra(18).Text = NUL$ Then Me.TxtEntra(18).Text = "0"
                   If Me.TxtEntra(12).Text = NUL$ Then Me.TxtEntra(12).Text = "0"
                   
                   Me.TxtPpto(2).Text = CStr(CDbl(Me.TxtEntra(10).Text) + CDbl(Me.TxtEntra(18).Text) + CDbl(Me.TxtEntra(12).Text))
                   'DAHV T3910 - FIN
                   
                   'APGR T1634 - INICIO
                   'Busca el registro pptal relacionado a la orden de compra, para generar la obligaci�n.
                   ReDim Arr(0)
                   Condicion = "NU_AUTO_ENCA_ENPP=" & ArDoc(InI) & " AND NU_TIPO_PTAL_ENPP=2"
                   Result = LoadData("IN_R_ENCA_PPTO", "NU_CONS_PTAL_ENPP", Condicion, Arr)
                   If Result <> FAIL And Encontro Then TxtPpto(0) = Arr(0) Else TxtPpto(0) = 0
                   'APGR T1634 - FIN
                   
                   If Me.lstDocsPadre.Visible = True Then sbPresupuesto_Entrada (ArDoc(InI))
                   If Result = FAIL Then Me.fraTOTALES.Visible = True: Exit Sub     'REOL M2387
                   If Valida_PPto = 0 Then Me.fraTOTALES.Visible = True: GoTo FLLCMMTRS
               End If
                             
                If Aplicacion.Interfaz_Contabilidad Then
                    continuar = False
                    If Not Aplicacion.Interfaz_Presupuesto Then
                        Call CargaGrillaInterface(Me, False, ArDoc(InI)) 'Se pasa mas arriba
                        Call calcular_entra(True) 'Se pasa mas arriba
                    End If

                     '01 |.DR.|, R:1633
                     stDesc = WinDoc.ElConsecutivo.Nombre
                     stDesc = IIf(boAfectC, WinDoc.ElConsecutivo.Nombre, "LA BODEGA NO AFECTA CONT.") '|.DR.|, R:1633
                     If Not boAfectC Then
                         For J& = LBound(Mcuen) To UBound(Mcuen)
                         If Mcuen(J&, 1) <> "" Then
                             Mcuen(J&, 2) = "0"
                             McuenD(J&, 2) = "0"
                         End If
                         Next J&
                         continuar = WarnMsg("Los movimientos contables se realizar�n con valor cero." & vbCrLf & "�Desea continuar?")
                     Else
                         muestra_cuenta "S", Me
                     End If
                  
                  '01 Fin.
                    
                End If
                'HRR M3229
                
                                    
                'HRR M3229
                             
                If Aplicacion.Interfaz_Contabilidad Then If Not continuar Then GoTo FLLBGNTRN
                  
               If BeginTran(STranIUp & "KARDEX") = FAIL Then GoTo FLLBGNTRN
               NumeroCompro = WinDoc.Guardar(False, True, ArDoc(InI))
               If NumeroCompro > 0 Then txtNumero.Text = NumeroCompro 'AASV M5669
            
               If Not NumeroCompro > 0 Then GoTo FLLCMMTRS
               Me.txtNumero.Text = NumeroCompro
            
               'HRR M3229
                    
               If Aplicacion.Interfaz_Contabilidad Then 'HRR M3229
               
                  comprobante = Buscar_Comprobante_Concepto(Entrada)
                  vNumero = Buscar_Numero_Comprobante(comprobante)
                  'Call Guardar_Movimiento_Contable("ENTR", Documento.Encabezado.FechaDocumento, Comprobante, vNumero, Left(Trim(stDesc) & Right(String(7, "0") & NumeroCompro, 7) & ". " & Documento.Encabezado.Observaciones, 250), NUL$)
                  'Call Guardar_Movimiento_Contable("ENTR", Documento.Encabezado.FechaDocumento, Comprobante, vNumero, "FACTURA No." & (Me.TxtCompra(10).Text) & Left(Trim(stDesc) & Right(String(7, "0") & NumeroCompro, 7) & ". " & Documento.Encabezado.Observaciones, 250), NUL$) 'AASV R1323
                  'Call Guardar_Movimiento_Contable("ENTR", Documento.Encabezado.FechaDocumento, Comprobante, vNumero, "FACTURA No." & (Me.TxtCompra(10).Text) & " " & Left(Trim(stDesc) & Right(String(7, "0") & NumeroCompro, 7) & ". " & Documento.Encabezado.Observaciones, 250), NUL$) 'AASV M3485
                  'Call Guardar_Movimiento_Contable("ENTR", Documento.Encabezado.FechaDocumento, Comprobante, vNumero, "FACTURA No." & UCase(Me.TxtCompra(10).Text) & " " & Left(Trim(stDesc) & Right(String(7, "0") & NumeroCompro, 7) & ". " & Documento.Encabezado.Observaciones, 250), NUL$) 'JACC R2221
                  'Call Guardar_Movimiento_Contable("ENTR", Documento.Encabezado.FechaDocumento, comprobante, vNumero, "FACTURA No." & UCase(Me.TxtCompra(10).Text) & " " & Left(Trim(stDesc) & Right(String(7, "0") & NumeroCompro, 7) & ". " & Documento.Encabezado.Observaciones, 250), NUL$, , Documento.Encabezado.TipDeDcmnt) 'APGR T10703 'SKRV T26242 COMENTARIO
                  Call Guardar_Movimiento_Contable("ENTR", Documento.Encabezado.FechaDocumento, comprobante, vNumero, "FACTURA No." & UCase(Me.TxtCompra(10).Text) & " " & Left(Trim(stDesc) & Right(String(7, "0") & NumeroCompro, 7) & ". " & Documento.Encabezado.Observaciones, 250), NUL$, , Documento.Encabezado.TipDeDcmnt, fnDevDato("MODU_MOV_NIIF", "NU_CODMO_MODU", "TX_DESCM_MODU = 'Entrada de Almac�n' AND NU_CODMO_MODU like '06%'", True), txtNumero) 'SKRV T26242
                  If Result <> FAIL Then Result = DoUpdate("IN_ENCABEZADO", "NU_COMP_CONTA_ENCA=" & vNumero, "NU_AUTO_COMP_ENCA=" & Documento.Encabezado.NumeroCOMPROBANTE & " AND NU_COMP_ENCA=" & NumeroCompro)
                  If Result <> FAIL Then
                      'AASV M5669 Nota: 26367 Inicio
                      'Result = DoDelete("IN_R_ENCA_IMDE", "NU_AUTO_ENCA_ENID=" & CDbl(txtNumero))
                      Result = DoDelete("IN_R_ENCA_IMDE", "NU_AUTO_ENCA_ENID=" & CDbl(txtNumero), "N") 'AASV M5855 Se adiciona parametro para que no valide siel usuario tiene permiso de borrado
                      If Result <> FAIL Then
                      'AASV M5669 Nota: 26367 Fin
                           Call Guardar_ImpuestoDescuento
                      End If 'AASV M5669 Nota: 26367
                      If Result = FAIL Then GoTo FLLCMMTRS
                  End If
                  If Result = FAIL Then GoTo FLLCMMTRS
                  Call GuardaValoresParticulatres
                End If
                
            'PedroJ
                If Aplicacion.Interfaz_Presupuesto And Result <> FAIL Then

'                    ArtiPpto = Articulos_Presupuesto(GrdArticulos, "NU_COMP_CUEN,NU_CDPE_CUEN", 15, 0)
                    'ArtiPpto = Articulos_Presupuesto(GrdArticulos, 15, 0, False, ArDoc(Ini))    'DEPURACION DE CODIGO
                    'ArtiPpto = Articulos_Presupuesto(GrdArticulos, 15, 0, False, ArDoc(Ini), Entrada)   'DAHV M4460 'HRR M5080
                   'APGR T3910 - INICIO
                   'ArtiPpto = Articulos_Presupuesto(GrdArticulos, 15, 0, ArDoc(InI), Entrada) 'DAHV M4460 'HRR M5080
                    'ArtiPpto = Articulos_Presupuesto(GrdArticulos, 15, 0, ArDoc(InI), Entrada, CDbl(Me.TxtEntra(16))) 'DAHV M4460 'HRR M5080 'LDCR R5027/T6173
                    ArtiPpto = Articulos_Presupuesto(GrdArticulos, 15, 0, ArDoc(InI), Entrada, CDbl(Me.TxtEntra(16)), Documento.Encabezado.Tercero.Nit) 'LDCR R5027/T6173
                   'APGR T3910 - FIN
                    If ArtiPpto(0, 0) = NUL$ Then Call Mensaje1("Fallo al buscar los Articulos Pptales del Grupo de Articulos", 1): GoTo FLLCMMTRS
                    TxtPpto(3).Text = Aplicacion.Formatear_Valor(TxtPpto(3)) 'NYCM M2968
                    
                    'If TotArtiPpto > Val(TxtPpto(3)) Then Call Mensaje1("El valor de la Entrada es mayor que el valor del RP", 1): GoTo FLLCMMTRS       'PJCA M1115
                    'If TotArtiPpto > CDbl(TxtPpto(3)) Then Call Mensaje1("El valor de la Entrada es mayor que el valor del RP", 1): GoTo FLLCMMTRS       'PJCA M3212
                    If CDbl(Trim(TotArtiPpto)) > CDbl(TxtPpto(3)) Then Call Mensaje1("El valor de la Entrada es mayor que el valor del RP", 1): GoTo FLLCMMTRS       'HRR M2968
                    
                    Valores = "NU_AUTO_ENCA_ENPP=" & Documento.Encabezado.AutoDelDocCARGADO & Coma
                    Valores = Valores & "NU_CONS_PTAL_ENPP=" & Trim(Me.TxtPpto(0)) & Coma
                    Valores = Valores & "NU_TIPO_PTAL_ENPP=2"  'ES UNA OBLIGACION
                    Result = DoInsertSQL("IN_R_ENCA_PPTO", Valores)
                    If Result = FAIL Then Call Mensaje1("Fallo al insertar en la tabla [IN_R_ENCA_PPTO]", 1): GoTo FLLCMMTRS
                    RegPPto = Leer_Consecutivo_Ppto("NU_CONS_OBLI") + 1
                    Result = DoUpdate("CONSECUTIVOS", "NU_CONS_OBLI=" & RegPPto, NUL$)
                    If (Result <> FAIL) Then Call Actualizar_Ppto
                    If BoResult = True Then GoTo FLLCMMTRS 'GAVL T4960
                    If (Result <> FAIL) Then
                       Valores = "NU_AUTO_ENCA_ENPP=" & Documento.Encabezado.AutoDelDocCARGADO & Coma
                       Valores = Valores & "NU_CONS_PTAL_ENPP=" & RegPPto & Coma
                       Valores = Valores & "NU_TIPO_PTAL_ENPP=3"  'ES UNA OBLIGACION
                       Result = DoInsertSQL("IN_R_ENCA_PPTO", Valores)
                       If Result = FAIL Then Call Mensaje1("Fallo al insertar en la tabla [IN_R_ENCA_PPTO]", 1): GoTo FLLCMMTRS
                    End If

                     
                End If
            
            'PedroJ
               Result = Almacenar_Auditoria_Mov(CDbl(fnDevDato("IN_ENCABEZADO", "NU_AUTO_ENCA", "NU_COMP_ENCA=" & NumeroCompro & " AND NU_AUTO_DOCU_ENCA=" & Documento.Encabezado.TipDeDcmnt & " AND NU_AUTO_COMP_ENCA=" & Documento.Encabezado.NumeroCOMPROBANTE)), comprobante, CDbl(vNumero), CInt(Documento.Encabezado.AutoDelUSUARIO)) 'JLPB T41459-R37519
               If Result = FAIL Then Call Mensaje1("Fallo al insertar en la tabla [IN_AUDMOVCONT]", 1): GoTo FLLCMMTRS 'JLPB T41459-R37519
               If CommitTran = FAIL Then
                  GoTo FLLCMMTRS
               Else
                  If StDoc <> NUL$ Then StDoc = StDoc & Coma
                  StDoc = StDoc & ArDoc(InI)
                    'DAHV M3615
                    'AASV R1883
                    If Aplicacion.PinvImpAntSCon Then
                     
                       InRespuesta = MsgBox("Desea imprimir el documento?", vbYesNo + vbInformation) 'AASV M3520
                       If InRespuesta = vbYes Then 'AASV M3520
                          Documento.Encabezado.EsNuevoDocumento = False
                          Call cmdOpciones_Click(2)
                       End If
                   End If
                   'AASV R1883
                   'AASV Bloqueos Rosario 04/02/2009 INICIO
                   If NumeroCompro <> FAIL Then
                      Call Mensaje1("Se gener� el documento n�mero " & NumeroCompro, 3)
                   End If
                   'AASV Bloqueos Rosario 04/02/2009 FIN
                  
               End If
               
            End If
         Next
         StDoc = NUL$
         If NumeroCompro <> FAIL Then Call cmdOpciones_Click(3)
         GoTo SLRDLTRNS
            
        Me.fraTOTALES.Visible = True
FLLBGNTRN:
FLLCMMTRS:
        Call RollBackTran
        'AASV Bloqueos Rosario 05/02/2009 INICIO
        If StDescripError <> NUL$ Then
           Call Mensaje1(StDescripError, 3)
           StDescripError = NUL$
        End If
        'AASV Bloqueos Rosario 05/02/2009 FIN
        BoResult = False 'GAVL T4960
SLRDLTRNS:

'         Call WinDoc_CambioRenglonGrilla(0, WinDoc.Calcular_Documento(Me.ColCantidad, Me.ColCostoUnidad))
         If continuar Then Call WinDoc_CambioRenglonGrilla(0, WinDoc.Calcular_Documento(Me.ColCantidad, Me.ColCostoUnidad))  'REOL M2085

   
End Sub
'HRR M1818

'HRR M1818
'---------------------------------------------------------------------------------------
' Procedure : sbPresupuesto_Entrada
' DateTime  : 23/04/2008 08:33
' Author    : hector_rodriguez
' Purpose   : visualiza el registro pptal asociado a una orden de compra
'                      se invoca este procedimiento si se hace una entrada dependiente de una orden de compra
'---------------------------------------------------------------------------------------
'
Private Sub sbPresupuesto_Entrada(ByVal StDocDep As String)
   
   Dim ArDoc() As String
   Dim StDocAux As String
   Dim InI As Integer
      
   'determinar que orden de compra se va a procesar, para que visualice el registro pptal y calcule el valor de la entrada asociada a esa orden de compra.
   If StDocDep = NUL$ Then
      NumeroCompro = WinDoc.Guardar(False, False, StDocAux) 'HRR M1818
      If NumeroCompro > 0 Then txtNumero.Text = NumeroCompro 'AASV M5669
      If NumeroCompro = -1 Then Exit Sub
      ArDoc = Split(StDocAux, ",")
      For InI = 0 To UBound(ArDoc)
         If InStr(StDoc, ArDoc(InI)) = 0 Then
            Call CargaGrillaInterface(Me, False, ArDoc(InI))
            Call calcular_entra(True)
               
            'Me.TxtPpto(2).Text = Me.TxtEntra(10).Text
            Me.TxtPpto(2).Text = Me.TxtEntra(15).Text 'DAHV M4460
            StDocDep = ArDoc(InI)
            Exit For
         End If
      Next
   End If

    
    
    'If CDbl(Me.TxtEntra(10).Text) = 0 Then Exit Sub
    If CDbl(Me.TxtEntra(15).Text) = 0 Then Exit Sub 'DAHV M4460
    FrmPresu.Desde_Forma = Me
'    PptoArti = Articulos_Presupuesto(GrdArticulos, "NU_COMP_CUEN,NU_CDPE_CUEN", 15, 0) 'REOL M807
    ' ----------------------------
    'DAHV M4460
    'PptoArti = Articulos_Presupuesto(GrdArticulos, 15, 0, False, StDocDep)    'DEPURACION DE CODIGO
    'PptoArti = Articulos_Presupuesto(GrdArticulos, 15, 0, False, StDocDep, Entrada) 'HRR M5080
     'DAHV T3910 - INICIO
    'PptoArti = Articulos_Presupuesto(GrdArticulos, 15, 0, StDocDep, Entrada) 'HRR M5080
     'PptoArti = Articulos_Presupuesto(GrdArticulos, 15, 0, StDocDep, Entrada, CDbl(Me.TxtEntra(16))) 'HRR M5080 'LDCR R5027/T6173
     PptoArti = Articulos_Presupuesto(GrdArticulos, 15, 0, StDocDep, Entrada, CDbl(Me.TxtEntra(16)), Documento.Encabezado.Tercero.Nit)  'LDCR R5027/T6173
    'DAHV T3910 - FIN
    ' ----------------------------

         'If lstDocsPadre.Visible = False Then            'Si es un documento independiente 'HRR R1700
         If lstDocsPadre.Visible = False Or BoConsMov Then 'HRR R1700 Si es un documento independiente o se consulta el movimiento
            If Me.cmdOpciones(0).Enabled = True Then     'Si es nuevo documento
               'Call FrmPresu.CargarDatos(CDbl(Me.TxtEntra(10).Text), txtDocum(1) & "/" & Me.txtDocum(2) & "/" & Me.txtDocum(3), Documento.Encabezado.TipDeDcmnt, 0, True) 'DAHV M4460
               Call FrmPresu.CargarDatos(CDbl(Me.TxtEntra(15).Text), txtDocum(1) & "/" & Me.txtDocum(2) & "/" & Me.txtDocum(3), Documento.Encabezado.TipDeDcmnt, 0, True) 'DAHV M4460
            Else                                         'si esta consultando uno guardado
               If Me.TxtPpto(0) = 0 Then Call Mensaje1("No se encontraron datos Pptales", 3): Exit Sub
               'Call FrmPresu.CargarDatos(CDbl(Me.TxtEntra(10).Text), txtDocum(1) & "/" & Me.txtDocum(2) & "/" & Me.txtDocum(3), Documento.Encabezado.TipDeDcmnt, 2, True) 'DAHV M4460
               Call FrmPresu.CargarDatos(CDbl(Me.TxtEntra(15).Text), txtDocum(1) & "/" & Me.txtDocum(2) & "/" & Me.txtDocum(3), Documento.Encabezado.TipDeDcmnt, 2, True) 'DAHV M4460
            End If
         Else                                            'Si es un documento dependiente
              'Call FrmPresu.CargarDatos(CDbl(Me.TxtEntra(10).Text), txtDocum(1) & "/" & Me.txtDocum(2) & "/" & Me.txtDocum(3), Documento.Encabezado.TipDeDcmnt, 1, False) 'DAHV M4460
              Call FrmPresu.CargarDatos(CDbl(Me.TxtEntra(15).Text), txtDocum(1) & "/" & Me.txtDocum(2) & "/" & Me.txtDocum(3), Documento.Encabezado.TipDeDcmnt, 1, False) 'DAHV M4460
         End If
    
    
    FrmPresu.Show 1
End Sub
'HRR M1818

'---------------------------------------------------------------------------------------
' Procedure : Movimiento_Asociado
' DateTime  : 02/02/2009 16:35
' Author    : albert_silva
' Purpose   : R2236 OB Busca el numero de Movimiento del cual depende
'---------------------------------------------------------------------------------------
'
'Function Movimiento_Asociado(LnNuMovi As Long, LnNuDocu As Long) As Long APGR T6899 PNC
Function Movimiento_Asociado(LnNuMovi As Long, LnNuDocu As Long, Optional LnNuComp As Long) As Long 'APGR T6899 PNC

   ReDim Arr(0)
   Desde = "IN_ENCABEZADO INNER JOIN IN_R_ENCA_ENCA LEFT OUTER JOIN "
   Desde = Desde & "IN_ENCABEZADO AS IN_ENCABEZADO_DEPE ON IN_R_ENCA_ENCA.NU_ENCDEP_RENEN = IN_ENCABEZADO_DEPE.NU_AUTO_ENCA ON "
   Desde = Desde & "IN_ENCABEZADO.NU_AUTO_ENCA = IN_R_ENCA_ENCA.NU_ENCCMP_RENEN "
   Campos = "IN_ENCABEZADO_DEPE.NU_COMP_ENCA AS DEPEMOV "
   Condicion = "IN_ENCABEZADO.NU_COMP_ENCA = " & LnNuMovi
   Condicion = Condicion & " AND IN_ENCABEZADO.NU_AUTO_DOCU_ENCA = " & LnNuDocu
   If LnNuComp <> 0 Then Condicion = Condicion & " AND IN_ENCABEZADO.NU_AUTO_COMP_ENCA=" & LnNuComp 'APGR T6899 PNC
   Result = LoadData(Desde, Campos, Condicion, Arr())
   If Result <> FAIL And Encontro Then
      Movimiento_Asociado = Arr(0)
   End If

End Function

'---------------------------------------------------------------------------------------
' Procedure : Saldar_Obligacion
' DateTime  : 14/08/2013 15:28
' Author    : Sinthia Rodriguez
' Purpose   : Salda la obligacion presupuestal cuando se ha realizado una devolucion
'              obligacion = consecutivo de obligacion en ppto
'---------------------------------------------------------------------------------------
Private Sub Saldar_Obligacion(obligacion As Long)
   
   'Busca el numero del comprobante
   ReDim VaArrTemp(0)
   
   Result = LoadData("COMPROBANTES", "NU_COMP_SALD", NUL$, VaArrTemp())
   If Result <> FAIL And Encontro Then
      'Actualiza el consecutivo del comprobante
      Condicion = "CD_CODI_COMP=" & Comi & VaArrTemp(0) & Comi
      Valores = "NU_CONS_COMP = NU_CONS_COMP + 1"
      Result = DoUpdate("TC_COMPROBANTE", Valores, Condicion)
   Else
      Call Mensaje1("No se realizara el comprobante contable;" _
      & "no se encontro el comprobante contable de documentos saldados", 2)
   End If
   
   If Result <> FAIL Then
            
      'Actualiza el registro presupuestal
      ReDim VaCompObli(1, 0)
      Desde = " OBLIGACION"
      Campos = " VL_VALO_OBLI, NU_REGI_OBLI"
      Condicion = " NU_CONS_OBLI = " & obligacion
      Result = LoadMulData(Desde, Campos, Condicion, VaCompObli()) ' se carga el valor de la obligacion
      If Result <> FAIL And Encontro Then
         Campos = "VL_COMP_REGI = VL_COMP_REGI - " & VaCompObli(0, 0)
         Result = DoUpdate("REGISTRO_PPTO", Campos, "NU_CONS_REGI=" & CLng(VaCompObli(1, 0)))
      End If
      
      'actualiza el valor total saldado de la Obligacion
      Condicion = Condicion & " AND NU_CONS_OBLI = " & CLng(obligacion) & " AND ID_ESTA_OBLI <> '2' "
      'Campos = " FE_SALD_OBLI = '" & txtDocum(3) & "/" & txtDocum(2) & "/" & txtDocum(1) & "'" 'JAUM T38333 Se deja linea en comentario
      Campos = " FE_SALD_OBLI = " & fecha2(txtDocum(3) & "/" & txtDocum(2) & "/" & txtDocum(1))  'JAUM T38333
      Campos = Campos & ", VL_NOUS_OBLI = VL_VALO_OBLI"
      If Result <> FAIL Then Result = DoUpdate("OBLIGACION", Campos, Condicion)
      
      'Actualiza Registro_Articulo
      If Result <> FAIL Then Call Leer_Articulos(obligacion, CLng(VaCompObli(1, 0)))
   End If
   
End Sub

'---------------------------------------------------------------------------------------
' Procedure : Leer_Articulos
' DateTime  : 14/08/2013 15:28
' Author    : Sinthia Rodriguez
' Purpose   : T15948
  ' actualiza los articulos (Tomado de ppto)
'              Numero = consecutivo de obligacion en ppto
'---------------------------------------------------------------------------------------

Private Sub Leer_Articulos(Numero As Long, NumReg As Long)
 ReDim Datos(5, 0)
ReDim Arr(0)
Dim J As Integer
Dim reg(1)
    Result = DoUpdate("OBL_ARTICULO", "VL_NOUS_OBAR=0", "VL_NOUS_OBAR IS NULL")
    Condicion = " NU_OBLI_OBAR =" & Numero & " AND " & _
                " VL_OBLI_OBAR <> (VL_PAGO_OBAR + VL_NOUS_OBAR) "
    If Result <> FAIL Then Result = LoadMulData("OBL_ARTICULO", Asterisco, Condicion, Datos())
    If Result <> FAIL Then

         For J = 0 To UBound(Datos(), 2)
           If Datos(0, J) <> NUL$ Then
              If Result = FAIL Then Exit For
              If Datos(2, J) = NUL$ Then Datos(2, J) = 0
              If Datos(4, J) = NUL$ Then Datos(4, J) = 0
              Call Actualiza_Gastos("VL_OBLI_PERI", CStr(Datos(1, J)), CDbl(Datos(4, J)) - CDbl(Datos(2, J)), txtDocum(2))

              If Result <> FAIL Then
                 Campos = "VL_COMP_REAR= VL_COMP_REAR - " & (CDbl(Datos(2, J)) - CDbl(Datos(4, J)))
                 Condicion = "NU_REGI_REAR = " & NumReg & " AND CD_ARTI_REAR = '" & CStr(Datos(1, J)) & Comi
                 Result = DoUpdate("REG_ARTICULO", Campos, Condicion)
              End If
           End If
         Next
    End If
      Condicion = " NU_OBLI_OBAR =" & Numero & " AND"
      Condicion = Condicion & " VL_OBLI_OBAR <> (VL_PAGO_OBAR + VL_NOUS_OBAR) "
      Campos = "VL_NOUS_OBAR= VL_OBLI_OBAR - VL_PAGO_OBAR"
      If Result <> FAIL Then Result = DoUpdate("OBL_ARTICULO", Campos, Condicion)
End Sub

'---------------------------------------------------------------------------------------
' Procedure : DescuImpues
' DateTime  : 27/05/2014 04:32 PM
' Author    : JAIRO LEONARDO PARRA BUITRAGO
' Purpose   : METODO QUE REALIZA EL CACULO DE LOS IMPUESTOS Y DESCUENTOS POR CADA UNO DE LOS PRODUCTOS LISTADOS EN EL GRID 'JLPB T15675
'---------------------------------------------------------------------------------------
Sub DescuImpues(CXP As Double)
        
   Dim DbInicial As Double
   Dim DbIvaEnt As Double
   Dim DbimpDesc As Double
   Dim DbTDCom As Double
   Dim DbTDCond As Double
   Dim DbTImp As Double
   Dim VrDesc() As Variant
   Dim dbVlNeto As Double
   Dim DbVlrIvaEnt As Double
   Dim InJ As Integer
   Dim ind As Integer
   Dim InA As Integer
   Dim StCant As String
   Dim VrCant() As Variant
   InJ = 0
   ind = 0
   'JLPB T37799 INICIO Se deja en comentario las siguientes lineas
   'InCantT = 0
   'InCantD = 0
   'InCantET = 0
   DbCantT = 0
   DbCantD = 0
   DbCantET = 0
   'JLPB T37799 FIN
   
   StCant = NUL$
   ReDim ArDescComp(8, 0)
   ReDim ArImpComp(8, 0)
   'JAUM T28836 INICIO
   ReDim ArrCxP(0)
   Result = LoadData("IN_ENCABEZADO", "NU_CXP_ENCA", "NU_AUTO_ENCA=" & AutoDocCargado, ArrCxP)
   If Result <> FAIL And Encontro Then
      Desde = "IN_ENCABEZADO"
      Campos = "NU_AUTO_ENCA"
      Condicion = "NU_CXP_ENCA=" & ArrCxP(0)
      ReDim ArrCxP(0, 0)
      Result = LoadMulData(Desde, Campos, Condicion, ArrCxP)
      If Result <> FAIL And Encontro Then
         For InA = 0 To UBound(ArrCxP, 2)
            StCant = StCant & ArrCxP(0, InA) & Coma
         Next
      End If
   End If
   'JAUM T28836 Fin
   ReDim VrCant(1, GrdArticulos.Rows - 2)
   'JAUM T28836 INICIO Se deja en comentario las siguientes lineas
   'For InA = 1 To GrdArticulos.Rows - 1
   '   StCant = StCant & GrdArticulos.TextMatrix(InA, 24) & Coma
   'Next
   'JAUM T28836 FIN
   StCant = Mid(StCant, 1, (Len(StCant) - 1))
   Result = LoadMulData("IN_DETALLE WITH(NOLOCK)", "NU_ENTRAD_DETA,NU_SALIDA_DETA", "NU_AUTO_MODCABE_DETA IN (" & StCant & ")", VrCant)
   
   For InA = 0 To UBound(VrCant, 2)
      'JLPB T37799 INICIO Se deja en comentario las siguientes lineas
      'InCantT = InCantT + VrCant(0, InA)
      'InCantET = InCantET + VrCant(1, InA)
      DbCantT = DbCantT + VrCant(0, InA)
      DbCantET = DbCantET + VrCant(1, InA)
      'JLPB T37799 FIN
   Next
   For InA = 1 To GrdArticulos.Rows - 1
      'InCantD = InCantD + GrdArticulos.TextMatrix(InA, 10) 'JLPB T37799 Se deja en comentario
      DbCantD = DbCantD + GrdArticulos.TextMatrix(InA, 10) 'JLPB T37799
   Next
   
   ReDim VrArr(0)
   Desde = "IN_R_ENCA_IMDE"
   Campos = "TX_NOMB_ENID"
   Condicion = "NU_AUTO_ENCA_ENID=" & txtNumero & " AND NU_AUTO_ENCAREAL_ENID=" & Documento.Encabezado.AutoDelDocCARGADO
   Result = LoadData(Desde, Campos, Condicion, VrArr)
   If Result <> FAIL And Encontro Then Exit Sub
   
   With GrdArticulos
      DbInicial = 0
      DbIvaEnt = 0
      DbTImp = 0
      dbVlNeto = 0
      DbVlrIvaEnt = 0
      DbTDCom = 0
      DbTDCond = 0
         
      DbInicial = txtTOTAL
      DbIvaEnt = txtIMPUE
      dbVlNeto = DbInicial - DbIvaEnt
      DbVlrIvaEnt = DbIvaEnt
      ReDim VrDesc(5, 0)
      'JAUM T28836 Inicio
      ReDim Arr(0)
      Result = LoadData("C_X_P", "CD_CONCE_CXP", "CD_NUME_CXP = " & CXP, Arr)
      If Result <> FAIL And Encontro Then
         If Arr(0) = "CINM" Then
            Desde = "DESCUENTO INNER JOIN R_CPCP_DESC ON CD_CODI_DESC=CD_DESC_RCPDE INNER JOIN R_CXP_CXP ON CD_NUME_RCPCP=CD_NUME_RCPDE"
            Campos = "DISTINCT CD_CODI_DESC,PR_PORC_DESC,VL_TOPE_DESC,CD_CUEN_DESC,VL_VALO_DESC,TX_TIPO_DESC"
            Condicion = "CD_NUME_CXP_RCPCP = " & CXP
         Else
      'JAUM T28836 Fin
            Desde = "DESCUENTO INNER JOIN R_CXP_DESC ON CD_CODI_DESC=CD_DESC_CPDE"
            Campos = "CD_CODI_DESC,PR_PORC_DESC,VL_TOPE_DESC,CD_CUEN_DESC,VL_VALO_DESC,TX_TIPO_DESC"
            Condicion = "CD_NUME_CPDE=" & CXP
         End If 'JAUM T28836
      End If 'JAUM T28836
      Result = LoadMulData(Desde, Campos, Condicion, VrDesc)
      If Result <> FAIL And Not Encontro Then GoTo SEGUIR1
      For InA = 0 To UBound(VrDesc, 2)
         DbimpDesc = 0
         If VrDesc(2, InA) < dbVlNeto Then
            If Val(VrDesc(1, InA)) = 0 Then
               DbimpDesc = Aplicacion.Formatear_Valor(VrDesc(4, InA))
            Else
               DbimpDesc = Aplicacion.Formatear_Valor(Val(dbVlNeto * VrDesc(1, InA)) / 100)
            End If
         End If
         If BoAproxCent Then DbimpDesc = AproxCentena(Round(DbimpDesc))
         If Val(VrDesc(5, InA)) <> 1 Then
            DbTDCom = DbTDCom + IIf(DbimpDesc < 0, 0, DbimpDesc)
         Else
            DbTDCond = DbTDCond + IIf(DbimpDesc < 0, 0, DbimpDesc)
         End If
         If DbimpDesc > 0 Then
            If InJ <> 0 Then
               ReDim Preserve ArDescComp(8, InJ)
            End If
            ArDescComp(0, InJ) = VrDesc(0, InA)
            ArDescComp(1, InJ) = VrDesc(1, InA)
            ArDescComp(2, InJ) = VrDesc(2, InA)
            ArDescComp(3, InJ) = VrDesc(3, InA)
            ArDescComp(4, InJ) = -DbimpDesc
            ArDescComp(5, InJ) = -DbVlrIvaEnt
            'ArDescComp(6, InJ) = InCantD 'JLPB T37799 Se deja en comentario
            ArDescComp(6, InJ) = DbCantD 'JLPB T37799
            ArDescComp(7, InJ) = DbInicial
            ArDescComp(8, InJ) = dbVlNeto
            InJ = InJ + 1
         End If
      Next
SEGUIR1:

      ReDim VrDesc(5, 0)
      'JAUM T28836 Inicio
      If Arr(0) = "CINM" Then
         Desde = "TC_IMPUESTOS INNER JOIN R_CPCP_IMPU ON CD_CODI_IMPU=CD_IMPU_RCPIM INNER JOIN R_CXP_CXP ON CD_NUME_RCPCP=CD_NUME_RCPIM"
         Campos = "DISTINCT CD_CODI_IMPU,PR_PORC_IMPU,VL_TOPE_IMPU,CD_CUEN_IMPU,ID_TIPO_IMPU"
         Condicion = "CD_NUME_CXP_RCPCP = " & CXP & " AND ID_TIPO_IMPU<>'I'"
      Else
      'JAUM T28836 Fin
         Desde = "TC_IMPUESTOS INNER JOIN R_CXP_IMPU ON CD_CODI_IMPU=CD_IMPU_CPIM"
         Campos = "CD_CODI_IMPU,PR_PORC_IMPU,VL_TOPE_IMPU,CD_CUEN_IMPU,ID_TIPO_IMPU"
         Condicion = "CD_NUME_CPIM=" & CXP & " AND ID_TIPO_IMPU<>'I'"
      End If 'JAUM T28836
      Result = LoadMulData(Desde, Campos, Condicion, VrDesc)
      If Result <> FAIL And Not Encontro Then GoTo SEGUIR2
      For InA = 0 To UBound(VrDesc, 2)
         DbimpDesc = 0
         If VrDesc(2, InA) < dbVlNeto Then
            If VrDesc(4, InA) = "C" Then
               DbimpDesc = Aplicacion.Formatear_Valor(Val((dbVlNeto - DbTDCom) * VrDesc(1, InA)) / 100)
            ElseIf VrDesc(4, InA) = "M" Then
               DbimpDesc = Aplicacion.Formatear_Valor((DbIvaEnt * VrDesc(1, InA)) / 100)
            ElseIf VrDesc(4, InA) = "R" Then
               DbimpDesc = Aplicacion.Formatear_Valor(Val((dbVlNeto - DbTDCom) * VrDesc(1, InA)) / 100)
            End If
         End If
         If BoAproxCent Then DbimpDesc = AproxCentena(Round(DbimpDesc))
         DbTImp = DbimpDesc
         If DbTImp > 0 Then
            If ind <> 0 Then
               ReDim Preserve ArImpComp(8, ind)
            End If
            ArImpComp(0, ind) = VrDesc(0, InA)
            ArImpComp(1, ind) = VrDesc(1, InA)
            ArImpComp(2, ind) = VrDesc(2, InA)
            ArImpComp(3, ind) = VrDesc(3, InA)
            ArImpComp(4, ind) = -DbTImp
            ArImpComp(5, ind) = -DbVlrIvaEnt
            'ArImpComp(6, ind) = InCantD 'JLPB T37799 Se deja en comentario
            ArImpComp(6, ind) = DbCantD 'JLPB T37799
            ArImpComp(7, ind) = DbInicial
            ArImpComp(8, ind) = dbVlNeto
            ind = ind + 1
         End If
      Next
SEGUIR2:
   End With
End Sub

'---------------------------------------------------------------------------------------
' Procedure : Almacenar_Auditoria_Mov
' DateTime  : 02/03/2018 15:59
' Author    : jairo_parra
' Purpose   : JLPB T41459-R37519 Almacena la auditoria de los movimientos enviados a contabilidad, los que genera el sistema y los que se envian despues de que usuarios los acepte
'---------------------------------------------------------------------------------------
'
Private Function Almacenar_Auditoria_Mov(DbAutEnc As Double, StComp As String, DbConsMov As Double, InAutUsu As Integer) As Boolean
   Dim InI As Integer 'Utilizada para los recorridos del for
   Dim InA As Integer 'Utilizada para los recorridos del for
   Dim LnRetorno As Long 'Envia cero (0) cuando no se ha dimensionado un arreglo
   
   GetSafeArrayPointer VrAudMov, LnRetorno
   If LnRetorno <> 0 Then
      For InA = 0 To UBound(VrAudMov, 2)
         For InI = 0 To UBound(VrAudMov, 3)
            If VrAudMov(8, InA, InI) <> NUL$ Then
               If Result <> FAIL Then
                  Valores = "NU_AUTENCA_AUDT=" & DbAutEnc & Coma
                  Valores = Valores & "TX_COMP_AUDT='" & StComp & Comi & Coma
                  Valores = Valores & "NU_CONMOV_AUDT=" & DbConsMov & Coma
                  Valores = Valores & "TX_CUENTA_AUDT='" & VrAudMov(0, InA, InI) & Comi & Coma
                  Valores = Valores & "TX_TERC_AUDT='" & VrAudMov(1, InA, InI) & Comi & Coma
                  Valores = Valores & "TX_CC_AUDT='" & VrAudMov(2, InA, InI) & Comi & Coma
                  Valores = Valores & "TX_NATU_AUDT='" & VrAudMov(3, InA, InI) & Comi & Coma
                  Valores = Valores & "NU_VALOR_AUDT=" & VrAudMov(4, InA, InI) & Coma
                  Valores = Valores & "NU_VALORB_AUDT=" & VrAudMov(5, InA, InI) & Coma
                  Valores = Valores & "NU_VALORIVA_AUDT=" & VrAudMov(6, InA, InI) & Coma
                  Valores = Valores & "NU_AUTUSU_AUDT=" & InAutUsu & Coma
                  Valores = Valores & "NU_IMPT_AUDT=" & VrAudMov(7, InA, InI) & Coma
                  Valores = Valores & "NU_ORIALM_AUDT=" & VrAudMov(8, InA, InI)
                  Result = DoInsertSQL("IN_AUDMOVCONT", Valores)
               End If
            End If
         Next
      Next
   End If
   Almacenar_Auditoria_Mov = Result
End Function

'---------------------------------------------------------------------------------------
' Procedure : Cargar_Logo
' DateTime  : 27/09/2018 12:28
' Author    : daniel_mesa
' Purpose   : DRMG T44173-R41288 Carga el Logo para las facturas
'---------------------------------------------------------------------------------------
'
Private Function Cargar_Logo()
   Set cQrCode = New ClsQrCode
   
   Condicion = "NU_MODULO_FAEL=1 AND NU_ESTADO_FAEL=0"
   If MotorBD = "SQL" Then Call Load_imagen("FACTURA_ELECTRONICA", "IM_LOGO_FAEL", Condicion, ImgLogo, 1950, 1500)
   If Result <> FAIL Then
      ImgLogo.Refresh
      If ImgLogo.Picture <> 0 Then
         PbLogo.Width = 3000
         PbLogo.Height = 3900
         PbLogo.AutoRedraw = True
         PbLogo.PaintPicture ImgLogo, 0, 0, PbLogo.ScaleWidth, PbLogo.ScaleHeight
         PbLogo.Picture = PbLogo.Image
         Call SavePicture(PbLogo.Picture, App.Path & "\Logo.bmp")
         Cargar_Logo = True
      Else
         Cargar_Logo = False
      End If
   End If
End Function

'---------------------------------------------------------------------------------------
' Procedure : ScmdFacEL_Click
' DateTime  : 28/09/2018 09:27
' Author    : daniel_mesa
' Purpose   : DRMG T44728-R41288
'---------------------------------------------------------------------------------------
'
Private Sub ScmdFacEL_Click(Index As Integer)
   ReDim VrArr(0)
   Select Case Index
      Case 0:
         Valores = "ESTADO_DIAN"
         Condicion = "DOCUMENTO LIKE 'FACTURA: ID" & TxtFactElt & "%' "
         Condicion = Condicion & " AND MODULO=1 ORDER BY FECHA DESC"
         Result = LoadData("TM_FACTURA_PLANO", Valores, Condicion, VrArr())
         If Result <> FAIL And Encontro Then
            If VrArr(0) = "6" Then
               Call ParametrosXML(TxtFactElt.Text)
            End If
         End If
      Case 1:
         Call Factura_PDF
      Case 2:
         Valores = "NU_ESTA_ENFA"
         Condicion = "NU_NUMFAC_ENFA=" & TxtFactElt
         Result = LoadData("ENVIO_FAEL", Valores, Condicion, VrArr())
         If Result <> FAIL And Encontro Then
            If VrArr(0) = False Then
               Call Enviar_PDF_XML
            End If
         End If
      Case 3:
         Call Actualizar_Estado(TxtFactElt)
   End Select
End Sub
'---------------------------------------------------------------------------------------
' Procedure : ScmdFacEL_KeyPress
' DateTime  : 02/10/2018 12:45
' Author    : daniel_mesa
' Purpose   : DRMG T44728-R41288 CAMBIA EL ENTER COMO TABULADOR
'---------------------------------------------------------------------------------------
'
Private Sub ScmdFacEL_KeyPress(Index As Integer, KeyAscii As Integer)
   Call Cambiar_Enter(KeyAscii)
End Sub

'---------------------------------------------------------------------------------------
' Procedure : Factura_PDF
' DateTime  : 02/10/2018 11:45
' Author    : daniel_mesa
' Purpose   : DRMG T44728-R41288 gENERA LA REPRESENTACION GRAFICA DE UNA FACTURA DE VENTA
'---------------------------------------------------------------------------------------
'
Function Factura_PDF()
   Dim StFormula As String 'Almacena la formula del reporte para qel filtro de los datos
   Dim CrxApp As New CRAXDRT.Application 'Objeto para abrir el reporte
   Dim CrxReport As New CRAXDRT.Report 'Objeto para instanciar el reporte
   Dim CrxTable As CRAXDRT.DatabaseTable  'Objeto para instanciar las tablas del reporte
   Dim InI As Integer 'Usado para ciclos for
   Dim BoQr As Boolean 'almacena true o false en cual se envia al informe si visualizar el codigo qr
   Dim BoLogo As Boolean 'almacena true o false en cual se envia al informe si visualizar el Logo
   Dim StPrefijo As String 'Almacena el Prefijo
   Dim DbValorParcial As Double 'ALMACENA EL VARLO PARCIAL
   Dim DbValorAcumula As Double 'ALMACENA EL VALOR ACUMULADO
   Dim Dueno As New ElTercero
   Dim StDoc(2) As String 'almacena el Encabesado  y los pies de pagina de la gactura
   Dim StCufe As String 'ALMACENA EL CODIGO CUFE DE LA FACTURA
   
   Dueno.IniXNit
   
   StFormula = "{IN_DETALLE.NU_AUTO_ORGCABE_DETA}={IN_ENCABEZADO.NU_AUTO_ENCA} AND "
   StFormula = StFormula & "{IN_DETALLE.NU_AUTO_MODCABE_DETA}={IN_ENCABEZADO_1.NU_AUTO_ENCA} AND "
   StFormula = StFormula & "{IN_DETALLE.NU_AUTO_BODE_DETA}=" & Me.cboBodega.ItemData(Me.cboBodega.ListIndex) & " AND "
   StFormula = StFormula & "{IN_ENCABEZADO.NU_AUTO_DOCU_ENCA}=" & Documento.Encabezado.TipDeDcmnt & " AND "
   StFormula = StFormula & "{IN_ENCABEZADO.NU_COMP_ENCA}=" & CLng(Me.txtNumero.Text)
   StFormula = StFormula & " AND {IN_ENCABEZADO.NU_AUTO_COMP_ENCA}=" & CLng(Documento.Encabezado.NumeroCOMPROBANTE)

   If Documento.Encabezado.TipDeDcmnt = FacturaVenta Then
      
      DbValorParcial = 0: DbValorAcumula = CCur(Me.txtTOTAL.Text)
      DbValorParcial = SumaOtrosCargos()
      DbValorAcumula = DbValorAcumula + DbValorParcial
      DbValorParcial = SumaOtrosDescuentos()
      DbValorAcumula = DbValorAcumula - IIf(UCase(Me.staINFORMA.Panels("NUEVO").Text) = UCase("Nuevo"), DbValorParcial, 0)
      
      ReDim VrArr(3)
      Valores = "TX_ENCA_FAEL,TX_PIEUNO_FAEL,TX_PIEDOS_FAEL,TX_URL_FAEL"
      Condicion = "NU_MODULO_FAEL=1"
      Condicion = Condicion & " AND NU_ESTADO_FAEL=0"
      Result = LoadData("FACTURA_ELECTRONICA", Valores, Condicion, VrArr())
      If Result <> FAIL Then
         If Encontro Then
            StDoc(0) = VrArr(0)
            StDoc(1) = VrArr(1)
            StDoc(2) = VrArr(2)
            StRutaPDF = VrArr(3)
         Else
            Factura_PDF = FAIL
            Exit Function
         End If
      Else
         Factura_PDF = FAIL
         Exit Function
      End If
      
      If StRutaPDF <> NUL$ Then
         If Mid(StRutaPDF, Len(StRutaPDF)) <> "\" Then StRutaPDF = StRutaPDF & "\"
         'DRMG T45943 INICIO SE DEJA EN COMENTARIO LA PRIMERA LINEA
         'StRutaPDF = StRutaPDF & Me.TxtFactElt.Text & ".PDF"
         ReDim VrArr(0)
         Desde = "TM_FACTURA_PLANO"
         Campos = "TOP 1 NOMBRE_ARCHIVO"
         Condicion = "DOCUMENTO LIKE 'FACTURA: ID" & Me.TxtFactElt.Text & "%' ORDER BY FECHA DESC"
         Result = LoadData(Desde, Campos, Condicion, VrArr)
         If Result <> FAIL And Encontro Then
            StRutaPDF = StRutaPDF & Replace(VrArr(0), ".xml", ".PDF")
         Else
            Result = FAIL
         End If
         'DRMG T45943 FIN
      Else
         Result = FAIL
      End If
      
      ReDim VrArr(1)
      Condicion = "NU_NUCOMP_FAFA=" & txtNumero
      Condicion = Condicion & " AND NU_DOCU_FAFA=" & FacturaVenta
      Condicion = Condicion & " AND NU_COMP_FAFA=" & Documento.Encabezado.NumeroCOMPROBANTE
      Valores = "TX_PREFI_FAFA + '-' + CONVERT(VARCHAR,NU_CNFAVE_FAFA)"
      Valores = Valores & Coma & "TX_CUFE_FAFA"
      Result = LoadData("R_FAVE_FAEL", Valores, Condicion, VrArr())
      If Result <> FAIL Then
         If Encontro Then
            StPrefijo = VrArr(0)
            StCufe = VrArr(1)
         Else
            Factura_PDF = FAIL
            Exit Function
         End If
      Else
         Factura_PDF = FAIL
         Exit Function
      End If
            
      BoQr = Generar_Qr(Me.TxtFactElt.Text)
      BoLogo = Cargar_Logo
      
      Set CrxReport = CrxApp.OpenReport(CStr(DirRpt & "infDCFDV_COS.RPT"), 0)
   
      CrxReport.Database.LogOnServer "PDSODBC.DLL", BDDSNODBCName, BDNameStr, cGUsuario, cGClave  'REALIZA LA CONEXION CON EL SERVIDOR
      CrxReport.DiscardSavedData
      
      For InI = 1 To CrxReport.FormulaFields.Count
         Select Case UCase$(CrxReport.FormulaFields.Item(InI).Name)
            Case "{@PREFIJ0}"
               CrxReport.FormulaFields.Item(InI).Text = "'" & IIf(StPrefijo = NUL$, NUL$, StPrefijo) & Comi
            Case "{@VALORBRUTO}"
               CrxReport.FormulaFields.Item(InI).Text = CCur(Me.txtTOTAL.Text)
            Case "{@OTROSCARGOS}"
               CrxReport.FormulaFields.Item(InI).Text = SumaOtrosCargos()
            Case "{@MOVASOC}"
               CrxReport.FormulaFields.Item(InI).Text = Movimiento_Asociado(txtNumero, CLng(Documento.Encabezado.TipDeDcmnt))
               If CrxReport.FormulaFields.Item(InI).value = 0 Then CrxReport.FormulaFields.Item(InI).Text = NUL$
            Case "{@DESCUENTO}"
               CrxReport.FormulaFields.Item(InI).Text = GrdArticulos.TextMatrix(1, 17)
            Case "{@OTROSDESCUENTOS}"
               CrxReport.FormulaFields.Item(InI).Text = DbValorParcial
            Case "{@VALORNUMEROS}"
               CrxReport.FormulaFields.Item(InI).Text = DbValorAcumula
            Case "{@VALORLETRAS}"
               CrxReport.FormulaFields.Item(InI).Text = Comi & Monto_Escrito(DbValorAcumula) & Comi
            Case "{@ENTIDAD}"
               CrxReport.FormulaFields.Item(InI).Text = Comi & Trim(Dueno.Nombre) & ", NIT:" & Trim(Dueno.Nit) & Comi
            Case "{@ENCABEZADO}"
               CrxReport.FormulaFields.Item(InI).Text = Comi & StDoc(0) & Comi
            Case "{@PIE1}"
               CrxReport.FormulaFields.Item(InI).Text = Comi & StDoc(1) & Comi
            Case "{@PIE2}"
               CrxReport.FormulaFields.Item(InI).Text = Comi & StDoc(2) & Comi
            Case "{@CUFE}"
               CrxReport.FormulaFields.Item(InI).Text = Comi & StCufe & Comi
            Case "{@QR}"
               If BoQr Then
                 CrxReport.FormulaFields.Item(InI).Text = Comi & "1" & Comi
               Else
                 CrxReport.FormulaFields.Item(InI).Text = Comi & "0" & Comi
               End If
            Case "{@LOGO}"
               If BoLogo Then
                 CrxReport.FormulaFields.Item(InI).Text = Comi & "1" & Comi
               Else
                 CrxReport.FormulaFields.Item(InI).Text = Comi & "0" & Comi
               End If
         End Select
      Next
         
      On Error GoTo Error_Archivo

         If Result <> FAIL Then
            CrxReport.RecordSelectionFormula = StFormula
            CrxReport.GroupSelectionFormula = StFormula
            For Each CrxTable In CrxReport.Database.Tables
               CrxTable.SetLogOnInfo BDDSNODBCName, NombBD, cGUsuario, cGClave
            Next
            CrxReport.ExportOptions.DiskFileName = StRutaPDF
            CrxReport.ExportOptions.DestinationType = crEDTDiskFile
            CrxReport.ExportOptions.FormatType = crEFTPortableDocFormat
            CrxReport.ExportOptions.PDFExportAllPages = False
            CrxReport.Export False
         End If
         MouseNorm
         Factura_PDF = Result
         Exit Function
            
Error_Archivo:
         Result = FAIL
         Factura_PDF = Result
         If ERR.Number = 0 Or ERR.Number = 20 Then Exit Function
         If ERR.Number = -2147206460 Then Call Mensaje1("No se puede encontrar una parte de la ruta de acceso", 1): Exit Function
         Call Mensaje1(ERR.Number & ": " & ERR.Description, 1)

   End If
   

End Function

'---------------------------------------------------------------------------------------
' Procedure : Generar_Qr
' DateTime  : 28/09/2018 10:47
' Author    : daniel_mesa
' Purpose   : DRMG T44728-R41288 'GENERA EL QR
'---------------------------------------------------------------------------------------
'
Private Function Generar_Qr(VrFactura As Variant)
   Dim StTexto As String 'Almacena el codigo que se enviara como qr
   Dim VrArr() As Variant 'Almacena resultados de la BD
   Set cQrCode = New ClsQrCode
   
   With Me
      ReDim VrArr(0)
      Result = LoadData("R_FAVE_FAEL", "TX_CUFE_FAFA", "NU_CNFAVE_FAFA=" & VrFactura, VrArr)
      If Result <> FAIL Then
         If Encontro And VrArr(0) <> NUL$ Then
            StTexto = VrArr(0)
            Generar_Qr = True
         Else
            Generar_Qr = False
            Exit Function
         End If
      Else
         Generar_Qr = False
         Exit Function
      End If
      .PbCordigoQr.Picture = cQrCode.GetPictureQrCode(StTexto, .PbCordigoQr.ScaleWidth, .PbCordigoQr.ScaleHeight)
      If .PbCordigoQr.Picture Is Nothing Then Generar_Qr = False
      If Result <> FAIL Then
         .PbCordigoQr.Refresh
         Call SavePicture(.PbCordigoQr.Image, App.Path & "\C�digo Qr.bmp")
      End If
   End With
   
End Function

'---------------------------------------------------------------------------------------
' Procedure : Enviar_PDF_XML
' DateTime  : 20/09/2018 16:55
' Author    : daniel_mesa
' Purpose   : DRMG T44728-R41288 ENVIA LA REPRESENTACION GRAFICA Y EL XML DE LA FACTURA ELECTRONICA
'---------------------------------------------------------------------------------------
'
Sub Enviar_PDF_XML()
   Dim VrArr() As Variant 'Almacena resultados de consultas SQL
   Dim StRutaXML As String 'Almacena la ruta del XML
   Dim VrCorreo(7) As Variant 'Almacena los datos a enviar
   Dim ClsMail As ClsCDOmail 'DRMG T45220 HEREDA LAS PROPIEDADES DE LA CLASE
   
   StRutaPDF = NUL$
   
   Call Factura_PDF

   If StRutaPDF <> NUL$ Then
   
      'ReDim VrArr(5) 'DRMG T45220 SE DEJA EN COMENTARIO
      ReDim VrArr(6) 'DRMG T45220
      Desde = "DATOS_EMAIL_FAC"
      Campos = "TX_ASUNTO_DEF,TX_DETA_DEF,TX_SMPT_DEF,TX_PUERTO_DEF,TX_CORREO_DEF,TX_CLAVE_DEF"
      Campos = Campos & Coma & "NU_SSL_DEF" 'DRMG T45220
      Condicion = "TX_MODULO_DEF=1"
      Result = LoadData(Desde, Campos, Condicion, VrArr)
      If Result <> FAIL And Encontro Then
         VrCorreo(0) = VrArr(0)
         VrCorreo(1) = VrArr(1)
         VrCorreo(2) = VrArr(2)
         VrCorreo(3) = VrArr(3)
         VrCorreo(4) = VrArr(4)
         VrCorreo(5) = VrArr(5)
         VrCorreo(6) = VrArr(6) 'DRMG T45220
      End If
       
      'TRAE EL CORREO DEL TERCERO
      ReDim VrArr(0)
      Desde = "R_FAVE_FAEL"
      Desde = Desde & " INNER JOIN IN_ENCABEZADO"
      Desde = Desde & " ON  NU_AUTO_DOCU_ENCA=NU_DOCU_FAFA"
      Desde = Desde & " AND NU_AUTO_COMP_ENCA=NU_COMP_FAFA"
      Desde = Desde & " AND NU_COMP_ENCA=NU_NUCOMP_FAFA"
      Desde = Desde & " INNER JOIN TERCERO ON CD_CODI_TERC_ENCA =CD_CODI_TERC"
      Campos = "TX_MAIL_TERC"
      Condicion = "NU_CNFAVE_FAFA =" & TxtFactElt
      Result = LoadData(Desde, Campos, Condicion, VrArr)
      If Result <> FAIL And Encontro Then
         VrCorreo(7) = VrArr(0)
      End If
        
      VrCorreo(0) = VrCorreo(0) & " Fra. No." & TxtFactElt
       
      ReDim VrArr(0)
      Desde = "TM_FACTURA_PLANO"
      Campos = "TOP 1 RUTA_ARCHIVO+'\'+NOMBRE_ARCHIVO"
      Condicion = "DOCUMENTO LIKE 'FACTURA: ID" & TxtFactElt & "%' ORDER BY FECHA DESC"
      Result = LoadData(Desde, Campos, Condicion, VrArr)
      If Result <> FAIL And Encontro Then
         StRutaXML = VrArr(0)
      End If
      
      If Dir$(StRutaXML) = "" Or StRutaXML = NUL$ Then
         Call Mensaje1("El archivo XML de la factura " & TxtFactElt & " no existe", 1)
         Call Guardar_email(TxtFactElt, 0, "El archivo XML de la factura " & TxtFactElt & " no existe", 2)
         Exit Sub
      End If
      
      If Result <> FAIL And VrCorreo(3) <> NUL$ Then
         'DRMG T45220 INICIO SE DEJA EN COMENTARIO LAS SIGUIENTES 29 LINEAS
'         Set poSendMail = New vbSendMail.clsSendMail
'         poSendMail.SMTPHostValidation = VALIDATE_NONE
'         poSendMail.EmailAddressValidation = VALIDATE_SYNTAX
'         poSendMail.Delimiter = ";"
'         poSendMail.SMTPHost = VrCorreo(2)
'         poSendMail.SMTPPort = VrCorreo(3)
'         poSendMail.From = VrCorreo(4)
'         poSendMail.FromDisplayName = "Factura Electronica"
'         poSendMail.Recipient = Trim(VrCorreo(7))
'         poSendMail.RecipientDisplayName = ""
'         poSendMail.CcRecipient = ""
'         poSendMail.CcDisplayName = ""
'         poSendMail.BccRecipient = ""
'         poSendMail.ReplyToAddress = VrCorreo(4)
'         poSendMail.Subject = Trim(VrCorreo(0))
'         poSendMail.Message = Trim(VrCorreo(1))
'         poSendMail.Attachment = StRutaPDF 'Trim(StRutaPDF & ";" & StRutaXML)
'         poSendMail.AsHTML = False
'         poSendMail.ContentBase = ""
'         poSendMail.EncodeType = MIME_ENCODE
'         poSendMail.Priority = NORMAL_PRIORITY
'         poSendMail.Receipt = False
'         poSendMail.UseAuthentication = True
'         poSendMail.UsePopAuthentication = False
'         poSendMail.UserName = VrCorreo(4)
'         poSendMail.PassWord = VrCorreo(5)
'         poSendMail.POP3Host = ""
'         poSendMail.MaxRecipients = 100
'         poSendMail.Send
         Set ClsMail = New ClsCDOmail
         With ClsMail
            .servidor = VrCorreo(2)
            .puerto = VrCorreo(3)
            .UseAuntentificacion = True
            .ssl = VrCorreo(6)
            .Usuario = VrCorreo(4)
            .PassWord = VrCorreo(5)
            .Asunto = Trim(VrCorreo(0))
            .de = VrCorreo(4)
            .para = Trim(VrCorreo(7))
            .Mensaje = Trim(VrCorreo(1))
            .Adjunto = Trim(StRutaPDF & ";" & StRutaXML)
            If .Enviar_Backup Then
               Call Mensaje1("Se envi� Factura Electr�nica Correctamente", 3)
               Call Guardar_email(TxtFactElt, 1, "Se envi� Factura Electr�nica Correctamente", 1)
            Else
               Call Mensaje1(.StMsj, 3)
               Call Guardar_email(TxtFactElt, 0, .StMsj, 2)
            End If
         End With
         Set ClsMail = Nothing
         'DRMG T45220 FIN
      End If
   End If
   
End Sub
'DRMG T45220 INICIO SE DEJA EN COMENTARIO EL SIGUIENTE BLOQUE
''---------------------------------------------------------------------------------------
'' Procedure : poSendMail_SendSuccesful
'' DateTime  : 13/06/2018 16:16
'' Author    : daniel_mesa
'' Purpose   : DRMG T44728-R41288 Muestra mensaje de envio exitozo
''---------------------------------------------------------------------------------------
''
'Private Sub poSendMail_SendSuccesful()
'    Call Mensaje1("Se envi� Factura Electr�nica Correctamente", 3)
'    Call Guardar_email(TxtFactElt, 1, "Se envi� Factura Electr�nica Correctamente", 1)
'End Sub
'
''---------------------------------------------------------------------------------------
'' Procedure : poSendMail_SendFailed
'' DateTime  : 13/06/2018 16:17
'' Author    : daniel_mesa
'' Purpose   : DRMG T44728-T41288 Muestra mensaje si no se pudo enviar el correo
''---------------------------------------------------------------------------------------
''
'Private Sub poSendMail_SendFailed(Explanation As String)
'   Call Mensaje1("Error al enviar el correo: " & Explanation, 3)
'   Call Guardar_email(TxtFactElt, 0, "Error al enviar el correo: " & Explanation, 2)
'End Sub
'DRMG T45220 FIN

'---------------------------------------------------------------------------------------
' Procedure : Guardar_email
' DateTime  : 21/09/2018 08:59
' Author    : daniel_mesa
' Purpose   : DRMG T44728-R41288 Guarda el estado de lo envios por correo y generacion de los XML
' de las facturas electronicas
'---------------------------------------------------------------------------------------
'
Sub Guardar_email(StFac As String, Optional InEmail As Integer = 0, Optional StMsjEmail As String = NUL$, Optional InXML As Integer = 0, Optional StMsjXML As String = NUL$)
   Dim VrArr() As Variant 'Almacena resutados de consultas por BD
   
   If StMsjEmail <> NUL$ Then StMsjEmail = Cambiar_Comas_Comillas(StMsjEmail) 'DRMG T45277-R41288
   If StMsjXML <> NUL$ Then StMsjXML = Cambiar_Comas_Comillas(StMsjXML) 'DRMG T45277-R41288
   
   ReDim VrArr(0)
   'DRMG T45136 INICIO SE DEJA EN COMENTARIO LAS SIGUIENTES 9 LINEAS
'   Condi = "NU_NUMFAC_FAEL=" & StFac
'   Condi = Condi & " AND NU_MODULO_FAEL=1"
'   Result = LoadData("ENVIO_FAEL", "NU_NUMFAC_FAEL", Condi, VrArr)
'
'   Valores = "NU_NUMFAC_FAEL=" & StFac
'   If InEmail <> 2 Then Valores = Valores & Coma & "NU_ESTA_FAEL=" & InEmail
'   If StMsjEmail <> NUL$ Then Valores = Valores & Coma & "TX_OBEMAIL_FAEL='" & StMsjEmail & Comi
'   If InXML <> 2 Then Valores = Valores & Coma & "NU_ESTXML_FAEL=" & InXML
'   If StMsjXML <> NUL$ Then Valores = Valores & Coma & "TX_OBXML_FAEL='" & StMsjXML & Comi
'   Valores = Valores & Coma & " NU_MODULO_FAEL=1"

   Condi = "NU_NUMFAC_ENFA=" & StFac
   Condi = Condi & " AND NU_MODULO_ENFA=1"
   Result = LoadData("ENVIO_FAEL", "NU_NUMFAC_ENFA", Condi, VrArr)
   
   Valores = "NU_NUMFAC_ENFA=" & StFac
   If InEmail <> 2 Then Valores = Valores & Coma & "NU_ESTA_ENFA=" & InEmail
   If StMsjEmail <> NUL$ Then Valores = Valores & Coma & "TX_OBEMAIL_ENFA='" & StMsjEmail & Comi
   If InXML <> 2 Then Valores = Valores & Coma & "NU_ESTXML_ENFA=" & InXML
   If StMsjXML <> NUL$ Then Valores = Valores & Coma & "TX_OBXML_ENFA='" & StMsjXML & Comi
   Valores = Valores & Coma & " NU_MODULO_ENFA=1"
   'DRMG T45136 FIN
   
   If Result <> FAIL Then
      If Encontro And VrArr(0) <> NUL$ Then
         Result = DoUpdate("ENVIO_FAEL", Valores, Condi)
      Else
         Result = DoInsertSQL("ENVIO_FAEL", Valores)
      End If
   End If
   
   If Result = FAIL Then
      Call Mensaje1("Error al guardar el estado del envio de la factura", 1)
   End If
End Sub

'---------------------------------------------------------------------------------------
' Procedure : ParametrosXML
' DateTime  : 30/05/2018 15:39
' Author    : daniel_mesa
' Purpose   : DRMG T44728-R41288
' Procedimiento en cual recolecta los parametros necesarios para la generacion del XML
'---------------------------------------------------------------------------------------
'
Function ParametrosXML(idFactura As String)
   Dim boError As Boolean
   Dim StMensaje As String 'Almacena el resultado de la generacion del XML
   Dim IdUsuario As String 'ALMACENA EL ID DEL USUARIO ACTUAL
   Dim equipo As String 'ALMACENA EL NOMBRE DEL EQUIPO
   Dim rutaFactura As String 'ALMACENA LA RUTA DE LA FACTURA
   Dim rutaCertificado As String 'ALMACENA LA RUTA DEL CERTIFICADO
   Dim contrasenia As String 'ALMACENA LA CONTRASE�A
   Dim conexion As String 'ALMACENA LA CONEXION A LA BD
   Dim VrArrr() As Variant 'Almacena las consultas sql
   
   ReDim VrArr(2)
   Valores = "TX_URL_FAEL,TX_URLCER_FAEL,TX_PSSWRD_FAEL"
   Condicion = "NU_MODULO_FAEL=1"
   Condicion = Condicion & " AND NU_ESTADO_FAEL=0"
   Result = LoadData("FACTURA_ELECTRONICA", Valores, Condicion, VrArr())
   If Result <> FAIL Then
      If Encontro Then
         rutaFactura = VrArr(0)
         rutaCertificado = VrArr(1)
         contrasenia = CStr(VrArr(2))
         IdUsuario = UserId
         equipo = ComputerName()
         conexion = "Data Source=" & ServerBD & "; Initial Catalog=" & NombBD & "; User ID=" & cGUsuario & "; Password = " & cGClave
         'stMensaje = GenerarFacturaElectronica(idFactura, IdUsuario, equipo, rutaFactura, rutaCertificado, contrasenia, conexion) 'JTVG T45164 Se deja en comentario
         StMensaje = GenerarFacturaElectronica(idFactura, IdUsuario, equipo, conexion, 1, "F") 'JTVG T45164
         Call Mensaje1(StMensaje, 3)
         ReDim VrArr(1)
         Campos = "TOP 1 RUTA_ARCHIVO+'\'+NOMBRE_ARCHIVO"
         Campos = Campos & Coma & "OBSERVACION"
         Desde = "TM_FACTURA_PLANO"
         Condicion = "DOCUMENTO like 'FACTURA: ID" & idFactura & "%' ORDER BY FECHA DESC"
         Result = LoadData(Desde, Campos, Condicion, VrArr())
            If Result <> FAIL Then
               If Encontro Then
                  If Dir$(VrArr(0)) <> "" Or VrArr(1) <> NUL$ Then
                     Call Guardar_email(idFactura, 2, , 1, StMensaje)
                  Else
                     Call Guardar_email(idFactura, 2, , , StMensaje)
                  End If
               Else
                  Call Guardar_email(idFactura, 2, , , StMensaje)
               End If
            Else
               Call Guardar_email(idFactura, 2, , , StMensaje)
            End If
      End If
   End If

End Function
'---------------------------------------------------------------------------------------
' Procedure : ComputerName
' DateTime  : 30/05/2018 17:32
' Author    : daniel_mesa
' Purpose   : DRMG T44728-R41288 Halla el nombre del equipo en el que se esta trabajando
'---------------------------------------------------------------------------------------
'
Public Function ComputerName() As String
   ComputerName = Space$(260)
   GetComputerName ComputerName, Len(ComputerName)
   ComputerName = Left$(ComputerName, InStr(ComputerName, vbNullChar) - 1)
End Function
'---------------------------------------------------------------------------------------
' Procedure : EncryptCadena
' DateTime  : 28/05/2018 07:00
' Author    : DANIEL MESA
' Purpose   : DRMG T44728-R41288 Genera la factura electronica
'---------------------------------------------------------------------------------------
'Function GenerarFacturaElectronica(idFactura As String, IdUsuario As String, equipo As String, rutaFactura As String, rutaCertificado As String, contrasenia As String, conexion As String) As String 'JTVG T45164 Se deja en comentario
 Function GenerarFacturaElectronica(idFactura As String, IdUsuario As String, equipo As String, conexion As String, modulo As Integer, tipoDocumento As String) As String 'JTVG T45164
  On Error GoTo GenerarFacturaElectronica_Error
   Dim Resultado As String
   Dim obFacturaElectronica As FacturacionElectronica.ComFacturaElectronica
   Set obFacturaElectronica = New FacturacionElectronica.ComFacturaElectronica
   'Resultado = obFacturaElectronica.GenerarFacturaElectronica(idFactura, IdUsuario, equipo, rutaFactura, rutaCertificado, contrasenia, conexion, CByte(0)) 'JTVG T45164 Se deja en comentario
   Resultado = obFacturaElectronica.GenerarFacturaElectronica(idFactura, IdUsuario, equipo, conexion, modulo, tipoDocumento, CByte(0)) ' JTVG T45164
   GoTo Terminar
    
GenerarFacturaElectronica_Error:
      If ERR = 429 Then
         Resultado = "La DLL FacturacionElectronica.dll no se encuentra registrada. No se realizar� la generacion de la factura xml."
      Else
         Resultado = "Error " & ERR.Number & " (" & ERR.Description & ")"
      End If
Terminar:
   GenerarFacturaElectronica = Resultado
   

End Function

'---------------------------------------------------------------------------------------
' Procedure : Fave_Impu_Desc_Fael
' DateTime  : 01/10/2018 13:06
' Author    : daniel_mesa
' Purpose   : DRMG T44728-R41288 GUARDA TODOS LOS IMPUESTOS Y DESCUENTOS APLICADOS A LA
'             FACTURA DE VENTA
'---------------------------------------------------------------------------------------
'
Sub Fave_Impu_Desc_Fael()

   Dim InFila As Integer 'Variable usada para recorrer las filas de los grids
   
   For InFila = 1 To Me.grdOtrosDescuentos.Rows - 1
      Valores = "NU_CONFA_DEFA= " & Me.TxtFactElt.Text
      Valores = Valores & Coma & "NU_PORCEN_DEFA=" & CDbl(Me.grdOtrosDescuentos.TextMatrix(InFila, 3))
      Valores = Valores & Coma & "NU_VALOR_DEFA=" & CDbl(Me.grdOtrosDescuentos.TextMatrix(InFila, 2))
      Valores = Valores & Coma & "TX_CODDES_DEFA='" & Me.grdOtrosDescuentos.TextMatrix(InFila, 0) & Comi
      Valores = Valores & Coma & "NU_BASE_DEFA=" & CDbl(Me.grdOtrosDescuentos.TextMatrix(InFila, 4)) 'DRMG T45221-R41288
      Valores = Valores & Coma & "NU_MODULO_DEFA=1"
      If Result <> FAIL Then Result = DoInsertSQL("DESCUENTO_FAVE", Valores)
   Next
   
   If Result = FAIL Then Exit Sub
   
   For InFila = 1 To Me.grdOtrosCargos.Rows - 1
      Valores = "NU_CONFA_IMFA=" & Me.TxtFactElt.Text
      Valores = Valores & Coma & "NU_PORCEN_IMFA=" & CDbl(Me.grdOtrosCargos.TextMatrix(InFila, 3))
      Valores = Valores & Coma & "NU_VALOR_IMFA=" & CDbl(Me.grdOtrosCargos.TextMatrix(InFila, 2))
      Valores = Valores & Coma & "TX_IMPU_IMFA='" & Me.grdOtrosCargos.TextMatrix(InFila, 0) & Comi
      Valores = Valores & Coma & "NU_BASE_IMFA=" & CDbl(Me.grdOtrosCargos.TextMatrix(InFila, 4)) 'DRMG T4221
      'Valores = Valores & Coma & "NU_MODULO_INFA=1" 'DRMG T45162-R41288 SE DEJA EN COMENTARIO
      Valores = Valores & Coma & "NU_MODULO_IMFA=1" 'DRMG T45162-R41288
      If Result <> FAIL Then Result = DoInsertSQL("IMPUESTO_FAVE", Valores)
   Next
   
   If Result = FAIL Then Exit Sub

End Sub

'---------------------------------------------------------------------------------------
' Procedure : Actualizar_Estado
' DateTime  : 29/11/2018
' Author    : daniel_mesa
' Purpose   : DRMG T44728-R41288 busca y actualiza los estados de las facturas electronicas
'---------------------------------------------------------------------------------------
'
Sub Actualizar_Estado(StFact As String)
   Dim StConexion As String 'Es utilidad para armar una conexion a la BD
   Dim StMensaje As String 'Almacena el resultado
   StConexion = "Data Source=" & ServerBD & "; Initial Catalog=" & NombBD & "; User ID=" & cGUsuario & "; Password = " & cGClave
   StMensaje = ConsultarEstadoDocumentoDian(StFact, StConexion, 1)
   Call Mensaje1(StMensaje, 3)

End Sub

'---------------------------------------------------------------------------------------
' Procedure : ConsultarEstadoDocumentoDian
' DateTime  : 29/11/2018
' Author    : Hector Rodriguez
' Purpose   : DRMG T44728-R41288 Consultar el estado de un documento en la Dian
'---------------------------------------------------------------------------------------
Function ConsultarEstadoDocumentoDian(idFactura As String, conexion As String, modulo As Integer) As String
   On Error GoTo ConsultarEstadoDocumentoDian_Error
   Dim Resultado As String
   Dim obFacturaElectronica As FacturacionElectronica.ComFacturaElectronica
   Set obFacturaElectronica = New FacturacionElectronica.ComFacturaElectronica
   Resultado = obFacturaElectronica.ConsultarEnvio(idFactura, 0, conexion, modulo)
   GoTo Terminar
    
ConsultarEstadoDocumentoDian_Error:
      If ERR = 429 Then
         Resultado = "La DLL FacturacionElectronica.dll no se encuentra registrada. No se realizar� la consulta del estado del documento en la Dian."
      Else
         Resultado = "Error " & ERR.Number & " (" & ERR.Description & ")"
      End If
Terminar:
   ConsultarEstadoDocumentoDian = Resultado
   

End Function

'---------------------------------------------------------------------------------------
' Procedure : ConsultaPacientes
' DateTime  : 16/04/2019 16:47
' Author    : daniel_mesa
' Purpose   : DRMG T45983 BUSCA SI EL MOVIMIENTO ACTUAL ESTA RELACIONADO CON EL MODULO DE PACIENTES
'---------------------------------------------------------------------------------------
'
Function ConsultaPacientes()
   Dim VrArr() As Variant 'Almacena resultados de la consultas
   
   ConsultaPacientes = False
   
   ReDim VrArr(0, 0)
   Campos = "NU_AUTO_ARTI_DETA"
   Desde = "IN_DETALLE, ARTICULO"
   Desde = Desde & Coma & "IN_ENCABEZADO, ENF_INVEMOV"
   Condi = "NU_AUTO_MODCABE_DETA=" & Documento.Encabezado.AutoDelDocCARGADO
   Condi = Condi & " AND NU_AUTO_ORGCABE_DETA=NU_AUTO_ENCA"
   Condi = Condi & " AND NU_AUTO_DOCU_ENCA =" & Documento.Encabezado.TipDeDcmnt
   Condi = Condi & " AND NU_AUTO_COMP_EINM= NU_AUTO_COMP_ENCA AND NU_CONSEC_EINM=NU_COMP_ENCA "
   Condi = Condi & " AND TX_ESTA_ENCA<>'A'"
   Condi = Condi & " AND NU_AUTO_ARTI=NU_AUTO_ARTI_DETA"
   Condi = Condi & " AND NU_AUTO_BODE_DETA=" & Documento.Encabezado.BodegaORIGEN
   
   Result = LoadMulData(Desde, Campos, Condi, VrArr())
   If Result <> FAIL And Encontro Then
      ConsultaPacientes = True
      Exit Function
   End If

End Function

