VERSION 5.00
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "MSFLXGRD.OCX"
Object = "{C932BA88-4374-101B-A56C-00AA003668DC}#1.1#0"; "MSMASK32.OCX"
Object = "{0BA686C6-F7D3-101A-993E-0000C0EF6F5E}#1.0#0"; "THREED32.OCX"
Begin VB.Form FrmSeleccion2 
   BorderStyle     =   1  'Fixed Single
   ClientHeight    =   5205
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   8505
   Icon            =   "FrmSele2.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   5205
   ScaleWidth      =   8505
   StartUpPosition =   2  'CenterScreen
   Begin VB.Frame FrmRango_Fechas 
      Height          =   615
      Left            =   480
      TabIndex        =   0
      Top             =   80
      Width           =   7215
      Begin VB.CheckBox ChkConFechas 
         Caption         =   "Condicionar rango de Fechas "
         Height          =   375
         Left            =   120
         TabIndex        =   1
         Top             =   120
         Width           =   2895
      End
      Begin MSMask.MaskEdBox MskFechaInicial 
         Height          =   315
         Left            =   3840
         TabIndex        =   3
         ToolTipText     =   "d�a/mes/a�o"
         Top             =   180
         Width           =   1170
         _ExtentX        =   2064
         _ExtentY        =   556
         _Version        =   393216
         MaxLength       =   10
         Format          =   "dd/mm/yyyy"
         Mask            =   "##/##/####"
         PromptChar      =   " "
      End
      Begin MSMask.MaskEdBox MskFechaFinal 
         Height          =   315
         Left            =   5880
         TabIndex        =   5
         ToolTipText     =   "d�a/mes/a�o"
         Top             =   180
         Width           =   1170
         _ExtentX        =   2064
         _ExtentY        =   556
         _Version        =   393216
         MaxLength       =   10
         Format          =   "dd/mm/yyyy"
         Mask            =   "##/##/####"
         PromptChar      =   " "
      End
      Begin VB.Label Lbl_Hasta 
         Alignment       =   1  'Right Justify
         Caption         =   "Hasta:"
         Height          =   255
         Left            =   5040
         TabIndex        =   4
         Top             =   240
         Width           =   750
      End
      Begin VB.Label Lbl_Desde 
         Alignment       =   1  'Right Justify
         Caption         =   "Desde:"
         Height          =   255
         Left            =   2880
         TabIndex        =   2
         Top             =   240
         Width           =   750
      End
   End
   Begin VB.ComboBox TxtDesde 
      Height          =   315
      ItemData        =   "FrmSele2.frx":058A
      Left            =   2880
      List            =   "FrmSele2.frx":05A0
      TabIndex        =   7
      Text            =   "Iniciales"
      Top             =   720
      Width           =   3015
   End
   Begin Threed.SSCommand SscMarcar 
      Height          =   495
      Left            =   7800
      TabIndex        =   8
      ToolTipText     =   "Marcar o Desmarcar todo"
      Top             =   480
      Visible         =   0   'False
      Width           =   615
      _Version        =   65536
      _ExtentX        =   1085
      _ExtentY        =   873
      _StockProps     =   78
      Caption         =   "TODO"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   6
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Picture         =   "FrmSele2.frx":05CC
   End
   Begin MSFlexGridLib.MSFlexGrid Grd_Selecciones 
      Height          =   3255
      Left            =   120
      TabIndex        =   9
      Top             =   1080
      Width           =   8235
      _ExtentX        =   14526
      _ExtentY        =   5741
      _Version        =   393216
      Cols            =   9
      FixedCols       =   0
      ForeColorSel    =   -2147483635
      BackColorBkg    =   12632256
      FocusRect       =   2
      MergeCells      =   2
      AllowUserResizing=   1
   End
   Begin Threed.SSCommand SscAceptar 
      Height          =   495
      Left            =   2760
      TabIndex        =   10
      Top             =   4440
      Width           =   1095
      _Version        =   65536
      _ExtentX        =   1931
      _ExtentY        =   873
      _StockProps     =   78
      Caption         =   "&ACEPTAR"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   6
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Picture         =   "FrmSele2.frx":08EE
   End
   Begin Threed.SSCommand SscCancelar 
      Height          =   495
      Left            =   4800
      TabIndex        =   11
      Top             =   4440
      Width           =   1095
      _Version        =   65536
      _ExtentX        =   1931
      _ExtentY        =   873
      _StockProps     =   78
      Caption         =   "&CANCELAR"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   6
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Picture         =   "FrmSele2.frx":0C40
   End
   Begin VB.Label LblTitulo3 
      Height          =   255
      Left            =   5160
      TabIndex        =   17
      Top             =   5400
      Visible         =   0   'False
      Width           =   1215
   End
   Begin VB.Label LblTitulo2 
      Height          =   255
      Left            =   5160
      TabIndex        =   16
      Top             =   4440
      Visible         =   0   'False
      Width           =   1215
   End
   Begin VB.Label LblTitulo1 
      Height          =   255
      Left            =   5160
      TabIndex        =   15
      Top             =   4080
      Visible         =   0   'False
      Width           =   1215
   End
   Begin VB.Label Lbl_Titulo 
      Alignment       =   2  'Center
      Caption         =   "Lbl_Titulo"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   1080
      TabIndex        =   14
      Top             =   4800
      Visible         =   0   'False
      Width           =   1035
   End
   Begin VB.Label Lbl_Campos 
      BackColor       =   &H80000009&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Lbl_Campos"
      Height          =   255
      Left            =   1080
      TabIndex        =   13
      Top             =   4440
      Visible         =   0   'False
      Width           =   975
   End
   Begin VB.Label Lbl_Tabla 
      BackColor       =   &H80000009&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Lbl_Tabla"
      Height          =   255
      Left            =   120
      TabIndex        =   12
      Top             =   4440
      Visible         =   0   'False
      Width           =   855
   End
   Begin VB.Label LblRango 
      AutoSize        =   -1  'True
      Caption         =   "Desde : "
      Height          =   195
      Left            =   2040
      TabIndex        =   6
      Top             =   720
      Width           =   600
   End
End
Attribute VB_Name = "FrmSeleccion2"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Dim ruta As String
Dim todo As Boolean
Dim Rowsel As Long
Private vClDoBu As String
'JLPB T15675 INICIO
Dim DbCXP As Double
Dim InPosc As Integer
Dim InNCXP As Integer
Dim StNCXP As String
'JLPB T15675 FIN
Public Lbl_Condicion As String 'DRMG T46252


Private Sub ChkConFechas_Click()
If ChkConFechas.value = 1 Then
    MskFechaInicial.Enabled = True
    MskFechaFinal.Enabled = True
Else
    MskFechaInicial.Enabled = False
    MskFechaFinal.Enabled = False
End If
 'AASV R2051
End Sub

Private Sub ChkConFechas_KeyPress(KeyAscii As Integer)
Call Cambiar_Enter(KeyAscii) 'AASV M3532
End Sub


Private Sub Grd_selecciones_Click()
Dim RowAux As Long
 Grd_Selecciones.Col = 7
 If Grd_Selecciones.TextMatrix(Grd_Selecciones.Row, 0) <> "" And Grd_Selecciones.Row <> 0 Then
  If Grd_Selecciones.TextMatrix(Grd_Selecciones.Row, 6) = "0" Then
      Grd_Selecciones.TextMatrix(Grd_Selecciones.Row, 6) = "1"
      Grd_Selecciones.CellPictureAlignment = flexAlignCenterTop
      Set Grd_Selecciones.CellPicture = MDI_Inventarios.ImgLstInventar.ListImages(1).Picture
      RowAux = Grd_Selecciones.Row
      If Rowsel <> 0 And Rowsel <> Grd_Selecciones.Row Then
        Grd_Selecciones.Row = Rowsel
        Grd_Selecciones.TextMatrix(Rowsel, 6) = "0"
        Grd_Selecciones.CellPictureAlignment = flexAlignCenterTop
        Set Grd_Selecciones.CellPicture = LoadPicture()
      End If
      Grd_Selecciones.Row = RowAux
      Rowsel = RowAux
  Else
      Grd_Selecciones.TextMatrix(Grd_Selecciones.Row, 6) = "0"
      Grd_Selecciones.CellPictureAlignment = flexAlignCenterTop
      Set Grd_Selecciones.CellPicture = LoadPicture()
  End If
 End If
 'JLPB T15675 INICIO
 If Screen.ActiveForm.Caption = "DEVOLUCION ENTRADA" Then
    Dim VrArr() As Variant
    ReDim VrArr(0)
    Result = LoadData("IN_ENCABEZADO with(nolock)", "NU_CXP_ENCA", "NU_AUTO_ENCA=" & Grd_Selecciones.TextMatrix(Grd_Selecciones.Row, 0), VrArr)
    If Result <> FAIL And Encontro And DbCXP = 0 Then DbCXP = IIf(VrArr(0) <> NUL$, VrArr(0), 0): InPosc = Grd_Selecciones.Row
    If DbCXP = 0 Then InNCXP = InPosc: StNCXP = VrArr(0)
    If DbCXP <> 0 Then
       If Grd_Selecciones.TextMatrix(Grd_Selecciones.Row, 6) = "1" Then
          ReDim VrArr(0)
          Result = LoadData("IN_ENCABEZADO with(nolock)", "NU_CXP_ENCA", "NU_AUTO_ENCA=" & Grd_Selecciones.TextMatrix(Grd_Selecciones.Row, 0), VrArr)
          If Result <> FAIL And Encontro Then
             If VrArr(0) <> DbCXP Then
                Grd_Selecciones.TextMatrix(Grd_Selecciones.Row, 6) = "0"
                Grd_Selecciones.CellPictureAlignment = flexAlignCenterTop
                Set Grd_Selecciones.CellPicture = LoadPicture()
             ElseIf StNCXP = NUL$ Then
                Grd_Selecciones.TextMatrix(Grd_Selecciones.Row, 6) = "0"
                Grd_Selecciones.CellPictureAlignment = flexAlignCenterTop
                Set Grd_Selecciones.CellPicture = LoadPicture()
             End If
          End If
       End If
    End If
    If Grd_Selecciones.TextMatrix(InPosc, 6) = "0" Then DbCXP = 0: InPosc = 0
 End If
 'JLPB T15675 FIN
End Sub

Private Sub Form_Load()
 Call GrdFDef(Grd_Selecciones, 0, 0, _
  "AutCons,0," & _
  "Consecutivo,1000," & _
  "N�mero,+1000," & _
  "Fecha,1000," & _
  "Nit,+1000," & _
  "Tercero,2000," & _
  "Sel,0," & _
  "Marcar,1000, " & _
  "NumDocu,0")
 ruta = App.Path & "\chulo2.bmp"
' Titulos_Grid
 limpia_matriz Mselec, 1000, 4
 
 MskFechaInicial.Enabled = False 'AASV R2051
 MskFechaFinal.Enabled = False 'AASV R2051
 MskFechaInicial.Text = Date
 MskFechaFinal.Text = Date
End Sub

Private Sub Grd_Selecciones_EnterCell()
    Rowsel = Grd_Selecciones.Row
End Sub

Private Sub Grd_Selecciones_KeyPress(KeyAscii As Integer)
    Select Case KeyAscii
    Case vbKeyReturn, vbKeySpace
        Call Grd_selecciones_Click
    End Select
End Sub

Private Sub SscAceptar_Click()
  Dim aux As Integer
  Dim aux2 As Integer
  aux = 1
  aux2 = 1
  Grd_Selecciones.Row = 0
  While aux <= Grd_Selecciones.Rows - 1
   Grd_Selecciones.Row = Grd_Selecciones.Row + 1
   Grd_Selecciones.Col = 6
   If Grd_Selecciones.Text = "1" Then
    If Grd_Selecciones.TextMatrix(aux, 0) <> "" Then
     Mselec(aux2, 1) = Grd_Selecciones.TextMatrix(aux, 0)
     Mselec(aux2, 2) = Grd_Selecciones.TextMatrix(aux, 2)
     Mselec(aux2, 3) = Grd_Selecciones.TextMatrix(aux, 4)
     Mselec(aux2, 4) = Grd_Selecciones.TextMatrix(aux, 8)
     aux2 = aux2 + 1
     BoSeleccion = True 'AASV M3694
    End If
   End If
   aux = aux + 1
  Wend
  Unload Me
End Sub
Private Sub SscCancelar_Click()
  Unload Me
End Sub
Private Sub SscMarcar_Click()
   aux = 1
   While aux <= Grd_Selecciones.Rows - 1
    Grd_Selecciones.Col = 7
    Grd_Selecciones.Row = aux
    If Grd_Selecciones.TextMatrix(aux, 0) <> "" Then
     If todo = False Then
      Grd_Selecciones.TextMatrix(aux, 6) = "1"
      Grd_Selecciones.CellPictureAlignment = 3
      Set Grd_Selecciones.CellPicture = MDI_Inventarios.ImgLstInventar.ListImages(1).Picture
     Else
      Grd_Selecciones.TextMatrix(aux, 6) = "0"
      Grd_Selecciones.CellPictureAlignment = 3
      Set Grd_Selecciones.CellPicture = LoadPicture()
     End If
    End If
    aux = aux + 1
   Wend
   If todo = False Then
     todo = True
   Else
     todo = False
   End If
End Sub

Private Sub TxtDesde_KeyPress(KeyAscii As Integer)
  Call Cambiar_Enter(KeyAscii)
End Sub
Private Sub TxtDesde_LostFocus()
   Dim aux As Integer
   Grd_Selecciones.Rows = 1
   Grd_Selecciones.Rows = 2
'   Titulos_Grid
   DoEvents
   Codigo = IIf(Len(vClDoBu) = 0, Mid(Lbl_Campos, 1, InStr(Lbl_Campos, ",") - 1), vClDoBu)
   If Trim(TxtDesde) = "*" Or Trim(TxtDesde) = "%" Then TxtDesde = CaractLike
   'Condicion = Codigo & " Like '" & TxtDesde & CaractLike & Comi & IIf(Lbl_Condicion = NUL$, NUL$, " AND " & Lbl_Condicion) & " ORDER BY " & Codigo
   '****JACC M2363
   
   If Mid(Lbl_Condicion, Len(Lbl_Condicion), Len(Lbl_Condicion)) = ")" Then
       'Condicion = Codigo & " Like '" & TxtDesde & CaractLike & Comi & IIf(Lbl_Condicion = NUL$, NUL$, " AND " & Lbl_Condicion) & " ORDER BY " & Codigo
       Condicion = Codigo & " Like '" & TxtDesde & CaractLike & Comi & IIf(Lbl_Condicion = NUL$, NUL$, " AND " & Lbl_Condicion)  'AASV R2051
   Else
       'Condicion = Codigo & " Like '" & TxtDesde & CaractLike & Comi & IIf(Lbl_Condicion = NUL$, NUL$, " AND " & Lbl_Condicion) & ") ORDER BY " & Codigo
       Condicion = Codigo & " Like '" & TxtDesde & CaractLike & Comi & IIf(Lbl_Condicion = NUL$, NUL$, " AND " & Lbl_Condicion)  'AASV R2051
   End If
   '****JACC M2363
   'AASV R2051
   If ChkConFechas.value = 1 Then
      Condicion = Condicion & " AND FE_CREA_ENCA >=" & FFechaCon(MskFechaInicial)
      Condicion = Condicion & " AND FE_CREA_ENCA <=" & FFechaCon(MskFechaFinal)
   End If
   Condicion = Condicion & " ORDER BY " & Codigo
   'AASV R2051
   If TxtDesde.Text <> "" Then
    Reult = LoadfGrid(Grd_Selecciones, Lbl_Tabla, Lbl_Campos, Condicion)
    For aux = 1 To Grd_Selecciones.Rows - 1 Step 1
        Grd_Selecciones.TextMatrix(aux, 3) = Format(Grd_Selecciones.TextMatrix(aux, 3), "dd/mm/yyyy")
'        Grd_Selecciones.TextMatrix(aux, 2) = Format(Grd_Selecciones.TextMatrix(aux, 2), "############0.##00")
    Next
   End If
   
   
salto:
   aux = 1
   Grd_Selecciones.Row = 0
   While aux <= Grd_Selecciones.Rows - 1
    Grd_Selecciones.Row = Grd_Selecciones.Row + 1
    Grd_Selecciones.Col = 6
    Grd_Selecciones.Text = "0"
    aux = aux + 1
   Wend
End Sub
Private Sub Form_Activate()
If todo = False Then
    TxtDesde.SetFocus
Else
    TxtDesde.Text = CaractLike
    TxtDesde_LostFocus
End If
   'JLPB T15675 INICIO
   DbCXP = 0
   InPosc = 1
   StNCXP = 0
   'JLPB T15675 FIN
End Sub
Private Sub TxtDesde_GotFocus()
    TxtDesde.SelStart = 0
    TxtDesde.SelLength = Len(TxtDesde.Text)
    SendKeys "{F4}" 'AASV M3532 DESPLIEGA EL COMBO CUANDO RECIBE EL FOCO
End Sub

'DEPURACION DE CODIGO
'Private Sub Titulos_Grid()
'    Grd_Selecciones.ColWidth(0) = 1700
'    Grd_Selecciones.ColWidth(1) = 1700
'    Grd_Selecciones.ColWidth(2) = 1700
'    Grd_Selecciones.ColWidth(3) = 0
'    Grd_Selecciones.ColWidth(4) = 700
'    Grd_Selecciones.Row = 0
'    Grd_Selecciones.Col = 0
'    Grd_Selecciones.CellAlignment = 4
'    Grd_Selecciones.Text = LblTitulo1
'    Grd_Selecciones.Col = 1
'    Grd_Selecciones.CellAlignment = 4
'    Grd_Selecciones.Text = LblTitulo2
'    Grd_Selecciones.Col = 2
'    Grd_Selecciones.CellAlignment = 4
'    Grd_Selecciones.Text = LblTitulo3
'    Grd_Selecciones.Col = 4
'    Grd_Selecciones.CellAlignment = 4
'    Grd_Selecciones.Text = "Marcar"
'    Grd_Selecciones.Row = 1
'    todo = False
'End Sub

Public Property Let ColDondeBusca(Cue As String)
    vClDoBu = Cue
End Property
'---------------------------------------------------------------------------------------
' Procedure : MskFechaFinal_KeyPress
' DateTime  : 06/06/2008 11:30
' Author    : albert_silva
' Purpose   : R2051 VALIDA QUE LOS DATOS DIGITADOS SEAN CORRECTOS
'---------------------------------------------------------------------------------------
'
Private Sub MskFechaFinal_KeyPress(KeyAscii As Integer)
Call Cambiar_Enter(KeyAscii)
Call ValKeyFecha(KeyAscii, MskFechaFinal)
End Sub
'---------------------------------------------------------------------------------------
' Procedure : MskFechaFinal_LostFocus
' DateTime  : 06/06/2008 11:32
' Author    : albert_silva
' Purpose   : R2051 VALIDA QUE LA FECHA FINAL NO SEA MENOR QUE LA INICIAL
'---------------------------------------------------------------------------------------
'
Private Sub MskFechaFinal_LostFocus()
    Call ValFecha(MskFechaFinal, 1)
    Call ValiFeInMenorFeFin
End Sub
'---------------------------------------------------------------------------------------
' Procedure : MskFechainicial_KeyPress
' DateTime  : 06/06/2008 11:33
' Author    : albert_silva
' Purpose   : R2051 VALIDA QUE LOS DATOS DIGITADOS SEAN CORRECTOS
'---------------------------------------------------------------------------------------
'
Private Sub MskFechaInicial_KeyPress(KeyAscii As Integer)
Call Cambiar_Enter(KeyAscii)
Call ValKeyFecha(KeyAscii, MskFechaInicial)
End Sub
'---------------------------------------------------------------------------------------
' Procedure : MskFechainicial_LostFocus
' DateTime  : 06/06/2008 11:34
' Author    : albert_silva
' Purpose   : R2051 VALIDA QUE LA FECHA SEA CORRECTA
'---------------------------------------------------------------------------------------
'
Private Sub MskFechaInicial_LostFocus()
    Call ValFecha(MskFechaInicial, 1)
    Call ValiFeInMenorFeFin
End Sub
'---------------------------------------------------------------------------------------
' Procedure : ValiFeInMenorFeFin
' DateTime  : 17/06/2008 16:37
' Author    : albert_silva
' Purpose   : M3532 valida que la fecha inicial no sea mayor que la final
'---------------------------------------------------------------------------------------
'
Sub ValiFeInMenorFeFin()
    BlFecha = False
     If DateDiff("yyyy", MskFechaInicial, MskFechaFinal) < 0 Then Call Mensaje1("La fecha inicial no puede ser mayor a la fecha final", 3): MskFechaFinal = Format(FechaServer, "dd/mm/yyyy"): BlFecha = True: MskFechaInicial.SetFocus: Exit Sub
     If DateDiff("m", MskFechaInicial, MskFechaFinal) < 0 And DateDiff("yyyy", MskFechaInicial, MskFechaFinal) <= 0 Then Call Mensaje1("La fecha inicial no puede ser mayor a la fecha final", 3): MskFechaFinal = Format(FechaServer, "dd/mm/yyyy"): BlFecha = True: MskFechaInicial.SetFocus: Exit Sub
     If DateDiff("d", MskFechaInicial, MskFechaFinal) < 0 Then Call Mensaje1("La fecha inicial no puede ser mayor a la fecha final", 3): MskFechaFinal = Format(FechaServer, "dd/mm/yyyy"): BlFecha = True: MskFechaInicial.SetFocus
End Sub
