VERSION 5.00
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "MSFLXGRD.OCX"
Object = "{C932BA88-4374-101B-A56C-00AA003668DC}#1.1#0"; "MSMASK32.OCX"
Object = "{0BA686C6-F7D3-101A-993E-0000C0EF6F5E}#1.0#0"; "THREED32.OCX"
Begin VB.Form FrmAuditoria 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Control Transaccional y Auditoria"
   ClientHeight    =   5250
   ClientLeft      =   1380
   ClientTop       =   1500
   ClientWidth     =   8490
   FillColor       =   &H8000000F&
   Icon            =   "Frmaudit.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   5250
   ScaleWidth      =   8490
   Begin VB.Frame Frame2 
      Height          =   2775
      Left            =   7320
      TabIndex        =   15
      Top             =   1680
      Width           =   1095
      Begin Threed.SSCommand SCmd_Options 
         Height          =   735
         Index           =   0
         Left            =   120
         TabIndex        =   16
         Top             =   210
         Width           =   855
         _Version        =   65536
         _ExtentX        =   1508
         _ExtentY        =   1296
         _StockProps     =   78
         Caption         =   "&EXAMINAR"
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   6
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Picture         =   "Frmaudit.frx":058A
      End
      Begin Threed.SSCommand SCmd_Options 
         Height          =   735
         Index           =   1
         Left            =   120
         TabIndex        =   17
         Top             =   1080
         Width           =   855
         _Version        =   65536
         _ExtentX        =   1508
         _ExtentY        =   1296
         _StockProps     =   78
         Caption         =   "&LISTAR"
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   6
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Picture         =   "Frmaudit.frx":152C
      End
      Begin Threed.SSCommand SCmd_Options 
         Height          =   735
         Index           =   2
         Left            =   120
         TabIndex        =   18
         Top             =   1920
         Width           =   855
         _Version        =   65536
         _ExtentX        =   1508
         _ExtentY        =   1296
         _StockProps     =   78
         Caption         =   "&SALIR"
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   6
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Picture         =   "Frmaudit.frx":1BF6
      End
   End
   Begin Threed.SSFrame Fra_Transacciones_Auditoria 
      Height          =   1365
      Index           =   0
      Left            =   6480
      TabIndex        =   8
      Top             =   120
      Width           =   1875
      _Version        =   65536
      _ExtentX        =   3307
      _ExtentY        =   2408
      _StockProps     =   14
      Caption         =   "&Transacciones :"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Begin VB.CheckBox Operacion 
         Caption         =   "&Eliminaci�n"
         Height          =   270
         Index           =   2
         Left            =   540
         TabIndex        =   14
         Top             =   960
         Width           =   1095
      End
      Begin VB.CheckBox Operacion 
         Caption         =   "&Modificaci�n"
         Height          =   270
         Index           =   1
         Left            =   540
         TabIndex        =   12
         Top             =   600
         Width           =   1305
      End
      Begin VB.CheckBox Operacion 
         Caption         =   "&Adici�n"
         Height          =   270
         Index           =   0
         Left            =   540
         TabIndex        =   10
         Top             =   240
         Width           =   915
      End
      Begin VB.Label LblAudi 
         AutoSize        =   -1  'True
         Caption         =   "3."
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   5
         Left            =   300
         TabIndex        =   13
         Top             =   990
         Width           =   180
      End
      Begin VB.Label LblAudi 
         AutoSize        =   -1  'True
         Caption         =   "2."
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   4
         Left            =   300
         TabIndex        =   11
         Top             =   630
         Width           =   180
      End
      Begin VB.Label LblAudi 
         AutoSize        =   -1  'True
         Caption         =   "1."
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   3
         Left            =   270
         TabIndex        =   9
         Top             =   270
         Width           =   180
      End
   End
   Begin Threed.SSFrame Fra_Resultado_Auditoria 
      Height          =   3435
      Left            =   30
      TabIndex        =   19
      Top             =   1530
      Width           =   7275
      _Version        =   65536
      _ExtentX        =   12832
      _ExtentY        =   6059
      _StockProps     =   14
      Caption         =   "Re&sultado de la Auditor�a:"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Begin MSFlexGridLib.MSFlexGrid Grdf_Listado 
         Height          =   3135
         Left            =   120
         TabIndex        =   20
         Top             =   240
         Width           =   7095
         _ExtentX        =   12515
         _ExtentY        =   5530
         _Version        =   393216
         Cols            =   4
         FocusRect       =   2
      End
   End
   Begin Threed.SSFrame Fra_Datos_Auditoria 
      Height          =   1305
      Left            =   120
      TabIndex        =   0
      Top             =   120
      Width           =   6225
      _Version        =   65536
      _ExtentX        =   10980
      _ExtentY        =   2302
      _StockProps     =   14
      Caption         =   "&Datos de Auditor�a:"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Begin VB.ComboBox CboTabla 
         Height          =   315
         Left            =   1680
         Style           =   2  'Dropdown List
         TabIndex        =   7
         Top             =   900
         Width           =   4455
      End
      Begin VB.ComboBox CboUsuario 
         Height          =   315
         Left            =   1680
         Style           =   2  'Dropdown List
         TabIndex        =   5
         Top             =   540
         Width           =   4455
      End
      Begin MSMask.MaskEdBox Msk_Fecha_Final 
         Height          =   255
         Left            =   3240
         TabIndex        =   3
         Top             =   180
         Width           =   975
         _ExtentX        =   1720
         _ExtentY        =   450
         _Version        =   393216
         BackColor       =   16777215
         MaxLength       =   10
         Format          =   "dd/mm/yyyy"
         Mask            =   "##/##/####"
         PromptChar      =   "#"
      End
      Begin MSMask.MaskEdBox Msk_Fecha_Inicial 
         Height          =   255
         Left            =   1680
         TabIndex        =   2
         Top             =   180
         Width           =   975
         _ExtentX        =   1720
         _ExtentY        =   450
         _Version        =   393216
         BackColor       =   16777215
         MaxLength       =   10
         Format          =   "dd/mm/yyyy"
         Mask            =   "##/##/####"
         PromptChar      =   "#"
      End
      Begin VB.Label LblAudi 
         AutoSize        =   -1  'True
         Caption         =   "Lista de &Tablas :"
         Height          =   195
         Index           =   2
         Left            =   150
         TabIndex        =   6
         Top             =   960
         Width           =   1170
      End
      Begin VB.Label LblAudi 
         AutoSize        =   -1  'True
         Caption         =   "&Rango de Fechas :"
         Height          =   195
         Index           =   0
         Left            =   150
         TabIndex        =   1
         Top             =   300
         Width           =   1365
      End
      Begin VB.Label LblAudi 
         AutoSize        =   -1  'True
         Caption         =   "&Nombre del Usuario :"
         Height          =   195
         Index           =   1
         Left            =   150
         TabIndex        =   4
         Top             =   630
         Width           =   1485
      End
   End
End
Attribute VB_Name = "FrmAuditoria"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
'Dim TEXTO As Integer AASV M2663 NO SE USA
Dim Cod_Usuario() As Variant
Public OpcCod  As String   'Opci�n de seguridad 'NYCM M2238
Dim Nombre_tabla() As Variant 'NYCM M2238

Private Sub CboTabla_GotFocus()
    SendKeys "{F4}" 'AASV M2663
End Sub

Private Sub CboTabla_KeyPress(KeyAscii As Integer)
    Call Cambiar_Enter(KeyAscii)
End Sub

Private Sub CboUsuario_GotFocus()
    SendKeys "{F4}" 'AASV M2663
End Sub

Private Sub CboUsuario_KeyPress(KeyAscii As Integer)
    Call Cambiar_Enter(KeyAscii)
End Sub

'AASV M2663 NO SE USA
'Private Sub CmdBackup_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
'QueEsEsto "Backup de Auditor�a"
'End Sub
Private Sub Form_Load()
   Call CenterForm(MDI_Inventarios, Me) 'AASV M2663
   Dim S As String
   Dim Condicion As String
   Msk_Fecha_Inicial = Hoy
   Msk_Fecha_Final = Hoy
   Call Leer_Permiso(Me.Name, SCmd_Options(0), "G")
   Call Leer_Permiso(Me.Name, SCmd_Options(1), "I")
   Condicion = "NU_AUTO_USUA = NU_AUTO_USUA_CONE ORDER BY TX_DESC_USUA"
   Desde = "CONEXION,USUARIO "
   Campos = "DISTINCT (TX_DESC_USUA) "
   Result = loadctrl(Desde, Campos, "TX_IDEN_USUA", CboUsuario, Cod_Usuario(), Condicion)
   
   Result = loadctrl("AUDITOR_IN", "DISTINCT AUDTABLA", "AUDTABLA", CboTabla, Nombre_tabla(), NUL$)
'   Dim s As String
'   Call MouseClock
'   MsgLin "Abriendo la auditoria del sistema"
'   Call CenterForm(MDI_Nomina, FrmAuditoria)
'   Result = LoadSysObjs(CboTabla, Nombre_Tabla())
'   CboTabla.AddItem "(Todas las Tablas)", 0
'   If (Result = FAIL) Then
'      Call Mensaje1("Error cargando la tabla [" & SSysObjs & "]", 3)
'      Exit Sub
'   End If
'   Result = LoadCtrl("USUARIOS_N", "UserNomb", "UserID", CboUsuario, Cod_Usuario(), NUL$)
'   If (Result = FAIL) Then
'      Call Mensaje1("Error cargando la tablas [" & "USUARIOS_N" & "]", 3)
'      Exit Sub
'   End If
'   Msk_Fecha_Inicial = Hoy
'   Msk_Fecha_Final = Hoy
'JAGS T7783
 'S = "Fecha y Hora,1400,      Tabla,1200,Trans,500, Descripci�n de la operaci�n,4100"
   'S = "Fecha y Hora,1400,      Tabla,1200,Trans,500, Descripci�n de la operaci�n,4100, Versi�n del Ejecutable,2000, Fecha del Ejecutable,2000"
   S = "Fecha y Hora,1400,      Tabla,1200,Trans,500, Descripci�n de la operaci�n,4100, Versi�n de ejecutable,2000, Fecha de ejecutable,2000" 'JAGS T8479
   'JAGS T7783
   Call GrdFDef(Grdf_Listado, 0, 0, S)
   Call MouseNorm
  Msglin NUL$
End Sub

Private Sub Grdf_Listado_DblClick()
    If Grdf_Listado.Col = 3 Then Call Mensaje1(Grdf_Listado.TextMatrix(Grdf_Listado.Row, 3), 3) 'CARV M5147
End Sub

Private Sub Msk_Fecha_Final_KeyPress(KeyAscii As Integer)
    Call Cambiar_Enter(KeyAscii)
    Call ValKeyFecha(KeyAscii, Msk_Fecha_Final)
End Sub

Private Sub Msk_Fecha_Final_LostFocus()
 'NYCM M2238
 If ValFecha(Msk_Fecha_Final, 0) <> FAIL Then
       If DateDiff("d", Msk_Fecha_Inicial, Msk_Fecha_Final) < 0 Then
            Call Mensaje1("La fecha final es menor que la fecha inicial", 3)
            Msk_Fecha_Final.Text = Hoy
            Msk_Fecha_Final.SetFocus
       End If
    End If
'Call ValFecha(Msk_Fecha_Final, 1)
End Sub

Private Sub Msk_Fecha_inicial_KeyPress(KeyAscii As Integer)
    Call Cambiar_Enter(KeyAscii)
    Call ValKeyFecha(KeyAscii, Msk_Fecha_Inicial)
End Sub

Private Sub Msk_Fecha_inicial_LostFocus()
'NYCM M2238
    If ValFecha(Msk_Fecha_Inicial, 0) <> FAIL Then
       If DateDiff("d", Msk_Fecha_Final, Msk_Fecha_Inicial) > 0 Then
            Call Mensaje1("La fecha inicial es mayor que la fecha final", 3)
            Msk_Fecha_Inicial.Text = Hoy
            Msk_Fecha_Inicial.SetFocus
       End If
    End If
'Call ValFecha(Msk_Fecha_Inicial, 1)
End Sub

Private Sub Operacion_KeyPress(Index As Integer, KeyAscii As Integer)
    Call Cambiar_Enter(KeyAscii)
End Sub

Private Sub SCmd_Options_Click(Index As Integer)
Dim Formula As String
Dim Band As Byte
Dim SQL As String 'JAGS T8248

Select Case Index
        Case 0: Buscar_Auditorias 'NYCM M2238
                'Buscar_Auditoria
        Case 1:
                Call MouseClock
                'If CboUsuario.ListIndex < 0 Then Call Mensaje1("Seleccione un usuario", 3): Call MouseNorm: Exit Sub
                   'Formula = "{AUDITOR_IN.AUDFECH}>=DATE(" & Format(Me.Msk_Fecha_Inicial, "yyyy,mm,dd") & ")"
                'If CboTabla.ListIndex > 0 Then Formula = Formula & " and {AUDITOR_IN.AUDTABLA}= '" & CboTabla & Comi
                'If Operacion(0).Value = 1 Or Operacion(1).Value = 1 Or Operacion(2).Value = 1 Then
                    'Formula = Formula & " AND ("
                    'Formula = Formula & IIf(Operacion(0).Value = 1, " {AUDITOR_IN.Audtrans}=" & Comi & TranIns & Comi, NUL$)
                    'Band = IIf(Operacion(0).Value = 1, 1, 0)
                    'Formula = Formula & IIf(Band = 1 And Operacion(1).Value = 1, " or ", NUL$)
                    
                    'Formula = Formula & IIf(Operacion(1).Value = 1, " {AUDITOR_IN.Audtrans}=" & Comi & TranUpd & Comi, NUL$)
                    'Band = IIf(Operacion(1).Value Or Operacion(0).Value = 1, 1, 0)
                    'Formula = Formula & IIf(Band = 1 And Operacion(2).Value = 1, " or ", NUL$)
                    
                    'Formula = Formula & IIf(Operacion(2).Value = 1, " {AUDITOR_IN.Audtrans}=" & Comi & TranDel & Comi, NUL$)
                    
                    'Formula = Formula & ")"
                    
                    'JAGS T8248 INICIO
                    If Not ExisteTABLA("AUDITORIA_TEMP") Then
                        SQL = "Select AudFech,AudUser,AudTabla,AudTrans,AudDesc,AudVerExe,AudFecExe,SUBSTRING( CONVERT(char(38),  AudFecExe,121), 12,8) as hora_exe"
                        SQL = SQL & " INTO AUDITORIA_TEMP "
                        SQL = SQL & "FROM AUDITOR_IN"
                        
                        Criterio = NUL$
                           If CboUsuario.ListIndex = -1 Then
                                Call Mensaje1("Selecciones el usuario para realizar la auditor�a", 3)
                                Call MouseNorm: Msglin NUL$: Exit Sub
                           End If
                           If (Not IsDate(Msk_Fecha_Inicial) Or Not IsDate(Msk_Fecha_Final)) Then
                             Call Mensaje1("Verifique el rango de fechas para la auditor�a", 3)
                             Call MouseNorm: Msglin NUL$: Exit Sub
                           End If
                            'If FFechaIns(Msk_Fecha_Inicial) > FFechaIns(Msk_Fecha_Final) Then
                            If DateDiff("d", CDate(Msk_Fecha_Inicial), CDate(Msk_Fecha_Final)) < 0 Then 'AASV M2663
                                Msk_Fecha_Inicial = Hoy
                                Msk_Fecha_Final = Hoy
                                Exit Sub
                           End If
                           
                           If (IsDate(Msk_Fecha_Inicial)) Then
                              'Condicion = "Audfech>=" & FFechaIns(Msk_Fecha_Inicial.Text & " 00:00:01")
                              Condicion = "Audfech>=" & FHFECHA(Msk_Fecha_Inicial.Text & " 00:00") 'AASV M2663
                              Criterio = "(" & Condicion & " And "
                           End If
                           If (IsDate(Msk_Fecha_Final.Text)) Then
                              'Condicion = "AudFech<= " & FFechaIns(Msk_Fecha_Final.Text & " 23:59:59 ")
                              Condicion = "AudFech<= " & FHFECHA(Msk_Fecha_Final.Text & "  23:59") 'AASV M2663
                              Criterio = Criterio & Condicion
                           End If
                           If ((Operacion(0) + Operacion(1) + Operacion(2)) > 0) Then
                              Call ConcatCrit(SParA, 1)
                              Condicion = Criterio
                              Criterio = NUL$
                              If (Operacion(0) <> 0) Then Call ConcatCrit("Audtrans=" & Comi & TranIns & Comi, 0)
                              If (Operacion(1) <> 0) Then Call ConcatCrit("Audtrans=" & Comi & TranUpd & Comi, 0)
                              If (Operacion(2) <> 0) Then Call ConcatCrit("Audtrans=" & Comi & TranDel & Comi, 0)
                              
                              Criterio = Condicion & Criterio & SParC
                           Else
                                Call Mensaje1("Debe especificar una operaci�n", 3)
                                Call MouseNorm: Msglin NUL$: Exit Sub
                           End If
                           If (CboTabla.ListIndex > -1) Then
                              Condicion = "AudTabla=" & Comi & Nombre_tabla(CboTabla.ListIndex) & Comi
                              Call ConcatCrit(Condicion, 1)
                              'Criterio = Criterio & " and " & Condicion & SParC
                              Criterio = Criterio & SParC ' AASV M2663
                           Else
                              Criterio = Criterio & SParC
                           End If
                        
                           Campos = "AudFech"
                           If (Len(Criterio) > 0) Then
                              Criterio = SParA & Criterio & " and AudUser =" & Comi & Cod_Usuario(CboUsuario.ListIndex) & Comi & SParC
                              Condicion = Criterio
                           Else
                              Condicion = "1=1"
                           End If
                           
                           Condicion = Condicion & " Order By " & Campos & ",AudTabla"
                       SQL = SQL & " Where " & Condicion
                        

                        Result = ExecSQLCommand(SQL)
                    'End If
                    'JAGS T8248 FIN
                Else
                    Call Mensaje1("Seleccione por lo menos un tipo de transacci�n)", 3)
                    Call MouseNorm
                    Exit Sub
                End If
                Debug.Print Formula
                'ReDim Arr(0)
                'Arr(0) = "paciente.mdb"
                ReDim Arr(1)
                Result = LoadData("ENTIDAD", "CD_NIT_ENTI, NO_NOMB_ENTI", NUL$, Arr)
                If Result <> FAIL Then
                    Entidad = Arr(1)
                    Nit = Arr(0)
                End If
                'Call Listar1("infAuditor.rpt", Formula, "{AUDITOR_IN.AUDUSER}='" & Cod_Usuario(CboUsuario.ListIndex) & Comi, crptToWindow, "Informe de auditoria", MDI_Inventarios)
                Call Listar1("infAuditor.rpt", "", "{AUDITORIA_TEMP.AUDUSER}='" & Cod_Usuario(CboUsuario.ListIndex) & Comi, crptToWindow, "Informe de auditoria", MDI_Inventarios)
                If ExisteTABLA("AUDITORIA_TEMP") Then Result = ExecSQLCommand("DROP TABLE AUDITORIA_TEMP") 'JAGS T8248
                Call MouseNorm
        Case 2: Unload Me
End Select

End Sub

'---------------------------------------------------------------------------------------
' Procedure : Buscar_Auditorias
' DateTime  : 11/06/2008 10:18
' Author    : albert_silva
' Purpose   : R Auditoria, Se trae esta funcion de Nomina
'---------------------------------------------------------------------------------------
'
Sub Buscar_Auditorias() 'NYCM M2238
   'Dim S As String'AASV M2663 NO SE USA
   'Dim S1 As String'AASV M2663 NO SE USA
   'Dim I As Integer'AASV M2663 NO SE USA
   'Dim j As Integer'AASV M2663 NO SE USA
   'Dim N As Integer'AASV M2663 NO SE USA
   'Dim l As Integer'AASV M2663 NO SE USA
   'Dim d As Integer'AASV M2663 NO SE USA
   'Dim Tablas As String'AASV M2663 NO SE USA
   
   DoEvents
   
   Call MouseClock
   Msglin "Buscando registros de auditoria"

   Criterio = NUL$
   If CboUsuario.ListIndex = -1 Then
        Call Mensaje1("Selecciones el usuario para realizar la auditor�a", 3)
        Call MouseNorm: Msglin NUL$: Exit Sub
   End If
   If (Not IsDate(Msk_Fecha_Inicial) Or Not IsDate(Msk_Fecha_Final)) Then
     Call Mensaje1("Verifique el rango de fechas para la auditor�a", 3)
     Call MouseNorm: Msglin NUL$: Exit Sub
   End If
    'If FFechaIns(Msk_Fecha_Inicial) > FFechaIns(Msk_Fecha_Final) Then
    If DateDiff("d", CDate(Msk_Fecha_Inicial), CDate(Msk_Fecha_Final)) < 0 Then 'AASV M2663
        Msk_Fecha_Inicial = Hoy
        Msk_Fecha_Final = Hoy
        Exit Sub
   End If
   
   If (IsDate(Msk_Fecha_Inicial)) Then
      'Condicion = "Audfech>=" & FFechaIns(Msk_Fecha_Inicial.Text & " 00:00:01")
      Condicion = "Audfech>=" & FHFECHA(Msk_Fecha_Inicial.Text & " 00:00") 'AASV M2663
      Criterio = "(" & Condicion & " And "
   End If
   If (IsDate(Msk_Fecha_Final.Text)) Then
      'Condicion = "AudFech<= " & FFechaIns(Msk_Fecha_Final.Text & " 23:59:59 ")
      Condicion = "AudFech<= " & FHFECHA(Msk_Fecha_Final.Text & "  23:59") 'AASV M2663
      Criterio = Criterio & Condicion
   End If
   If ((Operacion(0) + Operacion(1) + Operacion(2)) > 0) Then
      Call ConcatCrit(SParA, 1)
      Condicion = Criterio
      Criterio = NUL$
      If (Operacion(0) <> 0) Then Call ConcatCrit("Audtrans=" & Comi & TranIns & Comi, 0)
      If (Operacion(1) <> 0) Then Call ConcatCrit("Audtrans=" & Comi & TranUpd & Comi, 0)
      If (Operacion(2) <> 0) Then Call ConcatCrit("Audtrans=" & Comi & TranDel & Comi, 0)
      
      Criterio = Condicion & Criterio & SParC
   Else
        Call Mensaje1("Debe especificar una operaci�n", 3)
        Call MouseNorm: Msglin NUL$: Exit Sub
   End If
   If (CboTabla.ListIndex > -1) Then
      Condicion = "AudTabla=" & Comi & Nombre_tabla(CboTabla.ListIndex) & Comi
      Call ConcatCrit(Condicion, 1)
      'Criterio = Criterio & " and " & Condicion & SParC
      Criterio = Criterio & SParC ' AASV M2663
   Else
      Criterio = Criterio & SParC
   End If

   Campos = "AudFech"
   If (Len(Criterio) > 0) Then
      Criterio = SParA & Criterio & " and AudUser =" & Comi & Cod_Usuario(CboUsuario.ListIndex) & Comi & SParC
      Condicion = Criterio
   Else
      Condicion = "1=1"
   End If
   
   Condicion = Condicion & " Order By " & Campos & ",AudTabla"
   Result = LoadfGrid(Grdf_Listado, "AUDITOR_IN", Campos & ",AudTabla,AudTrans,AudDesc,AudVerExe,AudFecExe", Condicion)
   If (Result = FAIL) Then Call Mensaje1("Ha ocurrido un error buscando los registros de Auditoria", 3)
   If (Not Encontro) Then
       Call Mensaje1("No se encontraron registros de Auditor�a con ese criterio", 3)
   End If
   Call MouseNorm: Msglin NUL$
   
End Sub

