VERSION 5.00
Object = "{00025600-0000-0000-C000-000000000046}#5.2#0"; "CRYSTL32.OCX"
Object = "{0BA686C6-F7D3-101A-993E-0000C0EF6F5E}#1.0#0"; "THREED32.OCX"
Begin VB.Form FrmConso 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Consolidado de Existencias"
   ClientHeight    =   3090
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   3660
   Icon            =   "FrmConso.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   ScaleHeight     =   3090
   ScaleWidth      =   3660
   Begin VB.CheckBox ChkArti 
      Caption         =   "&Incluir art�culos sin existencia en Dependencia"
      Height          =   375
      Left            =   120
      TabIndex        =   11
      Top             =   1440
      Width           =   3375
   End
   Begin VB.Frame FraArticulos 
      Caption         =   "Rango de Art�culos"
      Height          =   1335
      Left            =   120
      TabIndex        =   0
      Top             =   0
      Width           =   3375
      Begin VB.TextBox TxtRango 
         Height          =   285
         Index           =   0
         Left            =   840
         MaxLength       =   20
         TabIndex        =   1
         Text            =   "0"
         Top             =   360
         Width           =   1815
      End
      Begin VB.TextBox TxtRango 
         Height          =   285
         Index           =   1
         Left            =   840
         MaxLength       =   20
         TabIndex        =   3
         Text            =   "ZZZZZZZZZZZZZZZZ"
         Top             =   840
         Width           =   1815
      End
      Begin Threed.SSCommand CmdSelec 
         Height          =   375
         Index           =   1
         Left            =   2640
         TabIndex        =   4
         ToolTipText     =   "Selecci�n de Art�culos"
         Top             =   840
         Width           =   615
         _Version        =   65536
         _ExtentX        =   1085
         _ExtentY        =   661
         _StockProps     =   78
         BevelWidth      =   0
         RoundedCorners  =   0   'False
         Outline         =   0   'False
         Picture         =   "FrmConso.frx":058A
      End
      Begin Threed.SSCommand CmdSelec 
         Height          =   375
         Index           =   0
         Left            =   2640
         TabIndex        =   2
         ToolTipText     =   "Selecci�n de Art�culos"
         Top             =   360
         Width           =   615
         _Version        =   65536
         _ExtentX        =   1085
         _ExtentY        =   661
         _StockProps     =   78
         BevelWidth      =   0
         RoundedCorners  =   0   'False
         Outline         =   0   'False
         Picture         =   "FrmConso.frx":0F3C
      End
      Begin VB.Label LblInicio 
         AutoSize        =   -1  'True
         Caption         =   "Inicial : "
         Height          =   195
         Left            =   240
         TabIndex        =   10
         Top             =   360
         Width           =   540
      End
      Begin VB.Label LblHasta 
         AutoSize        =   -1  'True
         Caption         =   "Final : "
         Height          =   195
         Left            =   240
         TabIndex        =   9
         Top             =   840
         Width           =   465
      End
   End
   Begin Threed.SSCommand SCmd_Options 
      Height          =   735
      Index           =   1
      Left            =   1440
      TabIndex        =   6
      Top             =   2280
      Width           =   855
      _Version        =   65536
      _ExtentX        =   1508
      _ExtentY        =   1296
      _StockProps     =   78
      Caption         =   "&IMPRIMIR"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   6
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Picture         =   "FrmConso.frx":18EE
   End
   Begin Threed.SSCommand SCmd_Options 
      Height          =   735
      Index           =   2
      Left            =   2520
      TabIndex        =   7
      Top             =   2280
      Width           =   855
      _Version        =   65536
      _ExtentX        =   1508
      _ExtentY        =   1296
      _StockProps     =   78
      Caption         =   "&SALIR"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   6
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Picture         =   "FrmConso.frx":1FB8
   End
   Begin Threed.SSCommand SCmd_Options 
      Height          =   735
      Index           =   0
      Left            =   360
      TabIndex        =   5
      Top             =   2280
      Width           =   855
      _Version        =   65536
      _ExtentX        =   1508
      _ExtentY        =   1296
      _StockProps     =   78
      Caption         =   "&PANTALLA"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   6
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Picture         =   "FrmConso.frx":2682
   End
   Begin Crystal.CrystalReport Crys_Listar 
      Left            =   2640
      Top             =   1320
      _ExtentX        =   741
      _ExtentY        =   741
      _Version        =   348160
      WindowLeft      =   20
      WindowWidth     =   750
      WindowHeight    =   450
      WindowTitle     =   "Consolidado de Existencias"
      WindowControlBox=   -1  'True
      WindowMaxButton =   -1  'True
      WindowMinButton =   -1  'True
      WindowControls  =   -1  'True
      PrintFileLinesPerPage=   60
      WindowShowCloseBtn=   -1  'True
      WindowShowSearchBtn=   -1  'True
      WindowShowPrintSetupBtn=   -1  'True
      WindowShowRefreshBtn=   -1  'True
   End
   Begin Threed.SSCommand SCmd_Options 
      Height          =   375
      Index           =   3
      Left            =   0
      TabIndex        =   12
      Top             =   0
      Visible         =   0   'False
      Width           =   975
      _Version        =   65536
      _ExtentX        =   1720
      _ExtentY        =   661
      _StockProps     =   78
   End
   Begin Threed.SSCommand SCmd_Options 
      Height          =   375
      Index           =   4
      Left            =   1080
      TabIndex        =   13
      Top             =   0
      Visible         =   0   'False
      Width           =   975
      _Version        =   65536
      _ExtentX        =   1720
      _ExtentY        =   661
      _StockProps     =   78
   End
   Begin VB.Label LblReg 
      Height          =   255
      Left            =   120
      TabIndex        =   14
      Top             =   1920
      Width           =   3375
   End
   Begin VB.Label tipo 
      Height          =   135
      Left            =   600
      TabIndex        =   8
      Top             =   3360
      Visible         =   0   'False
      Width           =   975
   End
End
Attribute VB_Name = "FrmConso"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Dim Cantidad As Double
Dim CantiK As Double
Dim Costo As Double
Dim CostoK As Double
Dim Rstdatos As ADODB.Recordset
Dim Primera As Boolean
Public OpcCod        As String   'Opci�n de seguridad

Private Sub Form_Load()
    Dim ElClnt As New ElTercero
    ElClnt.IniXNit
 Call CenterForm(MDI_Inventarios, Me)
' Call Leer_Permisos("03001", SCmd_Options(3), SCmd_Options(3), SCmd_Options(0))
 Crys_Listar.Formulas(0) = "ENTIDAD='" & ElClnt.Nombre & Comi
 Crys_Listar.Formulas(2) = "NIT= 'NIT. " & ElClnt.Nit & Comi
 Crys_Listar.Formulas(1) = "HORA= 'Hora: " & Format(Now, "hh:mm") & Comi
 Crys_Listar.Formulas(3) = "USUARIO = 'Usuario:" & UserId & Comi
 SCmd_Options(1).Enabled = SCmd_Options(0).Enabled
' Crys_Listar.Connect = conCrys
 Primera = True
End Sub
Private Sub TxtRango_Change(Index As Integer)
    Primera = True
End Sub
Private Sub TxtRango_GotFocus(Index As Integer)
    TxtRango(Index).SelStart = 0
    TxtRango(Index).SelLength = Len(TxtRango(Index).Text)
End Sub
Private Sub TxtRango_KeyPress(Index As Integer, KeyAscii As Integer)
    Call ValKeyAlfaNum(KeyAscii)
    If KeyAscii = 13 Then
     If Index = 0 Then
        TxtRango(1).SetFocus
     Else
        SCmd_Options(0).SetFocus
     End If
    End If
End Sub
Private Sub TxtRango_KeyDown(Index As Integer, KeyCode As Integer, Shift As Integer)
 Select Case Index
    Case 0
         If KeyCode = 40 Then
            TxtRango(1).SetFocus
         End If
    Case 1
         If KeyCode = 38 Then
            TxtRango(0).SetFocus
         End If
         If KeyCode = 40 Then
            SCmd_Options(0).SetFocus
         End If
 End Select
End Sub
Private Sub CmdSelec_Click(Index As Integer)
    Codigo = NUL$
    Codigo = Seleccion("ARTICULO", "NO_NOMB_ARTI", "CD_CODI_ARTI,NO_NOMB_ARTI", "ARTICULOS", NUL$)
    If Codigo <> NUL$ Then TxtRango(Index) = Codigo
    TxtRango(Index).SetFocus
End Sub
Private Sub SCmd_Options_Click(Index As Integer)
    Screen.MousePointer = 11
    Select Case Index
        '''True = pantalla
        '''False = impresora
        Case 0: Call Informe(True)
        Case 1: Call Informe(False)
        Case 2: Unload Me
    End Select
    Screen.MousePointer = 0
End Sub

Private Sub Informe(ByVal pantalla As Boolean)
  If TxtRango(0) > TxtRango(1) Or TxtRango(1) = "" Then
     Call Mensaje1("Revise rango de art�culos", 3)
     Exit Sub
  End If
  Crys_Listar.ReportFileName = DirTrab + "consov.RPT"
  Crys_Listar.SortFields(0) = "+{articulo.cd_codi_arti}"
  Crys_Listar.SelectionFormula = "{articulo.cd_codi_arti} in '" & TxtRango(0) & "' to '" & TxtRango(1) & Comi
  If ChkArti.Value = 0 Then
      Crys_Listar.SelectionFormula = Crys_Listar.SelectionFormula & _
                                     " and ({R_DEPE_ARTI.CT_EXIS_DEAR} > 0)"
                                     'OR {ARTICULO.CT_EXIS_ARTI} > 0) "
  End If
On Error GoTo control
  Debug.Print Crys_Listar.SelectionFormula
'Crys_Listar.Connect = conCrys
  If SCmd_Options(4).Enabled = False Then
     Call Mensaje1("Permiso Denegado", 3)
  Else
     Call Crea_Temporales ''Crea temporal para impresi�n
     If pantalla = True Then
        Crys_Listar.Destination = crptToWindow
     Else
        Crys_Listar.Destination = crptToPrinter
     End If
     Crys_Listar.Action = 1
  End If
Exit Sub
control:
Call Mensaje1(ERR.Number & ": " & ERR.Description & "  Nombre del Reporte : consov.RPT", 1)
End Sub
''Crea temporal para informaci�n del consolidado de existencias
Private Sub Crea_Temporales()
  ReDim Arr(2) 'Para manejar la tabla de saldos almac�n
  ReDim Arr2(3) 'Para manejar la tabla de saldos dependencias
  
  Valores = " CT_CANT_SALD=0, VL_COST_SALD= 0"
  Result = DoUpdate("SAL_ANT", Valores, NUL$)
  Result = DoUpdate("SAL_ANT_dep", Valores, NUL$)
  'SALDO ALMACEN
  Condicion = "CD_ARTI_KARD >= '" & TxtRango(0) & Comi
  Condicion = Condicion & " And CD_ARTI_KARD <= '" & TxtRango(1) & Comi
  Condicion = Condicion & " And ID_ESTA_KARD <> " & "2"
'  condicion = condicion & " And CD_ORDO_KARD <> " & "3"
  Condicion = Condicion & " And CD_ORDO_KARD <> " & "4"
  Condicion = Condicion & " And CD_ORDO_KARD <> " & "5"
  Condicion = Condicion & " And CD_ORDO_KARD <> " & "10"
  Condicion = Condicion & " ORDER BY CD_ARTI_KARD"
  LblReg = NUL$
  Set Rstdatos = New ADODB.Recordset
  Call SelectRST("KARDEX_ARTI", "DISTINCT (CD_ARTI_KARD)", Condicion, Rstdatos)
  If (Result <> False) Then
    If Not Rstdatos.EOF Then
      Rstdatos.MoveFirst
      Do While Not Rstdatos.EOF
         LblReg = "Leyendo Articulo: " & Rstdatos.Fields(0)
         DoEvents
         Cantidad = Saldo_Inicial(CStr(Rstdatos.Fields(0)), Hoy, Costo)
         CantiK = Saldo_Kardex(CStr(Rstdatos.Fields(0)), Hoy, CostoK)
         
         Cantidad = Cantidad + CantiK
         Costo = Costo + CostoK
         
         Condicion = "CD_ARTI_SALD =" & Comi & Rstdatos.Fields(0) & Comi
         Valores = " CT_CANT_SALD=" & Cantidad & Coma
         Valores = Valores & " VL_COST_SALD=" & Format(Costo, "############0.##00")
         Result = DoUpdate("SAL_ANT", Valores, Condicion)
         Rstdatos.MoveNext
         If Result = FAIL Then Exit Do
      Loop
    End If
  End If
  LblReg = NUL$
  Rstdatos.Close
  '''''''
  'SALDO DEPENDENCIAS
  Condicion = "CD_ARTI_KARD >= '" & TxtRango(0) & Comi
  Condicion = Condicion & "And CD_ARTI_KARD <= '" & TxtRango(1) & Comi
  'condicion = condicion & "And ID_ESTA_KARD <> " & "2"
  Condicion = Condicion & "And CD_ORDO_KARD <> " & "1"
  'condicion = condicion & "And CD_ORDO_KARD <> " & "7"
  Condicion = Condicion & " And CD_ORDO_KARD <>" & "9"
  Condicion = Condicion & " And CD_ORDO_KARD <>" & "8"
  Condicion = Condicion & " ORDER BY CD_DEPE_KARD, CD_ARTI_KARD"
  Set Rstdatos = New ADODB.Recordset
  Call SelectRST("KARDEX_ARTI", " DISTINCT CD_DEPE_KARD, CD_ARTI_KARD", Condicion, Rstdatos)
  If (Result <> False) Then
    If Not Rstdatos.EOF Then
       Rstdatos.MoveFirst
       Do While Not Rstdatos.EOF
         LblReg = "Leyendo Articulo: " & Rstdatos.Fields(1) & " Dependencia: " & Rstdatos.Fields(0)
         DoEvents
         
         Cantidad = Saldo_Inicial_Dep(CStr(Rstdatos.Fields(1)), CStr(Rstdatos.Fields(0)), Hoy, Costo)
         CantiK = Saldo_Kardex_Dep(CStr(Rstdatos.Fields(1)), CStr(Rstdatos.Fields(0)), Hoy, CostoK)
         
         Cantidad = Cantidad + CantiK
         Costo = Costo + CostoK
         
         If Cantidad = 0 Then Costo = 0
         
         Condicion = "CD_ARTI_SALD = '" & Rstdatos.Fields(1) & Comi
         Condicion = Condicion & "And CD_DEPE_SALD = '" & Rstdatos.Fields(0) & Comi
         
         Valores = " CT_CANT_SALD=" & Cantidad & Coma
         Valores = Valores & " VL_COST_SALD=" & Format(Costo, "############0.##00")
         ReDim AD(0)
         Result = LoadData("SAL_ANT_DEP", "CD_DEPE_SALD", Condicion, AD())
         If AD(0) = NUL$ Then
            Valores = "CD_DEPE_SALD=" & Comi & Rstdatos.Fields(0) & Comi & Coma & _
                      "CD_ARTI_SALD=" & Comi & Rstdatos.Fields(1) & Comi & Coma & _
                      "CT_CANT_SALD=" & Cantidad & Coma & _
                      "VL_COST_SALD=" & Format(Costo, "###0.##00")
            Result = DoInsertSQL("SAL_ANT_DEP", Valores)
         Else
            Result = DoUpdate("SAL_ANT_DEP", Valores, Condicion)
         End If
         Rstdatos.MoveNext
         If Result = FAIL Then Exit Do
       Loop
    End If
  End If
  Rstdatos.Close
  LblReg = NUL$
  '''''''
End Sub
