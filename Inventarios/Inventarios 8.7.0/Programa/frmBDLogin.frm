VERSION 5.00
Object = "{0BA686C6-F7D3-101A-993E-0000C0EF6F5E}#1.0#0"; "THREED32.OCX"
Begin VB.Form frmBDLogin 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Ingreso a la base de datos."
   ClientHeight    =   2340
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   4800
   Icon            =   "frmBDLogin.frx":0000
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   2340
   ScaleWidth      =   4800
   Begin VB.ComboBox cmbNick 
      Height          =   315
      Left            =   240
      TabIndex        =   0
      Tag             =   "Escriba el usuario y la contrase�a para el acceso a SQL, para Access ser� tenida en cuenta solamente la contrase�a."
      Top             =   360
      Width           =   2175
   End
   Begin VB.TextBox txtPassConf 
      Height          =   285
      IMEMode         =   3  'DISABLE
      Left            =   240
      MaxLength       =   40
      PasswordChar    =   "*"
      TabIndex        =   2
      Tag             =   "Confirme la contrase�a escrita."
      Top             =   1800
      Width           =   2175
   End
   Begin VB.TextBox txtPass 
      Height          =   285
      IMEMode         =   3  'DISABLE
      Left            =   240
      MaxLength       =   40
      PasswordChar    =   "*"
      TabIndex        =   1
      Tag             =   "Escriba la contrase�a de acceso a la base de datos (no se confirmar� si es correcta)."
      Top             =   1080
      Width           =   2175
   End
   Begin Threed.SSCommand cmdGuardar 
      Height          =   735
      Left            =   3240
      TabIndex        =   3
      Top             =   1440
      Width           =   735
      _Version        =   65536
      _ExtentX        =   1296
      _ExtentY        =   1296
      _StockProps     =   78
      Caption         =   "&GUARDAR"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   6
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Picture         =   "frmBDLogin.frx":058A
   End
   Begin Threed.SSCommand cmdSalir 
      Height          =   735
      HelpContextID   =   120
      Left            =   3960
      TabIndex        =   4
      ToolTipText     =   "Salir"
      Top             =   1440
      Width           =   735
      _Version        =   65536
      _ExtentX        =   1296
      _ExtentY        =   1296
      _StockProps     =   78
      Caption         =   "&SALIR"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   6
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Picture         =   "frmBDLogin.frx":0C54
   End
   Begin VB.Image imgKey 
      Height          =   480
      Left            =   2700
      Picture         =   "frmBDLogin.frx":131E
      Top             =   1560
      Width           =   480
   End
   Begin VB.Label lblInfo 
      Caption         =   "Cargando..."
      Height          =   2145
      Left            =   2640
      TabIndex        =   8
      Top             =   240
      Width           =   2055
      WordWrap        =   -1  'True
   End
   Begin VB.Label lblPassConf 
      AutoSize        =   -1  'True
      Caption         =   "Repita la contrase�a:"
      Height          =   195
      Left            =   240
      TabIndex        =   7
      Top             =   1560
      Width           =   1515
   End
   Begin VB.Label lblPass 
      AutoSize        =   -1  'True
      Caption         =   "Contrase�a:"
      Height          =   195
      Left            =   240
      TabIndex        =   6
      Top             =   840
      Width           =   855
   End
   Begin VB.Label lblNick 
      AutoSize        =   -1  'True
      Caption         =   "Usuario:"
      Height          =   195
      Left            =   240
      TabIndex        =   5
      Top             =   120
      Width           =   585
   End
End
Attribute VB_Name = "frmBDLogin"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Dim rstAMain As adodb.Recordset
Public OpcCod        As String   'Opci�n de seguridad
Sub sbListarSesiones()

    Dim cMsg As String

cMsg = lblInfo: lblInfo = "Espere...": DoEvents
    
cmbNick.Enabled = (MotorBD <> "ACCESS")
If MotorBD = "ACCESS" Then cmbNick = "admin": Exit Sub

Set rstAMain = New adodb.Recordset
rstAMain.ActiveConnection = BD(BDCurCon)
rstAMain.Open "sp_helplogins"

                If Not rstAMain.EOF Then
                    Do
                        cmbNick.AddItem rstAMain.Fields(0)
                        rstAMain.MoveNext
                    Loop Until rstAMain.EOF
                End If

rstAMain.Close


lblInfo = cMsg

End Sub

Private Sub cmbNick_GotFocus()
        lblInfo = cmbNick.Tag
End Sub


Private Sub cmdGuardar_Click()
 
        If cFile = NUL$ Then Exit Sub 'HRR CONEXION
        If fnTrim(txtPass) = "" Then Mensaje1 "La contrase�a no es v�lida", 2: txtPass.SetFocus: Exit Sub
        If txtPass <> txtPassConf Then Mensaje1 "La confirmaci�n de la contrase�a no es correcta.", 2: txtPassConf.SetFocus: Exit Sub
        If MotorBD <> "ACCESS" And fnTrim(Me.cmbNick) = "" Then Mensaje1 "Microsoft SQL requiere un nombre de inicio de sesi�n.", 2: Exit Sub
        
        If Not WarnMsg("Esto afectar� a todos los usuarios que se conecten al servidor actual." & vbCrLf & "�Desea continuar?") Then Exit Sub
        
        If Not fnEscribirClave(cFile, cmbNick, txtPass) Then
            Mensaje1 "Ocurri� un error al escribir al archivo:" & vbCrLf & cFile & vbCrLf & vbCrLf & "Verifique que dicha ubicaci�n exista, y que tiene permisos de escritura.", 2
        Else
            Mensaje1 "Se registraron los cambios.", 3
            CmdSalir_Click
        End If
        
End Sub

Private Sub CmdSalir_Click()
    Unload Me
End Sub


Private Sub Form_KeyPress(KeyAscii As Integer)
    Call Cambiar_Enter(KeyAscii)
End Sub

Private Sub Form_Load()
        
    CenterForm MDI_Inventarios, Me
    
    Me.Show
    DoEvents
    sbListarSesiones

End Sub


Private Sub txtPass_GotFocus()
        lblInfo = txtPass.Tag
End Sub


Private Sub txtPassConf_GotFocus()
        lblInfo = txtPassConf.Tag
End Sub


