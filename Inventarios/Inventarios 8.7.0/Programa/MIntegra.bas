Attribute VB_Name = "MIntegra"
 ''Variables para el manejo de las cuentas contables
''==========================================
'Matriz de Datos por Defecto
'INICIO LDCR 2da PARTE R9033/T9930
'Global McuenD(1 To 2000, 1 To 6) As String 'Datos por defecto
'Global Mcuen(1 To 2000, 1 To 6) As String 'Datos finales
Global McuenD(1 To 2000, 1 To 8) As String 'Datos por defecto
Global Mcuen(1 To 2000, 1 To 8) As String 'Datos finales
Global ArrTipoIVA() As Variant
'FIN LDCR 2da PARTE R9033/T9930
Global primera_entrada As Boolean
Global continuar As Boolean
Global boFact As Boolean '|.DR.| Indica si se ha especificado el n�mero de factura, R:979-1295-1632 (Esta variable global puede causar problemas si se cargan 2 ventanas de 'Entrada' simult�neamente)


'procedimiento que llama a la forma de cuentas
Sub muestra_cuenta(ByVal Cambiar As String, Formulario As VB.Form)
    FrmCuentas.Desde_Forma = Formulario
    FrmCuentas.LblCambiar = Cambiar
     FrmCuentas.Show 1
End Sub

'Busca la cuenta contable del concepto
Function Buscar_Valor_Concepto(ByVal concepto As Integer, ByVal Numero As Integer, ByVal campo As String) As String
ReDim arr(0)
   Buscar_Valor_Concepto = NUL$
   Result = LoadData("R_CUEN_MOVI", campo, "NU_MOVI_CUMO=" & concepto & " AND NU_CUEN_CUMO=" & Numero, arr())
   If (Result <> False) Then
     If Encontro Then
        Buscar_Valor_Concepto = arr(0)
     End If
   End If
End Function

'Busca el comprobante contable del concepto
Function Buscar_Comprobante_Concepto(ByVal concepto As Integer) As String
    ReDim arr(0)
    Buscar_Comprobante_Concepto = NUL$
    Result = LoadData("CONCEPTOT", "CD_COMP_CONC", "NU_MOVI_CONC=" & concepto, arr())
    If Result Then
        If Encontro Then Buscar_Comprobante_Concepto = arr(0)
    End If
End Function

'Busca las cuentas del comprobante del concepto     'REOL M1709
Function Buscar_Cuentas_Concepto(ByVal comprobante As String, Opcion As Integer) As String
    Select Case Opcion
      Case 1
        Campos = "CD_IVA_CONC"
      Case 2
        Campos = "CD_RIVA_CONC"
      Case 3
        Campos = "CD_ICA_CONC"
      Case 4
        Campos = "CD_RETE_CONC"
    End Select
    ReDim arr(0)
    Buscar_Cuentas_Concepto = NUL$
    Result = LoadData("CONCEPTOT", Campos, "CD_COMP_CONC=" & Comi & comprobante & Comi, arr())
    If Result Then
        If Encontro Then Buscar_Cuentas_Concepto = arr(0)
    End If
End Function

'Valida que la cuenta est� marcada para terceros,
'centro de costo o cheque
Function Valida_Marca_Cuenta(ByVal Cuenta As String, Marcas() As Variant) As Integer
   If Cuenta = NUL$ Then Valida_Marca_Cuenta = True: Exit Function
   Result = LoadData("CUENTAS", "ID_TERC_CUEN,ID_CECO_CUEN,ID_CHEQ_CUEN", "CD_CODI_CUEN=" & Comi & Cuenta & Comi, Marcas())
   Valida_Marca_Cuenta = Result
End Function

'Busca el centro de costo por defecto de la aplicaci�n
Function CentroC_Default() As String
    ReDim arr(0)
    CentroC_Default = NUL$
    Result = LoadData("TC_CONTROL", "ID_IDE2_CONT", NUL$, arr())
    If Result <> FAIL Then
      If Encontro Then
        CentroC_Default = arr(0)
      End If
    End If
End Function

'Verifica si existe tercero o centro de costo
Function Existe_Registro(ByVal tipo As String, ByVal Valor As String) As Boolean
ReDim arr(0)

   Existe_Registro = False
   Select Case tipo
      Case "TERCERO"
        Result = LoadData(tipo, "CD_CODI_TERC", "CD_CODI_TERC=" & Comi & Valor & Comi, arr())
      Case "CENTRO_COSTO"
        'Result = LoadData(tipo, "CD_CODI_CECO", "CD_CODI_CECO=" & Comi & valor & Comi, Arr()) 'APGR T1678
        Result = LoadData(tipo, "CD_CODI_CECO", "CD_CODI_CECO=" & Comi & Valor & Comi & " AND NU_ESTADO_CECO <>1", arr()) 'APGR T1678
   End Select
   If (Result <> False) Then
     If Encontro Then
        Existe_Registro = True
     End If
   End If
End Function

'Valida los datos que van a pasar a contabilidad
Function Valida_Datos_Conta(Optional Grilla As MSFlexGrid) As Boolean 'ronald se coloco el parametro opcional para poder recorrer la grilla.
    ReDim Cta(0)
    ReDim Marcas(2)
    Dim fin As Boolean
    Dim Linea As Long
    Dim vl_debito As Double
    Dim vl_credito As Double
    Dim I As Double
    vl_debito = 0
    vl_credito = 0
    fin = False
    Valida_Datos_Conta = True
    Linea = 1
    Do Until fin = True
       If (Mcuen(Linea, 1) = NUL$ And Mcuen(Linea, 2) = NUL$ And Mcuen(Linea, 3) = NUL$ And Mcuen(Linea, 4) = NUL$ And Mcuen(Linea, 5) = NUL$ And Mcuen(Linea, 6) = NUL$) Or Mcuen(Linea, 1) = "ULTIMO REGISTRO" Then
          fin = True
       Else
       
        'ARA 05012001 se agrego este if
        If Mcuen(Linea, 2) <> "0" And Trim(Mcuen(Linea, 2)) <> NUL$ Then
        If Mcuen(Linea, 1) = NUL$ Then
           Call Mensaje1("No puede ir cuenta en blanco", 3)
           Valida_Datos_Conta = False
           fin = True
         End If
         'Verifica cuentas
         If Valida_Nivel_Cuentas(Mcuen(Linea, 1)) = True Then
           Call Mensaje1("Revise Cuenta: " + Mcuen(Linea, 1), 3)
           Valida_Datos_Conta = False
           fin = True
         Else
            Condicion = "CD_CODI_CUEN ='" & Mcuen(Linea, 1) & "'"
            Result = LoadData("Cuentas", "CD_CODI_CUEN", Condicion, Cta())
            If Cta(0) = NUL$ Then
                Call Mensaje1("No existe la Cuentas: " + Mcuen(Linea, 1), 3)
                Valida_Datos_Conta = False
                fin = True
            End If
         End If
         'Verifica Valores
         
         'modificado ARA 05-01-2001 ********************
         'If fin = False Then
           'If Mcuen(linea, 2) = "0" Or Mcuen(linea, 2) = NUL$ Then
           '  Call Mensaje1("Revise Valores", 3)
           '  Valida_Datos_Conta = False
           '  fin = True
           'End If
         'End If
         'se puso en comentarios 7 lineas**fin modificaci�n
         
         'Verifica D�bitos y cr�ditos
         If fin = False Then
           If Mcuen(Linea, 3) <> "D" And Mcuen(Linea, 3) <> "C" Then
             Call Mensaje1("Revise D�bitos y Cr�ditos", 3)
             Valida_Datos_Conta = False
             fin = True
           Else
             If Mcuen(Linea, 3) = "D" Then
'                vl_debito = vl_debito + CDbl(Mcuen(Linea, 2))
                vl_debito = vl_debito + Aplicacion.Formatear_Valor(CDbl(Mcuen(Linea, 2)))   'REOL M1709
             Else
'                vl_credito = vl_credito + CDbl(Mcuen(Linea, 2))
                vl_credito = vl_credito + Aplicacion.Formatear_Valor(CDbl(Mcuen(Linea, 2))) 'REOL M1709
             End If
           End If
         End If
         
         'Verifica Terceros
         If fin = False Then
           'If Valida_Marca_Cuenta(Mcuen(Linea, 1), Marcas) = True Then
           If Valida_Marca_Cuenta(Mcuen(Linea, 1), Marcas) <> FAIL Then
              If Marcas(0) = "S" Then
                If Existe_Registro("TERCERO", Mcuen(Linea, 4)) = False Then
                  Call Mensaje1("Revise Terceros", 3)
                  Valida_Datos_Conta = False
                  fin = True
                End If
              End If
           Else
              'Mcuen(linea, 4) = NUL$
           End If
         End If
         'Verifica Centros de Costo
         If fin = False Then
           'If Valida_Marca_Cuenta(Mcuen(Linea, 1), Marcas) = True Then
           If Valida_Marca_Cuenta(Mcuen(Linea, 1), Marcas) <> FAIL Then
              If Marcas(1) = "S" Then
                If Existe_Registro("CENTRO_COSTO", Mcuen(Linea, 5)) = False Then
                  Call Mensaje1("Revise Centros de Costo", 3)
                  Valida_Datos_Conta = False
                  fin = True
                End If
              End If
           Else
              Mcuen(Linea, 5) = NUL$
           End If
         End If
         'Valida valor base
         If Mcuen(Linea, 6) = NUL$ Then
            Mcuen(Linea, 6) = "0"
         End If
        End If
        Linea = Linea + 1
       End If
    Loop
    If Valida_Datos_Conta = True Then
      If Val(vl_debito) <> Val(vl_credito) Then
         Msg = "Valores d�bito y cr�dito diferentes" & vbCrLf & "DB: " & Format(vl_debito, "###,###,###,##0.##00") & vbCrLf & "CR: " & Format(vl_credito, "###,###,###,##0.##00") & vbCrLf & "Dif: " & Format(vl_debito - vl_credito, "###,###,###,##0.##00")
         Call Mensaje1(Msg, 3)
         Valida_Datos_Conta = False
      End If
    End If
    
    'RONALD VALIDA QUE HAYA UN TERCERO
'    If Not (GRILLA Is Nothing) Then
'       Dim i As Integer
'       For i = 1 To GRILLA.Rows - 1
'         ReDim ARR7(0)
'         Condi = "CD_CODI_CUEN =" & Comi & Trim(GRILLA.TextMatrix(i, 0)) & Comi
'         Result = LoadData("CUENTAS", "ID_TERC_CUEN", Condi, ARR7)
'         If ARR7(0) = "S" And Trim(GRILLA.TextMatrix(i, 3)) = NUL$ Then
'            Result = MsgBox("Falta colocar el tercero en la fila: " & Val(i), vbOKOnly)
'            Valida_Datos_Conta = False
'         End If
'       Next
'    End If

   For I = 1 To UBound(Mcuen, 2)
      If Trim(Mcuen(I, 6)) = NUL$ Then Exit For
      If ValidaTercero(Trim(Mcuen(I, 1))) = True And Trim(Mcuen(I, 4)) = NUL$ Then
         Result = MsgBox("Falta colocar el tercero en la fila: " & Val(I), vbOKOnly)
         Valida_Datos_Conta = False
      End If
   Next

End Function

'Guarda el movimiento contable en Inventarios y en Contabilidad
'INICIO LDCR 2da PARTE R9033/T9930
'Sub Guardar_Movimiento_Contable(ByVal concepto As String, ByVal fecha As Variant, ByVal Comprobante As String, ByVal consecutivo As Long, ByVal descripcion As String, ByVal cheque As String)
'Sub Guardar_Movimiento_Contable(ByVal concepto As String, ByVal fecha As Variant, ByVal Comprobante As String, ByVal consecutivo As Long, ByVal descripcion As String, ByVal cheque As String, Optional Bodega As String, Optional Documento As Integer)'SKRV T24261/R22366 comentario
Sub Guardar_Movimiento_Contable(ByVal concepto As String, ByVal fecha As Variant, ByVal comprobante As String, ByVal consecutivo As Long, ByVal descripcion As String, ByVal cheque As String, Optional Bodega As String, Optional Documento As Integer, Optional StOrigen As String, Optional StConsec As String) 'SKRV T24261/R22366
   'Dim Linea(13) As Variant
   'Dim Linea(16) As Variant'SKRV T24261/R22366 comentario
   Dim Linea(18) As Variant 'SKRV T24261/R22366
   'FIN LDCR 2da PARTE R9033/T9930
   Dim filas As Integer
   'JLPB T29905-R28918 INICIO
   Dim BoTransInt As Boolean 'Variable utilizada para validar si ya se abrio la transacci�n interna del movimiento NIIF
   Dim BoGenMNIIF As Boolean 'Variable para validar si se va a generar el movimiento niif
   BoTransInt = False
   BoGenMNIIF = False
   'JLPB T29905-R28918 FIN
   'PJCA M1881
   If fnValidarCierreAnual Then Result = FAIL: Exit Sub
   'PJCA M1881
   'HRR M2745
   'Marca el comprobante, como comprobante de transferencia
   Valores = "ID_TRAN_COMP='S'"
   Condicion = "CD_CODI_COMP=" & Comi & comprobante & Comi
   Result = DoUpdate("TC_COMPROBANTE", Valores, Condicion)
   'HRR M2745
   'If Documento = entrada Then Call AgruparValIVA(bodega) 'LDCR 2da PARTE R9033/T9930
   filas = 1
   Do Until Mcuen(filas, 1) = NUL$ Or UCase(Mcuen(filas, 1)) = "ULTIMO REGISTRO"
      Linea(0) = Mcuen(filas, 1)      'cuenta
      Linea(1) = Mcuen(filas, 4)      'tercero
      Linea(2) = fecha                'fecha
      Linea(3) = comprobante          'comprobante
      Linea(4) = consecutivo          'consecutivo
      Linea(5) = filas                'imputacion
      Linea(6) = descripcion          'concepto
      Linea(7) = Mcuen(filas, 2)      'valor
      Linea(8) = Mcuen(filas, 3)      'naturaleza
      Linea(9) = Mcuen(filas, 5)      'centro de costo
      Linea(10) = cheque              'cheque
      Linea(11) = Mcuen(filas, 6)     'valor base
      Linea(12) = Format(fecha, "mm") 'mes
      Linea(13) = "V"                 'Indica el modulo que graba el movimiento
      'INICIO LDCR 2da PARTE R9033/T9930
      If Documento = Entrada Then
         If Mcuen(filas, 8) <> NUL$ Or UCase(Mcuen(filas, 8)) = "NO GENERA IVA" Then
            Linea(14) = IIf(UCase(Mcuen(filas, 8)) <> "DEDUCIBLE", 1, 0) 'TIPO IVA
            'Linea(15) = ArrTipoIVA(0, filas - 1) 'VALOR IVA
            'Linea(16) = ArrTipoIVA(1, filas - 1) 'VALOR BASE IVA
            Linea(15) = Mcuen(filas, 7) 'VALOR IVA
            Linea(16) = Mcuen(filas, 6) 'VALOR BASE IVA
         Else
            Linea(14) = 2
            Linea(15) = 0
            Linea(16) = 0
         End If
      Else
         Linea(14) = 2
         Linea(15) = 0
         Linea(16) = 0
      End If
      'SKRV T24261/R22366 inicio
      Linea(17) = StOrigen
      Linea(18) = StConsec
      'SKRV T24261/R22366 fin
      'FIN LDCR 2da PARTE R9033/T9930
      'Afecta la contabilidad
      If BoGenMNIIF = False Then 'JLPB T29905-R28918
         If Crear_Movimiento_Contable(Linea) = FAIL Then Exit Do
         'JLPB T31416 INICIO Se deja en comentario la siguiente linea
         'If Result = 2 Then Result = 0: Exit Do 'JLPB T29905-R28918
         If Result = 2 Then
            Call MsgBox("El proceso se va a cancelar porque la cuenta " & Mcuen(filas, 1) & " no se encuentra parametrizada como NIIF/PUC.", vbCritical, App.Title)
            Result = 0
            Exit Do
         End If
         'JLPB T31416 FIN
         'If Documento <> Traslado And Documento <> Despacho And Documento <> DevolucionDespacho Then If Crear_Movimiento_Contable_NIIF(Linea) = FAIL Then Exit Do 'JLPB T25784-R25328 'JLPB T26186 SE DEJA EN COMENTARIO
         'If Crear_Movimiento_Contable_NIIF(Linea) = FAIL Then Exit Do 'JLPB T26186 'JLPB T29905-R28918 Se deja en comentario
         'Graba el movimiento en inventarios
         Valores = Comi & concepto & Comi & Coma
         Valores = Valores & Comi & consecutivo & Comi & Coma
         Valores = Valores & Comi & filas & Comi & Coma
         Valores = Valores & Comi & Mcuen(filas, 1) & Comi & Coma
         'Valores = Valores & Format(Mcuen(filas, 2), "##0") & Coma
         Valores = Valores & CDbl(Mcuen(filas, 2)) & Coma             'PJCA M1733
         Valores = Valores & Comi & Mcuen(filas, 3) & Comi & Coma
         Valores = Valores & Comi & Mcuen(filas, 4) & Comi & Coma
         Valores = Valores & Comi & Mcuen(filas, 5) & Comi & Coma
         'Valores = Valores & Format(Mcuen(filas, 6), "##0")
         'APGR T10703
         'Valores = Valores & CDbl(Mcuen(filas, 6))                    'PJCA M1733
         Valores = Valores & CDbl(Mcuen(filas, 6)) & Coma
         If Documento = Entrada Then
            If Mcuen(filas, 8) <> NUL$ Or UCase(Mcuen(filas, 8)) = "NO GENERA IVA" Then
               Valores = Valores & IIf(UCase(Mcuen(filas, 8)) <> "DEDUCIBLE", 1, 0) & Coma 'TIPO IVA
               Valores = Valores & CDbl(Mcuen(filas, 7))  'VALOR IVA
            Else
               GoTo Siguiente
            End If
         Else
Siguiente:
            Valores = Valores & 2 & Coma
            Valores = Valores & 0
         End If
         'APGR T10703
         Result = DoInsert("MOV_CONTABLE", Valores)
         If Result = FAIL Then Exit Do
         '''''''''''''''''''''''''''''''''''''''''
      'JLPB T29905-R28918 INICIO
      End If
      If Mcuen(filas + 1, 1) = NUL$ And BoGenMNIIF = False Then filas = 0: BoGenMNIIF = True: GoTo SEGUIR
      If BoGenMNIIF Then
         If BoMovNiif = True Then 'JAUM T32800-R31099
            If Result <> FAIL And BoTransInt = False Then Result = ExecSQLCommand("SAVE TRANSACTION P1")
            BoTransInt = True
            If Crear_Movimiento_Contable_NIIF(Linea) = FAIL Then Exit Do
            If Result = 2 Then
               Select Case MsgBox("No se va a generar movimiento contable NIIF porque la cuenta " & Mcuen(filas, 1) & " no se encuentra parametrizada como NIIF/PUC." & vbCrLf & "Desea continuar?", vbYesNo Or vbQuestion Or vbDefaultButton1, App.Title)
                  Case vbYes
                     Result = ExecSQLCommand("ROLLBACK TRANSACTION P1")
                     Exit Do
                  Case vbNo
                     Result = 0
                     Exit Do
               End Select
            End If
         'JLPB T29905-R28918 FIN
            'JLPB T25784-R25328  INICIO
            'If Documento <> Traslado And Documento <> Despacho And Documento <> DevolucionDespacho Then 'JLPB T26186 SE DEJA EN COMENTARIO
            Valores = "CD_CONC_MCNF = " & Comi & concepto & Comi & Coma
            Valores = Valores & "NU_CONS_MCNF = " & consecutivo & Coma
            Valores = Valores & "NU_LINE_MCNF = " & filas & Coma
            Valores = Valores & "CD_CUEN_MCNF = " & Comi & Mcuen(filas, 1) & Comi & Coma
            Valores = Valores & "VL_VALO_MCNF = " & ConDoble(Mcuen(filas, 2)) & Coma
            Valores = Valores & "ID_NATU_MCNF = " & Comi & Mcuen(filas, 3) & Comi & Coma
            Valores = Valores & "CD_TERC_MCNF = " & Comi & Mcuen(filas, 4) & Comi & Coma
            Valores = Valores & "CD_CECO_MCNF = " & Comi & Mcuen(filas, 5) & Comi & Coma
            Valores = Valores & "VL_BASE_MCNF = " & ConDoble(Val(Format(Mcuen(filas, 6), "############0.#0"))) & Coma
            If Documento = Entrada Then
               If Mcuen(filas, 8) <> NUL$ Or UCase(Mcuen(filas, 8)) = "NO GENERA IVA" Then
                  Valores = Valores & "TX_IVADED_MCNF=" & IIf(UCase(Mcuen(filas, 8)) <> "DEDUCIBLE", 1, 0) & Coma
                  Valores = Valores & "TX_IVA_MCNF=" & CDbl(Mcuen(filas, 7))
               Else
                  GoTo Siguiente1
               End If
            Else
Siguiente1:
               Valores = Valores & "TX_IVADED_MCNF=2" & Coma
               Valores = Valores & "TX_IVA_MCNF=0"
            End If
            Result = DoInsertSQL("MOV_CONTABLE_NIIF", Valores)
            If Result = FAIL Then Exit Do
            'End If 'JLPB T26186
            'JLPB T25784-R25328 FIN
         End If 'JAUM T32800-R31099
      End If 'JLPB T29905-R28918
SEGUIR: 'JLPB T29905-R28918
      filas = filas + 1
   Loop
End Sub

'Muestra por pantalla el movimiento contable
Sub Mostrar_Movimiento_Contable(ByVal concepto As String, ByVal Numero As Long)
  ReDim MArr(5, 0)
  Dim C As Integer
  limpia_matriz Mcuen, 200, 6
  Mcuen(1, 1) = "ULTIMO REGISTRO"
  Condicion = "CD_CONC_MOVC=" & Comi & concepto & Comi
  Condicion = Condicion & " And NU_CONS_MOVC=" & Numero
  Condicion = Condicion & " Order by NU_LINE_MOVC"
  Result = LoadMulData("MOV_CONTABLE", "CD_CUEN_MOVC, VL_VALO_MOVC, ID_NATU_MOVC, CD_TERC_MOVC, CD_CECO_MOVC, VL_BASE_MOVC", Condicion, MArr())
  If (Result <> False) Then
    If Encontro Then
       C = 0
       Do While C <= UBound(MArr, 2)
         Mcuen(C + 1, 1) = MArr(0, C)
         Mcuen(C + 1, 2) = Format(MArr(1, C), "############0.##00")
         Mcuen(C + 1, 3) = MArr(2, C)
         Mcuen(C + 1, 4) = MArr(3, C)
         Mcuen(C + 1, 5) = MArr(4, C)
         Mcuen(C + 1, 6) = Format(MArr(5, C), "############0.##00")
         C = C + 1
       Loop
       Mcuen(C + 1, 1) = "ULTIMO REGISTRO"
    End If
  End If
End Sub

'HRR M4450 Se cambia nombre a la funcion se agrega el 1 al final, por si hay que dejar el cambio 4470
''CUENTAS CONTABLES DE LOS MOVIMIENTOS
'Sub Cuentas_Contables_Movi(ByVal concepto As Integer, valo_cont() As Variant, ByVal Tercero As String, ByVal Dependencia As String)
Sub Cuentas_Contables_Movi1(valo_cont() As Variant, ByVal Tercero As String, ByVal Dependencia As String)     'DEPURACION DE CODIGO
'  Dim c As Integer
'  limpia_matriz McuenD, 200, 6
'  ReDim Marcas(2)
'  'Cuenta,Valor,D/C,Tercero,C.Costo,V.Base
'  c = 1
'  Do While c <= UBound(valo_cont, 2) + 1
'     McuenD(c, 1) = valo_cont(0, c - 1)
'     McuenD(c, 2) = Aplicacion.Formatear_Valor(valo_cont(1, c - 1))
'     McuenD(c, 3) = valo_cont(3, c - 1)
'     'If Valida_Marca_Cuenta(McuenD(c, 1), Marcas) = True Then
'     '   If Marcas(0) = "S" Then
'     '     McuenD(c, 4) = Tercero
'     '   End If
'     'End If
'     If ValidaTercero(McuenD(c, 1)) = True Then
'        McuenD(c, 4) = Tercero
'     End If
'
'     'If Valida_Marca_Cuenta(McuenD(c, 1), Marcas) = True Then
'     If Valida_Marca_Cuenta(McuenD(c, 1), Marcas) <> FAIL Then
'        If Marcas(1) = "S" Then
'          If Dependencia = NUL$ Then
'            McuenD(c, 5) = CentroC_Default
'          Else
'            McuenD(c, 5) = Dependencia
'          End If
'        End If
'     End If
'     McuenD(c, 6) = IIf(valo_cont(2, c - 1) = 0, 0, Aplicacion.Formatear_Valor(valo_cont(2, c - 1)))
'     c = c + 1
'  Loop
'
'  McuenD(c, 1) = "ULTIMO REGISTRO"
'  If primera_entrada = True Then
'     asignar_matriz McuenD, Mcuen, 200, 6
'  End If

  '--------------------------------
  'DAHV M4470 - INICIO
  Dim Incont As Integer
  'DAHV M4470 - FIN
  '--------------------------------
  
  Dim C As Integer
  limpia_matriz McuenD, 200, 6
  ReDim Marcas(2)
  'Estructura del arreglo MCuenD: Cuenta,Valor,D/C,Tercero,C.Costo,V.Base
  C = 1
  Incont = 1
  Do While C <= UBound(valo_cont, 2) + 1
     '------------------
     'DAHV M4450
     If (valo_cont(0, C - 1) <> NUL$) And (valo_cont(1, C - 1) <> NUL$) And (valo_cont(3, C - 1) <> NUL$) And (valo_cont(2, C - 1) <> NUL$) Then
            McuenD(Incont, 1) = valo_cont(0, C - 1)
            McuenD(Incont, 2) = Aplicacion.Formatear_Valor(valo_cont(1, C - 1))
            McuenD(Incont, 3) = valo_cont(3, C - 1)
            
            If ValidaTercero(McuenD(Incont, 1)) = True Then
               McuenD(Incont, 4) = Tercero
            End If
            
            If Valida_Marca_Cuenta(McuenD(Incont, 1), Marcas) <> FAIL Then
               If Marcas(1) = "S" Then
                 If Dependencia = NUL$ Then
                   McuenD(Incont, 5) = CentroC_Default
                 Else
                   McuenD(Incont, 5) = Dependencia
                 End If
               End If
            End If
            
            McuenD(Incont, 6) = IIf(valo_cont(2, C - 1) = 0, 0, Aplicacion.Formatear_Valor(valo_cont(2, C - 1)))
            Incont = Incont + 1
     
     End If
     C = C + 1
     'DAHV M4450
     '------------------
  Loop
  
  McuenD(C, 1) = "ULTIMO REGISTRO"
  If primera_entrada = True Then
     asignar_matriz McuenD, Mcuen, 200, 6
  End If

End Sub

'Sub Cuentas_Contables_Movi(valo_cont() As Variant, ByVal Tercero As String, ByVal Dependencia As String)     'DEPURACION DE CODIGO
Sub Cuentas_Contables_Movi(valo_cont() As Variant, ByVal Tercero As String, ByVal Dependencia As String, Optional Origen As String)     'LDCR PNC T10191
  Dim C As Integer
  'limpia_matriz McuenD, 200, 6
  limpia_matriz McuenD, 200, 8 'LDCR PNC T10191
  ReDim Marcas(2)
  'Cuenta,Valor,D/C,Tercero,C.Costo,V.Base
  C = 1
  Do While C <= UBound(valo_cont, 2) + 1
     McuenD(C, 1) = valo_cont(0, C - 1)
     If valo_cont(1, C - 1) = NUL$ Then valo_cont(1, C - 1) = 0 'HRR M4450
     McuenD(C, 2) = Aplicacion.Formatear_Valor(valo_cont(1, C - 1))
     McuenD(C, 3) = valo_cont(3, C - 1)
     'If Valida_Marca_Cuenta(McuenD(c, 1), Marcas) = True Then
     '   If Marcas(0) = "S" Then
     '     McuenD(c, 4) = Tercero
     '   End If
     'End If
     If ValidaTercero(McuenD(C, 1)) = True Then
        McuenD(C, 4) = Tercero
     End If
     
     'If Valida_Marca_Cuenta(McuenD(c, 1), Marcas) = True Then
     If Valida_Marca_Cuenta(McuenD(C, 1), Marcas) <> FAIL Then
        If Marcas(1) = "S" Then
          If Dependencia = NUL$ Then
            McuenD(C, 5) = CentroC_Default
          Else
            McuenD(C, 5) = Dependencia
          End If
        End If
     End If
     McuenD(C, 6) = IIf(valo_cont(2, C - 1) = 0, 0, Aplicacion.Formatear_Valor(valo_cont(2, C - 1)))
     'INICIO LDCR PNC T10191
     If Origen = "ENTRA" Then
        McuenD(C, 7) = valo_cont(4, C - 1)
        McuenD(C, 8) = valo_cont(5, C - 1)
     End If
     'FIN LDCR PNC T10191
     C = C + 1
  Loop
  
  McuenD(C, 1) = "ULTIMO REGISTRO"
  If primera_entrada = True Then
     asignar_matriz McuenD, Mcuen, 200, 6
  End If
End Sub



'Sub Cuentas_Contables_MoviT(ByVal concepto As Integer, valo_cont() As Variant, ByVal Tercero As String, ByVal DepOrig As String, DepDest As String)
Sub Cuentas_Contables_MoviT(valo_cont() As Variant, ByVal Tercero As String, ByVal DepOrig As String, DepDest As String)      'DEPURACION DE CODIGO
  Dim C As Integer
  limpia_matriz McuenD, 200, 6
  ReDim Marcas(2)
  'Cuenta,Valor,D/C,Tercero,C.Costo,V.Base
  C = 1
  Do While C <= UBound(valo_cont, 2) + 1
     McuenD(C, 1) = valo_cont(0, C - 1)
     McuenD(C, 2) = Format(valo_cont(1, C - 1), "############0.##00")
     McuenD(C, 3) = valo_cont(3, C - 1)
     'If Valida_Marca_Cuenta(McuenD(c, 1), Marcas) = True Then
     If Valida_Marca_Cuenta(McuenD(C, 1), Marcas) <> FAIL Then
        If Marcas(0) = "S" Then
          McuenD(C, 4) = Tercero
        End If
     End If
     If valo_cont(3, C - 1) = "D" Then McuenD(C, 5) = DepDest Else McuenD(C, 5) = DepOrig
     McuenD(C, 6) = valo_cont(2, C - 1)
     C = C + 1
  Loop
  McuenD(C, 1) = "ULTIMO REGISTRO"
  If primera_entrada = True Then
     asignar_matriz McuenD, Mcuen, 200, 6
  End If
End Sub

'Function Cuentas_Articulos(GrdArti As MSFlexGrid, CtaGrupo As String, Nat As String) As Variant
Function Cuentas_Articulos(GrdArti As MSFlexGrid, CtaGrupo As String, Nat As String, Bodega As Integer) As Variant
    Dim Ctas(1)
    Dim I As Long, J As Long, k As Long
    'ReDim Preserve CtasConta(3, 0)
    ReDim Preserve CtasConta(5, 0) 'LDCR T10191
    With GrdArti
        For I = 1 To .Rows - 1 Step 1
            'Condicion = "CD_CODI_GRUP  = CD_GRUP_ARTI AND CD_CODI_ARTI =" & Comi & .TextMatrix(I, 0) & Comi
            'Result = LoadData("ARTICULO, GRUP_ARTICULO", "CD_GRUP_ARTI, " & CtaGrupo, Condicion, Ctas())
            Condicion = "CD_CODI_GRUP  = CD_GRUP_ARTI AND CD_CODI_ARTI =" & Comi & .TextMatrix(I, 0) & Comi
            Condicion = Condicion & " AND CD_CODI_GRUP=CD_CODI_GRUP_RGB AND NU_AUTO_BODE_RGB=" & Bodega
            Result = LoadData("ARTICULO, GRUP_ARTICULO, IN_R_GRUP_BODE", "CD_GRUP_ARTI, " & CtaGrupo, Condicion, Ctas())
            If Ctas(1) = NUL$ Then J = -1 Else J = FindInArrM(CtasConta(), CStr(Ctas(1)))
            If IsNumeric(.TextMatrix(I, 2)) And IsNumeric(.TextMatrix(I, 3)) Then
              If CDbl(.TextMatrix(I, 2)) > 0 Then
                If J = -1 Then
                    k = UBound(CtasConta, 2) + 1
                    'If I > 1 Then ReDim Preserve CtasConta(3, k) Else k = 0
                    If I > 1 Then ReDim Preserve CtasConta(5, k) Else k = 0 'LDCR T10191
                    CtasConta(0, k) = Ctas(1)
                    'DAHV Se devuelven los cambios
                    CtasConta(1, k) = Aplicacion.Formatear_Valor(CDbl(.TextMatrix(I, 2)) * CDbl(.TextMatrix(I, 3))) 'HRR M5080
                    'CtasConta(1, k) = Aplicacion.Formatear_Valor(CDbl(.TextMatrix(i, 2)) * CDbl(.TextMatrix(i, 3)) * CDbl(.TextMatrix(i, 8)) / CDbl(.TextMatrix(i, 9))) 'HRR M5080
                    'DAHV Se devuelven los cambios
                    CtasConta(2, k) = 0
                    CtasConta(3, k) = Nat
                Else
                    'DAHV Se devuelven los cambios
                    CtasConta(1, J) = Aplicacion.Formatear_Valor(CDbl(CtasConta(1, J)) + (CDbl(.TextMatrix(I, 2)) * CDbl(.TextMatrix(I, 3)))) 'HRR M5080
                    'CtasConta(1, j) = Aplicacion.Formatear_Valor(CDbl(CtasConta(1, j)) + (CDbl(.TextMatrix(i, 2)) * CDbl(.TextMatrix(i, 3))) * CDbl(.TextMatrix(i, 8)) / CDbl(.TextMatrix(i, 9))) 'HRR M5080
                    'DAHV Se devuelven los cambios
                End If
              End If
            End If
        Next
    End With
    Cuentas_Articulos = CtasConta()

End Function

'Function Cuentas_Articulos_Costo(GrdArti As MSFlexGrid, CtaSalida As String, CtaCosto As String, NatSal As String) As Variant
Function Cuentas_Articulos_Costo(GrdArti As MSFlexGrid, CtaSalida As String, CtaCosto As String, NatSal As String, Bodega As Integer) As Variant
Dim Ctas(2)
Dim I, J As Long
ReDim ctaSal(3, 0)
ReDim ctaCOS(3, 0)
    With GrdArti
        For I = 1 To .Rows - 1 Step 1
'            Condicion = "CD_CODI_GRUP=CD_GRUP_ARTI AND CD_CODI_ARTI=" & Comi & .TextMatrix(I, 0) & Comi
'            Result = LoadData("ARTICULO, GRUP_ARTICULO", "VL_COPR_ARTI, " & CtaSalida & Coma & CtaCosto, Condicion, Ctas())
            Condicion = "CD_CODI_GRUP=CD_CODI_GRUP_RGB AND CD_CODI_GRUP=CD_GRUP_ARTI AND CD_CODI_ARTI=" & Comi & .TextMatrix(I, 0) & Comi
            Condicion = Condicion & " AND NU_AUTO_BODE_RGB=" & Bodega
            Result = LoadData("ARTICULO, GRUP_ARTICULO, IN_R_GRUP_BODE", "VL_COPR_ARTI, " & CtaSalida & Coma & CtaCosto, Condicion, Ctas())
            If Ctas(1) = NUL$ Then J = -1 Else J = FindInArrM(ctaSal(), CStr(Ctas(1)))
            If Ctas(0) = NUL$ Then Ctas(0) = 0
            If IsNumeric(.TextMatrix(I, 2)) Then
              If CDbl(.TextMatrix(I, 2)) > 0 Then
                If J = -1 Then
                    If I > 1 Then ReDim Preserve ctaSal(3, I - 1)
                    ctaSal(0, I - 1) = Ctas(1)
                    'DAHV se devuelven los cambios
                    ctaSal(1, I - 1) = CDbl(.TextMatrix(I, 2)) * CDbl(Ctas(0)) 'HRR M5080
                    'ctaSal(1, i - 1) = CDbl(.TextMatrix(i, 2)) * CDbl(Ctas(0)) * CDbl(.TextMatrix(i, 8)) / CDbl(.TextMatrix(i, 9)) 'HRR M5080
                    'DAHV se devuelven los cambios
                    ctaSal(1, I - 1) = CDbl(Aplicacion.Formatear_Valor(ctaSal(1, I - 1))) 'JACC DEPURACION NOV 2008 'HRR M4537
                    ctaSal(2, I - 1) = 0
                    ctaSal(3, I - 1) = NatSal
                Else
                    'DAHV Se devuelven los cambios
                    ctaSal(1, J) = CDbl(ctaSal(1, J)) + (CDbl(.TextMatrix(I, 2)) * CDbl(Ctas(0))) 'HRR M5080
                    'ctaSal(1, j) = CDbl(ctaSal(1, j)) + (CDbl(.TextMatrix(i, 2)) * CDbl(Ctas(0)) * CDbl(.TextMatrix(i, 8)) / CDbl(.TextMatrix(i, 9))) 'HRR M5080
                    'DAHV Se devuelven los cambios
                    ctaSal(1, J) = CDbl(Aplicacion.Formatear_Valor(ctaSal(1, J))) 'JACC DEPURACION NOV 2008 'HRR M4537
                End If
              End If
            End If
            
            If Ctas(2) = NUL$ Then J = -1 Else J = FindInArrM(ctaCOS(), CStr(Ctas(2)))
            If IsNumeric(.TextMatrix(I, 2)) Then
              If CDbl(.TextMatrix(I, 2)) > 0 Then
                If J = -1 Then
                    If I > 1 Then ReDim Preserve ctaCOS(3, I - 1)
                    ctaCOS(0, I - 1) = Ctas(2)
                    'Se devuelven los cambios
                     ctaCOS(1, I - 1) = CDbl(.TextMatrix(I, 2)) * CDbl(Ctas(0)) 'HRR M5080
                    'ctaCOS(1, i - 1) = CDbl(.TextMatrix(i, 2)) * CDbl(Ctas(0)) * CDbl(.TextMatrix(i, 8)) / CDbl(.TextMatrix(i, 9)) 'HRR M5080
                    ' DAHV Se devuelven los cambios
                    
                    ctaCOS(1, I - 1) = CDbl(Aplicacion.Formatear_Valor(ctaCOS(1, I - 1))) 'JACC DEPURACION NOV 2008 'HRR M4537
                    ctaCOS(2, I - 1) = 0
                    ctaCOS(3, I - 1) = IIf(NatSal = "D", "C", "D")
                Else
                    ' DAHV Se devuelven los cambios
                      ctaCOS(1, J) = CDbl(ctaCOS(1, J)) + (CDbl(.TextMatrix(I, 2)) * CDbl(Ctas(0))) 'HRR M5080
                    ' ctaCOS(1, j) = CDbl(ctaCOS(1, j)) + (CDbl(.TextMatrix(i, 2)) * CDbl(Ctas(0)) * CDbl(.TextMatrix(i, 8)) / CDbl(.TextMatrix(i, 9))) 'HRR M5080
                    ' DAHV Se devuelven los cambios
                    ctaCOS(1, J) = CDbl(Aplicacion.Formatear_Valor(ctaCOS(1, J))) 'JACC DEPURACION NOV 2008 'HRR M4537
                End If
              End If
            End If
        Next
    End With
    
    I = UBound(CtasConta(), 2)
    For J = 0 To UBound(ctaSal(), 2)
        If Trim(ctaSal(2, J)) <> NUL$ Then
           If I = 0 And CtasConta(3, 0) = NUL$ Then I = 0 Else I = I + 1
           'ReDim Preserve CtasConta(3, I)
           ReDim Preserve CtasConta(5, I) 'LDCR PNC T10191
           CtasConta(0, I) = ctaSal(0, J)
           CtasConta(1, I) = ctaSal(1, J)
           CtasConta(2, I) = ctaSal(2, J)
           CtasConta(3, I) = ctaSal(3, J)
        End If
    Next
    I = UBound(CtasConta(), 2)
    For J = 0 To UBound(ctaCOS(), 2)
        If Trim(ctaCOS(2, J)) <> NUL$ Then
           If I = 0 And CtasConta(3, 0) = NUL$ Then I = 0 Else I = I + 1
           'ReDim Preserve CtasConta(3, I)
           ReDim Preserve CtasConta(5, I) 'LDCR PNC T10191
           CtasConta(0, I) = ctaCOS(0, J)
           CtasConta(1, I) = ctaCOS(1, J)
           CtasConta(2, I) = ctaCOS(2, J)
           CtasConta(3, I) = ctaCOS(3, J)
        End If
    Next
    Cuentas_Articulos_Costo = CtasConta()
   
End Function

Function Cuentas_Articulos_Dependencia(GrdArti As MSFlexGrid, CtaSalida As String, CtaCosto As String, NatSal As String, Dependencia As String) As Variant
Dim Ctas(2)
Dim I, J As Long
ReDim ctaSal(3, 0)
ReDim ctaCOS(3, 0)
    With GrdArti
        For I = 1 To .Rows - 1 Step 1
'            Condicion = "CD_CODI_GRUP=CD_GRUP_ARTI AND CD_CODI_ARTI=" & Comi & .TextMatrix(I, 0) & Comi
'            Result = LoadData("ARTICULO, GRUP_ARTICULO", "VL_COPR_ARTI, " & CtaSalida & Coma & CtaCosto, Condicion, Ctas())
            Condicion = "CD_GRUP_RGD=CD_GRUP_ARTI AND CD_DEPE_RGD=" & Comi & Dependencia & Comi & " AND CD_CODI_ARTI=" & Comi & .TextMatrix(I, 0) & Comi
            Result = LoadData("ARTICULO, R_GRUP_DEPE", "VL_COPR_ARTI, " & CtaSalida & Coma & CtaCosto, Condicion, Ctas())
            'AASV M5710
            If Result <> FAIL And Not Encontro Then
               Condicion = " CD_CODI_ARTI=" & Comi & .TextMatrix(I, 0) & Comi
               Result = LoadData("ARTICULO", "VL_COPR_ARTI,'',''", Condicion, Ctas())
            End If
            'AASV M5710
            If Ctas(1) = NUL$ Then J = -1 Else J = FindInArrM(ctaSal(), CStr(Ctas(1))) 'AASV M5710 Se coloca en comentario la validacion 'AASV M5710 Se revierte el cambio para que no agrupe las que no tienen cuenta
            'j = FindInArrM(ctaSal(), CStr(Ctas(1))) 'AASV M5710 se deja sin validacion para que agrupe los articulos que no tienen cuenta.'AASV M5710 Se revierte el cambio se coloca en comentario
            If Ctas(0) = NUL$ Then Ctas(0) = 0
            If IsNumeric(.TextMatrix(I, 2)) Then
              If CDbl(.TextMatrix(I, 2)) > 0 Then
                If J = -1 Then
                    If I > 1 Then ReDim Preserve ctaSal(3, I - 1)
                    ctaSal(0, I - 1) = Ctas(1)
                    'DAHV Se devuelven los cambios
                     ctaSal(1, I - 1) = CDbl(.TextMatrix(I, 2)) * CDbl(Ctas(0)) 'HRR M5080
                    'ctaSal(1, i - 1) = CDbl(.TextMatrix(i, 2)) * CDbl(Ctas(0)) * CDbl(.TextMatrix(i, 8)) / CDbl(.TextMatrix(i, 9)) 'HRR M5080
                    'DAHV Se devuelven los cambios
                    ctaSal(1, I - 1) = CDbl(Aplicacion.Formatear_Valor(ctaSal(1, I - 1))) 'JACC DEPURACION NOV 2008 'HRR M4537
                    ctaSal(2, I - 1) = 0
                    ctaSal(3, I - 1) = NatSal
                Else
                    'DAHV Se devuelven los cambios
                     ctaSal(1, J) = CDbl(ctaSal(1, J)) + (CDbl(.TextMatrix(I, 2)) * CDbl(Ctas(0))) 'HRR M5080
                    'ctaSal(1, j) = CDbl(ctaSal(1, j)) + (CDbl(.TextMatrix(i, 2)) * CDbl(Ctas(0)) * CDbl(.TextMatrix(i, 8)) / CDbl(.TextMatrix(i, 9))) 'HRR M5080
                    'DAHV Se devuelven los cambios
                    ctaSal(1, J) = CDbl(Aplicacion.Formatear_Valor(ctaSal(1, J))) 'AASV M5434
                End If
              End If
            End If
            If Ctas(2) = NUL$ Then J = -1 Else J = FindInArrM(ctaCOS(), CStr(Ctas(2)))
            If IsNumeric(.TextMatrix(I, 2)) Then
              If CDbl(.TextMatrix(I, 2)) > 0 Then
                If J = -1 Then
                    If I > 1 Then ReDim Preserve ctaCOS(3, I - 1)
                    ctaCOS(0, I - 1) = Ctas(2)
                    ' DAHV Se devuelven los cambios
                      ctaCOS(1, I - 1) = CDbl(.TextMatrix(I, 2)) * CDbl(Ctas(0)) 'HRR M5080
                    ' ctaCOS(1, i - 1) = CDbl(.TextMatrix(i, 2)) * CDbl(Ctas(0)) * CDbl(.TextMatrix(i, 8)) / CDbl(.TextMatrix(i, 9)) 'HRR M5080
                    ' DAHV Se devuelven los cambios
                    ctaCOS(1, I - 1) = CDbl(Aplicacion.Formatear_Valor(ctaCOS(1, I - 1))) 'JACC DEPURACION NOV 2008 'HRR M4537
                    ctaCOS(2, I - 1) = 0
                    ctaCOS(3, I - 1) = IIf(NatSal = "D", "C", "D")
                Else
                    'DAHV Se devuelven los cambios
                     ctaCOS(1, J) = CDbl(ctaCOS(1, J)) + (CDbl(.TextMatrix(I, 2)) * CDbl(Ctas(0))) 'HRR M5080
                     'ctaCOS(1, j) = CDbl(ctaCOS(1, j)) + (CDbl(.TextMatrix(i, 2)) * CDbl(Ctas(0)) * CDbl(.TextMatrix(i, 8)) / CDbl(.TextMatrix(i, 9))) 'HRR M5080
                    'DAHV Se devuelven los cambios
                    ctaCOS(1, J) = CDbl(Aplicacion.Formatear_Valor(ctaCOS(1, J))) 'JACC DEPURACION NOV 2008 'HRR M4537
                End If
              End If
            End If
        Next
    End With
    
    I = UBound(CtasConta(), 2)
    For J = 0 To UBound(ctaSal(), 2)
        If Trim(ctaSal(2, J)) <> NUL$ Then
           If I = 0 And CtasConta(3, 0) = NUL$ Then I = 0 Else I = I + 1
           'ReDim Preserve CtasConta(3, I)
           ReDim Preserve CtasConta(5, I) 'LDCR PNC T10191
           CtasConta(0, I) = ctaSal(0, J)
           CtasConta(1, I) = ctaSal(1, J)
           CtasConta(2, I) = ctaSal(2, J)
           CtasConta(3, I) = ctaSal(3, J)
        End If
    Next
    I = UBound(CtasConta(), 2)
    For J = 0 To UBound(ctaCOS(), 2)
        If Trim(ctaCOS(2, J)) <> NUL$ Then
           If I = 0 And CtasConta(3, 0) = NUL$ Then I = 0 Else I = I + 1
           'ReDim Preserve CtasConta(3, I)
           ReDim Preserve CtasConta(5, I) 'LDCR PNC T10191
           CtasConta(0, I) = ctaCOS(0, J)
           CtasConta(1, I) = ctaCOS(1, J)
           CtasConta(2, I) = ctaCOS(2, J)
           CtasConta(3, I) = ctaCOS(3, J)
        End If
    Next
    Cuentas_Articulos_Dependencia = CtasConta()
End Function

'Function Cuentas_Movimiento(Movimiento As Integer, ARRValor() As Variant, bruto As Double, Optional GrdImDe As MSFlexGrid, Optional GrdArti As MSFlexGrid) As Variant
Function Cuentas_Movimiento(Movimiento As Integer, ARRValor() As Variant, bruto As Double, Optional GrdImDe As MSFlexGrid, Optional GrdArti As MSFlexGrid, Optional Bodega As Integer) As Variant
    Dim I As Long
    Dim k As Double, J As Double
    Dim CtaGrupo As String
    Dim Impuesto As Double 'JAGS T10733
    Impuesto = 0 'JAGS T10733
    'JLPB T23820 INICIO SE DEJA EN COMENTARIO LA SIGUIENTE LINEA
'    'DAHV T6768 - INICIO
'    Dim BoAproxCentena As Boolean
'    BoAproxCentena = False
'    If Aplicacion.Interfaz_CxP Then
'       'INICIO LDCR T7213
'       'BoAproxCentena = CBool(fnDevDato("PARAMETROS_CXP", "NU_REDIMDE_PCXP", NUL$, True))
'       If ExisteCAMPO("PARAMETROS_CXP", "NU_REDIMDE_PCXP") Then BoAproxCentena = CBool(fnDevDato("PARAMETROS_CXP", "NU_REDIMDE_PCXP", NUL$, True))
'       'FIN LDCR T7213
'    End If
'
'    'DAHV T6768 - FIN
    'JLPB T23820 FIN
    
    
    ' DAHV T6254 - INICIO
    ' Proceso de consulta para determinar si se tiene parametrizado por articulo
      Dim StIvaPorArt As String
      StIvaPorArt = ""
      StIvaPorArt = fnDevDato("PARAMETROS_IMPUESTOS, ENTIDAD", "TX_IVADEDAR_PAIM", "CD_CODI_TERC_PAIM = CD_NIT_ENTI", True)
    ' DAHV T6254 - FIN
    
    
    ReDim arr(5)
    Result = LoadData("CONCEPTOT", Asterisco, "NU_MOVI_CONC=" & Movimiento, arr())
    If arr(0) <> NUL$ Then
        I = UBound(CtasConta, 2) + 1
        'iva
        If ARRValor(0) > 0 Then
'            ReDim Preserve CtasConta(3, I)
'            CtasConta(0, I) = Arr(2)
'            CtasConta(1, I) = ARRValor(0)
'            CtasConta(2, I) = VBase
'            CtasConta(3, I) = "D"
'            I = I + 1
            If IVADeducible() Then                  'Req 1112-983-1035
                'DAHV T6254 - INICIO
                If StIvaPorArt = "S" Then
                        With GrdArti
                            For J = 1 To .Rows - 1
                                 StIvaArt = fnDevDato("ARTICULO", "TX_IVADED_ARTI", "CD_CODI_ARTI=" & Comi & .TextMatrix(J, 0) & Comi, True)
                                 If StIvaArt = "1" Then
                                        'ReDim Preserve CtasConta(3, I)
                                        ReDim Preserve CtasConta(5, I) 'LDCR PNC 10191
                                        CtasConta(0, I) = arr(2)
                                        ' DAHV T7406
                                        'CtasConta(1, i) = ARRValor(0)
                                        CtasConta(1, I) = .TextMatrix(J, 6)
                                        ' DAHV T7406
                                        CtasConta(2, I) = VBase
                                        CtasConta(3, I) = "D"
                                        'INICIO LDCR PNC 10191
                                        CtasConta(4, I) = ArrIVA(J - 1)
                                        CtasConta(5, I) = "DEDUCIBLE"
                                        'FIN LDCR PNC 10191
                                        I = I + 1
                                 Else
                                        Condicion = "CD_CODI_GRUP=CD_CODI_GRUP_RGB AND CD_CODI_GRUP=CD_GRUP_ARTI AND CD_CODI_ARTI=" & Comi & .TextMatrix(J, 0) & Comi
                                        Condicion = Condicion & " AND NU_AUTO_BODE_RGB=" & Bodega
                                        CtaGrupo = BuscarCuenta("ARTICULO,GRUP_ARTICULO,IN_R_GRUP_BODE", IIf(boFact, "CD_ENTR_RGB", "CD_TRAN_RGB"), Condicion)
                                        k = FindInArrM(CtasConta, CtaGrupo, 0)
                                        If ArrIVA(J - 1) = NUL$ Then ArrIVA(J - 1) = 0
                                        CtasConta(1, k) = CDbl(CtasConta(1, k)) + ArrIVA(J - 1)
                                        'INICIO LDCR PNC 10191
                                        CtasConta(2, k) = VBase
                                        CtasConta(4, k) = ArrIVA(J - 1)
                                        CtasConta(5, k) = "NO DEDUCIBLE"
                                        'FIN LDCR PNC 10191
                                 End If
                            Next J
                        End With
                Else
                        'ReDim Preserve CtasConta(3, I)
                        ReDim Preserve CtasConta(5, I) 'LDCR PNC T10191
                        CtasConta(0, I) = arr(2)
                        CtasConta(1, I) = ARRValor(0)
                        CtasConta(2, I) = VBase
                        CtasConta(3, I) = "D"
                        'INICIO LDCR PNC 10191
                        CtasConta(4, I) = ARRValor(0)
                        CtasConta(5, I) = "DEDUCIBLE"
                        'FIN LDCR PNC 10191
                        I = I + 1
                End If
                'DAHV T6254 - FIN
            Else
                'Req 1112-983-1035
                With GrdArti
                  For J = 1 To .Rows - 1
                     'CtaGrupo = BuscarCuenta("ARTICULO,GRUP_ARTICULO", "CD_ENTR_GRUP", "CD_CODI_GRUP=CD_GRUP_ARTI AND CD_CODI_ARTI=" & Comi & .TextMatrix(J, 0) & Comi)
                     Condicion = "CD_CODI_GRUP=CD_CODI_GRUP_RGB AND CD_CODI_GRUP=CD_GRUP_ARTI AND CD_CODI_ARTI=" & Comi & .TextMatrix(J, 0) & Comi
                     Condicion = Condicion & " AND NU_AUTO_BODE_RGB=" & Bodega
                     CtaGrupo = BuscarCuenta("ARTICULO,GRUP_ARTICULO,IN_R_GRUP_BODE", IIf(boFact, "CD_ENTR_RGB", "CD_TRAN_RGB"), Condicion)
                     k = FindInArrM(CtasConta, CtaGrupo, 0)
                     If ArrIVA(J - 1) = NUL$ Then ArrIVA(J - 1) = 0
                     'JAGS T10733 inicio
                     Impuesto = Impuesto + ArrIVA(J - 1)
                     If k < 0 Then k = 0 'JLPB T22240
                     'CtasConta(1, k) = CDbl(CtasConta(1, k)) + ArrIVA(J - 1) 'AFMG T22240 SE ELIMINA COMENTARIO 'JLPB T22240 SE DEJA EN COMENTARIO
                     'CtasConta(1, k) = CDbl(CtasConta(1, k)) + IIf(BoAproxCentena, AproxCentena(Round(ArrIVA(J - 1))), ArrIVA(J - 1)) 'JLPB T22240 'JLPB T23820 SE DEJA EN COMENTARIO
                     CtasConta(1, k) = CDbl(CtasConta(1, k)) + IIf(BoAproxCent, AproxCentena(Round(ArrIVA(J - 1))), ArrIVA(J - 1)) 'JLPB T23820
'                     CtasConta(4, k) = ArrIVA(J - 1)
'                     CtasConta(5, k) = "NO DEDUCIBLE"
                     'JAGS T10733 fin
                  Next J
                    'JAGS T10733 inicio
                    'If BoAproxCentena Then Impuesto = AproxCentena(Round(Impuesto)) 'JLPB T23820 SE DEJA EN COMENTARIO
                    If BoAproxCent Then Impuesto = AproxCentena(Round(Impuesto)) 'JLPB T23820
                    'CtasConta(1, k) = CDbl(CtasConta(1, k)) + Impuesto 'AFMG T22240 SE DEJA EN COMENTARIO
                    CtasConta(2, 0) = VBase 'APGR T10757
                    CtasConta(4, 0) = Impuesto
                    CtasConta(5, 0) = "NO DEDUCIBLE"
                    'JAGS T10733 fin
                End With
            End If
        End If
        'retefte
        If ARRValor(3) > 0 Then
'            ReDim Preserve CtasConta(3, I)
'            CtasConta(0, I) = Arr(5)
'            CtasConta(1, I) = ARRValor(3)
'            CtasConta(2, I) = bruto
'            CtasConta(3, I) = "C"
'            I = I + 1
            With GrdImDe
              For k = 1 To .Rows - 1
                If Mid(.TextMatrix(k, 1), 1, 1) = "R" Then
                  'ReDim Preserve CtasConta(3, I)
                  ReDim Preserve CtasConta(5, I) 'LDCR PNC T10191
                  CtasConta(0, I) = BuscarCuenta("TC_IMPUESTOS", "CD_CUEN_IMPU", "CD_CODI_IMPU=" & Comi & Mid(.TextMatrix(k, 1), 2, Len(.TextMatrix(k, 1))) & Comi)
                  'CtasConta(1, I) = ARRValor(3)
                  CtasConta(1, I) = .TextMatrix(k, 4)     'M0924
                  ' DAHV T6788 - INICIO
                  ' CtasConta(2, i) = bruto
                  'JLPB T15675 INICIO LAS SIGUIENTES TRES LINEAS SE DEJA EN COMENTARIO
'                  If BoAproxCentena Then
'                        CtasConta(2, I) = AproxCentena(bruto)
'                  Else
                  'JLPB T15675 FIN
                        CtasConta(2, I) = bruto
'                  End If 'JLPB T15675 SE DEJA EN COMENTARIO
                  ' DAHV T6788 - INICIO
                  
                  CtasConta(3, I) = "C"
                  I = I + 1
                End If
              Next k
            End With
        End If
        'reteiva
        If ARRValor(1) > 0 Then
'            ReDim Preserve CtasConta(3, I)
'            CtasConta(0, I) = Arr(3)
'            CtasConta(1, I) = ARRValor(1)
'            CtasConta(2, I) = ARRValor(0)
'            CtasConta(3, I) = "C"
'            I = I + 1
          With GrdImDe
            For k = 1 To .Rows - 1
              If Mid(.TextMatrix(k, 1), 1, 1) = "M" Then
                'ReDim Preserve CtasConta(3, I)
                ReDim Preserve CtasConta(5, I) 'LDCR PNC T10191
                CtasConta(0, I) = BuscarCuenta("TC_IMPUESTOS", "CD_CUEN_IMPU", "CD_CODI_IMPU=" & Comi & Mid(.TextMatrix(k, 1), 2, Len(.TextMatrix(k, 1))) & Comi)
                'CtasConta(1, I) = ARRValor(1)
                CtasConta(1, I) = .TextMatrix(k, 4)     'M0924
                ' DAHV T6788 - INICIO
                'CtasConta(2, i) = ARRValor(0)
                'JLPB T15675 INICIO LAS SIGUIENTES TRES LINEAS SE DEJA EN COMENTARIO
'                  If BoAproxCentena Then
'                   CtasConta(2, I) = AproxCentena(CDbl(ARRValor(0)))
'                  Else
                'JLPB T15675 FIN
                    CtasConta(2, I) = ARRValor(0)
'                  End If 'JLPB T15675 SE DEJA EN COMENTARIO
                ' DAHV T6788 - FIN
                
                CtasConta(3, I) = "C"
                I = I + 1
              End If
            Next k
          End With
        End If
            'reteica
        If ARRValor(2) > 0 Then
'            ReDim Preserve CtasConta(3, I)
'            CtasConta(0, I) = Arr(4)
'            CtasConta(1, I) = ARRValor(2)
'            CtasConta(2, I) = bruto
'            CtasConta(3, I) = "C"
'            I = I + 1
          With GrdImDe
            For k = 1 To .Rows - 1
              If Mid(.TextMatrix(k, 1), 1, 1) = "C" Then
                'ReDim Preserve CtasConta(3, I)
                ReDim Preserve CtasConta(5, I) 'LDCR PNC T10191
                CtasConta(0, I) = BuscarCuenta("TC_IMPUESTOS", "CD_CUEN_IMPU", "CD_CODI_IMPU=" & Comi & Mid(.TextMatrix(k, 1), 2, Len(.TextMatrix(k, 1))) & Comi)
                'CtasConta(1, I) = ARRValor(2)
                CtasConta(1, I) = .TextMatrix(k, 4)     'M0924
                'DAHV T6788 INICIO
                'CtasConta(2, i) = bruto
                'JLPB T15675 INICIO LAS SIGUIENTES TRES LINEAS SE DEJA EN COMENTARIO
'                  If BoAproxCentena Then
'                        CtasConta(2, I) = AproxCentena(bruto)
'                  Else
                'JLPB T15675 FIN
                        CtasConta(2, I) = bruto
'                  End If 'JLPB T15675 SE DEJA EN COMENTARIO
                'DAHV T6788 FIN
                
                CtasConta(3, I) = "C"
                I = I + 1
              End If
            Next k
          End With
        End If
    Else
        Call Mensaje1("No se ha realizado la parametrizaci�n contable", 3)
    End If
    
    
    Cuentas_Movimiento = CtasConta()
End Function

Function Cuentas_Factura(Movimiento As Integer, ARRValor() As Variant, bruto As Currency) As Variant
Dim I As Long
ReDim arr(5)
    Result = LoadData("CONCEPTOT", Asterisco, "NU_MOVI_CONC=" & Movimiento, arr())
    If arr(0) <> NUL$ Then
        I = UBound(CtasConta(), 2) + 1
        'iva
        If ARRValor(0) > 0 Then
            'ReDim Preserve CtasConta(3, I)
            ReDim Preserve CtasConta(5, I) 'LDCR PNC T10191
            CtasConta(0, I) = arr(2)
            CtasConta(1, I) = ARRValor(0)
            CtasConta(2, I) = bruto
            CtasConta(3, I) = "C"
            I = I + 1
        End If
        'retefte
        If ARRValor(3) > 0 Then
            'ReDim Preserve CtasConta(3, I)
            ReDim Preserve CtasConta(5, I) 'LDCR PNC T10191
            CtasConta(0, I) = arr(5)
            CtasConta(1, I) = ARRValor(3)
            CtasConta(2, I) = bruto
            CtasConta(3, I) = "D"
            I = I + 1
        End If
        'reteiva
        If ARRValor(1) > 0 Then
            'ReDim Preserve CtasConta(3, I)
            ReDim Preserve CtasConta(5, I) 'LDCR PNC T10191
            CtasConta(0, I) = arr(3)
            CtasConta(1, I) = ARRValor(1)
            CtasConta(2, I) = bruto
            CtasConta(3, I) = "D"
            I = I + 1
        End If
            'reteica
        If ARRValor(2) > 0 Then
            'ReDim Preserve CtasConta(3, I)
            ReDim Preserve CtasConta(5, I) 'LDCR PNC T10191
            CtasConta(0, I) = arr(4)
            CtasConta(1, I) = ARRValor(2)
            CtasConta(2, I) = bruto
            CtasConta(3, I) = "D"
            I = I + 1
        End If
    Else
        Call Mensaje1("No se ha realizado la parametrizaci�n contable", 3)
    End If
    Cuentas_Factura = CtasConta()
End Function

Function ValidaTercero(Tercero As String) As Boolean
Dim ArrTerc() As Variant

   ValidaTercero = True
   
   ReDim ArrTerc(0)
   Condi = "CD_CODI_CUEN =" & Comi & Trim(Tercero) & Comi
   Result = LoadData("CUENTAS", "ID_TERC_CUEN", Condi, ArrTerc)
   If Result <> FAIL And Encontro Then
      If Trim(ArrTerc(0)) <> "S" Then
         ValidaTercero = False
      End If
   End If


End Function

Function IVADeducible() As Boolean
Dim ArrIVA1() As Variant

    IVADeducible = False
    
    ReDim ArrIVA1(0)
    Result = LoadData("ENTIDAD", "CD_NIT_ENTI", NUL$, ArrIVA1)
    If Result <> FAIL And Encontro Then
        Result = LoadData("PARAMETROS_IMPUESTOS", "ID_IVADEDU_PAIM", "CD_CODI_TERC_PAIM=" & Comi & ArrIVA1(0) & Comi, ArrIVA1())
        If Result <> FAIL And Encontro Then
            If ArrIVA1(0) = "" Then ArrIVA1(0) = 0
            IVADeducible = IIf(ArrIVA1(0) = "1", True, False)
        End If
   End If
        
End Function

Function BuscarCuenta(Tbl, CMP, Condi) As String
Dim ArrCta() As Variant
       
   ReDim ArrCta(0)
   Result = LoadData(Tbl, CMP, Condi, ArrCta)
   If Result <> FAIL Then
     If Encontro Then
        BuscarCuenta = ArrCta(0)
     Else
        BuscarCuenta = ""
     End If
   End If

End Function
Function Cuentas_ArticulosTraslado(GrdArti As MSFlexGrid, CtaGrupoOrigen As String, CtaGrupoDestino As String, _
                                   NatOrigen As String, NatDestino As String, BodegaORIGEN As Integer, BodegaDESTINO As Integer) As Variant
    
    
    Dim Ctas(2)
    Dim CtasConta1()
    Dim I As Long, J As Long, k As Long
    'ReDim Preserve CtasConta(3, 0)
    'ReDim CtasConta(3, 0) 'DAHV M3894
    ReDim CtasConta(5, 0) 'LDCR T10396
    With GrdArti
        For I = 1 To .Rows - 1 Step 1
            Campos = "CD_GRUP_ARTI, " & CtaGrupoOrigen & ",VL_COPR_ARTI"
            Condicion = "CD_CODI_GRUP  = CD_GRUP_ARTI AND CD_CODI_ARTI =" & Comi & .TextMatrix(I, 0) & Comi
            Condicion = Condicion & " AND CD_CODI_GRUP=CD_CODI_GRUP_RGB AND NU_AUTO_BODE_RGB=" & BodegaORIGEN
            Result = LoadData("ARTICULO, GRUP_ARTICULO, IN_R_GRUP_BODE", Campos, Condicion, Ctas())
            If Ctas(1) = NUL$ Then J = -1 Else J = FindInArrM(CtasConta(), CStr(Ctas(1)))
            If IsNumeric(.TextMatrix(I, 2)) And IsNumeric(.TextMatrix(I, 3)) Then
              If CDbl(.TextMatrix(I, 2)) > 0 Then
                If J = -1 Then
                    k = UBound(CtasConta, 2) + 1
                    'If I > 1 Then ReDim Preserve CtasConta(3, k) Else k = 0
                    If I > 1 Then ReDim Preserve CtasConta(5, k) Else k = 0 'LDCR T10191
                    CtasConta(0, k) = Ctas(1)
                    'CtasConta(1, K) = Aplicacion.Formatear_Valor(CDbl(.TextMatrix(I, 2)) * Ctas(2))
                    'DAHV Se devuelven los cambios
                     CtasConta(1, k) = Aplicacion.Formatear_Valor(CDbl(.TextMatrix(I, 2)) * CDbl(.TextMatrix(I, 3))) 'PJCA M974 'HRR M5080
                    'CtasConta(1, k) = Aplicacion.Formatear_Valor(CDbl(.TextMatrix(i, 2)) * CDbl(.TextMatrix(i, 3)) * CDbl(.TextMatrix(i, 8)) / CDbl(.TextMatrix(i, 9))) 'PJCA M974 'HRR M5080
                    'DAHV Se devuelven los cambios
                    
                    CtasConta(2, k) = 0
                    CtasConta(3, k) = NatOrigen
                Else
                    'DAHV Se devuelven los cambios
                     CtasConta(1, J) = Aplicacion.Formatear_Valor(CDbl(CtasConta(1, J)) + (CDbl(.TextMatrix(I, 2)) * CDbl(.TextMatrix(I, 3)))) 'HRR M5080
                    'CtasConta(1, j) = Aplicacion.Formatear_Valor(CDbl(CtasConta(1, j)) + (CDbl(.TextMatrix(i, 2)) * CDbl(.TextMatrix(i, 3)) * CDbl(.TextMatrix(i, 8)) / CDbl(.TextMatrix(i, 9)))) 'HRR M5080
                    'DAHV Se devuelven los cambios
                End If
              End If
            End If
        Next
        
        ReDim CtasConta1(3, 0)
        For I = 1 To .Rows - 1 Step 1
            Campos = "CD_GRUP_ARTI, " & CtaGrupoDestino & ",VL_COPR_ARTI"
            Condicion = "CD_CODI_GRUP  = CD_GRUP_ARTI AND CD_CODI_ARTI =" & Comi & .TextMatrix(I, 0) & Comi
            Condicion = Condicion & " AND CD_CODI_GRUP=CD_CODI_GRUP_RGB AND NU_AUTO_BODE_RGB=" & BodegaDESTINO
            Result = LoadData("ARTICULO, GRUP_ARTICULO, IN_R_GRUP_BODE", Campos, Condicion, Ctas())
            If Ctas(1) = NUL$ Then J = -1 Else J = FindInArrM(CtasConta1(), CStr(Ctas(1)))
            If IsNumeric(.TextMatrix(I, 2)) And IsNumeric(.TextMatrix(I, 3)) Then
              If CDbl(.TextMatrix(I, 2)) > 0 Then
                If J = -1 Then
                    k = UBound(CtasConta1, 2) + 1
                    If I > 1 Then ReDim Preserve CtasConta1(3, k) Else k = 0
                    CtasConta1(0, k) = Ctas(1)
                    'CtasConta1(1, K) = Aplicacion.Formatear_Valor(CDbl(.TextMatrix(I, 2)) * Ctas(2))
                    'DAHV Se devuelven los cambios
                     CtasConta1(1, k) = Aplicacion.Formatear_Valor(CDbl(.TextMatrix(I, 2)) * CDbl(.TextMatrix(I, 3)))    'PJCA M974 'HRR M5080
                    'CtasConta1(1, k) = Aplicacion.Formatear_Valor(CDbl(.TextMatrix(i, 2)) * CDbl(.TextMatrix(i, 3)) * CDbl(.TextMatrix(i, 8)) / CDbl(.TextMatrix(i, 9))) 'PJCA M974 'HRR M5080
                    'DAHV Se devuelven los cambios
                    CtasConta1(2, k) = 0
                    CtasConta1(3, k) = NatDestino
                Else
                    'DAHV Se devuelven los cambios
                     CtasConta1(1, J) = Aplicacion.Formatear_Valor(CDbl(CtasConta1(1, J)) + (CDbl(.TextMatrix(I, 2)) * CDbl(.TextMatrix(I, 3)))) 'HRR M5080
                    'CtasConta1(1, j) = Aplicacion.Formatear_Valor(CDbl(CtasConta1(1, j)) + (CDbl(.TextMatrix(i, 2)) * CDbl(.TextMatrix(i, 3)) * CDbl(.TextMatrix(i, 8)) / CDbl(.TextMatrix(i, 9)))) 'HRR M5080
                    'DAHV Se devuelven los cambios
                End If
              End If
            End If
        Next
    End With
    
    k = UBound(CtasConta, 2)
    J = UBound(CtasConta1, 2) + 1
    'ReDim Preserve CtasConta(3, k + J)
    ReDim Preserve CtasConta(5, k + J) 'LDCR T10191
    J = 0                                       'PJCA M974
    'For I = 0 + J To UBound(CtasConta, 2)
    For I = k + 1 To UBound(CtasConta, 2)       'PJCA M974
'        CtasConta(0, I) = CtasConta1(0, I - J)
'        CtasConta(1, I) = CtasConta1(1, I - J)
'        CtasConta(2, I) = CtasConta1(2, I - J)
'        CtasConta(3, I) = CtasConta1(3, I - J)
        CtasConta(0, I) = CtasConta1(0, J)       'PJCA M974
        CtasConta(1, I) = CtasConta1(1, J)       'PJCA M974
        CtasConta(2, I) = CtasConta1(2, J)       'PJCA M974
        CtasConta(3, I) = CtasConta1(3, J)       'PJCA M974
        J = J + 1                               'PJCA M974
    Next I
    
    
    Cuentas_ArticulosTraslado = CtasConta()
End Function
'---------------------------------------------------------------------------------------
' Procedure : AgruparValIVA
' DateTime  : 23/02/2012 10:11
' Author    : Luis David Colmenarez
' Purpose   : 2da PARTE R9033/T9930
'---------------------------------------------------------------------------------------
'
'Sub AgruparValIVA(bodega As String)
'Dim BoIvaDed, BoExiste As Boolean
'Dim StArticulo As String
'Dim StCuentaApli As String
'Dim ValFilas() As Variant
'Dim Info() As Variant
'Dim I, J, Y, Contador As Integer
'
'
'    ReDim Arr(0)
'    Campos = "ID_IVADEDU_PAIM"
'    Desde = "PARAMETROS_IMPUESTOS, ENTIDAD"
'    Condicion = "CD_CODI_TERC_PAIM = CD_NIT_ENTI"
'    Result = LoadData(Desde, Campos, Condicion, Arr())
'
'    If Arr(0) = 1 Then
'        BoIvaDed = True
'    Else
'        BoIvaDed = False
'    End If
'
'    ReDim Arr(0)
'    Campos = "CD_IVA_CONC"
'    Desde = "CONCEPTOT"
'    Condicion = "NU_MOVI_CONC  = " & entrada
'    Result = LoadData(Desde, Campos, Condicion, Arr())
'    StCuentaApli = Arr(0)
'
'    For I = 0 To UBound(ArrTipoIVA, 2)
'        If ArrTipoIVA(2, I) <> NUL$ Then
'            Condicion = "CD_CODI_GRUP=CD_CODI_GRUP_RGB AND CD_CODI_GRUP=CD_GRUP_ARTI AND CD_CODI_ARTI ='" & ArrTipoIVA(2, I) & Comi
'            Condicion = Condicion & " AND NU_AUTO_BODE_RGB=" & bodega
'            ReDim Arr(0)
'            Result = LoadData("ARTICULO", "TX_IVADED_ARTI", "CD_CODI_ARTI ='" & ArrTipoIVA(2, I) & Comi, Arr)
'
'            ArrTipoIVA(2, I) = IIf(Arr(0) = 0, BuscarCuenta("ARTICULO,GRUP_ARTICULO,IN_R_GRUP_BODE", IIf(boFact, "CD_ENTR_RGB", "CD_TRAN_RGB"), Condicion), StCuentaApli)
'
'            If ArrTipoIVA(2, I) = NUL$ Then ArrTipoIVA(2, I) = Mcuen(I + 1, 1)
'
'        End If
'    Next
'
'
    
'    ReDim Info(2, 0)
'
'    Info = ArrTipoIVA
'    Contador = 0
'
'    For I = 0 To UBound(ArrTipoIVA, 2)
'        For J = 0 To UBound(Info, 2)
'            If ArrTipoIVA(2, I) = Info(2, J) Then
'                If Contador <> 0 Then
'                    BoExiste = False
'
'                    For k = 0 To UBound(ValFilas, 2)
'                        If ValFilas(2, k) = ArrTipoIVA(2, J) Then
'                            ValFilas(0, k) = ValFilas(0, k) + ArrTipoIVA(0, I)
'                            ValFilas(1, k) = ValFilas(1, k) + ArrTipoIVA(1, I)
'                            ValFilas(2, k) = ArrTipoIVA(2, J)
'                            BoExiste = True
'                        End If
'                    Next
'
'                    If BoExiste = False Then
'                        ReDim Preserve ValFilas(2, Contador)
'                        ValFilas(0, Contador) = ValFilas(0, Contador) + ArrTipoIVA(0, I)
'                        ValFilas(1, Contador) = ValFilas(1, Contador) + ArrTipoIVA(1, I)
'                        ValFilas(2, Contador) = ArrTipoIVA(2, J)
'                        Contador = Contador + 1
'                    End If
'
'                Else
'                    ReDim Preserve ValFilas(2, Contador)
'                    ValFilas(0, Contador) = ValFilas(0, Contador) + ArrTipoIVA(0, I)
'                    ValFilas(1, Contador) = ValFilas(1, Contador) + ArrTipoIVA(1, I)
'                    ValFilas(2, Contador) = ArrTipoIVA(2, I)
'                    Contador = Contador + 1
'                End If
'                ArrTipoIVA(0, I) = 0
'                ArrTipoIVA(1, I) = 0
'            End If
'        Next
'    Next
'
'    ReDim ArrTipoIVA(2, UBound(ValFilas, 2))
'    ArrTipoIVA = ValFilas
     
'End Sub

