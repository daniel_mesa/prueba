VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "UnArticulo"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit
Private cod As String * 16 'C�digo del art�culo
Private nom As String * 50 'Nombre del art�culo
Private AuNum As Long 'autoN�mero del art�culo
Private AuUnd As Long 'autoN�mero UNIDAD DE CONTEO
Private NmUnd As String * 50 'Nombre UNIDAD DE CONTEO
Private mult As Integer 'Multiplicador
Private divi As Integer 'Divisor
Private EsPa As Byte 'Es para 0 Venta, 1 Consumo
Private CdGru As String 'C�digo del GRUPO
Private NmGru As String * 50 'Nombre del GRUPO
Private CdUso As String 'C�digo del USO
Private NmUso As String * 50 'C�digo del USO
'Private VlUlCom As Double 'Valor �ltima compra     'DEPURACION DE CODIGO
Private VlCoPro As Double 'Valor costo promedio

'NU_AUTO_ARTI, CD_CODI_ARTI, NO_NOMB_ARTI, NU_AUTO_UNVE_ARTI, TX_VENCE_ARTI, TX_PARA_ARTI,
'TX_ENTRA_ARTI, CD_GRUP_ARTI, CD_USOS_ARTI, VL_ULCO_ARTI, VL_COPR_ARTI, CT_EXIS_ARTI, DE_OBSE_ARTI, ID_TIPO_ARTIID_TIPO_ARTI, CD_RIPS_ARTI, NU_INDPYP_ARTI
'FROM ARTICULO;

Public Property Let Codigo(Cue As String)
    cod = Cue
End Property

Public Property Get Codigo() As String
    Codigo = cod
End Property

Public Property Let NombreUndConteo(Cue As String)
    NmUnd = Cue
End Property

Public Property Get NombreUndConteo() As String
    NombreUndConteo = NmUnd
End Property

Public Property Let CodigoGrupo(Cue As String)
    CdGru = Cue
End Property

Public Property Get CodigoGrupo() As String
    CodigoGrupo = CdGru
End Property

Public Property Let NombreGrupo(Cue As String)
    NmGru = Cue
End Property

Public Property Get NombreGrupo() As String
    NombreGrupo = NmGru
End Property

Public Property Let CodigoUso(Cue As String)
    CdUso = Cue
End Property

Public Property Get CodigoUso() As String
    CodigoUso = CdUso
End Property

Public Property Let NombreUso(Cue As String)
    NmUso = Cue
End Property

Public Property Get NombreUso() As String
    NombreUso = NmUso
End Property

Public Property Let Nombre(Cue As String)
    nom = Cue
End Property

Public Property Get Nombre() As String
    Nombre = nom
End Property

Public Property Let ParaQueEs(num As Byte)
    If num < 2 Then EsPa = num
End Property

Public Property Get ParaQueEs() As Byte
    ParaQueEs = EsPa
End Property

Public Property Let AutoNumero(num As Long)
    AuNum = num
End Property

Public Property Get AutoNumero() As Long
    AutoNumero = AuNum
End Property

Public Property Let CostoPromedio(num As Double)
    VlCoPro = num
End Property

Public Property Get CostoPromedio() As Double
    CostoPromedio = VlCoPro
End Property

Public Property Let AutoUDConteo(num As Long)
    AuUnd = num
End Property

Public Property Get AutoUDConteo() As Long
    AutoUDConteo = AuUnd
End Property

Public Property Let Multiplicador(num As Integer)
    If Not num > 0 Then Exit Property
    mult = num
End Property

Public Property Get Multiplicador() As Integer
    Multiplicador = mult
End Property

Public Property Let Divisor(num As Integer)
    If Not num > 0 Then Exit Property
    divi = num
End Property

Public Property Get Divisor() As Integer
    Divisor = divi
End Property

Private Sub Class_Initialize()
    mult = 1
    divi = 1
End Sub
