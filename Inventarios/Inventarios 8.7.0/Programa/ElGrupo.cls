VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "ElGrupo"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Private vCodigo As String
Private vNombre As String
Private vDebito As String
Private vCredito As String
Private vCuenta As Byte
Private vAcumulaDB As Single, vAcumulaCR As Single
Private vEntradaCosto As Single, vEntradaVenta As Single
Private vSalidaCosto As Single, vSalidaVenta As Single
Private vVentasCosto As Single, vVentasVenta As Single
Private vInventaFinal As Single
Private vTieneInfo As Boolean

Public Property Let Nombre(Cue As String)
    vNombre = Cue
End Property

Public Property Get Nombre() As String
    Nombre = vNombre
End Property

Public Property Let Codigo(Cue As String)
    vCodigo = Cue
End Property

Public Property Get Codigo() As String
    Codigo = vCodigo
End Property

Public Property Let CtaDebito(Cue As String)
    vDebito = Cue
End Property

Public Property Get CtaDebito() As String
    CtaDebito = vDebito
End Property

Public Property Let CtaCredito(Cue As String)
    vCredito = Cue
End Property

Public Property Get CtaCredito() As String
    CtaCredito = vCredito
End Property

Public Property Let AcumulaDebito(Num As Single)
    vAcumulaDB = Num
End Property

Public Property Get AcumulaDebito() As Single
    AcumulaDebito = vAcumulaDB
End Property

Public Property Let AcumulaCredito(Num As Single)
    vAcumulaCR = Num
End Property

Public Property Get AcumulaCredito() As Single
    AcumulaCredito = vAcumulaCR
End Property

Public Property Let AcumulaEntradaCosto(Num As Single)
    vEntradaCosto = Num
End Property

Public Property Get AcumulaEntradaCosto() As Single
    AcumulaEntradaCosto = vEntradaCosto
End Property

Public Property Let AcumulaVentasCosto(Num As Single)
    vVentasCosto = Num
End Property

Public Property Get AcumulaVentasVenta() As Single
    AcumulaVentasVenta = vVentasVenta
End Property

Public Property Let AcumulaVentasVenta(Num As Single)
    vVentasVenta = Num
End Property

Public Property Get AcumulaVentasCosto() As Single
    AcumulaVentasCosto = vVentasCosto
End Property

Public Property Let AcumulaSalidaCosto(Num As Single)
    vSalidaCosto = Num
End Property

Public Property Get AcumulaSalidaCosto() As Single
    AcumulaSalidaCosto = vSalidaCosto
End Property

Public Property Let AcumulaEntradaVenta(Num As Single)
    vEntradaVenta = Num
End Property

Public Property Get AcumulaEntradaVenta() As Single
    AcumulaEntradaVenta = vEntradaVenta
End Property

Public Property Let InventaFinal(Num As Single)
    vInventaFinal = Num
End Property

Public Property Get InventaFinal() As Single
    InventaFinal = vInventaFinal
End Property

Public Property Let AcumulaSalidaVenta(Num As Single)
    vSalidaVenta = Num
End Property

Public Property Get AcumulaSalidaVenta() As Single
    AcumulaSalidaVenta = vSalida
End Property

Public Property Let cuenta(Num As Byte)
    vCuenta = Num
End Property

Public Property Get cuenta() As Byte
    cuenta = vCuenta
End Property

Public Property Let TieneInfo(bol As Boolean)
    vTieneInfo = bol
End Property

Public Property Get TieneInfo() As Boolean
    TieneInfo = vTieneInfo
End Property

