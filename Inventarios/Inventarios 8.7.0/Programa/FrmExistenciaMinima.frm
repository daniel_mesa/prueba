VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Object = "{0BA686C6-F7D3-101A-993E-0000C0EF6F5E}#1.0#0"; "THREED32.OCX"
Begin VB.Form FrmExistenciaMinima 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Informe de existencias m�nimas"
   ClientHeight    =   7770
   ClientLeft      =   45
   ClientTop       =   435
   ClientWidth     =   11415
   Icon            =   "FrmExistenciaMinima.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   7770
   ScaleWidth      =   11415
   Begin VB.CheckBox ChkTodos 
      Caption         =   "Seleccionar todos los articulos"
      Height          =   255
      Left            =   360
      TabIndex        =   2
      Top             =   960
      Width           =   2655
   End
   Begin VB.TextBox TxtPorTopeMin 
      Height          =   375
      Left            =   6840
      MaxLength       =   3
      TabIndex        =   1
      Text            =   "0"
      Top             =   360
      Width           =   1215
   End
   Begin VB.ComboBox cboTIPO 
      Height          =   315
      Left            =   1920
      Style           =   2  'Dropdown List
      TabIndex        =   0
      Top             =   360
      Width           =   2655
   End
   Begin MSComctlLib.ListView lstBODEGAS 
      Height          =   2295
      Left            =   360
      TabIndex        =   4
      Top             =   5040
      Width           =   9375
      _ExtentX        =   16536
      _ExtentY        =   4048
      View            =   3
      LabelEdit       =   1
      MultiSelect     =   -1  'True
      LabelWrap       =   -1  'True
      HideSelection   =   -1  'True
      FullRowSelect   =   -1  'True
      GridLines       =   -1  'True
      _Version        =   393217
      ForeColor       =   -2147483640
      BackColor       =   -2147483643
      BorderStyle     =   1
      Appearance      =   1
      NumItems        =   0
   End
   Begin MSComctlLib.ListView lstARTICULOS 
      Height          =   3375
      Left            =   360
      TabIndex        =   3
      Top             =   1200
      Width           =   10620
      _ExtentX        =   18733
      _ExtentY        =   5953
      View            =   3
      LabelEdit       =   1
      MultiSelect     =   -1  'True
      LabelWrap       =   -1  'True
      HideSelection   =   0   'False
      AllowReorder    =   -1  'True
      Checkboxes      =   -1  'True
      FullRowSelect   =   -1  'True
      GridLines       =   -1  'True
      _Version        =   393217
      ForeColor       =   -2147483640
      BackColor       =   -2147483643
      BorderStyle     =   1
      Appearance      =   1
      NumItems        =   2
      BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Text            =   "Nombre del Art�culo"
         Object.Width           =   7761
      EndProperty
      BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   1
         Text            =   "C�digo"
         Object.Width           =   3881
      EndProperty
   End
   Begin Threed.SSCommand cmdIMPRIMIR 
      Height          =   735
      Left            =   10080
      TabIndex        =   7
      Top             =   6600
      Width           =   855
      _Version        =   65536
      _ExtentX        =   1508
      _ExtentY        =   1296
      _StockProps     =   78
      Caption         =   "&IMPRIMIR"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   6
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      RoundedCorners  =   0   'False
      Picture         =   "FrmExistenciaMinima.frx":058A
   End
   Begin VB.Label Label2 
      Caption         =   "% x Encima de Tope m�nimo"
      Height          =   375
      Left            =   5640
      TabIndex        =   6
      Top             =   360
      Width           =   1095
   End
   Begin VB.Label txtTIPO 
      Caption         =   "Tipo de Art�culos:"
      Height          =   315
      Left            =   360
      TabIndex        =   5
      Top             =   360
      Width           =   1455
   End
End
Attribute VB_Name = "FrmExistenciaMinima"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'---------------------------------------------------------------------------------------
' Module    : FrmExistenciaMinima
' DateTime : 27/06/2008 09:43
' Author    :   diego_hernandez
' Purpose   : Generar el reporte de Existencias Minimas Requerimiento 1182
'---------------------------------------------------------------------------------------

Option Explicit
Dim NumEnCombo As Integer
'Dim CnTdr As Integer JAUM T29577 Se deja linea en comentario
Dim CnTdr As Double 'JAUM T29577 Contador
Dim Fec_Actual As Long
Dim ArrExis() As Variant
Dim ArrUXB() As Variant
Dim ArrArt() As Variant


Private Sub cboTIPO_GotFocus()
    NumEnCombo = Me.cboTIPO.ListIndex
End Sub

Private Sub CboTipo_KeyPress(KeyAscii As Integer)
   Call Cambiar_Enter(KeyAscii)
End Sub

'---------------------------------------------------------------------------------------
' Procedure : cboTIPO_LostFocus
' DateTime  : 01/07/2008 10:16
' Author    :   diego_hernandez
' Purpose   :  C�digo Copiado del Evento LostFocus del control CboTipo del Formulario FrmPrmREPOSAL
'---------------------------------------------------------------------------------------
'
Private Sub cboTIPO_LostFocus()
'Declaraci�n de Variables
Dim tipo As String * 1
Dim Articulo As New UnArticulo
    
If NumEnCombo = Me.cboTIPO.ListIndex Then Exit Sub

    'Limpia los controles ListView
    Me.lstBODEGAS.ListItems.Clear
    Me.lstARTICULOS.ListItems.Clear

    'Selecciona el tipo del articulo, si es de consumo o para la venta
    Select Case Me.cboTIPO.ListIndex
        Case Is = -1
            tipo = "0"
        Case Is = 0
            tipo = "V"
        Case Is = 1
            tipo = "C"
    End Select
    
    'Consulta la informaci�n de las bodegas de acuerdo al tipo de articulo seleccionado en el ComboBox
    Campos = "NU_AUTO_BODE, NU_AUTO_BODE_BODE, TX_CODI_BODE, TX_NOMB_BODE, CD_CODI_CECO_BODE, TX_VENTA_BODE"
    Desde = "IN_BODEGA"
    Condi = "TX_VENTA_BODE='" & tipo & "' ORDER BY TX_NOMB_BODE"
    ReDim ArrUXB(5, 0)
    Result = LoadMulData(Desde, Campos, Condi, ArrUXB())
    
    If Result = FAIL Then GoTo FALLO
    
    On Error GoTo SIGUI
    For CnTdr = 0 To UBound(ArrUXB, 2)
        Me.lstBODEGAS.ListItems.Add , "B" & ArrUXB(2, CnTdr), ArrUXB(3, CnTdr)
        Me.lstBODEGAS.ListItems("B" & ArrUXB(2, CnTdr)).ListSubItems.Add , "COD", ArrUXB(2, CnTdr)
        Me.lstBODEGAS.ListItems("B" & ArrUXB(2, CnTdr)).Tag = ArrUXB(0, CnTdr)
SIGUI:
    Next
    On Error GoTo 0
    Campos = "DISTINCT NU_AUTO_ARTI, CD_CODI_ARTI, NO_NOMB_ARTI, NU_AUTO_UNVE_ARTI, TX_PARA_ARTI, " & _
        "DE_DESC_GRUP, DE_DESC_USOS, TX_NOMB_UNVE, CD_GRUP_ARTI, CD_USOS_ARTI, " & _
        "NU_MULT_UNVE, NU_DIVI_UNVE"
    
    Desde = "ARTICULO, USOS, GRUP_ARTICULO, IN_UNDVENTA, IN_DETALLE"
    Condi = "CD_CODI_GRUP = CD_GRUP_ARTI AND CD_CODI_USOS = CD_USOS_ARTI"
    Condi = Condi & " AND NU_AUTO_UNVE = NU_AUTO_UNVE_ARTI"
    Condi = Condi & " AND NU_AUTO_ARTI = NU_AUTO_ARTI_DETA"
    Condi = Condi & " AND TX_PARA_ARTI='" & tipo & "' ORDER BY NO_NOMB_ARTI"
    ReDim ArrUXB(11, 0)
    Result = LoadMulData(Desde, Campos, Condi, ArrUXB())
    If Result = FAIL Then GoTo FALLO
    If Not Encontro Then GoTo NOENC

    For CnTdr = 0 To UBound(ArrUXB, 2)

        Articulo.AutoNumero = ArrUXB(0, CnTdr)
        Articulo.Codigo = ArrUXB(1, CnTdr)
        Articulo.Nombre = ArrUXB(2, CnTdr)
        Articulo.AutoUDConteo = ArrUXB(3, CnTdr)
        Articulo.NombreGrupo = ArrUXB(5, CnTdr)
        Articulo.NombreUso = ArrUXB(6, CnTdr)
        Articulo.NombreUndConteo = ArrUXB(7, CnTdr)
        Articulo.ParaQueEs = InStr(1, "VC", ArrUXB(4, CnTdr)) - 1
        Articulo.CodigoGrupo = ArrUXB(8, CnTdr)
        Articulo.CodigoUso = ArrUXB(9, CnTdr)
        Me.lstARTICULOS.ListItems.Add , "A" & Articulo.AutoNumero, Trim(Articulo.Nombre)
        Me.lstARTICULOS.ListItems("A" & Articulo.AutoNumero).Tag = CStr(Articulo.AutoNumero)
        Me.lstARTICULOS.ListItems("A" & Articulo.AutoNumero).ListSubItems.Add , "COAR", Trim(Articulo.Codigo)
        Me.lstARTICULOS.ListItems("A" & Articulo.AutoNumero).ListSubItems.Add , "NOCO", Trim(Articulo.NombreUndConteo)
        Me.lstARTICULOS.ListItems("A" & Articulo.AutoNumero).ListSubItems.Add , "NOUS", Trim(Articulo.NombreUso)
        Me.lstARTICULOS.ListItems("A" & Articulo.AutoNumero).ListSubItems.Add , "NOGR", Trim(Articulo.NombreGrupo)
        Me.lstARTICULOS.ListItems("A" & Articulo.AutoNumero).ListSubItems.Add , "AUCO", Trim(Articulo.AutoUDConteo)
        Me.lstARTICULOS.ListItems("A" & Articulo.AutoNumero).ListSubItems.Add , "COGR", Trim(Articulo.CodigoGrupo)
        Me.lstARTICULOS.ListItems("A" & Articulo.AutoNumero).ListSubItems.Add , "COUS", Trim(Articulo.CodigoUso)
    Next
    Set Articulo = Nothing

NOENC:
FALLO:


End Sub





Private Sub chkFCRANGO_KeyPress(KeyAscii As Integer)
  Call Cambiar_Enter(KeyAscii)
End Sub

Private Sub ChkTodos_Click()
'  If Val(TxtPorTopeMin) > 0 Then
'     ChkTodos.Value = 1
'  End If
End Sub

Private Sub ChkTodos_KeyPress(KeyAscii As Integer)
  Call Cambiar_Enter(KeyAscii)
End Sub

'---------------------------------------------------------------------------------------
' Procedure : CmdImprimir_Click
' DateTime  : 27/06/2008 09:46
' Author    : diego_hernandez
' Purpose   : Imprime el Reporte de Existencias Minimas
' Primero Calcula las existencias de los articulos seleccionados
' Segundo consulta topes minimos y maximos de los articulos seleccionados
' Tercero inserta en la tabla de Existencias M�nimas (IN_EXISTENCIA_MINIMA)
'---------------------------------------------------------------------------------------
'
Private Sub CmdImprimir_Click()
    
    'Declaraci�n de Variables
    Dim strARTICULOS As String 'Variable para guardar los articulos seleccionados en la lista.
    Dim strBODEGAS As String 'Variable para guardar las bodegas seleccionados en la lista.
    Dim Renglon As MSComctlLib.ListItem
    Dim stCmd As String
    Dim NroRegs As Long
    Dim Incont As Integer ' Variable que se utilizara para recorrer los arreglos
    Dim CntMinima As Single
    Dim CntReorden As Single
    Dim CntMaxima As Single, CntMeses As Single, CntPromedio As Single
    Dim CntPedido As Single, porExist As Single
    Dim StListado As String 'DAHV M3844 Variable que almacenara los c�digos de los articulos que se seleccionaron
    
    If Me.cboTIPO.ListIndex = -1 Then Exit Sub
   
    Call Limpiar_CrysListar
    
    Desde = "IN_EXISTENCIA_MINIMA"
    Condicion = "NU_AUTO_USUA_EXMI =" & AutonumUser
    'Result = DoDelete(Desde, Condicion) DAHV M3795
    Result = DoDelete(Desde, Condicion, "N")
    
    'Si NO se encuentra seleccionada la opci�n de todos los articulos, se guardan en la variable  StrArticulos los art�culos seleccionados
    If Me.ChkTodos.Value = 0 Then
        For Each Renglon In Me.lstARTICULOS.ListItems
            If Renglon.Selected Then strARTICULOS = strARTICULOS & IIf(Len(strARTICULOS) > 0, " OR ", "") & "NU_AUTO_ARTI_KARD=" & Renglon.Tag
        Next
    End If
    
    'DAHV M3600
    If Me.ChkTodos.Value = 1 Then
        
        For Each Renglon In Me.lstARTICULOS.ListItems
            'strARTICULOS = strARTICULOS & IIf(Len(strARTICULOS) > 0, " OR ", "") & "NU_AUTO_ARTI_KARD=" & Renglon.Tag
            'DAHV M3844 Se hace el cambio debido a que access no soporta la comparaci�n OR demasiadas veces.
            StListado = StListado & Renglon.Tag & ","
        Next
        StListado = Mid$(StListado, 1, Len(StListado) - 1) 'DAHV M3844 Se elimina la �ltima coma adicionada en el ciclo
        strARTICULOS = "NU_AUTO_ARTI_KARD IN (" & StListado & ")" 'DAHV Se estructura el reng�n para el perefecto funcionamiento en la sentencia
    End If
    
    
    
    For Each Renglon In Me.lstBODEGAS.ListItems
        If Renglon.Selected Then strBODEGAS = strBODEGAS & IIf(Len(strBODEGAS) > 0, " OR ", "") & "NU_AUTO_BODE_KARD=" & Renglon.Tag
    Next
    
    If Len(strARTICULOS) > 0 Then strARTICULOS = "(" & strARTICULOS & ")"
    If Len(strBODEGAS) > 0 Then strBODEGAS = "(" & strBODEGAS & ")"
    
    If Len(strARTICULOS) > 0 And Len(strBODEGAS) > 0 Then
        strARTICULOS = strARTICULOS & " AND " & strBODEGAS
    Else
        strARTICULOS = strARTICULOS & strBODEGAS
    End If


    'Consulta para determinar las existencias del articulo
    
    stCmd = "(SELECT NU_AUTO_ARTI_KARD,CD_CODI_ARTI,TX_NOMB_BODE, NO_NOMB_ARTI,TX_NOMB_UNVE,NU_AUTO_BODE_KARD,"
    'stCmd = stCmd & "SUM(NU_ENTRAD_KARD*NU_MULT_KARD/NU_DIVI_KARD) AS ENTRADAS,"
    'stCmd = stCmd & "SUM(NU_SALIDA_KARD*NU_MULT_KARD/NU_DIVI_KARD) AS SALIDAS"
    stCmd = stCmd & "SUM(CAST(NU_ENTRAD_KARD AS REAL)* CAST(NU_MULT_KARD AS REAL)/CAST(NU_DIVI_KARD AS REAL)) AS ENTRADAS," 'CARV M4160
    stCmd = stCmd & "SUM(CAST(NU_SALIDA_KARD AS REAL)*CAST(NU_MULT_KARD AS REAL)/CAST(NU_DIVI_KARD AS REAL)) AS SALIDAS" 'CARV M4160
    stCmd = stCmd & " FROM IN_KARDEX, ARTICULO, IN_BODEGA, IN_UNDVENTA "
    stCmd = stCmd & " WHERE " & strARTICULOS
    stCmd = stCmd & " AND NU_AUTO_ARTI_KARD=NU_AUTO_ARTI"
    stCmd = stCmd & " AND NU_AUTO_BODE_KARD=NU_AUTO_BODE "
    stCmd = stCmd & " AND NU_AUTO_UNVE_ARTI=NU_AUTO_UNVE "
    stCmd = stCmd & " GROUP BY NU_AUTO_ARTI_KARD,NO_NOMB_ARTI,TX_NOMB_BODE,TX_NOMB_UNVE,CD_CODI_ARTI,NU_AUTO_BODE_KARD) AS T1"
     
    Campos = "NU_AUTO_ARTI_KARD,CD_CODI_ARTI,NO_NOMB_ARTI,NU_AUTO_BODE_KARD,TX_NOMB_BODE, (ENTRADAS - SALIDAS) AS EXISTENCIAS"
    Campos = Campos & ", '' AS NU_STMIN_EXMI, '' AS NU_STMAX_EXMI,'' AS NU_AUTO_USUARIO,'' AS NU_PORC_EXMI,'' AS NU_PEDIDO_EXMI,'' AS NU_STREPO_EXMI"
    Desde = stCmd
    
    ReDim ArrExis(12, 0)
    Result = LoadMulData(Desde, Campos, NUL$, ArrExis())
    If Result <> FAIL Then
       If Encontro Then
          For Incont = 0 To UBound(ArrExis(), 2)

           
            'DAHV R1182 OE
            Campos = "NU_STMIN_LIBA, NU_STMAX_LIBA,NU_REPOS_LIBA"
            Desde = "IN_LIMITES_BODE_ARTI"
            Condicion = "NU_AUTO_ARTI_LIBA=" & ArrExis(0, Incont)
            Condicion = Condicion & " AND NU_AUTO_BODE_LIBA =" & ArrExis(3, Incont)

            ReDim ArrArt(2)
            Result = LoadData(Desde, Campos, Condicion, ArrArt())

            If Result <> FAIL Then
               If Encontro Then
                    ArrExis(6, Incont) = CLng(ArrArt(0))
                    ArrExis(7, Incont) = CLng(ArrArt(1))
                    ArrExis(12, Incont) = CLng(ArrArt(2))
                    
               Else
                    ArrExis(6, Incont) = CLng(0)
                    ArrExis(7, Incont) = CLng(0)
                    ArrExis(12, Incont) = CLng(0)
               
               End If
            End If

             ArrExis(8, Incont) = AutonumUser
             
             CntPedido = 0 'DAHV M3600
             If ArrExis(6, Incont) = "" Then ArrExis(6, Incont) = CLng(0)
             If ArrExis(7, Incont) = "" Then ArrExis(7, Incont) = CLng(0)
             ArrExis(8, Incont) = AutonumUser
             
            
             porExist = CLng(0)

             
             'Calculo de las cantidades m�nimas a pedir
             If ((TxtPorTopeMin <> NUL$) And (TxtPorTopeMin <> "0")) Then
                    'DAHV R1182 OE
                    'CntPedido = (ArrExis(6, Incont) + (ArrExis(6, Incont) * TxtPorTopeMin / 100)) - ArrExis(5, Incont) 'HRR
                    CntPedido = (((ArrExis(12, Incont) + ArrExis(6, Incont)) * TxtPorTopeMin) / 100) - ArrExis(5, Incont) 'HRR
                    If CLng(CntPedido) > 0 Then
                         ArrExis(10, Incont) = CLng(CntPedido)
                    Else
                        ArrExis(10, Incont) = 0
                    End If

             Else
                    
                    'If CDbl(ArrExis(5, Incont)) < CDbl((ArrExis(12, Incont))) Then 'DAHV M3716
                    If CDbl(ArrExis(5, Incont)) < CDbl(ArrExis(6, Incont)) + CDbl((ArrExis(12, Incont))) Then
                        ArrExis(10, Incont) = CLng((ArrExis(12, Incont)) - ArrExis(5, Incont)) + ArrExis(6, Incont)
                    Else
                        ArrExis(10, Incont) = 0
                    End If

             End If
             
              
             'If ((ArrExis(5, Incont) - ArrExis(6, Incont)) <= 0) Then
                
                    Valores = "TX_CODI_ARTI_EXMI=" & Comi & CStr(ArrExis(1, Incont)) & Comi
                    Valores = Valores & ",TX_NOMB_ARTI_EXMI =" & Comi & Cambiar_Comas_Comillas(CStr(ArrExis(2, Incont))) & Comi
                    Valores = Valores & ",TX_CODI_BODE_EXMI = " & Comi & CStr(ArrExis(3, Incont)) & Comi
                    Valores = Valores & ",TX_NOMB_BODE_EXMI = " & Comi & Cambiar_Comas_Comillas(CStr(ArrExis(4, Incont))) & Comi
                    Valores = Valores & ",NU_STMIN_EXMI=" & ArrExis(6, Incont)
                    Valores = Valores & ",NU_STMAX_EXMI = " & ArrExis(7, Incont)
                    Valores = Valores & ",NU_EXISTE_EXMI=" & ArrExis(5, Incont)
                    Valores = Valores & ",NU_AUTO_USUA_EXMI =" & ArrExis(8, Incont)
                    Valores = Valores & ",NU_PORC_EXMI =" & porExist
                    Valores = Valores & ",NU_PEDIDO_EXMI=" & ArrExis(10, Incont)
                    Valores = Valores & ",NU_STREPO_EXMI=" & ArrExis(12, Incont)
                    Result = DoInsertSQL("IN_EXISTENCIA_MINIMA", Valores, "N")
                    'Result = DoInsertSQL("IN_EXISTENCIA_MINIMA", Valores) DAHV M3795
                
'             ElseIf ((ArrExis(10, Incont) > 0)) Then 'DAHV M3716
'
'                   Valores = "TX_CODI_ARTI_EXMI=" & Comi & CStr(ArrExis(1, Incont)) & Comi
'                   Valores = Valores & ",TX_NOMB_ARTI_EXMI =" & Comi & Cambiar_Comas_Comillas(CStr(ArrExis(2, Incont))) & Comi
'                   Valores = Valores & ",TX_CODI_BODE_EXMI = " & Comi & CStr(ArrExis(3, Incont)) & Comi
'                   Valores = Valores & ",TX_NOMB_BODE_EXMI = " & Comi & Cambiar_Comas_Comillas(CStr(ArrExis(4, Incont))) & Comi
'                   Valores = Valores & ",NU_STMIN_EXMI=" & ArrExis(6, Incont)
'                   Valores = Valores & ",NU_STMAX_EXMI = " & ArrExis(7, Incont)
'                   Valores = Valores & ",NU_EXISTE_EXMI=" & ArrExis(5, Incont)
'                   Valores = Valores & ",NU_AUTO_USUA_EXMI =" & ArrExis(8, Incont)
'                   Valores = Valores & ",NU_PORC_EXMI =" & porExist
'                   Valores = Valores & ",NU_PEDIDO_EXMI=" & ArrExis(10, Incont)
'                   Valores = Valores & ",NU_STREPO_EXMI=" & ArrExis(12, Incont)
'                   Result = DoInsertSQL("IN_EXISTENCIA_MINIMA", Valores, "N")
'
'             ElseIf ((ArrExis(5, Incont) > 0) And ((ArrExis(6, Incont) = 0) Or (ArrExis(7, Incont) = 0))) Then 'DAHV M3793
'
'                   Valores = "TX_CODI_ARTI_EXMI=" & Comi & CStr(ArrExis(1, Incont)) & Comi
'                   Valores = Valores & ",TX_NOMB_ARTI_EXMI =" & Comi & Cambiar_Comas_Comillas(CStr(ArrExis(2, Incont))) & Comi
'                   Valores = Valores & ",TX_CODI_BODE_EXMI = " & Comi & CStr(ArrExis(3, Incont)) & Comi
'                   Valores = Valores & ",TX_NOMB_BODE_EXMI = " & Comi & Cambiar_Comas_Comillas(CStr(ArrExis(4, Incont))) & Comi
'                   Valores = Valores & ",NU_STMIN_EXMI=" & ArrExis(6, Incont)
'                   Valores = Valores & ",NU_STMAX_EXMI = " & ArrExis(7, Incont)
'                   Valores = Valores & ",NU_EXISTE_EXMI=" & ArrExis(5, Incont)
'                   Valores = Valores & ",NU_AUTO_USUA_EXMI =" & ArrExis(8, Incont)
'                   Valores = Valores & ",NU_PORC_EXMI =" & porExist
'                   Valores = Valores & ",NU_PEDIDO_EXMI=" & ArrExis(10, Incont)
'                   Valores = Valores & ",NU_STREPO_EXMI=" & ArrExis(12, Incont)
'                   Result = DoInsertSQL("IN_EXISTENCIA_MINIMA", Valores, "N")
'
'             End If
             
          Next
       End If
    
    End If

   'Call ListarD("InfExminimas.rpt", NUL$, 0, "Existencias M�nimas", MDI_Inventarios, False)
   'Call ListarD("InfExminimas.rpt", NUL$, 0, "Generador de Pedidos", MDI_Inventarios, False) 'DAHV M3792
   Call ListarD("InfExminimas.rpt", "{IN_EXISTENCIA_MINIMA.NU_AUTO_USUA_EXMI} =" & AutonumUser, 0, "Generador de Pedidos", MDI_Inventarios, False) 'JACC M6708
   
   
   
End Sub


'---------------------------------------------------------------------------------------
' Procedure : Form_Load
' DateTime  : 27/06/2008 09:44
' Author    : diego_hernandez
' Purpose   :  Procedimiento para inicializar los controles del formulario
'---------------------------------------------------------------------------------------
'
Private Sub Form_Load()
    
    Call CenterForm(MDI_Inventarios, Me)
    Call Leer_Permiso(Me.Name, cmdIMPRIMIR, "I")
    Me.cboTIPO.Clear
    Me.cboTIPO.AddItem "Para la Venta"
    Me.cboTIPO.AddItem "Para El Consumo"

    'DAHV R1182 OE
'    Fec_Actual = Date
'    Me.txtFRACCION.Mask = "#.##" 'DAHV M3600
'    Me.DTPFecHasta.Value = DateAdd("d", -Day(Fec_Actual), Fec_Actual)
'    Me.DTPFecDesde.Value = DateAdd("m", -6, Me.DTPFecHasta.Value)
'    Me.DTPFecDesde.Value = DateAdd("d", 1, Me.DTPFecDesde.Value)
        
'    Me.chkFCRANGO.Value = False
'    Me.DTPFecHasta.Enabled = False
'    Me.DTPFecDesde.Enabled = False
'    Me.txtFRACCION.Enabled = False
        
    Me.ChkTodos.Value = 1
    Me.lstBODEGAS.ListItems.Clear
    Me.lstBODEGAS.Checkboxes = False
    Me.lstBODEGAS.MultiSelect = True
    Me.lstBODEGAS.HideSelection = False
    Me.lstBODEGAS.ColumnHeaders.Clear
    Me.lstBODEGAS.ColumnHeaders.Add , "NOM", "Nombre de la Bodega", 0.66 * Me.lstBODEGAS.Width
    Me.lstBODEGAS.ColumnHeaders.Add , "COD", "C�digo", 0.34 * Me.lstBODEGAS.Width
    
    Me.lstARTICULOS.ListItems.Clear
    Me.lstARTICULOS.Checkboxes = False
    Me.lstARTICULOS.MultiSelect = True
    Me.lstARTICULOS.HideSelection = False
    Me.lstARTICULOS.Width = 10500
    Me.lstARTICULOS.ColumnHeaders.Clear
    Me.lstARTICULOS.ColumnHeaders.Add , "NOAR", "Nombre del art�culo", 0.4 * Me.lstARTICULOS.Width
    Me.lstARTICULOS.ColumnHeaders.Add , "COAR", "C�digo", 0.15 * Me.lstARTICULOS.Width
    Me.lstARTICULOS.ColumnHeaders.Add , "NOCO", "Unidad de conteo", 0.15 * Me.lstARTICULOS.Width
    Me.lstARTICULOS.ColumnHeaders.Add , "NOUS", "Uso", 0.15 * Me.lstARTICULOS.Width
    Me.lstARTICULOS.ColumnHeaders.Add , "NOGR", "Grupo", 0.15 * Me.lstARTICULOS.Width
    Me.lstARTICULOS.ColumnHeaders.Add , "AUCO", "AutoUNC", 0
    Me.lstARTICULOS.ColumnHeaders.Add , "COGR", "CodGRU", 0
    Me.lstARTICULOS.ColumnHeaders.Add , "COUS", "CodUSO", 0
  
    
FALLO:

End Sub

Private Sub txtFRACCION_KeyPress(KeyAscii As Integer)
 Call Cambiar_Enter(KeyAscii)
End Sub

'---------------------------------------------------------------------------------------
' Procedure : lstARTICULOS_ColumnClick
' DateTime  : 31/07/2008 10:14
' Author    : diego_hernandez
' Purpose   : Ordenar segun la columna seleccionada por el usuarioM3796
'---------------------------------------------------------------------------------------
'
Private Sub lstARTICULOS_ColumnClick(ByVal ColumnHeader As MSComctlLib.ColumnHeader)
    Me.lstARTICULOS.SortKey = ColumnHeader.Index - 1
    Me.lstARTICULOS.Sorted = True
End Sub

Private Sub TxtPorTopeMin_KeyPress(KeyAscii As Integer)
  Call ValKeyNum(KeyAscii)
  Call Cambiar_Enter(KeyAscii)
End Sub

Private Sub TxtPorTopeMin_LostFocus()
   If Val(TxtPorTopeMin) > 100 Then
      Call Mensaje1("El porcentaje ingresado no es valido", 3)
      TxtPorTopeMin = "100"
  End If
  
'  If Val(TxtPorTopeMin) > 0 Then
'     ChkTodos.Value = 1
'  End If
  
End Sub
