VERSION 5.00
Object = "{0BA686C6-F7D3-101A-993E-0000C0EF6F5E}#1.0#0"; "THREED32.OCX"
Begin VB.Form FrmCuGE 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Cuentas Contables Generales"
   ClientHeight    =   5265
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   7035
   Icon            =   "FrmCuGe.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   ScaleHeight     =   5265
   ScaleWidth      =   7035
   Begin VB.TextBox Txt_Cuenta 
      Height          =   285
      Index           =   8
      Left            =   1680
      MaxLength       =   15
      TabIndex        =   15
      Top             =   3960
      Width           =   1455
   End
   Begin VB.TextBox Txt_Cuenta 
      Height          =   285
      Index           =   7
      Left            =   1680
      MaxLength       =   15
      TabIndex        =   14
      Top             =   3480
      Width           =   1455
   End
   Begin VB.TextBox Txt_Cuenta 
      Height          =   285
      Index           =   6
      Left            =   1680
      MaxLength       =   15
      TabIndex        =   13
      Top             =   3000
      Width           =   1455
   End
   Begin VB.TextBox Txt_Cuenta 
      Height          =   285
      Index           =   5
      Left            =   1680
      MaxLength       =   15
      TabIndex        =   11
      Top             =   2520
      Width           =   1455
   End
   Begin VB.TextBox Txt_Cuenta 
      Height          =   285
      Index           =   3
      Left            =   1680
      MaxLength       =   15
      TabIndex        =   7
      Top             =   1560
      Width           =   1455
   End
   Begin VB.TextBox Txt_Cuenta 
      Height          =   285
      Index           =   4
      Left            =   1680
      MaxLength       =   15
      TabIndex        =   9
      Top             =   2040
      Width           =   1455
   End
   Begin VB.TextBox Txt_Cuenta 
      Height          =   285
      Index           =   0
      Left            =   1680
      MaxLength       =   15
      TabIndex        =   1
      Top             =   120
      Width           =   1455
   End
   Begin VB.TextBox Txt_Cuenta 
      Height          =   285
      Index           =   1
      Left            =   1680
      MaxLength       =   15
      TabIndex        =   3
      Top             =   600
      Width           =   1455
   End
   Begin VB.TextBox Txt_Cuenta 
      Height          =   285
      Index           =   2
      Left            =   1680
      MaxLength       =   15
      TabIndex        =   5
      Top             =   1080
      Width           =   1455
   End
   Begin Threed.SSCommand CmdSelec 
      Height          =   375
      Index           =   0
      Left            =   3120
      TabIndex        =   16
      ToolTipText     =   "Selecci�n de Cuenta"
      Top             =   120
      Width           =   615
      _Version        =   65536
      _ExtentX        =   1085
      _ExtentY        =   661
      _StockProps     =   78
      BevelWidth      =   0
      RoundedCorners  =   0   'False
      Outline         =   0   'False
      Picture         =   "FrmCuGe.frx":058A
   End
   Begin Threed.SSCommand CmdSelec 
      Height          =   375
      Index           =   1
      Left            =   3120
      TabIndex        =   17
      ToolTipText     =   "Selecci�n de Cuenta"
      Top             =   600
      Width           =   615
      _Version        =   65536
      _ExtentX        =   1085
      _ExtentY        =   661
      _StockProps     =   78
      BevelWidth      =   0
      RoundedCorners  =   0   'False
      Outline         =   0   'False
      Picture         =   "FrmCuGe.frx":0F3C
   End
   Begin Threed.SSCommand CmdSelec 
      Height          =   375
      Index           =   2
      Left            =   3120
      TabIndex        =   18
      ToolTipText     =   "Selecci�n de Cuenta"
      Top             =   1080
      Width           =   615
      _Version        =   65536
      _ExtentX        =   1085
      _ExtentY        =   661
      _StockProps     =   78
      BevelWidth      =   0
      RoundedCorners  =   0   'False
      Outline         =   0   'False
      Picture         =   "FrmCuGe.frx":18EE
   End
   Begin Threed.SSCommand CmdSelec 
      Height          =   375
      Index           =   3
      Left            =   3120
      TabIndex        =   22
      ToolTipText     =   "Selecci�n de Cuenta"
      Top             =   1560
      Width           =   615
      _Version        =   65536
      _ExtentX        =   1085
      _ExtentY        =   661
      _StockProps     =   78
      BevelWidth      =   0
      RoundedCorners  =   0   'False
      Outline         =   0   'False
      Picture         =   "FrmCuGe.frx":22A0
   End
   Begin Threed.SSCommand CmdSelec 
      Height          =   375
      Index           =   4
      Left            =   3120
      TabIndex        =   23
      ToolTipText     =   "Selecci�n de Cuenta"
      Top             =   2040
      Width           =   615
      _Version        =   65536
      _ExtentX        =   1085
      _ExtentY        =   661
      _StockProps     =   78
      BevelWidth      =   0
      RoundedCorners  =   0   'False
      Outline         =   0   'False
      Picture         =   "FrmCuGe.frx":2C52
   End
   Begin Threed.SSCommand CmdSelec 
      Height          =   375
      Index           =   5
      Left            =   3120
      TabIndex        =   26
      ToolTipText     =   "Selecci�n de Cuenta"
      Top             =   2520
      Width           =   615
      _Version        =   65536
      _ExtentX        =   1085
      _ExtentY        =   661
      _StockProps     =   78
      BevelWidth      =   0
      RoundedCorners  =   0   'False
      Outline         =   0   'False
      Picture         =   "FrmCuGe.frx":3604
   End
   Begin Threed.SSCommand CmdSelec 
      Height          =   375
      Index           =   6
      Left            =   3120
      TabIndex        =   27
      ToolTipText     =   "Selecci�n de Cuenta"
      Top             =   3000
      Width           =   615
      _Version        =   65536
      _ExtentX        =   1085
      _ExtentY        =   661
      _StockProps     =   78
      BevelWidth      =   0
      RoundedCorners  =   0   'False
      Outline         =   0   'False
      Picture         =   "FrmCuGe.frx":3FB6
   End
   Begin Threed.SSCommand Cmd_Botones 
      Height          =   675
      HelpContextID   =   120
      Index           =   1
      Left            =   3720
      TabIndex        =   30
      ToolTipText     =   "Salir"
      Top             =   4440
      Width           =   1095
      _Version        =   65536
      _ExtentX        =   1931
      _ExtentY        =   1191
      _StockProps     =   78
      Caption         =   "&SALIR"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   6
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Picture         =   "FrmCuGe.frx":4968
   End
   Begin Threed.SSCommand Cmd_Botones 
      Height          =   675
      HelpContextID   =   120
      Index           =   0
      Left            =   2160
      TabIndex        =   31
      ToolTipText     =   "Guardar"
      Top             =   4440
      Width           =   1095
      _Version        =   65536
      _ExtentX        =   1931
      _ExtentY        =   1191
      _StockProps     =   78
      Caption         =   "&GUARDAR"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   6
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Picture         =   "FrmCuGe.frx":5032
   End
   Begin Threed.SSCommand CmdSelec 
      Height          =   375
      Index           =   7
      Left            =   3120
      TabIndex        =   34
      ToolTipText     =   "Selecci�n de Cuenta"
      Top             =   3480
      Width           =   615
      _Version        =   65536
      _ExtentX        =   1085
      _ExtentY        =   661
      _StockProps     =   78
      BevelWidth      =   0
      RoundedCorners  =   0   'False
      Outline         =   0   'False
      Picture         =   "FrmCuGe.frx":56FC
   End
   Begin Threed.SSCommand CmdSelec 
      Height          =   375
      Index           =   8
      Left            =   3120
      TabIndex        =   35
      ToolTipText     =   "Selecci�n de Cuenta"
      Top             =   3960
      Width           =   615
      _Version        =   65536
      _ExtentX        =   1085
      _ExtentY        =   661
      _StockProps     =   78
      BevelWidth      =   0
      RoundedCorners  =   0   'False
      Outline         =   0   'False
      Picture         =   "FrmCuGe.frx":60AE
   End
   Begin VB.Image imgSave 
      Height          =   480
      Left            =   1440
      Picture         =   "FrmCuGe.frx":6A60
      Top             =   4560
      Visible         =   0   'False
      Width           =   480
   End
   Begin VB.Label lbl_cuenta 
      BackColor       =   &H8000000E&
      BorderStyle     =   1  'Fixed Single
      Height          =   285
      Index           =   8
      Left            =   3720
      TabIndex        =   37
      Top             =   3960
      Width           =   3135
   End
   Begin VB.Label Label1 
      Caption         =   "Cuenta para CxP transitoria"
      Height          =   375
      Index           =   8
      Left            =   120
      TabIndex        =   36
      Top             =   3960
      Width           =   1455
   End
   Begin VB.Label Label1 
      Caption         =   "Cuenta para Donaciones"
      Height          =   375
      Index           =   4
      Left            =   120
      TabIndex        =   33
      Top             =   3360
      Width           =   1455
   End
   Begin VB.Label lbl_cuenta 
      BackColor       =   &H8000000E&
      BorderStyle     =   1  'Fixed Single
      Height          =   285
      Index           =   7
      Left            =   3720
      TabIndex        =   32
      Top             =   3480
      Width           =   3135
   End
   Begin VB.Label lbl_cuenta 
      BackColor       =   &H8000000E&
      BorderStyle     =   1  'Fixed Single
      Height          =   285
      Index           =   6
      Left            =   3720
      TabIndex        =   29
      Top             =   3000
      Width           =   3135
   End
   Begin VB.Label lbl_cuenta 
      BackColor       =   &H8000000E&
      BorderStyle     =   1  'Fixed Single
      Height          =   285
      Index           =   5
      Left            =   3720
      TabIndex        =   28
      Top             =   2520
      Width           =   3135
   End
   Begin VB.Label Label1 
      Caption         =   "Cuenta Descuentos Adicionales"
      Height          =   375
      Index           =   3
      Left            =   120
      TabIndex        =   10
      Top             =   2520
      Width           =   1455
   End
   Begin VB.Label Label1 
      Caption         =   "Cuenta para Fletes"
      Height          =   255
      Index           =   2
      Left            =   120
      TabIndex        =   12
      Top             =   3000
      Width           =   1455
   End
   Begin VB.Label lbl_cuenta 
      BackColor       =   &H8000000E&
      BorderStyle     =   1  'Fixed Single
      Height          =   285
      Index           =   3
      Left            =   3720
      TabIndex        =   25
      Top             =   1560
      Width           =   3135
   End
   Begin VB.Label lbl_cuenta 
      BackColor       =   &H8000000E&
      BorderStyle     =   1  'Fixed Single
      Height          =   285
      Index           =   4
      Left            =   3720
      TabIndex        =   24
      Top             =   2040
      Width           =   3135
   End
   Begin VB.Label Label1 
      Caption         =   "Cuenta Descuentos por Compras"
      Height          =   495
      Index           =   6
      Left            =   120
      TabIndex        =   6
      Top             =   1560
      Width           =   1455
   End
   Begin VB.Label Label1 
      Caption         =   "Cuenta Descuentos por Ventas"
      Height          =   375
      Index           =   7
      Left            =   120
      TabIndex        =   8
      Top             =   2040
      Width           =   1455
   End
   Begin VB.Label Label1 
      Caption         =   "Cuenta para CXC"
      Height          =   255
      Index           =   0
      Left            =   120
      TabIndex        =   0
      Top             =   120
      Width           =   1455
   End
   Begin VB.Label lbl_cuenta 
      BackColor       =   &H8000000E&
      BorderStyle     =   1  'Fixed Single
      Height          =   285
      Index           =   0
      Left            =   3720
      TabIndex        =   21
      Top             =   120
      Width           =   3135
   End
   Begin VB.Label Label1 
      Caption         =   "Cuenta para CXP"
      Height          =   255
      Index           =   1
      Left            =   120
      TabIndex        =   2
      Top             =   600
      Width           =   1455
   End
   Begin VB.Label lbl_cuenta 
      BackColor       =   &H8000000E&
      BorderStyle     =   1  'Fixed Single
      Height          =   285
      Index           =   1
      Left            =   3720
      TabIndex        =   20
      Top             =   600
      Width           =   3135
   End
   Begin VB.Label lbl_cuenta 
      BackColor       =   &H8000000E&
      BorderStyle     =   1  'Fixed Single
      Height          =   285
      Index           =   2
      Left            =   3720
      TabIndex        =   19
      Top             =   1080
      Width           =   3135
   End
   Begin VB.Label Label1 
      Caption         =   "Cuenta para Caja"
      Height          =   255
      Index           =   5
      Left            =   120
      TabIndex        =   4
      Top             =   1080
      Width           =   1455
   End
End
Attribute VB_Name = "FrmCuGE"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Dim Enc As Boolean

Private Sub Cmd_Botones_Click(Index As Integer)
    Select Case Index
        Case 0: Call Grabar
        Case 1: Unload Me
    End Select
End Sub

Private Sub CmdSelec_Click(Index As Integer)
   Codigo = NUL$
   'JLPB T29905/R28918 INICIO Se deja en comentario la siguiente linea
   'Codigo = Seleccion("CUENTAS", "NO_NOMB_CUEN", "CD_CODI_CUEN,NO_NOMB_CUEN", "CUENTAS CONTABLES", NUL$)
   Condicion = "TX_PUCNIIF_CUEN<>'1'"
   Codigo = Seleccion("CUENTAS", "NO_NOMB_CUEN", "CD_CODI_CUEN,NO_NOMB_CUEN", "CUENTAS CONTABLES", Condicion)
   'JLPB T29905/R28918 FIN
   If Codigo <> NUL$ Then Txt_Cuenta(Index) = Codigo:                                                                                                                                                                                          imgSave.Visible = False
   Txt_Cuenta_LostFocus (Index)
   Txt_Cuenta(Index).SetFocus
End Sub

Private Sub Form_Load()
    Call CenterForm(MDI_Inventarios, Me)
    Call Cargar_Datos
End Sub

Private Sub Txt_Cuenta_GotFocus(Index As Integer)
   Txt_Cuenta(Index).SelStart = 0
   Txt_Cuenta(Index).SelLength = Len(Txt_Cuenta(Index))
End Sub

Private Sub Txt_Cuenta_KeyPress(Index As Integer, KeyAscii As Integer)
    Call Cambiar_Enter(KeyAscii)
End Sub

Private Sub Txt_Cuenta_LostFocus(Index As Integer)
   Txt_Cuenta(Index) = Trim(Txt_Cuenta(Index))
   If Txt_Cuenta(Index) = NUL$ Then
      lbl_cuenta(Index) = NUL$
   Else
      Call Buscar_Cuenta(Index)
   End If
End Sub

Private Sub Buscar_Cuenta(Index As Integer)
   ReDim arr(0)
   If Txt_Cuenta(Index) = NUL$ Then lbl_cuenta(Index) = NUL$: Exit Sub
   'JLPB T29905/R28918 INICIO Se deja en comentario la siguiente linea
   'Result = LoadData("CUENTAS", "NO_NOMB_CUEN", "CD_CODI_CUEN=" & Comi & Txt_Cuenta(Index) & Comi, arr())
   Condicion = "CD_CODI_CUEN=" & Comi & Txt_Cuenta(Index) & Comi
   Condicion = Condicion & " AND TX_PUCNIIF_CUEN<>'1'"
   Result = LoadData("CUENTAS", "NO_NOMB_CUEN", Condicion, arr())
   'JLPB T29905/R28918 FIN
   lbl_cuenta(Index) = arr(0)
   If arr(0) <> NUL$ Then
      If Valida_Nivel_Cuentas(Txt_Cuenta(Index)) Then
         Call Mensaje1("Verifique que la cuenta contable sea de �ltimo nivel!", 2)
         If Txt_Cuenta(Index).Enabled And Me.Visible Then Txt_Cuenta(Index).SetFocus
      End If
   Else
      'Call Mensaje1("Cuenta contable no registrada !", 2)
      Txt_Cuenta(Index) = NUL$
   End If
End Sub

Private Sub Cargar_Datos()
Dim arr(8)
Dim I As Integer
    Result = LoadData("PARAMETROS_INVE", "CD_CXC_APLI, CD_CXP_APLI, CD_CAJA_APLI, CD_DECO_APLI, CD_DEVE_APLI, CD_DEAD_APLI,CD_FLET_APLI,CD_DONA_APLI,CD_CXPT_APLI", NUL$, arr()):                                                                                                                                                                                                                                                                       imgSave.Visible = False
    For I = 0 To 8
        Txt_Cuenta(I) = arr(I)
        Call Buscar_Cuenta(I)
    Next
    Enc = Encontro
End Sub

Private Sub Grabar()
    Valores = "CD_CXC_APLI= " & Comi & Txt_Cuenta(0) & Comi & Coma
    Valores = Valores & "CD_CXP_APLI = " & Comi & Txt_Cuenta(1) & Comi & Coma
    Valores = Valores & "CD_CAJA_APLI = " & Comi & Txt_Cuenta(2) & Comi & Coma
    Valores = Valores & "CD_DECO_APLI = " & Comi & Txt_Cuenta(3) & Comi & Coma
    Valores = Valores & "CD_DEVE_APLI = " & Comi & Txt_Cuenta(4) & Comi & Coma
    Valores = Valores & "CD_DEAD_APLI = " & Comi & Txt_Cuenta(5) & Comi & Coma
    Valores = Valores & "CD_FLET_APLI = " & Comi & Txt_Cuenta(6) & Comi & Coma
    Valores = Valores & "CD_DONA_APLI = " & Comi & Txt_Cuenta(7) & Comi & Coma
    Valores = Valores & "CD_CXPT_APLI = " & Comi & Txt_Cuenta(8) & Comi
    
    
    Result = DoUpdate("PARAMETROS_INVE", Valores, NUL$):                                                                                                                                                    imgSave.Visible = Result <> FAIL

End Sub
