VERSION 5.00
Object = "{C4847593-972C-11D0-9567-00A0C9273C2A}#8.0#0"; "crviewer.dll"
Begin VB.Form FrmViewReport 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Crystal"
   ClientHeight    =   6840
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   9840
   Icon            =   "FrmViewReport.frx":0000
   LinkTopic       =   "Form3"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   ScaleHeight     =   6840
   ScaleWidth      =   9840
   Begin CRVIEWERLibCtl.CRViewer CRViewer1 
      Height          =   6735
      Left            =   30
      TabIndex        =   0
      Top             =   15
      Width           =   9795
      DisplayGroupTree=   -1  'True
      DisplayToolbar  =   -1  'True
      EnableGroupTree =   -1  'True
      EnableNavigationControls=   -1  'True
      EnableStopButton=   -1  'True
      EnablePrintButton=   -1  'True
      EnableZoomControl=   -1  'True
      EnableCloseButton=   -1  'True
      EnableProgressControl=   -1  'True
      EnableSearchControl=   -1  'True
      EnableRefreshButton=   -1  'True
      EnableDrillDown =   -1  'True
      EnableAnimationControl=   -1  'True
      EnableSelectExpertButton=   0   'False
      EnableToolbar   =   -1  'True
      DisplayBorder   =   -1  'True
      DisplayTabs     =   -1  'True
      DisplayBackgroundEdge=   -1  'True
      SelectionFormula=   ""
      EnablePopupMenu =   -1  'True
      EnableExportButton=   -1  'True
      EnableSearchExpertButton=   0   'False
      EnableHelpButton=   0   'False
   End
End
Attribute VB_Name = "FrmViewReport"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Public Tabla As String
Dim m_Proj As CRAXDRT.Application
Dim m_Report As CRAXDRT.Report
Dim rs As ADODB.Recordset
Sub Listar(ByVal Campos As String)
'Dim Strcnn As String       'DEPURACION DE CODIGO
On Error GoTo err
   'Me.Icon = MDI_Inventarios.ImageList1.ListImages.Item(2).Picture
   Screen.MousePointer = vbHourglass
   Set m_Proj = New CRAXDRT.Application
   Set m_Report = m_Proj.OpenReport(DirRpt & "tablagen.rpt")
   Set rs = New ADODB.Recordset
   rs.Open "SELECT " & Campos & " FROM " & Tabla, BD(0)
   m_Report.Database.SetDataSource rs, 3, 1
   m_Report.FormulaFields(1).Text = Comi & Entidad & Comi
   m_Report.FormulaFields(2).Text = Comi & Me.Caption & Comi
   CRViewer1.ReportSource = m_Report
   CRViewer1.ViewReport
   Screen.MousePointer = vbDefault
   Me.Show
   Call MouseNorm
   Exit Sub
err:
   Call ConvertErr
   Screen.MousePointer = vbDefault
   Unload Me
End Sub

Private Sub Form_Load()
   Call CenterForm(MDI_Inventarios, Me)
End Sub
