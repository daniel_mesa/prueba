VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.1#0"; "Mscomctl.ocx"
Object = "{C932BA88-4374-101B-A56C-00AA003668DC}#1.1#0"; "MSMASK32.OCX"
Object = "{0BA686C6-F7D3-101A-993E-0000C0EF6F5E}#1.0#0"; "THREED32.OCX"
Begin VB.Form frmPrmREPOKAR 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Parametros para el KARDEX"
   ClientHeight    =   7425
   ClientLeft      =   45
   ClientTop       =   435
   ClientWidth     =   11475
   Icon            =   "frmPrmREPOKAR.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   7425
   ScaleWidth      =   11475
   Begin VB.CheckBox ChkDetalle 
      Caption         =   "Detallado por Art�culo"
      Height          =   255
      Left            =   3240
      TabIndex        =   16
      Top             =   840
      Width           =   2295
   End
   Begin VB.CheckBox ChkTodoArc 
      Caption         =   "Todos los Art�culos"
      Height          =   255
      Left            =   600
      TabIndex        =   15
      Top             =   840
      Width           =   1815
   End
   Begin VB.Frame fraRANGO 
      Height          =   2175
      Left            =   6360
      TabIndex        =   5
      Top             =   5160
      Width           =   4815
      Begin VB.Frame Frame1 
         Height          =   615
         Left            =   240
         TabIndex        =   12
         Top             =   1200
         Width           =   3225
         Begin VB.OptionButton opcSIN 
            Caption         =   "Sin Costear"
            Height          =   315
            Left            =   1800
            TabIndex        =   14
            Top             =   200
            Width           =   1300
         End
         Begin VB.OptionButton opcCON 
            Caption         =   "Costeado"
            Height          =   315
            Left            =   240
            TabIndex        =   13
            Top             =   200
            Value           =   -1  'True
            Width           =   1300
         End
      End
      Begin VB.CheckBox chkFCRANGO 
         Caption         =   "Condicionar las fechas del reporte"
         Height          =   315
         Left            =   360
         TabIndex        =   10
         Top             =   240
         Width           =   4095
      End
      Begin MSMask.MaskEdBox txtFCDESDE 
         Height          =   315
         Left            =   960
         TabIndex        =   6
         Top             =   600
         Width           =   1215
         _ExtentX        =   2143
         _ExtentY        =   556
         _Version        =   393216
         PromptChar      =   "_"
      End
      Begin MSMask.MaskEdBox txtFCHASTA 
         Height          =   315
         Left            =   3240
         TabIndex        =   7
         Top             =   600
         Width           =   1215
         _ExtentX        =   2143
         _ExtentY        =   556
         _Version        =   393216
         PromptChar      =   "_"
      End
      Begin Threed.SSCommand cmdIMPRIMIR 
         Height          =   735
         Index           =   2
         Left            =   3600
         TabIndex        =   11
         Top             =   1200
         Width           =   855
         _Version        =   65536
         _ExtentX        =   1508
         _ExtentY        =   1296
         _StockProps     =   78
         Caption         =   "&IMPRIMIR"
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   6
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         RoundedCorners  =   0   'False
         Picture         =   "frmPrmREPOKAR.frx":058A
      End
      Begin VB.Label lblDESDE 
         Alignment       =   1  'Right Justify
         Caption         =   "Desde:"
         Height          =   255
         Left            =   120
         TabIndex        =   9
         Top             =   600
         Width           =   735
      End
      Begin VB.Label Label1 
         Alignment       =   1  'Right Justify
         Caption         =   "Hasta:"
         Height          =   255
         Left            =   2400
         TabIndex        =   8
         Top             =   600
         Width           =   735
      End
   End
   Begin MSComctlLib.ProgressBar pgbLLEVO 
      Height          =   255
      Left            =   6480
      TabIndex        =   3
      Top             =   240
      Visible         =   0   'False
      Width           =   4455
      _ExtentX        =   7858
      _ExtentY        =   450
      _Version        =   393216
      Appearance      =   1
   End
   Begin VB.ComboBox cboTIPO 
      Height          =   315
      Left            =   3240
      TabIndex        =   0
      Text            =   "Combo1"
      Top             =   240
      Width           =   2655
   End
   Begin MSComctlLib.ImageList imgBotones 
      Left            =   10920
      Top             =   3960
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   20
      ImageHeight     =   14
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   7
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmPrmREPOKAR.frx":0C54
            Key             =   ""
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmPrmREPOKAR.frx":0FA6
            Key             =   "ASG"
         EndProperty
         BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmPrmREPOKAR.frx":1260
            Key             =   "CAN"
         EndProperty
         BeginProperty ListImage4 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmPrmREPOKAR.frx":15B2
            Key             =   "SAV"
         EndProperty
         BeginProperty ListImage5 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmPrmREPOKAR.frx":18CC
            Key             =   "ADD"
         EndProperty
         BeginProperty ListImage6 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmPrmREPOKAR.frx":1B86
            Key             =   "DEL"
         EndProperty
         BeginProperty ListImage7 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmPrmREPOKAR.frx":1FC8
            Key             =   "CHA"
         EndProperty
      EndProperty
   End
   Begin MSComctlLib.ListView lstBODEGAS 
      Height          =   2055
      Left            =   480
      TabIndex        =   2
      Top             =   5280
      Width           =   5655
      _ExtentX        =   9975
      _ExtentY        =   3625
      View            =   3
      LabelEdit       =   1
      MultiSelect     =   -1  'True
      LabelWrap       =   -1  'True
      HideSelection   =   -1  'True
      FullRowSelect   =   -1  'True
      GridLines       =   -1  'True
      _Version        =   393217
      ForeColor       =   -2147483640
      BackColor       =   -2147483643
      BorderStyle     =   1
      Appearance      =   1
      NumItems        =   0
   End
   Begin MSComctlLib.ListView lstARTICULOS 
      Height          =   3735
      Left            =   480
      TabIndex        =   1
      Top             =   1200
      Width           =   10500
      _ExtentX        =   18521
      _ExtentY        =   6588
      View            =   3
      LabelEdit       =   1
      MultiSelect     =   -1  'True
      LabelWrap       =   -1  'True
      HideSelection   =   0   'False
      AllowReorder    =   -1  'True
      Checkboxes      =   -1  'True
      FullRowSelect   =   -1  'True
      GridLines       =   -1  'True
      _Version        =   393217
      ForeColor       =   -2147483640
      BackColor       =   -2147483643
      BorderStyle     =   1
      Appearance      =   1
      NumItems        =   2
      BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Text            =   "Nombre del Art�culo"
         Object.Width           =   7761
      EndProperty
      BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   1
         Text            =   "C�digo"
         Object.Width           =   3881
      EndProperty
   End
   Begin VB.Label txtTIPO 
      Alignment       =   1  'Right Justify
      Caption         =   "Tipo de Art�culos:"
      Height          =   315
      Left            =   600
      TabIndex        =   4
      Top             =   240
      Width           =   2415
   End
End
Attribute VB_Name = "frmPrmREPOKAR"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Dim ArrUXB() As Variant
Dim Articulo As UnArticulo
Dim Dueno As New ElTercero
'Dim LaBodega As String, LaAccion As String
Dim LaAccion As String      'DEPURACION DE CODIGO
Dim CnTdr As Integer, NumEnCombo As Integer
Public OpcCod        As String   'Opci�n de seguridad
'NU_AUTO_ARTI, CD_CODI_ARTI, NO_NOMB_ARTI, NU_AUTO_UNVE_ARTI, TX_VENCE_ARTI, TX_PARA_ARTI,
'TX_ENTRA_ARTI, CD_GRUP_ARTI, CD_USOS_ARTI, VL_ULCO_ARTI, VL_COPR_ARTI, CT_EXIS_ARTI, DE_OBSE_ARTI, ID_TIPO_ARTIID_TIPO_ARTI, CD_RIPS_ARTI, NU_INDPYP_ARTI

Private Sub cboTIPO_GotFocus()
    NumEnCombo = Me.cboTIPO.ListIndex
End Sub

'---------------------------------------------------------------------------------------
' Procedure : ChkDetalle_Click
' DateTime  : 14/03/2019 09:51
' Author    : daniel_mesa
' Purpose   : DRMG T45889-R43750 habilita o deshabilita el lstBODEGAS
'---------------------------------------------------------------------------------------
'
Private Sub ChkDetalle_Click()
   Dim Renglon As MSComctlLib.ListItem 'Almacena los items seleccionados en el lstBODEGAS
   
   If ChkDetalle.value = 1 Then
      'chkFCRANGO.value = 0 'DRMG T46119 SE DEJA EN COMENTARIO
      'chkFCRANGO.Enabled = False 'DRMG T46119 SE DEJA EN COMENTARIO
      lstBODEGAS.Enabled = False
      For Each Renglon In Me.lstBODEGAS.ListItems
         Renglon.Selected = False
      Next
   Else
      'chkFCRANGO.Enabled = True 'DRMG T46119 SE DEJA EN COMENTARIO
      lstBODEGAS.Enabled = True
   End If
   
End Sub

Private Sub chkfcRANGO_Click()
    Me.txtFCDESDE.Enabled = False
    Me.txtFCHASTA.Enabled = False
    If Not CBool(Me.chkFCRANGO.value) = True Then Exit Sub
    Me.txtFCDESDE.Enabled = True
    Me.txtFCHASTA.Enabled = True
End Sub

Private Sub CmdImprimir_Click(Index As Integer)
   Dim Renglon As MSComctlLib.ListItem
   Dim strARTICULOS As String, strBODEGAS As String
   Dim strTITRANGO As String, strTITFECHA As String
   Dim strARTICULOS1 As String
   Dim consulta As String, consulta2 As String 'REOL603
   Dim StConsulta As String 'HRR M4050
   Dim StSql As String 'HRR M4050
   Dim InDec As Integer 'HRR M4050
   Dim StConsulta1 As String 'JAGS T9361
   Dim StDesde As String 'JLPB T42280 'Almacena la subconsulta a ejecutar
   'Dim InDec As Integer 'HRR M4559 Nota 0024085
    
   Call Limpiar_CrysListar
   'HRR M4559 Nota 0024085
   'HRR M4559
   'If Aplicacion.VerDecimales_Valores(InDec) Then
   'Else
   '   InDec = 0
   'End If
   'HRR M4559
   'HRR M4559 Nota 0024085
   'HRR M4050
   If Aplicacion.VerDecimales_Valores(InDec) Then
   Else
      InDec = 0
   End If
   'HRR M4050
   If ChkTodoArc.value = False Then 'DRMG T45889-R43750
      For Each Renglon In Me.lstARTICULOS.ListItems
         If Renglon.Selected Then strARTICULOS = strARTICULOS & IIf(Len(strARTICULOS) > 0, " OR ", "") & "{ARTICULO.CD_CODI_ARTI}='" & Renglon.ListSubItems("COAR").Text & "'"
         'If Renglon.Selected Then consulta = consulta & IIf(Len(consulta) > 0, " OR ", "") & "ARTICULO.CD_CODI_ARTI='" & Renglon.ListSubItems("COAR").Text & "'" 'REOL603 'JLPB T42280 Se deja en comentario
         If Renglon.Selected Then consulta = consulta & IIf(Len(consulta) > 0, " OR ", "") & "CD_CODI_ARTI='" & Renglon.ListSubItems("COAR").Text & "'" 'JLPB T42280
         If Me.opcCON.value = True Then
            If Renglon.Selected Then strARTICULOS1 = strARTICULOS1 & IIf(Len(strARTICULOS1) > 0, " OR ", "") & "{ARTICULO_1.CD_CODI_ARTI}='" & Renglon.ListSubItems("COAR").Text & "'"
         End If
      Next
   End If 'DRMG T45889-R43750
   For Each Renglon In Me.lstBODEGAS.ListItems
      If Renglon.Selected Then strBODEGAS = strBODEGAS & IIf(Len(strBODEGAS) > 0, " OR ", "") & "{IN_BODEGA.TX_CODI_BODE}='" & Renglon.ListSubItems("COD").Text & "'"
      'If Renglon.Selected Then consulta2 = consulta2 & IIf(Len(consulta2) > 0, " OR ", "") & "IN_BODEGA.TX_CODI_BODE='" & Renglon.ListSubItems("COD").Text & "'" 'REOL603 'JLPB T42280 Se deja en comentario
      If Renglon.Selected Then consulta2 = consulta2 & IIf(Len(consulta2) > 0, " OR ", "") & "TX_CODI_BODE='" & Renglon.ListSubItems("COD").Text & "'" 'JLPB T42280
   Next
   If Len(strARTICULOS) > 0 Then strARTICULOS = "(" & strARTICULOS & ")"
   If Me.opcCON.value = True Then 'EGL
      'If Len(strARTICULOS1) > 0 Then strARTICULOS1 = "(" & strARTICULOS1 & ")"
      'strARTICULOS = strARTICULOS & " AND  " & strARTICULOS1
   End If
   ''''''''''REOL603
   If Len(consulta2) > 0 Then consulta2 = "(" & consulta2 & ")"
   If Len(consulta) > 0 And Len(consulta2) > 0 Then
      consulta = "(" & consulta & ")" 'REOL907
      consulta = consulta & " AND " & consulta2
   Else
      consulta = consulta & consulta2
   End If
   ''''''''''REOL603
   If Len(strBODEGAS) > 0 Then strBODEGAS = "(" & strBODEGAS & ")"
   If Len(strARTICULOS) > 0 And Len(strBODEGAS) > 0 Then
      strARTICULOS = strARTICULOS & " AND " & strBODEGAS
   Else
      strARTICULOS = strARTICULOS & strBODEGAS
   End If
   strTITRANGO = "TODOS LOS COMPROBANTES"
   strTITFECHA = "A LA FECHA"
   StConsulta = NUL$ 'HRR M4050
   If CBool(Me.chkFCRANGO.value) Then
      strARTICULOS = strARTICULOS & IIf(Len(strARTICULOS) > 0, " AND ", "") & "({IN_KARDEX.FE_FECH_KARD} in CDate('" & Me.txtFCDESDE.Text & "') to CDate('" & Me.txtFCHASTA.Text & "'))"
      'JLPB T42280 INICIO Se deja en comentario las siguientes lineas
      'StConsulta = consulta & IIf(Len(consulta) > 0, " AND ", "") & "IN_KARDEX.FE_FECH_KARD < " & FFechaCon(txtFCDESDE.Text) 'HRR M4050
      'StConsulta1 = consulta & IIf(Len(consulta) > 0, " AND ", "") & "IN_KARDEX.FE_FECH_KARD = " & FFechaCon(DateAdd("d", -1, CDate(txtFCDESDE.Text))) 'JAGS T9361
      'consulta = consulta & IIf(Len(consulta) > 0, " AND ", "") & "IN_KARDEX.FE_FECH_KARD BETWEEN (" & FFechaCon(txtFCDESDE.Text) & ") AND (" & FFechaCon(txtFCHASTA.Text) & ")" 'REOL603 'REOL M1832
      StConsulta = consulta & IIf(Len(consulta) > 0, " AND ", "") & "FE_FECH_KARD < " & FFechaCon(txtFCDESDE.Text)
      StConsulta1 = consulta & IIf(Len(consulta) > 0, " AND ", "") & "FE_FECH_KARD = " & FFechaCon(DateAdd("d", -1, CDate(txtFCDESDE.Text)))
      consulta = consulta & IIf(Len(consulta) > 0, " AND ", "") & "FE_FECH_KARD BETWEEN (" & FFechaCon(txtFCDESDE.Text) & ") AND (" & FFechaCon(txtFCHASTA.Text) & ")"
      'JLPB T42280 FIN
      strTITFECHA = "RANGO DE FECHAS: Del " & Me.txtFCDESDE.Text & " al " & Me.txtFCHASTA.Text
   End If
   'If ChkDetalle.value <> 1 Then 'DRMG T45889-R43750 'DRMG T46176 SE DEJA EN COMENTARIO
   If ChkDetalle.value <> 1 Or (ChkTodoArc.value = False And ChkDetalle.value = 1) Then 'DRMG T46176
      If consulta = NUL$ Then Exit Sub
   End If 'DRMG T45889-R43750
   'Campos = "MAX(NU_ACTUNMR_KARD/NU_ACTUDNM_KARD) "                           'PJCA M1033 se comentarea porque ya no aplica
   'Desde = "IN_BODEGA, Articulo, IN_ENCABEZADO, IN_KARDEX, IN_DETALLE, IN_UNDVENTA, IN_DOCUMENTO "
   'Condi = "NU_AUTO_DOCU_ENCA=NU_AUTO_DOCU AND NU_AUTO_ORGCABE_KARD=NU_AUTO_ENCA AND "
   'Condi = Condi & "NU_AUTO_BODE_KARD=NU_AUTO_BODE AND NU_AUTO_ARTI_KARD=NU_AUTO_ARTI AND "
   'Condi = Condi & consulta
   'ReDim ArrUXB(0)
   'Result = LoadData(Desde, Campos, Condi, ArrUXB())
   'If ArrUXB(0) = NUL$ Then ArrUXB(0) = 0
   MDI_Inventarios.Crys_Listar.Formulas(9) = "RAGFecha='" & strTITFECHA & Comi
   MDI_Inventarios.Crys_Listar.Formulas(10) = "ENTIDAD=" & Comi & Trim(Dueno.Nombre) & ", NIT:" & Trim(Dueno.Nit) & Comi
   'MDI_Inventarios.Crys_Listar.Formulas(11) = "sald='" & ArrUXB(0) & Comi     'PJCA M1033
   ''''''''''REOL603
   Dim SQL As String 'Consulta General
   
   'DRMG T45889-R43750 INICIO
   If ChkDetalle.value = 1 Then
      If ExisteTABLA("IN_KARDETA_TEMP") Then Result = EliminarTabla("IN_KARDETA_TEMP")
      SQL = "SELECT DISTINCT"
      SQL = SQL & " NU_AUTO_ENCA,NU_AUTO_KARD,TX_NOMB_UNVE AS UNI_CONTEO,CD_CODI_ARTI,NO_NOMB_ARTI,"
      SQL = SQL & " TX_CODI_BODE,TX_NOMB_BODE,NU_AUTO_DOCU_ENCA as TIPO_MOVI,TX_CODI_DOCU AS MOVIMIENTO,NU_AUTO_COMP_ENCA,"
      'DRMG T46119 INICIO SE DEJA EN COMENTARIO LA SIGUIENTE PRIMER LINEA
      'SQL = SQL & " NU_COMP_ENCA AS NU_MOVI,FE_CREA_ENCA AS FECHA,NU_COSPRO_KARD AS COS_PROMEDIO,"
      SQL = SQL & " NU_COMP_ENCA AS NU_MOVI,FE_CREA_ENCA AS FECHA,"
      'SQL = SQL & " IIF(NU_AUTO_DOCU_ENCA IN (3,26),NU_COSPRO_KARD,NU_COSTO_KARD) AS COS_PROMEDIO," 'DRMG T46117 SE DEJA EN COMENTARIO
      SQL = SQL & " IIF(NU_AUTO_DOCU_ENCA IN (3,9,12,14,15,22,24,26),ISNULL(NU_COSPRO_KARD,0),NU_COSTO_KARD) AS COS_PROMEDIO," 'DRMG T46117
      'DRMG T46119 FIN
      SQL = SQL & " (NU_ENTRAD_KARD * (Cast(NU_MULT_KARD As Float)/Cast(NU_DIVI_KARD As Float))) AS ENTRADA,"
      SQL = SQL & " (CASE WHEN NU_ENTRAD_KARD=0 THEN 0 ELSE NU_COSTO_KARD END) AS COSTO_EN,"
      SQL = SQL & " (NU_SALIDA_KARD * (Cast(NU_MULT_KARD As Float)/Cast(NU_DIVI_KARD As Float))) AS SALIDA ,"
      SQL = SQL & " (CASE WHEN NU_SALIDA_KARD=0 THEN 0 ELSE NU_COSTO_KARD END) AS COSTO_SA,"
      SQL = SQL & " (CASE WHEN NU_ACTUDNM_KARD=0 THEN NU_ACTUNMR_KARD ELSE NU_ACTUNMR_KARD/NU_ACTUDNM_KARD END) AS SALDO"
      SQL = SQL & Coma & "ROW_NUMBER()OVER(PARTITION BY CD_CODI_ARTI ORDER BY NU_AUTO_ENCA ASC) + " & IIf(CBool(Me.chkFCRANGO.value), 1, 0) & " AS LINEA" 'DRMG T46119
      SQL = SQL & " INTO IN_KARDETA_TEMP"
      SQL = SQL & " FROM IN_KARDEX "
      SQL = SQL & " INNER JOIN IN_BODEGA ON  NU_AUTO_BODE_KARD=NU_AUTO_BODE"
      SQL = SQL & " INNER JOIN ARTICULO ON NU_AUTO_ARTI_KARD=NU_AUTO_ARTI"
      SQL = SQL & " INNER JOIN IN_ENCABEZADO ON NU_AUTO_ORGCABE_KARD=NU_AUTO_ENCA"
      SQL = SQL & " INNER JOIN IN_UNDVENTA ON NU_AUTO_UNVE=NU_AUTO_UNVE_ARTI"
      SQL = SQL & " INNER JOIN IN_DOCUMENTO ON  NU_AUTO_DOCU_ENCA=NU_AUTO_DOCU"
      SQL = SQL & " WHERE (TX_AFEC_DOCU)='S'"
      SQL = SQL & IIf(ChkTodoArc.value = False, " AND " & consulta, IIf(CBool(Me.chkFCRANGO.value), " AND " & "FE_FECH_KARD BETWEEN (" & FFechaCon(txtFCDESDE.Text) & ") AND (" & FFechaCon(txtFCHASTA.Text) & ")", NUL$))
      SQL = SQL & " ORDER BY NU_AUTO_ENCA,FE_CREA_ENCA"
      Result = ExecSQL(SQL)
      
      'DRMG T46119 INICIO CALCULA EL SALDO INICIAL
      If Result <> FAIL And CBool(Me.chkFCRANGO.value) Then
         SQL = "INSERT INTO IN_KARDETA_TEMP SELECT"
         SQL = SQL & " 1,1,TX_NOMB_UNVE,CD_CODI_ARTI,NO_NOMB_ARTI,0,'SALDO INICIAL',0,'',0,0," & FFechaCon(txtFCDESDE.Text) & ",0,"
         SQL = SQL & " SUM((CASE WHEN NU_AUTO_DOCU_ENCA IN (3,9,12,14,15,22,24,26) THEN NU_ENTRAD_KARD * (Cast(NU_MULT_KARD AS Float)/Cast(NU_DIVI_KARD AS Float)) ELSE 0 END)) AS ENTRADA,"
         SQL = SQL & " SUM((CASE WHEN NU_AUTO_DOCU_ENCA IN (3,9,12,14,15,22,24,26) THEN (CASE WHEN NU_ENTRAD_KARD=0 THEN 0 ELSE NU_COSTO_KARD END)*(NU_ENTRAD_KARD * (Cast(NU_MULT_KARD AS Float)/Cast(NU_DIVI_KARD AS Float)))ELSE 0 END)) AS COST_ENTRA,"
         SQL = SQL & " SUM((CASE WHEN NU_AUTO_DOCU_ENCA IN (4,8,11,13,19,27) THEN NU_SALIDA_KARD * (Cast(NU_MULT_KARD AS Float)/Cast(NU_DIVI_KARD AS Float))ELSE 0 END)) AS SALIDA,"
         SQL = SQL & " SUM((CASE WHEN NU_AUTO_DOCU_ENCA IN (4,8,11,13,19,27) THEN (CASE WHEN NU_SALIDA_KARD=0 THEN 0 ELSE NU_COSTO_KARD END)*((NU_SALIDA_KARD * (Cast(NU_MULT_KARD AS Float)/Cast(NU_DIVI_KARD AS Float))))ELSE 0 END)) AS COS_SALI,"
         SQL = SQL & " SUM((NU_ENTRAD_KARD * (Cast(NU_MULT_KARD AS Float)/Cast(NU_DIVI_KARD AS Float)))-(NU_SALIDA_KARD * (Cast(NU_MULT_KARD AS Float)/Cast(NU_DIVI_KARD AS Float)))) AS SALDO"
         SQL = SQL & ",1"
         SQL = SQL & " FROM IN_KARDEX "
         SQL = SQL & " INNER JOIN IN_BODEGA ON  NU_AUTO_BODE_KARD=NU_AUTO_BODE"
         SQL = SQL & " INNER JOIN ARTICULO ON NU_AUTO_ARTI_KARD=NU_AUTO_ARTI"
         SQL = SQL & " INNER JOIN IN_ENCABEZADO ON NU_AUTO_ORGCABE_KARD=NU_AUTO_ENCA"
         SQL = SQL & " INNER JOIN IN_UNDVENTA ON NU_AUTO_UNVE=NU_AUTO_UNVE_ARTI"
         SQL = SQL & " INNER JOIN IN_DOCUMENTO ON  NU_AUTO_DOCU_ENCA=NU_AUTO_DOCU"
         SQL = SQL & " WHERE (TX_AFEC_DOCU)='S'"
         SQL = SQL & IIf(ChkTodoArc.value = False, " AND (" & StConsulta & ") AND " & "FE_FECH_KARD < " & FFechaCon(txtFCDESDE.Text), " AND " & "FE_FECH_KARD < " & FFechaCon(txtFCDESDE.Text))
         SQL = SQL & " GROUP BY TX_NOMB_UNVE,CD_CODI_ARTI,NO_NOMB_ARTI"
         Result = ExecSQL(SQL)
      End If
      'DRMG T46119 FIN
      
      If Result <> FAIL Then
         If opcSIN.value = True Then
            Call ListarD("infKarDetaSinCos.rpt", "", crptToWindow, "KARDEX", MDI_Inventarios, True)
         Else
            Call ListarD("infKarDetaCos.rpt", "", crptToWindow, "KARDEX", MDI_Inventarios, True)
         End If
         If ExisteTABLA("IN_KARDETA_TEMP") Then Result = EliminarTabla("IN_KARDETA_TEMP")
      End If
      Exit Sub
   End If
   'DRMG T45889-R43750 FIN
   
   If Not Me.opcCON Then
      'SQL = "SELECT DISTINCT TX_NOMB_UNVE,TX_NOMB_BODE,CD_CODI_ARTI_KARD,NO_NOMB_ARTI,TX_CODI_DOCU,TX_NOMB_DOCU,FE_CREA_ENCA,NU_COMP_ENCA,"
      SQL = "SELECT DISTINCT TX_NOMB_UNVE,TX_NOMB_BODE,CD_CODI_ARTI AS CD_CODI_ARTI_KARD,NO_NOMB_ARTI,TX_CODI_DOCU,TX_NOMB_DOCU,FE_CREA_ENCA,NU_COMP_ENCA," 'JACC M6654
      'SQL = SQL & "(NU_ENTRAD_KARD*NU_MULT_KARD/NU_DIVI_KARD) AS ENTRADA,"    'REOL M1832
      'SQL = SQL & "(NU_SALIDA_KARD*NU_MULT_KARD/NU_DIVI_KARD) AS SALIDA "     'REOL M1832
      'LJSA M4726 --------------------------------
      If MotorBD = "SQL" Then
         SQL = SQL & "(NU_ENTRAD_KARD * (Cast(NU_MULT_KARD As Float)/Cast(NU_DIVI_KARD As Float))) AS ENTRADA,"
         SQL = SQL & "(NU_SALIDA_KARD * (Cast(NU_MULT_KARD As Float)/Cast(NU_DIVI_KARD As Float))) AS SALIDA"
      ElseIf MotorBD = "ACCESS" Then
         'SQL = SQL & "(NU_ENTRAD_KARD * (CDec(NU_MULT_KARD)/CDec(NU_DIVI_KARD))) AS ENTRADA,"
         'SQL = SQL & "(NU_SALIDA_KARD * (CDec(NU_MULT_KARD)/CDec(NU_DIVI_KARD))) AS SALIDA"
         SQL = SQL & "(NU_ENTRAD_KARD * (CDoble(NU_MULT_KARD)/CDoble(NU_DIVI_KARD))) AS ENTRADA,"    'LJSA M4726 Nota 21408
         SQL = SQL & "(NU_SALIDA_KARD * (CDoble(NU_MULT_KARD)/CDoble(NU_DIVI_KARD))) AS SALIDA"             'LJSA M4726 Nota 21408
      End If
      'LJSA M4726 --------------------------------
      SQL = SQL & " ,NU_COSTO_KARD " 'REOL M1021
      SQL = SQL & " ,NU_ACTUNMR_KARD, NU_ACTUDNM_KARD, NU_AUTO_ORGCABE_KARD "   'PJCA M1033
      SQL = SQL & " ,NU_AUTO_KARD "
      SQL = SQL & "INTO IN_KARDGRUP_TEMP "
      SQL = SQL & "FROM IN_BODEGA,ARTICULO,IN_ENCABEZADO,IN_KARDEX,IN_UNDVENTA,IN_DOCUMENTO " 'REOL907
      SQL = SQL & "WHERE NU_AUTO_UNVE=NU_AUTO_UNVE_ARTI AND NU_AUTO_DOCU_ENCA=NU_AUTO_DOCU AND NU_AUTO_ORGCABE_KARD=NU_AUTO_ENCA AND "
      SQL = SQL & "NU_AUTO_BODE_KARD=NU_AUTO_BODE AND NU_AUTO_ARTI_KARD=NU_AUTO_ARTI "
      If ExisteTABLA("IN_KARDGRUP_TEMP") Then Result = EliminarTabla("IN_KARDGRUP_TEMP")
      consulta = SQL & " AND " & consulta & " ORDER BY 7,8 DESC"
      Result = ExecSQL(consulta)
      'strARTICULOS = SQL & " AND " & strARTICULOS & " ORDER BY 6,7 DESC"
      Call ListarD("infKARDEXSIN.rpt", "", crptToWindow, "KARDEX", MDI_Inventarios, True)
   Else
      'SQL = "SELECT DISTINCT TX_NOMB_UNVE,TX_NOMB_BODE,CD_CODI_ARTI_KARD,NO_NOMB_ARTI,NU_AUTO_ORGCABE_KARD,TX_CODI_DOCU,FE_CREA_ENCA,NU_COMP_ENCA,"
      SQL = "SELECT DISTINCT TX_NOMB_UNVE,TX_NOMB_BODE,CD_CODI_ARTI AS CD_CODI_ARTI_KARD,NO_NOMB_ARTI,NU_AUTO_ORGCABE_KARD,TX_CODI_DOCU,FE_CREA_ENCA,NU_COMP_ENCA," 'JACC M6654
      'SQL = SQL & "(NU_ENTRAD_KARD*NU_MULT_KARD/NU_DIVI_KARD) AS ENTRADA,"       'REOL M1832
      'LJSA M4726 -----------------------------
      If MotorBD = "SQL" Then
         SQL = SQL & "(NU_ENTRAD_KARD * (Cast(NU_MULT_KARD As Float)/Cast(NU_DIVI_KARD As Float))) AS ENTRADA,"
      ElseIf MotorBD = "ACCESS" Then
         'SQL = SQL & "(NU_ENTRAD_KARD * (CDec(NU_MULT_KARD)/CDec(NU_DIVI_KARD))) AS ENTRADA,"
         SQL = SQL & "(NU_ENTRAD_KARD * (CDoble(NU_MULT_KARD)/CDoble(NU_DIVI_KARD))) AS ENTRADA,"        'LJSA M4726 Nota 21408
      End If
      'HRR M4559 Inicio
      ''LJSA M4726 -----------------------------
      ''SQL = SQL & "(NU_COSTO_KARD*NU_ENTRAD_KARD*NU_MULT_KARD/NU_DIVI_KARD) AS C_ENTRADA,"    'REOL M1832
      'SQL = SQL & "(NU_COSTO_KARD*(NU_ENTRAD_KARD*NU_MULT_KARD/NU_DIVI_KARD)) AS C_ENTRADA,"    'NMSR M3287
      ''SQL = SQL & "(NU_SALIDA_KARD*NU_MULT_KARD/NU_DIVI_KARD) AS SALIDA,"     'REOL M1832
      ''LJSA M4726 -----------------------------
      'If MotorBD = "SQL" Then
      '   SQL = SQL & "(NU_SALIDA_KARD * (Cast(NU_MULT_KARD As Float)/Cast(NU_DIVI_KARD As Float))) AS SALIDA,"
      'ElseIf MotorBD = "ACCESS" Then
      '   'SQL = SQL & "(NU_SALIDA_KARD * (CDec(NU_MULT_KARD)/CDec(NU_DIVI_KARD))) AS SALIDA,"
      '   SQL = SQL & "(NU_SALIDA_KARD * (CDoble(NU_MULT_KARD)/CDoble(NU_DIVI_KARD))) AS SALIDA,"     'LJSA M4726 Nota 21408
      'End If
      ''LJSA M4726 -----------------------------
      ''SQL = SQL & "(NU_COSTO_KARD*NU_SALIDA_KARD*NU_MULT_KARD/NU_DIVI_KARD) AS C_SALIDA,NU_AUTO_BODEDST_ENCA, "   'REOL M1832
      'SQL = SQL & "(NU_COSTO_KARD*(NU_SALIDA_KARD*NU_MULT_KARD/NU_DIVI_KARD)) AS C_SALIDA,NU_AUTO_BODEDST_ENCA, " 'NMSR M3287
      'HRR M4559 Fin
      'HRR M4559 Inicio 'HRR M4559 Nota 0024085 Se elimina el formateo de la consulta
      If MotorBD = "SQL" Then
         'DAHV T6246 - INICIO
         'SQL = SQL & "(CAST(NU_COSTO_KARD AS FLOAT)*CAST(NU_ENTRAD_KARD AS FLOAT)*CAST(NU_MULT_KARD AS FLOAT)/CAST(NU_DIVI_KARD AS FLOAT)) AS C_ENTRADA,"
         SQL = SQL & " (CAST(NU_ENTRAD_KARD AS FLOAT) * (CASE WHEN TX_IVADED_KARD = 'N' THEN CAST(NU_COSTO_KARD AS FLOAT) ELSE (CAST(NU_COSTO_KARD AS FLOAT) * 100)/(100+ CAST(NU_IMPU_KARD AS FLOAT)) END)) AS C_ENTRADA,"
         'DAHV T6246 - FIN
         SQL = SQL & "(NU_SALIDA_KARD * (CAST(NU_MULT_KARD AS FLOAT)/CAST(NU_DIVI_KARD AS FLOAT))) AS SALIDA,"
         SQL = SQL & "(CAST(NU_COSTO_KARD AS FLOAT)*CAST(NU_SALIDA_KARD AS FLOAT)*CAST(NU_MULT_KARD AS FLOAT)/CAST(NU_DIVI_KARD AS FLOAT)) AS C_SALIDA,NU_AUTO_BODEDST_ENCA, "
         'DAHV M5899 - Inicio
         'Se realiza modificaci�n en este punto para no presentar manipulaci�n de funcionamient
         'de inventarios. Se complementa el arreglo modificando formula de costo @costoent
         'en el reporte infkardexcon.rpt
         SQL = SQL & "(CAST(NU_VALOR_KARD AS FLOAT)*CAST(NU_ENTRAD_KARD AS FLOAT)*CAST(NU_MULT_KARD AS FLOAT)/CAST(NU_DIVI_KARD AS FLOAT)) AS COS_ENTRADA,"
         SQL = SQL & "(CAST(NU_VALOR_KARD AS FLOAT)*CAST(NU_SALIDA_KARD AS FLOAT)*CAST(NU_MULT_KARD AS FLOAT)/CAST(NU_DIVI_KARD AS FLOAT)) AS COS_SALIDA, "
         'DAHV M5899 - FIN
      ElseIf MotorBD = "ACCESS" Then
         SQL = SQL & "(CDOBLE(NU_COSTO_KARD)*CDOBLE(NU_ENTRAD_KARD)*CDOBLE(NU_MULT_KARD)/CDOBLE(NU_DIVI_KARD)) AS C_ENTRADA,"
         SQL = SQL & "(NU_SALIDA_KARD * (CDOBLE(NU_MULT_KARD)/CDOBLE(NU_DIVI_KARD))) AS SALIDA,"     'LJSA M4726 Nota 21408
         SQL = SQL & "(CDOBLE(NU_COSTO_KARD)*CDOBLE(NU_SALIDA_KARD)*CDOBLE(NU_MULT_KARD)/CDOBLE(NU_DIVI_KARD)) AS C_SALIDA,NU_AUTO_BODEDST_ENCA, "
      End If
      'HRR M4559 Fin
      SQL = SQL & " NU_COSTO_KARD " 'REOL M1021
      SQL = SQL & " ,NU_ACTUNMR_KARD, NU_ACTUDNM_KARD "   'PJCA M1033
      SQL = SQL & ",NO_NOMB_TERC " 'REOL M1196
      SQL = SQL & " ,NU_AUTO_KARD "
      SQL = SQL & "INTO IN_KARDGRUP_TEMP "
      If MotorBD = "SQL" Then 'NMSR M3633
         'Inicio JAGS T11335
         'JLPB T42280 INICIO Se deja en comentario
         'If BoVer2012 = True Then
         '   SQL = SQL & "FROM IN_BODEGA,ARTICULO,IN_ENCABEZADO LEFT OUTER JOIN TERCERO ON CD_CODI_TERC_ENCA = CD_CODI_TERC,IN_KARDEX,IN_UNDVENTA,IN_DOCUMENTO " 'REOL907 'JAGS T11335
         'Else
         ''Inicio JAGS T11335
         '    SQL = SQL & "FROM IN_BODEGA,ARTICULO,IN_ENCABEZADO,IN_KARDEX,IN_UNDVENTA,IN_DOCUMENTO " 'REOL907
         '    SQL = SQL & ",TERCERO " 'REOL M1196
         'End If 'Inicio JAGS T11335
         'SQL = SQL & "WHERE NU_AUTO_UNVE=NU_AUTO_UNVE_ARTI AND NU_AUTO_DOCU_ENCA=NU_AUTO_DOCU AND NU_AUTO_ORGCABE_KARD=NU_AUTO_ENCA AND "
         'SQL = SQL & "NU_AUTO_BODE_KARD=NU_AUTO_BODE AND NU_AUTO_ARTI_KARD=NU_AUTO_ARTI "
         ''SQL = SQL & "AND CD_CODI_TERC_ENCA = CD_CODI_TERC " 'REOL M1196
         ''Inicio JAGS T11335
         'If BoVer2012 = False Then
         '    SQL = SQL & "AND CD_CODI_TERC_ENCA *= CD_CODI_TERC " 'NMSR M3287
         'End If
         ''fin JAGS T11335
         'SQL = SQL & "AND (TX_AFEC_DOCU)='S'" 'HRR M4559
         SQL = SQL & " FROM IN_ENCABEZADO INNER JOIN IN_KARDEX ON NU_AUTO_ENCA=NU_AUTO_ORGCABE_KARD"
         SQL = SQL & " INNER JOIN ARTICULO ON NU_AUTO_ARTI_KARD=NU_AUTO_ARTI"
         SQL = SQL & " INNER JOIN IN_DOCUMENTO ON NU_AUTO_DOCU_ENCA=NU_AUTO_DOCU"
         SQL = SQL & " INNER JOIN IN_UNDVENTA ON NU_AUTO_UNVE_ARTI=NU_AUTO_UNVE"
         SQL = SQL & " INNER JOIN IN_BODEGA ON NU_AUTO_BODE_KARD=NU_AUTO_BODE"
         SQL = SQL & " LEFT OUTER JOIN TERCERO ON CD_CODI_TERC_ENCA=CD_CODI_TERC"
         SQL = SQL & " WHERE (TX_AFEC_DOCU)='S'"
         'JLPB T42280 FIN
         If ExisteTABLA("IN_KARDGRUP_TEMP") Then Result = EliminarTabla("IN_KARDGRUP_TEMP")
         consulta = SQL & " AND " & consulta & " ORDER BY 5,7 DESC,15 DESC" 'REOL M603
         'NMSR M3633
      Else
         SQL = SQL & " FROM ((((((IN_DOCUMENTO INNER JOIN IN_ENCABEZADO ON IN_DOCUMENTO.NU_AUTO_DOCU = IN_ENCABEZADO.NU_AUTO_DOCU_ENCA)"
         SQL = SQL & " INNER JOIN IN_KARDEX ON IN_ENCABEZADO.NU_AUTO_ENCA = IN_KARDEX.NU_AUTO_ORGCABE_KARD)"
         SQL = SQL & " INNER JOIN IN_BODEGA ON IN_KARDEX.NU_AUTO_BODE_KARD = IN_BODEGA.NU_AUTO_BODE)"
         SQL = SQL & " INNER JOIN ARTICULO ON IN_KARDEX.NU_AUTO_ARTI_KARD = ARTICULO.NU_AUTO_ARTI)"
         SQL = SQL & " INNER JOIN IN_UNDVENTA ON ARTICULO.NU_AUTO_UNVE_ARTI=IN_UNDVENTA.NU_AUTO_UNVE)"
         SQL = SQL & " LEFT OUTER JOIN TERCERO ON IN_ENCABEZADO.CD_CODI_TERC_ENCA = TERCERO.CD_CODI_TERC)"
         If ExisteTABLA("IN_KARDGRUP_TEMP") Then Result = EliminarTabla("IN_KARDGRUP_TEMP")
         consulta = SQL & "  WHERE " & consulta & " ORDER BY 5,7 DESC,15 DESC" 'REOL M603
      End If
      'NMSR M3633
      BD(BDCurCon).CommandTimeout = 0 'JLPB T42280
      Result = ExecSQL(consulta)
      'strARTICULOS = SQL & " AND " & strARTICULOS & " ORDER BY 6,7 DESC"
      'HRR M4050
      If Result <> FAIL Then
         StSql = NUL$
         If StConsulta <> NUL$ Then
            'JLPB T42280 INICIO
            StDesde = " FROM IN_ENCABEZADO INNER JOIN IN_KARDEX ON NU_AUTO_ENCA=NU_AUTO_ORGCABE_KARD"
            StDesde = StDesde & " INNER JOIN ARTICULO AT ON NU_AUTO_ARTI_KARD=NU_AUTO_ARTI"
            StDesde = StDesde & " INNER JOIN IN_DOCUMENTO ID ON NU_AUTO_DOCU_ENCA=NU_AUTO_DOCU"
            StDesde = StDesde & " INNER JOIN IN_UNDVENTA IU ON NU_AUTO_UNVE_ARTI=NU_AUTO_UNVE"
            StDesde = StDesde & " INNER JOIN IN_BODEGA IB ON NU_AUTO_BODE_KARD=NU_AUTO_BODE"
            StDesde = StDesde & " LEFT OUTER JOIN TERCERO TC ON CD_CODI_TERC_ENCA=CD_CODI_TERC"
            StDesde = StDesde & " WHERE TX_AFEC_DOCU='S'"
            'JLPB T42280 FIN
            'StSql = "INSERT INTO IN_KARDGRUP_TEMP SELECT DISTINCT TX_NOMB_UNVE,TX_NOMB_BODE,CD_CODI_ARTI_KARD,NO_NOMB_ARTI,NU_AUTO_ORGCABE_KARD,TX_CODI_DOCU,FE_CREA_ENCA,NU_COMP_ENCA,"
            'StSql = "INSERT INTO IN_KARDGRUP_TEMP SELECT DISTINCT TX_NOMB_UNVE,TX_NOMB_BODE,CD_CODI_ARTI  AS CD_CODI_ARTI_KARD,NO_NOMB_ARTI,NU_AUTO_ORGCABE_KARD,TX_CODI_DOCU,FE_CREA_ENCA,NU_COMP_ENCA," 'JACC M6654
            StSql = "INSERT INTO IN_KARDGRUP_TEMP SELECT DISTINCT TX_NOMB_UNVE,TX_NOMB_BODE, CD_CODI_ARTI_KARD,NO_NOMB_ARTI,NU_AUTO_ORGCABE_KARD,TX_CODI_DOCU,FE_CREA_ENCA,NU_COMP_ENCA," 'JACC M6713
            StSql = StSql & "ENTRADA,C_ENTRADA, SALIDA, C_SALIDA,"
            'AASV M5987 Inicio Se adicionan los campos para que no se genere error al insertar cuando se selecciona un rango de fechas
            'StSql = StSql & " NU_AUTO_BODEDST_ENCA, NU_COSTO_KARD "
            StSql = StSql & " NU_AUTO_BODEDST_ENCA "
            If Me.opcCON Then
               StSql = StSql & " ,0 AS COS_ENTRADA , 0 AS COS_SALIDA"
               'StSql = StSql & " ,C_ENTRADA ,C_SALIDA"
            End If
            StSql = StSql & " , NU_COSTO_KARD "
            'AASV M5987 Fin
            'StSql = StSql & " ,NU_ACTUNMR_KARD, NU_ACTUDNM_KARD "
            StSql = StSql & " ,ISNULL (NU_ACTUNMR_KARD,0) AS NU_ACTUNMR_KARD , ISNULL(NU_ACTUDNM_KARD,0) AS NU_ACTUDNM_KARD" 'JAGS T9361 PNC
            StSql = StSql & ",NO_NOMB_TERC "
            StSql = StSql & " ,NU_AUTO_KARD "
            'StSql = StSql & " FROM (SELECT TX_NOMB_UNVE,TX_NOMB_BODE,CD_CODI_ARTI_KARD,NO_NOMB_ARTI,0 AS NU_AUTO_ORGCABE_KARD,'' AS TX_CODI_DOCU," & FFechaCon(DateAdd("d", -1, CDate(txtFCDESDE.Text))) & " AS FE_CREA_ENCA,0 AS NU_COMP_ENCA," 'HRR M4050 Se quita distinct
            StSql = StSql & " FROM (SELECT TX_NOMB_UNVE,TX_NOMB_BODE,CD_CODI_ARTI  AS CD_CODI_ARTI_KARD,NO_NOMB_ARTI,0 AS NU_AUTO_ORGCABE_KARD,'' AS TX_CODI_DOCU," & FFechaCon(DateAdd("d", -1, CDate(txtFCDESDE.Text))) & " AS FE_CREA_ENCA,0 AS NU_COMP_ENCA," 'JACC M6654
            StSql = StSql & "0 AS ENTRADA,"
            StSql = StSql & "SUM(ROUND((CAST(NU_COSTO_KARD AS FLOAT)*CAST(NU_ENTRAD_KARD AS FLOAT)*CAST(NU_MULT_KARD AS FLOAT)/CAST(NU_DIVI_KARD AS FLOAT))," & InDec & ")) AS C_ENTRADA,"
            StSql = StSql & "0 AS SALIDA,"
            StSql = StSql & "SUM(ROUND((CAST(NU_COSTO_KARD AS FLOAT)*CAST(NU_SALIDA_KARD AS FLOAT)*CAST(NU_MULT_KARD AS FLOAT)/CAST(NU_DIVI_KARD AS FLOAT))," & InDec & ")) AS C_SALIDA,0 as NU_AUTO_BODEDST_ENCA, "
            StSql = StSql & "0 AS NU_COSTO_KARD "
            'StSql = StSql & " ,0 AS NU_ACTUNMR_KARD,0 AS NU_ACTUDNM_KARD " 'JAGS T9361
            'JAGS T9361 INICIO
            'StSql = StSql & " ,(SELECT NU_ACTUNMR_KARD FROM IN_BODEGA,ARTICULO,IN_ENCABEZADO,IN_KARDEX,IN_UNDVENTA,IN_DOCUMENTO ,TERCERO WHERE NU_AUTO_UNVE=NU_AUTO_UNVE_ARTI AND NU_AUTO_DOCU_ENCA=NU_AUTO_DOCU"
            'Inicio JAGS T11335
            'StSql = StSql & " ,(SELECT TOP 1 (NU_ACTUNMR_KARD) FROM IN_BODEGA,ARTICULO,IN_ENCABEZADO,IN_KARDEX,IN_UNDVENTA,IN_DOCUMENTO ,TERCERO WHERE NU_AUTO_UNVE=NU_AUTO_UNVE_ARTI AND NU_AUTO_DOCU_ENCA=NU_AUTO_DOCU" 'JAGS T10571
            'JLPB T42280 INICIO Se deja en comentario las siguientes lineas
            'If BoVer2012 = True Then
            '   StSql = StSql & " ,(SELECT TOP 1 (NU_ACTUNMR_KARD) FROM IN_BODEGA,ARTICULO,IN_ENCABEZADO LEFT OUTER JOIN TERCERO ON CD_CODI_TERC_ENCA=CD_CODI_TERC,IN_KARDEX,IN_UNDVENTA,IN_DOCUMENTO WHERE NU_AUTO_UNVE=NU_AUTO_UNVE_ARTI AND NU_AUTO_DOCU_ENCA=NU_AUTO_DOCU" 'JAGS T10571
            'Else
            '   StSql = StSql & " ,(SELECT TOP 1 (NU_ACTUNMR_KARD) FROM IN_BODEGA,ARTICULO,IN_ENCABEZADO,IN_KARDEX,IN_UNDVENTA,IN_DOCUMENTO ,TERCERO WHERE NU_AUTO_UNVE=NU_AUTO_UNVE_ARTI AND NU_AUTO_DOCU_ENCA=NU_AUTO_DOCU"
            'End If
            ''fin JAGS T11335
            'StSql = StSql & " AND NU_AUTO_ORGCABE_KARD=NU_AUTO_ENCA AND NU_AUTO_BODE_KARD=NU_AUTO_BODE"
            ''inicio JAGS T11335
            'If BoVer2012 = True Then
            '   StSql = StSql & " AND NU_AUTO_ARTI_KARD=NU_AUTO_ARTI"
            'Else
            '   StSql = StSql & " AND NU_AUTO_ARTI_KARD=NU_AUTO_ARTI AND CD_CODI_TERC_ENCA *= CD_CODI_TERC"
            'End If
            ''StSql = StSql & " AND NU_AUTO_ARTI_KARD=NU_AUTO_ARTI AND CD_CODI_TERC_ENCA *= CD_CODI_TERC"
            ''fin JAGS T11335
            'StSql = StSql & " AND (TX_AFEC_DOCU)='S'"
            StSql = StSql & ",(SELECT TOP 1 (NU_ACTUNMR_KARD)"
            StSql = StSql & StDesde
            'JLPB T42280 FIN
            'StSql = StSql & " AND " & StConsulta1 & ") AS NU_ACTUNMR_KARD"
            StSql = StSql & " AND " & StConsulta1 & " ORDER BY NU_AUTO_ENCA DESC) AS NU_ACTUNMR_KARD" 'JAGS 10571
            'StSql = StSql & ", (SELECT NU_ACTUDNM_KARD FROM IN_BODEGA,ARTICULO,IN_ENCABEZADO,IN_KARDEX,IN_UNDVENTA,IN_DOCUMENTO ,TERCERO WHERE NU_AUTO_UNVE=NU_AUTO_UNVE_ARTI AND NU_AUTO_DOCU_ENCA=NU_AUTO_DOCU"
            'inicio JAGS T11335
            'JLPB T42280 INICIO Se deja en comentario las siguientes lineas
            'If BoVer2012 = True Then
            '   StSql = StSql & ", (SELECT TOP 1 (NU_ACTUDNM_KARD) FROM IN_BODEGA,ARTICULO,IN_ENCABEZADO LEFT OUTER JOIN TERCERO ON CD_CODI_TERC_ENCA=CD_CODI_TERC,IN_KARDEX,IN_UNDVENTA,IN_DOCUMENTO WHERE NU_AUTO_UNVE=NU_AUTO_UNVE_ARTI AND NU_AUTO_DOCU_ENCA=NU_AUTO_DOCU"
            'Else
            '   StSql = StSql & ", (SELECT TOP 1 (NU_ACTUDNM_KARD) FROM IN_BODEGA,ARTICULO,IN_ENCABEZADO,IN_KARDEX,IN_UNDVENTA,IN_DOCUMENTO ,TERCERO WHERE NU_AUTO_UNVE=NU_AUTO_UNVE_ARTI AND NU_AUTO_DOCU_ENCA=NU_AUTO_DOCU" 'JAGS T10571
            'End If
            ''StSql = StSql & ", (SELECT TOP 1 (NU_ACTUDNM_KARD) FROM IN_BODEGA,ARTICULO,IN_ENCABEZADO,IN_KARDEX,IN_UNDVENTA,IN_DOCUMENTO ,TERCERO WHERE NU_AUTO_UNVE=NU_AUTO_UNVE_ARTI AND NU_AUTO_DOCU_ENCA=NU_AUTO_DOCU" 'JAGS T10571
            ''fin JAGS T11335
            'StSql = StSql & " AND NU_AUTO_ORGCABE_KARD=NU_AUTO_ENCA AND NU_AUTO_BODE_KARD=NU_AUTO_BODE"
            ''inicio JAGS T11335
            'If BoVer2012 = True Then
            '   StSql = StSql & " AND NU_AUTO_ARTI_KARD=NU_AUTO_ARTI"
            'Else
            '   StSql = StSql & " AND NU_AUTO_ARTI_KARD=NU_AUTO_ARTI AND CD_CODI_TERC_ENCA *= CD_CODI_TERC"
            'End If
            ''StSql = StSql & " AND NU_AUTO_ARTI_KARD=NU_AUTO_ARTI AND CD_CODI_TERC_ENCA *= CD_CODI_TERC"
            ''fin JAGS T11335
            'StSql = StSql & " AND (TX_AFEC_DOCU)='S'"
            StSql = StSql & ",(SELECT TOP 1 (NU_ACTUDNM_KARD)"
            StSql = StSql & StDesde
            'JLPB T42280 FIN
            'StSql = StSql & " AND " & StConsulta1 & ") AS NU_ACTUDNM_KARD "
            StSql = StSql & " AND " & StConsulta1 & " ORDER BY NU_AUTO_ENCA DESC) AS NU_ACTUDNM_KARD " 'JAGS T10571
            'JAGS T9361 FIN
            StSql = StSql & ",'Costo a ' + convert(varchar(10),'" & DateAdd("d", -1, CDate(txtFCDESDE.Text)) & "') AS NO_NOMB_TERC " 'HRR M4050
            StSql = StSql & " ,0 AS NU_AUTO_KARD "
            'inicio JAGS T11335
            'JLPB T42280 INICIO Se deja en comentario las siguientes lineas
            'If BoVer2012 = True Then
            '   StSql = StSql & "FROM IN_BODEGA,ARTICULO,IN_ENCABEZADO LEFT OUTER JOIN TERCERO ON CD_CODI_TERC_ENCA=CD_CODI_TERC,IN_KARDEX,IN_UNDVENTA,IN_DOCUMENTO "
            'Else
            '   StSql = StSql & "FROM IN_BODEGA,ARTICULO,IN_ENCABEZADO,IN_KARDEX,IN_UNDVENTA,IN_DOCUMENTO "
            '   StSql = StSql & ",TERCERO " 'JAGS T11335
            'End If
            ''StSql = StSql & "FROM IN_BODEGA,ARTICULO,IN_ENCABEZADO,IN_KARDEX,IN_UNDVENTA,IN_DOCUMENTO "
            ''StSql = StSql & ",TERCERO " 'JAGS T11335
            ''fin JAGS T11335
            'StSql = StSql & "WHERE NU_AUTO_UNVE=NU_AUTO_UNVE_ARTI AND NU_AUTO_DOCU_ENCA=NU_AUTO_DOCU AND NU_AUTO_ORGCABE_KARD=NU_AUTO_ENCA AND "
            'StSql = StSql & "NU_AUTO_BODE_KARD=NU_AUTO_BODE AND NU_AUTO_ARTI_KARD=NU_AUTO_ARTI "
            ''inicio JAGS T11335
            'If BoVer2012 = False Then
            '   StSql = StSql & "AND CD_CODI_TERC_ENCA *= CD_CODI_TERC "
            'End If
            ''fin JAGS T11335
            'StSql = StSql & "AND (TX_AFEC_DOCU)='S'"
            StSql = StSql & StDesde
            'JLPB T42280 FIN
            StSql = StSql & " AND " & StConsulta
            'StSql = StSql & " GROUP BY TX_NOMB_UNVE,TX_NOMB_BODE,CD_CODI_ARTI_KARD,NO_NOMB_ARTI) AS T1 WHERE ROUND(C_ENTRADA," & Indec & ")>ROUND(C_SALIDA," & Indec & ")" 'HRR M4050 Se agrega el round y la condicion sea >
            StSql = StSql & " GROUP BY TX_NOMB_UNVE,TX_NOMB_BODE,CD_CODI_ARTI,NO_NOMB_ARTI) AS T1 WHERE ROUND(C_ENTRADA," & InDec & ")>ROUND(C_SALIDA," & InDec & ")" 'JACC M6654
            BD(BDCurCon).CommandTimeout = 0 'JLPB T42280
            Result = ExecSQL(StSql)
            'consulta = SQL & " AND " & consulta & " ORDER BY 5,7 DESC,15 DESC"
         End If
      End If
      'HRR M4050
      Call ListarD("infKARDEXCON.rpt", "", crptToWindow, "KARDEX", MDI_Inventarios, True)
   End If
   ''''''''''REOL603
   'Debug.Print strARTICULOS
   'Call ListarD("infKARDEX" & IIf(Me.opcCON, "CON", "SIN") & ".rpt", strARTICULOS, crptToWindow, "KARDEX", MDI_Inventarios, True)
   MDI_Inventarios.Crys_Listar.Formulas(9) = ""
   MDI_Inventarios.Crys_Listar.Formulas(10) = ""
   MDI_Inventarios.Crys_Listar.Formulas(11) = ""
End Sub

Private Sub Form_Load()
'    Call Main
    Call CenterForm(MDI_Inventarios, Me)
    Dueno.IniXNit (Dueno.Nit)
    Me.cboTIPO.Clear
    Me.cboTIPO.AddItem "Para la Venta"
    Me.cboTIPO.AddItem "Para El Consumo"
    NumEnCombo = Me.cboTIPO.ListIndex
    If Len(FormatDateTime(Date, vbShortDate)) = 8 Then
        Me.txtFCDESDE.Mask = "##/##/##"
        Me.txtFCHASTA.Mask = "##/##/##"
    Else
        Me.txtFCDESDE.Mask = "##/##/####"
        Me.txtFCHASTA.Mask = "##/##/####"
    End If
    Me.txtFCDESDE.Text = FormatDateTime(DateAdd("d", -(Day(Date) - 1), Date), vbShortDate)
    Me.txtFCHASTA.Text = FormatDateTime(DateAdd("d", -1, DateAdd("M", 1, DateAdd("d", -(Day(Date) - 1), Date))), vbShortDate)
    Me.chkFCRANGO.value = False
    Me.txtFCDESDE.Enabled = False
    Me.txtFCHASTA.Enabled = False
    Me.lstBODEGAS.ListItems.Clear
    Me.lstBODEGAS.Checkboxes = False
    Me.lstBODEGAS.MultiSelect = True
    Me.lstBODEGAS.HideSelection = False
    Me.lstBODEGAS.ColumnHeaders.Clear
    Me.lstBODEGAS.ColumnHeaders.Add , "NOM", "Nombre de la Bodega", 0.66 * Me.lstBODEGAS.Width
    Me.lstBODEGAS.ColumnHeaders.Add , "COD", "C�digo", 0.34 * Me.lstBODEGAS.Width
    
    Me.lstARTICULOS.ListItems.Clear
    Me.lstARTICULOS.Checkboxes = False
    Me.lstARTICULOS.MultiSelect = True
    Me.lstARTICULOS.HideSelection = False
    Me.lstARTICULOS.Width = 10500
    Me.lstARTICULOS.ColumnHeaders.Clear
    Me.lstARTICULOS.ColumnHeaders.Add , "NOAR", "Nombre del art�culo", 0.4 * Me.lstARTICULOS.Width
    Me.lstARTICULOS.ColumnHeaders.Add , "COAR", "C�digo", 0.15 * Me.lstARTICULOS.Width
    Me.lstARTICULOS.ColumnHeaders.Add , "NOCO", "Unidad de conteo", 0.15 * Me.lstARTICULOS.Width
    Me.lstARTICULOS.ColumnHeaders.Add , "NOUS", "Uso", 0.15 * Me.lstARTICULOS.Width
    Me.lstARTICULOS.ColumnHeaders.Add , "NOGR", "Grupo", 0.15 * Me.lstARTICULOS.Width
    Me.lstARTICULOS.ColumnHeaders.Add , "AUCO", "AutoUNC", 0
    Me.lstARTICULOS.ColumnHeaders.Add , "COGR", "CodGRU", 0
    Me.lstARTICULOS.ColumnHeaders.Add , "COUS", "CodUSO", 0
FALLO:
End Sub

Private Sub cboTIPO_LostFocus()
    If NumEnCombo = Me.cboTIPO.ListIndex Then Exit Sub
    Dim tipo As String * 1
    Me.lstBODEGAS.ListItems.Clear
    Me.lstARTICULOS.ListItems.Clear
    Me.pgbLLEVO.value = 0
    Me.pgbLLEVO.Visible = True
    Select Case Me.cboTIPO.ListIndex
    Case Is = -1
        tipo = "0"
    Case Is = 0
        tipo = "V"
    Case Is = 1
        tipo = "C"
    End Select
    Campos = "NU_AUTO_BODE, NU_AUTO_BODE_BODE, TX_CODI_BODE, TX_NOMB_BODE, " & _
        "CD_CODI_CECO_BODE, TX_VENTA_BODE"
    Desde = "IN_BODEGA"
    Condi = "TX_VENTA_BODE='" & tipo & "' ORDER BY TX_NOMB_BODE"
    ReDim ArrUXB(5, 0)
    Result = LoadMulData(Desde, Campos, Condi, ArrUXB())
    If Result = FAIL Then GoTo FALLO
    On Error GoTo SIGUI
    For CnTdr = 0 To UBound(ArrUXB, 2)
        Me.lstBODEGAS.ListItems.Add , "B" & ArrUXB(2, CnTdr), ArrUXB(3, CnTdr)
        Me.lstBODEGAS.ListItems("B" & ArrUXB(2, CnTdr)).ListSubItems.Add , "COD", ArrUXB(2, CnTdr)
SIGUI:
    Next
    On Error GoTo 0
    'JLPB T22311 INICIO EL SIGUIENTE BLOQUE SE DEJA EN COMENTARIO
    'Campos = "DISTINCT NU_AUTO_ARTI, CD_CODI_ARTI, NO_NOMB_ARTI, NU_AUTO_UNVE_ARTI, TX_PARA_ARTI, " & _
        "DE_DESC_GRUP, DE_DESC_USOS, TX_NOMB_UNVE, CD_GRUP_ARTI, CD_USOS_ARTI, " & _
        "NU_MULT_UNVE, NU_DIVI_UNVE"
        
    Campos = "DISTINCT TOP 32767 NU_AUTO_ARTI, CD_CODI_ARTI, NO_NOMB_ARTI, NU_AUTO_UNVE_ARTI, TX_PARA_ARTI, " & _
        "DE_DESC_GRUP, DE_DESC_USOS, TX_NOMB_UNVE, CD_GRUP_ARTI, CD_USOS_ARTI, " & _
        "NU_MULT_UNVE, NU_DIVI_UNVE"
    'JLPB T22311 FIN
    
    Desde = "ARTICULO, USOS, GRUP_ARTICULO, IN_UNDVENTA, IN_DETALLE"
    Condi = "CD_CODI_GRUP = CD_GRUP_ARTI AND CD_CODI_USOS = CD_USOS_ARTI"
    Condi = Condi & " AND NU_AUTO_UNVE = NU_AUTO_UNVE_ARTI"
    Condi = Condi & " AND NU_AUTO_ARTI = NU_AUTO_ARTI_DETA"
    Condi = Condi & " AND TX_PARA_ARTI='" & tipo & "' ORDER BY NO_NOMB_ARTI"
    ReDim ArrUXB(11, 0)
    Result = LoadMulData(Desde, Campos, Condi, ArrUXB())
    If Result = FAIL Then GoTo FALLO
    If Not Encontro Then GoTo NOENC
    Me.pgbLLEVO.Min = 0
    Me.pgbLLEVO.value = 0
    Me.pgbLLEVO.Max = UBound(ArrUXB, 2) + 1
    Dim Articulo As New UnArticulo
    For CnTdr = 0 To UBound(ArrUXB, 2)
        Me.pgbLLEVO.value = CnTdr
        Articulo.AutoNumero = ArrUXB(0, CnTdr)
        Articulo.Codigo = ArrUXB(1, CnTdr)
        Articulo.Nombre = ArrUXB(2, CnTdr)
        Articulo.AutoUDConteo = ArrUXB(3, CnTdr)
        Articulo.NombreGrupo = ArrUXB(5, CnTdr)
        Articulo.NombreUso = ArrUXB(6, CnTdr)
        Articulo.NombreUndConteo = ArrUXB(7, CnTdr)
        Articulo.ParaQueEs = InStr(1, "VC", ArrUXB(4, CnTdr)) - 1
        Articulo.CodigoGrupo = ArrUXB(8, CnTdr)
        Articulo.CodigoUso = ArrUXB(9, CnTdr)
        Me.lstARTICULOS.ListItems.Add , "A" & Articulo.AutoNumero, Trim(Articulo.Nombre)
        Me.lstARTICULOS.ListItems("A" & Articulo.AutoNumero).Tag = CStr(Articulo.AutoNumero)
'lstArticulos, NOAR, COAR, NOCO, NOUS, NOGR, AUCO, COGR, COUS, MUCO, DICO
        Me.lstARTICULOS.ListItems("A" & Articulo.AutoNumero).ListSubItems.Add , "COAR", Trim(Articulo.Codigo)
        Me.lstARTICULOS.ListItems("A" & Articulo.AutoNumero).ListSubItems.Add , "NOCO", Trim(Articulo.NombreUndConteo)
        Me.lstARTICULOS.ListItems("A" & Articulo.AutoNumero).ListSubItems.Add , "NOUS", Trim(Articulo.NombreUso)
        Me.lstARTICULOS.ListItems("A" & Articulo.AutoNumero).ListSubItems.Add , "NOGR", Trim(Articulo.NombreGrupo)
        Me.lstARTICULOS.ListItems("A" & Articulo.AutoNumero).ListSubItems.Add , "AUCO", Trim(Articulo.AutoUDConteo)
        Me.lstARTICULOS.ListItems("A" & Articulo.AutoNumero).ListSubItems.Add , "COGR", Trim(Articulo.CodigoGrupo)
        Me.lstARTICULOS.ListItems("A" & Articulo.AutoNumero).ListSubItems.Add , "COUS", Trim(Articulo.CodigoUso)
    Next
    Set Articulo = Nothing
NOENC:
FALLO:
    Me.pgbLLEVO.Visible = False
End Sub

Private Sub lstARTICULOS_ColumnClick(ByVal ColumnHeader As MSComctlLib.ColumnHeader)
    Me.lstARTICULOS.SortKey = ColumnHeader.Index - 1
    Me.lstARTICULOS.Sorted = True
End Sub

Private Sub txtFCDESDE_LostFocus()
    If IsDate(Me.txtFCDESDE.Text) Then Exit Sub
    Me.txtFCDESDE.Text = FormatDateTime(DateAdd("d", -(Day(Date) - 1), Date), vbShortDate)
    Me.txtFCDESDE.SetFocus
End Sub
Private Sub txtFCHASTA_LostFocus()
    'smdl m1876
    If IsDate(Me.txtFCHASTA.Text) Then
       If DateDiff("d", CDate(Me.txtFCDESDE.Text), CDate(Me.txtFCHASTA)) < 0 Then
         Call Mensaje1("La fecha final debe ser mayor a la inicial.", 3)
         Me.txtFCHASTA.SetFocus
       End If
    Else
    'smdl m1876
       Me.txtFCHASTA.Text = FormatDateTime(DateAdd("d", -1, DateAdd("M", 1, DateAdd("d", -(Day(Date) - 1), Date))), vbShortDate)
       Me.txtFCHASTA.SetFocus
    End If
End Sub


