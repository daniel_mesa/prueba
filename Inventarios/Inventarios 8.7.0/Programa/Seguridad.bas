Attribute VB_Name = "Seguridad"
Option Explicit
'Dim TxtCnt As TextBox      'DEPURACION DE CODIGO

Type BytePack '|.DR.|
    btValue(7) As Byte '8 Valores
End Type

Global Const cSep As String = "|", cFPAth As String = "cntsiste.cnt\CntSeg.cnt" '|.DR.|, R:1400
Global cGUsuario As String, cGClave As String '|.DR.|, R:1400
Global cFile As String
Function fnBinToHex(cBin As String) As String '|.DR.|
    
    Dim cPar As String, vI As Long
    Dim cResult As String
    
    For vI = 1 To Len(cBin)
         cPar = Hex(Asc(Mid(cBin, vI, 2)))
         cPar = String(2 - Len(cPar), "0") & cPar
         cResult = cResult & cPar
    Next vI
    
    fnBinToHex = cResult
    
End Function


Function fnHexToBin(cHex As String) As String '|.DR.|
    
    Dim cPar As String * 2, vI As Long
    Dim cResult As String
    
    For vI = 1 To Len(cHex) Step 2
        cPar = Mid(cHex, vI, 2)
        cResult = cResult & Chr(Val("&H" & cPar))
    Next vI
    
    fnHexToBin = cResult
    
End Function


Function fnEscribirClave(ByVal cFile As String, ByVal cUsuario As String, ByVal cClave As String) As Boolean '|.DR.|
    
    Dim cFChain As String, cSalida As String * 16384, cTemp As String
    Dim vTama�o As Integer, vPos As Integer
    

On Error GoTo Fall�

    Randomize Timer
    
    cFChain = cUsuario & cSep & cClave
    cTemp = fnEncriptar(cFChain, True): vTama�o = Len(cTemp)
    cSalida = fnRandBytes(3781) & cTemp & fnRandBytes(12603 - Len(cTemp))
    vPos = Int(Rnd * 3000) + 26
    
    
    Open cFile For Binary As #1
        Put #1, 1, cSalida
        Put #1, 17, vPos
        Put #1, vPos, vTama�o
    Close #1
    
    fnEscribirClave = True
    
    Exit Function

Fall�:
    If FreeFile = 2 Then Close #1
    fnEscribirClave = False
    
End Function

Function fnLeerClave(ByVal cFile As String, ByRef cUsuario As String, ByRef cClave As String) As Boolean '|.DR.|
    
    Dim cEntrada As String * 16384, cTemp As String
    Dim vTama�o As Integer, vPos As Integer

On Error GoTo Fall�

    If Dir(cFile) = "" Then GoTo Fall�

    Open cFile For Binary As #1
        Get #1, 1, cEntrada
        Get #1, 17, vPos
        Get #1, vPos, vTama�o
    Close #1
    
    cTemp = fnDEncriptar(Mid(cEntrada, 3782, vTama�o), True): vTama�o = Len(cTemp)
    
    cUsuario = Mid(cTemp, 1, InStr(1, cTemp, cSep) - 1)
    cClave = Mid(cTemp, InStr(1, cTemp, cSep) + 1)
    
    fnLeerClave = True
    
    Exit Function

Fall�:
    If FreeFile = 2 Then Close #1
    fnLeerClave = False

End Function
Function fnRandBytes(lgSize As Long) As String '|.DR.|
    
    Dim i As Long, cResult As String
    
    Randomize Timer
    
    For i = 1 To lgSize
        cResult = cResult & Chr(Int(Rnd * 256))
    Next i
    
    fnRandBytes = cResult
    
End Function

Function fnDShuffle(ByVal vModo As Byte, ByRef cCad1 As String, ByRef cCad2 As String, ByVal cTotal As String) As Boolean '|.DR.|
'Usado para encriptar cadenas.
'Esta instrucci�n retorna una cadena de 16 bytes, y recibe 2 de 8.
    Dim bypPack As BytePack, i%, j%
    
    Randomize Timer
    
        cCad1 = ""
        cCad2 = ""

        sbUnPackByte vModo, bypPack
    
            j = 1
        For i = 0 To 7
            If bypPack.btValue(i) = 1 Then cCad1 = cCad1 & Mid(cTotal, j, 2)
            If bypPack.btValue(i) = 0 Then cCad2 = cCad2 & Mid(cTotal, j, 2)
            j = j + 2
        Next i
    
    
End Function

Sub sbUnPackByte(btDato As Byte, ByRef bypPack As BytePack)
'Rellena la estructura bypPack, seg�n el n�mero entregado

Dim i%
    
    For i% = 0 To 7
        If (btDato And (2 ^ i%)) = (2 ^ i%) Then
            bypPack.btValue(i%) = 1
        Else
            bypPack.btValue(i%) = 0
        End If
     Next i%
    
   
End Sub

Function fnShuffle(ByRef vMode As Byte, cCad1 As String, cCad2 As String) As String '|.DR.|
'Usado para encriptar cadenas.
'Esta instrucci�n retorna una cadena de 16 bytes, y recibe 2 de 8.
    Dim bypPack As BytePack, i%, j%, k%
    Dim cResult As String
    
    Randomize Timer
    
'Seleccionando orden:
    Do
        vMode = Int(Rnd * 256)
        sbUnPackByte vMode, bypPack
        j = 0
        For i = 0 To 7
            j = j + bypPack.btValue(i)
        Next i
    Loop Until j = 4
    
            j = 1: k = 1
        For i = 0 To 7
            If bypPack.btValue(i) = 1 Then cResult = cResult & Mid(cCad1, j, 2): j = j + 2
            If bypPack.btValue(i) = 0 Then cResult = cResult & Mid(cCad2, k, 2): k = k + 2
        Next i
    
        fnShuffle = cResult
    
End Function

Function fnDEncriptar(ByVal cCadena As String, Optional fBinary As Boolean) As String '|.DR.|
    
    Dim cResult As String, i%, vT As Currency, vTemp As Long
'    Dim vX As Long, vCoef As Long, cFocus As String
    Dim cFocus As String        'DEPURACION DE CODIGO
'    Dim cVal As String, cCoe As String, cCad1 As String, cCad2 As String
    Dim cCad1 As String, cCad2 As String         'DEPURACION DE CODIGO
    Dim vMode As Byte
    
    
    If fBinary Then cCadena = fnBinToHex(cCadena) 'Modo binario
    
    For i = 1 To Len(cCadena) Step 18
        cFocus = Mid(cCadena, i, 18)
        vMode = Val("&H" & Mid(cFocus, 17, 2))
        fnDShuffle vMode, cCad1, cCad2, Mid(cFocus, 1, 16)
        vTemp = Val("&H" & cCad2)
        vT = Int((Val("&H" & cCad1) + 2147483647) / vTemp)
        cResult = cResult & Chr(vT)
    Next i
    
        fnDEncriptar = cResult
    
End Function

Function fnEncriptar(cCadena As String, Optional fBinary As Boolean) As String '|.DR.|
    
    Dim cResult As String, i%, vT As Currency
    Dim vX As Long, vTemp As Long, vModex As Byte, cMode As String
    Dim cVal As String, cCoe As String
    Dim cValT As String, cCoeT As String
    
    Randomize Timer
    
    For i = 1 To Len(cCadena)
        cValT = "": cCoeT = ""
        vTemp = Int(Rnd * 15870248) + 1
        vT = CCur(Asc(Mid(cCadena, i, 1))) * vTemp
        vX = CLng(vT - 2147483647)
        cVal = Hex(vX): cVal = String(8 - Len(cVal), "0") & cVal
        cCoe = Hex(vTemp): cCoe = String(8 - Len(cCoe), "0") & cCoe
        cValT = cValT & cVal
        cCoeT = cCoeT & cCoe
    
        cResult = cResult & fnShuffle(vModex, cValT, cCoeT)
        cMode = Hex$(vModex)
        cResult = cResult & String(2 - Len(cMode), "0") & cMode
    Next i
        
       
    If fBinary Then 'Modo binario
        fnEncriptar = fnHexToBin(cResult)
    Else 'Modo texto
        fnEncriptar = cResult
    End If
    
End Function

Function ValPwd() As Integer
ReDim Arr(1)
   
   Result = LoadData(NombTabUsr, "NU_AUTO_USUA,TX_PASS_USUA", "TX_IDEN_USUA=" & Comi & UCase(UserId) & Comi, Arr())
   If (Result <> FAIL) Then
      If (Encontro) Then
         Result = IIf(UCase(UserPass) = UCase(Arr(1)), SUCCEED, FAIL)
         AutonumUser = Arr(0)
         'Hacer la habilitaci�n de menus
'         If UCase(UserId) = "ADMINISTRADOR" Then
'            MDI_Inventarios.mnumantenimiento.Visible = True
'         Else
'          '  MDI_Inventarios.mnumantenimiento.Visible = False
'         End If
      Else
'         Result = IIf(UCase(UserId) = "DESARROLLO" And UserPass = "TRIACON", SUCCEED, FAIL)
'         If Result = SUCCEED Then Result = MsgBox("Solucionar", vbAbortRetryIgnore) 'MDI_Inventarios.mnumantenimiento.Visible = True
      End If
   ElseIf (Result <> FAIL) Then
      Result = FAIL
   End If
   ValPwd = Result
End Function
Function CargaAutonumPerfil() As Integer
    Campos = "NU_AUTO_PERF_USUA"
    Condicion = "NU_AUTO_USUA=" & AutonumUser
    Desde = "USUARIO"
    If (DoSelect(Desde, Campos, Condicion) <> FAIL) Then
        Result = FAIL 'NMSR M3486
        If (ResultQuery()) Then
            If DataQuery(1) <> NUL$ Then 'NMSR M3486
                AutonumPerfil = DataQuery(1)
            'NMSR M3486
                Result = SUCCEED
            End If 'NMSR M3486
        End If
        'Result = SUCCEED 'NMSR M3486
    Else
        Result = FAIL
    End If
    CargaAutonumPerfil = Result
End Function



'Sub Leer_Permisos(Codigo As String, Optional BotonG As SSCommand, Optional BotonB As SSCommand, Optional BotonI As SSCommand)
'' La siguiente funcion permite la habilitacion o desabilitacion
'' de los botones de cada formulario por lo cual se llamara desde
'' cada uno
'Dim arr(2)
'
'   BDCurCon = 0
'   Campos = "ID_CREA_PERM, ID_ANUL_PERM, ID_IMPR_PERM"
'   Desde = "PERFILES,PERMISOS"
'   Condicion = "PERFILES.CD_CODI_PERF = PERMISOS.CD_CODI_PERF_PERM"
'   Condicion = Condicion & " AND PERFILES.CD_CODI_PERF= '" & PerfilID & Comi & " AND PERMISOS.CD_CODI_OPCI_PERM = '" & Codigo & Comi
'
'   Result = LoadData(Desde, Campos, Condicion, arr())
'   If Result <> FAIL Then
'      If Encontro Then
'        On Error Resume Next
'        If BotonG.Enabled = True Then
'            BotonG.Enabled = IIf(arr(0) = "S", True, False)
'        End If
'        If BotonB.Enabled = True Then
'            BotonB.Enabled = IIf(arr(1) = "S", True, False)
'        End If
'        If BotonI.Enabled = True Then
'            BotonI.Enabled = IIf(arr(2) = "S", True, False)
'        End If
'         With MDI_Inventarios
'              If BotonG.Enabled = False Then
'                   'BotonG.Picture = .imgbotones.ListImages(1).Picture
'                   'BotonG.Caption = "&Guardar"
'                   'BotonG.Outline = False
'              End If
'              If BotonB.Enabled = False Then
'                   'BotonB.Picture = .imgbotones.ListImages(2).Picture
'                   'BotonB.Caption = "&Borrar"
'                   'BotonB.Outline = False
'              End If
'              If BotonI.Enabled = False Then
'                   ''BotonI.Picture = .imgbotones.ListImages(3).Picture
'                   'BotonI.Caption = "&Imprimir"
'                   'BotonI.Outline = False
'              End If
'          End With
'
'      Else
''          Unload Frm
''         BotonG.Enabled = False
''         BotonB.Enabled = False
''         BotonI.Enabled = False
'      End If
'   Else
'      Call Mensaje1("Error leyendo permisos !!", 1)
''      Unload Frm
''      BotonG.Enabled = False
''      BotonB.Enabled = False
''      BotonI.Enabled = False
'   End If
' BDCurCon = 1
'End Sub

Sub Permiso_Consulta(Codigo As String, accesoc As Integer)
Dim Arr(0)
'Public accesoc As Integer
   BDCurCon = 0
   accesoc = 1
   
   Campos = "ID_CONS_PERM"
'   Condicion = "PERFILES.CD_CODI_PERF= '" & PerfilID & Comi & " AND PERMISOS.CD_CODI_OPCI_PERM = '" & Codigo & Comi
'   Desde = "PERFILES INNER JOIN PERMISOS ON PERFILES.CD_CODI_PERF = PERMISOS.CD_CODI_PERF_PERM"
   
   Desde = "PERFILES,PERMISOS"
   Condicion = "PERFILES.CD_CODI_PERF = PERMISOS.CD_CODI_PERF_PERM"
   Condicion = Condicion & " AND PERFILES.CD_CODI_PERF= '" & AutonumPerfil & Comi & " AND PERMISOS.CD_CODI_OPCI_PERM = '" & Codigo & Comi
   
   Result = LoadData(Desde, Campos, Condicion, Arr())
   If Result <> FAIL Then
      If Encontro Then
         If Arr(0) = "S" Then
               accesoc = 1
         Else
               Call Mensaje1("No tiene acceso a esta opci�n... Consulte al administrador de la aplicaci�n", 2)
               accesoc = 0
         End If
      Else
         Call Mensaje1("Esta opci�n no se encuentra registrada... Consulte al administrador de la aplicaci�n", 2)
         accesoc = 0
      End If
   End If
   BDCurCon = 1
End Sub

'DEPURACION DE CODIGO
'Function submenus(Opcion As Variant)
'End Function

Sub Deshabilitar_Menu()
End Sub

Function descargarmenu()
    
End Function

Sub PermisoFrm(CodOpc As String, Frm As Form)
Dim access As Integer
Call Permiso_Consulta(CodOpc, access)
If access = 1 Then
   Frm.Show
   Frm.SetFocus
End If
End Sub

Sub CrearOpcionesPerf(ByVal AutoPerf As Long)
Campos = AutoPerf & " AS NU_AUTO_PERF_PERM,NU_AUTO_OPCI AS NU_AUTO_OPCI_PERM,"
Campos = Campos & "'N' AS TX_CREA_PERM, 'N' AS TX_ANUL_PERM, 'N' AS TX_CONS_PERM,"
Campos = Campos & "'N' AS TX_IMPR_PERM, 'N' AS TX_OTRO_PERM, 'N' AS TX_OTR1_PERM"
Result = DoInsertFromSelectSql("PERMISO", "OPCION", Campos, NUL$)
End Sub


Sub CrearOpcion(OpcionC As Variant)
Dim ArrTemp() As Variant
Dim AutoOpc As Long
ReDim ArrTemp(0)
Result = LoadData("OPCION", "NU_AUTO_OPCI", "TX_CODI_OPCI=" & Comi & OpcionC & Comi, ArrTemp())
If Result <> FAIL Then
    AutoOpc = ArrTemp(0)
    Campos = "NU_AUTO_PERF AS NU_AUTO_PERF_PERM," & AutoOpc & " AS NU_AUTO_OPCI_PERM,"
    Campos = Campos & "'N' AS TX_CREA_PERM, 'N' AS TX_ANUL_PERM, 'N' AS TX_CONS_PERM,"
    Campos = Campos & "'N' AS TX_IMPR_PERM, 'N' AS TX_OTRO_PERM, 'N' AS TX_OTR1_PERM"
    Result = DoInsertFromSelectSql("PERMISO", "PERFIL", Campos, NUL)
    Campos = "TX_CREA_PERM='S',TX_ANUL_PERM='S',TX_CONS_PERM='S',TX_IMPR_PERM='S'"
    Campos = Campos & Coma & "TX_OTRO_PERM='N',TX_OTR1_PERM='N'"
    Condicion = "NU_AUTO_PERF_PERM=1 AND NU_AUTO_OPCI_PERM=" & AutoOpc
    Result = DoUpdate("PERMISO", Campos, Condicion)
End If
End Sub

Sub EliminarOpcion(OpcionC As Variant)
    Condi = "CD_CODI_OPCI_PERM=" & Comi & OpcionC & Comi
    Result = DoDelete("PERMISOS", Condi)
    Condi = "CD_CODI_OPCI=" & Comi & OpcionC & Comi
    Result = DoDelete("OPCIONES", Condi)
End Sub

Sub HabilitaActualizacion()
Dim arropc() As Variant
ReDim arropc(0)
Campos = "CD_CODI_OPCI_PERM"
Condicion = "CD_CODI_PERF = '" & AutonumPerfil & Comi
Condicion = Condicion & " AND OPCIONES.CD_CODI_OPCI = PERMISOS.CD_CODI_OPCI_PERM AND PERFILES.CD_CODI_PERF = PERMISOS.CD_CODI_PERF_PERM"
Condicion = Condicion & " AND (PERMISOS.ID_CREA_PERM='S'"
Condicion = Condicion & " OR  PERMISOS.ID_ANUL_PERM='S'"
Condicion = Condicion & " OR  PERMISOS.ID_CONS_PERM='S'"
Condicion = Condicion & " OR  PERMISOS.ID_IMPR_PERM='S'"
Condicion = Condicion & " OR  PERMISOS.ID_OTRO_PERM='S'"
Condicion = Condicion & " OR  PERMISOS.ID_OTR1_PERM='S')"
Condicion = Condicion & " AND CD_CODI_OPCI='10000401'"
Desde = "PERFILES, OPCIONES, PERMISOS "
Result = LoadData(Desde, Campos, Condicion, arropc())
If Encontro Then
   MDI_Inventarios.MnuAdministracion.Tag = True
'   Call submenus(arropc(0))    'DEPURACION DE CODIGO
End If
End Sub

''carga arbol para el Treeview control desde la tabla opciones
'Function Carga_Arbol(ByVal TableName As String, ByVal Whatsel As String, condi As String, Treeopc As TreeView)
'    Dim nodoarbol As Node
'    Dim nodo As Node
'    Dim Origen As String
'    Dim i As Integer
'    Dim j As Integer
'    Dim N As Integer
'    Dim l As Integer
'    Dim ind As Integer
'    Dim S, CodiOpc, NombOpc, PadrOpc As String
'    ind = 0
'    On Error GoTo 0
'    If (DoSelect(TableName, Whatsel, condi) <> FAIL) Then
'        If (ResultQuery()) Then
'         Call MouseClock
'         Do While (NextRowQuery())
'            'On Error Resume Next
'            If CancelTrans = 1 Then Exit Do
'            'DoEvents
'            S = NUL$
'            i = i + 1
'            'Se ha llegado al m�ximo soportado por Visual Basic para un grid?
'
'            'creaci�n nodo raiz principal nodo padre
'            If ind = 0 Then
'                Set nodoarbol = Treeopc.Nodes.Add(, , "CNT", UCase(App.Title), 1, 1)
'                Treeopc.Nodes(Treeopc.Nodes.Count).EnsureVisible
'                ind = 1
'            End If
'            CodiOpc = DataQuery(1)
'            NombOpc = DataQuery(2)
'            PadrOpc = Comi & DataQuery(3) & Comi
'           If CodiOpc = "09" Or CodiOpc = "14" Then MsgBox ("aa")
'            'creaci�n nodo segundario nodo hijo 2 nivel
'            If PadrOpc = "'CNT'" Then
'                Set nodoarbol = Treeopc.Nodes.Add("CNT", 4, Comi & CodiOpc & Comi, NombOpc, 2, 3)
'                Treeopc.Nodes(Treeopc.Nodes.Count).EnsureVisible
'                Treeopc.Style = tvwTreelinesPlusMinusPictureText     ' Estilo 7.
'                Treeopc.LineStyle = tvwRootLines  ' Estilo 1
'
'            'creacion nodo terciario hacia adelante,  nodos nietos o nivel 3 hacia adelante
'            Else
'                Origen = PadrOpc
'                Set nodo = FindInTreeView(Treeopc, Origen)
'                If Not (nodo Is Nothing) Then
'                  Set nodoarbol = Treeopc.Nodes.Add(Origen, 4, Comi & CodiOpc & Comi, NombOpc, 4, 5)
'                  'TreeOpc.Nodes(TreeOpc.Nodes.Count).EnsureVisible
'                  Treeopc.Style = tvwTreelinesPlusMinusPictureText     ' Estilo 7.
'                  Treeopc.LineStyle = tvwRootLines  ' Estilo 1
'               End If
'            End If
'         Loop
'         Call MouseNorm
'         'On Error Resume Next
'        End If
'      Result = SUCCEED
'   Else
'      Result = FAIL
'   End If
'End Function

Function Carga_Arbol(ByVal TableName As String, ByVal Whatsel As String, Condi As String, TreeOpc As TreeView)
Dim nodoArbol As Node
'Dim nodo As Node       'DEPURACION DE CODIGO
Dim CodiOpc As String
Dim NombOpc As String
Dim PadrOpc As String
   With TreeOpc
      .Nodes.Clear
      Set nodoArbol = TreeOpc.Nodes.Add(, , "CNT", UCase(App.Title), 1, 1)
      .Nodes(.Nodes.Count).EnsureVisible
      ReDim Arr(2, 0)
      Dim i As Long
      'TableName = "OPCION, PERMISO, USUARIO"
      'Whatsel = "TX_CODI_OPCI, TX_DESC_OPCI, TX_CODPADR_OPCI,0"
      'condi = "NU_AUTO_OPCI = NU_AUTO_OPCI_PERM AND NU_AUTO_PERF_PERM = NU_AUTO_PERF_USUA"
      'condi = condi & " AND NU_AUTO_USUA = " & AutonumUser & " AND TX_CONS_PERM = 'S'"
      'condi = condi & " ORDER BY TX_CODI_OPCI"
      Result = LoadMulData(TableName, Whatsel, Condi, Arr)
      If Result <> FAIL And Encontro Then
         For i = 0 To UBound(Arr, 2)
            If Arr(2, i) = "CNT" Then
            CodiOpc = Arr(0, i)
            NombOpc = Arr(1, i)
            PadrOpc = Comi & Arr(2, i) & Comi
            Set nodoArbol = TreeOpc.Nodes.Add("CNT", 4, Comi & CodiOpc & Comi, NombOpc, 2, 3)
            .Nodes(.Nodes.Count).EnsureVisible
            .Style = tvwTreelinesPlusMinusPictureText     ' Estilo 7.
            .LineStyle = tvwRootLines  ' Estilo 1
            Call CargarHijo(CodiOpc, TreeOpc, Arr)
            End If
         Next i
      End If
   End With
End Function

Private Sub CargarHijo(ByVal Padre As String, TreeOpc As TreeView, Arr)
Dim nodo As Node
Dim nodoArbol As Node
Dim i As Long
   For i = 0 To UBound(Arr, 2)
      If Arr(2, i) = Padre Then
         Set nodo = FindInTreeView(TreeOpc, Comi & CStr(Arr(2, i)) & Comi)
         If Not (nodo Is Nothing) Then
            Set nodoArbol = TreeOpc.Nodes.Add(Comi & Padre & Comi, 4, Comi & CStr(Arr(0, i)) & Comi, Arr(1, i), 4, 5)
            TreeOpc.Style = tvwTreelinesPlusMinusPictureText     ' Estilo 7.
            TreeOpc.LineStyle = tvwRootLines  ' Estilo 1
            Call CargarHijo(CStr(Arr(0, i)), TreeOpc, Arr)
         End If
      End If
   Next i
End Sub

Public Sub Estado_Botones_Forma(ByRef obj As Object, ByVal Opcion As String)
   ReDim Arr(4)
   Dim i     As Integer
   Dim tItem As ListItem
   Dim seg   As String
   Dim j     As Integer
   
   Opcion = opcUsu
   Desde = "OPCION,PERMISO,USUARIO"
   Campos = "TX_CREA_PERM,TX_ANUL_PERM,TX_IMPR_PERM,TX_OTRO_PERM,TX_OTR1_PERM"
   Condicion = "NU_AUTO_OPCI=NU_AUTO_OPCI_PERM"
   Condicion = Condicion & " AND NU_AUTO_PERF_PERM=NU_AUTO_PERF_USUA"
   Condicion = Condicion & " AND NU_AUTO_USUA=" & AutonumUser
   Condicion = Condicion & " AND TX_CODI_OPCI=" & Comi & Opcion & Comi
   Condicion = Condicion & " ORDER BY TX_CODI_OPCI"
   Result = LoadData(Desde, Campos, Condicion, Arr)
   If TypeOf obj Is MSComctlLib.Toolbar Then
      'Dim boton As MSComctlLib.Button
      For i = 1 To obj.Buttons.Count
         Debug.Print obj.Buttons(i).Key
         Select Case obj.Buttons(i).Key
            Case "G":      'g: GRABAR,
                           obj.Buttons(i).Enabled = IIf(Arr(0) = "S", True, False) 'Grabar
            Case "C":
                           obj.Buttons(i).Visible = IIf(Arr(0) = "S", True, False)  'Grabar
            Case "B", "AN": 'b:BORRAR, AN:Anular
                           obj.Buttons(i).Enabled = IIf(Arr(1) = "S", True, False) 'Borrar
            Case "I":
                           obj.Buttons(i).Enabled = IIf(Arr(2) = "S", True, False) 'Imprimir
         End Select
      Next
   ElseIf TypeOf obj Is VB.CommandButton Then
      obj.Enabled = IIf(Arr(0) = "S", True, False) 'Grabar
   End If
   
   'Verifica si la opcion ya esta cargada en el Lstw_Formas
   On Error Resume Next
   Set tItem = MDI_Inventarios.Lstw_Formas.ListItems("L" & Opcion)
   If tItem Is Nothing Then
      For j = 0 To UBound(Arr)
         seg = seg & Arr(j)
      Next j
      MDI_Inventarios.Lstw_Formas.ListItems.Add , "L" & FrmMenu.CodOpc, seg
   End If
End Sub

'Sub Leer_Permisos(Codigo As String, Optional BotonG As SSCommand, Optional BotonB As SSCommand, Optional BotonI As SSCommand)
'' La siguiente funcion permite la habilitacion o desabilitacion
'' de los botones de cada formulario por lo cual se llamara desde
'' cada uno
'Dim arr(2)
'
'   BDCurCon = 0
'   Campos = "ID_CREA_PERM, ID_ANUL_PERM, ID_IMPR_PERM"
'   Desde = "PERFILES,PERMISOS"
'   Condicion = "PERFILES.CD_CODI_PERF = PERMISOS.CD_CODI_PERF_PERM"
'   Condicion = Condicion & " AND PERFILES.CD_CODI_PERF= '" & PerfilID & Comi & " AND PERMISOS.CD_CODI_OPCI_PERM = '" & Codigo & Comi
'
'   Result = LoadData(Desde, Campos, Condicion, arr())
'   If Result <> FAIL Then
'      If Encontro Then
'        On Error Resume Next
'        If BotonG.Enabled = True Then
'            BotonG.Enabled = IIf(arr(0) = "S", True, False)
'        End If
'        If BotonB.Enabled = True Then
'            BotonB.Enabled = IIf(arr(1) = "S", True, False)
'        End If
'        If BotonI.Enabled = True Then
'            BotonI.Enabled = IIf(arr(2) = "S", True, False)
'        End If
'         With MDI_Pacientes
'              If BotonG.Enabled = False Then
'                   BotonG.Picture = .imgbotones.ListImages(1).Picture
'                   BotonG.Caption = "&Guardar"
'                   BotonG.Outline = False
'              End If
'              If BotonB.Enabled = False Then
'                   BotonB.Picture = .imgbotones.ListImages(2).Picture
'                   BotonB.Caption = "&Borrar"
'                   BotonB.Outline = False
'              End If
'              If BotonI.Enabled = False Then
'                   BotonI.Picture = .imgbotones.ListImages(3).Picture
'                   BotonI.Caption = "&Imprimir"
'                   BotonI.Outline = False
'              End If
'          End With
'
'      Else
''          Unload Frm
''         BotonG.Enabled = False
''         BotonB.Enabled = False
''         BotonI.Enabled = False
'      End If
'   Else
'      Call Mensaje1("Error leyendo permisos !!", 1)
''      Unload Frm
''      BotonG.Enabled = False
''      BotonB.Enabled = False
''      BotonI.Enabled = False
'   End If
' BDCurCon = 1
'End Sub
'


Function ConexionSeguridad() As Integer
   ConexionSeguridad = SUCCEED
   If ValPwd1 = 0 Then
       ConexionSeguridad = FAIL
   End If
End Function

Function ValPwd1() As Integer
Dim Clave As String
ReDim Arr(0)
Dim Num1 As Integer
Dim Num2 As Integer
Dim Num3 As Integer
Dim Pos As Integer
Dim cad As String
Dim Cripto As Boolean  'Utilizar si o no el m�todo de encriptaci�n para validar la clave

   Num1 = 0
   Num2 = 0
   Num3 = 0
   
    ReDim Arr(0)
    Result = LoadData("PARAMETROS_INVE", "NU_NVERDB_PINV", NUL$, Arr)
    If Result <> FAIL Then
        If (Mid(Arr(0), 1, 1) >= 5 And Mid(Arr(0), 3, 1) >= 4 And Mid(Arr(0), 5, 1) >= 0) Or Mid(Arr(0), 1, 1) > 5 Then
             Cripto = True
        Else
             Cripto = False
        End If
    End If

    ReDim Arr(1)
    
    'JAGS T7123
    If ExisteCAMPO("USUARIO", "NU_ESTA_USUA") Then 'T8016 JAGS
    'Result = LoadData(NombTabUsr, "DE_PSWD_USUA", "CD_CODI_USUA=" & Comi & UCase(UserId) & Comi, Arr())
        Result = LoadData(NombTabUsr, "NU_AUTO_USUA,TX_PASS_USUA", "TX_IDEN_USUA=" & Comi & UCase(UserId) & Comi & "AND NU_ESTA_USUA <> '1'", Arr())
    Else
        Result = LoadData(NombTabUsr, "NU_AUTO_USUA,TX_PASS_USUA", "TX_IDEN_USUA=" & Comi & UCase(UserId) & Comi, Arr()) 'T8016 JAGS

    End If
    'Result = LoadData(NombTabUsr, "NU_AUTO_USUA,TX_PASS_USUA", "TX_IDEN_USUA=" & Comi & UCase(UserId) & Comi, Arr())
    'JAGS R7123
    
    If Result <> FAIL And Encontro Then
       AutonumUser = Arr(0)
       Clave = Arr(1)
       If Cripto = True Then
          Result = IIf(ConvertCadenaHexa(Encriptar(UCase(Trim(UserPass)), UCase(Trim(UserId)))) = Clave, SUCCEED, FAIL)    'CA-10/05
       Else
          Result = IIf(UCase(UserPass) = UCase(Arr(1)), SUCCEED, FAIL)
       End If
    Else
       Result = IIf(UCase(UserId) = "DESARROLLO" And UserPass = "TRIACON", SUCCEED, FAIL)
       'If Result = SUCCEED Then MDIConta.Mnu_Mantenimiento.Visible = True
    End If
   
   ValPwd1 = Result
End Function

