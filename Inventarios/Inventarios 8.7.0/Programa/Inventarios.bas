Attribute VB_Name = "Inventarios"
Option Explicit
Public Enum TipoDocumento
'NoDefinido = 0, Cotizacion = 1, AnulaCotizacion = 20, ODCompra = 2
'AnulaODCompra = 18, Entrada = 3, DevolucionEntrada = 4, AnulaEntrada = 13
'Requisicion = 5, AnulaRequisicion = 21, Despacho = 6, DevolucionDespacho = 17
'AnulaDespacho = 16, BajaConsumo = 8, DevolucionBaja = 9, AnulaBaja = 14
'Traslado = 10, AnulaTraslado = 23, Salida = 19, DevolucionSalida = 24
'AnulaSalida = 22, FacturaVenta = 11, DevolucionFacturaVenta = 12
'AnulaFacturaVenta = 15, CompraDElementos = 25, AprovechaDonacion = 26
'DevolucionAprovecha = 28, AnulaAprovecha = 27, ListaPreciosCotiza = 29
'AnulaListaPreciosCotiza = 30,AjusteXInflacion = 31
    NoDefinido = 0
    Cotizacion = 1
    AnulaCotizacion = 20
    ODCompra = 2
    AnulaODCompra = 18
    Entrada = 3
    DevolucionEntrada = 4
    AnulaEntrada = 13
    Requisicion = 5
    AnulaRequisicion = 21
    Despacho = 6
    DevolucionDespacho = 17
    AnulaDespacho = 16
    BajaConsumo = 8
    DevolucionBaja = 9
    AnulaBaja = 14
    Traslado = 10
    AnulaTraslado = 23
    Salida = 19
    DevolucionSalida = 24
    AnulaSalida = 22
    FacturaVenta = 11
    DevolucionFacturaVenta = 12
    AnulaFacturaVenta = 15
    CompraDElementos = 25
    AprovechaDonacion = 26
    DevolucionAprovecha = 28
    AnulaAprovecha = 27
    ListaPreciosCotiza = 29
    AnulaListaPreciosCotiza = 30
    AjusteXInflacion = 31
End Enum
Public Enum ContieneLista
    Minimo = 1
    Maximo = 2
    Reposicion = 3
    MaxDespachar = 4
    SalidasPeriodo = 5
    EntradasPeriodo = 6
    SaldoActual = 7
End Enum
Public Enum ResultConsulta
    Encontroinfo = 1
    NoEncontroinfo = 0
    Errorinfo = -1
End Enum

Public Enum EstaDelDocumento
    Normal = 0
    Modificado = 1
    Anulado = 2
End Enum

Public Sub BorrarColeccion(cll As Collection)
    Dim obj As Variant
    If cll Is Nothing Then Exit Sub
    For Each obj In cll
        cll.Remove 1
    Next
End Sub

Public Sub DivideXPrimos(ByRef nmr As Long, ByRef dnm As Long)
'    Dim CnTr As Byte, PrMs(15) As Byte
'    PrMs(0) = 2
'    PrMs(1) = 3
'    PrMs(2) = 5
'    PrMs(3) = 7
'    PrMs(4) = 11
'    PrMs(5) = 13
'    PrMs(6) = 17
'    PrMs(7) = 19
'    PrMs(8) = 23
'    PrMs(9) = 29
'    PrMs(10) = 31
'    PrMs(11) = 37
'    PrMs(12) = 41
'    PrMs(13) = 43
'    PrMs(14) = 47
'    CnTr = 0
'    While CnTr < 15
'        If PrMs(CnTr) * Int(nmr / PrMs(CnTr)) = nmr And PrMs(CnTr) * Int(dnm / PrMs(CnTr)) = dnm Then
'            nmr = nmr / PrMs(CnTr)
'            dnm = dnm / PrMs(CnTr)
'        Else
'            CnTr = CnTr + 1
'        End If
'    Wend
'HRR Depuracion 19/05/2009
Dim CnTr As Long
Dim LnMenor As Long

CnTr = 2
LnMenor = dnm
If nmr < dnm Then LnMenor = nmr
While CnTr < LnMenor
   If (nmr Mod CnTr = 0) And (dnm Mod CnTr = 0) Then
      nmr = nmr / CnTr
      dnm = dnm / CnTr
   Else
      CnTr = CnTr + 1
   End If
Wend
'HRR Depuracion 19/05/2009

End Sub

Public Sub ReHacerSaldos()
    Dim Cnxn As New ADODB.Connection
'    Dim Tabla As New ADODB.Recordset, Accion As New ADODB.Recordset
    Dim Tabla As New ADODB.Recordset        'DEPURACION DE CODIGO
'    Dim Inst As String, Cambio As String
    Dim Cambio As String        'DEPURACION DE CODIGO
'    Dim Primero As Boolean, FinArchivo As Boolean
    Dim Primero As Boolean      'DEPURACION DE CODIGO
    Dim Bodega As Long, Articulo As Long, Saldo As Long
    Dim Apuntador As Long, RegActual As Long, RegAnterior As Long
    Primero = True
    
    Cnxn.Provider = "SQLOLEDB"
    Cnxn.Properties("User ID") = "ADMIN"
    Cnxn.Properties("Password") = "TRIACON"
    Cnxn.Properties("Data Source") = "10.10.15.3"
    Cnxn.Properties("Initial Catalog") = "BDCPO"
    Cnxn.Open
    
    Tabla.Open "SELECT IN_KARDEX.* FROM IN_KARDEX " & _
        "INNER JOIN DOSAPUNTA ON (IN_KARDEX.NU_AUTO_BODE_KARD = DOSAPUNTA.NU_AUTO_BODE_KARD) " & _
        "AND (IN_KARDEX.NU_AUTO_ARTI_KARD = DOSAPUNTA.NU_AUTO_ARTI_KARD) WHERE " & _
        "(((IN_KARDEX.NU_AUTO_ORGCABE_KARD) > 279364)) ORDER BY IN_KARDEX.NU_AUTO_BODE_KARD, " & _
        "IN_KARDEX.NU_AUTO_ARTI_KARD, IN_KARDEX.NU_AUTO_KARD;", Cnxn, adOpenDynamic
    
    While Not Tabla.EOF
        RegActual = Tabla("NU_AUTO_KARD")
        If RegActual = 773231 Then
            Debug.Print "Parar"
        End If
        If Primero Then
            Bodega = Tabla("NU_AUTO_BODE_KARD")
            Articulo = Tabla("NU_AUTO_ARTI_KARD")
            Primero = False
            Saldo = 0
            Apuntador = 0
        End If
        If (Apuntador > 0 And Bodega = Tabla("NU_AUTO_BODE_KARD") And Articulo = Tabla("NU_AUTO_ARTI_KARD")) Then
'            Tabla.MovePrevious
'            Debug.Print Tabla("NU_AUTO_KARD"), RegActual, Tabla("NU_AUTO_BODE_KARD"), Tabla("NU_AUTO_ARTI_KARD"), Tabla("NU_ACTUNMR_KARD"), Format(Saldo, "#,##0")
            Cambio = "UPDATE IN_KARDEX SET NU_APUN_KARD=" & RegActual & _
                " WHERE NU_AUTO_KARD=" & RegAnterior
            Cnxn.Execute Cambio
'            Tabla("NU_APUN_KARD") = RegActual
'            Tabla.Update
'            Tabla.MoveNext
        End If
        Apuntador = Tabla("NU_AUTO_KARD")
        If Bodega = Tabla("NU_AUTO_BODE_KARD") And Articulo = Tabla("NU_AUTO_ARTI_KARD") Then
            Saldo = Saldo + (Tabla("NU_ENTRAD_KARD") - Tabla("NU_SALIDA_KARD")) * Tabla("NU_MULT_KARD")
            Cambio = "UPDATE IN_KARDEX SET NU_ACTUNMR_KARD=" & Saldo & _
                ", NU_APUN_KARD=0" & _
                " WHERE NU_AUTO_KARD=" & Tabla("NU_AUTO_KARD")
            Cnxn.Execute Cambio
'            Tabla("NU_ACTUNMR_KARD") = Saldo
'            Tabla("NU_APUN_KARD") = 0
'            Tabla.Update
            Tabla.MoveNext
        Else
            Primero = True
        End If
        RegAnterior = RegActual
    Wend
    
    Tabla.Close
    Cnxn.Close
End Sub

'Tipo 0=Baja, 1=Devolucion
Function IN_Reversar_Documento(NoKardex As Long, tipo As Integer, Trans As Boolean) As Integer
Dim ArrDVL() As Variant
Dim Documento As New ElDocumento
   
   ReDim ArrDVL(0)
   Documento.CargaDocumentoActualXAutoNumerico (NoKardex)
   Debug.Print Documento.NroDeArticulos
   If tipo = 0 Then
      Result = LoadData("CONTROL", "VL_VALO_CONT", "CD_CONC_CONT='COMPDEVO'", ArrDVL())
      Documento.Encabezado.TipDeDcmnt = 9
   Else
      Result = LoadData("CONTROL", "VL_VALO_CONT", "CD_CONC_CONT='COMPBAJA'", ArrDVL())
      Documento.Encabezado.TipDeDcmnt = 8
   End If
   Documento.Encabezado.Actualiza
   If Result <> FAIL And Encontro Then Documento.Encabezado.NumeroCOMPROBANTE = Val(ArrDVL(0))
   Documento.Encabezado.FechaDocumento = CDate(FechaServer)
   Documento.Encabezado.EsNuevoDocumento = True
   Documento.Encabezado.EsIndependiente = False
   
   ReDim ArrDVL(1)
   Campos = "NU_AUTO_BODEORG_ENCA,CD_CODI_TERC_ENCA"
   Desde = "IN_ENCABEZADO"
   Condi = "NU_AUTO_ENCA=" & NoKardex
   If Result <> FAIL Then Result = LoadData(Desde, Campos, Condi, ArrDVL())
   If Result <> FAIL And Encontro Then
      Documento.Encabezado.BodegaORIGEN = Val(ArrDVL(0))
'      Documento.Encabezado.Actualiza
   End If
   If tipo = 1 Then Documento.Encabezado.AutoDelDocCARGADO = 0
   If Result <> FAIL Then ResultInv = Documento.GeneraMovimiento(Trans)
   If ResultInv > 0 Then Result = 10
   
   IN_Reversar_Documento = Result
   
End Function

'Public Sub CargaGrillaInterface(Frm As Form, Optional EsVenta As Boolean) 'HRR M1818
Public Sub CargaGrillaInterface(Frm As Form, Optional EsVenta As Boolean, Optional StDoc As String = NUL$) 'HRR M1818

      Dim BoCopia As Boolean 'HRR M1818
   'JLPB T23820 INICIO SE DEJA EN COMENTARIO EL SIGUIENTE BLOQUE
'   'JLPB T22240 INICIO
'   Dim BoAproxCent As Boolean  'VARIABLE QUE SIRVE PARA VISUALIZAR SI ESTA SELECCIONADA LA OPCION DE APROXIMAR
'                            'CENTENAS EN IMPUESTOS Y DESCUENTOS EN CXP
   'JLPB T23820 FIN
   Dim DbIva As Double  'VARIBLE PARA CALCULAR EL IVA
   'JLPB T23820 INICIO SE DEJA EN COMENTARIO EL SIGUIENTE BLOQUE
'   If boICxP Then
'     If ExisteCAMPO("PARAMETROS_CXP", "NU_REDIMDE_PCXP") Then
'         BoAproxCent = CBool(fnDevDato("PARAMETROS_CXP", "NU_REDIMDE_PCXP", NUL$))
'      End If
'   End If
'   'JLPB T22240 FIN
   'JLPB T23820 FIN
'        GrdArti.TextMatrix(1, 0) = Mselec2(aux, 1) 'CD_CODI_ARTI
'        GrdArti.TextMatrix(1, 1) = Mselec2(aux, 2) 'NO_NOMB_ARTI
'        GrdArti.TextMatrix(1, 2) = "0" 'CT_CANT_ENAR
'        GrdArti.TextMatrix(1, 3) = Aplicacion.Formatear_Valor(Buscar_Valor_Arti(Mselec2(aux, 1), "VL_ULCO_ARTI")) 'VL_ULCO_ARTI
'        GrdArti.TextMatrix(1, 4) = "0" 'VL_COTO_ENAR
'        GrdArti.TextMatrix(1, 5) = Aplicacion.Formatear_Porcentaje(Buscar_Valor_Arti(Mselec2(aux, 1), "PR_IMPU_ARTI")) 'PR_IMPU_ARTI
'        GrdArti.TextMatrix(1, 6) = "0"
'        GrdArti.TextMatrix(1, 7) = NUL$ 'FE_VENC_ENAR
'        GrdArti.TextMatrix(1, 9) = "0"

'        GrdArti.Col = 0= "C�digo"
'        GrdArti.Col = 1= "Descripci�n"
'        GrdArti.Col = 2= "Cantidad"
'        GrdArti.Col = 3= "Costo Unitario"
'        GrdArti.Col = 4= "Costo Total"
'        GrdArti.Col = 5= "% Iva"
'        GrdArti.Col = 6= "valor iva"
'        GrdArti.Col = 7= "F.Vencimiento (dd/mm/aaaa)"
'        GrdArti.Col = 9= "Cantidad a Entrar"
    
'    Frm.ColAutoArticulo = 0
'    Frm.Marcar = 1
'    Frm.ColCodigoArticulo = 2
'    Frm.ColNombreArticulo = 3
'    Frm.ColAutoUnidad = 4
'    Frm.ColNombreUnidad = 5
'    Frm.ColMultiplicador = 6
'    Frm.ColDivisor = 7
'    Frm.ColUltimoCosto = 8
'    Frm.ColCantidad = 9
'    Frm.ColCostoSinIVA = 10
'    Frm.ColPorceIVA = 11
'    Frm.ColCostoUnidad = 12
'    Frm.ColCostoTotal = 13
'    Frm.ColFecVence = 14
'    Frm.ColFecEntrada = 15
'    Frm.ColCantidadFija = 16
'    Frm.ColAutoDocumento = 17
'    Frm.Seleccionada = 18
    
'        GrdArti.Col = 0= "C�digo" 'ColCodigoArticulo
'        GrdArti.Col = 1= "Descripci�n" 'ColNombreArticulo
'        GrdArti.Col = 2= "Cantidad" 'ColCantidad * ColMultiplicador / ColDivisor
'        GrdArti.Col = 3= "Costo Unitario" 'ColCostoSinIVA
'        GrdArti.Col = 4= "Costo Total" 'ColCostoSinIVA * ColCantidad * ColMultiplicador / ColDivisor
'        GrdArti.Col = 5= "% Iva" 'ColPorceIVA
'        GrdArti.Col = 6= "valor iva" ' ColPorceIVA * ColCostoSinIVA * ColCantidad * ColMultiplicador / (100 * ColDivisor)
'        GrdArti.Col = 7= "F.Vencimiento (dd/mm/aaaa)" 'ColFecVence
'        GrdArti.Col = 9= "Cantidad a Entrar"
'    Dim items As Integer, Flg As Boolean, vImp As Single
    Dim items As Integer, Flg As Boolean    'DEPURACION DE CODIGO
'    Frm.GrdArti.FixedCols = 0
'    Frm.GrdArti.FixedRows = 1
'//////
'Calcular_Impuesto(Fila CnTr, Cant ColCantidad, valor ColVentaSinIVA, impu ColPorceIVA, mult ColMultiplicador, divi ColDivisor)
'Calcular_Impuesto(Fila As Integer, Cant As Byte, valor As Byte, impu As Byte, mult As Byte, divi As Byte) As Double
'Calcular_Impuesto = CLng(vFGrid.TextMatrix(Fila, ColCantidad)) * _
'                CDbl(vFGrid.TextMatrix(Fila, ColVentaSinIVA)) * CLng(vFGrid.TextMatrix(Fila, ColMultiplicador)) * CLng(vFGrid.TextMatrix(Fila, ColPorceIVA)) / _
'                (100 * CLng(vFGrid.TextMatrix(Fila, ColDivisor)))

'Public Function Calcular_Impuesto(Fila As Integer, Cant As Byte, valor As Byte, impu As Byte, mult As Byte, divi As Byte) As Double
'    If vFGrid.TextMatrix(Fila, Cant) <> NUL$ And vFGrid.TextMatrix(Fila, valor) <> NUL$ Then
'        If IsNumeric(vFGrid.TextMatrix(Fila, Cant)) And _
'            IsNumeric(vFGrid.TextMatrix(Fila, mult)) And _
'            IsNumeric(vFGrid.TextMatrix(Fila, impu)) And _
'            IsNumeric(vFGrid.TextMatrix(Fila, divi)) And _
'            IsNumeric(vFGrid.TextMatrix(Fila, valor)) Then
'            If CLng(vFGrid.TextMatrix(Fila, divi)) > 0 Then Calcular_Impuesto = CLng(vFGrid.TextMatrix(Fila, Cant)) * _
'                CDbl(vFGrid.TextMatrix(Fila, valor)) * CLng(vFGrid.TextMatrix(Fila, mult)) * CLng(vFGrid.TextMatrix(Fila, impu)) / _
'                (100 * CLng(vFGrid.TextMatrix(Fila, divi)))
'        End If
'    End If
'End Function
'//////
    Frm.GrdArti.Cols = 10
    Frm.GrdArti.Rows = 1
    
    For items = 1 To Frm.GrdArticulos.Rows - 1
         
        Flg = True
        If EsVenta Then
            If Not IsNumeric(Frm.GrdArticulos.TextMatrix(items, Frm.ColPorceIVA)) Then Frm.GrdArticulos.TextMatrix(items, Frm.ColPorceIVA) = "0"
            If Not IsNumeric(Frm.GrdArticulos.TextMatrix(items, Frm.ColVentaSinIVA)) Then Frm.GrdArticulos.TextMatrix(items, Frm.ColVentaSinIVA) = "0"
            
            Flg = Flg And (Len(Trim(Frm.GrdArticulos.TextMatrix(items, Frm.ColCodigoArticulo))) > 0)
            Flg = Flg And (Len(Trim(Frm.GrdArticulos.TextMatrix(items, Frm.ColNombreArticulo))) > 0)
            Flg = Flg And IsNumeric(Frm.GrdArticulos.TextMatrix(items, Frm.ColCantidad))
            Flg = Flg And IsNumeric(Frm.GrdArticulos.TextMatrix(items, Frm.ColMultiplicador))
            Flg = Flg And IsNumeric(Frm.GrdArticulos.TextMatrix(items, Frm.ColDivisor))
            Flg = Flg And IsNumeric(Frm.GrdArticulos.TextMatrix(items, Frm.ColVentaSinIVA))
            Flg = Flg And IsNumeric(Frm.GrdArticulos.TextMatrix(items, Frm.ColPorceIVA))
            If Flg Then
'            vImp = CSng(Frm.GrdArticulos.TextMatrix(items, Frm.ColVentaSinIVA)) * CLng(Frm.GrdArticulos.TextMatrix(items, Frm.ColPorceIVA)) / 100 )
                Frm.GrdArti.Rows = Frm.GrdArti.Rows + 1
                Frm.GrdArti.TextMatrix(Frm.GrdArti.Rows - 1, 0) = Frm.GrdArticulos.TextMatrix(items, Frm.ColCodigoArticulo)
                Frm.GrdArti.TextMatrix(Frm.GrdArti.Rows - 1, 1) = Frm.GrdArticulos.TextMatrix(items, Frm.ColNombreArticulo)
                
                'DAHV - Se devuelven los cambios - inicio
                Frm.GrdArti.TextMatrix(Frm.GrdArti.Rows - 1, 2) = Aplicacion.Formatear_Cantidad(Frm.GrdArticulos.TextMatrix(items, Frm.ColCantidad) * Frm.GrdArticulos.TextMatrix(items, Frm.ColMultiplicador) / Frm.GrdArticulos.TextMatrix(items, Frm.ColDivisor)) 'HRR M5080 Se habilita
                'Frm.GrdArti.TextMatrix(Frm.GrdArti.Rows - 1, 2) = Aplicacion.Formatear_Cantidad(Frm.GrdArticulos.TextMatrix(items, Frm.ColCantidad))  'HRR M5080
                'DAHV - Se devuelven los cambios - Fin
                
                'Frm.GrdArti.TextMatrix(Frm.GrdArti.Rows - 1, 3) = Aplicacion.Formatear_Valor(100 * Frm.GrdArticulos.TextMatrix(items, Frm.ColVenUndConDes) / (100 + Frm.GrdArticulos.TextMatrix(items, Frm.ColPorceIVA))) ''ColVenUndConDes
                Frm.GrdArti.TextMatrix(Frm.GrdArti.Rows - 1, 3) = Aplicacion.Formatear_Valor(100 * Frm.GrdArticulos.TextMatrix(items, Frm.ColVentaTotal) / (100 + Frm.GrdArticulos.TextMatrix(items, Frm.ColPorceIVA))) 'DAHV M3930
                ''A = (100 * Frm.GrdArticulos.TextMatrix(items, Frm.ColVenUndConDes) / (100 + Frm.GrdArticulos.TextMatrix(items, Frm.ColPorceIVA)))
                'Frm.GrdArti.TextMatrix(Frm.GrdArti.Rows - 1, 4) = Aplicacion.Formatear_Valor((100 * Frm.GrdArticulos.TextMatrix(items, Frm.ColVenUndConDes) / (100 + Frm.GrdArticulos.TextMatrix(items, Frm.ColPorceIVA))) * Frm.GrdArticulos.TextMatrix(items, Frm.ColCantidad) * Frm.GrdArticulos.TextMatrix(items, Frm.ColMultiplicador) / Frm.GrdArticulos.TextMatrix(items, Frm.ColDivisor))
                Frm.GrdArti.TextMatrix(Frm.GrdArti.Rows - 1, 4) = Aplicacion.Formatear_Valor((100 * Frm.GrdArticulos.TextMatrix(items, Frm.ColVentaTotal) / (100 + Frm.GrdArticulos.TextMatrix(items, Frm.ColPorceIVA))) * Frm.GrdArticulos.TextMatrix(items, Frm.ColCantidad) * Frm.GrdArticulos.TextMatrix(items, Frm.ColMultiplicador) / Frm.GrdArticulos.TextMatrix(items, Frm.ColDivisor)) 'DAHV M3930
                Frm.GrdArti.TextMatrix(Frm.GrdArti.Rows - 1, 5) = Frm.GrdArticulos.TextMatrix(items, Frm.ColPorceIVA)
                Frm.GrdArti.TextMatrix(Frm.GrdArti.Rows - 1, 6) = Aplicacion.Formatear_Valor(Frm.GrdArticulos.TextMatrix(items, Frm.ColPorceIVA) * (100 * Frm.GrdArticulos.TextMatrix(items, Frm.ColVenUndConDes) / (100 + Frm.GrdArticulos.TextMatrix(items, Frm.ColPorceIVA))) * Frm.GrdArticulos.TextMatrix(items, Frm.ColCantidad) * Frm.GrdArticulos.TextMatrix(items, Frm.ColMultiplicador) / (100 * Frm.GrdArticulos.TextMatrix(items, Frm.ColDivisor)))
                Frm.GrdArti.TextMatrix(Frm.GrdArti.Rows - 1, 7) = Frm.GrdArticulos.TextMatrix(items, Frm.ColFecVence)
                'Frm.GrdArti.TextMatrix(Frm.GrdArti.Rows, 9= "Cantidad a Entrar"
                'DAHV se devuelven los cambios - inicio
                'Frm.GrdArti.TextMatrix(Frm.GrdArti.Rows - 1, 8) = Frm.GrdArticulos.TextMatrix(items, Frm.ColMultiplicador) 'HRR M5080
                'Frm.GrdArti.TextMatrix(Frm.GrdArti.Rows - 1, 9) = Frm.GrdArticulos.TextMatrix(items, Frm.ColDivisor) 'HRR M5080
                'DAHV se devuelven los cambios - fin
            End If
        Else
               'HRR M1818
            BoCopia = True
            If StDoc <> NUL$ Then
               If StDoc <> Frm.GrdArticulos.TextMatrix(items, Frm.ColAutoDocumento) Then BoCopia = False
             End If
             
             If BoCopia Then
             'HRR M1818
            
                 If Not IsNumeric(Frm.GrdArticulos.TextMatrix(items, Frm.ColPorceIVA)) Then Frm.GrdArticulos.TextMatrix(items, Frm.ColPorceIVA) = "0"
                 If Not IsNumeric(Frm.GrdArticulos.TextMatrix(items, Frm.ColCostoSinIVA)) Then Frm.GrdArticulos.TextMatrix(items, Frm.ColCostoSinIVA) = "0"
                 If Not IsNumeric(Frm.GrdArticulos.TextMatrix(items, Frm.ColCostoUnidad)) Then Frm.GrdArticulos.TextMatrix(items, Frm.ColCostoUnidad) = "0"
                 
                 Flg = Flg And (Len(Trim(Frm.GrdArticulos.TextMatrix(items, Frm.ColCodigoArticulo))) > 0)
                 Flg = Flg And (Len(Trim(Frm.GrdArticulos.TextMatrix(items, Frm.ColNombreArticulo))) > 0)
                 Flg = Flg And IsNumeric(Frm.GrdArticulos.TextMatrix(items, Frm.ColCantidad))
                 Flg = Flg And IsNumeric(Frm.GrdArticulos.TextMatrix(items, Frm.ColMultiplicador))
                 Flg = Flg And IsNumeric(Frm.GrdArticulos.TextMatrix(items, Frm.ColDivisor))
                 Flg = Flg And IsNumeric(Frm.GrdArticulos.TextMatrix(items, Frm.ColCostoUnidad))
                 Flg = Flg And IsNumeric(Frm.GrdArticulos.TextMatrix(items, Frm.ColPorceIVA))
                 Flg = Flg And IsNumeric(Frm.GrdArticulos.TextMatrix(items, Frm.ColCostoSinIVA))
                 If Flg Then
                     Frm.GrdArti.Rows = Frm.GrdArti.Rows + 1
                     Frm.GrdArti.TextMatrix(Frm.GrdArti.Rows - 1, 0) = Frm.GrdArticulos.TextMatrix(items, Frm.ColCodigoArticulo)
                     Frm.GrdArti.TextMatrix(Frm.GrdArti.Rows - 1, 1) = Frm.GrdArticulos.TextMatrix(items, Frm.ColNombreArticulo)
                     'DAHV - se inician los cambios
                     Frm.GrdArti.TextMatrix(Frm.GrdArti.Rows - 1, 2) = Aplicacion.Formatear_Cantidad(Frm.GrdArticulos.TextMatrix(items, Frm.ColCantidad) * Frm.GrdArticulos.TextMatrix(items, Frm.ColMultiplicador) / Frm.GrdArticulos.TextMatrix(items, Frm.ColDivisor)) 'HRR M5080
                     'Frm.GrdArti.TextMatrix(Frm.GrdArti.Rows - 1, 2) = Aplicacion.Formatear_Cantidad(Frm.GrdArticulos.TextMatrix(items, Frm.ColCantidad)) 'HRR M5080
                     'DAHV - se inician los cambios
                     Frm.GrdArti.TextMatrix(Frm.GrdArti.Rows - 1, 3) = Frm.GrdArticulos.TextMatrix(items, Frm.ColCostoSinIVA)
         '        Frm.GrdArti.TextMatrix(Frm.GrdArti.Rows - 1, 4) = Aplicacion.Formatear_Valor(Frm.GrdArticulos.TextMatrix(Frm.GrdArti.Rows - 1, Frm.ColCostoSinIVA) * Frm.GrdArticulos.TextMatrix(Frm.GrdArti.Rows - 1, Frm.ColCantidad) * Frm.GrdArticulos.TextMatrix(Frm.GrdArti.Rows - 1, Frm.ColMultiplicador) / Frm.GrdArticulos.TextMatrix(Frm.GrdArti.Rows - 1, Frm.ColDivisor))
                     Frm.GrdArti.TextMatrix(Frm.GrdArti.Rows - 1, 4) = Aplicacion.Formatear_Valor(Frm.GrdArticulos.TextMatrix(items, Frm.ColCostoUnidad) * Frm.GrdArticulos.TextMatrix(items, Frm.ColCantidad) * Frm.GrdArticulos.TextMatrix(items, Frm.ColMultiplicador) / Frm.GrdArticulos.TextMatrix(items, Frm.ColDivisor))
                     Frm.GrdArti.TextMatrix(Frm.GrdArti.Rows - 1, 5) = Frm.GrdArticulos.TextMatrix(items, Frm.ColPorceIVA)
                     'JLPB T22240 INICIO LA SIGUIENTE LINEA SE DEJA EN COMENTARIO
                     'Frm.GrdArti.TextMatrix(Frm.GrdArti.Rows - 1, 6) = Aplicacion.Formatear_Valor(Frm.GrdArticulos.TextMatrix(items, Frm.ColPorceIVA) * Frm.GrdArticulos.TextMatrix(items, Frm.ColCostoSinIVA) * Frm.GrdArticulos.TextMatrix(items, Frm.ColCantidad) * Frm.GrdArticulos.TextMatrix(items, Frm.ColMultiplicador) / (100 * Frm.GrdArticulos.TextMatrix(items, Frm.ColDivisor)))
                     If BoAproxCent Then
                        DbIva = AproxCentena(Round(Aplicacion.Formatear_Valor(Frm.GrdArticulos.TextMatrix(items, Frm.ColPorceIVA) * Frm.GrdArticulos.TextMatrix(items, Frm.ColCostoSinIVA) * Frm.GrdArticulos.TextMatrix(items, Frm.ColCantidad) * Frm.GrdArticulos.TextMatrix(items, Frm.ColMultiplicador) / (100 * Frm.GrdArticulos.TextMatrix(items, Frm.ColDivisor)))))
                     Else
                        DbIva = Aplicacion.Formatear_Valor(Frm.GrdArticulos.TextMatrix(items, Frm.ColPorceIVA) * Frm.GrdArticulos.TextMatrix(items, Frm.ColCostoSinIVA) * Frm.GrdArticulos.TextMatrix(items, Frm.ColCantidad) * Frm.GrdArticulos.TextMatrix(items, Frm.ColMultiplicador) / (100 * Frm.GrdArticulos.TextMatrix(items, Frm.ColDivisor)))
                     End If
                     Frm.GrdArti.TextMatrix(Frm.GrdArti.Rows - 1, 6) = Aplicacion.Formatear_Valor(DbIva)
                     'JLPB T22240 INICIO
                     Frm.GrdArti.TextMatrix(Frm.GrdArti.Rows - 1, 7) = Frm.GrdArticulos.TextMatrix(items, Frm.ColFecVence)
                     'DAHV - Se devuelven los cambios
                     'Frm.GrdArti.TextMatrix(Frm.GrdArti.Rows - 1, 8) = Frm.GrdArticulos.TextMatrix(items, Frm.ColMultiplicador) 'HRR M5080
                     'Frm.GrdArti.TextMatrix(Frm.GrdArti.Rows - 1, 9) = Frm.GrdArticulos.TextMatrix(items, Frm.ColDivisor) 'HRR M5080
                     'Frm.GrdArti.TextMatrix(Frm.GrdArti.Rows, 9= "Cantidad a Entrar"
                     'DAHV - Se devuelven los cambios
                 End If
            
            End If 'HRR M1818
        End If
    Next

'calcular_salidaA
'  GrdArti.Col = 0= "C�digo"
'  GrdArti.Col = 1= "Descripci�n"
'  GrdArti.Col = 2= "Cantidad"
'  GrdArti.Col = 3= "Costo Unitario"
'  GrdArti.Col = 4= "Costo Total"
'  GrdArti.Col = 5= "Existencia"

End Sub

Public Sub INTCllBodegas(Col As Collection, Tip As String)
    Dim ArrBOD() As Variant, Tar As Integer, UnaBod As LaBodega
    Dim CMP, Dsd, Cnd As String
    Call BorrarColeccion(Col)
    CMP = "NU_AUTO_BODE, NU_AUTO_BODE_BODE, TX_CODI_BODE, TX_NOMB_BODE, CD_CODI_CECO_BODE, TX_VENTA_BODE"
    Dsd = "IN_BODEGA"
    Cnd = "TX_VENTA_BODE=" & Comi & Tip & Comi
    ReDim ArrBOD(5, 0)
    Result = LoadMulData(Dsd, CMP, Cnd, ArrBOD())
    If Result = FAIL Then Exit Sub
    If Not Encontro Then Exit Sub
    For Tar = 0 To UBound(ArrBOD, 2)
        Set UnaBod = New LaBodega
        UnaBod.AutoNumerico = ArrBOD(0, Tar) 'RST("NU_AUTO_BODE")
        UnaBod.Codigo = ArrBOD(2, Tar) 'RST("TX_CODI_BODE")
        UnaBod.Nombre = ArrBOD(3, Tar) 'RST("TX_NOMB_BODE")
        UnaBod.AlmacenaPara = ArrBOD(4, Tar) 'RST("TX_VENTA_BODE")
        Col.Add UnaBod, "B" & UnaBod.AutoNumerico
        Set UnaBod = Nothing
    Next
End Sub

Public Sub InfoProveedoresXArticulo(Lista As MSComctlLib.ListView, vArticulo As ElArticulo)
    Dim vProveedor As ElProveedor
    Dim vColeccion As Collection
    Dim vTercero As New ElTercero
    Dim vCodigoBarra As New CdgBarra
    Dim vContar As Integer, vclave As String
    Set vColeccion = vArticulo.GetCllDeProveedores
    With Lista
    .ListItems.Clear
    vContar = 1
    For Each vProveedor In vColeccion
        vclave = "P" & CStr(vContar)
        vTercero.IniXNit vProveedor.CodigoTercero
        vCodigoBarra.IniciaXCodigo vProveedor.CodigoDeBarra
        .ListItems.Add , vclave, vProveedor.CodigoDeBarra
        .ListItems.Item(vclave).ListSubItems.Add , "DES", vCodigoBarra.Comercial
        .ListItems.Item(vclave).ListSubItems.Add , "TER", vProveedor.CodigoTercero
        .ListItems.Item(vclave).ListSubItems.Add , "NOM", vTercero.Nombre
        .ListItems.Item(vclave).ListSubItems.Add , "COS", Format(vProveedor.Costo, "#,##0")
        .ListItems.Item(vclave).ListSubItems.Add , "IMP", Format(vProveedor.Impuesto, "#0.00")
        vContar = vContar + 1
    Next
    End With
End Sub

'Public Sub InfoArticuloXBodega(Lista As MSComctlLib.ListView, vArticulo As ElArticulo, vBodegas As Collection, LosPorceso As Boolean)
Public Sub InfoArticuloXBodega(Lista As MSComctlLib.ListView, vArticulo As ElArticulo, vBodegas As Collection, LosPorceso As Boolean, Optional lnVentana As Long)   'JACC M6675
    Dim LasSalidas As New InfXBodega, LasEntradas As New InfXBodega
    Dim EntTtl As Single, SalTtl As Single
    Dim Bode As LaBodega
    Dim SaldoActiva As Single, ElSa As New InfXBodega
    Dim SldTtl As Single, MaxTtl As Long, MinTtl As Long, RepTtl As Long, DesTtl As Long
'    Dim CnTr As Byte, UnBdg As ListItem, LaSlctd As String
    Dim UnBdg As ListItem, LaSlctd As String    'DEPURACION DE CODIGO
    Dim ParcialEntradas As Single, ParcialSalidas As Single
    With Lista
        If LosPorceso Then
            'vArticulo.INTCllSaldosXBodega
            vArticulo.INTCllSaldosXBodega (lnVentana) 'JACC M6675
            vArticulo.INTCllEntradasXBodega
            vArticulo.INTCllSalidasXBodega
            SldTtl = 0: MaxTtl = 0: MinTtl = 0: RepTtl = 0: EntTtl = 0: SalTtl = 0
            For Each UnBdg In .ListItems
                If UnBdg.Checked Then LaSlctd = LaSlctd & UnBdg.Key & "/"
            Next
            .ListItems.Clear
            For Each Bode In vBodegas
                Set ElSa = vArticulo.SaldoXBodega(Bode.AutoNumerico)
                If ElSa Is Nothing Then GoTo ELQUE
                Set LasEntradas = vArticulo.EntradasXBodega(Bode.AutoNumerico)
                If Not (LasEntradas Is Nothing) Then
                    ParcialEntradas = LasEntradas.Entradas.Numerador / LasEntradas.Entradas.Denominador
                End If
                Set LasSalidas = vArticulo.SalidasXBodega(Bode.AutoNumerico)
                If Not (LasSalidas Is Nothing) Then
                    ParcialSalidas = LasSalidas.Salidas.Numerador / LasSalidas.Salidas.Denominador
                End If
                SaldoActiva = ElSa.Saldo.Numerador / ElSa.Saldo.Denominador
                .ListItems.Add , "C" & Trim(Bode.Codigo), Trim(Bode.Codigo)
                If Len(LaSlctd) = 0 Then
                    .ListItems("C" & Trim(Bode.Codigo)).Checked = True
                    SldTtl = SldTtl + SaldoActiva
                    MinTtl = MinTtl + ElSa.Minimo
                    RepTtl = RepTtl + ElSa.Reposicion
                    MaxTtl = MaxTtl + ElSa.Maximo
                    DesTtl = DesTtl + ElSa.MaxDespachar
                    EntTtl = EntTtl + ParcialEntradas
                    SalTtl = SalTtl + ParcialSalidas
                Else
                    If InStr(1, LaSlctd, "C" & Trim(Bode.Codigo) & "/") > 0 Then
                        .ListItems("C" & Trim(Bode.Codigo)).Checked = True
                        SldTtl = SldTtl + SaldoActiva
                        MinTtl = MinTtl + ElSa.Minimo
                        RepTtl = RepTtl + ElSa.Reposicion
                        MaxTtl = MaxTtl + ElSa.Maximo
                        DesTtl = DesTtl + ElSa.MaxDespachar
                        EntTtl = EntTtl + ParcialEntradas
                        SalTtl = SalTtl + ParcialSalidas
                    End If
                End If
'NOM, SLD MIN REP MAX DES ENT SAL
                .ListItems("C" & Trim(Bode.Codigo)).ListSubItems.Add , "NOM", Trim(Bode.Nombre)
                .ListItems("C" & Trim(Bode.Codigo)).ListSubItems.Add , "SLD", Format(SaldoActiva, "##,###,###.000")
                .ListItems("C" & Trim(Bode.Codigo)).ListSubItems.Add , "MIN", Format(ElSa.Minimo, "##,###,###.000")
                .ListItems("C" & Trim(Bode.Codigo)).ListSubItems.Add , "REP", Format(ElSa.Reposicion, "##,###,###.000")
                .ListItems("C" & Trim(Bode.Codigo)).ListSubItems.Add , "MAX", Format(ElSa.Maximo, "##,###,###.000")
                .ListItems("C" & Trim(Bode.Codigo)).ListSubItems.Add , "DES", Format(ElSa.MaxDespachar, "##,###,###.000")
                .ListItems("C" & Trim(Bode.Codigo)).ListSubItems.Add , "ENT", Format(ParcialEntradas, "##,###,###.000")
                .ListItems("C" & Trim(Bode.Codigo)).ListSubItems.Add , "SAL", Format(ParcialSalidas, "##,###,###.000")
ELQUE:
            Next
            .ListItems.Add , "SldTtl", "TOTAL"
            .ListItems("SldTtl").ListSubItems.Add , "NOM", "Total SELECCIONADAS"
            .ListItems("SldTtl").ListSubItems.Add , "SLD", Format(SldTtl, "##,###,###.000")
            .ListItems("SldTtl").ListSubItems.Add , "MIN", Format(MinTtl, "##,###,###.000")
            .ListItems("SldTtl").ListSubItems.Add , "REP", Format(RepTtl, "##,###,###.000")
            .ListItems("SldTtl").ListSubItems.Add , "MAX", Format(MaxTtl, "##,###,###.000")
            .ListItems("SldTtl").ListSubItems.Add , "DES", Format(DesTtl, "##,###,###.000")
            .ListItems("SldTtl").ListSubItems.Add , "ENT", Format(EntTtl, "##,###,###.000")
            .ListItems("SldTtl").ListSubItems.Add , "SAL", Format(SalTtl, "##,###,###.000")
            Set ElSa = Nothing
        End If
    End With
End Sub

