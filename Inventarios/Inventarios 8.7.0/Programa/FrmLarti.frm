VERSION 5.00
Object = "{00025600-0000-0000-C000-000000000046}#5.2#0"; "CRYSTL32.OCX"
Object = "{0BA686C6-F7D3-101A-993E-0000C0EF6F5E}#1.0#0"; "THREED32.OCX"
Begin VB.Form FrmLarti 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Listado de Art�culos"
   ClientHeight    =   4605
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   6780
   Icon            =   "FrmLarti.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   ScaleHeight     =   4605
   ScaleWidth      =   6780
   Begin VB.Frame FraOrden 
      Caption         =   "Ordenar por"
      Height          =   735
      Left            =   0
      TabIndex        =   0
      Top             =   0
      Width           =   2055
      Begin VB.OptionButton OptOrden 
         BackColor       =   &H00C0C0C0&
         Caption         =   "C�digo"
         Height          =   255
         Index           =   0
         Left            =   120
         TabIndex        =   1
         Top             =   360
         Value           =   -1  'True
         Width           =   855
      End
      Begin VB.OptionButton OptOrden 
         BackColor       =   &H00C0C0C0&
         Caption         =   "Nombre"
         Height          =   255
         Index           =   1
         Left            =   1080
         TabIndex        =   2
         Top             =   360
         Width           =   855
      End
   End
   Begin VB.Frame FraCampos 
      Caption         =   "Mostrar campos"
      Height          =   735
      Left            =   3960
      TabIndex        =   6
      Top             =   0
      Width           =   2775
      Begin VB.CheckBox ChkOpcion 
         Caption         =   "Costo"
         Height          =   255
         Index           =   2
         Left            =   120
         TabIndex        =   7
         Top             =   360
         Width           =   855
      End
      Begin VB.CheckBox ChkOpcion 
         Caption         =   "Fecha Vencimiento"
         Height          =   255
         Index           =   3
         Left            =   960
         TabIndex        =   8
         Top             =   360
         Width           =   1695
      End
   End
   Begin VB.Frame FraAgrupar 
      Caption         =   "Agrupar por"
      Height          =   735
      Left            =   2160
      TabIndex        =   3
      Top             =   0
      Width           =   1695
      Begin VB.CheckBox ChkOpcion 
         Caption         =   "Grupo"
         Height          =   255
         Index           =   0
         Left            =   120
         TabIndex        =   4
         Top             =   360
         Width           =   855
      End
      Begin VB.CheckBox ChkOpcion 
         Caption         =   "Uso"
         Height          =   255
         Index           =   1
         Left            =   960
         TabIndex        =   5
         Top             =   360
         Width           =   615
      End
   End
   Begin VB.Frame FraRangos 
      Caption         =   "Rangos de Impresi�n"
      Height          =   2775
      Left            =   0
      TabIndex        =   9
      Top             =   840
      Width           =   6735
      Begin VB.Frame FraArticulos 
         Caption         =   "Art�culos"
         Height          =   855
         Left            =   120
         TabIndex        =   10
         Top             =   240
         Width           =   6495
         Begin VB.TextBox TxtRango 
            Height          =   285
            Index           =   1
            Left            =   3960
            MaxLength       =   20
            TabIndex        =   13
            Text            =   "ZZZZZZZZZZZZZZZZ"
            Top             =   360
            Width           =   1815
         End
         Begin VB.TextBox TxtRango 
            Height          =   285
            Index           =   0
            Left            =   720
            MaxLength       =   20
            TabIndex        =   11
            Text            =   "0"
            Top             =   360
            Width           =   1815
         End
         Begin Threed.SSCommand CmdSelec 
            Height          =   375
            Index           =   0
            Left            =   2520
            TabIndex        =   12
            ToolTipText     =   "Selecci�n de Art�culos"
            Top             =   240
            Width           =   615
            _Version        =   65536
            _ExtentX        =   1085
            _ExtentY        =   661
            _StockProps     =   78
            BevelWidth      =   0
            RoundedCorners  =   0   'False
            Outline         =   0   'False
            Picture         =   "FrmLarti.frx":058A
         End
         Begin Threed.SSCommand CmdSelec 
            Height          =   375
            Index           =   1
            Left            =   5760
            TabIndex        =   14
            ToolTipText     =   "Selecci�n de Art�culos"
            Top             =   240
            Width           =   615
            _Version        =   65536
            _ExtentX        =   1085
            _ExtentY        =   661
            _StockProps     =   78
            BevelWidth      =   0
            RoundedCorners  =   0   'False
            Outline         =   0   'False
            Picture         =   "FrmLarti.frx":0F3C
         End
         Begin VB.Label LblHasta 
            AutoSize        =   -1  'True
            Caption         =   "Hasta : "
            Height          =   195
            Left            =   3360
            TabIndex        =   30
            Top             =   360
            Width           =   555
         End
         Begin VB.Label LblInicio 
            AutoSize        =   -1  'True
            Caption         =   "Desde : "
            Height          =   195
            Left            =   120
            TabIndex        =   29
            Top             =   360
            Width           =   600
         End
      End
      Begin VB.Frame FraGrupos 
         Caption         =   "Grupos"
         Enabled         =   0   'False
         Height          =   1335
         Left            =   120
         TabIndex        =   15
         Top             =   1200
         Width           =   3015
         Begin VB.TextBox TxtRango 
            Height          =   285
            Index           =   2
            Left            =   1080
            MaxLength       =   9
            TabIndex        =   16
            Text            =   "0"
            Top             =   360
            Width           =   1095
         End
         Begin VB.TextBox TxtRango 
            Height          =   285
            Index           =   3
            Left            =   1080
            MaxLength       =   9
            TabIndex        =   18
            Text            =   "ZZZZZZZZZ"
            Top             =   840
            Width           =   1095
         End
         Begin Threed.SSCommand CmdSelec 
            Height          =   375
            Index           =   2
            Left            =   2160
            TabIndex        =   17
            ToolTipText     =   "Selecci�n de Grupos"
            Top             =   240
            Width           =   615
            _Version        =   65536
            _ExtentX        =   1085
            _ExtentY        =   661
            _StockProps     =   78
            BevelWidth      =   0
            RoundedCorners  =   0   'False
            Outline         =   0   'False
            Picture         =   "FrmLarti.frx":18EE
         End
         Begin Threed.SSCommand CmdSelec 
            Height          =   375
            Index           =   3
            Left            =   2160
            TabIndex        =   19
            ToolTipText     =   "Selecci�n de Grupos"
            Top             =   720
            Width           =   615
            _Version        =   65536
            _ExtentX        =   1085
            _ExtentY        =   661
            _StockProps     =   78
            BevelWidth      =   0
            RoundedCorners  =   0   'False
            Outline         =   0   'False
            Picture         =   "FrmLarti.frx":22A0
         End
         Begin VB.Label LblHastaG 
            AutoSize        =   -1  'True
            Caption         =   "Hasta : "
            Height          =   195
            Left            =   360
            TabIndex        =   28
            Top             =   840
            Width           =   555
         End
         Begin VB.Label LblInicioG 
            AutoSize        =   -1  'True
            Caption         =   "Desde : "
            Height          =   195
            Left            =   360
            TabIndex        =   27
            Top             =   360
            Width           =   600
         End
      End
      Begin VB.Frame FraUsos 
         Caption         =   "Usos"
         Enabled         =   0   'False
         Height          =   1335
         Left            =   3720
         TabIndex        =   20
         Top             =   1200
         Width           =   2895
         Begin VB.TextBox TxtRango 
            Height          =   285
            Index           =   4
            Left            =   1320
            MaxLength       =   4
            TabIndex        =   21
            Text            =   "0"
            Top             =   360
            Width           =   615
         End
         Begin VB.TextBox TxtRango 
            Height          =   285
            Index           =   5
            Left            =   1320
            MaxLength       =   4
            TabIndex        =   23
            Text            =   "ZZZZ"
            Top             =   840
            Width           =   615
         End
         Begin Threed.SSCommand CmdSelec 
            Height          =   375
            Index           =   4
            Left            =   1920
            TabIndex        =   22
            ToolTipText     =   "Selecci�n de Usos"
            Top             =   240
            Width           =   615
            _Version        =   65536
            _ExtentX        =   1085
            _ExtentY        =   661
            _StockProps     =   78
            BevelWidth      =   0
            RoundedCorners  =   0   'False
            Outline         =   0   'False
            Picture         =   "FrmLarti.frx":2C52
         End
         Begin Threed.SSCommand CmdSelec 
            Height          =   375
            Index           =   5
            Left            =   1920
            TabIndex        =   24
            ToolTipText     =   "Selecci�n de Usos"
            Top             =   720
            Width           =   615
            _Version        =   65536
            _ExtentX        =   1085
            _ExtentY        =   661
            _StockProps     =   78
            BevelWidth      =   0
            RoundedCorners  =   0   'False
            Outline         =   0   'False
            Picture         =   "FrmLarti.frx":3604
         End
         Begin VB.Label LblHastaU 
            AutoSize        =   -1  'True
            Caption         =   "Hasta : "
            Height          =   195
            Left            =   600
            TabIndex        =   26
            Top             =   840
            Width           =   555
         End
         Begin VB.Label LblInicioU 
            AutoSize        =   -1  'True
            Caption         =   "Desde : "
            Height          =   195
            Left            =   600
            TabIndex        =   25
            Top             =   360
            Width           =   600
         End
      End
   End
   Begin Crystal.CrystalReport Crys_Listar 
      Left            =   3720
      Top             =   4680
      _ExtentX        =   741
      _ExtentY        =   741
      _Version        =   348160
      WindowLeft      =   20
      WindowWidth     =   750
      WindowHeight    =   450
      WindowTitle     =   "Listado de Art�culos"
      WindowControlBox=   -1  'True
      WindowMaxButton =   -1  'True
      WindowMinButton =   -1  'True
      WindowControls  =   -1  'True
      PrintFileLinesPerPage=   60
      WindowShowCloseBtn=   -1  'True
      WindowShowSearchBtn=   -1  'True
      WindowShowPrintSetupBtn=   -1  'True
      WindowShowRefreshBtn=   -1  'True
   End
   Begin Threed.SSCommand SCmd_Options 
      Height          =   735
      Index           =   1
      Left            =   3000
      TabIndex        =   31
      Top             =   3720
      Width           =   855
      _Version        =   65536
      _ExtentX        =   1508
      _ExtentY        =   1296
      _StockProps     =   78
      Caption         =   "&IMPRIMIR"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   6
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Picture         =   "FrmLarti.frx":3FB6
   End
   Begin Threed.SSCommand SCmd_Options 
      Height          =   735
      Index           =   2
      Left            =   4080
      TabIndex        =   32
      Top             =   3720
      Width           =   855
      _Version        =   65536
      _ExtentX        =   1508
      _ExtentY        =   1296
      _StockProps     =   78
      Caption         =   "&SALIR"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   6
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Picture         =   "FrmLarti.frx":4680
   End
   Begin Threed.SSCommand SCmd_Options 
      Height          =   735
      Index           =   0
      Left            =   1920
      TabIndex        =   33
      Top             =   3720
      Width           =   855
      _Version        =   65536
      _ExtentX        =   1508
      _ExtentY        =   1296
      _StockProps     =   78
      Caption         =   "&PANTALLA"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   6
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Picture         =   "FrmLarti.frx":4D4A
   End
   Begin Threed.SSCommand SCmd_Options 
      Height          =   375
      Index           =   3
      Left            =   5160
      TabIndex        =   34
      Top             =   4800
      Visible         =   0   'False
      Width           =   975
      _Version        =   65536
      _ExtentX        =   1720
      _ExtentY        =   661
      _StockProps     =   78
   End
   Begin Threed.SSCommand SCmd_Options 
      Height          =   375
      Index           =   4
      Left            =   6120
      TabIndex        =   35
      Top             =   4800
      Visible         =   0   'False
      Width           =   975
      _Version        =   65536
      _ExtentX        =   1720
      _ExtentY        =   661
      _StockProps     =   78
   End
End
Attribute VB_Name = "FrmLarti"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Public OpcCod        As String   'Opci�n de seguridad
Private Sub Form_Load()
    Dim ElClnt As New ElTercero
    ElClnt.IniXNit
 Call CenterForm(MDI_Inventarios, Me)
' Call Leer_Permisos("03008", SCmd_Options(3), SCmd_Options(3), SCmd_Options(4))
 Crys_Listar.Formulas(0) = "ENTIDAD='" & ElClnt.Nombre & Comi
 Crys_Listar.Formulas(1) = "NIT= 'NIT. " & ElClnt.Nit & Comi
 Crys_Listar.Formulas(2) = "HORA= 'Hora: " & Format(Now, "hh:mm") & Comi
 Crys_Listar.Formulas(3) = "USUARIO = 'Usuario:" & UserId & Comi
' Crys_Listar.Connect = conCrys
End Sub
Private Sub OptOrden_KeyPress(Index As Integer, KeyAscii As Integer)
  Call Cambiar_Enter(KeyAscii)
End Sub
Private Sub ChkOpcion_KeyPress(Index As Integer, KeyAscii As Integer)
   Call Cambiar_Enter(KeyAscii)
End Sub
Private Sub ChkOpcion_Click(Index As Integer)
   If ChkOpcion(0).Value = 1 Then
      FraGrupos.Enabled = True
   Else
      FraGrupos.Enabled = False
      TxtRango(2).Text = "0"
      TxtRango(3).Text = "ZZZZZZZZZ"
   End If
   If ChkOpcion(1).Value = 1 Then
      FraUsos.Enabled = True
   Else
      FraUsos.Enabled = False
      TxtRango(4).Text = "0"
      TxtRango(5).Text = "ZZZ"
   End If
End Sub
Private Sub TxtRango_GotFocus(Index As Integer)
    TxtRango(Index).SelStart = 0
    TxtRango(Index).SelLength = Len(TxtRango(Index).Text)
End Sub
Private Sub TxtRango_KeyPress(Index As Integer, KeyAscii As Integer)
'    Dim i As Integer       'DEPURACION DE CODIGO
    Call ValKeyAlfaNum(KeyAscii)
    If KeyAscii = 13 Then
salto:
     If Index < 5 Then
      If TxtRango(Index + 1).Enabled = True Then
         TxtRango(Index + 1).SetFocus
      Else
         Index = Index + 1
         GoTo salto
      End If
     Else
      SCmd_Options(0).SetFocus
     End If
    End If
End Sub
Private Sub TxtRango_KeyDown(Index As Integer, KeyCode As Integer, Shift As Integer)
' Dim Salir As Boolean      'DEPURACION DE CODIGO
 If Index = 0 Then
   If KeyCode = 40 Then
      avanzar Index
   End If
 Else
   If KeyCode = 38 Then
      retroceder Index
   End If
   If KeyCode = 40 Then
      avanzar Index
   End If
 End If
End Sub
Private Sub avanzar(ByRef Index As Integer)
      Salir = False
      Do Until Salir = True
        If (Index + 1) <= 5 Then
         If TxtRango(Index + 1).Enabled = True Then
            TxtRango(Index + 1).SetFocus
            Salir = True
         Else
            Index = Index + 1
         End If
        Else
          Salir = True
          SCmd_Options(0).SetFocus
        End If
      Loop
End Sub
Private Sub retroceder(ByRef Index As Integer)
      Salir = False
      Do Until Salir = True
        If (Index - 1) >= 0 Then
         If TxtRango(Index - 1).Enabled = True Then
            TxtRango(Index - 1).SetFocus
            Salir = True
         Else
            Index = Index - 1
         End If
        Else
          Salir = True
          ChkOpcion(3).SetFocus
        End If
      Loop
End Sub
Private Sub CmdSelec_Click(Index As Integer)
    Codigo = NUL$
    Select Case Index
        Case 0, 1: Codigo = Seleccion("ARTICULO", "NO_NOMB_ARTI", "CD_CODI_ARTI,NO_NOMB_ARTI", "ARTICULOS", NUL$)
        Case 2, 3: Codigo = Seleccion("GRUP_ARTICULO", "DE_DESC_GRUP", "CD_CODI_GRUP,DE_DESC_GRUP", "GRUPOS DE ARTICULOS", NUL$)
        Case 4, 5: Codigo = Seleccion("USOS", "DE_DESC_USOS", "CD_CODI_USOS,DE_DESC_USOS", "USOS", NUL$)
    End Select
    If Codigo <> NUL$ Then TxtRango(Index) = Codigo
    TxtRango(Index).SetFocus
End Sub
Private Sub SCmd_Options_Click(Index As Integer)
    Screen.MousePointer = 11
    Select Case Index
        '''True = pantalla
        '''False = impresora
        Case 0: Call Informe(True)
        Case 1: Call Informe(False)
        Case 2: Unload Me
    End Select
    Screen.MousePointer = 0
End Sub
Private Sub Informe(ByVal pantalla As Boolean)
Dim nomrep As String
  If TxtRango(0) > TxtRango(1) Or TxtRango(1) = "" Then
     Call Mensaje1("Revise rango de art�culos", 3)
     Exit Sub
  End If
  If TxtRango(2) > TxtRango(3) Or TxtRango(3) = "" Then
     Call Mensaje1("Revise rango de grupos", 3)
     Exit Sub
  End If
  If TxtRango(4) > TxtRango(5) Or TxtRango(5) = "" Then
     Call Mensaje1("Revise rango de usos", 3)
     Exit Sub
  End If
  Crys_Listar.SelectionFormula = ""
  If OptOrden(1).Value = True Then
       Crys_Listar.SortFields(0) = "+{articulo.no_nomb_arti}"
  Else
       Crys_Listar.SortFields(0) = "+{articulo.cd_codi_arti}"
  End If
  If ChkOpcion(2).Value = 1 And ChkOpcion(3).Value = 1 Then
    Crys_Listar.Formulas(4) = "COSTO='" & "3" & Comi
  End If
  If ChkOpcion(2).Value = 0 And ChkOpcion(3).Value = 0 Then
    Crys_Listar.Formulas(4) = "COSTO='" & "4" & Comi
  End If
  If ChkOpcion(2).Value = 1 And ChkOpcion(3).Value = 0 Then
    Crys_Listar.Formulas(4) = "COSTO='" & "1" & Comi
  End If
  If ChkOpcion(2).Value = 0 And ChkOpcion(3).Value = 1 Then
    Crys_Listar.Formulas(4) = "COSTO='" & "2" & Comi
  End If
  If ChkOpcion(0).Value = 1 And ChkOpcion(1).Value = 1 Then
     Crys_Listar.ReportFileName = DirTrab + "artiGU.rpt"
     nomrep = "artiGU.rpt"
     Crys_Listar.SelectionFormula = "{grup_articulo.cd_codi_grup} in '" & TxtRango(2) & "' to '" & TxtRango(3) & Comi & _
     " and {usos.cd_codi_usos} in '" & TxtRango(4) & "' to '" & TxtRango(5) & Comi & _
     " and {articulo.cd_codi_arti} in '" & TxtRango(0) & "' to '" & TxtRango(1) & Comi
  End If
  If ChkOpcion(0).Value = 0 And ChkOpcion(1).Value = 0 Then
     Crys_Listar.ReportFileName = DirTrab + "artiS.rpt"
     nomrep = "artiS.rpt"
     Crys_Listar.SelectionFormula = "{articulo.cd_codi_arti} in '" & TxtRango(0) & "' to '" & TxtRango(1) & Comi
  End If
  If ChkOpcion(0).Value = 1 And ChkOpcion(1).Value = 0 Then
     Crys_Listar.ReportFileName = DirTrab + "artiG.rpt"
     nomrep = "artiG.rpt"
     Crys_Listar.SelectionFormula = "{grup_articulo.cd_codi_grup} in '" & TxtRango(2) & "' to '" & TxtRango(3) & Comi & _
     " and {articulo.cd_codi_arti} in '" & TxtRango(0) & "' to '" & TxtRango(1) & Comi
  End If
  If ChkOpcion(0).Value = 0 And ChkOpcion(1).Value = 1 Then
     Crys_Listar.ReportFileName = DirTrab + "artiU.rpt"
     nomrep = "artiU.rpt"
     Crys_Listar.SelectionFormula = "{usos.cd_codi_usos} in '" & TxtRango(4) & "' to '" & TxtRango(5) & Comi & _
     " and {articulo.cd_codi_arti} in '" & TxtRango(0) & "' to '" & TxtRango(1) & Comi
  End If
  If SCmd_Options(4).Enabled = False Then
     Call Mensaje1("Permiso Denegado", 3)
  Else
     On Error GoTo control
     If pantalla = True Then
        Crys_Listar.Destination = crptToWindow
     Else
        Crys_Listar.Destination = crptToPrinter
     End If
     Crys_Listar.Action = 1
  End If
  Exit Sub
control:
  Call Mensaje1(ERR.Number & ": " & ERR.Description & "  Nombre del Reporte : " & nomrep, 1)
End Sub





