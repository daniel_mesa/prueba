VERSION 5.00
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "Comdlg32.ocx"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Object = "{0D452EE1-E08F-101A-852E-02608C4D0BB4}#2.0#0"; "FM20.DLL"
Begin VB.Form FrmGenerarPlano 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Generador de Archivos Planos"
   ClientHeight    =   6780
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   8265
   Icon            =   "FrmGenerarPlano.frx":0000
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   ScaleHeight     =   6780
   ScaleWidth      =   8265
   Begin VB.Frame Fram_Consulta 
      Height          =   5295
      Left            =   120
      TabIndex        =   8
      Top             =   1440
      Width           =   8055
      Begin VB.TextBox TxtConsulta 
         Height          =   850
         Index           =   1
         Left            =   120
         MultiLine       =   -1  'True
         ScrollBars      =   2  'Vertical
         TabIndex        =   12
         ToolTipText     =   "Consulta a Ejecutar"
         Top             =   1560
         Width           =   6975
      End
      Begin VB.CommandButton Cmd_Ejecutar 
         Caption         =   "&EJECUTAR"
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   6
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   495
         Left            =   7200
         Picture         =   "FrmGenerarPlano.frx":058A
         Style           =   1  'Graphical
         TabIndex        =   13
         Top             =   1920
         Width           =   735
      End
      Begin VB.TextBox TxtConsulta 
         Height          =   850
         Index           =   0
         Left            =   120
         MultiLine       =   -1  'True
         ScrollBars      =   2  'Vertical
         TabIndex        =   10
         ToolTipText     =   "Consulta a Ejecutar"
         Top             =   360
         Width           =   7815
      End
      Begin MSComDlg.CommonDialog Guardar 
         Left            =   240
         Top             =   4200
         _ExtentX        =   847
         _ExtentY        =   847
         _Version        =   393216
      End
      Begin VB.Frame Fram_Parametros 
         Caption         =   "&Par�metros del Campo"
         Height          =   2535
         Index           =   0
         Left            =   3600
         TabIndex        =   16
         Top             =   2640
         Width           =   4335
         Begin VB.ComboBox CboTipoDato 
            Height          =   315
            Index           =   0
            ItemData        =   "FrmGenerarPlano.frx":08CC
            Left            =   1200
            List            =   "FrmGenerarPlano.frx":08D9
            TabIndex        =   19
            ToolTipText     =   "Alineacion del campo en el plano"
            Top             =   600
            Width           =   3015
         End
         Begin VB.TextBox TxtCaracter 
            Enabled         =   0   'False
            Height          =   285
            Index           =   0
            Left            =   3600
            MaxLength       =   1
            TabIndex        =   28
            ToolTipText     =   "Caracter del relleno"
            Top             =   2160
            Width           =   615
         End
         Begin VB.ComboBox CboAlin_relle 
            Enabled         =   0   'False
            Height          =   315
            Index           =   0
            ItemData        =   "FrmGenerarPlano.frx":08F5
            Left            =   1200
            List            =   "FrmGenerarPlano.frx":08FF
            TabIndex        =   26
            ToolTipText     =   "Alineacion del Relleno en el plano"
            Top             =   2160
            Width           =   1575
         End
         Begin VB.ComboBox CboAlineacion 
            Height          =   315
            Index           =   0
            ItemData        =   "FrmGenerarPlano.frx":0917
            Left            =   1200
            List            =   "FrmGenerarPlano.frx":0921
            TabIndex        =   21
            ToolTipText     =   "Alineacion del campo en el plano"
            Top             =   960
            Width           =   1575
         End
         Begin VB.TextBox TxtTama�o 
            Alignment       =   1  'Right Justify
            Height          =   315
            Index           =   0
            Left            =   3600
            MaxLength       =   5
            TabIndex        =   23
            ToolTipText     =   "Tama�o del campo en el plano"
            Top             =   960
            Width           =   615
         End
         Begin VB.Label LblLinea 
            BorderStyle     =   1  'Fixed Single
            Height          =   90
            Index           =   0
            Left            =   0
            TabIndex        =   31
            Top             =   1650
            Width           =   4305
         End
         Begin MSForms.CheckBox ChkDecimales 
            Height          =   300
            Index           =   0
            Left            =   120
            TabIndex        =   30
            Top             =   1320
            Width           =   2415
            VariousPropertyBits=   746588185
            BackColor       =   -2147483633
            ForeColor       =   -2147483630
            DisplayStyle    =   4
            Size            =   "4260;529"
            Value           =   "0"
            Caption         =   "Separador de Decimales"
            FontEffects     =   1073750016
            FontHeight      =   165
            FontCharSet     =   0
            FontPitchAndFamily=   2
         End
         Begin MSForms.Label LblDesCamp 
            Height          =   255
            Index           =   0
            Left            =   120
            TabIndex        =   17
            Top             =   240
            Width           =   4095
            Caption         =   "Nombre del Campo"
            Size            =   "7223;450"
            SpecialEffect   =   2
            FontHeight      =   165
            FontCharSet     =   0
            FontPitchAndFamily=   2
         End
         Begin MSForms.CheckBox Chkrellenar 
            Height          =   255
            Index           =   0
            Left            =   120
            TabIndex        =   24
            Top             =   1800
            Width           =   975
            BackColor       =   -2147483633
            ForeColor       =   -2147483630
            DisplayStyle    =   4
            Size            =   "1720;450"
            Value           =   "0"
            Caption         =   "Rellenar"
            FontHeight      =   165
            FontCharSet     =   0
            FontPitchAndFamily=   2
         End
         Begin VB.Label LblCaracter 
            Caption         =   "Carac&ter"
            Height          =   255
            Index           =   0
            Left            =   2880
            TabIndex        =   27
            Top             =   2160
            Width           =   615
         End
         Begin VB.Label LblRelleno 
            Caption         =   "&Rellenar a la "
            Height          =   255
            Index           =   0
            Left            =   120
            TabIndex        =   25
            Top             =   2160
            Width           =   975
         End
         Begin VB.Label LblTama�o 
            Caption         =   "Ta&ma�o "
            Height          =   255
            Index           =   0
            Left            =   2880
            TabIndex        =   22
            Top             =   960
            Width           =   735
         End
         Begin VB.Label LblAlineacion 
            Caption         =   "A&lineaci�n"
            Height          =   255
            Index           =   0
            Left            =   120
            TabIndex        =   20
            Top             =   960
            Width           =   855
         End
         Begin VB.Label LblTipo 
            Caption         =   "Tipo de &Dato"
            Height          =   255
            Index           =   0
            Left            =   120
            TabIndex        =   18
            Top             =   600
            Width           =   975
         End
      End
      Begin MSComctlLib.ListView LstCampos 
         Height          =   2415
         Left            =   120
         TabIndex        =   15
         ToolTipText     =   "Campos de la Consulta"
         Top             =   2760
         Width           =   3375
         _ExtentX        =   5953
         _ExtentY        =   4260
         View            =   2
         LabelEdit       =   1
         LabelWrap       =   -1  'True
         HideSelection   =   -1  'True
         Checkboxes      =   -1  'True
         _Version        =   393217
         ForeColor       =   -2147483640
         BackColor       =   -2147483643
         BorderStyle     =   1
         Appearance      =   1
         NumItems        =   0
      End
      Begin VB.Label Lbl_Consulta 
         Caption         =   "Condiciones &Adicionales"
         Height          =   240
         Index           =   1
         Left            =   120
         TabIndex        =   11
         Top             =   1250
         Width           =   1815
      End
      Begin VB.Label Lbl_Consulta 
         Caption         =   "Consulta S&QL"
         Height          =   255
         Index           =   0
         Left            =   120
         TabIndex        =   9
         Top             =   120
         Width           =   1815
      End
      Begin VB.Label Lbl_Campos 
         Caption         =   "Campos Se&leccionados"
         Height          =   255
         Left            =   120
         TabIndex        =   14
         Top             =   2520
         Width           =   2175
      End
   End
   Begin MSComctlLib.Toolbar Tlb_Opciones 
      Align           =   1  'Align Top
      Height          =   420
      Left            =   0
      TabIndex        =   29
      Top             =   0
      Width           =   8265
      _ExtentX        =   14579
      _ExtentY        =   741
      ButtonWidth     =   609
      ButtonHeight    =   582
      Appearance      =   1
      _Version        =   393216
      BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
         NumButtons      =   9
         BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Style           =   3
         EndProperty
         BeginProperty Button2 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "G"
            Object.ToolTipText     =   "Guardar"
         EndProperty
         BeginProperty Button3 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "B"
            Object.ToolTipText     =   "Borrar"
         EndProperty
         BeginProperty Button4 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "I"
            Object.ToolTipText     =   "Imprimir"
         EndProperty
         BeginProperty Button5 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Style           =   3
         EndProperty
         BeginProperty Button6 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Enabled         =   0   'False
            Key             =   "C"
            Object.ToolTipText     =   "Cambiar C�digo"
         EndProperty
         BeginProperty Button7 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Enabled         =   0   'False
            Key             =   "P"
            Object.ToolTipText     =   "Generar plano"
         EndProperty
         BeginProperty Button8 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Style           =   3
         EndProperty
         BeginProperty Button9 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "A"
            Object.ToolTipText     =   "Ayuda"
         EndProperty
      EndProperty
   End
   Begin VB.Frame Fram_Base 
      Height          =   975
      Left            =   120
      TabIndex        =   0
      Top             =   480
      Width           =   8055
      Begin VB.TextBox TxtSeparador 
         Height          =   285
         Left            =   6360
         MaxLength       =   1
         TabIndex        =   7
         ToolTipText     =   "Caracter de separaci�n de los campos en el plano"
         Top             =   480
         Width           =   1575
      End
      Begin VB.CommandButton Cmd_Buscar 
         Height          =   300
         Left            =   1320
         Picture         =   "FrmGenerarPlano.frx":0939
         Style           =   1  'Graphical
         TabIndex        =   3
         TabStop         =   0   'False
         Top             =   480
         UseMaskColor    =   -1  'True
         Width           =   495
      End
      Begin VB.TextBox TxtCodigo 
         Height          =   285
         Left            =   120
         MaxLength       =   6
         TabIndex        =   2
         Top             =   480
         Width           =   1095
      End
      Begin VB.TextBox TxtNombre 
         Height          =   285
         Left            =   2040
         MaxLength       =   60
         TabIndex        =   5
         ToolTipText     =   "Nombre de la Consulta"
         Top             =   480
         Width           =   4215
      End
      Begin VB.Label LblSeparador 
         Caption         =   "&Separador de campos "
         Height          =   255
         Index           =   0
         Left            =   6360
         TabIndex        =   6
         Top             =   240
         Width           =   1575
      End
      Begin VB.Label LblCodigo 
         Caption         =   "&C�digo "
         Height          =   255
         Left            =   120
         TabIndex        =   1
         Top             =   240
         Width           =   615
      End
      Begin VB.Label LblNombre 
         Caption         =   "&Nombre"
         Height          =   255
         Left            =   2040
         TabIndex        =   4
         Top             =   240
         Width           =   615
      End
   End
End
Attribute VB_Name = "FrmGenerarPlano"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Dim arrEventos()  As C_Eventos
'Dim Arr_Ctrl()    As Variant       'DEPURACION DE CODIGO
''Dim ArrOrdCol(7)  As Variant      'DEPURACION DE CODIGO
Dim Archivo       As Object
Dim Arch          As Object
Dim RsAdo         As New ADODB.Recordset
Dim Tabla         As String
Dim TabOpc        As String
'Dim S             As String        'DEPURACION DE CODIGO
Public OpcCod     As String   'Opci�n de seguridad

Private Sub CboAlin_relle_KeyPress(Index As Integer, KeyAscii As Integer)
   Call Cambiar_Enter(KeyAscii)
End Sub

Private Sub CboAlineacion_KeyPress(Index As Integer, KeyAscii As Integer)
   Call Cambiar_Enter(KeyAscii)
End Sub

Private Sub CboTipoDato_KeyPress(Index As Integer, KeyAscii As Integer)
   Call Cambiar_Enter(KeyAscii)
End Sub

Private Sub CboTipoDato_LostFocus(Index As Integer)
If CboTipoDato(Index).ListIndex = 1 Then
   ChkDecimales(Index).Enabled = True
Else
   ChkDecimales(Index).Enabled = True
End If
End Sub

Private Sub Chkrellenar_KeyPress(Index As Integer, KeyAscii As MSForms.ReturnInteger)
   Call Cambiar_Enter_MsForms(KeyAscii)
End Sub

Private Sub Cmd_Ejecutar_Click()
   Call Limpiar_Controles
   Call Ejecutar_Consulta
End Sub

Private Sub Form_Activate()
   FrmMenu.CodOpc = OpcCod
End Sub

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
   Select Case KeyCode
      Case vbKeyF2: If Tlb_Opciones.Buttons("G").Enabled = True Then Call Tlb_Opciones_ButtonClick(Tlb_Opciones.Buttons("G"))
      Case vbKeyF3: If Tlb_Opciones.Buttons("B").Enabled = True Then Call Tlb_Opciones_ButtonClick(Tlb_Opciones.Buttons("B"))
      Case vbKeyF6: If Tlb_Opciones.Buttons("C").Enabled = True Then Call Tlb_Opciones_ButtonClick(Tlb_Opciones.Buttons("C"))
      Case vbKeyF7: Call Tlb_Opciones_ButtonClick(Tlb_Opciones.Buttons("P"))
   End Select
End Sub

Private Sub TxtAlias_KeyPress(Index As Integer, KeyAscii As Integer)
   Call ValKeyAlfaNum(KeyAscii)
End Sub

Private Sub Form_Unload(Cancel As Integer)
   Call DestruirControles(arrEventos)
End Sub

Private Sub TxtCodigo_KeyPress(KeyAscii As Integer)
   Call ValKeySinEspeciales(KeyAscii)
End Sub

Private Sub TxtCodigo_LostFocus()
Dim i As Integer
Dim Band As Boolean
Dim Cad As String
ReDim arr(7, 0)
Dim SQL     As String
Dim RsAdo   As New ADODB.Recordset

   On Error GoTo Error_Encabezado
   Band = False
   SQL = "Select NU_AUTO_ARPL,TX_DESC_ARPL,TX_CONS_ARPL,TX_COND_ARPL,TX_SEPA_ARPL"
   SQL = SQL & " From ARCHIVO_PLANO"
   SQL = SQL & " Where TX_CODI_ARPL=" & Comi & txtCodigo.Text & Comi
   RsAdo.Open SQL, BD(BDCurCon)
   
   While Not (RsAdo.EOF)
      Band = True
      txtCodigo.Tag = RsAdo.Fields(0).Value
      Cad = RsAdo.Fields(0).Value
      TxtNombre = RsAdo.Fields(1).Value
      TxtConsulta(0) = CStr(RsAdo.Fields(2).Value)
      If IsNumeric(Len(RsAdo.Fields(3).Value)) Then
         TxtConsulta(1) = RsAdo.Fields(3).Value
      Else
         TxtConsulta(1) = ""
      End If
      If IsNumeric(Len(RsAdo.Fields(4).Value)) Then
         TxtSeparador = Restaurar_Comas_Comillas(CStr(RsAdo.Fields(4).Value))
      Else
         TxtSeparador.Text = ""
      End If
      Desde = TabOpc
      Campos = "TX_DESC_CAPL,TX_TIPO_CAPL,NU_LONG_CAPL,TX_ALIN_CAPL,TX_RELL_CAPL,TX_ALRE_CAPL,TX_CARE_CAPL,TX_SEDE_CAPL"
      Condicion = " NU_AUTO_ARPL_CAPL =" & Cad
      Condicion = Condicion & " ORDER BY NU_AUTO_ARPL_CAPL"
      Result = LoadMulData(Desde, Campos, Condicion, arr)

      If Result <> FAIL Then 'And Encontro Then
         Call Limpiar_Controles
         lstCAMPOS.ListItems.Clear
         For i = 0 To UBound(arr, 2)
            lstCAMPOS.ListItems.Add , , arr(0, i)
         Next
         Call Cargar_Para(Fram_Parametros)
         Call Cargar_Para(LblDesCamp)
         Call Cargar_Para(LblTipo)
         Call Cargar_Para(CboTipoDato)
         Call Cargar_Para(LblAlineacion)
         Call Cargar_Para(CboAlineacion)
         Call Cargar_Para(LblTama�o)
         Call Cargar_Para(TxtTama�o)
         Call Cargar_Para(ChkDecimales)
         Call Cargar_Para(LblLinea)
         Call Cargar_Para(Chkrellenar)
         Call Cargar_Para(LblRelleno)
         Call Cargar_Para(CboAlin_relle)
         Call Cargar_Para(LblCaracter)
         Call Cargar_Para(TxtCaracter)
         For i = 0 To lstCAMPOS.ListItems.Count - 1
            CboTipoDato(i).ListIndex = arr(1, i)
            CboAlineacion(i).ListIndex = arr(3, i)
            TxtTama�o(i).Text = arr(2, i)
            ChkDecimales(i).Value = IIf(arr(7, i) = "1", True, False)
            Chkrellenar(i).Value = IIf(arr(4, i) = "1", True, False)
            If arr(4, i) = "1" Then
               CboAlin_relle(i).ListIndex = arr(5, i)
               TxtCaracter(i).Text = Restaurar_Comas_Comillas(CStr(arr(6, i)))
               If TxtCaracter(i).Text = NUL$ Then TxtCaracter(i).Text = " "
            End If
         Next
         lstCAMPOS.ListItems(1).Checked = True
         LblDesCamp(0).Caption = lstCAMPOS.ListItems(1).Text
         Fram_Parametros(0).ZOrder 0
         Tlb_Opciones.Buttons("C").Enabled = True
         Tlb_Opciones.Buttons("P").Enabled = True
      Else
         Call Limpiar(False)
      End If
      RsAdo.MoveNext
   Wend
   
   If Band = False Then Call Limpiar(False)
   RsAdo.Close
Exit Sub

Error_Encabezado:
   Call ConvertErr
   RsAdo.Close
   Exit Sub


End Sub

Private Sub TxtConsulta_Change(Index As Integer)
   If Index = 0 Then Cmd_Ejecutar.Enabled = IIf(Trim(TxtConsulta(Index)) <> NUL$, True, False)
End Sub

Private Sub TxtConsulta_KeyPress(Index As Integer, KeyAscii As Integer)
   If KeyAscii <> 13 Then Call ValKeyAlfaNum(KeyAscii)
End Sub

Private Sub TxtNombre_KeyPress(KeyAscii As Integer)
   Call ValKeyAlfaNum(KeyAscii)
End Sub
Private Sub TxtSeparador_KeyPress(KeyAscii As Integer)
   Call ValKeyAlfaNum(KeyAscii)
End Sub

Private Sub TxtTama�o_KeyPress(Index As Integer, KeyAscii As Integer)
   Call ValKeyNum(KeyAscii)
End Sub

Private Sub TxtCaracter_KeyPress(Index As Integer, KeyAscii As Integer)
   Call Cambiar_Enter(KeyAscii)
End Sub

Private Sub Chkrellenar_Click(Index As Integer)
   If Chkrellenar(Index).Value = True Then
      CboAlin_relle(Index).Enabled = True
      TxtCaracter(Index).Enabled = True
   Else
      CboAlin_relle(Index).Enabled = False
      TxtCaracter(Index).Enabled = False
   End If
End Sub
Private Sub Cmd_Buscar_Click()
Dim Arrsel(3, 1) As Variant
Dim Codigo As String
   Arrsel(0, 0) = "TX_CODI_ARPL": Arrsel(1, 0) = "C�digo": Arrsel(2, 0) = "1000": Arrsel(3, 0) = "T"
   Arrsel(0, 1) = "TX_DESC_ARPL": Arrsel(1, 1) = "Descripci�n": Arrsel(2, 1) = "5000": Arrsel(3, 1) = "T"
   Codigo = Seleccion_General("ARCHIVO_PLANO", NUL$, Arrsel, 0, False, "Archivos Planos")
   DoEvents
   If Codigo <> NUL$ Then
      txtCodigo = Codigo
      txtCodigo.SetFocus
      SendKeys "{TAB}"
   End If
End Sub

Private Sub Cargar_Para(Ctrl As Object)
Dim i As Integer
   
   For i = 1 To lstCAMPOS.ListItems.Count - 1
      Load (Ctrl(i))
      Ctrl(i).Top = Ctrl(0).Top
      Ctrl(i).Left = Ctrl(0).Left
      Ctrl(i).Visible = True
      If Ctrl(i).Name = "CboAlineacion" Or Ctrl(i).Name = "CboAlin_relle" Then
         Ctrl(i).AddItem "Izquierda"
         Ctrl(i).AddItem "Derecha"
      End If
      If Ctrl(i).Name = "CboTipoDato" Then
         Ctrl(i).AddItem "Texto"
         Ctrl(i).AddItem "Numerico"
         Ctrl(i).AddItem "Fecha"
      End If
      On Error Resume Next
      Set Ctrl(i).Container = Fram_Parametros(i)
   Next

End Sub

Private Sub LstCampos_ItemCheck(ByVal Item As MSComctlLib.ListItem)
     Dim i As Integer
     For i = 1 To lstCAMPOS.ListItems.Count
        If lstCAMPOS.ListItems(i).Index <> Item.Index Then
            lstCAMPOS.ListItems(i).Checked = False
        End If
     Next i
End Sub

Private Sub LstCampos_ItemClick(ByVal Item As MSComctlLib.ListItem)
    Dim i As Integer
    For i = 1 To lstCAMPOS.ListItems.Count
         If lstCAMPOS.ListItems(i).Index <> Item.Index Then
            lstCAMPOS.ListItems(i).Checked = False
        Else
            Item.Checked = True
            LblDesCamp(i - 1).Caption = lstCAMPOS.ListItems(i).Text
            If CboTipoDato(i - 1).ListIndex = 1 Then ChkDecimales(i - 1).Enabled = True
        End If
    Next i

    For i = 1 To lstCAMPOS.ListItems.Count
      If lstCAMPOS.ListItems(i).Selected = True Then
          Fram_Parametros(i - 1).ZOrder
          Fram_Parametros(i - 1).Visible = True
          Exit For
      End If
    Next i
End Sub

Private Sub Tlb_Opciones_ButtonClick(ByVal Button As MSComctlLib.Button)
   Select Case Button.Key
      Case "A": Ejecutar_Consulta
'      Case "G": Grabar (1)
      Case "G": Grabar  'DEPURACION DE CODIGO
      Case "P": Generar_Archivo
      Case "C": Cambiar
      Case "B": Borrar
   End Select
End Sub

Private Sub Form_Load()
   Tabla = "ARCHIVO_PLANO"
   TabOpc = "CAMPO_PLANO"
   'Call EventosControles(Me, arrEventos)
   Call Estado_Botones_Forma(Tlb_Opciones, OpcCod)
   Set Arch = CreateObject("Scripting.FileSystemObject")
   Set Tlb_Opciones.ImageList = MDI_Inventarios.Img_List
   Tlb_Opciones.Buttons("G").Image = 1
   Tlb_Opciones.Buttons("B").Image = 2
   Tlb_Opciones.Buttons("I").Image = 3
   Tlb_Opciones.Buttons("C").Image = 5
   Tlb_Opciones.Buttons("A").Image = 6
   Tlb_Opciones.Buttons("P").Image = 18
   Set Tlb_Opciones.DisabledImageList = MDI_Inventarios.Img_Dis
   Call CenterForm(MDI_Inventarios, Me)
End Sub

Private Sub Generar_Archivo()
'Dim arrtmp()   As Variant  'DEPURACION DE CODIGO
Dim SQL        As String
Dim RsAdo      As New ADODB.Recordset
Dim Separador  As String

Call MouseClock

If txtCodigo.Tag = NUL$ Then Call Mensaje1("No se ha guardado la informaci�n", 3): Exit Sub

SQL = "Select TX_CONS_ARPL,TX_COND_ARPL,TX_SEPA_ARPL"
SQL = SQL & " From ARCHIVO_PLANO"
SQL = SQL & " Where NU_AUTO_ARPL=" & CLng(txtCodigo.Tag)
RsAdo.Open SQL, BD(BDCurCon)
While Not (RsAdo.EOF)
   SQL = CStr(RsAdo.Fields(0).Value & " " & RsAdo.Fields(1).Value)
   If IsNumeric(Len(RsAdo.Fields(2).Value)) Then
      Separador = Restaurar_Comas_Comillas(CStr(RsAdo.Fields(2).Value))
   Else
      Separador = ""
   End If
   RsAdo.MoveNext
Wend
RsAdo.Close

If Result <> FAIL Then Result = Archivo_Plano(SQL, Separador, 1, Guardar, Archivo, Arch, CLng(txtCodigo.Tag))
If Result <> FAIL Then Call Mensaje1("El archivo fue generado con exito", 3)

Call MouseNorm
End Sub

Private Sub Ejecutar_Consulta()
Dim i       As Integer
Dim SQL     As String
'Dim Index   As Integer     'DEPURACION DE CODIGO
Dim RsAdo   As New ADODB.Recordset

   LblDesCamp(0).Caption = "Nombre del Campo"
   CboTipoDato(0).ListIndex = -1
   CboAlineacion(0).ListIndex = -1
   TxtTama�o(0).Text = NUL$
   Chkrellenar(0).Value = False
   CboAlin_relle(0).ListIndex = -1
   TxtCaracter(0).Text = NUL$
   
   SQL = Trim(TxtConsulta(0))
   SQL = SQL & " " & Trim(TxtConsulta(1))
   lstCAMPOS.ListItems.Clear
   On Error GoTo Error_Handler
   RsAdo.Open SQL, BD(BDCurCon)
   
   While Not (RsAdo.EOF)
      For i = 0 To RsAdo.Fields.Count - 1
         lstCAMPOS.ListItems.Add , , RsAdo.Fields(i).Name
      Next
      Call Cargar_Para(Fram_Parametros)
      Call Cargar_Para(LblDesCamp)
      Call Cargar_Para(LblTipo)
      Call Cargar_Para(CboTipoDato)
      Call Cargar_Para(LblAlineacion)
      Call Cargar_Para(CboAlineacion)
      Call Cargar_Para(LblTama�o)
      Call Cargar_Para(TxtTama�o)
      Call Cargar_Para(ChkDecimales)
      Call Cargar_Para(LblLinea)
      Call Cargar_Para(Chkrellenar)
      Call Cargar_Para(LblRelleno)
      Call Cargar_Para(CboAlin_relle)
      Call Cargar_Para(LblCaracter)
      Call Cargar_Para(TxtCaracter)
      RsAdo.Close
      Exit Sub
   Wend
Exit Sub

Error_Handler:
   Call ConvertErr
   Exit Sub
End Sub

Private Sub Borrar()
Dim arr(0) As Variant

   DoEvents
   Call MouseClock

   If txtCodigo = NUL$ Then
      Call Mensaje1("Se debe especificar el codigo del archivo plano", 3)
      Call MouseNorm: Exit Sub
   End If
   
   Campos = "NU_AUTO_ARPL"
   Condicion = "TX_CODI_ARPL=" & Comi & txtCodigo & Comi
   Result = LoadData(Tabla, Campos, Condicion, arr)
   If Encontro Then
      If Not WarnDel() Then Call MouseNorm: Exit Sub
      If (BeginTran(STranDel & Tabla) <> FAIL) Then
         Condicion = "NU_AUTO_ARPL_CAPL=" & CDbl(arr(0))
         Result = DoDelete(TabOpc, Condicion)
         If Result <> FAIL Then
            Condicion = "NU_AUTO_ARPL=" & arr(0)
            Result = DoDelete(Tabla, Condicion)
         End If
      End If
   End If

   If (Result <> FAIL) Then
      If (CommitTran() <> FAIL) Then
         Call Limpiar(True)
         Call InfoDelete
      Else
         Call RollBackTran
      End If
   Else
      Call RollBackTran
   End If
   
   Call MouseNorm
   
End Sub

'--------------------
Private Sub Limpiar_Controles()
Dim i As Integer
   For i = 1 To lstCAMPOS.ListItems.Count - 1
      Unload LblTipo(i)
      Unload LblAlineacion(i)
      Unload LblTama�o(i)
      Unload LblRelleno(i)
      Unload LblCaracter(i)
      Unload TxtTama�o(i)
      Unload CboTipoDato(i)
      Unload CboAlineacion(i)
      Unload CboAlin_relle(i)
      Unload TxtCaracter(i)
      Unload ChkDecimales(i)
      Unload LblLinea(i)
      Unload Chkrellenar(i)
      Unload LblDesCamp(i)
      Unload Fram_Parametros(i)
   Next
End Sub

'band bandera que determina desde donde es llamada la funcion
' 0 (boton guardar), 1 (Boton Salir)
'Sub Grabar(ByRef Band As Integer, Optional Cambiar As String)
Sub Grabar(Optional Cambiar As String)  'DEPURACION DE CODIGO
ReDim arr(0)
Dim i As Integer
Dim Cmd As String

On Error GoTo Error_Grabar
   
   Call MouseClock

   If Validaciones = FAIL Then Call MouseNorm: Exit Sub
   
   Condicion = "TX_CODI_ARPL=" & Comi & txtCodigo & Comi
   Result = LoadData(Tabla, "NU_AUTO_ARPL", Condicion, arr)
   If Result <> FAIL Then
      If (Not Encontro) Then
         If Not WarnIns() Then Call MouseNorm: Result = FAIL
      Else
         If Not WarnUpd() Then Call MouseNorm: Result = FAIL
      End If
   End If
   
   If Result = FAIL Then Exit Sub
   
   If (BeginTran(STranDel & Tabla) <> FAIL) Then
      Campos = "TX_CODI_ARPL"
      Cmd = Comi & IIf(Cambiar = NUL$, txtCodigo, Cambiar) & Comi & Coma
      Valores = Campos & "=" & Cmd
      
      Campos = Campos & ",TX_DESC_ARPL"
      Valores = Valores & "TX_DESC_ARPL"
      Cmd = Cmd & Comi & Cambiar_Comas_Comillas(TxtNombre) & Comi & Coma
      Valores = Valores & "=" & Comi & Cambiar_Comas_Comillas(TxtNombre) & Comi & Coma
      
      Campos = Campos & ",TX_CONS_ARPL"
      Valores = Valores & "TX_CONS_ARPL"
      Cmd = Cmd & Comi & Replace(TxtConsulta(0), "'", "''") & Comi & Coma
      Valores = Valores & "=" & Comi & Replace(TxtConsulta(0), "'", "''") & Comi & Coma
      
      Campos = Campos & ",TX_COND_ARPL"
      Valores = Valores & "TX_COND_ARPL"
      Cmd = Cmd & Comi & Replace(TxtConsulta(1), "'", "''") & Comi & Coma
      Valores = Valores & "=" & Comi & Replace(TxtConsulta(1), "'", "''") & Comi & Coma
      
      If Trim(TxtSeparador) = NUL$ Then
         Campos = Campos & ",TX_SEPA_ARPL"
         Valores = Valores & "TX_SEPA_ARPL"
         Cmd = Cmd & Comi & TxtSeparador & Comi
         Valores = Valores & "=" & Comi & TxtSeparador & Comi
      Else
         Campos = Campos & ",TX_SEPA_ARPL"
         Valores = Valores & "TX_SEPA_ARPL"
         Cmd = Cmd & Comi & Cambiar_Comas_Comillas(TxtSeparador) & Comi
         Valores = Valores & "=" & Comi & Cambiar_Comas_Comillas(TxtSeparador) & Comi
      End If

      If Not Encontro Then
         Cmd = "Insert Into " & Tabla & "(" & Campos & ")" & " Values" & "(" & Cmd & ")"
         BD(BDCurCon).Execute Cmd, CountRecords
         Result = SUCCEED
      Else
         Cmd = "Update " & Tabla & " Set " & Valores & IIf(Len(Condicion) > 0, " Where " & Condicion, NUL$)
         BD(BDCurCon).Execute Cmd, CountRecords
         Result = SUCCEED
      End If
      If (Result <> FAIL) Then
         Condicion = "TX_CODI_ARPL=" & Comi & IIf(Cambiar = NUL$, txtCodigo, Cambiar) & Comi
         Result = LoadData(Tabla, "NU_AUTO_ARPL", Condicion, arr)
      End If
   
      If Result <> FAIL Then  'Borra los registro relacionados
         Condicion = "NU_AUTO_ARPL_CAPL=" & CDbl(arr(0))
         Result = DoDelete(TabOpc, Condicion)
      End If
   
      If Result <> FAIL And Encontro Then
         For i = 1 To lstCAMPOS.ListItems.Count
            Valores = "NU_AUTO_ARPL_CAPL=" & CDbl(arr(0)) & Coma
            Valores = Valores & "TX_DESC_CAPL=" & Comi & Cambiar_Comas_Comillas(lstCAMPOS.ListItems(i).Text) & Comi & Coma
            Valores = Valores & "TX_TIPO_CAPL=" & Comi & CboTipoDato(i - 1).ListIndex & Comi & Coma
            Valores = Valores & "NU_LONG_CAPL=" & TxtTama�o(i - 1) & Coma
            Valores = Valores & "TX_ALIN_CAPL=" & Comi & CboAlineacion(i - 1).ListIndex & Comi & Coma
            Valores = Valores & "TX_SEDE_CAPL=" & Comi & IIf(ChkDecimales(i - 1).Value = 0, 0, 1) & Comi & Coma
            Valores = Valores & "TX_RELL_CAPL=" & Comi & IIf(Chkrellenar(i - 1).Value = 0, 0, 1) & Comi
            If CboAlin_relle(i - 1).ListIndex <> -1 Then
               Valores = Valores & Coma & "TX_ALRE_CAPL=" & Comi & CboAlin_relle(i - 1).ListIndex & Comi & Coma
               Valores = Valores & "TX_CARE_CAPL=" & Comi & TxtCaracter(i - 1) & Comi
            End If
            Result = DoInsertSQL(TabOpc, Valores)
            If Result = FAIL Then Exit For
         Next
      End If
   End If

   If (Result <> FAIL) Then
      If (CommitTran() <> FAIL) Then
         Call InfoSave
         Call Limpiar(True)
      Else
         Call RollBackTran
      End If
   Else
      Call RollBackTran
   End If

   Call MouseNorm
   
   Exit Sub

Error_Grabar:
   Call ConvertErr
   Exit Sub

End Sub

Private Function Validaciones() As Integer
Dim i As Integer
Dim j As Integer
   
   Validaciones = FAIL
   
   If txtCodigo = NUL$ Then
      Call Mensaje1("Se debe especificar el c�digo de la consulta ", 3)
      txtCodigo.SetFocus: Call MouseNorm: Exit Function
   End If

   If TxtConsulta(0) = NUL$ Then
      Call Mensaje1("Se debe especificar la consulta a ejecutar", 3)
      TxtConsulta(0).SetFocus: Call MouseNorm: Exit Function
   End If

   If TxtNombre = NUL$ Then
      Call Mensaje1("Se debe especificar el nombre de la consulta", 3)
      TxtNombre.SetFocus: Call MouseNorm: Exit Function
   End If

'''   If TxtSeparador = NUL$ Then
'''      Call Mensaje1("Se debe especificar el caracter con el que desea separar los campos", 3)
'''      TxtSeparador.SetFocus: Call MouseNorm: Exit Function
'''   End If

   For i = 0 To lstCAMPOS.ListItems.Count - 1

      If CboTipoDato(i).ListIndex = -1 Then
         Call Mensaje1("Se debe especificar el tipo de dato del campo", 3)
         CboTipoDato(i).SetFocus: Exit For
      End If

      If CboAlineacion(i).ListIndex = -1 Then
         Call Mensaje1("Se debe especificar la alineaci�n del campo", 3)
         CboAlineacion(i).SetFocus: Exit For
      End If

      If TxtTama�o(i) = NUL$ Then
         Call Mensaje1("Se debe especificar el tama�o del campo", 3)
         TxtTama�o(i).SetFocus: Exit For
      End If

      If Chkrellenar(i).Value = False Then
         CboAlin_relle(i).Enabled = False
         TxtCaracter(i).Enabled = False
      Else
         CboAlin_relle(i).Enabled = True
         TxtCaracter(i).Enabled = True
         If CboAlin_relle(i).ListIndex = -1 Then
            Call Mensaje1("Se debe especificar la alineaci�n del relleno", 3)
            CboAlin_relle(i).SetFocus: Exit For
         End If
         If TxtCaracter(i) = NUL$ Then
            Call Mensaje1("Se debe especificar el caracter del relleno", 3)
            TxtCaracter(i).SetFocus: Exit For
         End If
      End If
   Next
   
   If lstCAMPOS.ListItems.Count = 0 Then
      Call Mensaje1("Se debe seleccionar al menos un campo", 3)
      lstCAMPOS.SetFocus: Call MouseNorm: Exit Function
   End If
   
   If Not (i > lstCAMPOS.ListItems.Count - 1) Then
      For j = 1 To lstCAMPOS.ListItems.Count
         lstCAMPOS.ListItems(j).Checked = False
      Next j
      lstCAMPOS.ListItems(i + 1).Checked = True
      Fram_Parametros(i).ZOrder 0
      Exit Function
   End If
   
   Validaciones = SUCCEED
   
End Function

Private Sub Limpiar(ByVal IndFoco As Boolean)
'Dim i As Integer       'DEPURACION DE CODIGO
   
   TxtConsulta(0) = NUL$
   TxtConsulta(1) = NUL$
   TxtNombre = NUL$
   TxtSeparador = NUL$
   TxtTama�o(0).Text = NUL$
   CboAlineacion(0).ListIndex = -1
   ChkDecimales(0).Value = False
   ChkDecimales(0).Enabled = False
   Chkrellenar(0).Value = False
   CboAlin_relle(0).ListIndex = -1
   CboTipoDato(0).ListIndex = -1
   TxtCaracter(0).Text = NUL$
   
   Call Limpiar_Controles
   LblDesCamp(0).Caption = NUL$
   lstCAMPOS.ListItems.Clear
   If IndFoco = True Then
      txtCodigo = NUL$
      txtCodigo.Tag = NUL$
      txtCodigo.SetFocus
   End If
   Tlb_Opciones.Buttons("P").Enabled = False
   Tlb_Opciones.Buttons("C").Enabled = False
End Sub

Private Sub Cambiar()
Dim Nuevo As String

   If Tlb_Opciones.Buttons("C").Enabled = False Then Exit Sub
   Nuevo = Cambiar_Codigo(txtCodigo.MaxLength, "T")
   If Nuevo <> NUL$ Then
'      Call Grabar(0, Nuevo)
      Call Grabar(Nuevo)    'DEPURACION DE CODIGO
   End If
End Sub

