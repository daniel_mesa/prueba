Attribute VB_Name = "MontoEsc"
Public Function Monto_Escrito(ByVal TOTAL As Currency) As String


    ' DAHV M4340
    ' Se realiza cambio de la funcion monto escrito dejandola igual que el modulo de CXP
    ' Adicionalmente se crea la funci�n Monto_Centavos
    
    Dim valor As String
    Dim tex
    Dim a, e, G, i As Integer
    Dim Decenas, indice, Opcion
    Dim Cuatorcenas
    Dim cadena As String
    Dim Pos As Variant
    Dim num As Integer
    Dim MONTO As Variant
    Dim Posiputo As Long
    Dim Valcent As String
    Dim Valpesos As String

   'busca posicion del punto
   Posiputo = InStr(1, CStr(TOTAL), ".", 1)
   'toma los centavos solamente
   '-----------------------------
   'DAHV M4340 -- INICIO
   'Se adiciona un cero en caso que el total enviado a la funci�n solo tenga un digito en la parte de los centavos.
   'Valcent = Mid(CStr(TOTAL), (Posiputo + 1), 2)
   Valcent = Mid(CStr(TOTAL) & "0", (Posiputo + 1), 2)
   'DAHV M4340 -- FIN
   '-----------------------------
   'toma solo los pesos
    
   If Posiputo = 0 Then
      Valpesos = TOTAL
   Else
      Valpesos = Mid(CStr(TOTAL), 1, (Posiputo - 1))
   End If
   TOTAL = CDbl(Valpesos)
    
    If TOTAL = 0 Then
       Monto_Escrito = "CERO PESOS" + Monto_Centavos(Valcent): Exit Function
    End If
    
    Pos = Array(0, 3, 2, 1, 4, 3, 2, 1, 8, 3, 2, 1)
    Decenas = Array("DIEZ ", "ONCE ", "DOCE ", "TRECE ", "CATORCE ", "QUINCE ", "DIECISEIS ", "DIECISIETE ", "DIECIOCHO ", "DIECINUEVE ")
    Cuatorcenas = Array(" ", "", "VEINT", "TREINTA ", "CUARENTA ", "CINCUENTA ", "SESENTA ", "SETENTA ", "OCHENTA ", "NOVENTA ")
    MONTO = Array(, , , , , , , , , , " ", _
            "UN ", "DOS ", "TRES ", "CUATRO ", "CINCO ", "SEIS ", "SIETE ", "OCHO ", "NUEVE ", _
            "", "", "VEINT", "TREINTA ", "CUARENTA ", "CINCUENTA ", "SESENTA ", "SETENTA ", "OCHENTA ", "NOVENTA ", _
            "", "", "DOSCIENTOS ", "TRESCIENTOS ", "CUATROCIENTOS ", "QUINIENTOS ", "SEISCIENTOS ", "SETECIENTOS ", "OCHOCIENTOS ", "NOVECIENTOS ")
    valor = ""
    a = 0: e = 1
    tex = Str(TOTAL)
    tex = Format(Val(tex), "###,###")
    i = 12 - Len(tex)
    cadena = Mid(tex, e, e)
    On Error GoTo siga
    Do While i < 12
            num = Val(cadena)
            indice = Pos(i)
            Opcion = indice * 10 + num
            Select Case Opcion
                Case 20, 30, 10
                Case 31
                        valor = valor + "CIEN" ' DOS POCIONES ADELANTE
                        If Val(Mid(tex, e, 3)) > 100 Then valor = valor + "TO "
                Case 21
                        valor = valor + Decenas(Val(Mid(tex, e + 1, e - a)))
                        a = a + 1
                        e = e + 1
                        i = i + 1
                Case 22
                        If Val(Mid(tex, e + 1, e - a)) = 0 Then
                            valor = valor + "VEINTE "
                            a = a + 1
                            e = e + 1
                            i = i + 1
                        Else
                            valor = valor + "VEINTI"
                        End If
                Case 23 To 29
                        valor = valor + Cuatorcenas(num)
                        If Val(Mid(tex, e + 1, e - a)) > 0 Then valor = valor + " Y "
                Case 40
                        If Val(Mid(tex, e - a, e - a + 1)) > 1 Then
                            valor = valor + "MILLONES "
                        ElseIf Val(Mid(tex, e - a, e - a + 1)) = 1 Then
                            valor = valor + "MILLON "
                        End If
                Case 80
                        If Val(Mid(tex, e - a, e)) = 1 And a = 1 Then
                            valor = "MIL "
                        Else
                            G = -1
                            On Error Resume Next
                            G = Val(Mid(tex, e - 3, 3))
                            If G = -1 Or G > 0 Then
                                valor = valor + " MIL "
                            End If
                        End If
                Case Else
                        valor = valor + MONTO(Opcion)
        End Select
               a = a + 1
               e = e + 1
               i = i + 1
               cadena = Mid(tex, e, e - a)
    Loop
siga:
    Dim millon As String
    millon = TOTAL / 1000000
    If InStr(1, millon, ".", 1) = 0 Then
        valor = valor + "DE PESOS"
    Else
        valor = valor + "PESOS"
    End If
    If Posiputo > 0 Then
       valor = valor + Monto_Centavos(Valcent)
    Else
       valor = valor & " MCTE"
    End If
    Debug.Print valor
    Monto_Escrito = valor




'    Dim valor As String
'    Dim tex
'    Dim cadena As String
'    Dim Pos As Variant
'    Dim num As Integer
'    Dim MONTO As Variant
'    Pos = Array(0, 3, 2, 1, 4, 3, 2, 1, 8, 3, 2, 1)
'    Decenas = Array("DIEZ ", "ONCE ", "DOCE ", "TRECE ", "CATORCE ", "QUINCE ", "DIECISEIS ", "DIECISIETE ", "DIECIOCHO ", "DIECINUEVE ")
'    Cuatorcenas = Array(" ", "", "VEINT", "TREINTA", "CUARENTA", "CINCUENTA", "SESENTA", "SETENTA", "OCHENTA", "NOVENTA")
'    MONTO = Array(, , , , , , , , , , " ", _
'            "UN ", "DOS ", "TRES ", "CUATRO ", "CINCO ", "SEIS ", "SIETE ", "OCHO ", "NUEVE ", _
'            "", "", "VEINT", "TREINTA", "CUARENTA", "CINCUENTA", "SESENTA", "SETENTA", "OCHENTA", "NOVENTA", _
'            "", "", "DOSCIENTOS ", "TRESCIENTOS ", "CUATROCIENTOS ", "QUINIENTOS ", "SEISCIENTOS ", "SETECIENTOS ", "OCHOCIENTOS ", "NOVECIENTOS ")
'    valor = ""
'    a = 0: e = 1
'    tex = Str(TOTAL)
'    tex = Format(Val(tex), "##,##")
'    i = 12 - Len(tex)
'    cadena = Mid(tex, e, e)
'    On Error GoTo siga
'    Do While i < 12
'            num = Val(cadena)
'            indice = Pos(i)
'            Opcion = indice * 10 + num
'            Select Case Opcion
'                Case 20, 30, 10
'                Case 31
'                        valor = valor + "CIEN" ' DOS POCIONES ADELANTE
'                        If Val(Mid(tex, e, 3)) > 100 Then valor = valor + "TO "
'                Case 21
'                        valor = valor + Decenas(Val(Mid(tex, e + 1, e - a)))
'                        a = a + 1
'                        e = e + 1
'                        i = i + 1
'                Case 22
'                        If Val(Mid(tex, e + 1, e - a)) = 0 Then
'                            valor = valor + "VEINTE "
'                            a = a + 1
'                            e = e + 1
'                            i = i + 1
'                        Else
'                            valor = valor + "VEINTI"
'                        End If
'                Case 23 To 29
'                        valor = valor + Cuatorcenas(num)
'                        If Val(Mid(tex, e + 1, e - a)) > 0 Then valor = valor + " Y "
'                Case 40
'                        If Val(Mid(tex, e - a, e - a + 1)) > 1 Then
'                            valor = valor + " MILLONES "
'                        ElseIf Val(Mid(tex, e - a, e - a + 1)) = 1 Then
'                            valor = valor + "MILLON "
'                        End If
'                Case 80
'                        If Val(Mid(tex, e - a, e)) = 1 And a = 1 Then
'                            valor = "MIL "
'                        Else
'                            G = -1
'                            On Error Resume Next
'                            G = Val(Mid(tex, e - 3, 3))
'                            If G = -1 Or G > 0 Then
'                                valor = valor + " MIL "
'                            End If
'                        End If
'                Case Else
'                        valor = valor + MONTO(Opcion)
'        End Select
'               a = a + 1
'               e = e + 1
'               i = i + 1
'               cadena = Mid(tex, e, e - a)
'    Loop
'siga:
'    Monto_Escrito = valor + " PESOS MCTE "
End Function
Function Monto_Centavos(Vlcenta As String)
    Dim valor As String
    Dim a, e, i As Integer
    Dim tex, Decenas, Cuatorcenas, indice, Opcion
    Dim cadena As String
    Dim Pos As Variant
    Dim num As Integer
    Dim MONTO As Variant
    'si no tiene centavos no escribe nada
    If CDbl(Vlcenta) = 0 Then Monto_Centavos = " MCTE": Exit Function

    Pos = Array(0, 3, 2, 1, 4, 3, 2, 1, 8, 3, 2, 1)
    Decenas = Array("DIEZ ", "ONCE ", "DOCE ", "TRECE ", "CATORCE ", "QUINCE ", "DIECISEIS ", "DIECISIETE ", "DIECIOCHO ", "DIECINUEVE ")
    Cuatorcenas = Array(" ", "", "VEINT", "TREINTA ", "CUARENTA ", "CINCUENTA ", "SESENTA ", "SETENTA ", "OCHENTA ", "NOVENTA ")
    MONTO = Array(, , , , , , , , , , " ", _
            "UN ", "DOS ", "TRES ", "CUATRO ", "CINCO ", "SEIS ", "SIETE ", "OCHO ", "NUEVE ", _
            "", "", "VEINT", "TREINTA ", "CUARENTA ", "CINCUENTA ", "SESENTA ", "SETENTA ", "OCHENTA ", "NOVENTA ", _
            "", "", "DOSCIENTOS ", "TRESCIENTOS ", "CUATROCIENTOS ", "QUINIENTOS ", "SEISCIENTOS ", "SETECIENTOS ", "OCHOCIENTOS ", "NOVECIENTOS ")
    valor = " CON "
    a = 0: e = 1
    tex = Vlcenta
    i = 12 - Len(tex)
    cadena = Mid(tex, e, e)
    On Error GoTo siga
    Do While i < 12
            num = Val(cadena)
            indice = Pos(i)
            Opcion = indice * 10 + num
            Select Case Opcion
                Case 20, 30, 10
                Case 21
                        valor = valor + Decenas(Val(Mid(tex, e + 1, e - a)))
                        a = a + 1
                        e = e + 1
                        i = i + 1
                Case 22
                        If Val(Mid(tex, e + 1, e - a)) = 0 Then
                            valor = valor + "VEINTE "
                            a = a + 1
                            e = e + 1
                            i = i + 1
                        Else
                            valor = valor + "VEINTI"
                        End If
                Case 23 To 29
                        valor = valor + Cuatorcenas(num)
                        If Val(Mid(tex, e + 1, e - a)) > 0 Then valor = valor + " Y "
                Case Else
                        valor = valor + MONTO(Opcion)
        End Select
               a = a + 1
               e = e + 1
               i = i + 1
               cadena = Mid(tex, e, e - a)
    Loop
siga:
    Monto_Centavos = valor & " CENTAVOS MCTE"
End Function
Function Char(ASCII As Integer) As String
    Select Case ASCII
        Case 48
            Char = "0"
        Case 49
            Char = "1"
        Case 50
            Char = "2"
        Case 51
            Char = "3"
        Case 52
            Char = "4"
        Case 53
            Char = "5"
        Case 54
            Char = "6"
        Case 55
            Char = "7"
        Case 56
            Char = "8"
        Case 57
            Char = "9"
    End Select
End Function

