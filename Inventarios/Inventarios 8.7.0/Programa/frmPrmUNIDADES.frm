VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Object = "{C932BA88-4374-101B-A56C-00AA003668DC}#1.1#0"; "MSMASK32.OCX"
Object = "{0BA686C6-F7D3-101A-993E-0000C0EF6F5E}#1.0#0"; "THREED32.OCX"
Begin VB.Form frmPrmUNIDADES 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Unidades de conteo, distribuci�n"
   ClientHeight    =   5940
   ClientLeft      =   45
   ClientTop       =   435
   ClientWidth     =   10725
   Icon            =   "frmPrmUNIDADES.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   5940
   ScaleWidth      =   10725
   Begin MSComctlLib.TreeView arbUnidades 
      Height          =   4335
      Left            =   480
      TabIndex        =   0
      Top             =   960
      Width           =   4455
      _ExtentX        =   7858
      _ExtentY        =   7646
      _Version        =   393217
      Indentation     =   529
      LabelEdit       =   1
      LineStyle       =   1
      Style           =   7
      Appearance      =   1
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin VB.Frame Frame1 
      Caption         =   "Informaci�n:"
      Height          =   3615
      Left            =   5160
      TabIndex        =   1
      Top             =   960
      Width           =   5415
      Begin MSMask.MaskEdBox txtDIVI 
         Height          =   375
         Left            =   2640
         TabIndex        =   7
         Top             =   2760
         Width           =   800
         _ExtentX        =   1402
         _ExtentY        =   661
         _Version        =   393216
         PromptInclude   =   0   'False
         MaxLength       =   6
         PromptChar      =   "_"
      End
      Begin MSMask.MaskEdBox txtNOMB 
         Height          =   855
         Left            =   1560
         TabIndex        =   5
         Top             =   1080
         Width           =   3615
         _ExtentX        =   6376
         _ExtentY        =   1508
         _Version        =   393216
         PromptInclude   =   0   'False
         MaxLength       =   50
         PromptChar      =   "_"
      End
      Begin MSMask.MaskEdBox txtMULT 
         Height          =   375
         Left            =   2640
         TabIndex        =   6
         Top             =   2160
         Width           =   795
         _ExtentX        =   1402
         _ExtentY        =   661
         _Version        =   393216
         PromptInclude   =   0   'False
         MaxLength       =   6
         PromptChar      =   "_"
      End
      Begin MSMask.MaskEdBox txtIDEN 
         Height          =   375
         Left            =   2640
         TabIndex        =   4
         Top             =   480
         Width           =   1695
         _ExtentX        =   2990
         _ExtentY        =   661
         _Version        =   393216
         PromptInclude   =   0   'False
         MaxLength       =   10
         PromptChar      =   "_"
      End
      Begin VB.Label Label1 
         Alignment       =   1  'Right Justify
         Caption         =   "C�digo:"
         Height          =   375
         Left            =   1200
         TabIndex        =   14
         Top             =   480
         Width           =   1335
      End
      Begin VB.Label Label4 
         Alignment       =   1  'Right Justify
         Caption         =   "Divisor:"
         Height          =   375
         Left            =   360
         TabIndex        =   13
         Top             =   2760
         Width           =   2175
      End
      Begin VB.Label Label2 
         Alignment       =   1  'Right Justify
         Caption         =   "Nombre:"
         Height          =   375
         Left            =   120
         TabIndex        =   3
         Top             =   1080
         Width           =   1215
      End
      Begin VB.Label Label3 
         Alignment       =   1  'Right Justify
         Caption         =   "Multiplicador:"
         Height          =   375
         Left            =   360
         TabIndex        =   2
         Top             =   2160
         Width           =   2175
      End
   End
   Begin MSComctlLib.Toolbar tlbACCIONES 
      Align           =   1  'Align Top
      Height          =   660
      Left            =   0
      TabIndex        =   12
      Top             =   0
      Visible         =   0   'False
      Width           =   10725
      _ExtentX        =   18918
      _ExtentY        =   1164
      ButtonWidth     =   1799
      ButtonHeight    =   1005
      AllowCustomize  =   0   'False
      Appearance      =   1
      _Version        =   393216
      BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
         NumButtons      =   4
         BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Enabled         =   0   'False
            Caption         =   "&Adicionar"
            Key             =   "ADD"
            Object.Tag             =   "Adicionar"
         EndProperty
         BeginProperty Button2 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Enabled         =   0   'False
            Caption         =   "&Borrar"
            Key             =   "DEL"
            Object.Tag             =   "Borrar"
         EndProperty
         BeginProperty Button3 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Enabled         =   0   'False
            Caption         =   "&Modificar"
            Key             =   "CHA"
            Object.Tag             =   "Modificar"
         EndProperty
         BeginProperty Button4 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Enabled         =   0   'False
            Caption         =   "&Cancelar"
            Key             =   "CAN"
            Object.Tag             =   "Cancelar"
         EndProperty
      EndProperty
      BorderStyle     =   1
   End
   Begin Threed.SSCommand btnCan 
      Height          =   735
      Left            =   9240
      TabIndex        =   11
      Top             =   4920
      Width           =   855
      _Version        =   65536
      _ExtentX        =   1508
      _ExtentY        =   1296
      _StockProps     =   78
      Caption         =   "&CANCELAR"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   6
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Enabled         =   0   'False
      Picture         =   "frmPrmUNIDADES.frx":058A
   End
   Begin Threed.SSCommand btnAdd 
      Height          =   735
      Left            =   5640
      TabIndex        =   8
      Top             =   4920
      Width           =   855
      _Version        =   65536
      _ExtentX        =   1508
      _ExtentY        =   1296
      _StockProps     =   78
      Caption         =   "&ADICIONAR"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   6
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Enabled         =   0   'False
      Picture         =   "frmPrmUNIDADES.frx":0C54
   End
   Begin Threed.SSCommand btnDel 
      Height          =   735
      Left            =   6840
      TabIndex        =   9
      Top             =   4920
      Width           =   855
      _Version        =   65536
      _ExtentX        =   1508
      _ExtentY        =   1296
      _StockProps     =   78
      Caption         =   "&BORRAR"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   6
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Enabled         =   0   'False
      Picture         =   "frmPrmUNIDADES.frx":18A6
   End
   Begin Threed.SSCommand btnCha 
      Height          =   735
      Left            =   8040
      TabIndex        =   10
      Top             =   4920
      Width           =   855
      _Version        =   65536
      _ExtentX        =   1508
      _ExtentY        =   1296
      _StockProps     =   78
      Caption         =   "&MODIFICAR"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   6
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Enabled         =   0   'False
      Picture         =   "frmPrmUNIDADES.frx":1F70
   End
End
Attribute VB_Name = "frmPrmUNIDADES"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Dim ArrBDG() As Variant
Dim ClldeUnidades As New Collection
Dim UnaUnidad As LaUnidad
Public OpcCod        As String   'Opci�n de seguridad
'NU_AUTO_UNVE, TX_CODI_UNVE, TX_NOMB_UNVE, NU_MULT_UNVE, NU_DIVI_UNVE

'DEPURACION DE CODIGO
'Private Sub arbBodegas_Collapse(ByVal Punto As MSComctlLib.Node)
'    If Punto.Key = "UNDS" Then
'        Me.txtIDEN.Text = String(Me.txtIDEN.MaxLength, " ")
'        Me.txtNOMB.Text = String(Me.txtNOMB.MaxLength, " ")
'        Me.txtMULT.Text = Left(CStr(1) & String(Me.txtMULT.MaxLength, " "), Me.txtMULT.MaxLength)
'        Me.txtDIVI.Text = Left(CStr(1) & String(Me.txtDIVI.MaxLength, " "), Me.txtDIVI.MaxLength)
'        Me.tlbACCIONES.Buttons("DEL").Enabled = False
'        Me.tlbACCIONES.Buttons("CHA").Enabled = False
'        Me.tlbACCIONES.Buttons("CAN").Enabled = False
'        Me.btnDel.Enabled = False
'        Me.btnCha.Enabled = False
'        Me.btnCan.Enabled = False
'        Me.txtIDEN.Enabled = False
'        Me.txtNOMB.Enabled = False
'        Me.txtMULT.Enabled = False
'        Me.txtDIVI.Enabled = False
'    End If
'End Sub

Private Sub arbUnidades_NodeClick(ByVal Punto As MSComctlLib.Node)
'    Dim NuMr As Integer, unNodo As Node
    Dim unNodo As Node      'DEPURACION DE CODIGO
    For Each unNodo In Me.arbUnidades.Nodes
        If unNodo.Bold Then unNodo.Bold = False
    Next
    Me.arbUnidades.SelectedItem.Bold = True
    Me.tlbACCIONES.Buttons("DEL").Enabled = False
    Me.tlbACCIONES.Buttons("CHA").Enabled = False
    Me.tlbACCIONES.Buttons("CAN").Enabled = False
    Me.btnDel.Enabled = False
    Me.btnCha.Enabled = False
    Me.btnCan.Enabled = False
    If Punto.Key = "UNDS" Then
'        Me.txtIDEN.Text = String(Me.txtIDEN.MaxLength, " ")
'        Me.txtNOMB.Text = String(Me.txtNOMB.MaxLength, " ")
'        Me.txtMULT.Text = Left(CStr(1) & String(Me.txtMULT.MaxLength, " "), Me.txtMULT.MaxLength)
'        Me.txtDIVI.Text = Left(CStr(1) & String(Me.txtDIVI.MaxLength, " "), Me.txtDIVI.MaxLength)
        Me.txtIDEN.Text = ""
        Me.txtNOMB.Text = ""
        Me.txtMULT.Text = "1"
        Me.txtDIVI.Text = "1"
        Me.tlbACCIONES.Buttons("DEL").Enabled = False
        Me.tlbACCIONES.Buttons("CHA").Enabled = False
        Me.tlbACCIONES.Buttons("CAN").Enabled = False
        Me.btnDel.Enabled = False
        Me.btnCha.Enabled = False
        Me.btnCan.Enabled = False
        Me.txtIDEN.Enabled = False
        Me.txtNOMB.Enabled = False
        Me.txtMULT.Enabled = False
        Me.txtDIVI.Enabled = False
        Exit Sub
    End If
    Me.tlbACCIONES.Buttons("DEL").Enabled = True
    Me.tlbACCIONES.Buttons("CHA").Enabled = True
    Me.btnDel.Enabled = True
    Me.btnCha.Enabled = True
    Me.txtIDEN.Enabled = False
    Me.txtNOMB.Enabled = False
    Me.txtMULT.Enabled = False
    Me.txtDIVI.Enabled = False
    Me.txtIDEN.Enabled = False
    Set UnaUnidad = ClldeUnidades.Item(Punto.Key)
    
'    Me.txtIDEN.Text = Left(Trim(UnaUnidad.Codigo) & String(Me.txtIDEN.MaxLength, " "), Me.txtIDEN.MaxLength)
'    Me.txtNOMB.Text = Left(Trim(UnaUnidad.Nombre) & String(Me.txtNOMB.MaxLength, " "), Me.txtNOMB.MaxLength)
'    Me.txtMULT.Text = Left(CStr(UnaUnidad.Multiplica) & String(Me.txtMULT.MaxLength, " "), Me.txtMULT.MaxLength)
'    Me.txtDIVI.Text = Left(CStr(UnaUnidad.Divide) & String(Me.txtDIVI.MaxLength, " "), Me.txtDIVI.MaxLength)
    Me.txtIDEN.Text = Trim(UnaUnidad.Codigo)
    Me.txtNOMB.Text = Trim(UnaUnidad.Nombre)
    Me.txtMULT.Text = CStr(UnaUnidad.Multiplica)
    Me.txtDIVI.Text = CStr(UnaUnidad.Divide)
End Sub

Private Sub btnAdd_Click()
    Dim UnaUnidad As New LaUnidad, Aviso As String, vclave As String
'    Dim ArrCSC() As Variant    'DEPURACION DE CODIGO
    Me.arbUnidades.Enabled = False
    If Me.tlbACCIONES.Buttons("ADD").Caption = "&Adicionar" Then
        Me.tlbACCIONES.Buttons("DEL").Enabled = False
        Me.tlbACCIONES.Buttons("CHA").Enabled = False
        Me.btnDel.Enabled = False
        Me.btnCha.Enabled = False
'        Me.txtIDEN.Text = String(Me.txtIDEN.MaxLength, " ")
'        Me.txtNOMB.Text = String(Me.txtNOMB.MaxLength, " ")
'        Me.txtMULT.Text = Left(CStr(1) & String(Me.txtMULT.MaxLength, " "), Me.txtMULT.MaxLength)
'        Me.txtDIVI.Text = Left(CStr(1) & String(Me.txtDIVI.MaxLength, " "), Me.txtDIVI.MaxLength)
        Me.txtIDEN.Text = ""
        Me.txtNOMB.Text = ""
        Me.txtMULT.Text = 1
        Me.txtDIVI.Text = 1
        Me.txtIDEN.Enabled = True
        Me.txtNOMB.Enabled = True
        Me.txtMULT.Enabled = True
        Me.txtDIVI.Enabled = True
        Me.tlbACCIONES.Buttons("ADD").Caption = "&Guardar"
        Me.btnAdd.Caption = "&Guardar"
        Me.tlbACCIONES.Buttons("CAN").Enabled = True
        Me.btnCan.Enabled = True
        Me.txtIDEN.SetFocus
    Else
        Me.txtIDEN.Enabled = False
        Me.txtNOMB.Enabled = False
        Me.txtMULT.Enabled = False
        Me.txtDIVI.Enabled = False
        Me.tlbACCIONES.Buttons("ADD").Caption = "&Adicionar"
        Me.btnAdd.Caption = "&Adicionar"
        Me.tlbACCIONES.Buttons("DEL").Enabled = True
        Me.tlbACCIONES.Buttons("CAN").Enabled = False
        Me.btnDel.Enabled = True
        Me.btnCan.Enabled = False
        Me.arbUnidades.Enabled = True
        If MalaInformacion(Aviso) Then
            Call MsgBox("Informaci�n incompleta en el(los) siguiente(s) campos:" & Aviso, vbExclamation)
        Else
            Me.arbUnidades.Enabled = True
            Set UnaUnidad = New LaUnidad
            UnaUnidad.AutoNumero = 0
            UnaUnidad.Codigo = Me.txtIDEN.Text
            UnaUnidad.Nombre = Me.txtNOMB.Text
            UnaUnidad.Multiplica = CInt(Me.txtMULT.Text)
            UnaUnidad.Divide = CInt(Me.txtDIVI.Text)
'            If Not Me.arbUnidades.SelectedItem.Key = "UNDS" Then UnaBodega.Padre = cint(Right(Me.arbBodegas.SelectedItem.Key, Len(Me.arbBodegas.SelectedItem.Key) - 1))
'NU_AUTO_UNVE, TX_CODI_UNVE, TX_NOMB_UNVE, NU_MULT_UNVE, NU_DIVI_UNVE
            Valores = "TX_CODI_UNVE='" & UnaUnidad.Codigo & _
                "', TX_NOMB_UNVE='" & UnaUnidad.Nombre & _
                "', NU_MULT_UNVE=" & UnaUnidad.Multiplica & _
                ", NU_DIVI_UNVE=" & UnaUnidad.Divide
            Result = DoInsertSQL("IN_UNDVENTA", Valores)
            If Result = FAIL Then GoTo FALLO
            Dim ArrTemp() As Variant
            Campos = "NU_AUTO_UNVE, TX_CODI_UNVE, TX_NOMB_UNVE, NU_MULT_UNVE, NU_DIVI_UNVE"
            Desde = "IN_UNDVENTA"
            Condi = "TX_CODI_UNVE='" & UnaUnidad.Codigo & "'"
            ReDim ArrTemp(4)
            Result = LoadData(Desde, Campos, Condi, ArrTemp())
            If Result = FAIL Then GoTo FALLO
            If Not Encontro Then GoTo NOENC
            UnaUnidad.AutoNumero = ArrTemp(0)
            vclave = "U" & UnaUnidad.AutoNumero
            Me.arbUnidades.Nodes.Add "UNDS", tvwChild, vclave, UnaUnidad.Nombre
            Me.arbUnidades.Nodes.Item(vclave).Tag = Trim(UnaUnidad.Codigo)
            ClldeUnidades.Add UnaUnidad, vclave
            Me.arbUnidades.Nodes.Item(vclave).Selected = True
            Me.txtIDEN.Text = Left(Trim(UnaUnidad.Codigo) & String(Me.txtIDEN.MaxLength, " "), Me.txtIDEN.MaxLength)
            Me.txtNOMB.Text = Left(Trim(UnaUnidad.Nombre) & String(Me.txtNOMB.MaxLength, " "), Me.txtNOMB.MaxLength)
            Me.txtMULT.Text = Left(CStr(UnaUnidad.Multiplica) & String(Me.txtMULT.MaxLength, " "), Me.txtMULT.MaxLength)
            Me.txtDIVI.Text = Left(Trim(UnaUnidad.Divide) & String(Me.txtDIVI.MaxLength, " "), Me.txtDIVI.MaxLength)
            Set Me.arbUnidades.SelectedItem = Me.arbUnidades.Nodes(vclave)
            Set UnaUnidad = Nothing
            Me.arbUnidades.SelectedItem.Bold = True
        End If
    End If
NOENC:
    Exit Sub
FALLO:
End Sub

Private Sub btnCha_Click()
'    Dim UnaUnidad As New LaUnidad, Aviso As String, vclave As String
    Dim UnaUnidad As New LaUnidad, Aviso As String      'DEPURACION DE CODIGO
'    Dim UnidadTem As New LaUnidad
    Result = SUCCEED
    Me.arbUnidades.Enabled = False
    If Me.tlbACCIONES.Buttons("CHA").Caption = "&Modificar" Then
        Me.tlbACCIONES.Buttons("DEL").Enabled = False
        Me.btnDel.Enabled = False
        Me.txtIDEN.Enabled = True
        Me.txtNOMB.Enabled = True
        Me.txtMULT.Enabled = True
        Me.txtDIVI.Enabled = True
        Me.tlbACCIONES.Buttons("CHA").Caption = "&Guardar"
        Me.btnCha.Caption = "&Guardar"
        Me.tlbACCIONES.Buttons("CAN").Enabled = True
        Me.btnCan.Enabled = True
        Me.txtIDEN.SetFocus
    Else
        Me.tlbACCIONES.Buttons("DEL").Enabled = True
        Me.btnDel.Enabled = True
        Me.txtIDEN.Enabled = False
        Me.txtNOMB.Enabled = False
        Me.txtMULT.Enabled = False
        Me.txtDIVI.Enabled = False
        Me.tlbACCIONES.Buttons("CHA").Caption = "&Modificar"
        Me.btnCha.Caption = "&Modificar"
        Me.tlbACCIONES.Buttons("CAN").Enabled = False
        Me.btnCan.Enabled = False
        Me.arbUnidades.Enabled = True
        If MalaInformacion(Aviso) Then
            Call MsgBox("Informaci�n incompleta en el(los) siguiente(s) campos:" & Aviso, vbExclamation)
        Else
            Set UnaUnidad = ClldeUnidades.Item(Me.arbUnidades.SelectedItem.Key)
            If UnaUnidad.Multiplica <> CInt(Me.txtMULT.Text) Or UnaUnidad.Divide <> CInt(Me.txtDIVI.Text) Then
               ReDim Arr(0)
               Result = LoadData("IN_DETALLE", "COUNT(*)", "NU_AUTO_UNVE_DETA=" & UnaUnidad.AutoNumero, Arr())
               If Result <> FAIL And Encontro And Val(Arr(0)) > 0 Then
                  Call Mensaje1("Esta Unidad ya ha sido utilizada y no se pueden cambiar sus par�metros ( Multiplicador / Divisor ).", 3)
                  Me.txtMULT.Text = UnaUnidad.Multiplica
                  Me.txtDIVI.Text = UnaUnidad.Divide
                  Exit Sub
               End If
            End If
            UnaUnidad.Codigo = Me.txtIDEN.Text
            UnaUnidad.Nombre = Me.txtNOMB.Text
            UnaUnidad.Multiplica = CInt(Me.txtMULT.Text)
            UnaUnidad.Divide = CInt(Me.txtDIVI.Text)
            Me.arbUnidades.SelectedItem.Text = UnaUnidad.Nombre
            Valores = "TX_CODI_UNVE='" & UnaUnidad.Codigo & _
                "', TX_NOMB_UNVE='" & UnaUnidad.Nombre & _
                "', NU_MULT_UNVE=" & UnaUnidad.Multiplica & _
                ", NU_DIVI_UNVE=" & UnaUnidad.Divide
            Condi = "NU_AUTO_UNVE=" & UnaUnidad.AutoNumero
            If Result <> FAIL Then Result = DoUpdate("IN_UNDVENTA", Valores, Condi)
            If Result = FAIL Then GoTo FALLO
            Me.arbUnidades.SelectedItem.Tag = Trim(UnaUnidad.Codigo)
        End If
    End If
FALLO:
End Sub

Private Sub btnCan_Click()
    If Me.tlbACCIONES.Buttons("ADD").Caption = "&Guardar" Then Me.tlbACCIONES.Buttons("ADD").Caption = "&Adicionar"
    If Me.btnAdd.Caption = "&Guardar" Then Me.btnAdd.Caption = "&Adicionar"
    If Me.tlbACCIONES.Buttons("CHA").Caption = "&Guardar" Then Me.tlbACCIONES.Buttons("CHA").Caption = "&Modificar"
    If Me.btnCha.Caption = "&Guardar" Then Me.btnCha.Caption = "&Modificar"
    If Not Me.tlbACCIONES.Buttons("DEL").Enabled Then Me.tlbACCIONES.Buttons("DEL").Enabled = True
    If Not Me.btnDel.Enabled Then Me.btnDel.Enabled = True
    Me.tlbACCIONES.Buttons("CHA").Enabled = False
    Me.btnCha.Enabled = False
    Call arbUnidades_NodeClick(Me.arbUnidades.SelectedItem)
    Me.arbUnidades.Enabled = True
End Sub

Private Sub Form_Load()
'    Dim CnTdr As Integer, Punto As MSComctlLib.Node, vclave As String
    Dim CnTdr As Integer, vclave As String      'DEPURACION DE CODIGO
'    Call main
   Call CenterForm(MDI_Inventarios, Me)
    Me.txtIDEN.Mask = ">" & String(Me.txtIDEN.MaxLength, "a")
    Me.txtNOMB.Mask = ">" & String(Me.txtNOMB.MaxLength, "a")
    Me.txtMULT.Mask = String(Me.txtMULT.MaxLength, "9")
    Me.txtDIVI.Mask = String(Me.txtDIVI.MaxLength, "9")
    
    Me.txtIDEN.Text = String(Me.txtIDEN.MaxLength, " ")
    Me.txtNOMB.Text = String(Me.txtNOMB.MaxLength, " ")
    Me.txtMULT.Text = Left(CStr(1) & String(Me.txtMULT.MaxLength, " "), Me.txtMULT.MaxLength)
    Me.txtDIVI.Text = Left(CStr(1) & String(Me.txtDIVI.MaxLength, " "), Me.txtDIVI.MaxLength)
    
    Me.txtIDEN.Enabled = False
    Me.txtNOMB.Enabled = False
    Me.txtMULT.Enabled = False
    Me.txtDIVI.Enabled = False
'NU_AUTO_UNVE, TX_CODI_UNVE, TX_NOMB_UNVE, NU_MULT_UNVE, NU_DIVI_UNVE
    Campos = "NU_AUTO_UNVE, TX_CODI_UNVE, TX_NOMB_UNVE, NU_MULT_UNVE, NU_DIVI_UNVE"
    Desde = "IN_UNDVENTA"
    Condi = ""
    ReDim ArrBDG(4, 0)
    Result = LoadMulData(Desde, Campos, Condi, ArrBDG())
    If Result = FAIL Then GoTo FALLO
    Me.tlbACCIONES.Buttons("ADD").Enabled = True
    Me.btnAdd.Enabled = True
    
    'PedroJ Def 1702
        Call BorrarColeccion(ClldeUnidades)
        Me.arbUnidades.Nodes.Clear
        Me.arbUnidades.Nodes.Add , , "UNDS", "UNIDADES DEFINIDAS"
        Me.arbUnidades.Nodes.Item("UNDS").Tag = "[[]]"
    'PedroJ
    
    If Encontro Then
'        Call BorrarColeccion(ClldeUnidades)
'        Me.arbUnidades.Nodes.Clear
'        Me.arbUnidades.Nodes.Add , , "UNDS", "UNIDADES DEFINIDAS"
'        Me.arbUnidades.Nodes.Item("UNDS").Tag = "[[]]"
        For CnTdr = 0 To UBound(ArrBDG, 2)
            Dim UnaUnidad As New LaUnidad
            UnaUnidad.AutoNumero = ArrBDG(0, CnTdr)
            UnaUnidad.Codigo = ArrBDG(1, CnTdr)
            UnaUnidad.Nombre = ArrBDG(2, CnTdr)
            UnaUnidad.Multiplica = ArrBDG(3, CnTdr)
            UnaUnidad.Divide = ArrBDG(4, CnTdr)
            vclave = "U" & UnaUnidad.AutoNumero
            ClldeUnidades.Add UnaUnidad, vclave
            Me.arbUnidades.Nodes.Add "UNDS", tvwChild, vclave, Trim(UnaUnidad.Nombre)
            Me.arbUnidades.Nodes.Item(vclave).Tag = Trim(UnaUnidad.Codigo)
            Set UnaUnidad = Nothing
        Next
    End If
FALLO:
End Sub

Private Sub tlbACCIONES_ButtonClick(ByVal Boton As MSComctlLib.Button)
    Select Case Boton.Key
    Case Is = "CHA"
        Call btnCha_Click
    Case Is = "ADD"
        Call btnAdd_Click
    Case Is = "DEL"
        Call btnDel_Click
    Case Is = "CAN"
        Call btnCan_Click
    End Select
End Sub

'Private Sub Form_Unload(Cancel As Integer)
'    BD(BDCur).Close
'End Sub

'Private Sub txtIDEN_KeyPress(Tecla As Integer)
'    Call txtNOMB_KeyPress(Tecla)
'End Sub
'
'Private Sub txtNOMB_KeyPress(Tecla As Integer)
'    Dim Cara As String * 1
'    Cara = Chr(Tecla)
'    If Cara = vbBack Then Exit Sub
'    If Asc(Cara) > 96 And Asc(Tecla) < 113 Then Tecla = Asc(UCase(Chr(Tecla)))
'    If InStr(" 01234567890ABCDEFGHIJKLMNOPQRSTUVWXYZ", Chr(Tecla)) = 0 Then Tecla = 0
'End Sub

Private Function MalaInformacion(ByRef Cual As String) As Boolean
    Cual = ""
    MalaInformacion = False
    If Len(Trim(Me.txtNOMB)) = 0 Then Cual = Cual & IIf(Len(Cual) = 0, "", ", ") & "Nombre"
    If Len(Trim(Me.txtIDEN)) = 0 Then Cual = Cual & IIf(Len(Cual) = 0, "", ", ") & "C�digo"
    If IsNumeric(Me.txtMULT.Text) Then
        If CInt(Me.txtMULT.Text) < 1 Then Cual = Cual & IIf(Len(Cual) = 0, "", ", ") & "Multiplica"
    Else
        Cual = Cual & IIf(Len(Cual) = 0, "", ", ") & "Multiplica"
    End If
    If IsNumeric(Me.txtDIVI.Text) Then
        If CInt(Me.txtDIVI.Text) < 1 Then Cual = Cual & IIf(Len(Cual) = 0, "", ", ") & "Divide"
    Else
        Cual = Cual & IIf(Len(Cual) = 0, "", ", ") & "Divide"
    End If
    If Len(Cual) > 0 Then MalaInformacion = True
End Function

Private Sub btnDel_Click()
    If MsgBox("Desea borrar la Unidad '" & Trim(Me.arbUnidades.SelectedItem.Text) & "'", vbYesNo) = vbNo Then GoTo DIJONO
    Dim Punto As MSComctlLib.Node, Clave As String, UnaUnidad As LaUnidad
    Clave = Me.arbUnidades.SelectedItem.Key
    Set Punto = Me.arbUnidades.SelectedItem.Parent
    Set UnaUnidad = ClldeUnidades.Item(Clave)
    Condi = "NU_AUTO_UNVE=" & UnaUnidad.AutoNumero
    Result = DoDelete("IN_UNDVENTA", Condi)
    If Result = FAIL Then GoTo FALLO
    Me.arbUnidades.Nodes.Remove Clave
    ClldeUnidades.Remove Clave
    Call arbUnidades_NodeClick(Punto)
    Exit Sub
FALLO:
DIJONO:
End Sub

Private Sub Form_KeyPress(KeyAscii As Integer)
    Call Cambiar_Enter(KeyAscii)
End Sub

Private Sub txtIDEN_LostFocus()
    Dim unNodo As MSComctlLib.Node
    For Each unNodo In Me.arbUnidades.Nodes
        If Me.tlbACCIONES.Buttons.Item("ADD").Caption = "&Guardar" Then
            If Trim(Me.txtIDEN.Text) = unNodo.Tag Then
                Call MsgBox("El C�digo digitado ya existe, debe cambiarlo", vbInformation)
                Me.txtIDEN.SetFocus
            End If
        ElseIf Me.tlbACCIONES.Buttons.Item("CHA").Caption = "&Guardar" Then
            If Not Me.arbUnidades.SelectedItem.Key = unNodo.Key Then
                If Trim(Me.txtIDEN.Text) = unNodo.Tag Then
                    Call MsgBox("El C�digo digitado ya existe, debe cambiarlo", vbInformation)
                    Me.txtIDEN.SetFocus
                End If
            End If
        End If
    Next
End Sub

Private Sub txtNOMB_KeyPress(KeyAscii As Integer)
    Call Cambiar_Enter(KeyAscii)
End Sub

Private Sub txtDIVI_KeyPress(KeyAscii As Integer)
    Call Cambiar_Enter(KeyAscii)
End Sub

Private Sub txtIDEN_KeyPress(KeyAscii As Integer)
    Call Cambiar_Enter(KeyAscii)
End Sub

Private Sub txtMULT_KeyPress(KeyAscii As Integer)
    Call Cambiar_Enter(KeyAscii)
End Sub

