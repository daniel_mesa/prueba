VERSION 5.00
Object = "{0BA686C6-F7D3-101A-993E-0000C0EF6F5E}#1.0#0"; "THREED32.OCX"
Begin VB.Form FrmGrupo 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Grupos de Art�culos"
   ClientHeight    =   7650
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   7200
   Icon            =   "FrmGrupo.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   ScaleHeight     =   7650
   ScaleWidth      =   7200
   Begin VB.ComboBox cboBodega 
      BackColor       =   &H00FFFFFF&
      ForeColor       =   &H00000000&
      Height          =   315
      Left            =   1320
      TabIndex        =   1
      Top             =   120
      Width           =   3255
   End
   Begin VB.Frame FrmPresu 
      Caption         =   "Cuentas Presupuestales"
      Height          =   1095
      Left            =   120
      TabIndex        =   45
      Top             =   5400
      Width           =   6975
      Begin VB.TextBox Txt_Articulo 
         Height          =   285
         Index           =   0
         Left            =   1680
         MaxLength       =   20
         TabIndex        =   18
         Top             =   240
         Width           =   1455
      End
      Begin VB.TextBox Txt_Articulo 
         Height          =   285
         Index           =   1
         Left            =   1680
         MaxLength       =   20
         TabIndex        =   20
         Top             =   600
         Width           =   1455
      End
      Begin Threed.SSCommand CmdPpto 
         Height          =   375
         Index           =   0
         Left            =   3120
         TabIndex        =   19
         ToolTipText     =   "Selecci�n de Cuenta"
         Top             =   240
         Width           =   615
         _Version        =   65536
         _ExtentX        =   1085
         _ExtentY        =   661
         _StockProps     =   78
         BevelWidth      =   0
         RoundedCorners  =   0   'False
         Outline         =   0   'False
         Picture         =   "FrmGrupo.frx":058A
      End
      Begin Threed.SSCommand CmdPpto 
         Height          =   375
         Index           =   1
         Left            =   3120
         TabIndex        =   21
         ToolTipText     =   "Selecci�n de Cuenta"
         Top             =   600
         Width           =   615
         _Version        =   65536
         _ExtentX        =   1085
         _ExtentY        =   661
         _StockProps     =   78
         BevelWidth      =   0
         RoundedCorners  =   0   'False
         Outline         =   0   'False
         Picture         =   "FrmGrupo.frx":0F3C
      End
      Begin VB.Label Label1 
         Caption         =   "Art�culo de Gastos"
         Height          =   255
         Index           =   2
         Left            =   120
         TabIndex        =   49
         Top             =   240
         Width           =   1455
      End
      Begin VB.Label lbl_articulo 
         BackColor       =   &H8000000E&
         BorderStyle     =   1  'Fixed Single
         Height          =   285
         Index           =   0
         Left            =   3720
         TabIndex        =   48
         Top             =   240
         Width           =   3135
      End
      Begin VB.Label Label1 
         Caption         =   "Art�culo de Ingresos"
         Height          =   255
         Index           =   3
         Left            =   120
         TabIndex        =   47
         Top             =   600
         Width           =   1455
      End
      Begin VB.Label lbl_articulo 
         BackColor       =   &H8000000E&
         BorderStyle     =   1  'Fixed Single
         Height          =   285
         Index           =   1
         Left            =   3720
         TabIndex        =   46
         Top             =   600
         Width           =   3135
      End
   End
   Begin VB.Frame FrmConta 
      Caption         =   "Cuentas Contables"
      Height          =   4095
      Left            =   120
      TabIndex        =   34
      Top             =   1200
      Width           =   6975
      Begin VB.TextBox Txt_Cuenta 
         Height          =   285
         Index           =   7
         Left            =   1680
         MaxLength       =   15
         TabIndex        =   55
         Top             =   3600
         Width           =   1455
      End
      Begin VB.TextBox Txt_Cuenta 
         Height          =   285
         Index           =   6
         Left            =   1680
         MaxLength       =   15
         TabIndex        =   16
         Top             =   3120
         Width           =   1455
      End
      Begin VB.TextBox Txt_Cuenta 
         Height          =   285
         Index           =   5
         Left            =   1680
         MaxLength       =   15
         TabIndex        =   14
         Top             =   2640
         Width           =   1455
      End
      Begin VB.TextBox Txt_Cuenta 
         Height          =   285
         Index           =   0
         Left            =   1680
         MaxLength       =   15
         TabIndex        =   4
         Top             =   240
         Width           =   1455
      End
      Begin VB.TextBox Txt_Cuenta 
         Height          =   285
         Index           =   1
         Left            =   1680
         MaxLength       =   15
         TabIndex        =   6
         Top             =   720
         Width           =   1455
      End
      Begin VB.TextBox Txt_Cuenta 
         Height          =   285
         Index           =   2
         Left            =   1680
         MaxLength       =   15
         TabIndex        =   8
         Top             =   1200
         Width           =   1455
      End
      Begin VB.TextBox Txt_Cuenta 
         Height          =   285
         Index           =   3
         Left            =   1680
         MaxLength       =   15
         TabIndex        =   10
         Top             =   1680
         Width           =   1455
      End
      Begin VB.TextBox Txt_Cuenta 
         Height          =   285
         Index           =   4
         Left            =   1680
         MaxLength       =   15
         TabIndex        =   12
         Top             =   2160
         Width           =   1455
      End
      Begin Threed.SSCommand CmdSelec 
         Height          =   375
         Index           =   0
         Left            =   3120
         TabIndex        =   5
         ToolTipText     =   "Selecci�n de Cuenta"
         Top             =   240
         Width           =   615
         _Version        =   65536
         _ExtentX        =   1085
         _ExtentY        =   661
         _StockProps     =   78
         BevelWidth      =   0
         RoundedCorners  =   0   'False
         Outline         =   0   'False
         Picture         =   "FrmGrupo.frx":18EE
      End
      Begin Threed.SSCommand CmdSelec 
         Height          =   375
         Index           =   1
         Left            =   3120
         TabIndex        =   7
         ToolTipText     =   "Selecci�n de Cuenta"
         Top             =   720
         Width           =   615
         _Version        =   65536
         _ExtentX        =   1085
         _ExtentY        =   661
         _StockProps     =   78
         BevelWidth      =   0
         RoundedCorners  =   0   'False
         Outline         =   0   'False
         Picture         =   "FrmGrupo.frx":22A0
      End
      Begin Threed.SSCommand CmdSelec 
         Height          =   375
         Index           =   2
         Left            =   3120
         TabIndex        =   9
         ToolTipText     =   "Selecci�n de Cuenta"
         Top             =   1200
         Width           =   615
         _Version        =   65536
         _ExtentX        =   1085
         _ExtentY        =   661
         _StockProps     =   78
         BevelWidth      =   0
         RoundedCorners  =   0   'False
         Outline         =   0   'False
         Picture         =   "FrmGrupo.frx":2C52
      End
      Begin Threed.SSCommand CmdSelec 
         Height          =   375
         Index           =   3
         Left            =   3120
         TabIndex        =   11
         ToolTipText     =   "Selecci�n de Cuenta"
         Top             =   1680
         Width           =   615
         _Version        =   65536
         _ExtentX        =   1085
         _ExtentY        =   661
         _StockProps     =   78
         BevelWidth      =   0
         RoundedCorners  =   0   'False
         Outline         =   0   'False
         Picture         =   "FrmGrupo.frx":3604
      End
      Begin Threed.SSCommand CmdSelec 
         Height          =   375
         Index           =   4
         Left            =   3120
         TabIndex        =   13
         ToolTipText     =   "Selecci�n de Cuenta"
         Top             =   2160
         Width           =   615
         _Version        =   65536
         _ExtentX        =   1085
         _ExtentY        =   661
         _StockProps     =   78
         BevelWidth      =   0
         RoundedCorners  =   0   'False
         Outline         =   0   'False
         Picture         =   "FrmGrupo.frx":3FB6
      End
      Begin Threed.SSCommand CmdSelec 
         Height          =   375
         Index           =   5
         Left            =   3120
         TabIndex        =   15
         ToolTipText     =   "Selecci�n de Cuenta"
         Top             =   2640
         Width           =   615
         _Version        =   65536
         _ExtentX        =   1085
         _ExtentY        =   661
         _StockProps     =   78
         BevelWidth      =   0
         RoundedCorners  =   0   'False
         Outline         =   0   'False
         Picture         =   "FrmGrupo.frx":4968
      End
      Begin Threed.SSCommand CmdSelec 
         Height          =   375
         Index           =   6
         Left            =   3120
         TabIndex        =   17
         ToolTipText     =   "Selecci�n de Cuenta"
         Top             =   3120
         Width           =   615
         _Version        =   65536
         _ExtentX        =   1085
         _ExtentY        =   661
         _StockProps     =   78
         BevelWidth      =   0
         RoundedCorners  =   0   'False
         Outline         =   0   'False
         Picture         =   "FrmGrupo.frx":531A
      End
      Begin Threed.SSCommand CmdSelec 
         Height          =   375
         Index           =   7
         Left            =   3120
         TabIndex        =   56
         ToolTipText     =   "Selecci�n de Cuenta"
         Top             =   3600
         Width           =   615
         _Version        =   65536
         _ExtentX        =   1085
         _ExtentY        =   661
         _StockProps     =   78
         BevelWidth      =   0
         RoundedCorners  =   0   'False
         Outline         =   0   'False
         Picture         =   "FrmGrupo.frx":5CCC
      End
      Begin VB.Label Label1 
         Alignment       =   1  'Right Justify
         Caption         =   "Cuenta Transitoria"
         Height          =   255
         Index           =   9
         Left            =   120
         TabIndex        =   58
         Top             =   3600
         Width           =   1455
      End
      Begin VB.Label lbl_cuenta 
         BackColor       =   &H8000000E&
         BorderStyle     =   1  'Fixed Single
         Height          =   285
         Index           =   7
         Left            =   3720
         TabIndex        =   57
         Top             =   3600
         Width           =   3135
      End
      Begin VB.Label Label1 
         Alignment       =   1  'Right Justify
         Caption         =   "Cuenta Aj X Infla CR"
         Height          =   255
         Index           =   8
         Left            =   120
         TabIndex        =   53
         Top             =   3120
         Width           =   1455
      End
      Begin VB.Label lbl_cuenta 
         BackColor       =   &H8000000E&
         BorderStyle     =   1  'Fixed Single
         Height          =   285
         Index           =   6
         Left            =   3720
         TabIndex        =   52
         Top             =   3120
         Width           =   3135
      End
      Begin VB.Label Label1 
         Alignment       =   1  'Right Justify
         Caption         =   "Cuenta Aj X Infla DB"
         Height          =   255
         Index           =   4
         Left            =   120
         TabIndex        =   51
         Top             =   2640
         Width           =   1455
      End
      Begin VB.Label lbl_cuenta 
         BackColor       =   &H8000000E&
         BorderStyle     =   1  'Fixed Single
         Height          =   285
         Index           =   5
         Left            =   3720
         TabIndex        =   50
         Top             =   2640
         Width           =   3135
      End
      Begin VB.Label Label1 
         Alignment       =   1  'Right Justify
         Caption         =   "Cuenta de Entradas"
         Height          =   255
         Index           =   0
         Left            =   120
         TabIndex        =   44
         Top             =   240
         Width           =   1455
      End
      Begin VB.Label lbl_cuenta 
         BackColor       =   &H8000000E&
         BorderStyle     =   1  'Fixed Single
         Height          =   285
         Index           =   0
         Left            =   3720
         TabIndex        =   43
         Top             =   240
         Width           =   3135
      End
      Begin VB.Label Label1 
         Alignment       =   1  'Right Justify
         Caption         =   "Cuenta de Salidas"
         Height          =   255
         Index           =   1
         Left            =   120
         TabIndex        =   42
         Top             =   720
         Width           =   1455
      End
      Begin VB.Label lbl_cuenta 
         BackColor       =   &H8000000E&
         BorderStyle     =   1  'Fixed Single
         Height          =   285
         Index           =   1
         Left            =   3720
         TabIndex        =   41
         Top             =   720
         Width           =   3135
      End
      Begin VB.Label lbl_cuenta 
         BackColor       =   &H8000000E&
         BorderStyle     =   1  'Fixed Single
         Height          =   285
         Index           =   2
         Left            =   3720
         TabIndex        =   40
         Top             =   1200
         Width           =   3135
      End
      Begin VB.Label lbl_cuenta 
         BackColor       =   &H8000000E&
         BorderStyle     =   1  'Fixed Single
         Height          =   285
         Index           =   3
         Left            =   3720
         TabIndex        =   39
         Top             =   1680
         Width           =   3135
      End
      Begin VB.Label lbl_cuenta 
         BackColor       =   &H8000000E&
         BorderStyle     =   1  'Fixed Single
         Height          =   285
         Index           =   4
         Left            =   3720
         TabIndex        =   38
         Top             =   2160
         Width           =   3135
      End
      Begin VB.Label Label1 
         Alignment       =   1  'Right Justify
         Caption         =   "Cuenta de ingreso"
         Height          =   255
         Index           =   5
         Left            =   120
         TabIndex        =   37
         Top             =   1200
         Width           =   1455
      End
      Begin VB.Label Label1 
         Alignment       =   1  'Right Justify
         Caption         =   "Cuenta de Gasto"
         Height          =   255
         Index           =   6
         Left            =   120
         TabIndex        =   36
         Top             =   1680
         Width           =   1455
      End
      Begin VB.Label Label1 
         Alignment       =   1  'Right Justify
         Caption         =   "Cuenta de Costo"
         Height          =   255
         Index           =   7
         Left            =   120
         TabIndex        =   35
         Top             =   2160
         Width           =   1455
      End
   End
   Begin VB.ComboBox Cbo_Impu 
      Height          =   315
      Left            =   1920
      Style           =   2  'Dropdown List
      TabIndex        =   32
      Top             =   7680
      Width           =   4455
   End
   Begin VB.ComboBox Cbo_Desc 
      Height          =   315
      Left            =   1920
      Style           =   2  'Dropdown List
      TabIndex        =   30
      Top             =   7680
      Visible         =   0   'False
      Width           =   4455
   End
   Begin VB.TextBox TxtGrupo 
      Height          =   285
      Index           =   1
      Left            =   1320
      MaxLength       =   40
      TabIndex        =   3
      Top             =   840
      Width           =   5175
   End
   Begin VB.TextBox TxtGrupo 
      Height          =   285
      Index           =   0
      Left            =   1320
      MaxLength       =   9
      TabIndex        =   2
      Top             =   480
      Width           =   1095
   End
   Begin VB.Frame Frame2 
      Height          =   1095
      Left            =   960
      TabIndex        =   33
      Top             =   6480
      Width           =   4935
      Begin Threed.SSCommand SCmd_Options 
         Height          =   735
         Index           =   0
         Left            =   120
         TabIndex        =   22
         Top             =   240
         Width           =   855
         _Version        =   65536
         _ExtentX        =   1508
         _ExtentY        =   1296
         _StockProps     =   78
         Caption         =   "&GUARDAR"
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   6
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Picture         =   "FrmGrupo.frx":667E
      End
      Begin Threed.SSCommand SCmd_Options 
         Height          =   735
         Index           =   1
         Left            =   1080
         TabIndex        =   23
         Top             =   270
         Width           =   855
         _Version        =   65536
         _ExtentX        =   1508
         _ExtentY        =   1296
         _StockProps     =   78
         Caption         =   "&BORRAR"
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   6
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Picture         =   "FrmGrupo.frx":6D48
      End
      Begin Threed.SSCommand SCmd_Options 
         Height          =   735
         Index           =   3
         Left            =   3000
         TabIndex        =   25
         Top             =   240
         Width           =   855
         _Version        =   65536
         _ExtentX        =   1508
         _ExtentY        =   1296
         _StockProps     =   78
         Caption         =   "&LISTAR"
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   6
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Picture         =   "FrmGrupo.frx":7412
      End
      Begin Threed.SSCommand SCmd_Options 
         Height          =   735
         Index           =   4
         Left            =   3960
         TabIndex        =   26
         Top             =   240
         Width           =   855
         _Version        =   65536
         _ExtentX        =   1508
         _ExtentY        =   1296
         _StockProps     =   78
         Caption         =   "&SALIR"
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   6
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Picture         =   "FrmGrupo.frx":7ADC
      End
      Begin Threed.SSCommand SCmd_Options 
         Height          =   735
         Index           =   2
         Left            =   2040
         TabIndex        =   24
         Top             =   240
         Width           =   855
         _Version        =   65536
         _ExtentX        =   1508
         _ExtentY        =   1296
         _StockProps     =   78
         Caption         =   "S&ELECCION"
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   6
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Picture         =   "FrmGrupo.frx":81A6
      End
   End
   Begin Threed.SSCommand CmdLimpia 
      Height          =   375
      Left            =   6600
      TabIndex        =   27
      ToolTipText     =   "Limpia Pantalla"
      Top             =   0
      Width           =   375
      _Version        =   65536
      _ExtentX        =   661
      _ExtentY        =   661
      _StockProps     =   78
      Picture         =   "FrmGrupo.frx":8B58
   End
   Begin VB.Label lblBODEGA 
      Caption         =   "&Bodega"
      Height          =   255
      Left            =   240
      TabIndex        =   54
      Top             =   120
      Width           =   645
   End
   Begin VB.Label Label3 
      Caption         =   "Impuesto:"
      Height          =   255
      Left            =   360
      TabIndex        =   31
      Top             =   7680
      Width           =   1335
   End
   Begin VB.Label Label2 
      Caption         =   "Descuento"
      Height          =   255
      Left            =   360
      TabIndex        =   29
      Top             =   7680
      Visible         =   0   'False
      Width           =   1335
   End
   Begin VB.Label LblDescripcion 
      Caption         =   "&Descripci�n :"
      Height          =   255
      Left            =   240
      TabIndex        =   28
      Top             =   840
      Width           =   975
   End
   Begin VB.Label LblCodigo 
      Caption         =   "&C�digo :"
      Height          =   255
      Left            =   240
      TabIndex        =   0
      Top             =   480
      Width           =   615
   End
End
Attribute VB_Name = "FrmGrupo"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Dim Tabla As String
Dim Mensaje As String
Dim EncontroT As Boolean
Dim Cod_Impuesto(), Cod_Descuento()
Dim Cod_Bodega() As Variant
Public OpcCod        As String   'Opci�n de seguridad
Dim IngResult As Integer 'NYCM 2342

Private Sub CboDesc_KeyPress(KeyAscii As Integer)
    Call Cambiar_Enter(KeyAscii)
End Sub

Private Sub CboImpu_KeyPress(KeyAscii As Integer)
    Call Cambiar_Enter(KeyAscii)
End Sub

Private Sub Cbo_Desc_KeyPress(KeyAscii As Integer)
    Call Cambiar_Enter(KeyAscii)
End Sub

Private Sub Cbo_Impu_KeyPress(KeyAscii As Integer)
Call Cambiar_Enter(KeyAscii)
End Sub

Private Sub cboBodega_KeyPress(KeyAscii As Integer)
    Call Cambiar_Enter(KeyAscii)
End Sub

Private Sub cboBodega_LostFocus()
  If TxtGrupo(0) <> NUL$ And cboBodega.ListIndex <> -1 Then Buscar_Grupo
End Sub

Private Sub CmdLimpia_Click()
   Call Limpiar
End Sub

Private Sub CmdPpto_Click(Index As Integer)
    Codigo = NUL$
    If Index = 0 Then
       Codigo = SelNivel("GASTOS", "DE_DESC_GAST", "CD_CODI_GAST,DE_DESC_GAST", "ARTICULOS DE GASTOS", NUL$)
    Else
       Codigo = SelNivel("INGRESOS", "DE_DESC_INGR", "CD_CODI_INGR,DE_DESC_INGR", "ARTICULOS DE INGRESOS", NUL$)
    End If
    If Codigo <> NUL$ Then Txt_Articulo(Index) = Codigo
    Txt_Articulo_LostFocus (Index)
    Txt_Articulo(Index).SetFocus
End Sub

Private Sub CmdSelec_Click(Index As Integer)
   Codigo = NUL$
   'JLPB T29905/R28918 INICIO Se deja en comentario la siguiente linea
   'Codigo = SelNivel("CUENTAS", "NO_NOMB_CUEN", "CD_CODI_CUEN,NO_NOMB_CUEN", "CUENTAS CONTABLES", NUL$)
   Condicion = "TX_PUCNIIF_CUEN<>'1'"
   Codigo = SelNivel("CUENTAS", "NO_NOMB_CUEN", "CD_CODI_CUEN,NO_NOMB_CUEN", "CUENTAS CONTABLES", Condicion)
   'JLPB T29905/R28918 FIN
   If Codigo <> NUL$ Then Txt_Cuenta(Index) = Codigo
   Txt_Cuenta_LostFocus (Index)
   Txt_Cuenta(Index).SetFocus
End Sub

Private Sub Form_Load()
    Call CenterForm(MDI_Inventarios, Me)
     'Dim Arr(0) As Variant
     Dim arr(1) As Variant
     'Result = LoadData("PARAMETROS_INVE", "ID_CONT_APLI", NUL$, Arr())
     Result = LoadData("PARAMETROS_INVE", "ID_CONT_APLI,ID_PRES_APLI", NUL$, arr())
     If Result <> FAIL And Encontro Then
        FrmConta.Enabled = IIf(arr(0) = "S", True, False)
        FrmPresu.Enabled = IIf(arr(1) = "S", True, False)
     Else
        Call Mensaje1("Error leyendo interfases", 2)
     End If
        
    Tabla = "GRUP_ARTICULO"
    Mensaje = "Grupo"
    
    Result = loadctrl("DESCUENTO ORDER BY DE_NOMB_DESC", "DE_NOMB_DESC", "CD_CODI_DESC", Cbo_Desc, Cod_Descuento(), NUL$)
    If Result = FAIL Then Call Mensaje1("Error cargando tabla [DESCUENTO]", 2)
    
    Result = loadctrl("TC_IMPUESTOS ORDER BY DE_NOMB_IMPU", "DE_NOMB_IMPU", "CD_CODI_IMPU", Cbo_Impu, Cod_Impuesto(), NUL$)
    If Result = FAIL Then Call Mensaje1("Error cargando tabla [TC_IMPUESTOS]", 2)
    
    Result = loadctrl("IN_BODEGA ORDER BY TX_NOMB_BODE", "TX_NOMB_BODE", "NU_AUTO_BODE", cboBodega, Cod_Bodega(), NUL$)
    If Result = FAIL Then Call Mensaje1("Error cargando tabla [IN_BODEGA]", 2)
    
End Sub

Private Sub SCmd_Options_Click(Index As Integer)
Dim Grup As String
    Select Case Index
        Case 0: Encontro = EncontroT
                Grabar
        Case 1: Borrar
                 
        Case 2: Grup = Seleccion(Tabla, "DE_DESC_GRUP", "DISTINCT CD_CODI_GRUP,DE_DESC_GRUP", "GRUPOS DE ARTICULOS", NUL$)
                If Grup <> NUL$ Then TxtGrupo(0) = Grup
                TxtGrupo(0).SetFocus
                SCmd_Options(2).SetFocus
        Case 3: Call Listar("GRUPOS.rpt", Tabla, "CD_CODI_GRUP", "DE_DESC_GRUP", "Grupos de Art�culos", NUL$, NUL$, NUL$, NUL$, NUL$)
                Me.SetFocus
        Case 4: Unload Me
    End Select
End Sub

Private Sub SCmd_Options_GotFocus(Index As Integer)
 Select Case Index
    Case 0: Msglin "Pulse este icono para grabar"
    Case 1: Msglin "Pulse este icono para borrar"
    Case 2: Msglin "Pulse este icono para selecci�n"
    Case 3: Msglin "Pulse este icono para generar un listado"
    Case 4: Msglin "Pulse este icono para cerrar la ventana "
  End Select
End Sub

Private Sub Buscar_Grupo()
'ReDim Arr(10)
Dim I As Integer
   
'   Condicion = "CD_CODI_GRUP=" & Comi & Cambiar_Comas_Comillas(TxtGrupo(0)) & Comi
'   Result = LoadData(Tabla, Asterisco, Condicion, Arr())
'   If (Result <> False) Then
'     If Encontro Then
'        TxtGrupo(1) = Arr(1)
'        For I = 2 To 6
'            Txt_Cuenta(I - 2) = Arr(I)
'            Call Buscar_Cuenta(I - 2)
'        Next
'        For I = 9 To 10
'            Txt_Cuenta(I - 4) = Arr(I)
'            Call Buscar_Cuenta(I - 4)
'        Next
'        Txt_Articulo(0) = Arr(7)
'        Txt_Articulo(1) = Arr(8)
'        Txt_Articulo_LostFocus (0)
'        Txt_Articulo_LostFocus (1)
'        'Call Leer_Descuento
'        'Call Leer_Impuesto
'        EncontroT = True
'     Else
'        TxtGrupo(1) = NUL$
'        For I = 0 To Txt_Cuenta.Count - 1
'            Txt_Cuenta(I) = NUL$
'            lbl_cuenta(I) = NUL$
'        Next
'        For I = 0 To Txt_Articulo.Count - 1
'            Txt_Articulo(I) = NUL$
'            lbl_articulo(I) = NUL$
'        Next I
'        Cbo_Desc.ListIndex = -1
'        Cbo_Impu.ListIndex = -1
'        EncontroT = False
'     End If
'   End If
   ReDim arr(2)
   Condicion = "CD_CODI_GRUP=" & Comi & Cambiar_Comas_Comillas(TxtGrupo(0)) & Comi
   Result = LoadData(Tabla, "DE_DESC_GRUP,CD_AGAS_GRUP,CD_AING_GRUP", Condicion, arr())
   If (Result <> False) Then
     If Encontro Then
        TxtGrupo(1) = Cambiar_Comas_Comillas(CStr(arr(0)))
        Txt_Articulo(0) = arr(1)
        Txt_Articulo(1) = arr(2)

        ReDim arr(7)
        'Campos = "CD_ENTR_RGB,CD_SALI_RGB,CD_INGR_RGB,CD_GAST_RGB,CD_COST_RGB,CD_AJDB_RGB,CD_AJCR_RGB,CD_AGAS_RGB,CD_AING_RGB"
        Campos = "CD_ENTR_RGB,CD_SALI_RGB,CD_INGR_RGB,CD_GAST_RGB,CD_COST_RGB,CD_AJDB_RGB,CD_AJCR_RGB,CD_TRAN_RGB"
        Condicion = "CD_CODI_GRUP_RGB=" & Comi & Cambiar_Comas_Comillas(TxtGrupo(0)) & Comi
        Condicion = Condicion & " AND NU_AUTO_BODE_RGB=" & Cod_Bodega(cboBodega.ListIndex)
        Result = LoadData("IN_R_GRUP_BODE", Campos, Condicion, arr())
        If (Result <> False) Then
           For I = 0 To 7
               Txt_Cuenta(I) = arr(I)
               'Call Buscar_Cuenta(i) 'HRR M5462
               Call Buscar_Cuenta(I, False) 'HRR M5462
           Next
           'Txt_Articulo(0) = Arr(7)
           'Txt_Articulo(1) = Arr(8)
           Txt_Articulo_LostFocus (0)
           Txt_Articulo_LostFocus (1)
        Else
           For I = 0 To Txt_Cuenta.Count - 1
               Txt_Cuenta(I) = NUL$
               lbl_cuenta(I) = NUL$
           Next
           For I = 0 To Txt_Articulo.Count - 1
               Txt_Articulo(I) = NUL$
               lbl_articulo(I) = NUL$
           Next I
        End If
        EncontroT = True
     Else
        TxtGrupo(1) = NUL$
        For I = 0 To Txt_Cuenta.Count - 1
           Txt_Cuenta(I) = NUL$
           lbl_cuenta(I) = NUL$
        Next
        For I = 0 To Txt_Articulo.Count - 1
           Txt_Articulo(I) = NUL$
           lbl_articulo(I) = NUL$
        Next I
        EncontroT = False
     End If
   End If
   IngResult = Result 'NYCM M2342
End Sub

Private Sub Grabar()
   DoEvents
   
   Call MouseClock
   Msglin "Ingresando informaci�n del " & Mensaje

   'If (TxtGrupo(0) = NUL$ Or TxtGrupo(1) = NUL$) Then
   If (TxtGrupo(0) = NUL$ Or TxtGrupo(1) = NUL$ Or cboBodega.ListIndex = -1) Then   'Req 1552
      Call Mensaje1("Se deben especificar los datos completos del " & Mensaje, 3)
      Call MouseNorm: Exit Sub
   End If
   
   If (BeginTran(STranIUp & Tabla) <> FAIL) Then
'       If (Not Encontro) Then
'          Valores = "CD_CODI_GRUP=" & Comi & Cambiar_Comas_Comillas(TxtGrupo(0)) & Comi & Coma
'          Valores = Valores & "DE_DESC_GRUP=" & Comi & Cambiar_Comas_Comillas(TxtGrupo(1)) & Comi & Coma
'          Valores = Valores & "CD_ENTR_GRUP=" & Comi & Cambiar_Comas_Comillas(Txt_Cuenta(0)) & Comi & Coma
'          Valores = Valores & "CD_SALI_GRUP=" & Comi & Cambiar_Comas_Comillas(Txt_Cuenta(1)) & Comi & Coma
'          Valores = Valores & "CD_INGR_GRUP=" & Comi & Cambiar_Comas_Comillas(Txt_Cuenta(2)) & Comi & Coma
'          Valores = Valores & "CD_GAST_GRUP=" & Comi & Cambiar_Comas_Comillas(Txt_Cuenta(3)) & Comi & Coma
'          Valores = Valores & "CD_COST_GRUP=" & Comi & Cambiar_Comas_Comillas(Txt_Cuenta(4)) & Comi & Coma
'          Valores = Valores & "CD_AJDB_GRUP=" & Comi & Cambiar_Comas_Comillas(Txt_Cuenta(5)) & Comi & Coma
'          Valores = Valores & "CD_AJCR_GRUP=" & Comi & Cambiar_Comas_Comillas(Txt_Cuenta(6)) & Comi & Coma
'          Valores = Valores & "CD_AGAS_GRUP=" & Comi & Cambiar_Comas_Comillas(Txt_Articulo(0)) & Comi & Coma
'          Valores = Valores & "CD_AING_GRUP=" & Comi & Cambiar_Comas_Comillas(Txt_Articulo(1)) & Comi
'          Valores = Valores & Coma & "NU_AUTO_BODE_GRUP=" & Cod_Bodega(cboBodega.ListIndex) 'Req 1552
'
'          Result = DoInsertSQL(Tabla, Valores)
'        '  If (Result <> FAIL) Then Grabar_Descuento
'        '  If (Result <> FAIL) Then Grabar_Impuesto
'          If (Result <> FAIL) Then Result = Auditor(Tabla, TranIns, LastCmd)
'       Else
'         If WarnMsg("Desea Modificar la Informaci�n del " & Mensaje, 256) Then
'            Valores = " DE_DESC_GRUP=" & Comi & Cambiar_Comas_Comillas(TxtGrupo(1)) & Comi & Coma
'            Valores = Valores & "CD_ENTR_GRUP=" & Comi & Cambiar_Comas_Comillas(Txt_Cuenta(0)) & Comi & Coma
'            Valores = Valores & "CD_SALI_GRUP=" & Comi & Cambiar_Comas_Comillas(Txt_Cuenta(1)) & Comi & Coma
'            Valores = Valores & "CD_INGR_GRUP=" & Comi & Cambiar_Comas_Comillas(Txt_Cuenta(2)) & Comi & Coma
'            Valores = Valores & "CD_GAST_GRUP=" & Comi & Cambiar_Comas_Comillas(Txt_Cuenta(3)) & Comi & Coma
'            Valores = Valores & "CD_COST_GRUP=" & Comi & Cambiar_Comas_Comillas(Txt_Cuenta(4)) & Comi & Coma
'            Valores = Valores & "CD_AJDB_GRUP=" & Comi & Cambiar_Comas_Comillas(Txt_Cuenta(5)) & Comi & Coma
'            Valores = Valores & "CD_AJCR_GRUP=" & Comi & Cambiar_Comas_Comillas(Txt_Cuenta(6)) & Comi & Coma
'            Valores = Valores & "CD_AGAS_GRUP=" & Comi & Cambiar_Comas_Comillas(Txt_Articulo(0)) & Comi & Coma
'            Valores = Valores & "CD_AING_GRUP=" & Comi & Cambiar_Comas_Comillas(Txt_Articulo(1)) & Comi
'            Condicion = "CD_CODI_GRUP=" & Comi & Cambiar_Comas_Comillas(TxtGrupo(0)) & Comi
'            Condicion = Condicion & " AND NU_AUTO_BODE_GRUP=" & Cod_Bodega(cboBodega.ListIndex) 'Req 1552
'
'            Result = DoUpdate(Tabla, Valores, Condicion)
'            'If (Result <> FAIL) Then Grabar_Descuento
'            'If (Result <> FAIL) Then Grabar_Impuesto
'            If (Result <> FAIL) Then Result = Auditor(Tabla, TranUpd, LastCmd)
'         End If
'       End If
       If (Not Encontro) Then
          Valores = "CD_CODI_GRUP=" & Comi & Cambiar_Comas_Comillas(TxtGrupo(0)) & Comi & Coma
          Valores = Valores & "DE_DESC_GRUP=" & Comi & Cambiar_Comas_Comillas(TxtGrupo(1)) & Comi & Coma
          Valores = Valores & "CD_AGAS_GRUP=" & Comi & Cambiar_Comas_Comillas(Txt_Articulo(0)) & Comi & Coma
          Valores = Valores & "CD_AING_GRUP=" & Comi & Cambiar_Comas_Comillas(Txt_Articulo(1)) & Comi
          Result = DoInsertSQL(Tabla, Valores)
          If Result <> FAIL Then
             Valores = "NU_AUTO_BODE_RGB=" & Cod_Bodega(cboBodega.ListIndex) & Coma
             Valores = Valores & "CD_CODI_GRUP_RGB=" & Comi & Cambiar_Comas_Comillas(TxtGrupo(0)) & Comi & Coma
             Valores = Valores & "CD_ENTR_RGB=" & Comi & Cambiar_Comas_Comillas(Txt_Cuenta(0)) & Comi & Coma
             Valores = Valores & "CD_SALI_RGB=" & Comi & Cambiar_Comas_Comillas(Txt_Cuenta(1)) & Comi & Coma
             Valores = Valores & "CD_INGR_RGB=" & Comi & Cambiar_Comas_Comillas(Txt_Cuenta(2)) & Comi & Coma
             Valores = Valores & "CD_GAST_RGB=" & Comi & Cambiar_Comas_Comillas(Txt_Cuenta(3)) & Comi & Coma
             Valores = Valores & "CD_COST_RGB=" & Comi & Cambiar_Comas_Comillas(Txt_Cuenta(4)) & Comi & Coma
             Valores = Valores & "CD_AJDB_RGB=" & Comi & Cambiar_Comas_Comillas(Txt_Cuenta(5)) & Comi & Coma
             Valores = Valores & "CD_TRAN_RGB=" & Comi & Cambiar_Comas_Comillas(Txt_Cuenta(7)) & Comi & Coma
             Valores = Valores & "CD_AJCR_RGB=" & Comi & Cambiar_Comas_Comillas(Txt_Cuenta(6)) & Comi
             'Valores = Valores & "CD_AGAS_RGB=" & Comi & Cambiar_Comas_Comillas(Txt_Articulo(0)) & Comi & Coma
             'Valores = Valores & "CD_AING_RGB=" & Comi & Cambiar_Comas_Comillas(Txt_Articulo(1)) & Comi
             Result = DoInsertSQL("IN_R_GRUP_BODE", Valores)
          End If
          If (Result <> FAIL) Then Result = Auditor(Tabla, TranIns, LastCmd)
       Else
         If WarnMsg("Desea Modificar la Informaci�n del " & Mensaje, 256) Then
            Valores = " DE_DESC_GRUP=" & Comi & Cambiar_Comas_Comillas(TxtGrupo(1)) & Comi & Coma
            Valores = Valores & "CD_AGAS_GRUP=" & Comi & Cambiar_Comas_Comillas(Txt_Articulo(0)) & Comi & Coma
            Valores = Valores & "CD_AING_GRUP=" & Comi & Cambiar_Comas_Comillas(Txt_Articulo(1)) & Comi
            Condicion = "CD_CODI_GRUP=" & Comi & Cambiar_Comas_Comillas(TxtGrupo(0)) & Comi
            Result = DoUpdate(Tabla, Valores, Condicion)
            If Result <> FAIL Then
                Valores = "CD_ENTR_RGB=" & Comi & Cambiar_Comas_Comillas(Txt_Cuenta(0)) & Comi & Coma
                Valores = Valores & "CD_SALI_RGB=" & Comi & Cambiar_Comas_Comillas(Txt_Cuenta(1)) & Comi & Coma
                Valores = Valores & "CD_INGR_RGB=" & Comi & Cambiar_Comas_Comillas(Txt_Cuenta(2)) & Comi & Coma
                Valores = Valores & "CD_GAST_RGB=" & Comi & Cambiar_Comas_Comillas(Txt_Cuenta(3)) & Comi & Coma
                Valores = Valores & "CD_COST_RGB=" & Comi & Cambiar_Comas_Comillas(Txt_Cuenta(4)) & Comi & Coma
                Valores = Valores & "CD_AJDB_RGB=" & Comi & Cambiar_Comas_Comillas(Txt_Cuenta(5)) & Comi & Coma
                Valores = Valores & "CD_TRAN_RGB=" & Comi & Cambiar_Comas_Comillas(Txt_Cuenta(7)) & Comi & Coma
                Valores = Valores & "CD_AJCR_RGB=" & Comi & Cambiar_Comas_Comillas(Txt_Cuenta(6)) & Comi
                Condicion = "CD_CODI_GRUP_RGB=" & Comi & Cambiar_Comas_Comillas(TxtGrupo(0)) & Comi
                Condicion = Condicion & " AND NU_AUTO_BODE_RGB=" & Cod_Bodega(cboBodega.ListIndex)
                'Result = DoUpdate("IN_R_GRUP_BODE", Valores, Condicion)
               'PJCA M1215
                ReDim arr(0)
                Result = LoadData("IN_R_GRUP_BODE", "CD_CODI_GRUP_RGB", Condicion, arr)
                If Encontro Then
                    Result = DoUpdate("IN_R_GRUP_BODE", Valores, Condicion)
                Else
                    Valores = Valores & Coma & " NU_AUTO_BODE_RGB=" & Cod_Bodega(cboBodega.ListIndex) & Coma
                    Valores = Valores & "CD_CODI_GRUP_RGB=" & Comi & Cambiar_Comas_Comillas(TxtGrupo(0)) & Comi
                    Result = DoInsertSQL("IN_R_GRUP_BODE", Valores)
                End If
               'PJCA M1215
            End If
            If (Result <> FAIL) Then Result = Auditor(Tabla, TranUpd, LastCmd)
         End If
       End If
   End If
   If (Result <> FAIL) Then
      If (CommitTran() <> FAIL) Then
          Limpiar
      Else
         Call RollBackTran
      End If
   Else
      Call RollBackTran
   End If
   Call MouseNorm
   Msglin NUL$
End Sub

Private Sub Borrar()
   DoEvents
   
   Call MouseClock
   
   Msglin "Borrando un " & Mensaje
   If (TxtGrupo(0) = NUL$) Then
       Call Mensaje1("Ingrese el c�digo del " & Mensaje, 1)
       Call MouseNorm: Msglin NUL$: Exit Sub
   End If
    If (Encontro <> False) Then
       If (Not WarnDel()) Then Call MouseNorm: Msglin NUL$: Exit Sub
         Condicion = "CD_CODI_GRUP_RGB=" & Comi & Cambiar_Comas_Comillas(TxtGrupo(0)) & Comi
         ' Condicion = " AND NU_AUTO_BODE_RGB=" & Cod_Bodega(cboBodega.ListIndex)
         Condicion = Condicion & " AND NU_AUTO_BODE_RGB=" & Cod_Bodega(cboBodega.ListIndex) 'NYCM M2342
         If (BeginTran(STranDel & Tabla) <> FAIL) Then
            'Call Borrar_Descuento
            'If (Result <> FAIL) Then Borrar_Impuesto
            If (Result <> FAIL) Then Result = DoDelete("IN_R_GRUP_BODE", Condicion)
            'NYCM M2342--------
            ReDim arr(0)
            Condicion = "CD_CODI_GRUP_RGB=" & Comi & Cambiar_Comas_Comillas(TxtGrupo(0)) & Comi
            Result = LoadData("IN_R_GRUP_BODE", "CD_CODI_GRUP_RGB", Condicion, arr())
            If (Result <> FAIL) Then
              If Not Encontro Then
                ReDim arr(0)
                Condicion = "CD_GRUP_RGD=" & Comi & Cambiar_Comas_Comillas(TxtGrupo(0)) & Comi
                Result = LoadData("R_GRUP_DEPE", "CD_GRUP_RGD", Condicion, arr())
                If (Result <> False And Encontro) Then
                'Exiten  asociaciones del grupo con una dependencia
                GoTo continua
                End If
                 Result = IngResult
                '/------------------
                Condicion = "CD_CODI_GRUP=" & Comi & Cambiar_Comas_Comillas(TxtGrupo(0)) & Comi
                If (Result <> FAIL) Then Result = DoDelete(Tabla, Condicion)
                If (Result <> FAIL) Then Result = Auditor(Tabla, TranDel, LastCmd)
              End If
            End If
         End If
   Else
       Call Mensaje1("No se pudo borrar el " & Mensaje & " de la base de datos, debido a que no existe !!!...", 1)
       Call MouseNorm: Msglin NUL$: Exit Sub
   End If
   If (Result <> FAIL) Then
       If (CommitTran() <> FAIL) Then
          Limpiar
       Else
          Call RollBackTran
       End If
   Else
continua: 'NYCM M2342
       Call RollBackTran
   End If
   Call MouseNorm
   Msglin NUL$
End Sub

Private Sub Limpiar()
Dim I As Byte
    cboBodega.ListIndex = -1
    For I = 0 To 1
        TxtGrupo(I) = NUL$
    Next I
    For I = 0 To Txt_Cuenta.Count - 1
        Txt_Cuenta(I) = NUL$
        lbl_cuenta(I) = NUL$
    Next I
    Cbo_Desc.ListIndex = -1
    Cbo_Impu.ListIndex = -1
    For I = 0 To Txt_Articulo.Count - 1
        Txt_Articulo(I) = NUL$
        lbl_articulo(I) = NUL$
    Next I
    cboBodega.SetFocus
End Sub

Private Sub Txt_Articulo_GotFocus(Index As Integer)
    Txt_Articulo(Index).SelStart = 0
    Txt_Articulo(Index).SelLength = Len(Txt_Articulo(Index))
End Sub

Private Sub Txt_Articulo_KeyPress(Index As Integer, KeyAscii As Integer)
    Call Cambiar_Enter(KeyAscii)
End Sub

Private Sub Txt_Articulo_LostFocus(Index As Integer)
    Txt_Articulo(Index) = Trim(Txt_Articulo(Index))
    If Txt_Articulo(Index) = NUL$ Then
       lbl_articulo(Index) = NUL$
    Else
        Call Buscar_Articulo(Index)
    End If
End Sub

Private Sub Buscar_Articulo(Index As Integer)
ReDim arr(0)
    If Txt_Articulo(Index) = NUL$ Then lbl_articulo(Index) = NUL$: Exit Sub
    If Index = 0 Then
       Result = LoadData("GASTOS", "DE_DESC_GAST", "CD_CODI_GAST=" & Comi & Txt_Articulo(Index) & Comi, arr())
    Else
       Result = LoadData("INGRESOS", "DE_DESC_INGR", "CD_CODI_INGR=" & Comi & Txt_Articulo(Index) & Comi, arr())
    End If
    lbl_articulo(Index) = arr(0)
    If arr(0) <> NUL$ Then
        If Valida_Nivel_Articulo(Txt_Articulo(Index), CByte(Index)) Then
           Call Mensaje1("Verifique que la Articulo sea de �ltimo nivel!", 2)
           Txt_Articulo(Index).SetFocus
        End If
    Else
        Call Mensaje1("Articulo no registrado !", 2)
        Txt_Articulo(Index) = NUL$
    End If
End Sub

Private Sub Txt_Cuenta_GotFocus(Index As Integer)
   Txt_Cuenta(Index).SelStart = 0
   Txt_Cuenta(Index).SelLength = Len(Txt_Cuenta(Index))
End Sub

Private Sub Txt_Cuenta_KeyPress(Index As Integer, KeyAscii As Integer)
    Call Cambiar_Enter(KeyAscii)
End Sub

Private Sub Txt_Cuenta_LostFocus(Index As Integer)
   Txt_Cuenta(Index) = Trim(Txt_Cuenta(Index))
   If Txt_Cuenta(Index) = NUL$ Then
      lbl_cuenta(Index) = NUL$
   Else
      'DAHV M5462
      Call Buscar_Cuenta(Index)
      'Call Buscar_Cuenta(Index, False)
      'DAHV M5462
   End If
End Sub

'Private Sub Buscar_Cuenta(Index As Integer) 'HRR M5462
Private Sub Buscar_Cuenta(Index As Integer, Optional BoDeleteCuen As Boolean = True) 'HRR M5462
   ReDim arr(0)
   If Txt_Cuenta(Index) = NUL$ Then lbl_cuenta(Index) = NUL$: Exit Sub
   'JLPB T29905/R28918 INICIO Se deja en comentario la siguiente linea
   'Result = LoadData("CUENTAS", "NO_NOMB_CUEN", "CD_CODI_CUEN=" & Comi & Txt_Cuenta(Index) & Comi, arr())
   Condicion = "CD_CODI_CUEN=" & Comi & Txt_Cuenta(Index) & Comi
   Condicion = Condicion & " AND TX_PUCNIIF_CUEN<>'1'"
   Result = LoadData("CUENTAS", "NO_NOMB_CUEN", Condicion, arr())
   'JLPB T29905/R28918 FIN
   lbl_cuenta(Index) = arr(0)
   If arr(0) <> NUL$ Then
      If Valida_Nivel_Cuentas(Txt_Cuenta(Index)) Then
         Call Mensaje1("Verifique que la cuenta contable sea de �ltimo nivel!", 2)
         Txt_Cuenta(Index).SetFocus
      End If
   Else
      'HRR M5462
      'Call Mensaje1("Cuenta contable no registrada !", 2)
      'Txt_Cuenta(Index) = NUL$ 'CARV R2309 CORREO se comentareo
      'lbl_cuenta(Index) = "Cuenta inexistente" 'CARV R2309 CORREO
      'Txt_Cuenta(Index).SetFocus 'CARV R2309 CORREO
      'HRR M5462
      'HRR M5462
      If BoDeleteCuen Then
         Call Mensaje1("Cuenta contable no registrada !", 2)
         Txt_Cuenta(Index) = NUL$
         lbl_cuenta(Index) = NUL$
      Else
         lbl_cuenta(Index) = "Cuenta Inexistente"
      End If
      'HRR M5462
   End If
End Sub

Private Sub TxtGrupo_GotFocus(Index As Integer)
TxtGrupo(Index).SelStart = 0
TxtGrupo(Index).SelLength = Len(TxtGrupo(Index).Text)
Select Case Index
    Case 0: Msglin "Digite el c�digo del " & Mensaje
    Case 1: Msglin "Digite la descripci�n del " & Mensaje
            If TxtGrupo(0) = NUL$ Then TxtGrupo(0).SetFocus: Exit Sub
End Select
End Sub

Private Sub TxtGrupo_KeyDown(Index As Integer, KeyCode As Integer, Shift As Integer)
    Select Case Index
        Case 0: If KeyCode = 40 Then TxtGrupo(Index + 1).SetFocus: Exit Sub
        Case 1: If KeyCode = 38 Then TxtGrupo(Index - 1).SetFocus: Exit Sub
    End Select
End Sub

Private Sub TxtGrupo_KeyPress(Index As Integer, KeyAscii As Integer)
  Call ValKeyAlfaNum(KeyAscii)
  Call Cambiar_Enter(KeyAscii)
End Sub

Private Sub TxtGrupo_LostFocus(Index As Integer)
     Msglin NUL$
     Select Case Index
        Case 0: 'If TxtGrupo(0) <> NUL$ Then Buscar_Grupo
                 If TxtGrupo(0) <> NUL$ And cboBodega.ListIndex <> -1 Then Buscar_Grupo
     End Select
End Sub

'DEPURACION DE CODIGO
'Private Sub Grabar_Descuento()
'    Call Borrar_Descuento
'    If Cbo_Desc.ListIndex = -1 Then Exit Sub
'    Valores = "CD_GRUP_RGDE=" & Comi & TxtGrupo(0) & Comi & Coma & _
'              "CD_DESC_RGDE=" & Comi & Cod_Descuento(Cbo_Desc.ListIndex) & Comi
'    Result = DoInsertSQL("R_GRUP_DESC", Valores)
'End Sub

'DEPURACION DE CODIGO
'Private Sub Grabar_Impuesto()
'    Call Borrar_Impuesto
'    If Cbo_Impu.ListIndex = -1 Then Exit Sub
'    Valores = "CD_GRUP_RGI=" & Comi & TxtGrupo(0) & Comi & Coma & _
'              "CD_IMPU_RGI=" & Comi & Cod_Impuesto(Cbo_Impu.ListIndex) & Comi
'    Result = DoInsertSQL("R_GRUP_IMPU", Valores)
'End Sub

Private Sub Borrar_Descuento()
    Valores = "CD_GRUP_RGDE=" & Comi & TxtGrupo(0) & Comi
    Result = DoDelete("R_GRUP_DESC", Valores)
End Sub

Private Sub Borrar_Impuesto()
    Valores = "CD_GRUP_RGI=" & Comi & TxtGrupo(0) & Comi
    Result = DoDelete("R_GRUP_IMPU", Valores)
End Sub

'DEPURACION DE CODIGO
'Private Sub Leer_Descuento()
'Dim Arr(0)
'    Valores = "CD_GRUP_RGDE=" & Comi & TxtGrupo(0) & Comi
'    Result = LoadData("R_GRUP_DESC", "CD_DESC_RGDE", Valores, Arr())
'    If Arr(0) = NUL$ Then Cbo_Desc.ListIndex = -1 Else Cbo_Desc.ListIndex = FindInArr(Cod_Descuento, Arr(0))
'End Sub

'DEPURACION DE CODIGO
'Private Sub Leer_Impuesto()
'Dim Arr(0)
'    Valores = "CD_GRUP_RGI=" & Comi & TxtGrupo(0) & Comi
'    Result = LoadData("R_GRUP_IMPU", "CD_IMPU_RGI", Valores, Arr())
'    If Arr(0) = NUL$ Then Cbo_Impu.ListIndex = -1 Else Cbo_Impu.ListIndex = FindInArr(Cod_Impuesto, Arr(0))
'End Sub
