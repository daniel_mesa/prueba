VERSION 5.00
Object = "{0BA686C6-F7D3-101A-993E-0000C0EF6F5E}#1.0#0"; "THREED32.OCX"
Begin VB.Form FrmLic 
   Appearance      =   0  'Flat
   Caption         =   "Generador de Licencias"
   ClientHeight    =   2565
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   7080
   Icon            =   "FrmLic.frx":0000
   LinkTopic       =   "Form1"
   ScaleHeight     =   2565
   ScaleWidth      =   7080
   StartUpPosition =   3  'Windows Default
   Begin VB.TextBox TxtEsta 
      Height          =   285
      Left            =   2640
      MaxLength       =   4
      TabIndex        =   11
      Top             =   2160
      Width           =   855
   End
   Begin VB.OptionButton OpLic 
      Caption         =   "Concurrente"
      Height          =   255
      Index           =   1
      Left            =   1320
      TabIndex        =   10
      Top             =   2160
      Width           =   1215
   End
   Begin VB.OptionButton OpLic 
      Caption         =   "Corporativa"
      Height          =   255
      Index           =   0
      Left            =   120
      TabIndex        =   9
      Top             =   2160
      Value           =   -1  'True
      Width           =   1215
   End
   Begin VB.TextBox TxtId 
      Height          =   285
      Left            =   1560
      MaxLength       =   15
      TabIndex        =   6
      Top             =   1200
      Width           =   1815
   End
   Begin VB.CheckBox ChkPrueba 
      Caption         =   "Version de Prueba"
      Height          =   240
      Left            =   4800
      TabIndex        =   2
      Top             =   270
      Width           =   2100
   End
   Begin VB.Frame Frame1 
      Height          =   1100
      Left            =   4800
      TabIndex        =   12
      Top             =   480
      Width           =   2150
      Begin Threed.SSCommand SCmd_Options 
         Height          =   735
         Index           =   0
         Left            =   150
         TabIndex        =   13
         Top             =   200
         Width           =   855
         _Version        =   65536
         _ExtentX        =   1508
         _ExtentY        =   1296
         _StockProps     =   78
         Caption         =   "&G                             "
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Serif"
            Size            =   6
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Picture         =   "FrmLic.frx":0442
      End
      Begin Threed.SSCommand SCmd_Options 
         Height          =   735
         Index           =   1
         Left            =   1150
         TabIndex        =   14
         Top             =   200
         Width           =   855
         _Version        =   65536
         _ExtentX        =   1508
         _ExtentY        =   1296
         _StockProps     =   78
         Caption         =   "&S                             "
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Serif"
            Size            =   6
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Picture         =   "FrmLic.frx":0FE4
      End
   End
   Begin VB.ComboBox TxtRuta 
      Height          =   315
      ItemData        =   "FrmLic.frx":1AEA
      Left            =   1080
      List            =   "FrmLic.frx":1AEC
      TabIndex        =   4
      Top             =   720
      Width           =   3495
   End
   Begin VB.ComboBox TxtArchivo 
      Height          =   315
      ItemData        =   "FrmLic.frx":1AEE
      Left            =   1080
      List            =   "FrmLic.frx":1AF0
      TabIndex        =   1
      Top             =   240
      Width           =   3495
   End
   Begin VB.TextBox TxtNombre 
      Height          =   285
      Left            =   1560
      MaxLength       =   100
      TabIndex        =   8
      Top             =   1680
      Width           =   4455
   End
   Begin VB.Label Label1 
      Caption         =   "Nit oCC del Cliente:"
      Height          =   255
      Left            =   120
      TabIndex        =   5
      Top             =   1200
      Width           =   1455
   End
   Begin VB.Label Label4 
      Caption         =   "Archivo .exe"
      Height          =   255
      Left            =   120
      TabIndex        =   0
      Top             =   240
      Width           =   975
   End
   Begin VB.Label Label3 
      Caption         =   "Guardar en :"
      Height          =   255
      Left            =   120
      TabIndex        =   3
      Top             =   720
      Width           =   975
   End
   Begin VB.Label Label2 
      Caption         =   "Nombre del usuario:"
      Height          =   255
      Left            =   120
      TabIndex        =   7
      Top             =   1680
      Width           =   1455
   End
End
Attribute VB_Name = "FrmLic"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'HRR R1249
Private Sub ChkPrueba_KeyPress(KeyAscii As Integer)
   If KeyAscii = 32 Then
      If ChkPrueba.Value Then
         'TxtEsta.Enabled = True
         OpLic(1).Enabled = True
      Else
         OpLic(0).Value = True
         OpLic(1).Enabled = False
         TxtEsta.Text = "0"
         TxtEsta.Enabled = False
      End If
   Else
      Call Cambiar_Enter(KeyAscii)
   End If

End Sub

Private Sub ChkPrueba_MouseUp(Button As Integer, Shift As Integer, X As Single, Y As Single)
      If ChkPrueba.Value Then
         OpLic(0).Value = True
         OpLic(1).Enabled = False
         TxtEsta.Text = "0"
         TxtEsta.Enabled = False
         
      Else
         OpLic(1).Enabled = True
         'TxtEsta.Enabled = True
         
      End If
End Sub
'HRR R1249



Private Sub Form_Load()
 For I = 0 To 9
   TxtArchivo.AddItem GetSetting("LICENCIAS", "GENERAL", "ARCHIVO" & I, "")
   TxtRuta.AddItem GetSetting("LICENCIAS", "GENERAL", "RUTA" & I, "")
 Next
End Sub
Private Sub Form_Unload(Cancel As Integer)
Dim I As Integer
For I = 0 To 9
   SaveSetting "LICENCIAS", "GENERAL", "ARCHIVO" & I, TxtArchivo.List(I)
   SaveSetting "LICENCIAS", "GENERAL", "RUTA" & I, TxtRuta.List(I)
Next
End Sub


Private Sub OpLic_KeyPress(Index As Integer, KeyAscii As Integer)

   Call Cambiar_Enter(KeyAscii)

End Sub

Private Sub OpLic_GotFocus(Index As Integer)
 
 If Index = 1 Then
    OpLic(1).Value = True
    TxtEsta.Enabled = True
    TxtEsta.SetFocus
 End If
End Sub

Private Sub OpLic_LostFocus(Index As Integer)

   If Index = 0 Then
      TxtEsta.Text = ""
      TxtEsta.Enabled = False
   Else
      TxtEsta.Enabled = True
      TxtEsta.SetFocus
   End If
   
End Sub

Private Sub SCmd_Options_Click(Index As Integer)
Dim Miregistro As Licencia
Dim Fecha As Date
Dim caracter As String
Screen.MousePointer = 11
Select Case Index
   Case 0:
            On Error GoTo control
            
            If validar_campos() Then Screen.MousePointer = 1: Exit Sub
                         
            If InStr(UCase(TxtArchivo), ".EXE") > 0 Then
                  caracter = Mid(TxtRuta, Len(TxtRuta), 1)
                  If caracter <> "\" Then
                     Screen.MousePointer = 1
                     MsgBox ("Ruta no v�lida")
                     Exit Sub
                  End If
                  Fecha = Format(FileDateTime(TxtArchivo), "dd/mm/yyyy")
                  Miregistro.Id = Trim(TxtId) 'HRR R1249
                  Miregistro.Nombre = TxtNombre
                  Miregistro.Fecha = CLng(Format(Fecha, "yyyy") & Format(Fecha, "mm") & Format(Fecha, "dd"))
                  Miregistro.Prueba = IIf(ChkPrueba.Value, "1", "0") 'HRR R1074
                  If OpLic(0) Then TxtEsta.Text = "0" 'HRR R1249
                  Miregistro.Numesta = CLng(TxtEsta) 'HRR R1249
                                    
                  Open TxtRuta & "CntLic" For Binary Access Write As #1 Len = Len(Miregistro)
                     Put #1, 1, Miregistro
                  Close #1
            Else
                  Call MsgBox("El archivo debe ser un ejecutable (.exe) ", vbExclamation)
                  Screen.MousePointer = 1
                  Exit Sub
            End If
            BAND = 0
            For I = 0 To 9
               If TxtArchivo.List(I) = TxtArchivo Then BAND = 1
            Next
            TxtArchivo.Tag = TxtArchivo.Text
            If BAND = 0 Then
               For I = 9 To 0 Step -1
                  TxtArchivo.List(I) = TxtArchivo.List(I - 1)
               Next
               TxtArchivo.List(0) = TxtArchivo.Tag
               TxtArchivo.ListIndex = 0
            End If
            
            BAND = 0
            For I = 0 To 9
               If TxtRuta.List(I) = TxtRuta Then BAND = 1
            Next
            TxtRuta.Tag = TxtRuta.Text
            If BAND = 0 Then
               For I = 9 To 0 Step -1
                  TxtRuta.List(I) = TxtRuta.List(I - 1)
               Next
               TxtRuta.List(0) = TxtRuta.Tag
               TxtRuta.ListIndex = 0
            End If
            MsgBox "Fin del proceso", vbInformation
   Case 1: Unload Me
End Select
Screen.MousePointer = 1
Exit Sub
control:
Select Case Err.Number
   Case 71
   Case 53: MsgBox "No se encuentra el archivo .exe", vbExclamation
   Case Else
            MsgBox Err.Number & " " & Err.Description
End Select
Screen.MousePointer = 1
End Sub

Private Sub TxtArchivo_KeyPress(KeyAscii As Integer)

   Call Cambiar_Enter(KeyAscii)

End Sub


Private Sub TxtEsta_KeyPress(KeyAscii As Integer)

   Call Cambiar_Enter(KeyAscii)

End Sub

Private Sub TxtId_KeyPress(KeyAscii As Integer)

   Call Cambiar_Enter(KeyAscii)

End Sub

Private Sub TxtNombre_KeyPress(KeyAscii As Integer)

   Call Cambiar_Enter(KeyAscii)

End Sub

Private Sub TxtNombre_LostFocus()

   TxtNombre = UCase(TxtNombre)
   
End Sub

Private Sub TxtRuta_KeyPress(KeyAscii As Integer)

   Call Cambiar_Enter(KeyAscii)
   
End Sub

Private Function validar_campos() As Boolean
   
   validar_campos = True
   
   If TxtNombre.Text = "" Then
      MsgBox "Establezca el nombre del usuario", vbExclamation
   ElseIf TxtId.Text = "" Or (Not IsNumeric(TxtId.Text)) Then
      MsgBox "Establezca el numero de nit o cedula, unicamente numeros.", vbExclamation
   ElseIf Len(Trim(TxtId.Text)) < 4 Then
      MsgBox "El numero de nit o la cedula debe tener mas de tres digitos", vbExclamation
   ElseIf OpLic(1).Value And (TxtEsta.Text = "" Or (Not IsNumeric(TxtEsta.Text))) Then
      MsgBox "Establezca el numero de estaciones y debe ser mayor a cero", vbExclamation
   ElseIf OpLic(1).Value Then
      If CLng(TxtEsta.Text) <= 0 Then
         MsgBox "El numero de estaciones debe ser mayor a cero", vbExclamation
      Else
         validar_campos = False
      End If
   Else
      validar_campos = False
   End If
   

End Function
