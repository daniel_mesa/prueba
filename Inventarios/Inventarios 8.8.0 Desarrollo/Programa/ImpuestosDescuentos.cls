VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "ImpuDesc"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Private ArImp() As Variant
Private ArDesc() As Variant
Private ClssID As ImpuDesc
Private BoIvaOtrosImp As Boolean
Private AutoRet As String
Private AutRetIva As String
Private AutRetIca As String
Private TipoRegimen As String
Private GranContrib As String
Private ExentoIva As String
Private ExentoIca As String
Private ExentoFuente As String
Private TipoPers As String
Private StConcepto As String
Private BoTieneDatos As Boolean
Private StImpCree As String 'OMOG R16934/T19113 VARIABLE QUE GUARDA LA OPCION SELECCIONADA PARA EL TERCERO EN CUANTO A "NO SUJETO PASIVO CREE O AUTORRETENEDORES DEL IMPUESTO CREE"
Dim StActi As String    'OMOG R16934/T19113 VARIABLE QUE GUARDA LA ACTIVIDAD ECONOMICA

Public Property Let AutoRetenedor(StAutoRet As String)
   AutoRet = StAutoRet
End Property

Public Property Let AutoRetenedorIva(StAutoRetIva As String)
   AutoRetIva = StAutoRetIva
End Property

Public Property Let AutoRetenedorIca(StAutoRetIca As String)
   AutoRetIca = StAutoRetIca
End Property

Public Property Let PTipoRegimen(StTipoReg As String)
   TipoRegimen = StTipoReg
End Property

Public Property Let GranContribuyente(StGranContrib As String)
   GranContrib = StGranContrib
End Property

Public Property Let PExentoIva(StExentoIva As String)
   ExentoIva = StExentoIva
End Property

Public Property Let PExentoIca(StExentoIca As String)
   ExentoIca = StExentoIca
End Property

Public Property Let PExentoFuente(StExentoFuente As String)
   ExentoFuente = StExentoFuente
End Property

Public Property Let TipoPersona(StTipoPersona As String)
   TipoPers = StTipoPersona
End Property

Public Property Let concepto(StConc As String)
   StConcepto = StConc
End Property

Public Property Let TieneDatos(BoDatos As Boolean)
   BoTieneDatos = BoDatos
End Property

Public Property Set RefImpuDesc(ByRef OImpDes As ImpuDesc)
   Set ClssID = OImpDes
End Property

Public Property Get AutoRetenedor() As String
   AutoRetencion = AutoRet
End Property

Public Property Get AutoRetenedorIva() As String
   AutoRetencionIva = AutoRetIva
End Property

Public Property Get AutoRetenedorIca() As String
   AutoRetencionIca = AutoRetIca
End Property

Public Property Get PTipoRegimen() As String
   PTipoRegimen = TipoReg
End Property

Public Property Get GranContribuyente() As String
   GranContribuyente = GranContrib
End Property

Public Property Get PExentoIva() As String
   PExentoIva = ExentoIva
End Property

Public Property Get PExentoIca() As String
   PExentoIca = ExentoIca
End Property

Public Property Get PExentoFuente() As String
   PExentoFuente = ExentoFuente
End Property

Public Property Get TipoPersona() As String
   TipoPersona = TipoPers
End Property

Public Property Get concepto() As String
   concepto = StConcepto
End Property

Public Property Get SizeImp() As Integer
   SizeImp = UBound(ArImp, 2)
End Property

Public Property Get SizeDesc() As Integer
   SizeDesc = UBound(ArDesc, 2)
End Property

Public Property Get ElemImp(ByVal InFila As Integer, ByVal InCol As Integer) As Variant
   ElemImp = ArImp(InCol, InFila)
End Property

Public Property Get ElemDesc(ByVal InFila As Integer, ByVal InCol As Integer) As Variant
   ElemDesc = ArDesc(InCol, InFila)
End Property

Public Property Get TieneDatos() As Boolean
   'BoTieneDatos = TieneDatos 'HRR M2644
   TieneDatos = BoTieneDatos 'HRR M2644
End Property

Public Sub Impuestos(ByRef ArrImp() As Variant)
    ArImp() = ArrImp
End Sub

Public Sub Descuentos(ByRef ArrDesc() As Variant)
    ArDesc() = ArrDesc
End Sub

Public Sub ImpuestosRef(ByRef ArrImp() As Variant)
    Call ClssID.Impuestos(ArrImp)
End Sub

Public Sub DescuentosRef(ByRef ArrDesc() As Variant)
    Call ClssID.Descuentos(ArrDesc)
End Sub

Public Function ConceptoRef() As String
   ConceptoRef = ClssID.concepto
End Function

Public Sub SetConceptoRef(StConc As String)
   ClssID.concepto = StConc
End Sub

Public Sub SetTieneDatosRef(BoDatos As Boolean)
   ClssID.TieneDatos = BoDatos
End Sub

Public Sub SetElemImp(ByVal InFila As Integer, ByVal InCol As Integer, ByVal valor As Variant)
   ArImp(InCol, InFila) = valor
End Sub

Public Sub SetElemDesc(ByVal InFila As Integer, ByVal InCol As Integer, ByVal valor As Variant)
   ArDesc(InCol, InFila) = valor
End Sub

Private Sub Class_Initialize()

   
   ReDim ArDesc(6, 0)
   ReDim ArImp(6, 0)
   StConcepto = "CINV"
   BoTieneDatos = False
   
End Sub

Public Sub Buscar_Descu()

   ReDim ArDesc(6, 0)
    'CARGAR DESCUENTOS
    Desde = "DESCUENTO,R_CONC_DESC"
    Campos = "CD_CODI_DESC,DE_NOMB_DESC,PR_PORC_DESC,VL_TOPE_DESC,VL_VALO_DESC,CD_CUEN_DESC,TX_TIPO_DESC"
    Condicion = "CD_CODI_DESC=CD_DESC_CODE AND CD_CONC_CODE='" & StConcepto & "'"
    'GAPM M6403:30625 INICIO
    'No se valido con tipo de documento ya que se muestran tan solo los descuentos que apliquen a cuentas por pagar y tesoreria
    Condicion = Condicion & " AND (TX_TIPDESC_DESC='2' OR TX_TIPDESC_DESC='3')"
    'GAPM M6403:30625 FIN
    Result = LoadMulData(Desde, Campos, Condicion, ArDesc)
    If Result <> FAIL And Encontro Then
      BoTieneDatos = True
    Else
      BoTieneDatos = False
    End If
    
    
End Sub

Public Sub Buscar_Impu()

Dim Ini As Integer
Dim InJ As Integer
Dim ArrImp() As Variant


   'ReDim Arr(5) 'OMOG R16934/T19113 SE ESTABLECE EN COMENTARIO
   ReDim Arr(6)  'OMOG R16934/T19113
   ReDim ArImp(6, 0)
     
   StImpCree = StImpCree
   StActi = StActi
   InJ = 0
   BoIvaOtrosImp = False
    
   Campos = "CD_IMPU_COIM"
   Desde = "R_CONC_IMPU, TC_IMPUESTOS"
   Condicion = "CD_CODI_IMPU=CD_IMPU_COIM AND CD_CONC_COIM='" & StConcepto & "'"
      
   If AutoRet = "1" Or ExentoFuente = "1" Then Condicion = Condicion & " AND ID_TIPO_IMPU <> 'R'"
   If AutRetIva = "1" Or ExentoIva = "1" Then Condicion = Condicion & " AND ID_TIPO_IMPU <> 'M'"
   If AutRetIca = "1" Or ExentoIca = "1" Then Condicion = Condicion & " AND ID_TIPO_IMPU <> 'C'"
    
   Result = FiltrarImpuestos(Campos, Desde, Condicion, TipoPers, TipoRegimen, GranContrib, ArrImp())
   If Result <> FAIL And Encontro Then
      BoTieneDatos = True
      For Ini = 0 To UBound(ArrImp())
         'Campos = "CD_CODI_IMPU,DE_NOMB_IMPU,PR_PORC_IMPU,VL_TOPE_IMPU,CD_CUEN_IMPU,ID_TIPO_IMPU" 'OMOG R16934/T19113 SE ESTABLECE EN COMENTARIO
         Campos = "CD_CODI_IMPU,DE_NOMB_IMPU,PR_PORC_IMPU,VL_TOPE_IMPU,CD_CUEN_IMPU,ID_TIPO_IMPU,CANT_PORC_IMAC" 'OMOG R16934/T19113
         Condicion = "CD_CODI_IMPU=" & Comi & ArrImp(Ini) & Comi
         'Result = LoadData("TC_IMPUESTOS", Campos, Condicion, Arr) 'OMOG R16934/T19113 SE ESTABLECE EN COMENTARIO
         Result = LoadData("TC_IMPUESTOS LEFT JOIN IMP_ACTECO_PORC ON (CD_CODI_IMPU=NU_NUME_IMP AND NU_NUME_ACTECO='" & StActi & "')", Campos, Condicion, Arr) 'OMOG R16934/T19113
         If Result <> FAIL And Encontro Then
            If Arr(5) = "I" Or Arr(5) = "O" Then
               
               BoIvaOtrosImp = True
                  
            Else
               If Arr(6) = NUL$ Then 'OMOG R16934/T19113
                  'HRR M2642
                  'If UBound(ArImp, 2) <> 0 Then
                  '   ReDim Preserve ArImp(6, UBound(ArImp, 2) + 1)
                  '   InJ = InJ + 1
                  'End If
                  'HRR M2642
                  
                  'HRR M2642
                  If InJ <> 0 Then
                     ReDim Preserve ArImp(6, UBound(ArImp, 2) + 1)
                  End If
                  'HRR M2642
                  
                  ArImp(0, InJ) = Arr(0)
                  ArImp(1, InJ) = Arr(1)
                  ArImp(2, InJ) = Arr(2)
                  ArImp(3, InJ) = Arr(3)
                  ArImp(4, InJ) = 0 'valor
                  ArImp(5, InJ) = Arr(4)
                  ArImp(6, InJ) = Arr(5)
                  InJ = InJ + 1
               'OMOG R16934/T19113 INICIO
               Else
                  If StImpCree <> 1 Then
                     If InJ <> 0 Then
                        ReDim Preserve ArImp(6, UBound(ArImp, 2) + 1)
                     End If
                     
                     ArImp(0, InJ) = Arr(0)
                     ArImp(1, InJ) = Arr(1)
                     ArImp(2, InJ) = Arr(6)
                     ArImp(3, InJ) = Arr(3)
                     ArImp(4, InJ) = 0 'valor
                     ArImp(5, InJ) = Arr(4)
                     ArImp(6, InJ) = Arr(5)
                     InJ = InJ + 1
                     End If
               End If
               'OMOG R16934/T19113 FIN
            End If
         End If
                                 
      Next
   'HRR DEFECTO Se debe llamar primero Buscar_desc y despues Buscar_impu
   'si hay descuento BoTieneDatos=TRUE, si no hay descuentos BoTieneDatos=false, si despuesde descuentos no se encuentra impuestos, depende del valor en descuentos.
'   Else
'        BoTieneDatos = False
   'HRR DEFECTO
   End If
   
End Sub
'HRR R1853

'---------------------------------------------------------------------------------------
' Procedure : ImpuestoCree()
' DateTime  : 08/11/2013 10:00 am
' Author    : Oskar_Orozco R16934/T19113
' Purpose   : Se crea esta propiedad para llamar el dato de "NO SUJETO PASIVO CREE O AUTORRETENEDORES DEL IMPUESTO CREE"
'             del Tercero escogido en la compra
'---------------------------------------------------------------------------------------

Public Property Let ImpuestoCree(StImpuCree As String)
   StImpCree = StImpuCree
End Property

Public Property Get ImpuestoCree() As String
   ImpuestoCree = StImpCree
End Property

'---------------------------------------------------------------------------------------
' Procedure : ActividadEconomica()
' DateTime  : 08/11/2013 10:15 am
' Author    : Oskar_Orozco R16934/T19113
' Purpose   : Se crea esta propiedad para llamar el dato de la Actividad Econůmica del Tercero escogido en la compra
'---------------------------------------------------------------------------------------

Public Property Let ActividadEconomica(StActiEco As String)
   StActi = StActiEco
End Property

Public Property Get ActividadEconomica() As String
   ActividadEconomica = StActi
End Property
