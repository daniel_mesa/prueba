VERSION 5.00
Begin VB.Form FrmCambiar 
   BorderStyle     =   4  'Fixed ToolWindow
   ClientHeight    =   1380
   ClientLeft      =   45
   ClientTop       =   270
   ClientWidth     =   5280
   Icon            =   "FrmCambiar.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   1380
   ScaleWidth      =   5280
   ShowInTaskbar   =   0   'False
   StartUpPosition =   3  'Windows Default
   Begin VB.TextBox Txt_Codigo 
      Height          =   285
      Left            =   0
      TabIndex        =   1
      Top             =   840
      Width           =   4215
   End
   Begin VB.CommandButton Cmd_Cancelar 
      Cancel          =   -1  'True
      Caption         =   "CANCELAR"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   6
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   465
      Left            =   4320
      Picture         =   "FrmCambiar.frx":000C
      Style           =   1  'Graphical
      TabIndex        =   3
      Top             =   720
      Width           =   855
   End
   Begin VB.CommandButton Cmd_Aceptar 
      Caption         =   "ACEPTAR"
      Default         =   -1  'True
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   6
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   465
      Left            =   4320
      Picture         =   "FrmCambiar.frx":034E
      Style           =   1  'Graphical
      TabIndex        =   2
      Top             =   120
      Width           =   855
   End
   Begin VB.Label Lbl_des 
      Caption         =   "Ingrese el nuevo c�digo"
      Height          =   375
      Left            =   120
      TabIndex        =   0
      Top             =   120
      Width           =   3615
   End
End
Attribute VB_Name = "FrmCambiar"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Dim arrEventos() As Variant
Public Longitud   As Integer
Public Tipo_Dato  As String

Private Sub Cmd_Aceptar_Click()
   Codigo = Trim(Txt_Codigo)
   Unload Me
End Sub

Private Sub Cmd_Cancelar_Click()
   Codigo = NUL$
   Unload Me
End Sub
Private Sub Form_Load()
   Call CenterForm(MDI_Inventarios, Me)
   Call EventosControles(Me, arrEventos)
   Me.Caption = ProgName
   Txt_Codigo.MaxLength = Longitud
End Sub

Private Sub Txt_Codigo_KeyPress(KeyAscii As Integer)
   If Tipo_Dato = "N" Then
      Call ValKeyNum(KeyAscii)
   Else
      Call ValKeySinComaComi(KeyAscii)
   End If
End Sub
