VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "MSFLXGRD.OCX"
Object = "{0D452EE1-E08F-101A-852E-02608C4D0BB4}#2.0#0"; "FM20.DLL"
Begin VB.Form FrmTabSec 
   BorderStyle     =   4  'Fixed ToolWindow
   ClientHeight    =   4080
   ClientLeft      =   45
   ClientTop       =   270
   ClientWidth     =   5640
   Icon            =   "FrmTabSec.frx":0000
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   4080
   ScaleWidth      =   5640
   ShowInTaskbar   =   0   'False
   Begin MSComctlLib.Toolbar Tlb_Opciones 
      Align           =   1  'Align Top
      Height          =   420
      Left            =   0
      TabIndex        =   0
      Top             =   0
      Width           =   5640
      _ExtentX        =   9948
      _ExtentY        =   741
      ButtonWidth     =   609
      ButtonHeight    =   582
      Appearance      =   1
      _Version        =   393216
      BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
         NumButtons      =   7
         BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Style           =   3
         EndProperty
         BeginProperty Button2 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "G"
            Object.ToolTipText     =   "Guardar"
         EndProperty
         BeginProperty Button3 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "B"
            Object.ToolTipText     =   "Borrar"
         EndProperty
         BeginProperty Button4 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "I"
            Object.ToolTipText     =   "Imprimir"
         EndProperty
         BeginProperty Button5 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Style           =   3
         EndProperty
         BeginProperty Button6 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Enabled         =   0   'False
            Key             =   "C"
            Object.ToolTipText     =   "Cambiar C�digo"
         EndProperty
         BeginProperty Button7 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "A"
            Object.ToolTipText     =   "Ayuda"
         EndProperty
      EndProperty
   End
   Begin MSFlexGridLib.MSFlexGrid FGrdLista 
      Height          =   2175
      Left            =   120
      TabIndex        =   8
      Top             =   1800
      Width           =   5415
      _ExtentX        =   9551
      _ExtentY        =   3836
      _Version        =   393216
      Rows            =   1
      Cols            =   5
      FixedCols       =   0
      BackColorBkg    =   16777215
      AllowUserResizing=   1
   End
   Begin VB.Frame Fram_Base 
      Height          =   1335
      Left            =   120
      TabIndex        =   1
      Top             =   400
      Width           =   5415
      Begin VB.TextBox Txt_Codigo 
         Height          =   285
         Left            =   840
         TabIndex        =   5
         Top             =   600
         Width           =   4455
      End
      Begin VB.TextBox Txt_Desc 
         Height          =   285
         Left            =   840
         TabIndex        =   7
         Top             =   960
         Width           =   4455
      End
      Begin MSForms.ComboBox Cbof_Padre 
         Height          =   315
         Left            =   840
         TabIndex        =   3
         Top             =   240
         Width           =   4455
         VariousPropertyBits=   679495707
         DisplayStyle    =   3
         Size            =   "7858;556"
         TextColumn      =   2
         ColumnCount     =   3
         cColumnInfo     =   3
         MatchEntry      =   1
         ShowDropButtonWhen=   2
         FontHeight      =   165
         FontCharSet     =   0
         FontPitchAndFamily=   2
         Object.Width           =   "0;1411;3527"
      End
      Begin VB.Label Lbl_Padre 
         Caption         =   "Pa�s"
         Height          =   375
         Left            =   120
         TabIndex        =   2
         Top             =   260
         Width           =   735
      End
      Begin VB.Label Lbl_Codigo 
         Caption         =   "&C�digo"
         Height          =   255
         Left            =   120
         TabIndex        =   4
         Top             =   600
         Width           =   615
      End
      Begin VB.Label Lbl_Nombre 
         Caption         =   "&Nombre"
         Height          =   255
         Left            =   120
         TabIndex        =   6
         Top             =   960
         Width           =   615
      End
   End
End
Attribute VB_Name = "FrmTabSec"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Dim arrEventos() As C_Eventos
Public LblPadre   As String
Public TabPadre   As String   'Tabla padre
Public AutPadre   As String   'Autonumerico tabla padre
Public AutHijo    As String   'Autonumerio hijo
Public CodPadre   As String   'Codigo tabla
Public DesPadre   As String   'Nombre tabla padre
Public ConPadre   As String   'Condicion
Public ColPadre   As String   'Columna que se muestra en el combo
Public Tabla      As String
Public CampCodi   As String   'Nombre del campo del codigo
Public CampDesc   As String   'Nombre del campo de la descripcion
Public TipoCodi   As String   'Tipo del codigo N:Numerico, Texto
Public Informe    As String
Dim Carcomi       As String   'Caracter comilla
Public OpcCod     As String   'Opci�n de seguridad

Private Sub Cbof_Padre_LostFocus()
   Txt_Codigo = NUL$
   Txt_Desc = NUL$
End Sub

Private Sub FGrdLista_Click()
If FGrdLista.MouseRow = 0 Then
 If Orden <> 0 Then
   FGrdLista.Sort = 1
   Orden = 0
 Else
   FGrdLista.Sort = 2
   Orden = 1
 End If
End If
'   If FGrdLista.MouseRow = 0 Then FGrdLista.Sort = 1
End Sub

Private Sub FGrdLista_DblClick()
   If FGrdLista.Row > 0 Then
      Cbof_Padre.ListIndex = FindInCtrlForms(Cbof_Padre, FGrdLista.TextMatrix(FGrdLista.Row, 4), 0)
      Txt_Codigo = FGrdLista.TextMatrix(FGrdLista.Row, 2)
      Txt_Desc = FGrdLista.TextMatrix(FGrdLista.Row, 3)
      Txt_Desc.SetFocus
      Tlb_Opciones.Buttons("C").Enabled = IIf(Txt_Codigo = NUL$, False, True)
   End If
End Sub

Private Sub Form_Activate()
   FrmMenu.CodOpc = OpcCod
End Sub

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
   Select Case KeyCode
      Case vbKeyF1:
      Case vbKeyF2: If Tlb_Opciones.Buttons("G").Enabled = True Then Call Tlb_Opciones_ButtonClick(Tlb_Opciones.Buttons("G"))
      Case vbKeyF3: If Tlb_Opciones.Buttons("B").Enabled = True Then Call Tlb_Opciones_ButtonClick(Tlb_Opciones.Buttons("B"))
      Case vbKeyF11: If Tlb_Opciones.Buttons("I").Enabled = True Then Call Tlb_Opciones_ButtonClick(Tlb_Opciones.Buttons("I"))
      Case vbKeyF5:
      Case vbKeyF6: If Tlb_Opciones.Buttons("C").Enabled = True Then Call Tlb_Opciones_ButtonClick(Tlb_Opciones.Buttons("C"))
      Case vbKeyF12: Unload Me
   End Select
End Sub
Private Sub Form_Load()
   Orden = 0
   Call CenterForm(MDI_Inventarios, Me)
   Call LoadIconos(Tlb_Opciones)
   Call EventosControles(Me, arrEventos)
   Call Estado_Botones_Forma(Tlb_Opciones, OpcCod)
   Campos = AutPadre & Coma & CodPadre & Coma & DesPadre
   Call LoadCtrlForms(TabPadre, Campos, NUL$, Cbof_Padre, ColPadre)
   Cbof_Padre.TextColumn = ColPadre
   FGrdLista.ColWidth(4) = 0
   Lbl_Padre = LblPadre
End Sub

Private Sub Form_Unload(Cancel As Integer)
   Call DestruirControles(arrEventos)
End Sub

Private Sub Tlb_Opciones_ButtonClick(ByVal Button As MSComctlLib.Button)
   Select Case Button.Key
      Case "G": Call Guardar
      Case "B": Call Borrar
      Case "I": Call Listar
      Case "C": Call Cambiar
      Case "E": Unload Me
      Case "A:"
   End Select
End Sub

Private Sub Txt_Codigo_KeyPress(KeyAscii As Integer)
   Call ValKeySinEspeciales(KeyAscii)
End Sub

Private Sub Txt_Codigo_LostFocus()
   Call Buscar
End Sub
Sub Guardar(Optional Cambiar As String)
   ReDim Arr(0)
   DoEvents
   Call MouseClock
   Msglin "Ingresando informaci�n"
   If (Txt_Codigo = NUL$ Or Txt_Desc = NUL$) Then
      Call Mensaje1("Se debe especificar los datos completos!!!...", 3)
      Call MouseNorm: Exit Sub
   End If
   If (Cbof_Padre.ListIndex = -1) Then
      Call Mensaje1("Se debe especificar los datos completos!!!...", 3)
      Call MouseNorm: Exit Sub
      Cbof_Padre.SetFocus
   End If
   
   If (BeginTran(STranIUp & Tabla) <> FAIL) Then
      Campos = CampCodi
      Condicion = CampCodi & "=" & Carcomi & Trim(Txt_Codigo) & Carcomi
      Condicion = Condicion & " AND " & ConPadre
      Condicion = Condicion & " AND " & AutPadre & "=" & Cbof_Padre.List(Cbof_Padre.ListIndex, 0)
      Valores = CampCodi & "=" & Carcomi & IIf(Cambiar = NUL$, Cambiar_Comas_Comillas(Txt_Codigo), Cambiar_Comas_Comillas(Cambiar)) & Carcomi & Coma
      Valores = Valores & CampDesc & "=" & Comi & Cambiar_Comas_Comillas(Txt_Desc) & Comi & Coma
      Valores = Valores & AutHijo & "=" & Cbof_Padre.List(Cbof_Padre.ListIndex, 0)
      Debug.Print Valores
      Result = LoadData(TabPadre & Coma & Tabla, Campos, Condicion, Arr)
      If (Not Encontro) Then
         If Not WarnIns() Then
            Result = FAIL
         Else
            Result = DoInsertSQL(Tabla, Valores)
         End If
      Else
         If Not WarnUpd() Then
            Result = FAIL
         Else
            Condicion = CampCodi & "=" & Carcomi & Trim(Txt_Codigo) & Carcomi
            Condicion = Condicion & " AND " & AutHijo & "=" & Cbof_Padre.List(Cbof_Padre.ListIndex, 0)
            Result = DoUpdate(Tabla, Valores, Condicion)
         End If
      End If
   End If
   If (Result <> FAIL) Then
      If (CommitTran() <> FAIL) Then
          Limpiar (True)
      Else
         Call RollBackTran
      End If
   Else
      Call RollBackTran
   End If
   Call MouseNorm
   Msglin NUL$
End Sub

Sub Borrar()
   ReDim Arr(0)
   DoEvents
   Call MouseClock
   Msglin "Borrando un registro"
   If (Txt_Codigo = NUL$) Then
      Call Mensaje1("Ingrese el c�digo", 1)
      Txt_Codigo.SetFocus
      Call MouseNorm: Msglin NUL$: Exit Sub
   End If
   If Cbof_Padre.ListIndex = -1 Then
      Call Mensaje1("Seleccione un elemento", 1)
      Cbof_Padre.SetFocus
      Call MouseNorm: Msglin NUL$: Exit Sub
   End If
   Campos = CampCodi
   Condicion = CampCodi & "=" & Carcomi & Trim(Txt_Codigo) & Carcomi
   Condicion = Condicion & " AND " & ConPadre
   Condicion = Condicion & " AND " & AutPadre & "=" & Cbof_Padre.List(Cbof_Padre.ListIndex, 0)
   Result = LoadData(TabPadre & Coma & Tabla, Campos, Condicion, Arr)
   If Encontro Then
      If Not WarnDel() Then Call MouseNorm: Exit Sub
      If (BeginTran(STranDel & Tabla) <> FAIL) Then
         Condicion = CampCodi & "=" & Carcomi & Trim(Txt_Codigo) & Carcomi
         Condicion = Condicion & " AND " & AutHijo & "=" & Cbof_Padre.List(Cbof_Padre.ListIndex, 0)
         Result = DoDelete(Tabla, Condicion)
      End If
      'If (Result <> FAIL) Then Result = Auditor(Tabla, TranDel, LastCmd)
   End If
   If (Result <> FAIL) Then
      If (CommitTran() <> FAIL) Then
         Limpiar (True)
      Else
         Call RollBackTran
      End If
   Else
       Call RollBackTran
   End If
   Call MouseNorm
   Msglin NUL$
End Sub
Sub Listar()
   Dim Forma As New FrmViewReport
   Forma.Tabla = Tabla
   Forma.Caption = Me.Informe
   Forma.Listar (CampCodi & Coma & CampDesc)
End Sub
Sub Buscar()
   ReDim Arr(0)
   If (Txt_Codigo) <> NUL$ And Cbof_Padre.ListIndex <> -1 Then
      Campos = CampDesc
      Condicion = CampCodi & "=" & Carcomi & Txt_Codigo & Carcomi
      Condicion = Condicion & " AND " & AutPadre & "=" & Cbof_Padre.List(Cbof_Padre.ListIndex, 0)
      Condicion = Condicion & " AND " & ConPadre
      Result = LoadData(TabPadre & Coma & Tabla, Campos, Condicion, Arr)
      If Encontro Then
         Txt_Desc = Arr(0)
         Tlb_Opciones.Buttons("C").Enabled = True
      Else
         Txt_Desc = NUL$
         Tlb_Opciones.Buttons("C").Enabled = False
      End If
   Else
      Txt_Desc = NUL$
   End If
End Sub
Sub CargarFrm()
   Call GrdFDef(Me.FGrdLista, 0, 0, LblPadre & ",700,Nombre,2000,C�digo,700,Nombre,2000, ,0")
   'Call Leer_Permisos(Me.Tag, SCmd_Options(0), SCmd_Options(1), SCmd_Options(3))
   Condicion = ConPadre & " ORDER BY " & CampDesc
   Result = LoadfGrid(FGrdLista, TabPadre & Coma & Tabla, CodPadre & Coma & DesPadre & Coma & CampCodi & Coma & CampDesc & Coma & AutPadre, Condicion)
   If TipoCodi = "N" Then
      Carcomi = ""
   Else
      Carcomi = Comi
   End If
End Sub
Private Sub Cambiar()
Dim Nuevo As String
   If Tlb_Opciones.Buttons("C").Enabled = False Then Exit Sub
   Nuevo = Cambiar_Codigo(Txt_Codigo.MaxLength, TipoCodi)
   If Nuevo <> NUL$ Then
      Call Guardar(Nuevo)
   End If
End Sub
Private Sub Txt_Desc_KeyPress(KeyAscii As Integer)
   Call ValKeyAlfaNum(KeyAscii)
End Sub
Private Sub Limpiar(ByVal IndFoco As Boolean)
   Call MouseClock
   Txt_Codigo = NUL$
   Txt_Desc = NUL$
   Condicion = ConPadre & " ORDER BY " & CampDesc
   Result = LoadfGrid(FGrdLista, TabPadre & Coma & Tabla, CodPadre & Coma & DesPadre & Coma & CampCodi & Coma & CampDesc & Coma & AutPadre, Condicion)
   Tlb_Opciones.Buttons("C").Enabled = False
   If IndFoco = True Then Txt_Codigo.SetFocus
   Call MouseNorm
End Sub


