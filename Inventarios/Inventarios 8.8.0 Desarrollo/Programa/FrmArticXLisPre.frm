VERSION 5.00
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "MSFLXGRD.OCX"
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Object = "{0BA686C6-F7D3-101A-993E-0000C0EF6F5E}#1.0#0"; "THREED32.OCX"
Begin VB.Form FrmArticXLispre 
   BorderStyle     =   1  'Fixed Single
   ClientHeight    =   5475
   ClientLeft      =   1470
   ClientTop       =   2490
   ClientWidth     =   9945
   Icon            =   "FrmArticXLisPre.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   5475
   ScaleWidth      =   9945
   Begin VB.Frame Frame2 
      Height          =   4335
      Left            =   120
      TabIndex        =   9
      Top             =   1020
      Width           =   9675
      Begin VB.PictureBox PictCboUnidad 
         Height          =   495
         Left            =   7080
         ScaleHeight     =   435
         ScaleWidth      =   2115
         TabIndex        =   13
         Top             =   840
         Visible         =   0   'False
         Width           =   2175
         Begin VB.ComboBox CboUnidad 
            Height          =   315
            Left            =   -50
            Style           =   2  'Dropdown List
            TabIndex        =   14
            Top             =   -60
            Width           =   1935
         End
      End
      Begin VB.OptionButton Opt_Accion 
         Caption         =   "Adicionar nuevos Precios"
         Height          =   375
         Index           =   1
         Left            =   2100
         TabIndex        =   6
         Top             =   240
         Width           =   1635
      End
      Begin VB.OptionButton Opt_Accion 
         Caption         =   "Modificar los precios actuales"
         Height          =   315
         Index           =   0
         Left            =   180
         TabIndex        =   5
         Top             =   240
         Value           =   -1  'True
         Width           =   1935
      End
      Begin MSComCtl2.DTPicker DTPDesde 
         Height          =   255
         Left            =   6660
         TabIndex        =   7
         Top             =   300
         Width           =   1275
         _ExtentX        =   2249
         _ExtentY        =   450
         _Version        =   393216
         Format          =   91619329
         CurrentDate     =   38187
      End
      Begin MSFlexGridLib.MSFlexGrid FGrdPrecios 
         Height          =   3435
         Left            =   120
         TabIndex        =   10
         Top             =   660
         Width           =   9375
         _ExtentX        =   16536
         _ExtentY        =   6059
         _Version        =   393216
         Cols            =   9
         FixedCols       =   0
         BackColorBkg    =   16777215
      End
      Begin Threed.SSCommand CmdSelArtic 
         Height          =   465
         HelpContextID   =   40
         Left            =   8640
         TabIndex        =   8
         ToolTipText     =   "Agregar Art�culos"
         Top             =   180
         Width           =   495
         _Version        =   65536
         _ExtentX        =   873
         _ExtentY        =   820
         _StockProps     =   78
         ForeColor       =   16777215
         Enabled         =   0   'False
         Picture         =   "FrmArticXLisPre.frx":058A
      End
      Begin VB.Label Label2 
         Caption         =   "Fecha de aplicaci�n para nuevos precios"
         Height          =   375
         Left            =   4740
         TabIndex        =   12
         Top             =   180
         Width           =   1875
      End
   End
   Begin VB.Frame Frame1 
      Height          =   855
      Left            =   120
      TabIndex        =   0
      Top             =   120
      Width           =   9675
      Begin VB.CommandButton CmdExaminar 
         Caption         =   "Articulos de la Lista"
         Height          =   435
         Left            =   4620
         TabIndex        =   2
         Top             =   300
         Width           =   2115
      End
      Begin VB.ComboBox Cbo_ListaPrecios 
         Height          =   315
         ItemData        =   "FrmArticXLisPre.frx":0C54
         Left            =   120
         List            =   "FrmArticXLisPre.frx":0C56
         TabIndex        =   1
         Top             =   420
         Width           =   3075
      End
      Begin Threed.SSCommand SSC_Examinar 
         Height          =   585
         HelpContextID   =   40
         Left            =   8880
         TabIndex        =   4
         ToolTipText     =   "Exportar, importar Articulos"
         Top             =   180
         Width           =   615
         _Version        =   65536
         _ExtentX        =   1085
         _ExtentY        =   1032
         _StockProps     =   78
         ForeColor       =   16777215
         AutoSize        =   1
         Picture         =   "FrmArticXLisPre.frx":0C58
      End
      Begin Threed.SSCommand SCmd_Options 
         Height          =   585
         Left            =   8160
         TabIndex        =   3
         ToolTipText     =   "Listar"
         Top             =   180
         Width           =   615
         _Version        =   65536
         _ExtentX        =   1085
         _ExtentY        =   1032
         _StockProps     =   78
         Caption         =   "LISTAR"
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   6
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Picture         =   "FrmArticXLisPre.frx":18AC
      End
      Begin VB.Label Label1 
         Caption         =   "Lista de Precios"
         Height          =   255
         Left            =   120
         TabIndex        =   11
         Top             =   180
         Width           =   3135
      End
   End
End
Attribute VB_Name = "FrmArticXLispre"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Public OpcCod        As String   'Opci�n de seguridad
Dim BandChangeGrid      As Boolean
Private FeCambio        As Date
Dim OrdenGrilla As Byte
Dim CodUnidad() As Variant
'Dim CambiarUnidad  As Boolean      'DEPURACION DE CODIGO

Public Property Let FechaRegistroActual(Fec As Date)
    FeCambio = Fec
End Property

Public Property Get FechaRegistroActual() As Date
    FechaRegistroActual = FeCambio
End Property

Private Sub Cbo_ListaPrecios_Click()
FGrdPrecios.Rows = 1
End Sub

Private Sub CboUnidad_Click()
If CboUnidad.ListIndex = -1 Or FGrdPrecios.Row = 0 Or FGrdPrecios.Col <> 7 Then Exit Sub

If Val(FGrdPrecios.TextMatrix(FGrdPrecios.Row, 6)) <> Val(CodUnidad(CboUnidad.ListIndex)) Then BandChangeGrid = True

End Sub

Private Sub CboUnidad_LostFocus()
   'If CambiarUnidad = True Then Call AlmacenarUnidad 'CA-02/06:
   If FGrdPrecios.TextMatrix(1, 0) <> NUL$ Then 'JAUM T39214
      If BandChangeGrid = True Then Call Guardar
   End If
   PictCboUnidad.Visible = False
   CboUnidad.ListIndex = -1
End Sub


Private Sub CmdExaminar_Click()
'BandLoadGrid = True
If Cbo_ListaPrecios.ListIndex <> -1 Then
   Campos = "'E',NU_AUTO_ARTI,CD_CODI_ARTI,NO_NOMB_ARTI,FE_DESDE_RLIA,NU_PRECIO_RLIA,NU_AUTO_UNVE,TX_NOMB_UNVE"
   Desde = "IN_R_LIST_ARTI,ARTICULO,IN_UNDVENTA"
   Condi = "NU_AUTO_ARTI=NU_AUTO_ARTI_RLIA"
   Condi = Condi & " AND NU_AUTO_LIPR_RLIA=" & Cbo_ListaPrecios.ItemData(Cbo_ListaPrecios.ListIndex)
   Condi = Condi & " AND NU_AUTO_UNVE = NU_AUTO_UNVE_RLIA ORDER BY NO_NOMB_ARTI,FE_DESDE_RLIA DESC"
   Result = LoadfGrid(FGrdPrecios, Desde, Campos, Condi)
   FGrdPrecios.MergeCells = flexMergeRestrictColumns
   FGrdPrecios.MergeCol(2) = True
   FGrdPrecios.MergeCol(3) = True
   FGrdPrecios.MergeCol(4) = True
End If
'BandLoadGrid = False
End Sub

Private Sub CmdSelArtic_Click()
If Cbo_ListaPrecios.ListIndex <> -1 Then
    'select * from ARTICULO, TC_IMPUESTOS where CD_IMPU_ARTI_TCIM=CD_CODI_IMPU
   Desde = "ARTICULO, TC_IMPUESTOS,IN_UNDVENTA"
   Condi = "CD_IMPU_ARTI_TCIM=CD_CODI_IMPU AND NU_AUTO_UNVE_ARTI=NU_AUTO_UNVE"
    Seleccion3 Desde, "'', NO_NOMB_ARTI, CD_CODI_ARTI, VL_COPR_ARTI, TX_NOMB_UNVE, DE_CTRA_ARTI, NU_AUTO_ARTI, '', PR_PORC_IMPU, VL_COPR_ARTI, PR_PORC_IMPU", "Selecci�n de Art�culos", Condi, "", False, ""
   Agregar_Arti
End If
End Sub

Private Sub FGrdPrecios_Click()
   If FGrdPrecios.Rows > 1 Then 'OATC T36712
      If FGrdPrecios.MouseRow = 0 And FGrdPrecios.MouseCol <= 3 Then
         If OrdenGrilla = 1 Then
            FGrdPrecios.Sort = 2
            OrdenGrilla = 2
         Else
            FGrdPrecios.Sort = 1
            OrdenGrilla = 1
         End If
      Else
         If FGrdPrecios.MouseRow > 0 And FGrdPrecios.MouseCol = 7 Then     'CA
            FGrdPrecios.Row = FGrdPrecios.MouseRow
            FGrdPrecios.Col = FGrdPrecios.MouseCol
            Call MostrarUnidades(FGrdPrecios)
         End If
      End If
   End If 'OATC T36712
End Sub
'
'Private Sub FGrdPrecios_DblClick()
'    Debug.Print Me.FGrdPrecios.Row, Me.FGrdPrecios.Col
'    Me.FGrdPrecios.Sort = flexSortGenericAscending
'End Sub

Private Sub FGrdPrecios_KeyPress(KeyAscii As Integer)
'Dim RowAct As Integer      'DEPURACION DE CODIGO
If FGrdPrecios.Col = 4 Then
    If KeyAscii = vbKeyBack Then
'    FGrdPrecios.TextMatrix(FGrdPrecios.Row, 4) = DTPDesde.Value
        If IsDate(FGrdPrecios.TextMatrix(FGrdPrecios.Row, 4)) Then
            Me.FechaRegistroActual = CDate(FGrdPrecios.TextMatrix(FGrdPrecios.Row, 4))
            FGrdPrecios.TextMatrix(FGrdPrecios.Row, 4) = Format(Me.DTPDesde.Value, "DD/MM/YYYY")
            BandChangeGrid = True
        End If
    End If
ElseIf FGrdPrecios.Col = 5 Then
'    If IsDate(FGrdPrecios.TextMatrix(FGrdPrecios.Row, 4)) Then
'        DTPDesde.Value = FGrdPrecios.TextMatrix(FGrdPrecios.Row, 4)
'    Else
'        FGrdPrecios.TextMatrix(FGrdPrecios.Row, 4) = DTPDesde.Value
'    End If
    Call Escribir_Fgrid(FGrdPrecios, FGrdPrecios.Col, KeyAscii, 9, 4, False, Me)
    If FGrdPrecios.TextMatrix(FGrdPrecios.Row, 0) = "N" Then FGrdPrecios.TextMatrix(FGrdPrecios.Row, 4) = Format(DTPDesde.Value, "DD/MM/YYYY")
    BandChangeGrid = True
    'smdl m1599
ElseIf FGrdPrecios.Col = 3 Then
    For i = 1 To FGrdPrecios.Rows - 1
          If UCase(Mid(FGrdPrecios.TextMatrix(i, FGrdPrecios.Col), 1, 1)) = UCase(Chr(KeyAscii)) Then
               FGrdPrecios.ColSel = 3
               
               If FGrdPrecios.Rows < i + 1 Then FGrdPrecios.TopRow = i + 1 Else FGrdPrecios.TopRow = i
               Exit Sub
          End If
          FGrdPrecios.Row = i 'smdl n1599
    Next i
   'smdl m1599

End If

End Sub

Private Sub FGrdPrecios_LeaveCell()
'Dim RowAct As Integer      'DEPURACION DE CODIGO

'If BandChangeGrid = True Then
'   If FGrdPrecios.Col = 4 Then
'        If FGrdPrecios.TextMatrix(FGrdPrecios.Row, 0) = "E" And IsDate(FGrdPrecios.TextMatrix(FGrdPrecios.Row, 4)) Then
'            Condi = "NU_AUTO_ARTI_RLIA=" & FGrdPrecios.TextMatrix(FGrdPrecios.Row, 1)
'            Condi = Condi & " AND NU_AUTO_LIPR_RLIA=" & Cbo_ListaPrecios.ItemData(Cbo_ListaPrecios.ListIndex)
'            Condi = Condi & " AND FE_DESDE_RLIA=" & FFechaCon(Me.FechaRegistroActual)
'            Valores = "FE_DESDE_RLIA=" & FFechaCon(CDate(FGrdPrecios.TextMatrix(FGrdPrecios.Row, 4)))
'            Result = DoUpdate("IN_R_LIST_ARTI", Valores, Condi)
'        End If
'   ElseIf FGrdPrecios.Col = 5 Then
'       RowAct = FGrdPrecios.Row
'       If FGrdPrecios.TextMatrix(RowAct, 5) = "" Then FGrdPrecios.TextMatrix(RowAct, 5) = "0"
'    '   If (FGrdPrecios.TextMatrix(RowAct, 0) = "N") Or (FGrdPrecios.TextMatrix(RowAct, 0) = "E" And Opt_Accion(1).Value) Then
'       If (FGrdPrecios.TextMatrix(RowAct, 0) = "N" And Opt_Accion(1).Value) Then
'          Valores = "NU_AUTO_ARTI_RLIA=" & FGrdPrecios.TextMatrix(RowAct, 1) & Coma
'          Valores = Valores & "NU_AUTO_LIPR_RLIA=" & Cbo_ListaPrecios.ItemData(Cbo_ListaPrecios.ListIndex) & Coma
'          Valores = Valores & "FE_DESDE_RLIA=" & FFechaIns(FGrdPrecios.TextMatrix(RowAct, 4)) & Coma
'          Valores = Valores & "NU_PRECIO_RLIA=" & FGrdPrecios.TextMatrix(RowAct, 5)
'          Result = DoInsertSQL("IN_R_LIST_ARTI", Valores)
'          If Result <> FAIL Then FGrdPrecios.TextMatrix(RowAct, 0) = "E"
'       End If
'       If FGrdPrecios.TextMatrix(RowAct, 0) = "E" And Opt_Accion(0).Value Then
'          Condi = "NU_AUTO_ARTI_RLIA=" & FGrdPrecios.TextMatrix(RowAct, 1)
'          Condi = Condi & " AND NU_AUTO_LIPR_RLIA=" & Cbo_ListaPrecios.ItemData(Cbo_ListaPrecios.ListIndex)
'          Condi = Condi & " AND FE_DESDE_RLIA=" & FFechaCon(CDate(FGrdPrecios.TextMatrix(RowAct, 4)))
'          Valores = "NU_PRECIO_RLIA=" & FGrdPrecios.TextMatrix(RowAct, 5)
'          Result = DoUpdate("IN_R_LIST_ARTI", Valores, Condi)
'       End If
'   End If
'End If

'CA-02/06
'If CambiarUnidad = True Then Call AlmacenarUnidad
   If FGrdPrecios.TextMatrix(1, 0) <> NUL$ Then 'JAUM T39214
      Call Guardar
   End If 'JAUM T39214
      If FGrdPrecios.Visible Then FGrdPrecios.SetFocus
      BandChangeGrid = False
      PictCboUnidad.Visible = False
      CboUnidad.ListIndex = -1
End Sub

Private Sub FGrdPrecios_Scroll()

   If BandChangeGrid = True Then Call Guardar
   
   FGrdPrecios.SetFocus
   PictCboUnidad.Visible = False
   CboUnidad.ListIndex = -1
End Sub

Private Sub Form_Load()
Call CenterForm(MDI_Inventarios, Me)
'BandLoadGrid = True
Call GrdFDef(FGrdPrecios, 0, 0, "tipo,1,Autarti,1,Codigo,1500,Nombre,3000,Fecha Desde,1500,Valor,1000,NumUnidad,1,Unidad Costo,2500")
'BandLoadGrid = False
DTPDesde.Value = FechaServer
'Result = LNC("IN_LISTAPRECIO", "NU_AUTO_LIPR", "TX_NOMB_LIPR", Cbo_ListaPrecios, NUL$)
Result = LNC("IN_LISTAPRECIO order by 2", "NU_AUTO_LIPR", "TX_NOMB_LIPR", Cbo_ListaPrecios, NUL$) 'JACC R2353

Result = loadctrl("IN_UNDVENTA", "TX_NOMB_UNVE", "NU_AUTO_UNVE", CboUnidad, CodUnidad, NUL$)   'CA-02/06
End Sub

Private Sub Agregar_Arti()
Dim i As Long
Dim ArrTemp() As Variant
Dim Articulo As New ElArticulo
Dim NumUnidad As Long
Dim NombUnidad As String

'FGrdPrecios.Rows = 1
i = 1
For i = 1 To UBound(Mselec2, 1)
   If Mselec2(i, 1) <> "" Then
      ReDim ArrTemp(1)
      Condi = "NU_AUTO_UNVE = NU_AUTO_UNVE_RLIA AND NU_AUTO_ARTI_RLIA=" & Mselec2(i, 1)
      Condi = Condi & " AND NU_AUTO_LIPR_RLIA=" & Cbo_ListaPrecios.ItemData(Cbo_ListaPrecios.ListIndex)
      Condi = Condi & " ORDER BY FE_DESDE_RLIA DESC"
      Result = LoadData("IN_R_LIST_ARTI,IN_UNDVENTA", "NU_AUTO_UNVE,TX_NOMB_UNVE", Condi, ArrTemp())
      If Result <> FAIL Then
         If Encontro Then
            NumUnidad = CLng(ArrTemp(0)): NombUnidad = ArrTemp(1)
         Else
            ReDim ArrTemp(1)
            Result = Articulo.IniciaXCodigo(Mselec2(i, 3))
            If Result <> FAIL Then Result = LoadData("IN_UNDVENTA", "NU_AUTO_UNVE,TX_NOMB_UNVE", "NU_AUTO_UNVE=" & Articulo.UnidadConteo, ArrTemp())
            If Result <> FAIL Then
               If Encontro Then
                  NumUnidad = CLng(ArrTemp(0)): NombUnidad = ArrTemp(1)
               Else
                  Call Mensaje1("El art�culo " & Mselec2(i, 2) & " no tiene Unidad de Conteo.", 3): Result = FAIL
               End If
            End If
         End If
      End If
      
      If Result <> FAIL Then
         Valores = "N" & vbTab & Mselec2(i, 1) & vbTab & Mselec2(i, 3) & vbTab & Mselec2(i, 2) & vbTab & vbTab & vbTab & NumUnidad & vbTab & NombUnidad
         FGrdPrecios.AddItem Valores
      End If
      Debug.Print FGrdPrecios.Rows
   Else
      Exit For
   End If
Next
End Sub

Private Sub Opt_Accion_Click(Index As Integer)
    Select Case Index
    Case Is = 0
        Me.CmdSelArtic.Enabled = False
    Case Is = 1
        Me.CmdSelArtic.Enabled = True
    End Select
End Sub



Private Sub SCmd_Options_Click() 'SMDL R. 1003, 1484
If Cbo_ListaPrecios.ListIndex = -1 Then
   Call Mensaje1("Seleccione una lista de precios", 3)
Else
   Condi = "{IN_R_LIST_ARTI.NU_AUTO_LIPR_RLIA} = " & Cbo_ListaPrecios.ItemData(Cbo_ListaPrecios.ListIndex)
   Call ListarD("InfListaPrecio.rpt", Condi, 0, "LISTA DE PRECIOS", MDI_Inventarios, False)
End If
'SMDL R. 1003, 1484
End Sub

Private Sub SSC_Examinar_Click()
FrmPlanosPrecios.Show 1
End Sub

Sub MostrarUnidades(Grid As MSFlexGrid)
With Grid
    If .Row > 0 And .Col = 7 Then    'CA
       PictCboUnidad.Visible = False
       PictCboUnidad.Left = .CellLeft + .Left
       PictCboUnidad.Top = .CellTop + .Top
       PictCboUnidad.Height = .CellHeight + 40
       PictCboUnidad.Width = .CellWidth + 40
       CboUnidad.Width = PictCboUnidad.Width
       CboUnidad.ListIndex = FindInArr(CodUnidad, .TextMatrix(.Row, 6))
       PictCboUnidad.ZOrder (0)
       PictCboUnidad.Visible = True
       CboUnidad.SetFocus
    End If
End With
End Sub

Sub Guardar()
   Dim Exito As Boolean

   Exito = False

   With FGrdPrecios

   If BandChangeGrid = True Then 'OATC T36710 Se deja linea en comentarioo 'JAUM T39214 Se quita comentario en linea
   'If BandChangeGrid = True And FGrdPrecios.TextMatrix(.Row, 2) <> "" And CboUnidad.ListIndex <> -1 Then 'OATC T36710 'JAUM T39214 Se deja linea en comentario
      If Trim(.TextMatrix(.Row, 5)) = "" Then FGrdPrecios.TextMatrix(.Row, 5) = "0"
      If IsDate(.TextMatrix(.Row, 4)) = False Then .TextMatrix(.Row, 4) = Format(DTPDesde.Value, "DD/MM/YYYY")

      Condi = "NU_AUTO_ARTI_RLIA=" & .TextMatrix(.Row, 1)
      Condi = Condi & " AND NU_AUTO_LIPR_RLIA=" & Cbo_ListaPrecios.ItemData(Cbo_ListaPrecios.ListIndex)
   
      Valores = "NU_AUTO_ARTI_RLIA=" & .TextMatrix(.Row, 1) & Coma
      Valores = Valores & "NU_AUTO_LIPR_RLIA=" & Cbo_ListaPrecios.ItemData(Cbo_ListaPrecios.ListIndex) & Coma
      Valores = Valores & "FE_DESDE_RLIA=" & FFechaIns(.TextMatrix(.Row, 4)) & Coma
      If .Col = 7 Then
         Valores = Valores & "NU_AUTO_UNVE_RLIA=" & CLng(CodUnidad(CboUnidad.ListIndex))
      Else
      Valores = Valores & "NU_AUTO_UNVE_RLIA=" & CLng(.TextMatrix(.Row, 6))
      End If
      If .TextMatrix(.Row, 0) = "E" Then
         If Opt_Accion(0).Value Then Valores = Valores & Coma & "NU_PRECIO_RLIA=" & .TextMatrix(.Row, 5)
      Else
         Valores = Valores & Coma & "NU_PRECIO_RLIA=" & .TextMatrix(.Row, 5)
      End If
   
         
      If .TextMatrix(.Row, 0) = "E" Then
         If .Col = 4 Then  'Si hubo cambio de fecha
            If DateDiff("d", Format(Me.FechaRegistroActual, "DD/MM/YYYY"), Format(.TextMatrix(.Row, 4), "DD/MM/YYYY")) <> 0 Then
               ReDim Arr(0)
               Condi = Condi & " AND FE_DESDE_RLIA=" & FFechaCon(CDate(.TextMatrix(.Row, 4)))
               Result = LoadData("IN_R_LIST_ARTI", "NU_AUTO_ARTI_RLIA", Condi, Arr())
                  If Result <> FAIL And Encontro Then
                     Call Mensaje1("Este registro no se puede modificar porque ya existe otro con la misma fecha.", 1)
                  ElseIf Result <> FAIL And Not Encontro Then
                     Condi = Condi & " AND FE_DESDE_RLIA=" & FFechaCon(Me.FechaRegistroActual)
              
                     Result = DoUpdate("IN_R_LIST_ARTI", Valores, Condi)   'Cambia la fecha del registro actual
'                     Result = DoInsertSQL("IN_R_LIST_ARTI", Valores)  'Agrega un nuevo registro
                     If Result <> FAIL Then Exito = True
               
                  End If
               End If
         Else
         
         'HRR R1624-1859
'         Condi = Condi & " AND FE_DESDE_RLIA=" & FFechaCon(CDate(.TextMatrix(.Row, 4)))
'         Result = DoUpdate("IN_R_LIST_ARTI", Valores, Condi)
'         If Result <> FAIL Then Exito = True
         'HRR R1624-1859
         
         'HRR R1624-1859
            If .Col = 5 Then 'Si cambio precio
            
               If Cambiar_Precio(CDbl(.TextMatrix(.Row, 1)), CDbl(.TextMatrix(.Row, 5))) Then
                  Condi = Condi & " AND FE_DESDE_RLIA=" & FFechaCon(CDate(.TextMatrix(.Row, 4)))
                  Result = DoUpdate("IN_R_LIST_ARTI", Valores, Condi)
                  If Result <> FAIL Then Exito = True
               End If
            
            Else
               Condi = Condi & " AND FE_DESDE_RLIA=" & FFechaCon(CDate(.TextMatrix(.Row, 4)))
               Result = DoUpdate("IN_R_LIST_ARTI", Valores, Condi)
               If Result <> FAIL Then Exito = True
            End If
            'HRR R1624-1859
         
         End If
   ElseIf .TextMatrix(.Row, 0) = "N" Then
      ReDim Arr(0)
      Condi = Condi & " AND FE_DESDE_RLIA=" & FFechaCon(CDate(.TextMatrix(.Row, 4)))
      Result = LoadData("IN_R_LIST_ARTI", "NU_AUTO_ARTI_RLIA", Condi, Arr())
      If Result <> FAIL And Encontro Then
         Call Mensaje1("Este registro no se puede insertar porque ya existe otro con la misma fecha.", 1)
         Result = FAIL
      ElseIf Result <> FAIL And Not Encontro Then
         
         'HRR R1624-1859
'         Result = DoInsertSQL("IN_R_LIST_ARTI", Valores)
'         If Result <> FAIL Then Exito = True
         'HRR R1624-1859
         
         'HRR R1624-1859
            If Cambiar_Precio(CDbl(.TextMatrix(.Row, 1)), CDbl(.TextMatrix(.Row, 5))) Then
               Result = DoInsertSQL("IN_R_LIST_ARTI", Valores)
               If Result <> FAIL Then Exito = True
            End If
            'HRR R1624-1859
         
         End If
      End If
   
      If Exito = True Then
         .TextMatrix(.Row, 0) = "E"
         If .Col = 7 Then
            .TextMatrix(.Row, 6) = CodUnidad(CboUnidad.ListIndex)
            .TextMatrix(.Row, 7) = CboUnidad.List(CboUnidad.ListIndex)
         End If
      End If
   
   End If

   End With
    
    
End Sub

'Sub AlmacenarUnidad()
'
'With FGrdPrecios
'    If .Col = 7 And .Row > 0 And CboUnidad.ListIndex <> -1 Then   'CA-02/06
'       If Val(.TextMatrix(.Row, 6)) <> Val(CodUnidad(CboUnidad.ListIndex)) Then
'          If .TextMatrix(.Row, 0) = "E" Then
'             Condi = "NU_AUTO_ARTI_RLIA=" & .TextMatrix(.Row, 1)
'             Condi = Condi & " AND NU_AUTO_LIPR_RLIA=" & Cbo_ListaPrecios.ItemData(Cbo_ListaPrecios.ListIndex)
'             Condi = Condi & " AND FE_DESDE_RLIA=" & FFechaCon(CDate(.TextMatrix(.Row, 4)))
'             Valores = "NU_AUTO_UNVE_RLIA=" & CLng(CodUnidad(CboUnidad.ListIndex))
'             Result = DoUpdate("IN_R_LIST_ARTI", Valores, Condi)
'          Else
'             Valores = "NU_AUTO_ARTI_RLIA=" & CLng(.TextMatrix(.Row, 1)) & Coma
'             Valores = Valores & "NU_AUTO_LIPR_RLIA=" & Cbo_ListaPrecios.ItemData(Cbo_ListaPrecios.ListIndex) & Coma
'             Valores = Valores & "FE_DESDE_RLIA=" & FFechaIns(.TextMatrix(.Row, 4)) & Coma
'             If IsNumeric(.TextMatrix(.Row, 5)) = False Then .TextMatrix(.Row, 5) = 0
'             Valores = Valores & "NU_PRECIO_RLIA=" & .TextMatrix(.Row, 5) & Coma
'             Valores = Valores & "NU_AUTO_UNVE_RLIA=" & CLng(CodUnidad(CboUnidad.ListIndex))
'             Result = DoInsertSQL("IN_R_LIST_ARTI", Valores)
'             If Result <> FAIL Then .TextMatrix(.Row, 0) = "E"
'          End If
'          If Result <> FAIL Then
'             .TextMatrix(.Row, 6) = CodUnidad(CboUnidad.ListIndex)
'             .TextMatrix(.Row, 7) = CboUnidad.List(CboUnidad.ListIndex)
'          End If
'       End If
'    End If
'End With
'CambiarUnidad = False
'
'End Sub

'HRR R1624-1859
Private Function Cambiar_Precio(ByVal DbArti As Double, ByVal DbPrecio As Double) As Boolean

   Cambiar_Precio = True
   ReDim Arr(0)
   Condicion = "NU_AUTO_ARTI=" & DbArti
   Result = LoadData("ARTICULO", "VL_COPR_ARTI", Condicion, Arr)
   If Result <> FAIL And Encontro Then
      If Arr(0) <> NUL$ And IsNumeric(Arr(0)) Then
         Arr(0) = Aplicacion.Formatear_Valor(CDbl(Arr(0))) 'HRR M3136
         If DbPrecio < CDbl(Arr(0)) Then
            If MsgBox("El valor es menor al costo promedio del art�culo." & vbCrLf _
                     & "Costo promedio : " & Arr(0) & vbCrLf & vbCrLf _
                     & "Desea continuar?", vbYesNo + vbQuestion) = vbNo Then
                     Condicion = "NU_AUTO_ARTI_RLIA=" & DbArti
                     Condicion = Condicion & " AND NU_AUTO_LIPR_RLIA=" & Cbo_ListaPrecios.ItemData(Cbo_ListaPrecios.ListIndex)
                     Condicion = Condicion & " AND FE_DESDE_RLIA=" & FFechaCon(FGrdPrecios.TextMatrix(FGrdPrecios.Row, 4))
                     Result = LoadData("IN_R_LIST_ARTI", "NU_PRECIO_RLIA", Condicion, Arr)
                     If Result <> FAIL And Encontro Then
                        If IsNumeric(Arr(0)) Then FGrdPrecios.TextMatrix(FGrdPrecios.Row, 5) = Arr(0)
                     End If
                     Cambiar_Precio = False
            End If
         End If
      End If
   End If
   
End Function
'HRR R1624-1859
