VERSION 5.00
Object = "{00025600-0000-0000-C000-000000000046}#5.2#0"; "CRYSTL32.OCX"
Object = "{0BA686C6-F7D3-101A-993E-0000C0EF6F5E}#1.0#0"; "THREED32.OCX"
Begin VB.Form FrmLdegr 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Existencias en Dependencias por Grupo"
   ClientHeight    =   4485
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   4605
   Icon            =   "FrmLdegr.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   ScaleHeight     =   4485
   ScaleWidth      =   4605
   Begin VB.Frame FraDependencias 
      Caption         =   "Rango de Dependencias"
      Height          =   1335
      Left            =   240
      TabIndex        =   3
      Top             =   840
      Width           =   4095
      Begin VB.TextBox TxtRango 
         Height          =   285
         Index           =   1
         Left            =   720
         MaxLength       =   30
         TabIndex        =   5
         Text            =   "ZZZZZZZZZZZ"
         Top             =   840
         Width           =   3255
      End
      Begin VB.TextBox TxtRango 
         Height          =   285
         Index           =   0
         Left            =   720
         MaxLength       =   30
         TabIndex        =   4
         Text            =   "0"
         Top             =   360
         Width           =   3255
      End
      Begin VB.Label LblInicio 
         AutoSize        =   -1  'True
         Caption         =   "Inicial : "
         Height          =   195
         Index           =   1
         Left            =   120
         TabIndex        =   15
         Top             =   360
         Width           =   540
      End
      Begin VB.Label LblHasta 
         AutoSize        =   -1  'True
         Caption         =   "Final : "
         Height          =   195
         Index           =   1
         Left            =   120
         TabIndex        =   14
         Top             =   840
         Width           =   465
      End
   End
   Begin VB.Frame FraArticulos 
      Caption         =   "Rango de Grupos de Art�culos "
      Height          =   1335
      Left            =   240
      TabIndex        =   6
      Top             =   2280
      Width           =   4095
      Begin VB.TextBox TxtRango 
         Height          =   285
         Index           =   2
         Left            =   720
         MaxLength       =   50
         TabIndex        =   7
         Text            =   "0"
         Top             =   360
         Width           =   3255
      End
      Begin VB.TextBox TxtRango 
         Height          =   285
         Index           =   3
         Left            =   720
         MaxLength       =   50
         TabIndex        =   8
         Text            =   "ZZZZZZZZZZZZZZZZ"
         Top             =   840
         Width           =   3255
      End
      Begin VB.Label LblHasta 
         AutoSize        =   -1  'True
         Caption         =   "Final : "
         Height          =   195
         Index           =   0
         Left            =   120
         TabIndex        =   10
         Top             =   840
         Width           =   465
      End
      Begin VB.Label LblInicio 
         AutoSize        =   -1  'True
         Caption         =   "Inicial : "
         Height          =   195
         Index           =   0
         Left            =   120
         TabIndex        =   9
         Top             =   360
         Width           =   540
      End
   End
   Begin VB.Frame FraOrden 
      Caption         =   "Ordenar por"
      Height          =   735
      Left            =   240
      TabIndex        =   0
      Top             =   0
      Width           =   4095
      Begin VB.OptionButton OptOrden 
         BackColor       =   &H00C0C0C0&
         Caption         =   "C�digo"
         Height          =   255
         Index           =   0
         Left            =   720
         TabIndex        =   1
         Top             =   360
         Value           =   -1  'True
         Width           =   855
      End
      Begin VB.OptionButton OptOrden 
         BackColor       =   &H00C0C0C0&
         Caption         =   "Nombre"
         Height          =   255
         Index           =   1
         Left            =   2400
         TabIndex        =   2
         Top             =   360
         Width           =   855
      End
   End
   Begin Crystal.CrystalReport Crys_Listar 
      Left            =   2640
      Top             =   4560
      _ExtentX        =   741
      _ExtentY        =   741
      _Version        =   348160
      WindowLeft      =   20
      WindowWidth     =   750
      WindowHeight    =   450
      WindowTitle     =   "Existencias en Dependencias"
      WindowControlBox=   -1  'True
      WindowMaxButton =   -1  'True
      WindowMinButton =   -1  'True
      WindowControls  =   -1  'True
      PrintFileLinesPerPage=   60
      WindowShowCloseBtn=   -1  'True
      WindowShowSearchBtn=   -1  'True
      WindowShowPrintSetupBtn=   -1  'True
      WindowShowRefreshBtn=   -1  'True
   End
   Begin Threed.SSCommand SCmd_Options 
      Height          =   375
      Index           =   3
      Left            =   0
      TabIndex        =   11
      Top             =   4560
      Visible         =   0   'False
      Width           =   975
      _Version        =   65536
      _ExtentX        =   1720
      _ExtentY        =   661
      _StockProps     =   78
   End
   Begin Threed.SSCommand SCmd_Options 
      Height          =   375
      Index           =   4
      Left            =   1200
      TabIndex        =   12
      Top             =   4560
      Visible         =   0   'False
      Width           =   975
      _Version        =   65536
      _ExtentX        =   1720
      _ExtentY        =   661
      _StockProps     =   78
   End
   Begin Threed.SSCommand SCmd_Options 
      Height          =   735
      Index           =   1
      Left            =   1920
      TabIndex        =   16
      Top             =   3720
      Width           =   855
      _Version        =   65536
      _ExtentX        =   1508
      _ExtentY        =   1296
      _StockProps     =   78
      Caption         =   "&IMPRIMIR"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   6
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Picture         =   "FrmLdegr.frx":058A
   End
   Begin Threed.SSCommand SCmd_Options 
      Height          =   735
      Index           =   2
      Left            =   3000
      TabIndex        =   17
      Top             =   3720
      Width           =   855
      _Version        =   65536
      _ExtentX        =   1508
      _ExtentY        =   1296
      _StockProps     =   78
      Caption         =   "&SALIR"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   6
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Picture         =   "FrmLdegr.frx":0C54
   End
   Begin Threed.SSCommand SCmd_Options 
      Height          =   735
      Index           =   0
      Left            =   840
      TabIndex        =   18
      Top             =   3720
      Width           =   855
      _Version        =   65536
      _ExtentX        =   1508
      _ExtentY        =   1296
      _StockProps     =   78
      Caption         =   "&PANTALLA"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   6
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Picture         =   "FrmLdegr.frx":131E
   End
   Begin VB.Label tipo 
      Height          =   135
      Left            =   840
      TabIndex        =   13
      Top             =   3360
      Visible         =   0   'False
      Width           =   975
   End
End
Attribute VB_Name = "FrmLdegr"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Public OpcCod        As String   'Opci�n de seguridad

Private Sub Form_Load()
    Dim ElClnt As New ElTercero
    ElClnt.IniXNit
 Call CenterForm(MDI_Inventarios, Me)
' Call Leer_Permisos("03003", SCmd_Options(3), SCmd_Options(3), SCmd_Options(4))
 Crys_Listar.Formulas(0) = "ENTIDAD='" & ElClnt.Nombre & Comi
 Crys_Listar.Formulas(1) = "NIT= 'NIT. " & ElClnt.Nit & Comi
 Crys_Listar.Formulas(2) = "HORA= 'Hora: " & Format(Now, "hh:mm") & Comi
 Crys_Listar.Formulas(3) = "USUARIO = 'Usuario:" & UserId & Comi
' Crys_Listar.Connect = conCrys
End Sub
Private Sub OptOrden_KeyPress(Index As Integer, KeyAscii As Integer)
  Call Cambiar_Enter(KeyAscii)
End Sub
Private Sub TxtRango_GotFocus(Index As Integer)
    TxtRango(Index).SelStart = 0
    TxtRango(Index).SelLength = Len(TxtRango(Index).Text)
End Sub
Private Sub TxtRango_KeyPress(Index As Integer, KeyAscii As Integer)
    Call ValKeyAlfaNum(KeyAscii)
    If KeyAscii = 13 Then
     If Index < 3 Then
        TxtRango(Index + 1).SetFocus
     Else
        SCmd_Options(0).SetFocus
     End If
    End If
End Sub
Private Sub TxtRango_KeyDown(Index As Integer, KeyCode As Integer, Shift As Integer)
 Select Case Index
    Case 0
         If KeyCode = 40 Then
            TxtRango(1).SetFocus
         End If
    Case 1, 2
         If KeyCode = 38 Then
            TxtRango(Index - 1).SetFocus
         End If
         If KeyCode = 40 Then
            TxtRango(Index + 1).SetFocus
         End If
    Case 3
         If KeyCode = 38 Then
            TxtRango(2).SetFocus
         End If
         If KeyCode = 40 Then
            SCmd_Options(0).SetFocus
         End If
 End Select
End Sub
Private Sub SCmd_Options_Click(Index As Integer)
    Screen.MousePointer = 11
    Select Case Index
        '''True = pantalla
        '''False = impresora
        Case 0: Call Informe(True)
        Case 1: Call Informe(False)
        Case 2: Unload Me
    End Select
    Screen.MousePointer = 0
End Sub
Private Sub Informe(ByVal pantalla As Boolean)
  If TxtRango(0) > TxtRango(1) Or TxtRango(1) = "" Then
     Call Mensaje1("Revise rango de dependencias", 3)
     Exit Sub
  End If
  If TxtRango(2) > TxtRango(3) Or TxtRango(3) = "" Then
     Call Mensaje1("Revise rango de grupo de art�culos", 3)
     Exit Sub
  End If
  Crys_Listar.SelectionFormula = ""
  Crys_Listar.ReportFileName = DirTrab + "lexidegr.RPT"
  If OptOrden(1) = True Then
     Crys_Listar.SortFields(0) = "+{centro_costo.no_nomb_ceco}"
     Crys_Listar.SelectionFormula = "{centro_costo.no_nomb_ceco} in '" & TxtRango(0) & "' to '" & TxtRango(1) & "Z'" & _
     " and {grup_articulo.de_desc_grup} in '" & TxtRango(2) & "' to '" & TxtRango(3) & "Z'"
  Else
     Crys_Listar.SortFields(0) = "+{centro_costo.cd_codi_ceco}"
     Crys_Listar.SelectionFormula = "{centro_costo.cd_codi_ceco} in '" & TxtRango(0) & "' to '" & TxtRango(1) & Comi & _
     " and {grup_articulo.cd_codi_grup} in '" & TxtRango(2) & "' to '" & TxtRango(3) & Comi
  End If
  If SCmd_Options(4).Enabled = False Then
     Call Mensaje1("Permiso Denegado", 3)
  Else
  On Error GoTo control
     If pantalla = True Then
        Crys_Listar.Destination = crptToWindow
     Else
        Crys_Listar.Destination = crptToPrinter
     End If
     Crys_Listar.Action = 1
  End If
  Debug.Print Crys_Listar.SelectionFormula
  Exit Sub
control:
  Call Mensaje1(ERR.Number & ": " & ERR.Description & "  Nombre del Reporte : lexidegr.RPT", 1)
End Sub







