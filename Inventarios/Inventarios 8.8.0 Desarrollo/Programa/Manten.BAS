Attribute VB_Name = "Mantenimiento"
Option Explicit

Global Const SupUsr = "ADMINISTRADOR"

Function Auditor(ByVal TableName As Variant, ByVal Trans As Integer, ByVal Desc As Variant) As Integer
'   Dim Result As Integer
'   Dim S As String
'   Dim P As Integer
'   S = FFECHAHORA(Format(Nowserver, "dd/mm/yyyy hh:mm:ss")) & Coma
'   S = Replace(S, Asterisco, Coma)
'   S = S & Comi & UCase(UserId) & Comi & Coma
'   S = S & Comi & TableName & Comi & Coma
'   S = S & Comi & Trans & Comi & Coma
'   S = S & Comi & Cambiar_Comas_Comillas(CStr(Desc)) & Comi & Coma
'   S = S & NumConex
'   Debug.Print S
'   Result = ExecSQLCommand("INSERT INTO " & NombTabAud & " VALUES (" & S & ")")
'   If (Result = FAIL) Then Call Mensaje1("No se pudo hacer el registro de auditor�a", 1)
'   Auditor = Result
   Dim Result As Integer
   Dim S As String
   'JAGS T7783
        Dim stFecha As String
        Dim stVersion As String
   'JAGS T7783
   'JAGS T7783
    If ModEjecucion = "VB" Then
    
        stFecha = Format(FileDateTime(App.Path & "\" & App.EXEName & ".vbp"), "YYYY/MM/DD" & " " & "hh:mm am/pm") 'JAGS T8009
        'stVersion = "Ver." & App.Major & "." & App.Minor & "." & App.Revision
        stVersion = App.Major & "." & App.Minor & "." & App.Revision 'JAGS T8210
        Else
            stFecha = Format(FileDateTime(App.Path & "\" & App.EXEName & ".exe"), "YYYY/MM/DD" & " " & "hh:mm am/pm") 'JAGS T8009
            'stVersion = "Ver." & App.Major & "." & App.Minor & "." & App.Revision
            stVersion = App.Major & "." & App.Minor & "." & App.Revision 'JAGS T8210
        End If
        
  'JAGS T7783
'   Dim P As Integer    'DEPURACION DE CODIGO
   
   'S = Comi & Format(Now, FormatF) & Comi & Coma
   S = fecha2(Now) & Coma 'HRR REQ.1309
   S = S & Comi & UCase(UserId) & Comi & Coma
   S = S & Comi & TableName & Comi & Coma
   S = S & Trans & Coma
   S = S & Comi & Cambiar_Comas_Comillas(CStr(Desc)) & Comi
   'JAGS T7783
   S = S & Coma & Comi & stVersion & Comi
   'S = S & Coma & Comi & stFecha & Comi
   S = S & Coma & Comi & Format(stFecha, FormatF) & Comi 'Piloto Chicamocha
   'JAGS T7783
   Result = DoInsert(NombTabAud, S)
   If (Result = FAIL) Then Call Mensaje1("No se pudo hacer el registro de auditor�a", 1)
   Auditor = Result
End Function

Sub Habilitar_Menu()
End Sub

Sub Limpiar_CrysListar()
   Dim I As Integer
   
   With MDI_Inventarios
      For I = 1 To 50: .Crys_Listar.Formulas(I) = NUL$: Next I
      For I = 1 To 50: .Crys_Listar.ParameterFields(I) = NUL$: Next I
      .Crys_Listar.GroupCondition(0) = NUL$
      .Crys_Listar.GroupSelectionFormula = NUL$
      .Crys_Listar.SelectionFormula = NUL$
      .Crys_Listar.SortFields(0) = NUL$
      For I = 1 To 50: .Crys_Listar.SectionFormat(I) = NUL$: Next I
      .Crys_Listar.DataFiles(0) = NUL$
   End With
  'Call InfoEncabezado
End Sub

Function Avanzar_Registro(Tabla As String, campo As String, CampoN As String, cadena As String) As String
     'HRR M2115
'    ReDim Arr(0, 0)
'    If CampoN <> NUL$ Then
'       ReDim Arr(1, 0)
'       Condicion = CampoN & " > '" & cadena & Comi & " ORDER BY " & CampoN
'       Campos = campo & Coma & CampoN
'    Else
'       Condicion = campo & " > '" & cadena & Comi & " ORDER BY " & campo
'       Campos = campo
'    End If
'    Result = LoadMulData(Tabla, Campos, Condicion, Arr())
'    If Result = FAIL Then
'       Avanzar_Registro = cadena
'    Else
'       Avanzar_Registro = Arr(0, 0)
'    End If
    'HRR M2115
    
    'HRR M2115
    ReDim Arr(0)
    If CampoN <> NUL$ Then
       ReDim Arr(1)
       Campos = campo & Coma & "MIN(" & CampoN & ")"
       Condicion = CampoN & " > '" & cadena & Comi & " GROUP BY " & campo
       Condicion = Condicion & " HAVING MIN(" & CampoN & ")= (SELECT MIN(" & CampoN & ")"
       Condicion = Condicion & " FROM " & Tabla & " WHERE " & CampoN & " > '" & cadena & Comi & ")"
    Else
       Condicion = campo & " > '" & cadena & Comi
       Campos = "MIN(" & campo & ")"
    End If
    Result = LoadData(Tabla, Campos, Condicion, Arr())
    If Result = FAIL Then
       Avanzar_Registro = cadena
    Else
       Avanzar_Registro = Arr(0)
    End If
    'HRR M2115
    
End Function

Function Retroceder_Registro(Tabla As String, campo As String, CampoN As String, cadena As String) As String
     'HRR M2115
'    ReDim Arr(0, 0)
'    Dim I As Long
'    If CampoN <> NUL$ Then
'       ReDim Arr(1, 0)
'       Condicion = CampoN & " < '" & cadena & Comi & " ORDER BY " & CampoN
'       Campos = campo & Coma & CampoN
'    Else
'       Condicion = campo & " < '" & cadena & Comi & " ORDER BY " & campo
'       Campos = campo
'    End If
'    Result = LoadMulData(Tabla, campo, Condicion, Arr())
'    If Result = FAIL Then
'       Retroceder_Registro = cadena
'    Else
'       I = UBound(Arr(), 2)
'       Retroceder_Registro = Arr(0, I)
'    End If
    'HRR M2115
    
    'HRR M2115
    ReDim Arr(0)
    If CampoN <> NUL$ Then
       ReDim Arr(1)
       Campos = campo & Coma & "MAX(" & CampoN & ")"
       Condicion = CampoN & " < '" & cadena & Comi & " GROUP BY " & campo
       Condicion = Condicion & " HAVING MAX(" & CampoN & ")= (SELECT MAX(" & CampoN & ")"
       Condicion = Condicion & " FROM " & Tabla & " WHERE " & CampoN & " < '" & cadena & Comi & ")"
    Else
       Condicion = campo & " < '" & cadena & Comi
       Campos = "MAX(" & campo & ")"
    End If
    Result = LoadData(Tabla, Campos, Condicion, Arr())
    If Result = FAIL Then
       Retroceder_Registro = cadena
    Else
       Retroceder_Registro = Arr(0)
    End If
    'HRR M2115
    
End Function

'HRR R1717
'Leer Permisos para cada opcion (Crear, Borrar, Imprimir)
'---------------------------------------------------------------------------------------
' Procedure : Leer_Permisos
' DateTime  : 11/10/2007 15:25
' Author    : hector_rodriguez
' Purpose   : busca los permisos de una opcion y un perfil y habilita o no los botones
' codigo: codigo de opcion
' botong: boton guardar
' botonb: boton borrar
' botoni: boton imprimir
'---------------------------------------------------------------------------------------
'
'Sub Leer_Permisos(Codigo As String, BotonG As SSCommand, BotonB As SSCommand, BotonI As SSCommand)
Sub Leer_Permisos(Codigo As String, BotonG As SSCommand, BotonB As SSCommand, BotonI As SSCommand, Optional StDescForm As String)   'GAVL T5266


'HRR R1872
'Dim Arr(2)
'
'   Campos = "TX_CREA_PERM, TX_ANUL_PERM, TX_IMPR_PERM"
'   'Condicion = "NU_AUTO_PERF_PERM = '" & AutonumPerfil & comi & " AND NU_AUTO_OPCI_PERM = " & Codigo & comi 'HRR M2197
'   Condicion = "NU_AUTO_PERF_PERM = " & AutonumPerfil & " AND NU_AUTO_OPCI_PERM = " & Codigo 'HRR M2197
'   Result = LoadData("PERMISO", Campos, Condicion, Arr())
'HRR R1872

'HRR R1872
Dim Arr(2)

   Campos = "TX_CREA_PERM, TX_ANUL_PERM, TX_IMPR_PERM"
   Desde = "PERFIL,PERMISO,OPCION,FORMULARIO"
   Condicion = "NU_AUTO_PERF = NU_AUTO_PERF_PERM"
   Condicion = Condicion & " AND NU_AUTO_OPCI_PERM=NU_AUTO_OPCI"
   Condicion = Condicion & " AND NU_AUTO_FORM_OPCI=NU_AUTO_FORM"
   'Condicion = Condicion & " AND NU_AUTO_PERF= " & AutonumUser & " AND TX_FORM_FORM = '" & UCase(Codigo) & Comi 'HRR M3037
   Condicion = Condicion & " AND NU_AUTO_PERF= " & AutonumPerfil & " AND TX_FORM_FORM = '" & UCase(Codigo) & Comi 'HRR M3037
   '************** 'INICIO GAVL T5266
   'If StDescForm <> " " Then
   If StDescForm <> "" Then
        Condicion = Condicion & "   AND TX_DESC_FORM=" & Comi & StDescForm & Comi
   End If
   '**************** 'FIN GAVL T5266
   Result = LoadData(Desde, Campos, Condicion, Arr())
'HRR R1872

   If Result <> FAIL Then
      If Encontro Then
         BotonG.Enabled = IIf(Arr(0) = "S", True, False)
         BotonB.Enabled = IIf(Arr(1) = "S", True, False)
         BotonI.Enabled = IIf(Arr(2) = "S", True, False)
      Else
         BotonG.Enabled = False
         BotonB.Enabled = False
         BotonI.Enabled = False
      End If
   Else
      Call Mensaje1("Error leyendo permisos !!", 1)
      BotonG.Enabled = False
      BotonB.Enabled = False
      BotonI.Enabled = False
   End If

End Sub
'HRR R1717
'---------------------------------------------------------------------------------------
' Procedure : BackupDatabase
' DateTime  : 30/01/2008 15:27
' Author    : Gerardo Mantilla Soto
' Purpose   : Genera el Backup de la BD
'---------------------------------------------------------------------------------------
Sub BackupDatabase()
    Dim StFileName As String
    
    If WarnMsg("Desea realizar el Backup de la Base de Datos?") Then
        DoEvents
        validabackup = 1: StFileName = gAbrirAch(1, True)
        DoEvents
        If (StFileName = NUL$) Then
            Call Mensaje1("El Backup de la base de datos, no se ha realizado", 3)
        Else
            Call GeneraBackup(StFileName)
        End If
    End If
End Sub
'---------------------------------------------------------------------------------------
' Procedure : GeneraBackup
' DateTime  : 30/01/2008 11:27
' Author    : Gerardo Mantilla Soto
' Purpose   : Realiza el llamado del SP para realizar la copia de seguridad
'---------------------------------------------------------------------------------------
Sub GeneraBackup(StFileName As String)
    Dim SPCmd As New adodb.Command
    Dim StDescripcionCopia As String
    
    On Error GoTo Errr_GeneraBackup
    
    Call MouseClock: Msglin "Generando Backup ..."
    If ExisteStoreProcedure("PA_BACKUP_DATABASE") Then
        '------------------------------------------------------
        'Ejecuta un Store Procedure para inicializar el proceso
        '------------------------------------------------------
        'PA_BACKUP_DATABASE
        '------------------------------------------------------
        'Parametros: @cBasedeDatos = "Base de datos"
        '            @cFile = "Archivo del Backup"
        '            @CDescription = "Marcarcion del conjunto de la copia de seguridad"
        '            @Resultado OUTPUT
        '------------------------------------------------------
        StDescripcionCopia = NombBD & "-" & ProgName & Space(1) & UserId & Space(1) & Nowserver
        Call CreaParametrosSP(SPCmd, "cBasedeDatos", adVarChar, NombBD, adParamInput, 256)
        Call CreaParametrosSP(SPCmd, "cFile", adVarChar, StFileName, adParamInput, 256)
        Call CreaParametrosSP(SPCmd, "CDescription", adVarChar, StDescripcionCopia, adParamInput, 128) 'GMS M3035
        Call CreaParametrosSP(SPCmd, "Resultado", adBigInt, True, adParamOutput)
        
        SPCmd.CommandTimeout = 0
        SPCmd.CommandType = adCmdStoredProc
        SPCmd.CommandText = "PA_BACKUP_DATABASE"
        SPCmd.ActiveConnection = BD(BDCurCon)
        SPCmd.Execute
        SPCmd.CommandTimeout = 600
        
        Result = IIf(SPCmd.Parameters.Item(3).Value, SUCCEED, FAIL): Set SPCmd = Nothing
        If Result <> FAIL Then Call Mensaje1("El Backup de la Base de Datos " & NombBD & " se ha realizado con �xito.", 2)
    Else
        Call Mensaje1("No existe el objeto PA_BACKUP_DATABASE, comuniquese con el Administrador del Sistema.", 2)
    End If
    
    Call MouseNorm
    Msglin NUL$
        
    Exit Sub

Errr_GeneraBackup:
    '-------------------------------------------------------------------
    'GMS 2008-01-29. Error de permisos de escritura al generar un Backup
    If ERR = -2147217900 Then
        Call Mensaje1("Error de escritura realizando la copia de seguridad de la base de datos. Verifique la ruta especificada, permisos y espacio en disco.", 1) 'GMS M3035
        Call MouseNorm
    Else
        Call ConvertErr
    End If
    '-------------------------------------------------------------------
    Msglin NUL$
End Sub

'HRR R1872
'---------------------------------------------------------------------------------------
' Procedure : Leer_Permiso
' DateTime  : 27/02/2008 16:15
' Author    : hector_rodriguez
' Purpose   : Busca un permiso especifico de una opcion
'                      StFrame: nombre del formulario, SCmdBoton: boton, StOpcion: "G" permiso para crear
'                       "B": permiso para borrar, "I":permiso para imprimir
'---------------------------------------------------------------------------------------
'
Sub Leer_Permiso(StFrame As String, SCmdBoton As SSCommand, StOpcion As String)

Dim Arr(0)

   Select Case StOpcion
      Case "G"
         Campos = "TX_CREA_PERM"
      Case "B"
         Campos = "TX_ANUL_PERM"
      Case "I"
         Campos = "TX_IMPR_PERM"
      Case Else
   End Select
   Desde = "PERFIL,PERMISO,OPCION,FORMULARIO"
   Condicion = "NU_AUTO_PERF = NU_AUTO_PERF_PERM"
   Condicion = Condicion & " AND NU_AUTO_OPCI_PERM=NU_AUTO_OPCI"
   Condicion = Condicion & " AND NU_AUTO_FORM_OPCI=NU_AUTO_FORM"
   'Condicion = Condicion & " AND NU_AUTO_PERF= " & AutonumUser & " AND TX_FORM_FORM = '" & UCase(StFrame) & Comi 'HRR M3037
   Condicion = Condicion & " AND NU_AUTO_PERF= " & AutonumPerfil & " AND TX_FORM_FORM = '" & UCase(StFrame) & Comi 'HRR M3037
   Result = LoadData(Desde, Campos, Condicion, Arr())

   If Result <> FAIL Then
      If Encontro Then
         SCmdBoton.Enabled = IIf(Arr(0) = "S", True, False)
      Else
         SCmdBoton.Enabled = False
      End If
   Else
      Call Mensaje1("Error leyendo permisos !!", 1)
      SCmdBoton.Enabled = False
   End If

End Sub

' Procedure : Leer_Permiso_ProcEspParam
' DateTime  : 14/01/2014 12:40
' Author    : Andres Fabian Manrique Garcia
' Purpose   :  AFMG T20266- R17890 Busca si en Parametros de la aplicacion el usuario tiene permisos para que el sistema ingrese al formulario
              'StOpcion: "G" permiso para crear
'---------------------------------------------------------------------------------------
Sub Leer_Permiso_ProcEspParam(StFrame As String, StOpcion As String)
   Dim StEstado As String
   Dim Arr(0)
   Select Case StOpcion
      Case "G"
         Campos = "TX_CREA_PERM"
      Case Else
   End Select
   Desde = "PERFIL,PERMISO,OPCION,FORMULARIO"
   Condicion = "NU_AUTO_PERF = NU_AUTO_PERF_PERM"
   Condicion = Condicion & " AND NU_AUTO_OPCI_PERM=NU_AUTO_OPCI"
   Condicion = Condicion & " AND NU_AUTO_FORM_OPCI=NU_AUTO_FORM"
   Condicion = Condicion & " AND NU_AUTO_PERF= " & AutonumPerfil & " AND TX_FORM_FORM = '" & UCase(StFrame) & Comi
   Result = LoadData(Desde, Campos, Condicion, Arr())
   If Result <> FAIL Then
      If Encontro Then
         Result = IIf(Arr(0) = "S", True, False)
      Else
         Result = FAIL
      End If
   End If

End Sub

'---------------------------------------------------------------------------------------
' Procedure : ConcatCrit
' DateTime  : 10/06/2008 15:02
' Author    : albert_silva
' Purpose   : R Auditoria SE TRAE DE NOMINA
'---------------------------------------------------------------------------------------
'
Sub ConcatCrit(S As String, Tipo As Integer)
   
   If (Len(Criterio) > 0) Then
      Criterio = Criterio & SParC & IIf(Tipo = 1, "and", "or") & SParA & S
   Else
      Criterio = S
   End If
End Sub

