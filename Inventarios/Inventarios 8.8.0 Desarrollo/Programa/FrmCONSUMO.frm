VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Object = "{0BA686C6-F7D3-101A-993E-0000C0EF6F5E}#1.0#0"; "THREED32.OCX"
Begin VB.Form FrmCONSUMO 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "C�lculo de Consumo"
   ClientHeight    =   3435
   ClientLeft      =   45
   ClientTop       =   435
   ClientWidth     =   7380
   Icon            =   "FrmCONSUMO.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   3435
   ScaleWidth      =   7380
   Begin MSComctlLib.ListView lstBODEGAS 
      Height          =   2055
      Left            =   480
      TabIndex        =   7
      Top             =   480
      Width           =   5295
      _ExtentX        =   9340
      _ExtentY        =   3625
      View            =   3
      LabelEdit       =   1
      MultiSelect     =   -1  'True
      LabelWrap       =   -1  'True
      HideSelection   =   -1  'True
      FullRowSelect   =   -1  'True
      GridLines       =   -1  'True
      _Version        =   393217
      ForeColor       =   -2147483640
      BackColor       =   -2147483643
      BorderStyle     =   1
      Appearance      =   1
      NumItems        =   2
      BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Key             =   "COD"
         Text            =   "C�digo"
         Object.Width           =   2540
      EndProperty
      BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   1
         Key             =   "NOM"
         Text            =   "Nombre"
         Object.Width           =   5292
      EndProperty
   End
   Begin VB.Frame framActions 
      Height          =   2085
      Left            =   6120
      TabIndex        =   0
      Top             =   720
      Width           =   1065
      Begin Threed.SSCommand Cmd_Botones 
         Height          =   795
         HelpContextID   =   120
         Index           =   2
         Left            =   120
         TabIndex        =   1
         ToolTipText     =   "Salir"
         Top             =   1140
         Width           =   855
         _Version        =   65536
         _ExtentX        =   1508
         _ExtentY        =   1402
         _StockProps     =   78
         Caption         =   "&SALIR"
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   6
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Picture         =   "FrmCONSUMO.frx":058A
      End
      Begin Threed.SSCommand Cmd_Botones 
         Height          =   795
         Index           =   1
         Left            =   120
         TabIndex        =   2
         Top             =   240
         Width           =   855
         _Version        =   65536
         _ExtentX        =   1508
         _ExtentY        =   1402
         _StockProps     =   78
         Caption         =   "&CALCULAR"
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   6
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         RoundedCorners  =   0   'False
         Picture         =   "FrmCONSUMO.frx":0C54
      End
   End
   Begin MSComCtl2.DTPicker dtpDESDE 
      Height          =   375
      Left            =   1200
      TabIndex        =   3
      Top             =   2790
      Width           =   1575
      _ExtentX        =   2778
      _ExtentY        =   661
      _Version        =   393216
      Format          =   99942401
      CurrentDate     =   38393
   End
   Begin MSComCtl2.DTPicker dtpHASTA 
      Height          =   375
      Left            =   3840
      TabIndex        =   4
      Top             =   2790
      Width           =   1575
      _ExtentX        =   2778
      _ExtentY        =   661
      _Version        =   393216
      Format          =   99942401
      CurrentDate     =   38393
   End
   Begin VB.Label Label1 
      Alignment       =   1  'Right Justify
      Caption         =   "Desde:"
      Height          =   255
      Left            =   360
      TabIndex        =   6
      Top             =   2910
      Width           =   750
   End
   Begin VB.Label Label2 
      Alignment       =   1  'Right Justify
      Caption         =   "Hasta:"
      Height          =   255
      Left            =   3000
      TabIndex        =   5
      Top             =   2910
      Width           =   750
   End
End
Attribute VB_Name = "FrmCONSUMO"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Public OpcCod        As String   'Opci�n de seguridad

Private Sub Cmd_Botones_Click(Index As Integer)
    Dim strBODEGAS As String, vContar As Integer, vPeriodo As Long
    Dim vUno As MSComctlLib.ListItem
    Dim ArrCLCL() As Variant
    Dim vQuien As Long, vCuanto As Single
    vPeriodo = (Me.dtpHASTA.Value - Me.DTPDesde.Value)
    If vPeriodo = 0 Then Exit Sub
    Select Case Index
    Case Is = 1
        strBODEGAS = ""
        For Each vUno In Me.lstBODEGAS.ListItems
            If vUno.Selected Then
                strBODEGAS = strBODEGAS & IIf(Len(strBODEGAS) = 0, "", ",") & vUno.Tag
            End If
        Next
        Condi = "NU_AUTO_ORGCABE_KARD=NU_AUTO_ENCA"
        Condi = Condi & " AND NU_AUTO_DOCU_ENCA=NU_AUTO_DOCU"
        Condi = Condi & " AND NU_AUTO_DOCU IN " & "(" & _
            BajaConsumo & "," & DevolucionBaja & "," & AnulaBaja & "," & _
            Salida & "," & DevolucionSalida & "," & AnulaSalida & "," & _
            FacturaVenta & "," & DevolucionFacturaVenta & "," & AnulaFacturaVenta & ")"
'        Condi = Condi & " AND NU_AUTO_ARTI_KARD IN " & LosAutoArticulo
        Condi = Condi & " AND NU_AUTO_BODE_KARD IN (" & strBODEGAS & ")"
        Condi = Condi & " AND FE_FECH_KARD >= " & FFechaCon(Me.DTPDesde.Value)
        Condi = Condi & " AND FE_FECH_KARD <= " & FFechaCon(Me.dtpHASTA.Value)
        Condi = Condi & " GROUP BY NU_AUTO_ARTI_KARD"
        Condi = Condi & " HAVING SUM((NU_SALIDA_KARD-NU_ENTRAD_KARD)*NU_MULT_KARD/NU_DIVI_KARD)>0"
        Desde = "IN_KARDEX, IN_ENCABEZADO, IN_DOCUMENTO"
        Campos = "NU_AUTO_ARTI_KARD, SUM((NU_SALIDA_KARD-NU_ENTRAD_KARD)*NU_MULT_KARD/NU_DIVI_KARD)"
        ReDim ArrCLCL(2, 0)
        Result = LoadMulData(Desde, Campos, Condi, ArrCLCL())
        If Result = FAIL Then Exit Sub
        If Not Encontro Then Exit Sub
        For vContar = 0 To UBound(ArrCLCL, 2)
            vQuien = CLng(ArrCLCL(0, vContar))
            vCuanto = CSng(ArrCLCL(1, vContar)) * 365.25 / 12 / vPeriodo
            Result = DoUpdate("ARTICULO", "NU_SUMO_ARTI=" & vCuanto, "NU_AUTO_ARTI=" & vQuien)
        Next
    Case Is = 2
        Unload Me
    End Select
End Sub

Private Sub Form_Load()
    Dim ArrUXB() As Variant, vContar As Integer, vClave As String
    Call CenterForm(MDI_Inventarios, Me)
    Me.dtpHASTA.Value = DateAdd("d", -Day(FechaServer), FechaServer)
    Me.DTPDesde.Value = DateAdd("yyyy", -1, Me.dtpHASTA.Value)
    Me.DTPDesde.Value = DateAdd("d", 1, Me.DTPDesde.Value)

    Campos = "NU_AUTO_BODE, NU_AUTO_BODE_BODE, TX_CODI_BODE, TX_NOMB_BODE, " & _
        "CD_CODI_CECO_BODE, TX_VENTA_BODE"
    Desde = "IN_BODEGA ORDER BY TX_NOMB_BODE"
    Condi = ""
    ReDim ArrUXB(5, 0)
    Result = LoadMulData(Desde, Campos, Condi, ArrUXB())
    If Result = FAIL Then Exit Sub
    Me.lstBODEGAS.ListItems.Clear
    For vContar = 0 To UBound(ArrUXB, 2)
        vClave = "B" & ArrUXB(0, vContar)
        Me.lstBODEGAS.ListItems.Add , vClave, ArrUXB(2, vContar)
        Me.lstBODEGAS.ListItems.Item(vClave).Tag = ArrUXB(0, vContar)
        Me.lstBODEGAS.ListItems.Item(vClave).ListSubItems.Add , "NOM", ArrUXB(3, vContar)
    Next
End Sub
