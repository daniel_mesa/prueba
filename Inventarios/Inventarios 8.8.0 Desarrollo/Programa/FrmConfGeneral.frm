VERSION 5.00
Object = "{00025600-0000-0000-C000-000000000046}#5.2#0"; "Crystl32.OCX"
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "MSFLXGRD.OCX"
Object = "{0BA686C6-F7D3-101A-993E-0000C0EF6F5E}#1.0#0"; "THREED32.OCX"
Begin VB.Form FrmConfGeneral 
   BorderStyle     =   1  'Fixed Single
   ClientHeight    =   4560
   ClientLeft      =   45
   ClientTop       =   375
   ClientWidth     =   6315
   Icon            =   "FrmConfGeneral.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   ScaleHeight     =   4560
   ScaleWidth      =   6315
   Begin VB.Frame Frmlista 
      Caption         =   "Lista"
      Height          =   3015
      Left            =   120
      TabIndex        =   8
      Top             =   1440
      Width           =   4935
      Begin MSFlexGridLib.MSFlexGrid MsfgdLista 
         Height          =   2655
         Left            =   120
         TabIndex        =   3
         Top             =   240
         Width           =   4695
         _ExtentX        =   8281
         _ExtentY        =   4683
         _Version        =   393216
         Rows            =   1
         Cols            =   5
         BackColor       =   16777215
         ScrollTrack     =   -1  'True
         HighLight       =   0
         AllowUserResizing=   3
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
   End
   Begin VB.TextBox TxtDescripcion 
      Height          =   525
      Left            =   1080
      MaxLength       =   254
      MultiLine       =   -1  'True
      ScrollBars      =   2  'Vertical
      TabIndex        =   2
      Top             =   600
      Width           =   3855
   End
   Begin VB.TextBox TxtCodigo 
      Height          =   285
      Left            =   1080
      MaxLength       =   3
      TabIndex        =   0
      Top             =   120
      Width           =   1575
   End
   Begin VB.Frame Frml 
      Height          =   4335
      Left            =   5160
      TabIndex        =   9
      Top             =   120
      Width           =   1095
      Begin Threed.SSCommand CommandButton 
         Height          =   735
         Index           =   0
         Left            =   120
         TabIndex        =   4
         ToolTipText     =   "Guardar"
         Top             =   360
         Width           =   855
         _Version        =   65536
         _ExtentX        =   1508
         _ExtentY        =   1296
         _StockProps     =   78
         Caption         =   "&GUARDAR"
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   6
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Picture         =   "FrmConfGeneral.frx":058A
      End
      Begin Threed.SSCommand CommandButton 
         Height          =   735
         Index           =   1
         Left            =   120
         TabIndex        =   5
         ToolTipText     =   "Borrar"
         Top             =   1320
         Width           =   855
         _Version        =   65536
         _ExtentX        =   1508
         _ExtentY        =   1296
         _StockProps     =   78
         Caption         =   "&BORRAR"
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   6
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Picture         =   "FrmConfGeneral.frx":0C54
      End
      Begin Threed.SSCommand CommandButton 
         Height          =   735
         Index           =   2
         Left            =   120
         TabIndex        =   6
         ToolTipText     =   "Listar"
         Top             =   2280
         Width           =   855
         _Version        =   65536
         _ExtentX        =   1508
         _ExtentY        =   1296
         _StockProps     =   78
         Caption         =   "&LISTAR "
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   6
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Picture         =   "FrmConfGeneral.frx":131E
      End
      Begin Threed.SSCommand CommandButton 
         Height          =   735
         Index           =   3
         Left            =   120
         TabIndex        =   7
         ToolTipText     =   "Salir"
         Top             =   3240
         Width           =   855
         _Version        =   65536
         _ExtentX        =   1508
         _ExtentY        =   1296
         _StockProps     =   78
         Caption         =   "&SALIR "
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   6
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Picture         =   "FrmConfGeneral.frx":1F72
      End
   End
   Begin Threed.SSCommand CmdSeleccion 
      Height          =   255
      Left            =   2880
      TabIndex        =   1
      Top             =   120
      Width           =   615
      _Version        =   65536
      _ExtentX        =   1085
      _ExtentY        =   450
      _StockProps     =   78
      BevelWidth      =   0
      RoundedCorners  =   0   'False
      Outline         =   0   'False
      Picture         =   "FrmConfGeneral.frx":263C
   End
   Begin Crystal.CrystalReport Crys_Listar2 
      Left            =   0
      Top             =   0
      _ExtentX        =   741
      _ExtentY        =   741
      _Version        =   348160
      WindowLeft      =   20
      WindowWidth     =   750
      WindowHeight    =   450
      WindowControlBox=   -1  'True
      WindowMaxButton =   -1  'True
      WindowMinButton =   -1  'True
      WindowControls  =   -1  'True
      PrintFileLinesPerPage=   60
      WindowShowGroupTree=   -1  'True
      WindowAllowDrillDown=   -1  'True
      WindowShowCloseBtn=   -1  'True
      WindowShowSearchBtn=   -1  'True
      WindowShowPrintSetupBtn=   -1  'True
      WindowShowRefreshBtn=   -1  'True
   End
   Begin VB.Label Label2 
      Caption         =   "Descripción:"
      Height          =   255
      Left            =   120
      TabIndex        =   11
      Top             =   600
      Width           =   975
   End
   Begin VB.Label Label1 
      Caption         =   "Código:"
      Height          =   255
      Left            =   120
      TabIndex        =   10
      Top             =   120
      Width           =   975
   End
End
Attribute VB_Name = "FrmConfGeneral"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'---------------------------------------------------------------------------------------
' Module    : FrmConfGeneral
' DateTime  : 25/02/2011 13:50
' Author    : gabriel_vallejo
' Purpose   : FORMULARIO EN QUE SE INSTACI PRINCIPIO ACTIVO, CLASIFICACION DE RIESGO, PRESENTACION COMERCIAL
'---------------------------------------------------------------------------------------
'' GAVL R3201 / T5101 25/02/2011

Option Explicit
Dim InOpcion As Integer
Dim Encontro_MeDis As Integer
Dim Cambiar_Codigo As Boolean
Dim sss As String


Private Sub CmdSeleccion_Click()
Dim Codigo As String
If InOpcion = 1 Then
 Codigo = Seleccion("PRINCIPIO_ACTIVO", "TX_DESC_PRAC", " TX_CODIGO_PRAC , TX_DESC_PRAC", "PRINCIPIO_ACTIVO", NUL$)
                   'If Codigo <> NUL$ Then TxtCodigo.Text = Codigo
                   If Codigo <> NUL$ Then txtCodigo.Text = Codigo Else txtCodigo.Text = "" 'GAVL T5264
                   Call txtCodigo_LostFocus
                   'TxtCodigo.SetFocus
                   TxtDescripcion.SetFocus 'GAVL T5264
ElseIf InOpcion = 0 Then
 Codigo = Seleccion("CLASIFICACION_RIESGO", "TX_DESC_CLRI", "TX_CODIGO_CLRI , TX_DESC_CLRI", "CLASIFICACIÓN RIESGO", NUL$)
                   'If Codigo <> NUL$ Then TxtCodigo.Text = Codigo
                   If Codigo <> NUL$ Then txtCodigo.Text = Codigo Else txtCodigo.Text = "" 'GAVL T5264
                   Call txtCodigo_LostFocus
                   'TxtCodigo.SetFocus
                   TxtDescripcion.SetFocus 'GAVL T5264
ElseIf InOpcion = 2 Then
 Codigo = Seleccion("PRESENTACION_COMERCIAL", "TX_DESC_PRCO", "TX_CODI_PRCO , TX_DESC_PRCO", "PRESENTACIÓN COMERCIAL", NUL$)
                   'If Codigo <> NUL$ Then TxtCodigo.Text = Codigo
                   If Codigo <> NUL$ Then txtCodigo.Text = Codigo Else txtCodigo.Text = "" 'GAVL T5264
                   Call txtCodigo_LostFocus
                   'TxtCodigo.SetFocus
                   TxtDescripcion.SetFocus 'GAVL T5264
End If
                  
End Sub

Private Sub Form_Load()
 Call CenterForm(MDI_Inventarios, Me)
 'Call GrdFDef(MsfgdLista, 0, 0, "  Código,700,                                       Nombre,5550")
 Call GrdFDef(MsfgdLista, 0, 0, "  Código,700,                                        Descripción,5500") 'GAVL  T5257
 
 If FrmMenu.StNomForm = "PRINCIPIO ACTIVO" Then
    Call PRINCIPIO_ACTIVO
    CmdSeleccion.ToolTipText = "Selección Principio Activo" 'GAVL T5175
    Call Leer_Permisos(Me.Name, CommandButton(0), CommandButton(1), CommandButton(2), FrmMenu.StNomForm)  'GAVL T5266
    InOpcion = 1
 ElseIf FrmMenu.StNomForm = "CLASIFICACIÓN DE RIESGO" Then
     Call CLASIFICACION_RIESGO
     CmdSeleccion.ToolTipText = "Selección Clasificación De Riesgo" 'GAVL T5175
     Call Leer_Permisos(Me.Name, CommandButton(0), CommandButton(1), CommandButton(2), FrmMenu.StNomForm)  'GAVL T5266
    InOpcion = 0
 ElseIf FrmMenu.StNomForm = "PRESENTACIÓN COMERCIAL" Then
     Call PRESENTACION_COMERCIAL
     CmdSeleccion.ToolTipText = "Selección Presentación Comercial" 'GAVL T5175
     Call Leer_Permisos(Me.Name, CommandButton(0), CommandButton(1), CommandButton(2), FrmMenu.StNomForm)  'GAVL T5266
     InOpcion = 2
 End If
  
End Sub
Sub PRINCIPIO_ACTIVO()
Dim CodAnt As String
   Desde = "PRINCIPIO_ACTIVO"
   Campos = "TX_CODIGO_PRAC, TX_DESC_PRAC"
   Result = DoSelect(Desde, Campos, NUL$)
   MsfgdLista.Rows = 1
   If Result <> FAIL Then
      If ResultQuery() Then
         While NextRowQuery()
            If CodAnt <> DataQuery(1) Then
               MsfgdLista.AddItem DataQuery(1) & vbTab & DataQuery(2)
               CodAnt = DataQuery(1)
            End If
         Wend
      End If
   End If
End Sub


Sub CLASIFICACION_RIESGO()
Dim CodAnt As String
   Desde = "CLASIFICACION_RIESGO"
   Campos = "TX_CODIGO_CLRI,TX_DESC_CLRI"
   Result = DoSelect(Desde, Campos, NUL$)
   MsfgdLista.Rows = 1
   If Result <> FAIL Then
      If ResultQuery() Then
         While NextRowQuery()
            If CodAnt <> DataQuery(1) Then
               MsfgdLista.AddItem DataQuery(1) & vbTab & DataQuery(2)
               CodAnt = DataQuery(1)
            End If
         Wend
      End If
   End If
End Sub
Sub PRESENTACION_COMERCIAL()
Dim CodAnt As String
   Desde = "PRESENTACION_COMERCIAL"
   Campos = "TX_CODI_PRCO, TX_DESC_PRCO"
   Result = DoSelect(Desde, Campos, NUL$)
   MsfgdLista.Rows = 1
   If Result <> FAIL Then
      If ResultQuery() Then
         While NextRowQuery()
            If CodAnt <> DataQuery(1) Then
               MsfgdLista.AddItem DataQuery(1) & vbTab & DataQuery(2)
               CodAnt = DataQuery(1)
            End If
         Wend
      End If
   End If
End Sub

Private Sub MsfgdLista_DblClick()

  MsfgdLista.Col = 0
   txtCodigo.Text = MsfgdLista.Text
   txtCodigo.Tag = MsfgdLista.Text
  MsfgdLista.Col = 1
   TxtDescripcion.Text = MsfgdLista.Text
  Encontro_MeDis = True
  txtCodigo.SetFocus
    
End Sub

Private Sub TxtCodigo_GotFocus()
    txtCodigo.Tag = txtCodigo.Text
    Msglin "Digite el código del servicio"
    Cambiar_Codigo = False
End Sub

Private Sub txtCodigo_LostFocus()
Dim ArrMeDis() As Variant


ReDim ArrMeDis(1)
If InOpcion = 1 Then
    ReDim ArrMeDis(1)
    Condi = "TX_CODIGO_PRAC =" & Comi & txtCodigo.Text & Comi
    Result = LoadData("PRINCIPIO_ACTIVO", "TX_CODIGO_PRAC, TX_DESC_PRAC", Condi, ArrMeDis())
    If Result <> FAIL And Encontro Then
       txtCodigo.Text = ArrMeDis(0)
       TxtDescripcion = Restaurar_Comas_Comillas(CStr(ArrMeDis(1)))
       If Len(TxtDescripcion.Text) > 40 Then
        


       End If
       Encontro_MeDis = True
       Cambiar_Codigo = False
    Else
       If Cambiar_Codigo = False Then
            TxtDescripcion = NUL$
            Encontro_MeDis = False
       Else
            Encontro_MeDis = False
       End If
     End If
ElseIf InOpcion = 0 Then
    Condi = "TX_CODIGO_CLRI =" & Comi & txtCodigo.Text & Comi
    Result = LoadData("CLASIFICACION_RIESGO", "TX_CODIGO_CLRI , TX_DESC_CLRI", Condi, ArrMeDis())
    If Result <> FAIL And Encontro Then
        txtCodigo.Text = ArrMeDis(0)
        TxtDescripcion = Restaurar_Comas_Comillas(CStr(ArrMeDis(1)))
        Encontro_MeDis = True
        Cambiar_Codigo = False
    Else
        If Cambiar_Codigo = False Then
            TxtDescripcion = NUL$
            Encontro_MeDis = False
        Else
            Encontro_MeDis = False
        End If
     End If
ElseIf InOpcion = 2 Then
     Condi = "TX_CODI_PRCO  =" & Comi & txtCodigo.Text & Comi
    Result = LoadData("PRESENTACION_COMERCIAL", "TX_CODI_PRCO , TX_DESC_PRCO", Condi, ArrMeDis())
    If Result <> FAIL And Encontro Then
        txtCodigo.Text = ArrMeDis(0)
        TxtDescripcion = Restaurar_Comas_Comillas(CStr(ArrMeDis(1)))
        Encontro_MeDis = True
        Cambiar_Codigo = False
    Else
        If Cambiar_Codigo = False Then
            TxtDescripcion = NUL$
            Encontro_MeDis = False
        Else
            Encontro_MeDis = False
        End If
     End If
     
End If
End Sub

Private Sub CommandButton_Click(Index As Integer)
Dim IntAux As Integer
Encontro = Encontro_MeDis
Select Case Index
        Case 0:
                If InOpcion = 1 Then
                 Grabar_PrinActivo
                ElseIf InOpcion = 0 Then
                 Grabar_ClasiRiesgo
                ElseIf InOpcion = 2 Then
                  Grabar_PresentaCome
                End If
        Case 1:
                If InOpcion = 1 Then
                 Borrar_PrinActivo
                ElseIf InOpcion = 0 Then
                 Borrar_ClasiRiesgo
                ElseIf InOpcion = 2 Then
                 Borrar_PresentaCome
                End If
        Case 2:
                If InOpcion = 1 Then
                   'Call Listar1("InfPrincActivo.rpt", NUL$, NUL$, 0, "PRINCIPIO ACTIVO", MDI_Inventarios) 'GAVL T5119
                   Call ListarGene("InfPrincActivo.rpt", 0, "PRINCIPIO ACTIVO")
                ElseIf InOpcion = 0 Then
                   'Call Listar1("InfClasificacion.rpt", NUL$, NUL$, 0, "CLASIFICACIÓN DE RIESGO", MDI_Inventarios) 'GAVL T5119
                   Call ListarGene("InfClasificacion.rpt", 0, "CLASIFICACIÓN DE RIESGO")
                ElseIf InOpcion = 2 Then
                   'Call Listar1("InfPresentComer.rpt", NUL$, NUL$, 0, "PRESENTACIÓN COMERCIAL", MDI_Inventarios) 'GAVL T5119
                   Call ListarGene("InfPresentComer.rpt", 0, "PRESENTACIÓN COMERCIAL")
                End If
        Case 3:    Unload Me
End Select
End Sub

Sub Grabar_PrinActivo()

     If (txtCodigo.Text = NUL$ Or TxtDescripcion = NUL$) Then
       'Call Mensaje1("Se debe especificar los datos completos del medicamento!!!...", 3)
       Call Mensaje1("Se debe especificar los datos completos del principio activo!!!...", 3) 'GAVL  T5257
       Call MouseNorm: Exit Sub
     End If

        Valores = "TX_CODIGO_PRAC=" & Comi & txtCodigo.Text & Comi & Coma
        Valores = Valores & "TX_DESC_PRAC=" & Comi & Cambiar_Comas_Comillas(TxtDescripcion.Text) & Comi

      If (Not Encontro) Then
          Result = DoInsertSQL("PRINCIPIO_ACTIVO", Valores)
          If (Result <> FAIL) Then Result = Auditor("PRINCIPIO_ACTIVO", TranIns, LastCmd)
       Else
        'INICIO GAVL T5174
         If MsgBox("Desea modificar la información?", vbYesNo, "Inventarios - Confirmación") = vbNo Then
            Exit Sub
         End If
         'FIN  GAVL T5174
         
          If txtCodigo.Tag <> NUL$ And Cambiar_Codigo = True Then
            Condicion = "TX_CODIGO_PRAC=" & Comi & txtCodigo.Tag & Comi
          Else
            Condicion = "TX_CODIGO_PRAC=" & Comi & txtCodigo.Text & Comi
          End If
          
          Result = DoUpdate("PRINCIPIO_ACTIVO", Valores, Condicion)
          If (Result <> FAIL) Then Result = Auditor("PRINCIPIO_ACTIVO", TranUpd, LastCmd)
       End If
    
    LimpiarMeDis


End Sub
Sub Grabar_ClasiRiesgo()
 
 
   If (txtCodigo.Text = NUL$ Or TxtDescripcion = NUL$) Then
      'Call Mensaje1("Se debe especificar los datos completos del dispositivo!!!...", 3)
      Call Mensaje1("Se debe especificar los datos completos de la clasificación de riesgo!!!...", 3) 'GAVL T5260
      Call MouseNorm: Exit Sub
   End If
   
 
        Valores = "TX_CODIGO_CLRI=" & Comi & txtCodigo.Text & Comi & Coma
        Valores = Valores & "TX_DESC_CLRI=" & Comi & Cambiar_Comas_Comillas(TxtDescripcion.Text) & Comi

      If (Not Encontro) Then
          Result = DoInsertSQL("CLASIFICACION_RIESGO", Valores)
          If (Result <> FAIL) Then Result = Auditor("CLASIFICACION_RIESGO", TranIns, LastCmd)
       Else
         'INICIO GAVL T5174
         If MsgBox("Desea modificar la información?", vbYesNo, "Inventarios - Confirmación") = vbNo Then
            Exit Sub
         End If
         'FIN  GAVL T5174
          If txtCodigo.Tag <> NUL$ And Cambiar_Codigo = True Then
            Condicion = "TX_CODIGO_CLRI=" & Comi & txtCodigo.Tag & Comi
          Else
            Condicion = "TX_CODIGO_CLRI=" & Comi & txtCodigo.Text & Comi
          End If
          
          Result = DoUpdate("CLASIFICACION_RIESGO", Valores, Condicion)
          If (Result <> FAIL) Then Result = Auditor("CLASIFICACION_RIESGO", TranUpd, LastCmd)
       End If
    
    LimpiarMeDis
End Sub
Sub Grabar_PresentaCome()

 If (txtCodigo.Text = NUL$ Or TxtDescripcion = NUL$) Then
      Call Mensaje1("Se debe especificar los datos completos de la presentación comercial!!!...", 3)
      Call MouseNorm: Exit Sub
   End If
   
 
        Valores = "TX_CODI_PRCO=" & Comi & txtCodigo.Text & Comi & Coma
        Valores = Valores & "TX_DESC_PRCO=" & Comi & Cambiar_Comas_Comillas(TxtDescripcion.Text) & Comi

      If (Not Encontro) Then
          Result = DoInsertSQL("PRESENTACION_COMERCIAL", Valores)
          If (Result <> FAIL) Then Result = Auditor("PRESENTACION_COMERCIAL", TranIns, LastCmd)
       Else
         'INICIO GAVL T5174
         If MsgBox("Desea modificar la información?", vbYesNo, "Inventarios - Confirmación") = vbNo Then
            Exit Sub
         End If
         'FIN  GAVL T5174
          If txtCodigo.Tag <> NUL$ And Cambiar_Codigo = True Then
            Condicion = "TX_CODI_PRCO=" & Comi & txtCodigo.Tag & Comi
          Else
            Condicion = "TX_CODI_PRCO=" & Comi & txtCodigo.Text & Comi
          End If
          
          Result = DoUpdate("PRESENTACION_COMERCIAL", Valores, Condicion)
          If (Result <> FAIL) Then Result = Auditor("PRESENTACION_COMERCIAL", TranUpd, LastCmd)
       End If
    
    LimpiarMeDis

End Sub
Sub LimpiarMeDis()
    txtCodigo = NUL$
    TxtDescripcion = NUL$
   
 If InOpcion = 1 Then
    Call PRINCIPIO_ACTIVO
 ElseIf InOpcion = 0 Then
    Call CLASIFICACION_RIESGO
 ElseIf InOpcion = 2 Then
    Call PRESENTACION_COMERCIAL
 End If
  txtCodigo.SetFocus
 
End Sub
Sub Borrar_PrinActivo()

   If txtCodigo = NUL$ Then
       'Call Mensaje1("Ingrese el código del medicamento", 1)
       Call Mensaje1("Ingrese el código del principio activo", 1) 'GAVL T5260
       Call MouseNorm: Exit Sub
   End If

   If TxtDescripcion = NUL$ Then
       Call Mensaje1("No se puede borrar la descripcion del medicamento por que no tiene un codigo seleccionado", 3)
       Call MouseNorm: Exit Sub
   End If

   If (Encontro <> False) Then
       If (Not WarnDel()) Then Call MouseNorm:  Exit Sub

        'Se valida que la dependencia no este atada a un articulo
        Condi = "TX_CODPRI_ARTI= '" & txtCodigo.Text & Comi
        If existedato("ARTICULO", "TX_CODPRI_ARTI", Condi) = True Then
           Call RollBackTran
           Call MouseNorm: Exit Sub
        End If
       
       Condicion = "TX_CODIGO_PRAC=" & Comi & txtCodigo.Text & Comi
   
        If (BeginTran(STranDel & "PRINCIPIO_ACTIVO") <> FAIL) Then
            Result = DoDelete("PRINCIPIO_ACTIVO", Condicion)
            If (Result <> FAIL) Then Result = Auditor("PRINCIPIO_ACTIVO", TranDel, LastCmd)
        End If
   Else
       Call Mensaje1("No se pudo borrar el principio activo de la base de datos, debido a que no existe !!!...", 1)
       Call MouseNorm: Msglin NUL$: Exit Sub
   End If
   If (Result <> FAIL) Then
       If (CommitTran() <> FAIL) Then
             LimpiarMeDis
       Else
          Call RollBackTran
       End If
   Else
       Call RollBackTran
   End If
End Sub
Sub Borrar_ClasiRiesgo()

 If txtCodigo = NUL$ Then
       'Call Mensaje1("Ingrese el código del medicamento", 1)
       Call Mensaje1("Ingrese el código de clasificación de Riesgo", 1) 'GAVL T5260
       Call MouseNorm: Exit Sub
   End If

   If TxtDescripcion = NUL$ Then
       Call Mensaje1("No se puede borrar la descripcion del medicamento por que no tiene un codigo seleccionado", 3)
       Call MouseNorm: Exit Sub
   End If

   If (Encontro <> False) Then
       If (Not WarnDel()) Then Call MouseNorm:  Exit Sub

        'Se valida que la dependencia no este atada a un articulo
        Condi = "TX_CODCLAS_ARTI= '" & txtCodigo.Text & Comi
        If existedato("ARTICULO", "TX_CODCLAS_ARTI", Condi) = True Then
           Call RollBackTran
           Call MouseNorm: Exit Sub
        End If
       
       Condicion = "TX_CODIGO_CLRI=" & Comi & txtCodigo.Text & Comi
   
        If (BeginTran(STranDel & "CLASIFICACION_RIESGO") <> FAIL) Then
            Result = DoDelete("CLASIFICACION_RIESGO", Condicion)
            If (Result <> FAIL) Then Result = Auditor("CLASIFICACION_RIESGO", TranDel, LastCmd)
        End If
   Else
       Call Mensaje1("No se pudo borrar la clasificación de riego de la base de datos, debido a que no existe !!!...", 1)
       Call MouseNorm: Msglin NUL$: Exit Sub
   End If
   If (Result <> FAIL) Then
       If (CommitTran() <> FAIL) Then
          LimpiarMeDis
       Else
          Call RollBackTran
       End If
   Else
       Call RollBackTran
   End If
    
End Sub
Sub Borrar_PresentaCome()


 If txtCodigo = NUL$ Then
       Call Mensaje1("Ingrese el código de la presentación comercial", 1)
       Call MouseNorm: Exit Sub
   End If

   If TxtDescripcion = NUL$ Then
       Call Mensaje1("No se puede borrar la descripción comercial por que no tiene un codigo seleccionado", 3)
       Call MouseNorm: Exit Sub
   End If

   If (Encontro <> False) Then
       If (Not WarnDel()) Then Call MouseNorm:  Exit Sub

        'Se valida que la dependencia no este atada a un articulo
        Condi = "TX_PRCO_ARTI= '" & txtCodigo.Text & Comi
        If existedato("ARTICULO", "TX_PRCO_ARTI", Condi) = True Then
           Call RollBackTran
           Call MouseNorm: Exit Sub
        End If
       
       Condicion = "TX_CODI_PRCO=" & Comi & txtCodigo.Text & Comi
   
        If (BeginTran(STranDel & "PRESENTACION_COMERCIAL") <> FAIL) Then
            Result = DoDelete("PRESENTACION_COMERCIAL", Condicion)
            If (Result <> FAIL) Then Result = Auditor("PRESENTACION_COMERCIAL", TranDel, LastCmd)
        End If
   Else
       Call Mensaje1("No se pudo borrar la clasificación de riego de la base de datos, debido a que no existe !!!...", 1)
       Call MouseNorm: Msglin NUL$: Exit Sub
   End If
   If (Result <> FAIL) Then
       If (CommitTran() <> FAIL) Then
          LimpiarMeDis
       Else
          Call RollBackTran
       End If
   Else
       Call RollBackTran
   End If
End Sub
Private Sub CommandButton_KeyPress(Index As Integer, KeyAscii As Integer)
  Call Cambiar_Enter(KeyAscii)
End Sub
Private Sub MsfgdLista_KeyPress(KeyAscii As Integer)
   Call Cambiar_Enter(KeyAscii)
End Sub
Private Sub TxtCodigo_KeyPress(KeyAscii As Integer)
  Call ValKeyAlfaNum(KeyAscii)
  Call Cambiar_Enter(KeyAscii)
End Sub

Private Sub TxtDescripcion_GotFocus()
   Msglin "Digite el codigo de " & Me.Caption
End Sub

Private Sub TxtDescripcion_KeyPress(KeyAscii As Integer)
 Call ValKeyAlfaNum(KeyAscii)
 Call Cambiar_Enter(KeyAscii)
End Sub

'---------------------------------------------------------------------------------------
' Procedure : ListarGene
' DateTime  : 28/02/2011 16:05
' Author    : gabriel_vallejo
' Purpose   : PROCESO DE IMPRIMIR 'GAVL T5119
'---------------------------------------------------------------------------------------
'
Sub ListarGene(ByVal Reporte As String, ByVal Destino As Integer, ByVal Titulo As String)

On Error GoTo control

    ReportConnect = "ODBC;DSN=" + BDDSNODBCName + ";"
    ReportConnect = ReportConnect & ";uid=" + UserIdBD + ";pwd=" + UserPwdBD
    ReportConnect = ReportConnect & ";server=" + ServName
    
    Crys_Listar2.Formulas(0) = "ENTIDAD='" & Entidad & Comi
    Crys_Listar2.Formulas(1) = "NIT= 'NIT. " & Nit & Comi
    Crys_Listar2.Formulas(2) = "HORA= '" & Format(Now, "hh:mm") & Comi
    Crys_Listar2.Formulas(3) = "USUARIO= '" & UserId & Comi
    Crys_Listar2.ReportFileName = DirRpt & Reporte
    Crys_Listar2.Destination = Destino
    Crys_Listar2.WindowTitle = Titulo
    Crys_Listar2.Connect = ReportConnect
    Crys_Listar2.Action = 1
    

Exit Sub
control:
Call Mensaje1(ERR.Number & ": " & ERR.Description, 1)
End Sub


