VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Begin VB.Form FrmMaestroDetalle 
   BorderStyle     =   1  'Fixed Single
   ClientHeight    =   6645
   ClientLeft      =   45
   ClientTop       =   435
   ClientWidth     =   10455
   Icon            =   "FrmMaestroDetalle.frx":0000
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   6645
   ScaleWidth      =   10455
   Begin VB.ComboBox CboAsignar 
      Height          =   315
      Left            =   1320
      Style           =   2  'Dropdown List
      TabIndex        =   1
      Top             =   120
      Width           =   3855
   End
   Begin VB.Frame FramSer 
      Height          =   6015
      Left            =   5280
      TabIndex        =   6
      Top             =   480
      Width           =   5055
      Begin MSComctlLib.ImageList ImageList1 
         Left            =   240
         Top             =   5280
         _ExtentX        =   1005
         _ExtentY        =   1005
         BackColor       =   -2147483643
         ImageWidth      =   16
         ImageHeight     =   16
         MaskColor       =   12632256
         _Version        =   393216
         BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
            NumListImages   =   3
            BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
               Picture         =   "FrmMaestroDetalle.frx":058A
               Key             =   ""
            EndProperty
            BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
               Picture         =   "FrmMaestroDetalle.frx":09CC
               Key             =   ""
            EndProperty
            BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
               Picture         =   "FrmMaestroDetalle.frx":0D1E
               Key             =   ""
            EndProperty
         EndProperty
      End
      Begin MSComctlLib.Toolbar Tlb_Bar 
         Height          =   390
         Index           =   1
         Left            =   120
         TabIndex        =   7
         Top             =   120
         Width           =   4815
         _ExtentX        =   8493
         _ExtentY        =   688
         ButtonWidth     =   609
         ButtonHeight    =   582
         Appearance      =   1
         ImageList       =   "ImageList1"
         _Version        =   393216
         BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
            NumButtons      =   3
            BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Enabled         =   0   'False
               Key             =   "S"
               Object.ToolTipText     =   "F4 - Buscar"
               ImageIndex      =   1
            EndProperty
            BeginProperty Button2 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Enabled         =   0   'False
               Key             =   "ST"
               Object.ToolTipText     =   "Marcar Todo"
               ImageIndex      =   2
            EndProperty
            BeginProperty Button3 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Enabled         =   0   'False
               Key             =   "QT"
               Object.ToolTipText     =   "Desmarcar Todo"
               ImageIndex      =   3
            EndProperty
         EndProperty
      End
      Begin MSComctlLib.ListView Lstw 
         Height          =   4935
         Index           =   1
         Left            =   120
         TabIndex        =   9
         Top             =   960
         Width           =   4815
         _ExtentX        =   8493
         _ExtentY        =   8705
         View            =   3
         LabelEdit       =   1
         MultiSelect     =   -1  'True
         LabelWrap       =   -1  'True
         HideSelection   =   -1  'True
         AllowReorder    =   -1  'True
         Checkboxes      =   -1  'True
         FullRowSelect   =   -1  'True
         _Version        =   393217
         ForeColor       =   -2147483640
         BackColor       =   -2147483643
         BorderStyle     =   1
         Appearance      =   1
         NumItems        =   3
         BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Text            =   "C�digo"
            Object.Width           =   2646
         EndProperty
         BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   1
            Text            =   "Nombre"
            Object.Width           =   8819
         EndProperty
         BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   2
            Object.Width           =   0
         EndProperty
      End
      Begin VB.Label Lbl_PanelB 
         Alignment       =   2  'Center
         Caption         =   "Panel B"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   120
         TabIndex        =   8
         Top             =   600
         Width           =   4815
      End
   End
   Begin VB.Frame FramEspe 
      Height          =   6015
      Left            =   120
      TabIndex        =   2
      Top             =   480
      Width           =   5055
      Begin MSComctlLib.ListView Lstw 
         Height          =   4935
         Index           =   0
         Left            =   120
         TabIndex        =   5
         Top             =   960
         Width           =   4815
         _ExtentX        =   8493
         _ExtentY        =   8705
         View            =   3
         LabelEdit       =   1
         LabelWrap       =   -1  'True
         HideSelection   =   -1  'True
         AllowReorder    =   -1  'True
         FullRowSelect   =   -1  'True
         _Version        =   393217
         Icons           =   "Img_List"
         SmallIcons      =   "Img_List"
         ForeColor       =   -2147483640
         BackColor       =   -2147483643
         BorderStyle     =   1
         Appearance      =   1
         NumItems        =   3
         BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Text            =   "C�digo"
            Object.Width           =   2646
         EndProperty
         BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   1
            Text            =   "Nombre"
            Object.Width           =   8819
         EndProperty
         BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   2
            Object.Width           =   0
         EndProperty
      End
      Begin MSComctlLib.Toolbar Tlb_Bar 
         Height          =   390
         Index           =   0
         Left            =   120
         TabIndex        =   3
         Top             =   120
         Width           =   4815
         _ExtentX        =   8493
         _ExtentY        =   688
         ButtonWidth     =   609
         ButtonHeight    =   582
         Appearance      =   1
         ImageList       =   "ImageList1"
         _Version        =   393216
         BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
            NumButtons      =   1
            BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Key             =   "S"
               Object.ToolTipText     =   "F3 - Buscar"
               ImageIndex      =   1
            EndProperty
         EndProperty
      End
      Begin VB.Label Lbl_PanelA 
         Alignment       =   2  'Center
         Caption         =   "Panel A"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   120
         TabIndex        =   4
         Top             =   600
         Width           =   4815
      End
   End
   Begin MSComctlLib.ImageList Img_List 
      Left            =   5880
      Top             =   0
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   16
      ImageHeight     =   16
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   2
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FrmMaestroDetalle.frx":1070
            Key             =   ""
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FrmMaestroDetalle.frx":173A
            Key             =   ""
         EndProperty
      EndProperty
   End
   Begin VB.Label Lbl_Asignar 
      Caption         =   "Asignaci�n"
      Height          =   255
      Left            =   240
      TabIndex        =   0
      Top             =   120
      Width           =   975
   End
End
Attribute VB_Name = "FrmMaestroDetalle"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
'Dim arrEventos() As Variant        'DEPURACION DE CODIGO
Public ListaA
Public ListaB
Public OtroCampo As String
Public R_Columna As Integer
Public R_Opcion
Public CampoPadre As String
Public CampoHijo  As String
Public CampoRela  As String
Public TablaRela  As String
Public Titulo As String
Public Titulo_PanelA As String
Public Titulo_PanelB As String
Dim Tabla As String
Private Sub CboAsignar_KeyPress(KeyAscii As Integer)
   Call Cambiar_Enter(KeyAscii)
End Sub
Private Sub CboAsignar_LostFocus()
   Lstw(0).ListItems.Clear
   Lstw(1).ListItems.Clear
   If CboAsignar.ListIndex = 0 Then
      Lbl_PanelA = Titulo_PanelA
      Lbl_PanelB = Titulo_PanelB
   Else
      Lbl_PanelB = Titulo_PanelA
      Lbl_PanelA = Titulo_PanelB
   End If
   Tlb_Bar(1).Buttons(1).Enabled = False
   Tlb_Bar(1).Buttons(2).Enabled = False
   Tlb_Bar(1).Buttons(3).Enabled = False
End Sub

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
   Select Case KeyCode
      Case vbKeyF1:
      Case vbKeyF7:
                     If Tlb_Bar(0).Buttons(1).Enabled = True Then
                        Call Tlb_Bar_ButtonClick(0, Tlb_Bar(0).Buttons(1))
                     End If
      Case vbKeyF8:
                     If Tlb_Bar(1).Buttons(1).Enabled = True Then
                        Call Tlb_Bar_ButtonClick(0, Tlb_Bar(0).Buttons(1))
                     End If
      Case vbKeyF12: Unload Me
   End Select
End Sub

Private Sub Form_Load()
   CboAsignar.AddItem R_Opcion(0)
   CboAsignar.AddItem R_Opcion(1)
   CboAsignar.ListIndex = 0
   Me.Caption = Titulo
   Me.Height = 7125
   Me.Width = 10545
   Call CenterForm(MDI_Inventarios, Me)
   Lbl_PanelA.Caption = Titulo_PanelA
   Lbl_PanelB.Caption = Titulo_PanelB
   Tabla = TablaRela
End Sub

Private Sub Lstw_Click(Index As Integer)
Dim I As Long

   If Index = 0 Then
      Tlb_Bar(1).Buttons(1).Enabled = False
      If Lstw(0).ListItems.Count = 0 Then Exit Sub
      For I = 1 To Lstw(0).ListItems.Count
         If Lstw(0).ListItems(I).Selected = True Then
            Tlb_Bar(1).Buttons(1).Enabled = True
            Tlb_Bar(1).Buttons(2).Enabled = True
            Tlb_Bar(1).Buttons(3).Enabled = True
            Exit For
         End If
      Next I
   End If
   
End Sub

Private Sub Lstw_ColumnClick(Index As Integer, ByVal ColumnHeader As MSComctlLib.ColumnHeader)
Lstw(Index).Sorted = False
If Lstw(Index).SortOrder = lvwAscending Then
   Lstw(Index).SortOrder = lvwDescending
Else
   Lstw(Index).SortOrder = lvwAscending
End If
Debug.Print ColumnHeader.Index
Select Case ColumnHeader.Index
   Case 1: Lstw(Index).SortKey = 0
   Case 2: Lstw(Index).SortKey = 1
End Select
Lstw(Index).Sorted = True
End Sub

Private Sub Lstw_ItemCheck(Index As Integer, ByVal Item As MSComctlLib.ListItem)
   If Index = 1 Then
      If Item.Checked = True Then
         Call Grabar(Item.Index)
      Else
         Call Borrar(Item.Index)
      End If
   End If
End Sub

Private Sub Lstw_ItemClick(Index As Integer, ByVal Item As MSComctlLib.ListItem)
Dim I As Integer
Dim BandItem As Byte
BandItem = 0

   If Index = 0 Then
      For I = 1 To Lstw(0).ListItems.Count
         If Lstw(0).ListItems(I).Key <> Item.Key Then Lstw(0).ListItems(I).SmallIcon = 2
      Next
      Item.SmallIcon = 1
      
      If CboAsignar.ListIndex = 0 Then
         Desde = ListaB(0) & Coma & Tabla
         Campos = ListaB(1) & Coma & ListaB(2) & Coma & ListaB(3)
         If UBound(ListaB) > 3 And OtroCampo <> "" Then
            Desde = ListaB(0) & Coma & ListaA(0) & Coma & Tabla
            Condicion = CampoPadre & "=" & Mid(Item.Key, 2, Len(Item.Key) - 1)
            Condicion = Condicion & " AND " & ListaB(4)
            Condicion = Condicion & " AND " & OtroCampo & "=" & CampoHijo
            Condicion = Condicion & " AND " & CampoPadre & "=" & ListaA(3)
            Call LoadListView(Lstw(1), Desde, Campos, OtroCampo, Condicion)
         Else
            Condicion = ListaB(3) & "=" & CampoHijo
            If ListaA(5) = "T" Then
               Condicion = Condicion & " AND " & CampoPadre & "=" & Comi & Mid(Item.Key, 2, Len(Item.Key) - 1) & Comi
            Else
               Condicion = Condicion & " AND " & CampoPadre & "=" & Mid(Item.Key, 2, Len(Item.Key) - 1)
            End If
            Call LoadListView(Lstw(1), Desde, Campos, ListaB(3), Condicion)
         End If
         
      ElseIf CboAsignar.ListIndex = 1 Then
         Desde = ListaA(0) & Coma & Tabla
         Campos = ListaA(1) & Coma & ListaA(2) & Coma & ListaA(3)
         Condicion = ListaA(3) & "=" & CampoPadre
         If ListaB(5) = "T" Then
            Condicion = Condicion & " AND " & CampoHijo & "=" & Comi & Mid(Item.Key, 2, Len(Item.Key) - 1) & Comi
         Else
            Condicion = Condicion & " AND " & CampoHijo & "=" & Mid(Item.Key, 2, Len(Item.Key) - 1)
         End If
         If UBound(ListaB) > 3 And OtroCampo <> "" Then
            Condicion = Condicion & " AND " & ListaB(4)
            Condicion = Condicion & " AND " & OtroCampo & "=" & CampoHijo
            Desde = Desde & Coma & ListaB(0)
            Debug.Print Condicion
         End If
         Call LoadListView(Lstw(1), Desde, Campos, ListaA(3), Condicion)
      End If
      
      For I = 1 To Lstw(1).ListItems.Count
         Lstw(1).ListItems(I).Checked = True
      Next I
   Else
      For I = 1 To Lstw(0).ListItems.Count
         If Lstw(0).ListItems(I).Selected = True Then
            BandItem = 1
            Exit For
         End If
      Next
      If BandItem = 0 Then
         Call Mensaje1("Seleccione un elemento en la parte izquierda", 3)
      End If
   End If
End Sub

Private Sub Tlb_Bar_ButtonClick(Index As Integer, ByVal Button As MSComctlLib.Button)
Dim Arrsel(3, 2) As Variant
Dim arr_mae() As Variant
Dim BandItem As Byte
Dim Elemento As Long
Dim I As Integer
Dim J As Integer
'Dim k As Integer   'DEPURACION DE CODIGO
                     
   BandItem = 0
   Select Case Index
      Case 0:
               If CboAsignar.ListIndex = 0 Then
                  Desde = ListaA(0)
                  Campos = ListaA(1) & Coma & ListaA(2) & Coma & ListaA(3)
               Else
                  Desde = ListaB(0)
                  Campos = ListaB(1) & Coma & ListaB(2) & Coma & ListaB(3)
               End If
               If Button.Key = "T" Then Call LoadListView(Lstw(0), Desde, Campos, ListaA(3), NUL$)
      Case 1:
               If CboAsignar.ListIndex = 0 Then
                  Desde = ListaB(0)
                  Campos = ListaB(1) & Coma & ListaB(2) & Coma & ListaB(3)
               Else
                  Desde = ListaA(0)
                  Campos = ListaA(1) & Coma & ListaA(2) & Coma & ListaA(3)
               End If
               
               Select Case Button.Key
                  Case "T": Call LoadListView(Lstw(1), Desde, Campos, ListaB(3), Condicion)
                  Case "ST":
                           If WarnMsg("Esta seguro que desea marcar todo?") Then
                              Call MouseClock
                              For I = 1 To Lstw(1).ListItems.Count
                                 If Lstw(1).ListItems(I).Checked = False Then
                                    Lstw(1).ListItems(I).Selected = True
                                    Lstw(1).ListItems(I).Checked = True
                                    Call Lstw_ItemCheck(1, Lstw(1).SelectedItem)
                                 End If
                              Next
                              Call MouseNorm
                           End If
                  Case "QT":
                           If WarnMsg("Esta seguro que desea desmarcar todo?") Then
                              Call MouseClock
                              For I = 1 To Lstw(1).ListItems.Count
                                 Lstw(1).ListItems(I).Selected = True
                                 Lstw(1).ListItems(I).Checked = False
                                 Call Lstw_ItemCheck(1, Lstw(1).SelectedItem)
                              Next
                              Call MouseNorm
                           End If
               End Select
   End Select
   
   If Button.Key = "S" Then
      Lstw(Index).SetFocus
      If Index = 0 Then
         If CboAsignar.ListIndex = 0 Then
            Arrsel(0, 0) = ListaA(1): Arrsel(1, 0) = "C�digo": Arrsel(2, 0) = "1000": Arrsel(3, 0) = "T"
            Arrsel(0, 1) = ListaA(2): Arrsel(1, 1) = "Descripci�n": Arrsel(2, 1) = "5000": Arrsel(3, 1) = "T"
            Arrsel(0, 2) = ListaA(3): Arrsel(1, 2) = " ": Arrsel(2, 2) = "0": Arrsel(3, 2) = "T"
            If UBound(ListaA) > 3 Then
               Condi = ListaA(4)
            Else
               Condi = NUL$
            End If
            arr_mae = Seleccion_General(Desde, Condi, Arrsel, 2, True, Titulo_PanelA)
         Else
            Arrsel(0, 0) = ListaB(1): Arrsel(1, 0) = "C�digo": Arrsel(2, 0) = "1000": Arrsel(3, 0) = "T"
            Arrsel(0, 1) = ListaB(2): Arrsel(1, 1) = "Descripci�n": Arrsel(2, 1) = "5000": Arrsel(3, 1) = "T"
            Arrsel(0, 2) = ListaB(3): Arrsel(1, 2) = " ": Arrsel(2, 2) = "0": Arrsel(3, 2) = "T"
            If UBound(ListaB) > 3 Then
               Condi = ListaB(4)
               If OtroCampo <> "" Then
                  Arrsel(0, 2) = OtroCampo: Arrsel(1, 2) = " ": Arrsel(2, 2) = "0": Arrsel(3, 2) = "T"
               End If
            Else
               Condi = NUL$
            End If
            arr_mae = Seleccion_General(Desde, Condi, Arrsel, 2, True, Titulo_PanelB)
         End If
      Else
         For I = 1 To Lstw(0).ListItems.Count
            If Lstw(0).ListItems(I).Selected = True Then
               BandItem = 1
               Elemento = I
               Exit For
            End If
         Next
         
         If BandItem = 0 Then
            Call Mensaje1("Seleccione un elemento en la parte izquierda", 3)
            Exit Sub
         End If
         
         If CboAsignar.ListIndex = 0 Then
            Arrsel(0, 0) = ListaB(1): Arrsel(1, 0) = "C�digo": Arrsel(2, 0) = "1000": Arrsel(3, 0) = "T"
            Arrsel(0, 1) = ListaB(2): Arrsel(1, 1) = "Descripci�n": Arrsel(2, 1) = "5000": Arrsel(3, 1) = "T"
            Arrsel(0, 2) = ListaB(3): Arrsel(1, 2) = " ": Arrsel(2, 2) = "0": Arrsel(3, 2) = "T"
            'condicion para que no busque los que ya estan cargados
            If UBound(ListaB) > 3 Then
               Condicion = ListaB(4)
               If OtroCampo <> NUL$ Then
                  Arrsel(0, 2) = OtroCampo: Arrsel(1, 2) = " ": Arrsel(2, 2) = "0": Arrsel(3, 2) = "T"
                  Condicion = ListaB(4) & " AND "
               End If
            Else
               Condicion = NUL$
            End If
'            Condicion = Condicion & ListaB(3) & " NOT IN"
'            Condicion = Condicion & "(SELECT " & ListaB(3)
'            Condicion = Condicion & " FROM " & ListaB(0) & Coma & Tabla
            If UBound(ListaB) > 3 And OtroCampo <> NUL$ Then
'               Condicion = Condicion & " WHERE " & ListaB(4)
'               Condicion = Condicion & " AND  " & OtroCampo & "=" & CampoHijo
               arr_mae = Seleccion_General(Desde, Condicion, Arrsel, 2, True, Titulo_PanelA)
            Else
'               Condicion = Condicion & " WHERE " & ListaB(3) & "=" & CampoHijo & SParC
               arr_mae = Seleccion_General(Desde, Condicion, Arrsel, 2, True, Titulo_PanelB)
            End If
'            Condicion = Condicion & " AND " & CampoPadre & "=" & Mid(Lstw(0).ListItems(Elemento).Key, 2, Len(Lstw(0).ListItems(Elemento).Key)) & ")"
            
             'arr_mae = Seleccion_General(Desde, Condicion, Arrsel, 2, True, Titulo_PanelB)
         Else
            Arrsel(0, 0) = ListaA(1): Arrsel(1, 0) = "C�digo": Arrsel(2, 0) = "1000": Arrsel(3, 0) = "T"
            Arrsel(0, 1) = ListaA(2): Arrsel(1, 1) = "Descripci�n": Arrsel(2, 1) = "5000": Arrsel(3, 1) = "T"
            Arrsel(0, 2) = ListaA(3): Arrsel(1, 2) = " ": Arrsel(2, 2) = "0": Arrsel(3, 2) = "T"
            If UBound(ListaA) > 3 Then
               Condicion = ListaA(4)
            Else
               Condicion = NUL$
            End If
            arr_mae = Seleccion_General(Desde, Condicion, Arrsel, 2, True, Titulo_PanelA)
         End If
      End If
    
      For I = 0 To UBound(arr_mae, 1) - 1
         If arr_mae(I, 0) <> NUL$ Then
            Dim itemb As ListItem
            'Verifica que no se encuentre el elemento
            Set itemb = Lstw(Index).FindItem(arr_mae(I, 0), , , 0)
            If itemb Is Nothing Then
               Lstw(Index).ListItems.Add , "L" & CStr(arr_mae(I, 2)), arr_mae(I, 0)
               For J = 1 To Lstw(0).ColumnHeaders.Count - 1
                  Lstw(Index).ListItems(Lstw(Index).ListItems.Count).SubItems(J) = arr_mae(I, J)
               Next J
            End If
         End If
      Next I
   End If
   
End Sub

Private Sub Grabar(ByVal indice As Long)
Dim I As Long
Dim BandItem As Byte
Dim IndIzq As Long

   Call MouseClock
   BandItem = 0
   For I = 1 To Lstw(0).ListItems.Count
      If Lstw(0).ListItems.Item(I).Selected = True And Lstw(0).ListItems.Item(I).SmallIcon = 1 Then
         IndIzq = I
         BandItem = 1
         Exit For
      End If
   Next I
   If BandItem = 0 Then
      Call Mensaje1("Seleccione un elemento de la izquierda", 3)
      Lstw(1).ListItems.Item(indice).Checked = False
      Call MouseNorm: Exit Sub
   End If

   Call MouseClock
   If (BeginTran(STranIUp & Tabla) <> FAIL) Then
      If CboAsignar.ListIndex = 0 Then
         Valores = CampoPadre & "=" & Mid(Lstw(0).ListItems(IndIzq).Key, 2, Len(Lstw(0).ListItems(IndIzq).Key)) & Coma
         Debug.Print Lstw(1).ListItems(indice).Key
         If ListaB(5) = "T" Then
            Valores = Valores & CampoHijo & "=" & Comi & Mid(Lstw(1).ListItems(indice).Key, 2, Len(Lstw(1).ListItems(indice).Key)) & Comi
         Else
            Valores = Valores & CampoHijo & "=" & Mid(Lstw(1).ListItems(indice).Key, 2, Len(Lstw(1).ListItems(indice).Key))
         End If
         Result = DoInsertSQL(Tabla, Valores)
         
      ElseIf CboAsignar.ListIndex = 1 Then
         If ListaB(5) = "T" Then
            Valores = CampoHijo & "=" & Comi & Mid(Lstw(0).ListItems(IndIzq).Key, 2, Len(Lstw(0).ListItems(IndIzq).Key)) & Comi & Coma
         Else
            Valores = CampoHijo & "=" & Mid(Lstw(0).ListItems(IndIzq).Key, 2, Len(Lstw(0).ListItems(IndIzq).Key)) & Coma
         End If
         If ListaA(5) = "T" Then
            Valores = Valores & CampoPadre & "=" & Comi & Mid(Lstw(1).ListItems(indice).Key, 2, Len(Lstw(1).ListItems(indice).Key)) & Comi
         Else
            Valores = Valores & CampoPadre & "=" & Mid(Lstw(1).ListItems(indice).Key, 2, Len(Lstw(1).ListItems(indice).Key))
         End If
         Result = DoInsertSQL(Tabla, Valores)
      End If
   End If
   
   If (Result <> FAIL) Then
      If (CommitTran() <> FAIL) Then
       
      Else
         Call RollBackTran
      End If
   Else
      Call RollBackTran
   End If
   Call MouseNorm

End Sub

Private Sub Borrar(ByVal indice As Long)
Dim I As Long
Dim BandItem As Byte
Dim IndIzq As Long

   Call MouseClock
   
   BandItem = 0
   For I = 1 To Lstw(0).ListItems.Count
      If Lstw(0).ListItems.Item(I).Selected = True And Lstw(0).ListItems.Item(I).SmallIcon = 1 Then
         IndIzq = I
         BandItem = 1
         Exit For
      End If
   Next I
   If BandItem = 0 Then Call Mensaje1("Seleccione un elemento de la izquierda", 3): Call MouseNorm: Exit Sub

   Call MouseClock
   If (BeginTran(STranIUp & Tabla) <> FAIL) Then
      Debug.Print ListaA(5) & vbTab & ListaB(5)
      If CboAsignar.ListIndex = 0 Then
         If ListaA(5) = "T" Then
            Condicion = CampoPadre & "=" & Comi & Mid(Lstw(0).ListItems(IndIzq).Key, 2, Len(Lstw(0).ListItems(IndIzq).Key)) & Comi
         Else
            Condicion = CampoPadre & "=" & Mid(Lstw(0).ListItems(IndIzq).Key, 2, Len(Lstw(0).ListItems(IndIzq).Key))
         End If
         If ListaB(5) = "T" Then
            Condicion = Condicion & " AND " & CampoHijo & "=" & Comi & Mid(Lstw(1).ListItems(indice).Key, 2, Len(Lstw(1).ListItems(indice).Key)) & Comi
         Else
            Condicion = Condicion & " AND " & CampoHijo & "=" & Mid(Lstw(1).ListItems(indice).Key, 2, Len(Lstw(1).ListItems(indice).Key))
         End If
      ElseIf CboAsignar.ListIndex = 1 Then
         If ListaB(5) = "T" Then
            Condicion = CampoHijo & "=" & Comi & Mid(Lstw(0).ListItems(IndIzq).Key, 2, Len(Lstw(0).ListItems(IndIzq).Key)) & Comi
         Else
            Condicion = CampoHijo & "=" & Mid(Lstw(0).ListItems(IndIzq).Key, 2, Len(Lstw(0).ListItems(IndIzq).Key))
         End If
         If ListaA(5) = "T" Then
            Condicion = Condicion & " AND " & CampoPadre & "=" & Comi & Mid(Lstw(1).ListItems(indice).Key, 2, Len(Lstw(1).ListItems(indice).Key)) & Comi
         Else
            Condicion = Condicion & " AND " & CampoPadre & "=" & Mid(Lstw(1).ListItems(indice).Key, 2, Len(Lstw(1).ListItems(indice).Key))
         End If
      End If
      Result = DoDelete(Tabla, Condicion)
   End If
   
   If (Result <> FAIL) Then
      If (CommitTran() <> FAIL) Then
       
      Else
         Call RollBackTran
      End If
   Else
      Call RollBackTran
   End If
   
   Call MouseNorm

End Sub

