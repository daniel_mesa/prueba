VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.1#0"; "Mscomctl.ocx"
Object = "{C932BA88-4374-101B-A56C-00AA003668DC}#1.1#0"; "MSMASK32.OCX"
Object = "{0BA686C6-F7D3-101A-993E-0000C0EF6F5E}#1.0#0"; "THREED32.OCX"
Begin VB.Form FrmAudtMov 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Parametros para SALDOS"
   ClientHeight    =   5010
   ClientLeft      =   45
   ClientTop       =   375
   ClientWidth     =   7740
   Icon            =   "FrmAudtMov.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   5010
   ScaleWidth      =   7740
   Begin VB.Frame FrmFechas 
      Height          =   975
      Left            =   120
      TabIndex        =   4
      Top             =   3960
      Width           =   3855
      Begin VB.CheckBox ChkFechas 
         Caption         =   "Condicionar las Fechas del Reporte"
         Height          =   315
         Left            =   120
         TabIndex        =   5
         Top             =   120
         Width           =   3015
      End
      Begin MSMask.MaskEdBox MskFechaFin 
         Height          =   315
         Left            =   2640
         TabIndex        =   9
         Top             =   480
         Width           =   1215
         _ExtentX        =   2143
         _ExtentY        =   556
         _Version        =   393216
         MaxLength       =   10
         Mask            =   "##/##/####"
         PromptChar      =   "_"
      End
      Begin MSMask.MaskEdBox MskFechaIni 
         Height          =   315
         Left            =   720
         TabIndex        =   7
         Top             =   480
         Width           =   1215
         _ExtentX        =   2143
         _ExtentY        =   556
         _Version        =   393216
         MaxLength       =   10
         Mask            =   "##/##/####"
         PromptChar      =   "_"
      End
      Begin VB.Label LblHasta 
         Caption         =   "Hasta:"
         Height          =   255
         Left            =   2040
         TabIndex        =   8
         Top             =   480
         Width           =   495
      End
      Begin VB.Label LblDesde 
         Caption         =   "Desde:"
         Height          =   255
         Left            =   120
         TabIndex        =   6
         Top             =   480
         Width           =   495
      End
   End
   Begin VB.ComboBox CboTDocumento 
      Height          =   315
      Left            =   3600
      Style           =   2  'Dropdown List
      TabIndex        =   2
      Top             =   120
      Width           =   3975
   End
   Begin VB.CheckBox ChkTTerceros 
      Caption         =   "Todos los Terceros"
      Height          =   255
      Left            =   120
      TabIndex        =   0
      Top             =   120
      Width           =   1815
   End
   Begin MSComctlLib.ListView LstTerceros 
      Height          =   3375
      Left            =   120
      TabIndex        =   3
      Top             =   480
      Width           =   7500
      _ExtentX        =   13229
      _ExtentY        =   5953
      View            =   3
      LabelEdit       =   1
      MultiSelect     =   -1  'True
      LabelWrap       =   -1  'True
      HideSelection   =   0   'False
      AllowReorder    =   -1  'True
      Checkboxes      =   -1  'True
      FullRowSelect   =   -1  'True
      GridLines       =   -1  'True
      _Version        =   393217
      ForeColor       =   -2147483640
      BackColor       =   -2147483643
      BorderStyle     =   1
      Appearance      =   1
      NumItems        =   2
      BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Text            =   "Nombre del Art�culo"
         Object.Width           =   7761
      EndProperty
      BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   1
         Text            =   "C�digo"
         Object.Width           =   3881
      EndProperty
   End
   Begin Threed.SSCommand CmdImprimir 
      Height          =   855
      Left            =   6600
      TabIndex        =   10
      ToolTipText     =   "Imprimir"
      Top             =   3960
      Width           =   975
      _Version        =   65536
      _ExtentX        =   1720
      _ExtentY        =   1508
      _StockProps     =   78
      Caption         =   "&IMPRIMIR"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   6
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      RoundedCorners  =   0   'False
      Picture         =   "FrmAudtMov.frx":058A
   End
   Begin VB.Label LblTDocumento 
      Caption         =   "Tipo de Documento:"
      Height          =   195
      Left            =   2040
      TabIndex        =   1
      Top             =   120
      Width           =   1455
   End
End
Attribute VB_Name = "FrmAudtMov"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'---------------------------------------------------------------------------------------
' Module    : FrmAudtMov
' DateTime  : 05/03/2018 10:38
' Author    : jairo_parra
' Purpose   : JLPB T41459-R37519 'Formulario para listar la auditoria de los movimientos contables
'---------------------------------------------------------------------------------------

Option Explicit

Dim VrArr() As Variant 'Almacena los resultados devueltos por una consulta
Dim InI As Integer 'Utilizada para los recorridos del for
Dim VrDocumento() As Variant 'Almacena los codigos de los tipos de documentos

'---------------------------------------------------------------------------------------
' Procedure : CboTDocumento_GotFocus
' DateTime  : 05/03/2018 10:38
' Author    : jairo_parra
' Purpose   : JLPB T41459-R37519 'Permite que se listen los items cuendo este tiene el foco
'---------------------------------------------------------------------------------------
'
Private Sub CboTDocumento_GotFocus()
   SendKeys "{F4}"
End Sub

'---------------------------------------------------------------------------------------
' Procedure : CboTDocumento_KeyPress
' DateTime  : 05/03/2018 10:38
' Author    : jairo_parra
' Purpose   : JLPB T41459-R37519 'Permite que el enter funcione como tabulador
'---------------------------------------------------------------------------------------
'
Private Sub CboTDocumento_KeyPress(KeyAscii As Integer)
   Call Cambiar_Enter(KeyAscii)
End Sub

'---------------------------------------------------------------------------------------
' Procedure : ChkFechas_Click
' DateTime  : 05/03/2018 10:39
' Author    : jairo_parra
' Purpose   : JLPB T41459-R37519 'Valida si se deben inhabilitar los campos de fecha segun la amrcaci�n del check
'---------------------------------------------------------------------------------------
'
Private Sub ChkFechas_Click()
   Me.MskFechaIni.Enabled = False
   Me.MskFechaFin.Enabled = False
   If Not CBool(Me.ChkFechas.Value) = True Then Exit Sub
   Me.MskFechaIni.Enabled = True
   Me.MskFechaFin.Enabled = True
End Sub

'---------------------------------------------------------------------------------------
' Procedure : ChkFechas_KeyPress
' DateTime  : 30/04/2018 15:56
' Author    : daniel_mesa
' Purpose   : DRMG T43539 'Permite que el enter funcione como tabulador
'---------------------------------------------------------------------------------------
'
Private Sub ChkFechas_KeyPress(KeyAscii As Integer)
   Call Cambiar_Enter(KeyAscii)
End Sub

'---------------------------------------------------------------------------------------
' Procedure : ChkTTerceros_KeyPress
' DateTime  : 05/03/2018 10:39
' Author    : jairo_parra
' Purpose   : JLPB T41459-R37519 'Permite que el enter funcione como tabulador
'---------------------------------------------------------------------------------------
'
Private Sub ChkTTerceros_KeyPress(KeyAscii As Integer)
   Call Cambiar_Enter(KeyAscii)
End Sub

'---------------------------------------------------------------------------------------
' Procedure : CmdImprimir_Click
' DateTime  : 05/03/2018 10:39
' Author    : jairo_parra
' Purpose   : JLPB T41459-R37519 'Cnsulta los registros segun el filtro y si encuentra genera el informe de los contrario genera mensaje informativo.
'---------------------------------------------------------------------------------------
'
Private Sub CmdImprimir_Click()
   Dim StSql As String 'Almacena la consulta a ejecutar
   Dim Seleccion As MSComctlLib.ListItem 'Almacena el registro de la lista para validar si se selecciono
   Dim StTerceros As String 'Almacena los terceros seleccionados
   
   If CboTDocumento.ListIndex = -1 Or CboTDocumento = NUL$ Then
      Call MsgBox("Debe seleccionar el tipo de documento.", vbCritical, App.Title)
      If CboTDocumento.Enabled Then CboTDocumento.SetFocus
      Exit Sub
   End If
   StTerceros = NUL$
   If ChkTTerceros.Value = 0 Then
      For Each Seleccion In Me.LstTerceros.ListItems
         If Seleccion.Selected Then StTerceros = StTerceros & Comi & Seleccion.ListSubItems("COD").Text & Comi & Coma
      Next
   End If
   If ChkTTerceros.Value = 0 And StTerceros = NUL$ Then
      Call MsgBox("Debe seleccionar los terceros.", vbCritical, App.Title)
      If LstTerceros.Enabled Then LstTerceros.SetFocus
      Exit Sub
   End If
   'DRMG T43535 INICIO 'SI SE ESCOGI� COMPRA SE CAMBIA LA CONSULTA
   If CboTDocumento = "COMPRA INVENTARIOS" Then
      StSql = "SELECT NU_CONMOV_AUDT,NU_AUTO_COMP AS NU_COMP_ENCA,NO_NOMB_COMP AS TX_NOMB_COMP,CD_CODI_TERC,CD_CODI_CUEN,TX_NATU_AUDT,NU_VALOR_AUDT,"
      StSql = StSql & "NU_VALORB_AUDT,NU_VALORIVA_AUDT,NO_NOMB_CECO,TX_IDEN_USUA,CD_CODI_TERC_COMP AS CD_CODI_TERC_ENCA,NU_ORIALM_AUDT "
      StSql = StSql & "INTO IN_AUDMOV_TEMP  FROM IN_AUDMOVCONT "
      StSql = StSql & "INNER JOIN IN_COMPRA ON NU_AUTENCA_AUDT=IN_COMPRA.NU_AUTO_COMP "
      StSql = StSql & "INNER JOIN TC_COMPROBANTE ON CD_CODI_COMP = TX_COMP_AUDT "
      StSql = StSql & "LEFT JOIN TERCERO ON TX_TERC_AUDT=CD_CODI_TERC "
      StSql = StSql & "LEFT JOIN CENTRO_COSTO ON TX_CC_AUDT=CD_CODI_CECO "
      StSql = StSql & "LEFT JOIN CUENTAS ON TX_CUENTA_AUDT=CD_CODI_CUEN "
      StSql = StSql & "INNER JOIN USUARIO ON NU_AUTUSU_AUDT=NU_AUTO_USUA "
      If ChkFechas.Value Then StSql = StSql & " AND FE_FECH_COMP BETWEEN " & FFechaCon(MskFechaIni) & " AND " & FFechaCon(CDate(MskFechaFin) + 1)
      If ChkTTerceros.Value = 0 Then StSql = StSql & " AND CD_CODI_TERC_COMP IN (" & Mid(StTerceros, 1, Len(StTerceros) - 1) & ")"
   Else
   'DRMG T43535 FIN
      StSql = "SELECT NU_CONMOV_AUDT,NU_COMP_ENCA,TX_NOMB_COMP,CD_CODI_TERC,CD_CODI_CUEN,TX_NATU_AUDT,NU_VALOR_AUDT,NU_VALORB_AUDT,NU_VALORIVA_AUDT,"
      StSql = StSql & "NO_NOMB_CECO,TX_IDEN_USUA,CD_CODI_TERC_ENCA,NU_ORIALM_AUDT INTO IN_AUDMOV_TEMP"
      StSql = StSql & " FROM IN_AUDMOVCONT INNER JOIN IN_ENCABEZADO ON NU_AUTENCA_AUDT=NU_AUTO_ENCA"
      StSql = StSql & " INNER JOIN IN_DOCUMENTO ON NU_AUTO_DOCU_ENCA=NU_AUTO_DOCU"
      StSql = StSql & " INNER JOIN IN_COMPROBANTE ON (NU_AUTO_COMP_ENCA=NU_AUTO_COMP"
      StSql = StSql & " AND NU_AUTO_DOCU=NU_AUTO_DOCU_COMP)"
      StSql = StSql & " LEFT JOIN TERCERO ON TX_TERC_AUDT=CD_CODI_TERC"
      StSql = StSql & " LEFT JOIN CENTRO_COSTO ON TX_CC_AUDT=CD_CODI_CECO"
      StSql = StSql & " LEFT JOIN CUENTAS ON TX_CUENTA_AUDT=CD_CODI_CUEN"
      StSql = StSql & " INNER JOIN USUARIO ON NU_AUTUSU_AUDT=NU_AUTO_USUA"
      StSql = StSql & " WHERE NU_AUTO_DOCU=" & VrDocumento(CboTDocumento.ListIndex)
      If ChkFechas.Value Then StSql = StSql & " AND FE_CREA_ENCA BETWEEN " & FFechaCon(MskFechaIni) & " AND " & FFechaCon(CDate(MskFechaFin) + 1)
      If ChkTTerceros.Value = 0 Then StSql = StSql & " AND CD_CODI_TERC_ENCA IN (" & Mid(StTerceros, 1, Len(StTerceros) - 1) & ")"
   End If 'DRMG T43535
   StSql = StSql & " ORDER BY NU_ORIALM_AUDT,NU_IMPT_AUDT"
   If ExisteTABLA("IN_AUDMOV_TEMP") Then Result = EliminarTabla("IN_AUDMOV_TEMP")
   Result = ExecSQL(StSql)
   If Result <> FAIL Then
      If Val(fnDevDato("IN_AUDMOV_TEMP", "COUNT(*)", NUL$, True)) > 0 Then
         Call Limpiar_CrysListar
         Call Listar1("InfAudMov.rpt", NUL$, NUL$, crptToWindow, "AUDITORIA MOVIMIENTOS", MDI_Inventarios)
         If ExisteTABLA("IN_AUDMOV_TEMP") Then Result = EliminarTabla("IN_AUDMOV_TEMP")
      Else
         Call MsgBox("No se encontraron registros.", vbCritical, App.Title)
      End If
   End If
End Sub

'---------------------------------------------------------------------------------------
' Procedure : Form_Load
' DateTime  : 05/03/2018 10:39
' Author    : jairo_parra
' Purpose   : JLPB T41459-R37519 'Carga los terceros y los documentos
'---------------------------------------------------------------------------------------
'
Private Sub Form_Load()
   Call CenterForm(MDI_Inventarios, Me)
   Call Leer_Permiso(Me.Name, CmdImprimir, "I")
   If Len(FormatDateTime(Date, vbShortDate)) = 8 Then
      Me.MskFechaIni.Mask = "##/##/##"
      Me.MskFechaFin.Mask = "##/##/##"
   Else
      Me.MskFechaIni.Mask = "##/##/####"
      Me.MskFechaFin.Mask = "##/##/####"
   End If
   Me.MskFechaIni.Text = FormatDateTime(DateAdd("d", -(Day(Date) - 1), Date), vbShortDate)
   Me.MskFechaFin.Text = FormatDateTime(DateAdd("d", -1, DateAdd("M", 1, DateAdd("d", -(Day(Date) - 1), Date))), vbShortDate)
   Me.ChkFechas.Value = False
   Me.MskFechaIni.Enabled = False
   Me.MskFechaFin.Enabled = False
   Me.LstTerceros.ListItems.Clear
   Me.LstTerceros.Checkboxes = False
   Me.LstTerceros.MultiSelect = True
   Me.LstTerceros.HideSelection = False
   Me.LstTerceros.ColumnHeaders.Clear
   Me.LstTerceros.ColumnHeaders.Add , "NIT", "NIT", 0.3 * Me.LstTerceros.Width
   Me.LstTerceros.ColumnHeaders.Add , "NOM", "Nombre", 0.68 * Me.LstTerceros.Width
   Me.LstTerceros.ColumnHeaders.Add , "COD", "NIT", 0 * Me.LstTerceros.Width
   ReDim VrArr(1, 0)
   Campos = "CD_CODI_TERC,NO_NOMB_TERC"
   Desde = "TERCERO INNER JOIN PARAMETROS_IMPUESTOS ON CD_CODI_TERC=CD_CODI_TERC_PAIM"
   Condicion = "NU_ESTADO_TERC<>1 ORDER BY CD_CODI_TERC"
   Result = LoadMulData(Desde, Campos, Condicion, VrArr())
   If Result <> FAIL And Encontro Then
      For InI = 0 To UBound(VrArr, 2)
          Me.LstTerceros.ListItems.Add , "NIT" & VrArr(0, InI), VrArr(0, InI)
          Me.LstTerceros.ListItems("NIT" & VrArr(0, InI)).ListSubItems.Add , "NOM", VrArr(1, InI)
          Me.LstTerceros.ListItems("NIT" & VrArr(0, InI)).ListSubItems.Add , "COD", VrArr(0, InI)
      Next
   End If
   If Result <> FAIL Then
      'DRMG T43543 INICIO SE DEJA COMENTARIO LA PRIMERA LINEA Y
      'SE AGREGA CONDICION PARA QUE SOLO TRAIGA LOS QUE GENERAN MOVIMIENTO
      'Result = loadctrl("IN_DOCUMENTO ORDER BY TX_NOMB_DOCU", "TX_NOMB_DOCU", "NU_AUTO_DOCU", CboTDocumento, VrDocumento(), NUL$)
      Condi = "NU_AUTO_DOCU NOT IN(1,2,5,13,14,15,16,18,20,21,22,23,27,29,30) ORDER BY TX_NOMB_DOCU"
      Result = loadctrl("IN_DOCUMENTO", "TX_NOMB_DOCU", "NU_AUTO_DOCU", CboTDocumento, VrDocumento(), Condi)
      'DRMG T43543 FIN
      If (Result = FAIL) Then
         Call Mensaje1("Error cargando la tabla [IN_DOCUMENTO]", 1)
         Exit Sub
      Else 'DRMG T43535
         CboTDocumento.AddItem ("COMPRA INVENTARIOS") 'DRMG T43535
      End If
   End If
End Sub

'---------------------------------------------------------------------------------------
' Procedure : lstTERCEROS_ColumnClick
' DateTime  : 05/03/2018 10:39
' Author    : jairo_parra
' Purpose   : JLPB T41459-R37519 'Permite que se puedan organizar las columnas del grid cuendo se oprima el encabezado
'---------------------------------------------------------------------------------------
'
Private Sub lstTERCEROS_ColumnClick(ByVal ColumnHeader As MSComctlLib.ColumnHeader)
   Me.LstTerceros.SortKey = ColumnHeader.Index - 1
   Me.LstTerceros.Sorted = True
End Sub

'---------------------------------------------------------------------------------------
' Procedure : LstTERCEROS_KeyPress
' DateTime  : 05/03/2018 10:39
' Author    : jairo_parra
' Purpose   : JLPB T41459-R37519 'Permite que el enter funcione como tabulador
'---------------------------------------------------------------------------------------
'
Private Sub LstTERCEROS_KeyPress(KeyAscii As Integer)
   Call Cambiar_Enter(KeyAscii)
End Sub

'---------------------------------------------------------------------------------------
' Procedure : MskFechaFin_KeyPress
' DateTime  : 05/03/2018 10:39
' Author    : jairo_parra
' Purpose   : JLPB T41459-R37519 'Permite que el enter funcione como tabulador
'---------------------------------------------------------------------------------------
'
Private Sub MskFechaFin_KeyPress(KeyAscii As Integer)
   Call Cambiar_Enter(KeyAscii)
End Sub

'---------------------------------------------------------------------------------------
' Procedure : MskFechaFin_LostFocus
' DateTime  : 05/03/2018 10:39
' Author    : jairo_parra
' Purpose   : JLPB T41459-R37519 'Valida que la fecha final no sea mayor a la inicial
'---------------------------------------------------------------------------------------
'
Private Sub MskFechaFin_LostFocus()
   'If IsDate(Me.MskFechaFin.Text) Then 'DRMG T43366 Se deja en comentario
   If ValFecha(Me.MskFechaFin, 1) <> FAIL Then 'DRMG T43366 se agrega validacion de la fechas
      If DateDiff("d", CDate(Me.MskFechaIni.Text), CDate(Me.MskFechaFin)) < 0 Then
         Call MsgBox("La fecha final debe ser mayor a la inicial.", vbCritical, App.Title)
         Me.MskFechaFin.SetFocus
      End If
   'DRMG T43366 INICIO Se deja en comentario las siguientes lineas
   'Else
      'Me.MskFechaFin.Text = FormatDateTime(DateAdd("d", -1, DateAdd("M", 1, DateAdd("d", -(Day(Date) - 1), Date))), vbShortDate)
      'Me.MskFechaFin.SetFocus
   'DRMG T43366 FIN
   End If
End Sub

'---------------------------------------------------------------------------------------
' Procedure : MskFechaIni_KeyPress
' DateTime  : 05/03/2018 10:39
' Author    : jairo_parra
' Purpose   : JLPB T41459-R37519 'Permite que el enter funcione como tabulador
'---------------------------------------------------------------------------------------
'
Private Sub MskFechaIni_KeyPress(KeyAscii As Integer)
   Call Cambiar_Enter(KeyAscii)
End Sub

'---------------------------------------------------------------------------------------
' Procedure : MskFechaIni_LostFocus
' DateTime  : 05/03/2018 10:39
' Author    : jairo_parra
' Purpose   : JLPB T41459-R37519 'Valida la fecha ingresada
'---------------------------------------------------------------------------------------
'
Private Sub MskFechaIni_LostFocus()
   'DRMG T43366 INICIO SE DEJA EN COMENTARIO LAS SIGUIENTES TRES LINEAS
   'ADEMAS SE AGREGA LA VALIDACION DE LA FECHA
   'If IsDate(Me.MskFechaIni.Text) Then Exit Sub
      'Me.MskFechaIni.Text = FormatDateTime(DateAdd("d", -(Day(Date) - 1), Date), vbShortDate)
      'Me.MskFechaIni.SetFocus
   Call ValFecha(Me.MskFechaIni, 1)
   'DRMG T43366 FIN
End Sub
