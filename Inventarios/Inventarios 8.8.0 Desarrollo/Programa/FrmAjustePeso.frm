VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Object = "{C932BA88-4374-101B-A56C-00AA003668DC}#1.1#0"; "MSMASK32.OCX"
Object = "{0BA686C6-F7D3-101A-993E-0000C0EF6F5E}#1.0#0"; "THREED32.OCX"
Begin VB.Form FrmAjustePeso 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Diferencia ajuste al peso"
   ClientHeight    =   6495
   ClientLeft      =   45
   ClientTop       =   375
   ClientWidth     =   11775
   Icon            =   "FrmAjustePeso.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   6495
   ScaleWidth      =   11775
   Begin VB.Frame fraRANGO 
      Height          =   2175
      Left            =   8280
      TabIndex        =   3
      Top             =   4200
      Width           =   3495
      Begin VB.CheckBox chkFCRANGO 
         Caption         =   "Condicionar las fechas del reporte"
         Height          =   315
         Left            =   120
         TabIndex        =   4
         Top             =   240
         Width           =   3015
      End
      Begin MSMask.MaskEdBox txtFCDESDE 
         Height          =   315
         Left            =   960
         TabIndex        =   5
         Top             =   600
         Width           =   1215
         _ExtentX        =   2143
         _ExtentY        =   556
         _Version        =   393216
         PromptChar      =   "_"
      End
      Begin MSMask.MaskEdBox txtFCHASTA 
         Height          =   315
         Left            =   960
         TabIndex        =   6
         Top             =   960
         Width           =   1215
         _ExtentX        =   2143
         _ExtentY        =   556
         _Version        =   393216
         PromptChar      =   "_"
      End
      Begin Threed.SSCommand cmdIMPRIMIR 
         Height          =   735
         Index           =   2
         Left            =   2400
         TabIndex        =   7
         Top             =   600
         Width           =   855
         _Version        =   65536
         _ExtentX        =   1508
         _ExtentY        =   1296
         _StockProps     =   78
         Caption         =   "&IMPRIMIR"
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   6
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         RoundedCorners  =   0   'False
         Picture         =   "FrmAjustePeso.frx":058A
      End
      Begin VB.Label lblDESDE 
         Caption         =   "Desde:"
         Height          =   255
         Left            =   120
         TabIndex        =   9
         Top             =   600
         Width           =   735
      End
      Begin VB.Label LblHasta 
         Caption         =   "Hasta:"
         Height          =   255
         Left            =   120
         TabIndex        =   8
         Top             =   960
         Width           =   735
      End
   End
   Begin VB.ComboBox cboTIPO 
      Height          =   315
      Left            =   2400
      TabIndex        =   2
      Text            =   "Combo1"
      Top             =   360
      Width           =   3135
   End
   Begin VB.CheckBox ChkGrupos 
      Alignment       =   1  'Right Justify
      Caption         =   "Todos Los grupos"
      Height          =   255
      Left            =   240
      TabIndex        =   1
      Top             =   960
      Width           =   2175
   End
   Begin VB.CheckBox ChkBodegas 
      Alignment       =   1  'Right Justify
      Caption         =   "Todas las Bodegas"
      Height          =   255
      Left            =   240
      TabIndex        =   0
      Top             =   3960
      Width           =   1935
   End
   Begin MSComctlLib.ListView lstBODEGAS 
      Height          =   2055
      Left            =   240
      TabIndex        =   10
      Top             =   4320
      Width           =   7935
      _ExtentX        =   13996
      _ExtentY        =   3625
      View            =   3
      LabelEdit       =   1
      MultiSelect     =   -1  'True
      LabelWrap       =   -1  'True
      HideSelection   =   -1  'True
      FullRowSelect   =   -1  'True
      GridLines       =   -1  'True
      _Version        =   393217
      ForeColor       =   -2147483640
      BackColor       =   -2147483643
      BorderStyle     =   1
      Appearance      =   1
      NumItems        =   0
   End
   Begin MSComctlLib.ListView lstGRUPOS 
      Height          =   2655
      Left            =   240
      TabIndex        =   11
      Top             =   1200
      Width           =   11460
      _ExtentX        =   20214
      _ExtentY        =   4683
      View            =   3
      LabelEdit       =   1
      MultiSelect     =   -1  'True
      LabelWrap       =   -1  'True
      HideSelection   =   0   'False
      AllowReorder    =   -1  'True
      Checkboxes      =   -1  'True
      FullRowSelect   =   -1  'True
      GridLines       =   -1  'True
      _Version        =   393217
      ForeColor       =   -2147483640
      BackColor       =   -2147483643
      BorderStyle     =   1
      Appearance      =   1
      NumItems        =   2
      BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Text            =   "Nombre del Art�culo"
         Object.Width           =   7761
      EndProperty
      BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   1
         Text            =   "C�digo"
         Object.Width           =   3881
      EndProperty
   End
   Begin MSComctlLib.ProgressBar pgbLLEVO 
      Height          =   255
      Left            =   7200
      TabIndex        =   12
      Top             =   360
      Visible         =   0   'False
      Width           =   4455
      _ExtentX        =   7858
      _ExtentY        =   450
      _Version        =   393216
      Appearance      =   1
   End
   Begin VB.Label LblTipo 
      Caption         =   "Tipo de Art�culos:"
      Height          =   315
      Left            =   240
      TabIndex        =   13
      Top             =   360
      Width           =   1575
   End
End
Attribute VB_Name = "FrmAjustePeso"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False

Option Explicit
Dim StTipoArt As String
Dim ArrUXB() As Variant

Private Sub cboTIPO_LostFocus()
Dim Incont As Integer
    
    Me.lstBODEGAS.ListItems.Clear
    Me.lstGRUPOS.ListItems.Clear
    Me.pgbLLEVO.Value = 0
    Me.pgbLLEVO.Visible = True
    Select Case Me.cboTIPO.ListIndex
    Case Is = -1
        StTipoArt = "0"
    Case Is = 0
        StTipoArt = "V"
    Case Is = 1
        StTipoArt = "C"
    End Select
    Campos = "NU_AUTO_BODE, NU_AUTO_BODE_BODE, TX_CODI_BODE, TX_NOMB_BODE, CD_CODI_CECO_BODE, TX_VENTA_BODE"
    Desde = "IN_BODEGA"
    Condi = "TX_VENTA_BODE='" & StTipoArt & "' ORDER BY TX_NOMB_BODE"
    
    ReDim ArrUXB(5, 0)
    Result = LoadMulData(Desde, Campos, Condi, ArrUXB())
    
    
    If Result = FAIL Then GoTo FALLO
    On Error GoTo SIGUI
    For Incont = 0 To UBound(ArrUXB, 2)
        Me.lstBODEGAS.ListItems.Add , "B" & ArrUXB(2, Incont), ArrUXB(3, Incont)
        Me.lstBODEGAS.ListItems("B" & ArrUXB(2, Incont)).ListSubItems.Add , "COD", ArrUXB(2, Incont)
SIGUI:
    Next
    On Error GoTo 0
    Campos = "CD_CODI_GRUP, DE_DESC_GRUP"
    Desde = "GRUP_ARTICULO"
    ReDim ArrUXB(2, 0)
    Result = LoadMulData(Desde, Campos, "", ArrUXB())
    
    If Result = FAIL Then GoTo FALLO
    If Not Encontro Then GoTo NOENC
    Me.pgbLLEVO.Min = 0
    Me.pgbLLEVO.Value = 0
    Me.pgbLLEVO.Max = UBound(ArrUXB, 2) + 1
    
    For Incont = 0 To UBound(ArrUXB, 2)
        Me.pgbLLEVO.Value = Incont
        Me.lstGRUPOS.ListItems.Add , "G" & ArrUXB(0, Incont), Trim(ArrUXB(1, Incont))
        Me.lstGRUPOS.ListItems("G" & ArrUXB(0, Incont)).Tag = ArrUXB(0, Incont)
        
        Me.lstGRUPOS.ListItems("G" & ArrUXB(0, Incont)).ListSubItems.Add , "COAR", Trim(ArrUXB(0, Incont))
    Next
NOENC:
FALLO:
    Me.pgbLLEVO.Visible = False


End Sub

Private Sub chkfcRANGO_Click()
    Me.txtFCDESDE.Enabled = False
    Me.txtFCHASTA.Enabled = False
    If Not CBool(Me.chkFCRANGO.Value) = True Then Exit Sub
    Me.txtFCDESDE.Enabled = True
    Me.txtFCHASTA.Enabled = True
End Sub

Private Sub CmdImprimir_Click(Index As Integer)


    Dim Renglon As MSComctlLib.ListItem
    Dim strGRUPOS As String
    Dim strBODEGAS As String
    Dim strTITRANGO As String
    Dim strTITFECHA As String
    Dim FecDesde As Date
    Dim FecHasta As Date
    Dim SQL As String
    
    FecDesde = CDate(Me.txtFCDESDE.Text)
    FecHasta = CDate(Me.txtFCHASTA.Text)
    
    Call Limpiar_CrysListar
    
    If Me.lstGRUPOS.ListItems.Count = 0 Then Exit Sub
    
    If Me.ChkGrupos.Value = 0 Then
        For Each Renglon In Me.lstGRUPOS.ListItems
            If Renglon.Selected Then strGRUPOS = strGRUPOS & IIf(Len(strGRUPOS) > 0, " OR ", "") & "(ARTICULO.CD_GRUP_ARTI)='" & Renglon.ListSubItems("COAR").Text & "'"
        Next
    End If
    If Me.ChkBodegas.Value = 0 Then
        For Each Renglon In Me.lstBODEGAS.ListItems
            If Renglon.Selected Then strBODEGAS = strBODEGAS & IIf(Len(strBODEGAS) > 0, " OR ", "") & "(IN_BODEGA.TX_CODI_BODE)='" & Renglon.ListSubItems("COD").Text & "'"
        Next
    End If
    
    If Len(strGRUPOS) > 0 Then strGRUPOS = "(" & strGRUPOS & ")"
    If Len(strBODEGAS) > 0 Then strBODEGAS = "(" & strBODEGAS & ")"
    If Len(strGRUPOS) > 0 And Len(strBODEGAS) > 0 Then
        strGRUPOS = strGRUPOS & " AND " & strBODEGAS
    Else
        strGRUPOS = strGRUPOS & strBODEGAS
    End If
    

    
    strTITRANGO = "TODOS LOS COMPROBANTES"
    strTITFECHA = "A LA FECHA"

    If CBool(Me.chkFCRANGO.Value) Then
        FecDesde = CDate(Me.txtFCDESDE.Text): FecHasta = CDate(Me.txtFCHASTA.Text)
        FecDesde = DateAdd("d", -1, FecDesde): FecHasta = DateAdd("d", 1, FecHasta)
        strTITFECHA = "RANGO DE FECHAS: Del  " & Me.txtFCDESDE.Text & "  al  " & Me.txtFCHASTA.Text
    End If

    strGRUPOS = strGRUPOS & IIf(Len(strGRUPOS) > 0, " AND ", "") & "(ARTICULO.TX_PARA_ARTI)='" & StTipoArt & Comi & " AND (IN_BODEGA.TX_VENTA_BODE)='" & StTipoArt & Comi

'    MDI_Inventarios.Crys_Listar.Formulas(9) = "RAGFecha='" & strTITFECHA & Comi
'    MDI_Inventarios.Crys_Listar.Formulas(10) = "ENTIDAD=" & Comi & Trim(Dueno.Nombre) & ", NIT:" & Trim(Dueno.Nit) & Comi
    
       
'DAHV M6374
        'Consulta General
    
'        SQL = "SELECT FE_FECH_DETA, TX_CODI_BODE, TX_NOMB_BODE,T3.NO_NOMB_COMP,T3.TIPO_MOVIMIENTO_INVE,  CD_GRUP_ARTI,CD_ENTR_RGB,"
'        SQL = SQL & " T3.NU_MOVI_CONTA , T3.NU_MOVI_INVE, T3.NU_AUTO_ENCA, T3.VALOR_INVE, T3.valor"
'        SQL = SQL & " ,CD_SALI_RGB,CD_INGR_RGB,CD_GAST_RGB,CD_COST_RGB,CD_AJDB_RGB,CD_AJCR_RGB,CD_TRAN_RGB"
'        SQL = SQL & "  INTO AJUSTEPESO_TEMP"
'        SQL = SQL & " FROM IN_DETALLE,ARTICULO, GRUP_ARTICULO, IN_R_GRUP_BODE, IN_BODEGA,"
'        SQL = SQL & " (SELECT T2.NO_NOMB_COMP,T1.TIPO_MOVIMIENTO_INVE, T2.NU_MOVI_CONTA,T2.VALOR,T1.NU_MOVI_INVE,T1.VALOR_INVE,T1.NU_AUTO_ENCA,T1.NU_AUTO_BODEORG_ENCA"
'        SQL = SQL & " From"
'        SQL = SQL & " ("
'        SQL = SQL & " SELECT"
'        SQL = SQL & " (SELECT TOP 1 CD_COMP_CONC FROM CONCEPTOT WHERE CONCEPTOT.NU_MOVI_CONC =IN_ENCABEZADO.NU_AUTO_DOCU_ENCA ) AS TIPO_MOVIMIENTO_INVE,"
'        SQL = SQL & " NU_COMP_CONTA_ENCA AS COMPROCONTABLE_INVE,NU_COMP_ENCA AS NU_MOVI_INVE,"
'        SQL = SQL & " sum(CASE NU_AUTO_DOCU_ENCA WHEN 11 THEN NU_VALOR_DETA ELSE NU_COSTO_DETA END * (CASE NU_SALIDA_DETA WHEN 0 THEN  NU_ENTRAD_DETA ELSE NU_SALIDA_DETA END * (NU_MULT_DETA/CASE NU_DIVI_DETA WHEN 0 THEN 1 ELSE NU_DIVI_DETA END ))) AS VALOR_INVE,"
'        SQL = SQL & " NU_COMP_ENCA , FE_CREA_ENCA, NU_AUTO_ENCA, NU_AUTO_BODEORG_ENCA"
'        SQL = SQL & " From IN_DETALLE, IN_ENCABEZADO"
'        SQL = SQL & " Where"
'        SQL = SQL & " NU_AUTO_ORGCABE_DETA = NU_AUTO_ENCA"
'        SQL = SQL & " AND NOT NU_COMP_CONTA_ENCA IS NULL"
'        SQL = SQL & " AND NU_AUTO_BODE_DETA = NU_AUTO_BODEORG_ENCA"
'        SQL = SQL & " AND(NU_AUTO_ORGCABE_DETA = NU_AUTO_MODCABE_DETA"
'        SQL = SQL & " OR ((NU_AUTO_DOCU_ENCA = 6 OR NU_AUTO_DOCU_ENCA = 10 ) AND NU_AUTO_ORGCABE_DETA <> NU_AUTO_MODCABE_DETA))"
'        SQL = SQL & " AND TX_ESTA_ENCA <> 'A'"
'
'        If CBool(Me.chkFCRANGO.Value) Then
'            SQL = SQL & " AND FE_FECH_DETA >=" & FFechaCon(FecDesde)
'            SQL = SQL & " AND FE_FECH_DETA <=" & FFechaCon(FecHasta)
'        End If
'
'        SQL = SQL & " GROUP by  NU_AUTO_DOCU_ENCA, NU_COMP_ENCA,NU_AUTO_ORGCABE_DETA,NU_COMP_CONTA_ENCA,FE_CREA_ENCA,NU_AUTO_ENCA ,NU_AUTO_BODEORG_ENCA"
'        SQL = SQL & " ) AS T1 ,"
'        SQL = SQL & " ( SELECT NU_COMP_MOVI AS COMPROBANTE_CONTA,NU_CONS_MOVI AS NU_MOVI_CONTA,(SUM(VL_VALO_MOVI)) AS VALOR,NO_NOMB_COMP"
'        SQL = SQL & " ,FE_FECH_MOVI"
'        SQL = SQL & " From MOVIMIENTOS, TC_COMPROBANTE"
'        SQL = SQL & " WHERE NU_COMP_MOVI IN (SELECT CD_COMP_CONC FROM CONCEPTOT)"
'        SQL = SQL & " AND ID_NATU_MOVI = 'D'AND CD_CODI_COMP = NU_COMP_MOVI"
'        SQL = SQL & " GROUP BY  NU_CONS_MOVI,NU_COMP_MOVI,NO_NOMB_COMP,FE_FECH_MOVI"
'        SQL = SQL & " )  T2"
'        SQL = SQL & " Where T1.TIPO_MOVIMIENTO_INVE = T2.COMPROBANTE_CONTA"
'        SQL = SQL & " AND T1.COMPROCONTABLE_INVE = T2.NU_MOVI_CONTA"
'        SQL = SQL & " AND ROUND(T1.VALOR_INVE,4) <> ROUND(T2.VALOR,4)"
'        SQL = SQL & " AND T1.FE_CREA_ENCA = T2.FE_FECH_MOVI"
'        SQL = SQL & " )  T3"
'        SQL = SQL & " Where"
'        SQL = SQL & " NU_AUTO_ORGCABE_DETA = T3.NU_AUTO_ENCA"
'        SQL = SQL & " AND CD_CODI_GRUP  = CD_GRUP_ARTI AND"
'        SQL = SQL & " NU_AUTO_ARTI = NU_AUTO_ARTI_DETA AND"
'        SQL = SQL & " CD_CODI_GRUP=CD_CODI_GRUP_RGB AND"
'        SQL = SQL & " NU_AUTO_BODE_RGB = T3.NU_AUTO_BODEORG_ENCA"
'        SQL = SQL & " AND NU_AUTO_BODE_RGB = NU_AUTO_BODE"
'        SQL = SQL & "  AND " & strGRUPOS
'        SQL = SQL & " Group By"
'        SQL = SQL & " FE_FECH_DETA,TX_CODI_BODE, TX_NOMB_BODE,CD_GRUP_ARTI, CD_ENTR_RGB,T3.NO_NOMB_COMP,T3.TIPO_MOVIMIENTO_INVE,"
'        SQL = SQL & " T3.NU_MOVI_CONTA , T3.NU_MOVI_INVE, T3.NU_AUTO_ENCA"
'        SQL = SQL & " ,CD_SALI_RGB, T3.VALOR_INVE,T3.VALOR ,"
'        SQL = SQL & " CD_INGR_RGB,"
'        SQL = SQL & " CD_GAST_RGB,"
'        SQL = SQL & " CD_COST_RGB,"
'        SQL = SQL & " CD_AJDB_RGB,"
'        SQL = SQL & " CD_AJCR_RGB,"
'        SQL = SQL & " CD_TRAN_RGB"

        SQL = " SELECT"
        SQL = SQL & "  CD_CODI_GRUP AS TX_CODGRU_AJPE"
        SQL = SQL & "  ,DE_DESC_GRUP AS TX_DESGRU_AJPE,"
        SQL = SQL & "  TX_CODI_BODE AS TX_CODBOD_AJPE, TX_NOMB_BODE AS TX_NOMBODE_AJPE,"
        SQL = SQL & "  FECHA AS FE_FECH_AJPE,"
        SQL = SQL & "  T3.TIPO_MOVIMIENTO_INVE AS TX_COMP_AJPE, T3.NU_NUME_INVE AS NU_MOVINV_AJPE,"
        SQL = SQL & "  T3.COMPROCONTABLE_INVE AS NU_MOVCON_AJPE, T3.VALOR_INVE AS NU_VALINV_AJPE, VALOR AS NU_VALCON_AJPE, ROUND((T5.VALOR_INVE - VALOR),2) NU_DIF_AJPE"
        SQL = SQL & "  INTO AJUSTEPESO_TEMP"
        SQL = SQL & "  FROM"
        SQL = SQL & "  (SELECT"
        SQL = SQL & "  CD_CODI_GRUP"
        SQL = SQL & "  ,  DE_DESC_GRUP,"
        SQL = SQL & "  TX_CODI_BODE, TX_NOMB_BODE,FE_CREA_ENCA AS FECHA, CD_COMP_CONC TIPO_MOVIMIENTO_INVE, NU_COMP_CONTA_ENCA COMPROCONTABLE_INVE,"
        SQL = SQL & "  NU_COMP_ENCA NU_NUME_INVE, SUM(COSTO_TOTAL) VALOR_INVE"
        SQL = SQL & "  FROM"
        SQL = SQL & "  (SELECT"
        SQL = SQL & "  CD_CODI_GRUP"
        SQL = SQL & "  , DE_DESC_GRUP,"
        SQL = SQL & "  TX_CODI_BODE, TX_NOMB_BODE,CD_COMP_CONC, NU_COMP_CONTA_ENCA, NU_COMP_ENCA, FE_CREA_ENCA , NU_AUTO_ENCA, NU_AUTO_BODEORG_ENCA,"
        SQL = SQL & "  NO_NOMB_ARTI, NU_ENTRAD_DETA, SINIVA, IVA, ROUND(((((SINIVA) * (IVA/100)) + SINIVA)  *"
        SQL = SQL & "  CASE NU_ENTRAD_DETA WHEN 0 THEN NU_SALIDA_DETA ELSE NU_ENTRAD_DETA END),2) AS COSTO_TOTAL"
        SQL = SQL & "  FROM"
        SQL = SQL & "  (SELECT"
        SQL = SQL & "  CD_CODI_GRUP"
        SQL = SQL & "  , DE_DESC_GRUP,"
        SQL = SQL & "  TX_CODI_BODE, TX_NOMB_BODE, CD_COMP_CONC, NU_COMP_CONTA_ENCA, NU_COMP_ENCA, FE_CREA_ENCA , NU_AUTO_ENCA, NU_AUTO_BODEORG_ENCA,"
        SQL = SQL & "  NO_NOMB_ARTI, NU_ENTRAD_DETA,NU_SALIDA_DETA, ROUND (((100 * ROUND(NU_COSTO_DETA,2))/(100 + NU_IMPU_DETA)),2) AS SINIVA,"
        SQL = SQL & "  NU_IMPU_DETA AS IVA"
        SQL = SQL & "  FROM IN_ENCABEZADO, IN_DOCUMENTO, IN_DETALLE, CONCEPTOT, Articulo, IN_BODEGA"
        SQL = SQL & "  , GRUP_ARTICULO"
        SQL = SQL & "  , IN_R_GRUP_BODE"
        SQL = SQL & "  WHERE "
        If CBool(Me.chkFCRANGO.Value) Then
                SQL = SQL & "  FE_CREA_ENCA >= " & FFechaCon(FecDesde) & " AND "
                SQL = SQL & "  FE_CREA_ENCA <= " & FFechaCon(FecHasta) & " AND"
        End If
        SQL = SQL & "   TX_ESTA_ENCA = 'N'"
        SQL = SQL & "  AND NU_AUTO_DOCU_ENCA =NU_AUTO_DOCU"
        'SQL = SQL & "  --AND NU_AUTO_ENCA = 371912 -- COMPROBANTE DE PRUEBA"
        SQL = SQL & "  AND (/*(NU_AUTO_ORGCABE_DETA = NU_AUTO_ENCA) OR */ (NU_AUTO_MODCABE_DETA = NU_AUTO_ENCA))"
        SQL = SQL & "  AND NU_AUTO_DOCU_ENCA = NU_MOVI_CONC"
        SQL = SQL & "  AND NU_AUTO_ARTI_DETA = NU_AUTO_ARTI"
        SQL = SQL & "  AND NU_AUTO_BODEORG_ENCA = NU_AUTO_BODE"
        SQL = SQL & "  AND CD_CODI_GRUP = CD_GRUP_ARTI"
        SQL = SQL & "  AND CD_CODI_GRUP = CD_CODI_GRUP_RGB"
        SQL = SQL & "  AND NU_AUTO_BODE_RGB = NU_AUTO_BODEORG_ENCA"
        SQL = SQL & "  AND " & strGRUPOS
        SQL = SQL & "  ) AS T1 "
        SQL = SQL & "  ) AS T2"
        SQL = SQL & "  GROUP BY "
        SQL = SQL & "  CD_CODI_GRUP, DE_DESC_GRUP,"
        SQL = SQL & "  TX_CODI_BODE, TX_NOMB_BODE,FE_CREA_ENCA,CD_COMP_CONC, NU_COMP_CONTA_ENCA , NU_COMP_ENCA ) AS T3,"
        SQL = SQL & "  ( SELECT NU_COMP_MOVI AS COMPROBANTE_CONTA,NU_CONS_MOVI AS NU_MOVI_CONTA,"
        SQL = SQL & "  (SUM(VL_VALO_MOVI)) AS VALOR,NO_NOMB_COMP ,FE_FECH_MOVI"
        SQL = SQL & "  FROM MOVIMIENTOS, TC_COMPROBANTE"
        SQL = SQL & "  WHERE NU_COMP_MOVI IN (SELECT CD_COMP_CONC FROM CONCEPTOT)"
        SQL = SQL & "  AND ID_NATU_MOVI = 'D'AND CD_CODI_COMP = NU_COMP_MOVI"
        If CBool(Me.chkFCRANGO.Value) Then
                SQL = SQL & "  AND FE_FECH_MOVI >= " & FFechaCon(FecDesde)
                SQL = SQL & "  AND FE_FECH_MOVI <= " & FFechaCon(FecHasta)
        End If
        'SQL = SQL & "  --AND NU_CONS_MOVI = 1466"
        SQL = SQL & "  GROUP BY  NU_CONS_MOVI,NU_COMP_MOVI,NO_NOMB_COMP,FE_FECH_MOVI )  T4,"
        SQL = SQL & "  ("
        SQL = SQL & "  SELECT"
        SQL = SQL & "  CD_COMP_CONC TIPO_MOVIMIENTO_INVE, NU_COMP_CONTA_ENCA COMPROCONTABLE_INVE,"
        SQL = SQL & "  NU_COMP_ENCA NU_NUME_INVE, SUM(COSTO_TOTAL) VALOR_INVE"
        SQL = SQL & "  From"
        SQL = SQL & "  (SELECT"
        SQL = SQL & "  CD_COMP_CONC, NU_COMP_CONTA_ENCA, NU_COMP_ENCA, FE_CREA_ENCA , NU_AUTO_ENCA, NU_AUTO_BODEORG_ENCA,"
        SQL = SQL & "  NO_NOMB_ARTI, NU_ENTRAD_DETA, SINIVA, IVA, ROUND(((((SINIVA) * (IVA/100)) + SINIVA)  *"
        SQL = SQL & "  CASE NU_ENTRAD_DETA WHEN 0 THEN NU_SALIDA_DETA ELSE NU_ENTRAD_DETA  END),2) AS COSTO_TOTAL"
        SQL = SQL & "  From"
        SQL = SQL & "  (SELECT"
        SQL = SQL & "  TX_CODI_BODE, TX_NOMB_BODE, CD_COMP_CONC, NU_COMP_CONTA_ENCA, NU_COMP_ENCA, FE_CREA_ENCA , NU_AUTO_ENCA, NU_AUTO_BODEORG_ENCA,"
        SQL = SQL & "  NO_NOMB_ARTI, NU_ENTRAD_DETA,NU_SALIDA_DETA, ROUND (((100 * ROUND(NU_COSTO_DETA,2))/(100 + NU_IMPU_DETA)),2) AS SINIVA,"
        SQL = SQL & "  NU_IMPU_DETA As Iva"
        SQL = SQL & "  From IN_ENCABEZADO, IN_DOCUMENTO, IN_DETALLE, CONCEPTOT, Articulo, IN_BODEGA"
        SQL = SQL & "  WHERE "
        If CBool(Me.chkFCRANGO.Value) Then
                SQL = SQL & "  FE_CREA_ENCA >= " & FFechaCon(FecDesde) & " AND "
                SQL = SQL & "  FE_CREA_ENCA <= " & FFechaCon(FecHasta) & " AND"
        End If
        SQL = SQL & "  TX_ESTA_ENCA = 'N'"
        SQL = SQL & "  AND NU_AUTO_DOCU_ENCA =NU_AUTO_DOCU"
        'SQL = SQL & "  AND NU_AUTO_ENCA = 371920 -- COMPROBANTE DE PRUEBA"
        SQL = SQL & "  AND (/*(NU_AUTO_ORGCABE_DETA = NU_AUTO_ENCA) OR */ (NU_AUTO_MODCABE_DETA = NU_AUTO_ENCA))"
        SQL = SQL & "  AND NU_AUTO_DOCU_ENCA = NU_MOVI_CONC"
        SQL = SQL & "  AND NU_AUTO_ARTI_DETA = NU_AUTO_ARTI"
        SQL = SQL & "  AND NU_AUTO_BODEORG_ENCA = NU_AUTO_BODE"
        SQL = SQL & "  AND " & strGRUPOS
        SQL = SQL & "  ) AS T1 "
        SQL = SQL & "  ) AS T2"
        SQL = SQL & "  Group By"
        SQL = SQL & "  CD_COMP_CONC , NU_COMP_CONTA_ENCA, NU_COMP_ENCA"
         SQL = SQL & "  ) as T5"

        SQL = SQL & "  Where T3.TIPO_MOVIMIENTO_INVE = T4.COMPROBANTE_CONTA"
        SQL = SQL & "  AND T3.COMPROCONTABLE_INVE = T4.NU_MOVI_CONTA"
        SQL = SQL & "  AND T5.COMPROCONTABLE_INVE = T3.COMPROCONTABLE_INVE"
        SQL = SQL & "  AND T5.NU_NUME_INVE = T3.NU_NUME_INVE"
        SQL = SQL & "  AND T5.TIPO_MOVIMIENTO_INVE  = T3.TIPO_MOVIMIENTO_INVE"
        ' DAHV M6399 -INICIO
        'SQL = SQL & "  AND ROUND(T5.VALOR_INVE,0) <> ROUND(VALOR,0)"
        SQL = SQL & "  AND ROUND(T5.VALOR_INVE,2) <> ROUND(VALOR,2)"
        ' DAHV M6399 -FIN
        'SQL = SQL & "  --  AND CONVERT(VARCHAR(10),T3.TIPO_MOVIMIENTO_INVE) = CONVERT(VARCHAR(10),T4.NO_NOMB_COMP)"
        SQL = SQL & "  ORDER BY 2"
        'DAHV M6374
 
         If ExisteTABLA("AJUSTEPESO_TEMP") Then Result = EliminarTabla("AJUSTEPESO_TEMP")
         Debug.Print SQL
        
        Call MouseClock
        Result = ExecSQL(SQL)
        
        'DAHV M6372 - INICIO
        If Result <> FAIL Then
            ReDim Arr(0)
            Campos = "COUNT(*)"
            Desde = "AJUSTEPESO_TEMP"
            Result = LoadData(Desde, Campos, NUL$, Arr())
            If Result <> FAIL Then
                If Encontro Then
                    If Arr(0) = NUL$ Then Arr(0) = 0
                    If Arr(0) > 0 Then
                        Call ListarD("infDifInvCont.rpt", "", crptToWindow, "DIFERENCIAS AJUSTE AL PESO", MDI_Inventarios, True)
                    Else
                        Call Mensaje1("No se encontraron datos de consulta", 3)
                    End If
                End If
            End If
        End If
    
         'Call ListarD("infDifInvCont.rpt", "", crptToWindow, "DIFERENCIAS AJUSTE AL PESO", MDI_Inventarios, True)
        'DAHV M6372 - FIN
        
  Call MouseNorm
'
'    MDI_Inventarios.Crys_Listar.Formulas(9) = ""
'    MDI_Inventarios.Crys_Listar.Formulas(10) = ""
'    MDI_Inventarios.Crys_Listar.Formulas(11) = ""
End Sub

Private Sub Form_Load()
    Call CenterForm(MDI_Inventarios, Me)
    
    Me.cboTIPO.Clear
    Me.cboTIPO.AddItem "Para la Venta"
    Me.cboTIPO.AddItem "Para El Consumo"
    If Len(FormatDateTime(Date, vbShortDate)) = 8 Then
        Me.txtFCDESDE.Mask = "##/##/##"
        Me.txtFCHASTA.Mask = "##/##/##"
    Else
        Me.txtFCDESDE.Mask = "##/##/####"
        Me.txtFCHASTA.Mask = "##/##/####"
    End If
    Me.txtFCDESDE.Text = FormatDateTime(DateAdd("d", -(Day(Date) - 1), Date), vbShortDate)
    Me.txtFCHASTA.Text = FormatDateTime(DateAdd("d", -1, DateAdd("M", 1, DateAdd("d", -(Day(Date) - 1), Date))), vbShortDate)
    Me.chkFCRANGO.Value = False
    Me.txtFCDESDE.Enabled = False
    Me.txtFCHASTA.Enabled = False
    Me.lstBODEGAS.ListItems.Clear
    Me.lstBODEGAS.Checkboxes = False
    Me.lstBODEGAS.MultiSelect = True
    Me.lstBODEGAS.HideSelection = False
    Me.lstBODEGAS.ColumnHeaders.Clear
    Me.lstBODEGAS.ColumnHeaders.Add , "NOM", "Nombre de la Bodega", 0.66 * Me.lstBODEGAS.Width
    Me.lstBODEGAS.ColumnHeaders.Add , "COD", "C�digo", 0.34 * Me.lstBODEGAS.Width
    
    Me.lstGRUPOS.ListItems.Clear
    Me.lstGRUPOS.Checkboxes = False
    Me.lstGRUPOS.MultiSelect = True
    Me.lstGRUPOS.HideSelection = False
    Me.lstGRUPOS.ColumnHeaders.Clear
    Me.lstGRUPOS.ColumnHeaders.Add , "NOAR", "Nombre del Grupo", 0.4 * Me.lstGRUPOS.Width
    Me.lstGRUPOS.ColumnHeaders.Add , "COAR", "C�digo", 0.15 * Me.lstGRUPOS.Width
    

End Sub



Private Sub lstBODEGAS_ColumnClick(ByVal ColumnHeader As MSComctlLib.ColumnHeader)
    ' DAHV M6371
    lstBODEGAS.SortKey = ColumnHeader.Index - 1
    lstBODEGAS.Sorted = True
End Sub

Private Sub lstGRUPOS_ColumnClick(ByVal ColumnHeader As MSComctlLib.ColumnHeader)
    ' DAHV M6371
    lstGRUPOS.SortKey = ColumnHeader.Index - 1
    lstGRUPOS.Sorted = True
End Sub

Private Sub txtFCDESDE_KeyPress(KeyAscii As Integer)
    'DAHV M6373
    Call ValKeyFecha(KeyAscii, txtFCDESDE)
End Sub

Private Sub txtFCDESDE_LostFocus()
    If IsDate(Me.txtFCDESDE.Text) Then Exit Sub
    Me.txtFCDESDE.Text = FormatDateTime(DateAdd("d", -(Day(Date) - 1), Date), vbShortDate)
    'Me.txtFCDESDE.SetFocus DAHV M6373
End Sub

Private Sub txtFCDESDE_Validate(Cancel As Boolean)
        ' DAHV M6373
        If ValFecha(txtFCDESDE, 0) <> FAIL Then
           If DateDiff("d", txtFCDESDE, txtFCHASTA) < 0 Then
                Call Mensaje1("La fecha final es menor que la fecha inicial", 3)
                Me.txtFCDESDE.Text = FormatDateTime(DateAdd("d", -(Day(Date) - 1), Date), vbShortDate)
                Me.txtFCDESDE.SetFocus
                Cancel = True
            End If
         End If
End Sub

Private Sub txtFCHASTA_KeyPress(KeyAscii As Integer)
    'DAHV M6373
    Call ValKeyFecha(KeyAscii, txtFCHASTA)
End Sub

Private Sub txtFCHASTA_LostFocus()
    If IsDate(Me.txtFCHASTA.Text) Then
       If DateDiff("d", CDate(Me.txtFCDESDE.Text), CDate(Me.txtFCHASTA)) < 0 Then
          Call Mensaje1("La fecha final debe ser mayor a la inicial.", 3)
          Me.txtFCHASTA.SetFocus
       End If
    Else
       Me.txtFCHASTA.Text = FormatDateTime(DateAdd("d", -1, DateAdd("M", 1, DateAdd("d", -(Day(Date) - 1), Date))), vbShortDate)
       Me.txtFCHASTA.SetFocus
       'Me.txtFCHASTA.SetFocus DAHV M6373
    End If
End Sub

Private Sub txtFCHASTA_Validate(Cancel As Boolean)
'DAHV M6373
        If ValFecha(txtFCHASTA, 0) <> FAIL Then
           If DateDiff("d", txtFCDESDE, txtFCHASTA) < 0 Then
                Call Mensaje1("La fecha final es menor que la fecha inicial", 3)
                Me.txtFCHASTA.Text = FormatDateTime(DateAdd("d", -1, DateAdd("M", 1, DateAdd("d", -(Day(Date) - 1), Date))), vbShortDate)
                Me.txtFCHASTA.SetFocus
                Cancel = True
           End If
        End If
End Sub
