VERSION 5.00
Object = "{00025600-0000-0000-C000-000000000046}#5.2#0"; "Crystl32.OCX"
Object = "{C932BA88-4374-101B-A56C-00AA003668DC}#1.1#0"; "msmask32.ocx"
Object = "{0BA686C6-F7D3-101A-993E-0000C0EF6F5E}#1.0#0"; "THREED32.OCX"
Begin VB.Form FrmLpropro 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Comparativo de precios entre proveedores"
   ClientHeight    =   5145
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   3375
   Icon            =   "FrmLpropro.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   ScaleHeight     =   5145
   ScaleWidth      =   3375
   Begin VB.Frame Frame2 
      Height          =   1095
      Left            =   0
      TabIndex        =   25
      Top             =   3960
      Width           =   3255
      Begin Threed.SSCommand SCmd_Options 
         Height          =   735
         Index           =   1
         Left            =   1200
         TabIndex        =   26
         Top             =   240
         Width           =   855
         _Version        =   65536
         _ExtentX        =   1508
         _ExtentY        =   1296
         _StockProps     =   78
         Caption         =   "&Imprimir"
         Picture         =   "FrmLpropro.frx":0442
      End
      Begin Threed.SSCommand SCmd_Options 
         Height          =   735
         Index           =   2
         Left            =   2280
         TabIndex        =   27
         Top             =   240
         Width           =   855
         _Version        =   65536
         _ExtentX        =   1508
         _ExtentY        =   1296
         _StockProps     =   78
         Caption         =   "&Salir"
         Picture         =   "FrmLpropro.frx":0984
      End
      Begin Threed.SSCommand SCmd_Options 
         Height          =   735
         Index           =   0
         Left            =   120
         TabIndex        =   28
         Top             =   240
         Width           =   855
         _Version        =   65536
         _ExtentX        =   1508
         _ExtentY        =   1296
         _StockProps     =   78
         Caption         =   "&Pantalla"
         Picture         =   "FrmLpropro.frx":1092
      End
   End
   Begin VB.Frame Frame1 
      Caption         =   "Nivel de Detalle"
      Height          =   615
      Left            =   0
      TabIndex        =   22
      Top             =   3360
      Width           =   3255
      Begin VB.OptionButton Optres 
         Caption         =   "Resumido"
         Height          =   255
         Left            =   1800
         TabIndex        =   24
         Top             =   240
         Width           =   1095
      End
      Begin VB.OptionButton Optdet 
         Caption         =   "Detallado"
         Height          =   195
         Left            =   360
         TabIndex        =   23
         Top             =   240
         Width           =   1095
      End
   End
   Begin VB.Frame FraArticulos 
      Caption         =   "Rango de Art�culos"
      Height          =   1335
      Left            =   0
      TabIndex        =   5
      Top             =   1080
      Width           =   3255
      Begin VB.TextBox TxtRango 
         Height          =   285
         Index           =   2
         Left            =   720
         MaxLength       =   16
         TabIndex        =   6
         Text            =   "0"
         Top             =   360
         Width           =   1815
      End
      Begin VB.TextBox TxtRango 
         Height          =   285
         Index           =   3
         Left            =   720
         MaxLength       =   16
         TabIndex        =   8
         Text            =   "ZZZZZZZZZZZZZZZZ"
         Top             =   840
         Width           =   1815
      End
      Begin Threed.SSCommand CmdSelec 
         Height          =   255
         Index           =   2
         Left            =   2520
         TabIndex        =   7
         ToolTipText     =   "Selecci�n de Art�culos"
         Top             =   360
         Width           =   615
         _Version        =   65536
         _ExtentX        =   1085
         _ExtentY        =   450
         _StockProps     =   78
         BevelWidth      =   0
         RoundedCorners  =   0   'False
         Outline         =   0   'False
         Picture         =   "FrmLpropro.frx":14E4
      End
      Begin Threed.SSCommand CmdSelec 
         Height          =   255
         Index           =   3
         Left            =   2520
         TabIndex        =   9
         ToolTipText     =   "Selecci�n de Art�culos"
         Top             =   840
         Width           =   615
         _Version        =   65536
         _ExtentX        =   1085
         _ExtentY        =   450
         _StockProps     =   78
         BevelWidth      =   0
         RoundedCorners  =   0   'False
         Outline         =   0   'False
         Picture         =   "FrmLpropro.frx":1936
      End
      Begin VB.Label LblHasta 
         AutoSize        =   -1  'True
         Caption         =   "Final : "
         Height          =   195
         Left            =   120
         TabIndex        =   18
         Top             =   840
         Width           =   465
      End
      Begin VB.Label LblInicio 
         AutoSize        =   -1  'True
         Caption         =   "Inicial : "
         Height          =   195
         Left            =   120
         TabIndex        =   17
         Top             =   360
         Width           =   540
      End
   End
   Begin VB.Frame FraFecha 
      Caption         =   "Fecha    (dd/mm/aaaa)"
      Height          =   855
      Left            =   0
      TabIndex        =   10
      Top             =   2520
      Width           =   3255
      Begin MSMask.MaskEdBox MskFecha 
         Height          =   255
         Index           =   0
         Left            =   120
         TabIndex        =   11
         Top             =   480
         Width           =   1095
         _ExtentX        =   1931
         _ExtentY        =   450
         _Version        =   393216
         MaxLength       =   10
         Mask            =   "##/##/####"
         PromptChar      =   "_"
      End
      Begin MSMask.MaskEdBox MskFecha 
         Height          =   255
         Index           =   1
         Left            =   1560
         TabIndex        =   12
         Top             =   480
         Width           =   1095
         _ExtentX        =   1931
         _ExtentY        =   450
         _Version        =   393216
         MaxLength       =   10
         Mask            =   "##/##/####"
         PromptChar      =   "_"
      End
      Begin VB.Label LblFeIni 
         Caption         =   "Inicial :"
         Height          =   255
         Left            =   120
         TabIndex        =   16
         Top             =   240
         Width           =   495
      End
      Begin VB.Label LblFeFinal 
         Caption         =   "Final :"
         Height          =   255
         Left            =   1560
         TabIndex        =   15
         Top             =   240
         Width           =   495
      End
   End
   Begin VB.Frame FraTerceros 
      Caption         =   "Rango de Terceros"
      Height          =   1095
      Left            =   0
      TabIndex        =   0
      Top             =   0
      Width           =   3255
      Begin VB.TextBox TxtRango 
         Height          =   285
         Index           =   1
         Left            =   1080
         MaxLength       =   11
         TabIndex        =   3
         Text            =   "ZZZZZZZZZZZ"
         Top             =   720
         Width           =   1335
      End
      Begin VB.TextBox TxtRango 
         Height          =   285
         Index           =   0
         Left            =   1080
         MaxLength       =   11
         TabIndex        =   1
         Text            =   "0"
         Top             =   360
         Width           =   1335
      End
      Begin Threed.SSCommand CmdSelec 
         Height          =   255
         Index           =   1
         Left            =   2400
         TabIndex        =   4
         ToolTipText     =   "Selecci�n de Terceros"
         Top             =   720
         Width           =   615
         _Version        =   65536
         _ExtentX        =   1085
         _ExtentY        =   450
         _StockProps     =   78
         BevelWidth      =   0
         RoundedCorners  =   0   'False
         Outline         =   0   'False
         Picture         =   "FrmLpropro.frx":1D88
      End
      Begin Threed.SSCommand CmdSelec 
         Height          =   375
         Index           =   0
         Left            =   2400
         TabIndex        =   2
         ToolTipText     =   "Selecci�n de Terceros"
         Top             =   360
         Width           =   615
         _Version        =   65536
         _ExtentX        =   1085
         _ExtentY        =   661
         _StockProps     =   78
         BevelWidth      =   0
         RoundedCorners  =   0   'False
         Outline         =   0   'False
         Picture         =   "FrmLpropro.frx":21DA
      End
      Begin VB.Label Label1 
         AutoSize        =   -1  'True
         Caption         =   "Inicial : "
         Height          =   195
         Left            =   480
         TabIndex        =   14
         Top             =   360
         Width           =   540
      End
      Begin VB.Label Label2 
         AutoSize        =   -1  'True
         Caption         =   "Final : "
         Height          =   195
         Left            =   480
         TabIndex        =   13
         Top             =   720
         Width           =   465
      End
   End
   Begin Crystal.CrystalReport Crys_Listar 
      Left            =   0
      Top             =   1920
      _ExtentX        =   741
      _ExtentY        =   741
      _Version        =   348160
      WindowLeft      =   20
      WindowWidth     =   750
      WindowHeight    =   450
      WindowControlBox=   -1  'True
      WindowMaxButton =   -1  'True
      WindowMinButton =   -1  'True
      WindowControls  =   -1  'True
      PrintFileLinesPerPage=   60
      WindowShowCloseBtn=   -1  'True
      WindowShowSearchBtn=   -1  'True
      WindowShowPrintSetupBtn=   -1  'True
      WindowShowRefreshBtn=   -1  'True
   End
   Begin Threed.SSCommand SCmd_Options 
      Height          =   375
      Index           =   3
      Left            =   0
      TabIndex        =   20
      Top             =   0
      Visible         =   0   'False
      Width           =   975
      _Version        =   65536
      _ExtentX        =   1720
      _ExtentY        =   661
      _StockProps     =   78
   End
   Begin Threed.SSCommand SCmd_Options 
      Height          =   375
      Index           =   4
      Left            =   1080
      TabIndex        =   21
      Top             =   0
      Visible         =   0   'False
      Width           =   975
      _Version        =   65536
      _ExtentX        =   1720
      _ExtentY        =   661
      _StockProps     =   78
   End
   Begin VB.Label tipo 
      Height          =   375
      Left            =   1320
      TabIndex        =   19
      Top             =   5400
      Visible         =   0   'False
      Width           =   855
   End
End
Attribute VB_Name = "FrmLpropro"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False

Private Sub Form_Load()
 Call CenterForm(MDI_Inventarios, Me)
 Call Leer_Permisos("03025", SCmd_Options(3), SCmd_Options(3), SCmd_Options(4))
 Crys_Listar.Formulas(0) = "ENTIDAD='" & Entidad & Comi
 Crys_Listar.Formulas(1) = "NIT= 'NIT. " & Nit & Comi
 Crys_Listar.Formulas(2) = "HORA= 'Hora: " & Format(Now, "hh:mm") & Comi
 Crys_Listar.Formulas(3) = "USUARIO = 'Usuario:" & UserId & Comi
 Crys_Listar.Connect = ReportConnect
 MskFecha(0) = Hoy
 MskFecha(1) = Hoy
 Optdet.Value = True
End Sub
Private Sub TxtRango_GotFocus(Index As Integer)
    TxtRango(Index).SelStart = 0
    TxtRango(Index).SelLength = Len(TxtRango(Index).Text)
End Sub
Private Sub TxtRango_KeyPress(Index As Integer, KeyAscii As Integer)
    Dim I As Integer
    Call ValKeyAlfaNum(KeyAscii)
    If KeyAscii = 13 Then
     If Index = 3 Then
      MskFecha(0).SetFocus
     Else
      TxtRango(Index + 1).SetFocus
     End If
    End If
End Sub
Private Sub TxtRango_KeyDown(Index As Integer, KeyCode As Integer, Shift As Integer)
 Select Case Index
    Case 0
           If KeyCode = 40 Then
              TxtRango(1).SetFocus
           End If
    Case 1, 2
           If KeyCode = 38 Then
              TxtRango(Index - 1).SetFocus
           End If
           If KeyCode = 40 Then
              TxtRango(Index + 1).SetFocus
           End If
    Case 3
           If KeyCode = 38 Then
              TxtRango(Index - 1).SetFocus
           End If
           If KeyCode = 40 Then
              MskFecha(0).SetFocus
           End If
 End Select
End Sub
Private Sub MskFecha_GotFocus(Index As Integer)
    MskFecha(Index).SelStart = 0
    MskFecha(Index).SelLength = Len(MskFecha(Index).Text)
End Sub
Private Sub MskFecha_KeyPress(Index As Integer, KeyAscii As Integer)
    Call ValKeyFecha(KeyAscii, MskFecha(Index))
    Call Cambiar_Enter(KeyAscii)
End Sub
Private Sub MskFecha_LostFocus(Index As Integer)
   Call ValFecha(MskFecha(Index), 0)
End Sub
Private Sub MskFecha_KeyDown(Index As Integer, KeyCode As Integer, Shift As Integer)
 If Index = 0 Then
   If KeyCode = 38 Then
      TxtRango(3).SetFocus
   End If
   If KeyCode = 40 Then
      MskFecha(1).SetFocus
   End If
 Else
   If KeyCode = 38 Then
      MskFecha(0).SetFocus
   End If
   If KeyCode = 40 Then
      SCmd_Options(0).SetFocus
   End If
 End If
End Sub
Private Sub CmdSelec_Click(Index As Integer)
    Codigo = NUL$
    Select Case Index
        Case 0, 1: Codigo = Seleccion("TERCERO", "NO_NOMB_TERC", "CD_CODI_TERC,NO_NOMB_TERC", "TERCEROS", "CD_CODI_TERC <> 'ZZZZZZZZZZZ'")
        Case 2, 3: Codigo = Seleccion("ARTICULO", "NO_NOMB_ARTI", "CD_CODI_ARTI,NO_NOMB_ARTI", "ARTICULOS", NUL$)
    End Select
    If Codigo <> NUL$ Then TxtRango(Index) = Codigo
    TxtRango(Index).SetFocus
End Sub
Private Sub SCmd_Options_Click(Index As Integer)
    Screen.MousePointer = 11
    Select Case Index
        '''True = pantalla
        '''False = impresora
        Case 0: Call Informe(True)
        Case 1: Call Informe(False)
        Case 2: Unload Me
    End Select
    Screen.MousePointer = 0
End Sub
Private Sub Informe(ByVal pantalla As Boolean)
  Dim algo
  If TxtRango(0) > TxtRango(1) Or TxtRango(1) = "" Then
     Call Mensaje1("Revise rango de terceros", 3)
     Exit Sub
  End If
  If TxtRango(2) > TxtRango(3) Or TxtRango(3) = "" Then
     Call Mensaje1("Revise rango de art�culos", 3)
     Exit Sub
  End If
  algo = DateDiff("d", MskFecha(0), MskFecha(1))
  If CLng(algo) < 0 Then
     Call Mensaje1("Fecha Inicial Mayor que Fecha Final", 3)
     Exit Sub
  End If
  ''Env�a rango de fechas al informe
  Crys_Listar.Formulas(4) = "FECHAI= '" & MskFecha(0) & Comi
  Crys_Listar.Formulas(5) = "FECHAF= '" & MskFecha(1) & Comi
  ''''''''''''''''''''''''''''''''''
  If Optres.Value = True Then
     Crys_Listar.WindowTitle = "Comparativo Resumido"
     Crys_Listar.ReportFileName = DirTrab + "lpropro1.RPT"
  Else
     Crys_Listar.WindowTitle = "Comparativo Detallado"
     Crys_Listar.ReportFileName = DirTrab + "lpropro.RPT"
  End If
  Crys_Listar.SelectionFormula = "{R_COMP_ARTI.CD_ARTI_COAR} in '" & TxtRango(2) & "' to '" & TxtRango(3) & Comi & _
  " and {COMPRA.CD_TERC_COMP} in '" & TxtRango(0) & "' to '" & TxtRango(1) & Comi & _
  " and {COMPRA.FE_FECH_COMP}>=date(" & Format(MskFecha(0), "yyyy,mm,dd") & ") " & _
  " and {COMPRA.FE_FECH_COMP}<=date(" & Format(MskFecha(1), "yyyy,mm,dd") & ") "
  '''''''''''''''''''''''''''''''
  Debug.Print Crys_Listar.SelectionFormula
  If SCmd_Options(4).Enabled = False Then
     Call Mensaje1("Permiso Denegado", 3)
  Else
     If pantalla = True Then
        Crys_Listar.Destination = crptToWindow
     Else
        Crys_Listar.Destination = crptToPrinter
     End If
     Crys_Listar.Action = 1
  End If
End Sub
