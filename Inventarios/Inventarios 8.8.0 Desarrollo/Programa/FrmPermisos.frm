VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "MSFLXGRD.OCX"
Begin VB.Form FrmPermisos 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Creacion de Opciones de Perfil"
   ClientHeight    =   6195
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   10605
   Icon            =   "FrmPermisos.frx":0000
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   MDIChild        =   -1  'True
   ScaleHeight     =   6195
   ScaleWidth      =   10605
   Tag             =   "10000203"
   Begin VB.Frame Frame6 
      Caption         =   "Opciones parciales"
      Height          =   735
      Left            =   4920
      TabIndex        =   8
      Top             =   480
      Width           =   5655
      Begin VB.CheckBox Check1 
         Caption         =   "&Imprimir"
         Height          =   195
         Index           =   3
         Left            =   4320
         TabIndex        =   12
         Top             =   360
         Width           =   1215
      End
      Begin VB.CheckBox Check1 
         Caption         =   "&Consultar"
         Height          =   195
         Index           =   2
         Left            =   3120
         TabIndex        =   11
         Top             =   360
         Width           =   1095
      End
      Begin VB.CheckBox Check1 
         Caption         =   "&Borrar/Anular"
         Height          =   195
         Index           =   1
         Left            =   1800
         TabIndex        =   10
         Top             =   360
         Width           =   1335
      End
      Begin VB.CheckBox Check1 
         Caption         =   "&Guardar/Modificar"
         Height          =   195
         Index           =   0
         Left            =   120
         TabIndex        =   9
         Top             =   360
         Width           =   1815
      End
   End
   Begin VB.Frame Frame2 
      Height          =   4815
      Left            =   120
      TabIndex        =   5
      Top             =   1320
      Width           =   10455
      Begin VB.Frame Frame4 
         Height          =   4455
         Left            =   3960
         TabIndex        =   6
         Top             =   240
         Width           =   6375
         Begin MSFlexGridLib.MSFlexGrid FgrdPermisos 
            Height          =   4215
            Left            =   120
            TabIndex        =   7
            Top             =   120
            Width           =   6210
            _ExtentX        =   10954
            _ExtentY        =   7435
            _Version        =   393216
            Cols            =   7
            RowHeightMin    =   3
            BackColorBkg    =   16777215
         End
      End
      Begin VB.Frame Frame3 
         Caption         =   "Creacion de opciones"
         Height          =   4455
         Left            =   120
         TabIndex        =   3
         Top             =   240
         Width           =   3735
         Begin MSComctlLib.TreeView TreeOpc 
            Height          =   4095
            Left            =   120
            TabIndex        =   4
            Top             =   240
            Width           =   3495
            _ExtentX        =   6165
            _ExtentY        =   7223
            _Version        =   393217
            Indentation     =   529
            LabelEdit       =   1
            Style           =   7
            ImageList       =   "ImageArbol"
            Appearance      =   1
            Enabled         =   0   'False
         End
      End
   End
   Begin VB.Frame Frame1 
      Height          =   735
      Left            =   120
      TabIndex        =   0
      Top             =   480
      Width           =   4695
      Begin VB.ComboBox CmbPerfil 
         Height          =   315
         Left            =   1440
         TabIndex        =   2
         Top             =   240
         Width           =   2895
      End
      Begin VB.Label Label1 
         Caption         =   "&Nombre de Perfil"
         Height          =   255
         Left            =   120
         TabIndex        =   1
         Top             =   240
         Width           =   1335
      End
   End
   Begin MSComctlLib.Toolbar Tlb_Opciones 
      Align           =   1  'Align Top
      Height          =   420
      Left            =   0
      TabIndex        =   13
      Top             =   0
      Width           =   10605
      _ExtentX        =   18706
      _ExtentY        =   741
      ButtonWidth     =   609
      ButtonHeight    =   582
      Appearance      =   1
      _Version        =   393216
      BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
         NumButtons      =   7
         BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Style           =   3
         EndProperty
         BeginProperty Button2 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Enabled         =   0   'False
            Key             =   "G"
            Object.ToolTipText     =   "Guardar"
         EndProperty
         BeginProperty Button3 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Enabled         =   0   'False
            Key             =   "B"
            Object.ToolTipText     =   "Borrar"
         EndProperty
         BeginProperty Button4 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Enabled         =   0   'False
            Key             =   "I"
            Object.ToolTipText     =   "Imprimir"
         EndProperty
         BeginProperty Button5 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Style           =   3
         EndProperty
         BeginProperty Button6 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Enabled         =   0   'False
            Key             =   "C"
            Object.ToolTipText     =   "Cambiar C�digo"
         EndProperty
         BeginProperty Button7 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "A"
            Object.ToolTipText     =   "Ayuda"
         EndProperty
      EndProperty
   End
   Begin MSComctlLib.ImageList ImageArbol 
      Left            =   120
      Top             =   960
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   16
      ImageHeight     =   16
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   15
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FrmPermisos.frx":058A
            Key             =   ""
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FrmPermisos.frx":069C
            Key             =   ""
         EndProperty
         BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FrmPermisos.frx":07AE
            Key             =   ""
         EndProperty
         BeginProperty ListImage4 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FrmPermisos.frx":08C0
            Key             =   ""
         EndProperty
         BeginProperty ListImage5 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FrmPermisos.frx":09D2
            Key             =   ""
         EndProperty
         BeginProperty ListImage6 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FrmPermisos.frx":0AE4
            Key             =   ""
         EndProperty
         BeginProperty ListImage7 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FrmPermisos.frx":0BF6
            Key             =   ""
         EndProperty
         BeginProperty ListImage8 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FrmPermisos.frx":0D08
            Key             =   ""
         EndProperty
         BeginProperty ListImage9 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FrmPermisos.frx":0E1A
            Key             =   ""
         EndProperty
         BeginProperty ListImage10 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FrmPermisos.frx":0F2C
            Key             =   ""
         EndProperty
         BeginProperty ListImage11 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FrmPermisos.frx":103E
            Key             =   ""
         EndProperty
         BeginProperty ListImage12 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FrmPermisos.frx":1150
            Key             =   ""
         EndProperty
         BeginProperty ListImage13 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FrmPermisos.frx":1262
            Key             =   ""
         EndProperty
         BeginProperty ListImage14 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FrmPermisos.frx":1374
            Key             =   ""
         EndProperty
         BeginProperty ListImage15 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FrmPermisos.frx":162E
            Key             =   ""
         EndProperty
      EndProperty
   End
End
Attribute VB_Name = "FrmPermisos"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Dim CodPerfil() As Variant
Dim arrEventos() As C_Eventos
Public OpcCod     As String   'Opci�n de seguridad

Private Sub CmbPerfil_Click()
   TreeOpc.Enabled = True
   FgrdPermisos.Rows = 1
End Sub

Private Sub FgrdPermisos_DblClick()
Dim I As Integer
Dim columna As Integer
With FgrdPermisos
If .ColSel = 6 And .Col = 1 Then
   For I = 0 To 3
      If Check1(I) Then
         .TextMatrix(.Row, I + 1) = " "
         .Col = I + 1
         .CellPictureAlignment = 4
         Set .CellPicture = ImageArbol.ListImages(14).Picture
      Else
         .Col = I + 1
         .TextMatrix(.Row, I + 1) = NUL$
         Set .CellPicture = ImageArbol.ListImages(15).Picture
      End If
   Next
ElseIf .TextMatrix(.Row, .Col) <> NUL$ Then
   
   'jjrg 15617
   'INICIO AFMG T20266-R17890 SE DEJA EN COMENTARIO
   'If FgrdPermisos.TextMatrix(FgrdPermisos.Row, 0) = "PARAMETROS DE LA APLICACION" Then
      'If UCase(CmbPerfil.Text) <> "ADMINISTRADOR" Then
         'Call Mensaje1("Elija usuario administrador", 1)
         'Exit Sub
      'End If
   'End If
   'FIN AFMG T20266- R17890
   'jjrg 15617
   
   .TextMatrix(.Row, .Col) = NUL$
   .CellPictureAlignment = 4
   .Text = "" 'Identificar si esta asignado
      
   Set .CellPicture = ImageArbol.ListImages(15).Picture
Else
   
   'jjrg 15617
   'INICIO AFMG T20266-R17890 SE DEJA EN COMENTARIO
   'If FgrdPermisos.TextMatrix(FgrdPermisos.Row, 0) = "PARAMETROS DE LA APLICACION" Then
      'If UCase(CmbPerfil.Text) <> "ADMINISTRADOR" Then
         'Call Mensaje1("Elija usuario administrador", 1)
         'Exit Sub
      'End If
   'End If
   'FIN AFMG T20266-R17890
   'jjrg 15617

   
   .TextMatrix(.Row, .Col) = " "
   .CellPictureAlignment = 4
   Set .CellPicture = ImageArbol.ListImages(14).Picture
End If
columna = .Col
If GrabarOpcion(.Row, columna, 2) Then
   
End If

End With
End Sub

Private Sub FgrdPermisos_KeyPress(KeyAscii As Integer)
Call Cambiar_Enter(KeyAscii)
End Sub

Private Sub Form_Activate()
   FrmMenu.CodOpc = OpcCod
End Sub

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
Select Case KeyCode
      Case vbKeyF1:
      Case vbKeyF2:
      Case vbKeyF3:
      Case vbKeyF11:
      Case vbKeyF5:
      Case vbKeyF6:
      Case vbKeyF12: Unload Me
   End Select
End Sub

Private Sub Form_Load()
   Call CenterForm(MDI_Inventarios, Me)
   Call LoadIconos(Tlb_Opciones)
   Call EventosControles(Me, arrEventos)
    
    'CARGAR COMBO PERFILES
    Result = loadctrl("PERFIL ORDER BY TX_DESC_PERF ", "TX_DESC_PERF", "NU_AUTO_PERF", CmbPerfil, CodPerfil(), NUL$)
    Campos = "TX_CODI_OPCI,TX_DESC_OPCI,TX_CODPADR_OPCI"
'    Call Carga_Arbol("OPCION", Campos, " NU_AUTO_FORM_OPCI IS NULL ORDER BY TX_CODI_OPCI", TreeOpc)
    Call Carga_Arbol("OPCION", Campos, NUL$, TreeOpc)
    Call Estado_Botones_Forma(Tlb_Opciones, OpcCod)
    Call FreeBufs(Result)
    If (Result = FAIL) Then
      Call Mensaje1("ERROR CARGANDO TABLA [PERFIL]", 1)
      Exit Sub
    End If
    TreeOpc.Nodes(1).Selected = True
End Sub
Private Sub Cmbperfil_KeyPress(KeyAscii As Integer)
    Call Cambiar_Enter(KeyAscii)
End Sub

'DEPURACION DE CODIGO
'Private Sub SCmd_Options_Click(Index As Integer)
'    Select Case Index
'        Case 0: Call NingunPermiso
'        Case 1: Call ParcialPermiso
'        Case 2: Call TodoPermiso
'        Case 3: Unload Me
'    End Select
'End Sub

Private Sub Form_Unload(Cancel As Integer)
Call DestruirControles(arrEventos())
End Sub

Private Sub Tlb_Opciones_ButtonClick(ByVal Button As MSComctlLib.Button)
Select Case Button.Key
      
      Case "I": 'DAHV M0003306
                ReDim Arr(1)
                Result = LoadData("ENTIDAD", "CD_NIT_ENTI, NO_NOMB_ENTI", NUL$, Arr)
                If Result <> FAIL Then
                    Entidad = Arr(1)
                    Nit = Arr(0)
                End If
                Call Listar1("InfPerxperfil.rpt", NUL$, NUL$, 0, "LISTADO DE PERMISOS", MDI_Inventarios)
      
   End Select
End Sub

Private Sub TreeOpc_KeyPress(KeyAscii As Integer)
Call Cambiar_Enter(KeyAscii)
End Sub

Private Sub TreeOpc_NodeClick(ByVal Node As Node)
  Dim ArrTemp() As Variant
  ReDim ARRE(1)
  Dim Clave As String
  Dim S As String
  Dim nodoArbol As Node
  Dim I, J, k As Integer
  'verificar si se crearon las opciones del perfil
  If CmbPerfil.ListIndex = -1 Then Exit Sub
  ReDim ArrTemp(0)
  Condi = "NU_AUTO_PERF_PERM=" & CodPerfil(CmbPerfil.ListIndex)
  Result = LoadData("PERMISO", "NU_AUTO_PERF_PERM", Condi, ArrTemp())
  If Result <> FAIL Then
      If Not Encontro Then
         CrearOpcionesPerf (CodPerfil(CmbPerfil.ListIndex))
      End If
  End If
  FgrdPermisos.ColWidth(0) = 3100
  FgrdPermisos.ColWidth(1) = 700
  FgrdPermisos.ColWidth(2) = 700
  FgrdPermisos.ColWidth(3) = 700
  FgrdPermisos.ColWidth(4) = 700
  FgrdPermisos.ColWidth(5) = 1
  FgrdPermisos.ColWidth(6) = 1
  FgrdPermisos.TextMatrix(0, 0) = "DESCRIPCION"
  FgrdPermisos.TextMatrix(0, 1) = "Guardar"
  FgrdPermisos.TextMatrix(0, 2) = "Borrar"
  FgrdPermisos.TextMatrix(0, 3) = "Consultar"
  FgrdPermisos.TextMatrix(0, 4) = "Imprimir"
  FgrdPermisos.TextMatrix(0, 5) = "Cod opcion" ''Para comprobar si esta grabado
  FgrdPermisos.TextMatrix(0, 6) = "Cod opcion"
  ARRE(0) = CmbPerfil
  Clave = Node.Key
  If Clave <> "CNT" Then
      Clave = quitar_comillas(Clave)
  End If
  
  ARRE(1) = Clave
  With FgrdPermisos
  Campos = "TX_CODI_PERF,NU_AUTO_OPCI,TX_CREA_PERM,TX_ANUL_PERM,"
  Campos = Campos & "TX_CONS_PERM,TX_IMPR_PERM,TX_OTRO_PERM,TX_OTR1_PERM,"
  Campos = Campos & "TX_CODPADR_OPCI,NU_AUTO_OPCI,TX_DESC_OPCI,TX_CODPADR_OPCI"
  Desde = "PERMISO,OPCION,PERFIL"
  Condi = "OPCION.NU_AUTO_OPCI = PERMISO.NU_AUTO_OPCI_PERM"
  Condi = Condi & " AND PERFIL.NU_AUTO_PERF = PERMISO.NU_AUTO_PERF_PERM"
  Condi = Condi & " AND TX_DESC_PERF=" & Comi & ARRE(0) & Comi
  Condi = Condi & " AND TX_CODPADR_OPCI=" & Comi & ARRE(1) & Comi
  Condi = Condi & " ORDER BY TX_CODI_OPCI"
  Result = DoSelect(Desde, Campos, Condi)
  If Result <> FAIL Then
  'If ExecQuery("ASIGNACIONES_PERMISOS", ARRE()) <> FAIL Then
'  If ExecQuery("ASIGNACIONES_PERMISOS", ARRE()) <> FAIL Then
    If (ResultQuery()) Then
        Call MouseClock
        I = 1
        FgrdPermisos.Rows = 1
        Do While (NextRowQuery())
            
            S = NUL$
            J = 1
            S = S & DataQuery(11)
            FgrdPermisos.AddItem S, I
            .TextMatrix(I, 5) = DataQuery(2)
            .TextMatrix(I, 6) = DataQuery(10)
            For k = 3 To 6
                If DataQuery(k) = "S" Then
                    .Row = I
                    .Col = J
                    .CellPictureAlignment = 4
                    .Text = " " 'Identificar si esta asignado
                    Set .CellPicture = ImageArbol.ListImages(14).Picture
                    
                End If
                J = J + 1
            Next k
            I = I + 1
        Loop
        Call MouseNorm
    Else
        FgrdPermisos.Rows = 1
    End If
   
  End If
  End With
  Call FreeBufs(Result)
End Sub
Function quitar_comillas(Clave As String)
    Dim dimension As Variant
    If Clave <> "" Then
        dimension = Len(Clave)
        Clave = Mid(Clave, 2, (dimension - 2))
    End If
    quitar_comillas = Clave
End Function

Function GrabarOpcion(Fila As Integer, Optional columna As Integer, Optional OpcGravar As Integer) As Integer
    Dim OPC As String
'    Dim perfil As String, OpcCrea As String, OpcAnul As String, OpcCons As String, OpcImpr As String
    Dim OpcCrea As String, OpcAnul As String, OpcCons As String, OpcImpr As String  'DEPURACION DE CODIGO
    Dim SCrea As String, SAnul As String, SCons As String, SImpr As String
    Dim Cambio As Boolean
    Dim AutPerf As Long
    Dim AutOpc As Long
    Dim ArrTemp() As Variant
    Call MouseClock
    If CmbPerfil.ListIndex = -1 Then
        Call Mensaje1("Debe seleccionar un perfil", 3)
        Call MouseNorm
        Exit Function
    End If
    With FgrdPermisos
        If (BeginTran(STranIUp & "Permisos") <> FAIL) Then
            AutOpc = CLng(.TextMatrix(Fila, 6))
            AutPerf = CodPerfil(CmbPerfil.ListIndex)
            Call SalvarOpciones(AutPerf, AutOpc, SCrea, SAnul, SCons, SImpr)
            
            Valores = "NU_AUTO_PERF_PERM=" & CodPerfil(CmbPerfil.ListIndex) & Coma
            Valores = Valores & "NU_AUTO_OPCI_PERM=" & AutOpc & Coma
            Valores = Valores & "TX_CREA_PERM='" & IIf(.TextMatrix(Fila, 1) <> NUL$, "S", "N") & Comi & Coma
            Valores = Valores & "TX_ANUL_PERM='" & IIf(.TextMatrix(Fila, 2) <> NUL$, "S", "N") & Comi & Coma
            Valores = Valores & "TX_CONS_PERM='" & IIf(.TextMatrix(Fila, 3) <> NUL$, "S", "N") & Comi & Coma
            Valores = Valores & "TX_IMPR_PERM='" & IIf(.TextMatrix(Fila, 4) <> NUL$, "S", "N") & Comi
            
            AutOpc = .TextMatrix(Fila, 6)
            AutPerf = CodPerfil(CmbPerfil.ListIndex)
            OpcCrea = IIf(.TextMatrix(Fila, 1) <> NUL$, "S", "N")
            OpcAnul = IIf(.TextMatrix(Fila, 2) <> NUL$, "S", "N")
            OpcCons = IIf(.TextMatrix(Fila, 3) <> NUL$, "S", "N")
            OpcImpr = IIf(.TextMatrix(Fila, 4) <> NUL$, "S", "N")
            Condicion = "NU_AUTO_PERF_PERM=" & AutPerf
            Condicion = Condicion & " AND NU_AUTO_OPCI_PERM=" & AutOpc
            Result = DoUpdate("PERMISO", Valores, Condicion)
            Result = Auditor("PERMISO", TranUpd, LastCmd) 'AFMG T20667
            
        End If
        If Result <> FAIL Then
            If CommitTran() = FAIL Then
                Call RollBackTran
            End If
        Else
            Call RollBackTran
        End If
    End With
    GrabarOpcion = Result
    Call FreeBufs(Result)
    
    If SCrea <> OpcCrea And OpcCrea = "S" Then
        Cambio = True
    ElseIf SAnul <> OpcAnul And OpcAnul = "S" Then
        Cambio = True
    ElseIf SCons <> OpcCons And OpcCons = "S" Then
        Cambio = True
    ElseIf SImpr <> OpcImpr And OpcImpr = "S" Then
        Cambio = True
    End If
    
    If OpcGravar = 1 Then
        Valores = "TX_CREA_PERM='" & OpcCrea & Comi & Coma
        Valores = Valores & "TX_ANUL_PERM='" & OpcAnul & Comi & Coma
        Valores = Valores & "TX_CONS_PERM='" & OpcCons & Comi & Coma
        Valores = Valores & "TX_IMPR_PERM='" & OpcImpr & Comi & Coma
        Valores = Valores & "TX_OTRO_PERM='N'" & Coma
        Valores = Valores & "TX_OTR1_PERM='N'"
    Else
        Select Case columna
            Case 1:
                Valores = "TX_CREA_PERM='" & OpcCrea & Comi
            Case 2:
                Valores = "TX_ANUL_PERM='" & OpcAnul & Comi
            Case 3:
                Valores = "TX_CONS_PERM='" & OpcCons & Comi
            Case 4:
                Valores = "TX_IMPR_PERM='" & OpcImpr & Comi
        End Select
       
    End If
    ReDim ArrTemp(0)
    Result = LoadData("OPCION", "TX_CODI_OPCI", "NU_AUTO_OPCI=" & AutOpc, ArrTemp())
    OPC = ArrTemp(0)
    Call HijosOpcion(OPC, AutPerf, Valores)
    If Cambio = True Then
        Call PadresOpcion(AutOpc, OPC, AutPerf, Valores)
    End If
    Call MouseNorm
End Function

Sub SalvarOpciones(ByVal AutPerfil As Long, ByVal AutOpcS As Long, SCrea As String, SAnul As String, SCons As String, SImpr As String)
    Campos = "TX_CREA_PERM, TX_ANUL_PERM, TX_CONS_PERM, TX_IMPR_PERM"
    Condicion = "NU_AUTO_PERF_PERM=" & AutPerfil & " AND NU_AUTO_OPCI_PERM=" & AutOpcS
    If (DoSelect("PERMISO", Campos, Condicion) <> FAIL) Then
        SCrea = DataQuery(1)
        SAnul = DataQuery(2)
        SCons = DataQuery(3)
        SImpr = DataQuery(4)
    End If
End Sub

Sub NingunPermiso()
Dim InI As Integer
Dim fin As Integer
Dim I, J As Integer
With FgrdPermisos
   Call InicioFin(InI, fin)
   For I = InI To fin
      .Row = I
      For J = 1 To 4
         .Col = J
         .TextMatrix(I, J) = NUL$
         Set .CellPicture = ImageArbol.ListImages(15).Picture
      Next
      Call GrabarOpcion(CInt(I), , 1)
   Next
End With
End Sub
Sub TodoPermiso()
Dim InI As Integer
Dim fin As Integer
Dim I, J As Integer

With FgrdPermisos
   Call InicioFin(InI, fin)
   For I = InI To fin
      .Row = I
      For J = 1 To 4
         .Col = J
         .TextMatrix(I, J) = " "
         .CellPictureAlignment = 4
         Set .CellPicture = ImageArbol.ListImages(14).Picture
      Next
      Call GrabarOpcion(CInt(I), , 1)
   Next
End With
End Sub

Sub ParcialPermiso()
Dim InI As Integer
Dim fin As Integer
Dim I, J As Integer


With FgrdPermisos
   Call InicioFin(InI, fin)
   For I = InI To fin
      .Row = I
      For J = 0 To 3
         If Check1(J) Then
            .TextMatrix(I, J + 1) = " "
            .Col = J + 1
            .CellPictureAlignment = 4
            Set .CellPicture = ImageArbol.ListImages(14).Picture
         Else
            .Col = J + 1
            .TextMatrix(I, J + 1) = NUL$
            Set .CellPicture = ImageArbol.ListImages(15).Picture
         End If
      Next
      Call GrabarOpcion(CInt(I), , 1)
   Next
   
End With
End Sub

Sub InicioFin(ByRef InI As Integer, ByRef fin As Integer)
With FgrdPermisos
   If .Row <= .Rowsel Then
      InI = .Row
      fin = .Rowsel
   Else
      InI = .Rowsel
      fin = .Row
   End If
End With
End Sub

Function HijosOpcion(OPC As String, AutPerf As Long, Otros As String)
'Esta Funcion realiza la actualizacion de un hijo cuando a su
'padre se le habilita una opcion, el hijo hereda la opcion
'habilitada
    Dim ARREOPT() As Variant
    Dim I As Integer
    ReDim ARREOPT(0)
    ARREOPT(0) = 30
    Desde = "OPCION"
    Campos = "NU_AUTO_OPCI"
    Condicion = "TX_CODPADR_OPCI= '" & OPC & Comi
    If (LoadArray(Desde, Campos, Condicion, ARREOPT) <> FAIL) Then
      If Not ARREOPT(0) = 30 Then
        For I = 0 To UBound(ARREOPT)
            Call HijosOpcion(CStr(ARREOPT(I)), AutPerf, Otros)
            Condicion = "NU_AUTO_PERF_PERM=" & AutPerf & " AND NU_AUTO_OPCI_PERM=" & ARREOPT(I)
            Result = DoUpdate("PERMISO", Otros, Condicion)
        Next
      End If
   End If
   Call FreeBufs(Result)
End Function

Function PadresOpcion(AutOpc As Long, OPC As String, AutPerf As Long, Otros As String)
'    Dim i As Integer   'DEPURACION DE CODIGO
    ReDim Arr(1)
    
    Desde = "OPCION,PERMISO"
    Campos = "OPCION.NU_AUTO_OPCI,OPCION.TX_CODPADR_OPCI"
    Condicion = "OPCION.NU_AUTO_OPCI = PERMISO.NU_AUTO_OPCI_PERM"
    Condicion = Condicion & " AND OPCION.TX_CODI_OPCI= '" & OPC & Comi & " AND PERMISO.NU_AUTO_PERF_PERM=" & AutPerf
    
    If (LoadData(Desde, Campos, Condicion, Arr) <> FAIL) Then
      'Condicion = "NU_AUTO_PERF_PERM=" & AutPerf & " AND NU_AUTO_OPCI_PERM=" & AutOpc 'HRR M4909
      Condicion = "NU_AUTO_PERF_PERM=" & AutPerf & " AND NU_AUTO_OPCI_PERM=" & Arr(0) 'HRR M4909
      Result = DoUpdate("PERMISO", Otros, Condicion)
      Result = Auditor("PERMISO", TranUpd, LastCmd) 'AFMG T20667
      If Arr(1) <> "CNT" Then Call PadresOpcion(CLng(Arr(0)), CStr(Arr(1)), AutPerf, Otros)
    End If
    
End Function
