VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "ClsQrCode"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
'---------------------------------------------------------------------------------------
' Module    : ClsQrCode
' DateTime  : 19/07/2018 10:54
' Author    : daniel_mesa
' Purpose   : DRMG T44728-R41288 GENERA EL QR
'---------------------------------------------------------------------------------------

Option Explicit
Private Declare Function InternetOpen Lib "wininet.dll" Alias "InternetOpenA" (ByVal sAgent As String, ByVal lAccessType As Long, ByVal sProxyName As String, ByVal sProxyBypass As String, ByVal lFlags As Long) As Long
Private Declare Function InternetOpenUrl Lib "wininet" Alias "InternetOpenUrlA" (ByVal hInternetSession As Long, ByVal lpszUrl As String, ByVal lpszHeaders As String, ByVal dwHeadersLength As Long, ByVal dwFlags As Long, ByVal dwContext As Long) As Long
Private Declare Function HttpQueryInfo Lib "wininet.dll" Alias "HttpQueryInfoA" (ByVal hHttpRequest As Long, ByVal lInfoLevel As Long, ByRef sBuffer As Any, ByRef lBufferLength As Long, ByRef lIndex As Long) As Integer
Private Declare Function InternetCloseHandle Lib "wininet.dll" (ByVal hInternet As Long) As Boolean
Private Declare Function InternetReadFile Lib "wininet.dll" (ByVal hFile As Long, ByVal sBuffer As String, ByVal lNumBytesToRead As Long, lNumberOfBytesRead As Long) As Integer
Private Declare Function OleCreatePictureIndirect Lib "olepro32.dll" (PicDesc As PICTDESC, RefIID As GUID, ByVal fPictureOwnsHandle As Long, IPic As IPicture) As Long
Private Declare Function VarPtrArray Lib "msvbvm60.dll" Alias "VarPtr" (ByRef Ptr() As Any) As Long
Private Declare Function CreateStreamOnHGlobal Lib "ole32" (ByVal hGlobal As Long, ByVal fDeleteOnRelease As Long, ppstm As Any) As Long
Private Declare Function GlobalAlloc Lib "KERNEL32" (ByVal uFlags As Long, ByVal dwBytes As Long) As Long
Private Declare Function GlobalLock Lib "KERNEL32" (ByVal hMem As Long) As Long
Private Declare Sub CopyMemory Lib "KERNEL32" Alias "RtlMoveMemory" (Destination As Any, Source As Any, ByVal Length As Long)
Private Declare Function GlobalUnlock Lib "KERNEL32" (ByVal hMem As Long) As Long
Private Declare Function WideCharToMultiByte Lib "KERNEL32" (ByVal CodePage As Long, ByVal dwFlags As Long, ByVal lpWideCharStr As Long, ByVal cchWideChar As Long, ByRef lpMultiByteStr As Any, ByVal cchMultiByte As Long, ByVal lpDefaultChar As String, ByVal lpUsedDefaultChar As Long) As Long
Private Declare Function GdipLoadImageFromStream Lib "GdiPlus.dll" (ByVal mStream As IUnknown, ByRef mImage As Long) As Long
Private Declare Function GdipCreateHBITMAPFromBitmap Lib "GdiPlus.dll" (ByVal mBitmap As Long, ByRef mHbmReturn As Long, ByVal mBackground As Long) As Long
Private Declare Function GdipDisposeImage Lib "gdiplus" (ByVal Image As Long) As Long
Private Declare Function GdiplusStartup Lib "gdiplus" (ByRef token As Long, ByRef lpInput As GDIPlusStartupInput, Optional ByRef lpOutput As Any) As Long
Private Declare Function GdiplusShutdown Lib "gdiplus" (ByVal token As Long) As Long
Private Declare Function OleTranslateColor Lib "oleaut32.dll" (ByVal lOleColor As Long, ByVal lHPalette As Long, ByVal lColorRef As Long) As Long
'Structure
Private Type GDIPlusStartupInput
   LnGdiPlusVersion As Long
   LnDebugEventCallback As Long
   LnSuppressBackgroundThread As Long
   LnSuppressExternalCodecs As Long
End Type

Private Type GUID
   LnData1 As Long
   InData2 As Integer
   InData3 As Integer
   ByData4(0 To 7) As Byte
End Type

Private Type PICTDESC
   LnSize As Long
   LnType As Long
   LnhBmp As Long
   LnhPal As Long
   LnReserved As Long
End Type

Private Const LnINTERNET_OPEN_TYPE_DIRECT As Long = 1
Private Const LnINTERNET_FLAG_RELOAD As Long = &H80000000
Private Const LnINTERNET_FLAG_NO_AUTO_REDIRECT As Long = &H200000
Private Const LnHTTP_QUERY_STATUS_CODE As Long = 19
Private Const LnHTTP_STATUS_OK As Long = 200
Private Const LnGdiPlusVersion As Long = 1
Private Const LnCP_UTF8 As Long = 65001
Dim LnGdipToken As Long

'---------------------------------------------------------------------------------------
' Procedure : CreateBitmapPicture
' DateTime  : 16/07/2018 10:55
' Author    : daniel_mesa
' Purpose   : DRMG T44728-R41288 Crea la Imagen
'---------------------------------------------------------------------------------------
'
Private Function CreateBitmapPicture(ByVal hBmp As Long, ByVal hPal As Long) As Picture
   Dim LnR As Long 'Almacena en longitud la imagen
   Dim Pic As PICTDESC 'Almacena tama�o, tipo, manejo mapa de bits y manejo de paleta
   Dim IPic As IPicture '
   Dim IID_IDispatch As GUID

   With IID_IDispatch
      .LnData1 = &H20400
      .ByData4(0) = &HC0
      .ByData4(7) = &H46
   End With

   With Pic
      .LnSize = Len(Pic) ' Length of structure
      .LnType = vbPicTypeBitmap ' Type of Picture (bitmap)
      .LnhBmp = hBmp ' Handle to bitmap
      .LnhPal = hPal ' Handle to palette (may be null)
   End With
   'Create the picture
   LnR = OleCreatePictureIndirect(Pic, IID_IDispatch, 1, IPic)
   Set CreateBitmapPicture = IPic
End Function

'---------------------------------------------------------------------------------------
' Procedure : CreateStream
' DateTime  : 16/07/2018 10:57
' Author    : daniel_mesa
' Purpose   : DRMG T44728-R41288 Almacena en un arreglo los bytes de la imagen
'---------------------------------------------------------------------------------------
'
Private Function CreateStream(byteContent() As Byte, Optional byteOffset As Long = 0&) As stdole.IUnknown
   ' Purpose: Create an IStream-compatible IUnknown interface containing the
   ' passed byte aray. This IUnknown interface can be passed to GDI+ functions
   ' that expect an IStream interface -- neat hack
   Dim Lno_lngLowerBound As Long
   Dim Lno_lngByteCount  As Long
   Dim LNo_hMem As Long
   Dim Lno_lpMem  As Long
   
   On Error GoTo HandleError

   If iparseIsArrayEmpty(VarPtrArray(byteContent)) = 0& Then 'create a growing stream as needed
      Call CreateStreamOnHGlobal(0, 1, CreateStream)
   Else 'create a fixed stream
      Lno_lngByteCount = UBound(byteContent) - byteOffset + 1
      LNo_hMem = GlobalAlloc(&H2&, Lno_lngByteCount)
      If LNo_hMem <> 0 Then
         Lno_lpMem = GlobalLock(LNo_hMem)
         If Lno_lpMem <> 0 Then
            CopyMemory ByVal Lno_lpMem, byteContent(byteOffset), Lno_lngByteCount
            Call GlobalUnlock(LNo_hMem)
            Call CreateStreamOnHGlobal(LNo_hMem, 1, CreateStream)
         End If
      End If
   End If
HandleError:
End Function

'---------------------------------------------------------------------------------------
' Procedure : GetCode
' DateTime  : 16/07/2018 10:58
' Author    : daniel_mesa
' Purpose   : DRMG T44728-R41288 Genera el Codigo
'---------------------------------------------------------------------------------------
'
Private Function GetCode(ByVal sUrl As String, ByRef StrOut As String) As Boolean
   Dim LnhOpen As Long 'Almacena informacion de la libreria InternetOpen
   Dim LnhFile As Long 'Almacena informacion de la libreria InternetOpenUrl
   Dim StBuffer As String * 1024 'Almacena informacion de la libreria HttpQueryInfo
   Dim LnRet As Long 'Almacena informacion de la libreria InternetReadFile
   
   StrOut = vbNullString
   
   LnhOpen = InternetOpen("IE", LnINTERNET_OPEN_TYPE_DIRECT, vbNullString, vbNullString, 0)
   If LnhOpen Then
      LnhFile = InternetOpenUrl(LnhOpen, sUrl, vbNullString, ByVal 0&, LnINTERNET_FLAG_RELOAD Or LnINTERNET_FLAG_NO_AUTO_REDIRECT, ByVal 0&)
      If LnhFile Then
         If HttpQueryInfo(LnhFile, LnHTTP_QUERY_STATUS_CODE, ByVal StBuffer, 5&, 0) Then
            If (Left$(StBuffer, InStr(StBuffer, Chr$(0)) - 1) = LnHTTP_STATUS_OK) Then
               Do
                  InternetReadFile LnhFile, StBuffer, 1024, LnRet
                  StrOut = StrOut & Left$(StBuffer, LnRet)
                  If LnRet = 0 Then Exit Do
               Loop
               GetCode = True
            End If
         End If
         InternetCloseHandle LnhFile
      End If
      InternetCloseHandle LnhOpen
   End If
End Function

'---------------------------------------------------------------------------------------
' Procedure : GetPictureQrCode
' DateTime  : 16/07/2018 11:00
' Author    : daniel_mesa
' Purpose   : DRMG T44728-R41288 Almacena la imagen generada
'---------------------------------------------------------------------------------------
'
Public Function GetPictureQrCode(ByVal sText As String, ByVal Width As Long, ByVal Height As Long, Optional ByVal Encoding As String = "UTF-8", Optional ByVal ErrCorrectionLevel As String = "L", Optional ByVal ForeColor As OLE_COLOR = vbWhite, Optional ByVal BackColor As OLE_COLOR = vbBlack, Optional ByVal Margin As Long) As Picture
   Dim IIStream As IUnknown 'Instancia el objeto
   Dim StrOut As String 'Almacena la informacion consultada
   Dim LnhImage As Long 'Almacena la longitud de la imagen
   Dim LnhBmp As Long 'Almacena la longitud del mapa de bits
   Dim StAPI As String 'Almacena la informacion de la api utilizada

   StAPI = "http://api.qrserver.com/v1/create-qr-code/?data=" & GetSafeURL(Unicode2UTF8(sText)) & "&size=" & Width & "x" & Height
   If Margin > 0 Then StAPI = StAPI & "&qzone=" & Margin
   If UCase(ErrCorrectionLevel) <> "L" Then StAPI = StAPI & "&ecc=" & ErrCorrectionLevel
   If ForeColor <> vbWhite Then StAPI = StAPI & "&color=" & LongToHtml(ForeColor)
   If BackColor <> vbBlack Then StAPI = StAPI & "&bgcolor=" & LongToHtml(BackColor)
   If UCase(Encoding) <> "UTF-8" Then StAPI = StAPI & "&charset-target=" & UCase(Encoding)
   
   If GetCode(StAPI, StrOut) Then
      Set IIStream = CreateStream(StrConv(StrOut, vbFromUnicode))
      If Not IIStream Is Nothing Then
         InitGDI
         If GdipLoadImageFromStream(IIStream, LnhImage) = 0 Then
            If GdipCreateHBITMAPFromBitmap(LnhImage, LnhBmp, 0&) = 0 Then
               Set GetPictureQrCode = CreateBitmapPicture(LnhBmp, 0)
            End If
            GdipDisposeImage LnhImage
         End If
         TerminateGDI
      End If
   End If
End Function

'---------------------------------------------------------------------------------------
' Procedure : GetSafeURL
' DateTime  : 16/07/2018 11:02
' Author    : daniel_mesa
' Purpose   : DRMG T44728-R41288 Almacena la url
'---------------------------------------------------------------------------------------
'
Private Function GetSafeURL(ByVal sText As String) As String
   Dim StASC As String 'Almacena caracter
   Dim ByChr As Byte 'Almacena caracter ASCII
   Dim StHex As String 'Almacena caracter hexadecimal
   Dim LnI As Long 'Contador
   For LnI = 1 To Len(sText)
      StASC = Mid$(sText, LnI, 1)
      ByChr = Asc(StASC)
      If (ByChr > 47 And ByChr < 58) Or (ByChr > 64 And ByChr < 91) Or (ByChr > 96 And ByChr < 123) Then
         GetSafeURL = GetSafeURL & StASC
      Else
         StHex = Hex(ByChr)
         If Len(StHex) = 1 Then
            GetSafeURL = GetSafeURL & "%0" & StHex
         Else
            GetSafeURL = GetSafeURL & "%" & StHex
         End If
      End If
   Next
End Function

'---------------------------------------------------------------------------------------
' Procedure : InitGDI
' DateTime  : 16/07/2018 11:02
' Author    : daniel_mesa
' Purpose   : DRMG T44728-R41288 Envia token y posicion inicial a la libreria GdiplusStartup
'---------------------------------------------------------------------------------------
'
Private Sub InitGDI()
   Dim GdipStartupInput As GDIPlusStartupInput 'Instancia el objeto
   GdipStartupInput.LnGdiPlusVersion = LnGdiPlusVersion
   Call GdiplusStartup(LnGdipToken, GdipStartupInput, ByVal 0)
End Sub

'---------------------------------------------------------------------------------------
' Procedure : iparseIsArrayEmpty
' DateTime  : 16/07/2018 11:03
' Author    : daniel_mesa
' Purpose   : DRMG T44728-R41288 Verifica si se ha inicializado un arreglo
'---------------------------------------------------------------------------------------
'
Private Function iparseIsArrayEmpty(FarPointer As Long) As Long
   'test to see if an array has been initialized
   CopyMemory iparseIsArrayEmpty, ByVal FarPointer, 4&
End Function

'---------------------------------------------------------------------------------------
' Procedure : LongToHtml
' DateTime  : 16/07/2018 11:04
' Author    : daniel_mesa
' Purpose   : DRMG T44728-R41288 Envia el color ingresado
'---------------------------------------------------------------------------------------
'
Private Function LongToHtml(ByVal lColor As OLE_COLOR) As String
   Dim ByCl(3) As Byte 'Arreglo que guarda el color
   OleTranslateColor lColor, 0, VarPtr(ByCl(0))
   LongToHtml = Format(Hex(ByCl(0)), "00") & Format(Hex(ByCl(1)), "00") & Format(Hex(ByCl(2)), "00")
End Function

'---------------------------------------------------------------------------------------
' Procedure : TerminateGDI
' DateTime  : 16/07/2018 11:04
' Author    : daniel_mesa
' Purpose   : DRMG T44728-R41288 Ingresa token a la libreria gdiplusshutdown
'---------------------------------------------------------------------------------------
'
Private Sub TerminateGDI()
   Call GdiplusShutdown(LnGdipToken)
End Sub

'---------------------------------------------------------------------------------------
' Procedure : Unicode2UTF8
' DateTime  : 16/07/2018 11:05
' Author    : daniel_mesa
' Purpose   : DRMG T44728-R41288 Codifica los caracteres
'---------------------------------------------------------------------------------------
'
Private Function Unicode2UTF8(ByVal strUnicode As String) As String
   Dim LnenUNI As Long 'Almacena la longitud del texto
   Dim LnBufferSize As Long 'Almacena el tama�o del arreglo
   Dim LnLenUTF8 As Long 'Almacena el ancho de la imagen en byte
   Dim BybUTF8() As Byte 'Almacena la informacion generada
   LnenUNI = Len(strUnicode)
   If LnenUNI = 0 Then Exit Function
   LnBufferSize = LnenUNI * 3 + 1
   ReDim BybUTF8(LnBufferSize - 1)
   LnLenUTF8 = WideCharToMultiByte(LnCP_UTF8, 0, StrPtr(strUnicode), LnenUNI, BybUTF8(0), LnBufferSize, vbNullString, 0)
   If LnLenUTF8 Then
      ReDim Preserve BybUTF8(LnLenUTF8 - 1)
      Unicode2UTF8 = StrConv(BybUTF8, vbUnicode)
   End If
End Function





