VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Begin VB.Form FrmEjecReporteUsuario 
   BorderStyle     =   4  'Fixed ToolWindow
   ClientHeight    =   4710
   ClientLeft      =   45
   ClientTop       =   375
   ClientWidth     =   5295
   Icon            =   "FrmEjecReporteUsuario.frx":0000
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   4710
   ScaleWidth      =   5295
   ShowInTaskbar   =   0   'False
   Begin VB.Frame FramSer 
      Caption         =   "Reportes - Usuario "
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   4095
      Left            =   120
      TabIndex        =   0
      Top             =   480
      Width           =   5055
      Begin MSComctlLib.ListView Lstw 
         Height          =   3255
         Left            =   120
         TabIndex        =   2
         Top             =   720
         Width           =   4815
         _ExtentX        =   8493
         _ExtentY        =   5741
         View            =   3
         LabelEdit       =   1
         MultiSelect     =   -1  'True
         LabelWrap       =   -1  'True
         HideSelection   =   -1  'True
         AllowReorder    =   -1  'True
         Checkboxes      =   -1  'True
         FullRowSelect   =   -1  'True
         _Version        =   393217
         ForeColor       =   -2147483640
         BackColor       =   -2147483643
         BorderStyle     =   1
         Appearance      =   1
         NumItems        =   3
         BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Text            =   "C�digo"
            Object.Width           =   1764
         EndProperty
         BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   1
            Text            =   "Descripci�n"
            Object.Width           =   6174
         EndProperty
         BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   2
            Text            =   "Archivo"
            Object.Width           =   7937
         EndProperty
      End
      Begin MSComctlLib.ImageList Img_List 
         Left            =   0
         Top             =   0
         _ExtentX        =   1005
         _ExtentY        =   1005
         BackColor       =   -2147483643
         ImageWidth      =   20
         ImageHeight     =   20
         MaskColor       =   12632256
         _Version        =   393216
         BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
            NumListImages   =   13
            BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
               Picture         =   "FrmEjecReporteUsuario.frx":000C
               Key             =   ""
            EndProperty
            BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
               Picture         =   "FrmEjecReporteUsuario.frx":0570
               Key             =   ""
            EndProperty
            BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
               Picture         =   "FrmEjecReporteUsuario.frx":0AD4
               Key             =   ""
            EndProperty
            BeginProperty ListImage4 {2C247F27-8591-11D1-B16A-00C0F0283628} 
               Picture         =   "FrmEjecReporteUsuario.frx":1038
               Key             =   ""
            EndProperty
            BeginProperty ListImage5 {2C247F27-8591-11D1-B16A-00C0F0283628} 
               Picture         =   "FrmEjecReporteUsuario.frx":159C
               Key             =   ""
            EndProperty
            BeginProperty ListImage6 {2C247F27-8591-11D1-B16A-00C0F0283628} 
               Picture         =   "FrmEjecReporteUsuario.frx":1B00
               Key             =   ""
            EndProperty
            BeginProperty ListImage7 {2C247F27-8591-11D1-B16A-00C0F0283628} 
               Picture         =   "FrmEjecReporteUsuario.frx":205A
               Key             =   ""
            EndProperty
            BeginProperty ListImage8 {2C247F27-8591-11D1-B16A-00C0F0283628} 
               Picture         =   "FrmEjecReporteUsuario.frx":25B4
               Key             =   ""
            EndProperty
            BeginProperty ListImage9 {2C247F27-8591-11D1-B16A-00C0F0283628} 
               Picture         =   "FrmEjecReporteUsuario.frx":2E06
               Key             =   ""
            EndProperty
            BeginProperty ListImage10 {2C247F27-8591-11D1-B16A-00C0F0283628} 
               Picture         =   "FrmEjecReporteUsuario.frx":3360
               Key             =   ""
            EndProperty
            BeginProperty ListImage11 {2C247F27-8591-11D1-B16A-00C0F0283628} 
               Picture         =   "FrmEjecReporteUsuario.frx":38BA
               Key             =   ""
            EndProperty
            BeginProperty ListImage12 {2C247F27-8591-11D1-B16A-00C0F0283628} 
               Picture         =   "FrmEjecReporteUsuario.frx":3E14
               Key             =   ""
            EndProperty
            BeginProperty ListImage13 {2C247F27-8591-11D1-B16A-00C0F0283628} 
               Picture         =   "FrmEjecReporteUsuario.frx":436E
               Key             =   ""
            EndProperty
         EndProperty
      End
      Begin MSComctlLib.ImageList Img_Dis 
         Left            =   720
         Top             =   0
         _ExtentX        =   1005
         _ExtentY        =   1005
         BackColor       =   -2147483643
         ImageWidth      =   20
         ImageHeight     =   20
         MaskColor       =   12632256
         _Version        =   393216
         BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
            NumListImages   =   13
            BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
               Picture         =   "FrmEjecReporteUsuario.frx":5048
               Key             =   ""
            EndProperty
            BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
               Picture         =   "FrmEjecReporteUsuario.frx":55A2
               Key             =   ""
            EndProperty
            BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
               Picture         =   "FrmEjecReporteUsuario.frx":5AFC
               Key             =   ""
            EndProperty
            BeginProperty ListImage4 {2C247F27-8591-11D1-B16A-00C0F0283628} 
               Picture         =   "FrmEjecReporteUsuario.frx":6060
               Key             =   ""
            EndProperty
            BeginProperty ListImage5 {2C247F27-8591-11D1-B16A-00C0F0283628} 
               Picture         =   "FrmEjecReporteUsuario.frx":65C4
               Key             =   ""
            EndProperty
            BeginProperty ListImage6 {2C247F27-8591-11D1-B16A-00C0F0283628} 
               Picture         =   "FrmEjecReporteUsuario.frx":6B1E
               Key             =   ""
            EndProperty
            BeginProperty ListImage7 {2C247F27-8591-11D1-B16A-00C0F0283628} 
               Picture         =   "FrmEjecReporteUsuario.frx":7078
               Key             =   ""
            EndProperty
            BeginProperty ListImage8 {2C247F27-8591-11D1-B16A-00C0F0283628} 
               Picture         =   "FrmEjecReporteUsuario.frx":75D2
               Key             =   ""
            EndProperty
            BeginProperty ListImage9 {2C247F27-8591-11D1-B16A-00C0F0283628} 
               Picture         =   "FrmEjecReporteUsuario.frx":7B2C
               Key             =   ""
            EndProperty
            BeginProperty ListImage10 {2C247F27-8591-11D1-B16A-00C0F0283628} 
               Picture         =   "FrmEjecReporteUsuario.frx":8086
               Key             =   ""
            EndProperty
            BeginProperty ListImage11 {2C247F27-8591-11D1-B16A-00C0F0283628} 
               Picture         =   "FrmEjecReporteUsuario.frx":85E0
               Key             =   ""
            EndProperty
            BeginProperty ListImage12 {2C247F27-8591-11D1-B16A-00C0F0283628} 
               Picture         =   "FrmEjecReporteUsuario.frx":8B3A
               Key             =   ""
            EndProperty
            BeginProperty ListImage13 {2C247F27-8591-11D1-B16A-00C0F0283628} 
               Picture         =   "FrmEjecReporteUsuario.frx":9094
               Key             =   ""
            EndProperty
         EndProperty
      End
      Begin VB.Label Lbl_PanelB 
         Caption         =   "Seleccione el reporte a ejecutar"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   120
         TabIndex        =   1
         Top             =   360
         Width           =   2775
      End
   End
   Begin MSComctlLib.Toolbar Tlb_Opciones 
      Align           =   1  'Align Top
      Height          =   480
      Left            =   0
      TabIndex        =   3
      Top             =   0
      Width           =   5295
      _ExtentX        =   9340
      _ExtentY        =   847
      ButtonWidth     =   714
      ButtonHeight    =   688
      Appearance      =   1
      ImageList       =   "Img_List"
      DisabledImageList=   "Img_Dis"
      _Version        =   393216
      BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
         NumButtons      =   3
         BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Enabled         =   0   'False
            ImageIndex      =   12
            Style           =   3
         EndProperty
         BeginProperty Button2 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "I"
            Object.ToolTipText     =   "Imprimir"
            ImageIndex      =   3
         EndProperty
         BeginProperty Button3 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "A"
            Object.ToolTipText     =   "Ayuda"
            ImageIndex      =   6
         EndProperty
      EndProperty
   End
End
Attribute VB_Name = "FrmEjecReporteUsuario"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Dim arrEventos()  As Variant
Public OtroCampo  As String
Public R_Columna  As Integer
Public R_Opcion
Public CampoPadre As String
Public CampoHijo  As String
Public CampoRela  As String
Public TablaRela  As String
Public Titulo     As String
'Dim Tabla         As String        'DEPURACION DE CODIGO
Dim Reporte       As String

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
   Select Case KeyCode
      Case vbKeyF1:
      Case vbKeyF11: If Tlb_Opciones.Buttons("I").Enabled = True Then Call Tlb_Opciones_ButtonClick(Tlb_Opciones.Buttons("I"))
      Case vbKeyF12: Unload Me
   End Select
End Sub

Private Sub Form_Load()
   Call EventosControles(Me, arrEventos)
   Call CenterForm(MDI_Inventarios, Me)
   
   Desde = "REPORTES_USUARIO"
   Campos = "TX_CODI_REPT, TX_DESC_REPT, TX_ARCH_REPT"
   OtroCampo = "TX_ARCH_REPT"
   Call LoadListView(Lstw, Desde, Campos, OtroCampo, "")
End Sub

Private Sub Form_Unload(Cancel As Integer)
   Call DestruirControles(arrEventos)
End Sub

Private Sub Tlb_Opciones_ButtonClick(ByVal Button As MSComctlLib.Button)
   Select Case Button.Key
      Case "I": Call Imprimir
      Case "E": Unload Me
      Case "A:"
   End Select
End Sub

Private Sub Lstw_ItemCheck(ByVal Item As MSComctlLib.ListItem)
   Dim I As Integer
   For I = 1 To Lstw.ListItems.Count
      Lstw.ListItems(I).Checked = False
   Next I
   Item.Checked = True
   Reporte = Item.Key
End Sub

Public Sub Imprimir()
On Error GoTo ERR

   Dim Conex As String
   Dim I     As Integer

   With MDI_Inventarios.Crys_Listar
      Conex = .Connect
      .WindowTitle = "Reportes Usuario"
      .Destination = crptToWindow
      .ReportFileName = Mid(Reporte, 2, Len(Reporte))
      .Action = 1
   End With
   Exit Sub

ERR:

   Call ConvertErr
End Sub
