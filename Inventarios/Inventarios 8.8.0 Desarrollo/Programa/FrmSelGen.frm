VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "MSFLXGRD.OCX"
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Begin VB.Form FrmSelgen 
   BorderStyle     =   1  'Fixed Single
   ClientHeight    =   5640
   ClientLeft      =   45
   ClientTop       =   225
   ClientWidth     =   9495
   Icon            =   "FrmSelGen.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   5640
   ScaleWidth      =   9495
   StartUpPosition =   3  'Windows Default
   Begin VB.Frame Fram_Base 
      Height          =   5535
      Left            =   120
      TabIndex        =   0
      Top             =   0
      Width           =   9255
      Begin MSComctlLib.StatusBar Stbar 
         Height          =   255
         Left            =   120
         TabIndex        =   15
         Top             =   4560
         Width           =   9015
         _ExtentX        =   15901
         _ExtentY        =   450
         _Version        =   393216
         BeginProperty Panels {8E3867A5-8586-11D1-B16A-00C0F0283628} 
            NumPanels       =   5
            BeginProperty Panel1 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
               Text            =   "Registro"
               TextSave        =   "Registro"
            EndProperty
            BeginProperty Panel2 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            EndProperty
            BeginProperty Panel3 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
               Text            =   "de "
               TextSave        =   "de "
            EndProperty
            BeginProperty Panel4 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            EndProperty
            BeginProperty Panel5 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
               Bevel           =   0
               Object.Width           =   4939
               MinWidth        =   4939
            EndProperty
         EndProperty
      End
      Begin VB.ComboBox Cbo_Texto 
         Height          =   315
         ItemData        =   "FrmSelGen.frx":058A
         Left            =   3240
         List            =   "FrmSelGen.frx":05A0
         Style           =   2  'Dropdown List
         TabIndex        =   4
         Top             =   240
         Visible         =   0   'False
         Width           =   1095
      End
      Begin VB.ComboBox Cbo_Numero 
         Height          =   315
         ItemData        =   "FrmSelGen.frx":05BF
         Left            =   3240
         List            =   "FrmSelGen.frx":05D2
         Style           =   2  'Dropdown List
         TabIndex        =   3
         Top             =   240
         Visible         =   0   'False
         Width           =   1095
      End
      Begin VB.CommandButton Cmd_Seleccion 
         Caption         =   "&SELECCIONAR"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   6
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   495
         Left            =   3000
         Picture         =   "FrmSelGen.frx":05E7
         Style           =   1  'Graphical
         TabIndex        =   11
         Top             =   4920
         Width           =   1335
      End
      Begin VB.CommandButton Cmd_Cancelar 
         Caption         =   "&CANCELAR"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   6
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   495
         Left            =   5040
         Picture         =   "FrmSelGen.frx":0B39
         Style           =   1  'Graphical
         TabIndex        =   12
         Top             =   4920
         Width           =   1335
      End
      Begin VB.ComboBox Txt_Seleccion 
         Height          =   315
         ItemData        =   "FrmSelGen.frx":0E7B
         Left            =   5040
         List            =   "FrmSelGen.frx":0E88
         TabIndex        =   6
         Top             =   240
         Width           =   2775
      End
      Begin VB.ComboBox CboBuscar 
         Height          =   315
         Left            =   960
         Style           =   2  'Dropdown List
         TabIndex        =   2
         Top             =   240
         Width           =   2175
      End
      Begin VB.CommandButton Cmd_Buscar 
         Caption         =   "&BUSCAR AHORA"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   6
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   495
         Left            =   7920
         Picture         =   "FrmSelGen.frx":0E95
         Style           =   1  'Graphical
         TabIndex        =   9
         Top             =   240
         Width           =   1215
      End
      Begin MSFlexGridLib.MSFlexGrid Grd_Selecciones 
         Height          =   3585
         Left            =   120
         TabIndex        =   10
         Top             =   960
         Width           =   9015
         _ExtentX        =   15901
         _ExtentY        =   6324
         _Version        =   393216
         Rows            =   1
         Cols            =   0
         FixedCols       =   0
         BackColorBkg    =   16777215
         AllowUserResizing=   1
      End
      Begin VB.ComboBox Txt_Hasta 
         Height          =   315
         ItemData        =   "FrmSelGen.frx":12C7
         Left            =   5040
         List            =   "FrmSelGen.frx":12D1
         TabIndex        =   8
         Top             =   600
         Visible         =   0   'False
         Width           =   2775
      End
      Begin MSComCtl2.DTPicker Dtp_Desde 
         Height          =   300
         Left            =   5040
         TabIndex        =   13
         Top             =   240
         Visible         =   0   'False
         Width           =   1695
         _ExtentX        =   2990
         _ExtentY        =   529
         _Version        =   393216
         Format          =   100663297
         CurrentDate     =   37789
      End
      Begin MSComCtl2.DTPicker Dtp_Hasta 
         Height          =   300
         Left            =   5040
         TabIndex        =   14
         Top             =   600
         Visible         =   0   'False
         Width           =   1695
         _ExtentX        =   2990
         _ExtentY        =   529
         _Version        =   393216
         Format          =   100663297
         CurrentDate     =   37789
      End
      Begin VB.Label Lbl_Hasta 
         Caption         =   "&Hasta"
         Height          =   255
         Left            =   4440
         TabIndex        =   7
         Top             =   600
         Visible         =   0   'False
         Width           =   495
      End
      Begin VB.Label Lbl_Desde 
         Caption         =   "Texto"
         Height          =   255
         Left            =   4440
         TabIndex        =   5
         Top             =   240
         Width           =   495
      End
      Begin VB.Label Lbl_Buscar 
         Caption         =   "Buscar por"
         Height          =   255
         Left            =   120
         TabIndex        =   1
         Top             =   240
         Width           =   855
      End
   End
End
Attribute VB_Name = "FrmSelgen"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Dim arrEventos() As Variant
Public Sel_Cam
Public Sel_Mul    As Boolean
Public Sel_Tab    As String
Public Sel_Con    As String
Public Sel_Nro    As Integer
Public Sel_ColOrden As Integer
Dim Arrsel()      As Variant
Dim Sel_Campos    As String
Public TituloSel  As String
Dim Orden         As Integer  'Indicador de Ordenamientos
Private Sub Cbo_Numero_KeyPress(KeyAscii As Integer)
   Call Cambiar_Enter(KeyAscii)
End Sub

Private Sub Cbo_Texto_KeyPress(KeyAscii As Integer)
   Call Cambiar_Enter(KeyAscii)
End Sub

Private Sub CboBuscar_Click()
   If Sel_Mul = False Then Call Mostrar_Combo
End Sub

Private Sub CboBuscar_KeyPress(KeyAscii As Integer)
   Call Cambiar_Enter(KeyAscii)
End Sub
Private Sub CboBuscar_Validate(Cancel As Boolean)
   If Sel_Cam(3, CboBuscar.ItemData(CboBuscar.ListIndex)) = "F" Then
      Dtp_Desde.ZOrder 0: Dtp_Desde.Visible = True
      Dtp_Hasta.ZOrder 0: Dtp_Hasta.Visible = True
      Lbl_Desde.Caption = "Desde": Lbl_Desde.Visible = True
      Lbl_Hasta.Caption = "Hasta": Lbl_Hasta.Visible = True
      Txt_Seleccion.Visible = False
   Else
      Dtp_Desde.Visible = False
      Dtp_Hasta.Visible = False
      Lbl_Hasta.Visible = False
      Txt_Seleccion.Visible = True
      Txt_Hasta.Visible = False
      If Sel_Mul = True Then Txt_Hasta.Visible = True: Lbl_Hasta.Visible = True
   End If
   If Sel_Mul = False Then Call Mostrar_Combo
End Sub

Private Sub Form_Unload(Cancel As Integer)
   Call DestruirControles(arrEventos)
End Sub

Private Sub Grd_selecciones_DblClick()
   Call Cmd_Seleccion_Click
End Sub

Private Sub Grd_Selecciones_EnterCell()
   Stbar.Panels(2) = Grd_Selecciones.Row
End Sub

Private Sub Grd_selecciones_KeyPress(KeyAscii As Integer)
   If KeyAscii = 13 Then Call Cmd_Seleccion_Click
End Sub

Private Sub Cmd_Cancelar_Click()
   Codigo = NUL$
   Unload Me
End Sub

Private Sub Txt_Seleccion_GotFocus()
   Txt_Seleccion.SelStart = 0
   Txt_Seleccion.SelLength = Len(Txt_Seleccion.Text)
End Sub
Private Sub Txt_Seleccion_KeyPress(KeyAscii As Integer)
   If Sel_Cam(3, CboBuscar.ItemData(CboBuscar.ListIndex)) = "T" Then
      Call ValKeyAlfaNum(KeyAscii)
   ElseIf Sel_Cam(3, CboBuscar.ItemData(CboBuscar.ListIndex)) = "N" Then
      Call ValKeyNum(KeyAscii)
   End If
End Sub

Private Sub Form_Load()
Dim I As Long
   
   Call CenterForm(MDI_Inventarios, Me)
   Call EventosControles(Me, arrEventos)
   Me.Left = 3000
   Me.Top = 2000
   Me.Caption = TituloSel
   
   Codigo = NUL$
   Sel_Campos = NUL$
   Orden = 0
   
   'Carga las opciones de busqueda
   CboBuscar.Clear
   For I = 0 To UBound(Sel_Cam, 2)
      If Val(Sel_Cam(2, I)) > 0 Then
         CboBuscar.AddItem Sel_Cam(1, I)
         CboBuscar.ItemData(CboBuscar.NewIndex) = I
      End If
      With Grd_Selecciones
         .Cols = I + 1
         .Row = 0
         .Col = I
         .Text = Sel_Cam(1, I)
         .CellAlignment = 5
         If Val(Sel_Cam(2, I)) >= 0 Then
            .ColWidth(I) = Sel_Cam(2, I)
         ElseIf Val(Sel_Cam(2, I)) < 0 Then
            .ColWidth(I) = Abs(Sel_Cam(2, I))
            .MergeCells = flexMergeRestrictColumns
            .MergeCol(0) = True
            .MergeCol(1) = True
            .MergeCol(2) = True
            .MergeCol(3) = True
         End If
         Sel_Campos = Sel_Campos & Sel_Cam(0, I) & IIf(I <> UBound(Sel_Cam, 2), Coma, NUL$)
      End With
      
   Next I
   
   CboBuscar.ListIndex = 0
   If Sel_Mul = True Then
      ReDim Arr(0) As Variant
      Lbl_Desde.Caption = "Desde"
      Lbl_Hasta.Visible = True
      Txt_Hasta.Visible = True
      CboBuscar.RemoveItem (CboBuscar.ListCount - 1)
      Cbo_Numero.Visible = False
      Cbo_Texto.Visible = False
      CboBuscar.Width = CboBuscar.Width + 1000
      Sel_Registros = Arr
   Else
      Call Mostrar_Combo
   End If
   
End Sub

Private Sub Cmd_Seleccion_Click()
Dim Arrsel() As Variant
Dim I As Long
Dim J As Long
   
   If Sel_Mul = False Then
      Grd_Selecciones.Col = Sel_Nro
      Codigo = IIf(Grd_Selecciones.Row = 0, NUL$, Grd_Selecciones.Text)
   Else
      If Grd_Selecciones.Rows > 1 Then
         ReDim Arrsel(Grd_Selecciones.Rows - 1, Grd_Selecciones.Cols - 1)
         For I = 1 To Grd_Selecciones.Rows - 1
            For J = 0 To Grd_Selecciones.Cols - 1
               Arrsel(I - 1, J) = Grd_Selecciones.TextMatrix(I, J)
            Next J
         Next I
      Else
         ReDim Arrsel(Grd_Selecciones.Rows - 1, Grd_Selecciones.Cols - 1)
      End If
   End If
   Sel_Registros = Arrsel
   Unload Me
   
End Sub
Private Sub Cmd_Buscar_Click()
   Dim CarCondi As String
   Dim CondUnion As String  'Condicion para uniones
   
   
   CondUnion = Sel_Con
   Cmd_Buscar.Enabled = False
   Cmd_Seleccion.Enabled = False
   Cmd_Cancelar.Enabled = False
   Grd_Selecciones.Rows = 1
   
   CarCondi = IIf(Sel_Cam(3, CboBuscar.ItemData(CboBuscar.ListIndex)) = "N", "", IIf(Sel_Cam(3, CboBuscar.ItemData(CboBuscar.ListIndex)) = "T", Comi, ComiF))
   If Sel_Mul = False Then
      If Sel_Cam(3, CboBuscar.ItemData(CboBuscar.ListIndex)) = "N" Then
         Condicion = Sel_Cam(0, CboBuscar.ItemData(CboBuscar.ListIndex)) & Cbo_Numero.Text & CarCondi & Val(Txt_Seleccion.Text) & CarCondi
         Condicion = Replace(Condicion, "DISTINCT", "")
      ElseIf Sel_Cam(3, CboBuscar.ItemData(CboBuscar.ListIndex)) = "F" Then
         Condicion = Sel_Cam(0, CboBuscar.ItemData(CboBuscar.ListIndex)) & ">" & FFechaCon(DateAdd("d", -1, Dtp_Desde.Value))
         Condicion = Condicion & " AND " & Sel_Cam(0, CboBuscar.ItemData(CboBuscar.ListIndex)) & "<" & FFechaCon(DateAdd("d", 1, Dtp_Hasta.Value))
         Condicion = Replace(Condicion, "DISTINCT", "")
      Else
         If Cbo_Texto.ListIndex <> 0 Then
            Condicion = Sel_Cam(0, CboBuscar.ItemData(CboBuscar.ListIndex)) & Cbo_Texto.Text & CarCondi & Txt_Seleccion.Text & CarCondi
         Else
            Condicion = Sel_Cam(0, CboBuscar.ItemData(CboBuscar.ListIndex)) & " LIKE " & CarCondi & CaractLike & Txt_Seleccion.Text & IIf(Sel_Cam(3, CboBuscar.ItemData(CboBuscar.ListIndex)) = "N", NUL$, CaractLike) & CarCondi
         End If
         Condicion = Replace(Condicion, "DISTINCT", "")
      End If
   Else
      If Sel_Cam(3, CboBuscar.ItemData(CboBuscar.ListIndex)) = "F" Then
         Condicion = Sel_Cam(0, CboBuscar.ItemData(CboBuscar.ListIndex)) & ">" & FFechaCon(DateAdd("d", -1, Dtp_Desde.Value))
         Condicion = Condicion & " AND " & Sel_Cam(0, CboBuscar.ItemData(CboBuscar.ListIndex)) & "<" & FFechaCon(DateAdd("d", 1, Dtp_Hasta.Value))
      Else
         If Trim(Txt_Seleccion) <> NUL$ And Trim(Txt_Hasta) <> NUL$ Then
            Condicion = Sel_Cam(0, CboBuscar.ItemData(CboBuscar.ListIndex)) & " >= " & CarCondi & Txt_Seleccion.Text & IIf(Sel_Cam(3, CboBuscar.ItemData(CboBuscar.ListIndex)) = "N", NUL$, NUL$) & CarCondi
            Condicion = Condicion & " AND " & Sel_Cam(0, CboBuscar.ItemData(CboBuscar.ListIndex)) & " <= " & CarCondi & Txt_Hasta.Text & IIf(Sel_Cam(3, CboBuscar.ItemData(CboBuscar.ListIndex)) = "N", NUL$, NUL$) & CarCondi
         Else
            'Busca que contenga el inicio
            If Trim(Txt_Seleccion) <> NUL$ Then
               Condicion = Sel_Cam(0, CboBuscar.ItemData(CboBuscar.ListIndex)) & " LIKE " & CarCondi & CaractLike & Txt_Seleccion.Text & IIf(Sel_Cam(3, CboBuscar.ItemData(CboBuscar.ListIndex)) = "N", NUL$, CaractLike) & CarCondi
            Else
               Condicion = Sel_Cam(0, CboBuscar.ItemData(CboBuscar.ListIndex)) & " LIKE " & CarCondi & CaractLike & Txt_Hasta.Text & IIf(Sel_Cam(3, CboBuscar.ItemData(CboBuscar.ListIndex)) = "N", NUL$, CaractLike) & CarCondi
            End If
         End If
         
      End If
   End If
   
   If InStr(Condicion, "%") > 0 Then Mid(Condicion, InStr(Condicion, "%"), 1) = NUL$
   
   'Verifica si hay palabras de union, para agregar la condicion a cada union
   Dim posini As Long
   
   posini = 1
   If Sel_Con <> NUL$ Then
      CondUnion = Sel_Con
   Else
      CondUnion = Condicion
   End If
   Do While InStr(posini, UCase(CondUnion), "UNION") <> 0
      posini = InStr(posini, UCase(CondUnion), "UNION")
      If Condicion <> NUL$ Then
         CondUnion = Mid(CondUnion, 1, posini - 1) & " AND " & Condicion & " " & Mid(CondUnion, posini, Len(CondUnion) - posini + 1)
         posini = (posini + Len(Condicion) + Len(" AND  ") + 1)
      End If
   Loop
   
   If Sel_Con <> NUL$ Then
      Condicion = CondUnion & IIf(Condicion <> NUL$, " AND " & Condicion, NUL$)
   Else
      CondUnion = Condicion
   End If
   If Sel_ColOrden = 0 Then Sel_ColOrden = 1
   Condicion = Condicion & " ORDER BY " & Sel_ColOrden
   Result = LoadfGrid(Grd_Selecciones, Sel_Tab, Sel_Campos, Condicion)
   If Not Encontro Then
      Call Mensaje1("Ning�n elemento encontrado", 3)
   Else
      On Error Resume Next
      Grd_Selecciones.Col = 0
      Grd_Selecciones.Row = 1
      Grd_Selecciones.SetFocus
   End If
   
   If Grd_Selecciones.Rows > 1 Then
      Grd_Selecciones.TopRow = 1
      Grd_Selecciones.Row = 1
      Stbar.Panels(2) = Grd_Selecciones.Row
      Stbar.Panels(4) = Grd_Selecciones.Rows - 1
   Else
      Stbar.Panels(2) = NUL$
      Stbar.Panels(4) = NUL$
   End If
   
   If Val(Sel_Cam(2, UBound(Sel_Cam, 2) - 1)) < 0 Then
      Grd_Selecciones.ColWidth(UBound(Sel_Cam, 2)) = Abs(Sel_Cam(2, UBound(Sel_Cam, 2)))
      Grd_Selecciones.MergeCells = flexMergeRestrictAll
      Grd_Selecciones.MergeCol(0) = True
      Grd_Selecciones.MergeCol(1) = True
      Grd_Selecciones.MergeCol(2) = True
      Grd_Selecciones.MergeCol(3) = True
   End If
   
   Cmd_Seleccion.Enabled = True
   Cmd_Cancelar.Enabled = True
   Cmd_Buscar.Enabled = True
End Sub

Private Sub Grd_Selecciones_Click()
   If Grd_Selecciones.MouseRow = 0 Then
      If Orden <> 0 Then
         Grd_Selecciones.Sort = 1
         Orden = 0
      Else
         Grd_Selecciones.Sort = 2
         Orden = 1
      End If
   Else
      Stbar.Panels(2) = Grd_Selecciones.Row
   End If
End Sub

Private Sub Mostrar_Combo()
   Cbo_Numero.Visible = False
   Cbo_Texto.Visible = False
   If Sel_Cam(3, CboBuscar.ItemData(CboBuscar.ListIndex)) = "N" Then
      Cbo_Numero.Visible = True
      Cbo_Numero.ListIndex = 0
   Else
      Cbo_Texto.Visible = True
      Cbo_Texto.ListIndex = 0
   End If
End Sub
