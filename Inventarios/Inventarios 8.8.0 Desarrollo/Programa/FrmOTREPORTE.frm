VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Object = "{C932BA88-4374-101B-A56C-00AA003668DC}#1.1#0"; "MSMASK32.OCX"
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Object = "{0BA686C6-F7D3-101A-993E-0000C0EF6F5E}#1.0#0"; "THREED32.OCX"
Begin VB.Form FrmOTREPORTE 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Otros Reportes"
   ClientHeight    =   3360
   ClientLeft      =   2955
   ClientTop       =   1635
   ClientWidth     =   8190
   Icon            =   "FrmOTREPORTE.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   ScaleHeight     =   3360
   ScaleWidth      =   8190
   Begin VB.Frame framActions 
      Height          =   2085
      Left            =   6840
      TabIndex        =   1
      Top             =   720
      Width           =   1065
      Begin Threed.SSCommand Cmd_Botones 
         Height          =   705
         HelpContextID   =   120
         Index           =   2
         Left            =   120
         TabIndex        =   2
         ToolTipText     =   "Salir"
         Top             =   1140
         Width           =   855
         _Version        =   65536
         _ExtentX        =   1508
         _ExtentY        =   1244
         _StockProps     =   78
         Caption         =   "&SALIR"
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   6
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Picture         =   "FrmOTREPORTE.frx":058A
      End
      Begin Threed.SSCommand Cmd_Botones 
         Height          =   705
         Index           =   1
         Left            =   120
         TabIndex        =   3
         Top             =   360
         Width           =   855
         _Version        =   65536
         _ExtentX        =   1508
         _ExtentY        =   1244
         _StockProps     =   78
         Caption         =   "&IMPRIMIR"
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   6
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         RoundedCorners  =   0   'False
         Picture         =   "FrmOTREPORTE.frx":0C54
      End
   End
   Begin TabDlg.SSTab sstOTREPORTE 
      Height          =   2895
      Left            =   120
      TabIndex        =   0
      Top             =   240
      Width           =   6405
      _ExtentX        =   11298
      _ExtentY        =   5106
      _Version        =   393216
      Tabs            =   6
      TabsPerRow      =   6
      TabHeight       =   882
      TabCaption(0)   =   "Rotaci�n"
      TabPicture(0)   =   "FrmOTREPORTE.frx":131E
      Tab(0).ControlEnabled=   -1  'True
      Tab(0).Control(0)=   "Label1"
      Tab(0).Control(0).Enabled=   0   'False
      Tab(0).Control(1)=   "Label2"
      Tab(0).Control(1).Enabled=   0   'False
      Tab(0).Control(2)=   "Label10"
      Tab(0).Control(2).Enabled=   0   'False
      Tab(0).Control(3)=   "dtpROTHASTA"
      Tab(0).Control(3).Enabled=   0   'False
      Tab(0).Control(4)=   "dtpROTDESDE"
      Tab(0).Control(4).Enabled=   0   'False
      Tab(0).Control(5)=   "cmbBODEGA(0)"
      Tab(0).Control(5).Enabled=   0   'False
      Tab(0).ControlCount=   6
      TabCaption(1)   =   "Inactivos"
      TabPicture(1)   =   "FrmOTREPORTE.frx":133A
      Tab(1).ControlEnabled=   0   'False
      Tab(1).Control(0)=   "Label3"
      Tab(1).Control(0).Enabled=   0   'False
      Tab(1).Control(1)=   "Label11"
      Tab(1).Control(1).Enabled=   0   'False
      Tab(1).Control(2)=   "dtpINACORTE"
      Tab(1).Control(2).Enabled=   0   'False
      Tab(1).Control(3)=   "cmbBODEGA(1)"
      Tab(1).Control(3).Enabled=   0   'False
      Tab(1).ControlCount=   4
      TabCaption(2)   =   "Ventas"
      TabPicture(2)   =   "FrmOTREPORTE.frx":1356
      Tab(2).ControlEnabled=   0   'False
      Tab(2).Control(0)=   "Label4"
      Tab(2).Control(1)=   "Label5"
      Tab(2).Control(2)=   "Label12"
      Tab(2).Control(3)=   "dtpVENHASTA"
      Tab(2).Control(4)=   "dtpVENDESDE"
      Tab(2).Control(5)=   "cmbBODEGA(2)"
      Tab(2).ControlCount=   6
      TabCaption(3)   =   "Lotes"
      TabPicture(3)   =   "FrmOTREPORTE.frx":1372
      Tab(3).ControlEnabled=   0   'False
      Tab(3).Control(0)=   "Label6"
      Tab(3).Control(1)=   "Label7"
      Tab(3).Control(2)=   "dtpHASLote"
      Tab(3).Control(3)=   "dtpDESLote"
      Tab(3).Control(4)=   "Frame1"
      Tab(3).Control(5)=   "lstARTICULOS"
      Tab(3).ControlCount=   6
      TabCaption(4)   =   "Gesti�n"
      TabPicture(4)   =   "FrmOTREPORTE.frx":138E
      Tab(4).ControlEnabled=   0   'False
      Tab(4).Control(0)=   "cmbAUDITORIA"
      Tab(4).Control(1)=   "dptHASlotes"
      Tab(4).Control(2)=   "Label13"
      Tab(4).Control(3)=   "Label8"
      Tab(4).ControlCount=   4
      TabCaption(5)   =   "Actas"
      TabPicture(5)   =   "FrmOTREPORTE.frx":13AA
      Tab(5).ControlEnabled=   0   'False
      Tab(5).Control(0)=   "Label9"
      Tab(5).Control(1)=   "Label14"
      Tab(5).Control(2)=   "cmbENTRADAS"
      Tab(5).Control(3)=   "txtNROENTRA"
      Tab(5).ControlCount=   4
      Begin MSMask.MaskEdBox txtNROENTRA 
         Height          =   375
         Left            =   -72240
         TabIndex        =   29
         Top             =   1800
         Width           =   1575
         _ExtentX        =   2778
         _ExtentY        =   661
         _Version        =   393216
         PromptInclude   =   0   'False
         MaxLength       =   7
         Mask            =   "9999999"
         PromptChar      =   "_"
      End
      Begin VB.ComboBox cmbENTRADAS 
         Height          =   315
         ItemData        =   "FrmOTREPORTE.frx":13C6
         Left            =   -72960
         List            =   "FrmOTREPORTE.frx":13D6
         Style           =   2  'Dropdown List
         TabIndex        =   28
         Top             =   1080
         Width           =   3735
      End
      Begin VB.ComboBox cmbAUDITORIA 
         Height          =   315
         ItemData        =   "FrmOTREPORTE.frx":1401
         Left            =   -72840
         List            =   "FrmOTREPORTE.frx":1411
         Style           =   2  'Dropdown List
         TabIndex        =   27
         Top             =   1095
         Width           =   2535
      End
      Begin MSComctlLib.ListView lstARTICULOS 
         Height          =   1575
         Left            =   -74880
         TabIndex        =   24
         Top             =   720
         Width           =   4815
         _ExtentX        =   8493
         _ExtentY        =   2778
         View            =   3
         LabelEdit       =   1
         MultiSelect     =   -1  'True
         LabelWrap       =   -1  'True
         HideSelection   =   -1  'True
         FullRowSelect   =   -1  'True
         GridLines       =   -1  'True
         _Version        =   393217
         ForeColor       =   -2147483640
         BackColor       =   -2147483643
         BorderStyle     =   1
         Appearance      =   1
         NumItems        =   2
         BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Key             =   "COD"
            Text            =   "C�digo"
            Object.Width           =   2540
         EndProperty
         BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   1
            Key             =   "NOM"
            Text            =   "Nombre"
            Object.Width           =   5292
         EndProperty
      End
      Begin VB.ComboBox cmbBODEGA 
         Height          =   315
         Index           =   2
         Left            =   -72960
         Style           =   2  'Dropdown List
         TabIndex        =   19
         Top             =   2295
         Width           =   3975
      End
      Begin VB.ComboBox cmbBODEGA 
         Height          =   315
         Index           =   1
         Left            =   -72960
         Style           =   2  'Dropdown List
         TabIndex        =   18
         Top             =   2280
         Width           =   3975
      End
      Begin VB.ComboBox cmbBODEGA 
         Height          =   315
         Index           =   0
         Left            =   2040
         Style           =   2  'Dropdown List
         TabIndex        =   17
         Top             =   2295
         Width           =   3975
      End
      Begin VB.Frame Frame1 
         Caption         =   "Art�culos para:"
         Height          =   855
         Left            =   -74280
         TabIndex        =   14
         Top             =   1095
         Visible         =   0   'False
         Width           =   3615
         Begin VB.OptionButton opcCONSUMO 
            Caption         =   "Consumo"
            Height          =   255
            Left            =   1800
            TabIndex        =   16
            Top             =   360
            Width           =   1335
         End
         Begin VB.OptionButton opcVENTA 
            Caption         =   "Venta"
            Height          =   255
            Left            =   240
            TabIndex        =   15
            Top             =   360
            Value           =   -1  'True
            Width           =   1335
         End
      End
      Begin MSComCtl2.DTPicker dtpROTDESDE 
         Height          =   375
         Left            =   2400
         TabIndex        =   5
         Top             =   975
         Width           =   1575
         _ExtentX        =   2778
         _ExtentY        =   661
         _Version        =   393216
         Format          =   96010241
         CurrentDate     =   38393
      End
      Begin MSComCtl2.DTPicker dtpROTHASTA 
         Height          =   375
         Left            =   2400
         TabIndex        =   7
         Top             =   1605
         Width           =   1575
         _ExtentX        =   2778
         _ExtentY        =   661
         _Version        =   393216
         Format          =   96010241
         CurrentDate     =   38393
      End
      Begin MSComCtl2.DTPicker dtpINACORTE 
         Height          =   375
         Left            =   -72480
         TabIndex        =   9
         Top             =   1335
         Width           =   1575
         _ExtentX        =   2778
         _ExtentY        =   661
         _Version        =   393216
         Format          =   96010241
         CurrentDate     =   38393
      End
      Begin MSComCtl2.DTPicker dtpVENDESDE 
         Height          =   375
         Left            =   -72600
         TabIndex        =   11
         Top             =   975
         Width           =   1575
         _ExtentX        =   2778
         _ExtentY        =   661
         _Version        =   393216
         Format          =   96010241
         CurrentDate     =   38393
      End
      Begin MSComCtl2.DTPicker dtpVENHASTA 
         Height          =   375
         Left            =   -72600
         TabIndex        =   13
         Top             =   1605
         Width           =   1575
         _ExtentX        =   2778
         _ExtentY        =   661
         _Version        =   393216
         Format          =   96010241
         CurrentDate     =   38393
      End
      Begin MSComCtl2.DTPicker dtpDESLote 
         Height          =   375
         Left            =   -74040
         TabIndex        =   20
         Top             =   2400
         Width           =   1575
         _ExtentX        =   2778
         _ExtentY        =   661
         _Version        =   393216
         Format          =   96010241
         CurrentDate     =   38393
      End
      Begin MSComCtl2.DTPicker dtpHASLote 
         Height          =   375
         Left            =   -71760
         TabIndex        =   21
         Top             =   2400
         Width           =   1575
         _ExtentX        =   2778
         _ExtentY        =   661
         _Version        =   393216
         Format          =   96010241
         CurrentDate     =   38393
      End
      Begin MSComCtl2.DTPicker dptHASlotes 
         Height          =   375
         Left            =   -72840
         TabIndex        =   25
         Top             =   1935
         Width           =   1575
         _ExtentX        =   2778
         _ExtentY        =   661
         _Version        =   393216
         Format          =   96010241
         CurrentDate     =   38393
      End
      Begin VB.Label Label14 
         Alignment       =   1  'Right Justify
         Caption         =   "Consecutivo:"
         Height          =   255
         Left            =   -74400
         TabIndex        =   35
         Top             =   1130
         Width           =   1215
      End
      Begin VB.Label Label13 
         Alignment       =   1  'Right Justify
         Caption         =   "Tipo de gesti�n:"
         Height          =   255
         Left            =   -74280
         TabIndex        =   34
         Top             =   1130
         Width           =   1215
      End
      Begin VB.Label Label12 
         Alignment       =   1  'Right Justify
         Caption         =   "Bodega:"
         Height          =   255
         Left            =   -74400
         TabIndex        =   33
         Top             =   2350
         Width           =   1215
      End
      Begin VB.Label Label11 
         Alignment       =   1  'Right Justify
         Caption         =   "Bodega:"
         Height          =   255
         Left            =   -74280
         TabIndex        =   32
         Top             =   2350
         Width           =   1215
      End
      Begin VB.Label Label10 
         Alignment       =   1  'Right Justify
         Caption         =   "Bodega:"
         Height          =   255
         Left            =   600
         TabIndex        =   31
         Top             =   2350
         Width           =   1215
      End
      Begin VB.Label Label9 
         Alignment       =   1  'Right Justify
         Caption         =   "N�mero:"
         Height          =   255
         Left            =   -73800
         TabIndex        =   30
         Top             =   1920
         Width           =   1335
      End
      Begin VB.Label Label8 
         Alignment       =   1  'Right Justify
         Caption         =   "Hasta:"
         Height          =   255
         Left            =   -73800
         TabIndex        =   26
         Top             =   1995
         Width           =   750
      End
      Begin VB.Label Label7 
         Alignment       =   1  'Right Justify
         Caption         =   "Desde:"
         Height          =   255
         Left            =   -74880
         TabIndex        =   23
         Top             =   2460
         Width           =   750
      End
      Begin VB.Label Label6 
         Alignment       =   1  'Right Justify
         Caption         =   "Hasta:"
         Height          =   255
         Left            =   -72600
         TabIndex        =   22
         Top             =   2460
         Width           =   750
      End
      Begin VB.Label Label5 
         Alignment       =   1  'Right Justify
         Caption         =   "Hasta:"
         Height          =   255
         Left            =   -74040
         TabIndex        =   12
         Top             =   1725
         Width           =   1215
      End
      Begin VB.Label Label4 
         Alignment       =   1  'Right Justify
         Caption         =   "Desde:"
         Height          =   255
         Left            =   -74040
         TabIndex        =   10
         Top             =   1095
         Width           =   1215
      End
      Begin VB.Label Label3 
         Alignment       =   1  'Right Justify
         Caption         =   "Corte al:"
         Height          =   255
         Left            =   -73920
         TabIndex        =   8
         Top             =   1455
         Width           =   1215
      End
      Begin VB.Label Label2 
         Alignment       =   1  'Right Justify
         Caption         =   "Hasta:"
         Height          =   255
         Left            =   960
         TabIndex        =   6
         Top             =   1725
         Width           =   1215
      End
      Begin VB.Label Label1 
         Alignment       =   1  'Right Justify
         Caption         =   "Desde:"
         Height          =   255
         Left            =   960
         TabIndex        =   4
         Top             =   1095
         Width           =   1215
      End
   End
End
Attribute VB_Name = "FrmOTREPORTE"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Dim ArrUXB()
Private Dueno As New ElTercero
Public OpcCod        As String   'Opci�n de seguridad

Private Sub Cmd_Botones_Click(Index As Integer)
   Dim DocumentoVentas As String, DocumentoCompras As String
   Dim strARTICULOS As String, strFECORTE As String
   Dim FrmlENTRA As String, FrmlSALE As String, strREGISTROS As String
   Dim vUno As MSComctlLib.ListItem, vContar As Integer
   'Dim NroRegs As Long, ArrTPS() As Variant, strTITULO As String
   Dim ArrTPS() As Variant, strTITULO As String    'DEPURACION DE CODIGO
   'NoDefinido = 0
   'Cotizacion = 1
   'AnulaCotizacion = 20
   'ODCompra = 2
   'AnulaODCompra = 18
   'Entrada = 3
   'DevolucionEntrada = 4
   'AnulaEntrada = 13
   'Requisicion = 5
   'AnulaRequisicion = 21
   'Despacho = 6
   'DevolucionDespacho = 17
   'AnulaDespacho = 16
   'BajaConsumo = 8
   'DevolucionBaja = 9
   'AnulaBaja = 14
   'TRASLADO = 10
   'AnulaTraslado = 23
   'SALIDA = 19
   'DevolucionSalida = 24
   'AnulaSalida = 22
   'FacturaVenta = 11
   'DevolucionFacturaVenta = 12
   'AnulaFacturaVenta = 15
   'CompraDElementos = 25
   'AprovechaDonacion = 26
   'DevolucionAprovecha = 28
   'AnulaAprovecha = 27
   DocumentoVentas = "(" & FacturaVenta & "," & DevolucionFacturaVenta & "," & AnulaFacturaVenta
   DocumentoVentas = DocumentoVentas & "," & BajaConsumo & "," & DevolucionBaja & "," & AnulaBaja
   DocumentoVentas = DocumentoVentas & "," & Salida & "," & AnulaSalida 'JAUM T35216
   'DAHV M2861 -INICIO
   ' Se quitan las salidas.
   ' DocumentoVentas = DocumentoVentas & "," & Salida & "," & DevolucionSalida & "," & AnulaSalida & ")"
   DocumentoVentas = DocumentoVentas & ")"
   'DAHV M2861 -FIN
    
   DocumentoCompras = "(" & Entrada & "," & DevolucionEntrada & "," & AnulaEntrada
   DocumentoCompras = DocumentoCompras & "," & AprovechaDonacion & "," & DevolucionAprovecha & "," & AnulaAprovecha & ")"

   Select Case Index
      Case Is = 1
         Select Case Me.sstOTREPORTE.Tab
            Case Is = 0
            If Me.cmbBODEGA(Me.sstOTREPORTE.Tab).ListIndex < 0 Then Call MsgBox("Falta seleccionar la Bodega", vbExclamation): Exit Sub
FrmlSALE = "({IN_ROTAREPORTE.NU_AUTO_DOCU_ENCA}=" & FacturaVenta & " OR {IN_ROTAREPORTE.NU_AUTO_DOCU_ENCA}=" & DevolucionFacturaVenta & " OR {IN_ROTAREPORTE.NU_AUTO_DOCU_ENCA}=" & AnulaFacturaVenta
FrmlSALE = FrmlSALE & " OR {IN_ROTAREPORTE.NU_AUTO_DOCU_ENCA}=" & BajaConsumo & " OR {IN_ROTAREPORTE.NU_AUTO_DOCU_ENCA}=" & DevolucionBaja & " OR {IN_ROTAREPORTE.NU_AUTO_DOCU_ENCA}=" & AnulaBaja
FrmlSALE = FrmlSALE & " OR {IN_ROTAREPORTE.NU_AUTO_DOCU_ENCA}=" & Salida & " OR {IN_ROTAREPORTE.NU_AUTO_DOCU_ENCA}=" & DevolucionSalida & " OR {IN_ROTAREPORTE.NU_AUTO_DOCU_ENCA}=" & AnulaSalida & ")"

FrmlENTRA = "({IN_ROTAREPORTE.NU_AUTO_DOCU_ENCA}=" & Entrada & " OR {IN_ROTAREPORTE.NU_AUTO_DOCU_ENCA}=" & DevolucionEntrada & " OR {IN_ROTAREPORTE.NU_AUTO_DOCU_ENCA}=" & AnulaEntrada
FrmlENTRA = FrmlENTRA & " OR {IN_ROTAREPORTE.NU_AUTO_DOCU_ENCA}=" & AprovechaDonacion & " OR {IN_ROTAREPORTE.NU_AUTO_DOCU_ENCA}=" & DevolucionAprovecha & " OR {IN_ROTAREPORTE.NU_AUTO_DOCU_ENCA}=" & AnulaAprovecha & ")"
        
            If ExisteTABLA("IN_ROTAREPORTE") Then Result = EliminarTabla("IN_ROTAREPORTE")
            'FFechaCon
            'strARTICULOS = "SELECT NU_AUTO_ENCA FROM IN_ENCABEZADO, IN_DOCUMENTO WHERE " & _
                "NU_AUTO_DOCU_ENCA=NU_AUTO_DOCU AND (NU_AUTO_DOCU IN " & DocumentoVentas & _
                " OR NU_AUTO_DOCU IN " & DocumentoCompras & ")" & _
                " AND FE_CREA_ENCA>='" & FormatDateTime(Me.dtpROTDESDE.Value, vbShortDate) & "' AND FE_CREA_ENCA<='" & FormatDateTime(Me.dtpROTHASTA.Value, vbShortDate) & "'" & _
                " AND NU_AUTO_BODEORG_ENCA=" & Me.cmbBODEGA(Me.sstOTREPORTE.Tab).ItemData(Me.cmbBODEGA(Me.sstOTREPORTE.Tab).ListIndex)
             
            strARTICULOS = "SELECT NU_AUTO_ENCA FROM IN_ENCABEZADO, IN_DOCUMENTO WHERE " & _
                "NU_AUTO_DOCU_ENCA=NU_AUTO_DOCU AND (NU_AUTO_DOCU IN " & DocumentoVentas & _
                " OR NU_AUTO_DOCU IN " & DocumentoCompras & ")" & _
                " AND FE_CREA_ENCA>=" & FFechaCon(Me.dtpROTDESDE.Value) & " AND FE_CREA_ENCA<=" & FFechaCon(Me.dtpROTHASTA.Value) & _
                " AND NU_AUTO_BODEORG_ENCA=" & Me.cmbBODEGA(Me.sstOTREPORTE.Tab).ItemData(Me.cmbBODEGA(Me.sstOTREPORTE.Tab).ListIndex)      'PedroJ Def 3879
                
            strARTICULOS = "SELECT NU_AUTO_ARTI_KARD, NU_ENTRAD_KARD, NU_SALIDA_KARD, NU_MULT_KARD, " & _
                "NU_DIVI_KARD, NU_VALOR_KARD, NU_AUTO_UNVE_KARD, TX_NOMB_UNVE, CD_CODI_ARTI, NO_NOMB_ARTI, NU_AUTO_DOCU_ENCA, " & _
                "TX_NOMB_BODE" & _
                " INTO IN_ROTAREPORTE FROM IN_KARDEX, IN_UNDVENTA, ARTICULO, IN_ENCABEZADO, IN_BODEGA WHERE" & _
                " NU_AUTO_UNVE_KARD=NU_AUTO_UNVE" & _
                " AND NU_AUTO_ARTI_KARD=NU_AUTO_ARTI" & _
                " AND NU_AUTO_ORGCABE_KARD=NU_AUTO_ENCA" & _
                " AND NU_AUTO_BODE_KARD=NU_AUTO_BODE" & _
                " AND NU_AUTO_ORGCABE_KARD IN (" & strARTICULOS & ")"
            Debug.Print strARTICULOS

'            BD(BDCurCon).Execute strARTICULOS, NroRegs
            Result = ExecSQL(strARTICULOS)
            
            MDI_Inventarios.Crys_Listar.Formulas(8) = "RAGFecha='ROTACION DEL:" & FormatDateTime(Me.dtpROTDESDE.Value, vbShortDate) & ", AL:" & FormatDateTime(Me.dtpROTHASTA.Value, vbShortDate) & Comi
            MDI_Inventarios.Crys_Listar.Formulas(10) = "ENTIDAD=" & Comi & Trim(Dueno.Nombre) & ", NIT:" & Trim(Dueno.Nit) & Comi
            MDI_Inventarios.Crys_Listar.Formulas(11) = "acuENTRA=IF " & FrmlENTRA & " THEN " & _
" ({IN_ROTAREPORTE.NU_ENTRAD_KARD}-{IN_ROTAREPORTE.NU_SALIDA_KARD})*{IN_ROTAREPORTE.NU_MULT_KARD}/{IN_ROTAREPORTE.NU_DIVI_KARD}" & _
" ELSE 0"
            MDI_Inventarios.Crys_Listar.Formulas(12) = "acuSALIDA=IF " & FrmlSALE & " THEN " & _
" ({IN_ROTAREPORTE.NU_SALIDA_KARD}-{IN_ROTAREPORTE.NU_ENTRAD_KARD})*{IN_ROTAREPORTE.NU_MULT_KARD}/{IN_ROTAREPORTE.NU_DIVI_KARD}" & _
" ELSE 0"
            Call ListarD("infROTACION.rpt", "", crptToWindow, "ROTACION DE INVENTARIO", MDI_Inventarios, True)
            
            MDI_Inventarios.Crys_Listar.Formulas(8) = ""
            MDI_Inventarios.Crys_Listar.Formulas(10) = ""
            MDI_Inventarios.Crys_Listar.Formulas(11) = ""
            MDI_Inventarios.Crys_Listar.Formulas(12) = ""
        Case Is = 1
            If Me.cmbBODEGA(Me.sstOTREPORTE.Tab).ListIndex < 0 Then Call MsgBox("Falta seleccionar la Bodega", vbExclamation): Exit Sub
            'strFECORTE = "(FE_FECH_KARD <='" & FormatDateTime(Me.dtpINACORTE.Value, vbShortDate) & _
                "' AND NU_APUN_KARD=0 AND NU_AUTO_BODE_KARD=" & Me.cmbBODEGA(Me.sstOTREPORTE.Tab).ItemData(Me.cmbBODEGA(Me.sstOTREPORTE.Tab).ListIndex) & ")"
                
'            strFECORTE = "(FE_FECH_KARD <=" & FFechaCon(Me.dtpINACORTE.Value) & _
'                " AND NU_APUN_KARD=0 AND NU_AUTO_BODE_KARD=" & Me.cmbBODEGA(Me.sstOTREPORTE.Tab).ItemData(Me.cmbBODEGA(Me.sstOTREPORTE.Tab).ListIndex) & ")"    'PedroJ Def 3879
            
            strFECORTE = "AND FE_FECH_KARD <=" & FFechaCon(Me.dtpINACORTE.Value) & _
                " AND NU_AUTO_BODE_KARD=" & Me.cmbBODEGA(Me.sstOTREPORTE.Tab).ItemData(Me.cmbBODEGA(Me.sstOTREPORTE.Tab).ListIndex) 'OMOG T15786
            
            If ExisteTABLA("IN_SALDOREPORTE") Then Result = EliminarTabla("IN_SALDOREPORTE")
            If ExisteTABLA("ANULADOS") Then Result = EliminarTabla("ANULADOS") 'OMOG T15786
            If ExisteTABLA("KARD_MAYOR") Then Result = EliminarTabla("KARD_MAYOR") 'OMOG T15786
            
        'INICIO OMOG T15786
            'strARTICULOS = "SELECT TX_NOMB_BODE, CD_CODI_ARTI, NO_NOMB_ARTI, TX_NOMB_UNVE, NU_ACTUNMR_KARD, NU_ACTUDNM_KARD, VL_COPR_ARTI INTO IN_SALDOREPORTE" & _
            '" From IN_KARDEX, Articulo, IN_BODEGA, IN_UNDVENTA" & _
            '" WHERE NU_AUTO_KARD IN (SELECT MAX(NU_AUTO_KARD)" & _
            '" From IN_KARDEX" & IIf(Len(strFECORTE) > 0, " WHERE ", "") & strFECORTE & _
            '" GROUP BY NU_AUTO_BODE_KARD, NU_AUTO_ARTI_KARD)" & _
            '" AND NU_ACTUNMR_KARD<>0" & _
            '" AND NU_AUTO_ARTI_KARD=NU_AUTO_ARTI" & _
            '" AND NU_AUTO_BODE_KARD=NU_AUTO_BODE" & _
            '" AND NU_AUTO_UNVE_ARTI=NU_AUTO_UNVE" & _
            '" ORDER BY TX_NOMB_BODE, NO_NOMB_ARTI"
            
'            strARTICULOS = "SELECT TX_NOMB_BODE, CD_CODI_ARTI, NO_NOMB_ARTI, TX_NOMB_UNVE, NU_ACTUNMR_KARD, NU_ACTUDNM_KARD, VL_COPR_ARTI INTO IN_SALDOREPORTE" & _
'            " From IN_KARDEX, Articulo, IN_BODEGA, IN_UNDVENTA, IN_ENCABEZADO, IN_DOCUMENTO" & _
'            " WHERE NU_AUTO_KARD IN (SELECT MAX(NU_AUTO_KARD)" & _
'            " From IN_KARDEX" & IIf(Len(strFECORTE) > 0, " WHERE ", "") & strFECORTE & _
'            " AND NU_AUTO_ENCA IN (SELECT NU_AUTO_ENCA FROM IN_ENCABEZADO" & _
'            " WHERE NU_AUTO_DOCU IN (SELECT NU_AUTO_DOCU FROM IN_DOCUMENTO" & _
'            " WHERE NU_AUTO_DOCU<>8 AND NU_AUTO_DOCU<>10 AND NU_AUTO_DOCU<>19))" & _
'            " GROUP BY NU_AUTO_BODE_KARD, NU_AUTO_ARTI_KARD)" & _
'            " AND NU_ACTUNMR_KARD<>0" & _
'            " AND NU_AUTO_ARTI_KARD=NU_AUTO_ARTI" & _
'            " AND NU_AUTO_BODE_KARD=NU_AUTO_BODE" & _
'            " AND NU_AUTO_UNVE_ARTI=NU_AUTO_UNVE" & _
'            " AND NU_AUTO_ENCA=NU_AUTO_ORGCABE_KARD" & _
'            " AND NU_AUTO_DOCU=NU_AUTO_DOCU_ENCA" & _
'            " ORDER BY TX_NOMB_BODE, NO_NOMB_ARTI"

            
            'Se realiza la comparacion del numero de elementos anulados de un articulo contra los que no estan anulados
            'Esta comparaci�n debe ser igual para que me traiga los datos
             strARTICULOS = "SELECT NU_AUTO_ARTI_KARD, SUM(CASE WHEN (NU_AUTO_DOCU=3 AND TX_ESTA_ENCA='N') OR (TX_ESTA_ENCA='A' AND" & _
            " (NU_AUTO_DOCU=8 OR NU_AUTO_DOCU=10 OR NU_AUTO_DOCU=19)) THEN 1 ELSE 0 END) AS CUANTOS1," & _
            " SUM(CASE WHEN (NU_AUTO_DOCU=3 AND TX_ESTA_ENCA='N') OR (NU_AUTO_DOCU=8 OR NU_AUTO_DOCU=10 OR NU_AUTO_DOCU=19) THEN 1 ELSE 0 END) AS CUANTOS2" & _
            " INTO ANULADOS FROM IN_KARDEX" & _
            " INNER JOIN IN_BODEGA ON (NU_AUTO_BODE_KARD=NU_AUTO_BODE " & strFECORTE & ")" & _
            " INNER JOIN IN_ENCABEZADO ON (NU_AUTO_ENCA=NU_AUTO_ORGCABE_KARD)" & _
            " INNER JOIN IN_DOCUMENTO ON (NU_AUTO_DOCU=NU_AUTO_DOCU_ENCA)" & _
            " GROUP BY NU_AUTO_ARTI_KARD" & _
            " ORDER BY NU_AUTO_ARTI_KARD"
            
            Debug.Print strARTICULOS
        
            Result = ExecSQL(strARTICULOS)
            
            'En esta consulta la unimos con la tabla anterior. Se realiza para obtener el registro mayor de KARDEX
            'y as� mostrar la ultima entrada
            strARTICULOS = "SELECT NU_AUTO_ARTI,MAX(NU_AUTO_KARD) AS KARDEXMAY INTO KARD_MAYOR From IN_KARDEX" & _
             " INNER JOIN Articulo ON (NU_AUTO_ARTI_KARD=NU_AUTO_ARTI AND NU_ACTUNMR_KARD<>0)" & _
             " INNER JOIN IN_BODEGA ON (NU_AUTO_BODE_KARD=NU_AUTO_BODE " & strFECORTE & ")" & _
             " INNER JOIN IN_UNDVENTA ON (NU_AUTO_UNVE_ARTI=NU_AUTO_UNVE)" & _
             " INNER JOIN IN_ENCABEZADO ON (NU_AUTO_ENCA=NU_AUTO_ORGCABE_KARD)" & _
             " INNER JOIN IN_DOCUMENTO ON (NU_AUTO_DOCU=NU_AUTO_DOCU_ENCA AND NU_AUTO_DOCU=3 and FE_CREA_ENCA<='2013/06/20')" & _
             " INNER JOIN ANULADOS ON (IN_KARDEX.NU_AUTO_ARTI_KARD=ANULADOS.NU_AUTO_ARTI_KARD)" & _
             " Where ANULADOS.CUANTOS1 = ANULADOS.CUANTOS2" & _
             " GROUP BY NU_AUTO_ARTI" & _
             " ORDER BY NU_AUTO_ARTI"
             
             Debug.Print strARTICULOS
        
            Result = ExecSQL(strARTICULOS)

            'Unimos con la tabla anterior para as� mostrar los productos que necesitamos
             strARTICULOS = "SELECT DISTINCT NU_AUTO_KARD, TX_NOMB_BODE, NO_NOMB_ARTI," & _
             " TX_NOMB_UNVE, NU_ACTUNMR_KARD,NU_ACTUDNM_KARD, VL_COPR_ARTI," & _
             " NU_AUTO_DOCU, NU_ENTRAD_KARD, NU_SALIDA_KARD, TX_NOMB_DOCU," & _
             " TX_ESTA_ENCA,NU_AUTO_ENCA,IN_KARDEX.NU_AUTO_ARTI_KARD INTO IN_SALDOREPORTE From IN_KARDEX" & _
             " INNER JOIN Articulo ON (NU_AUTO_ARTI_KARD=NU_AUTO_ARTI AND NU_ACTUNMR_KARD<>0)" & _
             " INNER JOIN IN_BODEGA ON (NU_AUTO_BODE_KARD=NU_AUTO_BODE " & strFECORTE & ")" & _
             " INNER JOIN IN_UNDVENTA ON (NU_AUTO_UNVE_ARTI=NU_AUTO_UNVE)" & _
             " INNER JOIN IN_ENCABEZADO ON (NU_AUTO_ENCA=NU_AUTO_ORGCABE_KARD)" & _
             " INNER JOIN IN_DOCUMENTO ON (NU_AUTO_DOCU=NU_AUTO_DOCU_ENCA AND NU_AUTO_DOCU=3)" & _
             " INNER JOIN KARD_MAYOR ON (IN_KARDEX.NU_AUTO_KARD=KARD_MAYOR.KARDEXMAY)" & _
             " ORDER BY NO_NOMB_ARTI,IN_KARDEX.NU_AUTO_ARTI_KARD,NU_AUTO_ENCA,NU_AUTO_DOCU,TX_ESTA_ENCA"
        'FIN OMOG T15786
        
            Debug.Print strARTICULOS
        
            Result = ExecSQL(strARTICULOS)
'            BD(BDCurCon).Execute strARTICULOS, NroRegs
            
            MDI_Inventarios.Crys_Listar.Formulas(8) = "RAGFecha='CORTE AL:" & FormatDateTime(Me.dtpINACORTE.Value, vbShortDate) & Comi
            MDI_Inventarios.Crys_Listar.Formulas(10) = "ENTIDAD=" & Comi & Trim(Dueno.Nombre) & ", NIT:" & Trim(Dueno.Nit) & Comi
            MDI_Inventarios.Crys_Listar.Formulas(11) = "SaldoCOSTO={@Saldo}*{IN_SALDOREPORTE.VL_COPR_ARTI}"
            Call ListarD("infINACTIVOS.rpt", "", crptToWindow, "ARTICULOS INACTIVOS", MDI_Inventarios, True)
            MDI_Inventarios.Crys_Listar.Formulas(8) = ""
            MDI_Inventarios.Crys_Listar.Formulas(10) = ""
            MDI_Inventarios.Crys_Listar.Formulas(11) = ""
        Case Is = 2
            If Me.cmbBODEGA(Me.sstOTREPORTE.Tab).ListIndex < 0 Then Call MsgBox("Falta seleccionar la Bodega", vbExclamation): Exit Sub
            If ExisteTABLA("IN_VENTAREPORTE") Then Result = EliminarTabla("IN_VENTAREPORTE")
            'strARTICULOS = "SELECT NU_AUTO_ENCA FROM IN_ENCABEZADO, IN_DOCUMENTO WHERE " & _
                "NU_AUTO_DOCU_ENCA=NU_AUTO_DOCU AND NU_AUTO_DOCU IN " & DocumentoVentas & _
                " AND FE_CREA_ENCA>='" & FormatDateTime(Me.dtpVENDESDE.Value, vbShortDate) & "' AND FE_CREA_ENCA<='" & FormatDateTime(Me.dtpVENHASTA.Value, vbShortDate) & "'" & _
                " AND NU_AUTO_BODEORG_ENCA=" & Me.cmbBODEGA(Me.sstOTREPORTE.Tab).ItemData(Me.cmbBODEGA(Me.sstOTREPORTE.Tab).ListIndex)
                
'            strARTICULOS = "SELECT NU_AUTO_ENCA FROM IN_ENCABEZADO, IN_DOCUMENTO WHERE " & _
'                "NU_AUTO_DOCU_ENCA=NU_AUTO_DOCU AND NU_AUTO_DOCU IN " & DocumentoVentas & _
'                " AND FE_CREA_ENCA>=" & FFechaCon(Me.dtpVENDESDE.Value) & " AND FE_CREA_ENCA<=" & FFechaCon(Me.dtpVENHASTA.Value) & _
'                " AND NU_AUTO_BODEORG_ENCA=" & Me.cmbBODEGA(Me.sstOTREPORTE.Tab).ItemData(Me.cmbBODEGA(Me.sstOTREPORTE.Tab).ListIndex)      'PedroJ Def 3879
'
'            strARTICULOS = "SELECT TX_CODI_COMP, TX_NOMB_COMP, NU_AUTO_ARTI_KARD, NU_ENTRAD_KARD, NU_SALIDA_KARD, NU_MULT_KARD, NU_DIVI_KARD, NU_VALOR_KARD, NU_AUTO_UNVE_KARD, TX_NOMB_UNVE, CD_CODI_ARTI, NO_NOMB_ARTI" & _
'                " INTO IN_VENTAREPORTE FROM IN_KARDEX, IN_UNDVENTA, ARTICULO, IN_ENCABEZADO, IN_COMPROBANTE WHERE" & _
'                " NU_AUTO_UNVE_KARD=NU_AUTO_UNVE " & _
'                "AND NU_AUTO_ORGCABE_KARD=NU_AUTO_ENCA " & _
'                "AND NU_AUTO_COMP_ENCA=NU_AUTO_COMP " & _
'                " AND NU_AUTO_ARTI_KARD=NU_AUTO_ARTI" & _
'                " AND NU_AUTO_ORGCABE_KARD IN (" & strARTICULOS & ")"
            
                strARTICULOS = " SELECT TX_CODI_COMP, TX_NOMB_COMP, NU_AUTO_ARTI_KARD, NU_ENTRAD_KARD, "
                'strARTICULOS = strARTICULOS & " NU_SALIDA_KARD , NU_MULT_KARD, NU_DIVI_KARD, NU_VALOR_KARD, NU_AUTO_UNVE_KARD, TX_NOMB_UNVE, CD_CODI_ARTI, NO_NOMB_ARTI" 'APGR T6938
                strARTICULOS = strARTICULOS & " NU_SALIDA_KARD , NU_MULT_KARD, NU_DIVI_KARD, NU_VALOR_KARD, NU_AUTO_UNVE_KARD, TX_NOMB_UNVE, CD_CODI_ARTI, NO_NOMB_ARTI, NU_ENCDEP_RENEN"  'APGR T6938
                strARTICULOS = strARTICULOS & " INTO IN_VENTAREPORTE FROM"
                strARTICULOS = strARTICULOS & " ("
                strARTICULOS = strARTICULOS & " SELECT NU_AUTO_ENCA,NU_ENCDEP_RENEN, NU_ENCCMP_RENEN, NU_COMP_ENCA"
                'strARTICULOS = strARTICULOS & " ,CASE NU_ENCDEP_RENEN WHEN 0 THEN  NU_VALOR_KARD ELSE (SELECT NU_VALOR_KARD FROM IN_KARDEX WHERE NU_AUTO_ORGCABE_KARD =NU_ENCDEP_RENEN ) END  AS NU_VALOR_KARD,"
                'strARTICULOS = strARTICULOS & " ,CASE NU_ENCDEP_RENEN WHEN 0 THEN  NU_VALOR_KARD ELSE (SELECT DISTINCT NU_VALOR_KARD FROM IN_KARDEX WHERE NU_AUTO_ORGCABE_KARD =NU_ENCDEP_RENEN ) END  AS NU_VALOR_KARD," 'GAVL T5664 - APGR T6938
                strARTICULOS = strARTICULOS & " ,NU_VALOR_KARD, " 'APGR T6938
                strARTICULOS = strARTICULOS & " NU_COSTO_KARD,TX_CODI_COMP, TX_NOMB_COMP,"
                strARTICULOS = strARTICULOS & " NU_AUTO_ARTI_KARD, NU_ENTRAD_KARD, NU_SALIDA_KARD, NU_MULT_KARD, NU_DIVI_KARD,"
                strARTICULOS = strARTICULOS & " NU_AUTO_UNVE_KARD,"
                strARTICULOS = strARTICULOS & " TX_NOMB_UNVE , CD_CODI_ARTI, NO_NOMB_ARTI"
                strARTICULOS = strARTICULOS & " From"
                strARTICULOS = strARTICULOS & " ("
                strARTICULOS = strARTICULOS & " SELECT NU_AUTO_ENCA,ISNULL(NU_ENCDEP_RENEN,0) AS NU_ENCDEP_RENEN, ISNULL(NU_ENCCMP_RENEN,0) AS NU_ENCCMP_RENEN , NU_COMP_ENCA,NU_VALOR_KARD, NU_COSTO_KARD,TX_CODI_COMP, TX_NOMB_COMP,"
                strARTICULOS = strARTICULOS & " NU_AUTO_ARTI_KARD, NU_ENTRAD_KARD, NU_SALIDA_KARD, NU_MULT_KARD, NU_DIVI_KARD,"
                strARTICULOS = strARTICULOS & " NU_AUTO_UNVE_KARD,"
                strARTICULOS = strARTICULOS & " TX_NOMB_UNVE , CD_CODI_ARTI, NO_NOMB_ARTI"
                'strARTICULOS = strARTICULOS & " From IN_KARDEX, IN_UNDVENTA, Articulo, IN_ENCABEZADO, IN_COMPROBANTE, IN_R_ENCA_ENCA" 'OMOG T17848 SE ESTABLECE EN COMENTARIO
                strARTICULOS = strARTICULOS & " From IN_KARDEX, IN_UNDVENTA, Articulo, IN_ENCABEZADO LEFT OUTER JOIN IN_R_ENCA_ENCA ON (NU_AUTO_ENCA=NU_ENCCMP_RENEN), IN_COMPROBANTE" 'OMOG T17848
                strARTICULOS = strARTICULOS & " Where NU_AUTO_UNVE_KARD = NU_AUTO_UNVE And NU_AUTO_ORGCABE_KARD = NU_AUTO_ENCA"
                strARTICULOS = strARTICULOS & " AND NU_AUTO_COMP_ENCA=NU_AUTO_COMP  AND NU_AUTO_ARTI_KARD=NU_AUTO_ARTI"
                strARTICULOS = strARTICULOS & " AND NU_AUTO_ORGCABE_KARD IN (SELECT NU_AUTO_ENCA FROM IN_ENCABEZADO, IN_DOCUMENTO WHERE NU_AUTO_DOCU_ENCA=NU_AUTO_DOCU AND NU_AUTO_DOCU IN " & DocumentoVentas
                strARTICULOS = strARTICULOS & " AND FE_CREA_ENCA>=" & FFechaCon(Me.dtpVENDESDE.Value) & " AND FE_CREA_ENCA<=" & FFechaCon(Me.dtpVENHASTA.Value)
                strARTICULOS = strARTICULOS & " AND NU_AUTO_BODEORG_ENCA=" & Me.cmbBODEGA(Me.sstOTREPORTE.Tab).ItemData(Me.cmbBODEGA(Me.sstOTREPORTE.Tab).ListIndex) & ")"
                'strARTICULOS = strARTICULOS & " AND NU_AUTO_ENCA*=NU_ENCCMP_RENEN" 'SE COMENTAREA ESTA L�NEA OMOG T17848
                'strARTICULOS = strARTICULOS & " AND NU_AUTO_ENCA=NU_ENCCMP_RENEN" 'OMOG T17848 SE ESTABLECE EN COMENTARIO
                strARTICULOS = strARTICULOS & " ) AS T1 ) AS T2"
                                                         
            'DAHV M2861
            ' Se coloca en comentario
            ' strARTICULOS = strARTICULOS & " AND IN_KARDEX.NU_VALOR_KARD <> 0 " 'AASV M5517 Se adiciona para que no traiga movimientos que no tengan costo de venta
            'DAHV M2861
                
            Debug.Print strARTICULOS
        
            Result = ExecSQL(strARTICULOS)
'            BD(BDCurCon).Execute strARTICULOS, NroRegs

           'APGR T6938 - INICIO
            Campos = "NU_VALOR_KARD=  T1.VALOR FROM (SELECT DISTINCT IN_KARDEX.NU_VALOR_KARD AS VALOR FROM IN_KARDEX , IN_VENTAREPORTE  WHERE NU_AUTO_ORGCABE_KARD =NU_ENCDEP_RENEN"
            Campos = Campos & " AND CD_CODI_ARTI_KARD IN  (SELECT CD_CODI_ARTI FROM IN_VENTAREPORTE WHERE NU_ENCDEP_RENEN <>0)) AS T1 "
            Condicion = " NU_ENCDEP_RENEN<> 0"
            Result = DoUpdate("IN_VENTAREPORTE", Campos, Condicion)
           'APGR T6938 - FIN
            
            MDI_Inventarios.Crys_Listar.Formulas(8) = "RAGFecha='VENTAS DEL:" & FormatDateTime(Me.dtpVENDESDE.Value, vbShortDate) & ", AL:" & FormatDateTime(Me.dtpVENHASTA.Value, vbShortDate) & Comi
            MDI_Inventarios.Crys_Listar.Formulas(10) = "ENTIDAD=" & Comi & Trim(Dueno.Nombre) & ", NIT:" & Trim(Dueno.Nit) & Comi
            MDI_Inventarios.Crys_Listar.Formulas(11) = "VALOR={@CANTIDAD}*{IN_VENTAREPORTE.NU_VALOR_KARD}"
            Call ListarD("infVENTAS.rpt", "", crptToWindow, "VENTAS", MDI_Inventarios, True)
            
            MDI_Inventarios.Crys_Listar.Formulas(8) = ""
            MDI_Inventarios.Crys_Listar.Formulas(10) = ""
            MDI_Inventarios.Crys_Listar.Formulas(11) = ""
        Case Is = 3
            'HRR T13995 Inicio
'            strARTICULOS = ""
'            For Each vUno In Me.lstARTICULOS.ListItems
'                If vUno.Selected Then
'                    strARTICULOS = strARTICULOS & IIf(Len(strARTICULOS) = 0, "", ",") & vUno.Tag
'                End If
'            Next
'            If Len(strARTICULOS) > 0 Then
'               strARTICULOS = "[" & strARTICULOS & "]"   'PedroJ Def 1748
'            Else
'               Call Mensaje1("No hay articulos para Listar", 3): Exit Sub
'            End If
'
'            Debug.Print strARTICULOS
'            strARTICULOS = "{IN_LOTE.NU_AUTO_ARTI_LOTE} IN " & strARTICULOS         'PedroJ Def 1748
'            strARTICULOS = strARTICULOS & " AND {IN_LOTE.FE_FECHA_LOTE}>=Date (" & Format(Me.dtpDESLote, "yyyy,mm,dd") & ")"     'PedroJ Def 1748
'            strARTICULOS = strARTICULOS & " AND {IN_LOTE.FE_FECHA_LOTE}<=Date (" & Format(Me.dtpHASLote, "yyyy,mm,dd") & ")"     'PedroJ Def 1748
'                    'strARTICULOS = strARTICULOS & " AND {IN_LOTE.TX_TIPO_LOTE}='V'" 'HRR T13995 PNC se hace validacion en el informe
            Dim stFiltro As String
            stFiltro = ""
            For Each vUno In Me.lstARTICULOS.ListItems
                If vUno.Selected Then
                    stFiltro = stFiltro & IIf(Len(stFiltro) = 0, "", ",") & vUno.Tag
                End If
            Next
            If Len(stFiltro) > 0 Then
               stFiltro = "(" & stFiltro & ")"   'PedroJ Def 1748
            Else
               Call Mensaje1("No hay articulos para Listar", 3): Exit Sub
            End If
            
            stFiltro = "IN_LOTE.NU_AUTO_ARTI_LOTE IN " & stFiltro
            stFiltro = stFiltro & " AND IN_LOTE.FE_FECHA_LOTE BETWEEN " & FFechaCon(CStr(Me.dtpDESLote))
            stFiltro = stFiltro & " AND " & FFechaCon(CStr(Me.dtpHASLote))
            
            Debug.Print stFiltro
                       
            If ExisteTABLA("IN_KARDEX_LOTE") Then Result = EliminarTabla("IN_KARDEX_LOTE")
            strARTICULOS = "SELECT IN_KARDEXLOTE.NU_ENTRAD_KLOT, IN_KARDEXLOTE.NU_SALIDA_KLOT,"
            strARTICULOS = strARTICULOS & " IN_KARDEX.NU_AUTO_KARD, IN_KARDEX.TX_LOTE_KARD,"
            strARTICULOS = strARTICULOS & " IN_LOTE.FE_FECHA_LOTE, IN_LOTE.TX_TIPO_LOTE,"
            strARTICULOS = strARTICULOS & " ARTICULO.CD_CODI_ARTI, ARTICULO.NO_NOMB_ARTI, ARTICULO.NU_AUTO_ARTI,"
            strARTICULOS = strARTICULOS & " IN_BODEGA.TX_NOMB_BODE,"
            strARTICULOS = strARTICULOS & " IN_UNDVENTA.TX_NOMB_UNVE"
            strARTICULOS = strARTICULOS & " Into IN_KARDEX_LOTE"
            strARTICULOS = strARTICULOS & " From"
            strARTICULOS = strARTICULOS & " (SELECT DISTINCT NU_AUTO_ARTI_LOTE, FE_FECHA_LOTE, MIN(NU_AUTO_LOTE) AS NU_AUTO_LOTE FROM IN_LOTE"
            strARTICULOS = strARTICULOS & " WHERE " & stFiltro & " GROUP BY NU_AUTO_ARTI_LOTE, FE_FECHA_LOTE) AS T1"
            strARTICULOS = strARTICULOS & " INNER JOIN IN_LOTE"
            strARTICULOS = strARTICULOS & " ON IN_LOTE.NU_AUTO_LOTE = T1.NU_AUTO_LOTE"
            strARTICULOS = strARTICULOS & " INNER JOIN IN_KARDEXLOTE"
            strARTICULOS = strARTICULOS & " ON IN_KARDEXLOTE.NU_AUTO_LOTE_KLOT = IN_LOTE.NU_AUTO_LOTE"
            strARTICULOS = strARTICULOS & " INNER JOIN IN_KARDEX"
            strARTICULOS = strARTICULOS & " ON IN_KARDEXLOTE.NU_AUTO_KARD_KLOT = IN_KARDEX.NU_AUTO_KARD"
            strARTICULOS = strARTICULOS & " AND IN_KARDEX.NU_AUTO_ARTI_KARD = IN_LOTE.NU_AUTO_ARTI_LOTE"
            strARTICULOS = strARTICULOS & " INNER JOIN IN_BODEGA"
            strARTICULOS = strARTICULOS & " ON IN_KARDEX.NU_AUTO_BODE_KARD = IN_BODEGA.NU_AUTO_BODE"
            strARTICULOS = strARTICULOS & " INNER JOIN ARTICULO"
            strARTICULOS = strARTICULOS & " ON IN_LOTE.NU_AUTO_ARTI_LOTE = ARTICULO.NU_AUTO_ARTI"
            strARTICULOS = strARTICULOS & " INNER JOIN IN_UNDVENTA"
            strARTICULOS = strARTICULOS & " ON ARTICULO.NU_AUTO_UNVE_ARTI = IN_UNDVENTA.NU_AUTO_UNVE"
            strARTICULOS = strARTICULOS & " Where"
            strARTICULOS = strARTICULOS & " IN_KARDEXLOTE.NU_ENTRAD_KLOT > 0 OR"
            strARTICULOS = strARTICULOS & " IN_KARDEXLOTE.NU_SALIDA_KLOT > 0"
            strARTICULOS = strARTICULOS & " Order By"
            strARTICULOS = strARTICULOS & " IN_KARDEX.TX_LOTE_KARD ASC,"
            strARTICULOS = strARTICULOS & " IN_KARDEX.NU_AUTO_KARD ASC,"
            strARTICULOS = strARTICULOS & " Articulo.NU_AUTO_ARTI Asc"
            
            Debug.Print strARTICULOS
        
            Result = ExecSQL(strARTICULOS)
            'HRR T13995 Inicio
            
            MDI_Inventarios.Crys_Listar.Formulas(8) = "RAGFecha='LOTES DEL: " & FormatDateTime(Me.dtpDESLote.Value, vbShortDate) & ", AL: " & FormatDateTime(Me.dtpHASLote.Value, vbShortDate) & Comi
            MDI_Inventarios.Crys_Listar.Formulas(10) = "ENTIDAD=" & Comi & Trim(Dueno.Nombre) & ", NIT:" & Trim(Dueno.Nit) & Comi
            'Call ListarD("infLOTES.rpt", "", crptToWindow, "VENTAS", MDI_Inventarios, True)
            'Call ListarD("infLOTES.rpt", strARTICULOS, crptToWindow, "LOTES", MDI_Inventarios, True)    'PedroJ Def 1748 'HRR T13995
            Call ListarD("infLOTES.rpt", NUL$, crptToWindow, "LOTES", MDI_Inventarios, True)    'PedroJ Def 1748 'HRR T13995
            
            MDI_Inventarios.Crys_Listar.Formulas(8) = ""
            MDI_Inventarios.Crys_Listar.Formulas(10) = ""
            MDI_Inventarios.Crys_Listar.Formulas(11) = ""
        Case Is = 4
            If Me.cmbAUDITORIA.ListIndex < 0 Then Call MsgBox("Falta seleccionar el tipo de Auditoria", vbExclamation): Exit Sub
            If ExisteTABLA("IN_TOPESDESPACHO") Then Result = EliminarTabla("IN_TOPESDESPACHO")
            Desde = "IN_KARDEX"
            'Condi = "FE_FECH_KARD<='" & FormatDateTime(Me.dptHASlotes.Value, vbShortDate) & "' GROUP BY NU_AUTO_BODE_KARD, NU_AUTO_ARTI_KARD ORDER BY NU_AUTO_ARTI_KARD"
            Condi = "FE_FECH_KARD<=" & FFechaCon(Me.dptHASlotes.Value) & " GROUP BY NU_AUTO_BODE_KARD, NU_AUTO_ARTI_KARD ORDER BY NU_AUTO_ARTI_KARD"        'PedroJ Def 3879
            Campos = "MAX(NU_AUTO_KARD)"
            ReDim ArrTPS(0)
            Result = LoadMulData(Desde, Campos, Condi, ArrTPS())
            If Result = FAIL Then Exit Sub
            If Not Encontro Then Exit Sub
            strARTICULOS = ""
            For vContar = 0 To UBound(ArrTPS, 2)
                strARTICULOS = strARTICULOS & IIf(Len(strARTICULOS) = 0, "", ",") & ArrTPS(0, vContar)
            Next
            'Campos = "CD_CODI_ARTI, NO_NOMB_ARTI, TX_CODI_BODE, TX_NOMB_BODE, " & _
            "NU_STMIN_LIBA, NU_STMAX_LIBA, NU_REPOS_LIBA, NU_MXDES_LIBA, " & _
                "NU_SALINMR_KARD, ISNULL(NU_SALIDNM_KARD,1) AS NU_SALIDNM_KARD, NU_ENTRNMR_KARD, " & _
                "ISNULL(NU_ENTRDNM_KARD,1) AS NU_ENTRDNM_KARD, NU_ACTUNMR_KARD, ISNULL(NU_ACTUDNM_KARD,1) AS NU_ACTUDNM_KARD"
           'PedroJ Def 3879
            If MotorBD <> "ACCESS" Then
                Campos = "CD_CODI_ARTI, NO_NOMB_ARTI, TX_CODI_BODE, TX_NOMB_BODE, " & _
                    "NU_STMIN_LIBA, NU_STMAX_LIBA, NU_REPOS_LIBA, NU_MXDES_LIBA, " & _
                    "NU_SALINMR_KARD, ISNULL(NU_SALIDNM_KARD,1) AS NU_SALIDNM_KARD, NU_ENTRNMR_KARD, " & _
                    "ISNULL(NU_ENTRDNM_KARD,1) AS NU_ENTRDNM_KARD, NU_ACTUNMR_KARD, ISNULL(NU_ACTUDNM_KARD,1) AS NU_ACTUDNM_KARD"
            Else
                Campos = "CD_CODI_ARTI, NO_NOMB_ARTI, TX_CODI_BODE, TX_NOMB_BODE, " & _
                    "NU_STMIN_LIBA, NU_STMAX_LIBA, NU_REPOS_LIBA, NU_MXDES_LIBA, " & _
                    "NU_SALINMR_KARD, NU_SALIDNM_KARD, NU_ENTRNMR_KARD, " & _
                    "NU_ENTRDNM_KARD, NU_ACTUNMR_KARD, NU_ACTUDNM_KARD"
            End If
           'PedroJ Def 3879
           
            Desde = "IN_LIMITES_BODE_ARTI, ARTICULO, IN_KARDEX, IN_BODEGA"
            Condi = "NU_AUTO_ARTI_LIBA=NU_AUTO_ARTI"
            Condi = Condi & " AND NU_AUTO_BODE_LIBA=NU_AUTO_BODE"
            Condi = Condi & " AND NU_AUTO_ARTI_KARD=NU_AUTO_ARTI_LIBA"
            Condi = Condi & " AND NU_AUTO_BODE_KARD=NU_AUTO_BODE_LIBA"
            Condi = Condi & " AND NU_AUTO_KARD IN (" & strARTICULOS & ")"
            If MotorBD = "ACCESS" Then Condi = Condi & " AND ISNULL(NU_SALIDNM_KARD) AND ISNULL(NU_ENTRDNM_KARD) AND ISNULL(NU_ACTUDNM_KARD)"
            
            Debug.Print "SELECT " & Campos & " FROM " & Desde & " WHERE " & Condi
            strARTICULOS = "SELECT " & Campos & " INTO IN_TOPESDESPACHO FROM " & Desde & " WHERE " & Condi
            Select Case Me.cmbAUDITORIA.ListIndex
            Case Is = 0
                strTITULO = "AUDITORIA DE DESPACHOS"
                strREGISTROS = "{IN_TOPESDESPACHO.NU_MXDES_LIBA}<{IN_TOPESDESPACHO.NU_ENTRNMR_KARD}/{IN_TOPESDESPACHO.NU_ENTRDNM_KARD}" ' NU_SALINMR_KARD, NU_SALIDNM_KARD, NU_ENTRNMR_KARD, NU_ENTRDNM_KARD, NU_ACTUNMR_KARD, NU_ACTUDNM_KARD"
                MDI_Inventarios.Crys_Listar.Formulas(11) = "DESPA={IN_TOPESDESPACHO.NU_ENTRNMR_KARD}/{IN_TOPESDESPACHO.NU_ENTRDNM_KARD}"
                MDI_Inventarios.Crys_Listar.Formulas(12) = "CANTIDAD={IN_TOPESDESPACHO.NU_MXDES_LIBA}"
                MDI_Inventarios.Crys_Listar.Formulas(13) = "DIFERE={@CANTIDAD}-{@DESPA}"
            Case Is = 1
                strTITULO = "AUDITORIA DE MINIMOS"
                strREGISTROS = "{IN_TOPESDESPACHO.NU_STMIN_LIBA}>{IN_TOPESDESPACHO.NU_ACTUNMR_KARD}/{IN_TOPESDESPACHO.NU_ACTUDNM_KARD}" ' NU_SALINMR_KARD, NU_SALIDNM_KARD, NU_ENTRNMR_KARD, NU_ENTRDNM_KARD, NU_ACTUNMR_KARD, NU_ACTUDNM_KARD"
                MDI_Inventarios.Crys_Listar.Formulas(11) = "DESPA={IN_TOPESDESPACHO.NU_ACTUNMR_KARD}/{IN_TOPESDESPACHO.NU_ACTUDNM_KARD}"
                MDI_Inventarios.Crys_Listar.Formulas(12) = "CANTIDAD={IN_TOPESDESPACHO.NU_STMIN_LIBA}"
                MDI_Inventarios.Crys_Listar.Formulas(13) = "DIFERE={@CANTIDAD}-{@DESPA}"
            Case Is = 2
                strTITULO = "AUDITORIA DE MAXIMOS"
                strREGISTROS = "{IN_TOPESDESPACHO.NU_STMAX_LIBA}<{IN_TOPESDESPACHO.NU_ACTUNMR_KARD}/{IN_TOPESDESPACHO.NU_ACTUDNM_KARD}" ' NU_SALINMR_KARD, NU_SALIDNM_KARD, NU_ENTRNMR_KARD, NU_ENTRDNM_KARD, NU_ACTUNMR_KARD, NU_ACTUDNM_KARD"
                MDI_Inventarios.Crys_Listar.Formulas(11) = "DESPA={IN_TOPESDESPACHO.NU_ACTUNMR_KARD}/{IN_TOPESDESPACHO.NU_ACTUDNM_KARD}"
                MDI_Inventarios.Crys_Listar.Formulas(12) = "CANTIDAD={IN_TOPESDESPACHO.NU_STMAX_LIBA}"
                MDI_Inventarios.Crys_Listar.Formulas(13) = "DIFERE={@DESPA}-{@CANTIDAD}"
            Case Is = 3
                strTITULO = "AUDITORIA DE REPOSICION"
                strREGISTROS = "{IN_TOPESDESPACHO.NU_REPOS_LIBA}>{IN_TOPESDESPACHO.NU_ACTUNMR_KARD}/{IN_TOPESDESPACHO.NU_ACTUDNM_KARD}" ' NU_SALINMR_KARD, NU_SALIDNM_KARD, NU_ENTRNMR_KARD, NU_ENTRDNM_KARD, NU_ACTUNMR_KARD, NU_ACTUDNM_KARD"
                MDI_Inventarios.Crys_Listar.Formulas(11) = "DESPA={IN_TOPESDESPACHO.NU_ACTUNMR_KARD}/{IN_TOPESDESPACHO.NU_ACTUDNM_KARD}"
                MDI_Inventarios.Crys_Listar.Formulas(12) = "CANTIDAD={IN_TOPESDESPACHO.NU_REPOS_LIBA}"
                MDI_Inventarios.Crys_Listar.Formulas(13) = "DIFERE={@CANTIDAD}-{@DESPA}"
            End Select
            Result = ExecSQL(strARTICULOS)
'            BD(BDCurCon).Execute strARTICULOS, NroRegs
            MDI_Inventarios.Crys_Listar.Formulas(8) = "RAGFecha='" & strTITULO & " CON CORTE AL:" & FormatDateTime(Me.dptHASlotes.Value, vbShortDate) & Comi
            MDI_Inventarios.Crys_Listar.Formulas(10) = "ENTIDAD=" & Comi & Trim(Dueno.Nombre) & ", NIT:" & Trim(Dueno.Nit) & Comi
            Call ListarD("infTOPES.rpt", strREGISTROS, crptToWindow, strTITULO, MDI_Inventarios, True)
            
            MDI_Inventarios.Crys_Listar.Formulas(8) = ""
            MDI_Inventarios.Crys_Listar.Formulas(10) = ""
            MDI_Inventarios.Crys_Listar.Formulas(11) = ""
            MDI_Inventarios.Crys_Listar.Formulas(12) = ""
            MDI_Inventarios.Crys_Listar.Formulas(13) = ""
        Case Is = 5
            If Me.cmbENTRADAS.ListIndex < 0 Then Call MsgBox("Falta seleccionar el Consecutivo", vbExclamation): Exit Sub
            If Not Len(Me.txtNROENTRA.Text) > 0 Then Call MsgBox("Falta el N�mero", vbExclamation): Exit Sub
            If Not CLng(Me.txtNROENTRA.Text) > 0 Then Call MsgBox("Falta el N�mero", vbExclamation): Exit Sub
            strREGISTROS = "{Query.NU_AUTO_COMP_ENCA}=" & Me.cmbENTRADAS.ItemData(Me.cmbENTRADAS.ListIndex) & " AND {Query.NU_COMP_ENCA}=" & Me.txtNROENTRA.Text
            'Call ListarD("961.rpt", strREGISTROS, crptToWindow, "Impresi�n de ACTAS", MDI_Inventarios, True)
            
            Dim SQL As String
            If ExisteTABLA("IN_ACTA_TEMP") Then Result = EliminarTabla("IN_ACTA_TEMP")
            
            SQL = "SELECT NU_AUTO_KARD AS AUTONUME, NU_AUTO_ORGCABE_KARD, NU_AUTO_MODCABE_KARD, TX_CODI_BODE, TX_NOMB_BODE,"
            SQL = SQL & "COOR.TX_PRFJ_COMP, COOR.TX_NOMB_COMP, ORG.NU_COMP_ENCA, ORG.FE_CREA_ENCA, TX_LOTE_KARD, "
            SQL = SQL & "NU_AUTO_ARTI_KARD, TX_COBA_COBA, TX_REGI_COBA, TX_LABO_COBA, CD_GRUP_ARTI, DE_DESC_GRUP, "
            SQL = SQL & "CD_CODI_ARTI, NO_NOMB_ARTI, TX_CIAL_COBA, FE_FECH_KARD, COOR.NU_COMP_COMP,NU_ENTRAD_KARD,"
            SQL = SQL & "NU_SALIDA_KARD, NU_COSTO_KARD, NU_MULT_KARD, NU_DIVI_KARD, NU_ACTUNMR_KARD, NU_ACTUDNM_KARD, "
            SQL = SQL & "TX_PARA_ARTI, DOOR.TX_AFEC_DOCU, CD_CODI_TERC, NO_NOMB_TERC, TX_VENCE_ARTI,"
            SQL = SQL & "TX_ENTRA_ARTI, 0 AS FECHAVENCE, ORG.NU_AUTO_COMP_ENCA"
            SQL = SQL & " INTO IN_ACTA_TEMP "
            SQL = SQL & "FROM ((((((((IN_KARDEX LEFT JOIN IN_CODIGOBAR ON NU_AUTO_ARTI_KARD=NU_AUTO_ARTI_COBA)"
            SQL = SQL & "INNER JOIN ARTICULO ON NU_AUTO_ARTI_KARD=NU_AUTO_ARTI)"
            SQL = SQL & "INNER JOIN GRUP_ARTICULO ON CD_GRUP_ARTI=CD_CODI_GRUP)"
            SQL = SQL & "INNER JOIN IN_BODEGA ON NU_AUTO_BODE_KARD=NU_AUTO_BODE)"
            SQL = SQL & "INNER JOIN IN_ENCABEZADO AS ORG ON NU_AUTO_ORGCABE_KARD=ORG.NU_AUTO_ENCA)"
            SQL = SQL & "INNER JOIN IN_COMPROBANTE AS COOR ON ORG.NU_AUTO_COMP_ENCA=COOR.NU_AUTO_COMP)"
            SQL = SQL & "INNER JOIN IN_DOCUMENTO AS DOOR ON ORG.NU_AUTO_DOCU_ENCA=DOOR.NU_AUTO_DOCU)"
            SQL = SQL & "LEFT JOIN TERCERO ON CD_CODI_TERC_ENCA=CD_CODI_TERC) "
            SQL = SQL & "WHERE (DOOR.TX_CODI_DOCU='ENTRA') AND (TX_VENCE_ARTI='N') AND (TX_ENTRA_ARTI='N') "
            SQL = SQL & "AND ORG.NU_AUTO_COMP_ENCA=" & Me.cmbENTRADAS.ItemData(Me.cmbENTRADAS.ListIndex) & " AND ORG.NU_COMP_ENCA=" & Me.txtNROENTRA.Text
            SQL = SQL & " UNION SELECT NU_AUTO_KARD AS AUTONUME, NU_AUTO_ORGCABE_KARD, NU_AUTO_MODCABE_KARD, TX_CODI_BODE, TX_NOMB_BODE,"
            SQL = SQL & "COOR.TX_PRFJ_COMP, COOR.TX_NOMB_COMP, ORG.NU_COMP_ENCA, ORG.FE_CREA_ENCA, TX_LOTE_KARD,"
            SQL = SQL & "NU_AUTO_ARTI_KARD, TX_COBA_COBA, TX_REGI_COBA, TX_LABO_COBA, CD_GRUP_ARTI, DE_DESC_GRUP,"
            SQL = SQL & "CD_CODI_ARTI, NO_NOMB_ARTI, TX_CIAL_COBA, FE_FECH_KARD, COOR.NU_COMP_COMP, NU_ENTRAD_KARD,"
            SQL = SQL & "NU_SALIDA_KARD, NU_COSTO_KARD, NU_MULT_KARD, NU_DIVI_KARD, NU_ACTUNMR_KARD, NU_ACTUDNM_KARD,"
            SQL = SQL & "TX_PARA_ARTI, DOOR.TX_AFEC_DOCU, CD_CODI_TERC, NO_NOMB_TERC, TX_VENCE_ARTI, TX_ENTRA_ARTI,"
            SQL = SQL & "FE_FECHA_LOTE, ORG.NU_AUTO_COMP_ENCA "
            SQL = SQL & "FROM ((((((((((IN_KARDEX LEFT JOIN IN_CODIGOBAR ON NU_AUTO_ARTI_KARD=NU_AUTO_ARTI_COBA)"
            SQL = SQL & "INNER JOIN ARTICULO ON NU_AUTO_ARTI_KARD=NU_AUTO_ARTI)"
            SQL = SQL & "INNER JOIN GRUP_ARTICULO ON CD_GRUP_ARTI=CD_CODI_GRUP)"
            SQL = SQL & "INNER JOIN IN_BODEGA ON NU_AUTO_BODE_KARD=NU_AUTO_BODE)"
            SQL = SQL & "INNER JOIN IN_ENCABEZADO AS ORG ON NU_AUTO_ORGCABE_KARD=ORG.NU_AUTO_ENCA)"
            SQL = SQL & "INNER JOIN IN_COMPROBANTE AS COOR ON ORG.NU_AUTO_COMP_ENCA=COOR.NU_AUTO_COMP)"
            SQL = SQL & "INNER JOIN IN_DOCUMENTO AS DOOR ON ORG.NU_AUTO_DOCU_ENCA=DOOR.NU_AUTO_DOCU)"
            SQL = SQL & "LEFT JOIN TERCERO ON CD_CODI_TERC_ENCA=CD_CODI_TERC)"
            SQL = SQL & "LEFT JOIN IN_KARDEXLOTE ON NU_AUTO_KARD=NU_AUTO_KARD_KLOT)"
            SQL = SQL & "LEFT JOIN IN_LOTE ON NU_AUTO_LOTE=NU_AUTO_LOTE_KLOT) "
            SQL = SQL & "WHERE (TX_TIPO_LOTE='V') AND (DOOR.TX_CODI_DOCU='ENTRA') AND (TX_VENCE_ARTI='S' OR TX_ENTRA_ARTI='S') "
            SQL = SQL & "AND ORG.NU_AUTO_COMP_ENCA=" & Me.cmbENTRADAS.ItemData(Me.cmbENTRADAS.ListIndex) & " AND ORG.NU_COMP_ENCA=" & Me.txtNROENTRA.Text
            SQL = SQL & " ORDER BY COOR.TX_PRFJ_COMP, COOR.TX_NOMB_COMP, ORG.NU_COMP_ENCA"
            Debug.Print SQL
            Result = ExecSQL(SQL)
            Call ListarD("InfActa.rpt", "", crptToWindow, "Impresi�n de ACTAS", MDI_Inventarios, True)
            
        End Select
    Case Is = 2: Unload Me
    End Select
End Sub
Private Sub dtpHASLote_LostFocus() 'smdl m1876
    If IsDate(Me.dtpHASLote) Then
       If CDate(Me.dtpDESLote) > CDate(Me.dtpHASLote) Then
         Call Mensaje1("La fecha final debe ser mayor a la inicial.", 3)
         Me.dtpHASLote.SetFocus
       End If
    End If
End Sub
Private Sub dtpROTHASTA_LostFocus()   'smdl m1876
    If IsDate(Me.dtpROTHASTA) Then
       If CDate(Me.dtpROTDESDE) > CDate(Me.dtpROTHASTA) Then
         Call Mensaje1("La fecha final debe ser mayor a la inicial.", 3)
         Me.dtpROTHASTA.SetFocus
       End If
    End If
End Sub
Private Sub dtpVENHASTA_LostFocus() 'smdl m1876
    If IsDate(Me.dtpVENHASTA) Then
       If CDate(Me.dtpVENDESDE) > CDate(Me.dtpVENHASTA) Then
         Call Mensaje1("La fecha final debe ser mayor a la inicial.", 3)
         Me.dtpVENHASTA.SetFocus
       End If
    End If
End Sub

Private Sub Form_Load()
    Dim FIL As Integer, Col As Integer, vclave As String
    Call CenterForm(MDI_Inventarios, Me)
    Dueno.IniXNit
    Me.sstOTREPORTE.Tab = 0
    Me.cmbAUDITORIA.ListIndex = 0
    
    Desde = "IN_BODEGA ORDER BY TX_NOMB_BODE"
    Campos = "NU_AUTO_BODE, TX_NOMB_BODE, NU_AUTO_BODE_BODE"
    Condi = ""
    ReDim ArrUXB(2, 0)
    Result = LoadMulData(Desde, Campos, Condi, ArrUXB())
    If Result = FAIL Then Exit Sub
    If Not Encontro Then Exit Sub
    For FIL = 0 To UBound(ArrUXB, 2)
        For Col = 0 To 2
            Me.cmbBODEGA(Col).AddItem ArrUXB(1, FIL)
            Me.cmbBODEGA(Col).ItemData((Me.cmbBODEGA(Col).NewIndex)) = CLng(ArrUXB(0, FIL))
            If Len(ArrUXB(2, FIL)) = 0 Then Me.cmbBODEGA(Col).ListIndex = FIL
        Next
    Next
    
    Desde = "ARTICULO"
    Campos = "CD_CODI_ARTI,NU_AUTO_ARTI,NO_NOMB_ARTI"
    Condi = "TX_VENCE_ARTI='S' OR TX_ENTRA_ARTI='S' ORDER BY NO_NOMB_ARTI"
    ReDim ArrUXB(2, 0)
    Result = LoadMulData(Desde, Campos, Condi, ArrUXB())
    If Result = FAIL Then Exit Sub
    If Not Encontro Then Exit Sub
    For FIL = 0 To UBound(ArrUXB, 2)
        vclave = "A" & ArrUXB(1, FIL)
        Me.lstARTICULOS.ListItems.Add , vclave, ArrUXB(0, FIL)
        Me.lstARTICULOS.ListItems.Item(vclave).Tag = ArrUXB(1, FIL)
        Me.lstARTICULOS.ListItems.Item(vclave).ListSubItems.Add , "NOM", ArrUXB(2, FIL)
    Next
    
    Desde = "IN_COMPROBANTE, IN_DOCUMENTO"
    Campos = "TX_CODI_COMP, NU_AUTO_COMP, TX_NOMB_COMP"
    Condi = "(NU_AUTO_DOCU_COMP=NU_AUTO_DOCU) AND (TX_CODI_DOCU='ENTRA') ORDER BY TX_NOMB_COMP"
    ReDim ArrUXB(2, 0)
    Me.cmbENTRADAS.Clear
    Result = LoadMulData(Desde, Campos, Condi, ArrUXB())
    If Result = FAIL Then Exit Sub
    If Not Encontro Then Exit Sub
    For FIL = 0 To UBound(ArrUXB, 2)
        Me.cmbENTRADAS.AddItem ArrUXB(2, FIL)
        Me.cmbENTRADAS.ItemData((Me.cmbENTRADAS.NewIndex)) = CLng(ArrUXB(1, FIL))
    Next
    
    Me.dtpROTHASTA.Value = DateAdd("d", -Day(Nowserver), Nowserver)
    Me.dtpROTDESDE.Value = DateAdd("m", -1, DateAdd("d", 1, Me.dtpROTHASTA.Value))
    Me.dtpINACORTE.Value = DateAdd("m", -3, Me.dtpROTHASTA.Value)
    Me.dtpVENDESDE.Value = Nowserver
    Me.dtpVENHASTA.Value = Nowserver
    Me.dtpDESLote.Value = Me.dtpROTDESDE.Value
    Me.dtpHASLote.Value = Me.dtpROTHASTA.Value
    Me.dptHASlotes.Value = Me.dtpROTHASTA.Value
End Sub

