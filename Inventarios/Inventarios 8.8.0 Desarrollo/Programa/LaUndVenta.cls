VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "LaUndVenta"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit

Private AutUnd As Long 'autonumerico de la unidad de venta
Private bdg As Long 'Autonum�rico de la bodega
Private cdi As String * 10 'C�digo de la unidad de venta
Private bre As String * 50 'Nombre de la unidad de venta
Private num As Long 'Autonum�rico del art�culo
Private art As String * 16 'C�digo del art�culo
Private nmb As String * 50 'Nombre del art�culo
Private mlt As Long 'Multiplicador de la unidad de venta
Private dvs As Long 'Divisor de la unidad de venta

Public Property Get AutoNumerico() As Long
    AutoNumerico = AutUnd
End Property

Public Property Let AutoNumerico(aut As Long)
    AutUnd = aut
End Property

Public Property Let Bodega(cnt As Long)
    bdg = cnt
End Property

Public Property Get Bodega() As Long
    Bodega = bdg
End Property

Public Property Let Multiplicador(cnt As Long)
    If cnt > 1 Then mlt = cnt
End Property

Public Property Get Multiplicador() As Long
    Multiplicador = mlt
End Property

Public Property Let CodigoDeUnidad(Cue As String)
    cdi = Cue
End Property

Public Property Get CodigoDeUnidad() As String
    CodigoDeUnidad = cdi
End Property

Public Property Let NumeroArticulo(cnt As Long)
    num = cnt
End Property

Public Property Get NumeroArticulo() As Long
    NumeroArticulo = num
End Property

Public Property Let CodigoArticulo(Cue As String)
    art = Cue
End Property

Public Property Get CodigoArticulo() As String
    CodigoArticulo = art
End Property

Public Property Let NombreDeUnidad(Cue As String)
    bre = Cue
End Property

Public Property Get NombreDeUnidad() As String
    NombreDeUnidad = bre
End Property

Public Property Get NombreArticulo() As String
    NombreArticulo = nmb
End Property

Public Property Let NombreArticulo(Cue As String)
    nmb = Cue
End Property

Public Property Let Divisor(cnt As Long)
    If cnt > 1 Then dvs = cnt
End Property

Public Property Get Divisor() As Long
    Divisor = dvs
End Property

Private Sub Class_Initialize()
    mlt = 1
    dvs = 1
End Sub

Public Sub INIXNumero()
    Dim Cmp As String, Dsd As String, Cdc As String
'    Dim ArrCLS() As Variant, CnTdr As Integer
    Dim ArrCLS() As Variant     'DEPURACION DE CODIGO
    Cmp = "NU_AUTO_ARTI, CD_CODI_ARTI, NO_NOMB_ARTI, NU_AUTO_UNVE, TX_CODI_UNVE, " & _
        "TX_NOMB_UNVE, NU_MULT_UNVE, NU_DIVI_UNVE"
    Dsd = "ARTICULO, IN_UNDVENTA"
    Cdc = "ARTICULO.NU_AUTO_UNVE_ARTI=IN_UNDVENTA.NU_AUTO_UNVE AND " & _
        "ARTICULO.NU_AUTO_ARTI=" & num
    ReDim ArrCLS(7)
    Result = LoadData(Dsd, Cmp, Cdc, ArrCLS())
    If Result = FAIL Then GoTo FALLO
    If Not Encontro Then GoTo NOENC
    AutUnd = ArrCLS(3)
    cdi = ArrCLS(4) 'TX_CODI_UNVE
    art = ArrCLS(1) 'CD_CODI_ARTI
    nmb = ArrCLS(2) 'NO_NOMB_ARTI
    bre = ArrCLS(5) 'TX_NOMB_UNVE
    mlt = ArrCLS(6) 'NU_MULT_UNVE
    dvs = ArrCLS(7) 'NU_DIVI_UNVE
FALLO:
NOENC:
End Sub

'    Dim ArrTemp() As Variant, CnTdr As Integer
'    Campos = ""
'    Desde = ""
'    Condi = ""
'    ReDim ArrTemp(1, 0)
'    Result = LoadMulData(Desde, Campos, Condi, ArrTemp())
'    If Result <> FAIL Then
'        If Encontro Then
'            For CnTdr = 0 To UBound(ArrTemp, 2)
'                cbx.ItemData(cbx.NewIndex) = ArrTemp(0, CnTdr) 'RST("NU_AUTO_BODE")
'            Next
'        End If
'    End If

'    Dim ArrTemp() As Variant
'    Campos = ""
'    Desde = ""
'    Condi = ""
'    ReDim ArrTemp(0)
'    Result = LoadData(Desde, Campos, Condi, ArrTemp())
'    If Result <> FAIL Then
'        If Encontro Then
'           cdi = ArrTemp(4) 'TX_CODI_UNVE
'        End If
'    End If

