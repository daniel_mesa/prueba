VERSION 5.00
Begin VB.Form frmAyuda 
   BackColor       =   &H80000010&
   BorderStyle     =   0  'None
   ClientHeight    =   2295
   ClientLeft      =   0
   ClientTop       =   0
   ClientWidth     =   5355
   Icon            =   "frmAyuda.frx":0000
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   ScaleHeight     =   2295
   ScaleWidth      =   5355
   ShowInTaskbar   =   0   'False
   StartUpPosition =   3  'Windows Default
   Begin VB.PictureBox picAyuda 
      Appearance      =   0  'Flat
      BackColor       =   &H80000005&
      BorderStyle     =   0  'None
      ForeColor       =   &H80000008&
      Height          =   735
      Left            =   50
      ScaleHeight     =   735
      ScaleWidth      =   5055
      TabIndex        =   0
      Top             =   50
      Width           =   5055
      Begin VB.Timer tmrDisable 
         Enabled         =   0   'False
         Interval        =   100
         Left            =   1440
         Top             =   120
      End
      Begin VB.Label lblMsg 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Contenido"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Index           =   1
         Left            =   120
         TabIndex        =   2
         Top             =   120
         Width           =   870
      End
      Begin VB.Image imgIcon 
         Height          =   495
         Left            =   4320
         Top             =   120
         Width           =   615
      End
      Begin VB.Label lblMsg 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "T�tulo"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Index           =   0
         Left            =   120
         TabIndex        =   1
         Top             =   120
         Visible         =   0   'False
         Width           =   540
      End
   End
   Begin VB.Image imgLib 
      Height          =   480
      Index           =   10
      Left            =   1440
      Picture         =   "frmAyuda.frx":000C
      Top             =   1680
      Visible         =   0   'False
      Width           =   480
   End
   Begin VB.Image imgLib 
      Height          =   480
      Index           =   9
      Left            =   840
      Picture         =   "frmAyuda.frx":08D6
      Top             =   1680
      Visible         =   0   'False
      Width           =   480
   End
   Begin VB.Image imgLib 
      Height          =   480
      Index           =   8
      Left            =   120
      Picture         =   "frmAyuda.frx":11A0
      Top             =   1680
      Visible         =   0   'False
      Width           =   480
   End
   Begin VB.Image imgLib 
      Height          =   480
      Index           =   7
      Left            =   4440
      Picture         =   "frmAyuda.frx":1A6A
      Top             =   1200
      Visible         =   0   'False
      Width           =   480
   End
   Begin VB.Image imgLib 
      Height          =   480
      Index           =   6
      Left            =   3840
      Picture         =   "frmAyuda.frx":2334
      Top             =   1200
      Visible         =   0   'False
      Width           =   480
   End
   Begin VB.Image imgLib 
      Height          =   480
      Index           =   5
      Left            =   3240
      Picture         =   "frmAyuda.frx":2BFE
      Top             =   1200
      Visible         =   0   'False
      Width           =   480
   End
   Begin VB.Image imgLib 
      Height          =   480
      Index           =   4
      Left            =   2640
      Picture         =   "frmAyuda.frx":34C8
      Top             =   1200
      Visible         =   0   'False
      Width           =   480
   End
   Begin VB.Image imgLib 
      Height          =   480
      Index           =   3
      Left            =   2040
      Picture         =   "frmAyuda.frx":3D92
      Top             =   1200
      Visible         =   0   'False
      Width           =   480
   End
   Begin VB.Image imgLib 
      Height          =   480
      Index           =   2
      Left            =   1440
      Picture         =   "frmAyuda.frx":465C
      Top             =   1200
      Visible         =   0   'False
      Width           =   480
   End
   Begin VB.Image imgLib 
      Height          =   480
      Index           =   1
      Left            =   840
      Picture         =   "frmAyuda.frx":4F26
      Top             =   1200
      Visible         =   0   'False
      Width           =   480
   End
   Begin VB.Image imgLib 
      Height          =   480
      Index           =   0
      Left            =   120
      Picture         =   "frmAyuda.frx":57F0
      Top             =   1200
      Visible         =   0   'False
      Width           =   480
   End
End
Attribute VB_Name = "frmAyuda"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Declare Function SetWindowPos Lib "user32" (ByVal hwnd As Long, ByVal hWndInsertAfter As Long, ByVal x As Long, ByVal y As Long, ByVal cx As Long, ByVal cy As Long, ByVal wFlags As Long) As Long
Private Declare Function GetWindowRect Lib "user32" (ByVal hwnd As Long, lpRect As RECT) As Long
Private Type RECT
        Left As Long
        Top As Long
        Right As Long
        Bottom As Long
End Type
Private Declare Function GetFocus Lib "user32" () As Long

Dim lgLastActive As Long


Function fnCutStr(ByRef stSource As String, stI As String, stF As String) As Integer 'Obtiene una cadena que se encuentre entre 2 cadenas.
'"Rellena" la variable stSource con el resultado, y devuelve la posici�n final de
' la cadena obtenida en la cadena original.
    
    Dim stResult As String, inPsI As Integer, inPsF As Integer

    If InStr(1, stSource, stI) > 0 Then
        inPsI = InStr(1, stSource, stI) + Len(stI)
        stResult = Mid(stSource, inPsI)
    End If
    
    If InStr(inPsI, stResult, stF) > 0 Then
        inPsF = InStr(inPsI, stResult, stF) - 1
        stResult = Mid(stResult, 1, inPsF)
        inPsF = inPsF + Len(stF)
    End If
    
        stSource = stResult
        fnCutStr = inPsI + inPsF
    
End Function



Sub H()
    On Error Resume Next
    frmAyuda.Hide
    On Error GoTo 0
End Sub

'Mensaje est�ndar, se oculta haciendo click sobre cualquier zona.
Public Sub sbMsg(stMsg As String, Optional obObject As Object, Optional inIcon As Integer, Optional inPos As Integer, Optional boNoShow As Boolean)
'(inPos par�metro para posici�n relativa a obObject) 0 Debajo del objeto, 1 delante del objeto, 2 encima del objeto, 3 atr�s del objeto
'boNoShow, (True) no permite que se ejecuten instrucciones para mostrar la ventana.
'    Dim rctC As RECT, boPlace As Boolean, boAutoHide As Boolean
    Dim rctC As RECT, boAutoHide As Boolean     'DEPURACION DE CODIGO
    
'Siempre visible:
    If Not boNoShow Then SetWindowPos Me.hwnd, -1, 0, 0, Me.Width / Screen.TwipsPerPixelX, Me.Height / Screen.TwipsPerPixelY, 2
    If boNoShow Then SetWindowPos Me.hwnd, -2, 0, 0, Me.Width / Screen.TwipsPerPixelX, Me.Height / Screen.TwipsPerPixelY, 2


'Detectando si se especific� un t�tulo:
    Dim stTitle As String, boTitle As Boolean
    stTitle = stMsg
    inIx = fnCutStr(stTitle, "%t", "%t")
    If inIx > 0 Then
        lblMsg(0) = stTitle
        lblMsg(0).Visible = True
        stMsg = Mid(stMsg, inIx)
        boTitle = True
    End If
    
    lblMsg(0).Visible = boTitle
    imgIcon.Picture = imgLib(inIcon).Picture
    stMsg = Replace(stMsg, "%2", vbCrLf)
    lblMsg(1).WordWrap = False
    lblMsg(1) = stMsg
    lblMsg(1).Top = 120 + (240 * Abs(boTitle))
    
    'Estableciendo el ancho del p�rrafo:
    If lblMsg(1).Width > 4095 Then '4095 es el ancho m�ximo permitido.
        lblMsg(1).WordWrap = True
        lblMsg(1).Width = 4095
    End If
    
    'Reestableciendo el tama�o de los controles:
    imgIcon.Left = lblMsg(1).Left + lblMsg(1).Width + 100
    picAyuda.Width = imgIcon.Left + imgIcon.Width + 100
    picAyuda.Height = lblMsg(1).Height + (lblMsg(0).Top * 2) + (lblMsg(0).Height * Abs(boTitle))
    If picAyuda.Height < 735 Then picAyuda.Height = 735
    frmAyuda.Width = (picAyuda.Left * 2) + picAyuda.Width
    frmAyuda.Height = (picAyuda.Top * 2) + picAyuda.Height
    
    'Centrar el aviso en el eje Y:
    If Not boTitle Then
        lblMsg(1).Top = (picAyuda.Height - lblMsg(1).Height) / 2
    End If
    
    '00 Determinando si la posici�n del mensaje est� condicionada:
    If Not obObject Is Nothing Then
        GetWindowRect obObject.hwnd, rctC
        rctC.Left = rctC.Left * Screen.TwipsPerPixelX
        rctC.Bottom = rctC.Bottom * Screen.TwipsPerPixelY
        rctC.Right = rctC.Right * Screen.TwipsPerPixelY
        rctC.Top = rctC.Top * Screen.TwipsPerPixelY
    Else
        rctC.Left = (Screen.Width - frmAyuda.Width) / 2
        rctC.Bottom = (Screen.Height - frmAyuda.Height) / 2.5
    End If
    '00 Fin
    
    If inPos = 0 Then 'Debajo
        frmAyuda.Left = rctC.Left
        frmAyuda.Top = rctC.Bottom
    ElseIf inPos = 1 Then 'Frente
        frmAyuda.Left = rctC.Right
        frmAyuda.Top = rctC.Top
    ElseIf inPos = 2 Then 'Encima
        frmAyuda.Left = rctC.Left
        frmAyuda.Top = rctC.Top - frmAyuda.Height
    ElseIf inPos = 3 Then 'Atr�s
        frmAyuda.Left = rctC.Left - frmAyuda.Left
        frmAyuda.Top = rctC.Top
    End If
    
    boAutoHide = True
    'Correcci�n de desaparici�n:
    If frmAyuda.Left + frmAyuda.Width > Screen.Width Then frmAyuda.Left = Screen.Width - frmAyuda.Width
    If frmAyuda.Top + frmAyuda.Height > Screen.Height Then frmAyuda.Top = Screen.Height - frmAyuda.Height
    
    
On Error Resume Next
    If Not boNoShow Then
        frmAyuda.Show
        If ERR.Number <> 0 Then boAutoHide = False: frmAyuda.Show 1
        obObject.SetFocus
        tmrDisable.Enabled = True
        lgLastActive = GetFocus
    End If
    If obObject Is Nothing Or boAutoHide = False Then lgLastActive = -1
On Error GoTo 0

End Sub


Sub sbW(stMsg As String, Optional inIcon As Integer)
    
    If Trim(stMsg) = "" Then frmAyuda.Hide: Exit Sub
    sbMsg stMsg, , inIcon, , True
        
    On Error Resume Next
        If Not frmAyuda.Visible Then frmAyuda.Visible = True: frmAyuda.ZOrder 0
        DoEvents
    On Error GoTo 0
    
End Sub

Private Sub Form_Click()
    picAyuda_Click
End Sub

Private Sub Form_KeyUp(KeyCode As Integer, Shift As Integer)
    If KeyCode <> 13 Then tmrDisable.Enabled = False: frmAyuda.Hide
End Sub


Private Sub imgIcon_Click()
    picAyuda_Click
End Sub

Private Sub lblMsg_Click(Index As Integer)
    picAyuda_Click
End Sub

Private Sub picAyuda_Click()
    frmAyuda.Hide
    tmrDisable.Enabled = False
End Sub


Private Sub tmrDisable_Timer()
    
    If lgLastActive < 0 Then Exit Sub
    
    On Error Resume Next
        If lgLastActive <> GetFocus Then Me.Hide: tmrDisable.Enabled = False
    On Error GoTo 0
    
End Sub


