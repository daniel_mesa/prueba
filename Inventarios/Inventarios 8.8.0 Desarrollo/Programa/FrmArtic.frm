VERSION 5.00
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Object = "{0BA686C6-F7D3-101A-993E-0000C0EF6F5E}#1.0#0"; "THREED32.OCX"
Begin VB.Form FrmArtic 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Art�culos"
   ClientHeight    =   7305
   ClientLeft      =   3990
   ClientTop       =   3045
   ClientWidth     =   7785
   Icon            =   "FrmArtic.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   ScaleHeight     =   7305
   ScaleWidth      =   7785
   Begin VB.Frame FraObse 
      Caption         =   "Observaciones"
      Height          =   615
      Left            =   0
      TabIndex        =   63
      Top             =   5520
      Width           =   7695
      Begin VB.TextBox TxtArtic 
         Height          =   285
         Index           =   20
         Left            =   120
         MaxLength       =   70
         TabIndex        =   53
         Top             =   240
         Width           =   7455
      End
   End
   Begin VB.Frame Frame5 
      Height          =   1095
      Left            =   1440
      TabIndex        =   64
      Top             =   6120
      Width           =   4935
      Begin Threed.SSCommand SCmd_Options 
         Height          =   735
         Index           =   0
         Left            =   120
         TabIndex        =   54
         Top             =   240
         Width           =   855
         _Version        =   65536
         _ExtentX        =   1508
         _ExtentY        =   1296
         _StockProps     =   78
         Caption         =   "&GUARDAR"
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   6
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Picture         =   "FrmArtic.frx":058A
      End
      Begin Threed.SSCommand SCmd_Options 
         Height          =   735
         Index           =   1
         Left            =   1080
         TabIndex        =   55
         Top             =   240
         Width           =   855
         _Version        =   65536
         _ExtentX        =   1508
         _ExtentY        =   1296
         _StockProps     =   78
         Caption         =   "&BORRAR"
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   6
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Picture         =   "FrmArtic.frx":0C54
      End
      Begin Threed.SSCommand SCmd_Options 
         Height          =   735
         Index           =   3
         Left            =   3000
         TabIndex        =   57
         Top             =   240
         Width           =   855
         _Version        =   65536
         _ExtentX        =   1508
         _ExtentY        =   1296
         _StockProps     =   78
         Caption         =   "&LISTAR"
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   6
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Picture         =   "FrmArtic.frx":131E
      End
      Begin Threed.SSCommand SCmd_Options 
         Height          =   735
         Index           =   4
         Left            =   3960
         TabIndex        =   58
         Top             =   240
         Width           =   855
         _Version        =   65536
         _ExtentX        =   1508
         _ExtentY        =   1296
         _StockProps     =   78
         Caption         =   "&SALIR"
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   6
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Picture         =   "FrmArtic.frx":19E8
      End
      Begin Threed.SSCommand SCmd_Options 
         Height          =   735
         Index           =   2
         Left            =   2040
         TabIndex        =   56
         Top             =   240
         Width           =   855
         _Version        =   65536
         _ExtentX        =   1508
         _ExtentY        =   1296
         _StockProps     =   78
         Caption         =   "S&ELECCION"
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   6
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Picture         =   "FrmArtic.frx":20B2
      End
   End
   Begin VB.Frame FraArti 
      Height          =   1455
      Left            =   0
      TabIndex        =   0
      Top             =   0
      Width           =   7695
      Begin VB.TextBox TxtArtic 
         Height          =   285
         Index           =   23
         Left            =   5040
         MaxLength       =   20
         TabIndex        =   2
         Top             =   240
         Width           =   2055
      End
      Begin VB.TextBox TxtArtic 
         Height          =   285
         Index           =   0
         Left            =   1560
         MaxLength       =   20
         TabIndex        =   1
         Top             =   240
         Width           =   2055
      End
      Begin VB.TextBox TxtArtic 
         Height          =   285
         Index           =   1
         Left            =   1560
         MaxLength       =   60
         MultiLine       =   -1  'True
         TabIndex        =   3
         Top             =   720
         Width           =   5535
      End
      Begin VB.TextBox TxtArtic 
         Height          =   285
         Index           =   2
         Left            =   1560
         MaxLength       =   60
         MultiLine       =   -1  'True
         TabIndex        =   4
         Top             =   1080
         Width           =   5535
      End
      Begin Threed.SSCommand CmdLimpia 
         Height          =   375
         Left            =   7200
         TabIndex        =   59
         ToolTipText     =   "Limpia Pantalla"
         Top             =   240
         Width           =   375
         _Version        =   65536
         _ExtentX        =   661
         _ExtentY        =   661
         _StockProps     =   78
         Picture         =   "FrmArtic.frx":2A64
      End
      Begin VB.Label LblCodigoRIPS 
         Caption         =   "C�digo RIPS:"
         Height          =   255
         Left            =   3720
         TabIndex        =   75
         Top             =   240
         Width           =   975
      End
      Begin VB.Label LblCodigo 
         Caption         =   "C�digo :"
         Height          =   255
         Left            =   840
         TabIndex        =   67
         Top             =   240
         Width           =   615
      End
      Begin VB.Label LblNombre 
         Caption         =   "Nombre Comercial :"
         Height          =   255
         Left            =   120
         TabIndex        =   66
         Top             =   720
         Width           =   1455
      End
      Begin VB.Label LblDescripcion 
         Caption         =   "Nombre Cient�fico :"
         Height          =   255
         Left            =   120
         TabIndex        =   65
         Top             =   1080
         Width           =   1455
      End
   End
   Begin TabDlg.SSTab SSTArti 
      Height          =   3855
      Left            =   0
      TabIndex        =   5
      Top             =   1560
      Width           =   7695
      _ExtentX        =   13573
      _ExtentY        =   6800
      _Version        =   393216
      TabHeight       =   520
      TabCaption(0)   =   "Clasificaci�n"
      TabPicture(0)   =   "FrmArtic.frx":312E
      Tab(0).ControlEnabled=   -1  'True
      Tab(0).Control(0)=   "FraClasi"
      Tab(0).Control(0).Enabled=   0   'False
      Tab(0).ControlCount=   1
      TabCaption(1)   =   "Caracter�sticas"
      TabPicture(1)   =   "FrmArtic.frx":314A
      Tab(1).ControlEnabled=   0   'False
      Tab(1).Control(0)=   "FraCarac"
      Tab(1).ControlCount=   1
      TabCaption(2)   =   "K�rdex"
      TabPicture(2)   =   "FrmArtic.frx":3166
      Tab(2).ControlEnabled=   0   'False
      Tab(2).Control(0)=   "FraKardex"
      Tab(2).ControlCount=   1
      Begin VB.Frame FraClasi 
         Height          =   3375
         Left            =   120
         TabIndex        =   60
         Top             =   360
         Width           =   7455
         Begin VB.CheckBox ChkCondi 
            Caption         =   "Condicionado"
            Height          =   255
            Left            =   360
            TabIndex        =   19
            ToolTipText     =   "Medicamento Condicionado"
            Top             =   1920
            Width           =   1455
         End
         Begin VB.Frame FrmParametros 
            Height          =   1095
            Left            =   1680
            TabIndex        =   21
            Top             =   2160
            Width           =   5535
            Begin VB.TextBox TxtClasDis 
               Enabled         =   0   'False
               Height          =   285
               Left            =   3120
               TabIndex        =   25
               Top             =   600
               Width           =   2055
            End
            Begin VB.TextBox TxtClasificacion 
               Enabled         =   0   'False
               Height          =   285
               Left            =   1440
               MaxLength       =   3
               TabIndex        =   23
               Top             =   600
               Width           =   1095
            End
            Begin VB.TextBox TxtVUtil 
               Enabled         =   0   'False
               Height          =   285
               Left            =   1440
               MaxLength       =   3
               TabIndex        =   22
               Top             =   240
               Width           =   735
            End
            Begin Threed.SSCommand CmdSeleccion 
               Height          =   255
               Index           =   0
               Left            =   2640
               TabIndex        =   24
               ToolTipText     =   "Selecci�n de Clasificaci�n"
               Top             =   600
               Width           =   375
               _Version        =   65536
               _ExtentX        =   661
               _ExtentY        =   450
               _StockProps     =   78
               Enabled         =   0   'False
               BevelWidth      =   0
               RoundedCorners  =   0   'False
               Outline         =   0   'False
               Picture         =   "FrmArtic.frx":3182
            End
            Begin VB.Label LblClasificacion 
               Caption         =   "Clasificaci�n: "
               Height          =   255
               Left            =   120
               TabIndex        =   87
               Top             =   600
               Width           =   975
            End
            Begin VB.Label LblVidaUtil 
               Caption         =   "Vida �til (Meses):"
               Height          =   255
               Left            =   120
               TabIndex        =   86
               Top             =   240
               Width           =   1335
            End
         End
         Begin VB.CheckBox ChkDispositivo 
            Caption         =   "Dispositivo"
            Height          =   255
            Left            =   360
            TabIndex        =   20
            Top             =   2640
            Width           =   1455
         End
         Begin VB.CheckBox ChkInNoPos 
            Caption         =   "Material Insumo NO POS"
            Height          =   255
            Left            =   4920
            TabIndex        =   18
            Top             =   1635
            Visible         =   0   'False
            Width           =   2175
         End
         Begin VB.CheckBox ChkControl 
            Caption         =   "De control"
            Height          =   195
            Left            =   1200
            TabIndex        =   15
            Top             =   1670
            Width           =   1095
         End
         Begin VB.CheckBox ChkRopa 
            Caption         =   "Ropa"
            Height          =   255
            Left            =   3600
            TabIndex        =   17
            Top             =   1635
            Width           =   1215
         End
         Begin VB.CheckBox ChkInactivo 
            Caption         =   "Inactivo"
            Height          =   255
            Left            =   2520
            TabIndex        =   16
            Top             =   1635
            Width           =   1215
         End
         Begin VB.ComboBox CboGrupoFact 
            Height          =   315
            ItemData        =   "FrmArtic.frx":3B34
            Left            =   4725
            List            =   "FrmArtic.frx":3B47
            TabIndex        =   13
            Top             =   1200
            Width           =   2660
         End
         Begin VB.TextBox TxtArtic 
            Enabled         =   0   'False
            Height          =   285
            Index           =   6
            Left            =   2880
            MaxLength       =   40
            TabIndex        =   11
            Top             =   560
            Width           =   4455
         End
         Begin VB.CheckBox ChkPyp 
            Caption         =   "PYP"
            Height          =   255
            Left            =   360
            TabIndex        =   14
            Top             =   1635
            Width           =   735
         End
         Begin VB.ComboBox CboTipo 
            Height          =   315
            ItemData        =   "FrmArtic.frx":3BA0
            Left            =   840
            List            =   "FrmArtic.frx":3BB3
            TabIndex        =   12
            Top             =   1200
            Width           =   2660
         End
         Begin VB.TextBox TxtArtic 
            Enabled         =   0   'False
            Height          =   285
            Index           =   4
            Left            =   2880
            MaxLength       =   40
            TabIndex        =   8
            Top             =   240
            Width           =   4455
         End
         Begin VB.TextBox TxtArtic 
            Height          =   285
            Index           =   3
            Left            =   840
            MaxLength       =   9
            TabIndex        =   6
            Top             =   240
            Width           =   975
         End
         Begin VB.TextBox TxtArtic 
            Height          =   285
            Index           =   5
            Left            =   840
            MaxLength       =   4
            TabIndex        =   9
            Top             =   560
            Width           =   615
         End
         Begin Threed.SSCommand CmdSelec 
            Height          =   375
            Index           =   0
            Left            =   2040
            TabIndex        =   7
            ToolTipText     =   "Selecci�n de Grupos de Art�culos"
            Top             =   240
            Width           =   615
            _Version        =   65536
            _ExtentX        =   1085
            _ExtentY        =   661
            _StockProps     =   78
            BevelWidth      =   0
            RoundedCorners  =   0   'False
            Outline         =   0   'False
            Picture         =   "FrmArtic.frx":3C0C
         End
         Begin Threed.SSCommand CmdSelec 
            Height          =   375
            Index           =   1
            Left            =   2040
            TabIndex        =   10
            ToolTipText     =   "Selecci�n de Usos"
            Top             =   560
            Width           =   615
            _Version        =   65536
            _ExtentX        =   1085
            _ExtentY        =   661
            _StockProps     =   78
            BevelWidth      =   0
            RoundedCorners  =   0   'False
            Outline         =   0   'False
            Picture         =   "FrmArtic.frx":45BE
         End
         Begin VB.Label Label11 
            Caption         =   "&Grupo Facturaci�n"
            Height          =   375
            Left            =   3720
            TabIndex        =   82
            Top             =   1125
            Width           =   855
         End
         Begin VB.Label Label4 
            Caption         =   "&Tipo:"
            Height          =   255
            Left            =   240
            TabIndex        =   61
            Top             =   1200
            Width           =   495
         End
         Begin VB.Label Label1 
            Caption         =   "Grupo :"
            Height          =   255
            Left            =   240
            TabIndex        =   74
            Top             =   240
            Width           =   615
         End
         Begin VB.Label Label2 
            Caption         =   "Uso :"
            Height          =   255
            Left            =   240
            TabIndex        =   73
            Top             =   600
            Width           =   615
         End
      End
      Begin VB.Frame FraCarac 
         Height          =   3375
         Left            =   -74880
         TabIndex        =   51
         Top             =   360
         Width           =   7455
         Begin VB.ComboBox CmbUMedida 
            Height          =   315
            Left            =   2040
            TabIndex        =   29
            Top             =   960
            Width           =   3855
         End
         Begin VB.TextBox TxtDePre 
            Enabled         =   0   'False
            Height          =   285
            Left            =   3720
            TabIndex        =   32
            Top             =   1320
            Width           =   2415
         End
         Begin VB.TextBox TxtPrinA 
            Enabled         =   0   'False
            Height          =   285
            Left            =   3720
            TabIndex        =   42
            Top             =   2760
            Width           =   2415
         End
         Begin VB.TextBox TxtPresCom 
            Height          =   285
            Left            =   2040
            MaxLength       =   3
            TabIndex        =   30
            Top             =   1320
            Width           =   1095
         End
         Begin VB.TextBox TxtPrincipio 
            Height          =   285
            Left            =   2040
            MaxLength       =   3
            TabIndex        =   36
            Top             =   2760
            Width           =   1095
         End
         Begin VB.TextBox TxtCodCUM 
            Height          =   285
            Index           =   1
            Left            =   6720
            MaxLength       =   2
            TabIndex        =   41
            Top             =   2400
            Width           =   495
         End
         Begin VB.TextBox TxtCodCUM 
            Height          =   285
            Index           =   0
            Left            =   5040
            MaxLength       =   15
            TabIndex        =   40
            Top             =   2400
            Width           =   1455
         End
         Begin VB.TextBox TxtRegInvima 
            Height          =   285
            Left            =   2040
            MaxLength       =   50
            TabIndex        =   35
            Top             =   2400
            Width           =   1935
         End
         Begin VB.ComboBox cboFormFarm 
            Height          =   315
            ItemData        =   "FrmArtic.frx":4F70
            Left            =   2040
            List            =   "FrmArtic.frx":4F72
            TabIndex        =   27
            Top             =   600
            Width           =   3855
         End
         Begin VB.ComboBox CboPARA 
            Height          =   315
            ItemData        =   "FrmArtic.frx":4F74
            Left            =   2040
            List            =   "FrmArtic.frx":4F76
            TabIndex        =   33
            Text            =   "CboPARA"
            Top             =   1680
            Width           =   2295
         End
         Begin VB.ComboBox CboENTRA 
            Height          =   315
            ItemData        =   "FrmArtic.frx":4F78
            Left            =   6075
            List            =   "FrmArtic.frx":4F7A
            TabIndex        =   39
            Text            =   "CboENTRA"
            Top             =   2040
            Width           =   1095
         End
         Begin VB.ComboBox CboVENCE 
            Height          =   315
            ItemData        =   "FrmArtic.frx":4F7C
            Left            =   6075
            List            =   "FrmArtic.frx":4F7E
            TabIndex        =   38
            Text            =   "CboVENCE"
            Top             =   1680
            Width           =   1095
         End
         Begin VB.ComboBox CboUDConteo 
            Height          =   315
            ItemData        =   "FrmArtic.frx":4F80
            Left            =   2040
            List            =   "FrmArtic.frx":4F82
            TabIndex        =   26
            Text            =   "CboUDConteo"
            Top             =   240
            Width           =   3855
         End
         Begin VB.TextBox TxtArtic 
            Height          =   285
            Index           =   10
            Left            =   2040
            MaxLength       =   30
            TabIndex        =   34
            Top             =   2040
            Width           =   1935
         End
         Begin Threed.SSCommand CmdSeleccion 
            Height          =   255
            Index           =   1
            Left            =   3240
            TabIndex        =   37
            ToolTipText     =   "Selecci�n de Principio"
            Top             =   2760
            Width           =   375
            _Version        =   65536
            _ExtentX        =   661
            _ExtentY        =   450
            _StockProps     =   78
            BevelWidth      =   0
            RoundedCorners  =   0   'False
            Outline         =   0   'False
            Picture         =   "FrmArtic.frx":4F84
         End
         Begin Threed.SSCommand CmdSeleccion 
            Height          =   255
            Index           =   2
            Left            =   3240
            TabIndex        =   31
            ToolTipText     =   "Selecci�n de Presentaci�n"
            Top             =   1320
            Width           =   375
            _Version        =   65536
            _ExtentX        =   661
            _ExtentY        =   450
            _StockProps     =   78
            BevelWidth      =   0
            RoundedCorners  =   0   'False
            Outline         =   0   'False
            Picture         =   "FrmArtic.frx":5936
         End
         Begin VB.Label LblUMedida 
            Caption         =   "Unidad de medida:"
            Height          =   255
            Left            =   360
            TabIndex        =   28
            Top             =   960
            Width           =   1575
         End
         Begin VB.Label lblPresCom 
            Caption         =   "Presentaci�n Comercial :"
            Height          =   255
            Left            =   120
            TabIndex        =   89
            Top             =   1320
            Width           =   1815
         End
         Begin VB.Label PrincipioActivo 
            Caption         =   "Principio Activo:"
            Height          =   255
            Left            =   720
            TabIndex        =   88
            Top             =   2760
            Width           =   1215
         End
         Begin VB.Label Label13 
            Alignment       =   1  'Right Justify
            Caption         =   "__"
            Height          =   255
            Left            =   6480
            TabIndex        =   85
            Top             =   2400
            Width           =   195
         End
         Begin VB.Label LblCodCUM 
            Alignment       =   1  'Right Justify
            AutoSize        =   -1  'True
            Caption         =   "Codigo CUM:"
            Height          =   195
            Left            =   4080
            TabIndex        =   84
            Top             =   2400
            Width           =   945
         End
         Begin VB.Label Label12 
            Alignment       =   1  'Right Justify
            Caption         =   "Registro Invima :"
            Height          =   255
            Left            =   120
            TabIndex        =   83
            Top             =   2400
            Width           =   1755
         End
         Begin VB.Label Label9 
            Alignment       =   1  'Right Justify
            Caption         =   "Destino del art�culo:"
            Height          =   255
            Left            =   120
            TabIndex        =   80
            Top             =   1680
            Width           =   1755
         End
         Begin VB.Label Label8 
            Alignment       =   1  'Right Justify
            Caption         =   "X Fecha de entrada:"
            Height          =   255
            Left            =   4200
            TabIndex        =   78
            Top             =   2040
            Width           =   1755
         End
         Begin VB.Label Label6 
            Alignment       =   1  'Right Justify
            Caption         =   "Lotes x vencimiento:"
            Height          =   255
            Left            =   4200
            TabIndex        =   77
            Top             =   1680
            Width           =   1755
         End
         Begin VB.Label Label7 
            Alignment       =   1  'Right Justify
            Caption         =   "Unidad de conteo :"
            Height          =   255
            Left            =   240
            TabIndex        =   76
            Top             =   240
            Width           =   1635
         End
         Begin VB.Label Label3 
            Alignment       =   1  'Right Justify
            Caption         =   "Forma farmac�utica:"
            Height          =   255
            Left            =   120
            TabIndex        =   62
            Top             =   600
            Width           =   1635
         End
         Begin VB.Label LblConcen 
            Alignment       =   1  'Right Justify
            Caption         =   "Concentraci�n :"
            Height          =   255
            Left            =   120
            TabIndex        =   72
            Top             =   2040
            Width           =   1755
         End
      End
      Begin VB.Frame FraKardex 
         Height          =   2535
         Left            =   -74880
         TabIndex        =   68
         Top             =   360
         Width           =   7455
         Begin VB.CheckBox ChkIvaDed 
            Caption         =   "Iva Deducible"
            Height          =   255
            Left            =   3360
            TabIndex        =   45
            Top             =   240
            Width           =   1335
         End
         Begin VB.ComboBox cboIVA 
            Enabled         =   0   'False
            Height          =   315
            Left            =   1200
            Style           =   2  'Dropdown List
            TabIndex        =   44
            Top             =   240
            Width           =   1815
         End
         Begin VB.TextBox TxtArtic 
            Height          =   285
            Index           =   24
            Left            =   6600
            MaxLength       =   5
            TabIndex        =   46
            Top             =   240
            Width           =   615
         End
         Begin VB.TextBox TxtArtic 
            Height          =   1005
            Index           =   22
            Left            =   4080
            MaxLength       =   60
            MultiLine       =   -1  'True
            TabIndex        =   50
            Top             =   1200
            Width           =   3135
         End
         Begin VB.TextBox TxtArtic 
            Enabled         =   0   'False
            Height          =   285
            Index           =   16
            Left            =   1200
            MaxLength       =   5
            TabIndex        =   52
            Top             =   240
            Visible         =   0   'False
            Width           =   1815
         End
         Begin VB.CheckBox ChkIva 
            Caption         =   "% Iva"
            Height          =   255
            Left            =   480
            TabIndex        =   43
            Top             =   240
            Width           =   735
         End
         Begin VB.TextBox TxtArtic 
            Enabled         =   0   'False
            Height          =   285
            Index           =   17
            Left            =   1680
            MaxLength       =   18
            TabIndex        =   47
            Top             =   1200
            Width           =   1815
         End
         Begin VB.TextBox TxtArtic 
            Enabled         =   0   'False
            Height          =   285
            Index           =   19
            Left            =   1680
            MaxLength       =   18
            TabIndex        =   49
            Top             =   1920
            Width           =   1815
         End
         Begin VB.TextBox TxtArtic 
            Enabled         =   0   'False
            Height          =   285
            Index           =   18
            Left            =   1680
            MaxLength       =   18
            TabIndex        =   48
            Top             =   1560
            Width           =   1815
         End
         Begin VB.Label Label10 
            Alignment       =   1  'Right Justify
            Caption         =   "% Utilidad"
            Height          =   255
            Index           =   23
            Left            =   5400
            TabIndex        =   81
            Top             =   240
            Width           =   1095
         End
         Begin VB.Label Label5 
            Caption         =   "Ubicaci�n F�sica"
            Height          =   255
            Left            =   4080
            TabIndex        =   79
            Top             =   960
            Width           =   1335
         End
         Begin VB.Label LblUltCos 
            Alignment       =   1  'Right Justify
            Caption         =   "Ultimo Costo :"
            Height          =   255
            Left            =   240
            TabIndex        =   71
            Top             =   1200
            Width           =   1335
         End
         Begin VB.Label LblCosPro 
            Alignment       =   1  'Right Justify
            Caption         =   "Costo Promedio :"
            Height          =   255
            Left            =   240
            TabIndex        =   70
            Top             =   1920
            Width           =   1335
         End
         Begin VB.Label LblExistencia 
            Alignment       =   1  'Right Justify
            Caption         =   "Existencia :"
            Height          =   255
            Left            =   240
            TabIndex        =   69
            Top             =   1560
            Width           =   1335
         End
      End
   End
End
Attribute VB_Name = "FrmArtic"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Dim Tabla As String
Dim Mensaje As String
Dim EncontroT As Boolean, vEsDesdeSele As Boolean
Dim Cambia_Codigo As Boolean
'FIXIT: Declare 'ItmDATAUnConteo' with an early-bound data type                            FixIT90210ae-R1672-R1B8ZE
Dim ItmDATAUnConteo() As Variant
'FIXIT: Declare 'ItmDATAFormFarm' with an early-bound data type                            FixIT90210ae-R1672-R1B8ZE
Dim ItmDATAFormFarm() As Variant
'FIXIT: Declare 'itmIVA' with an early-bound data type                                     FixIT90210ae-R1672-R1B8ZE
Dim itmIVA() As Variant
'FIXIT: Declare 'Cod_GruFac' with an early-bound data type                                 FixIT90210ae-R1672-R1B8ZE
Dim Cod_GruFac() As Variant
Public OpcCod As String   'Opci�n de seguridad
Dim StComercial As String 'GAVL  R3201 / T5101
Dim ArrVer() As Variant 'GAVL R3201 / T5101
Dim InI As Integer 'JAUM T36823 VARIABLE UTILIZADA PARA CONTADOR
Dim InFoco As Integer 'JAUM T36823 CAPTURA LA POSICI�N INICIAL DEL FOCO DEL CONTROL
Dim VrUMedida() As Variant 'JLPB T36875-R35965 'Almacena el codigo de las unidades de medida

Private Sub CboENTRA_GotFocus()
   SendKeys "{F4}" 'smdl m1737
End Sub

Private Sub CboENTRA_KeyPress(KeyAscii As Integer) 'smdl m1737
' If KeyAscii = vbKeyReturn Then
'      SSTArti.Tab = 2
'      ChkIva.SetFocus
'   End If
 Call Cambiar_Enter(KeyAscii) 'GAVL NPC  T5268
End Sub

Private Sub cboFormFarm_GotFocus()
   SendKeys "{F4}" 'smdl m1737
End Sub

Private Sub cboFormFarm_KeyPress(KeyAscii As Integer)
    Call Cambiar_Enter(KeyAscii) 'smdl m1737
End Sub

Private Sub CboGrupoFact_GotFocus()
   SendKeys "{F4}" 'smdl m1737
End Sub

Private Sub CboGrupoFact_KeyPress(KeyAscii As Integer)
    If ChkPyp.Enabled = True Then ChkPyp.SetFocus 'smdl m1737
End Sub

Private Sub cboIVA_GotFocus()
   SendKeys "{F4}" 'smdl m1737
End Sub

Private Sub cboIVA_KeyPress(KeyAscii As Integer)
    Call Cambiar_Enter(KeyAscii) 'smdl m1737
End Sub

Private Sub CboPARA_GotFocus()
   SendKeys "{F4}" 'smdl m1737
End Sub

Private Sub CboPARA_KeyPress(KeyAscii As Integer)
   Call Cambiar_Enter(KeyAscii) 'smdl m1737
End Sub

Private Sub CboTipo_Click()
   'ChkPyp.Value = 0
   If CboTipo.ListIndex = 4 Then
      ChkPyp.value = 0
      ChkPyp.Enabled = False
   Else
      ChkPyp.Enabled = True
   End If
   
   'GAPM R2367 INICIO
   If CboTipo.ListIndex = 2 Then
      ChkInNoPos.Visible = True
   Else
      ChkInNoPos.value = vbUnchecked
      ChkInNoPos.Visible = False
   End If
   'GAPM R2367 FIN
   'GSCD T45902-R42283 INICIO
   If CboTipo.ListIndex = 1 Then
      Me.ChkCondi.Visible = True
   Else
      Me.ChkCondi.value = 0
      Me.ChkCondi.Visible = False
   End If
   'GSCD T45902-R42283 FIN
End Sub

Private Sub cboTIPO_GotFocus()
   SendKeys "{F4}" 'smdl m1737
End Sub

Private Sub CboTipo_KeyPress(KeyAscii As Integer)
  '  se pasa instruccion al combo CboGrupoFact_KeyPress
  ' If ChkPyp.Enabled = True Then ChkPyp.SetFocus
   Call Cambiar_Enter(KeyAscii) 'smdl m1737
End Sub
Private Sub CboUDConteo_GotFocus()
   SendKeys "{F4}" 'smdl m1737
End Sub

Private Sub CboUDConteo_KeyPress(KeyAscii As Integer)
   Call Cambiar_Enter(KeyAscii)
End Sub

Private Sub CboVENCE_GotFocus()
   SendKeys "{F4}" 'smdl m1737
End Sub

Private Sub CboVENCE_KeyPress(KeyAscii As Integer)
    Call Cambiar_Enter(KeyAscii) 'smdl m1737
End Sub


Private Sub ChkControl_KeyPress(KeyAscii As Integer) 'CARV R2320
  ' DAHV R2320 - INICIO
  ' Se coloca en comentario porque esta funcionalidad no es �til
'   If KeyAscii = vbKeyReturn Then 'inicio CARV R2320
'      SSTArti.Tab = 1
'      CboUDConteo.SetFocus
'   End If
   'DAHV R2320 - FIN
   
    Call Cambiar_Enter(KeyAscii) 'fin CARV R2320
End Sub







Private Sub ChkInactivo_Click()
   'GMS R1636
   If ChkInactivo And Me.ActiveControl Is ChkInactivo Then
      'If TxtArtic(18) > 0 Then
      If Val(TxtArtic(18)) > 0 Then 'AASV M3468
         Call Mensaje1("No se puede inactivar este " & Mensaje & ". Tiene existencia en bodega.", 3)
         ChkInactivo.value = vbUnchecked
      Else
         Dim cadena As String, i As Integer
         ReDim Arr(1, 0)
         Condicion = "CD_CODI_ELE_REP = " & Comi & TxtArtic(0) & Comi
         Condicion = Condicion & " AND CD_CODI_PLAN_REP = CD_CODI_PLAN"
         Campos = "DISTINCT CD_CODI_PLAN_REP, DE_DESC_PLAN"
         'REOL M2502
         Condicion = Condicion & " AND CD_CODI_ELE_REP=CD_CODI_ARTI  AND CD_CODI_CECO_REP = CD_CODI_CECO_RCCBO"
         Condicion = Condicion & " AND NU_AUTO_ARTI=NU_AUTO_ARTI_KARD  AND NU_AUTO_BODE_KARD=NU_AUTO_BODE_RCCBO"
         Desde = "R_ELE_PLAN,PLANES,IN_R_CENTRO_COSTOBODEGA,IN_KARDEX,ARTICULO"
         Result = LoadMulData(Desde, Campos, Condicion, Arr())
         'REOL M2502
'         Result = LoadMulData("R_ELE_PLAN,PLANES", Campos, Condicion, Arr())
         If Result <> FAIL Then
            If Encontro Then
                cadena = "No se puede inactivar este " & Mensaje & ". Esta asociado a los siguientes planes de salud :" & vbCrLf & vbCrLf
                For i = 0 To UBound(Arr, 2)
                    cadena = cadena & Arr(1, i) & IIf(i = UBound(Arr, 2), NUL$, Coma)
                Next i
                cadena = cadena & Punto & vbCrLf & vbCrLf & "Debe desmarcarlo de estos planes de salud, para poder inactivar este " & Mensaje
                Call Mensaje1(cadena, 3)
                ChkInactivo.value = vbUnchecked
            End If
         End If
      End If
   End If
End Sub

Private Sub ChkInactivo_KeyPress(KeyAscii As Integer) 'smdl m1737
'NMSR M3415
'   If KeyAscii = vbKeyReturn Then
'      SSTArti.Tab = 1
'      CboUDConteo.SetFocus
'   End If 'NMSR M3415
   Call Cambiar_Enter(KeyAscii) 'NMSR M3415
End Sub

Private Sub ChkInNoPos_KeyPress(KeyAscii As Integer)
   'GAPM R2367 INICIO
   'GAVL NPC T5268 SE HACE CAMBIO POR NUEVOS CONTROLES EN EL FORMULARIO
'   If KeyAscii = vbKeyReturn Then
'      SSTArti.Tab = 1
'      CboUDConteo.SetFocus
'   End If
    Call Cambiar_Enter(KeyAscii) 'GAPM R2367
End Sub


'---------------------------------------------------------------------------------------
' Procedure :  ChkIvaDed_Click
' DateTime   :  09/05/2011 14:35
' Author        : Diego_hernandez
' Purpose     : Implementar el TAB con el enter
'---------------------------------------------------------------------------------------
'
Private Sub ChkIvaDed_KeyPress(KeyAscii As Integer)
    Call Cambiar_Enter(KeyAscii)
End Sub

Private Sub ChkPyp_Click()
'FIXIT: Declare 'ArActi' with an early-bound data type                                     FixIT90210ae-R1672-R1B8ZE
Dim ArActi() As Variant 'NMSR M3456
Dim stMsg As String 'NMSR M3456
Dim InI As Integer 'NMSR M3456
    
    'NMSR M3456
   If ChkPyp = vbUnchecked And Me.ActiveControl Is ChkPyp Then
        Condicion = "CD_CODI_ACTI = CD_CODI_ACTI_RARA"
        Condicion = Condicion & " AND CD_CODI_ARTI_RARA=" & Comi & TxtArtic(0).Text & Comi
        ReDim ArActi(1, 0)
         Result = LoadMulData("R_ARTI_ACTI, ACTIVIDAD_PYP", "CD_CODI_ACTI,NO_NOMB_ACTI", Condicion, ArActi())
         If Result <> FAIL Then
            If Encontro Then
                stMsg = "No se puede desmarcar este Art�culo de PyP. Esta asociado a las siguientes Actividades :" & vbCrLf & vbCrLf
                For InI = 0 To UBound(ArActi, 2)
                    stMsg = stMsg & ArActi(0, InI) & " - " & ArActi(1, InI) & IIf(InI = UBound(ArActi, 2), NUL$, Coma)
                Next InI
                'stMsg = stMsg & Punto & vbCrLf & vbCrLf & "Debe desmarcarlo de estas , para poder desmarcar este Art�culo"
                Call Mensaje1(stMsg, 3)
                ChkPyp.value = vbChecked
            End If
         End If
   End If
    'NMSR M3456
End Sub

Private Sub ChkPyp_KeyPress(KeyAscii As Integer) 'smdl m1737
'    se cambia instruccion a ChkInactivo_KeyPress
'    If KeyAscii = vbKeyReturn Then
'      SSTArti.Tab = 1
'      CboUDConteo.SetFocus
'   End If
'FIXIT: Declare 'ArActi' with an early-bound data type                                     FixIT90210ae-R1672-R1B8ZE
Dim ArActi() As Variant 'NMSR M3456
Dim stMsg As String 'NMSR M3456
Dim InI As Integer 'NMSR M3456
    
    'NMSR M3456
   If ChkPyp = vbUnchecked And Me.ActiveControl Is ChkPyp Then
        Condicion = "CD_CODI_ACTI = CD_CODI_ACTI_RARA"
        Condicion = Condicion & " AND CD_CODI_ARTI_RARA=" & Comi & TxtArtic(0).Text & Comi
        ReDim ArActi(1, 0)
         Result = LoadMulData("R_ARTI_ACTI, ACTIVIDAD_PYP", "CD_CODI_ACTI,NO_NOMB_ACTI", Condicion, ArActi())
         If Result <> FAIL Then
            If Encontro Then
                stMsg = "No se puede desmarcar este Art�culo de PyP. Esta asociado a las siguientes Actividades :" & vbCrLf & vbCrLf
                For InI = 0 To UBound(ArActi, 2)
                    stMsg = stMsg & ArActi(0, InI) & " - " & ArActi(1, InI) & IIf(InI = UBound(ArActi, 2), NUL$, Coma)
                Next InI
                'stMsg = stMsg & Punto & vbCrLf & vbCrLf & "Debe desmarcarlo de estas , para poder desmarcar este Art�culo"
                Call Mensaje1(stMsg, 3)
                ChkPyp.value = vbChecked
            End If
         End If
   End If
    'NMSR M3456
   Call Cambiar_Enter(KeyAscii)
End Sub

Private Sub ChkRopa_KeyPress(KeyAscii As Integer)
   'GAPM R2367 INICIO
   'SE AGREGA LA VALIDACION PARA EL CHECK CREADO
   '***********GAVL NPC 5268 SE HACE CAMBIO POR NUEVOS CONTROLES
'   If ChkInNoPos.Visible = False Then
'        'GAPM R2367 SE PASA BLOQUE DE CODIGO A LA CONDICION
'        'NMSR M3415
'        If KeyAscii = vbKeyReturn Then
'           SSTArti.Tab = 1
'           CboUDConteo.SetFocus
'        End If
'        'GAPM R2367 SE PASA BLOQUE DE CODIGO A LA CONDICION
'   End If
   'GAPM R2367 FIN
    Call Cambiar_Enter(KeyAscii) 'NMSR M3415
End Sub

'---------------------------------------------------------------------------------------
' Procedure : CmbUMedida_GotFocus
' DateTime  : 30/09/2016 12:45
' Author    : jairo_parra
' Purpose   : JLPB T36875-R35965 Cambia el enter por el tab
'---------------------------------------------------------------------------------------
'
Private Sub CmbUMedida_GotFocus()
   SendKeys "{F4}"
End Sub

'---------------------------------------------------------------------------------------
' Procedure : CmbUMedida_KeyPress
' DateTime  : 04/10/2016 14:12
' Author    : jairo_parra
' Purpose   : JLPB T36875-R35965 Cambia el enter por el tab
'---------------------------------------------------------------------------------------
'
Private Sub CmbUMedida_KeyPress(KeyAscii As Integer)
   Call Cambiar_Enter(KeyAscii)
End Sub


'---------------------------------------------------------------------------------------
' Procedure : CmbUMedida_LostFocus
' DateTime  : 30/09/2016 14:21
' Author    : jairo_parra
' Purpose   : JLPB T36875-R35965 'Valida que este seleccionado algun item, de no ser asi limpia el combo
'---------------------------------------------------------------------------------------
'
Private Sub CmbUMedida_LostFocus()
   If CmbUMedida.ListIndex = -1 Then CmbUMedida = NUL$
End Sub

Private Sub CmdLimpia_Click()
   Call Limpiar
End Sub

Private Sub CmdSelec_Click(Index As Integer)
    Select Case Index
        Case 0: Codigo = NUL$
                'Codigo = Seleccion("GRUP_ARTICULO", "CD_CODI_GRUP", "CD_CODI_GRUP,DE_DESC_GRUP", "GRUPOS DE ARTICULOS", NUL$)
                Codigo = Seleccion("GRUP_ARTICULO", "DE_DESC_GRUP", "CD_CODI_GRUP,DE_DESC_GRUP", "GRUPOS DE ARTICULOS", NUL$) 'smdl m1737
                If Codigo <> NUL$ Then TxtArtic(3) = Codigo
                TxtArtic(3).SetFocus
                TxtArtic(5).SetFocus
        Case 1: Codigo = NUL$
                'Codigo = Seleccion("USOS", "CD_CODI_USOS", "CD_CODI_USOS,DE_DESC_USOS", "USOS", NUL$)
                Codigo = Seleccion("USOS", "DE_DESC_USOS", "CD_CODI_USOS,DE_DESC_USOS", "USOS", NUL$) 'smdl m1737
                If Codigo <> NUL$ Then TxtArtic(5) = Codigo
                TxtArtic(5).SetFocus
                'SSTArti.Tab = 1
                CboTipo.SetFocus
'        Case 2: FrmLisPr.Show 1
    End Select
End Sub

'ALTER TABLE [ARTICULO] ADD [NU_UTIL_ARTI] [FLOAT] NULL
Private Sub Form_Load()
   Call CenterForm(MDI_Inventarios, Me)
   Tabla = "ARTICULO"
   Mensaje = "Art�culo"
   'Call Leer_Permisos("01005", SCmd_Options(0), SCmd_Options(1), SCmd_Options(3))
   SSTArti.Tab = 0
   CboTipo.ListIndex = 0
   Me.CboENTRA.Clear
   Me.CboENTRA.AddItem "NO"
   'FIXIT: CboENTRA.ItemData(Me.CboENTRA.NewIndex property has no Visual Basic .NET equivalent and will not be upgraded.     FixIT90210ae-R7594-R67265
   Me.CboENTRA.ItemData(Me.CboENTRA.NewIndex) = 0
   Me.CboENTRA.AddItem "SI"
   'FIXIT: CboENTRA.ItemData(Me.CboENTRA.NewIndex property has no Visual Basic .NET equivalent and will not be upgraded.     FixIT90210ae-R7594-R67265
   Me.CboENTRA.ItemData(Me.CboENTRA.NewIndex) = 1
   Me.CboVENCE.Clear
   Me.CboVENCE.AddItem "NO"
   'FIXIT: CboVENCE.ItemData(Me.CboVENCE.NewIndex property has no Visual Basic .NET equivalent and will not be upgraded.     FixIT90210ae-R7594-R67265
   Me.CboVENCE.ItemData(Me.CboVENCE.NewIndex) = 0
   Me.CboVENCE.AddItem "SI"
   'FIXIT: CboVENCE.ItemData(Me.CboVENCE.NewIndex property has no Visual Basic .NET equivalent and will not be upgraded.     FixIT90210ae-R7594-R67265
   Me.CboVENCE.ItemData(Me.CboVENCE.NewIndex) = 1
   Me.CboPARA.Clear
   Me.CboPARA.AddItem "VENTA"
   'FIXIT: CboPARA.ItemData(Me.CboPARA.NewIndex property has no Visual Basic .NET equivalent and will not be upgraded.     FixIT90210ae-R7594-R67265
   Me.CboPARA.ItemData(Me.CboPARA.NewIndex) = 0
   Me.CboPARA.AddItem "CONSUMO"
   'FIXIT: CboPARA.ItemData(Me.CboPARA.NewIndex property has no Visual Basic .NET equivalent and will not be upgraded.     FixIT90210ae-R7594-R67265
   Me.CboPARA.ItemData(Me.CboPARA.NewIndex) = 1
   'Result = loadctrl("IN_UNDVENTA", "TX_NOMB_UNVE", "NU_AUTO_UNVE", CboUDConteo, ItmDATAUnConteo, "NU_MULT_UNVE=1 AND NU_DIVI_UNVE=1")
   Result = loadctrl("IN_UNDVENTA", "TX_NOMB_UNVE", "NU_AUTO_UNVE", CboUDConteo, ItmDATAUnConteo, "NU_MULT_UNVE=1 AND NU_DIVI_UNVE=1 ORDER BY 1") 'JACC R2353
   'Result = loadctrl("IN_FORMAFARMACEUTICA", "TX_NOMB_FOFA", "NU_AUTO_FOFA", cboFormFarm, ItmDATAFormFarm, "")
   Result = loadctrl("IN_FORMAFARMACEUTICA ORDER BY 1 ", "TX_NOMB_FOFA", "NU_AUTO_FOFA", cboFormFarm, ItmDATAFormFarm, "") 'JACC R2353
   ''SELECT CD_CODI_IMPU, DE_NOMB_IMPU, PR_PORC_IMPU FROM TC_IMPUESTOS WHERE ID_TIPO_IMPU='I'
   'Result = loadctrl("TC_IMPUESTOS", "DE_NOMB_IMPU", "CD_CODI_IMPU", Me.cboIVA, itmIVA, "ID_TIPO_IMPU='I'")
   Result = loadctrl("TC_IMPUESTOS", "DE_NOMB_IMPU", "CD_CODI_IMPU", Me.cboIVA, itmIVA, "ID_TIPO_IMPU='I' ORDER BY 1")
   'FAAA T8365
   'Condi = "CD_CODI_GRUF='01' OR CD_CODI_GRUF='02' OR CD_CODI_GRUF='03'"
   Condi = NUL$
   Result = loadctrl("GRUPO_FACTURAS", "DE_DESC_GRUF", "CD_CODI_GRUF", CboGrupoFact, Cod_GruFac, Condi)
   'FAAA T8365
   'Result = loadctrl("GRUPO_FACTURAS", "DE_DESC_GRUF", "CD_CODI_GRUF", CboGrupoFact, Cod_GruFac, Condi & " ORDER BY 1") 'JACC R2353
   Result = loadctrl("GRUPO_FACTURAS", "DE_DESC_GRUF", "CD_CODI_GRUF", CboGrupoFact, Cod_GruFac, Condi)  'JACC R2353
   'FAAA T8365
   'JLPB T36875-R35965 INICIO
   'Result = loadctrl("UNIDAD_MEDIDA", "TX_DESC_UMED", "TX_CODIGO_UMED", CmbUMedida, VrUMedida, Condi) 'JLPB T37127 Se deja en comentario
   Result = loadctrl("UNIDAD_MEDIDA ORDER BY TX_DESC_UMED", "TX_DESC_UMED", "TX_CODIGO_UMED", CmbUMedida, VrUMedida, Condi) 'JLPB T37127
   If Result = FAIL Then Call MsgBox("Se presento un error cargando la tabla UNIDAD_MEDIDA.", vbCritical, App.Title)
   CmbUMedida.ListIndex = -1
   'JLPB T36875-R35965 FIN
   CboGrupoFact.ListIndex = 0
   'DAHV T6105 - Inicio
   ChkIvaDed.Enabled = False
   'DAHV T6105 -FIN
End Sub

Private Sub SCmd_Options_Click(Index As Integer)
   Dim arti As String
   Select Case Index
      Case 0: Encontro = EncontroT
         Grabar
      Case 1: Borrar
      Case 2:
         arti = Seleccion(Tabla, "NO_NOMB_ARTI", "CD_CODI_ARTI,NO_NOMB_ARTI", "ARTICULOS", NUL$)
         vEsDesdeSele = True
         If arti <> NUL$ Then TxtArtic(0) = arti
         TxtArtic(0).SetFocus
         SCmd_Options(2).SetFocus
      Case 3: Call Listar("ARTICULO.rpt", Tabla, "CD_CODI_ARTI", "NO_NOMB_ARTI", "Art�culos", NUL$, NUL$, NUL$, NUL$, NUL$)
         Me.SetFocus
      Case 4: Unload Me
   End Select
End Sub

Private Sub SCmd_Options_GotFocus(Index As Integer)
 Select Case Index
    Case 0: Msglin "Pulse este icono para grabar"
    Case 1: Msglin "Pulse este icono para borrar"
    Case 2: Msglin "Pulse este icono para selecci�n"
    Case 3: Msglin "Pulse este icono para generar un listado"
    Case 4: Msglin "Pulse este icono para cerrar la ventana "
  End Select
End Sub

Private Sub Buscar_Articulo()
   'ReDim Arr(23)
   'ReDim Arr(25) 'PedroJ Def 1839
   'ReDim Arr(26) 'GMS R1636
   'ReDim Arr(27) 'NMSR R1690
   'ReDim Arr(28) 'GMS 2007-10-18. M�dulo de Costos
   'DAHV R2320 - INICIO
   'Se cambia el tama�o del arreglo
   'ReDim Arr(29) 'AASV M3279
   'ReDim Arr(30) 'CARV R2320
   'ReDim Arr(31) 'GAPM R2367 SE REDIMENSIONA LA MATRIZ PARA EL NUEVO VALOR
   'DAHV R2320 - FIN
   'DAHV T6105 - INICIO
   'ReDim Arr(36) 'GAVL R3201 / T5101
   'ReDim Arr(37) 'JLPB T36875-R35965 Se deja en comentario
   'ReDim Arr(38) 'JLPB T36875-R35965 'GSCD T45902-R42283 Se deja en comentario
   ReDim Arr(39) 'GSCD T45902-R42283
   'DAHV T6105 - FIN
   'DAHV T6230 - INICIO
   Dim StIvaArt As String
   StIvaArt = ""
   StIvaArt = fnDevDato("PARAMETROS_IMPUESTOS, ENTIDAD", "TX_IVADEDAR_PAIM", "CD_CODI_TERC_PAIM = CD_NIT_ENTI", True)
   If Not IVADeducible() Then
      Result = DoUpdate("ARTICULO", "TX_IVADED_ARTI='0'", "CD_CODI_ARTI = " & Comi & Cambiar_Comas_Comillas(TxtArtic(0)) & Comi)
   Else
      If StIvaArt = "N" Then
         Result = DoUpdate("ARTICULO", "TX_IVADED_ARTI='0'", "CD_CODI_ARTI = " & Comi & Cambiar_Comas_Comillas(TxtArtic(0)) & Comi)
      End If
   End If
   'DAHV T6230 - FIN
   
   If Cambia_Codigo Then Exit Sub
   Dim i As Byte
   Condicion = "CD_IMPU_ARTI_TCIM=CD_CODI_IMPU AND CD_CODI_ARTI=" & Comi & Cambiar_Comas_Comillas(TxtArtic(0)) & Comi
   Campos = ""
   Campos = Campos & "CD_CODI_ARTI," '0
   Campos = Campos & "NO_NOMB_ARTI," '1
   Campos = Campos & "DE_DESC_ARTI," '2
   Campos = Campos & "CD_GRUP_ARTI," '3
   Campos = Campos & "CD_USOS_ARTI," '4
   Campos = Campos & "DE_CTRA_ARTI," '5
   Campos = Campos & "ID_GRAV_ARTI," '6
   Campos = Campos & "PR_PORC_IMPU," '7
   Campos = Campos & "VL_ULCO_ARTI," '8
   Campos = Campos & "VL_COPR_ARTI," '9
   Campos = Campos & "CT_EXIS_ARTI," '10
   Campos = Campos & "DE_OBSE_ARTI," '11
   Campos = Campos & "ID_TIPO_ARTI," '12
   Campos = Campos & "DE_FOFA_ARTI," '13
   Campos = Campos & "DE_UBIC_ARTI," '14
   Campos = Campos & "CD_RIPS_ARTI," '15
   Campos = Campos & "NU_INDPYP_ARTI," '16
   Campos = Campos & "NU_AUTO_UNVE_ARTI," '17
   Campos = Campos & "TX_VENCE_ARTI," '18
   Campos = Campos & "TX_ENTRA_ARTI," '19
   Campos = Campos & "TX_PARA_ARTI," '20
   Campos = Campos & "NU_AUTO_FOFA_ARTI," '21
   Campos = Campos & "NU_UTIL_ARTI," '22
   Campos = Campos & "CD_CODI_IMPU" '23
   Campos = Campos & ",NU_AUTO_ARTI" '24        'PedroJ
   Campos = Campos & ",CD_CODI_GRUF_ARTI" '25   'Req 1207
   Campos = Campos & ",TX_ESTA_ARTI" '26        'GMS R1636
   Campos = Campos & ",TX_INVIMA_ARTI" '27 'NMSR R1690
   Campos = Campos & ",TX_ROPA_ARTI" '28 GMS 2007-10-18
   Campos = Campos & ",NU_CODCUM_ARTI" 'AASV M3279
   Campos = Campos & ",NU_MEDCTRL_ARTI" '30     'CARV R2320
   Campos = Campos & ",NU_MINSNP_ART" 'GAPM R2367 SE AGREGA EL NUEVO CAMPO CREADO
   'INICIO GAVL R3201 /  T5101
   Campos = Campos & ",NU_DISP_ARTI"
   Campos = Campos & ",NU_VIUT_ARTI"
   Campos = Campos & ",TX_CODCLAS_ARTI"
   Campos = Campos & ",TX_CODPRI_ARTI"
   Campos = Campos & ",TX_PRCO_ARTI"
   'FIN GAVL R3201 /  T5101
   'DAHV T6105 - INICIO
   Campos = Campos & Coma & "TX_IVADED_ARTI"
   'DAHV T6105 - FIN
   Campos = Campos & Coma & "TX_CODIGO_UMED_ARTI" 'JLPB T36875-R35965
   Campos = Campos & Coma & "NU_CONDI_ARTI" 'GSCD T45902-R42283
   Result = LoadData("ARTICULO, TC_IMPUESTOS", Campos, Condicion, Arr())
   If (Result <> False) Then
      If Encontro Then
         '***RONALD PYP
         Encontro = FindInArr(ItmDATAUnConteo, Arr(17))
         If Encontro > -1 Then CboUDConteo.ListIndex = Encontro
         Encontro = FindInArr(ItmDATAFormFarm, Arr(21))
         If Encontro > -1 Then cboFormFarm.ListIndex = Encontro
         Encontro = FindInArr(itmIVA, Arr(23))
         If Encontro > -1 Then cboIVA.ListIndex = Encontro
         ChkPyp.value = IIf((Arr(16) = 1), 1, 0)
         '*****
         TxtArtic(0) = Arr(0)
         TxtArtic(1) = Arr(1)
         TxtArtic(2) = Arr(2)
         TxtArtic(3) = Arr(3)
         TxtArtic(5) = Arr(4)
         Buscar_Grupo
         Buscar_Uso
         TxtArtic(10) = Arr(5)
         ChkIva.value = IIf((Arr(6) = "S"), 1, 0)
         TxtArtic(16) = Format(Arr(7), "#0.##00")
         'If vEsDesdeSele Then vEsDesdeSele = False: TxtArtic(24) = Format(Arr(22), "#0.00")
         TxtArtic(24) = Format(Arr(22), "#0.00")
         TxtArtic(17) = Aplicacion.Formatear_Valor(Arr(8))       'Def 1845
         TxtArtic(18) = Aplicacion.Formatear_Cantidad(BuscarExistencia(CDbl(Arr(24))))    'PedroJ Def 1839 y 1845
         TxtArtic(19) = Aplicacion.Formatear_Valor(Arr(9))       'Def 1845
         TxtArtic(20) = Arr(11)
         'GAVL R3201 / T5101
         ChkDispositivo.value = IIf(Arr(32), "1", "0")
         TxtVUtil.Text = Arr(33)
         TxtClasificacion.Text = Arr(34)
         TxtClasificacion.Tag = Arr(34)
         Call TxtClasificacion_LostFocus
         TxtPrincipio.Text = Arr(35)
         TxtPrincipio.Tag = Arr(35)
         Call TxtPrincipio_LostFocus
         TxtPresCom.Text = Arr(36)
         TxtPresCom.Tag = Arr(36)
         Call TxtPresCom_LostFocus
         'GAVL R3201 / T5101
         If Arr(12) = NUL$ Then
            CboTipo.ListIndex = 2
         Else
            CboTipo.ListIndex = Arr(12)
         End If
         TxtArtic(22) = Arr(14)
         TxtArtic(23) = Arr(15)
         'GAPM R2367 INICIO
         If CboTipo.ListIndex = 2 Then
            ChkInNoPos.Visible = True
         Else
            ChkInNoPos.value = vbUnchecked
            ChkInNoPos.Visible = False
         End If
         'GAPM R2367 FIN
         'Busca la lista de precios
         Me.CboENTRA.ListIndex = IIf(Arr(19) = "S", 1, 0)
         Me.CboVENCE.ListIndex = IIf(Arr(18) = "S", 1, 0)
         Me.CboPARA.ListIndex = IIf(Arr(20) = "C", 1, 0)
         'Buscar_Lista_Precios TxtArtic(0)
         'Req 1207
         Encontro = FindInArr(Cod_GruFac, Arr(25))
         If Encontro > -1 Then CboGrupoFact.ListIndex = Encontro
         'ChkInactivo.Value = Arr(26)
         ChkInactivo.value = Val(Arr(26))    'REOL M1983
         TxtRegInvima.Text = Arr(27) 'NMSR R1690
         ChkRopa.value = Val(Arr(28)) 'GMS 2007-10-18
         'AASV M3279
         If Arr(29) <> "" Then
            'FIXIT: Replace 'Right' function with 'Right$' function                                    FixIT90210ae-R9757-R1B8ZE
            TxtCodCUM(1) = Right(Arr(29), 2) 'SKRV T14590 se quita de comentario  por el caso
            'SKRV T14590 INICIO
            If Mid(TxtCodCUM(1), 1, 1) = "-" Then
               TxtCodCUM(1) = Right(Arr(29), 1)
            End If
            'SKRV T14590 FIN
            'TxtCodCUM(1) = Right(Arr(29), 1) 'JAGS T8459
            'FIXIT: Replace 'Mid' function with 'Mid$' function                                        FixIT90210ae-R9757-R1B8ZE
            TxtCodCUM(0) = Mid(Arr(29), 1, InStr(1, Arr(29), "-") - 1)
         End If
         'AASV M3279
         'inicio CARV R2320
         If Arr(30) = True Then
            ChkControl.value = 1
         Else
            ChkControl.value = 0
         End If
         'fin CARV R2320
         ChkInNoPos.value = IIf(Arr(31) = True, vbChecked, vbUnchecked) 'GAPM R2367 SE AGREGA LA VINCULACION DEL CONTROL A LA BD
         'DAHV T6230 - INICIO
         'DAHV T6105 - INICIO
         ChkIvaDed.value = Val(Arr(37))
         'DAHV T6105 - FIN
         'DAHV T6230 - FIN
         'JLPB T36875-R35965 INICIO
         If Arr(38) <> NUL$ Then
            For i = 0 To UBound(VrUMedida)
               If VrUMedida(i) = Arr(38) Then
                  CmbUMedida.ListIndex = i
               End If
            Next
         Else
            CmbUMedida.ListIndex = -1
         End If
         'JLPB T36875-R35965 FIN
         Me.ChkCondi.value = IIf(UCase(Arr(39)) = "VERDADERO", 1, 0) 'GSCD T45902-R42283
         EncontroT = True
      Else
         For i = 1 To 24
            Select Case i
               Case 7, 8, 9, 11, 12, 13, 14, 15, 21
               Case 16, 17, 18, 19, 24: TxtArtic(i) = "0"
               Case Else: TxtArtic(i) = NUL$
            End Select
         Next i
         TxtArtic(23) = TxtArtic(0)
         ChkIva.value = 0
         TxtArtic(16).Enabled = False
         TxtRegInvima = NUL$
         'Busca la lista de precios
         'Buscar_Lista_Precios TxtArtic(0)
         'DAHV T6105 - INICIO
         ChkIvaDed.value = 0
         'DAHV T6105 - FIN
         Me.ChkCondi.value = 0 'GSCD T45902-R42283
         EncontroT = False
      End If
   End If
   Call ChkDispositivo_Click 'GAVL R3201 / T5101
End Sub

Private Sub Buscar_Grupo()
ReDim Arr(0)
Dim i As Byte
   Condicion = "CD_CODI_GRUP=" & Comi & Cambiar_Comas_Comillas(TxtArtic(3)) & Comi
               
   Result = LoadData("GRUP_ARTICULO", "DE_DESC_GRUP", Condicion, Arr())
   If (Result <> False) Then
     If Encontro Then
        TxtArtic(4) = Arr(0)
     Else
        TxtArtic(3) = NUL$
        TxtArtic(4) = NUL$
     End If
   End If
End Sub

Private Sub Buscar_Uso()
ReDim Arr(0)
Dim i As Byte
   Condicion = "CD_CODI_USOS=" & Comi & Cambiar_Comas_Comillas(TxtArtic(5)) & Comi
               
   Result = LoadData("USOS", "DE_DESC_USOS", Condicion, Arr())
   If (Result <> False) Then
     If Encontro Then
        TxtArtic(6) = Arr(0)
     Else
        TxtArtic(5) = NUL$
        TxtArtic(6) = NUL$
     End If
   End If
End Sub

Private Sub Grabar()
Dim StCodCUM As String 'AASV M3279

   DoEvents
   
   ReDim ArrVer(0) 'GAVL R3201 / T5101
   Call MouseClock
   Msglin "Ingresando informaci�n del " & Mensaje

   If (TxtArtic(0) = NUL$ Or TxtArtic(1) = NUL$ Or TxtArtic(3) = NUL$ Or TxtArtic(5) = NUL$ Or TxtArtic(23) = NUL$) Then
      Call Mensaje1("Se deben especificar los datos completos del " & Mensaje, 3)
      Call MouseNorm: Exit Sub
   End If
   
   If TxtArtic(16) = NUL$ Then TxtArtic(16) = 0
   If TxtArtic(24) = NUL$ Then TxtArtic(24) = 0
'   If ChkIva.Value = 1 And cboIVA.ListIndex = -1 Then
   If cboIVA.ListIndex = -1 Then
      Call Mensaje1("Especifique el impuesto", 3)
      Call MouseNorm: Exit Sub
   End If
'    Call Mensaje1("Especifique el porcentaje de iva", 3)
'    Call MouseNorm: Exit Sub
'   End If
   
   If CboUDConteo.ListIndex = -1 Then
      Call Mensaje1("Especifique La Unidad de conteo", 3)
      Call MouseNorm: Exit Sub
   End If
   If CboTipo.ListIndex = -1 Then
      Call Mensaje1("Especifique el Tipo de Art�culo", 3)
      Call MouseNorm: Exit Sub
   End If
   If cboFormFarm.ListIndex = -1 Then
      Call Mensaje1("Especifique la forma farmac�utica", 3)
      Call MouseNorm: Exit Sub
   End If
   If CboTipo.ListIndex = -1 Then
      Call Mensaje1("Especifique el tipo de medicamento", 3)
      Call MouseNorm: Exit Sub
   End If
   'JAUM T39710 Inicio se deja bloque en comentario
   'If cboIVA.ListIndex = -1 Then
      'Call Mensaje1("Especifique el Impuesto del medicamento", 3)
      'Call MouseNorm: Exit Sub
   'End If
   'JAUM T39710 Fin
   If Me.CboPARA.ListIndex = -1 Then
      Call Mensaje1("Especifique el destino del articulo", 3)
      Call MouseNorm: Exit Sub
   End If
   
   If Me.CboGrupoFact.ListIndex = -1 Then
      Call Mensaje1("Especifique el Grupo de Facturaci�n del articulo", 3)
      Call MouseNorm: Exit Sub
   End If
   'natalia
   'If (InStr(1, TxtArtic(1), Comi) Or InStr(1, TxtArtic(1), Coma)) Then
'      Call Mensaje1("Se debe utilizar Punto para separar unidades en el Nombre Comercial", 3)
'      Call MouseNorm: Exit Sub
'   End If
'   If (InStr(1, TxtArtic(2), Comi) Or InStr(1, TxtArtic(2), Coma)) Then
'      Call Mensaje1("Se debe utilizar Punto para separar unidades en el Nombre Cient�fico", 3)
'      Call MouseNorm: Exit Sub
'   End If
   If (InStr(1, TxtArtic(10), Comi) Or InStr(1, TxtArtic(10), Coma)) Then
      Call Mensaje1("Se debe utilizar Punto para separar unidades en la Concentraci�n", 3)
      Call MouseNorm: Exit Sub
   End If
   'natalia
   'AASV M3279
   If TxtCodCUM(0) <> "" And TxtCodCUM(1) = "" Then
        Call Mensaje1("No se ha diligenciado el codigo CUM correctamente", 3)
        TxtCodCUM(1).SetFocus
        Exit Sub
   ElseIf TxtCodCUM(1) <> "" And TxtCodCUM(0) = "" Then
        Call Mensaje1("No se ha diligenciado el codigo CUM correctamente", 3)
        TxtCodCUM(0).SetFocus
        Exit Sub
   End If
   'INICIO GAVL5263
   If ChkDispositivo.value = 1 And TxtClasificacion.Text = NUL$ Then
      Call Mensaje1("No se ha diligenciado el c�digo de la clasificaci�n", 3)
      TxtClasificacion.SetFocus
      Call MouseNorm: Exit Sub
   End If
   'FIN GAVL5263
   'AASV M3279
   
   If (BeginTran(STranIUp & Tabla) <> FAIL) Then
       If (Not Encontro) Then
         Valores = "CD_CODI_ARTI=" & Comi & Cambiar_Comas_Comillas(TxtArtic(0)) & Comi & Coma
         Valores = Valores & "NO_NOMB_ARTI=" & Comi & Cambiar_Comas_Comillas(TxtArtic(1)) & Comi & Coma
         Valores = Valores & "DE_DESC_ARTI=" & Comi & Cambiar_Comas_Comillas(TxtArtic(2)) & Comi & Coma
         Valores = Valores & "CD_GRUP_ARTI=" & Comi & Cambiar_Comas_Comillas(TxtArtic(3)) & Comi & Coma
         Valores = Valores & "CD_USOS_ARTI=" & Comi & Cambiar_Comas_Comillas(TxtArtic(5)) & Comi & Coma
         Valores = Valores & "DE_CTRA_ARTI=" & Comi & Cambiar_Comas_Comillas(TxtArtic(10)) & Comi & Coma
         Valores = Valores & "ID_GRAV_ARTI=" & Comi & IIf(ChkIva.value = 1, "S", "N") & Comi & Coma
         Valores = Valores & "PR_IMPU_ARTI=" & TxtArtic(16) & Coma
         Valores = Valores & "NU_AUTO_UNVE_ARTI=" & ItmDATAUnConteo(CboUDConteo.ListIndex) & Coma
         Valores = Valores & "NU_AUTO_FOFA_ARTI=" & ItmDATAFormFarm(cboFormFarm.ListIndex) & Coma
         Valores = Valores & "CD_IMPU_ARTI_TCIM=" & Comi & itmIVA(cboIVA.ListIndex) & Comi & Coma
         Valores = Valores & "NU_UTIL_ARTI=" & TxtArtic(24) & Coma


        If Me.CboVENCE.ListIndex = -1 Then
           Valores = Valores & "TX_VENCE_ARTI=" & Comi & "N" & Comi & Coma
        Else
           Valores = Valores & "TX_VENCE_ARTI=" & Comi & IIf(Me.CboVENCE.ItemData(Me.CboVENCE.ListIndex) = 1, "S", "N") & Comi & Coma
        End If
        If Me.CboENTRA.ListIndex = -1 Then
           Valores = Valores & "TX_ENTRA_ARTI=" & Comi & "N" & Comi & Coma
        Else
           Valores = Valores & "TX_ENTRA_ARTI=" & Comi & IIf(Me.CboENTRA.ItemData(Me.CboENTRA.ListIndex) = 1, "S", "N") & Comi & Coma
        End If
        Valores = Valores & "TX_PARA_ARTI=" & Comi & IIf(Me.CboPARA.ItemData(Me.CboPARA.ListIndex) = 1, "C", "V") & Comi & Coma

         '********RONALD PYP*
         If ChkPyp Then
            Valores = Valores & "NU_INDPYP_ARTI = 1" & Coma
         Else
            Valores = Valores & "NU_INDPYP_ARTI = 0" & Coma
         End If
         '*******************
         Valores = Valores & "DE_OBSE_ARTI=" & Comi & Cambiar_Comas_Comillas(TxtArtic(20)) & Comi & Coma
         Valores = Valores & "ID_TIPO_ARTI=" & CboTipo.ListIndex & Coma
         Valores = Valores & "DE_UBIC_ARTI=" & Comi & Cambiar_Comas_Comillas(TxtArtic(22)) & Comi & Coma
         Valores = Valores & "CD_RIPS_ARTI=" & Comi & Cambiar_Comas_Comillas(TxtArtic(23)) & Comi & Coma
         'AASV M3279
         If TxtCodCUM(0) <> "" And TxtCodCUM(1) <> "" Then
             StCodCUM = TxtCodCUM(0) & "-"
             'If Len(TxtCodCUM(1)) = 1 Then StCodCUM = StCodCUM & "0" 'JAGS T8459
             StCodCUM = StCodCUM & TxtCodCUM(1)
             Valores = Valores & "NU_CODCUM_ARTI=" & "'" & StCodCUM & "'" & Coma
         End If
         'AASV M3279
         'Req 1207
         Valores = Valores & "CD_CODI_GRUF_ARTI=" & Comi & Cod_GruFac(CboGrupoFact.ListIndex) & Comi
         'GMS R1636
         Valores = Valores & Coma & "TX_ESTA_ARTI =" & Comi & ChkInactivo & Comi
         
         Valores = Valores & Coma & "TX_INVIMA_ARTI =" & Comi & Cambiar_Comas_Comillas(TxtRegInvima) & Comi 'NMSR R1690
         
         'GMS 2007-10-18. M�dulo de Costos
         Valores = Valores & Coma & "TX_ROPA_ARTI =" & Comi & ChkRopa & Comi
         
         ' DAHV R2320 - INICIO
         ' Como es un campo num�rico no se deben tener en cuenta las comillas en el momento de
         ' enviar el valor.
          'Valores = Valores & Coma & "NU_MEDCTRL_ARTI  =" & Comi & ChkControl.Value & Comi  'CARV R2320
          Valores = Valores & Coma & "NU_MEDCTRL_ARTI  =" & ChkControl.value    'CARV R2320
         ' DAHV R2320 - FIN
         Valores = Valores & Coma & "NU_MINSNP_ART=" & ChkInNoPos.value 'GAPM R2367 SE AGREGA EL NUEVON CONTROL AL PROCESO DE ALMACENAMIENTO DE LA BD
         'INICIO GAVL R3201 24/02/2011
        
      
         If ChkDispositivo.value = 1 Then
            If TxtClasificacion.Text = NUL$ Then Call Mensaje1("Debe ingresar la clasificaci�n", 1): GoTo Siguiente
            Result = LoadData("CLASIFICACION_RIESGO", "TX_CODIGO_CLRI", "TX_CODIGO_CLRI= " & Comi & TxtClasificacion.Text & Comi, ArrVer())
            If Result <> FAIL And Encontro Then
              Valores = Valores & Coma & "NU_DISP_ARTI= " & Comi & ChkDispositivo.value & Comi
              Valores = Valores & Coma & "NU_VIUT_ARTI= " & Comi & TxtVUtil.Text & Comi
              Valores = Valores & Coma & "TX_CODCLAS_ARTI= " & Comi & TxtClasificacion.Text & Comi
            End If
         Else
            Valores = Valores & Coma & "NU_DISP_ARTI= " & Comi & ChkDispositivo.value & Comi
            Valores = Valores & Coma & "NU_VIUT_ARTI= " & Comi & TxtVUtil.Text & Comi
            Valores = Valores & Coma & "TX_CODCLAS_ARTI= " & Comi & TxtClasificacion.Text & Comi
         End If
         
         If TxtPrincipio.Text <> NUL$ Then
            Result = LoadData("PRINCIPIO_ACTIVO", "TX_CODIGO_PRAC", "TX_CODIGO_PRAC= " & Comi & TxtPrincipio.Text & Comi, ArrVer())
            If Result <> FAIL And Encontro Then
             Valores = Valores & Coma & "TX_CODPRI_ARTI= " & Comi & TxtPrincipio.Text & Comi
            End If
         Else
          Valores = Valores & Coma & "TX_CODPRI_ARTI= " & Comi & TxtPrincipio.Text & Comi
         End If
         
         
         If TxtPresCom.Text <> NUL$ Then
            Result = LoadData("PRESENTACION_COMERCIAL", "TX_CODI_PRCO", "TX_CODI_PRCO= " & Comi & TxtPresCom.Text & Comi, ArrVer())
            If Result <> FAIL And Encontro Then
                Valores = Valores & Coma & "TX_PRCO_ARTI= " & Comi & TxtPresCom.Text & Comi
             End If
         Else
            Valores = Valores & Coma & "TX_PRCO_ARTI= " & Comi & TxtPresCom.Text & Comi
         End If
         
         'FIN GAVL R3201 24/02/2011
         
         ' DAHV T6105 - INICIO
         Valores = Valores & Coma & "TX_IVADED_ARTI= " & Comi & ChkIvaDed.value & Comi
         ' DAHV T6105 - FIN
         'JLPB T36875-R35965 INICIO
         If CmbUMedida.ListIndex = -1 Then
            Valores = Valores & Coma & "TX_CODIGO_UMED_ARTI=NULL"
         Else
            Valores = Valores & Coma & "TX_CODIGO_UMED_ARTI='" & VrUMedida(CmbUMedida.ListIndex) & Comi
         End If
         'JLPB T36875-R35965 FIN
         Valores = Valores & Coma & "NU_CONDI_ARTI=" & Me.ChkCondi.value 'GSCD T45902-R42283
         Result = DoInsertSQL(Tabla, Valores)
         ' DAHV T6231 - INICIO
          Result = Auditor(Tabla, TranIns, LastCmd)
        ' DAHV T6231 - FIIN
       Else
         If WarnMsg("Desea Modificar la Informaci�n del " & Mensaje, 256) Then
            Valores = " NO_NOMB_ARTI=" & Comi & Cambiar_Comas_Comillas(TxtArtic(1)) & Comi & Coma
            Valores = Valores & " DE_DESC_ARTI=" & Comi & Cambiar_Comas_Comillas(TxtArtic(2)) & Comi & Coma
            Valores = Valores & " CD_GRUP_ARTI=" & Comi & Cambiar_Comas_Comillas(TxtArtic(3)) & Comi & Coma
            Valores = Valores & " CD_USOS_ARTI=" & Comi & Cambiar_Comas_Comillas(TxtArtic(5)) & Comi & Coma
            Valores = Valores & " DE_CTRA_ARTI=" & Comi & TxtArtic(10) & Comi & Coma
            Valores = Valores & " DE_OBSE_ARTI=" & Comi & Cambiar_Comas_Comillas(TxtArtic(20)) & Comi & Coma
            Valores = Valores & " ID_TIPO_ARTI=" & CboTipo.ListIndex & Coma
            Valores = Valores & "ID_GRAV_ARTI=" & Comi & IIf(ChkIva.value = 1, "S", "N") & Comi & Coma
            Valores = Valores & "PR_IMPU_ARTI=" & TxtArtic(16) & Coma
            Valores = Valores & "NU_UTIL_ARTI=" & TxtArtic(24) & Coma
            Valores = Valores & "NU_AUTO_UNVE_ARTI=" & ItmDATAUnConteo(CboUDConteo.ListIndex) & Coma
            Valores = Valores & "NU_AUTO_FOFA_ARTI=" & ItmDATAFormFarm(cboFormFarm.ListIndex) & Coma
            Valores = Valores & "CD_IMPU_ARTI_TCIM=" & Comi & itmIVA(cboIVA.ListIndex) & Comi & Coma
            Valores = Valores & "TX_VENCE_ARTI=" & Comi & IIf(Me.CboVENCE.ItemData(Me.CboVENCE.ListIndex) = 1, "S", "N") & Comi & Coma
            Valores = Valores & "TX_ENTRA_ARTI=" & Comi & IIf(Me.CboENTRA.ItemData(Me.CboENTRA.ListIndex) = 1, "S", "N") & Comi & Coma
            Valores = Valores & "TX_PARA_ARTI=" & Comi & IIf(Me.CboPARA.ItemData(Me.CboPARA.ListIndex) = 1, "C", "V") & Comi & Coma
            Valores = Valores & "NU_INDPYP_ARTI=" & IIf(ChkPyp, 1, 0) & Coma '****RONALD PYP
            Valores = Valores & " DE_UBIC_ARTI=" & Comi & TxtArtic(22) & Comi & Coma
            Valores = Valores & " CD_RIPS_ARTI=" & Comi & Cambiar_Comas_Comillas(TxtArtic(23)) & Comi & Coma
            'AASV M3279
            If TxtCodCUM(0) <> "" And TxtCodCUM(1) <> "" Then
                StCodCUM = TxtCodCUM(0) & "-"
                'If Len(TxtCodCUM(1)) = 1 Then StCodCUM = StCodCUM & "0"'SKRV T14590 se establece en comentario por el caso
                StCodCUM = StCodCUM & TxtCodCUM(1)
                Valores = Valores & "NU_CODCUM_ARTI=" & "'" & StCodCUM & "'" & Coma
            Else 'JAUM T34741
               Valores = Valores & "NU_CODCUM_ARTI = ''" & Coma 'JAUM T34741
            End If
            'AASV M3279
            'Req 1207
            Valores = Valores & "CD_CODI_GRUF_ARTI=" & Comi & Cod_GruFac(CboGrupoFact.ListIndex) & Comi
            'GMS R1636
            Valores = Valores & Coma & "TX_ESTA_ARTI =" & Comi & ChkInactivo & Comi
            
            Valores = Valores & Coma & "TX_INVIMA_ARTI =" & Comi & Cambiar_Comas_Comillas(TxtRegInvima) & Comi 'NMSR R1690
            
            'GMS 2007-10-18. M�dulo de Costos
            Valores = Valores & Coma & "TX_ROPA_ARTI =" & Comi & ChkRopa & Comi
            
            Valores = Valores & Coma & "NU_MEDCTRL_ARTI  =" & Comi & ChkControl.value & Comi  'CARV R2320
            
            Valores = Valores & Coma & "NU_MINSNP_ART=" & ChkInNoPos.value 'GAPM R2367 SE AGREGA EL NUEVON CONTROL AL PROCESO DE ALMACENAMIENTO DE LA BD
            
            'INICIO GAVL R3201 24/02/2011
            If TxtClasificacion.Tag <> TxtClasificacion.Text Then
              If ChkDispositivo.value = 1 Then
                    If TxtClasificacion.Text = NUL$ Then Call Mensaje1("Debe ingresar la clasificaci�n", 1): GoTo Siguiente
                    Result = LoadData("CLASIFICACION_RIESGO", "TX_CODIGO_CLRI", "TX_CODIGO_CLRI= " & Comi & TxtClasificacion.Text & Comi, ArrVer)
                    If Result <> FAIL And Encontro Then
                            Valores = Valores & Coma & "NU_DISP_ARTI= " & Comi & ChkDispositivo.value & Comi
                            Valores = Valores & Coma & "NU_VIUT_ARTI= " & Comi & TxtVUtil.Text & Comi
                            Valores = Valores & Coma & "TX_CODCLAS_ARTI= " & Comi & TxtClasificacion.Text & Comi
                    Else
                        Call Mensaje1("El codigo de clasificaci�n no existe", 1): GoTo Siguiente
                    End If
                Else
                    Valores = Valores & Coma & "NU_DISP_ARTI= " & Comi & ChkDispositivo.value & Comi
                    Valores = Valores & Coma & "NU_VIUT_ARTI= " & Comi & TxtVUtil.Text & Comi
                    Valores = Valores & Coma & "TX_CODCLAS_ARTI= " & Comi & TxtClasificacion.Text & Comi
                End If
             End If
            
            
            If TxtPrincipio.Tag <> TxtPrincipio.Text Then
                If TxtPrincipio.Text <> NUL$ Then
                    Result = LoadData("PRINCIPIO_ACTIVO", "TX_CODIGO_PRAC", "TX_CODIGO_PRAC= " & Comi & TxtPrincipio.Text & Comi, ArrVer())
                    If Result <> FAIL And Encontro Then
                       Valores = Valores & Coma & "TX_CODPRI_ARTI= " & Comi & TxtPrincipio.Text & Comi
                    Else
                        Call Mensaje1("El codigo de principio no existe", 1): GoTo Siguiente
                    End If
                 Else
                    Valores = Valores & Coma & "TX_CODPRI_ARTI= " & Comi & TxtPrincipio.Text & Comi
                 End If
            End If
            
             If TxtPresCom.Text <> TxtPresCom.Tag Then
                If TxtPresCom.Text <> NUL$ Then
                    Result = LoadData("PRESENTACION_COMERCIAL", "TX_CODI_PRCO", "TX_CODI_PRCO= " & Comi & TxtPresCom.Text & Comi, ArrVer())
                    If Result <> FAIL And Encontro Then
                        Valores = Valores & Coma & "TX_PRCO_ARTI= " & Comi & TxtPresCom.Text & Comi
                    Else
                        Call Mensaje1("El codigo de presentacion comercial no existe", 1): GoTo Siguiente
                    End If
                Else
                  Valores = Valores & Coma & "TX_PRCO_ARTI= " & Comi & TxtPresCom.Text & Comi
                 End If
            End If
             'FIN GAVL R3201 24/02/2011
            
           ' DAHV T6105 - INICIO
             Valores = Valores & Coma & "TX_IVADED_ARTI= " & Comi & ChkIvaDed.value & Comi
           ' DAHV T6105 - FIN
            'JLPB T36875-R35965 INICIO
            If CmbUMedida.ListIndex = -1 Then
               Valores = Valores & Coma & "TX_CODIGO_UMED_ARTI=NULL"
            Else
               Valores = Valores & Coma & "TX_CODIGO_UMED_ARTI='" & VrUMedida(CmbUMedida.ListIndex) & Comi
            End If
            'JLPB T36875-R35965 FIN
            If Cambia_Codigo Then
               Condicion = "CD_CODI_ARTI=" & Comi & Cambiar_Comas_Comillas(TxtArtic(0).Tag) & Comi
            Else
               Condicion = "CD_CODI_ARTI=" & Comi & Cambiar_Comas_Comillas(TxtArtic(0)) & Comi
            End If
            Valores = Valores & Coma & "NU_CONDI_ARTI=" & Me.ChkCondi.value 'GSCD T45902-R42283
            
            If Cambia_Codigo Then
                Call Cambiar_Articulo
            Else
                Debug.Print Valores
                Result = DoUpdate(Tabla, Valores, Condicion)
                ' DAHV T6231 - INICIO
                  Result = Auditor(Tabla, TranUpd, LastCmd)
                ' DAHV T6231 - FIIN
            End If
         End If
       End If
   End If
   If (Result <> FAIL) Then
      If (CommitTran() <> FAIL) Then
          Limpiar
      Else
Siguiente:  'GAVL R3201 / T5101
         Call RollBackTran
      End If
   Else
      Call RollBackTran
   End If
   Call MouseNorm
   Msglin NUL$
End Sub

Private Sub Borrar()
   DoEvents
   
   Call MouseClock
   
   Msglin "Borrando un " & Mensaje
   If (TxtArtic(0) = NUL$) Then
       Call Mensaje1("Ingrese el c�digo del " & Mensaje, 1)
       Call MouseNorm: Msglin NUL$: Exit Sub
   End If
   
   If (EncontroT <> False) Then
      If (Not WarnDel()) Then Call MouseNorm: Msglin NUL$: Exit Sub
      If ValidaArtAsoMovimiento Then Exit Sub 'AASV M3647
         Condicion = "CD_CODI_ARTI=" & Comi & Cambiar_Comas_Comillas(TxtArtic(0)) & Comi
         If (BeginTran(STranDel & Tabla) <> FAIL) Then
            ''Borra la lista de precios
            Call Borrar_Precios
            ''
            Result = DoDelete(Tabla, Condicion)
            If (Result <> FAIL) Then
               Result = Auditor(Tabla, TranDel, LastCmd)
               Condicion = "CD_ARTI_SALD=" & Comi & Cambiar_Comas_Comillas(TxtArtic(0)) & Comi
               Result = DoDelete("SAL_ANT", Condicion)
            End If
        End If
   Else
       Call Mensaje1("No se pudo borrar el " & Mensaje & " de la base de datos, debido a que no existe !!!...", 1)
       Call MouseNorm: Msglin NUL$: Exit Sub
   End If
   If (Result <> FAIL) Then
       If (CommitTran() <> FAIL) Then
          Limpiar
       Else
          Call RollBackTran
       End If
   Else
       Call RollBackTran
   End If
   Call MouseNorm
   Msglin NUL$
End Sub

Private Sub Limpiar()
   Dim i As Byte
   For i = 0 To 24
      Select Case i
         Case 7, 8, 9, 11, 12, 13, 14, 15, 21
         Case 16, 17, 18, 19, 24: TxtArtic(i) = "0"
         Case Else: TxtArtic(i) = NUL$
      End Select
   Next i
   CboTipo.ListIndex = -1
   CboUDConteo.ListIndex = -1
   cboFormFarm.ListIndex = -1
   Me.CboENTRA.ListIndex = -1
   Me.CboPARA.ListIndex = -1
   Me.CboVENCE.ListIndex = -1
   Me.cboIVA.ListIndex = -1
   Me.CboGrupoFact.ListIndex = -1
   ChkPyp.value = False
   ChkIva.value = 0
   TxtArtic(16).Enabled = False
   TxtArtic(0).SetFocus
   'GMS R1636
   ChkInactivo.value = False
   TxtRegInvima = NUL$ 'NMSR R1690
   ChkRopa.value = False 'GMS 2007-10-18. M�dulo de Costos
   TxtCodCUM(0) = "" 'AASV M3279
   TxtCodCUM(1) = "" 'AASV M3279
   ChkControl.value = 0 'CARV R2320
   ChkInNoPos.value = vbUnchecked 'GAPM R2367 SE DEJA EL VALOR POR DEFECTO DESMARCADO
   'GAVL R3201/ T5101
   TxtVUtil.Text = NUL$
   ChkDispositivo.value = 0 'GAVL T5117
   TxtDePre.Text = NUL$ 'GAVL T5117
   TxtClasificacion.Text = NUL$
   TxtClasDis.Text = NUL$
   TxtPrincipio.Text = NUL$
   TxtPresCom.Text = NUL$
   TxtPrinA.Text = NUL$
   TxtClasDis.Text = NUL$
   'GAVL R3201/ T5101
   ' DAHV T6105 - INICIO
   ChkIvaDed.value = 0
   ' DAHV T6105 - FIN
   CmbUMedida.ListIndex = -1 'JLPB T36875-R35965
   Me.ChkCondi.value = 0 'GSCD T45902-R42283
End Sub
'NMSR M3415
Private Sub SSTArti_KeyPress(KeyAscii As Integer)
   If KeyAscii = 13 Then
      Select Case SSTArti.Tab
         Case 0: If TxtArtic(3).Enabled Then TxtArtic(3).SetFocus
         Case 1: If CboUDConteo.Enabled Then CboUDConteo.SetFocus
         Case 2: If ChkIva.Enabled Then ChkIva.SetFocus
      End Select
   End If
End Sub 'NMSR M3415
'---------------------------------------------------------------------------------------
' Procedure : TxtArtic_Change
' DateTime  : 19/12/2016 18:06
' Author    : juan_urrego
' Purpose   : T36823 Valida el Tab y lo remplaza por espacio
'---------------------------------------------------------------------------------------
'
Private Sub TxtArtic_Change(Index As Integer)
   If Index = 1 Or Index = 2 Then
      If (InStr(TxtArtic(Index), vbTab) > 0 Or InStr(TxtArtic(Index), vbCrLf) > 0) And InI = 1 And InFoco > 0 Then InI = 0
      If (InStr(TxtArtic(Index), vbTab) = 0 Or InStr(TxtArtic(Index), vbCrLf) = 0) And InI = 0 And InFoco > 0 Then InI = 0
      If TxtArtic(Index) <> NUL$ And InI = 0 And (InStr(TxtArtic(Index), vbTab) > 0 Or InStr(TxtArtic(Index), vbCrLf) > 0) Then
         InFoco = TxtArtic(Index).SelStart
         InI = 1
         TxtArtic(Index) = Replace(Replace(TxtArtic(Index), vbTab, " "), vbCrLf, " ")
      ElseIf InI = 1 Then
         TxtArtic(Index).SelStart = InFoco
         InFoco = 0
         InI = 0
      End If
   End If
End Sub

Private Sub TxtArtic_GotFocus(Index As Integer)
TxtArtic(Index).SelStart = 0
TxtArtic(Index).SelLength = Len(TxtArtic(Index).Text)
Select Case Index
    Case 0: Msglin "Digite el C�digo del " & Mensaje
            Cambia_Codigo = False
            TxtArtic(0).Tag = TxtArtic(0)
    Case 1: Msglin "Digite el Nombre Comercial del " & Mensaje
            If TxtArtic(0) = NUL$ Then TxtArtic(0).SetFocus: Exit Sub
    Case 2: Msglin "Digite el Nombre Cient�fico del " & Mensaje
    Case 3: Msglin "Digite el Grupo del " & Mensaje
    Case 5: Msglin "Digite el Uso del " & Mensaje
    Case 7: Msglin "Digite la Unidad de Medida del " & Mensaje
    Case 8: Msglin "Digite la Cantidad de Conversi�n del " & Mensaje
    Case 9: Msglin "Digite la Unidad de Conversi�n del " & Mensaje
    Case 10: Msglin "Digite la Concentraci�n del " & Mensaje
    Case 11: Msglin "Digite la Cantidad M�xima del " & Mensaje
    Case 12: Msglin "Digite la Cantidad M�nima del " & Mensaje
    Case 16: Msglin "Digite el Porcentaje de Iva del " & Mensaje
    Case 20: Msglin "Digite las Observaciones del " & Mensaje
    Case 21: Msglin "Digite la forma farmaceutica del " & Mensaje
    Case 22: Msglin "Digite la Ubicaci�n F�sica del " & Mensaje
    Case 24: Msglin "Digite el Porcentaje de ut�lidad del " & Mensaje
End Select
End Sub

Private Sub TxtArtic_KeyDown(Index As Integer, KeyCode As Integer, Shift As Integer)
''NMSR M3415 SE COLOCA EN COMENTARIO PORQUE PARA TODOS LOS PROGRAMAS DE CNT SE DEBE UTILIZAR EL ENTER
'COMO TABULADOR NO LAS TECLAS DE ABAJO Y ARRIBA, AHORA SE VA A AGREGAR EL EVENTO KEYPRESS Y ORGANIZAR
'LOS TAB INDEX
'    Select Case Index
'        Case 0: If KeyCode = vbKeyDown Then TxtArtic(Index + 1).SetFocus: Exit Sub
'                If KeyCode = vbKeyF3 Then Cambia_Codigo = True
'        Case 2, 12:
'                    If KeyCode = vbKeyUp Then
'                     If TxtArtic(Index - 1).Enabled = True Then
'                       TxtArtic(Index - 1).SetFocus
'                     Else
'                       TxtArtic(Index - 2).SetFocus
'                     End If
'                    Else
'                     If KeyCode = vbKeyDown Then
'                      If Index = 2 Then
'                        SSTArti.Tab = 0
'                        TxtArtic(3).SetFocus
'                      Else
'                        SSTArti.Tab = 2
'                        ChkIva.SetFocus
'                      End If
'                     End If
'                    End If
'
'        Case 20:
'                    If KeyCode = vbKeyUp Then
'                      SSTArti.Tab = 2
'                      If TxtArtic(16).Enabled = True Then
'                         TxtArtic(16).SetFocus
'                      Else
'                         ChkIva.SetFocus
'                      End If
'                      Exit Sub
'                    End If
'        Case 10:
'                   'smdl m1736
'                   If KeyCode = vbKeyUp Then
'                      If CboPARA.Enabled = True Then CboPARA.SetFocus
'                   End If
'
'                   If KeyCode = vbKeyDown Then
'                     If TxtRegInvima.Enabled = True Then TxtRegInvima.SetFocus
'                   End If
'        Case 24:
'                   If KeyCode = vbKeyDown Then
'                      If TxtArtic(17).Enabled = True Then TxtArtic(17).SetFocus
'                   End If
'                    'smdl m1736
'        Case Else:
'                    If KeyCode = vbKeyUp Then
'                     If Index = 7 Then
'                       SSTArti.Tab = 0
'                     End If
'                     If Index = 16 Then
'                       SSTArti.Tab = 1
'                     End If
'                     If TxtArtic(Index - 1).Enabled = True Then
'                       TxtArtic(Index - 1).SetFocus
'                     Else
'                      If Index = 16 Then
'                       TxtArtic(12).SetFocus
'                      Else
'                       TxtArtic(Index - 2).SetFocus
'                      End If
'                     End If
'                    Else
'                     If KeyCode = vbKeyDown Then
'                      If Index = 5 Then
'                         SSTArti.Tab = 1
'                      End If
'                      If TxtArtic(Index + 1).Enabled = True Then
'                        TxtArtic(Index + 1).SetFocus
'                      Else
'                       If Index = 16 Then
'                        TxtArtic(20).SetFocus
'                       Else
'                        TxtArtic(Index + 2).SetFocus
'                       End If
'                      End If
'                     End If
'                    End If
'    End Select
'NMSR M3415
If Index = 0 Then
    If KeyCode = vbKeyF3 Then Cambia_Codigo = True
End If 'NMSR M3415
End Sub

Private Sub TxtArtic_KeyPress(Index As Integer, KeyAscii As Integer)
 Select Case Index
'NMSR M3415
'   Case 3, 5:
'          Call ValKeyAlfaNum(KeyAscii)
'          If KeyAscii = 13 Then
'            If Index = 3 Then
'             TxtArtic(5).SetFocus
'            Else
'              'SSTArti.Tab = 1
'              CboTipo.SetFocus
'              'TxtArtic(17).SetFocus
'            End If
'          End If
'   Case 20:
'          Call Cambiar_Enter(KeyAscii) 'smdl m1737
'   Case 2:
'          Call ValKeyAlfaNum(KeyAscii)
'          If KeyAscii = 13 Then
'             SSTArti.Tab = 0
'             TxtArtic(3).SetFocus
'          End If
'
'   Case 8, 11, 12, 16, 24:
'          If Index = 16 Or Index = 24 Then
'             Call ValKeyReal(TxtArtic(Index), KeyAscii)
'          Else
'             Call ValKeyNum(KeyAscii)
'          End If
'        If KeyAscii = 13 Then
'            If Index = 12 Then
'                SSTArti.Tab = 2
'                ChkIva.SetFocus
'            ElseIf Index = 10 Then
'                SSTArti.Tab = 3
'                Me.CboUDConteo.SetFocus
''                TxtArtic(20).SetFocus
'            ElseIf Index = 24 Then
''                Me.SCmd_Options(2).SetFocus
'
'            Else
'                Call Cambiar_Enter(KeyAscii)
'            End If
'        End If
'   Case Else:
'          Call ValKeyAlfaNum(KeyAscii)
'          Call Cambiar_Enter(KeyAscii)
' End Select
'NMSR M3415
'NMSR M3415
   Case 2, 3, 5:
          Call ValKeyAlfaNum(KeyAscii)
    Case 16, 24:
          Call ValKeyReal(TxtArtic(Index), KeyAscii)
    Case Else:
          Call ValKeyAlfaNum(KeyAscii)
 End Select
 Call Cambiar_Enter(KeyAscii)
'NMSR M3415
End Sub

Private Sub TxtArtic_LostFocus(Index As Integer)
   Msglin NUL$
   Select Case Index
      Case 0: If TxtArtic(0) <> NUL$ Then Buscar_Articulo
      Case 3: If TxtArtic(3) <> NUL$ Then Buscar_Grupo
      Case 5: If TxtArtic(5) <> NUL$ Then Buscar_Uso
      Case 8, 11, 12, 16, 24:
         If TxtArtic(Index) = NUL$ Then
            TxtArtic(Index) = "0"
         Else
            If Index = 16 Then
               TxtArtic(Index) = Format(CDbl(TxtArtic(Index)), "#0.##00")
            ElseIf Index = 24 Then
               If Len(TxtArtic(Index)) = 1 And Asc(TxtArtic(Index)) = 46 Then TxtArtic(Index).Text = 0 'AASV M4201
               TxtArtic(Index) = Format(CDbl(TxtArtic(Index)), "#0.00")
            Else
               TxtArtic(Index) = Format(CDbl(TxtArtic(Index)), "######0")
            End If
         End If
   End Select
End Sub

Private Sub ChkIva_Click()
   If ChkIva.value = 1 Then
      TxtArtic(16).Enabled = True
      cboIVA.Enabled = True 'JAUM T39710
      'DAHV T6105 -INICIO
      ' ChkIvaDed.Enabled = IVADeducible()
      ' DAHV T6228 - INICIO
      If IVADeducible() Then
         If fnDevDato("PARAMETROS_IMPUESTOS, ENTIDAD", "TX_IVADEDAR_PAIM", "CD_CODI_TERC_PAIM = CD_NIT_ENTI", True) = "S" Then
            ChkIvaDed.Enabled = True
         Else
            ChkIvaDed.value = 0
            ChkIvaDed.Enabled = False
         End If
      Else
         ChkIvaDed.value = 0
         ChkIvaDed.Enabled = False
         End If
      'DAHV T6228 - INICIO
      'DAHV T6105 - FIN
   Else
      TxtArtic(16).Enabled = False
      cboIVA.Enabled = False 'JAUM T39710
      cboIVA.ListIndex = -1 'JAUM T39710
      'DAHV T6105 -INICIO
      ChkIvaDed.value = 0
      ChkIvaDed.Enabled = False
      'DAHV T6105 - FIN
   End If
   TxtArtic(16) = "0"
End Sub

Private Sub ChkIva_KeyPress(KeyAscii As Integer)
    If ChkIva.value = 1 Then
        If KeyAscii = 13 Then
        'smdl m1737
        'TxtArtic(16).SetFocus
        cboIVA.SetFocus
        'smdl m1737
        End If
        
        ' DAHV T7387 - INICIO
        'DAHV T6105 -INICIO
         'ChkIvaDed.Enabled = IVADeducible()
        'DAHV T6105 - FIN
        
        If IVADeducible() Then
             If fnDevDato("PARAMETROS_IMPUESTOS, ENTIDAD", "TX_IVADEDAR_PAIM", "CD_CODI_TERC_PAIM = CD_NIT_ENTI", True) = "S" Then
                 ChkIvaDed.Enabled = True
             Else
                 ChkIvaDed.value = 0
                 ChkIvaDed.Enabled = False
             End If
        Else
             ChkIvaDed.value = 0
             ChkIvaDed.Enabled = False
        End If
        ' DAHV T7387 - FIN
    Else
        If KeyAscii = 13 Then
        'smdl m1737
        'TxtArtic(20).SetFocus
        TxtArtic(24).SetFocus
        'smdl m1737
        End If
        'DAHV T6105 -INICIO
         ChkIvaDed.Enabled = False
        'DAHV T6105 - FIN
    End If
End Sub

Private Sub Borrar_Precios()
    Result = DoDelete("R_TARI_ARTI", "CD_ARTI_TAAR= '" & TxtArtic(0) & Comi)
End Sub

Private Sub Cambiar_Articulo()
'PASO 1: Crear un codigo temporal CNT000001 y actualizar las tablas que tengan el codigo anterior
    Valores = "CD_CODI_ARTI='CNT000001'" & Coma
    Valores = Valores & "NO_NOMB_ARTI=" & Comi & Cambiar_Comas_Comillas(TxtArtic(1)) & Comi & Coma
    Valores = Valores & "DE_DESC_ARTI=" & Comi & Cambiar_Comas_Comillas(TxtArtic(2)) & Comi & Coma
    Valores = Valores & "CD_GRUP_ARTI=" & Comi & Cambiar_Comas_Comillas(TxtArtic(3)) & Comi & Coma
    Valores = Valores & "CD_USOS_ARTI=" & Comi & Cambiar_Comas_Comillas(TxtArtic(5)) & Comi & Coma
    Valores = Valores & "DE_CTRA_ARTI=" & Comi & Cambiar_Comas_Comillas(TxtArtic(10)) & Comi & Coma
    Valores = Valores & "ID_GRAV_ARTI=" & Comi & IIf(ChkIva.value = 1, "S", "N") & Comi & Coma
    Valores = Valores & "PR_IMPU_ARTI=" & TxtArtic(16) & Coma
    Valores = Valores & "NU_AUTO_UNVE_ARTI=" & ItmDATAUnConteo(CboUDConteo.ListIndex) & Coma
    Valores = Valores & "NU_AUTO_FOFA_ARTI=" & ItmDATAFormFarm(cboFormFarm.ListIndex) & Coma
    Valores = Valores & "CD_IMPU_ARTI_TCIM=" & Comi & itmIVA(cboIVA.ListIndex) & Comi & Coma
    Valores = Valores & "NU_UTIL_ARTI=" & TxtArtic(24) & Coma
    If Me.CboVENCE.ListIndex = -1 Then
       Valores = Valores & "TX_VENCE_ARTI=" & Comi & "N" & Comi & Coma
    Else
       Valores = Valores & "TX_VENCE_ARTI=" & Comi & IIf(Me.CboVENCE.ItemData(Me.CboVENCE.ListIndex) = 1, "S", "N") & Comi & Coma
    End If
    If Me.CboENTRA.ListIndex = -1 Then
       Valores = Valores & "TX_ENTRA_ARTI=" & Comi & "N" & Comi & Coma
    Else
       Valores = Valores & "TX_ENTRA_ARTI=" & Comi & IIf(Me.CboENTRA.ItemData(Me.CboENTRA.ListIndex) = 1, "S", "N") & Comi & Coma
    End If
    Valores = Valores & "TX_PARA_ARTI=" & Comi & IIf(Me.CboPARA.ItemData(Me.CboPARA.ListIndex) = 1, "C", "V") & Comi & Coma
    If ChkPyp Then Valores = Valores & "NU_INDPYP_ARTI = 1" & Coma Else Valores = Valores & "NU_INDPYP_ARTI = 0" & Coma
    
    Valores = Valores & "DE_OBSE_ARTI=" & Comi & Cambiar_Comas_Comillas(TxtArtic(20)) & Comi
    Valores = Valores & Coma & "ID_TIPO_ARTI=" & CboTipo.ListIndex & Coma
    Valores = Valores & "DE_UBIC_ARTI=" & Comi & Cambiar_Comas_Comillas(TxtArtic(22)) & Comi & Coma
    Valores = Valores & "CD_RIPS_ARTI=" & Comi & Cambiar_Comas_Comillas(TxtArtic(23)) & Comi & Coma
    
    'Req 1207
    Valores = Valores & "CD_CODI_GRUF_ARTI=" & Comi & Cod_GruFac(CboGrupoFact.ListIndex) & Comi
    
    'GMS R1636
    Valores = Valores & Coma & "TX_ESTA_ARTI =" & Comi & ChkInactivo & Comi
    Valores = Valores & Coma & "TX_INVIMA_ARTI =" & Comi & Cambiar_Comas_Comillas(TxtRegInvima) & Comi 'NMSR R1690
    
    'INICIO GAVL R3201 24/02/2011
    If ChkDispositivo.value = 1 Then
     If TxtClasificacion.Text = NUL$ Then Call Mensaje1("Debe ingresar la clasificaci�n", 1): Exit Sub
      Result = LoadData("CLASIFICACION_RIESGO", "TX_CODIGO_CLRI", "TX_CODIGO_CLRI= " & Comi & TxtClasificacion.Text & Comi, ArrVer())
      If Result <> FAIL And Encontro Then
        Valores = Valores & Coma & "NU_DISP_ARTI= " & Comi & ChkDispositivo.value & Comi
        Valores = Valores & Coma & "NU_VIUT_ARTI= " & Comi & TxtVUtil.Text & Comi
        Valores = Valores & Coma & "TX_CODCLAS_ARTI= " & Comi & TxtClasificacion.Text & Comi
      End If
    Else
       Valores = Valores & Coma & "NU_DISP_ARTI= " & Comi & ChkDispositivo.value & Comi
        Valores = Valores & Coma & "NU_VIUT_ARTI= " & Comi & TxtVUtil.Text & Comi
        Valores = Valores & Coma & "TX_CODCLAS_ARTI= " & Comi & TxtClasificacion.Text & Comi
    End If

    If TxtPrincipio.Text <> NUL$ Then
        Result = LoadData("PRINCIPIO_ACTIVO", "TX_CODIGO_PRAC", "TX_CODIGO_PRAC= " & Comi & TxtPrincipio.Text & Comi, ArrVer())
        If Result <> FAIL And Encontro Then
         Valores = Valores & Coma & "TX_CODPRI_ARTI= " & Comi & TxtPrincipio.Text & Comi
        End If
    Else
    'faaa t8786
     'Valores = Valores & Coma & "TX_PRCO_ARTI= " & Comi & TxtPrincipio.Text & Comi
     Valores = Valores & Coma & "TX_CODPRI_ARTI= " & Comi & TxtPrincipio.Text & Comi
     'faaa t8786
    End If
   
    If TxtPresCom.Text <> NUL$ Then
        Result = LoadData("PRESENTACION_COMERCIAL", "TX_CODI_PRCO", "TX_CODI_PRCO= " & Comi & TxtPresCom.Text & Comi, ArrVer())
        If Result <> FAIL And Encontro Then
          Valores = Valores & Coma & "TX_PRCO_ARTI= " & Comi & TxtPresCom.Text & Comi
        End If
     Else
        Valores = Valores & Coma & "TX_PRCO_ARTI= " & Comi & TxtPresCom.Text & Comi
     End If
    'FIN GAVL R3201 24/02/2011
    
    Result = DoInsertSQL(Tabla, Valores)
    If Result = FAIL Then Exit Sub
    
    Call ActualizarTablas("CNT000001", CStr(TxtArtic(0).Tag))
    If Result = FAIL Then Exit Sub
'FIN PASO 1

'PASO 2: Actualizar el codigo anterior del articulo por el codigo nuevo
    Result = DoUpdate(Tabla, "CD_CODI_ARTI=" & Comi & TxtArtic(0) & Comi, "CD_CODI_ARTI=" & Comi & TxtArtic(0).Tag & Comi)
    If Result = FAIL Then Exit Sub
'FIN PASO 2

'PASO 3: Actualizar las tablas que tengan el codigo temporal por el nuevo codigo
    Call ActualizarTablas(CStr(TxtArtic(0)), "CNT000001")
    If Result = FAIL Then Exit Sub
'FIN PASO 3

'Borrar el registro del codigo temporal
    Condicion = "CD_CODI_ARTI='CNT000001'"
    Result = DoDelete(Tabla, Condicion)

End Sub

'PedroJ Def 1839
Private Function BuscarExistencia(AutoNum As Double) As Double
Dim bodegas As String
'FIXIT: Declare 'ArrB' with an early-bound data type                                       FixIT90210ae-R1672-R1B8ZE
Dim ArrB() As Variant
Dim i As Double, Saldo As Double

    ReDim ArrB(0, 0)
    Result = LoadMulData("IN_BODEGA ORDER BY NU_AUTO_BODE", "NU_AUTO_BODE", NUL$, ArrB)
    If Result <> FAIL And Encontro Then
        bodegas = "("
        For i = 0 To UBound(ArrB, 2)
            bodegas = bodegas & ArrB(0, i)
            If i <> UBound(ArrB, 2) Then bodegas = bodegas & ","
        Next i
        bodegas = bodegas & ")"
    End If
    
    Campos = "NU_ACTUNMR_KARD, NU_ACTUDNM_KARD"
    Desde = "IN_KARDEX, ARTICULO, IN_BODEGA, IN_UNDVENTA"
    
    Condi = "NU_AUTO_KARD IN (SELECT MAX(NU_AUTO_KARD)"                          'Kardex por bodegas
    Condi = Condi & " From IN_KARDEX "
    Condi = Condi & " Where (NU_AUTO_ARTI_KARD = " & AutoNum & ")"
    'Condi = Condi & " And NU_AUTO_BODE_KARD IN " & bodegas
    If Len(bodegas) > 0 Then Condi = Condi & " And NU_AUTO_BODE_KARD IN " & bodegas     'Mantis 413
    Condi = Condi & " And (NU_AUTO_KARD>=0) GROUP BY NU_AUTO_BODE_KARD, NU_AUTO_ARTI_KARD)"

    Condi = Condi & " AND NU_ACTUNMR_KARD<>0 AND NU_AUTO_ARTI_KARD=NU_AUTO_ARTI"
    Condi = Condi & " AND NU_AUTO_BODE_KARD=NU_AUTO_BODE AND NU_AUTO_UNVE_ARTI=NU_AUTO_UNVE"
    Condi = Condi & " ORDER BY NU_AUTO_ARTI"

    ReDim ArrB(1, 0)
    Saldo = 0
    Result = LoadMulData(Desde, Campos, Condi, ArrB)
    If Result <> FAIL And Encontro Then 'BuscarExistencia = ArrB(0) / ArrB(1) Else BuscarExistencia = 0
       For i = 0 To UBound(ArrB, 2)
          ' DAHV M4817
          ' Saldo = Saldo + ArrB(0, i) / ArrB(1, i)
           If ArrB(1, i) <> 0 Then
                Saldo = Saldo + ArrB(0, i) / ArrB(1, i)
           End If
           
          ' DAHV M4817
       Next i
    Else
        Saldo = 0
    End If
    
      BuscarExistencia = Saldo
End Function
'PedroJ Def 1839

'Actualiza las tablas que usan como llave el codigo del articulo
'FIXIT: Declare 'ActualizarTablas' with an early-bound data type                           FixIT90210ae-R1672-R1B8ZE
Private Function ActualizarTablas(CodNuevo As String, CodAnterior As String)
    Result = DoUpdate("R_TARI_ARTI", "CD_ARTI_TAAR=" & Comi & CodNuevo & Comi, "CD_ARTI_TAAR=" & Comi & CodAnterior & Comi)
    If Result = FAIL Then Exit Function
    Result = DoUpdate("R_DEPE_ARTI", "CD_ARTI_DEAR=" & Comi & CodNuevo & Comi, "CD_ARTI_DEAR=" & Comi & CodAnterior & Comi)
    If Result = FAIL Then Exit Function
    Result = DoUpdate("R_AJUS_ARTI", "CD_ARTI_AJAR=" & Comi & CodNuevo & Comi, "CD_ARTI_AJAR=" & Comi & CodAnterior & Comi)
    If Result = FAIL Then Exit Function
    Result = DoUpdate("R_COTI_ARTI", "CD_ARTI_COAR=" & Comi & CodNuevo & Comi, "CD_ARTI_COAR=" & Comi & CodAnterior & Comi)
    If Result = FAIL Then Exit Function
    Result = DoUpdate("R_ORCO_ARTI", "CD_ARTI_ORAR=" & Comi & CodNuevo & Comi, "CD_ARTI_ORAR=" & Comi & CodAnterior & Comi)
    If Result = FAIL Then Exit Function
    Result = DoUpdate("R_COMP_ARTI", "CD_ARTI_COAR=" & Comi & CodNuevo & Comi, "CD_ARTI_COAR=" & Comi & CodAnterior & Comi)
    If Result = FAIL Then Exit Function
    Result = DoUpdate("R_ENTRA_ARTI", "CD_ARTI_ENAR=" & Comi & CodNuevo & Comi, "CD_ARTI_ENAR=" & Comi & CodAnterior & Comi)
    If Result = FAIL Then Exit Function
    Result = DoUpdate("R_VENT_ARTI", "CD_ARTI_VEAR=" & Comi & CodNuevo & Comi, "CD_ARTI_VEAR=" & Comi & CodAnterior & Comi)
    If Result = FAIL Then Exit Function
    Result = DoUpdate("R_REQU_ARTI", "CD_ARTI_RQAR=" & Comi & CodNuevo & Comi, "CD_ARTI_RQAR=" & Comi & CodAnterior & Comi)
    If Result = FAIL Then Exit Function
    Result = DoUpdate("R_DESPA_ARTI", "CD_ARTI_DEAR=" & Comi & CodNuevo & Comi, "CD_ARTI_DEAR=" & Comi & CodAnterior & Comi)
    If Result = FAIL Then Exit Function
    Result = DoUpdate("R_SALIA_ARTI", "CD_ARTI_SAAR=" & Comi & CodNuevo & Comi, "CD_ARTI_SAAR=" & Comi & CodAnterior & Comi)
    If Result = FAIL Then Exit Function
    Result = DoUpdate("R_DEVO_ARTI", "CD_ARTI_DEAR=" & Comi & CodNuevo & Comi, "CD_ARTI_DEAR=" & Comi & CodAnterior & Comi)
    If Result = FAIL Then Exit Function
    Result = DoUpdate("R_DEVOD_ARTI", "CD_ARTI_DVAR=" & Comi & CodNuevo & Comi, "CD_ARTI_DVAR=" & Comi & CodAnterior & Comi)
    If Result = FAIL Then Exit Function
    Result = DoUpdate("R_BAJAD_ARTI", "CD_ARTI_BDAR=" & Comi & CodNuevo & Comi, "CD_ARTI_BDAR=" & Comi & CodAnterior & Comi)
    If Result = FAIL Then Exit Function
    Result = DoUpdate("R_TRASD_ARTI", "CD_ARTI_TRAR=" & Comi & CodNuevo & Comi, "CD_ARTI_TRAR=" & Comi & CodAnterior & Comi)
    If Result = FAIL Then Exit Function
    Result = DoUpdate("SAL_ANT", "CD_ARTI_SALD=" & Comi & CodNuevo & Comi, "CD_ARTI_SALD=" & Comi & CodAnterior & Comi)
    If Result = FAIL Then Exit Function
    Result = DoUpdate("SAL_ANT_DEP", "CD_ARTI_SALD=" & Comi & CodNuevo & Comi, "CD_ARTI_SALD=" & Comi & CodAnterior & Comi)
    If Result = FAIL Then Exit Function
    Result = DoUpdate("KARDEX_ARTI", "CD_ARTI_KARD=" & Comi & CodNuevo & Comi, "CD_ARTI_KARD=" & Comi & CodAnterior & Comi)
    If Result = FAIL Then Exit Function
    Result = DoUpdate("INVE_FISICO", "CD_ARTI_INVF=" & Comi & CodNuevo & Comi, "CD_ARTI_INVF=" & Comi & CodAnterior & Comi)
    If Result = FAIL Then Exit Function
    Result = DoUpdate("R_DEVOL_ARTI", "CD_ARTI_DVAR=" & Comi & CodNuevo & Comi, "CD_ARTI_DVAR=" & Comi & CodAnterior & Comi)
    If Result = FAIL Then Exit Function
    Result = DoUpdate("CIERRE_MES_DEP", "CD_ARTI_CIER=" & Comi & CodNuevo & Comi, "CD_ARTI_CIER=" & Comi & CodAnterior & Comi)
    If Result = FAIL Then Exit Function
    Result = DoUpdate("CIERRE_MES", "CD_ARTI_CIER=" & Comi & CodNuevo & Comi, "CD_ARTI_CIER=" & Comi & CodAnterior & Comi)
    If Result = FAIL Then Exit Function
    Result = DoUpdate("DOSIFICACION", "CD_CODI_ARTI_DOSF=" & Comi & CodNuevo & Comi, "CD_CODI_ARTI_DOSF=" & Comi & CodAnterior & Comi)
    If Result = FAIL Then Exit Function
    Result = DoUpdate("R_DONA_ARTI", "CD_ARTI_DOAR=" & Comi & CodNuevo & Comi, "CD_ARTI_DOAR=" & Comi & CodAnterior & Comi)
    If Result = FAIL Then Exit Function
    Result = DoUpdate("R_DEVPROV_ARTI", "CD_ARTI_DEAR=" & Comi & CodNuevo & Comi, "CD_ARTI_DEAR=" & Comi & CodAnterior & Comi)
    If Result = FAIL Then Exit Function
   
    If Pacientes Then
       Result = DoUpdate("FORMULAS", "CD_CODI_ELE_FORM=" & Comi & CodNuevo & Comi, "CD_CODI_ELE_FORM=" & Comi & CodAnterior & Comi)
       If Result = FAIL Then Exit Function
       Result = DoUpdate("R_ELE_PLAN", "CD_CODI_ELE_REP=" & Comi & CodNuevo & Comi, "CD_CODI_ELE_REP=" & Comi & CodAnterior & Comi)
       If Result = FAIL Then Exit Function
       Result = DoUpdate("R_SER_ARTI", "CD_CODI_ARTI_RSA=" & Comi & CodNuevo & Comi, "CD_CODI_ARTI_RSA=" & Comi & CodAnterior & Comi)
       If Result = FAIL Then Exit Function
       Result = DoUpdate("DOSIS", "CD_CODI_ARTI_DOSI=" & Comi & CodNuevo & Comi, "CD_CODI_ARTI_DOSI=" & Comi & CodAnterior & Comi)
       If Result = FAIL Then Exit Function
       Result = DoUpdate("ORDEN_MEDI", "CD_CODI_ARTI_ORME=" & Comi & CodNuevo & Comi, "CD_CODI_ARTI_ORME=" & Comi & CodAnterior & Comi)
       If Result = FAIL Then Exit Function
       Result = DoUpdate("ENF_DETAORDEN", "CD_CODI_ARTI_ENDO=" & Comi & CodNuevo & Comi, "CD_CODI_ARTI_ENDO=" & Comi & CodAnterior & Comi)
       If Result = FAIL Then Exit Function
       Result = DoUpdate("R_ARTICULO_DOCANEXO", "CD_ARTI_RARDA=" & Comi & CodNuevo & Comi, "CD_ARTI_RARDA=" & Comi & CodAnterior & Comi)
       If Result = FAIL Then Exit Function
       Result = DoUpdate("CX_SOLICITUD_ARTICULO", "CD_CODI_ARTI_SOAR=" & Comi & CodNuevo & Comi, "CD_CODI_ARTI_SOAR=" & Comi & CodAnterior & Comi)
       If Result = FAIL Then Exit Function
       Result = DoUpdate("CX_R_CANA_ARTI", "CD_CODI_ARTI_RCAR=" & Comi & CodNuevo & Comi, "CD_CODI_ARTI_RCAR=" & Comi & CodAnterior & Comi)
       If Result = FAIL Then Exit Function
       Result = DoUpdate("R_DIAGNOSTICO_MEDICAMENTO", "CD_ARTI_RDIME=" & Comi & CodNuevo & Comi, "CD_ARTI_RDIME=" & Comi & CodAnterior & Comi)
       If Result = FAIL Then Exit Function
       Result = DoUpdate("R_ARTI_ACTI", "CD_CODI_ARTI_RARA=" & Comi & CodNuevo & Comi, "CD_CODI_ARTI_RARA=" & Comi & CodAnterior & Comi)
       If Result = FAIL Then Exit Function
       Result = DoUpdate("ENF_APLIMEDINS", "CD_CODI_ARTI_EAMI=" & Comi & CodNuevo & Comi, "CD_CODI_ARTI_EAMI=" & Comi & CodAnterior & Comi)
       If Result = FAIL Then Exit Function
       Result = DoUpdate("CX_REGANEST_MEDI", "CD_CODI_ARTI_RMED=" & Comi & CodNuevo & Comi, "CD_CODI_ARTI_RMED=" & Comi & CodAnterior & Comi)
       If Result = FAIL Then Exit Function
       Result = DoUpdate("CX_HOJA_GASTO", "CD_CODI_ARTI_HGAS=" & Comi & CodNuevo & Comi, "CD_CODI_ARTI_HGAS=" & Comi & CodAnterior & Comi)
       If Result = FAIL Then Exit Function
       Result = DoUpdate("CX_SOLICITUD_MATESPECIAL", "CD_CODI_ARTI_SOME=" & Comi & CodNuevo & Comi, "CD_CODI_ARTI_SOME=" & Comi & CodAnterior & Comi)
       If Result = FAIL Then Exit Function
       Result = DoUpdate("HIST_MEDI_FREC", "CD_CODI_ARTI_HMEF=" & Comi & CodNuevo & Comi, "CD_CODI_ARTI_HMEF=" & Comi & CodAnterior & Comi)
       If Result = FAIL Then Exit Function
       Result = DoUpdate("HIST_MEDI", "CD_CODI_ARTI_HMED=" & Comi & CodNuevo & Comi, "CD_CODI_ARTI_HMED=" & Comi & CodAnterior & Comi) 'Dayan, Def 5579
    End If

End Function

Private Sub TxtCodCUM_KeyPress(Index As Integer, KeyAscii As Integer)
Call ValKeyAlfaNumSinCar(KeyAscii) 'AASV VALIDA QUE EL CARACTER DIGITADO SEA ALFANUMERICO SIN CARACTERES ESPECIALES
If Index = 1 Then
    SSTArti.Tab = 2 'GAVL T5268
End If
Call Cambiar_Enter(KeyAscii) 'AASV M3279 CAMBIA EL ENTER POR TAB
End Sub
Private Sub TxtRegInvima_KeyPress(KeyAscii As Integer)
    Call Cambiar_Enter(KeyAscii)
End Sub
'---------------------------------------------------------------------------------------
' Procedure : ValidaArtAsoMovimiento
' DateTime  : 15/08/2008 17:11
' Author    : albert_silva
' Purpose   : AASV M3647 VALIDA QUE EL ARTICULO NO ESTE ASOCIADO A UN MOVIMIENTO
'---------------------------------------------------------------------------------------
'
Function ValidaArtAsoMovimiento() As Boolean
Dim ArTablaCampo() As String
Dim InTablaCampo As Integer
Dim StCampo As String

ValidaArtAsoMovimiento = False
ReDim ArTablaCampo(43, 1)

ArTablaCampo(0, 0) = "DOSIFICACION":               ArTablaCampo(0, 1) = "CD_CODI_ARTI_DOSF"
ArTablaCampo(1, 0) = "MEDICAMENTOS_RIPS":          ArTablaCampo(1, 1) = "CD_CODI_ARTI_MERI"
ArTablaCampo(2, 0) = "CARGOS_ADSCRITO":            ArTablaCampo(2, 1) = "CD_CODI_ARTI_CAAD"
ArTablaCampo(3, 0) = "CX_HOJA_GASTO":              ArTablaCampo(3, 1) = "CD_CODI_ARTI_HGAS"
ArTablaCampo(4, 0) = "CX_R_CANA_ARTI":             ArTablaCampo(4, 1) = "CD_CODI_ARTI_RCAR"
ArTablaCampo(5, 0) = "CX_REGANEST_MEDI":           ArTablaCampo(5, 1) = "CD_CODI_ARTI_RMED"
ArTablaCampo(6, 0) = "CX_SEPARAR_ARTICULO":        ArTablaCampo(6, 1) = "CD_CODI_ARTI_SEAR"
ArTablaCampo(7, 0) = "CX_SOLICITUD_ARTICULO":      ArTablaCampo(7, 1) = "CD_CODI_ARTI_SOAR"
ArTablaCampo(8, 0) = "CX_SOLICITUD_MATESPECIAL":   ArTablaCampo(8, 1) = "CD_CODI_ARTI_SOME"
ArTablaCampo(9, 0) = "DESPA_TEMP":                 ArTablaCampo(9, 1) = "CD_CODI_ARTI"
ArTablaCampo(10, 0) = "DET_MEDINS_COSTO":          ArTablaCampo(10, 1) = "TX_CODI_ARTI_MEDI"
ArTablaCampo(11, 0) = "DOSIS":                     ArTablaCampo(11, 1) = "CD_CODI_ARTI_DOSI"
ArTablaCampo(12, 0) = "ENF_APLIMEDINS":            ArTablaCampo(12, 1) = "CD_CODI_ARTI_EAMI"
ArTablaCampo(13, 0) = "ENF_DETAORDEN":             ArTablaCampo(13, 1) = "CD_CODI_ARTI_ENDO"
ArTablaCampo(14, 0) = "ENTRAORDEN_TEMP":           ArTablaCampo(14, 1) = "CD_CODI_ARTI"
ArTablaCampo(15, 0) = "HIST_MEDI":                 ArTablaCampo(15, 1) = "CD_CODI_ARTI_HMED"
ArTablaCampo(16, 0) = "HIST_MEDI_FREC":            ArTablaCampo(16, 1) = "CD_CODI_ARTI_HMEF"
ArTablaCampo(17, 0) = "HIST_MEDI_T":               ArTablaCampo(17, 1) = "CD_CODI_ARTI_HMED"
ArTablaCampo(18, 0) = "IN_ACTA_TEMP":              ArTablaCampo(18, 1) = "CD_CODI_ARTI"
ArTablaCampo(19, 0) = "IN_EXISTENCIA_MINIMA":      ArTablaCampo(19, 1) = "TX_CODI_ARTI_EXMI"
ArTablaCampo(20, 0) = "IN_KARDGRUP_TEMP":          ArTablaCampo(20, 1) = "CD_CODI_ARTI_KARD"
ArTablaCampo(21, 0) = "IN_SALDOREPORTE":           ArTablaCampo(21, 1) = "CD_CODI_ARTI"
ArTablaCampo(22, 0) = "ITEM_GLOSA":                ArTablaCampo(22, 1) = "CD_CODI_ARTI_ITGL"
ArTablaCampo(23, 0) = "R_ARTI_ACTI":               ArTablaCampo(23, 1) = "CD_CODI_ARTI_RARA"
ArTablaCampo(24, 0) = "R_MPOS_HMJU":               ArTablaCampo(24, 1) = "TX_CODI_ARTI_RMJU"
ArTablaCampo(25, 0) = "R_SER_ARTI":                ArTablaCampo(25, 1) = "CD_CODI_ARTI_RSA"
ArTablaCampo(26, 0) = "R_AJUS_ARTI":               ArTablaCampo(26, 1) = "CD_ARTI_AJAR"
ArTablaCampo(27, 0) = "R_DIAGNOSTICO_MEDICAMENTO": ArTablaCampo(27, 1) = "CD_ARTI_RDIME"
ArTablaCampo(28, 0) = "R_ARTICULO_DOCANEXO":       ArTablaCampo(28, 1) = "CD_ARTI_RARDA"
ArTablaCampo(29, 0) = "ANULA_TEMP":                ArTablaCampo(29, 1) = "CD_CODI_ARTI"
ArTablaCampo(30, 0) = "IN_LOTE":                   ArTablaCampo(30, 1) = "NU_AUTO_ARTI_LOTE"
ArTablaCampo(31, 0) = "IN_KARDEX":                 ArTablaCampo(31, 1) = "NU_AUTO_ARTI_KARD"
ArTablaCampo(32, 0) = "IN_DETALLE":                ArTablaCampo(32, 1) = "NU_AUTO_ARTI_DETA"
ArTablaCampo(33, 0) = "IN_EXISTENCIA":             ArTablaCampo(33, 1) = "NU_AUTO_ARTI_EXIS"
ArTablaCampo(34, 0) = "IN_R_LIST_ARTI":            ArTablaCampo(34, 1) = "NU_AUTO_ARTI_RLIA"
ArTablaCampo(35, 0) = "CX_SOLICITUD_ARTICULO":     ArTablaCampo(35, 1) = "NU_AUTO_ARTI_ACPT"
ArTablaCampo(36, 0) = "IN_CODIGOBAR":              ArTablaCampo(36, 1) = "NU_AUTO_ARTI_COBA"
ArTablaCampo(37, 0) = "IN_KARDUSOS_TEMP":          ArTablaCampo(37, 1) = "NU_AUTO_ARTI_KARD"
ArTablaCampo(38, 0) = "IN_LIMITES_BODE_ARTI":      ArTablaCampo(38, 1) = "NU_AUTO_ARTI_LIBA"
ArTablaCampo(39, 0) = "IN_R_BODE_ARTI_UNDVENTA":   ArTablaCampo(39, 1) = "NU_AUTO_ARTI_RBAU"
ArTablaCampo(40, 0) = "IN_R_LIST_ARTI":            ArTablaCampo(40, 1) = "NU_AUTO_ARTI_RLIA"
ArTablaCampo(41, 0) = "IN_ROTAREPORTE":            ArTablaCampo(41, 1) = "NU_AUTO_ARTI_KARD"
ArTablaCampo(42, 0) = "IN_VENTAREPORTE":           ArTablaCampo(42, 1) = "NU_AUTO_ARTI_KARD"
ArTablaCampo(43, 0) = "R_ARTI_MEVE_MEBI":          ArTablaCampo(43, 1) = "NU_AUTO_ARTI_RAMM"
ReDim Arr(0)
   For InTablaCampo = 0 To UBound(ArTablaCampo)
      If ExisteTABLA(ArTablaCampo(InTablaCampo, 0)) Then
         If ExisteCAMPO(ArTablaCampo(InTablaCampo, 0), ArTablaCampo(InTablaCampo, 1)) Then
            If InTablaCampo <= 29 Then
               Condicion = ArTablaCampo(InTablaCampo, 1) & "='" & Cambiar_Comas_Comillas(TxtArtic(0)) & Comi
               StCampo = " TOP 1 " & ArTablaCampo(InTablaCampo, 1)
               Result = LoadData(ArTablaCampo(InTablaCampo, 0), StCampo, Condicion, Arr)
            Else
               Condicion = " CD_CODI_ARTI= '" & Cambiar_Comas_Comillas(TxtArtic(0)) & Comi
               Result = LoadData("ARTICULO", "TOP 1 NU_AUTO_ARTI", Condicion, Arr)
               If Result <> FAIL And Encontro Then
                  Condicion = ArTablaCampo(InTablaCampo, 1) & "='" & (Arr(0)) & Comi
                  StCampo = " TOP 1 " & ArTablaCampo(InTablaCampo, 1)
                  Result = LoadData(ArTablaCampo(InTablaCampo, 0), StCampo, Condicion, Arr)
               End If
            End If
            If Result <> FAIL And Encontro Then
                  Call Mensaje1("Este Articulo no se puede eliminar porque esta asociado a un movimiento", 1)
                  Call MouseNorm
                  ValidaArtAsoMovimiento = True
                  Exit For
               End If
         End If
      End If
   Next InTablaCampo

End Function
'******************CAMBIOS GAVL  R3201
'---------------------------------------------------------------------------------------
' Procedure : ChkDispositivo_Click
' DateTime  : 25/02/2011 13:52
' Author    : gabriel_vallejo
' Purpose   :chekbox de dispositivo
'---------------------------------------------------------------------------------------
' ' GAVL R3201 / T5101 25/02/2011
Private Sub ChkDispositivo_Click()
    If ChkDispositivo.value = 0 Then
        TxtVUtil.Enabled = False
        TxtClasificacion.Enabled = False
        CmdSeleccion(0).Enabled = False
        TxtClasDis.Enabled = False
        TxtVUtil.Text = NUL$
        TxtClasificacion.Text = NUL$
        TxtClasDis.Text = NUL$
        ChkDispositivo.TabIndex = 24 'GAVL T5268
     Else
        TxtVUtil.Enabled = True
        TxtClasificacion.Enabled = True
        CmdSeleccion(0).Enabled = True
        ChkDispositivo.TabIndex = 19 'GAVL T5268
        
    End If
End Sub

'INICIO GAVL R3201 / T5101
'---------------------------------------------------------------------------------------
' Procedure : CmdSeleccion_Click
' DateTime  : 25/02/2011 13:53
' Author    : gabriel_vallejo
' Purpose   : seleccion del la informacion tanto para principio activo, clasificacion_ riesgo y presentacion comercial
'---------------------------------------------------------------------------------------
'' GAVL R3201 / T5101 25/02/2011
Private Sub CmdSeleccion_Click(Index As Integer)
Dim Codigos As String
Select Case Index
    Case 0: Codigos = Seleccion("CLASIFICACION_RIESGO", "TX_DESC_CLRI", "TX_CODIGO_CLRI, TX_DESC_CLRI", "CLASIFICACI�N RIESGO", NUL$)
                   If Codigo <> NUL$ Then TxtClasificacion.Text = Codigos
                   TxtClasificacion.SetFocus
                   Call TxtClasificacion_LostFocus
    Case 1: Codigos = Seleccion("PRINCIPIO_ACTIVO", "TX_DESC_PRAC", " TX_CODIGO_PRAC, TX_DESC_PRAC", "PRINCIPIO ACTIVO", NUL$)
                   If Codigo <> NUL$ Then TxtPrincipio.Text = Codigos
                   TxtPrincipio.SetFocus
                    Call TxtPrincipio_LostFocus
                    CboVENCE.SetFocus 'GAVL T5268
    Case 2: Codigos = Seleccion("PRESENTACION_COMERCIAL", "TX_DESC_PRCO", "TX_CODI_PRCO, TX_DESC_PRCO", "PRESENTACION COMERCIAL", NUL$)
                   If Codigo <> NUL$ Then TxtPresCom.Text = Codigos
                   TxtPresCom.SetFocus
                   Call TxtPresCom_LostFocus
                    CboPARA.SetFocus 'GAVL T5268
End Select
End Sub
Private Sub TxtVUtil_KeyPress(KeyAscii As Integer)
  Call ValKeyNum(KeyAscii)
  Call Cambiar_Enter(KeyAscii)
End Sub
Private Sub ChkDispositivo_KeyPress(KeyAscii As Integer)
   'Call Cambiar_Enter(KeyAscii)
   'INICIO GAVL T5268
   If ChkDispositivo.TabIndex = 24 Then
    SSTArti.Tab = 1
    Call Cambiar_Enter(KeyAscii)
   Else
    Call Cambiar_Enter(KeyAscii)
   End If
   'FIN GAVL T5268
End Sub
Private Sub TxtClasificacion_KeyPress(KeyAscii As Integer)
   Call ValKeyAlfaNum(KeyAscii)
   'SSTArti.Tab = 1 'GAVL T5268 SE HACE CAMBIO
   Call Cambiar_Enter(KeyAscii)
End Sub
Private Sub TxtPresCom_KeyPress(KeyAscii As Integer)
    Call ValKeyAlfaNum(KeyAscii)
   Call Cambiar_Enter(KeyAscii)
End Sub
Private Sub TxtDePre_KeyPress(KeyAscii As Integer)
    Call ValKeyAlfaNum(KeyAscii)
    Call Cambiar_Enter(KeyAscii)
End Sub

Private Sub TxtPrincipio_KeyPress(KeyAscii As Integer)
    Call ValKeyAlfaNum(KeyAscii)
    Call Cambiar_Enter(KeyAscii)
End Sub


Private Sub TxtClasificacion_LostFocus()
ReDim Arr(0)

   Condicion = "TX_CODIGO_CLRI=" & Comi & (TxtClasificacion.Text) & Comi
   Result = LoadData("CLASIFICACION_RIESGO", "TX_DESC_CLRI", Condicion, Arr())
   If (Result <> False) Then
     If Encontro Then
        TxtClasDis.Text = Arr(0)
     Else
        TxtClasDis.Text = NUL$
        TxtClasificacion.Text = NUL$
     End If
   End If
End Sub

Private Sub TxtPrincipio_LostFocus()
ReDim Arr(0)
   Condicion = "TX_CODIGO_PRAC=" & Comi & (TxtPrincipio.Text) & Comi
   Result = LoadData("PRINCIPIO_ACTIVO", " TX_DESC_PRAC", Condicion, Arr())
   If (Result <> False) Then
     If Encontro Then
        TxtPrinA.Text = Arr(0)
     Else
        TxtPrinA.Text = NUL$
        TxtPrincipio.Text = NUL$
     End If
   End If
End Sub
Private Sub TxtPresCom_LostFocus()
ReDim Arr(0)
    Condicion = "TX_CODI_PRCO=" & Comi & TxtPresCom.Text & Comi
    Result = LoadData("PRESENTACION_COMERCIAL", "TX_DESC_PRCO", Condicion, Arr())
     If (Result <> False) Then
        If Encontro Then
         TxtDePre.Text = Arr(0)
        Else
          TxtPresCom.Text = NUL$
          TxtDePre.Text = NUL$
        End If
     End If
End Sub
'FIN  GAVL R3201 / T5101


