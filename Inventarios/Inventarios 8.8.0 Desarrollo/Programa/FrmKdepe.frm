VERSION 5.00
Object = "{00025600-0000-0000-C000-000000000046}#5.2#0"; "CRYSTL32.OCX"
Object = "{C932BA88-4374-101B-A56C-00AA003668DC}#1.1#0"; "MSMASK32.OCX"
Object = "{0BA686C6-F7D3-101A-993E-0000C0EF6F5E}#1.0#0"; "THREED32.OCX"
Begin VB.Form FrmKdepe 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "K�rdex Dependencias"
   ClientHeight    =   5490
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   3600
   Icon            =   "FrmKdepe.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   ScaleHeight     =   5490
   ScaleWidth      =   3600
   Begin VB.Frame FraDependencias 
      Caption         =   "Rango de Dependencias"
      Height          =   1335
      Left            =   120
      TabIndex        =   0
      Top             =   0
      Width           =   3255
      Begin VB.TextBox TxtRango 
         Height          =   285
         Index           =   0
         Left            =   1080
         MaxLength       =   11
         TabIndex        =   1
         Text            =   "0"
         Top             =   360
         Width           =   1335
      End
      Begin VB.TextBox TxtRango 
         Height          =   285
         Index           =   1
         Left            =   1080
         MaxLength       =   11
         TabIndex        =   3
         Text            =   "ZZZZZZZZZZZ"
         Top             =   840
         Width           =   1335
      End
      Begin Threed.SSCommand CmdSelec 
         Height          =   375
         Index           =   1
         Left            =   2280
         TabIndex        =   4
         ToolTipText     =   "Selecci�n de Dependencias"
         Top             =   720
         Width           =   615
         _Version        =   65536
         _ExtentX        =   1085
         _ExtentY        =   661
         _StockProps     =   78
         BevelWidth      =   0
         RoundedCorners  =   0   'False
         Outline         =   0   'False
         Picture         =   "FrmKdepe.frx":058A
      End
      Begin Threed.SSCommand CmdSelec 
         Height          =   375
         Index           =   0
         Left            =   2280
         TabIndex        =   2
         ToolTipText     =   "Selecci�n de Dependencias"
         Top             =   240
         Width           =   615
         _Version        =   65536
         _ExtentX        =   1085
         _ExtentY        =   661
         _StockProps     =   78
         BevelWidth      =   0
         RoundedCorners  =   0   'False
         Outline         =   0   'False
         Picture         =   "FrmKdepe.frx":0F3C
      End
      Begin VB.Label Label2 
         AutoSize        =   -1  'True
         Caption         =   "Final : "
         Height          =   195
         Left            =   480
         TabIndex        =   23
         Top             =   840
         Width           =   465
      End
      Begin VB.Label Label1 
         AutoSize        =   -1  'True
         Caption         =   "Inicial : "
         Height          =   195
         Left            =   480
         TabIndex        =   22
         Top             =   360
         Width           =   540
      End
   End
   Begin VB.Frame FraFecha 
      Caption         =   "Fecha    (dd/mm/aaaa)"
      Height          =   1335
      Left            =   120
      TabIndex        =   10
      Top             =   2880
      Width           =   3255
      Begin MSMask.MaskEdBox MskFecha 
         Height          =   375
         Index           =   0
         Left            =   1440
         TabIndex        =   11
         Top             =   360
         Width           =   1095
         _ExtentX        =   1931
         _ExtentY        =   661
         _Version        =   393216
         MaxLength       =   10
         Mask            =   "##/##/####"
         PromptChar      =   "_"
      End
      Begin MSMask.MaskEdBox MskFecha 
         Height          =   375
         Index           =   1
         Left            =   1440
         TabIndex        =   12
         Top             =   840
         Width           =   1095
         _ExtentX        =   1931
         _ExtentY        =   661
         _Version        =   393216
         MaxLength       =   10
         Mask            =   "##/##/####"
         PromptChar      =   "_"
      End
      Begin VB.Label LblFeFinal 
         Caption         =   "Final :"
         Height          =   255
         Left            =   720
         TabIndex        =   21
         Top             =   840
         Width           =   495
      End
      Begin VB.Label LblFeIni 
         Caption         =   "Inicial :"
         Height          =   255
         Left            =   720
         TabIndex        =   20
         Top             =   360
         Width           =   495
      End
   End
   Begin VB.Frame FraArticulos 
      Caption         =   "Rango de Art�culos"
      Height          =   1335
      Left            =   120
      TabIndex        =   5
      Top             =   1440
      Width           =   3255
      Begin VB.TextBox TxtRango 
         Height          =   285
         Index           =   3
         Left            =   720
         MaxLength       =   16
         TabIndex        =   8
         Text            =   "ZZZZZZZZZZZZZZZZ"
         Top             =   840
         Width           =   1815
      End
      Begin VB.TextBox TxtRango 
         Height          =   285
         Index           =   2
         Left            =   720
         MaxLength       =   16
         TabIndex        =   6
         Text            =   "0"
         Top             =   360
         Width           =   1815
      End
      Begin Threed.SSCommand CmdSelec 
         Height          =   375
         Index           =   2
         Left            =   2400
         TabIndex        =   7
         ToolTipText     =   "Selecci�n de Art�culos"
         Top             =   240
         Width           =   615
         _Version        =   65536
         _ExtentX        =   1085
         _ExtentY        =   661
         _StockProps     =   78
         BevelWidth      =   0
         RoundedCorners  =   0   'False
         Outline         =   0   'False
         Picture         =   "FrmKdepe.frx":18EE
      End
      Begin Threed.SSCommand CmdSelec 
         Height          =   375
         Index           =   3
         Left            =   2400
         TabIndex        =   9
         ToolTipText     =   "Selecci�n de Art�culos"
         Top             =   720
         Width           =   615
         _Version        =   65536
         _ExtentX        =   1085
         _ExtentY        =   661
         _StockProps     =   78
         BevelWidth      =   0
         RoundedCorners  =   0   'False
         Outline         =   0   'False
         Picture         =   "FrmKdepe.frx":22A0
      End
      Begin VB.Label LblInicio 
         AutoSize        =   -1  'True
         Caption         =   "Inicial : "
         Height          =   195
         Left            =   120
         TabIndex        =   19
         Top             =   360
         Width           =   540
      End
      Begin VB.Label LblHasta 
         AutoSize        =   -1  'True
         Caption         =   "Final : "
         Height          =   195
         Left            =   120
         TabIndex        =   18
         Top             =   840
         Width           =   465
      End
   End
   Begin VB.Frame FraOpciones 
      Caption         =   "Opciones"
      Height          =   855
      Left            =   120
      TabIndex        =   13
      Top             =   3360
      Visible         =   0   'False
      Width           =   3015
      Begin VB.CheckBox ChkOpcion 
         Caption         =   "Detallado"
         Height          =   255
         Index           =   0
         Left            =   480
         TabIndex        =   14
         Top             =   360
         Value           =   1  'Checked
         Width           =   1095
      End
      Begin VB.CheckBox ChkOpcion 
         Caption         =   "Valorizado"
         Height          =   255
         Index           =   1
         Left            =   1920
         TabIndex        =   15
         Top             =   360
         Width           =   1095
      End
   End
   Begin Crystal.CrystalReport Crys_Listar 
      Left            =   0
      Top             =   1920
      _ExtentX        =   741
      _ExtentY        =   741
      _Version        =   348160
      WindowLeft      =   20
      WindowWidth     =   750
      WindowHeight    =   450
      WindowControlBox=   -1  'True
      WindowMaxButton =   -1  'True
      WindowMinButton =   -1  'True
      WindowControls  =   -1  'True
      PrintFileLinesPerPage=   60
      WindowShowCloseBtn=   -1  'True
      WindowShowSearchBtn=   -1  'True
      WindowShowPrintSetupBtn=   -1  'True
      WindowShowRefreshBtn=   -1  'True
   End
   Begin Threed.SSCommand SCmd_Options 
      Height          =   375
      Index           =   3
      Left            =   120
      TabIndex        =   16
      Top             =   5880
      Visible         =   0   'False
      Width           =   975
      _Version        =   65536
      _ExtentX        =   1720
      _ExtentY        =   661
      _StockProps     =   78
   End
   Begin Threed.SSCommand SCmd_Options 
      Height          =   375
      Index           =   4
      Left            =   1320
      TabIndex        =   17
      Top             =   5880
      Visible         =   0   'False
      Width           =   975
      _Version        =   65536
      _ExtentX        =   1720
      _ExtentY        =   661
      _StockProps     =   78
   End
   Begin Threed.SSCommand SCmd_Options 
      Height          =   735
      Index           =   1
      Left            =   1320
      TabIndex        =   24
      Top             =   4680
      Width           =   855
      _Version        =   65536
      _ExtentX        =   1508
      _ExtentY        =   1296
      _StockProps     =   78
      Caption         =   "&IMPRIMIR"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   6
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Picture         =   "FrmKdepe.frx":2C52
   End
   Begin Threed.SSCommand SCmd_Options 
      Height          =   735
      Index           =   2
      Left            =   2400
      TabIndex        =   25
      Top             =   4680
      Width           =   855
      _Version        =   65536
      _ExtentX        =   1508
      _ExtentY        =   1296
      _StockProps     =   78
      Caption         =   "&SALIR"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   6
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Picture         =   "FrmKdepe.frx":331C
   End
   Begin Threed.SSCommand SCmd_Options 
      Height          =   735
      Index           =   0
      Left            =   240
      TabIndex        =   26
      Top             =   4680
      Width           =   855
      _Version        =   65536
      _ExtentX        =   1508
      _ExtentY        =   1296
      _StockProps     =   78
      Caption         =   "&PANTALLA"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   6
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Picture         =   "FrmKdepe.frx":39E6
   End
   Begin VB.Label lblReg 
      Height          =   255
      Left            =   120
      TabIndex        =   27
      Top             =   4320
      Width           =   3255
   End
End
Attribute VB_Name = "FrmKdepe"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Dim Cantidad As Double
Dim CantiK As Double
Dim Costo As Double
Dim CostoK As Double
Dim Rstdatos As ADODB.Recordset
Public OpcCod        As String   'Opci�n de seguridad

Private Sub Form_Load()
    Dim ElClnt As New ElTercero
    ElClnt.IniXNit
 Call CenterForm(MDI_Inventarios, Me)
' Call Leer_Permisos("03006", SCmd_Options(3), SCmd_Options(3), SCmd_Options(0))
 Crys_Listar.Formulas(0) = "ENTIDAD='" & ElClnt.Nombre & Comi
 Crys_Listar.Formulas(1) = "NIT= 'NIT. " & ElClnt.Nit & Comi
 Crys_Listar.Formulas(2) = "HORA= 'Hora: " & Format(Now, "hh:mm") & Comi
 Crys_Listar.Formulas(3) = "USUARIO = 'Usuario:" & UserId & Comi
' Crys_Listar.Connect = conCrys
 SCmd_Options(1).Enabled = SCmd_Options(0).Enabled
 MskFecha(0) = Hoy
 MskFecha(1) = Hoy
End Sub

Private Sub TxtRango_GotFocus(Index As Integer)
    TxtRango(Index).SelStart = 0
    TxtRango(Index).SelLength = Len(TxtRango(Index).Text)
End Sub
Private Sub TxtRango_KeyPress(Index As Integer, KeyAscii As Integer)
'    Dim i As Integer       'DEPURACION DE CODIGO
    Call ValKeyAlfaNum(KeyAscii)
    If KeyAscii = 13 Then
     If Index = 3 Then
      MskFecha(0).SetFocus
     Else
      TxtRango(Index + 1).SetFocus
     End If
    End If
End Sub
Private Sub TxtRango_KeyDown(Index As Integer, KeyCode As Integer, Shift As Integer)
 Select Case Index
    Case 0
           If KeyCode = 40 Then
              TxtRango(1).SetFocus
           End If
    Case 1, 2
           If KeyCode = 38 Then
              TxtRango(Index - 1).SetFocus
           End If
           If KeyCode = 40 Then
              TxtRango(Index + 1).SetFocus
           End If
    Case 3
           If KeyCode = 38 Then
              TxtRango(Index - 1).SetFocus
           End If
           If KeyCode = 40 Then
              MskFecha(0).SetFocus
           End If
 End Select
End Sub
Private Sub MskFecha_GotFocus(Index As Integer)
    MskFecha(Index).SelStart = 0
    MskFecha(Index).SelLength = Len(MskFecha(Index).Text)
End Sub
Private Sub MskFecha_KeyPress(Index As Integer, KeyAscii As Integer)
    Call ValKeyFecha(KeyAscii, MskFecha(Index))
    Call Cambiar_Enter(KeyAscii)
End Sub
Private Sub MskFecha_LostFocus(Index As Integer)
   Call ValFecha(MskFecha(Index), 0)
End Sub
Private Sub MskFecha_KeyDown(Index As Integer, KeyCode As Integer, Shift As Integer)
 If Index = 0 Then
   If KeyCode = 38 Then
      TxtRango(3).SetFocus
   End If
   If KeyCode = 40 Then
      MskFecha(1).SetFocus
   End If
 Else
   If KeyCode = 38 Then
      MskFecha(0).SetFocus
   End If
   If KeyCode = 40 Then
   '   If ChkOpcion(0).Enabled Then ChkOpcion(0).SetFocus
   End If
 End If
End Sub
Private Sub ChkOpcion_KeyPress(Index As Integer, KeyAscii As Integer)
    Call Cambiar_Enter(KeyAscii)
End Sub
Private Sub CmdSelec_Click(Index As Integer)
    Codigo = NUL$
    Select Case Index
        Case 0, 1: Codigo = Seleccion("CENTRO_COSTO", "NO_NOMB_CECO", "CD_CODI_CECO,NO_NOMB_CECO", "DEPENDENCIAS", NUL$)
        Case 2, 3: Codigo = Seleccion("ARTICULO", "NO_NOMB_ARTI", "CD_CODI_ARTI,NO_NOMB_ARTI", "ARTICULOS", NUL$)
    End Select
    If Codigo <> NUL$ Then TxtRango(Index) = Codigo
    TxtRango(Index).SetFocus
End Sub
Private Sub SCmd_Options_Click(Index As Integer)
    Screen.MousePointer = 11
    Select Case Index
        '''True = pantalla
        '''False = impresora
        Case 0: Call Informe(True)
        Case 1: Call Informe(False)
        Case 2: Unload Me
    End Select
    Screen.MousePointer = 0
End Sub

Private Sub Informe(ByVal pantalla As Boolean)
Dim ArrTemp() As Variant
Dim nomrep As String
  Dim algo
  If TxtRango(0) > TxtRango(1) Or TxtRango(1) = "" Then
     Call Mensaje1("Revise rango de dependencias", 3)
     Exit Sub
  End If
  If TxtRango(2) > TxtRango(3) Or TxtRango(3) = "" Then
     Call Mensaje1("Revise rango de art�culos", 3)
     Exit Sub
  End If
  algo = DateDiff("d", MskFecha(0), MskFecha(1))
  If CLng(algo) < 0 Then
     Call Mensaje1("Fecha Inicial Mayor que Fecha Final", 3)
     Exit Sub
  End If
  'validar que el mes de donde se van a tomar los saldos iniciales este cerrado
  ReDim ArrTemp(0)
  Result = LoadData("PARAMETROS_INVE", "FE_CIER_APLI", NUL$, ArrTemp())
  If Result <> FAIL And Encontro Then
     If (CLng(Format(MskFecha(0), "yyyymm")) - 1) > CLng(Format(ArrTemp(0), "yyyymm")) Then
        Call Mensaje1("No se puede generar el listado porque " & MonthName(CLng(Format(MskFecha(0), "mm")) - 1) & " de " & Format(MskFecha(0), "yyyy") & " no se ha cerrado", 1)
        Call MouseNorm
        Exit Sub
     End If
  Else
     Call Mensaje1("No se pudo obtener la fecha de cierre", 1)
     Call MouseNorm
     Exit Sub
  End If

  
  ''Env�a rango de fechas al informe
  Crys_Listar.Formulas(4) = "FECHAI= '" & MskFecha(0) & Comi
  Crys_Listar.Formulas(5) = "FECHAF= '" & MskFecha(1) & Comi
  ''''''''''''''''''''''''''''''''''
  If ChkOpcion(0).Value = 1 Then 'detallado
    If ChkOpcion(1).Value = 1 Then 'valorizado
      Crys_Listar.WindowTitle = "K�rdex Dependencias Detallado Valorizado"
      Crys_Listar.ReportFileName = DirTrab + "kdepeV.RPT"
      nomrep = "kdepeV.RPT"
    Else 'no Valorizado
      Crys_Listar.WindowTitle = "K�rdex Dependencias Detallado"
      Crys_Listar.ReportFileName = DirTrab + "kdepe.RPT"
      nomrep = "kdepe.RPT"
    End If
  Else 'no detallado
    If ChkOpcion(1).Value = 1 Then 'valorizado
      Crys_Listar.WindowTitle = "K�rdex Dependencias Valorizado"
      Crys_Listar.ReportFileName = DirTrab + "kdepeV2.RPT"
      nomrep = "kdepeV2.RPT"
    Else 'no Valorizado
      Crys_Listar.WindowTitle = "K�rdex Dependencias"
      Crys_Listar.ReportFileName = DirTrab + "kdepe2.RPT"
      nomrep = "kdepe2.RPT"
    End If
  End If
  On Error GoTo control
  Crys_Listar.SelectionFormula = "{KARDEX_ARTI.CD_ARTI_KARD} in '" & TxtRango(2) & "' to '" & TxtRango(3) & Comi & _
  " and {KARDEX_ARTI.CD_DEPE_KARD} in '" & TxtRango(0) & "' to '" & TxtRango(1) & Comi
  If SCmd_Options(4).Enabled = False Then
     Call Mensaje1("Permiso Denegado", 3)
  Else
     Crea_Temporales ''Crea temporal para impresi�n
     If pantalla = True Then
        Crys_Listar.Destination = crptToWindow
     Else
        Crys_Listar.Destination = crptToPrinter
     End If
     Crys_Listar.Action = 1
  End If
  Exit Sub
control:
  Call Mensaje1(ERR.Number & ": " & ERR.Description & "  Nombre del Reporte : " & nomrep, 1)
End Sub
Private Sub Crea_Temporales()
Dim Fuc As String
Dim Arr(0)
  Fuc = "01/" & Format(MskFecha(0), "mm") & "/" & Format(MskFecha(0), "yyyy")
  Fuc = DateAdd("d", -1, Fuc)
  
  Valores = " CT_CANT_SALD=0,  VL_COST_SALD=0"
  Result = DoUpdate("SAL_ANT_DEP", Valores, NUL$)
  Condicion = "CD_ARTI_KARD >= '" & TxtRango(2) & Comi
  Condicion = Condicion & " And CD_ARTI_KARD <= '" & TxtRango(3) & Comi
  Condicion = Condicion & " And CD_DEPE_KARD >= '" & TxtRango(0) & Comi
  Condicion = Condicion & " And CD_DEPE_KARD <= '" & TxtRango(1) & Comi
  Condicion = Condicion & " And ID_ESTA_KARD <> " & "2"
  'condicion = condicion & " And FE_FECH_KARD < " & fecha(MskFecha(0))
  'condicion = condicion & " And FE_FECH_KARD >= " & fecha(MskFecha(0))
  'condicion = condicion & " And FE_FECH_KARD <= " & fecha(MskFecha(1))
  Condicion = Condicion & " And CD_ORDO_KARD <> " & "1"
  Condicion = Condicion & " And CD_ORDO_KARD <> " & "7"
  Condicion = Condicion & " ORDER  BY CD_DEPE_KARD, CD_ARTI_KARD"
  Set Rstdatos = New ADODB.Recordset
  Call SelectRST("KARDEX_ARTI", "DISTINCT CD_DEPE_KARD, CD_ARTI_KARD", Condicion, Rstdatos)
  If (Result <> False) Then
    If Not Rstdatos.EOF Then
       Rstdatos.MoveFirst
       Do While Not Rstdatos.EOF
         LblReg = "Leyendo Art�culo: " & Rstdatos.Fields(1) & " Depend: " & Rstdatos.Fields(0)
         DoEvents
         
         Cantidad = Saldo_Inicial_Dep(Rstdatos.Fields(1), Rstdatos.Fields(0), Fuc, Costo)
         CantiK = Saldo_Kardex_Dep(Rstdatos.Fields(1), Rstdatos.Fields(0), MskFecha(0), CostoK)
         Cantidad = Cantidad + CantiK
         Costo = Costo + CostoK
         If Cantidad = 0 Then Costo = 0
         
         Valores = " CT_CANT_SALD=" & Cantidad & Coma
         Valores = Valores & " VL_COST_SALD=" & Format(Costo, "############0.##00")
         Condicion = "CD_ARTI_SALD = '" & Rstdatos.Fields(1) & Comi
         Condicion = Condicion & "And CD_DEPE_SALD = '" & Rstdatos.Fields(0) & Comi
         Result = LoadData("SAL_ANT_DEP", "CD_DEPE_SALD", Condicion, Arr())
         If Arr(0) = NUL$ Then
            Valores = "CD_DEPE_SALD=" & Comi & Rstdatos.Fields(0) & Comi & Coma & _
                      "CD_ARTI_SALD=" & Comi & Rstdatos.Fields(1) & Comi & Coma & _
                      "CT_CANT_SALD=" & Cantidad & Coma & _
                      "VL_COST_SALD=" & Format(Costo, "###0.##00")
            Result = DoInsertSQL("SAL_ANT_DEP", Valores)
         Else
            Result = DoUpdate("SAL_ANT_DEP", Valores, Condicion)
         End If
         Rstdatos.MoveNext
       Loop
    End If
  End If
  LblReg = NUL$
  Rstdatos.Close
End Sub
