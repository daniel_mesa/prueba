VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "mscomctl.ocx"
Object = "{C932BA88-4374-101B-A56C-00AA003668DC}#1.1#0"; "MSMASK32.OCX"
Object = "{0BA686C6-F7D3-101A-993E-0000C0EF6F5E}#1.0#0"; "THREED32.OCX"
Begin VB.Form frmPrmCDBARRA 
   BorderStyle     =   1  'Fixed Single
   Caption         =   " "
   ClientHeight    =   7200
   ClientLeft      =   45
   ClientTop       =   435
   ClientWidth     =   10725
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   7200
   ScaleWidth      =   10725
   Begin MSComctlLib.ListView lstArticulos 
      Height          =   6615
      Left            =   240
      TabIndex        =   9
      Top             =   360
      Width           =   5055
      _ExtentX        =   8916
      _ExtentY        =   11668
      View            =   3
      LabelEdit       =   1
      LabelWrap       =   -1  'True
      HideSelection   =   0   'False
      AllowReorder    =   -1  'True
      FullRowSelect   =   -1  'True
      GridLines       =   -1  'True
      _Version        =   393217
      ForeColor       =   -2147483640
      BackColor       =   -2147483643
      BorderStyle     =   1
      Appearance      =   1
      NumItems        =   0
   End
   Begin VB.Frame Frame1 
      Caption         =   "Informaci�n:"
      Height          =   6615
      Left            =   5520
      TabIndex        =   0
      Top             =   360
      Width           =   5055
      Begin VB.TextBox txtCIAL 
         Height          =   735
         Left            =   1200
         TabIndex        =   20
         Text            =   "txtCOMERCIAL"
         Top             =   2520
         Width           =   3615
      End
      Begin VB.TextBox txtREGI 
         Height          =   375
         Left            =   1200
         TabIndex        =   18
         Text            =   "txtREGITRO"
         Top             =   2040
         Width           =   3615
      End
      Begin MSMask.MaskEdBox txtCDBarra 
         Height          =   375
         Left            =   1200
         TabIndex        =   17
         Top             =   1560
         Visible         =   0   'False
         Width           =   1695
         _ExtentX        =   2990
         _ExtentY        =   661
         _Version        =   393216
         PromptInclude   =   0   'False
         MaxLength       =   13
         PromptChar      =   "_"
      End
      Begin VB.TextBox txtNOMB 
         Height          =   735
         Left            =   1200
         TabIndex        =   8
         Text            =   "txtNOMB"
         Top             =   240
         Width           =   3615
      End
      Begin VB.TextBox txtIDEN 
         Height          =   375
         Left            =   1200
         TabIndex        =   7
         Text            =   "txtIDEN"
         Top             =   1080
         Width           =   1695
      End
      Begin VB.TextBox txtGRUP 
         Height          =   375
         Left            =   720
         TabIndex        =   6
         Text            =   "txtGRUP"
         Top             =   1560
         Width           =   1695
      End
      Begin VB.TextBox txtUSOS 
         Height          =   375
         Left            =   3120
         TabIndex        =   5
         Text            =   "txtUSOS"
         Top             =   1560
         Width           =   1695
      End
      Begin MSComctlLib.ListView lstBarras 
         Height          =   1935
         Left            =   120
         TabIndex        =   10
         Top             =   3480
         Width           =   4815
         _ExtentX        =   8493
         _ExtentY        =   3413
         View            =   3
         LabelEdit       =   1
         LabelWrap       =   -1  'True
         HideSelection   =   -1  'True
         FullRowSelect   =   -1  'True
         GridLines       =   -1  'True
         _Version        =   393217
         ForeColor       =   -2147483640
         BackColor       =   -2147483643
         BorderStyle     =   1
         Appearance      =   1
         NumItems        =   0
      End
      Begin MSComctlLib.ListView lstTerceros 
         Height          =   1935
         Left            =   120
         TabIndex        =   11
         Top             =   3720
         Visible         =   0   'False
         Width           =   4815
         _ExtentX        =   8493
         _ExtentY        =   3413
         View            =   3
         LabelEdit       =   1
         MultiSelect     =   -1  'True
         LabelWrap       =   -1  'True
         HideSelection   =   -1  'True
         FullRowSelect   =   -1  'True
         GridLines       =   -1  'True
         _Version        =   393217
         ForeColor       =   -2147483640
         BackColor       =   -2147483643
         BorderStyle     =   1
         Appearance      =   1
         NumItems        =   0
      End
      Begin Threed.SSCommand btnCan 
         Height          =   735
         Left            =   3960
         TabIndex        =   13
         Top             =   5760
         Width           =   855
         _Version        =   65536
         _ExtentX        =   1508
         _ExtentY        =   1296
         _StockProps     =   78
         Caption         =   "&Cancelar"
         Enabled         =   0   'False
         Picture         =   "frmPrmCDBARRA.frx":0000
      End
      Begin Threed.SSCommand btnAdd 
         Height          =   735
         Left            =   360
         TabIndex        =   14
         Top             =   5760
         Width           =   855
         _Version        =   65536
         _ExtentX        =   1508
         _ExtentY        =   1296
         _StockProps     =   78
         Caption         =   "&Adicionar"
         Enabled         =   0   'False
         Picture         =   "frmPrmCDBARRA.frx":031A
      End
      Begin Threed.SSCommand btnDel 
         Height          =   735
         Left            =   1560
         TabIndex        =   15
         Top             =   5760
         Width           =   855
         _Version        =   65536
         _ExtentX        =   1508
         _ExtentY        =   1296
         _StockProps     =   78
         Caption         =   "&Borrar"
         Enabled         =   0   'False
         Picture         =   "frmPrmCDBARRA.frx":0634
      End
      Begin Threed.SSCommand btnCha 
         Height          =   735
         Left            =   2760
         TabIndex        =   16
         Top             =   5760
         Width           =   855
         _Version        =   65536
         _ExtentX        =   1508
         _ExtentY        =   1296
         _StockProps     =   78
         Caption         =   "&Modificar"
         Enabled         =   0   'False
         Picture         =   "frmPrmCDBARRA.frx":094E
      End
      Begin VB.Label Label7 
         Alignment       =   1  'Right Justify
         Caption         =   "Comercial:"
         Height          =   255
         Left            =   240
         TabIndex        =   21
         Top             =   2520
         Visible         =   0   'False
         Width           =   750
      End
      Begin VB.Label Label6 
         Alignment       =   1  'Right Justify
         Caption         =   "Registro:"
         Height          =   255
         Left            =   240
         TabIndex        =   19
         Top             =   2040
         Visible         =   0   'False
         Width           =   750
      End
      Begin VB.Label Label5 
         Alignment       =   1  'Right Justify
         Caption         =   "CD Barra:"
         Height          =   255
         Left            =   240
         TabIndex        =   12
         Top             =   1560
         Visible         =   0   'False
         Width           =   750
      End
      Begin VB.Label Label4 
         Alignment       =   1  'Right Justify
         Caption         =   "Usos:"
         Height          =   255
         Left            =   2520
         TabIndex        =   4
         Top             =   1560
         Width           =   495
      End
      Begin VB.Label Label2 
         Alignment       =   1  'Right Justify
         Caption         =   "Nombre:"
         Height          =   255
         Left            =   240
         TabIndex        =   3
         Top             =   240
         Width           =   750
      End
      Begin VB.Label Label1 
         Alignment       =   1  'Right Justify
         Caption         =   "C�digo:"
         Height          =   255
         Left            =   240
         TabIndex        =   2
         Top             =   1080
         Width           =   750
      End
      Begin VB.Label Label3 
         Alignment       =   1  'Right Justify
         Caption         =   "Grupo:"
         Height          =   255
         Left            =   120
         TabIndex        =   1
         Top             =   1560
         Width           =   495
      End
   End
End
Attribute VB_Name = "frmPrmCDBARRA"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Dim ArrBRR() As Variant
Dim Articulo As UnArticulo
Dim erda As String
Private vClaveArticuloActual As String
Public OpcCod        As String   'Opci�n de seguridad
'NU_AUTO_UNVE, TX_CODI_UNVE, TX_NOMB_UNVE, NU_MULT_UNVE, NU_DIVI_UNVE

Private Sub btnAdd_Click()
    Dim vUno As MSComctlLib.ListItem
    If Me.btnAdd.Caption = "&Adicionar" Then
        Me.lstBarras.Visible = False
        Me.lstTerceros.Top = Me.lstBarras.Top
        Me.lstTerceros.Left = Me.lstBarras.Left
        Me.lstTerceros.Visible = True
        Me.btnDel.Enabled = False
        Me.btnCha.Enabled = False
        Me.btnCan.Enabled = True
        Me.Label3.Visible = False
        Me.Label4.Visible = False
        Me.Label5.Visible = True
        Me.txtCDBarra.Visible = True
        Me.txtGRUP.Visible = False
        Me.txtUSOS.Visible = False
        Me.btnAdd.Caption = "&Guardar"
        Me.txtCDBarra.Text = ""
        Me.txtREGI.Text = ""
        Me.txtCIAL.Text = ""
        Me.txtREGI.Enabled = True
        Me.txtCIAL.Enabled = True
        Me.txtCDBarra.SetFocus
    Else
        If MalaInformacion(erda) Then
            Call MsgBox("Incompleta informaci�n en:" & erda, vbInformation)
        Else
'SELECT IN_CODIGOBAR.NU_AUTO_ARTI_COBA, IN_CODIGOBAR.TX_COBA_COBA, IN_CODIGOBAR.CD_CODI_TERC_COBA
'FROM IN_CODIGOBAR;
''IN_R_COBA_TERC(TX_COBA_COBA_RCBTE,CD_CODI_TERC_RCBTE)
            For Each vUno In Me.lstTerceros.ListItems
                If vUno.Selected Then
                    Valores = "TX_COBA_COBA_RCBTE='" & Trim(Me.txtCDBarra.Text) & _
                        "', CD_CODI_TERC_RCBTE='" & vUno.Tag & "'"
                    Result = DoInsertSQL("IN_R_COBA_TERC", Valores)
                End If
            Next
'            If Result = FAIL Then GoTo FALLO
'            Campos = "NU_AUTO_COBA"
'            Desde = "IN_CODIGOBAR"
'            Condi = "NU_AUTO_ARTI_COBA=" & Me.lstArticulos.SelectedItem.Tag
'            Condi = Condi & " AND TX_COBA_COBA='" & Me.txtCDBarra.Text & "'"
'            ReDim ArrBRR(0)
'            Result = LoadData("IN_CODIGOBAR", Campos, Condi, ArrBRR)
'            If Result = FAIL Then GoTo FALLO
'            Me.lstBarras.ListItems.Add , "C" & ArrBRR(0), Me.txtCDBarra.Text
'            Me.lstBarras.ListItems("C" & ArrBRR(0)).Tag = ArrBRR(0)
'            Me.lstBarras.ListItems("C" & ArrBRR(0)).ListSubItems.Add , "NIT", Me.lstTerceros.SelectedItem.Tag
'            Me.lstBarras.ListItems("C" & ArrBRR(0)).ListSubItems.Add , "NOM", Me.lstTerceros.SelectedItem.ListSubItems("NOM").Text
'        End If
'FALLO:
'        Call btnCan_Click
            Call lstArticulos_Click
        End If
    End If
End Sub

Private Sub btnCan_Click()
    Me.btnAdd.Caption = "&Adicionar"
    Me.btnCha.Caption = "&Modificar"
    Me.lstTerceros.Visible = False
    Me.lstBarras.Visible = True
    Me.btnAdd.Enabled = False
    Me.btnDel.Enabled = False
    Me.btnCha.Enabled = False
    Me.btnCan.Enabled = False
    Me.txtIDEN.Visible = True
    Me.txtNOMB.Visible = True
    Me.txtGRUP.Visible = True
    Me.txtUSOS.Visible = True
    Me.txtREGI.Enabled = False
    Me.txtCIAL.Enabled = False
    Me.txtCDBarra.Visible = False
    Me.Label1.Visible = True
    Me.Label2.Visible = True
    Me.Label3.Visible = True
    Me.Label4.Visible = True
    Me.Label5.Visible = False
    Me.lstArticulos.Enabled = True
    Me.lstArticulos.SetFocus
End Sub

Private Sub btnCha_Click()
    Dim erda As String, auto As Long
    If Me.btnCha.Caption = "&Modificar" Then
        Me.lstBarras.Visible = False
        Me.lstTerceros.Top = Me.lstBarras.Top
        Me.lstTerceros.Left = Me.lstBarras.Left
        Me.lstTerceros.Visible = True
        Me.btnAdd.Enabled = False
        Me.btnDel.Enabled = False
        Me.btnCan.Enabled = True
        Me.Label3.Visible = False
        Me.Label4.Visible = False
        Me.Label5.Visible = True
        Me.txtCDBarra.Visible = True
        Me.txtGRUP.Visible = False
        Me.txtUSOS.Visible = False
        Me.txtREGI.Enabled = True
        Me.txtCIAL.Enabled = True
        Me.btnCha.Caption = "&Guardar"
        Me.txtCDBarra.Text = Me.lstBarras.SelectedItem.Text
        Me.txtCDBarra.SetFocus
    Else
        Me.btnCha.Caption = "&Modificar"
        If MalaInformacion(erda) Then
            Call MsgBox("Incompleta informaci�n en:" & erda, vbInformation)
        Else
'SELECT IN_CODIGOBAR.NU_AUTO_ARTI_COBA, IN_CODIGOBAR.TX_COBA_COBA, IN_CODIGOBAR.CD_CODI_TERC_COBA
'FROM IN_CODIGOBAR;
''NU_AUTO_ARTI_COBA,TX_COBA_COBA,TX_REGI_COBA,TX_CIAL_COBA
'            Valores = "TX_COBA_COBA='" & Me.txtCDBarra.Text & _
'                "', CD_CODI_TERC_COBA='" & Me.lstTerceros.SelectedItem.Tag & "'"
'            Condi = "NU_AUTO_COBA=" & Me.lstBarras.SelectedItem.Tag
            Valores = "TX_REGI_COBA='" & Trim(Me.txtREGI.Text) & "'" & _
                ", TX_CIAL_COBA='" & Trim(Me.txtCIAL.Text) & "'"
            Condi = "TX_COBA_COBA=" & Me.lstBarras.SelectedItem.Tag
            Result = DoUpdate("IN_CODIGOBAR", Valores, Condi)
            If Result = FAIL Then GoTo FALLO
            erda = Me.lstBarras.SelectedItem.Key
            auto = CLng(Me.lstBarras.SelectedItem.Tag)
            Me.lstBarras.ListItems.Remove erda
            Me.lstBarras.ListItems.Add , erda, Me.txtCDBarra.Text
            Me.lstBarras.ListItems(erda).Tag = CStr(auto)
            Me.lstBarras.ListItems(erda).ListSubItems.Add , "NIT", Me.lstTerceros.SelectedItem.Tag
            Me.lstBarras.ListItems(erda).ListSubItems.Add , "NOM", Me.lstTerceros.SelectedItem.ListSubItems("NOM").Text
        End If
FALLO:
        Call btnCan_Click
    End If
End Sub

Private Sub Form_Load()
    Dim CnTdr As Integer
    Dim Articulo As New UnArticulo
'    Call Main
    Call CenterForm(MDI_Inventarios, Me)
    Me.txtIDEN.Text = ""
    Me.txtNOMB.Text = ""
    Me.txtGRUP.Text = ""
    Me.txtUSOS.Text = ""
    Me.txtUSOS.Text = ""
    Me.txtREGI.Text = ""
    Me.txtCIAL.Text = ""
    Me.txtCDBarra.Mask = String(Me.txtCDBarra.MaxLength, "9")
    Me.txtIDEN.Enabled = True
    Me.txtNOMB.Enabled = True
    Me.txtGRUP.Enabled = True
    Me.txtUSOS.Enabled = True
    Me.txtREGI.Enabled = True
    Me.txtCIAL.Enabled = True
    Me.btnAdd.Enabled = False
    Me.btnDel.Enabled = False
    Me.btnCha.Enabled = False
    Me.btnCan.Enabled = False
'SELECT CD_CODI_TERC, NO_NOMB_TERC
'FROM TERCERO;
    Campos = "CD_CODI_TERC, NO_NOMB_TERC"
    Desde = "TERCERO ORDER BY NO_NOMB_TERC"
    Condi = ""
    ReDim ArrUXB(1, 0)
    Result = LoadMulData(Desde, Campos, Condi, ArrUXB())
    If Result = FAIL Then GoTo FALLO
    If Not Encontro Then GoTo NOENC
    Me.lstTerceros.ListItems.Clear
    Me.lstTerceros.ColumnHeaders.Clear
'lstArticulos, NOAR, COAR, NOCO, NOUS, NOGR, AUCO, COGR, COUS
    Me.lstTerceros.ColumnHeaders.Add , "NIT", "Nit", 0.25 * Me.lstArticulos.Width
    Me.lstTerceros.ColumnHeaders.Add , "NOM", "Proveedor", 0.75 * Me.lstArticulos.Width
    For CnTdr = 0 To UBound(ArrUXB, 2)
        Me.lstTerceros.ListItems.Add , "T" & ArrUXB(0, CnTdr), ArrUXB(0, CnTdr)
        Me.lstTerceros.ListItems("T" & ArrUXB(0, CnTdr)).Tag = ArrUXB(0, CnTdr)
        Me.lstTerceros.ListItems("T" & ArrUXB(0, CnTdr)).ListSubItems.Add , "NOM", ArrUXB(1, CnTdr)
    Next
    Campos = "NU_AUTO_ARTI, CD_CODI_ARTI, NO_NOMB_ARTI, NU_AUTO_UNVE_ARTI, TX_PARA_ARTI, " & _
        "DE_DESC_GRUP, DE_DESC_USOS, TX_NOMB_UNVE, CD_GRUP_ARTI, CD_USOS_ARTI, " & _
        "NU_MULT_UNVE, NU_DIVI_UNVE"
    
'NU_AUTO_ARTI, CD_CODI_ARTI, NO_NOMB_ARTI, NU_AUTO_UNVE_ARTI, TX_PARA_ARTI, DE_DESC_GRUP, DE_DESC_USOS, TX_NOMB_UNVE
'FROM ARTICULO, USOS, GRUP_ARTICULO, IN_UNDVENTA

    Desde = "ARTICULO, USOS, GRUP_ARTICULO, IN_UNDVENTA"
    Condi = "CD_CODI_GRUP = CD_GRUP_ARTI AND CD_CODI_USOS = CD_USOS_ARTI AND " & _
        "NU_AUTO_UNVE = NU_AUTO_UNVE_ARTI"
    ReDim ArrUXB(11, 0)
    Result = LoadMulData(Desde, Campos, Condi, ArrUXB())
    If Result = FAIL Then GoTo FALLO
    If Not Encontro Then GoTo NOENC
    Me.lstArticulos.ListItems.Clear
    Me.lstArticulos.Width = 5250
    Me.lstArticulos.ColumnHeaders.Clear
'lstArticulos, NOAR, COAR, NOCO, NOUS, NOGR, AUCO, COGR, COUS
    Me.lstArticulos.ColumnHeaders.Add , "NOAR", "Nombre del art�culo", 0.4 * Me.lstArticulos.Width
    Me.lstArticulos.ColumnHeaders.Add , "COAR", "C�digo", 0.2 * Me.lstArticulos.Width
    Me.lstArticulos.ColumnHeaders.Add , "NOCO", "Unidad de conteo", 0.2 * Me.lstArticulos.Width
    Me.lstArticulos.ColumnHeaders.Add , "NOUS", "Uso", 0.1 * Me.lstArticulos.Width
    Me.lstArticulos.ColumnHeaders.Add , "NOGR", "Grupo", 0.1 * Me.lstArticulos.Width
    Me.lstArticulos.ColumnHeaders.Add , "AUCO", "AutoUNC", 0
    Me.lstArticulos.ColumnHeaders.Add , "COGR", "CodGRU", 0
    Me.lstArticulos.ColumnHeaders.Add , "COUS", "CodUSO", 0
    Me.lstArticulos.MultiSelect = False
    For CnTdr = 0 To UBound(ArrUXB, 2)
        Articulo.AutoNumero = ArrUXB(0, CnTdr)
        Articulo.Codigo = ArrUXB(1, CnTdr)
        Articulo.Nombre = ArrUXB(2, CnTdr)
        Articulo.AutoUDConteo = ArrUXB(3, CnTdr)
        Articulo.NombreGrupo = ArrUXB(5, CnTdr)
        Articulo.NombreUso = ArrUXB(6, CnTdr)
        Articulo.NombreUndConteo = ArrUXB(7, CnTdr)
        Articulo.ParaQueEs = InStr(1, "VC", ArrUXB(4, CnTdr)) - 1
        Articulo.CodigoGrupo = ArrUXB(8, CnTdr)
        Articulo.CodigoUso = ArrUXB(9, CnTdr)
        Me.lstArticulos.ListItems.Add , "A" & Articulo.AutoNumero, Trim(Articulo.Nombre)
        Me.lstArticulos.ListItems("A" & Articulo.AutoNumero).Tag = CStr(Articulo.AutoNumero)
'lstArticulos, NOAR, COAR, NOCO, NOUS, NOGR, AUCO, COGR, COUS, MUCO, DICO
        Me.lstArticulos.ListItems("A" & Articulo.AutoNumero).ListSubItems.Add , "COAR", Trim(Articulo.Codigo)
        Me.lstArticulos.ListItems("A" & Articulo.AutoNumero).ListSubItems.Add , "NOCO", Trim(Articulo.NombreUndConteo)
        Me.lstArticulos.ListItems("A" & Articulo.AutoNumero).ListSubItems.Add , "NOUS", Trim(Articulo.NombreUso)
        Me.lstArticulos.ListItems("A" & Articulo.AutoNumero).ListSubItems.Add , "NOGR", Trim(Articulo.NombreGrupo)
        Me.lstArticulos.ListItems("A" & Articulo.AutoNumero).ListSubItems.Add , "AUCO", Trim(Articulo.AutoUDConteo)
        Me.lstArticulos.ListItems("A" & Articulo.AutoNumero).ListSubItems.Add , "COGR", Trim(Articulo.CodigoGrupo)
        Me.lstArticulos.ListItems("A" & Articulo.AutoNumero).ListSubItems.Add , "COUS", Trim(Articulo.CodigoUso)
        If CnTdr = 0 Then Set Me.lstArticulos.SelectedItem = Me.lstArticulos.ListItems("A" & Articulo.AutoNumero)
    Next
    Call lstArticulos_Click
    Set Articulo = Nothing
NOENC:
FALLO:
End Sub

Private Sub lstArticulos_KeyUp(KeyCode As Integer, Shift As Integer)
    If vClaveArticuloActual = Me.lstArticulos.SelectedItem.Key Then Exit Sub
    vClaveArticuloActual = Me.lstArticulos.SelectedItem.Key
    Call lstArticulos_Click
End Sub

Private Sub lstBarras_Click()
'    If Me.lstBarras.ListItems.Count = 0 Then Exit Sub
    Me.lstArticulos.Enabled = False
    Me.btnAdd.Enabled = True
    Me.btnDel.Enabled = False
    Me.btnCan.Enabled = True
    Me.btnCha.Enabled = False
End Sub

Private Sub lstBarras_ColumnClick(ByVal ColumnHeader As MSComctlLib.ColumnHeader)
    Me.lstArticulos.SortKey = ColumnHeader.Index - 1
    Me.lstArticulos.Sorted = True
End Sub

Private Sub lstBarras_DblClick()
    If Me.lstBarras.ListItems.Count = 0 Then Exit Sub
    Me.lstArticulos.Enabled = False
    Me.btnAdd.Enabled = False
    Me.btnDel.Enabled = True
    Me.btnCan.Enabled = True
    Me.btnCha.Enabled = True
End Sub

Private Sub lstArticulos_Click()
    Dim NuMr As Integer
    Me.btnDel.Enabled = False
    Me.btnCha.Enabled = False
    Me.btnCan.Enabled = False
    Me.txtNOMB.Enabled = False
    Me.txtIDEN.Enabled = False
    Me.txtGRUP.Enabled = False
    Me.txtUSOS.Enabled = False
    Me.txtNOMB.Text = Me.lstArticulos.SelectedItem.Text
    Me.txtIDEN.Text = Me.lstArticulos.SelectedItem.ListSubItems("COAR").Text
    Me.txtGRUP.Text = Me.lstArticulos.SelectedItem.ListSubItems("NOGR").Text
    Me.txtUSOS.Text = Me.lstArticulos.SelectedItem.ListSubItems("NOUS").Text
    Me.lstBarras.ListItems.Clear
    Me.lstBarras.ColumnHeaders.Clear
    Me.lstBarras.ColumnHeaders.Add , "COD", "CD Barras", 0.25 * Me.lstBarras.Width
    Me.lstBarras.ColumnHeaders.Add , "REG", "Nit", 0.25 * Me.lstBarras.Width
    Me.lstBarras.ColumnHeaders.Add , "COM", "Nombre", 0.5 * Me.lstBarras.Width
'    Me.lstBarras.AddItem "C�digo" & vbTab & "Nit" & vbTab & "Nombre", 0
'SELECT NU_AUTO_COBA, TX_COBA_COBA, CD_CODI_TERC, NO_NOMB_TERC
'FROM TERCERO, IN_CODIGOBAR ON TERCERO.CD_CODI_TERC = IN_CODIGOBAR.CD_CODI_TERC_COBA;
''NU_AUTO_ARTI_COBA, TX_COBA_COBA, TX_REGI_COBA, TX_CIAL_COBA

    Campos = "NU_AUTO_ARTI_COBA, TX_COBA_COBA, TX_REGI_COBA, TX_CIAL_COBA"
    Desde = "IN_CODIGOBAR"
    Condi = "NU_AUTO_ARTI_COBA=" & Me.lstArticulos.SelectedItem.Tag
    ReDim ArrBRR(4, 0)
    Result = LoadMulData(Desde, Campos, Condi, ArrBRR())
    If Result = FAIL Then GoTo FALLO
    If Not Encontro Then GoTo NOENC
    For NuMr = 0 To UBound(ArrBRR, 2)
        Me.lstBarras.ListItems.Add , "C" & ArrBRR(0, NuMr), ArrBRR(1, NuMr)
        Me.lstBarras.ListItems("C" & ArrBRR(0, NuMr)).Tag = ArrBRR(0, NuMr)
        Me.lstBarras.ListItems("C" & ArrBRR(0, NuMr)).ListSubItems.Add , "REG", ArrBRR(2, NuMr)
        Me.lstBarras.ListItems("C" & ArrBRR(0, NuMr)).ListSubItems.Add , "COM", ArrBRR(3, NuMr)
    Next
FALLO:
NOENC:
End Sub

'Private Sub lstTerceros_BeforeLabelEdit(Cancel As Integer)
'    Me.lstArticulos.SortKey = ColumnHeader.Index - 1
'    Me.lstArticulos.Sorted = True
'End Sub

Private Sub tlbACCIONES_ButtonClick(ByVal Boton As MSComctlLib.Button)
    Select Case Boton.Key
    Case Is = "CHA"
        Call btnCha_Click
    Case Is = "ADD"
        Call btnAdd_Click
    Case Is = "DEL"
        Call btnDel_Click
    Case Is = "CAN"
        Call btnCan_Click
    End Select
End Sub

'Private Sub Form_Unload(Cancel As Integer)
'    BD(BDCur).Close
'End Sub

Private Sub txtIDEN_KeyPress(Tecla As Integer)
    Call txtCDBarra_KeyPress(Tecla)
End Sub

Private Sub txtCDBarra_KeyPress(Tecla As Integer)
    Dim Cara As String * 1
    Cara = Chr(Tecla)
    If Cara = vbBack Then Exit Sub
    If Asc(Cara) > 96 And Asc(Tecla) < 113 Then Tecla = Asc(UCase(Chr(Tecla)))
    If InStr(" 01234567890ABCDEFGHIJKLMNOPQRSTUVWXYZ", Chr(Tecla)) = 0 Then Tecla = 0
End Sub

Private Function MalaInformacion(ByRef Cual As String) As Boolean
    Cual = ""
    MalaInformacion = False
    If Len(Trim(Me.txtCDBarra.Text)) = 0 Then Cual = Cual & IIf(Len(Cual) = 0, "", ", ") & "CD Barras"
    If Len(Cual) > 0 Then MalaInformacion = True
End Function

Private Sub btnDel_Click()
    If MsgBox("Borrar el CD Barras:" & Me.lstBarras.SelectedItem.Text, vbYesNo) = vbNo Then GoTo DIJNO
    Condi = "NU_AUTO_COBA=" & Me.lstBarras.SelectedItem.Tag
    Result = DoDelete("IN_CODIGOBAR", Condi)
    If Result = FAIL Then GoTo FALLO
    Me.lstBarras.ListItems.Remove Me.lstBarras.SelectedItem.Key
DIJNO:
FALLO:
    Call btnCan_Click
End Sub

Private Sub lstArticulos_ColumnClick(ByVal ColumnHeader As MSComctlLib.ColumnHeader)
    Me.lstArticulos.SortKey = ColumnHeader.Index - 1
    Me.lstArticulos.Sorted = True
End Sub

Private Sub lstTERCEROS_ColumnClick(ByVal ColumnHeader As MSComctlLib.ColumnHeader)
    Me.lstTerceros.SortKey = ColumnHeader.Index - 1
    Me.lstTerceros.Sorted = True
End Sub

