VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Object = "{C932BA88-4374-101B-A56C-00AA003668DC}#1.1#0"; "MSMASK32.OCX"
Object = "{0BA686C6-F7D3-101A-993E-0000C0EF6F5E}#1.0#0"; "THREED32.OCX"
Begin VB.Form frmPrmREPOLST 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Parametros para los ARTICULOS"
   ClientHeight    =   7095
   ClientLeft      =   45
   ClientTop       =   435
   ClientWidth     =   11475
   Icon            =   "frmPrmREPOLST.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   7095
   ScaleWidth      =   11475
   Begin VB.CheckBox chkARTICULOS 
      Caption         =   "Todos Los Art�culos"
      Height          =   375
      Left            =   600
      TabIndex        =   1
      Top             =   600
      Width           =   2895
   End
   Begin VB.Frame fraRANGO 
      Height          =   1335
      Left            =   480
      TabIndex        =   5
      Top             =   5640
      Width           =   10575
      Begin VB.ComboBox cboORDEN 
         Height          =   315
         Left            =   1080
         TabIndex        =   15
         Text            =   "Combo1"
         Top             =   720
         Width           =   3495
      End
      Begin VB.Frame Frame1 
         Height          =   615
         Left            =   5400
         TabIndex        =   12
         Top             =   480
         Width           =   3225
         Begin VB.OptionButton opcSIN 
            Caption         =   "Descendente"
            Height          =   315
            Left            =   1800
            TabIndex        =   14
            Top             =   200
            Width           =   1300
         End
         Begin VB.OptionButton opcCON 
            Caption         =   "Ascendente"
            Height          =   315
            Left            =   240
            TabIndex        =   13
            Top             =   200
            Value           =   -1  'True
            Width           =   1300
         End
      End
      Begin MSMask.MaskEdBox txtFCDESDE 
         Height          =   315
         Left            =   1560
         TabIndex        =   6
         Top             =   960
         Visible         =   0   'False
         Width           =   1215
         _ExtentX        =   2143
         _ExtentY        =   556
         _Version        =   393216
         PromptChar      =   "_"
      End
      Begin MSMask.MaskEdBox txtFCHASTA 
         Height          =   315
         Left            =   3840
         TabIndex        =   7
         Top             =   960
         Visible         =   0   'False
         Width           =   1215
         _ExtentX        =   2143
         _ExtentY        =   556
         _Version        =   393216
         PromptChar      =   "_"
      End
      Begin Threed.SSCommand cmdIMPRIMIR 
         Height          =   735
         Index           =   2
         Left            =   9480
         TabIndex        =   11
         Top             =   360
         Width           =   855
         _Version        =   65536
         _ExtentX        =   1508
         _ExtentY        =   1296
         _StockProps     =   78
         Caption         =   "&IMPRIMIR"
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   6
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         RoundedCorners  =   0   'False
         Picture         =   "frmPrmREPOLST.frx":058A
      End
      Begin VB.CheckBox chkRANGO 
         Caption         =   "Condicionar las fechas del reporte"
         Height          =   315
         Left            =   5400
         TabIndex        =   10
         Top             =   240
         Visible         =   0   'False
         Width           =   3255
      End
      Begin VB.Label Label2 
         Alignment       =   1  'Right Justify
         Caption         =   "Ordenar el reporte X:"
         Height          =   255
         Left            =   240
         TabIndex        =   16
         Top             =   360
         Width           =   1935
      End
      Begin VB.Label lblDESDE 
         Alignment       =   1  'Right Justify
         Caption         =   "Desde:"
         Height          =   255
         Left            =   720
         TabIndex        =   9
         Top             =   960
         Visible         =   0   'False
         Width           =   735
      End
      Begin VB.Label Label1 
         Alignment       =   1  'Right Justify
         Caption         =   "Hasta:"
         Height          =   255
         Left            =   3000
         TabIndex        =   8
         Top             =   960
         Visible         =   0   'False
         Width           =   735
      End
   End
   Begin MSComctlLib.ProgressBar pgbLLEVO 
      Height          =   255
      Left            =   6480
      TabIndex        =   3
      Top             =   240
      Visible         =   0   'False
      Width           =   4455
      _ExtentX        =   7858
      _ExtentY        =   450
      _Version        =   393216
      Appearance      =   1
   End
   Begin VB.ComboBox cboTIPO 
      Height          =   315
      Left            =   3240
      TabIndex        =   0
      Text            =   "Combo1"
      Top             =   240
      Width           =   2655
   End
   Begin MSComctlLib.ImageList imgBotones 
      Left            =   10920
      Top             =   3960
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   20
      ImageHeight     =   14
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   7
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmPrmREPOLST.frx":0C54
            Key             =   ""
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmPrmREPOLST.frx":0FA6
            Key             =   "ASG"
         EndProperty
         BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmPrmREPOLST.frx":1260
            Key             =   "CAN"
         EndProperty
         BeginProperty ListImage4 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmPrmREPOLST.frx":15B2
            Key             =   "SAV"
         EndProperty
         BeginProperty ListImage5 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmPrmREPOLST.frx":18CC
            Key             =   "ADD"
         EndProperty
         BeginProperty ListImage6 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmPrmREPOLST.frx":1B86
            Key             =   "DEL"
         EndProperty
         BeginProperty ListImage7 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmPrmREPOLST.frx":1FC8
            Key             =   "CHA"
         EndProperty
      EndProperty
   End
   Begin MSComctlLib.ListView lstARTICULOS 
      Height          =   4455
      Left            =   480
      TabIndex        =   2
      Top             =   1080
      Width           =   10500
      _ExtentX        =   18521
      _ExtentY        =   7858
      View            =   3
      LabelEdit       =   1
      MultiSelect     =   -1  'True
      LabelWrap       =   -1  'True
      HideSelection   =   0   'False
      AllowReorder    =   -1  'True
      Checkboxes      =   -1  'True
      FullRowSelect   =   -1  'True
      GridLines       =   -1  'True
      _Version        =   393217
      ForeColor       =   -2147483640
      BackColor       =   -2147483643
      BorderStyle     =   1
      Appearance      =   1
      NumItems        =   2
      BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Text            =   "Nombre del Art�culo"
         Object.Width           =   7761
      EndProperty
      BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   1
         Text            =   "C�digo"
         Object.Width           =   3881
      EndProperty
   End
   Begin VB.Label txtTIPO 
      Alignment       =   1  'Right Justify
      Caption         =   "Tipo de Art�culos:"
      Height          =   315
      Left            =   600
      TabIndex        =   4
      Top             =   240
      Width           =   2415
   End
End
Attribute VB_Name = "frmPrmREPOLST"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Dim ArrUXB() As Variant
Dim Articulo As UnArticulo
Dim Dueno As New ElTercero
'Dim LaBodega As String, LaAccion As String
Dim LaAccion As String      'DEPURACION DE CODIGO
'JAUM T29577 Inicio se deja linea en comentario
'Dim CnTdr As Integer, NumEnCombo As Integer
Dim CnTdr As Double 'Contador
Dim NumEnCombo As Integer
'JAUM T29577 Fin
Public OpcCod        As String   'Opci�n de seguridad

Private Sub cboTIPO_GotFocus()
    NumEnCombo = Me.cboTIPO.ListIndex
End Sub

Private Sub chkRANGO_Click()
    Me.txtFCDESDE.Enabled = False
    Me.txtFCHASTA.Enabled = False
    If Not CBool(Me.chkRANGO.Value) = True Then Exit Sub
    Me.txtFCDESDE.Enabled = True
    Me.txtFCHASTA.Enabled = True
End Sub

Private Sub CmdImprimir_Click(Index As Integer)
    Dim Renglon As MSComctlLib.ListItem
    Dim strARTICULOS As String
    Call Limpiar_CrysListar
    If Me.chkARTICULOS.Value = 0 Then
        For Each Renglon In Me.lstARTICULOS.ListItems
            If Renglon.Selected Then strARTICULOS = strARTICULOS & IIf(Len(strARTICULOS) > 0, " OR ", "") & "{ARTICULO.CD_CODI_ARTI}='" & Renglon.ListSubItems("COAR").Text & "'"
        Next
    End If
    If Len(strARTICULOS) > 0 Then strARTICULOS = "(" & strARTICULOS & ")"
    If CBool(Me.chkRANGO.Value) Then strARTICULOS = strARTICULOS & IIf(Len(strARTICULOS) > 0, " AND ", "") & _
        "({IN_KARDEX.FE_FECH_KARD} in CDate('" & Me.txtFCDESDE.Text & "') to CDate('" & Me.txtFCHASTA.Text & "'))"
    Debug.Print strARTICULOS
    Select Case Me.cboORDEN.ListIndex
    Case 0
        MDI_Inventarios.Crys_Listar.SortFields(0) = IIf(Me.opcCON.Value, "+", "-") & "{ARTICULO.CD_CODI_ARTI}"
    Case 1
        MDI_Inventarios.Crys_Listar.SortFields(0) = IIf(Me.opcCON.Value, "+", "-") & "{ARTICULO.NO_NOMB_ARTI}"
    Case 2
        MDI_Inventarios.Crys_Listar.SortFields(0) = IIf(Me.opcCON.Value, "+", "-") & "{GRUP_ARTICULO.DE_DESC_GRUP}"
    Case 3
        MDI_Inventarios.Crys_Listar.SortFields(0) = IIf(Me.opcCON.Value, "+", "-") & "{USOS.DE_DESC_USOS}"
    Case 4
        MDI_Inventarios.Crys_Listar.SortFields(0) = IIf(Me.opcCON.Value, "+", "-") & "{ARTICULO.VL_COPR_ARTI}"
    End Select
    
'    'REOL M1589
'    If cboTIPO.ListIndex = 0 Then
'       MDI_Inventarios.Crys_Listar.Formulas(11) = "TIPO = '0'"
'    Else
'       MDI_Inventarios.Crys_Listar.Formulas(11) = "TIPO = '1'"
'    End If
    'SMDL M1848
    If cboTIPO.ListIndex = 0 Then
       strARTICULOS = strARTICULOS & IIf(Len(strARTICULOS) > 0, " AND ", "") & _
       "({ARTICULO.TX_PARA_ARTI} = 'V')"
    Else
       strARTICULOS = strARTICULOS & IIf(Len(strARTICULOS) > 0, " AND ", "") & _
       "({ARTICULO.TX_PARA_ARTI} = 'C')"
    End If
    Debug.Print strARTICULOS
    'SMDL M1848
    
    MDI_Inventarios.Crys_Listar.Formulas(10) = "ENTIDAD=" & Comi & Trim(Dueno.Nombre) & ", NIT:" & Trim(Dueno.Nit) & Comi
    Call ListarD("infLSTART.rpt", strARTICULOS, crptToWindow, "ARTICULOS", MDI_Inventarios, True)
    MDI_Inventarios.Crys_Listar.Formulas(10) = ""
    MDI_Inventarios.Crys_Listar.Formulas(11) = "" 'REOL M1589
End Sub

Private Sub Form_Load()
    Call CenterForm(MDI_Inventarios, Me)
    Dueno.IniXNit (Dueno.Nit)
    Me.cboORDEN.Clear
    Me.cboORDEN.AddItem "C�digo"
    Me.cboORDEN.AddItem "Nombre"
    Me.cboORDEN.AddItem "Grupos"
    Me.cboORDEN.AddItem "Usos"
    Me.cboORDEN.AddItem "Costo"
    Me.cboORDEN.ListIndex = 1
    Me.cboTIPO.Clear
    Me.cboTIPO.AddItem "Para la Venta"
    Me.cboTIPO.AddItem "Para El Consumo"
    NumEnCombo = Me.cboTIPO.ListIndex
    If Len(FormatDateTime(Date, vbShortDate)) = 8 Then
        Me.txtFCDESDE.Mask = "##/##/##"
        Me.txtFCHASTA.Mask = "##/##/##"
    Else
        Me.txtFCDESDE.Mask = "##/##/####"
        Me.txtFCHASTA.Mask = "##/##/####"
    End If
    Me.txtFCDESDE.Text = FormatDateTime(DateAdd("d", -(Day(Date) - 1), Date), vbShortDate)
    Me.txtFCHASTA.Text = FormatDateTime(DateAdd("d", -1, DateAdd("M", 1, DateAdd("d", -(Day(Date) - 1), Date))), vbShortDate)
    Me.chkRANGO.Value = False
    Me.txtFCDESDE.Enabled = False
    Me.txtFCHASTA.Enabled = False
    
    Me.lstARTICULOS.ListItems.Clear
    Me.lstARTICULOS.Checkboxes = False
    Me.lstARTICULOS.MultiSelect = True
    Me.lstARTICULOS.HideSelection = False
    Me.lstARTICULOS.Width = 10500
    Me.lstARTICULOS.ColumnHeaders.Clear
    Me.lstARTICULOS.ColumnHeaders.Add , "NOAR", "Nombre del art�culo", 0.4 * Me.lstARTICULOS.Width
    Me.lstARTICULOS.ColumnHeaders.Add , "COAR", "C�digo", 0.12 * Me.lstARTICULOS.Width
    Me.lstARTICULOS.ColumnHeaders.Add , "NOCO", "Unidad de conteo", 0.12 * Me.lstARTICULOS.Width
    Me.lstARTICULOS.ColumnHeaders.Add , "COPR", "Costo", 0.1 * Me.lstARTICULOS.Width
    Me.lstARTICULOS.ColumnHeaders("COPR").Alignment = lvwColumnRight
    Me.lstARTICULOS.ColumnHeaders.Add , "NOUS", "Uso", 0.12 * Me.lstARTICULOS.Width
    Me.lstARTICULOS.ColumnHeaders.Add , "NOGR", "Grupo", 0.12 * Me.lstARTICULOS.Width
    Me.lstARTICULOS.ColumnHeaders.Add , "AUCO", "AutoUNC", 0
    Me.lstARTICULOS.ColumnHeaders.Add , "COGR", "CodGRU", 0
    Me.lstARTICULOS.ColumnHeaders.Add , "COUS", "CodUSO", 0
FALLO:
End Sub

Private Sub cboTIPO_LostFocus()
    If NumEnCombo = Me.cboTIPO.ListIndex Then Exit Sub
    Dim tipo As String * 1
    Me.lstARTICULOS.ListItems.Clear
    Me.pgbLLEVO.Value = 0
    Me.pgbLLEVO.Visible = True
    Select Case Me.cboTIPO.ListIndex
    Case Is = -1
        tipo = "0"
    Case Is = 0
        tipo = "V"
    Case Is = 1
        tipo = "C"
    End Select
    
    Campos = "NU_AUTO_ARTI, CD_CODI_ARTI, NO_NOMB_ARTI, NU_AUTO_UNVE_ARTI, TX_PARA_ARTI, " & _
        "DE_DESC_GRUP, DE_DESC_USOS, TX_NOMB_UNVE, CD_GRUP_ARTI, CD_USOS_ARTI, " & _
        "NU_MULT_UNVE, NU_DIVI_UNVE, VL_COPR_ARTI"
    Desde = "ARTICULO, USOS, GRUP_ARTICULO, IN_UNDVENTA"
    Condi = "CD_CODI_GRUP = CD_GRUP_ARTI AND CD_CODI_USOS = CD_USOS_ARTI"
    Condi = Condi & " AND NU_AUTO_UNVE = NU_AUTO_UNVE_ARTI"
    Condi = Condi & " AND TX_PARA_ARTI='" & tipo & "' ORDER BY NO_NOMB_ARTI"
    ReDim ArrUXB(12, 0)
    Result = LoadMulData(Desde, Campos, Condi, ArrUXB())
    If Result = FAIL Then GoTo FALLO
    If Not Encontro Then GoTo NOENC
    Me.pgbLLEVO.Min = 0
    Me.pgbLLEVO.Value = 0
    Me.pgbLLEVO.Max = UBound(ArrUXB, 2) + 1
    Dim Articulo As New UnArticulo
    For CnTdr = 0 To UBound(ArrUXB, 2)
        Me.pgbLLEVO.Value = CnTdr
        Articulo.AutoNumero = ArrUXB(0, CnTdr)
        Articulo.Codigo = ArrUXB(1, CnTdr)
        Articulo.Nombre = ArrUXB(2, CnTdr)
        Articulo.AutoUDConteo = ArrUXB(3, CnTdr)
        Articulo.NombreGrupo = ArrUXB(5, CnTdr)
        Articulo.NombreUso = ArrUXB(6, CnTdr)
        Articulo.NombreUndConteo = ArrUXB(7, CnTdr)
        Articulo.ParaQueEs = InStr(1, "VC", ArrUXB(4, CnTdr)) - 1
        Articulo.CodigoGrupo = ArrUXB(8, CnTdr)
        Articulo.CodigoUso = ArrUXB(9, CnTdr)
        If Not IsNumeric(ArrUXB(12, CnTdr)) Then ArrUXB(12, CnTdr) = "0"
        Articulo.CostoPromedio = ArrUXB(12, CnTdr)
        On Error GoTo NLADD
        Me.lstARTICULOS.ListItems.Add , "A" & Articulo.AutoNumero, Trim(Articulo.Nombre)
        Me.lstARTICULOS.ListItems("A" & Articulo.AutoNumero).Tag = CStr(Articulo.AutoNumero)
'lstArticulos, NOAR, COAR, NOCO, COPR, NOUS, NOGR, AUCO, COGR, COUS, MUCO, DICO
        Me.lstARTICULOS.ListItems("A" & Articulo.AutoNumero).ListSubItems.Add , "COAR", Trim(Articulo.Codigo)
        Me.lstARTICULOS.ListItems("A" & Articulo.AutoNumero).ListSubItems.Add , "NOCO", Trim(Articulo.NombreUndConteo)
        'Me.lstARTICULOS.ListItems("A" & Articulo.AutoNumero).ListSubItems.Add , "COPR", Format(Articulo.CostoPromedio, "00'000,000")
        Me.lstARTICULOS.ListItems("A" & Articulo.AutoNumero).ListSubItems.Add , "COPR", Format(Articulo.CostoPromedio, "####0.#0") 'JACC DEPURACION NOV 2008
        
        Me.lstARTICULOS.ListItems("A" & Articulo.AutoNumero).ListSubItems.Add , "NOUS", Trim(Articulo.NombreUso)
        Me.lstARTICULOS.ListItems("A" & Articulo.AutoNumero).ListSubItems.Add , "NOGR", Trim(Articulo.NombreGrupo)
        Me.lstARTICULOS.ListItems("A" & Articulo.AutoNumero).ListSubItems.Add , "AUCO", Trim(Articulo.AutoUDConteo)
        Me.lstARTICULOS.ListItems("A" & Articulo.AutoNumero).ListSubItems.Add , "COGR", Trim(Articulo.CodigoGrupo)
        Me.lstARTICULOS.ListItems("A" & Articulo.AutoNumero).ListSubItems.Add , "COUS", Trim(Articulo.CodigoUso)
NLADD:
        On Error GoTo 0
    Next
    Set Articulo = Nothing
NOENC:
FALLO:
    Me.pgbLLEVO.Visible = False
End Sub

Private Sub lstARTICULOS_ColumnClick(ByVal ColumnHeader As MSComctlLib.ColumnHeader)
    Me.lstARTICULOS.SortKey = ColumnHeader.Index - 1
    Me.lstARTICULOS.Sorted = True
End Sub

Private Sub txtFCDESDE_LostFocus()
    If IsDate(Me.txtFCDESDE.Text) Then Exit Sub
    Me.txtFCDESDE.Text = FormatDateTime(DateAdd("d", -(Day(Date) - 1), Date), vbShortDate)
    Me.txtFCDESDE.SetFocus
End Sub

Private Sub txtFCHASTA_LostFocus()
    If IsDate(Me.txtFCHASTA.Text) Then Exit Sub
    Me.txtFCHASTA.Text = FormatDateTime(DateAdd("d", -1, DateAdd("M", 1, DateAdd("d", -(Day(Date) - 1), Date))), vbShortDate)
    Me.txtFCHASTA.SetFocus
End Sub

