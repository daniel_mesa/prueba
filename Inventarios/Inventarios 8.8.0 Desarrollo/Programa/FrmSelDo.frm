VERSION 5.00
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "MSFLXGRD.OCX"
Object = "{0BA686C6-F7D3-101A-993E-0000C0EF6F5E}#1.0#0"; "THREED32.OCX"
Begin VB.Form FrmSelecDocu 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Selecci�n"
   ClientHeight    =   4860
   ClientLeft      =   2595
   ClientTop       =   2445
   ClientWidth     =   7890
   ClipControls    =   0   'False
   Icon            =   "FrmSelDo.frx":0000
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   4860
   ScaleWidth      =   7890
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin VB.Frame Frame1 
      Height          =   4815
      Left            =   120
      TabIndex        =   4
      Top             =   0
      Width           =   7695
      Begin VB.CommandButton Cmd_Cancelar 
         Caption         =   "&CANCELAR"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   6
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   435
         Left            =   4320
         MouseIcon       =   "FrmSelDo.frx":058A
         Picture         =   "FrmSelDo.frx":0894
         Style           =   1  'Graphical
         TabIndex        =   2
         Top             =   4320
         Width           =   1335
      End
      Begin VB.CommandButton Cmd_Seleccion 
         Caption         =   "&SELECCIONAR"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   6
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   435
         Left            =   1200
         Picture         =   "FrmSelDo.frx":0BD6
         Style           =   1  'Graphical
         TabIndex        =   1
         Top             =   4320
         Width           =   1335
      End
      Begin Threed.SSPanel SSPanel2 
         Height          =   3375
         Left            =   120
         TabIndex        =   5
         Top             =   840
         Width           =   7425
         _Version        =   65536
         _ExtentX        =   13097
         _ExtentY        =   5953
         _StockProps     =   15
         BackColor       =   12632256
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Begin VB.PictureBox Picture1 
            BorderStyle     =   0  'None
            Height          =   360
            Left            =   6600
            Picture         =   "FrmSelDo.frx":1128
            ScaleHeight     =   360
            ScaleWidth      =   600
            TabIndex        =   3
            Top             =   600
            Visible         =   0   'False
            Width           =   600
         End
         Begin MSFlexGridLib.MSFlexGrid Grd_Selecciones 
            Height          =   2745
            Left            =   120
            TabIndex        =   0
            Top             =   480
            Width           =   7215
            _ExtentX        =   12726
            _ExtentY        =   4842
            _Version        =   393216
            Cols            =   4
            FixedCols       =   0
            BackColorBkg    =   16777215
            Redraw          =   -1  'True
            SelectionMode   =   1
            MouseIcon       =   "FrmSelDo.frx":1ACA
         End
         Begin VB.Label Lbl_Condicion 
            BackColor       =   &H80000009&
            BorderStyle     =   1  'Fixed Single
            Caption         =   "Lbl_Condicion"
            Height          =   255
            Left            =   4680
            TabIndex        =   10
            Top             =   120
            Visible         =   0   'False
            Width           =   975
         End
         Begin VB.Image Image1 
            Height          =   615
            Left            =   3000
            Top             =   1200
            Width           =   1095
         End
         Begin VB.Label Lbl_Tabla 
            BackColor       =   &H80000009&
            BorderStyle     =   1  'Fixed Single
            Caption         =   "Lbl_Tabla"
            Height          =   255
            Left            =   1320
            TabIndex        =   8
            Top             =   120
            Visible         =   0   'False
            Width           =   855
         End
         Begin VB.Label Lbl_Campos 
            BackColor       =   &H80000009&
            BorderStyle     =   1  'Fixed Single
            Caption         =   "Lbl_Campos"
            Height          =   255
            Left            =   5640
            TabIndex        =   9
            Top             =   120
            Visible         =   0   'False
            Width           =   975
         End
         Begin VB.Label Lbl_OrderBY 
            BackColor       =   &H80000009&
            BorderStyle     =   1  'Fixed Single
            Caption         =   "Lbl_OrderBY"
            Height          =   255
            Left            =   120
            TabIndex        =   7
            Top             =   120
            Visible         =   0   'False
            Width           =   975
         End
         Begin VB.Label Lbl_Titulo 
            Alignment       =   2  'Center
            Caption         =   "Lbl_Titulo"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   12
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Left            =   120
            TabIndex        =   6
            Top             =   120
            Width           =   7155
         End
      End
      Begin VB.Label Lbl_Tercero 
         Caption         =   "Lbl Tercero"
         Height          =   255
         Left            =   120
         TabIndex        =   12
         Top             =   480
         Width           =   6615
      End
      Begin VB.Label Lbl_Dependencia 
         Caption         =   "Lbl Dependencia"
         Height          =   255
         Left            =   120
         TabIndex        =   11
         Top             =   240
         Width           =   6615
      End
   End
End
Attribute VB_Name = "FrmSelecDocu"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Dim BAND As Integer
'Dim SQL2 As String         'DEPURACION DE CODIGO

Private Sub Cmd_Cancelar_Click()
    Codigo = NUL$
    If BAND = 1 Then ' cargando el grid
      CancelTrans = 1
    End If
    If BAND = 0 Then
      Unload Me
      CancelTrans = 0
    End If
End Sub
Private Sub Cmd_Seleccion_Click()

Grd_Selecciones.Col = 0
Codigo = Grd_Selecciones.Text
Unload Me
End Sub
Private Sub Form_Activate()
    If Lbl_Tercero = NUL$ Then
       Lbl_Tercero.Visible = False
    Else
       Lbl_Tercero.Visible = True
       Call Buscar_Tercero
    End If
    
    With Grd_Selecciones
        .TextMatrix(0, 0) = "N�mero"
        .TextMatrix(0, 1) = "Valor"
        .TextMatrix(0, 2) = "Fecha "
        .FixedAlignment(0) = 4
        .FixedAlignment(1) = 4
        .FixedAlignment(2) = 4
        .FixedAlignment(3) = 4
        If Trim(Lbl_Tabla) = "CERTIFICADO" Then
           .TextMatrix(0, 3) = "Fecha Expiraci�n"
           .ColWidth(3) = 1500
        Else
           If Lbl_Tabla = "GIRO" Then
              .TextMatrix(0, 3) = NUL$
              .ColWidth(3) = 1
           Else
            If (Lbl_Tercero = NUL$ And (Mid(Lbl_Tabla, 1, 8) = "REGISTRO" Or Mid(Lbl_Tabla, 1, 10) = "OBLIGACION" Or Mid(Lbl_Tabla, 1, 4) = "GIRO")) Then
              'faaa ob 8278
              '.TextMatrix(0, 3) = "Tercero"
              .TextMatrix(0, 3) = "Concepto"
              'faaa ob 8278
              .ColWidth(3) = 3000
            Else
              .TextMatrix(0, 3) = "Fecha"
              .TextMatrix(0, 2) = "Valor"
              .TextMatrix(0, 1) = "Descripci�n"
              .ColWidth(3) = 1100
              .ColWidth(2) = 1800
              .ColWidth(1) = 4501
            End If
           End If
        End If
    End With
    
    Call Busqueda
End Sub
Private Sub Form_Load()
   With Grd_Selecciones
    .ColWidth(0) = 800
    .ColWidth(1) = 2000
    .ColWidth(2) = 1000
   End With
   CancelTrans = 0
End Sub
Private Sub Form_Unload(Cancel As Integer)
   If BAND = 1 Then
      CancelTrans = 1
      Cancel = True
   Else
      CancelTrans = 0
   End If
End Sub
Private Sub Grd_selecciones_DblClick()
If BAND = 0 Then Call Cmd_Seleccion_Click
End Sub

Private Sub Grd_selecciones_KeyPress(KeyAscii As Integer)
If BAND = 0 Then
   If KeyAscii = 13 Then Call Cmd_Seleccion_Click
End If
End Sub
Private Sub Grd_Selecciones_RowColChange()
Dim A As Long
A = Grd_Selecciones.RowPos(Grd_Selecciones.Row)
If (A < 2640 And A >= 0) Then
 Picture1.Top = 480 + A
End If
End Sub
Private Sub Busqueda()
Dim Pos As Integer

DoEvents

Grd_Selecciones.Rows = 2
With Grd_Selecciones
    .TextMatrix(1, 0) = NUL$
    .TextMatrix(1, 1) = NUL$
    .TextMatrix(1, 2) = NUL$
    .TextMatrix(1, 3) = NUL$
End With

Pos = InStr(Lbl_Campos, ",")
Codigo = Mid(Lbl_Campos, 1, Pos - 1)

Call desabilitar
Result = LoadfGrid(Grd_Selecciones, Lbl_Tabla, Lbl_Campos, Lbl_Condicion)
Call habilitar
Cmd_Seleccion.Enabled = True
If Not Encontro And Grd_Selecciones.Rows = 1 Then
  Call Mensaje1("Ningun elemento encontrado", 3)
End If
Grd_Selecciones.Col = 0
Grd_Selecciones.SetFocus
If Result = FAIL Then
    Call Mensaje1("Error al cargar la tabla " & Lbl_Tabla, 1)
    Unload Me
End If
    
CancelTrans = 0
End Sub
Sub desabilitar()
    BAND = 1
    Cmd_Seleccion.Enabled = False
    'SSPanel2.Enabled = False
End Sub

Sub habilitar()
    BAND = 0
    Cmd_Seleccion.Enabled = True
    SSPanel2.Enabled = True

End Sub
Private Sub Buscar_Tercero()
ReDim Arr(0)
   Condicion = "CD_CODI_TERC=" & Comi & Cambiar_Comas_Comillas(Lbl_Tercero) & Comi
               
   Result = LoadData("TERCERO", "NO_NOMB_TERC", Condicion, Arr())
   If (Result <> False) Then
      Lbl_Tercero = Arr(0)
   End If
End Sub
