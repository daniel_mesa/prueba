VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Begin VB.Form FrmInfo 
   Caption         =   "Depuraci�n SQL"
   ClientHeight    =   3570
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   9735
   Icon            =   "FrmInfo.frx":0000
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   MDIChild        =   -1  'True
   NegotiateMenus  =   0   'False
   ScaleHeight     =   3570
   ScaleWidth      =   9735
   Visible         =   0   'False
   Begin MSComctlLib.TreeView treeConsole 
      Height          =   1935
      Left            =   120
      TabIndex        =   8
      Top             =   1080
      Visible         =   0   'False
      Width           =   3735
      _ExtentX        =   6588
      _ExtentY        =   3413
      _Version        =   393217
      LabelEdit       =   1
      Style           =   1
      ImageList       =   "imglstConsole"
      Appearance      =   1
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin MSComctlLib.Toolbar toolInfo 
      Align           =   1  'Align Top
      Height          =   420
      Left            =   0
      TabIndex        =   6
      Top             =   0
      Width           =   9735
      _ExtentX        =   17171
      _ExtentY        =   741
      ButtonWidth     =   609
      ButtonHeight    =   582
      Appearance      =   1
      ImageList       =   "imglstInfo"
      _Version        =   393216
      BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
         NumButtons      =   9
         BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "a01"
            Style           =   3
         EndProperty
         BeginProperty Button2 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "MSSQL"
            Object.ToolTipText     =   "Analizador de consultas"
         EndProperty
         BeginProperty Button3 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "CATCH_TABLE"
            Object.ToolTipText     =   "Interceptar acceso a tabla"
         EndProperty
         BeginProperty Button4 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "CATCH_EXPR"
            Object.ToolTipText     =   "Interceptar expresi�n en instrucci�n SQL"
         EndProperty
         BeginProperty Button5 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Style           =   4
            Object.Width           =   300
         EndProperty
         BeginProperty Button6 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "CONSOLE"
            Object.ToolTipText     =   "Ver salida de la consola"
            Style           =   1
         EndProperty
         BeginProperty Button7 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Style           =   4
            Object.Width           =   300
         EndProperty
         BeginProperty Button8 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "EMPTY_FULL"
            Object.ToolTipText     =   "S�lo rastrear instrucciones con registros involucrados."
            Style           =   1
         EndProperty
         BeginProperty Button9 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "CLEAR_LIST"
            Object.ToolTipText     =   "Limpiar lista"
         EndProperty
      EndProperty
   End
   Begin VB.CommandButton cmdStatus 
      Caption         =   "n"
      BeginProperty Font 
         Name            =   "Wingdings"
         Size            =   14.25
         Charset         =   2
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   1440
      TabIndex        =   5
      Top             =   3120
      Width           =   495
   End
   Begin VB.ComboBox cmbMenu 
      Height          =   315
      ItemData        =   "FrmInfo.frx":058A
      Left            =   2280
      List            =   "FrmInfo.frx":058C
      Style           =   2  'Dropdown List
      TabIndex        =   4
      Top             =   1080
      Visible         =   0   'False
      Width           =   1935
   End
   Begin MSComctlLib.TreeView treeInfo 
      Height          =   1935
      Left            =   120
      TabIndex        =   3
      Top             =   1080
      Width           =   3735
      _ExtentX        =   6588
      _ExtentY        =   3413
      _Version        =   393217
      LabelEdit       =   1
      Style           =   1
      ImageList       =   "imglstInfo"
      Appearance      =   1
   End
   Begin VB.Timer tmrInfo 
      Interval        =   250
      Left            =   3480
      Top             =   3360
   End
   Begin VB.CommandButton cmdDesactivar 
      Caption         =   "Desactivar"
      Height          =   375
      Left            =   120
      TabIndex        =   1
      Top             =   3120
      Width           =   1215
   End
   Begin MSComctlLib.ImageList imglstConsole 
      Left            =   7800
      Top             =   360
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      MaskColor       =   12632256
      _Version        =   393216
   End
   Begin MSComctlLib.ImageList imglstInfo 
      Left            =   1920
      Top             =   2400
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      MaskColor       =   12632256
      _Version        =   393216
   End
   Begin VB.Image imgConsole 
      Height          =   480
      Index           =   5
      Left            =   7200
      Picture         =   "FrmInfo.frx":058E
      Top             =   960
      Visible         =   0   'False
      Width           =   480
   End
   Begin VB.Image imgConsole 
      Height          =   480
      Index           =   4
      Left            =   6000
      Picture         =   "FrmInfo.frx":0E58
      Top             =   960
      Visible         =   0   'False
      Width           =   480
   End
   Begin VB.Image imgConsole 
      Height          =   480
      Index           =   3
      Left            =   6600
      Picture         =   "FrmInfo.frx":129A
      Top             =   960
      Visible         =   0   'False
      Width           =   480
   End
   Begin VB.Image imgConsole 
      Height          =   480
      Index           =   2
      Left            =   5400
      Picture         =   "FrmInfo.frx":1B64
      Top             =   960
      Visible         =   0   'False
      Width           =   480
   End
   Begin VB.Image imgInfo 
      Height          =   480
      Index           =   10
      Left            =   4200
      Picture         =   "FrmInfo.frx":242E
      Top             =   2520
      Visible         =   0   'False
      Width           =   480
   End
   Begin VB.Image imgConsole 
      Height          =   480
      Index           =   1
      Left            =   4800
      Picture         =   "FrmInfo.frx":2CF8
      Top             =   960
      Visible         =   0   'False
      Width           =   480
   End
   Begin VB.Image imgConsole 
      Height          =   480
      Index           =   0
      Left            =   4200
      Picture         =   "FrmInfo.frx":313A
      Top             =   960
      Visible         =   0   'False
      Width           =   480
   End
   Begin VB.Image imgInfo 
      Height          =   480
      Index           =   9
      Left            =   2760
      Picture         =   "FrmInfo.frx":3A04
      Top             =   2160
      Visible         =   0   'False
      Width           =   480
   End
   Begin VB.Image imgInfo 
      Height          =   480
      Index           =   8
      Left            =   2760
      Picture         =   "FrmInfo.frx":42CE
      Top             =   1680
      Visible         =   0   'False
      Width           =   480
   End
   Begin VB.Label lblInfo 
      AutoSize        =   -1  'True
      Caption         =   "�"
      BeginProperty Font 
         Name            =   "Wingdings"
         Size            =   24
         Charset         =   2
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   540
      Index           =   2
      Left            =   5400
      TabIndex        =   7
      ToolTipText     =   "Click sobre esta etiqueta, para copiar el contenido al portapapeles."
      Top             =   600
      Width           =   435
   End
   Begin VB.Image imgInfo 
      Height          =   480
      Index           =   7
      Left            =   2760
      Picture         =   "FrmInfo.frx":4B98
      Top             =   1200
      Visible         =   0   'False
      Width           =   480
   End
   Begin VB.Image imgInfo 
      Height          =   480
      Index           =   6
      Left            =   2160
      Picture         =   "FrmInfo.frx":5462
      Top             =   2280
      Visible         =   0   'False
      Width           =   480
   End
   Begin VB.Image imgInfo 
      Height          =   480
      Index           =   5
      Left            =   2160
      Picture         =   "FrmInfo.frx":58A4
      Top             =   1800
      Visible         =   0   'False
      Width           =   480
   End
   Begin VB.Image imgInfo 
      Height          =   480
      Index           =   4
      Left            =   2160
      Picture         =   "FrmInfo.frx":5BAE
      Top             =   1200
      Visible         =   0   'False
      Width           =   480
   End
   Begin VB.Image imgInfo 
      Height          =   480
      Index           =   3
      Left            =   1200
      Picture         =   "FrmInfo.frx":5EB8
      Top             =   2520
      Visible         =   0   'False
      Width           =   480
   End
   Begin VB.Image imgInfo 
      Height          =   480
      Index           =   2
      Left            =   1440
      Picture         =   "FrmInfo.frx":62FA
      Top             =   1800
      Visible         =   0   'False
      Width           =   480
   End
   Begin VB.Image imgInfo 
      Height          =   480
      Index           =   1
      Left            =   1320
      Picture         =   "FrmInfo.frx":673C
      Top             =   1200
      Visible         =   0   'False
      Width           =   480
   End
   Begin VB.Image imgInfo 
      Height          =   480
      Index           =   0
      Left            =   1800
      Picture         =   "FrmInfo.frx":6B7E
      Top             =   1920
      Visible         =   0   'False
      Width           =   480
   End
   Begin VB.Label lblInfo 
      AutoSize        =   -1  'True
      BackColor       =   &H000000C0&
      BackStyle       =   0  'Transparent
      Caption         =   "..."
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   195
      Index           =   1
      Left            =   120
      TabIndex        =   2
      Top             =   800
      Width           =   195
   End
   Begin VB.Label lblInfo 
      AutoSize        =   -1  'True
      BackColor       =   &H00808000&
      BackStyle       =   0  'Transparent
      Caption         =   "Haga doble click sobre la lista, para obtener informaci�n sobre la tabla."
      Height          =   195
      Index           =   0
      Left            =   2160
      TabIndex        =   0
      ToolTipText     =   "Click sobre esta etiqueta, para copiar el contenido al portapapeles."
      Top             =   3240
      Width           =   4995
   End
End
Attribute VB_Name = "FrmInfo"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Declare Function ShellExecute Lib "shell32.dll" Alias "ShellExecuteA" (ByVal hwnd As Long, ByVal lpOperation As String, ByVal lpFile As String, ByVal lpParameters As String, ByVal lpDirectory As String, ByVal nShowCmd As Long) As Long
Dim stLastStr As String, stCompleteCmd() As String, stLastCmd As String
Dim inX As Integer, inY As Integer, stCatchTable As String, stCatchAny As String
Private Declare Function GetTickCount Lib "kernel32" () As Long

'Realiza un c�lculo y devuelve el cuadro que deber�a mostrarse en el tiempo actual.
'lgFrames -> Cantidad total de cuadros, inFPS -> Velocidad de los cuadros.
Function fnGetCurFrame(lgFrames As Long, inFPS As Integer) As Long '.D.
    
On Error GoTo Fallo
    
    Dim lgTickCount As Long, dbTickCount As Double
    
    dbTickCount = GetTickCount()
    dbTickCount = (dbTickCount / 1000) * inFPS
    lgTickCount = dbTickCount
    
    fnGetCurFrame = lgTickCount
    
    fnGetCurFrame = lgTickCount - (Int(lgTickCount / lgFrames) * lgFrames)
    Exit Function
    
Fallo:
    fnGetCurFrame = -1
    
End Function

Function fnShell(stArchivo As String, inCmdShow As Integer)
'Abre cualquier documento, como si se tratara de una ejecuci�n desde el explorador
        
        Dim lgRet As Long
    
        lgRet = ShellExecute(0, "open", stArchivo, vbNullString, Mid(stArchivo, 1, fnInStr(stArchivo, "\")), inCmdShow)
    
        If lgRet <= 32 Then
            'sbMostrarError lgRet, stArchivo
        End If
        
        fnShell = lgRet
        
End Function

Function fnInStr(a$, b$) As Integer
'Esta instrucci�n hace la operaci�n inversa de Instr$
' encuentra el �ltimo car�cter b$ en a$.
    Dim i%, j%
    
    i% = 1
    
    Do
    j% = i%
    i% = InStr(i%, a$, b$) + 1
    Loop Until i% = 1
    
    fnInStr = j% - 1
    
End Function

'Desde el c�digo externo, nunca llame esta funci�n directamente ya que
' forzar�a la carga de este formulario.
Sub sbConsole(stOper As String, inIcon As Integer)
        With treeConsole
            .Nodes.Add , tvwPrevious, "A" & .Nodes.Count, stOper, inIcon
            .Nodes(.Nodes.Count).Tag = stLastCmd
        End With
End Sub

Sub sbInspec(stCmd As String)
        
        Dim stOrden As String, stTabla As String, inIn As Integer, inImg As Integer
        Dim stCurStr As String, boCatch As Boolean, lgRecsInvol As Long
        
        'toolInfo.Buttons("EMPTY_FULL")
        
        inImg = 1 'Imagen por defecto.
        
        stCmd = UCase(Trim(stCmd))
        
        '00 Signos visuales:
            lblInfo(2) = Chr(Asc(lblInfo(2)) + 1)
            If Asc(lblInfo(2)) > 194 Then lblInfo(2) = Chr(183)
        '00 Fin
        
        '01 Eliminar cadenas que confundan el an�lisis:
            Dim stFW(2) As String, stRP(2) As String, i%, stExtra As String
                        
            stFW(0) = " ,": stRP(0) = ","
            stFW(1) = ", ": stRP(1) = ","
            stFW(2) = "  ": stRP(2) = " "
                                            
            For i% = 0 To UBound(stFW)
                Do While InStr(1, stCmd, stFW(i%)) > 0
                    stCmd = Replace(stCmd, stFW(i%), stRP(i%))
                Loop
            Next i%
            
            stLastCmd = stCmd
        '01 Fin
        
        
        
        stOrden = Trim(Mid(stCmd, 1, InStr(1, stCmd, " ")))
        
        Select Case stOrden
            Case "SELECT", "DELETE"
                stTabla = Trim(Mid(stCmd, InStr(1, stCmd, " FROM ") + 6))
                inIn = InStr(1, stTabla, " ")
                If inIn > 0 Then stTabla = Trim(Mid(stTabla, 1, inIn))
            Case "INSERT"
                stTabla = Trim(Mid(stCmd, InStr(1, stCmd, " INTO ") + 6))
                inIn = InStr(1, stTabla, " ")
                If inIn > 0 Then stTabla = Trim(Mid(stTabla, 1, inIn))
            Case "UPDATE"
                stTabla = Trim(Mid(stCmd, InStr(1, stCmd, stOrden) + Len(stOrden)))
                inIn = InStr(1, stTabla, " ")
                If inIn > 0 Then stTabla = Trim(Mid(stTabla, 1, inIn))
        End Select
        
        '02 Informaci�n extra:
         If stOrden = "SELECT" Then
            If MotorBD = "ACCESS" Then
                lgRecsInvol = BDSSDAO(BDCurCon).RecordCount
            Else
                lgRecsInvol = BDSS(BDCurCon).RecordCount
            End If
                stExtra = " (" & lgRecsInvol & ") "
         Else
            If MotorBD = "ACCESS" Then
                lgRecsInvol = BDDAO(BDCurCon).RecordsAffected
            Else
                lgRecsInvol = lgRecsAffect
            End If
               stExtra = " [" & lgRecsInvol & "] "
         End If
        '02 Fin.
        
        '021 Condicionar a instrucciones con registros involucrados:
            If lgRecsInvol < 1 And toolInfo.Buttons("EMPTY_FULL").Value = tbrPressed Then Exit Sub
        '021 Fin
        
        '03 Seleccionar imagen para el TreeView:
            If stOrden = "UPDATE" Then inImg = 2
            If stOrden = "INSERT" Then inImg = 3
            If stOrden = "DELETE" Then inImg = 4
        '03 Fin.
        
        '04 Intercepci�n de instrucci�n SQL:
        If stCatchTable <> "" Then
            boCatch = InStr(1, stTabla, stCatchTable) > 0
        End If
        
        If stCatchAny <> "" Then
            boCatch = boCatch Or InStr(1, stCmd, stCatchAny) > 0
        End If
        
        '04 Fin.
        
        stCurStr = stOrden & " -> " & stTabla
        If stLastStr <> stCurStr Then
            treeInfo.Nodes.Add , tvwPrevious, "A" & treeInfo.Nodes.Count, stCurStr & stExtra, inImg
            treeInfo.Nodes(treeInfo.Nodes.Count).Tag = stTabla
            ReDim Preserve stCompleteCmd(treeInfo.Nodes.Count)
            stCompleteCmd(treeInfo.Nodes.Count) = stCmd 'Recuerde aqu� se agrega la primera instrucci�n.
        Else
            If Len(stCompleteCmd(treeInfo.Nodes.Count)) < 12000 Then   'Recuerde aqu� se agrega la primera instrucci�n.
                stCompleteCmd(treeInfo.Nodes.Count) = stCompleteCmd(treeInfo.Nodes.Count) & vbCrLf & stCmd
            Else
                stCompleteCmd(treeInfo.Nodes.Count) = stCompleteCmd(treeInfo.Nodes.Count) & "-- etc."
            End If
            treeInfo.Nodes(treeInfo.Nodes.Count).Text = treeInfo.Nodes(treeInfo.Nodes.Count).Text & "+" & stExtra
        End If
        stLastStr = stCurStr
        
        treeInfo.Nodes(treeInfo.Nodes.Count).EnsureVisible
        
        If boCatch Then
            Clipboard.Clear
            Clipboard.SetText stCmd & "/* Regs -> " & lgRecsInvol & " */"
            Debug.Print "/* Aqu� */ " & stCmd
            'En este punto la instrucci�n SQL ya ha sido enviada al portapapeles!!
            Stop 'La intercepci�n se ha activado, la expresi�n se encontr�.
        End If
End Sub

'DEPURACION DE CODIGO
'Sub sbMnuAdd(stItem As String)
''    If mnuTask.Count = 1 And mnuTask(0).Caption = "" Then
''        mnuTask(0).Caption = stItem
''    ElseIf mnuTask(0).Caption <> "" Then
''        Load mnuTask(mnuTask.Count)
''        mnuTask(mnuTask.Count - 1).Caption = stItem
''    End If
'End Sub

'Sub sbMnuClear()
''    Dim i%
''
''    For i% = 1 To mnuTask.Count - 1
''        Unload mnuTask(i%)
''    Next i%
''
''    mnuTask(0).Caption = ""
''
'End Sub

Private Sub cmbMenu_Click()
        
        cmbMenu.Visible = False
        
    '00 Usar m�todo ActiveX para abrir la p�gina:
        Dim ie1 As Object
    Set ie1 = CreateObject("InternetExplorer.Application")
    ie1.MenuBar = False
    ie1.Toolbar = False
    ie1.Visible = True
    ie1.Navigate2 ("\\Cnt-server\@CNT\@Tecnologia\Diccionario de datos\Tables\" & cmbMenu.Text & "_dbo.html")
    '00 Fin
    
        
    'MsgBox ("Error al intentar acceder a la informaci�n de la tabla."), 16, "Error"
    'End If

End Sub

Private Sub cmbMenu_LostFocus()
    cmbMenu.Visible = False
End Sub


Private Sub cmdDesactivar_Click()
    
    boDebug = False
    Unload FrmInfo
    
End Sub

Private Sub cmdStatus_Click()
        
        If cmdStatus.Caption = "�" Then
            cmdStatus.Caption = "n"
            boDebug = True
        Else
            cmdStatus.Caption = "�"
            boDebug = False
        End If

End Sub

Private Sub Form_Load()
    
    Dim obItera As Object, obPrevForm As Form
    
    ReDim stCompleteCmd(0)
    stCompleteCmd(0) = lblInfo(0)
    
    
    'Cargas iniciales:
    For Each obItera In imgInfo
        imglstInfo.ListImages.Add obItera.Index + 1, "A" & obItera.Index, obItera.Picture
    Next obItera
    
    For Each obItera In imgConsole
        imglstConsole.ListImages.Add obItera.Index + 1, "A" & obItera.Index, obItera.Picture
    Next obItera
    
    
    'Im�genes de la barra de herramientas:
    toolInfo.Buttons("MSSQL").Image = 5
    toolInfo.Buttons("CATCH_TABLE").Image = 6
    toolInfo.Buttons("CATCH_EXPR").Image = 8
    toolInfo.Buttons("CLEAR_LIST").Image = 7
    toolInfo.Buttons("EMPTY_FULL").Image = 9
    toolInfo.Buttons("CONSOLE").Image = 11

    Set obPrevForm = Screen.ActiveForm

If obPrevForm.WindowState = 0 Then
    On Error Resume Next
    FrmInfo.Show
    obPrevForm.Left = 0
    FrmInfo.Left = obPrevForm.Left + obPrevForm.Width
    FrmInfo.Top = obPrevForm.Top
    obPrevForm.SetFocus
    FrmInfo.Width = 9855
    On Error GoTo 0
End If
    
End Sub


Private Sub Form_Resize()
        
        With FrmInfo
            
            If .WindowState <> 1 Then
            
            treeInfo.Width = .Width - 300 - treeInfo.Left
            treeInfo.Height = .Height - cmdDesactivar.Height - 550 - treeInfo.Top
            treeConsole.Width = treeInfo.Width
            treeConsole.Height = treeInfo.Height
            cmdDesactivar.Top = treeInfo.Height + treeInfo.Top + 100
            cmdStatus.Top = cmdDesactivar.Top
            lblInfo(0).Top = cmdDesactivar.Top + 120
            lblInfo(2).Left = .Width - lblInfo(2).Width - 100
        
            End If
            
        End With
         
End Sub

Private Sub Form_Unload(Cancel As Integer)
        stLastStr = ""
End Sub

Private Sub lblInfo_Click(Index As Integer)
        
        Static lgTimer As Long
        
        If lgTimer <> 0 Then Exit Sub
        
        Clipboard.Clear
        Clipboard.SetText lblInfo(Index)
        
        lgTimer = GetTickCount
        
        Do
            lblInfo(Index).BackStyle = fnGetCurFrame(2, 15)
            DoEvents
        Loop Until GetTickCount - lgTimer > 500
        
        lblInfo(Index).BackStyle = 0
        lgTimer = 0
        
End Sub

Private Sub tmrInfo_Timer()
    
    If Screen.ActiveForm Is Nothing Or Screen.ActiveControl Is Nothing Then Exit Sub
    
    If Not Screen.ActiveForm Is FrmInfo Then
        lblInfo(1) = Screen.ActiveForm.Name & " -> " & Screen.ActiveControl.Name
    End If

End Sub

Private Sub toolInfo_ButtonClick(ByVal Button As MSComctlLib.Button)
            
    Select Case Trim(UCase(Button.Key))
        Case "MSSQL"
            If MotorBD = "ACCESS" Then
                 MsgBox "El analizador de consultas no es compatible con Access.", 16
            Else
                 Shell "c:\Archivos de programa\Microsoft SQL Server\80\Tools\Binn\isqlw.exe " & Replace("/S'" & BD(BDCurCon).Properties(15).Value & "' /U'admin' /P'TRIACON' /d'" & BD(BDCurCon).Properties(0).Value & "'", "'", Chr(34)), 1
            End If
        Case "CATCH_TABLE"
            stCatchTable = UCase(Trim(InputBox("Escriba el nombre de la tabla a interceptar (si cancela o no escribe nada, se anula la intercepci�n):", "Interceptar acceso a tabla", stCatchTable)))
            Button.Value = Abs(stCatchTable <> "")
        Case "CATCH_EXPR"
            stCatchAny = UCase(Trim(InputBox("Escriba la expresi�n a interceptar (si cancela o no escribe nada, se anula la intercepci�n):", "Interceptar instrucci�n SQL con el contenido escrito.", stCatchAny)))
            Button.Value = Abs(stCatchAny <> "")
        Case "CLEAR_LIST"
            ReDim stCompleteCmd(0)
            stCompleteCmd(0) = lblInfo(0)
            treeInfo.Nodes.Clear
            treeConsole.Nodes.Clear
            stLastStr = ""
        Case "CONSOLE"
            treeConsole.Visible = CBool(Button.Value)
        Case "EMPTY_FULL"
            Button.Image = 9 + Button.Value
    End Select
    
End Sub

Private Sub treeConsole_NodeClick(ByVal Node As MSComctlLib.Node)
        lblInfo(0) = Node.Tag
End Sub


Private Sub treeInfo_DblClick()

    Dim stTablas As String
    
'No se hace uso del Men� normal, ya que este fuerza a esconder el men� del
' formulario MDI, cuando esta ventana pasa a ser activa, se uso en su lugar
' de un ComboList (cmbMenu).
    
    cmbMenu.Clear
    
    stTablas = treeInfo.SelectedItem.Tag
    
        If InStr(1, stTablas, ",") < 1 Then
            cmbMenu.AddItem stTablas
            cmbMenu.ListIndex = 0
         Else
            Do While InStr(1, stTablas, ",") > 0
                cmbMenu.AddItem Mid(stTablas, 1, InStr(1, stTablas, ",") - 1)
                stTablas = Mid(stTablas, InStr(1, stTablas, ",") + 1)
            Loop
                cmbMenu.AddItem stTablas
                cmbMenu.Left = inX: cmbMenu.Top = inY
                cmbMenu.Visible = True
                cmbMenu.ZOrder 0
                cmbMenu.SetFocus
        End If


End Sub


Private Sub treeInfo_LostFocus()
    
    lblInfo(0) = stCompleteCmd(0)
    
End Sub

Private Sub treeInfo_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
        
        inX = X + treeInfo.Left: inY = Y + treeInfo.Top
        
End Sub


Private Sub treeInfo_NodeClick(ByVal Node As MSComctlLib.Node)
        
        lblInfo(0) = stCompleteCmd(Node.Index)
        
End Sub


