Attribute VB_Name = "Calendario"
'=================================================
'Funciones para el manejo de los calendarios
'=================================================
Sub ConfigGrid(Grid As MSFlexGrid)
Dim i, J As Byte
   For i = 0 To Grid.Rows - 1
      Grid.Row = i
      Grid.RowHeight(i) = 225
      For J = 0 To Grid.Cols - 1
         Grid.Col = J
         Grid.TextMatrix(i, J) = NUL$
         Grid.CellBackColor = vbWindowBackground
         Grid.CellForeColor = vbWindowText
         Grid.ColWidth(J) = 360
         Grid.CellFontBold = False
         
      Next J
   Next i
End Sub

Sub CARGAR(ByVal Grid As MSFlexGrid, Calendar As MonthView)
'Dim i, J As Byte
Dim i As Byte       'DEPURACION DE CODIGO
Dim DiaSem As Long
Dim Mes As Integer
Dim a�o As Integer

Mes = Calendar.Month
a�o = Calendar.Year
DiaSem = 0
Calendar.Day = 1
i = 0
Do While a�o = Calendar.Year And Mes = Calendar.Month
   DiaSem = Calendar.DayOfWeek
   Codigo = Grid.TextMatrix(1, 0)
   If Grid.TextMatrix(i, DiaSem - 1) = NUL$ Then
      Grid.TextMatrix(i, DiaSem - 1) = Calendar.Day
      On Error GoTo final
      Calendar.Day = Calendar.Day + 1
      If (DiaSem - 1) = 6 Then i = i + 1
   End If
Loop
final:
   Exit Sub

End Sub

Function BusLastday(ByVal Grid As MSFlexGrid) As Integer
Dim i, J As Integer
Dim ind As Integer
   ind = 0
   For i = Grid.Rows - 1 To 0 Step -1
      Grid.Row = i
      For J = Grid.Cols - 1 To 0 Step -1
         Grid.Col = J
         If Grid.Text <> NUL$ Then ind = 1: Exit For
      Next J
      If ind = 1 Then Exit For
   Next i
   On Error Resume Next
   BusLastday = CInt(Grid.Text)
End Function

Sub Seldia(Grid As MSFlexGrid, ByVal Color As ColorConstants, ByVal Fondo As ColorConstants, Opcion As String)
   If Grid.Text <> NUL$ Then
      If Opcion = "T" Then Grid.CellForeColor = Color
      Grid.CellBackColor = Fondo
      Grid.CellFontBold = True
   End If
End Sub

Sub Desdia(Grid As MSFlexGrid, Opcion As String)
   If Grid.Text <> NUL$ Then
      If Opcion = "T" Then Grid.CellForeColor = vbWindowText
      Grid.CellBackColor = vbWindowBackground
      Grid.CellFontBold = False
   End If
End Sub

Sub VerificaDia(ByRef Grid As MSFlexGrid, ByVal dia As String, ByVal Color As ColorConstants, ByVal Fondo As ColorConstants, Opcion As String)
Dim i, J As Integer

   For i = 0 To Grid.Rows - 1
      Grid.Row = i
      For J = 0 To Grid.Cols - 1
         Grid.Col = J
         If Grid.TextMatrix(i, J) = dia Then
            Call Seldia(Grid, Color, Fondo, Opcion)
         End If
      Next J
   Next i
End Sub

'Sub VerificaDiaFestivo(ByRef Grid As MSFlexGrid, ByVal dia As String, ByVal Color As ColorConstants, ByVal Fondo As ColorConstants)
Sub VerificaDiaFestivo(ByRef Grid As MSFlexGrid, ByVal dia As String, Optional ByVal Color As ColorConstants, Optional ByVal Fondo As ColorConstants)   'DEPURACION DE CODIGO
Dim i, J As Integer

   For i = 0 To Grid.Rows - 1
      Grid.Row = i
      For J = 0 To Grid.Cols - 1
         Grid.Col = J
         If Grid.TextMatrix(i, J) = dia Then
            Grid.CellForeColor = vbRed
         End If
      Next J
   Next i
End Sub

Sub Seleccionar_Dias(ByVal Grid As MSFlexGrid, ByVal Colum As Integer, ByVal Accion As Byte, ByVal Color As ColorConstants, ByVal Fondo As ColorConstants, a�o As Variant, Mes As Variant, Opcion As String)
'Dim i, J As Integer
Dim i As Integer        'DEPURACION DE CODIGO
Dim f As Variant

   For i = 0 To Grid.Rows - 1
      Grid.Col = Colum
      If Grid.TextMatrix(i, Colum) <> NUL$ Then
         Grid.Row = i
         If Accion = 1 Then
            
            Condi = "FE_FECH_CALE=" & FFechaCon(a�o & "/" & Mes & "/" & Grid)
            Condi = Condi & " AND NU_TIPO_CALE=0"
            f = Leer_Tabla("CALENDARIO", "FE_FECH_CALE", Condi)
            
            If f = "" Then ' Si no existe en la tabla
               Valores = "FE_FECH_CALE=" & FFechaIns(a�o & "/" & Mes & "/" & Grid)
               Valores = Valores & ",NU_TIPO_CALE=0"
               Result = DoInsertSQL("CALENDARIO", Valores)
               If Result <> FAIL Then
                Call Seldia(Grid, Color, Fondo, Opcion)
            End If
         
         End If
         
         ElseIf Accion = 0 Then
            Condi = "FE_FECH_CALE=" & FFechaCon(a�o & "/" & Mes & "/" & Grid)
            Condi = Condi & " AND NU_TIPO_CALE=0"
            Result = DoDelete("CALENDARIO", Condi)
            
            If Result <> FAIL Then
               Call Desdia(Grid, Opcion)
            End If
         
         End If
      End If
   Next i
End Sub

Function Leer_Tabla(Tabla As String, campo As String, Condicion As String) As String
Dim Arr(0)
    Result = LoadData(Tabla, campo, Condicion, Arr())
    Leer_Tabla = Arr(0)
End Function
