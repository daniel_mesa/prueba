VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "ElTercero"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit

'Private trc As Long 'Autonumerico del Tercero      'DEPURACION DE CODIGO
Private ide As String * 11 'Nit
Private nmb As String * 50 'Nombre

Public Property Let Nit(cnt As String)
    If Len(Trim(cnt)) > 0 Then ide = Trim(cnt)
End Property

Public Property Get Nit() As String
    Nit = ide
End Property

Public Property Get Nombre() As String
    Nombre = nmb
End Property

Public Property Let Nombre(Cue As String)
    nmb = Cue
End Property

Public Function IniXNit(Optional ByVal rut As String) As ResultConsulta
    If rut = "" Then rut = ide
    Dim ArrTemp() As Variant
    Campos = "CD_CODI_TERC, NO_NOMB_TERC"
    Desde = "TERCERO"
    Condi = "CD_CODI_TERC='" & rut & "'"
    ReDim ArrTemp(1)
    Result = LoadData(Desde, Campos, Condi, ArrTemp())
    If Result = FAIL Then GoTo FALLO
    If Not Encontro Then GoTo NOENC
'    trc = ArrTemp(0) 'RST("CD_CODI_TERC")
    ide = rut
    nmb = ArrTemp(1) 'RST("NO_NOMB_TERC")
    IniXNit = Encontroinfo
    Exit Function
FALLO:
   IniXNit = Errorinfo
   Exit Function
NOENC:
    nmb = String(Len(nmb), " ")
    IniXNit = NoEncontroinfo
   Exit Function
End Function

Private Sub Class_Initialize()
    ide = GetINIString("Cliente", "Nit", "")
'    ide = "800149453" 'CPO
'    ide = "860048656" 'LAFON
'    ide = "811017810" 'ITAGUI
End Sub
'
'Public Function LSTTerceros(cbx As ComboBox) As String
'    Dim ArrTemp() As Variant, CnTdr As Integer
'    Campos = "CD_CODI_TERC, CD_CODI_TERC, NO_NOMB_TERC"
'    Desde = "TERCERO ORDER BY 3"
'    Condi = ""
'    ReDim ArrTemp(2, 0)
'    Result = LoadMulData(Desde, Campos, Condi, ArrTemp())
'    If Result <> FAIL Then
'        If Encontro Then
'            For CnTdr = 0 To UBound(ArrTemp, 2)
'                cbx.AddItem ArrTemp(2, CnTdr) 'rst("NO_NOMB_TERC")
'                cbx.ItemData(cbx.NewIndex) = ArrTemp(0, CnTdr) 'rst("CD_CODI_TERC")
'            Next
'        End If
'    End If
'    LSTTerceros = ""
'End Function

