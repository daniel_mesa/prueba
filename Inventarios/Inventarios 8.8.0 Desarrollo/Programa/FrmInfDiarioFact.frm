VERSION 5.00
Object = "{C932BA88-4374-101B-A56C-00AA003668DC}#1.1#0"; "MSMASK32.OCX"
Object = "{0BA686C6-F7D3-101A-993E-0000C0EF6F5E}#1.0#0"; "THREED32.OCX"
Begin VB.Form FrmInfDiarioFact 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "INFORME DIARIO DE FACTURACION"
   ClientHeight    =   1935
   ClientLeft      =   45
   ClientTop       =   435
   ClientWidth     =   5205
   Icon            =   "FrmInfDiarioFact.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   1935
   ScaleWidth      =   5205
   StartUpPosition =   3  'Windows Default
   Begin VB.Frame framActions 
      Height          =   1845
      Left            =   120
      TabIndex        =   0
      Top             =   0
      Width           =   5025
      Begin MSMask.MaskEdBox MskFechaInicial 
         Height          =   285
         Left            =   1320
         TabIndex        =   1
         ToolTipText     =   "d�a/mes/a�o"
         Top             =   360
         Width           =   1050
         _ExtentX        =   1852
         _ExtentY        =   503
         _Version        =   393216
         MaxLength       =   10
         Format          =   "dd/mm/yyyy"
         Mask            =   "##/##/####"
         PromptChar      =   " "
      End
      Begin MSMask.MaskEdBox MskFechaFinal 
         Height          =   285
         Left            =   3720
         TabIndex        =   2
         ToolTipText     =   "d�a/mes/a�o"
         Top             =   360
         Width           =   1050
         _ExtentX        =   1852
         _ExtentY        =   503
         _Version        =   393216
         MaxLength       =   10
         Format          =   "dd/mm/yyyy"
         Mask            =   "##/##/####"
         PromptChar      =   " "
      End
      Begin Threed.SSCommand SCmd_Options 
         Height          =   735
         Index           =   1
         Left            =   1560
         TabIndex        =   5
         Top             =   960
         Width           =   855
         _Version        =   65536
         _ExtentX        =   1508
         _ExtentY        =   1296
         _StockProps     =   78
         Caption         =   "&IMPRIMIR"
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   6
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         RoundedCorners  =   0   'False
         Picture         =   "FrmInfDiarioFact.frx":058A
      End
      Begin Threed.SSCommand SCmd_Options 
         Height          =   735
         HelpContextID   =   120
         Index           =   2
         Left            =   2640
         TabIndex        =   6
         ToolTipText     =   "Salir"
         Top             =   960
         Width           =   855
         _Version        =   65536
         _ExtentX        =   1508
         _ExtentY        =   1296
         _StockProps     =   78
         Caption         =   "&SALIR"
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   6
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Picture         =   "FrmInfDiarioFact.frx":0C54
      End
      Begin VB.Label Label5 
         Caption         =   "Fecha final"
         Height          =   255
         Left            =   2760
         TabIndex        =   4
         Top             =   360
         Width           =   975
      End
      Begin VB.Label Label4 
         Caption         =   "Fecha inicial"
         Height          =   255
         Left            =   240
         TabIndex        =   3
         Top             =   360
         Width           =   1095
      End
   End
End
Attribute VB_Name = "FrmInfDiarioFact"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'---------------------------------------------------------------------------------------
' Module    : FrmInfDiarioFact
' DateTime  : 08/06/2009 08:57
' Author    : jeison_camacho
' Purpose   : IMPRIMIR FACTURAS DEL DIA
'---------------------------------------------------------------------------------------
Option Explicit
Dim BlFecha As Boolean
Private Sub SCmd_Options_Click(Index As Integer)
   Select Case Index
      Case 1
               BlFecha = False
               MskFechaFinal_LostFocus
               If BlFecha = True Then Exit Sub
              Call InfoDia
      Case 2
             Unload Me
   End Select
End Sub
'Private Function InfoDia() As Boolean 'JACC M
Private Sub InfoDia() 'JACC M5304 Se cambila la declaracion
   If ExisteTABLA("TEMP_REPORT") Then Result = ExecSQLCommand("DROP TABLE TEMP_REPORT"): If Result = FAIL Then Call Mensaje1("Error al borrar la tabla TEMP_REPORT", 2): Exit Sub
    
   Campos = "select NU_COMP_ENCA ,FE_CREA_ENCA ,TOTAL ,DESCUENTO ,IMPUESTOS ,TX_COMP_CONE ,CONTADOR,TIPO INTO TEMP_REPORT FROM ("
   'Campos = Campos & " SELECT NU_COMP_ENCA ,FE_CREA_ENCA , SUM(T2.NU_VALO_PRPR)AS TOTAL ,(SELECT ISNULL(SUM(T.NU_VALO_PRPR),0)"
   'Campos = Campos & " FROM IN_PARTIPARTI T, IN_ENCABEZADO Where t.NU_AUTO_ENCA_PRPR = NU_AUTO_ENCA"
   'Campos = Campos & " AND IN_ENCABEZADO.NU_AUTO_DOCU_ENCA = 11"
   'Campos = Campos & " AND TX_CUAL_PRPR = 'D'"
   'Campos = Campos & " AND T.NU_AUTO_ENCA_PRPR = T2.NU_AUTO_ENCA_PRPR) as DESCUENTO"
   'Campos = Campos & " ,(SELECT isnull(SUM(T.NU_VALO_PRPR),0)"
   'Campos = Campos & " FROM IN_PARTIPARTI T, IN_ENCABEZADO"
   'Campos = Campos & " Where t.NU_AUTO_ENCA_PRPR = NU_AUTO_ENCA"
   'Campos = Campos & " AND IN_ENCABEZADO.NU_AUTO_DOCU_ENCA = 11"
   'Campos = Campos & " AND TX_CUAL_PRPR = 'I' AND T.NU_AUTO_ENCA_PRPR = T2.NU_AUTO_ENCA_PRPR) AS IMPUESTOS"
   'Campos = Campos & " ,TX_COMP_CONE ,1 AS CONTADOR,CASE CD_CATEG_FOPA"
   'Campos = Campos & " WHEN 0 then 'Efectivo' WHEN 1 then 'Cheque'"
   'Campos = Campos & " WHEN 2 then 'Tarjeta' WHEN 3 then 'Otros'"
   'Campos = Campos & " WHEN 4 then 'Cheque Posfechado' END As tipo"
   'Campos = Campos & " FROM IN_PARTIPARTI T2, In_encabezado,FORMA_PAGO,CONEXION"
   'Campos = Campos & " Where T2.NU_AUTO_ENCA_PRPR = NU_AUTO_ENCA"
   'Campos = Campos & " AND IN_ENCABEZADO.NU_AUTO_DOCU_ENCA = 11"
   'Campos = Campos & " AND TX_CUAL_PRPR = 'F' AND NOT TX_CUAL_PRPR IS NULL"
   'Campos = Campos & " AND  T2.TX_CODI_PART_PRPR = NU_NUME_FOPA"
   'Campos = Campos & " AND NU_CONE_ENCA = NU_AUTO_CONE"
   'Campos = Campos & " AND FE_CREA_ENCA Between " & FFechaCon(MskFechaInicial) & " AND " & FFechaCon(MskFechaFinal)
   'Campos = Campos & " GROUP BY NU_COMP_ENCA,T2.NU_AUTO_ENCA_PRPR,TX_COMP_CONE,NU_COMP_ENCA"
   'Campos = Campos & " ,FE_CREA_ENCA,CD_CATEG_FOPA ) AS T5"

   ''JACC M5304
   Campos = Campos & " select NU_COMP_ENCA, FE_CREA_ENCA, TOTAL, DESCUENTO, IMPUESTOS, TX_COMP_CONE, CONTADOR, TIPO from"
   Campos = Campos & " (SELECT row_number() over (order by nu_comp_enca) as rowid,"
   'Campos = Campos & " NU_COMP_ENCA ,FE_CREA_ENCA , SUM(T2.NU_VALO_PRPR)AS TOTAL ,"
   Campos = Campos & " NU_COMP_ENCA ,FE_CREA_ENCA , SUM(cast(ISNULL(T2.NU_VALO_PRPR,0) as float)) AS TOTAL ,"  'JACC M6726
   'campo descuentos
   'Campos = Campos & " (SELECT ISNULL(SUM(T.NU_VALO_PRPR),0) FROM IN_PARTIPARTI T, IN_ENCABEZADO"
   Campos = Campos & " (SELECT ISNULL(SUM(cast(ISNULL(T.NU_VALO_PRPR,0) as float)),0) FROM IN_PARTIPARTI T, IN_ENCABEZADO" 'JACC M6726
   Campos = Campos & " Where t.NU_AUTO_ENCA_PRPR = NU_AUTO_ENCA And IN_ENCABEZADO.NU_AUTO_DOCU_ENCA = 11"
   Campos = Campos & " AND TX_CUAL_PRPR = 'D' AND T.NU_AUTO_ENCA_PRPR = T2.NU_AUTO_ENCA_PRPR) as DESCUENTO ,"
   'campo impuestos
   'Campos = Campos & " (SELECT isnull(SUM(T.NU_VALO_PRPR),0) FROM IN_PARTIPARTI T, IN_ENCABEZADO"
   Campos = Campos & " (SELECT isnull(SUM(cast(ISNULL(T.NU_VALO_PRPR,0) as float)),0) FROM IN_PARTIPARTI T, IN_ENCABEZADO" 'JACC M6726
   Campos = Campos & " Where t.NU_AUTO_ENCA_PRPR = NU_AUTO_ENCA AND IN_ENCABEZADO.NU_AUTO_DOCU_ENCA = 11 AND"
   'New cambio
   Campos = Campos & " (TX_CUAL_PRPR = 'I' OR TX_CUAL_PRPR = 'A' OR TX_CUAL_PRPR = 'O')"
   Campos = Campos & " AND T.NU_AUTO_ENCA_PRPR = T2.NU_AUTO_ENCA_PRPR) AS IMPUESTOS ,"
   Campos = Campos & " TX_COMP_CONE ,1 AS CONTADOR, CASE CD_CATEG_FOPA WHEN 0 then 'Efectivo' WHEN 1 then 'Cheque' WHEN 2 then 'Tarjeta'"
   Campos = Campos & " WHEN 3 then 'Otros' WHEN 4 then 'Cheque Posfechado' END As tipo"
   Campos = Campos & " FROM IN_PARTIPARTI T2,In_encabezado,FORMA_PAGO,CONEXION"
   Campos = Campos & " Where T2.NU_AUTO_ENCA_PRPR = NU_AUTO_ENCA AND"
   Campos = Campos & " IN_ENCABEZADO.NU_AUTO_DOCU_ENCA = 11 AND TX_CUAL_PRPR = 'F' AND NOT TX_CUAL_PRPR IS NULL AND"
   Campos = Campos & " T2.TX_CODI_PART_PRPR = NU_NUME_FOPA AND NU_CONE_ENCA = NU_AUTO_CONE AND"
   Campos = Campos & " TX_ESTA_ENCA = 'N' AND"
   'Campos = Campos & " FE_CREA_ENCA Between " & FFechaCon(MskFechaInicial) & " AND " & FFechaCon(MskFechaFinal) 'JAUM T39086 Se deja linea en comentario
   Campos = Campos & " FE_CREA_ENCA Between " & FFechaCon(MskFechaInicial) & " AND " & FHFECHA(MskFechaFinal & " 23:59:59") 'JAUM T39086
   Campos = Campos & " GROUP BY NU_COMP_ENCA,T2.NU_AUTO_ENCA_PRPR,TX_COMP_CONE,NU_COMP_ENCA ,FE_CREA_ENCA,CD_CATEG_FOPA"
   Campos = Campos & " ) as t1"
   Campos = Campos & " where CAST(rowid AS VARCHAR(10)) + CAST(nu_comp_enca AS VARCHAR(20))  IN ("
   Campos = Campos & " select CAST(max(rowid) AS VARCHAR(10)) + CAST(nu_comp_enca AS VARCHAR(20))"
   Campos = Campos & " From (SELECT"
   Campos = Campos & " row_number() over (order by nu_comp_enca) as rowid,"
   'Campos = Campos & " NU_COMP_ENCA ,FE_CREA_ENCA , SUM(T2.NU_VALO_PRPR)AS TOTAL ,"
   Campos = Campos & " NU_COMP_ENCA ,FE_CREA_ENCA , SUM(cast(ISNULL(T2.NU_VALO_PRPR,0) as float)) AS TOTAL ," 'JACC M6726
   'campo descuentos
   'Campos = Campos & " (SELECT ISNULL(SUM(T.NU_VALO_PRPR),0) FROM IN_PARTIPARTI T, IN_ENCABEZADO"
   Campos = Campos & " (SELECT ISNULL(SUM(cast(ISNULL(T.NU_VALO_PRPR,0) as float)),0) FROM IN_PARTIPARTI T, IN_ENCABEZADO" 'JACC M6726
   Campos = Campos & " Where t.NU_AUTO_ENCA_PRPR = NU_AUTO_ENCA And IN_ENCABEZADO.NU_AUTO_DOCU_ENCA = 11"
   Campos = Campos & " AND TX_CUAL_PRPR = 'D' AND T.NU_AUTO_ENCA_PRPR = T2.NU_AUTO_ENCA_PRPR) as DESCUENTO ,"
   'campo impuestos
   'Campos = Campos & " (SELECT isnull(SUM(T.NU_VALO_PRPR),0) FROM IN_PARTIPARTI T, IN_ENCABEZADO"
   Campos = Campos & " (SELECT isnull(SUM(cast(ISNULL(T.NU_VALO_PRPR,0) as float)),0) FROM IN_PARTIPARTI T, IN_ENCABEZADO" 'JACC M6726
   Campos = Campos & " Where t.NU_AUTO_ENCA_PRPR = NU_AUTO_ENCA AND IN_ENCABEZADO.NU_AUTO_DOCU_ENCA = 11 AND"
   'New cambio
   Campos = Campos & " (TX_CUAL_PRPR = 'I' OR TX_CUAL_PRPR = 'A' OR TX_CUAL_PRPR = 'O')"
   Campos = Campos & " AND T.NU_AUTO_ENCA_PRPR = T2.NU_AUTO_ENCA_PRPR) AS IMPUESTOS ,"
   Campos = Campos & " TX_COMP_CONE ,1 AS CONTADOR,"
   Campos = Campos & " CASE CD_CATEG_FOPA WHEN 0 then 'Efectivo' WHEN 1 then 'Cheque' WHEN 2 then 'Tarjeta'"
   Campos = Campos & " WHEN 3 then 'Otros' WHEN 4 then 'Cheque Posfechado' END As tipo"
   Campos = Campos & " FROM IN_PARTIPARTI T2,In_encabezado,FORMA_PAGO,CONEXION"
   Campos = Campos & " Where T2.NU_AUTO_ENCA_PRPR = NU_AUTO_ENCA AND"
   Campos = Campos & " IN_ENCABEZADO.NU_AUTO_DOCU_ENCA = 11 AND TX_CUAL_PRPR = 'F' AND NOT TX_CUAL_PRPR IS NULL AND"
   Campos = Campos & " T2.TX_CODI_PART_PRPR = NU_NUME_FOPA AND NU_CONE_ENCA = NU_AUTO_CONE AND"
   Campos = Campos & " TX_ESTA_ENCA = 'N' AND"
   'Campos = Campos & " FE_CREA_ENCA Between " & FFechaCon(MskFechaInicial) & " AND " & FFechaCon(MskFechaFinal) 'JAUM T39086 Se deja linea en comentario
   Campos = Campos & " FE_CREA_ENCA Between " & FFechaCon(MskFechaInicial) & " AND " & FHFECHA(MskFechaFinal & " 23:59:59") 'JAUM T39086
   Campos = Campos & " GROUP BY NU_COMP_ENCA,T2.NU_AUTO_ENCA_PRPR,TX_COMP_CONE,NU_COMP_ENCA ,FE_CREA_ENCA,CD_CATEG_FOPA) as t2"
   Campos = Campos & " group by nu_comp_enca )"
   '
   Campos = Campos & " Union"
   Campos = Campos & " select NU_COMP_ENCA, FE_CREA_ENCA, TOTAL, DESCUENTO, IMPUESTOS, TX_COMP_CONE, CONTADOR, TIPO from"
   Campos = Campos & " (SELECT row_number() over (order by nu_comp_enca) as rowid,"
   'Campos = Campos & " NU_COMP_ENCA ,FE_CREA_ENCA , SUM(T2.NU_VALO_PRPR)AS TOTAL ,"
   Campos = Campos & " NU_COMP_ENCA ,FE_CREA_ENCA , SUM(cast(ISNULL(T2.NU_VALO_PRPR,0) as float)) AS TOTAL ," 'JACC M6726
   'campo descuentos
   Campos = Campos & " '0' as DESCUENTO ,"
   'Campo Impuestos
   Campos = Campos & " '0' AS IMPUESTOS ,"
   Campos = Campos & " TX_COMP_CONE ,1 AS CONTADOR,"
   Campos = Campos & " CASE CD_CATEG_FOPA WHEN 0 then 'Efectivo' WHEN 1 then 'Cheque' WHEN 2 then 'Tarjeta'"
   Campos = Campos & " WHEN 3 then 'Otros' WHEN 4 then 'Cheque Posfechado' END As tipo"
   Campos = Campos & " FROM IN_PARTIPARTI T2,In_encabezado,FORMA_PAGO,CONEXION"
   Campos = Campos & " Where T2.NU_AUTO_ENCA_PRPR = NU_AUTO_ENCA AND"
   Campos = Campos & " IN_ENCABEZADO.NU_AUTO_DOCU_ENCA = 11 AND TX_CUAL_PRPR = 'F' AND NOT TX_CUAL_PRPR IS NULL AND"
   Campos = Campos & " T2.TX_CODI_PART_PRPR = NU_NUME_FOPA AND NU_CONE_ENCA = NU_AUTO_CONE AND"
   Campos = Campos & " TX_ESTA_ENCA = 'N' AND"
   'Campos = Campos & " FE_CREA_ENCA Between " & FFechaCon(MskFechaInicial) & " AND " & FFechaCon(MskFechaFinal) 'JAUM T39086 Se deja linea en comentario
   Campos = Campos & " FE_CREA_ENCA Between " & FFechaCon(MskFechaInicial) & " AND " & FHFECHA(MskFechaFinal & " 23:59:59") 'JAUM T39086
   Campos = Campos & " GROUP BY NU_COMP_ENCA,T2.NU_AUTO_ENCA_PRPR,TX_COMP_CONE,NU_COMP_ENCA ,FE_CREA_ENCA,CD_CATEG_FOPA"
   Campos = Campos & " ) as t1"
   Campos = Campos & " where CAST(rowid AS VARCHAR(10)) + CAST(nu_comp_enca AS VARCHAR(20)) NOT IN ("
   Campos = Campos & " select CAST(max(rowid) AS VARCHAR(10)) + CAST(nu_comp_enca AS VARCHAR(20))"
   Campos = Campos & " From (SELECT row_number() over (order by nu_comp_enca) as rowid,"
   'Campos = Campos & " NU_COMP_ENCA ,FE_CREA_ENCA , SUM(T2.NU_VALO_PRPR)AS TOTAL ,"
   Campos = Campos & " NU_COMP_ENCA ,FE_CREA_ENCA , SUM(cast(ISNULL(T2.NU_VALO_PRPR,0) as float)) AS TOTAL ," 'JACC M6726
   'campo descuentos
   'Campos = Campos & " (SELECT ISNULL(SUM(T.NU_VALO_PRPR),0) FROM IN_PARTIPARTI T, IN_ENCABEZADO"
   Campos = Campos & " (SELECT ISNULL(SUM(cast(ISNULL(T.NU_VALO_PRPR,0) as float)),0) FROM IN_PARTIPARTI T, IN_ENCABEZADO" 'JACC M6726
   Campos = Campos & " Where t.NU_AUTO_ENCA_PRPR = NU_AUTO_ENCA And IN_ENCABEZADO.NU_AUTO_DOCU_ENCA = 11"
   Campos = Campos & " AND TX_CUAL_PRPR = 'D' AND T.NU_AUTO_ENCA_PRPR = T2.NU_AUTO_ENCA_PRPR) as DESCUENTO ,"
   'campo impuestos
   'Campos = Campos & " (SELECT isnull(SUM(T.NU_VALO_PRPR),0) FROM IN_PARTIPARTI T, IN_ENCABEZADO"
   Campos = Campos & " (SELECT isnull(SUM(cast(ISNULL(T.NU_VALO_PRPR,0) as float)),0) FROM IN_PARTIPARTI T, IN_ENCABEZADO" 'JACC M6726
   Campos = Campos & " Where t.NU_AUTO_ENCA_PRPR = NU_AUTO_ENCA AND IN_ENCABEZADO.NU_AUTO_DOCU_ENCA = 11 AND"
   'New cambio
   Campos = Campos & " (TX_CUAL_PRPR = 'I' OR TX_CUAL_PRPR = 'A' OR TX_CUAL_PRPR = 'O')"
   Campos = Campos & " AND T.NU_AUTO_ENCA_PRPR = T2.NU_AUTO_ENCA_PRPR) AS IMPUESTOS ,"
   Campos = Campos & " TX_COMP_CONE ,1 AS CONTADOR,"
   Campos = Campos & " CASE CD_CATEG_FOPA WHEN 0 then 'Efectivo' WHEN 1 then 'Cheque' WHEN 2 then 'Tarjeta'"
   Campos = Campos & " WHEN 3 then 'Otros' WHEN 4 then 'Cheque Posfechado' END As tipo"
   Campos = Campos & " FROM IN_PARTIPARTI T2,In_encabezado,FORMA_PAGO,CONEXION"
   Campos = Campos & " Where T2.NU_AUTO_ENCA_PRPR = NU_AUTO_ENCA AND"
   Campos = Campos & " IN_ENCABEZADO.NU_AUTO_DOCU_ENCA = 11 AND TX_CUAL_PRPR = 'F' AND NOT TX_CUAL_PRPR IS NULL AND"
   Campos = Campos & " T2.TX_CODI_PART_PRPR = NU_NUME_FOPA AND NU_CONE_ENCA = NU_AUTO_CONE AND"
   Campos = Campos & " TX_ESTA_ENCA = 'N' AND"
   'Campos = Campos & " FE_CREA_ENCA Between " & FFechaCon(MskFechaInicial) & " AND " & FFechaCon(MskFechaFinal) 'JAUM T39086 Se deja linea en comentario
   Campos = Campos & " FE_CREA_ENCA Between " & FFechaCon(MskFechaInicial) & " AND " & FHFECHA(MskFechaFinal & " 23:59:59") 'JAUM T39086
   Campos = Campos & " GROUP BY NU_COMP_ENCA,T2.NU_AUTO_ENCA_PRPR,TX_COMP_CONE,NU_COMP_ENCA ,FE_CREA_ENCA,CD_CATEG_FOPA) as t2"
   Campos = Campos & " group by nu_comp_enca )"
   Campos = Campos & " ) AS FINAL"
         
   

   'JACC M5304
   Dim SPCmd As New ADODB.Command
   Dim param As New ADODB.Parameter, param1 As New ADODB.Parameter
   param.Name = "@SqlQuery": param.Type = adLongVarChar: param.Direction = adParamInput: param.Value = Campos: param.Size = Len(Campos)
   param1.Name = "@Result": param1.Type = adInteger: param1.Direction = adParamOutput
   SPCmd.Parameters.Append param: SPCmd.Parameters.Append param1
   SPCmd.CommandType = adCmdStoredProc
   SPCmd.CommandText = "PA_DoInsertFromSelect "
   SPCmd.CommandTimeout = 0
   SPCmd.ActiveConnection = BD(BDCurCon)
   SPCmd.Execute
   SPCmd.CommandTimeout = 30
   Result = param1.Value
   Set SPCmd = Nothing: Set param = Nothing: Set param1 = Nothing
   'InfoDia = True 'JACC M5304
   If Result <> FAIL Then
      ''JACC M5361
      Dim IntAux As Integer
      For IntAux = 0 To 50
         MDI_Inventarios.Crys_Listar.Formulas(IntAux) = ""
      Next
      'JACC M5361
      'JACC M5562
      MDI_Inventarios.Crys_Listar.Formulas(4) = "Titulo='INFORME DIARIO DE FACTURAS CONTADO'"
      MDI_Inventarios.Crys_Listar.Formulas(5) = "FeIni='" & Format(MskFechaInicial, "DD/MM/YYYY") & Comi
      MDI_Inventarios.Crys_Listar.Formulas(6) = "FeFin='" & Format(MskFechaFinal, "DD/MM/YYYY") & Comi
      'JACC M5562
      Call Listar1("InfFacturaMaq.rpt", NUL$, NUL$, 0, "Informe diario", MDI_Inventarios)
      'MDI_Inventarios.Crys_Listar.Formulas(0) = ""
      'MDI_Inventarios.Crys_Listar.Formulas(1) = ""
   End If
    
End Sub

Private Sub Form_Load()
    Call CenterForm(MDI_Inventarios, Me)
    MskFechaInicial = Hoy
    MskFechaFinal = Hoy
End Sub

Private Sub MskFechaFinal_KeyPress(KeyAscii As Integer)
   Call ValKeyFecha(KeyAscii, MskFechaFinal)
End Sub

Private Sub MskFechaFinal_LostFocus()
   Call ValFecha(MskFechaFinal, 0)
   If DateDiff("d", MskFechaInicial, MskFechaFinal) < 0 Then Call Mensaje1("La fecha inicial no puede ser mayor a la fecha final", 3): MskFechaFinal = Format(FechaServer, "dd/mm/yyyy"): BlFecha = True: MskFechaInicial.SetFocus
End Sub
Private Sub MskFechaInicial_KeyPress(KeyAscii As Integer)
   Call ValKeyFecha(KeyAscii, MskFechaInicial)
End Sub

Private Sub MskFechaInicial_LostFocus()
   Call ValFecha(MskFechaInicial, 0)
End Sub


