VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.1#0"; "Mscomctl.ocx"
Object = "{C932BA88-4374-101B-A56C-00AA003668DC}#1.1#0"; "MSMASK32.OCX"
Object = "{0BA686C6-F7D3-101A-993E-0000C0EF6F5E}#1.0#0"; "THREED32.OCX"
Begin VB.Form FrmRDPendientes 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Requisiciones pendientes por Despacho"
   ClientHeight    =   7710
   ClientLeft      =   45
   ClientTop       =   435
   ClientWidth     =   7560
   Icon            =   "FrmRDPendientes.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   7710
   ScaleWidth      =   7560
   Begin VB.CheckBox ChckBodegas 
      Caption         =   "Todas la Bodegas"
      Height          =   375
      Left            =   240
      TabIndex        =   3
      Top             =   3240
      Width           =   2295
   End
   Begin VB.CheckBox ChkTercero 
      Caption         =   "Todos los Terceros"
      Height          =   315
      Left            =   240
      TabIndex        =   1
      Top             =   120
      Width           =   3015
   End
   Begin VB.Frame FrmFechas 
      Height          =   1215
      Left            =   120
      TabIndex        =   0
      Top             =   6360
      Width           =   4575
      Begin VB.CheckBox ChkFechas 
         Caption         =   "Condicionar las fechas del reporte"
         Height          =   315
         Left            =   120
         TabIndex        =   5
         Top             =   240
         Width           =   4095
      End
      Begin MSMask.MaskEdBox MskFechFin 
         Height          =   315
         Left            =   3120
         TabIndex        =   7
         Top             =   600
         Width           =   1215
         _ExtentX        =   2143
         _ExtentY        =   556
         _Version        =   393216
         Enabled         =   0   'False
         MaxLength       =   10
         Format          =   "dd/mm/yyyy"
         Mask            =   "##/##/####"
         PromptChar      =   "_"
      End
      Begin MSMask.MaskEdBox MskFechIni 
         Height          =   315
         Left            =   1080
         TabIndex        =   6
         Top             =   600
         Width           =   1215
         _ExtentX        =   2143
         _ExtentY        =   556
         _Version        =   393216
         Enabled         =   0   'False
         MaxLength       =   10
         Format          =   "dd/mm/yyyy"
         Mask            =   "##/##/####"
         PromptChar      =   "_"
      End
      Begin VB.Label LblHasta 
         Alignment       =   1  'Right Justify
         Caption         =   "Hasta:"
         Height          =   255
         Left            =   2400
         TabIndex        =   10
         Top             =   600
         Width           =   615
      End
      Begin VB.Label LblDesde 
         Alignment       =   1  'Right Justify
         Caption         =   "Desde:"
         Height          =   255
         Left            =   360
         TabIndex        =   9
         Top             =   600
         Visible         =   0   'False
         Width           =   615
      End
   End
   Begin MSComctlLib.ListView LstVBodega 
      Height          =   2535
      Left            =   120
      TabIndex        =   4
      Top             =   3720
      Width           =   7215
      _ExtentX        =   12726
      _ExtentY        =   4471
      View            =   3
      LabelEdit       =   1
      MultiSelect     =   -1  'True
      LabelWrap       =   -1  'True
      HideSelection   =   0   'False
      Checkboxes      =   -1  'True
      FullRowSelect   =   -1  'True
      GridLines       =   -1  'True
      _Version        =   393217
      ForeColor       =   -2147483640
      BackColor       =   -2147483643
      BorderStyle     =   1
      Appearance      =   1
      NumItems        =   0
   End
   Begin MSComctlLib.ListView LstVTercero 
      Height          =   2655
      Left            =   120
      TabIndex        =   2
      Top             =   480
      Width           =   7215
      _ExtentX        =   12726
      _ExtentY        =   4683
      View            =   3
      LabelEdit       =   1
      MultiSelect     =   -1  'True
      LabelWrap       =   -1  'True
      HideSelection   =   0   'False
      AllowReorder    =   -1  'True
      Checkboxes      =   -1  'True
      FullRowSelect   =   -1  'True
      GridLines       =   -1  'True
      _Version        =   393217
      ForeColor       =   -2147483640
      BackColor       =   -2147483643
      BorderStyle     =   1
      Appearance      =   1
      NumItems        =   2
      BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Text            =   "Nit."
         Object.Width           =   2647
      EndProperty
      BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   1
         Text            =   "Tercero"
         Object.Width           =   7409
      EndProperty
   End
   Begin Threed.SSCommand SCmdOpcion 
      Height          =   735
      Left            =   6360
      TabIndex        =   8
      Top             =   6600
      Width           =   855
      _Version        =   65536
      _ExtentX        =   1508
      _ExtentY        =   1296
      _StockProps     =   78
      Caption         =   "&IMPRIMIR"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   6
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      RoundedCorners  =   0   'False
      Picture         =   "FrmRDPendientes.frx":058A
   End
End
Attribute VB_Name = "FrmRDPendientes"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'---------------------------------------------------------------------------------------
' Module    : FrmRDPendientes
' DateTime  : 24/02/2010 15:15
' Author    : jeison_camacho
' Purpose   : busca las requisiciones pendientes por despacho
'---------------------------------------------------------------------------------------
'JACC R2396
Option Explicit
Dim IntCont As Integer

Private Sub ChckBodegas_Click()
   If ChckBodegas.Value = 1 Then
      For IntCont = 1 To LstVBodega.ListItems.Count
         LstVBodega.ListItems(IntCont).Checked = True
      Next
   Else
      For IntCont = 1 To LstVBodega.ListItems.Count
         LstVBodega.ListItems(IntCont).Checked = False
      Next
   End If
End Sub

Private Sub ChckBodegas_KeyPress(KeyAscii As Integer)
   Call Cambiar_Enter(KeyAscii)
End Sub

Private Sub ChkFechas_Click()
   If ChkFechas.Value = Unchecked Then
      MskFechIni.Enabled = False
      MskFechFin.Enabled = False
   Else
      MskFechIni.Enabled = True
      MskFechFin.Enabled = True
   End If
End Sub

Private Sub ChkFechas_KeyPress(KeyAscii As Integer)
   Call Cambiar_Enter(KeyAscii)
End Sub

Private Sub ChkTercero_Click()
   If ChkTercero.Value = 1 Then
      For IntCont = 1 To LstVTercero.ListItems.Count
         LstVTercero.ListItems(IntCont).Checked = True
      Next
   Else
      For IntCont = 1 To LstVTercero.ListItems.Count
         LstVTercero.ListItems(IntCont).Checked = False
      Next
   End If
End Sub

Private Sub ChkTercero_KeyPress(KeyAscii As Integer)
   Call Cambiar_Enter(KeyAscii)
End Sub

Private Sub Form_Load()
   Call CenterForm(MDI_Inventarios, Me)
   Call CargarListas
End Sub

Sub CargarListas()

   Me.LstVBodega.ColumnHeaders.Add , "NOM", "Nombre de la Bodega", 0.66 * Me.LstVBodega.Width
   Me.LstVBodega.ColumnHeaders.Add , "COD", "C�digo", 0.34 * Me.LstVBodega.Width

   
  'Carga unicamente los terceros que tiene requisiciones pendientes
   ReDim ArTerc(1, 0)
   Campos = " DISTINCT CD_CODI_TERC, NO_NOMB_TERC"
   Desde = "TERCERO, IN_ENCABEZADO, IN_DOCUMENTO,IN_DETALLE"
   Condicion = "NU_AUTO_DOCU_ENCA = NU_AUTO_DOCU AND CD_CODI_TERC_ENCA = CD_CODI_TERC AND NU_AUTO_DOCU ="
   Condicion = Condicion & " (select TOP 1 NU_AUTO_DOCUDEPE_DOCU from in_documento where NU_AUTO_DOCU = 6)"
   Condicion = Condicion & " AND CD_CODI_TERC = CD_CODI_TERC_ENCA  AND NU_AUTO_ORGCABE_DETA = NU_AUTO_ENCA"
   'Condicion = Condicion & " GROUP BY  NU_AUTO_ENCA ,CD_CODI_TERC, NO_NOMB_TERC HAVING SUM(NU_SALIDA_DETA-NU_ENTRAD_DETA)<>0" 'DRMG T43252 Se deja en comentario
   Condicion = Condicion & " GROUP BY  NU_AUTO_ENCA ,CD_CODI_TERC, NO_NOMB_TERC " 'DRMG T43252
   Condicion = Condicion & " ORDER BY 2 "
   Result = LoadMulData(Desde, Campos, Condicion, ArTerc)
   For IntCont = 0 To UBound(ArTerc, 2)
       Me.LstVTercero.ListItems.Add , "T" & Trim(ArTerc(0, IntCont)), Trim(ArTerc(0, IntCont))
       Me.LstVTercero.ListItems("T" & Trim(ArTerc(0, IntCont))).ListSubItems.Add , "NAME", Trim(ArTerc(1, IntCont))
   Next

   ReDim ArBodega(2, 0)
   Desde = "IN_BODEGA,IN_R_BODE_COMP,IN_COMPROBANTE"
   Campos = "DISTINCT NU_AUTO_BODE,TX_CODI_BODE, TX_NOMB_BODE"
   Condicion = "NU_AUTO_BODE=NU_AUTO_BODE_RBOCO AND NU_AUTO_COMP_RBOCO=NU_AUTO_COMP"
   Condicion = Condicion & " AND NU_AUTO_DOCU_COMP="
   Condicion = Condicion & " (select TOP 1 NU_AUTO_DOCUDEPE_DOCU from in_documento where NU_AUTO_DOCU = 6)"
   Condicion = Condicion & " ORDER BY 3" 'JACC M6365
   Result = LoadMulData(Desde, Campos, Condicion, ArBodega())
   For IntCont = 0 To UBound(ArBodega, 2)
      Me.LstVBodega.ListItems.Add , "B" & ArBodega(0, IntCont), ArBodega(2, IntCont)
      Me.LstVBodega.ListItems("B" & ArBodega(0, IntCont)).ListSubItems.Add , "COD", ArBodega(1, IntCont)
   Next
   
   MskFechIni.Text = Format(Date, "dd/mm/yyyy")
   MskFechFin.Text = MskFechIni.Text

End Sub

Sub Tabla_imprimir(StTercero As String, StBodega As String)
   Dim StFechaIni As String
   Dim StFechaFin As String
   Dim StFecha As String
   
   On Error GoTo GoErr
   
      Call MouseClock
      If ChkFechas.Value = Unchecked Then
         StFechaIni = FHFECHA(CDate("01/01/1900 00:00:00"))
         StFechaFin = FHFECHA(Now)
      Else
         StFechaIni = FHFECHA(CDate(MskFechIni.Text))
         StFechaFin = FHFECHA(CDate(MskFechFin.Text))
      End If
   
      If ExisteTABLA("REQ_DESP_PEND_TEMP") Then
         Result = ExecSQLCommand("DROP TABLE REQ_DESP_PEND_TEMP")
         If Result = FAIL Then
            Call MouseNorm
            Exit Sub
         End If
      End If
   
      StFecha = "(FE_CREA_ENCA BETWEEN " & StFechaIni & " AND " & StFechaFin & ")"
      
      Campos = "SELECT NU_ORDEN_OEPT,NU_ARTI_OEPT,SUM(CANTDEPACHO) TODEPACHO,SUM(CANTREQUISICION) TOREQUISICION,CD_CODI_TERC_ENCA  INTO REQ_DESP_PEND_TEMP FROM"
      
      Condicion = "(SELECT NU_AUTO_MODCABE_DETA AS NU_ORDEN_OEPT,NU_AUTO_ARTI_DETA AS NU_ARTI_OEPT, SUM(NU_ENTRAD_DETA)AS CANTDEPACHO,0 AS CANTREQUISICION,CD_CODI_TERC_ENCA"
      Condicion = Condicion & " FROM IN_DETALLE,IN_ENCABEZADO WHERE"
      Condicion = Condicion & " NU_AUTO_ORGCABE_DETA IN  (SELECT NU_AUTO_ENCA FROM IN_ENCABEZADO"
      Condicion = Condicion & " Where"
      Condicion = Condicion & " NU_AUTO_DOCU_ENCA = 6"
      'Condicion = Condicion & " AND NU_AUTO_BODEORG_ENCA IN ( " & StBodega & ")"   'JACC M6366
      Condicion = Condicion & " AND CD_CODI_TERC_ENCA IN (" & StTercero & ")"
      Condicion = Condicion & " AND " & StFecha
      Condicion = Condicion & " AND TX_ESTA_ENCA<>'A')"
      Condicion = Condicion & " AND NU_AUTO_ORGCABE_DETA<>NU_AUTO_ENCA"
      Condicion = Condicion & " AND NU_AUTO_MODCABE_DETA=NU_AUTO_ENCA"
      Condicion = Condicion & " AND NU_AUTO_DOCU_ENCA=5"
      Condicion = Condicion & " AND NU_AUTO_BODEORG_ENCA IN ( " & StBodega & ")"
      Condicion = Condicion & " AND CD_CODI_TERC_ENCA IN (" & StTercero & ")"
      Condicion = Condicion & " AND " & StFecha
      Condicion = Condicion & " AND TX_ESTA_ENCA='N'"
      
     'Condicion = Condicion & " AND NU_COMP_ENCA = 20107"
      Condicion = Condicion & " GROUP BY NU_AUTO_MODCABE_DETA,NU_AUTO_ARTI_DETA,NU_ENTRAD_DETA, NU_COSTO_DETA,CD_CODI_TERC_ENCA"
      
      Condicion = Condicion & " Union"
      'Condicion = Condicion & " SELECT NU_AUTO_MODCABE_DETA AS NU_ORDEN_OEPT,NU_AUTO_ARTI_DETA AS NU_ARTI_OEPT, (-1)*SUM(NU_SALIDA_DETA) AS CANTDEPACHO,0 AS CANTREQUISICION,CD_CODI_TERC_ENCA"
      Condicion = Condicion & " SELECT NU_AUTO_MODCABE_DETA AS NU_ORDEN_OEPT,NU_AUTO_ARTI_DETA AS NU_ARTI_OEPT, (-1)*SUM(NU_ENTRAD_DETA) AS CANTDEPACHO,0 AS CANTREQUISICION,CD_CODI_TERC_ENCA" 'JACC M6366
      Condicion = Condicion & " From IN_DETALLE, IN_ENCABEZADO"
      Condicion = Condicion & " Where"
      Condicion = Condicion & " NU_AUTO_ORGCABE_DETA IN"
      Condicion = Condicion & " (SELECT NU_AUTO_ENCA FROM IN_ENCABEZADO"
      Condicion = Condicion & " Where NU_AUTO_DOCU_ENCA = 17 " 'DOCUMENTOS DEVUELTOS
      Condicion = Condicion & " AND NU_AUTO_BODEORG_ENCA IN ( " & StBodega & ")"
      Condicion = Condicion & " AND CD_CODI_TERC_ENCA IN (" & StTercero & ")"
      Condicion = Condicion & " AND " & StFecha
      Condicion = Condicion & " AND TX_ESTA_ENCA='N')"
      Condicion = Condicion & " AND NU_AUTO_ORGCABE_DETA<>NU_AUTO_ENCA"
      Condicion = Condicion & " AND NU_AUTO_MODCABE_DETA=NU_AUTO_ENCA"
      Condicion = Condicion & " AND NU_AUTO_DOCU_ENCA=5"
      Condicion = Condicion & " AND NU_AUTO_BODEORG_ENCA IN ( " & StBodega & ")"
      Condicion = Condicion & " AND CD_CODI_TERC_ENCA IN (" & StTercero & ")"
      Condicion = Condicion & " AND " & StFecha
      Condicion = Condicion & " AND TX_ESTA_ENCA='N'"
      
     'Condicion = Condicion & " AND NU_COMP_ENCA = 20107"
      Condicion = Condicion & " GROUP BY NU_AUTO_MODCABE_DETA,NU_AUTO_ARTI_DETA,NU_ENTRAD_DETA, NU_COSTO_DETA,CD_CODI_TERC_ENCA"

      Condicion = Condicion & " Union"
      Condicion = Condicion & " SELECT NU_AUTO_ENCA AS NU_ORDEN_OEPT, NU_AUTO_ARTI_DETA AS NU_ARTI_OEPT, 0 AS CANTDEPACHO,NU_SALIDA_DETA AS CANTREQUISICION,CD_CODI_TERC_ENCA"
      Condicion = Condicion & " From IN_DETALLE, IN_ENCABEZADO"
      Condicion = Condicion & " Where"
      Condicion = Condicion & " NU_AUTO_MODCABE_DETA = NU_AUTO_ENCA"
      Condicion = Condicion & " AND NU_AUTO_ORGCABE_DETA=NU_AUTO_ENCA"
      Condicion = Condicion & " AND NU_AUTO_DOCU_ENCA=5"
      Condicion = Condicion & " AND NU_AUTO_BODEORG_ENCA IN ( " & StBodega & ")"
      Condicion = Condicion & " AND CD_CODI_TERC_ENCA IN (" & StTercero & ")"
      Condicion = Condicion & " AND " & StFecha
      Condicion = Condicion & " AND TX_ESTA_ENCA='N'"
      
      'Condicion = Condicion & " AND NU_COMP_ENCA = 20107"
      Condicion = Condicion & " GROUP BY NU_AUTO_ENCA,NU_AUTO_ARTI_DETA ,NU_SALIDA_DETA,CD_CODI_TERC_ENCA"
      Condicion = Condicion & " ) AS T2"
      Condicion = Condicion & " GROUP BY NU_ORDEN_OEPT,NU_ARTI_OEPT,CD_CODI_TERC_ENCA  "
      Condicion = Condicion & " Having Sum(CANTDEPACHO) <> Sum(CANTREQUISICION) And Sum(CANTDEPACHO) < Sum(CANTREQUISICION)"
      Condicion = Condicion & " ORDER BY 1,2"
      Result = ExecSQLCommand(Campos & Condicion, "N")

      Call MouseNorm
    Exit Sub
GoErr:
    
    Call ConvertErr
    Call MouseNorm
End Sub


Private Sub LstVBodega_ColumnClick(ByVal ColumnHeader As MSComctlLib.ColumnHeader) 'JACC M6365
    LstVBodega.SortKey = ColumnHeader.Index - 1
    LstVBodega.Sorted = True
End Sub

Private Sub LstVBodega_KeyPress(KeyAscii As Integer)
   Call Cambiar_Enter(KeyAscii)
End Sub

Private Sub LstVTercero_ColumnClick(ByVal ColumnHeader As MSComctlLib.ColumnHeader) 'JACC M6365
    LstVTercero.SortKey = ColumnHeader.Index - 1
    LstVTercero.Sorted = True
End Sub

Private Sub LstVTercero_KeyPress(KeyAscii As Integer)
   Call Cambiar_Enter(KeyAscii)
End Sub

Private Sub MskFechFin_KeyPress(KeyAscii As Integer)
   Call Cambiar_Enter(KeyAscii)
End Sub

Private Sub MskFechFin_LostFocus()
  If ChkFechas.Value = Checked Then
      If ValFecha(MskFechFin, 1) <> FAIL Then
         If DateDiff("d", CDate(MskFechFin.Text), CDate(MskFechIni.Text)) > 0 Then
            Call Mensaje1("La fecha hasta no puede ser menor a la fecha desde", 3)
            MskFechFin.Text = MskFechIni.Text
         End If
      End If
   End If
End Sub

Private Sub MskFechIni_KeyPress(KeyAscii As Integer)
   Call Cambiar_Enter(KeyAscii)
End Sub

Private Sub MskFechIni_LostFocus()
   If ChkFechas.Value = Checked Then
      If ValFecha(MskFechIni, 1) <> FAIL Then
         If DateDiff("d", CDate(MskFechFin.Text), CDate(MskFechIni.Text)) > 0 Then
            Call Mensaje1("La fecha desde no puede ser mayor a la fecha hasta", 3)
            MskFechIni.Text = MskFechFin.Text
         End If
      End If
   End If
End Sub

Private Sub SCmdOpcion_Click()
   Dim StBodega As String
   Dim StTercero As String
   Dim InA As Integer 'DRMG T43252 Se crea variable para saber cuantos terceros fueron seleccionados
   
   For IntCont = 1 To LstVTercero.ListItems.Count
      If LstVTercero.ListItems(IntCont).Checked Then
         If StTercero <> NUL$ Then StTercero = StTercero & Coma
         StTercero = StTercero & Comi & LstVTercero.ListItems(IntCont).Text & Comi
         InA = InA + 1
      End If
   Next

   If StTercero = NUL$ Then
      Call Mensaje1("Debe seleccionar los terceros ", 2)
      Exit Sub
   End If
   'DRMG T43252 INICIO Se agrega validacion en cual sirve para deterner el proceso si llegara a ver mas de 4000 terceros
   If InA > 4000 Then
      Call Mensaje1("Por la cantidad de registros se puede generar inconvenientes con el motor de BD y no se genera el informe." _
      & vbCrLf & "Seleccione par�metros m�s precisos.", 1)
      Exit Sub
   End If
   'DRMG T43252 FIN
   
   For IntCont = 1 To LstVBodega.ListItems.Count
      If LstVBodega.ListItems(IntCont).Checked Then
         If StBodega <> NUL$ Then StBodega = StBodega & ","
         StBodega = StBodega & Mid(LstVBodega.ListItems(IntCont).Key, 2)
      End If
   Next
   
   If StBodega = NUL$ Then
      Call Mensaje1("Debe seleccionar como m�nimo una bodega ", 2)
      Exit Sub
   End If
   
   Call Tabla_imprimir(StTercero, StBodega)
   If Result <> FAIL Then
         Call Limpiar_CrysListar
         With MDI_Inventarios.Crys_Listar
      
            .Formulas(0) = "ENTIDAD= " & Comi & Entidad & Comi
            .Formulas(1) = "NIT= 'Nit. " & Nit & Comi
            '--nombre de reporte
            .ReportFileName = App.Path & "\InfRDPendiente.rpt"
            
            .Destination = crptToWindow
            .WindowTitle = "Informe de requisiciones pendientes"
            .PassWord = Chr(10) & UserPwdBD
            .Action = 1
            .Formulas(0) = NUL$
            .Formulas(1) = NUL$
         End With
   End If
   
End Sub
