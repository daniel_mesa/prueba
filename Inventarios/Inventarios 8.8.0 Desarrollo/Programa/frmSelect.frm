VERSION 5.00
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "MSFLXGRD.OCX"
Begin VB.Form frmSelect 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Selecci�n de registros"
   ClientHeight    =   4785
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   8385
   ForeColor       =   &H00000000&
   Icon            =   "frmSelect.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   ScaleHeight     =   4785
   ScaleWidth      =   8385
   StartUpPosition =   3  'Windows Default
   Begin VB.Frame fraResult 
      Height          =   3975
      Left            =   120
      TabIndex        =   7
      Top             =   720
      Width           =   8175
      Begin VB.CommandButton cmdOk 
         Caption         =   "&ACEPTAR"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   6
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   495
         Left            =   4920
         Picture         =   "frmSelect.frx":058A
         Style           =   1  'Graphical
         TabIndex        =   2
         Top             =   3240
         Width           =   1335
      End
      Begin VB.CommandButton cmdCancel 
         Cancel          =   -1  'True
         Caption         =   "&CANCELAR"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   6
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   495
         Left            =   6720
         Picture         =   "frmSelect.frx":08CC
         Style           =   1  'Graphical
         TabIndex        =   3
         Top             =   3240
         Width           =   1335
      End
      Begin MSFlexGridLib.MSFlexGrid grdSeleccion 
         Height          =   2655
         Left            =   120
         TabIndex        =   1
         Top             =   480
         Width           =   7935
         _ExtentX        =   13996
         _ExtentY        =   4683
         _Version        =   393216
         Cols            =   3
         FixedCols       =   0
         SelectionMode   =   1
      End
      Begin VB.Shape shpBox 
         FillStyle       =   4  'Upward Diagonal
         Height          =   2535
         Left            =   120
         Top             =   600
         Width           =   7935
      End
      Begin VB.Label lblMsg 
         AutoSize        =   -1  'True
         Caption         =   "Escriba el criterio de b�squeda."
         Height          =   195
         Left            =   120
         TabIndex        =   9
         Top             =   3240
         Width           =   4395
         WordWrap        =   -1  'True
      End
      Begin VB.Label lblTitle 
         Alignment       =   2  'Center
         Caption         =   "Selecci�n"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   120
         TabIndex        =   8
         Top             =   240
         Width           =   7935
      End
      Begin VB.Image imgLib 
         Height          =   180
         Index           =   0
         Left            =   2640
         Picture         =   "frmSelect.frx":0C0E
         Top             =   480
         Visible         =   0   'False
         Width           =   180
      End
   End
   Begin VB.Frame fraSelect 
      Height          =   650
      Left            =   120
      TabIndex        =   4
      Top             =   0
      Width           =   8175
      Begin VB.TextBox txtCriterio 
         Height          =   285
         Left            =   3000
         TabIndex        =   0
         Text            =   "%"
         Top             =   240
         Width           =   4935
      End
      Begin VB.OptionButton optM 
         Caption         =   "Nombre"
         Height          =   255
         Index           =   1
         Left            =   1200
         TabIndex        =   5
         Top             =   240
         Value           =   -1  'True
         Width           =   1215
      End
      Begin VB.OptionButton optM 
         Caption         =   "C�digo"
         Height          =   255
         Index           =   0
         Left            =   120
         TabIndex        =   6
         Top             =   240
         Width           =   1215
      End
   End
End
Attribute VB_Name = "frmSelect"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Dim fResultado As Boolean
Dim cTabla As String, cCampos As String, cCond As String
Dim cCampo1, cCampo2
Dim fMult As Boolean
Const vLim As Integer = 300 'N�mero m�ximo de registros a mostrar en la grilla
Dim aTemp()
'Realiza una selecci�n m�ltiple o sencilla seg�n lo especifique en el par�metro opcional fMultX, si se omite
'  se asume que es una selecci�n sencilla, en cualquier caso la matriz se rellenar� del mismo modo, s�lo varian
'  el n�mero de elementos. En la selecci�n sencilla solo encontrar� aSelecC(0) y aSelectD(0).
'La matriz aSelectC contiene el c�digo (es la principal) y aSelectD contiene nombres/descripciones, pretende
'  evitar rehacer una consulta para obtener esta informaci�n para mostrarla al usuario.
'Debe especificar 2 campos, siempre se tratar� el primero como un c�digo y el segundo como una descripci�n.
Function fnSeleccionar(cTablaX As String, cCamposX As String, Optional cCondX As String, Optional fMultX As Boolean, Optional cT�tulo As String) As Boolean '|.DR.|
'Esta funci�n rellenar� la matriz global aSelectC (c�digos) y aSelectD (descripci�n/nombre),
'   con el registro o registros seleccionados.
'Si el usuario selecciona cancelar, la matriz no ser� modificada ni redimensionada.
'Al dar clic en aceptar, la matriz se redimensionar� y sus valores cambiar�n.
'Si se inicia una selecci�n m�ltiple y no se seleccionan registros, la matriz quedara de tama�o 0,
'  y con <empty>.
'La funci�n devuelve TRUE si se hizo alguna selecci�n y se hizo clic en ACEPTAR, de lo contrario
'  retorna FALSE. (Es decir que si se trata de una selecci�n m�ltiple y no se selecciona ning�n elemento y
'  se hace clic en aceptar retornar� FALSE).

    cTabla = cTablaX
    cCampos = cCamposX
    cCond = Trim(cCondX)
    fMult = fMultX
    cCampo1 = Mid(cCampos, 1, InStr(1, cCampos, ",") - 1)
    cCampo2 = Mid(cCampos, InStr(1, cCampos, ",") + 1)
    If Trim(cT�tulo) <> "" Then lblTitle = cT�tulo
    
    With grdSeleccion
        .TextMatrix(0, 0) = "C�digo"
        .TextMatrix(0, 1) = "Nombre/descripci�n"
        .ColWidth(0) = 1000
        .ColWidth(1) = IIf(fMult, 6200, 7000)
        .ColWidth(2) = IIf(fMult, 250, 1)
        .ScrollBars = 2 'IIf(fMult, 3, 2)
        .SelectionMode = 1 - Abs(fMult)
    End With
    
    fResultado = False
    Me.Show 1
    fnSeleccionar = fResultado
    
End Function


Private Sub cmdCancel_Click()
    Unload Me
End Sub


Private Sub cmdOk_Click()

    Dim I&, J&

With grdSeleccion
    If fMult Then
        ReDim aSelectC(0), aSelectD(0)
        
        For I& = 0 To .Rows - 1
            If .TextMatrix(I&, 2) = " " Then
                aSelectC(J&) = .TextMatrix(I&, 0)
                aSelectD(J&) = .TextMatrix(I&, 1)
                ReDim Preserve aSelectC(J& + 1), aSelectD(J& + 1)
                J& = J& + 1
            End If
        Next I&
            
            fResultado = J& > 0
            If fResultado Then ReDim Preserve aSelectC(J& - 1), aSelectD(J& - 1)
                
    Else
        ReDim aSelectC(0), aSelectD(0)
        aSelectC(0) = .TextMatrix(.Row, 0)
        aSelectD(0) = .TextMatrix(.Row, 1)
        fResultado = True
    End If
    
    Unload Me
    
End With

End Sub

Private Sub Form_Load()
    CenterForm MDI_Inventarios, Me
End Sub


Private Sub grdSeleccion_Click()

If fMult And Trim(grdSeleccion.TextMatrix(grdSeleccion.Row, 0) <> "") Then
        With grdSeleccion
            If .Col = 2 And .Row > 0 Then
                If .Text = " " Then
                    .Col = 2 'Al arrastrar puede aplicarlo a columnas no deseadas.
                    Set .CellPicture = Nothing
                    .Text = ""
                Else
                    .Col = 2 'Al arrastrar puede aplicarlo a columnas no deseadas.
                    Set .CellPicture = imgLib(0).Picture
                    .CellPictureAlignment = 3
                    .Text = " "
                End If
            End If
        End With
End If

End Sub


Private Sub grdSeleccion_DblClick()
    
    If Not fMult Then cmdOk_Click
    
End Sub


Private Sub grdSeleccion_KeyUp(KeyCode As Integer, Shift As Integer)
        
        If KeyCode = 32 Then grdSeleccion_Click
        If KeyCode = 13 Then cmdOk_Click
        
End Sub


Private Sub txtCriterio_GotFocus()
        
        txtCriterio.SelStart = 0
        txtCriterio.SelLength = 32000
        
End Sub

Private Sub txtCriterio_KeyUp(KeyCode As Integer, Shift As Integer)
    
    Dim I%, vLimite As Long, fUpd As Boolean
    
    ReDim aTemp(1, 0)
        
    If KeyCode = 13 Then
        LoadMulData cTabla, cCampos, IIf(optM(1).Value, cCampo2, cCampo1) & " Like '" & txtCriterio & "%'" & IIf(cCond = "", "", " And " & cCond) & " Order By " & cCampo2, aTemp
        
        vLimite = UBound(aTemp, 2)
        If vLimite > vLim Then vLimite = vLim: fUpd = True
        
        grdSeleccion.Rows = 1
        grdSeleccion.Visible = False
        For I% = 0 To vLimite
                    grdSeleccion.AddItem aTemp(0, I%) & vbTab & aTemp(1, I%)
        Next I%
        grdSeleccion.Visible = True
        
        If fUpd Then
            sbVMsg lblMsg, "El resultado es muy extenso, s�lo se muestran los primeros " & vLim & " registros.", -&HAA
            grdSeleccion.SetFocus
        ElseIf Encontro Then
            sbVMsg lblMsg, "Se encontraron " & vLimite + 1 & " registros.", 0
            grdSeleccion.SetFocus
        Else
            sbVMsg lblMsg, "No hay resultados.", -&H40C0&
            grdSeleccion.Rows = 1
        End If
        
    End If
    
End Sub


