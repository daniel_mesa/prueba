VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Object = "{C932BA88-4374-101B-A56C-00AA003668DC}#1.1#0"; "MSMASK32.OCX"
Object = "{0BA686C6-F7D3-101A-993E-0000C0EF6F5E}#1.0#0"; "THREED32.OCX"
Begin VB.Form frmPrmREPODOC 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Parametros para DOCUMENTOS"
   ClientHeight    =   7095
   ClientLeft      =   45
   ClientTop       =   435
   ClientWidth     =   11475
   Icon            =   "frmPrmREPODOC.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   7095
   ScaleWidth      =   11475
   Begin VB.CheckBox chkTERCEROS 
      Caption         =   "Todos los Terceros"
      Height          =   375
      Left            =   600
      TabIndex        =   2
      Top             =   840
      Width           =   3015
   End
   Begin VB.ComboBox cboDOCUM 
      Height          =   315
      Left            =   3240
      Style           =   2  'Dropdown List
      TabIndex        =   0
      Top             =   240
      Width           =   5295
   End
   Begin VB.Frame fraRANGO 
      Height          =   6255
      Left            =   6360
      TabIndex        =   6
      Top             =   720
      Width           =   4815
      Begin VB.Frame FrmDepe 
         Enabled         =   0   'False
         Height          =   1215
         Left            =   120
         TabIndex        =   33
         Top             =   1440
         Width           =   4575
         Begin VB.CheckBox ChkDepe 
            Caption         =   "Condicionar las dependencias del reporte"
            Height          =   315
            Left            =   120
            TabIndex        =   11
            Top             =   240
            Width           =   4095
         End
         Begin MSMask.MaskEdBox txtdepfin 
            Height          =   315
            Left            =   3120
            TabIndex        =   13
            Top             =   600
            Width           =   1215
            _ExtentX        =   2143
            _ExtentY        =   556
            _Version        =   393216
            PromptInclude   =   0   'False
            MaxLength       =   11
            PromptChar      =   "_"
         End
         Begin MSMask.MaskEdBox txtdepini 
            Height          =   315
            Left            =   1080
            TabIndex        =   12
            Top             =   600
            Width           =   1215
            _ExtentX        =   2143
            _ExtentY        =   556
            _Version        =   393216
            PromptInclude   =   0   'False
            MaxLength       =   11
            PromptChar      =   "_"
         End
         Begin VB.Label LblHasta 
            Alignment       =   1  'Right Justify
            Caption         =   "Hasta:"
            Height          =   255
            Left            =   2280
            TabIndex        =   35
            Top             =   600
            Width           =   735
         End
         Begin VB.Label LablDesde 
            Alignment       =   1  'Right Justify
            Caption         =   "Desde:"
            Height          =   255
            Left            =   240
            TabIndex        =   34
            Top             =   600
            Visible         =   0   'False
            Width           =   735
         End
      End
      Begin VB.Frame Frame5 
         Height          =   615
         Left            =   120
         TabIndex        =   29
         Top             =   5520
         Width           =   3225
         Begin VB.OptionButton opcRES 
            Caption         =   "Resumido"
            Height          =   315
            Left            =   240
            TabIndex        =   31
            Top             =   200
            Value           =   -1  'True
            Width           =   1300
         End
         Begin VB.OptionButton opcDET 
            Caption         =   "Detallado"
            Height          =   315
            Left            =   1800
            TabIndex        =   30
            Top             =   200
            Width           =   1300
         End
      End
      Begin VB.Frame Frame4 
         Height          =   1215
         Left            =   120
         TabIndex        =   26
         Top             =   2760
         Width           =   4575
         Begin VB.CheckBox chkCPRANGO 
            Caption         =   "Condicionar rango de Comprobantes"
            Height          =   315
            Left            =   120
            TabIndex        =   14
            Top             =   240
            Width           =   4095
         End
         Begin MSMask.MaskEdBox txtCPHASTA 
            Height          =   315
            Left            =   3120
            TabIndex        =   16
            Top             =   600
            Width           =   1215
            _ExtentX        =   2143
            _ExtentY        =   556
            _Version        =   393216
            PromptInclude   =   0   'False
            MaxLength       =   7
            PromptChar      =   "_"
         End
         Begin MSMask.MaskEdBox txtCPDESDE 
            Height          =   315
            Left            =   1080
            TabIndex        =   15
            Top             =   600
            Width           =   1215
            _ExtentX        =   2143
            _ExtentY        =   556
            _Version        =   393216
            PromptInclude   =   0   'False
            MaxLength       =   7
            PromptChar      =   "_"
         End
         Begin VB.Label Label3 
            Alignment       =   1  'Right Justify
            Caption         =   "Desde:"
            Height          =   255
            Left            =   240
            TabIndex        =   28
            Top             =   600
            Visible         =   0   'False
            Width           =   735
         End
         Begin VB.Label Label2 
            Alignment       =   1  'Right Justify
            Caption         =   "Hasta:"
            Height          =   255
            Left            =   2280
            TabIndex        =   27
            Top             =   600
            Width           =   735
         End
      End
      Begin VB.Frame Frame2 
         Height          =   615
         Left            =   120
         TabIndex        =   21
         Top             =   4080
         Width           =   4545
         Begin VB.OptionButton opcANU 
            Caption         =   "Anulados"
            Height          =   315
            Left            =   3120
            TabIndex        =   32
            Top             =   200
            Width           =   1185
         End
         Begin VB.OptionButton opcMOD 
            Caption         =   "Todos"
            Height          =   315
            Left            =   240
            TabIndex        =   17
            Top             =   200
            Value           =   -1  'True
            Width           =   1065
         End
         Begin VB.OptionButton opcORG 
            Caption         =   "No anulados"
            Height          =   315
            Left            =   1440
            TabIndex        =   18
            Top             =   200
            Width           =   1545
         End
      End
      Begin VB.Frame Frame1 
         Height          =   615
         Left            =   120
         TabIndex        =   7
         Top             =   4920
         Width           =   3225
         Begin VB.OptionButton opcSIN 
            Caption         =   "Sin Costear"
            Height          =   315
            Left            =   1800
            TabIndex        =   20
            Top             =   200
            Width           =   1300
         End
         Begin VB.OptionButton opcCON 
            Caption         =   "Costeado"
            Height          =   315
            Left            =   240
            TabIndex        =   19
            Top             =   200
            Value           =   -1  'True
            Width           =   1300
         End
      End
      Begin Threed.SSCommand cmdIMPRIMIR 
         Height          =   735
         Left            =   3720
         TabIndex        =   22
         Top             =   5160
         Width           =   855
         _Version        =   65536
         _ExtentX        =   1508
         _ExtentY        =   1296
         _StockProps     =   78
         Caption         =   "&IMPRIMIR"
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   6
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         RoundedCorners  =   0   'False
         Picture         =   "frmPrmREPODOC.frx":058A
      End
      Begin VB.Frame Frame3 
         Height          =   1215
         Left            =   120
         TabIndex        =   23
         Top             =   120
         Width           =   4575
         Begin VB.CheckBox chkFCRANGO 
            Caption         =   "Condicionar las fechas del reporte"
            Height          =   315
            Left            =   120
            TabIndex        =   8
            Top             =   240
            Width           =   4095
         End
         Begin MSMask.MaskEdBox txtFCHASTA 
            Height          =   315
            Left            =   3120
            TabIndex        =   10
            Top             =   600
            Width           =   1215
            _ExtentX        =   2143
            _ExtentY        =   556
            _Version        =   393216
            PromptChar      =   "_"
         End
         Begin MSMask.MaskEdBox txtFCDESDE 
            Height          =   315
            Left            =   1080
            TabIndex        =   9
            Top             =   600
            Width           =   1215
            _ExtentX        =   2143
            _ExtentY        =   556
            _Version        =   393216
            PromptChar      =   "_"
         End
         Begin VB.Label Label1 
            Alignment       =   1  'Right Justify
            Caption         =   "Hasta:"
            Height          =   255
            Left            =   2280
            TabIndex        =   25
            Top             =   600
            Width           =   735
         End
         Begin VB.Label lblDESDE 
            Alignment       =   1  'Right Justify
            Caption         =   "Desde:"
            Height          =   255
            Left            =   240
            TabIndex        =   24
            Top             =   600
            Visible         =   0   'False
            Width           =   735
         End
      End
   End
   Begin MSComctlLib.ProgressBar pgbLLEVO 
      Height          =   255
      Left            =   6480
      TabIndex        =   4
      Top             =   240
      Visible         =   0   'False
      Width           =   4455
      _ExtentX        =   7858
      _ExtentY        =   450
      _Version        =   393216
      Appearance      =   1
      Max             =   1
   End
   Begin MSComctlLib.ImageList imgBotones 
      Left            =   10920
      Top             =   3960
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   20
      ImageHeight     =   14
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   7
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmPrmREPODOC.frx":0C54
            Key             =   ""
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmPrmREPODOC.frx":0FA6
            Key             =   "ASG"
         EndProperty
         BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmPrmREPODOC.frx":1260
            Key             =   "CAN"
         EndProperty
         BeginProperty ListImage4 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmPrmREPODOC.frx":15B2
            Key             =   "SAV"
         EndProperty
         BeginProperty ListImage5 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmPrmREPODOC.frx":18CC
            Key             =   "ADD"
         EndProperty
         BeginProperty ListImage6 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmPrmREPODOC.frx":1B86
            Key             =   "DEL"
         EndProperty
         BeginProperty ListImage7 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmPrmREPODOC.frx":1FC8
            Key             =   "CHA"
         EndProperty
      EndProperty
   End
   Begin MSComctlLib.ListView lstBODEGAS 
      Height          =   2535
      Left            =   480
      TabIndex        =   3
      Top             =   4320
      Width           =   5655
      _ExtentX        =   9975
      _ExtentY        =   4471
      View            =   3
      LabelEdit       =   1
      MultiSelect     =   -1  'True
      LabelWrap       =   -1  'True
      HideSelection   =   0   'False
      FullRowSelect   =   -1  'True
      GridLines       =   -1  'True
      _Version        =   393217
      ForeColor       =   -2147483640
      BackColor       =   -2147483643
      BorderStyle     =   1
      Appearance      =   1
      NumItems        =   0
   End
   Begin MSComctlLib.ListView lstTERCEROS 
      Height          =   2655
      Left            =   480
      TabIndex        =   1
      Top             =   1320
      Width           =   5655
      _ExtentX        =   9975
      _ExtentY        =   4683
      View            =   3
      LabelEdit       =   1
      MultiSelect     =   -1  'True
      LabelWrap       =   -1  'True
      HideSelection   =   0   'False
      AllowReorder    =   -1  'True
      FullRowSelect   =   -1  'True
      GridLines       =   -1  'True
      _Version        =   393217
      ForeColor       =   -2147483640
      BackColor       =   -2147483643
      BorderStyle     =   1
      Appearance      =   1
      NumItems        =   2
      BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Text            =   "Nombre del Art�culo"
         Object.Width           =   7761
      EndProperty
      BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   1
         Text            =   "C�digo"
         Object.Width           =   3881
      EndProperty
   End
   Begin VB.Label txtCOMP 
      Alignment       =   1  'Right Justify
      Caption         =   "Tipo de Documentos:"
      Height          =   315
      Left            =   600
      TabIndex        =   5
      Top             =   240
      Width           =   2415
   End
End
Attribute VB_Name = "frmPrmREPODOC"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Dim ArrUXB() As Variant
Dim Tercero As ElTercero, Docume As ElEncabezado
Dim Dueno As New ElTercero
Dim CLLDeDocume As New Collection
'Dim LaBodega As String, LaAccion As String
Dim LaAccion As String      'DEPURACION DE CODIGO
'JLPB T38376 INICIO Se deja en comentario la siguiente liena
'Dim CnTdr As Integer, NumEnCombo As Integer
Dim CnTdr As Double
Dim NumEnCombo As Integer
'JLPB T38376 FIN
Public OpcCod        As String   'Opci�n de seguridad
'NU_AUTO_ARTI, CD_CODI_ARTI, NO_NOMB_ARTI, NU_AUTO_UNVE_ARTI, TX_VENCE_ARTI, TX_PARA_ARTI,
'TX_ENTRA_ARTI, CD_GRUP_ARTI, CD_USOS_ARTI, VL_ULCO_ARTI, VL_COPR_ARTI, CT_EXIS_ARTI, DE_OBSE_ARTI, ID_TIPO_ARTIID_TIPO_ARTI, CD_RIPS_ARTI, NU_INDPYP_ARTI
Dim BoIntCont As Boolean

Private Sub cboDocum_GotFocus()
    NumEnCombo = Me.cboDOCUM.ListIndex
End Sub
'GAPM R2427 CREACION DE PROCEDIMIENTO
Private Sub cboDOCUM_KeyPress(KeyAscii As Integer)
    Call Cambiar_Enter(KeyAscii) 'GAPM R2427 COLOCO ENTER COMO ENTE DE TABULACION
End Sub

Private Sub chkCPRANGO_Click()
    Me.txtCPDESDE.Enabled = False
    Me.txtCPHASTA.Enabled = False
    If Not CBool(Me.chkCPRANGO.Value) = True Then Exit Sub
    Me.txtCPDESDE.Enabled = True
    Me.txtCPHASTA.Enabled = True
End Sub
'GAPM R2427 CREACION DE PROCEDIMIENTO
Private Sub chkCPRANGO_KeyPress(KeyAscii As Integer)
    Call Cambiar_Enter(KeyAscii) 'GAPM R2427 COLOCO ENTER COMO ENTE DE TABULACION
End Sub

Private Sub ChkDepe_Click()
    'GAPM R2427 INICIO
    'DESHABILITO O HABILITO LOS CONTROLES
    Me.txtdepini.Enabled = False
    Me.txtdepfin.Enabled = False
    If Not CBool(Me.ChkDepe.Value) = True Then Exit Sub
    Me.txtdepini.Enabled = True
    Me.txtdepfin.Enabled = True
    'GAPM R2427 FIN
End Sub
'GAPM R2427 CREACION DE PROCEDIMIENTO
Private Sub ChkDepe_KeyPress(KeyAscii As Integer)
    Call Cambiar_Enter(KeyAscii) 'GAPM R2427 COLOCO ENTER COMO ENTE DE TABULACION
End Sub


Private Sub chkfcRANGO_Click()
    Me.txtFCDESDE.Enabled = False
    Me.txtFCHASTA.Enabled = False
    If Not CBool(Me.chkFCRANGO.Value) = True Then Exit Sub
    Me.txtFCDESDE.Enabled = True
    Me.txtFCHASTA.Enabled = True
End Sub


Private Sub chkFCRANGO_KeyPress(KeyAscii As Integer)
    Call Cambiar_Enter(KeyAscii) 'GAPM R2427 COLOCO ENTER COMO ENTE DE TABULACION
End Sub
'GAPM R2427 CREACION DE PROCEDIMIENTO
Private Sub chkTERCEROS_KeyPress(KeyAscii As Integer)
    Call Cambiar_Enter(KeyAscii) 'GAPM R2427 COLOCO ENTER COMO ENTE DE TABULACION
End Sub

Private Sub CmdImprimir_Click()
        If Me.cboDOCUM.ListIndex < 0 Then Exit Sub
    Call Limpiar_CrysListar
    Dim Renglon As MSComctlLib.ListItem, txtDR As String * 1
    Dim strTERCEROS As String, strBODEGAS As String
    Dim strTITRANGO As String, strTITFECHA As String
    Dim StTITDEP As String
    
    Dim StVal As String 'GAPM R2427 VARIABLE PARA QUE CAPTURE SI SON COMPROBANTES O REPORTES
    StVal = "" 'GAPM R2427 INICIALIZACION DE LA VARIABLE
    
    'GAPM M6757 INICIO
    'Si se lecciona la opcion de dependencia, y no se digita nada, muestre el mensaje y termine el mensaje
    If Me.ChkDepe.Value = 1 And (txtdepini = NUL$ Or txtdepfin = NUL$) Then
        'Mensaje1 "Debe Ingresar los datos completos enla parte de dependencia", 3
        Mensaje1 "No ha condicionado las dependencias para realizar la consulta ", 3
        txtdepini.SetFocus
        Exit Sub
    End If
    'GAPM M6757 FIN
    
    If Me.chkTERCEROS.Value = 0 Then
        For Each Renglon In Me.lstTERCEROS.ListItems
            If Renglon.Selected Then strTERCEROS = strTERCEROS & IIf(Len(strTERCEROS) > 0, " OR ", "") & "{TERCERO.CD_CODI_TERC}='" & Renglon.Text & "'"
        Next
    End If
    For Each Renglon In Me.lstBODEGAS.ListItems
        If Renglon.Selected Then strBODEGAS = strBODEGAS & IIf(Len(strBODEGAS) > 0, " OR ", "") & "{IN_BODEGA.TX_CODI_BODE}='" & Renglon.ListSubItems("COD").Text & "'"
    Next
    If Len(strTERCEROS) > 0 Then strTERCEROS = "(" & strTERCEROS & ")"
    If Len(strBODEGAS) > 0 Then strBODEGAS = "(" & strBODEGAS & ")"
    If Len(strTERCEROS) > 0 And Len(strBODEGAS) > 0 Then
        strTERCEROS = strTERCEROS & " AND " & strBODEGAS
    Else
        strTERCEROS = strTERCEROS & strBODEGAS
    End If
    Set Docume = CLLDeDocume.Item("D" & Me.cboDOCUM.ItemData(Me.cboDOCUM.ListIndex))
    strTITRANGO = "TODOS LOS COMPROBANTES"
    strTITFECHA = "A LA FECHA"
    If CBool(Me.chkFCRANGO.Value) Then
        strTERCEROS = strTERCEROS & IIf(Len(strTERCEROS) > 0, " AND ", "") & _
            "({IN_ENCABEZADO.FE_CREA_ENCA} in CDate('" & Me.txtFCDESDE.Text & "') to CDate('" & Me.txtFCHASTA.Text & "'))"
        strTITFECHA = "RANGO DE FECHAS: Del " & Me.txtFCDESDE.Text & " al " & Me.txtFCHASTA.Text
    End If
    MDI_Inventarios.Crys_Listar.Formulas(8) = "RAGFecha='" & strTITFECHA & Comi
    
    If CBool(Me.chkCPRANGO.Value) Then
        'strTERCEROS = strTERCEROS & IIf(Len(strTERCEROS) > 0, " AND ", "") & _
        '    "({IN_ENCABEZADO.NU_COMP_ENCA} in " & Me.txtCPDESDE.Text & " to " & Me.txtCPHASTA.Text & ")" 'GAPM R2427 SE DEJA EN COMENTARIO
        
        'APGR M1576- INICIO SE VERIFICA QUE LOS CAMPOS TENGAN DATOS.
        If Me.txtCPDESDE.Text <> NUL$ And Me.txtCPDESDE.Text <> NUL$ Then
             StVal = StVal & IIf(Len(strTERCEROS) > 0, " AND ", "") & _
                  "({IN_ENCABEZADO.NU_COMP_ENCA} in " & Me.txtCPDESDE.Text & " to " & Me.txtCPHASTA.Text & ")" 'GAPM R2427 SE CAPTURA LA FORMULA
                 strTITRANGO = "RANGO DE COMPROBANTES: Del " & Right(String(6, "0") & Trim(Me.txtCPDESDE.Text), 6) & " al " & Right(String(6, "0") & Trim(Me.txtCPHASTA.Text), 6)
        Else
             Call Mensaje1("Debe indicar un rango de Comprobantes.", 1): Exit Sub
        End If
         'APGR M1576- FIN
    End If
    'GAPM R2427 INICIO
    'REALIZO LA FORMULA PARA EL CAMPO DE LOS NUEVOS REPORTES
    If CBool(Me.ChkDepe.Value) Then
        'SE PONE LA FORMULA DE ORDENAMIENTO
        StVal = "" 'SE BORRA LA FORMULA SI SE TENIA SELECCIONADO LA OPCION DE COMPROBANTES
        'GAPM M7002 INICIO
        'StVal = StVal & IIf(Len(strTERCEROS) > 0, " AND ", "") & _
            "({MOVIMIENTOS.CD_CECO_MOVI} in " & Comi & Me.txtdepini.Text & Comi & " to " & Comi & Me.txtdepfin.Text & Comi & ")"'GAPM M7002 COMENTARIO
        'StTITDEP = "RANGO DE DEPENDENCIAS: Del " & Right(String(6, "0") & Trim(Me.txtdepini.Text), 6) & " al " & Right(String(6, "0") & Trim(Me.txtdepfin.Text), 6)'GAPM M7002 COMENTARIO
        StVal = StVal & IIf(Len(strTERCEROS) > 0, " AND ", "")
        'StVal = StVal & "({MOVIMIENTOS.CD_CECO_MOVI} in " & Comi & Cambiar_Comas_Comillas(Me.txtdepini.Text) & Comi & " to " & Comi & Cambiar_Comas_Comillas(Me.txtdepfin.Text) & Comi & ")"'GAPM M7004 COMENTARIO
        ' DAHV M7008 - INICIO
        ' SE HABILITA NUEVAMENTE LA TABLA MOVIMIENTOS
         ' DAHV M7054 - INICIO
          'StVal = StVal & "(({MOVIMIENTOS.CD_CECO_MOVI} in " & Comi & Cambiar_Comas_Comillas(Me.txtdepini.Text) & Comi & " to " & Comi & Cambiar_Comas_Comillas(Me.txtdepfin.Text) & Comi & ") OR ({MOVIMIENTOS.CD_CECO_MOVI}=''))" 'GAPM M7004 AGREGO LOS QUE SEAN VACIOS 'GAPM M7008 COMENTARIO
          StVal = StVal & "(({IN_R_ENCA_DEPE.TX_CECO_RENDE} in " & Comi & Cambiar_Comas_Comillas(Me.txtdepini.Text) & Comi & " to " & Comi & Cambiar_Comas_Comillas(Me.txtdepfin.Text) & Comi & "))"
          
          ' DAHV M7054 - FIN
        ' StVal = StVal & "({R_GRUP_DEPE.CD_DEPE_RGD} in " & Comi & Cambiar_Comas_Comillas(Me.txtdepini.Text) & Comi & " to " & Comi & Cambiar_Comas_Comillas(Me.txtdepfin.Text) & Comi & ")" 'GAPM M7008 CAMBIO CAMPOS NO TRAIGO LO DE CONTABILIDAD, TRAIGO LO DE INVENTARIOS
        ' DAHV M7008 - FIN
        StTITDEP = "RANGO DE DEPENDENCIAS: Del " & Right(String(6, "0") & Cambiar_Comas_Comillas(Me.txtdepini.Text), 6) & " al " & Right(String(6, "0") & Cambiar_Comas_Comillas(Me.txtdepfin.Text), 6)
        'GAPM M7002 FIN
        MDI_Inventarios.Crys_Listar.Formulas(15) = "RAGDep='" & StTITDEP & Comi
    End If
    strTERCEROS = strTERCEROS & StVal
    'GAPM R2427 FIN
    MDI_Inventarios.Crys_Listar.Formulas(9) = "RAGCompr='" & strTITRANGO & Comi
    
    'GAPM R2427 INICIO
    'VALIDO SI LA CLASE TERCERO CAPTURA EL TERCERO POR .INI
    If Trim(Dueno.Nombre) = "" Then
        MDI_Inventarios.Crys_Listar.Formulas(10) = "ENTIDAD=" & Comi & Entidad & ", NIT:" & Nit & Comi
    Else
        MDI_Inventarios.Crys_Listar.Formulas(10) = "ENTIDAD=" & Comi & Trim(Dueno.Nombre) & ", NIT:" & Trim(Dueno.Nit) & Comi
    End If
    'MDI_Inventarios.Crys_Listar.Formulas(10) = "ENTIDAD=" & Comi & Trim(Dueno.Nombre) & ", NIT:" & Trim(Dueno.Nit) & Comi'GAPM R2427 SE DEJA EN COMENTARIO
    'GAPM R2427 FIN
    
    
    txtDR = IIf(Me.opcDET.Value, "D", "R")
    Select Case Docume.TipDeDcmnt
        Case Cotizacion
            'If Me.opcORG.Value Then
            If Me.opcMOD.Value Then 'JACC M5760 TODOS
                strTERCEROS = strTERCEROS & IIf(Len(strTERCEROS) > 0, " AND ", "") & _
                     "({IN_DOCUMENTO.NU_AUTO_DOCU}=" & Cotizacion & ")" 'HRR DEPURACION VERSION JULIO 2008
                    '"({IN_DOCUMENTO.NU_AUTO_DOCU}=" & Cotizacion 'HRR DEPURACION VERSION JULIO 2008
                    
            'ElseIf Me.opcMOD.Value Then 'JACC M5760
            ElseIf Me.opcORG.Value Then 'JACC M5760 NO ANULADOS
                strTERCEROS = strTERCEROS & IIf(Len(strTERCEROS) > 0, " AND ", "") & _
                    "({IN_DOCUMENTO.NU_AUTO_DOCU}=" & Cotizacion
                strTERCEROS = strTERCEROS & " AND {IN_ENCABEZADO.TX_ESTA_ENCA}<>'A')"
            ElseIf Me.opcANU.Value Then
                strTERCEROS = strTERCEROS & IIf(Len(strTERCEROS) > 0, " AND ", "") & _
                    "({IN_DOCUMENTO.NU_AUTO_DOCU}=" & Cotizacion
                strTERCEROS = strTERCEROS & " AND {IN_ENCABEZADO.TX_ESTA_ENCA}='A')"
            End If
            Debug.Print strTERCEROS
            'Me.opcMOD.Value = True
            'Call ListarD("infCPCOT_" & IIf(Not Me.opcMOD.Value, "ORG", "ACT") & txtDR & ".rpt", strTERCEROS, crptToWindow, "COTIZACIONES", MDI_Inventarios, True)
             Call ListarD("infCPCOT_" & "ACT" & txtDR & ".rpt", strTERCEROS, crptToWindow, "COTIZACIONES", MDI_Inventarios, True) 'DAHV M3621
        Case ODCompra
            'If Me.opcORG.Value Then
            If Me.opcMOD.Value Then 'JACC M5760
                strTERCEROS = strTERCEROS & IIf(Len(strTERCEROS) > 0, " AND ", "") & _
                    "({IN_DOCUMENTO.NU_AUTO_DOCU}=" & ODCompra
                strTERCEROS = strTERCEROS & ")" 'AASV M3620
            'ElseIf Me.opcMOD.Value Then
            ElseIf Me.opcORG.Value Then 'JACC M5760
                strTERCEROS = strTERCEROS & IIf(Len(strTERCEROS) > 0, " AND ", "") & _
                    "({IN_DOCUMENTO.NU_AUTO_DOCU}=" & ODCompra
                strTERCEROS = strTERCEROS & " AND {IN_ENCABEZADO.TX_ESTA_ENCA}<>'A')"
            ElseIf Me.opcANU.Value Then
                strTERCEROS = strTERCEROS & IIf(Len(strTERCEROS) > 0, " AND ", "") & _
                    "({IN_DOCUMENTO.NU_AUTO_DOCU}=" & ODCompra
                strTERCEROS = strTERCEROS & " AND {IN_ENCABEZADO.TX_ESTA_ENCA}='A')"
            End If
            Debug.Print strTERCEROS
            'Me.opcMOD.Value = True 'infCPCOT_
            'Call ListarD("infCPCOT_" & IIf(Not Me.opcMOD.Value, "ORG", "ACT") & txtDR & ".rpt", strTERCEROS, crptToWindow, "ORDENES DE COMPRA", MDI_Inventarios, True)
             Call ListarD("infCPCOT_" & "ACT" & txtDR & ".rpt", strTERCEROS, crptToWindow, "ORDENES DE COMPRA", MDI_Inventarios, True) 'DAHV M3621
        Case Entrada, AprovechaDonacion
            If Me.opcORG.Value Then
                'strTERCEROS = strTERCEROS & IIf(Len(strTERCEROS) > 0, " AND ", "") & _
                    "({IN_DOCUMENTO_1.NU_AUTO_DOCU}=" & Entrada
                strTERCEROS = strTERCEROS & IIf(Len(strTERCEROS) > 0, " AND ", "") & _
                    "({IN_DOCUMENTO_1.NU_AUTO_DOCU}=" & Docume.TipDeDcmnt                   'PedroJ Def 3323
                strTERCEROS = strTERCEROS & " AND {IN_ENCABEZADO.TX_ESTA_ENCA}<>'A'"
                strTERCEROS = strTERCEROS & " AND {IN_ENCABEZADO_1.TX_ESTA_ENCA}<>'A')"
            ElseIf Me.opcMOD.Value Then
                'strTERCEROS = strTERCEROS & IIf(Len(strTERCEROS) > 0, " AND ", "") & _
                    "{IN_DOCUMENTO_1.NU_AUTO_DOCU}=" & Entrada
                strTERCEROS = strTERCEROS & IIf(Len(strTERCEROS) > 0, " AND ", "") & _
                    "{IN_DOCUMENTO_1.NU_AUTO_DOCU}=" & Docume.TipDeDcmnt                    'PedroJ Def 3323
            ElseIf Me.opcANU.Value Then
                'strTERCEROS = strTERCEROS & IIf(Len(strTERCEROS) > 0, " AND ", "") & _
                    "({IN_DOCUMENTO_1.NU_AUTO_DOCU}=" & Entrada
                strTERCEROS = strTERCEROS & IIf(Len(strTERCEROS) > 0, " AND ", "") & _
                    "({IN_DOCUMENTO_1.NU_AUTO_DOCU}=" & Docume.TipDeDcmnt                    'PedroJ Def 3323
                strTERCEROS = strTERCEROS & " AND {IN_ENCABEZADO.TX_ESTA_ENCA}='A')"
            End If
            'LJSA M4591 -----------------------------
            strTERCEROS = strTERCEROS & " And {IN_ENCABEZADO.NU_AUTO_COMP_ENCA} = {IN_COMPROBANTE.NU_AUTO_COMP}"
            strTERCEROS = strTERCEROS & " And {IN_COMPROBANTE.NU_AUTO_DOCU_COMP} = " & Docume.TipDeDcmnt
            'LJSA M4591 -----------------------------
            Debug.Print strTERCEROS
            'Me.opcMOD.Value = True
            'Call ListarD("infCPENT_" & IIf(Not Me.opcMOD.Value, "ORG", "ACT") & txtDR & ".rpt", strTERCEROS, crptToWindow, "ENTRADAS", MDI_Inventarios, True)
             Call ListarD("infCPENT_" & "ACT" & txtDR & ".rpt", strTERCEROS, crptToWindow, "ENTRADAS", MDI_Inventarios, True) 'DAHV M3621
        Case DevolucionEntrada
            'If Me.opcORG.Value Then
            If Me.opcMOD.Value Then 'JACC M5760
                strTERCEROS = strTERCEROS & IIf(Len(strTERCEROS) > 0, " AND ", "") & _
                    "({IN_DOCUMENTO.NU_AUTO_DOCU}=" & DevolucionEntrada
                strTERCEROS = strTERCEROS & ")" 'AASV M3620
                strTERCEROS = strTERCEROS & " AND ({IN_COMPROBANTE_1.NU_AUTO_DOCU_COMP}={IN_DOCUMENTO.NU_AUTO_DOCU})" 'APGR T7158
            'ElseIf Me.opcMOD.Value Then
            ElseIf Me.opcORG.Value Then 'JACC M5760
                strTERCEROS = strTERCEROS & IIf(Len(strTERCEROS) > 0, " AND ", "") & _
                    "({IN_DOCUMENTO.NU_AUTO_DOCU}=" & DevolucionEntrada
                strTERCEROS = strTERCEROS & " AND {IN_ENCABEZADO.TX_ESTA_ENCA}<>'A')"
                strTERCEROS = strTERCEROS & " AND (IN_COMPROBANTE_1.NU_AUTO_DOCU_COMP=IN_DOCUMENTO.NU_AUTO_DOCU)" 'APGR T7158
            ElseIf Me.opcANU.Value Then
                strTERCEROS = strTERCEROS & IIf(Len(strTERCEROS) > 0, " AND ", "") & _
                    "({IN_DOCUMENTO.NU_AUTO_DOCU}=" & DevolucionEntrada
                strTERCEROS = strTERCEROS & " AND {IN_ENCABEZADO.TX_ESTA_ENCA}='A')"
                strTERCEROS = strTERCEROS & " AND (IN_COMPROBANTE_1.NU_AUTO_DOCU_COMP=IN_DOCUMENTO.NU_AUTO_DOCU)" 'APGR T7158
            End If
            Debug.Print strTERCEROS
            'Me.opcMOD.Value = True
            'Call ListarD("infCPENT_" & IIf(Not Me.opcMOD.Value, "ORG", "ACT") & txtDR & ".rpt", strTERCEROS, crptToWindow, "DEVOLUCION ENTRADAS", MDI_Inventarios, True)
            Call ListarD("infCPENT_" & "ACT" & txtDR & ".rpt", strTERCEROS, crptToWindow, "DEVOLUCION ENTRADAS", MDI_Inventarios, True) 'DAHV M3621
        Case Requisicion
            'If Me.opcORG.Value Then
            If Me.opcMOD.Value Then 'JACC M5760
                strTERCEROS = strTERCEROS & IIf(Len(strTERCEROS) > 0, " AND ", "") & _
                    "({IN_DOCUMENTO.NU_AUTO_DOCU}=" & Requisicion
                strTERCEROS = strTERCEROS & ")" 'AASV M3620
            'ElseIf Me.opcMOD.Value Then
            ElseIf Me.opcORG.Value Then 'JACC M5760
                strTERCEROS = strTERCEROS & IIf(Len(strTERCEROS) > 0, " AND ", "") & _
                    "({IN_DOCUMENTO.NU_AUTO_DOCU}=" & Requisicion
                strTERCEROS = strTERCEROS & " AND {IN_ENCABEZADO.TX_ESTA_ENCA}<>'A')"
            ElseIf Me.opcANU.Value Then
                strTERCEROS = strTERCEROS & IIf(Len(strTERCEROS) > 0, " AND ", "") & _
                    "({IN_DOCUMENTO.NU_AUTO_DOCU}=" & Requisicion
                strTERCEROS = strTERCEROS & " AND {IN_ENCABEZADO.TX_ESTA_ENCA}='A')"
            End If
            Debug.Print strTERCEROS
            'Me.opcMOD.Value = True 'infCPCOT_
            'Call ListarD("infCPCOT_" & IIf(Not Me.opcMOD.Value, "ORG", "ACT") & txtDR & ".rpt", strTERCEROS, crptToWindow, "REQUISICIONES", MDI_Inventarios, True)
             Call ListarD("infCPCOT_" & "ACT" & txtDR & ".rpt", strTERCEROS, crptToWindow, "REQUISICIONES", MDI_Inventarios, True) 'DAHV M3621
        Case Despacho
            If Me.opcORG.Value Then
                'strTERCEROS = strTERCEROS & IIf(Len(strTERCEROS) > 0, " AND ", "") & _
                    "({IN_DOCUMENTO_1.NU_AUTO_DOCU}=" & Despacho & _
                    " AND ({IN_BODEGA.NU_AUTO_BODE}={IN_DETALLE.NU_AUTO_BODE_DETA})"
                strTERCEROS = strTERCEROS & IIf(Len(strTERCEROS) > 0, " AND ", "") & _
                    "({IN_DOCUMENTO_1.NU_AUTO_DOCU}=" & Despacho & _
                    " AND ({IN_BODEGA.NU_AUTO_BODE}={IN_DETALLE.NU_AUTO_BODE_DETA})"   'PedroJ Def 3746
                strTERCEROS = strTERCEROS & " AND {IN_ENCABEZADO.TX_ESTA_ENCA}<>'A')"
            ElseIf Me.opcMOD.Value Then
                strTERCEROS = strTERCEROS & IIf(Len(strTERCEROS) > 0, " AND ", "") & _
                    "({IN_DOCUMENTO_1.NU_AUTO_DOCU}=" & Despacho & _
                    " AND ({IN_BODEGA.NU_AUTO_BODE}={IN_DETALLE.NU_AUTO_BODE_DETA}))"
                'strTERCEROS = strTERCEROS & " AND {IN_ENCABEZADO.TX_ESTA_ENCA}<>'A')"
            ElseIf Me.opcANU.Value Then
                strTERCEROS = strTERCEROS & IIf(Len(strTERCEROS) > 0, " AND ", "") & _
                    "({IN_DOCUMENTO_1.NU_AUTO_DOCU}=" & Despacho & _
                    " AND ({IN_BODEGA.NU_AUTO_BODE}={IN_DETALLE.NU_AUTO_BODE_DETA})"
                strTERCEROS = strTERCEROS & " AND {IN_ENCABEZADO.TX_ESTA_ENCA}='A')"
            End If
            Debug.Print strTERCEROS
          '  Me.opcMOD.Value = True 'infCPCOT_
          '  Call ListarD("infCPCOT_" & IIf(Not Me.opcMOD.Value, "ORG", "ACT") & txtDR & ".rpt", strTERCEROS, crptToWindow, "DESPACHOS", MDI_Inventarios, True)
          Call ListarD("infCPCOT_" & "ACT" & txtDR & ".rpt", strTERCEROS, crptToWindow, "DESPACHOS", MDI_Inventarios, True) 'DAHV M3621
    
    'PedroJ Def 4426 imprimir devoluciones
        Case DevolucionDespacho
            'If Me.opcORG.Value Then
            If Me.opcMOD.Value Then 'JACC M5760
                strTERCEROS = strTERCEROS & IIf(Len(strTERCEROS) > 0, " AND ", "") & _
                    "({IN_DOCUMENTO_1.NU_AUTO_DOCU}=" & DevolucionDespacho & _
                    " AND ({IN_BODEGA.NU_AUTO_BODE}={IN_DETALLE.NU_AUTO_BODE_DETA}))"
            'ElseIf Me.opcMOD.Value Then
            ElseIf Me.opcORG.Value Then 'JACC M5760
                strTERCEROS = strTERCEROS & IIf(Len(strTERCEROS) > 0, " AND ", "") & _
                    "({IN_DOCUMENTO_1.NU_AUTO_DOCU}=" & DevolucionDespacho & _
                    " AND ({IN_BODEGA.NU_AUTO_BODE}={IN_DETALLE.NU_AUTO_BODE_DETA})"
                strTERCEROS = strTERCEROS & " AND {IN_ENCABEZADO.TX_ESTA_ENCA}<>'A')"
            ElseIf Me.opcANU.Value Then
                strTERCEROS = strTERCEROS & IIf(Len(strTERCEROS) > 0, " AND ", "") & _
                    "({IN_DOCUMENTO_1.NU_AUTO_DOCU}=" & DevolucionDespacho & _
                    " AND ({IN_BODEGA.NU_AUTO_BODE}={IN_DETALLE.NU_AUTO_BODE_DETA})"
                strTERCEROS = strTERCEROS & " AND {IN_ENCABEZADO.TX_ESTA_ENCA}='A')"
            End If
            Debug.Print strTERCEROS
            'Me.opcMOD.Value = True
            'Call ListarD("infCPCOT_" & IIf(Not Me.opcMOD.Value, "ORG", "ACT") & txtDR & ".rpt", strTERCEROS, crptToWindow, "DEVOLUCION DESPACHOS", MDI_Inventarios, True)
            Call ListarD("infCPCOT_" & "ACT" & txtDR & ".rpt", strTERCEROS, crptToWindow, "DEVOLUCION DESPACHOS", MDI_Inventarios, True) 'DAHV M3621
    'PedroJ
        Case BajaConsumo, FacturaVenta
            'If Me.opcORG.Value Then
            If Me.opcMOD.Value Then 'JACC M5760
                'strTERCEROS = strTERCEROS & IIf(Len(strTERCEROS) > 0, " AND ", "") & _
                    "({IN_DOCUMENTO.NU_AUTO_DOCU}=" & BajaConsumo
                 strTERCEROS = strTERCEROS & IIf(Len(strTERCEROS) > 0, " AND ", "") & _
                    "({IN_DOCUMENTO.NU_AUTO_DOCU}=" & Docume.TipDeDcmnt & ")"                     'PedroJ Def 4388
            'ElseIf Me.opcMOD.Value Then
            ElseIf Me.opcORG.Value Then 'JACC M5760
                'strTERCEROS = strTERCEROS & IIf(Len(strTERCEROS) > 0, " AND ", "") & _
                    "({IN_DOCUMENTO.NU_AUTO_DOCU}=" & BajaConsumo
                 strTERCEROS = strTERCEROS & IIf(Len(strTERCEROS) > 0, " AND ", "") & _
                    "({IN_DOCUMENTO.NU_AUTO_DOCU}=" & Docume.TipDeDcmnt                           'PedroJ Def 4388
                strTERCEROS = strTERCEROS & " AND {IN_ENCABEZADO.TX_ESTA_ENCA}<>'A')"
            ElseIf Me.opcANU.Value Then
                'strTERCEROS = strTERCEROS & IIf(Len(strTERCEROS) > 0, " AND ", "") & _
                    "({IN_DOCUMENTO.NU_AUTO_DOCU}=" & BajaConsumo
                strTERCEROS = strTERCEROS & IIf(Len(strTERCEROS) > 0, " AND ", "") & _
                    "({IN_DOCUMENTO.NU_AUTO_DOCU}=" & Docume.TipDeDcmnt                           'PedroJ Def 4388
                strTERCEROS = strTERCEROS & " AND {IN_ENCABEZADO.TX_ESTA_ENCA}='A')"
            End If
            Debug.Print strTERCEROS
          '  Me.opcMOD.Value = True 'infCPCOT_
          '  Call ListarD("infCPCOT_" & IIf(Not Me.opcMOD.Value, "ORG", "ACT") & txtDR & ".rpt", strTERCEROS, crptToWindow, "BAJAS X CONSUMO", MDI_Inventarios, True)
             
             'GAPM R2432 INICIO
             'VALIDO SI ES POR DEPENDENCIA
             If CBool(Me.ChkDepe.Value) Then
                Call ListarD("infCPCOT_" & "ACT" & txtDR & "_DEP.rpt", strTERCEROS, crptToWindow, "BAJAS X CONSUMO", MDI_Inventarios, True) 'GAPM R2427 CUANDO ES POR DEPENDENCIA
             Else
               'APGR M2483 - INICIO Se agrega case.
                Select Case Docume.TipDeDcmnt
                     Case BajaConsumo
                         Call ListarD("infCPCOT_" & "ACT" & txtDR & ".rpt", strTERCEROS, crptToWindow, "BAJAS X CONSUMO", MDI_Inventarios, True) 'SIGUE SU PROCESO NORMAL
                           'Call ListarD("infCPCOT_" & "ACT" & txtDR & ".rpt", strTERCEROS, crptToWindow, "BAJAS X CONSUMO", MDI_Inventarios, True) 'DAHV M3621 'GAPM R2427 SE PONE EN COMENTARIO
             'GAPM R2432 FIN
                      Case FacturaVenta
                         Call ListarD("infCPCOT_" & "ACT" & txtDR & ".rpt", strTERCEROS, crptToWindow, "FACTURA DE VENTA", MDI_Inventarios, True)
                End Select
                'APGR M2483 FIN
             End If
           
             
        Case DevolucionBaja
            If Me.opcMOD.Value Then 'REOL M1788
                strTERCEROS = strTERCEROS & IIf(Len(strTERCEROS) > 0, " AND ", "") & _
                    "({IN_DOCUMENTO.NU_AUTO_DOCU}=" & DevolucionBaja & ")"    'REOL M1788
            ElseIf Me.opcORG.Value Then 'REOL M1788
                strTERCEROS = strTERCEROS & IIf(Len(strTERCEROS) > 0, " AND ", "") & _
                    "({IN_DOCUMENTO.NU_AUTO_DOCU}=" & DevolucionBaja
                strTERCEROS = strTERCEROS & " AND {IN_ENCABEZADO.TX_ESTA_ENCA}<>'A')"
            ElseIf Me.opcANU.Value Then
                strTERCEROS = strTERCEROS & IIf(Len(strTERCEROS) > 0, " AND ", "") & _
                    "({IN_DOCUMENTO.NU_AUTO_DOCU}=" & DevolucionBaja
                strTERCEROS = strTERCEROS & " AND {IN_ENCABEZADO.TX_ESTA_ENCA}='A')"
            End If
            Debug.Print strTERCEROS
            'Me.opcMOD.Value = True 'infCPCOT_
            'Call ListarD("infCPBAJ_" & IIf(Not Me.opcMOD.Value, "ORG", "ACT") & txtDR & ".rpt", strTERCEROS, crptToWindow, "DEVOLUCION DE BAJAS", MDI_Inventarios, True)    'REOL M1788
             Call ListarD("infCPBAJ_" & "ACT" & txtDR & ".rpt", strTERCEROS, crptToWindow, "DEVOLUCION DE BAJAS", MDI_Inventarios, True) 'DAHV M3621
        Case Salida
'            If Me.opcMOD.Value Then 'REOL M1788
'                strTERCEROS = strTERCEROS & IIf(Len(strTERCEROS) > 0, " AND ", "") & _
'                    "({IN_DOCUMENTO.NU_AUTO_DOCU}=" & BajaConsumo & ")"    'REOL M1788
'            ElseIf Me.opcORG.Value Then 'REOL M1788
'                strTERCEROS = strTERCEROS & IIf(Len(strTERCEROS) > 0, " AND ", "") & _
'                    "({IN_DOCUMENTO.NU_AUTO_DOCU}=" & BajaConsumo
'                strTERCEROS = strTERCEROS & " AND {IN_ENCABEZADO.TX_ESTA_ENCA}<>'A')"
'            ElseIf Me.opcANU.Value Then
'                strTERCEROS = strTERCEROS & IIf(Len(strTERCEROS) > 0, " AND ", "") & _
'                    "({IN_DOCUMENTO.NU_AUTO_DOCU}=" & BajaConsumo
'                strTERCEROS = strTERCEROS & " AND {IN_ENCABEZADO.TX_ESTA_ENCA}='A')"
'            End If
            'NMSR M3428
            If Me.opcMOD.Value Then
                strTERCEROS = strTERCEROS & IIf(Len(strTERCEROS) > 0, " AND ", "") & _
                    "({IN_DOCUMENTO.NU_AUTO_DOCU}=" & Salida & ")"
            ElseIf Me.opcORG.Value Then
                strTERCEROS = strTERCEROS & IIf(Len(strTERCEROS) > 0, " AND ", "") & _
                    "({IN_DOCUMENTO.NU_AUTO_DOCU}=" & Salida
                strTERCEROS = strTERCEROS & " AND {IN_ENCABEZADO.TX_ESTA_ENCA}<>'A')"
            ElseIf Me.opcANU.Value Then
                strTERCEROS = strTERCEROS & IIf(Len(strTERCEROS) > 0, " AND ", "") & _
                    "({IN_DOCUMENTO.NU_AUTO_DOCU}=" & Salida
                strTERCEROS = strTERCEROS & " AND {IN_ENCABEZADO.TX_ESTA_ENCA}='A')"
            End If 'NMSR M3428
            Debug.Print strTERCEROS
            'Me.opcMOD.Value = True
            'Call ListarD("infCPCOT_" & IIf(Not Me.opcMOD.Value, "ORG", "ACT") & txtDR & ".rpt", strTERCEROS, crptToWindow, "SALIDA DE MERCANCIA", MDI_Inventarios, True)    'REOL M1788
            Call ListarD("infCPCOT_" & "ACT" & txtDR & ".rpt", strTERCEROS, crptToWindow, "SALIDA DE MERCANCIA", MDI_Inventarios, True) 'DAHV M3621
        Case Traslado
            If Me.opcMOD.Value Then 'REOL M1788
                strTERCEROS = strTERCEROS & IIf(Len(strTERCEROS) > 0, " AND ", "") & _
                    "({IN_DOCUMENTO.NU_AUTO_DOCU}=" & Traslado & ")"    'REOL M1788
            ElseIf Me.opcORG.Value Then 'REOL M1788
                strTERCEROS = strTERCEROS & IIf(Len(strTERCEROS) > 0, " AND ", "") & _
                    "({IN_DOCUMENTO.NU_AUTO_DOCU}=" & Traslado
                strTERCEROS = strTERCEROS & " AND {IN_ENCABEZADO.TX_ESTA_ENCA}<>'A')"
            ElseIf Me.opcANU.Value Then
                strTERCEROS = strTERCEROS & IIf(Len(strTERCEROS) > 0, " AND ", "") & _
                    "({IN_DOCUMENTO.NU_AUTO_DOCU}=" & Traslado
                strTERCEROS = strTERCEROS & " AND {IN_ENCABEZADO.TX_ESTA_ENCA}='A')"
            End If
            Debug.Print strTERCEROS
            'Me.opcMOD.Value = True
            'Call ListarD("infCPCOT_" & IIf(Not Me.opcMOD.Value, "ORG", "ACT") & txtDR & ".rpt", strTERCEROS, crptToWindow, "TRASLADOS", MDI_Inventarios, True)    'REOL M1788
            Call ListarD("infCPCOT_" & "ACT" & txtDR & ".rpt", strTERCEROS, crptToWindow, "TRASLADOS", MDI_Inventarios, True) 'DAHV M3621
    'PedroJ Def 2328 Imprimir Anulaciones
        Case AnulaODCompra
            'If Me.opcORG.Value Then
            If Me.opcMOD.Value Then 'JACC M5760
                strTERCEROS = strTERCEROS & IIf(Len(strTERCEROS) > 0, " AND ", "") & _
                    "({IN_DOCUMENTO.NU_AUTO_DOCU}=" & Docume.TipDeDcmnt & ")"
            'ElseIf Me.opcMOD.Value Then
            ElseIf Me.opcORG.Value Then 'JACC M5760
                strTERCEROS = strTERCEROS & IIf(Len(strTERCEROS) > 0, " AND ", "") & _
                    "({IN_DOCUMENTO.NU_AUTO_DOCU}=" & Docume.TipDeDcmnt
                strTERCEROS = strTERCEROS & " AND {IN_ENCABEZADO.TX_ESTA_ENCA}<>'A')"
            ElseIf Me.opcANU.Value Then
                strTERCEROS = strTERCEROS & IIf(Len(strTERCEROS) > 0, " AND ", "") & _
                    "({IN_DOCUMENTO.NU_AUTO_DOCU}=" & Docume.TipDeDcmnt
                strTERCEROS = strTERCEROS & " AND {IN_ENCABEZADO.TX_ESTA_ENCA}='A')"
            End If
            Debug.Print strTERCEROS
            Call ListarD("infCPANL_ODC" & txtDR & ".rpt", strTERCEROS, crptToWindow, "ANULACION ORDENES DE COMPRA", MDI_Inventarios, True)
        Case AnulaTraslado
            'If Me.opcORG.Value Then
            If Me.opcMOD.Value Then 'JACC M5760
                strTERCEROS = strTERCEROS & IIf(Len(strTERCEROS) > 0, " AND ", "") & _
                    "({IN_DOCUMENTO.NU_AUTO_DOCU}=" & Docume.TipDeDcmnt & ")"
            'ElseIf Me.opcMOD.Value Then
            ElseIf Me.opcORG.Value Then 'JACC M5760
                strTERCEROS = strTERCEROS & IIf(Len(strTERCEROS) > 0, " AND ", "") & _
                    "({IN_DOCUMENTO.NU_AUTO_DOCU}=" & Docume.TipDeDcmnt
                strTERCEROS = strTERCEROS & " AND {IN_ENCABEZADO.TX_ESTA_ENCA}<>'A')"
            ElseIf Me.opcANU.Value Then
                strTERCEROS = strTERCEROS & IIf(Len(strTERCEROS) > 0, " AND ", "") & _
                    "({IN_DOCUMENTO.NU_AUTO_DOCU}=" & Docume.TipDeDcmnt
                strTERCEROS = strTERCEROS & " AND {IN_ENCABEZADO.TX_ESTA_ENCA}='A')"
            End If
            Debug.Print strTERCEROS
            Call ListarD("infCPANL_ODC" & txtDR & ".rpt", strTERCEROS, crptToWindow, "ANULACION TRASLADO", MDI_Inventarios, True)
        Case AnulaSalida
            'If Me.opcORG.Value Then
            If Me.opcMOD.Value Then 'JACC M5760
                strTERCEROS = strTERCEROS & IIf(Len(strTERCEROS) > 0, " AND ", "") & _
                    "({IN_DOCUMENTO.NU_AUTO_DOCU}=" & Docume.TipDeDcmnt & ")"
            'ElseIf Me.opcMOD.Value Then
            ElseIf Me.opcORG.Value Then 'JACC M5760
                strTERCEROS = strTERCEROS & IIf(Len(strTERCEROS) > 0, " AND ", "") & _
                    "({IN_DOCUMENTO.NU_AUTO_DOCU}=" & Docume.TipDeDcmnt
                strTERCEROS = strTERCEROS & " AND {IN_ENCABEZADO.TX_ESTA_ENCA}<>'A')"
            ElseIf Me.opcANU.Value Then
                strTERCEROS = strTERCEROS & IIf(Len(strTERCEROS) > 0, " AND ", "") & _
                    "({IN_DOCUMENTO.NU_AUTO_DOCU}=" & Docume.TipDeDcmnt
                strTERCEROS = strTERCEROS & " AND {IN_ENCABEZADO.TX_ESTA_ENCA}='A')"
            End If
            Debug.Print strTERCEROS
            Call ListarD("infCPANL_ODC" & txtDR & ".rpt", strTERCEROS, crptToWindow, "ANULACION SALIDA", MDI_Inventarios, True)
        Case AnulaBaja
            'If Me.opcORG.Value Then
            If Me.opcMOD.Value Then 'JACC M5760
                strTERCEROS = strTERCEROS & IIf(Len(strTERCEROS) > 0, " AND ", "") & _
                    "({IN_DOCUMENTO.NU_AUTO_DOCU}=" & Docume.TipDeDcmnt & ")"
            'ElseIf Me.opcMOD.Value Then
            ElseIf Me.opcORG.Value Then 'JACC M5760
                strTERCEROS = strTERCEROS & IIf(Len(strTERCEROS) > 0, " AND ", "") & _
                    "({IN_DOCUMENTO.NU_AUTO_DOCU}=" & Docume.TipDeDcmnt
                strTERCEROS = strTERCEROS & " AND {IN_ENCABEZADO.TX_ESTA_ENCA}<>'A')"
            ElseIf Me.opcANU.Value Then
                strTERCEROS = strTERCEROS & IIf(Len(strTERCEROS) > 0, " AND ", "") & _
                    "({IN_DOCUMENTO.NU_AUTO_DOCU}=" & Docume.TipDeDcmnt
                strTERCEROS = strTERCEROS & " AND {IN_ENCABEZADO.TX_ESTA_ENCA}='A')"
            End If
            Debug.Print strTERCEROS
            Call ListarD("infCPANL_ODC" & txtDR & ".rpt", strTERCEROS, crptToWindow, "ANULACION BAJA", MDI_Inventarios, True)
        Case AnulaDespacho
            'If Me.opcORG.Value Then
            If Me.opcMOD.Value Then 'JACC M5760
                strTERCEROS = strTERCEROS & IIf(Len(strTERCEROS) > 0, " AND ", "") & _
                    "({IN_DOCUMENTO.NU_AUTO_DOCU}=" & Docume.TipDeDcmnt & ")"
            'ElseIf Me.opcMOD.Value Then
            ElseIf Me.opcORG.Value Then 'JACC M5760
                strTERCEROS = strTERCEROS & IIf(Len(strTERCEROS) > 0, " AND ", "") & _
                    "({IN_DOCUMENTO.NU_AUTO_DOCU}=" & Docume.TipDeDcmnt
                strTERCEROS = strTERCEROS & " AND {IN_ENCABEZADO.TX_ESTA_ENCA}<>'A')"
            ElseIf Me.opcANU.Value Then
                strTERCEROS = strTERCEROS & IIf(Len(strTERCEROS) > 0, " AND ", "") & _
                    "({IN_DOCUMENTO.NU_AUTO_DOCU}=" & Docume.TipDeDcmnt
                strTERCEROS = strTERCEROS & " AND {IN_ENCABEZADO.TX_ESTA_ENCA}='A')"
            End If
            Debug.Print strTERCEROS
            Call ListarD("infCPANL_ODC" & txtDR & ".rpt", strTERCEROS, crptToWindow, "ANULACION DESPACHO", MDI_Inventarios, True)
        Case AnulaRequisicion
            'If Me.opcORG.Value Then
            If Me.opcMOD.Value Then 'JACC M5760
                strTERCEROS = strTERCEROS & IIf(Len(strTERCEROS) > 0, " AND ", "") & _
                    "({IN_DOCUMENTO.NU_AUTO_DOCU}=" & Docume.TipDeDcmnt & ")"
            'ElseIf Me.opcMOD.Value Then
            ElseIf Me.opcORG.Value Then 'JACC M5760
                strTERCEROS = strTERCEROS & IIf(Len(strTERCEROS) > 0, " AND ", "") & _
                    "({IN_DOCUMENTO.NU_AUTO_DOCU}=" & Docume.TipDeDcmnt
                strTERCEROS = strTERCEROS & " AND {IN_ENCABEZADO.TX_ESTA_ENCA}<>'A')"
            ElseIf Me.opcANU.Value Then
                strTERCEROS = strTERCEROS & IIf(Len(strTERCEROS) > 0, " AND ", "") & _
                    "({IN_DOCUMENTO.NU_AUTO_DOCU}=" & Docume.TipDeDcmnt
                strTERCEROS = strTERCEROS & " AND {IN_ENCABEZADO.TX_ESTA_ENCA}='A')"
            End If
            Debug.Print strTERCEROS
            Call ListarD("infCPANL_ODC" & txtDR & ".rpt", strTERCEROS, crptToWindow, "ANULACION REQUISICION", MDI_Inventarios, True)
        Case AnulaEntrada, AnulaAprovecha
            'If Me.opcORG.Value Then
            If Me.opcMOD.Value Then 'JACC M5760
                strTERCEROS = strTERCEROS & IIf(Len(strTERCEROS) > 0, " AND ", "") & _
                    "({IN_DOCUMENTO.NU_AUTO_DOCU}=" & Docume.TipDeDcmnt & ")"
                strTERCEROS = strTERCEROS & " AND ({IN_COMPROBANTE_1.NU_AUTO_DOCU_COMP}={IN_DOCUMENTO.NU_AUTO_DOCU})" 'APGR T7158
            'ElseIf Me.opcMOD.Value Then
            ElseIf Me.opcORG.Value Then 'JACC M5760
                strTERCEROS = strTERCEROS & IIf(Len(strTERCEROS) > 0, " AND ", "") & _
                    "({IN_DOCUMENTO.NU_AUTO_DOCU}=" & Docume.TipDeDcmnt
                strTERCEROS = strTERCEROS & " AND {IN_ENCABEZADO.TX_ESTA_ENCA}<>'A')"
                strTERCEROS = strTERCEROS & " AND ({IN_COMPROBANTE_1.NU_AUTO_DOCU_COMP}={IN_DOCUMENTO.NU_AUTO_DOCU})" 'APGR T7158
            ElseIf Me.opcANU.Value Then
                strTERCEROS = strTERCEROS & IIf(Len(strTERCEROS) > 0, " AND ", "") & _
                    "({IN_DOCUMENTO.NU_AUTO_DOCU}=" & Docume.TipDeDcmnt
                strTERCEROS = strTERCEROS & " AND {IN_ENCABEZADO.TX_ESTA_ENCA}='A')"
                strTERCEROS = strTERCEROS & " AND ({IN_COMPROBANTE_1.NU_AUTO_DOCU_COMP}={IN_DOCUMENTO.NU_AUTO_DOCU})" 'APGR T7158
            End If
            Debug.Print strTERCEROS
            Call ListarD("infCPANL_ODC" & txtDR & ".rpt", strTERCEROS, crptToWindow, "ANULACION ENTRADA", MDI_Inventarios, True)
    'PedroJ FIN ANULACIONES
            'NMSR M3428
        Case AnulaFacturaVenta
            If Me.opcMOD.Value Then
                strTERCEROS = strTERCEROS & IIf(Len(strTERCEROS) > 0, " AND ", "") & _
                    "({IN_DOCUMENTO.NU_AUTO_DOCU}=" & AnulaFacturaVenta & ")"
            ElseIf Me.opcORG.Value Then
                strTERCEROS = strTERCEROS & IIf(Len(strTERCEROS) > 0, " AND ", "") & _
                    "({IN_DOCUMENTO.NU_AUTO_DOCU}=" & AnulaFacturaVenta
                strTERCEROS = strTERCEROS & " AND {IN_ENCABEZADO.TX_ESTA_ENCA}<>'A')"
            ElseIf Me.opcANU.Value Then
                strTERCEROS = strTERCEROS & IIf(Len(strTERCEROS) > 0, " AND ", "") & _
                    "({IN_DOCUMENTO.NU_AUTO_DOCU}=" & AnulaFacturaVenta
                strTERCEROS = strTERCEROS & " AND {IN_ENCABEZADO.TX_ESTA_ENCA}='A')"
            End If
            Call ListarD("infCPANL_ODC" & txtDR & ".rpt", strTERCEROS, crptToWindow, "ANULACION FACTURA", MDI_Inventarios, True)
            'NMSR M3428
    End Select
    MDI_Inventarios.Crys_Listar.Formulas(8) = ""
    MDI_Inventarios.Crys_Listar.Formulas(9) = ""
    MDI_Inventarios.Crys_Listar.Formulas(10) = ""
End Sub
'GAPM R2427 CREACION DE PROCEDIMIENTO
Private Sub Form_Activate()
'DAHV R2427. SE COLOCA EN COMENTARIO PORQUE NO SE OBSERVA FUNCIONALIDAD
    'GAPM R2427 INICIO
'    BoIntCont = (Aplicacion.Interfaz_Contabilidad)
'    If BoIntCont = True Then
'        FrmDepe.Enabled = True
'    Else
'        FrmDepe.Enabled = False
'        ChkDepe.Value = vbUnchecked
'        txtdepini.Text = ""
'        txtdepfin.Text = ""
'    End If
    'GAPM R2427 FIN
End Sub

Private Sub Form_Load()
'    Call Main
    BoIntCont = False 'GAPM R2427 INICIALIZACION DE LA VARIABLE
    'DAHV R2427 -  INICIO
    FrmDepe.Enabled = False
    'DAHV R2427 - FIN
    
    Call CenterForm(MDI_Inventarios, Me)
    Me.cboDOCUM.Clear
    Dueno.IniXNit (Dueno.Nit)
    Campos = "NU_AUTO_DOCU, TX_CODI_DOCU, TX_NOMB_DOCU, TX_AFEC_DOCU, TX_INTE_DOCU, NU_AUTO_DOCUDEPE_DOCU, NU_AUTO_DOCUREVE_DOCU, TX_TIPOVAL_DOCU, IN_DOCUMENTO.TX_INDE_DOCU"
    Desde = "IN_DOCUMENTO ORDER BY TX_NOMB_DOCU"
    Condi = ""
    ReDim ArrUXB(4, 0)
    Result = LoadMulData(Desde, Campos, Condi, ArrUXB())
    If Result = FAIL Then GoTo FALLO:
    If Encontro Then
        For CnTdr = 0 To UBound(ArrUXB, 2)
            Set Docume = New ElEncabezado
            Docume.TipDeDcmnt = ArrUXB(0, CnTdr)
            Docume.Actualiza
            CLLDeDocume.Add Docume, "D" & Docume.TipDeDcmnt
            Me.cboDOCUM.AddItem ArrUXB(2, CnTdr)
            Me.cboDOCUM.ItemData(Me.cboDOCUM.NewIndex) = ArrUXB(0, CnTdr)
            Set Docume = Nothing
        Next
    End If
    If Len(FormatDateTime(Date, vbShortDate)) = 8 Then
        Me.txtFCDESDE.Mask = "##/##/##"
        Me.txtFCHASTA.Mask = "##/##/##"
    Else
        Me.txtFCDESDE.Mask = "##/##/####"
        Me.txtFCHASTA.Mask = "##/##/####"
    End If
    Me.txtCPDESDE.Mask = String(7, "9")
    Me.txtCPHASTA.Mask = String(7, "9")
    'GAPM R2427 INICIO
    'ASIGNO LA MASCARA PARA LOS CONTROLES NUEVOS
    'DAHV R2427 LAS DEPENDENCIAS PUEDEN SER DE TIPO TEXTO
    'Me.txtdepini.Mask = String(7, "9")
    'Me.txtdepfin.Mask = String(7, "9")
    'DESHABILITO LOS CONTROLES
    Me.txtdepini.Enabled = False
    Me.txtdepfin.Enabled = False
    'GAPM R2427 FIN
    Me.txtFCDESDE.Text = FormatDateTime(DateAdd("d", -(Day(Date) - 1), Date), vbShortDate)
    Me.txtFCHASTA.Text = FormatDateTime(DateAdd("d", -1, DateAdd("M", 1, DateAdd("d", -(Day(Date) - 1), Date))), vbShortDate)
    Me.chkCPRANGO.Value = False
    Me.chkFCRANGO.Value = False
    Me.txtFCDESDE.Enabled = False
    Me.txtFCHASTA.Enabled = False
    Me.txtCPDESDE.Enabled = False
    Me.txtCPHASTA.Enabled = False
    
'NU_AUTO_BODE, NU_AUTO_BODE_BODE, TX_CODI_BODE, TX_NOMB_BODE, CD_CODI_CECO_BODE, TX_VENTA_BODE
'FROM IN_BODEGA;
    Me.lstBODEGAS.ListItems.Clear
    Me.lstBODEGAS.Checkboxes = False
    Me.lstBODEGAS.MultiSelect = True
    Me.lstBODEGAS.HideSelection = False
    Me.lstBODEGAS.ColumnHeaders.Clear
    'Me.lstBODEGAS.ColumnHeaders.Add , "NOM", "Noombre de la Bodega", 0.66 * Me.lstBODEGAS.Width
    Me.lstBODEGAS.ColumnHeaders.Add , "NOM", "Nombre de la Bodega", 0.66 * Me.lstBODEGAS.Width 'NYCM M2816
    Me.lstBODEGAS.ColumnHeaders.Add , "COD", "C�digo", 0.34 * Me.lstBODEGAS.Width
    
    Me.lstTERCEROS.ListItems.Clear
    Me.lstTERCEROS.Checkboxes = False
    Me.lstTERCEROS.MultiSelect = True
    Me.lstTERCEROS.HideSelection = False
    Me.lstTERCEROS.ColumnHeaders.Clear
'lstTerceros, NOAR, COAR, NOCO, NOUS, NOGR, AUCO, COGR, COUS
    Me.lstTERCEROS.ColumnHeaders.Add , "COTE", "NIT", 0.3 * Me.lstTERCEROS.Width
    Me.lstTERCEROS.ColumnHeaders.Add , "NOTE", "Nombre", 0.6 * Me.lstTERCEROS.Width
FALLO:
End Sub

Private Sub cboDocum_LostFocus()
    'If NumEnCombo = Me.cboDOCUM.ListIndex Then Exit Sub'GAPM R2427 SE DEJA EN COMENTARIO
    If NumEnCombo = Me.cboDOCUM.ListIndex Then ChkDepe.Value = vbUnchecked: Exit Sub 'GAPM R2427 SE DESHABILITA EL CONTROL
    Dim tipo As String * 1
    Me.lstBODEGAS.ListItems.Clear
    Me.lstTERCEROS.ListItems.Clear
    Me.cboDOCUM.Width = 2895
    Me.pgbLLEVO.Value = 0
    Me.pgbLLEVO.Visible = True
    'GAPM R2427 INICIO
    'valido si el documento es baja por consumo
    If Me.cboDOCUM.Text = "BAJA CONSUMO" Then
        'valido si inventarios tiene interfaz con contabilidad
        BoIntCont = (Aplicacion.Interfaz_Contabilidad)
        If BoIntCont = True Then
            FrmDepe.Enabled = True
            txtdepini.Text = ""
            txtdepfin.Text = ""
        Else
            FrmDepe.Enabled = False
            ChkDepe.Value = vbUnchecked
            txtdepini.Text = ""
            txtdepfin.Text = ""
        End If
    Else
        ChkDepe.Value = vbUnchecked
        txtdepini.Text = ""
        txtdepfin.Text = ""
        FrmDepe.Enabled = False
    End If
    'GAPM R2427 FIN
    Campos = "NU_AUTO_BODE, NU_AUTO_BODE_BODE, TX_CODI_BODE, TX_NOMB_BODE, " & _
        "CD_CODI_CECO_BODE, TX_VENTA_BODE"
    Desde = "IN_BODEGA ORDER BY TX_NOMB_BODE"
    Condi = ""
    ReDim ArrUXB(5, 0)
    Result = LoadMulData(Desde, Campos, Condi, ArrUXB())
    If Result = FAIL Then GoTo FALLO
    On Error GoTo SIGUI
    For CnTdr = 0 To UBound(ArrUXB, 2)
        Me.lstBODEGAS.ListItems.Add , "B" & ArrUXB(2, CnTdr), ArrUXB(3, CnTdr)
        Me.lstBODEGAS.ListItems("B" & ArrUXB(2, CnTdr)).ListSubItems.Add , "COD", ArrUXB(2, CnTdr)
SIGUI:
    Next
    On Error GoTo 0
'SELECT CD_CODI_TERC, NO_NOMB_TERC, DE_CIUD_TERC
'FROM TERCERO;
    
'SELECT TERCERO.CD_CODI_TERC, TERCERO.NO_NOMB_TERC, TERCERO.DE_CIUD_TERC, IN_ENCABEZADO.NU_COMP_ENCA
'FROM IN_ENCABEZADO INNER JOIN TERCERO ON IN_ENCABEZADO.CD_CODI_TERC_ENCA = TERCERO.CD_CODI_TERC
'WHERE (((IN_ENCABEZADO.NU_COMP_ENCA)=10));
'AASV M5776 Inicio
'    Campos = "DISTINCT CD_CODI_TERC, NO_NOMB_TERC, DE_CIUD_TERC"
'    Desde = "TERCERO, IN_ENCABEZADO, IN_DOCUMENTO"
'    Condi = "NU_AUTO_DOCU_ENCA = NU_AUTO_DOCU" & _
'        " AND CD_CODI_TERC_ENCA = CD_CODI_TERC" & _
'        " AND NU_AUTO_DOCU=" & Me.cboDOCUM.ItemData(Me.cboDOCUM.ListIndex) & " ORDER BY NO_NOMB_TERC"
    Campos = " DISTINCT TERCERO.CD_CODI_TERC, TERCERO.NO_NOMB_TERC, TERCERO.DE_CIUD_TERC"
    Desde = " TERCERO INNER JOIN"
    Desde = Desde & " IN_ENCABEZADO ON TERCERO.CD_CODI_TERC = IN_ENCABEZADO.CD_CODI_TERC_ENCA"
    Condi = " (IN_ENCABEZADO.NU_AUTO_DOCU_ENCA = " & Me.cboDOCUM.ItemData(Me.cboDOCUM.ListIndex) & ") ORDER BY NO_NOMB_TERC"
'AASV M5776 Fin
    ReDim ArrUXB(2, 0)
    Result = LoadMulData(Desde, Campos, Condi, ArrUXB())
    If Result = FAIL Then GoTo FALLO
    If Not Encontro Then GoTo NOENC
    Me.pgbLLEVO.Min = 0
    Me.pgbLLEVO.Value = 0
    Me.pgbLLEVO.Max = UBound(ArrUXB, 2) + 1
    Dim Tercero As New ElTercero
    For CnTdr = 0 To UBound(ArrUXB, 2)
        Me.pgbLLEVO.Value = CnTdr
        Tercero.Nit = ArrUXB(0, CnTdr)
        Tercero.Nombre = ArrUXB(1, CnTdr)
        Me.lstTERCEROS.ListItems.Add , "T" & CnTdr, Trim(Tercero.Nit)
        Me.lstTERCEROS.ListItems("T" & CnTdr).ListSubItems.Add , "NOTE", Trim(Tercero.Nombre)
    Next
    Set Tercero = Nothing
NOENC:
FALLO:
    Me.pgbLLEVO.Visible = False
    Me.cboDOCUM.Width = 5295
End Sub

Private Sub lstBODEGAS_ColumnClick(ByVal ColumnHeader As MSComctlLib.ColumnHeader)
    Me.lstBODEGAS.SortKey = ColumnHeader.Index - 1
    Me.lstBODEGAS.Sorted = True
End Sub
'GAPM R2427 CREACION DE PROCEDIMIETO
Private Sub lstBODEGAS_KeyPress(KeyAscii As Integer)
    Call Cambiar_Enter(KeyAscii) 'GAPM R2427 COLOCO ENTER COMO ENTE DE TABULACION
End Sub

Private Sub lstTERCEROS_ColumnClick(ByVal ColumnHeader As MSComctlLib.ColumnHeader)
    Me.lstTERCEROS.SortKey = ColumnHeader.Index - 1
    Me.lstTERCEROS.Sorted = True
End Sub
'GAPM R2427 CREACION DE PROCEDIMIENTO
Private Sub lstTERCEROS_KeyPress(KeyAscii As Integer)
    Call Cambiar_Enter(KeyAscii) 'GAPM R2427 COLOCO ENTER COMO ENTE DE TABULACION
End Sub


'GAPM R2427 CREACION DE PROCEDIMIENTO
Private Sub opcANU_KeyPress(KeyAscii As Integer)
    Call Cambiar_Enter(KeyAscii) 'GAPM R2427 COLOCO ENTER COMO ENTE DE TABULACION
End Sub
'GAPM R2427 CREACION DE PROCEDIMIENTO
Private Sub opcCON_KeyPress(KeyAscii As Integer)
    Call Cambiar_Enter(KeyAscii) 'GAPM R2427 COLOCO ENTER COMO ENTE DE TABULACION
End Sub
'GAPM R2427 CREACION DE PROCEDIMIENTO
Private Sub opcDET_KeyPress(KeyAscii As Integer)
    Call Cambiar_Enter(KeyAscii) 'GAPM R2427 COLOCO ENTER COMO ENTE DE TABULACION
End Sub
'GAPM R2427 CREACION DE PROCEDIMIENTO
Private Sub opcMOD_KeyPress(KeyAscii As Integer)
    Call Cambiar_Enter(KeyAscii) 'GAPM R2427 COLOCO ENTER COMO ENTE DE TABULACION
End Sub
'GAPM R2427 CREACION DE PROCEDIMIENTO
Private Sub opcORG_KeyPress(KeyAscii As Integer)
    Call Cambiar_Enter(KeyAscii) 'GAPM R2427 COLOCO ENTER COMO ENTE DE TABULACION
End Sub
'GAPM R2427 CREACION DE PROCEDIMIENTO
Private Sub opcRES_KeyPress(KeyAscii As Integer)
    Call Cambiar_Enter(KeyAscii) 'GAPM R2427 COLOCO ENTER COMO ENTE DE TABULACION
End Sub
'GAPM R2427 CREACION DE PROCEDIMIENTO
Private Sub opcSIN_KeyPress(KeyAscii As Integer)
    Call Cambiar_Enter(KeyAscii) 'GAPM R2427 COLOCO ENTER COMO ENTE DE TABULACION
End Sub

'GAPM R2427 CREACION DE PROCEDIMIENTO
Private Sub txtCPDESDE_KeyPress(KeyAscii As Integer)
    Call Cambiar_Enter(KeyAscii) 'GAPM R2427 COLOCO ENTER COMO ENTE DE TABULACION
End Sub
'GAPM R2427 CREACION DE PROCEDIMIENTO
Private Sub txtCPHASTA_KeyPress(KeyAscii As Integer)
    Call Cambiar_Enter(KeyAscii) 'GAPM R2427 COLOCO ENTER COMO ENTE DE TABULACION
End Sub

'GAPM R2427 CREACION DE PROCEDIMIENTO
Private Sub txtdepfin_KeyPress(KeyAscii As Integer)
    Call Cambiar_Enter(KeyAscii) 'GAPM R2427 COLOCO ENTER COMO ENTE DE TABULACION
End Sub

'GAPM R2427 CREACION DE PORCEDIMIENTO
Private Sub txtdepini_KeyPress(KeyAscii As Integer)
    Call Cambiar_Enter(KeyAscii) 'GAPM R2427 COLOCO ENTER COMO ENTE DE TABULACION
End Sub

Private Sub txtFCDESDE_KeyPress(KeyAscii As Integer)
    Call Cambiar_Enter(KeyAscii) 'GAPM R2427 COLOCO ENTER COMO ENTE DE TABULACION
End Sub

Private Sub txtFCDESDE_LostFocus()
    If IsDate(Me.txtFCDESDE.Text) Then Exit Sub
    Me.txtFCDESDE.Text = FormatDateTime(DateAdd("d", -(Day(Date) - 1), Date), vbShortDate)
    Me.txtFCDESDE.SetFocus
End Sub

Private Sub txtFCHASTA_LostFocus()
    'smdl m1876
    If IsDate(Me.txtFCHASTA.Text) Then
       If DateDiff("d", CDate(Me.txtFCDESDE.Text), CDate(Me.txtFCHASTA)) < 0 Then
         Call Mensaje1("La fecha final debe ser mayor a la inicial.", 3)
         Me.txtFCHASTA.SetFocus
       End If
    Else
    'smdl m1876
       Me.txtFCHASTA.Text = FormatDateTime(DateAdd("d", -1, DateAdd("M", 1, DateAdd("d", -(Day(Date) - 1), Date))), vbShortDate)
       Me.txtFCHASTA.SetFocus
    End If
End Sub
Private Sub txtFCHASTA_KeyPress(KeyAscii As Integer) 'smdl m 1876
    Call Cambiar_Enter(KeyAscii)
End Sub
