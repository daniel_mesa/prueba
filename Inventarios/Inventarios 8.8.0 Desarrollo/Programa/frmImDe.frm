VERSION 5.00
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "MSFLXGRD.OCX"
Object = "{6B7E6392-850A-101B-AFC0-4210102A8DA7}#1.3#0"; "COMCTL32.OCX"
Begin VB.Form frmImDe 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Selecci�n de impuestos y descuentos"
   ClientHeight    =   4290
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   9810
   Icon            =   "frmImDe.frx":0000
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   4290
   ScaleWidth      =   9810
   StartUpPosition =   3  'Windows Default
   Begin VB.ListBox lstImpu 
      Height          =   450
      ItemData        =   "frmImDe.frx":058A
      Left            =   4440
      List            =   "frmImDe.frx":058C
      TabIndex        =   13
      Top             =   600
      Visible         =   0   'False
      Width           =   1695
   End
   Begin VB.ListBox lstConcepto 
      Height          =   255
      ItemData        =   "frmImDe.frx":058E
      Left            =   7680
      List            =   "frmImDe.frx":0590
      TabIndex        =   12
      Top             =   240
      Visible         =   0   'False
      Width           =   1335
   End
   Begin VB.ListBox lstDesc 
      Height          =   450
      ItemData        =   "frmImDe.frx":0592
      Left            =   6360
      List            =   "frmImDe.frx":0594
      TabIndex        =   11
      Top             =   600
      Visible         =   0   'False
      Width           =   1695
   End
   Begin VB.ComboBox cmbConcepto 
      Height          =   315
      Left            =   3000
      Style           =   2  'Dropdown List
      TabIndex        =   9
      Top             =   120
      Width           =   4095
   End
   Begin VB.PictureBox picHelp 
      Appearance      =   0  'Flat
      AutoSize        =   -1  'True
      BackColor       =   &H80000005&
      BorderStyle     =   0  'None
      ForeColor       =   &H80000008&
      Height          =   120
      Index           =   0
      Left            =   7155
      ScaleHeight     =   120
      ScaleWidth      =   120
      TabIndex        =   8
      Tag             =   $"frmImDe.frx":0596
      ToolTipText     =   "Ayuda"
      Top             =   120
      Width           =   120
   End
   Begin VB.CommandButton cmdCancel 
      Cancel          =   -1  'True
      Caption         =   "&CANCELAR"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   6
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   495
      Left            =   8520
      Picture         =   "frmImDe.frx":0694
      Style           =   1  'Graphical
      TabIndex        =   5
      Top             =   3720
      Width           =   1095
   End
   Begin VB.CommandButton cmdOk 
      Caption         =   "&ACEPTAR"
      Default         =   -1  'True
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   6
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   495
      Left            =   7080
      Picture         =   "frmImDe.frx":09D6
      Style           =   1  'Graphical
      TabIndex        =   4
      Top             =   3720
      Width           =   1095
   End
   Begin MSFlexGridLib.MSFlexGrid flexImpDesc 
      Height          =   2895
      Left            =   4080
      TabIndex        =   3
      Top             =   600
      Width           =   5535
      _ExtentX        =   9763
      _ExtentY        =   5106
      _Version        =   393216
      Cols            =   7
      FixedCols       =   0
   End
   Begin VB.CommandButton cmdRemove 
      BeginProperty Font 
         Name            =   "Wingdings"
         Size            =   12
         Charset         =   2
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   3480
      Picture         =   "frmImDe.frx":0D18
      Style           =   1  'Graphical
      TabIndex        =   2
      Top             =   1920
      Width           =   495
   End
   Begin VB.CommandButton cmdAdd 
      BeginProperty Font 
         Name            =   "Wingdings"
         Size            =   12
         Charset         =   2
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   3480
      Picture         =   "frmImDe.frx":106A
      Style           =   1  'Graphical
      TabIndex        =   1
      ToolTipText     =   "Agregar (F6)"
      Top             =   1440
      Width           =   495
   End
   Begin ComctlLib.TreeView treeImDe 
      Height          =   2895
      Left            =   240
      TabIndex        =   0
      Top             =   600
      Width           =   3135
      _ExtentX        =   5530
      _ExtentY        =   5106
      _Version        =   327682
      Indentation     =   176
      LabelEdit       =   1
      LineStyle       =   1
      Style           =   7
      ImageList       =   "imglstImDe"
      Appearance      =   1
   End
   Begin VB.Label lblEtiq 
      Alignment       =   1  'Right Justify
      AutoSize        =   -1  'True
      Caption         =   "Filtrar por concepto"
      Height          =   195
      Index           =   2
      Left            =   1560
      TabIndex        =   10
      Top             =   120
      Width           =   1365
   End
   Begin VB.Image imgImDe 
      Height          =   240
      Index           =   10
      Left            =   2040
      Picture         =   "frmImDe.frx":13BC
      Top             =   2880
      Visible         =   0   'False
      Width           =   240
   End
   Begin VB.Image imgImDe 
      Height          =   240
      Index           =   9
      Left            =   1680
      Picture         =   "frmImDe.frx":1506
      Top             =   2880
      Visible         =   0   'False
      Width           =   240
   End
   Begin VB.Image imgImDe 
      Height          =   240
      Index           =   5
      Left            =   1680
      Picture         =   "frmImDe.frx":1650
      Top             =   2280
      Visible         =   0   'False
      Width           =   240
   End
   Begin VB.Image imgImDe 
      Height          =   240
      Index           =   7
      Left            =   2280
      Picture         =   "frmImDe.frx":1BDA
      Top             =   3000
      Visible         =   0   'False
      Width           =   240
   End
   Begin VB.Label lblEtiq 
      Alignment       =   2  'Center
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "�"
      BeginProperty Font 
         Name            =   "Wingdings"
         Size            =   20.25
         Charset         =   2
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00800080&
      Height          =   450
      Index           =   1
      Left            =   3555
      TabIndex        =   7
      Top             =   960
      Visible         =   0   'False
      Width           =   345
   End
   Begin VB.Label lblEtiq 
      Alignment       =   2  'Center
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "I"
      BeginProperty Font 
         Name            =   "Wingdings"
         Size            =   20.25
         Charset         =   2
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00800000&
      Height          =   450
      Index           =   0
      Left            =   3480
      TabIndex        =   6
      Top             =   2400
      Visible         =   0   'False
      Width           =   495
   End
   Begin ComctlLib.ImageList imglstImDe 
      Left            =   6480
      Top             =   840
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      MaskColor       =   12632256
      _Version        =   327682
   End
   Begin VB.Image imgImDe 
      Height          =   240
      Index           =   6
      Left            =   2280
      Picture         =   "frmImDe.frx":1D24
      Top             =   2760
      Visible         =   0   'False
      Width           =   240
   End
   Begin VB.Image imgImDe 
      Height          =   240
      Index           =   8
      Left            =   2040
      Picture         =   "frmImDe.frx":1E6E
      Top             =   3120
      Visible         =   0   'False
      Width           =   240
   End
   Begin VB.Image imgImDe 
      Height          =   240
      Index           =   4
      Left            =   1320
      Picture         =   "frmImDe.frx":1FB8
      Top             =   3000
      Visible         =   0   'False
      Width           =   240
   End
   Begin VB.Image imgImDe 
      Height          =   240
      Index           =   3
      Left            =   1560
      Picture         =   "frmImDe.frx":2102
      Top             =   3000
      Visible         =   0   'False
      Width           =   240
   End
   Begin VB.Image imgImDe 
      Height          =   240
      Index           =   1
      Left            =   960
      Picture         =   "frmImDe.frx":224C
      Top             =   3000
      Visible         =   0   'False
      Width           =   240
   End
   Begin VB.Image imgImDe 
      Height          =   240
      Index           =   0
      Left            =   480
      Picture         =   "frmImDe.frx":278E
      Top             =   3000
      Visible         =   0   'False
      Width           =   240
   End
   Begin VB.Image imgImDe 
      Height          =   240
      Index           =   2
      Left            =   960
      Picture         =   "frmImDe.frx":2CD0
      Top             =   3000
      Visible         =   0   'False
      Width           =   240
   End
End
Attribute VB_Name = "frmImDe"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Dim arrImp(), arrDesc(), lgRCount As Long
Dim dbVlNeto As Double

'HRR M1698
Public AutoRet As String, AutRetIva As String, AutRetIca As String
Public TipoRegimen As String, GranContrib As String
Public ExentoIva As String, ExentoIca As String, ExentoFuente As String
Public TipoPers As String
'HRR M1698

Sub sbCargarImpDesc()

Dim stDCondi As String, stICondi As String '|.DR.|, R:1631
Dim VrArrI() As Variant 'HRR M1698
Dim VrArr(0) As Variant 'HRR M1698
Dim InI As Integer 'HRR M1698
Dim InJ As Integer 'HRR M1698
Dim Ink As Integer 'HRR M1698
Dim BoImpO As Boolean 'HRR M1698


BoImpO = True 'HRR M1698
Ink = 0 'HRR M1698
    
    'AA |.DR.|, R:1631
    If cmbConcepto.ListIndex > -1 Then
        stDCondi = " CD_CODI_DESC In " & lstDesc.List(cmbConcepto.ListIndex)
        stICondi = " CD_CODI_IMPU In " & lstImpu.List(cmbConcepto.ListIndex)
    End If
    'AA Fin.

    'HRR M1698
'    '00 Cargar los impuestos y descuentos a las matrices.
'        ReDim arrDesc(6, 0)
'        Result = LoadMulData("DESCUENTO", "CD_CODI_DESC,DE_NOMB_DESC,PR_PORC_DESC,VL_TOPE_DESC,CD_CUEN_DESC,VL_VALO_DESC,TX_TIPO_DESC", stDCondi, arrDesc())
'        ReDim arrImp(6, 0)
'        'Result = LoadMulData("TC_IMPUESTOS", "CD_CODI_IMPU,DE_NOMB_IMPU,PR_PORC_IMPU,'0',ID_SUMA_IMPU,CD_CUEN_IMPU,ID_TIPO_IMPU", stICondi, arrImp())
'        Result = LoadMulData("TC_IMPUESTOS", "CD_CODI_IMPU,DE_NOMB_IMPU,PR_PORC_IMPU,VL_TOPE_IMPU,ID_SUMA_IMPU,CD_CUEN_IMPU,ID_TIPO_IMPU", stICondi, arrImp())
'    '00 Fin.
   'HRR M1698
   
    '01 Llenar el �rbol
        Dim I&
        flexImpDesc.Rows = 1
        lgRCount = 0
        treeImDe.Nodes.Clear
        treeImDe.Nodes.Add , , "IR", "Impuestos", 4: treeImDe.Nodes("IR").Tag = "x"
        treeImDe.Nodes.Add , , "DR", "Descuentos", 5: treeImDe.Nodes("DR").Tag = "x"
        
        'HRR M1698
'        For I& = 0 To UBound(arrImp, 2)
'            treeImDe.Nodes.Add "IR", tvwChild, "I" & I&, arrImp(1, I&), IIf(arrImp(6, I&) = "M", 6, 9)
'        Next I&
        'HRR M1698
        
        'HRR M1698
        ReDim arrDesc(6, 0)
        ReDim arrImp(6, 0)
        ReDim Arr(6, 0)
        Result = LoadMulData("DESCUENTO", "CD_CODI_DESC,DE_NOMB_DESC,PR_PORC_DESC,VL_TOPE_DESC,CD_CUEN_DESC,VL_VALO_DESC,TX_TIPO_DESC", stDCondi, arrDesc())
        ReDim arrImp(6, 0)
        Condicion = "( ID_TIPO_IMPU NOT IN('I','M'))" 'HRR M2001 Impuestos distinto de iva de reteiva.
        If AutoRet = "1" Or ExentoFuente = "1" Then Condicion = Condicion & IIf(Condicion <> NUL$, " AND ", "") & " ID_TIPO_IMPU <> 'R'"
        'If AutRetIva = "1" Or ExentoIva = "1" Then Condicion = Condicion & IIf(Condicion <> NUL$, " AND ", "") & " ID_TIPO_IMPU <> 'M'" 'HRR M2001
        If AutRetIca = "1" Or ExentoIca = "1" Then Condicion = Condicion & IIf(Condicion <> NUL$, " AND ", "") & " ID_TIPO_IMPU <> 'C'"
        Result = FiltrarImpuestos("CD_CODI_IMPU", "TC_IMPUESTOS", Condicion, TipoPers, TipoRegimen, GranContrib, VrArrI)
           If Result <> FAIL Then
            For InJ = 0 To UBound(VrArrI())
                Condicion = "CD_CODI_IMPU=" & Comi & VrArrI(InJ) & Comi
                Result = LoadMulData("TC_IMPUESTOS", "CD_CODI_IMPU,DE_NOMB_IMPU,PR_PORC_IMPU,VL_TOPE_IMPU,ID_SUMA_IMPU,CD_CUEN_IMPU,ID_TIPO_IMPU", Condicion, Arr)
                If Result <> FAIL Then
                   If Encontro Then
                      BoImpO = True 'HRR M1698
                      'HRR M1698 'Trae unicamente los impuestos tipo "otros impuestos" del concepto de compra de inventarios.
                      If Arr(6, 0) = "O" Then
                         If Arr(0, 0) <> NUL$ Then
                            BoImpO = False
                            Desde = "R_CONC_IMPU"
                            Condicion = " CD_CONC_COIM=" & Comi & "CINV" & Comi ' Codigo del concepto compra de inventarios
                            Condicion = Condicion & " AND CD_IMPU_COIM='" & Arr(0, 0) & Comi
                            Result = LoadData(Desde, "CD_IMPU_COIM", Condicion, VrArr)
                            If Result <> FAIL And Encontro Then
                               BoImpO = True
                            End If
                         End If
                      End If
                      'HRR M1698
                      If BoImpO Then 'HRR M1698
                           For InI = 0 To UBound(Arr, 2)
                              treeImDe.Nodes.Add "IR", tvwChild, "I" & Ink, Arr(1, InI), IIf(Arr(6, InI) = "M", 6, 9)
                           Next InI
                           ReDim Preserve arrImp(6, Ink) 'se hizo esto para no afectar lo que realizo Daniel
                           For InI = 0 To 5
                              arrImp(InI, Ink) = Arr(InI, 0)
                           Next InI
                           Ink = Ink + 1
                      End If
                   End If
                End If
            Next InJ
         End If
   
        'HRR M1698
       
        For I& = 0 To UBound(arrDesc, 2)
            treeImDe.Nodes.Add "DR", tvwChild, "D" & I&, arrDesc(1, I&), IIf(Val(arrDesc(6, I&)), 7, 8)
        Next I&
    '01 Fin.

End Sub

Sub sbFlexImg()
    
    Dim I&
    
    With flexImpDesc
        For I& = 1 To .Rows - 1
            .Row = I&: .Col = 6
            If .TextMatrix(I&, 6) = " " Then
                Set .CellPicture = imglstImDe.ListImages(2).Picture
            Else
                Set .CellPicture = imglstImDe.ListImages(1).Picture
            End If
        Next I&
    End With
    
End Sub

Sub sbRecursos()
    
    Dim obItera As Object
    
    'Cargas iniciales:
    imglstImDe.ListImages.Clear
    For Each obItera In imgImDe
        imglstImDe.ListImages.Add obItera.Index + 1, "A" & obItera.Index, obItera.Picture
    Next obItera
End Sub

'Function fnSelDescImp(ByRef arrData()) As Boolean
Function fnSelDescImp(ByRef arrData(), dbNeto As Double) As Boolean 'PJCA M1862
'Rellena la matriz del par�metro con la selecci�n del usuario,
' creando un arreglo similar a este aspecto:
'    0  |  1
'--------------
'0 | I  |  024        arrData(0, 0)  arrData(1, 0)
'1 | I  |  456        arrData(0, 1)  arrData(1, 1)
'2 | D  |  F12        arrData(0, 2)  arrData(1, 2)
'x |... |...          arrData(0, x)  arrData(1, x)
'
' La primera "columna" contendr� el tipo (imp/desc), la segunda el c�digo asignado.
' Si el usuario cancela o hace clic en aceptar sin agregar elementos, se
'  devuelve False y no se modifica la matriz.
' Si el usuario agrega uno o m�s elementos y hace click en aceptar devuelve True.
        dbVlNeto = dbNeto
        frmImDe.Show 1
        
        '00 Rellenar la matriz que el usuario envio con los datos de la selecci�n:
        Dim I&
        
        fnSelDescImp = (lgRCount > 0)
        If Not (lgRCount > 0) Then Unload frmImDe: lgRCount = 0: Exit Function
        
        '00 Rellenar la matriz que el usuario envio con los datos de la selecci�n:
        'Dim i&
        
        With flexImpDesc
          If .Rows <> 1 Then    'REOL
            ReDim arrData(1, .Rows - 2)
            For I& = 0 To .Rows - 2
                arrData(0, I&) = .TextMatrix(I& + 1, 1) 'Tipo (I) impuesto o (D) descuento
                arrData(1, I&) = .TextMatrix(I& + 1, 2) 'C�digo del impuesto o descuento.
            Next I&
          End If
        End With
        '00 Fin.
        
        'lgRCount = 0
        'Unload frmImDe     PJCA M1864
            
End Function

Private Sub cmbConcepto_Click()
        If cmbConcepto.ListIndex = cmbConcepto.ListCount - 1 Then cmbConcepto.ListIndex = -1 '|.DR.|, R:1631
        sbCargarImpDesc '|.DR.|, R:1631
End Sub


Private Sub cmdAdd_Click()
    
        Dim lgIndex As Long, stType As String * 1

    '00 Agregar nodo seleccionado a la grilla.
    If treeImDe.SelectedItem.Tag <> "x" Then
        flexImpDesc.Rows = lgRCount + 1
        'treeImDe.SelectedItem.Tag = "x"
        'treeImDe.SelectedItem.Image = 3
        lgIndex = Val(Mid(treeImDe.SelectedItem.Key, 2))
        stType = Mid(treeImDe.SelectedItem.Key, 1, 1)
        'If stType = "I" Then flexImpDesc.AddItem treeImDe.SelectedItem.Key & vbTab & stType & vbTab & arrImp(0, lgIndex) & vbTab & arrImp(1, lgIndex) & vbTab & arrImp(2, lgIndex) & vbTab & arrImp(3, lgIndex) & vbTab & " "
        'If stType = "D" Then flexImpDesc.AddItem treeImDe.SelectedItem.Key & vbTab & stType & vbTab & arrDesc(0, lgIndex) & vbTab & arrDesc(1, lgIndex) & vbTab & arrDesc(2, lgIndex) & vbTab & arrDesc(5, lgIndex) & vbTab & ""
        If stType = "I" Then
            If arrImp(3, lgIndex) > dbVlNeto Then
                Call Mensaje1("El valor de la Compra no supera el Tope del Impuesto", 2)
                Exit Sub
            Else
                flexImpDesc.AddItem treeImDe.SelectedItem.Key & vbTab & stType & vbTab & arrImp(0, lgIndex) & vbTab & arrImp(1, lgIndex) & vbTab & arrImp(2, lgIndex) & vbTab & arrImp(3, lgIndex) & vbTab & " "
            End If
        ElseIf stType = "D" Then
            If arrDesc(3, lgIndex) > dbVlNeto Then
                Call Mensaje1("El valor de la Compra no supera el Tope del Descuento", 2)
                Exit Sub
            Else
                flexImpDesc.AddItem treeImDe.SelectedItem.Key & vbTab & stType & vbTab & arrDesc(0, lgIndex) & vbTab & arrDesc(1, lgIndex) & vbTab & arrDesc(2, lgIndex) & vbTab & arrDesc(5, lgIndex) & vbTab & ""
            End If
        End If
        treeImDe.SelectedItem.Tag = "x"
        treeImDe.SelectedItem.Image = 3
        sbFlexImg
        lgRCount = lgRCount + 1
    Else
        sbVFlash 0, True, lblEtiq(1)
    End If
    '00 Fin.

End Sub

Private Sub cmdCancel_Click()
        
        If lgRCount > 1 Then If Not WarnMsg("�Est� seguro que desea perder los cambios?") Then Exit Sub
        lgRCount = 0
        frmImDe.Hide
        
End Sub

Private Sub cmdOk_Click()
        frmImDe.Hide
End Sub

Private Sub cmdRemove_Click()
        
        If lgRCount < 1 Then sbVFlash 0, True, lblEtiq(0): Exit Sub
        
        With flexImpDesc
            If .TextMatrix(.Row, 1) = "I" Then treeImDe.Nodes(.TextMatrix(.Row, 0)).Image = 11
            If .TextMatrix(.Row, 1) = "D" Then treeImDe.Nodes(.TextMatrix(.Row, 0)).Image = 10
            treeImDe.Nodes(.TextMatrix(.Row, 0)).Tag = ""
            If .Rows <= 2 Then
                .Rows = 1
                .Rows = 2
            Else
                .RemoveItem .Row
            End If
            lgRCount = lgRCount - 1
        End With
        
End Sub

Private Sub Form_KeyUp(KeyCode As Integer, Shift As Integer)
        If KeyCode = 117 Then cmdAdd_Click
End Sub

Sub sbCargRecursos() '|.DR.|
 picHelp(0).Picture = MDI_Inventarios.imgH.Picture
End Sub

Sub sbCListas()

        '01 |.DR.|, Agregar todos los conceptos de movimiento tipo "inventarios".
Dim ArrX(), I&, J&, A$
ReDim ArrX(1, 0)

    Condicion = "ID_TIPO_CONC=12"
    Result = LoadMulData("Concepto", "CD_CODI_CONC,DE_DESC_CONC", Condicion, ArrX())
    
    cmbConcepto.Clear
    lstConcepto.Clear
    
    For I& = 0 To UBound(ArrX, 2)
        lstConcepto.AddItem ArrX(0, I&)
        cmbConcepto.AddItem ArrX(1, I&)
    Next I&
        
        cmbConcepto.AddItem " - (Anular filtro) - "
        
        '01 Fin.
        
        '02 |.DR.|, Agregar los impuestos con los que est� relacionado cada Concepto.
        ReDim ArrX(0, 0)
    
                lstImpu.Clear
            For I& = 0 To lstConcepto.ListCount - 1
            Condicion = "CD_CONC_COIM='" & lstConcepto.List(I&) & "'"
            Result = LoadMulData("R_CONC_IMPU", "CD_IMPU_COIM", Condicion, ArrX())
                A$ = ""
                For J& = 0 To UBound(ArrX, 2)
                    A$ = A$ & "'" & ArrX(0, J&) & "',"
                Next J&
                    If Encontro And Result <> FAIL Then
                        A$ = Mid(A$, 1, Len(A$) - 1)
                        A$ = "(" & A$ & ")"
                    Else
                        A$ = "(Null)"
                    End If
                    lstImpu.AddItem A$
            Next I&
        '02 Fin.

        '03 |.DR.|, Agregar los descuentos con los que est� relacionado cada Concepto.
        ReDim ArrX(0, 0)
    
                lstDesc.Clear
            For I& = 0 To lstConcepto.ListCount - 1
            Condicion = "CD_CONC_CODE='" & lstConcepto.List(I&) & "'"
            Result = LoadMulData("R_CONC_DESC", "CD_DESC_CODE", Condicion, ArrX())
                A$ = ""
                For J& = 0 To UBound(ArrX, 2)
                    A$ = A$ & "'" & ArrX(0, J&) & "',"
                Next J&
                    If Encontro And Result <> FAIL Then
                        A$ = Mid(A$, 1, Len(A$) - 1)
                        A$ = "(" & A$ & ")"
                    Else
                        A$ = "(Null)"
                    End If
                    lstDesc.AddItem A$
            Next I&
        '03 Fin.


End Sub

Private Sub Form_Load()

        CenterForm MDI_Inventarios, Me
        sbRecursos
        
    sbCargarImpDesc
  
    '02 Uso del filtro:
        sbCargRecursos
        sbCListas
    '02 Fin.
  
    GrdFDef flexImpDesc, 0, 0, "Key,1,Tipo,1,C�digo,1,Descripci�n,3500,%,750,Valor,750, ,250"

End Sub


Private Sub picHelp_Click(Index As Integer)
    h.sbMsg picHelp(Index).Tag, picHelp(Index)
End Sub


