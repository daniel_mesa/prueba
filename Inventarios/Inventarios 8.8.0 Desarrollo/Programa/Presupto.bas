Attribute VB_Name = "Presupto"
'VALIDA QUE NO EXISTAN NIVELES SUPERIORES; TRUE, FALSO
Function Valida_Nivel_Articulo(Articulo As String, tipo As Byte) As Boolean
ReDim Arr(0, 0)
     If tipo = 1 Then 'ingresos
        Condicion = "CD_CODI_INGR LIKE '" & Cambiar_Comas_Comillas(Articulo) & CaractLike & Comi
        Result = LoadMulData("INGRESOS", "CD_CODI_INGR", Condicion, Arr())
     Else 'gastos
        Condicion = "CD_CODI_GAST LIKE '" & Cambiar_Comas_Comillas(Articulo) & CaractLike & Comi
        Result = LoadMulData("GASTOS", "CD_CODI_GAST", Condicion, Arr())
     End If
     If Result <> FAIL Then
        If UBound(Arr(), 2) > 0 Then
           Valida_Nivel_Articulo = True
        Else
           Valida_Nivel_Articulo = False
        End If
     Else
        Valida_Nivel_Articulo = True
     End If
End Function
'opcion 1.ingresos 0. gastos 2. Cuentas
Function LoadfGrid_Nivel(Grid As MSFlexGrid, ByVal TableName As String, ByVal Fields As String, ByVal Condi As Variant, ByVal Opcion As Byte) As Integer
   Dim I As Integer
'   Dim j As Integer    'DEPURACION DE CODIGO
   Dim N As Integer
   Dim l As Integer
   Dim S As String
   ReDim Datos(1, 0) As Variant
   Dim Existe As Boolean
   Dim Conta As Integer

   DoEvents
  
   Grid.Rows = 1
   Grid.Row = 0
   Encontro = False
   If (LoadMulData(TableName, Fields, Condi, Datos()) <> FAIL) Then
       N = 0
       l = Grid.Cols - 1
       I = UBound(Datos, 2)
       If (I < (l - N + 1)) Then
       End If
       Call MouseClock
       For Conta = 0 To I
           S = Datos(0, Conta) & Chr$(9) & Datos(1, Conta)
          'Se ha llegado al m�ximo soportado por Visual Basic para un grid?
           If (Conta = 2000) Then
              Call MouseNorm
              Call Mensaje1("Indique parametros mas precisos para seleccionar", 3)
              Exit For
           End If
           If Opcion = 2 Then
              Existe = Valida_Nivel_Cuentas(CStr(Datos(0, Conta)))
           Else
              Existe = Valida_Nivel_Articulo(CStr(Datos(0, Conta)), Opcion)
           End If
           If Existe = False Then Grid.AddItem S
       Next
       Call MouseNorm
       On Error Resume Next
       Grid.RemoveItem 0  'Grid.Rows - 1
       Result = SUCCEED
   Else
      Result = FAIL
   End If
   If Encontro = False Then Grid.Rows = 1
   Call FreeBufs(Result)
   Grid.Visible = True
   LoadfGrid_Nivel = Result
End Function

'Tipo => 0. Gastos 1. Ingresos
'CV => Columna Valor    CC => Columna Cantidad
'Function Articulos_Presupuesto(GrdArti As MSFlexGrid, campo As String, CV As Byte, tipo As Byte) As Variant
'Function Articulos_Presupuesto(GrdArti As MSFlexGrid, CV As Byte, tipo As Byte) As Variant       'DEPURACION DE CODIGO
'Function Articulos_Presupuesto(GrdArti As MSFlexGrid, CV As Byte, tipo As Byte, Optional Bolfactu As Boolean) As Variant  'JACC M3206 'HRR M1818
'Function Articulos_Presupuesto(GrdArti As MSFlexGrid, CV As Byte, tipo As Byte, Optional Bolfactu As Boolean, Optional StDoc As String = NUL$) As Variant 'JACC M3206 'HRR M1818 'NMSR M3544
'Function Articulos_Presupuesto(GrdArti As MSFlexGrid, CV As Byte, tipo As Byte, Optional Bolfactu As Boolean, Optional StDoc As String = NUL$, Optional LnTipoDoc As Long = 0) As Variant  'NMSR M3544 'HRR M5080
'Function Articulos_Presupuesto(GrdArti As MSFlexGrid, CV As Byte, tipo As Byte, Optional StDoc As String = NUL$, Optional LnTipoDoc As Long = 0) As Variant  'NMSR M3544 'HRR M5080
'INICIO  LDCR R5027/T6173
'Function Articulos_Presupuesto(GrdArti As MSFlexGrid, CV As Byte, tipo As Byte, Optional StDoc As String = NUL$, Optional LnTipoDoc As Long = 0, Optional DbRetIv As Double = 0) As Variant 'JACC M6427
Function Articulos_Presupuesto(GrdArti As MSFlexGrid, CV As Byte, tipo As Byte, Optional StDoc As String = NUL$, Optional LnTipoDoc As Long = 0, Optional DbRetIv As Double = 0, Optional StTercero As String) As Variant
'FIN  LDCR R5027/T6173
Dim arti(1)
Dim Ctas(1)
Dim I, J As Long
Dim Valor As Double 'smdl m1529
Dim BoBand As Boolean 'HRR M1818
Dim DbVReteIva As Double 'JACC M6527
Dim inValorIva As Double 'LDCR T6790
'INICIO LDCR  NPC T6790
Dim stTpoReg As String
Dim stOpcCxP As String
'FIN LDCR  NPC T6790
'INICIO  LDCR R5027/T6173
Dim stRegEmp As String
'JLPB T23820 INICIO SE DEJA EN COMENTARIO EL SIGUIENTE BLOQUE
'Dim BoAproxCent As Boolean  'OMOG T17510 VARIABLE QUE SIRVE PARA VISUALIZAR SI ESTA SELECCIONADA LA OPCION DE APROXIMAR
                            'CENTENAS EN IMPUESTOS Y DESCUENTOS EN CXP

''OMOG T17510 INICIO
'If boICxP Then
'   If ExisteCAMPO("PARAMETROS_CXP", "NU_REDIMDE_PCXP") Then
'      BoAproxCent = CBool(fnDevDato("PARAMETROS_CXP", "NU_REDIMDE_PCXP", NUL$))
'   End If
'End If
''OMOG T17510 FIN
'JLPB T23820 FIN

If StTercero <> NUL$ Then
    'stTpoReg = fnDevDato("PARAMETROS_IMPUESTOS", "ID_TIPO_REGI_PAIM", "CD_CODI_TERC_PAIM='" & Trim(stTercero) & Comi) 'LDCR R5027/T6173
    stTpoReg = fnDevDato("PARAMETROS_IMPUESTOS", "ID_TIPO_REGI_PAIM", "CD_CODI_TERC_PAIM='" & Trim(StTercero) & Comi, True) 'LDCR T6506
End If
'FIN  LDCR R5027/T6173

'INICIO LDCR  NPC T6790
   Condi = "CD_CODI_TERC_PAIM =(SELECT CD_NIT_ENTI  FROM ENTIDAD)"
   stRegEmp = fnDevDato("PARAMETROS_IMPUESTOS", "ID_TIPO_REGI_PAIM", Condi, True)
   
   If ExisteTABLA("PARAMETROS_CXP") Then
        If ExisteCAMPO("PARAMETROS_CXP", "TX_RIVARS_PCXP") Then stOpcCxP = fnDevDato("PARAMETROS_CXP", "TX_RIVARS_PCXP", NUL$, True)
   End If
'FIN LDCR  NPC T6790

ReDim PptoArti(3, 0)
    BoBand = True 'HRR M1818
    TotArtiPpto = 0
    With GrdArti
        For I = 1 To .Rows - 1 Step 1
           If Trim(.TextMatrix(I, 2)) <> NUL$ Then
               
               'HRR M1818
               'Determina los articulos que se van a procesar (cuando es una entrada dependiente de varias ordenes)
               If StDoc <> NUL$ Then
                  If .TextMatrix(I, 24) = StDoc Then
                     BoBand = True
                  Else
                     BoBand = False
                  End If
               End If
               
               If BoBand Then
               'HRR M1818
            
                     'HRR M5080
                     'Condicion = "CD_CODI_GRUP  = CD_GRUP_ARTI AND CD_CODI_ARTI =" & Comi & Trim(.TextMatrix(I, 2)) & Comi 'JACC M3206
'                     '****JACC M3206
'                      If Bolfactu = True And Trim(.TextMatrix(I, 0)) <> NUL$ Then
'                          Condicion = "CD_CODI_GRUP  = CD_GRUP_ARTI AND CD_CODI_ARTI =" & Comi & Trim(.TextMatrix(I, 0)) & Comi
'                      Else
'                          Condicion = "CD_CODI_GRUP  = CD_GRUP_ARTI AND CD_CODI_ARTI =" & Comi & Trim(.TextMatrix(I, 2)) & Comi
'                      End If
'                     '****JACC M3206
                     'HRR M5080
                      Condicion = "CD_CODI_GRUP  = CD_GRUP_ARTI AND CD_CODI_ARTI =" & Comi & Trim(.TextMatrix(I, 2)) & Comi 'HRR M5080
                      Desde = "ARTICULO, GRUP_ARTICULO"
                      If tipo = 0 Then
                          Campos = "CD_AGAS_GRUP, CD_CODI_GRUP"
                      Else
                          Campos = "CD_AING_GRUP, CD_CODI_GRUP"
                      End If
                      Result = LoadData(Desde, Campos, Condicion, arti())
                      If arti(0) = NUL$ Then
                         J = -1
                         Call Mensaje1("No se ha especificado art�culos presupuestales para el grupo:" & arti(1), 1)
                         ReDim PptoArti(3, 0)
                         Exit For
                      Else
                         J = FindInArrM(PptoArti(), CStr(arti(0)))
                         If tipo = 0 Then
                            'Result = LoadData("GASTOS", "DE_DESC_GAST", "CD_ARTI_CUEN ='" & arti(0) & Comi, Ctas())
                         Else
                            Result = LoadData("INGRESOS", "DE_DESC_INGR," & .TextMatrix(I, CV), "CD_CODI_INGR='" & arti(0) & Comi, Ctas())
                         End If
                      End If
                      If IsNumeric(.TextMatrix(I, CV)) Then
                        If CDbl(.TextMatrix(I, CV)) > 0 Then 'SMDL M1529 Cambio de tipo de Ccur a tipo Cdbl
                            
'                           valor = CDbl(.TextMatrix(I, CV)) 'SMDL M1529
                           'NMSR M3544 antes estaba calculando para la orden de compra el subtotal para crear el RP
                           'If ((LnTipoDoc = ODCompra) ) Then
                           
                           DbVReteIva = 0 'JACC M6527
                           If ((LnTipoDoc = ODCompra) Or (LnTipoDoc = Entrada)) Then 'DAHV M4460
                           If .TextMatrix(I, 10) <> NUL$ And .TextMatrix(I, 11) <> NUL$ Then
                                    If IsNumeric(.TextMatrix(I, 10)) And IsNumeric(.TextMatrix(I, 7)) And IsNumeric(.TextMatrix(I, 13)) And IsNumeric(.TextMatrix(I, 8)) And IsNumeric(.TextMatrix(I, 11)) Then
                                        If CLng(.TextMatrix(I, 8)) > 0 Then
'                                                valor = Aplicacion.Formatear_Cantidad(.TextMatrix(I, 10)) * _
'                                                                            Aplicacion.Formatear_Valor(.TextMatrix(I, 11)) * _
'                                                                            (Aplicacion.Formatear_Valor(.TextMatrix(I, 13)) / 100) 'HRR T15434
                                                Valor = Aplicacion.Formatear_Valor(Aplicacion.Formatear_Cantidad(.TextMatrix(I, 10)) * CDbl(.TextMatrix(I, 11)) * (CDbl(.TextMatrix(I, 13)) / 100)) 'HRR T15434
                                                inValorIva = Valor 'LDCR T6790
                                                'OMOG T17510 INICIO
                                                If BoAproxCent = True Then
                                                   Valor = AproxCentena(Round(Valor))
                                                End If
                                                'OMOG T17510 FIN
                                                Valor = Aplicacion.Formatear_Valor(Valor)
                                                DbVReteIva = ((Valor * DbRetIv) / 100) 'JACC M6527
                                                DbVReteIva = Aplicacion.Formatear_Valor(DbVReteIva) 'JACC M6527
                                                Valor = Valor + CLng(.TextMatrix(I, 10)) * CDbl(.TextMatrix(I, 11)) * CLng(.TextMatrix(I, 7)) / CLng(.TextMatrix(I, 8))
                                          End If
                                    End If
                                End If
                            Else
                                Valor = CDbl(.TextMatrix(I, CV))
                            End If 'NMSR M3544
                           If J = -1 Then
                              If PptoArti(0, 0) = NUL$ Then J = 0 Else J = UBound(PptoArti(), 2) + 1
                              If J >= 1 Then ReDim Preserve PptoArti(3, J)
                              PptoArti(0, J) = arti(0)
                              PptoArti(1, J) = Ctas(0)
                              PptoArti(2, J) = Ctas(1)
                              PptoArti(3, J) = Valor
                              TotArtiPpto = TotArtiPpto + Valor   'PJCA M1115
                              ''JACC M6427
                               'PptoArti(3, J) = CDbl(PptoArti(3, J)) + DbRetIv
                               'TotArtiPpto = TotArtiPpto + DbRetIv
                                 ''JACC M6527
                                 'INICIO  LDCR R5027/T6173 SE VALIDA EL TIPO DE DOCUMENTO   Y EL TIPO DE REGIMEN DEL TERCERO
                                 'PptoArti(3, J) = CDbl(PptoArti(3, J)) + DbVReteIva
                                 'TotArtiPpto = TotArtiPpto + DbVReteIva
                                 If LnTipoDoc = 3 Then
                                        'INICIO LDCR PNC T6790
                                        'If stTpoReg = "1" Then
                                        If stTpoReg = "1" And stRegEmp = "0" And stOpcCxP = "1" Then
                                        'FIN LDCR PNC T6790
                                             'INICIO LDCR T6790
'                                            PptoArti(3, J) = CDbl(PptoArti(3, J)) + DbVReteIva
'                                            TotArtiPpto = TotArtiPpto + DbVReteIva
                                             PptoArti(3, J) = Abs(CDbl(PptoArti(3, J)) + DbVReteIva - inValorIva)
                                             TotArtiPpto = Abs(TotArtiPpto + DbVReteIva - inValorIva)
                                             'FIN LDCR T6790
                                        Else
                                             PptoArti(3, J) = CDbl(PptoArti(3, J))
                                             TotArtiPpto = TotArtiPpto
                                        End If
                                 Else
                                        PptoArti(3, J) = CDbl(PptoArti(3, J))
                                        TotArtiPpto = TotArtiPpto
                                 End If
                                 'FIN LDCR R5027/T6173
                                 'JACC M6527
                              'JACC M6427
                           Else
                              'PptoArti(3, J) = CCur(PptoArti(3, J)) + valor
                              'TotArtiPpto = TotArtiPpto + valor   'PJCA M1115
                              'JACC M6527
                                 'INICIO  LDCR R5027/T6173
                                 'PptoArti(3, J) = CDbl(PptoArti(3, J)) + DbVReteIva
                                 'TotArtiPpto = TotArtiPpto + DbVReteIva
                                 If LnTipoDoc = 3 Then
                                        'INICIO LDCR PNC T6790
                                        'If stTpoReg = "1" Then
                                        If stTpoReg = "1" And stRegEmp = "0" And stOpcCxP = "1" Then
                                        'FIN LDCR PNC T6790
                                             'INICIO LDCR T6790
'                                            PptoArti(3, J) = CDbl(PptoArti(3, J)) + DbVReteIva
'                                            TotArtiPpto = TotArtiPpto + DbVReteIva
                                             'DRMG T42553 Inicio hace las suma de los articulos si el tercero es de tipo SIMPLIFICADO
                                             'PptoArti(3, J) = Abs(CDbl(PptoArti(3, J)) + DbVReteIva - inValorIva)
                                             PptoArti(3, J) = Abs(CDbl(PptoArti(3, J)) + Valor + DbVReteIva - inValorIva)
                                             TotArtiPpto = TotArtiPpto + Valor
                                             'DRMG T42553 Fin
                                             TotArtiPpto = Abs(TotArtiPpto + DbVReteIva - inValorIva)
                                             'FIN LDCR T6790
                                'DAHV T8004 - Se incluyen los procesos de sumatoria de los nuevos valores (variable valor) -  INICIO
                                        Else
                                             PptoArti(3, J) = CDbl(PptoArti(3, J)) + Valor
                                             TotArtiPpto = TotArtiPpto + Valor
                                        End If
                                 Else
                                        PptoArti(3, J) = CDbl(PptoArti(3, J)) + Valor
                                        TotArtiPpto = TotArtiPpto + Valor
                                 End If
                                 'DAHV T8004 - Se incluyen los procesos de sumatoria de los nuevos valores (variable valor) -  FIN
                                 'FIN LDCR R5027/T6173
                                 'JACC M6527
                           End If
                        End If
                      End If
                  End If 'HRR M1818
             
          End If
        Next
    End With
    
    Articulos_Presupuesto = PptoArti()
End Function

Function Leer_Ingresos(Articulo As String) As String
Dim Arr(0)
    Condicion = "CD_CODI_INGR ='" & Articulo & Comi
    Result = LoadData("INGRESOS", "DE_DESC_INGR", Condicion, Arr())
    Leer_Ingresos = Arr(0)
End Function
'VALIDA QUE NO EXISTAN NIVELES SUPERIORES; TRUE, FALSO
Function Valida_Nivel_Ingresos(Articulo As String) As Boolean
ReDim Arr(0, 0)
      
     Condicion = "CD_CODI_INGR LIKE '" & Cambiar_Comas_Comillas(Articulo) & CaractLike & Comi
     Result = LoadMulData("INGRESOS", "CD_CODI_INGR", Condicion, Arr())
     If Result <> FAIL Then
        If UBound(Arr(), 2) > 0 Then
           Valida_Nivel_Ingresos = True
        Else
           Valida_Nivel_Ingresos = False
        End If
     Else
        Valida_Nivel_Ingresos = True
     End If
End Function
Function Valida_Nivel_Gastos(Articulo As String) As Boolean
ReDim Arr(0, 0)
     Condicion = "CD_CODI_GAST LIKE '" & Cambiar_Comas_Comillas(Articulo) & CaractLike & Comi
     Result = LoadMulData("GASTOS", "CD_CODI_GAST", Condicion, Arr())
     If Result <> FAIL Then
        If UBound(Arr(), 2) > 0 Then
           Valida_Nivel_Gastos = True
        Else
           Valida_Nivel_Gastos = False
        End If
     Else
        Valida_Nivel_Gastos = True
     End If
End Function
Function Leer_Consecutivo_Ppto(campo As String) As Double
ReDim Arr(0) As Variant
    Result = LoadData("CONSECUTIVOS", campo, NUL$, Arr())
    If Arr(0) = NUL$ Then Leer_Consecutivo_Ppto = 0 Else Leer_Consecutivo_Ppto = CLng(Arr(0))
End Function
Sub Actualiza_Gastos(campo As String, Articulo As String, Valor As Double, Mes As Byte)
    Call Actualiza_Gastos_Mes(Articulo, campo, Valor, Mes)
    Call Actualiza_Gastos_Dep(Articulo, campo, Valor, Mes, Depesel)
End Sub
'Actualiza la Tabla de Gastos_Mes por niveles
Sub Actualiza_Gastos_Mes(ByVal Articulo As String, ByVal campo As String, Valor As Double, Mes As Byte)
Dim Tabla As String
Dim I As Byte
Dim VlrI As String
Dim VlrU As String
    
    Tabla = "GASTOS_MES (CD_ARTI_PERI, NU_MES_PERI, " & campo & ")"
    VlrU = campo & " = " & campo & " + " & Valor
    I = Nivel_Articulo(Articulo)
    Do While I > 0 And Articulo <> NUL$
       Select Case I
           Case 1: Articulo = Mid(Articulo, 1, 1)
           Case 2: Articulo = Mid(Articulo, 1, 2)
           Case 3: Articulo = Mid(Articulo, 1, 4)
           Case 4: Articulo = Mid(Articulo, 1, 6)
           Case 5: Articulo = Mid(Articulo, 1, 9)
           Case 6: Articulo = Mid(Articulo, 1, 12)
           Case 7: Articulo = Mid(Articulo, 1, 16)
           Case 8: Articulo = Mid(Articulo, 1, 20)
       End Select
       VlrI = Comi & Articulo & Comi & Coma & Mes & Coma & Valor
       Condicion = "CD_ARTI_PERI = '" & Articulo & Comi & " AND NU_MES_PERI =" & Mes
'       Result = Actualiza("GASTOS_MES", campo, Condicion, Tabla, VlrI, VlrU)
       Result = Actualiza("GASTOS_MES", Condicion, Tabla, VlrI, VlrU)       'DEPURACION DE CODIGO
       I = I - 1
       If Result = FAIL Then Exit Do
    Loop
End Sub
Sub Actualiza_Gastos_Dep(ByVal Articulo As String, ByVal campo As String, Valor As Double, Mes As Byte, Dependencia As String)
Dim Tabla As String
Dim I As Byte
Dim VlrI As String
Dim VlrU As String
    
    Tabla = "GTO_DEPENDENCIA (CD_DEPE_PERI, CD_ARTI_PERI, NU_MES_PERI, " & campo & ")"
    VlrU = campo & " = " & campo & " + " & Valor
    I = Nivel_Articulo(Articulo)
    Do While I > 0 And Articulo <> NUL$
       Select Case I
           Case 1: Articulo = Mid(Articulo, 1, 1)
           Case 2: Articulo = Mid(Articulo, 1, 2)
           Case 3: Articulo = Mid(Articulo, 1, 4)
           Case 4: Articulo = Mid(Articulo, 1, 6)
           Case 5: Articulo = Mid(Articulo, 1, 9)
           Case 6: Articulo = Mid(Articulo, 1, 12)
           Case 7: Articulo = Mid(Articulo, 1, 16)
           Case 8: Articulo = Mid(Articulo, 1, 20)
       End Select
       VlrI = Comi & Dependencia & Comi & Coma & Comi & Articulo & Comi & Coma & Mes & Coma & Valor
       Condicion = "CD_ARTI_PERI = '" & Articulo & Comi & " AND NU_MES_PERI =" & Mes & " AND CD_DEPE_PERI = '" & Dependencia & Comi
'       Result = Actualiza("GTO_DEPENDENCIA", campo, Condicion, Tabla, VlrI, VlrU)
       Result = Actualiza("GTO_DEPENDENCIA", Condicion, Tabla, VlrI, VlrU)      'DEPURACION DE CODIGO
       I = I - 1
       If Result = FAIL Then Exit Do
    Loop
End Sub

'ingresos
Sub Actualiza_Ingresos(TipoR As Byte, Articulo As String, Valor As Double, Mes As Byte)
Dim campo As String
    Select Case TipoR
        Case 0: campo = "VL_CAUS_PERI"
        Case 1: campo = "VL_EFEC_PERI"
        Case 2: campo = "VL_REPA_PERI"
        Case 3: campo = "VL_OTCA_PERI"
        Case 4: campo = "VL_RECO_PERI"
    End Select
    Call Actualiza_Ingresos_Mes(Articulo, campo, Valor, Mes)
    If Result <> FAIL Then Call Actualiza_Ingresos_Dep(Articulo, campo, Valor, Mes, Depesel)
End Sub

'Actualiza la Tabla de Ingresos_Mes por niveles
Sub Actualiza_Ingresos_Mes(ByVal Articulo As String, ByVal campo As String, Valor As Double, Mes As Byte)
Dim Tabla As String
Dim I As Byte
Dim VlrI As String
Dim VlrU As String
    
    Tabla = "INGRESOS_MES (CD_ARTI_PERI, NU_MES_PERI, " & campo & ")"
    VlrU = campo & " = " & campo & " + " & Valor
    I = Nivel_Articulo(Articulo)
    Do While I > 0 And Articulo <> NUL$
       Select Case I
           Case 1: Articulo = Mid(Articulo, 1, 1)
           Case 2: Articulo = Mid(Articulo, 1, 2)
           Case 3: Articulo = Mid(Articulo, 1, 4)
           Case 4: Articulo = Mid(Articulo, 1, 6)
           Case 5: Articulo = Mid(Articulo, 1, 9)
           Case 6: Articulo = Mid(Articulo, 1, 12)
           Case 7: Articulo = Mid(Articulo, 1, 16)
           Case 8: Articulo = Mid(Articulo, 1, 20)
       End Select
       VlrI = Comi & Articulo & Comi & Coma & Mes & Coma & Valor
       Condicion = "CD_ARTI_PERI = '" & Articulo & Comi & " AND NU_MES_PERI =" & Mes
'       Result = Actualiza("INGRESOS_MES", campo, Condicion, Tabla, VlrI, VlrU)
       Result = Actualiza("INGRESOS_MES", Condicion, Tabla, VlrI, VlrU)         'DEPURACION DE CODIGO
       I = I - 1
       If Result = FAIL Then Exit Do
    Loop
End Sub
Sub Actualiza_Ingresos_Dep(ByVal Articulo As String, ByVal campo As String, Valor As Double, Mes As Byte, Dependencia As String)
Dim Tabla As String
Dim I As Byte
Dim VlrI As String
Dim VlrU As String
    
    Tabla = "ING_DEPENDENCIA (CD_DEPE_PERI, CD_ARTI_PERI, NU_MES_PERI, " & campo & ")"
    VlrU = campo & " = " & campo & " + " & Valor
    I = Nivel_Articulo(Articulo)
    Do While I > 0 And Articulo <> NUL$
       Select Case I
           Case 1: Articulo = Mid(Articulo, 1, 1)
           Case 2: Articulo = Mid(Articulo, 1, 2)
           Case 3: Articulo = Mid(Articulo, 1, 4)
           Case 4: Articulo = Mid(Articulo, 1, 6)
           Case 5: Articulo = Mid(Articulo, 1, 9)
           Case 6: Articulo = Mid(Articulo, 1, 12)
           Case 7: Articulo = Mid(Articulo, 1, 16)
           Case 8: Articulo = Mid(Articulo, 1, 20)
       End Select
       VlrI = Comi & Dependencia & Comi & Coma & Comi & Articulo & Comi & Coma & Mes & Coma & Valor
       Condicion = "CD_ARTI_PERI = '" & Articulo & Comi & " AND NU_MES_PERI =" & Mes & " AND CD_DEPE_PERI = '" & Dependencia & Comi
'       Result = Actualiza("ING_DEPENDENCIA", campo, Condicion, Tabla, VlrI, VlrU)
       Result = Actualiza("ING_DEPENDENCIA", Condicion, Tabla, VlrI, VlrU)      'DEPURACION DE CODIGO
       If Result = FAIL Then Exit Do
       I = I - 1
    Loop
End Sub
'Function Actualiza(Tabla As String, ByVal campo As String, Condicion As String, TablaN As String, VlrI As String, VlrU As String) As Integer
Function Actualiza(Tabla As String, Condicion As String, TablaN As String, VlrI As String, VlrU As String) As Integer        'DEPURACION DE CODIGO
'Dim l As Byte      'DEPURACION DE CODIGO
Dim Result As Integer
Dim Tbl As ADODB.Recordset

    'Campo = "sum(" & Campo & ")"
    Set Tbl = New ADODB.Recordset
    Call SelectRST(Tabla, Asterisco, Condicion, Tbl)
    If (Tbl.EOF) Then
       Result = DoInsert(TablaN, VlrI)
    Else
       Debug.Print Tbl.Fields(0)
      ' If DataQuery(1) = NUL$ Then
     '     L = InStr(VlrU, "+")
     '     VlrU = Mid(VlrU, L + 1, Len(VlrU) - L)
     '     VlrU = Campo & "=" & VlrU
     '  End If
       Result = DoUpdate(Tabla, VlrU, Condicion)
    End If
    Actualiza = Result
End Function
Function Nivel_Articulo(Articulo As String) As Byte
    Select Case Len(Articulo)
        Case 1: Nivel_Articulo = 1
        Case 2: Nivel_Articulo = 2
        Case 3 To 4: Nivel_Articulo = 3
        Case 5 To 6: Nivel_Articulo = 4
        Case 7 To 9: Nivel_Articulo = 5
        Case 10 To 12: Nivel_Articulo = 6
        Case 13 To 16: Nivel_Articulo = 7
        Case 17 To 20: Nivel_Articulo = 8
    End Select
End Function
Function Leer_GiroIng(NumGiri As Double) As Variant
ReDim MatVals(2, 0)
ReDim Arr(1)
    Desde = "GIROING_ARTICULO, INGRESOS"
    Campos = "CD_ARTI_GIAR,DE_DESC_INGR,VL_GIRO_GIAR"
    Condicion = "CD_ARTI_GIAR = CD_CODI_INGR AND NU_GIRO_GIAR = " & NumGiri
    Result = LoadMulData(Desde, Campos, Condicion, MatVals())
    
    Result = LoadData("GIROING", "CD_DEPE_GIRI, CD_ORDE_GIRI", "NU_CONS_GIRI=" & NumGiri, Arr())
    Depesel = Arr(0): OrdeSel = Arr(1)
    Leer_GiroIng = MatVals()
End Function
Function Leer_Comprobante(campo As String) As String
Dim Arr(0)
    Result = LoadData("COMPROBANTES", campo, NUL$, Arr())
    Leer_Comprobante = Arr(0)
End Function
Function Leer_Cuenta_Ppto(Tabla As String, campo As String, Condicion As String) As String
Dim Arr(0)
    Result = LoadData(Tabla, campo, Condicion, Arr())
    Leer_Cuenta_Ppto = Arr(0)
End Function
'8 Giro ingresos
Sub Anulacion_Documentos_Ppto(TipoDoc As Integer, NumeroDoc As Double, ActConta As Boolean, FechaD As String)
   Dim comprobante As String
   Dim descripcion As String
   Dim NumDoc As Double
   comprobante = Leer_Comprobante("NU_COMP_ANUL")
   NumDoc = Leer_Consecutivo_Ppto("NU_CONS_ANUL") + 1
   Select Case TipoDoc
      Case 5: descripcion = "ANULACION REGISTRO DESDE INVENTARIOS"
      Case 6: descripcion = "ANULACION OBLIGACION DESDE INVENTARIOS"
      Case 16: descripcion = "ANULACION OBLIGACION RESERVA DESDE INVENTARIOS"
      Case 8: descripcion = "ANULACION GIRO DE INGRESOS DESDE INVENTARIOS"
   End Select
   'JAUM T33493-R32409 Inicio se deja bloque en comentario
   'Valores = "NU_CONS_DOAN=" & NumDoc & Coma & _
   "ID_TIPO_DOAN=" & IIf(TipoDoc = 38, 8, TipoDoc) & Coma & _
   "NU_DOCU_DOAN=" & NumeroDoc & Coma & _
   "FE_PERI_DOAN=" & Format(FechaD, "yyyy") & Coma & _
   "FE_FECH_DOAN=" & FFechaIns(FechaD) & Coma & _
   "DE_DESC_DOAN=" & Comi & descripcion & Comi & Coma & _
   "CD_ORDE_DOAN=" & OrdeSel
   'JAUM T33864 Inicio se deja bloque en comentario
   'Valores = "NU_CONS_DOAN = " & NumDoc & ", ID_TIPO_DOAN = " & IIf(TipoDoc = 38, 8, TipoDoc) & ", "
   'Valores = Valores & "NU_DOCU_DOAN = " & NumeroDoc & ", FE_PERI_DOAN = " & Format(FechaD, "yyyy") & ", "
   'Valores = Valores & "FE_FECH_DOAN = " & FFechaIns(FechaD) & ", DE_DESC_DOAN = '" & descripcion & "', "
   'Valores = Valores & "CD_ORDE_DOAN = '" & OrdeSel & "'"
   Valores = "NU_CONS_DOAN = " & NumDoc & Coma & " ID_TIPO_DOAN = " & IIf(TipoDoc = 38, 8, TipoDoc) & Coma
   Valores = Valores & "NU_DOCU_DOAN = " & NumeroDoc & Coma & " FE_PERI_DOAN = " & Format(FechaD, "yyyy") & Coma
   Valores = Valores & "FE_FECH_DOAN = " & FFechaIns(FechaD) & Coma & " DE_DESC_DOAN = " & Comi & descripcion & Comi & Coma
   Valores = Valores & "CD_ORDE_DOAN = " & Comi & OrdeSel & Comi
   'JAUM T33864 Fin
   'JAUM T33493-R32409 Fin
   Result = DoInsertSQL("DOC_ANULADOS", Valores)
   If Result <> FAIL Then Result = DoUpdate("CONSECUTIVOS", "NU_CONS_ANUL=NU_CONS_ANUL+1", NUL$)
   If Result <> FAIL Then
      Select Case TipoDoc
         'Case 5: Call Anula_Registro(NumeroDoc, ActConta, NumDoc, FechaD, 0)
         Case 5: Call Anula_Registro(NumeroDoc, ActConta, NumDoc, FechaD)       'DEPURACION DE CODIGO
         'Case 6: Call Anula_Obligacion(NumeroDoc, ActConta, NumDoc, FechaD, 0)
         Case 6: Call Anula_Obligacion(NumeroDoc, ActConta, NumDoc, FechaD)      'DEPURACION DE CODIGO
         'Case 16: Call Anula_Obligacion_Reserva(NumeroDoc, ActConta, NumDoc, FechaD, 0)
         Case 16: Call Anula_Obligacion_Reserva(NumeroDoc, ActConta, NumDoc, FechaD)     'DEPURACION DE CODIGO
         Case 8: Call Anula_Giro_Ingresos(NumeroDoc, ActConta, NumDoc, FechaD)
      End Select
   End If

End Sub
Private Sub Anula_Giro_Ingresos(Numero As Double, ActConta As Boolean, NumDocAn As Double, FechaD As String)
ReDim Datos(2, 0)
Dim ArrD(1)
ReDim Arr(1)
Dim campo As String
Dim CampoC As String
Dim CompPpto As String
Dim Imput As Integer
Dim Val As Double

    CompPpto = Leer_Comprobante("NU_COMP_ANUL")
    'AASV M4858 Nota:22212 Inicio
    Valores = "ID_ESTA_GIRI = '2', FE_ANUL_GIRI = " & FFechaIns(FechaD)
    Valores = Valores & Coma & "TX_USER_ANUL_GIRI = '" & NombreUsuario(UserId) & Comi
    'Result = DoUpdate("GIROING", "ID_ESTA_GIRI = '2', FE_ANUL_GIRI = " & FFechaIns(FechaD), "NU_CONS_GIRI = " & Numero)
    Result = DoUpdate("GIROING", Valores, "NU_CONS_GIRI = " & Numero)
    'AASV M4858 Nota:22212 Fin
    If Result <> FAIL Then
       Result = LoadData("GIROING", "CD_DEPE_GIRI, ID_TIPO_GIRI", "NU_CONS_GIRI=" & Numero, ArrD())
       Select Case CByte(ArrD(1))
            Case 0: CampoC = "NU_EFEC_CUEN": campo = "VL_CAAN_PERI"
            Case 1: CampoC = "NU_EFEC_CUEN": campo = "VL_EFAN_PERI"
            Case 2: CampoC = "NU_PAPE_CUEN": campo = "VL_RPAN_PERI"
            Case 3: CampoC = "NU_OTCA_CUEN": campo = "VL_OCAN_PERI"
            Case 4: CampoC = "NU_RECO_CUEN": campo = "VL_RCAN_PERI"
       End Select
       
       Campos = "CD_ARTI_GIAR, VL_GIRO_GIAR, ID_NOAF_GIAR"
       Result = LoadMulData("GIROING_ARTICULO", Campos, "NU_GIRO_GIAR = " & Numero, Datos())
       
       If Result <> FAIL Then
          Imput = 1
          For I = 0 To UBound(Datos(), 2)
              If Datos(1, I) = NUL$ Then Datos(1, I) = 0
              Val = CDbl(Datos(1, I))
              If Result <> FAIL Then Call Actualiza_Ingresos_Mes(CStr(Datos(0, I)), campo, Val, Format(Hoy(), "mm"))
              
              If Result <> FAIL Then Call Actualiza_Ingresos_Dep(CStr(Datos(0, I)), campo, Val, Format(Hoy(), "mm"), CStr(ArrD(0)))
              'INICIO LDCR R9384/T9676 SE COLOCA EN COMENTARIO POR REQ.
'              If CByte(ArrD(1)) > 0 And ActConta Then
'                Result = LoadData("CUENTAS_INGRESOS", "NU_EFEC_CUEN, NU_XEJE_CUEN", "CD_ARTI_CUEN =" & Comi & CStr(Datos(0, I)) & Comi, Arr())
'                If Arr(0) <> NUL$ And Arr(1) <> NUL$ Then
'                    If Result <> FAIL Then Call Actualiza_Contabilidad(CStr(Arr(0)), "D", CStr(ArrD(0)), NUL$, Val, NumDocAn, FechaD, CompPpto, "ANULACION GIRO INGRESOS No. " & Numero, Imput)
'                    If Result <> FAIL Then Imput = Imput + 1
'                    If Result <> FAIL Then Call Actualiza_Contabilidad(CStr(Arr(1)), "C", CStr(ArrD(0)), NUL$, Val, NumDocAn, FechaD, CompPpto, "ANULACION GIRO INGRESOS No. " & Numero, Imput)
'                    If Result <> FAIL Then Imput = Imput + 1
'                End If
'              End If
              'FIN LDCR R9384/T9676 SE COLOCA EN COMENTARIO POR REQ.
              If Result = FAIL Then Exit For
          Next I
       End If
    End If
End Sub
'Private Sub Anula_Registro(Numero As Double, ActConta As Boolean, NumDocAn As Double, FechaD As String, NumReg As Long)
Private Sub Anula_Registro(Numero As Double, ActConta As Boolean, NumDocAn As Double, FechaD As String)     'DEPURACION DE CODIGO
ReDim Datos(1, 0)
Dim Ctas(1)
'ReDim Arr(3) 'SKRV T17757 Se establece en comentario
Dim campo As String
Dim CampoC As String
Dim CompPpto As String
Dim Imput As Integer
Dim BoSaldar As Boolean 'SKRV T17757 identifica si se saldo parcialmente
Dim DoValSal As Double 'SKRV T17757 identifica el valor total saldado
   'SKRV T17757 inicio
   ReDim Arr(0)
   Campos = "FE_SALD_REGI"
   Result = LoadData("REGISTRO_PPTO", Campos, "NU_CONS_REGI = " & Numero, Arr())
   If Arr(0) = Null Or Arr(0) = "" Then
   ReDim Arr(3)
   'SKRV T17757 Fin
       CompPpto = Leer_Comprobante("NU_COMP_ANUL")
       'AASV M4858 Nota:22212 Inicio
       Valores = "ID_ESTA_REGI = '2', FE_ANUL_REGI = " & FFechaIns(FechaD)
       Valores = Valores & Coma & "TX_USER_ANUL_REGI = '" & NombreUsuario(UserId) & Comi
       'Result = DoUpdate("REGISTRO_PPTO", "ID_ESTA_REGI = '2', FE_ANUL_REGI = " & FFechaIns(FechaD), "NU_CONS_REGI = " & Numero)
       Result = DoUpdate("REGISTRO_PPTO", Valores, "NU_CONS_REGI = " & Numero)
       'AASV M4858 Nota:22212 Fin
       If Result <> FAIL Then
          Result = LoadData("REGISTRO_PPTO", "CD_DEPE_REGI, VL_VALO_REGI, NU_CERT_REGI, CD_TERC_REGI", "NU_CONS_REGI = " & Numero, Arr())
          If Arr(1) = NUL$ Then Arr(1) = 0
          If Arr(2) = NUL$ Then Arr(2) = 0
          If Result <> FAIL Then Result = DoUpdate("CERTIFICADO", "VL_COMP_CERT = VL_COMP_CERT -" & CDbl(Arr(1)), "NU_CONS_CERT = " & CLng(Arr(2)))
       End If
       If Result <> FAIL Then
          campo = "VL_REAN_PERI"
          
          Campos = "CD_ARTI_REAR, VL_REGI_REAR"
          Result = LoadMulData("REG_ARTICULO", Campos, "NU_REGI_REAR = " & Numero, Datos())
          
          If Result <> FAIL Then
             Imput = 1
             For I = 0 To UBound(Datos(), 2)
                 If Datos(1, I) = NUL$ Then Datos(1, I) = 0
                 
                 If Result <> FAIL Then Call Actualiza_Gastos_Mes(CStr(Datos(0, I)), campo, CDbl(Datos(1, I)), Format(Hoy(), "mm"))
                 If Result <> FAIL Then Call Actualiza_Gastos_Dep(CStr(Datos(0, I)), campo, CDbl(Datos(1, I)), Format(Hoy(), "mm"), CStr(Arr(0)))
                 If Result <> FAIL Then
                    Condicion = "NU_CERT_CEAR =" & CLng(Arr(2)) & " AND CD_ARTI_CEAR=" & Comi & CStr(Datos(0, I)) & Comi
                    Result = DoUpdate("CDP_ARTICULO", "VL_COMP_CEAR = VL_COMP_CEAR -" & CDbl(Datos(1, I)), Condicion)
                 End If
                 'INICIO LDCR R9384/T9676 SE COLOCA EN COMENTARIO POR REQ.
   '              If ActConta Then
   '                 'Result = LoadData("CUENTAS_GASTOS", "NU_CDPE_CUEN, NU_COMP_CUEN", "CD_ARTI_CUEN =" & Comi & CStr(Datos(0, I)) & Comi, Ctas())
   '                 Result = LoadData("CUENTAS_GASTOS", "NU_XEJE_CUEN, NU_COMP_CUEN", "CD_ARTI_CUEN =" & Comi & CStr(Datos(0, I)) & Comi, Ctas()) 'HRR R1678-1681
   '                 If Arr(0) <> NUL$ And Arr(1) <> NUL$ Then
   '                    If Result <> FAIL Then Call Actualiza_Contabilidad(CStr(Ctas(0)), "D", CStr(Arr(0)), CStr(Arr(3)), CDbl(Datos(1, I)), NumDocAn, FechaD, CompPpto, "ANULACION REGISTRO No. " & Numero, Imput)
   '                    If Result <> FAIL Then Imput = Imput + 1
   '                    If Result <> FAIL Then Call Actualiza_Contabilidad(CStr(Ctas(1)), "C", CStr(Arr(0)), CStr(Arr(3)), CDbl(Datos(1, I)), NumDocAn, FechaD, CompPpto, "ANULACION REGISTRO No. " & Numero, Imput)
   '                    If Result <> FAIL Then Imput = Imput + 1
   '                End If
   '              End If
                 'FIN  LDCR R9384/T9676 SE COLOCA EN COMENTARIO POR REQ.
                 If Result = FAIL Then Exit For
             Next I
          End If
       End If
   'SKRV T17757 inicio
   Else
      ReDim Datos(2, 0)
      Campos = "CD_ARTI_REAR, VL_REGI_REAR, VL_NOUS_REAR"
      Result = LoadMulData("REG_ARTICULO", Campos, "NU_REGI_REAR = " & Numero, Datos())
      If Result <> FAIL Then
         For I = 0 To UBound(Datos(), 2)
            If CDbl(Datos(1, I)) <> CDbl(Datos(2, I)) Then
               BoSaldar = True
            End If
            DoValSal = DoValSal + CDbl(Datos(2, I))
         Next I
      End If
      If BoSaldar = False Then
         Call Mensaje1("El registro pptal No. " & Numero & " se encuentra saldado, no se realizara movimiento en presupuesto.", 3)
         Exit Sub
      Else
         
         Call Mensaje1("El registro pptal No. " & Numero & " se encuentra saldado parcialmente, Con la anulacion se realizara el proceso de saldar por el valor total del registro pptal.", 3)
         
         If Result <> FAIL Then
            
            ReDim Arr(3)
            Result = LoadData("REGISTRO_PPTO", "CD_DEPE_REGI, VL_VALO_REGI, NU_CERT_REGI, CD_TERC_REGI", "NU_CONS_REGI = " & Numero, Arr())
            
            If Arr(1) = NUL$ Then
               Arr(1) = 0
            Else
               'le resta el valor saldado al valor el registro
               Arr(1) = CDbl(Arr(1)) - CDbl(DoValSal)
            End If
            
            If Arr(2) = NUL$ Then Arr(2) = 0
            If Result <> FAIL Then Result = DoUpdate("CERTIFICADO", "VL_COMP_CERT = VL_COMP_CERT -" & CDbl(Arr(1)), "NU_CONS_CERT = " & CLng(Arr(2)))
            'actualiza el valor saldado
            Valores = " VL_NOUS_REGI = " & CDbl(Arr(1)) + CDbl(DoValSal)
            Result = DoUpdate("REGISTRO_PPTO", Valores, "NU_CONS_REGI = " & Numero)
         
         End If
         
         campo = "VL_REAN_PERI"
         
         Imput = 1
         For I = 0 To UBound(Datos(), 2)
            If Datos(1, I) = NUL$ Then Datos(1, I) = 0
            
            If Result <> FAIL Then Call Actualiza_Gastos_Mes(CStr(Datos(0, I)), campo, CDbl(Arr(1)) + CDbl(DoValSal), Format(Hoy(), "mm"))
            If Result <> FAIL Then Call Actualiza_Gastos_Dep(CStr(Datos(0, I)), campo, CDbl(Arr(1)) + CDbl(DoValSal), Format(Hoy(), "mm"), CStr(Arr(0)))
            If Result <> FAIL Then
               Condicion = "NU_CERT_CEAR =" & CLng(Arr(2)) & " AND CD_ARTI_CEAR=" & Comi & CStr(Datos(0, I)) & Comi
               Result = DoUpdate("CDP_ARTICULO", "VL_COMP_CEAR = VL_COMP_CEAR -" & CDbl(Arr(1)), Condicion)
            End If
            
            'actualizo saldado_ppto
            Valores = Numero & Coma
            Valores = Valores & CDate(Hoy()) & Coma
            Valores = Valores & CDbl(Arr(1)) + CDbl(DoValSal) & Coma
            Valores = Valores & CDbl(Arr(1)) & Coma
            Valores = Valores & 2 & Coma
            Valores = Valores & Comi & Arr(0) & Comi & Coma
            Valores = Valores & Comi & Datos(0, I) & Comi
            
            Result = DoInsert("SALDADO_PPTO", Valores)
            If (Result <> FAIL) Then Result = Auditor("SALDADO_PPTO", TranIns, LastCmd)
            
            If Result = FAIL Then Exit For
         Next I
         
      End If
   End If
   'SKRV T17757 fin
End Sub
'Private Sub Anula_Obligacion(Numero As Double, ActConta As Boolean, NumDocAn As Double, FechaD As String, NumReg As Long)
Private Sub Anula_Obligacion(Numero As Double, ActConta As Boolean, NumDocAn As Double, FechaD As String)       'DEPURACION DE CODIGO
ReDim Datos(1, 0)
Dim Ctas(1)
ReDim Arr(3)
Dim campo As String
Dim CampoC As String
Dim CompPpto As String
Dim Imput As Integer

    CompPpto = Leer_Comprobante("NU_COMP_ANUL")
    'AASV M4858 Nota:22212 Inicio
    Valores = "ID_ESTA_OBLI = '2', FE_ANUL_OBLI = " & FFechaIns(FechaD)
    Valores = Valores & Coma & "TX_USER_ANUL_OBLI = '" & NombreUsuario(UserId) & Comi
    'Result = DoUpdate("OBLIGACION", "ID_ESTA_OBLI = '2', FE_ANUL_OBLI = " & FFechaIns(FechaD), "NU_CONS_OBLI = " & Numero)
    Result = DoUpdate("OBLIGACION", Valores, "NU_CONS_OBLI = " & Numero)
    'AASV M4858 Nota:22212 Fin
    If Result <> FAIL Then
       Result = LoadData("OBLIGACION", "CD_DEPE_OBLI, VL_VALO_OBLI, NU_REGI_OBLI, CD_TERC_OBLI", "NU_CONS_OBLI = " & Numero, Arr())
       If Arr(1) = NUL$ Then Arr(1) = 0
       If Arr(2) = NUL$ Then Arr(2) = 0
       If Result <> FAIL Then Result = DoUpdate("REGISTRO_PPTO", "VL_COMP_REGI = VL_COMP_REGI -" & CDbl(Arr(1)), "NU_CONS_REGI = " & CLng(Arr(2)))
    End If
    If Result <> FAIL Then
       campo = "VL_OBAN_PERI"
       
       Campos = "CD_ARTI_OBAR, VL_OBLI_OBAR"
       Result = LoadMulData("OBL_ARTICULO", Campos, "NU_OBLI_OBAR = " & Numero, Datos())
       
       If Result <> FAIL Then
          Imput = 1
          For I = 0 To UBound(Datos(), 2)
              If Datos(1, I) = NUL$ Then Datos(1, I) = 0
              
              If Result <> FAIL Then Call Actualiza_Gastos_Mes(CStr(Datos(0, I)), campo, CDbl(Datos(1, I)), Format(Hoy(), "mm"))
              If Result <> FAIL Then Call Actualiza_Gastos_Dep(CStr(Datos(0, I)), campo, CDbl(Datos(1, I)), Format(Hoy(), "mm"), CStr(Arr(0)))
              If Result <> FAIL Then
                 Condicion = "NU_REGI_REAR =" & CLng(Arr(2)) & " AND CD_ARTI_REAR=" & Comi & CStr(Datos(0, I)) & Comi
                 Result = DoUpdate("REG_ARTICULO", "VL_COMP_REAR = VL_COMP_REAR -" & CDbl(Datos(1, I)), Condicion)
              End If
              'INICIO LDCR R9384/T9676 SE COLOCA EN COMENTARIO POR REQ.
'              If ActConta Then
'                 Result = LoadData("CUENTAS_GASTOS", "NU_COMP_CUEN, NU_OBLI_CUEN", "CD_ARTI_CUEN =" & Comi & CStr(Datos(0, I)) & Comi, Ctas())
'                 If Arr(0) <> NUL$ And Arr(1) <> NUL$ Then
'                    If Result <> FAIL Then Call Actualiza_Contabilidad(CStr(Ctas(0)), "D", CStr(Arr(0)), CStr(Arr(3)), CDbl(Datos(1, I)), NumDocAn, FechaD, CompPpto, "ANULACION OBLIGACION No. " & Numero, Imput)
'                    If Result <> FAIL Then Imput = Imput + 1
'                    If Result <> FAIL Then Call Actualiza_Contabilidad(CStr(Ctas(1)), "C", CStr(Arr(0)), CStr(Arr(3)), CDbl(Datos(1, I)), NumDocAn, FechaD, CompPpto, "ANULACION OBLIGACION No. " & Numero, Imput)
'                    If Result <> FAIL Then Imput = Imput + 1
'                End If
'              End If
              'FIN LDCR R9384/T9676 SE COLOCA EN COMENTARIO POR REQ.
              If Result = FAIL Then Exit For
          Next I
       End If
    End If
End Sub
'Private Sub Anula_Obligacion_Reserva(Numero As Double, ActConta As Boolean, NumDocAn As Double, FechaD As String, NumReg As Long)
Private Sub Anula_Obligacion_Reserva(Numero As Double, ActConta As Boolean, NumDocAn As Double, FechaD As String)   'DEPURACION DE CODIGO
ReDim Datos(1, 0)
Dim Ctas(1)
ReDim Arr(3)
Dim campo As String
Dim CampoC As String
Dim CompPpto As String
Dim Imput As Integer

    CompPpto = Leer_Comprobante("NU_COMP_ANUL")
    'AASV M4858 Nota:22212 Inicio
    Valores = "ID_ESTA_OBRE = '2', FE_ANUL_OBRE = " & FFechaIns(FechaD)
    Valores = Valores & Coma & "TX_USER_ANUL_OBRE = '" & NombreUsuario(UserId) & Comi
    'Result = DoUpdate("OBL_RESERVA", "ID_ESTA_OBRE = '2', FE_ANUL_OBRE = " & FFechaIns(FechaD), "NU_CONS_OBRE = " & Numero)
    Result = DoUpdate("OBL_RESERVA", Valores, "NU_CONS_OBRE = " & Numero)
    'AASV M4858 Nota:22212 Fin
    If Result <> FAIL Then
       Result = LoadData("OBL_RESERVA", "CD_DEPE_OBRE, VL_VALO_OBRE, NU_RESE_OBRE, CD_TERC_OBRE", "NU_CONS_OBRE = " & Numero, Arr())
       If Arr(1) = NUL$ Then Arr(1) = 0
       If Arr(2) = NUL$ Then Arr(2) = 0
       If Result <> FAIL Then Result = DoUpdate("RESERVAS", "VL_COMP_RESE = VL_COMP_RESE -" & CDbl(Arr(1)), "NU_CONS_RESE = " & CLng(Arr(2)))
    End If
    If Result <> FAIL Then
       campo = "VL_ORAN_PERI"
       
       Campos = "CD_ARTI_ORAR, VL_OBRE_ORAR"
       Result = LoadMulData("OBRE_ARTICULO", Campos, "NU_OBRE_ORAR = " & Numero, Datos())
       
       If Result <> FAIL Then
          Imput = 1
          For I = 0 To UBound(Datos(), 2)
              If Datos(1, I) = NUL$ Then Datos(1, I) = 0
              
              If Result <> FAIL Then Call Actualiza_Gastos_Mes(CStr(Datos(0, I)), campo, CDbl(Datos(1, I)), Format(Hoy(), "mm"))
              If Result <> FAIL Then Call Actualiza_Gastos_Dep(CStr(Datos(0, I)), campo, CDbl(Datos(1, I)), Format(Hoy(), "mm"), CStr(Arr(0)))
              If Result <> FAIL Then
                 Condicion = "NU_RESE_RSAR =" & CLng(Arr(2)) & " AND CD_ARTI_RSAR=" & Comi & CStr(Datos(0, I)) & Comi
                 Result = DoUpdate("RES_ARTICULO", "VL_COMP_RSAR = VL_COMP_RSAR -" & CDbl(Datos(1, I)), Condicion)
              End If
              'INICIO LDCR R9384/T9676 SE COLOCA EN COMENTARIO POR REQ.
'              If ActConta Then
'                 Result = LoadData("CUENTAS_GASTOS", "NU_RPXE_CUEN, NU_RPOC_CUEN", "CD_ARTI_CUEN =" & Comi & CStr(Datos(0, I)) & Comi, Ctas())
'                 If Arr(0) <> NUL$ And Arr(1) <> NUL$ Then
'                    If Result <> FAIL Then Call Actualiza_Contabilidad(CStr(Ctas(0)), "D", CStr(Arr(0)), CStr(Arr(3)), CDbl(Datos(1, I)), NumDocAn, FechaD, CompPpto, "ANULACION OBLIGACION RESERVA No. " & Numero, Imput)
'                    If Result <> FAIL Then Imput = Imput + 1
'                    If Result <> FAIL Then Call Actualiza_Contabilidad(CStr(Ctas(1)), "C", CStr(Arr(0)), CStr(Arr(3)), CDbl(Datos(1, I)), NumDocAn, FechaD, CompPpto, "ANULACION OBLIGACION RESERVA No. " & Numero, Imput)
'                    If Result <> FAIL Then Imput = Imput + 1
'                End If
'              End If
              'FIN LDCR R9384/T9676 SE COLOCA EN COMENTARIO POR REQ.
              If Result = FAIL Then Exit For
          Next I
       End If
    End If
End Sub
'INICIO LDCR R9384/T9676 SE COLOCA EN COMENTARIO POR REQ.
'Public Sub Actualiza_Contabilidad(cuenta As String, Naturaleza As String, Dependencia As String, Tercero As String, valor As Double, NumDocAn As Double, FechaD As String, Comprobante As String, Documento As String, Imput As Integer)
'Dim Datos(13)
'Dim Ok As Boolean
'    If cuenta = NUL$ Then Exit Sub
'    Datos(0) = cuenta
'    Datos(1) = Tercero
'    Datos(2) = FechaD
'    Datos(3) = Comprobante
'    Datos(4) = NumDocAn
'    Datos(5) = Imput
'    Datos(6) = Documento
'    Datos(7) = valor
'    Datos(8) = Naturaleza
'    Datos(9) = Dependencia
'    Datos(10) = NUL$
'    Datos(11) = SCero
'    Datos(12) = Format(FechaD, "mm")
'    'Datos(13) = "P" 'Indica el modulo que graba el movimiento
'    Datos(13) = "R" 'Indica el modulo que graba el movimiento 'NMSR R1934
'    Ok = Crear_Movimiento_Contable(Datos())
'End Sub
'FIN LDCR R9384/T9676 SE COLOCA EN COMENTARIO POR REQ.
