VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Object = "{C932BA88-4374-101B-A56C-00AA003668DC}#1.1#0"; "msmask32.ocx"
Object = "{0BA686C6-F7D3-101A-993E-0000C0EF6F5E}#1.0#0"; "THREED32.OCX"
Begin VB.Form frmPrmREPORTES 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Parametros para el KARDEX"
   ClientHeight    =   7095
   ClientLeft      =   45
   ClientTop       =   435
   ClientWidth     =   11475
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   7095
   ScaleWidth      =   11475
   Begin VB.Frame fraRANGO 
      Height          =   1095
      Left            =   6360
      TabIndex        =   5
      Top             =   4800
      Width           =   4575
      Begin VB.CheckBox chkRANGO 
         Caption         =   "Condicionar las fechas del reporte"
         Height          =   315
         Left            =   360
         TabIndex        =   10
         Top             =   200
         Width           =   4095
      End
      Begin MSMask.MaskEdBox txtDESDE 
         Height          =   315
         Left            =   960
         TabIndex        =   6
         Top             =   600
         Width           =   1215
         _ExtentX        =   2143
         _ExtentY        =   556
         _Version        =   393216
         PromptChar      =   "_"
      End
      Begin MSMask.MaskEdBox txtHASTA 
         Height          =   315
         Left            =   3240
         TabIndex        =   7
         Top             =   600
         Width           =   1215
         _ExtentX        =   2143
         _ExtentY        =   556
         _Version        =   393216
         PromptChar      =   "_"
      End
      Begin VB.Label lblDESDE 
         Alignment       =   1  'Right Justify
         Caption         =   "Desde:"
         Height          =   255
         Left            =   120
         TabIndex        =   9
         Top             =   600
         Width           =   735
      End
      Begin VB.Label Label1 
         Alignment       =   1  'Right Justify
         Caption         =   "Hasta:"
         Height          =   255
         Left            =   2400
         TabIndex        =   8
         Top             =   600
         Width           =   735
      End
   End
   Begin MSComctlLib.ProgressBar pgbLLEVO 
      Height          =   255
      Left            =   6480
      TabIndex        =   3
      Top             =   240
      Visible         =   0   'False
      Width           =   4455
      _ExtentX        =   7858
      _ExtentY        =   450
      _Version        =   393216
      Appearance      =   1
   End
   Begin VB.ComboBox cboTIPO 
      Height          =   315
      Left            =   3240
      TabIndex        =   0
      Text            =   "Combo1"
      Top             =   240
      Width           =   2655
   End
   Begin MSComctlLib.ImageList imgBotones 
      Left            =   10920
      Top             =   3960
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   20
      ImageHeight     =   14
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   7
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmPrmREPORTES.frx":0000
            Key             =   ""
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmPrmREPORTES.frx":013A
            Key             =   "ASG"
         EndProperty
         BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmPrmREPORTES.frx":0454
            Key             =   "CAN"
         EndProperty
         BeginProperty ListImage4 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmPrmREPORTES.frx":076E
            Key             =   "SAV"
         EndProperty
         BeginProperty ListImage5 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmPrmREPORTES.frx":0A88
            Key             =   "ADD"
         EndProperty
         BeginProperty ListImage6 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmPrmREPORTES.frx":0DA2
            Key             =   "DEL"
         EndProperty
         BeginProperty ListImage7 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmPrmREPORTES.frx":10BC
            Key             =   "CHA"
         EndProperty
      EndProperty
   End
   Begin MSComctlLib.ListView lstBODEGAS 
      Height          =   2055
      Left            =   480
      TabIndex        =   2
      Top             =   4800
      Width           =   5655
      _ExtentX        =   9975
      _ExtentY        =   3625
      View            =   3
      LabelEdit       =   1
      MultiSelect     =   -1  'True
      LabelWrap       =   -1  'True
      HideSelection   =   -1  'True
      FullRowSelect   =   -1  'True
      GridLines       =   -1  'True
      _Version        =   393217
      ForeColor       =   -2147483640
      BackColor       =   -2147483643
      BorderStyle     =   1
      Appearance      =   1
      NumItems        =   0
   End
   Begin MSComctlLib.ListView lstARTICULOS 
      Height          =   3735
      Left            =   480
      TabIndex        =   1
      Top             =   720
      Width           =   10500
      _ExtentX        =   18521
      _ExtentY        =   6588
      View            =   3
      LabelEdit       =   1
      MultiSelect     =   -1  'True
      LabelWrap       =   -1  'True
      HideSelection   =   0   'False
      AllowReorder    =   -1  'True
      Checkboxes      =   -1  'True
      FullRowSelect   =   -1  'True
      GridLines       =   -1  'True
      _Version        =   393217
      ForeColor       =   -2147483640
      BackColor       =   -2147483643
      BorderStyle     =   1
      Appearance      =   1
      NumItems        =   2
      BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Text            =   "Nombre del Art�culo"
         Object.Width           =   7761
      EndProperty
      BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   1
         Text            =   "C�digo"
         Object.Width           =   3881
      EndProperty
   End
   Begin Threed.SSCommand cmdIMPRIMIR 
      Height          =   735
      Index           =   2
      Left            =   10080
      TabIndex        =   11
      Top             =   6120
      Width           =   855
      _Version        =   65536
      _ExtentX        =   1508
      _ExtentY        =   1296
      _StockProps     =   78
      Caption         =   "&Imprimir"
      RoundedCorners  =   0   'False
      Picture         =   "frmPrmREPORTES.frx":13D6
   End
   Begin VB.Label txtTIPO 
      Alignment       =   1  'Right Justify
      Caption         =   "Tipo de Art�culos:"
      Height          =   315
      Left            =   600
      TabIndex        =   4
      Top             =   240
      Width           =   2415
   End
End
Attribute VB_Name = "frmPrmREPORTES"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Dim ArrUXB() As Variant
Dim Articulo As UnArticulo
Dim LaBodega As String, LaAccion As String
Dim CnTdr As Integer, NumEnCombo As Integer
Public OpcCod        As String   'Opci�n de seguridad
'NU_AUTO_ARTI, CD_CODI_ARTI, NO_NOMB_ARTI, NU_AUTO_UNVE_ARTI, TX_VENCE_ARTI, TX_PARA_ARTI,
'TX_ENTRA_ARTI, CD_GRUP_ARTI, CD_USOS_ARTI, VL_ULCO_ARTI, VL_COPR_ARTI, CT_EXIS_ARTI, DE_OBSE_ARTI, ID_TIPO_ARTIID_TIPO_ARTI, CD_RIPS_ARTI, NU_INDPYP_ARTI

Private Sub cboTIPO_GotFocus()
    NumEnCombo = Me.cboTIPO.ListIndex
End Sub

Private Sub chkRANGO_Click()
    Me.txtDESDE.Enabled = False
    Me.txtHASTA.Enabled = False
    If Not CBool(Me.chkRANGO.Value) = True Then Exit Sub
    Me.txtDESDE.Enabled = True
    Me.txtHASTA.Enabled = True
End Sub

Private Sub cmdIMPRIMIR_Click(Index As Integer)
    Dim Renglon As MSComctlLib.ListItem
    Dim strARTICULOS As String, strBODEGAS As String
    For Each Renglon In Me.lstARTICULOS.ListItems
        If Renglon.Selected Then strARTICULOS = strARTICULOS & IIf(Len(strARTICULOS) > 0, " OR ", "") & "{ARTICULO.CD_CODI_ARTI}='" & Renglon.ListSubItems("COAR").Text & "'"
    Next
    For Each Renglon In Me.lstBODEGAS.ListItems
        If Renglon.Selected Then strBODEGAS = strBODEGAS & IIf(Len(strBODEGAS) > 0, " OR ", "") & "{IN_BODEGA.TX_CODI_BODE}='" & Renglon.ListSubItems("COD").Text & "'"
    Next
    If Len(strARTICULOS) > 0 Then strARTICULOS = "(" & strARTICULOS & ")"
    If Len(strBODEGAS) > 0 Then strBODEGAS = "(" & strBODEGAS & ")"
    If Len(strARTICULOS) > 0 And Len(strBODEGAS) > 0 Then
        strARTICULOS = strARTICULOS & " AND " & strBODEGAS
    Else
        strARTICULOS = strARTICULOS & strBODEGAS
    End If
    If CBool(Me.chkRANGO.Value) Then strARTICULOS = strARTICULOS & IIf(Len(strARTICULOS) > 0, " AND ", "") & _
        "({IN_KARDEX.FE_FECH_KARD} in CDate('" & Me.txtDESDE.Text & "') to CDate('" & Me.txtHASTA.Text & "'))"
    Debug.Print strARTICULOS
    Call ListarD("infKARDEX.rpt", strARTICULOS, crptToWindow, "KARDEX", MDI_Inventarios)
End Sub

Private Sub Form_Load()
'    Call Main
    Call CenterForm(MDI_Inventarios, Me)
    Me.cboTIPO.Clear
    Me.cboTIPO.AddItem "Para la Venta"
    Me.cboTIPO.AddItem "Para El Consumo"
    NumEnCombo = Me.cboTIPO.ListIndex
    If Len(FormatDateTime(Date, vbShortDate)) = 8 Then
        Me.txtDESDE.Mask = "##/##/##"
        Me.txtHASTA.Mask = "##/##/##"
    Else
        Me.txtDESDE.Mask = "##/##/####"
        Me.txtHASTA.Mask = "##/##/####"
    End If
    Me.txtDESDE.Text = FormatDateTime(DateAdd("d", -(Day(Date) - 1), Date), vbShortDate)
    Me.txtHASTA.Text = FormatDateTime(DateAdd("d", -1, DateAdd("M", 1, DateAdd("d", -(Day(Date) - 1), Date))), vbShortDate)
    Me.chkRANGO.Value = False
    Me.txtDESDE.Enabled = False
    Me.txtHASTA.Enabled = False
'NU_AUTO_BODE, NU_AUTO_BODE_BODE, TX_CODI_BODE, TX_NOMB_BODE, CD_CODI_CECO_BODE, TX_VENTA_BODE
'FROM IN_BODEGA;
    Me.lstBODEGAS.ListItems.Clear
    Me.lstBODEGAS.CheckBoxes = False
    Me.lstBODEGAS.MultiSelect = True
    Me.lstBODEGAS.HideSelection = False
    Me.lstBODEGAS.ColumnHeaders.Clear
    Me.lstBODEGAS.ColumnHeaders.Add , "NOM", "Noombre de la Bodega", 0.66 * Me.lstBODEGAS.Width
    Me.lstBODEGAS.ColumnHeaders.Add , "COD", "C�digo", 0.34 * Me.lstBODEGAS.Width
    
    Me.lstARTICULOS.ListItems.Clear
    Me.lstARTICULOS.CheckBoxes = False
    Me.lstARTICULOS.MultiSelect = True
    Me.lstARTICULOS.HideSelection = False
    Me.lstARTICULOS.Width = 10500
    Me.lstARTICULOS.ColumnHeaders.Clear
'lstArticulos, NOAR, COAR, NOCO, NOUS, NOGR, AUCO, COGR, COUS
    Me.lstARTICULOS.ColumnHeaders.Add , "NOAR", "Nombre del art�culo", 0.4 * Me.lstARTICULOS.Width
    Me.lstARTICULOS.ColumnHeaders.Add , "COAR", "C�digo", 0.15 * Me.lstARTICULOS.Width
    Me.lstARTICULOS.ColumnHeaders.Add , "NOCO", "Unidad de conteo", 0.15 * Me.lstARTICULOS.Width
    Me.lstARTICULOS.ColumnHeaders.Add , "NOUS", "Uso", 0.15 * Me.lstARTICULOS.Width
    Me.lstARTICULOS.ColumnHeaders.Add , "NOGR", "Grupo", 0.15 * Me.lstARTICULOS.Width
    Me.lstARTICULOS.ColumnHeaders.Add , "AUCO", "AutoUNC", 0
    Me.lstARTICULOS.ColumnHeaders.Add , "COGR", "CodGRU", 0
    Me.lstARTICULOS.ColumnHeaders.Add , "COUS", "CodUSO", 0
FALLO:
End Sub

Private Sub cboTIPO_LostFocus()
    If NumEnCombo = Me.cboTIPO.ListIndex Then Exit Sub
    Dim tipo As String * 1
    Me.lstBODEGAS.ListItems.Clear
    Me.lstARTICULOS.ListItems.Clear
    Me.pgbLLEVO.Value = 0
    Me.pgbLLEVO.Visible = True
    Select Case Me.cboTIPO.ListIndex
    Case Is = -1
        tipo = "0"
    Case Is = 0
        tipo = "V"
    Case Is = 1
        tipo = "C"
    End Select
    Campos = "NU_AUTO_BODE, NU_AUTO_BODE_BODE, TX_CODI_BODE, TX_NOMB_BODE, " & _
        "CD_CODI_CECO_BODE, TX_VENTA_BODE"
    Desde = "IN_BODEGA"
    Condi = "TX_VENTA_BODE='" & tipo & "' ORDER BY TX_NOMB_BODE"
    ReDim ArrUXB(5, 0)
    Result = LoadMulData(Desde, Campos, Condi, ArrUXB())
    If Result = FAIL Then GoTo FALLO
    For CnTdr = 0 To UBound(ArrUXB, 2)
        Me.lstBODEGAS.ListItems.Add , "B" & ArrUXB(2, CnTdr), ArrUXB(3, CnTdr)
        Me.lstBODEGAS.ListItems("B" & ArrUXB(2, CnTdr)).ListSubItems.Add , "COD", ArrUXB(2, CnTdr)
    Next
    
    Campos = "NU_AUTO_ARTI, CD_CODI_ARTI, NO_NOMB_ARTI, NU_AUTO_UNVE_ARTI, TX_PARA_ARTI, " & _
        "DE_DESC_GRUP, DE_DESC_USOS, TX_NOMB_UNVE, CD_GRUP_ARTI, CD_USOS_ARTI, " & _
        "NU_MULT_UNVE, NU_DIVI_UNVE"
    
    Desde = "ARTICULO, USOS, GRUP_ARTICULO, IN_UNDVENTA"
    Condi = "CD_CODI_GRUP = CD_GRUP_ARTI AND CD_CODI_USOS = CD_USOS_ARTI"
    Condi = Condi & " AND NU_AUTO_UNVE = NU_AUTO_UNVE_ARTI"
    Condi = Condi & " AND TX_PARA_ARTI='" & tipo & "'"
    ReDim ArrUXB(11, 0)
    Result = LoadMulData(Desde, Campos, Condi, ArrUXB())
    If Result = FAIL Then GoTo FALLO
    If Not Encontro Then GoTo NOENC
    Me.pgbLLEVO.Min = 0
    Me.pgbLLEVO.Max = UBound(ArrUXB, 2)
    Dim Articulo As New UnArticulo
    For CnTdr = 0 To UBound(ArrUXB, 2)
        Me.pgbLLEVO.Value = CnTdr
        Articulo.AutoNumero = ArrUXB(0, CnTdr)
        Articulo.Codigo = ArrUXB(1, CnTdr)
        Articulo.Nombre = ArrUXB(2, CnTdr)
        Articulo.AutoUDConteo = ArrUXB(3, CnTdr)
        Articulo.NombreGrupo = ArrUXB(5, CnTdr)
        Articulo.NombreUso = ArrUXB(6, CnTdr)
        Articulo.NombreUndConteo = ArrUXB(7, CnTdr)
        Articulo.ParaQueEs = InStr(1, "VC", ArrUXB(4, CnTdr)) - 1
        Articulo.CodigoGrupo = ArrUXB(8, CnTdr)
        Articulo.CodigoUso = ArrUXB(9, CnTdr)
        Me.lstARTICULOS.ListItems.Add , "A" & Articulo.AutoNumero, Trim(Articulo.Nombre)
        Me.lstARTICULOS.ListItems("A" & Articulo.AutoNumero).Tag = CStr(Articulo.AutoNumero)
'lstArticulos, NOAR, COAR, NOCO, NOUS, NOGR, AUCO, COGR, COUS, MUCO, DICO
        Me.lstARTICULOS.ListItems("A" & Articulo.AutoNumero).ListSubItems.Add , "COAR", Trim(Articulo.Codigo)
        Me.lstARTICULOS.ListItems("A" & Articulo.AutoNumero).ListSubItems.Add , "NOCO", Trim(Articulo.NombreUndConteo)
        Me.lstARTICULOS.ListItems("A" & Articulo.AutoNumero).ListSubItems.Add , "NOUS", Trim(Articulo.NombreUso)
        Me.lstARTICULOS.ListItems("A" & Articulo.AutoNumero).ListSubItems.Add , "NOGR", Trim(Articulo.NombreGrupo)
        Me.lstARTICULOS.ListItems("A" & Articulo.AutoNumero).ListSubItems.Add , "AUCO", Trim(Articulo.AutoUDConteo)
        Me.lstARTICULOS.ListItems("A" & Articulo.AutoNumero).ListSubItems.Add , "COGR", Trim(Articulo.CodigoGrupo)
        Me.lstARTICULOS.ListItems("A" & Articulo.AutoNumero).ListSubItems.Add , "COUS", Trim(Articulo.CodigoUso)
    Next
    Set Articulo = Nothing
NOENC:
FALLO:
    Me.pgbLLEVO.Visible = False
End Sub

Private Sub lstArticulos_ColumnClick(ByVal ColumnHeader As MSComctlLib.ColumnHeader)
    Me.lstARTICULOS.SortKey = ColumnHeader.Index - 1
    Me.lstARTICULOS.Sorted = True
End Sub

Private Sub txtIDEN_KeyPress(Tecla As Integer)
    Call txtNOMB_KeyPress(Tecla)
End Sub

Private Sub txtNOMB_KeyPress(Tecla As Integer)
    Dim Cara As String * 1
    Cara = Chr(Tecla)
    If Cara = vbBack Then Exit Sub
    If Asc(Cara) > 96 And Asc(Tecla) < 113 Then Tecla = Asc(UCase(Chr(Tecla)))
    If InStr(" 01234567890ABCDEFGHIJKLMNOPQRSTUVWXYZ", Chr(Tecla)) = 0 Then Tecla = 0
End Sub

Private Sub TxtDesde_LostFocus()
    If IsDate(Me.txtDESDE.Text) Then Exit Sub
    Me.txtDESDE.Text = FormatDateTime(DateAdd("d", -(Day(Date) - 1), Date), vbShortDate)
    Me.txtDESDE.SetFocus
End Sub

Private Sub TxtHasta_LostFocus()
    If IsDate(Me.txtHASTA.Text) Then Exit Sub
    Me.txtHASTA.Text = FormatDateTime(DateAdd("d", -1, DateAdd("M", 1, DateAdd("d", -(Day(Date) - 1), Date))), vbShortDate)
    Me.txtHASTA.SetFocus
End Sub
