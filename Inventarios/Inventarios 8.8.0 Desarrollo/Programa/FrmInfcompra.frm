VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Object = "{C932BA88-4374-101B-A56C-00AA003668DC}#1.1#0"; "MSMASK32.OCX"
Object = "{0BA686C6-F7D3-101A-993E-0000C0EF6F5E}#1.0#0"; "THREED32.OCX"
Begin VB.Form FrmInfcompra 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Informe de Compra"
   ClientHeight    =   4575
   ClientLeft      =   45
   ClientTop       =   435
   ClientWidth     =   7485
   Icon            =   "FrmInfcompra.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   4575
   ScaleWidth      =   7485
   Begin VB.Frame FrmTipoRpt 
      Caption         =   "Tipo de informe"
      Height          =   1215
      Left            =   4800
      TabIndex        =   8
      Top             =   3240
      Width           =   1455
      Begin VB.OptionButton OptTipoRPT 
         Caption         =   "Detallado"
         Height          =   375
         Index           =   1
         Left            =   120
         TabIndex        =   10
         Top             =   720
         Width           =   1215
      End
      Begin VB.OptionButton OptTipoRPT 
         Caption         =   "Resumido"
         Height          =   255
         Index           =   0
         Left            =   120
         TabIndex        =   9
         Top             =   360
         Value           =   -1  'True
         Width           =   1215
      End
   End
   Begin VB.CheckBox ChkTercero 
      Caption         =   "Todos los Terceros"
      Height          =   255
      Left            =   240
      TabIndex        =   0
      Top             =   120
      Width           =   3015
   End
   Begin VB.Frame FrmFechas 
      Height          =   1215
      Left            =   120
      TabIndex        =   2
      Top             =   3240
      Width           =   4575
      Begin VB.CheckBox ChkFechas 
         Caption         =   "Condicionar las fechas del reporte"
         Height          =   315
         Left            =   120
         TabIndex        =   3
         Top             =   240
         Width           =   4095
      End
      Begin MSMask.MaskEdBox MskFechFin 
         Height          =   315
         Left            =   3120
         TabIndex        =   7
         Top             =   600
         Width           =   1215
         _ExtentX        =   2143
         _ExtentY        =   556
         _Version        =   393216
         Enabled         =   0   'False
         MaxLength       =   10
         Format          =   "dd/mm/yyyy"
         Mask            =   "##/##/####"
         PromptChar      =   "_"
      End
      Begin MSMask.MaskEdBox MskFechIni 
         Height          =   315
         Left            =   1080
         TabIndex        =   5
         Top             =   600
         Width           =   1215
         _ExtentX        =   2143
         _ExtentY        =   556
         _Version        =   393216
         Enabled         =   0   'False
         MaxLength       =   10
         Format          =   "dd/mm/yyyy"
         Mask            =   "##/##/####"
         PromptChar      =   "_"
      End
      Begin VB.Label LblHasta 
         Alignment       =   1  'Right Justify
         Caption         =   "Hasta:"
         Height          =   255
         Left            =   2400
         TabIndex        =   6
         Top             =   600
         Width           =   615
      End
      Begin VB.Label LblDesde 
         Alignment       =   1  'Right Justify
         Caption         =   "Desde:"
         Height          =   255
         Left            =   360
         TabIndex        =   4
         Top             =   600
         Visible         =   0   'False
         Width           =   615
      End
   End
   Begin MSComctlLib.ListView LstVTercero 
      Height          =   2655
      Left            =   120
      TabIndex        =   1
      Top             =   480
      Width           =   7215
      _ExtentX        =   12726
      _ExtentY        =   4683
      View            =   3
      LabelEdit       =   1
      MultiSelect     =   -1  'True
      LabelWrap       =   -1  'True
      HideSelection   =   0   'False
      AllowReorder    =   -1  'True
      FullRowSelect   =   -1  'True
      GridLines       =   -1  'True
      _Version        =   393217
      ForeColor       =   -2147483640
      BackColor       =   -2147483643
      BorderStyle     =   1
      Appearance      =   1
      NumItems        =   2
      BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Text            =   "Nombre del Art�culo"
         Object.Width           =   7761
      EndProperty
      BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   1
         Text            =   "C�digo"
         Object.Width           =   3881
      EndProperty
   End
   Begin Threed.SSCommand SCmdOpcion 
      Height          =   735
      Left            =   6480
      TabIndex        =   11
      Top             =   3480
      Width           =   855
      _Version        =   65536
      _ExtentX        =   1508
      _ExtentY        =   1296
      _StockProps     =   78
      Caption         =   "&IMPRIMIR"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   6
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      RoundedCorners  =   0   'False
      Picture         =   "FrmInfcompra.frx":058A
   End
End
Attribute VB_Name = "FrmInfcompra"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'---------------------------------------------------------------------------------------
' Module    : FrmInfcompra
' DateTime  : 04/03/2008 07:56
' Author    : hector_rodriguez
' Purpose   : genera un informe detallado y resumido de compras de inventario
'Vr. 6.1.0
'---------------------------------------------------------------------------------------

Option Explicit

Private Sub ChkFechas_Click()
   Call Un_Enable_Fechas
End Sub

Private Sub ChkFechas_KeyPress(KeyAscii As Integer)
   Call Cambiar_Enter(KeyAscii)
   If KeyAscii = 32 Then Call Un_Enable_Fechas
End Sub

Private Sub ChkTercero_KeyPress(KeyAscii As Integer)
   Call Cambiar_Enter(KeyAscii)
End Sub

Private Sub Form_Activate()
   Me.Caption = "Informe de compras"
End Sub

Private Sub Form_Load()
   
   Dim ArTerc() As Variant
   Dim InI As Integer
   
   Call CenterForm(MDI_Inventarios, Me)
   Call Leer_Permiso(Me.Name, SCmdOpcion, "I")
   Me.LstVTercero.ListItems.Clear
   Me.LstVTercero.Checkboxes = False
   Me.LstVTercero.MultiSelect = True
   Me.LstVTercero.HideSelection = False
   Me.LstVTercero.ColumnHeaders.Clear
   Me.LstVTercero.ColumnHeaders.Add , "COTE", "NIT", 0.3 * Me.LstVTercero.Width
   Me.LstVTercero.ColumnHeaders.Add , "NOTE", "Nombre", 0.6 * Me.LstVTercero.Width
   
   ReDim ArTerc(1, 0)
   'JLPB T22948 INICIO EL SIGUIENTE BLOQUE SE DEJA EN COMENTARIO
'   Campos = "DISTINCT CD_CODI_TERC, NO_NOMB_TERC"
'   Desde = "TERCERO, IN_ENCABEZADO, IN_DOCUMENTO"
'   Condicion = "NU_AUTO_DOCU_ENCA = NU_AUTO_DOCU AND CD_CODI_TERC_ENCA = CD_CODI_TERC"
'   Condicion = Condicion & " AND NU_AUTO_DOCU=2 ORDER BY NO_NOMB_TERC"
   Campos = "DISTINCT CD_CODI_TERC,NO_NOMB_TERC"
   Desde = "IN_R_COMP_ENCA INNER JOIN IN_ENCABEZADO ON NU_AUTO_ENCA_COEN=NU_AUTO_ENCA INNER JOIN IN_DOCUMENTO ON NU_AUTO_DOCU_ENCA=NU_AUTO_DOCU INNER JOIN TERCERO ON CD_CODI_TERC_ENCA=CD_CODI_TERC AND NU_AUTO_DOCU= 3 ORDER BY NO_NOMB_TERC"
   Condicion = NUL$
   'JLPB T22948 FIN
   Result = LoadMulData(Desde, Campos, Condicion, ArTerc)
   
   For InI = 0 To UBound(ArTerc, 2)
       Me.LstVTercero.ListItems.Add , "T" & Trim(ArTerc(0, InI)), Trim(ArTerc(0, InI))
       Me.LstVTercero.ListItems("T" & Trim(ArTerc(0, InI))).ListSubItems.Add , "NAME", Trim(ArTerc(1, InI))
   Next

   MskFechIni.Text = Format(Date, "dd/mm/yyyy")
   MskFechFin.Text = MskFechIni.Text
   
End Sub

'HRR M3021
Private Sub LstVTercero_ColumnClick(ByVal ColumnHeader As MSComctlLib.ColumnHeader)
   LstVTercero.SortKey = ColumnHeader.Index - 1
   LstVTercero.Sorted = True
End Sub
'HRR M3021

Private Sub LstVTercero_KeyPress(KeyAscii As Integer)
   Call Cambiar_Enter(KeyAscii)
End Sub

Private Sub MskFechFin_KeyPress(KeyAscii As Integer)
   Call Cambiar_Enter(KeyAscii)
End Sub

Private Sub MskFechIni_KeyPress(KeyAscii As Integer)
   Call Cambiar_Enter(KeyAscii)
End Sub

Private Sub OptTipoRPT_KeyPress(Index As Integer, KeyAscii As Integer)
   Call Cambiar_Enter(KeyAscii)
End Sub

'---------------------------------------------------------------------------------------
' Procedure : Un_Enable_Fechas
' DateTime  : 04/03/2008 07:56
' Author    : hector_rodriguez
' Purpose   : habilita o inhabilita digitar un rango de fechas
'---------------------------------------------------------------------------------------
'
Private Sub Un_Enable_Fechas()
   
   If ChkFechas.Value = Unchecked Then
      MskFechIni.Enabled = False
      MskFechFin.Enabled = False
   Else
      MskFechIni.Enabled = True
      MskFechFin.Enabled = True
   End If
   
End Sub

Private Sub MskFechFin_LostFocus()
    If ChkFechas.Value = Checked Then
      If ValFecha(MskFechFin, 1) <> FAIL Then
         If DateDiff("d", CDate(MskFechFin.Text), CDate(MskFechIni.Text)) > 0 Then
            Call Mensaje1("La fecha hasta no puede ser menor a la fecha desde", 3)
            MskFechFin.Text = MskFechIni.Text
         End If
      End If
   End If
End Sub

Private Sub MskFechIni_LostFocus()
   If ChkFechas.Value = Checked Then
      If ValFecha(MskFechIni, 1) <> FAIL Then
         If DateDiff("d", CDate(MskFechFin.Text), CDate(MskFechIni.Text)) > 0 Then
            Call Mensaje1("La fecha desde no puede ser mayor a la fecha hasta", 3)
            MskFechIni.Text = MskFechFin.Text
         End If
      End If
   End If
End Sub

'---------------------------------------------------------------------------------------
' Procedure : imprimir
' DateTime  : 28/02/2008 14:53
' Author    : hector_rodriguez
' Purpose   : visualiza el reporte enviando como filtro los terceros seleccionados
'---------------------------------------------------------------------------------------
'
Private Sub imprimir()
      
   Dim InI As Integer
   Dim StTercero As String
   Dim StFechaIni As String
   Dim StFechaFin As String
   Dim StRangoFecha As String
       
    On Error GoTo ERR
    
    Call Limpiar_CrysListar 'HRR M3001
   
   If ChkTercero.Value = Unchecked Then
      For InI = 1 To LstVTercero.ListItems.Count
         If LstVTercero.ListItems(InI).Selected Then
            If StTercero <> NUL$ Then StTercero = StTercero & Coma
            StTercero = StTercero & Chr(34) & LstVTercero.ListItems(InI).Text & Chr(34)
         End If
      Next
      
   End If
   
   If ChkFechas.Value = Unchecked Then
      StFechaIni = Format("01/01/1900 00:00:00", "yyyy,mm,dd,hh,mm,ss")
      StFechaFin = Format(Now, "yyyy,mm,dd,hh,mm,ss")
      StRangoFecha = "Del  01/01/1900 al " & Format(Now, "dd/mm/yyyy")
   Else
      StFechaIni = Format(CDate(MskFechIni.Text), "yyyy,mm,dd,hh,mm,ss")
      StFechaFin = Format(CDate(MskFechFin.Text), "yyyy,mm,dd,hh,mm,ss")
      StRangoFecha = "Del  " & Format(MskFechIni, "dd/mm/yyyy") & " al " & Format(MskFechFin, "dd/mm/yyyy")
   End If
   
   With MDI_Inventarios.Crys_Listar

      .Formulas(0) = "ENTIDAD= " & Comi & Entidad & Comi
      .Formulas(1) = "NIT= 'Nit. " & Nit & Comi
      .Formulas(2) = "RangoFecha='" & StRangoFecha & Comi
      If OptTipoRPT(0).Value = True Then
         .ReportFileName = App.Path & "\InfCompraInvResu.rpt"
         .WindowTitle = "Informe resumido de compras"
      Else
         .ReportFileName = App.Path & "\InfCompraInvDeta.rpt"
         .WindowTitle = "Informe detallado de compras"
      End If
      If StTercero <> NUL$ Then
         .SelectionFormula = NUL$
         'HRR M3038
         '.SelectionFormula = "{IN_COMPRA.CD_CODI_TERC_COMP} In [ " & StTercero & " ] AND {IN_COMPRA.FE_FECH_COMP} In DateTime(" & StFechaIni & ") To DateTime(" & StFechaFin & ") AND {IN_ENCABEZADO.NU_AUTO_DOCU_ENCA}=3" _
         '  & " AND {IN_DETALLE.NU_AUTO_MODCABE_DETA}={IN_DETALLE.NU_AUTO_ORGCABE_DETA}"
         'HRR M3038
         'HRR M3038
         .SelectionFormula = "{IN_COMPRA.CD_CODI_TERC_COMP} In [ " & StTercero & " ] AND {IN_COMPRA.FE_FACT_COMP} In DateTime(" & StFechaIni & ") To DateTime(" & StFechaFin & ") AND {IN_ENCABEZADO.NU_AUTO_DOCU_ENCA}=3" _
           & " AND {IN_DETALLE.NU_AUTO_MODCABE_DETA}={IN_DETALLE.NU_AUTO_ORGCABE_DETA}"
           'HRR M3038
      Else
      
         .SelectionFormula = NUL$
         'HRR M3038
         '.SelectionFormula = "{IN_COMPRA.FE_FECH_COMP} In DateTime(" & StFechaIni & ") To DateTime(" & StFechaFin & ") AND {IN_ENCABEZADO.NU_AUTO_DOCU_ENCA}=3" _
         '& " AND {IN_DETALLE.NU_AUTO_MODCABE_DETA}={IN_DETALLE.NU_AUTO_ORGCABE_DETA}"
         'HRR M3038
         'HRR M3038
         .SelectionFormula = "{IN_COMPRA.FE_FACT_COMP} In DateTime(" & StFechaIni & ") To DateTime(" & StFechaFin & ") AND {IN_ENCABEZADO.NU_AUTO_DOCU_ENCA}=3" _
         & " AND {IN_DETALLE.NU_AUTO_MODCABE_DETA}={IN_DETALLE.NU_AUTO_ORGCABE_DETA}"
         'HRR M3038
      End If
      
      .Destination = crptToWindow
      
      .PassWord = Chr(10) & UserPwdBD
      DoEvents
      .Action = 1
      
      .Formulas(0) = NUL$
      .Formulas(1) = NUL$
      .Formulas(2) = NUL$
      
   End With
   Exit Sub

ERR:

   Call ConvertErr
   
End Sub

Private Sub SCmdOpcion_Click()
   Call imprimir
End Sub
