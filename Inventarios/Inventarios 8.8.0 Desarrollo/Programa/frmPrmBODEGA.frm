VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Object = "{C932BA88-4374-101B-A56C-00AA003668DC}#1.1#0"; "MSMASK32.OCX"
Object = "{0BA686C6-F7D3-101A-993E-0000C0EF6F5E}#1.0#0"; "THREED32.OCX"
Begin VB.Form frmPrmBODEGA 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Consecutivos"
   ClientHeight    =   5175
   ClientLeft      =   45
   ClientTop       =   435
   ClientWidth     =   10725
   Icon            =   "frmPrmBODEGA.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   5175
   ScaleWidth      =   10725
   Begin VB.ComboBox cboQUEAlma 
      Height          =   315
      Left            =   7800
      TabIndex        =   4
      Top             =   2880
      Width           =   1695
   End
   Begin MSComctlLib.Toolbar tlbACCIONES 
      Align           =   1  'Align Top
      Height          =   660
      Left            =   0
      TabIndex        =   1
      Top             =   0
      Visible         =   0   'False
      Width           =   10725
      _ExtentX        =   18918
      _ExtentY        =   1164
      ButtonWidth     =   1799
      ButtonHeight    =   1005
      Appearance      =   1
      _Version        =   393216
      BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
         NumButtons      =   5
         BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Enabled         =   0   'False
            Caption         =   "&Adicionar"
            Key             =   "ADD"
            Object.Tag             =   "Adicionar"
         EndProperty
         BeginProperty Button2 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Enabled         =   0   'False
            Caption         =   "&Borrar"
            Key             =   "DEL"
            Object.Tag             =   "Borrar"
         EndProperty
         BeginProperty Button3 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Enabled         =   0   'False
            Caption         =   "&Modificar"
            Key             =   "CHA"
            Object.Tag             =   "Modificar"
         EndProperty
         BeginProperty Button4 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Enabled         =   0   'False
            Caption         =   "&Cancelar"
            Key             =   "CAN"
            Object.Tag             =   "Cancelar"
         EndProperty
         BeginProperty Button5 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "Actua"
            Key             =   "ACTU"
         EndProperty
      EndProperty
      BorderStyle     =   1
   End
   Begin MSComctlLib.TreeView arbBodegas 
      Height          =   3615
      Left            =   480
      TabIndex        =   0
      Top             =   960
      Width           =   4455
      _ExtentX        =   7858
      _ExtentY        =   6376
      _Version        =   393217
      Indentation     =   529
      LabelEdit       =   1
      LineStyle       =   1
      Style           =   7
      Appearance      =   1
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin VB.Frame Frame1 
      Caption         =   "Informaci�n:"
      Height          =   3135
      Left            =   5160
      TabIndex        =   10
      Top             =   840
      Width           =   5415
      Begin VB.CheckBox chkNoCont 
         Caption         =   "&No afecta contabilidad"
         Height          =   255
         Left            =   720
         TabIndex        =   5
         Top             =   2640
         Width           =   2895
      End
      Begin MSMask.MaskEdBox txtIDEN 
         Height          =   375
         Left            =   2640
         TabIndex        =   2
         Top             =   360
         Width           =   1695
         _ExtentX        =   2990
         _ExtentY        =   661
         _Version        =   393216
         PromptInclude   =   0   'False
         MaxLength       =   10
         PromptChar      =   "_"
      End
      Begin MSMask.MaskEdBox txtNOMB 
         Height          =   855
         Left            =   1440
         TabIndex        =   3
         Top             =   960
         Width           =   3615
         _ExtentX        =   6376
         _ExtentY        =   1508
         _Version        =   393216
         PromptInclude   =   0   'False
         MaxLength       =   50
         PromptChar      =   "_"
      End
      Begin VB.Label Label2 
         Alignment       =   1  'Right Justify
         AutoSize        =   -1  'True
         Caption         =   "Nombre:"
         Height          =   195
         Left            =   735
         TabIndex        =   13
         Top             =   960
         Width           =   600
      End
      Begin VB.Label Label1 
         Alignment       =   1  'Right Justify
         Caption         =   "Codigo:"
         Height          =   375
         Left            =   1080
         TabIndex        =   12
         Top             =   360
         Width           =   1335
      End
      Begin VB.Label Label3 
         Alignment       =   1  'Right Justify
         Caption         =   "Almacena para Venta:"
         Height          =   375
         Left            =   240
         TabIndex        =   11
         Top             =   2040
         Width           =   2175
      End
   End
   Begin Threed.SSCommand btnCan 
      Height          =   735
      Left            =   9120
      TabIndex        =   9
      Top             =   4200
      Width           =   855
      _Version        =   65536
      _ExtentX        =   1508
      _ExtentY        =   1296
      _StockProps     =   78
      Caption         =   "&CANCELAR"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   6
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Enabled         =   0   'False
      Picture         =   "frmPrmBODEGA.frx":058A
   End
   Begin Threed.SSCommand btnAdd 
      Height          =   735
      Left            =   5520
      TabIndex        =   6
      Top             =   4200
      Width           =   855
      _Version        =   65536
      _ExtentX        =   1508
      _ExtentY        =   1296
      _StockProps     =   78
      Caption         =   "&ADICIONAR"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   6
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Enabled         =   0   'False
      Picture         =   "frmPrmBODEGA.frx":0C54
   End
   Begin Threed.SSCommand btnDel 
      Height          =   735
      Left            =   6720
      TabIndex        =   7
      Top             =   4200
      Width           =   855
      _Version        =   65536
      _ExtentX        =   1508
      _ExtentY        =   1296
      _StockProps     =   78
      Caption         =   "&BORRAR"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   6
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Enabled         =   0   'False
      Picture         =   "frmPrmBODEGA.frx":18A6
   End
   Begin Threed.SSCommand btnCha 
      Height          =   735
      Left            =   7920
      TabIndex        =   8
      Top             =   4200
      Width           =   855
      _Version        =   65536
      _ExtentX        =   1508
      _ExtentY        =   1296
      _StockProps     =   78
      Caption         =   "&MODIFICAR"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   6
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Enabled         =   0   'False
      Picture         =   "frmPrmBODEGA.frx":1F70
   End
End
Attribute VB_Name = "frmPrmBODEGA"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Dim ArrBDG() As Variant
Dim ClldeBodegas As New Collection
Dim UnaBodega As LaBodega
Public OpcCod        As String   'Opci�n de seguridad

Private Sub arbBodegas_Collapse(ByVal Punto As MSComctlLib.Node)
    If Punto.Key = "BDGS" Then
        Me.txtIDEN.Text = ""
        Me.txtNOMB.Text = ""
        Me.chkNoCont.Value = 0
        Me.cboQUEAlma.ListIndex = -1
        Me.tlbACCIONES.Buttons("DEL").Enabled = False
        Me.tlbACCIONES.Buttons("CHA").Enabled = False
        Me.tlbACCIONES.Buttons("CAN").Enabled = False
        Me.btnDel.Enabled = False
        Me.btnCha.Enabled = False
        Me.btnCan.Enabled = False
        Me.txtIDEN.Enabled = False
        Me.txtNOMB.Enabled = False
        Me.chkNoCont.Enabled = False
        Me.cboQUEAlma.Enabled = False
    End If
End Sub

Private Sub ARBBodegas_NodeClick(ByVal Punto As MSComctlLib.Node)
'    Dim NuMr As Integer, unNodo As Node
    Dim unNodo As Node      'DEPURACION DE CODIGO
    For Each unNodo In Me.arbBodegas.Nodes
        If unNodo.Bold Then unNodo.Bold = False
    Next
    Me.arbBodegas.SelectedItem.Bold = True
    Me.tlbACCIONES.Buttons("DEL").Enabled = False
    Me.tlbACCIONES.Buttons("CHA").Enabled = False
    Me.tlbACCIONES.Buttons("CAN").Enabled = False
    Me.btnDel.Enabled = False
    Me.btnCha.Enabled = False
    Me.btnCan.Enabled = False
    If Punto.Key = "BDGS" Then
        Me.txtIDEN.Text = ""
        Me.txtNOMB.Text = ""
        Me.chkNoCont.Value = 0
'        Me.txtIDEN.Text = Left(Trim(UnaBodega.Codigo) & String(Me.txtIDEN.MaxLength, " "), Me.txtIDEN.MaxLength)
'        Me.txtNOMB.Text = Left(Trim(UnaBodega.Nombre) & String(Me.txtNOMB.MaxLength, " "), Me.txtNOMB.MaxLength)
        Me.cboQUEAlma.ListIndex = -1
'        If Not Me.tlbACCIONES.Buttons("ADD").Enabled Then Me.tlbACCIONES.Buttons("ADD").Enabled = True
        Me.tlbACCIONES.Buttons("DEL").Enabled = False
        Me.tlbACCIONES.Buttons("CHA").Enabled = False
        Me.tlbACCIONES.Buttons("CAN").Enabled = False
        Me.btnDel.Enabled = False
        Me.btnCha.Enabled = False
        Me.btnCan.Enabled = False
        If Me.txtIDEN.Enabled Then Me.txtIDEN.Enabled = False
        If Me.txtNOMB.Enabled Then Me.txtNOMB.Enabled = False
        Me.chkNoCont.Enabled = False
        If Me.cboQUEAlma.Enabled Then Me.cboQUEAlma.Enabled = False
        Exit Sub
    End If
'    If Me.tlbACCIONES.Buttons("ADD").Enabled Then Me.tlbACCIONES.Buttons("ADD").Enabled = False
    Me.tlbACCIONES.Buttons("DEL").Enabled = True
    Me.tlbACCIONES.Buttons("CHA").Enabled = True
    Me.btnDel.Enabled = True
    Me.btnCha.Enabled = True
    Me.txtIDEN.Enabled = True
    Me.cboQUEAlma.Enabled = True
    Me.txtIDEN.Enabled = False
    Me.txtNOMB.Enabled = False
    Me.cboQUEAlma.Enabled = False
    Set UnaBodega = ClldeBodegas.Item(Punto.Key)
    Me.txtIDEN.Text = Trim(UnaBodega.Codigo)
    Me.txtNOMB.Text = Trim(UnaBodega.Nombre)
    Me.chkNoCont.Value = Abs(UnaBodega.AfectaCont = "N") '|.DR.|, R:1633
'    Me.txtIDEN.Text = Left(Trim(UnaBodega.Codigo) & String(Me.txtIDEN.MaxLength, " "), Me.txtIDEN.MaxLength)
'    Me.txtNOMB.Text = Left(Trim(UnaBodega.Nombre) & String(Me.txtNOMB.MaxLength, " "), Me.txtNOMB.MaxLength)
    Me.cboQUEAlma.ListIndex = IIf(UnaBodega.AlmacenaPara = "V", 0, 1)
End Sub

Private Sub btnAdd_Click()
    Dim UnaBodega As New LaBodega, Aviso As String
'    Dim ArrCSC() As Variant        'DEPURACION DE CODIGO
    Me.arbBodegas.Enabled = False
    If Me.tlbACCIONES.Buttons("ADD").Caption = "&Adicionar" Then
        Me.tlbACCIONES.Buttons("DEL").Enabled = False
        Me.tlbACCIONES.Buttons("CHA").Enabled = False
        Me.btnDel.Enabled = False
        Me.btnCha.Enabled = False
        Me.txtIDEN.Text = Left(Trim(UnaBodega.Codigo) & String(Me.txtIDEN.MaxLength, " "), Me.txtIDEN.MaxLength)
        Me.txtNOMB.Text = Left(Trim(UnaBodega.Nombre) & String(Me.txtNOMB.MaxLength, " "), Me.txtNOMB.MaxLength)
        Me.cboQUEAlma.ListIndex = -1
        Me.cboQUEAlma.Text = "Seleccione una opci�n"
        Me.txtIDEN.Enabled = True
        Me.txtNOMB.Enabled = True
        Me.chkNoCont.Enabled = True
        Me.chkNoCont.Value = 0
        Me.cboQUEAlma.Enabled = True
        Me.tlbACCIONES.Buttons("ADD").Caption = "&Guardar"
        Me.btnAdd.Caption = "&Guardar"
        Me.tlbACCIONES.Buttons("CAN").Enabled = True
        Me.btnCan.Enabled = True
        Me.txtIDEN.SetFocus
    Else
        Me.txtIDEN.Enabled = False
        Me.txtNOMB.Enabled = False
        Me.cboQUEAlma.Enabled = False
        Me.tlbACCIONES.Buttons("ADD").Caption = "&Adicionar"
        Me.btnAdd.Caption = "&Adicionar"
        Me.tlbACCIONES.Buttons("DEL").Enabled = True
        Me.tlbACCIONES.Buttons("CAN").Enabled = False
        Me.btnDel.Enabled = True
        Me.btnCan.Enabled = False
        Me.arbBodegas.Enabled = True
        If MalaInformacion(Aviso) Then
            Call MsgBox("Informaci�n incompleta en el(los) siguiente(s) campos:" & Aviso, vbExclamation)
        Else
            Me.arbBodegas.Enabled = True
            Set UnaBodega = New LaBodega
            UnaBodega.AutoNumerico = 0
            UnaBodega.Codigo = Me.txtIDEN.Text
            UnaBodega.Nombre = Me.txtNOMB.Text
            If Not Me.arbBodegas.SelectedItem.Key = "BDGS" Then UnaBodega.Padre = CLng(Right(Me.arbBodegas.SelectedItem.Key, Len(Me.arbBodegas.SelectedItem.Key) - 1))
            UnaBodega.AlmacenaPara = Mid("VC", Me.cboQUEAlma.ListIndex + 1, 1)
            Valores = "TX_CODI_BODE='" & UnaBodega.Codigo & _
                "', TX_NOMB_BODE='" & UnaBodega.Nombre & _
                "', CD_CODI_CECO_BODE=0" & _
                ", TX_CONTA_BODE='" & IIf(chkNoCont.Value, "N", "S") & "'" & _
                ", TX_VENTA_BODE='" & UnaBodega.AlmacenaPara & "'"
            If UnaBodega.Padre > 0 Then Valores = Valores & ", NU_AUTO_BODE_BODE=" & UnaBodega.Padre
            Result = DoInsertSQL("IN_BODEGA", Valores)
            If Result = FAIL Then GoTo Fallo
            Dim ArrTemp() As Variant
            Campos = "NU_AUTO_BODE, NU_AUTO_BODE_BODE, TX_CODI_BODE, TX_NOMB_BODE, " & _
                "CD_CODI_CECO_BODE, TX_VENTA_BODE"
            Desde = "IN_BODEGA"
            Condi = "TX_CODI_BODE='" & UnaBodega.Codigo & "'"
            ReDim ArrTemp(5)
            Result = LoadData(Desde, Campos, Condi, ArrTemp())
            If Result = FAIL Then GoTo Fallo
            If Not Encontro Then GoTo NOENC
            UnaBodega.AutoNumerico = ArrTemp(0)
            If UnaBodega.Padre > 0 Then
                arbBodegas.Nodes.Add "B" & UnaBodega.Padre, tvwChild, "B" & UnaBodega.AutoNumerico, UnaBodega.Nombre
            Else
                arbBodegas.Nodes.Add "BDGS", tvwChild, "B" & UnaBodega.AutoNumerico, UnaBodega.Nombre
            End If
            ClldeBodegas.Add UnaBodega, "B" & UnaBodega.AutoNumerico
            Me.arbBodegas.Nodes.Item("B" & UnaBodega.AutoNumerico).Selected = True
            Me.txtIDEN.Text = Trim(UnaBodega.Codigo)
            Me.txtNOMB.Text = Trim(UnaBodega.Nombre)
            Me.cboQUEAlma.ListIndex = IIf(UnaBodega.AlmacenaPara = "V", 0, 1)
            Set Me.arbBodegas.SelectedItem = Me.arbBodegas.Nodes("B" & UnaBodega.AutoNumerico)
            Set UnaBodega = Nothing
            Me.arbBodegas.SelectedItem.Bold = True
        End If
    End If
NOENC:
    Exit Sub
Fallo:
End Sub

Private Sub btnCha_Click()
    Dim UnaBodega As New LaBodega, Aviso As String
    Me.arbBodegas.Enabled = False
    If Me.tlbACCIONES.Buttons("CHA").Caption = "&Modificar" Then
        Me.tlbACCIONES.Buttons("DEL").Enabled = False
        Me.btnDel.Enabled = False
        Me.txtIDEN.Enabled = True
        Me.txtNOMB.Enabled = True
        Me.chkNoCont.Enabled = True
        Me.cboQUEAlma.Enabled = True
        Me.tlbACCIONES.Buttons("CHA").Caption = "&Guardar"
        Me.btnCha.Caption = "&Guardar"
        Me.tlbACCIONES.Buttons("CAN").Enabled = True
        Me.btnCan.Enabled = True
        Me.txtIDEN.SetFocus
    Else
        Me.tlbACCIONES.Buttons("DEL").Enabled = True
        Me.btnDel.Enabled = True
        Me.txtIDEN.Enabled = False
        Me.txtNOMB.Enabled = False
        Me.chkNoCont.Enabled = False
        Me.cboQUEAlma.Enabled = False
        If MalaInformacion(Aviso) Then
            Call MsgBox("Informaci�n incompleta en el(los) siguiente(s) campos:" & Aviso, vbExclamation)
        Else
'            Dim UnaBodega As LaBodega
            Set UnaBodega = ClldeBodegas.Item(Me.arbBodegas.SelectedItem.Key)
            UnaBodega.Codigo = Me.txtIDEN
            UnaBodega.Nombre = Me.txtNOMB
            UnaBodega.AfectaCont = IIf(chkNoCont.Value, "N", "S") '|.DR.|, R:1633
            UnaBodega.AlmacenaPara = Mid("VC", Me.cboQUEAlma.ListIndex + 1, 1)
            Me.arbBodegas.SelectedItem.Text = UnaBodega.Nombre
            Valores = "TX_CODI_BODE='" & UnaBodega.Codigo & _
                "', TX_NOMB_BODE='" & UnaBodega.Nombre & _
                "', TX_CONTA_BODE='" & IIf(chkNoCont.Value, "N", "S") & _
                "', TX_VENTA_BODE='" & UnaBodega.AlmacenaPara & "'"
            Condi = "NU_AUTO_BODE=" & UnaBodega.AutoNumerico
            Result = DoUpdate("IN_BODEGA", Valores, Condi)
            If Result = FAIL Then GoTo Fallo
        End If
        Me.tlbACCIONES.Buttons("CHA").Caption = "&Modificar"
        Me.btnCha.Caption = "&Modificar"
        Me.tlbACCIONES.Buttons("CAN").Enabled = False
        Me.btnCan.Enabled = False
        Me.arbBodegas.Enabled = True
    End If
Fallo:
End Sub

Private Sub btnCan_Click()
'    If Me.arbBodegas.SelectedItem.Key = "BDGS" Then
        If Me.tlbACCIONES.Buttons("ADD").Caption = "&Guardar" Then Me.tlbACCIONES.Buttons("ADD").Caption = "&Adicionar"
        If Me.btnAdd.Caption = "&Guardar" Then Me.btnAdd.Caption = "&Adicionar"
'    Else
        If Me.tlbACCIONES.Buttons("CHA").Caption = "&Guardar" Then Me.tlbACCIONES.Buttons("CHA").Caption = "&Modificar"
        If Me.btnCha.Caption = "&Guardar" Then Me.btnCha.Caption = "&Modificar"
        Me.tlbACCIONES.Buttons("DEL").Enabled = True
        Me.btnDel.Enabled = True
        Me.tlbACCIONES.Buttons("CHA").Enabled = False
        Me.btnCha.Enabled = False
'    End If
    Call ARBBodegas_NodeClick(Me.arbBodegas.SelectedItem)
    Me.arbBodegas.Enabled = True
End Sub

Private Sub Form_Load()
'    Dim CnTdr As Integer, Punto As MSComctlLib.Node, vclave As String
    Dim CnTdr As Integer, vclave As String      'DEPURACION DE CODIGO
    

'   Call main
   Call CenterForm(MDI_Inventarios, Me)
   Me.txtIDEN.Mask = ">" & String(Me.txtIDEN.MaxLength, "a")
   Me.txtNOMB.Mask = ">" & String(Me.txtNOMB.MaxLength, "a")
    Me.txtIDEN.Text = String(Me.txtIDEN.MaxLength, " ")
    Me.txtNOMB.Text = String(Me.txtNOMB.MaxLength, " ")
    Me.txtIDEN.Enabled = False
    Me.txtNOMB.Enabled = False
    Me.chkNoCont.Enabled = False
'    Campos = "NU_AUTO_BODE, NU_AUTO_BODE_BODE, TX_CODI_BODE, TX_NOMB_BODE, " & _
'        "CD_CODI_CECO_BODE, TX_VENTA_BODE,TX_CONTA_BODE" '|.DR.|, Se agreg� TX_CONTA_BODE. R:1633
'    Desde = "IN_BODEGA ORDER BY NU_AUTO_BODE_BODE"
'    Condi = ""

    'DAHV M4108 - Inicio
    
    'Campos = "NU_AUTO_BODE, NU_AUTO_BODE_BODE, TX_CODI_BODE, TX_NOMB_BODE, CD_CODI_CECO_BODE, TX_VENTA_BODE,TX_CONTA_BODE"
    'Desde = "IN_BODEGA"
    'Condi = "NU_AUTO_BODE IN (SELECT NU_AUTO_BODE_BODE FROM IN_BODEGA)"
    'Condi = Condi & " ORDER BY NU_AUTO_BODE_BODE"
    
    'LJSA M4705 -----  Consulta las primeras bodegas y las siguientes en la jerarquia --------
    Campos = "NU_AUTO_BODE, NU_AUTO_BODE_BODE, TX_CODI_BODE, TX_NOMB_BODE, CD_CODI_CECO_BODE, TX_VENTA_BODE,TX_CONTA_BODE"
    Desde = "IN_BODEGA"
    Condi = "NU_AUTO_BODE_BODE Is Null"         'Bodegas Padre
    Condi = Condi & " Union Select NU_AUTO_BODE, NU_AUTO_BODE_BODE, TX_CODI_BODE, TX_NOMB_BODE, CD_CODI_CECO_BODE, TX_VENTA_BODE,TX_CONTA_BODE"
    Condi = Condi & " From IN_BODEGA Where NU_AUTO_BODE_BODE IN (Select NU_AUTO_BODE From IN_BODEGA Where NU_AUTO_BODE_BODE Is Null)"           'Bodegas Hija
    Condi = Condi & " Order By NU_AUTO_BODE_BODE, NU_AUTO_BODE"
    'LJSA M4705 -----  Consulta las primeras bodegas y las siguientes en la jerarquia --------

    ReDim ArrBDG(6, 0)
    Result = LoadMulData(Desde, Campos, Condi, ArrBDG())
    

    
    'DAHV M4108 - Fin
    
    If Result = FAIL Then GoTo Fallo
    Me.cboQUEAlma.AddItem "SI"
    Me.cboQUEAlma.ItemData(Me.cboQUEAlma.NewIndex) = -1
    Me.cboQUEAlma.AddItem "NO"
    Me.cboQUEAlma.ItemData(Me.cboQUEAlma.NewIndex) = 0
    Me.cboQUEAlma.ListIndex = -1
    Me.cboQUEAlma.Enabled = False
    Me.tlbACCIONES.Buttons("ADD").Enabled = True
    Me.btnAdd.Enabled = True
    Call BorrarColeccion(ClldeBodegas)
    Me.arbBodegas.Nodes.Clear
    Me.arbBodegas.Nodes.Add , , "BDGS", "BODEGAS DEFINIDAS"
    Me.arbBodegas.Nodes.Item("BDGS").Tag = "[[]]"
    If Encontro Then
        For CnTdr = 0 To UBound(ArrBDG, 2)
            Dim UnaBodega As New LaBodega
            UnaBodega.AutoNumerico = ArrBDG(0, CnTdr)
            UnaBodega.Codigo = ArrBDG(2, CnTdr)
            UnaBodega.Nombre = ArrBDG(3, CnTdr)
            UnaBodega.Padre = CLng(IIf(ArrBDG(1, CnTdr) = NUL$, "0", ArrBDG(1, CnTdr)))
            UnaBodega.AlmacenaPara = ArrBDG(5, CnTdr)
            UnaBodega.AfectaCont = ArrBDG(6, CnTdr) '|.DR.|, R:1633
            vclave = "B" & UnaBodega.AutoNumerico
            ClldeBodegas.Add UnaBodega, vclave
            If UnaBodega.Padre = 0 Then
                Me.arbBodegas.Nodes.Add "BDGS", tvwChild, vclave, Trim(UnaBodega.Nombre)
                Set Me.arbBodegas.SelectedItem = Me.arbBodegas.Nodes("BDGS")
            Else
                arbBodegas.Nodes.Add "B" & UnaBodega.Padre, tvwChild, vclave, Trim(UnaBodega.Nombre)
            End If
            Me.arbBodegas.Nodes.Item(vclave).Tag = Trim(UnaBodega.Codigo)
            Set UnaBodega = Nothing
        Next
        'DAHV M4108 - Inicio
        'Campos = "NU_AUTO_BODE, NU_AUTO_BODE_BODE, TX_CODI_BODE, TX_NOMB_BODE, CD_CODI_CECO_BODE, TX_VENTA_BODE,TX_CONTA_BODE"
        'Desde = "IN_BODEGA"
        'Condi = "NU_AUTO_BODE NOT IN (  SELECT NU_AUTO_BODE "
        'Condi = Condi & " FROM IN_BODEGA "
        'Condi = Condi & " WHERE NU_AUTO_BODE IN (SELECT NU_AUTO_BODE_BODE FROM IN_BODEGA))"
        'Condi = Condi & " ORDER BY NU_AUTO_BODE_BODE"
        
        'LJSA M4705  -- Consulta las hijas restantes   ---
        Campos = "NU_AUTO_BODE, NU_AUTO_BODE_BODE, TX_CODI_BODE, TX_NOMB_BODE, CD_CODI_CECO_BODE, TX_VENTA_BODE,TX_CONTA_BODE"
        Desde = "IN_BODEGA"
        Condi = "NU_AUTO_BODE_BODE IN ( SELECT NU_AUTO_BODE "
        Condi = Condi & " FROM IN_BODEGA "
        Condi = Condi & " WHERE NU_AUTO_BODE_BODE IN (SELECT NU_AUTO_BODE_BODE FROM IN_BODEGA Where NU_AUTO_BODE_BODE Is Not Null))"
        Condi = Condi & " And NU_AUTO_BODE_BODE Is Not Null"
        Condi = Condi & " ORDER BY NU_AUTO_BODE_BODE"
        'LJSA M4705  -- Consulta las hijas restantes   ---
    
  
        ReDim ArrBDG(6, 0)
        Result = LoadMulData(Desde, Campos, Condi, ArrBDG())
        
        If Encontro Then
            For CnTdr = 0 To UBound(ArrBDG, 2)
                UnaBodega.AutoNumerico = ArrBDG(0, CnTdr)
                UnaBodega.Codigo = ArrBDG(2, CnTdr)
                UnaBodega.Nombre = ArrBDG(3, CnTdr)
                UnaBodega.Padre = CLng(IIf(ArrBDG(1, CnTdr) = NUL$, "0", ArrBDG(1, CnTdr)))
                UnaBodega.AlmacenaPara = ArrBDG(5, CnTdr)
                UnaBodega.AfectaCont = ArrBDG(6, CnTdr) '|.DR.|, R:1633
                vclave = "B" & UnaBodega.AutoNumerico
                ClldeBodegas.Add UnaBodega, vclave
                If UnaBodega.Padre = 0 Then
                    Me.arbBodegas.Nodes.Add "BDGS", tvwChild, vclave, Trim(UnaBodega.Nombre)
                    Set Me.arbBodegas.SelectedItem = Me.arbBodegas.Nodes("BDGS")
                Else
                    arbBodegas.Nodes.Add "B" & UnaBodega.Padre, tvwChild, vclave, Trim(UnaBodega.Nombre)
                End If
                Me.arbBodegas.Nodes.Item(vclave).Tag = Trim(UnaBodega.Codigo)
                Set UnaBodega = Nothing
            Next
        End If
        'DAHV M4108 - Fin
    End If
Fallo:
End Sub

Private Sub tlbACCIONES_ButtonClick(ByVal Boton As MSComctlLib.Button)
    Select Case Boton.Key
    Case Is = "CHA"
        Call btnCha_Click
    Case Is = "ADD"
        Call btnAdd_Click
    Case Is = "DEL"
        Call btnDel_Click
    Case Is = "CAN"
        Call btnCan_Click
    Case "ACTU"
'        Call ActuaLaBase
    End Select
End Sub

'Private Sub Form_Unload(Cancel As Integer)
'    BD(BDCur).Close
'End Sub

'Private Sub txtIDEN_KeyPress(Tecla As Integer)
'    Call txtNOMB_KeyPress(Tecla)
'End Sub

'Private Sub txtNOMB_KeyPress(Tecla As Integer)
'    Dim Cara As String * 1
'    Cara = Chr(Tecla)
'    If Cara = vbBack Then Exit Sub
'    If Asc(Cara) > 96 And Asc(Tecla) < 113 Then Tecla = Asc(UCase(Chr(Tecla)))
'    If InStr(" 01234567890ABCDEFGHIJKLMNOPQRSTUVWXYZ", Chr(Tecla)) = 0 Then Tecla = 0
'End Sub

Private Function MalaInformacion(ByRef Cual As String) As Boolean
    Cual = ""
    MalaInformacion = False
    If Len(Trim(Me.txtNOMB)) = 0 Then Cual = Cual & IIf(Len(Cual) = 0, "", ", ") & "Nombre"
    If Len(Trim(Me.txtIDEN)) = 0 Then Cual = Cual & IIf(Len(Cual) = 0, "", ", ") & "C�digo"
    If Me.cboQUEAlma.ListIndex < 0 Then Cual = Cual & IIf(Len(Cual) = 0, "", ", ") & "Almacena"
    If Len(Cual) > 0 Then MalaInformacion = True
End Function

Private Sub btnDel_Click()
    If Me.arbBodegas.SelectedItem.Children > 0 Then Call MsgBox("La bodega " & Me.arbBodegas.SelectedItem.Text & ", no puede ser eliminada", vbInformation): GoTo DIJONO
    If MsgBox("Desea borrar la bodega '" & Me.arbBodegas.SelectedItem.Text & "'", vbYesNo) = vbNo Then GoTo DIJONO
    Dim Punto As MSComctlLib.Node, Clave As String, UnaBodega As LaBodega
    Clave = Me.arbBodegas.SelectedItem.Key
    Set Punto = Me.arbBodegas.SelectedItem.Parent
    Set UnaBodega = ClldeBodegas.Item(Clave)
   'REOL M2311
    ReDim Arr(0)
    Result = LoadData("IN_R_BODE_COMP", "NU_AUTO_BODE_RBOCO", "NU_AUTO_BODE_RBOCO = " & UnaBodega.AutoNumerico, Arr)
        If Result <> FAIL And Encontro Then Call Mensaje1("La bodega se encuentra atada a un Comprobante, no se puede borrar", 3): Exit Sub
    Result = LoadData("IN_R_PERFIL_BODEGA", "NU_AUTO_BODE_RPEB", "NU_AUTO_BODE_RPEB = " & UnaBodega.AutoNumerico, Arr)
        If Result <> FAIL And Encontro Then Call Mensaje1("La bodega se encuentra atada a un Perfil, no se puede borrar", 3): Exit Sub
    Result = LoadData("IN_R_GRUP_BODE", "NU_AUTO_BODE_RGB", "NU_AUTO_BODE_RGB = " & UnaBodega.AutoNumerico, Arr)
        If Result <> FAIL And Encontro Then Call Mensaje1("La bodega se encuentra atada a un Grupo, no se puede borrar", 3): Exit Sub
   'REOL M2311
    Condi = "NU_AUTO_BODE=" & UnaBodega.AutoNumerico
    Result = DoDelete("IN_BODEGA", Condi)
    If Result = FAIL Then GoTo Fallo
    Me.arbBodegas.Nodes.Remove Clave
    ClldeBodegas.Remove Clave
    Call ARBBodegas_NodeClick(Punto)
    Exit Sub
Fallo:
DIJONO:
End Sub

'Private Sub btnAdicionar_Click()
'    Dim Punto As MSComctlLib.Node
'    If Me.tlbACCIONES.Buttons("ADD").Caption = "&Adicionar" Then
'        Me.txtIDEN.Enabled = True
'        Me.txtNOMB.Enabled = True
'        Me.cboQUEAlma.Enabled = True
'        Me.txtIDEN.Text = ""
'        Me.txtNOMB.Text = ""
'        Me.cboQUEAlma.ListIndex = -1
'        Me.tlbACCIONES.Buttons("ADD").Caption = "&Guardar"
'    Else
'        Me.tlbACCIONES.Buttons("ADD").Caption = "&Adicionar"
'        If Len(Trim(Me.txtIDEN)) = 0 Or Len(Trim(Me.txtNOMB)) = 0 Or Me.cboQUEAlma.ListIndex < 0 Then Call MsgBox("Informaci�n incompleta", vbInformation): Exit Sub
'        If MsgBox("Desea guardar la informaci�n", vbYesNo) = vbNo Then GoTo DIJONO
'        Set Unabodega = New LaBodega
'        Set Punto = Me.arbBodegas.SelectedItem
'        Unabodega.AutoNumerico = 0
'        Unabodega.Codigo = Me.txtIDEN
'        Unabodega.Nombre = Me.txtNOMB
'        Unabodega.Padre = CLng(Right(Punto.Key, Len(Punto.Key) - 1))
'        Unabodega.AlmacenaPara = Mid("VC", Me.cboQUEAlma.ListIndex + 1, 1)
'        Valores = "NU_AUTO_BODE_BODE=" & Unabodega.Padre & _
'            ", TX_CODI_BODE='" & Unabodega.Codigo & _
'            "', TX_NOMB_BODE='" & Unabodega.Nombre & _
'            "', CD_CODI_CECO_BODE=0" & _
'            ", TX_VENTA_BODE='" & Unabodega.AlmacenaPara & "'"
'        Result = DoInsertSQL("IN_BODEGA", Valores)
'        If Result = FAIL Then GoTo FALLO
'        Dim ArrTemp() As Variant
'        Campos = "NU_AUTO_BODE, NU_AUTO_BODE_BODE, TX_CODI_BODE, TX_NOMB_BODE, " & _
'            "CD_CODI_CECO_BODE, TX_VENTA_BODE"
'        Desde = "IN_BODEGA"
'        Condi = "TX_CODI_BODE='" & Unabodega.Codigo & "'"
'        ReDim ArrTemp(5)
'        Result = LoadData(Desde, Campos, Condi, ArrTemp())
'        If Result = FAIL Then GoTo FALLO
'        If Not Encontro Then GoTo NOENC
'        Unabodega.AutoNumerico = ArrTemp(0)
'        arbBodegas.Nodes.Add "B" & Unabodega.Padre, tvwChild, "B" & Unabodega.AutoNumerico, Unabodega.Nombre
'        ClldeBodegas.Add Unabodega, "B" & Unabodega.AutoNumerico
'        Me.arbBodegas.Nodes.Item("B" & Unabodega.AutoNumerico).Selected = True
'        Set Punto = Me.arbBodegas.SelectedItem
'        Call arbBodegas_NodeClick(Punto)
'        Set Unabodega = Nothing
'    End If
'    Exit Sub
'NOENC:
'    Exit Sub
'FALLO:
'    Exit Sub
'DIJONO:
'End Sub

Private Sub txtIDEN_KeyPress(KeyAscii As Integer)
    Call Cambiar_Enter(KeyAscii)
End Sub

Private Sub txtIDEN_LostFocus()
    Dim unNodo As MSComctlLib.Node
    For Each unNodo In Me.arbBodegas.Nodes
        If Me.tlbACCIONES.Buttons.Item("CHA").Caption = "&Guardar" Then
            If Not Me.arbBodegas.SelectedItem.Key = unNodo.Key Then
                If Trim(Me.txtIDEN.Text) = unNodo.Tag Then
                    Call MsgBox("El C�digo digitado ya existe, debe cambiarlo", vbInformation)
                    Me.txtIDEN.SetFocus
                    Exit Sub
                End If
            End If
        ElseIf Me.tlbACCIONES.Buttons.Item("ADD").Caption = "&Guardar" Then
            If Trim(Me.txtIDEN.Text) = unNodo.Tag Then
                Call MsgBox("El C�digo digitado ya existe, debe cambiarlo", vbInformation)
                Me.txtIDEN.SetFocus
                Exit Sub
            End If
        End If
    Next
End Sub


Private Sub txtNOMB_KeyPress(KeyAscii As Integer)
    Call Cambiar_Enter(KeyAscii)
End Sub
