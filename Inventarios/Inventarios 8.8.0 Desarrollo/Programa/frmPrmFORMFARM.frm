VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "Mscomctl.ocx"
Object = "{C932BA88-4374-101B-A56C-00AA003668DC}#1.1#0"; "MSMASK32.OCX"
Object = "{0BA686C6-F7D3-101A-993E-0000C0EF6F5E}#1.0#0"; "THREED32.OCX"
Begin VB.Form frmPrmFORMFARM 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Forma Farmac�utica"
   ClientHeight    =   5940
   ClientLeft      =   45
   ClientTop       =   435
   ClientWidth     =   10725
   Icon            =   "frmPrmFORMFARM.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   5940
   ScaleWidth      =   10725
   Begin MSComctlLib.ListView lstFORMFARM 
      Height          =   3015
      Left            =   1320
      TabIndex        =   8
      Top             =   840
      Width           =   7575
      _ExtentX        =   13361
      _ExtentY        =   5318
      View            =   3
      LabelEdit       =   1
      LabelWrap       =   -1  'True
      HideSelection   =   0   'False
      AllowReorder    =   -1  'True
      FullRowSelect   =   -1  'True
      _Version        =   393217
      ForeColor       =   -2147483640
      BackColor       =   -2147483643
      BorderStyle     =   1
      Appearance      =   1
      NumItems        =   0
   End
   Begin VB.Frame Frame1 
      Caption         =   "Informaci�n:"
      Height          =   1935
      Left            =   120
      TabIndex        =   0
      Top             =   3960
      Width           =   5415
      Begin MSMask.MaskEdBox txtNOMB 
         Height          =   855
         Left            =   1560
         TabIndex        =   10
         Top             =   900
         Width           =   3615
         _ExtentX        =   6376
         _ExtentY        =   1508
         _Version        =   393216
         PromptInclude   =   0   'False
         MaxLength       =   50
         PromptChar      =   "_"
      End
      Begin MSMask.MaskEdBox txtIDEN 
         Height          =   375
         Left            =   1560
         TabIndex        =   9
         Top             =   360
         Width           =   1695
         _ExtentX        =   2990
         _ExtentY        =   661
         _Version        =   393216
         PromptInclude   =   0   'False
         MaxLength       =   10
         PromptChar      =   "_"
      End
      Begin VB.Label Label2 
         Alignment       =   1  'Right Justify
         Caption         =   "Nombre:"
         Height          =   375
         Left            =   480
         TabIndex        =   2
         Top             =   1080
         Width           =   1005
      End
      Begin VB.Label Label1 
         Alignment       =   1  'Right Justify
         Caption         =   "C�digo:"
         Height          =   375
         Left            =   480
         TabIndex        =   1
         Top             =   360
         Width           =   1005
      End
   End
   Begin MSComctlLib.Toolbar tlbACCIONES 
      Align           =   1  'Align Top
      Height          =   660
      Left            =   0
      TabIndex        =   3
      Top             =   0
      Visible         =   0   'False
      Width           =   10725
      _ExtentX        =   18918
      _ExtentY        =   1164
      ButtonWidth     =   1799
      ButtonHeight    =   1005
      AllowCustomize  =   0   'False
      Appearance      =   1
      _Version        =   393216
      BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
         NumButtons      =   4
         BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Enabled         =   0   'False
            Caption         =   "&Adicionar"
            Key             =   "ADD"
            Object.Tag             =   "Adicionar"
         EndProperty
         BeginProperty Button2 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Enabled         =   0   'False
            Caption         =   "&Borrar"
            Key             =   "DEL"
            Object.Tag             =   "Borrar"
         EndProperty
         BeginProperty Button3 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Enabled         =   0   'False
            Caption         =   "&Modificar"
            Key             =   "CHA"
            Object.Tag             =   "Modificar"
         EndProperty
         BeginProperty Button4 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Enabled         =   0   'False
            Caption         =   "&Cancelar"
            Key             =   "CAN"
            Object.Tag             =   "Cancelar"
         EndProperty
      EndProperty
      BorderStyle     =   1
   End
   Begin Threed.SSCommand btnCan 
      Height          =   735
      Left            =   9480
      TabIndex        =   4
      Top             =   4560
      Width           =   855
      _Version        =   65536
      _ExtentX        =   1508
      _ExtentY        =   1296
      _StockProps     =   78
      Caption         =   "&CANCELAR"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   6
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Enabled         =   0   'False
      Picture         =   "frmPrmFORMFARM.frx":058A
   End
   Begin Threed.SSCommand btnAdd 
      Height          =   735
      Left            =   5880
      TabIndex        =   5
      Top             =   4560
      Width           =   855
      _Version        =   65536
      _ExtentX        =   1508
      _ExtentY        =   1296
      _StockProps     =   78
      Caption         =   "&ADICIONAR"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   6
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Enabled         =   0   'False
      Picture         =   "frmPrmFORMFARM.frx":0C54
   End
   Begin Threed.SSCommand btnDel 
      Height          =   735
      Left            =   7080
      TabIndex        =   6
      Top             =   4560
      Width           =   855
      _Version        =   65536
      _ExtentX        =   1508
      _ExtentY        =   1296
      _StockProps     =   78
      Caption         =   "&BORRAR"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   6
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Enabled         =   0   'False
      Picture         =   "frmPrmFORMFARM.frx":18A6
   End
   Begin Threed.SSCommand btnCha 
      Height          =   735
      Left            =   8280
      TabIndex        =   7
      Top             =   4560
      Width           =   855
      _Version        =   65536
      _ExtentX        =   1508
      _ExtentY        =   1296
      _StockProps     =   78
      Caption         =   "&MODIFICAR"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   6
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Enabled         =   0   'False
      Picture         =   "frmPrmFORMFARM.frx":1F70
   End
End
Attribute VB_Name = "frmPrmFORMFARM"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Dim ArrBDG() As Variant
Public OpcCod        As String   'Opci�n de seguridad
'NU_AUTO_UNVE, TX_CODI_UNVE, TX_NOMB_UNVE, NU_MULT_UNVE, NU_DIVI_UNVE

'DEPURACION DE CODIGO
'Private Sub arbBodegas_Collapse(ByVal Punto As MSComctlLib.Node)
'    If Punto.Key = "UNDS" Then
'        Me.txtIDEN.Text = String(Me.txtIDEN.MaxLength, " ")
'        Me.txtNOMB.Text = String(Me.txtNOMB.MaxLength, " ")
'        Me.tlbACCIONES.Buttons("DEL").Enabled = False
'        Me.tlbACCIONES.Buttons("CHA").Enabled = False
'        Me.tlbACCIONES.Buttons("CAN").Enabled = False
'        Me.btnDel.Enabled = False
'        Me.btnCha.Enabled = False
'        Me.btnCan.Enabled = False
'        Me.txtIDEN.Enabled = False
'        Me.txtNOMB.Enabled = False
'    End If
'End Sub

Private Sub btnAdd_Click()
    Dim ArrCSC() As Variant, Aviso As String, vclave As String
    If Me.tlbACCIONES.Buttons("ADD").Caption = "&Adicionar" Then
        Me.tlbACCIONES.Buttons("DEL").Enabled = False
        Me.tlbACCIONES.Buttons("CHA").Enabled = False
        Me.btnDel.Enabled = False
        Me.btnCha.Enabled = False
'        Me.txtIDEN.Text = String(Me.txtIDEN.MaxLength, " ")
'        Me.txtNOMB.Text = String(Me.txtNOMB.MaxLength, " ")
        Me.txtIDEN.Text = ""
        Me.txtNOMB.Text = ""
        Me.txtIDEN.Enabled = True
        Me.txtNOMB.Enabled = True
        Me.tlbACCIONES.Buttons("ADD").Caption = "&Guardar"
        Me.btnAdd.Caption = "&Guardar"
        Me.tlbACCIONES.Buttons("CAN").Enabled = True
        Me.btnCan.Enabled = True
        Me.txtIDEN.SetFocus
    Else
        Me.txtIDEN.Enabled = False
        Me.txtNOMB.Enabled = False
        Me.tlbACCIONES.Buttons("ADD").Caption = "&Adicionar"
        Me.btnAdd.Caption = "&Adicionar"
        Me.tlbACCIONES.Buttons("DEL").Enabled = True
        Me.tlbACCIONES.Buttons("CAN").Enabled = False
        Me.btnDel.Enabled = True
        Me.btnCan.Enabled = False
        Me.lstFORMFARM.Enabled = True
        If MalaInformacion(Aviso) Then
            Call MsgBox("Informaci�n incompleta en el(los) siguiente(s) campos:" & Aviso, vbExclamation)
        Else
            Me.lstFORMFARM.Enabled = True
            Valores = "TX_CODI_FOFA='" & Me.txtIDEN.Text & _
                "', TX_NOMB_FOFA='" & Cambiar_Comas_Comillas(Me.txtNOMB.Text) & "'"
            Result = DoInsertSQL("IN_FORMAFARMACEUTICA", Valores)
            If Result = FAIL Then GoTo FALLO
            Campos = "NU_AUTO_FOFA, TX_CODI_FOFA, TX_NOMB_FOFA"
'NU_AUTO_FORM, TX_CODI_FORM, TX_NOMB_FORM
'FROM IN_FORMAFARMACEUTICA;
            Desde = "IN_FORMAFARMACEUTICA"
            Condi = "TX_CODI_FOFA='" & Me.txtIDEN.Text & "'"
            ReDim ArrCSC(2)
            Result = LoadData(Desde, Campos, Condi, ArrCSC())
            If Result = FAIL Then GoTo FALLO
            If Not Encontro Then GoTo NOENC
            vclave = "F" & ArrCSC(1)
            Me.lstFORMFARM.ListItems.Add , vclave, ArrCSC(1)
            Me.lstFORMFARM.ListItems(vclave).Tag = ArrCSC(1)
            Me.lstFORMFARM.ListItems(vclave).ListSubItems.Add , "AUTO", ArrCSC(0)
            Me.lstFORMFARM.ListItems(vclave).ListSubItems.Add , "NOMB", ArrCSC(2)
            Me.txtIDEN.Text = ArrCSC(1)
            Me.txtNOMB.Text = ArrCSC(2)
            Set Me.lstFORMFARM.SelectedItem = Me.lstFORMFARM.ListItems.Item(vclave)
            Me.lstFORMFARM.SelectedItem.EnsureVisible
        End If
    End If
NOENC:
    Exit Sub
FALLO:
End Sub

Private Sub btnCha_Click()
    Dim Aviso As String
    Me.lstFORMFARM.Enabled = False
    If Me.tlbACCIONES.Buttons("CHA").Caption = "&Modificar" Then
        Me.tlbACCIONES.Buttons("DEL").Enabled = False
        Me.btnDel.Enabled = False
        Me.txtIDEN.Enabled = True
        Me.txtNOMB.Enabled = True
        Me.tlbACCIONES.Buttons("CHA").Caption = "&Guardar"
        Me.btnCha.Caption = "&Guardar"
        Me.tlbACCIONES.Buttons("CAN").Enabled = True
        Me.btnCan.Enabled = True
        Me.txtIDEN.SetFocus
    Else
        Me.tlbACCIONES.Buttons("DEL").Enabled = True
        Me.btnDel.Enabled = True
        Me.txtIDEN.Enabled = False
        Me.txtNOMB.Enabled = False
        Me.tlbACCIONES.Buttons("CHA").Caption = "&Modificar"
        Me.btnCha.Caption = "&Modificar"
        Me.tlbACCIONES.Buttons("CAN").Enabled = False
        Me.btnCan.Enabled = False
        Me.lstFORMFARM.Enabled = True
        If MalaInformacion(Aviso) Then
            Call MsgBox("Informaci�n incompleta en el(los) siguiente(s) campos:" & Aviso, vbExclamation)
        Else
            
'NU_AUTO_FORM, TX_CODI_FORM, TX_NOMB_FORM
'FROM IN_FORMAFARMACEUTICA;
            
            Valores = "TX_CODI_FOFA='" & Me.txtIDEN.Text & _
                "', TX_NOMB_FOFA='" & Cambiar_Comas_Comillas(Me.txtNOMB.Text) & "'"
            Condi = "NU_AUTO_FOFA=" & Me.lstFORMFARM.SelectedItem.ListSubItems("AUTO").Text
            Result = DoUpdate("IN_FORMAFARMACEUTICA", Valores, Condi)
            If Result = FAIL Then GoTo FALLO
            Me.lstFORMFARM.SelectedItem.Text = Me.txtIDEN.Text
            Me.lstFORMFARM.SelectedItem.ListSubItems("NOMB").Text = Me.txtNOMB.Text
        End If
    End If
FALLO:
End Sub

Private Sub btnCan_Click()
    If Me.tlbACCIONES.Buttons("ADD").Caption = "&Guardar" Then Me.tlbACCIONES.Buttons("ADD").Caption = "&Adicionar"
    If Me.btnAdd.Caption = "&Guardar" Then Me.btnAdd.Caption = "&Adicionar"
    If Me.tlbACCIONES.Buttons("CHA").Caption = "&Guardar" Then Me.tlbACCIONES.Buttons("CHA").Caption = "&Modificar"
    If Me.btnCha.Caption = "&Guardar" Then Me.btnCha.Caption = "&Modificar"
    If Not Me.tlbACCIONES.Buttons("DEL").Enabled Then Me.tlbACCIONES.Buttons("DEL").Enabled = True
    If Not Me.btnDel.Enabled Then Me.btnDel.Enabled = True
    Me.tlbACCIONES.Buttons("CHA").Enabled = False
    Me.btnCha.Enabled = False
    Me.lstFORMFARM.Enabled = True
End Sub

Private Sub Form_Load()
'    Dim CnTdr As Integer, Punto As MSComctlLib.Node, vclave As String
    Dim CnTdr As Integer, vclave As String      'DEPURACION CODIGO
'    Call main
   Call CenterForm(MDI_Inventarios, Me)
   Me.txtIDEN.Mask = ">" & String(Me.txtIDEN.MaxLength, "a")
   Me.txtNOMB.Mask = ">" & String(Me.txtNOMB.MaxLength, "&")
    Me.txtIDEN.Text = String(Me.txtIDEN.MaxLength, " ")
    Me.txtNOMB.Text = String(Me.txtNOMB.MaxLength, " ")
    Me.txtIDEN.Enabled = False
    Me.txtNOMB.Enabled = False
    Me.lstFORMFARM.MultiSelect = False
    Me.lstFORMFARM.FullRowSelect = True
'NU_AUTO_FORM, TX_CODI_FORM, TX_NOMB_FORM
'FROM IN_FORMAFARMACEUTICA;
    Campos = "NU_AUTO_FOFA, TX_CODI_FOFA, TX_NOMB_FOFA"
    Desde = "IN_FORMAFARMACEUTICA"
    Condi = ""
    ReDim ArrBDG(2, 0)
    Result = LoadMulData(Desde, Campos, Condi, ArrBDG())
    If Result = FAIL Then GoTo FALLO
    Me.tlbACCIONES.Buttons("ADD").Enabled = True
    Me.btnAdd.Enabled = True
    If Encontro Then
        Me.lstFORMFARM.ListItems.Clear
        Me.lstFORMFARM.ColumnHeaders.Clear
        Me.lstFORMFARM.ColumnHeaders.Add , "CODI", "C�digo", 0.35 * Me.lstFORMFARM.Width
        Me.lstFORMFARM.ColumnHeaders.Add , "AUTO", "Auto", 0 * Me.lstFORMFARM.Width
        Me.lstFORMFARM.ColumnHeaders.Add , "NOMB", "Nombre", 0.65 * Me.lstFORMFARM.Width
        On Error GoTo SIGUI
        For CnTdr = 0 To UBound(ArrBDG, 2)
            vclave = "F" & ArrBDG(1, CnTdr)
            Me.lstFORMFARM.ListItems.Add , vclave, ArrBDG(1, CnTdr)
            Me.lstFORMFARM.ListItems(vclave).Tag = ArrBDG(1, CnTdr)
            Me.lstFORMFARM.ListItems(vclave).ListSubItems.Add , "AUTO", ArrBDG(0, CnTdr)
            Me.lstFORMFARM.ListItems(vclave).ListSubItems.Add , "NOMB", Restaurar_Comas_Comillas(CStr(ArrBDG(2, CnTdr)))
SIGUI:
        Next
        On Error GoTo 0
    End If
FALLO:
End Sub

Private Sub lstFORMFARM_Click()
    Me.tlbACCIONES.Buttons.Item("DEL").Enabled = False
    Me.tlbACCIONES.Buttons.Item("CHA").Enabled = False
    Me.tlbACCIONES.Buttons.Item("CAN").Enabled = False
    Me.btnDel.Enabled = False
    Me.btnCha.Enabled = False
    Me.btnCan.Enabled = False
    Me.tlbACCIONES.Buttons("DEL").Enabled = True
    Me.tlbACCIONES.Buttons("CHA").Enabled = True
    Me.btnDel.Enabled = True
    Me.btnCha.Enabled = True
    Me.txtIDEN.Enabled = False
    Me.txtNOMB.Enabled = False

    If Me.lstFORMFARM.ListItems.Count = 0 Then 'AFMG T15305 se valida desde cada una de las posiciones de la lista
            
             
       Else
       If Me.lstFORMFARM.ListItems.Count > 0 Then
     
          Me.txtIDEN.Text = Trim(Me.lstFORMFARM.SelectedItem.Text)
          Me.txtNOMB.Text = Trim(Me.lstFORMFARM.SelectedItem.ListSubItems("NOMB").Text)
          Call lstFORMFARM.SelectedItem.EnsureVisible  'AFMG T15305 muestra los valores al igresarlos
      
       End If
        
     End If 'AFMG T15305
          
    'Me.txtIDEN.Text = Left(Me.lstFORMFARM.SelectedItem.Text & String(Me.txtIDEN.MaxLength, " "), Me.txtIDEN.MaxLength)
 
    'Me.txtNOMB.Text = Left(Me.lstFORMFARM.SelectedItem.ListSubItems("NOMB").Text & String(Me.txtNOMB.MaxLength, " "), Me.txtNOMB.MaxLength)
    
    
End Sub

Private Sub tlbACCIONES_ButtonClick(ByVal Boton As MSComctlLib.Button)
    Select Case Boton.Key
    Case Is = "CHA"
        Call btnCha_Click
    Case Is = "ADD"
        Call btnAdd_Click
    Case Is = "DEL"
        Call btnDel_Click
    Case Is = "CAN"
        Call btnCan_Click
    End Select
End Sub

Private Function MalaInformacion(ByRef Cual As String) As Boolean
    Cual = ""
    MalaInformacion = False
    If Len(Trim(Me.txtNOMB)) = 0 Then Cual = Cual & IIf(Len(Cual) = 0, "", ", ") & "Nombre"
    If Len(Trim(Me.txtIDEN)) = 0 Then Cual = Cual & IIf(Len(Cual) = 0, "", ", ") & "C�digo"
    If Len(Cual) > 0 Then MalaInformacion = True
End Function

Private Sub btnDel_Click()
    If MsgBox("Desea borrar la Forma farmac�utica '" & Trim(Me.lstFORMFARM.SelectedItem.Text) & "'", vbYesNo) = vbNo Then GoTo DIJONO
    Condi = "NU_AUTO_FOFA=" & Me.lstFORMFARM.SelectedItem.ListSubItems("AUTO").Text
    Result = DoDelete("IN_FORMAFARMACEUTICA", Condi)
    If Result = FAIL Then GoTo FALLO
    Me.lstFORMFARM.ListItems.Remove Me.lstFORMFARM.SelectedItem.Key
    Exit Sub
FALLO:
DIJONO:
End Sub

Private Sub Form_KeyPress(KeyAscii As Integer)
    If KeyAscii = vbKeyReturn Then KeyAscii = vbKeyTab
End Sub

Private Sub txtIDEN_LostFocus()
    Dim vUno As MSComctlLib.ListItem
    For Each vUno In Me.lstFORMFARM.ListItems
        If Me.tlbACCIONES.Buttons.Item("ADD").Caption = "&Guardar" Then
            If Trim(Me.txtIDEN.Text) = vUno.Tag Then
                Call MsgBox("El C�digo digitado ya existe, debe cambiarlo", vbInformation)
                Me.txtIDEN.SetFocus
            End If
        ElseIf Me.tlbACCIONES.Buttons.Item("CHA").Caption = "&Guardar" Then
            If Not Me.lstFORMFARM.SelectedItem.Key = vUno.Key Then
                If Trim(Me.txtIDEN.Text) = vUno.Tag Then
                    Call MsgBox("El C�digo digitado ya existe, debe cambiarlo", vbInformation)
                    Me.txtIDEN.SetFocus
                End If
            End If
        End If
    Next
End Sub
