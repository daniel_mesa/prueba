Attribute VB_Name = "Archivos"
Option Explicit

Public Const conNumBoxes = 5
Public Const conSaveFile = 1, conLoadFile = 2
Public Const conReplaceFile = 1, conReadFile = 2, conAddToFile = 3
Public Const conRandomFile = 4, conBinaryFile = 5
Public Const errDeviceUnavailable = 68
Public Const errDiskNotReady = 71, errFileAlreadyExists = 58
Public Const errTooManyFiles = 67, errRenameAcrossDisks = 74
Public Const errPathFileAccessError = 75, errDeviceIO = 57
Public Const errDiskFull = 61, errBadFileName = 64
Public Const errBadFileNameOrNumber = 52, errFileNotFound = 53
Public Const errPathDoesNotExist = 76, errBadFileMode = 54
Public Const errFileAlreadyOpen = 55, errInputPastEndOfFile = 62
Global Const OFN_PATHMUSTEXIST = &H800&
Global Const OFN_FILEMUSTEXIST = &H1000&
Global Const DLG_FILE_OPEN = 1
Sub AbriArchivo(filename)
'    Dim NL, TextIn, GetLine    'DEPURACION DE CODIGO
    Dim NL
'    Dim fIndex As Integer       'DEPURACION DE CODIGO
'    Dim lscadena As Variant    'DEPURACION DE CODIGO
'    Dim lCadena As String       'DEPURACION DE CODIGO
    Dim fecha As Variant
    Dim Usuario As Variant
'    Dim Clave As Variant       'DEPURACION DE CODIGO
    Dim Tabla As Variant
    Dim Trans As Variant
    Dim Desc As Variant
'    Dim Vlr As Integer         'DEPURACION DE CODIGO
     
    NL = Chr(13) + Chr(10)
    
    On Error Resume Next
    ' Abre el archivo seleccionado.
    Call MouseClock
    Open filename For Input As #1
    If ERR Then
        MsgBox "No es posible abrir el archivo: " + filename
        Exit Sub
    End If
    Dim i As Long
    If (BeginTran(STranIns & "AUDITOR") <> FAIL) Then
      Do While Not EOF(1) ' Repite el bucle hasta el final del archivo.
            Input #1, fecha, Usuario, Tabla, Trans, Desc
            Valores = Format(Trim(fecha), "dd/mm/yyyy hh:mm:ss AM/PM") & Coma
            Valores = Valores & Usuario & Coma 'variable que toma el usuario logeado
            Valores = Valores & Tabla & Coma
            Valores = Valores & Trans & Coma
            Desc = Left(Desc, Len(Desc) - 1)
            Desc = Right(Desc, Len(Desc) - 1)
            Valores = Valores & Comi & Cambiar_Comas_Comillas(Trim(Desc)) & Comi
            If Valores <> NUL$ Then
                Result = DoInsertSQL("AUDITOR", Valores)
            End If
            If (Result = FAIL) Or i = 547 Then
                Call Mensaje1("No se realiz� el registro de auditor�a", 1)
                Call RollBackTran: Exit Do
                
            End If
            i = i + 1
            Debug.Print Valores
        Loop
        
    End If
    Debug.Print Valores
    If Result <> FAIL Then
        If CommitTran() = FAIL Then
            Call RollBackTran
        End If
    End If
    Close #1
    Call MouseNorm
End Sub

Sub gAbrirFile(filename)
    Dim NL
'    Dim NL, TextIn, GetLine    'DEPURACION DE CODIGO
'    Dim fIndex As Integer      'DEPURACION DE CODIGO
'    Dim lscadena As Variant    'DEPURACION DE CODIGO
'    Dim lCadena As String      'DEPURACION DE CODIGO
'    Dim fecha As Variant       'DEPURACION DE CODIGO
'    Dim Usuario As Variant     'DEPURACION DE CODIGO
'    Dim Clave As Variant       'DEPURACION DE CODIGO
'    Dim Tabla As Variant       'DEPURACION DE CODIGO
'    Dim Trans As Variant       'DEPURACION DE CODIGO
'    Dim Desc As Variant        'DEPURACION DE CODIGO
    NL = Chr(13) + Chr(10)
    
    On Error Resume Next
    ' Abre el archivo seleccionado.
    Screen.MousePointer = 11
    Open filename For Input As #1
    If ERR Then
        MsgBox "No es posible abrir el archivo: " + filename
        Exit Sub
    End If
    Close #1
    Screen.MousePointer = 0
End Sub

'DEPURACION DE CODIGO
'Function GetFileName(filename As Variant)
'     Muestra un cuadro de dialogo "Guardar como" y devuelve el nombre del archivo.
'     Si el usuario elige "Cancelar", devuelve una cadena vac�a.
'    On Error Resume Next
'    MDI_Inventarios.CMDialog1.filename = ""
'    MDI_Inventarios.CMDialog1.ShowSave
'    If ERR <> 32755 Then    ' El usuario eligi� "Cancelar".
'        GetFileName = MDI_Inventarios.CMDialog1.filename
'    Else
'        GetFileName = ""
'    End If
'End Function

Function gAbrirAch(ByVal Indicador As Integer, Optional BoEspeServidor As Boolean = False) As String
'Function gAbrirAch(ByVal Indicador As Integer) As String
'    Dim filename As String
'    Dim lNomArch As String
'    Dim lN As Integer
'    Dim li As Integer
'    Dim lNl As String
'    Dim lMsg As String
'    Dim lRespuesta As Integer
'    Dim lInd As Integer
    On Error Resume Next
    MDI_Inventarios.CMDialog1.InitDir = DirDB
    MDI_Inventarios.CMDialog1.filename = ""
    '--------------------------------------------------------------------------------------
    'GMS 2008-01-30
    'MDI_Inventarios.CMDialog1.Filter = IIf(Indicador = 1, "(*.bak)|*.bak", "(*.bdb)|*.bdb")
    MDI_Inventarios.CMDialog1.Filter = IIf(Indicador = 1, IIf(BoEspeServidor, "Especifique la ruta al servidor  (*.bak)|*.bak", "(*.bak)|*.bak"), "(*.bdb)|*.bdb")
    '--------------------------------------------------------------------------------------
    MDI_Inventarios.CMDialog1.FilterIndex = 1
    MDI_Inventarios.CMDialog1.Flags = OFN_PATHMUSTEXIST Or OFN_FILEMUSTEXIST
    MDI_Inventarios.CMDialog1.CancelError = True
    If validabackup = 1 Then
        MDI_Inventarios.CMDialog1.Action = 2
    Else
        MDI_Inventarios.CMDialog1.Action = 1
    End If
    If ERR Then Exit Function
    gAbrirAch = MDI_Inventarios.CMDialog1.filename
End Function

Sub GuardarBak(filename)
On Error Resume Next
    Dim Contents As String
    Dim ls As Integer
    Dim li As Integer

    ' Abre el archivo.
    Open filename For Output As #1
    ReDim lArr(4, 0) As Variant
    Condi = NUL$
    Result = LoadMulData("AUDITOR", "*", Condi, lArr())
    If Encontro Then
        For ls = 0 To UBound(lArr(), 2)
            For li = 0 To UBound(lArr(), 1)
                If IsDate(lArr(li, ls)) Then lArr(li, ls) = Format(lArr(li, ls), "dd/mm/yyyy" & ESP$ & "hh:mm:ss AM/PM")
                Contents = Contents & Comi & Cambiar_Comas_Comillas(CStr(lArr(li, ls))) & Comi & Coma & Chr(9)
                'ComiD
            Next li
            Contents = Left(Contents, Len(Contents) - 2)
            Print #1, Contents
            Contents = NUL$
        Next ls
    End If
    Close #1
    Screen.MousePointer = 0
    If ERR Then
        MsgBox Error, 48, App.Title
    Else
    End If
End Sub
'---------------------------------------------------------------
'Copies file Filename from SourcePath to DestinationPath.
'If VerFlag is set to true (-1) then use version checking
'algorithm so older versions are not copied over newer versions
'
'Returns 0 if it could not find the file, or other runtime
'error occurs.  Otherwise, returns true.
'
'If the source file is older, and the older% parameter is
'true, the function returns success (-1) even though no
'file was copied, since no error occurred.
'---------------------------------------------------------------
Function CopyFile(ByVal SourcePath As String, ByVal DestinationPath As String, ByVal filename As String, VerFlag As Integer)
   Dim Index As Integer
   Dim FileLength As Long
   Dim LeftOver As Long
   Dim FileData As String
   Dim szBufSrc As String
   Dim szBufDest As String
   Dim NumBlocks As Long
   
   Call MouseClock
   '--------------------------------------
   'Add ending \ symbols to path variables
   '--------------------------------------
   Call AjustarPath(SourcePath)
   Call AjustarPath(DestinationPath)
   
   '----------------------------
   'Update status dialog info
   '----------------------------
   If (Not FileExist(SourcePath & filename)) Then
      Call Mensaje1("No se encontr� el archivo " & SourcePath & filename, 1)
      GoTo ErrorCopy
   End If

   On Error GoTo ErrorCopy
   '-------------------------------------------------
   ' If version checking set to True, then get their
   ' version info, skip if older version
   '-------------------------------------------------
   If (VerFlag) Then
      szBufSrc = String(255, 32)
      Call GetFileVersion(SourcePath & filename, szBufSrc, Len(szBufSrc))
      
      szBufDest = String(255, 32)
      Call GetFileVersion(DestinationPath & filename, szBufDest, Len(szBufDest))
      
      If (szBufSrc < szBufDest) Then GoTo SkipCopy
   End If
   '-------------
   'Copy the file
   '-------------
   Const BlockSize = 32768
   
   Open SourcePath & filename For Binary Access Read As #1
   Open DestinationPath & filename For Output As #2
   Close #2
   Open DestinationPath & filename For Binary As #2
   
   FileLength = LOF(1)
   NumBlocks = FileLength \ BlockSize
   LeftOver = FileLength Mod BlockSize
   If (LeftOver > 0) Then
      FileData = String(LeftOver, 32)
      Get #1, , FileData
      Put #2, , FileData
   End If
   
   FileData = String(BlockSize, 32)
   For Index = 1 To NumBlocks
      Get #1, , FileData
      Put #2, , FileData
   Next Index
   Close #1, #2
   Call SetFileDateTime(SourcePath & filename, DestinationPath & filename)
   
SkipCopy:
   szBufSrc = NUL$
   szBufDest = NUL$
   Call MouseNorm
   CopyFile = SUCCEED
   Exit Function
   
ErrorCopy:
   Call Mensaje1("Ha ocurrido un error copiando el archivo " & SourcePath & filename & ENTER & DestinationPath & filename & ENTER & ERR & ESP & Error(ERR), 1)
   Close #1, #2
   Call MouseNorm
   CopyFile = FAIL
   Exit Function
End Function

'---------------------------------------------
'Create the path contained in DestPath$
'First char must be drive letter, followed by
'a ":\" followed by the path, if any.
'---------------------------------------------
Function CreatePath(ByVal DestPath As Variant) As Integer
   Dim BackPos As Integer
   Dim ForePos As Integer
   Dim Temp As String
   Dim S As String
   
   '---------------------------------------------
   'Add slash to end of path if not there already
   '---------------------------------------------
   S = CurDir
   Call AjustarPath(DestPath)
   
   '-----------------------------------
   'Change to the root dir of the drive
   '-----------------------------------
   On Error Resume Next
   ChDrive DestPath
   If (ERR <> 0) Then GoTo ErrorOut
   ChDir SBSlash
   
   '-------------------------------------------------
   'Attempt to make each directory, then change to it
   '-------------------------------------------------
   BackPos = 3
   ForePos = InStr(4, DestPath, SBSlash)
   Do While (ForePos <> 0)
      Temp = Mid$(DestPath, BackPos + 1, ForePos - BackPos - 1)
      ERR = 0
      MkDir Temp
      If ((ERR <> 0) And (ERR <> 75)) Then GoTo ErrorOut
      ERR = 0
      ChDir Temp
      If (ERR <> 0) Then GoTo ErrorOut
      BackPos = ForePos
      ForePos = InStr(BackPos + 1, DestPath, SBSlash)
   Loop
   ChDrive S
   ChDir S
   CreatePath = SUCCEED
   Exit Function
      
ErrorOut:
   MsgBox "Error creando el directorio " & DestPath
   ChDrive S
   ChDir S
   CreatePath = FAIL
   Exit Function
   
End Function

Function FileErrors(ErrVal As Integer) As Integer
   ' Return Value  Meaning     Return Value    Meaning
   '--------------|-----------|---------------|-------------------|
   ' 0             Resume      2               Unrecoverable error
   ' 1             Resume Next 3               Unrecognized error
   Dim Response As Integer
'   Dim Action As Integer       'DEPURACION DE CODIGO
   Dim Msg As String

   Select Case ErrVal
      Case Err_DeviceUnavailable  ' Error #68
         Msg = "El dispositivo no est� disponible."
      Case Err_DiskNotReady       ' Error #71
         Msg = "La unidad de diskette no est� lista."
      Case Err_DeviceIO
         Msg = "El disco est� lleno."
      Case Err_BadFileName, Err_BadFileNameOrNumber   ' Errors #64 & 52
         Msg = "Nombre de archivo inv�lido."
      Case Err_PathDoesNotExist                       ' Error #76
         Msg = "El path no existe."
      Case Err_BadFileMode                            ' Error #54
         Msg = "Can't open your file for that type of access."
      Case Err_FileAlreadyOpen                        ' Error #55
         Msg = "El archivo ya est� abierto."
      Case Err_InputPastEndOfFile                     ' Error #62
      Msg = "This file has a nonstandard end-of-file marker,"
      Msg = Msg & "or an attempt was made to read beyond "
      Msg = Msg & "the end-of-file marker."
      Case Else
         FileErrors = 3
         Call Mensaje1(ERR.Description, 1)
         Exit Function
      End Select
      Response = MsgBox(Msg, MB_EXCLAIM, ProgName)
      Select Case Response
         Case 4          ' Retry button.
            FileErrors = 0
         Case 5          ' Ignore button.
            FileErrors = 1
         Case 1, 2, 3    ' Ok and Cancel buttons.
            FileErrors = 2
         Case Else
            FileErrors = 3
      End Select
End Function

'False: No existe
'True : Si existe
Function FileExist(ByVal filename As String) As Integer
   Dim FNum As Integer
   
   On Error GoTo FileExistErr

   FNum = FreeFile ' Determina el n�mero del archivo
   Open filename For Input As FNum  ' Abre el archivo.
   Close #FNum
   FileExist = True
   Exit Function

FileExistErr:
   FileExist = (ERR <> 53) And (ERR <> 76)
   Call ConvertErr
   Exit Function
End Function

Function Crear_archivo(ByRef ArPl As CommonDialog, ByRef Archivo As Object, ByRef Arch As Object) As Integer
   
   ArPl.ShowSave
   If ArPl.filename <> "" Then
      Set Archivo = Arch.CreateTextFile(ArPl.filename, True)
   End If

   Crear_archivo = SUCCEED
End Function

'Valores Entrada
'  SQL:        Consulta a ejecutar
'  Separador:  Separador de campos en el archivo plano
'  Formato:    Indicador (0:No 1:Si) Si los campos del archivo plano tienen formato definido en la tabla CAMPO_PLANO
'  Arpl:
'  Archivo:
'  Arch:
'  AutArpl:    Autonumerico del archivo plano
'  Encabezado: Encabezado del archivo plano "Cadena Estatica"
'  Tipo_Arch:  Indica si se esta trabajando con Archivo Plano "valor:0" o con Cabecera Archivo "valor:1"
'  IndSql:     Indica si se realiza mas de una consulta sobre el mismo archivo para que solo cree el archivo una vez
'              los valores son (Nul$:Abre el archvio,  n:Numero de SQl a realizar siempre y cuando sea mas de uno,   valor de n=1,2,3,4,5....
'  NoSql:      Si se realiza mas de una consulta SQl se debe colocar el No de SQL Maximo para cerrar el archivo
'              los valores son (Nul$:Cierra el archvio,  n:Numero de SQl realizados,   valor de n=1,2,3,4,5....
'              si IndSql = NoSql se cierra el archivo

'Valor de Salida
'  Exito /Fallo
Function Archivo_Plano(ByVal SQL As String, ByVal Separador As String, ByVal Formato As Integer, ByRef ArPl As CommonDialog, _
                       ByRef Archivo As Object, ByRef Arch As Object, Optional ByVal AutArpl As Long, _
                       Optional ByVal Encabezado As String, Optional Tipo_Arch As Byte, Optional IndSql As String, Optional NoSQL As String)
                       
Dim RsAdo      As New adodb.Recordset
Dim linea      As String
Dim j          As Long
Dim i          As Long
Dim k          As Integer
Dim arrtmp()   As Variant
Dim cad        As String
Dim CadTmp     As String
Dim IndArch    As Boolean
Dim bandD      As Integer

On Error GoTo CTRLERR
   
   If IndSql = NUL$ Then
      Result = Crear_archivo(ArPl, Archivo, Arch)
      If ArPl.FileTitle = "" Then Call Mensaje1("Debe especificar el nombre del archivo a generar.", 3): Exit Function
      If Result <> FAIL Then IndArch = True 'Archivo Abierto
   Else
      IndArch = True 'Archivo Abierto
   End If
   
   RsAdo.Open SQL, BD(BDCurCon)
   RsAdo.MoveFirst
   linea = NUL$
   
   Select Case Formato
      Case 0:  j = 1
               If Encabezado <> NUL$ Then
                  linea = Encabezado
                  Archivo.Write (linea)
                  j = j + 1: linea = NUL$
               End If
               While Not (RsAdo.EOF)
                  For i = 0 To RsAdo.Fields.Count - 1
                     If Not IsNumeric(Len(RsAdo.Fields(i).Value)) Then
                        cad = ""
                     Else
                        cad = RsAdo.Fields(i).Value
                     End If
                     linea = linea & cad
                     If i <> RsAdo.Fields.Count - 1 Then linea = linea & Separador
                  Next
                  If j <> 1 Then Archivo.Write (Chr(13) + Chr(10))
                  Archivo.Write (linea)
            
                  j = j + 1
                  linea = NUL$
                  RsAdo.MoveNext
               Wend
   
   
      Case 1:  ReDim arrtmp(4, 0)
               If Tipo_Arch = 1 Then
                  Campos = "NU_LONG_CAEN, TX_ALIN_CAEN, TX_CARE_CAEN, TX_SEDE_CAEN, TX_TIPO_CAEN"
                  Desde = "CAMPO_CABECERA"
                  Condi = "NU_AUTO_AREN_CAEN = " & AutArpl
               Else
                  Campos = "NU_LONG_CAPL,TX_ALIN_CAPL,TX_CARE_CAPL,TX_SEDE_CAPL,TX_TIPO_CAPL"
                  Desde = "CAMPO_PLANO"
                  Condi = "NU_AUTO_ARPL_CAPL = " & AutArpl
               End If
               Result = LoadMulData(Desde, Campos, Condi, arrtmp())
               If Result <> FAIL And Encontro Then
                  If IndSql = NUL$ Then
                     j = 1
                  Else
                     j = 2
                  End If
                  If Encabezado <> NUL$ Then
                     linea = Encabezado
                     Archivo.Write (linea)
                     j = j + 1: linea = NUL$
                  End If
                  While Not (RsAdo.EOF)
                     For i = 0 To UBound(arrtmp, 2)
                        If Not IsNumeric(Len(RsAdo.Fields(i).Value)) Then
                           cad = ""
                        Else
                           cad = RsAdo.Fields(i).Value
                        End If
                           
                        'Se adecua el formato de salida si tiene o no seprador de decimales
                        If arrtmp(3, i) = 0 And IsNumeric(cad) And arrtmp(4, i) = 1 Then
                           bandD = 0
                           CadTmp = NUL$
                           For k = 1 To Len(cad)
                              If IsNumeric((Mid(cad, k, 1))) Then
                                 CadTmp = CadTmp & Mid(cad, k, 1)
                              Else
                                 If (Mid(cad, k, 1)) = "." Then bandD = 1
                                 If bandD = 1 And (Len(cad) - k) < 2 Then bandD = 2
                              End If
                           Next
                           If bandD = 0 Then
                              cad = CadTmp & "00"
                           Else
                              If bandD = 1 Then cad = CadTmp
                              If bandD = 2 Then cad = CadTmp & "0"
                           End If
                        End If
                        
                        If arrtmp(1, i) = 0 Then
                           linea = linea & LeftAlign(CInt(arrtmp(0, i)), cad, CStr(arrtmp(2, i)))
                        Else
                           linea = linea & RightAlign(CInt(arrtmp(0, i)), cad, CStr(arrtmp(2, i)))
                        End If
                        
                        If i <> UBound(arrtmp, 2) Then linea = linea & Separador
                     Next
                     
                     If j <> 1 Then Archivo.Write (Chr(13) + Chr(10))
                     Archivo.Write (linea)
               
                     j = j + 1
                     linea = NUL$
                     RsAdo.MoveNext
                  Wend
               End If
      
   End Select
   
   If IndArch = True And IndSql = NoSQL Then Archivo.Close: IndArch = False
   Archivo_Plano = SUCCEED
   RsAdo.Close
   Exit Function
   
CTRLERR:
   Archivo_Plano = FAIL
   If ERR.Number = 0 Then
      Call Mensaje1("El nombre de archivo no es valido", 1)
   Else
      Call Mensaje1(ERR.Number & ": " & ERR.Description, 1)
   End If
   On Error Resume Next
   If IndArch = True Then Archivo.Close: IndArch = False
   RsAdo.Close
   
   
End Function

'Alinea a la izquierda
Function LeftAlign(Lng As Integer, cad As String, Optional Char As String) As String
Dim i As Integer
Dim S As String

   If Char = "" Then Char = " "
   If Len(cad) <= Lng Then
   'Devuelve la longitud de cadena o el n�mero de bytes necesarios para almacenar una variable
      S = S & cad
      For i = 1 To (Lng - Len(cad))
         S = S & Char
      Next i
   Else
      S = Left(cad, Lng)
     ' Devuelve un n�mero especificado de caracteres del lado izquierdo de una cadena
   End If
   LeftAlign = S
   End Function

'Alinea a la derecha
Function RightAlign(Lng As Integer, cad As String, Optional Char As String) As String
Dim i As Integer
Dim S As String
   If Char = "" Then Char = " "
   If Len(cad) <= Lng Then
      For i = 1 To (Lng - Len(cad))
         S = S & Char
      Next i
      S = S & cad
   Else
      S = Right(cad, Lng)
   End If
   RightAlign = S
End Function

