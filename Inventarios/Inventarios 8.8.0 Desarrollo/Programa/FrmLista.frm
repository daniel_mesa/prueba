VERSION 5.00
Begin VB.Form FrmListar 
   Appearance      =   0  'Flat
   BackColor       =   &H00E0E0E0&
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Listar"
   ClientHeight    =   3810
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   4455
   Icon            =   "FrmLista.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   3810
   ScaleWidth      =   4455
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin VB.Frame Frame2 
      Caption         =   "&Rangos"
      Height          =   1215
      Left            =   1800
      TabIndex        =   3
      Top             =   240
      Width           =   2535
      Begin VB.TextBox Txt_Final 
         Height          =   285
         Left            =   600
         TabIndex        =   7
         Text            =   "Z"
         Top             =   720
         Width           =   1815
      End
      Begin VB.TextBox Txt_Inicio 
         Height          =   285
         Left            =   600
         TabIndex        =   5
         Text            =   "0"
         Top             =   360
         Width           =   1815
      End
      Begin VB.Label Label2 
         Caption         =   "Final"
         Height          =   255
         Left            =   120
         TabIndex        =   6
         Top             =   720
         Width           =   615
      End
      Begin VB.Label Label1 
         Caption         =   "Inicio"
         Height          =   255
         Left            =   120
         TabIndex        =   4
         Top             =   360
         Width           =   735
      End
   End
   Begin VB.Frame Frame3 
      Caption         =   "&Listar por"
      Height          =   1215
      Left            =   120
      TabIndex        =   0
      Top             =   240
      Width           =   1695
      Begin VB.OptionButton Opt_Por 
         Caption         =   "Nombre"
         Height          =   195
         Index           =   1
         Left            =   120
         TabIndex        =   2
         Top             =   720
         Width           =   1335
      End
      Begin VB.OptionButton Opt_Por 
         Caption         =   "C�digo"
         Height          =   255
         Index           =   0
         Left            =   120
         TabIndex        =   1
         Top             =   360
         Value           =   -1  'True
         Width           =   1455
      End
   End
   Begin VB.Frame Frame1 
      Caption         =   "Enviar a"
      Height          =   615
      Left            =   120
      TabIndex        =   8
      Top             =   1440
      Width           =   4215
      Begin VB.OptionButton Opt_Enviar 
         Caption         =   "&Pantalla"
         Height          =   255
         Index           =   0
         Left            =   720
         TabIndex        =   9
         Top             =   240
         Value           =   -1  'True
         Width           =   1455
      End
      Begin VB.OptionButton Opt_Enviar 
         Caption         =   "&Impresora"
         Height          =   195
         Index           =   1
         Left            =   2280
         TabIndex        =   10
         Top             =   240
         Width           =   1455
      End
   End
   Begin VB.ComboBox Cbo_Opciones 
      Height          =   315
      ItemData        =   "FrmLista.frx":058A
      Left            =   120
      List            =   "FrmLista.frx":058C
      TabIndex        =   12
      Text            =   "Cbo_Opciones"
      Top             =   2400
      Width           =   4215
   End
   Begin VB.CommandButton Cmd_Aceptar 
      Caption         =   "&ACEPTAR"
      Default         =   -1  'True
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   6
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   495
      Left            =   840
      Picture         =   "FrmLista.frx":058E
      Style           =   1  'Graphical
      TabIndex        =   13
      Top             =   2880
      Width           =   1215
   End
   Begin VB.CommandButton Cmd_Cancel 
      Caption         =   "&CANCELAR"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   6
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   495
      Left            =   2400
      Picture         =   "FrmLista.frx":08D0
      Style           =   1  'Graphical
      TabIndex        =   14
      Top             =   2880
      Width           =   1335
   End
   Begin VB.Label Seccion 
      Caption         =   "Seccion"
      Height          =   255
      Left            =   960
      TabIndex        =   18
      Top             =   3000
      Width           =   855
   End
   Begin VB.Label Lbl_Codigo 
      Caption         =   "Lbl_Codigo"
      Height          =   255
      Left            =   3360
      TabIndex        =   17
      Top             =   3480
      Width           =   975
   End
   Begin VB.Label Lbl_Nombre 
      Caption         =   "Lbl_Nombre"
      Height          =   255
      Left            =   1680
      TabIndex        =   16
      Top             =   3480
      Width           =   1095
   End
   Begin VB.Label Lbl_Tabla 
      Caption         =   "Lbl_Tabla"
      Height          =   255
      Left            =   360
      TabIndex        =   15
      Top             =   3480
      Width           =   855
   End
   Begin VB.Label Lbl_Opciones 
      Caption         =   "&Opciones"
      Height          =   255
      Left            =   120
      TabIndex        =   11
      Top             =   2160
      Width           =   5415
   End
End
Attribute VB_Name = "FrmListar"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Dim Cod_Opciones() As Variant
Dim Tabla As String
Dim Codigo_Campo As String
Dim Nombre_Campo As String

Private Sub Cmd_Aceptar_Click()

Dim Formula, Ordenar As String

Ordenar = "+" & CorA & Lbl_Tabla & Punto & IIf(Opt_Por(0).Value = True, Lbl_Codigo, Lbl_Nombre) & CorB
If (UCase(Lbl_Tabla) = "ORDENADOR" Or UCase(Lbl_Tabla) = "FORMA_PAGO") And Opt_Por(0).Value = True Then
    Formula = CorA & Lbl_Tabla & Punto & IIf(Opt_Por(0).Value = True, Lbl_Codigo, Lbl_Nombre) & CorB
    Formula = Formula & " >= " & Txt_Inicio
    Formula = Formula & " AND " & CorA & Lbl_Tabla & Punto & IIf(Opt_Por(0).Value = True, Lbl_Codigo, Lbl_Nombre) & CorB
    Formula = Formula & " <= " & Txt_Final
Else
    Formula = CorA & Lbl_Tabla & Punto & IIf(Opt_Por(0).Value = True, Lbl_Codigo, Lbl_Nombre) & CorB
    Formula = Formula & " >= '" & Txt_Inicio & "'"
    Formula = Formula & " AND " & CorA & Lbl_Tabla & Punto & IIf(Opt_Por(0).Value = True, Lbl_Codigo, Lbl_Nombre) & CorB
    Formula = Formula & " <= '" & Txt_Final & "Z" & Comi
End If
Debug.Print Formula
If Cbo_Opciones.Visible = True And Cbo_Opciones.ListIndex > -1 Then
    If Cod_Opciones(Cbo_Opciones.ListIndex) <> "TODOS" Then
        Formula = Formula & " AND " & CorA & Tabla & Punto & Codigo_Campo & CorB
        Formula = Formula & "='" & Cod_Opciones(Cbo_Opciones.ListIndex) & Comi
    End If
End If

Formula = Formula & IIf(Seccion = NUL$, NUL$, " and " & Seccion)
Call Limpiar_CrysListar

MDI_Inventarios.Crys_Listar.SortFields(0) = Ordenar
Call ListarD(MDI_Inventarios.Crys_Listar.ReportFileName, Formula, crptToWindow, MDI_Inventarios.Crys_Listar.WindowTitle, Me, False)
'MDI_Inventarios.Crys_Listar.Formulas(2) = "HORA= 'Hora: " & Format(Now, "hh:mm") & Comi
'MDI_Inventarios.Crys_Listar.SelectionFormula = Formula
'MDI_Inventarios.Crys_Listar.Destination = IIf(Opt_Enviar(0).Value = True, 0, 1)
'On Error GoTo Control_Listar
'MDI_Inventarios.Crys_Listar.GroupSelectionFormula = ""
''Crys_Listar.Connect = conCrys
'MDI_Inventarios.Crys_Listar.SortFields(0) = Ordenar
'MDI_Inventarios.Crys_Listar.Action = 1
'MDI_Inventarios.Crys_Listar.SortFields(0) = NUL$
Exit Sub
Control_Listar:
    Call Mensaje1(ERR.Number & ESP$ & ERR.Description & "  Nombre del Reporte : " & NombRep, 1)
Resume Next
End Sub
Private Sub Cmd_Cancel_Click()
Unload Me
End Sub
Private Sub Form_Activate()
If Cbo_Opciones.Visible = False Then
'APGR M2894 - INICIO
'    Me.Height = 3135
'    Cmd_Aceptar.Top = 2280
'    Cmd_Cancel.Top = 2280
    Me.Height = 3300
    Cmd_Aceptar.Top = 2200
    Cmd_Cancel.Top = 2200
'APGR  M2894 - FIN
    Lbl_Opciones.Visible = False
End If
If (UCase(Lbl_Tabla) = "ORDENADOR" Or UCase(Lbl_Tabla) = "FORMA_PAGO") Then Txt_Final = 999999999 Else Txt_Final = "Z"
End Sub
Private Sub Form_Load()
'Me.Height = 3660 APGR  M2894
Me.Height = 4005 'APGR
Me.Width = 4545
End Sub
Sub Opciones(ByVal TablaOpcion As String, ByVal NombreOpcion As String, ByVal CodigoOpcion As String, ByVal CondiOpcion As String, ByVal OcultarSeccion As String)
Result = loadctrl(TablaOpcion, NombreOpcion, CodigoOpcion, Cbo_Opciones, Cod_Opciones(), CondiOpcion)
If Result <> FAIL Then
    Tabla = TablaOpcion
    Nombre_Campo = NombreOpcion
    Codigo_Campo = CodigoOpcion
    ReDim Preserve Cod_Opciones(UBound(Cod_Opciones()) + 1)
    Cod_Opciones(UBound(Cod_Opciones())) = "TODOS"
    Cbo_Opciones.AddItem "TODOS"
    Seccion.Caption = OcultarSeccion
End If
End Sub
Private Sub Opt_Por_Click(Index As Integer)
If Index = 0 Then
   Txt_Inicio = 0
   If (UCase(Lbl_Tabla) = "ORDENADOR" Or UCase(Lbl_Tabla) = "FORMA_PAGO") Then Txt_Final = 999999999 Else Txt_Final = "Z"
Else
   Txt_Inicio = "A"
   Txt_Final = "Z"
End If
End Sub

