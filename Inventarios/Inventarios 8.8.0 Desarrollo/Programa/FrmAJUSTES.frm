VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "mscomctl.ocx"
Object = "{C932BA88-4374-101B-A56C-00AA003668DC}#1.1#0"; "MSMASK32.OCX"
Begin VB.Form FrmAJUSTES 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Ajustes X Inflaci�n"
   ClientHeight    =   7095
   ClientLeft      =   45
   ClientTop       =   435
   ClientWidth     =   11475
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   7095
   ScaleWidth      =   11475
   Begin VB.Frame frmCONSUMO 
      Height          =   855
      Left            =   480
      TabIndex        =   2
      Top             =   5880
      Visible         =   0   'False
      Width           =   10575
      Begin MSMask.MaskEdBox edbPORAJUSTE 
         Height          =   375
         Left            =   4920
         TabIndex        =   6
         Top             =   360
         Width           =   975
         _ExtentX        =   1720
         _ExtentY        =   661
         _Version        =   393216
         MaxLength       =   5
         PromptChar      =   "_"
      End
      Begin VB.CommandButton cmdCALCULAR 
         Caption         =   "&Calcular"
         Height          =   375
         Left            =   8880
         TabIndex        =   3
         Top             =   360
         Width           =   975
      End
      Begin VB.Label Label4 
         Alignment       =   1  'Right Justify
         Caption         =   "% de ajuste por Inflaci�n:"
         Height          =   375
         Left            =   600
         TabIndex        =   4
         Top             =   360
         Width           =   4155
      End
   End
   Begin MSComctlLib.ImageList imgBotones 
      Left            =   10800
      Top             =   4200
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   20
      ImageHeight     =   14
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   8
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FrmAJUSTES.frx":0000
            Key             =   ""
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FrmAJUSTES.frx":013A
            Key             =   "ASG"
         EndProperty
         BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FrmAJUSTES.frx":0454
            Key             =   "CAN"
         EndProperty
         BeginProperty ListImage4 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FrmAJUSTES.frx":076E
            Key             =   "SAV"
         EndProperty
         BeginProperty ListImage5 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FrmAJUSTES.frx":0A88
            Key             =   "ADD"
         EndProperty
         BeginProperty ListImage6 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FrmAJUSTES.frx":0DA2
            Key             =   "DEL"
         EndProperty
         BeginProperty ListImage7 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FrmAJUSTES.frx":10BC
            Key             =   "CHA"
         EndProperty
         BeginProperty ListImage8 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FrmAJUSTES.frx":13D6
            Key             =   "PRN"
            Object.Tag             =   "PRN"
         EndProperty
      EndProperty
   End
   Begin MSComctlLib.ListView lstArticulos 
      Height          =   5700
      Left            =   480
      TabIndex        =   0
      Top             =   1080
      Width           =   10425
      _ExtentX        =   18389
      _ExtentY        =   10054
      View            =   3
      LabelEdit       =   1
      MultiSelect     =   -1  'True
      LabelWrap       =   -1  'True
      HideSelection   =   0   'False
      AllowReorder    =   -1  'True
      FullRowSelect   =   -1  'True
      GridLines       =   -1  'True
      _Version        =   393217
      ForeColor       =   -2147483640
      BackColor       =   -2147483643
      BorderStyle     =   1
      Appearance      =   1
      NumItems        =   2
      BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Text            =   "Nombre del Art�culo"
         Object.Width           =   7761
      EndProperty
      BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   1
         Text            =   "C�digo"
         Object.Width           =   3881
      EndProperty
   End
   Begin MSComctlLib.Toolbar tlbACCIONES 
      Align           =   1  'Align Top
      Height          =   630
      Left            =   0
      TabIndex        =   1
      Top             =   0
      Width           =   11475
      _ExtentX        =   20241
      _ExtentY        =   1111
      ButtonWidth     =   1535
      ButtonHeight    =   953
      AllowCustomize  =   0   'False
      Appearance      =   1
      _Version        =   393216
      BorderStyle     =   1
   End
   Begin MSComctlLib.ProgressBar pgbREVISA 
      Height          =   375
      Left            =   2880
      TabIndex        =   5
      Top             =   6240
      Visible         =   0   'False
      Width           =   5895
      _ExtentX        =   10398
      _ExtentY        =   661
      _Version        =   393216
      Appearance      =   1
   End
End
Attribute VB_Name = "FrmAJUSTES"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Dim ArrUXB() As Variant
Dim Articulo As UnArticulo
Dim CualBodega As New LaBodega
Dim LaBodega As Long, LaAccion As String
Private vSeCargaron As Boolean
Public OpcCod As String   'Opci�n de seguridad
'NU_AUTO_ARTI, CD_CODI_ARTI, NO_NOMB_ARTI, NU_AUTO_UNVE_ARTI, TX_VENCE_ARTI, TX_PARA_ARTI,
'TX_ENTRA_ARTI, CD_GRUP_ARTI, CD_USOS_ARTI, VL_ULCO_ARTI, VL_COPR_ARTI, CT_EXIS_ARTI, DE_OBSE_ARTI, ID_TIPO_ARTIID_TIPO_ARTI, CD_RIPS_ARTI, NU_INDPYP_ARTI

Private Sub cmdGENERAR_Click()
    Dim CostoPromedio As Single
    If Not CSng(Me.edbPORAJUSTE.Text) > 0 Then Exit Sub
    Me.pgbREVISA.Min = 0
    Me.pgbREVISA.Max = Me.lstArticulos.ListItems.Count
    Me.pgbREVISA.Value = 0
    Me.pgbREVISA.Visible = True
    Dim Uno As MSComctlLib.ListItem
    CostoPromedio = CSng(Uno.ListSubItems("ULTI").Text)
    
    For Each Uno In Me.lstArticulos.ListItems
        Uno.ListSubItems("CSTO").Text = Format(CostoPromedio * (100 + CSng(Me.edbPORAJUSTE.Text)) / 100, "#########.#0")
        Me.pgbREVISA.Value = Me.pgbREVISA.Value + 1
    Next
    
    Me.pgbREVISA.Visible = False
    Me.lstArticulos.Height = 5700
End Sub

Private Sub Form_Load()
    Dim CnTdr As Integer, AutoNumero As Long
    Me.edbPORAJUSTE.Mask = "99.99"
    Call CenterForm(MDI_Inventarios, Me)
    Me.tlbACCIONES.ImageList = Me.imgBotones
    Me.tlbACCIONES.Buttons.Clear
'LIMPIAR,CALCULAR,GUARDAR,CANCELAR
    Me.tlbACCIONES.Buttons.Add , "LIMPIAR", "&Limpiar"
'    Me.tlbACCIONES.Buttons("LIMPIAR").Image = "LMP"
    Me.tlbACCIONES.Buttons("LIMPIAR").Style = tbrDefault
    Me.tlbACCIONES.Buttons.Add , "CALCULAR", "C&alcular"
    Me.tlbACCIONES.Buttons("CALCULAR").Enabled = False
'    Me.tlbACCIONES.Buttons("CALCULAR").Image = "CMP"
    Me.tlbACCIONES.Buttons("CALCULAR").Style = tbrDefault
    Me.tlbACCIONES.Buttons.Add , "GUARDAR", "&Guardar"
'    Me.tlbACCIONES.Buttons("GUARDAR").Image = "GNR"
    Me.tlbACCIONES.Buttons("GUARDAR").Style = tbrDefault
    Me.tlbACCIONES.Buttons("GUARDAR").Enabled = False
    Me.tlbACCIONES.Buttons.Add , "CANCELAR", "&Cancelar"
'    Me.tlbACCIONES.Buttons("CANCELAR").Image = "GNR"
    Me.tlbACCIONES.Buttons("CANCELAR").Style = tbrDefault

    Call PrepararLista
    Me.frmCONSUMO.Visible = False
    
    Campos = "NU_AUTO_ARTI, CD_CODI_ARTI, NO_NOMB_ARTI, NU_AUTO_UNVE_ARTI, TX_PARA_ARTI, " & _
        "DE_DESC_GRUP, DE_DESC_USOS, TX_NOMB_UNVE, CD_GRUP_ARTI, CD_USOS_ARTI, " & _
        "NU_MULT_UNVE, NU_DIVI_UNVE, VL_COPR_ARTI, VL_COPR_ARTI"
    Desde = "ARTICULO, USOS, GRUP_ARTICULO, IN_UNDVENTA"
    Condi = "CD_CODI_GRUP = CD_GRUP_ARTI AND CD_CODI_USOS = CD_USOS_ARTI"
    Condi = Condi & " AND NU_AUTO_UNVE = NU_AUTO_UNVE_ARTI"
    Condi = Condi & " ORDER BY NO_NOMB_ARTI"
    ReDim ArrUXB(13, 0)
    Result = LoadMulData(Desde, Campos, Condi, ArrUXB())
    If Result = FAIL Then Exit Sub
    If Not Encontro Then Exit Sub
    Me.tlbACCIONES.Buttons("CALCULAR").Enabled = True
    Me.pgbREVISA.Min = 0
    Me.pgbREVISA.Max = UBound(ArrUXB, 2) + 1
    Me.pgbREVISA.Value = 0
    Me.pgbREVISA.Visible = True
    
    For CnTdr = 0 To UBound(ArrUXB, 2)
'        Articulo.AutoNumero = ArrUXB(0, CnTdr)
'        Articulo.Codigo = ArrUXB(1, CnTdr)
'        Articulo.Nombre = ArrUXB(2, CnTdr)
'        Articulo.AutoUDConteo = ArrUXB(3, CnTdr)
'        Articulo.NombreGrupo = ArrUXB(5, CnTdr)
'        Articulo.NombreUso = ArrUXB(6, CnTdr)
'        Articulo.NombreUndConteo = ArrUXB(7, CnTdr)
'        Articulo.ParaQueEs = InStr(1, "VC", ArrUXB(4, CnTdr)) - 1
'        Articulo.CodigoGrupo = ArrUXB(8, CnTdr)
'        Articulo.CodigoUso = ArrUXB(9, CnTdr)
'lstArticulos, NOAR, COAR, DOSI, GRUP, USOS, ULTI, CSTO
        AutoNumero = CLng(ArrUXB(0, CnTdr))
        Me.lstArticulos.ListItems.Add , "A" & AutoNumero, Trim(CStr(ArrUXB(2, CnTdr)))
        Me.lstArticulos.ListItems("A" & AutoNumero).Tag = CStr(AutoNumero)
        Me.lstArticulos.ListItems("A" & AutoNumero).ListSubItems.Add , "COAR", Trim(CStr(ArrUXB(1, CnTdr)))
        Me.lstArticulos.ListItems("A" & AutoNumero).ListSubItems.Add , "DOSI", Trim(CStr(ArrUXB(7, CnTdr)))
        Me.lstArticulos.ListItems("A" & AutoNumero).ListSubItems.Add , "GRUP", Trim(CStr(ArrUXB(5, CnTdr)))
        Me.lstArticulos.ListItems("A" & AutoNumero).ListSubItems.Add , "USOS", Trim(CStr(ArrUXB(6, CnTdr)))
        Me.lstArticulos.ListItems("A" & AutoNumero).ListSubItems.Add , "ULTI", Trim(Format(CStr(ArrUXB(13, CnTdr)), "#########.#0"))
        Me.lstArticulos.ListItems("A" & AutoNumero).ListSubItems.Add , "CSTO", Trim(Format(0, "#########.#0"))
        Me.pgbREVISA.Value = CnTdr
    Next
    Me.pgbREVISA.Visible = False
    Me.lstArticulos.Height = 5700
End Sub

Private Sub lstArticulos_ColumnClick(ByVal ColumnHeader As MSComctlLib.ColumnHeader)
    Me.lstArticulos.SortKey = ColumnHeader.Index - 1
    Me.lstArticulos.Sorted = True
End Sub

Private Sub tlbACCIONES_ButtonClick(ByVal Boton As MSComctlLib.Button)
    Dim Uno As MSComctlLib.ListItem
    Select Case Boton.Key
'LIMPIAR,CALCULAR,GUARDAR,CANCELAR
    Case Is = "LIMPIAR"
        Me.tlbACCIONES.Buttons("CALCULAR").Enabled = False
        Me.frmCONSUMO.Visible = False
'        Call PrepararLista
        Me.lstArticulos.Height = 5700
    Case Is = "CALCULAR"
        If Me.frmCONSUMO.Visible Then
            Me.lstArticulos.Height = 5700
            Me.frmCONSUMO.Visible = False
        Else
            Me.lstArticulos.Height = 4700
            Me.frmCONSUMO.Visible = True
        End If
    Case Is = "GUARDAR"
    Case Is = "CANCELAR"
    End Select
End Sub

Private Sub Form_KeyPress(KeyAscii As Integer)
    If KeyAscii = vbKeyReturn Then KeyAscii = vbKeyTab
End Sub

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyReturn Then KeyCode = vbKeyTab
End Sub

Private Sub PrepararLista()
    Me.lstArticulos.ListItems.Clear
    Me.lstArticulos.Width = 10500
    Me.lstArticulos.ColumnHeaders.Clear
'lstArticulos, NOAR, COAR, DOSI, GRUP, USOS, ULTI, CSTO
    Me.lstArticulos.ColumnHeaders.Add , "NOAR", "Nombre del art�culo", 0.4 * Me.lstArticulos.Width
    Me.lstArticulos.ColumnHeaders.Add , "COAR", "C�digo", 0.15 * Me.lstArticulos.Width
    Me.lstArticulos.ColumnHeaders.Add , "DOSI", "Unidad", 0.2 * Me.lstArticulos.Width
    Me.lstArticulos.ColumnHeaders.Add , "GRUP", "Unidad", 0.2 * Me.lstArticulos.Width
    Me.lstArticulos.ColumnHeaders.Add , "USOS", "Unidad", 0.2 * Me.lstArticulos.Width
    Me.lstArticulos.ColumnHeaders.Add , "ULTI", "Costo U/dad", 0.1 * Me.lstArticulos.Width
    Me.lstArticulos.ColumnHeaders("ULTI").Alignment = lvwColumnRight
    Me.lstArticulos.ColumnHeaders.Add , "CSTO", "Costo", 0.1 * Me.lstArticulos.Width
    Me.lstArticulos.ColumnHeaders("CSTO").Alignment = lvwColumnRight
End Sub

