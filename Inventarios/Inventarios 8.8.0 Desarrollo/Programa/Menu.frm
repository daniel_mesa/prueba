VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Begin VB.Form FrmMenu 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Menu"
   ClientHeight    =   6630
   ClientLeft      =   795
   ClientTop       =   1725
   ClientWidth     =   4380
   Icon            =   "Menu.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   6630
   ScaleWidth      =   4380
   Begin MSComctlLib.TreeView Trvw_menu 
      Height          =   6135
      Left            =   0
      TabIndex        =   0
      Top             =   180
      Width           =   4335
      _ExtentX        =   7646
      _ExtentY        =   10821
      _Version        =   393217
      Indentation     =   441
      LabelEdit       =   1
      LineStyle       =   1
      Style           =   6
      FullRowSelect   =   -1  'True
      ImageList       =   "ImageArbol"
      BorderStyle     =   1
      Appearance      =   1
   End
   Begin MSComctlLib.ImageList ImageArbol 
      Left            =   0
      Top             =   0
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   16
      ImageHeight     =   16
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   15
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "Menu.frx":058A
            Key             =   ""
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "Menu.frx":069C
            Key             =   ""
         EndProperty
         BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "Menu.frx":07AE
            Key             =   ""
         EndProperty
         BeginProperty ListImage4 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "Menu.frx":08C0
            Key             =   ""
         EndProperty
         BeginProperty ListImage5 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "Menu.frx":09D2
            Key             =   ""
         EndProperty
         BeginProperty ListImage6 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "Menu.frx":0AE4
            Key             =   ""
         EndProperty
         BeginProperty ListImage7 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "Menu.frx":0BF6
            Key             =   ""
         EndProperty
         BeginProperty ListImage8 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "Menu.frx":0D08
            Key             =   ""
         EndProperty
         BeginProperty ListImage9 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "Menu.frx":0E1A
            Key             =   ""
         EndProperty
         BeginProperty ListImage10 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "Menu.frx":0F2C
            Key             =   ""
         EndProperty
         BeginProperty ListImage11 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "Menu.frx":103E
            Key             =   ""
         EndProperty
         BeginProperty ListImage12 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "Menu.frx":1150
            Key             =   ""
         EndProperty
         BeginProperty ListImage13 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "Menu.frx":1262
            Key             =   ""
         EndProperty
         BeginProperty ListImage14 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "Menu.frx":1374
            Key             =   ""
         EndProperty
         BeginProperty ListImage15 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "Menu.frx":18A6
            Key             =   ""
         EndProperty
      EndProperty
   End
End
Attribute VB_Name = "FrmMenu"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Dim RelTablaA() As Variant
Dim RelTablaB() As Variant
Dim RelOpcion() As Variant
'Arreglo opciones - formularios
Dim ArrForms()    As Variant
Dim FrmPerfiles   As New FrmTabGen
Dim FrmLisPre     As New FrmTabGen
Dim FrmFormaFarmaceutica As New FrmTabGen
Dim FrmCriterios  As New FrmTabGen
Dim FrmPerfXBod   As New FrmMaestroDetalle
Dim FrmDocXBod    As New FrmMaestroDetalle
Dim FrmComXBod    As New FrmMaestroDetalle
Public CodOpc     As String
Public StNomForm As String 'GAVL R3201 24/02/2011
   
Private Sub Form_Load()
   'Me.Width = 3960 'HRR Depuracion
   Me.Width = 4470 'HRR Depuracion
   
   Me.Height = 6630
   Desde = "OPCION, PERMISO, USUARIO"
   Campos = "TX_CODI_OPCI, TX_DESC_OPCI, TX_CODPADR_OPCI"
   Condicion = "NU_AUTO_OPCI = NU_AUTO_OPCI_PERM AND NU_AUTO_PERF_PERM = NU_AUTO_PERF_USUA"
   Condicion = Condicion & " AND NU_AUTO_USUA = " & AutonumUser & " AND TX_CONS_PERM = 'S'"
   Condicion = Condicion & " ORDER BY TX_CODI_OPCI"
   Result = Carga_Arbol(Desde, Campos, Condicion, Trvw_menu)
   'Carga en un arreglo los formularios asociados a
   'las opciones del menu
   Call Carga_Lista_Forms
End Sub

Private Sub Form_Unload(Cancel As Integer)
   Set FrmPerfiles = Nothing
   Set FrmLisPre = Nothing
   Set FrmFormaFarmaceutica = Nothing
   Set FrmCriterios = Nothing
   Set FrmPerfXBod = Nothing
   Set FrmDocXBod = Nothing
End Sub

Private Sub Trvw_menu_DblClick()
   Dim i       As Integer
'   Dim J       As Integer      'DEPURACION DE CODIGO
   Dim Frm     As Form
   
   CodOpc = Mid(Trvw_menu.SelectedItem.Key, 2, Len(Trvw_menu.SelectedItem.Key) - 2)
   
   'Busca el formulario de la opci�n seleccionada
   For i = 0 To UBound(ArrForms, 2)
      If ArrForms(0, i) = CodOpc Then
         opcUsu = ArrForms(0, i)
         If ArrForms(1, i) = "" Then
            '------------------------------------------------------
            'GMS 2008-01-30
            'Debug.Print "No tiene asociado ningun formulario)"
            If opcUsu = Buscar_Opcion("BACKUP BASE DE DATOS") Then
                'Esta opcion solo aplica para SQL
                If MotorBD = "SQL" Then Call BackupDatabase
            Else
                Debug.Print "No tiene asociado ningun formulario)"
            End If
            '------------------------------------------------------
         Else 'FrmCriterios
            'If ArrForms(1, i) = "FrmCriterios" Then
            ArrForms(1, i) = UCase(ArrForms(1, i)) 'GMS 3814
            If ArrForms(1, i) = UCase("FrmCriterios") Then 'GMS M3814
               Call CargarFormaGen(FrmCriterios, "Crit�rios de Compra", CodOpc, "IN_CRITERIO", "TX_CODI_CRIT", "TX_DESC_CRIT", "Perfiles", 10, 30)
               FrmCriterios.Caption = ArrForms(2, i)
            'ElseIf ArrForms(1, i) = "FrmPerfiles" Then
            ElseIf ArrForms(1, i) = UCase("FrmPerfiles") Then 'GMS M3814
               Call CargarFormaGen(FrmPerfiles, "Perfil", CodOpc, "PERFIL", "TX_CODI_PERF", "TX_DESC_PERF", "Perfiles", 10, 50)
               FrmPerfiles.Caption = ArrForms(2, i)
            'ElseIf ArrForms(1, i) = "FrmLisPre" Then
            ElseIf ArrForms(1, i) = UCase("FrmLisPre") Then 'GMS M3814
               Call CargarFormaGen(FrmLisPre, "Listas de Pr�cios", CodOpc, "IN_LISTAPRECIO", "TX_CODI_LIPR", "TX_NOMB_LIPR", "Listas de Precios", 10, 50)
               FrmLisPre.Caption = ArrForms(2, i)
            'ElseIf ArrForms(1, i) = "FrmFormaFarmaceutica" Then
            ElseIf ArrForms(1, i) = UCase("FrmFormaFarmaceutica") Then 'GMS M3814
               Call CargarFormaGen(FrmFormaFarmaceutica, "Forma Farmac�utica", CodOpc, "IN_FORMAFARMACEUTICA", "TX_CODI_FOFA", "TX_NOMB_FOFA", "Formas Farmaceuticas", 16, 50)
               FrmFormaFarmaceutica.Caption = ArrForms(2, i)
            'ElseIf ArrForms(1, i) = "FrmPerfXBod" Then
            ElseIf ArrForms(1, i) = UCase("FrmPerfXBod") Then 'GMS M3814
               ReDim RelTablaA(4)
               ReDim RelTablaB(4)
               ReDim RelOpcion(1)
               RelTablaA = Array("PERFIL", "TX_CODI_PERF", "TX_DESC_PERF", "NU_AUTO_PERF", "", "N")
               RelTablaB = Array("IN_BODEGA", "TX_CODI_BODE", "TX_NOMB_BODE", "NU_AUTO_BODE", "", "N")
               RelOpcion = Array("Perfiles", "Bodegas")
               Call CargarFormaRel(FrmPerfXBod, "Perfiles Por Bodega", RelTablaA, RelTablaB, RelOpcion, 1, "NU_AUTO_PERF_RPEB", "NU_AUTO_BODE_RPEB", "IN_R_PERFIL_BODEGA", "Perfiles", "Bodegas")
            'ElseIf ArrForms(1, i) = "FrmDocXBod" Then
            ElseIf ArrForms(1, i) = UCase("FrmDocXBod") Then 'GMS M3814
               ReDim RelTablaA(4)
               ReDim RelTablaB(4)
               ReDim RelOpcion(1)
               RelTablaA = Array("IN_DOCUMENTO", "TX_CODI_DOCU", "TX_NOMB_DOCU", "NU_AUTO_DOCU", "", "N")
               RelTablaB = Array("IN_BODEGA", "TX_CODI_BODE", "TX_NOMB_BODE", "NU_AUTO_BODE", "", "N")
               RelOpcion = Array("Documentos", "Bodegas")
               Call CargarFormaRel(FrmDocXBod, "Documentos Por Bodega", RelTablaA, RelTablaB, RelOpcion, 1, "NU_AUTO_DOCU_RBODU", "NU_AUTO_BODE_RBODU", "IN_R_BODE_DOCU", "Documentos", "Bodegas")
            'ElseIf ArrForms(1, i) = "FrmComXBod" Then
            ElseIf ArrForms(1, i) = UCase("FrmComXBod") Then 'GMS M3814
               ReDim RelTablaA(4)
               ReDim RelTablaB(4)
               ReDim RelOpcion(1)
               RelTablaA = Array("IN_COMPROBANTE", "TX_CODI_COMP", "TX_NOMB_COMP", "NU_AUTO_COMP", "", "N")
               RelTablaB = Array("IN_BODEGA", "TX_CODI_BODE", "TX_NOMB_BODE", "NU_AUTO_BODE", "", "N")
               RelOpcion = Array("Comprobantes", "Bodegas")
               Call CargarFormaRel(FrmComXBod, "Comprobantes Por Bodega", RelTablaA, RelTablaB, RelOpcion, 1, "NU_AUTO_COMP_RBOCO", "NU_AUTO_BODE_RBOCO", "IN_R_BODE_COMP", "Comprobantes", "Bodegas")
            'ElseIf ArrForms(1, i) = "FrmGENERAL" Then
            ElseIf ArrForms(1, i) = UCase("FrmGENERAL") Then 'GMS M3814
                Set Frm = Forms.Add(ArrForms(1, i))
                Frm.OpcCod = CodOpc
                Frm.AutoTipoDocumento = CLng(ArrForms(3, i))
                Frm.ProcesaEntradaXlotes = True
                Frm.CodigoFormaCredito = 3 'para saber si generar CXC cuando se factura
                Frm.CodigoFormaEfectivo = 0 'Que codigo en forma_pago es efectivo
'                Frm.DefinicionDeGrilla = "Autoart,+0,Marcar,800,," & _
'                    "Codigo,1900," & _
'                    "Nombre,3500," & _
'                    "AutoUndMedida,+0," & _
'                    "Und Medida,1000," & _
'                    "Multiplicador,+0," & _
'                    "Divisor,+0," & _
'                    "Ultimo Costo,+0," & _
'                    "Cantidad,+800," & _
'                    "Sin IVA,+0," & _
'                    "IVA,+0," & _
'                    "Costo Unitario,+1500," & _
'                    "Costo Total,+1500," & _
'                    "Fecha Vencimiento,+0," & _
'                    "Fecha Entrada,+0," & _
'                    "Pertenece A,+0," & _
'                    "Control C/dad,+0," & _
'                    "Auto Doc,+0," & _
'                    "Select,+0"
''//////
                Frm.DefinicionDeGrilla = "Autoart,+0,Marcar,800,Codigo,1900," & _
                    "Lote Producci�n,0,Nombre,3500," & _
                    "AutoUndMedida,+0," & _
                    "Und Medida,1000," & _
                    "Multiplicador,+0," & _
                    "Divisor,+0," & _
                    "Ultimo Costo,+0," & _
                    "Cantidad,+800," & _
                    "Costo Sin IVA,+0," & _
                    "Venta Sin IVA,+0," & _
                    "IVA,+800," & _
                    "Costo Unitario,+1500," & _
                    "Costo Total,+1500," & _
                    "Venta Sin DESC,+0," & _
                    "% Descuento,+0," & _
                    "Valor Descuento,+0," & _
                    "Venta Unitario,+0," & _
                    "Venta Total,+0," & _
                    "Fecha Vencimiento,0," & _
                    "Fecha Entrada,0," & _
                    "Control C/dad,+0," & _
                    "Pertenece A,+0," & _
                    "Select,+0," & _
                    "COD Impuesto,+0,"
                    'HRR Req1553
                    Frm.DefinicionDeGrilla = Frm.DefinicionDeGrilla & _
                    "Control CostSinIva,+0," & _
                    "Control PorceIva,+0," & _
                    "Control CostUnidad,+0"
                    'Req1553

''//////
'Frm.ColAutoArticulo = 0, Frm.Marcar = 1, Frm.ColCodigoArticulo = 2, Frm.LoteProduccion = 3
'Frm.ColNombreArticulo = 4, Frm.ColAutoUnidad = 5, Frm.ColNombreUnidad = 6
'Frm.ColMultiplicador = 7, Frm.ColDivisor = 8, Frm.ColUltimoCosto = 9
'Frm.ColCantidad = 10, Frm.ColCostoSinIVA = 11, Frm.ColVentaSinIVA = 12,
'Frm.ColPorceIVA = 13 OK
'Frm.ColCostoUnidad = 14, Frm.ColCostoTotal = 15 OK
'Frm.ColVentaTotal = 16, Frm.ColDescPorcentual = 17
'Frm.ColDescAbsoluto = 18, Frm.ColVenUndConDes = 19, Frm.ColVenTotConDes = 20, Frm.ColFecVence = 21
'Frm.ColFecEntrada = 22, Frm.ColCantidadFija = 23, Frm.ColAutoDocumento = 24, Frm.Seleccionada = 25
'Frm.ColCodigoImpuesto = 26
                Frm.ColAutoArticulo = 0
                Frm.Marcar = 1
                Frm.ColCodigoArticulo = 2
                Frm.LoteProduccion = 3
                Frm.ColNombreArticulo = 4
                Frm.ColAutoUnidad = 5
                Frm.ColNombreUnidad = 6
                Frm.ColMultiplicador = 7
                Frm.ColDivisor = 8
                Frm.ColUltimoCosto = 9
                Frm.ColCantidad = 10
                Frm.ColCostoSinIVA = 11
                Frm.ColVentaSinIVA = 12
                Frm.ColPorceIVA = 13
                Frm.ColCostoUnidad = 14
                Frm.ColCostoTotal = 15
'                Frm.ColPorceIVAVenta = 16
                Frm.ColVentaTotal = 16
                Frm.ColDescPorcentual = 17
                Frm.ColDescAbsoluto = 18
                Frm.ColVenUndConDes = 19
                Frm.ColVenTotConDes = 20
                Frm.ColFecVence = 21
                Frm.ColFecEntrada = 22
                Frm.ColCantidadFija = 23
                Frm.ColAutoDocumento = 24
                Frm.Seleccionada = 25
                Frm.ColCodigoImpuesto = 26
                Frm.ColCostoSinIvaFija = 27
                Frm.ColPorceIvaFija = 28
                Frm.ColCostoUnidadFija = 29
                
                                

                Frm.Show
'vGriUltimoCosto, vGriCostoSinIVA, vGriColPorceIVA
            'HRR M2998
'            'HRR R1872
'            ElseIf ArrForms(1, i) = "FrmOCPendiente" Then
'               Set Frm = Forms.Add(ArrForms(1, i))
'               Frm.Show 1
'            'HRR R1872
            'HRR M2998
            
            ElseIf ArrForms(1, i) = UCase("FrmAjusteCostoProm") Then 'GMS R2074
                FrmClave.LblOpcion = "1"
                FrmClave.Caption = "Ajuste costo historico" 'CARV M5229
                FrmClave.Show
            ElseIf ArrForms(1, i) = UCase("FrmInfDiariofact") Then 'GMS R2074
                FrmInfDiarioFact.Show
            'DAHV R2395 - INICIO
            ElseIf ArrForms(1, i) = UCase("FrmAjustePeso") Then
                 If (Aplicacion.Interfaz_Contabilidad) Then
                    FrmAjustePeso.Show
                 Else
                    Call Mensaje1("La interface de contabilidad no se encuentra habilitada", 1)
                 End If
            'DAHV R2395 - FIN
            Else
            
              'INICIO GAVL R3201
                  If ArrForms(2, i) = "PRINCIPIO ACTIVO" Then
                        StNomForm = "PRINCIPIO ACTIVO"
                  ElseIf ArrForms(2, i) = "CLASIFICACI�N DE RIESGO" Then
                        StNomForm = "CLASIFICACION DE RIESGO"
                  ElseIf ArrForms(2, i) = "PRESENTACI�N COMERCIAL" Then
                        StNomForm = "PRESENTACION COMERCIAL"
                  End If
                  'FIN  GAVL R3201
            
            
               'Llama los formularios seleccionado en el menu
               If ArrForms(3, i) = "FRM" Then
                  Set Frm = Forms.Add(ArrForms(1, i))
                  'Frm.OpcCod = CodOpc 'HRR R1717
                  Frm.Caption = ArrForms(2, i)
                  Frm.Show
               ElseIf ArrForms(3, i) = "RPT" Then
                  Call ListarD(ArrForms(1, i), NUL$, crptToWindow, ArrForms(0, i), MDI_Inventarios, False)
               End If
            End If
            Debug.Print CodOpc & " --- " & ArrForms(2, i) & " Formulario ----" & ArrForms(1, i)
         End If
         Exit Sub
      End If
   Next i
End Sub

Private Sub Trvw_menu_KeyPress(KeyAscii As Integer)
   If KeyAscii = vbKeyReturn Then Trvw_menu_DblClick
End Sub

Private Sub Carga_Lista_Forms()
   ReDim ArrForms(3, 0)
   
   Desde = "OPCION, FORMULARIO"
   Campos = "TX_CODI_OPCI, TX_FORM_FORM, TX_DESC_OPCI,TX_TIPO_FORM"
   Condicion = "NU_AUTO_FORM = NU_AUTO_FORM_OPCI "
   Condicion = Condicion & " AND TX_FORM_FORM IS NOT NULL"
   Condicion = Condicion & " ORDER BY TX_CODI_OPCI"
   Result = LoadMulData(Desde, Campos, Condicion, ArrForms)
   If Result = FAIL Or Not Encontro Then
      Debug.Print "Lista de Formas cargada"
   End If
End Sub
