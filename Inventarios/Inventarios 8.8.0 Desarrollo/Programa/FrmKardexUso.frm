VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Object = "{C932BA88-4374-101B-A56C-00AA003668DC}#1.1#0"; "MSMASK32.OCX"
Object = "{0BA686C6-F7D3-101A-993E-0000C0EF6F5E}#1.0#0"; "THREED32.OCX"
Begin VB.Form FrmKardexUso 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Kardex por Uso"
   ClientHeight    =   7440
   ClientLeft      =   45
   ClientTop       =   435
   ClientWidth     =   11370
   Icon            =   "FrmKardexUso.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   7440
   ScaleWidth      =   11370
   Begin VB.ComboBox cboTIPO 
      Height          =   315
      Left            =   2760
      TabIndex        =   0
      Text            =   "Combo1"
      Top             =   210
      Width           =   2655
   End
   Begin VB.Frame fraRANGO 
      Height          =   2175
      Left            =   6360
      TabIndex        =   2
      Top             =   4800
      Width           =   4815
      Begin VB.CheckBox chkFCRANGO 
         Caption         =   "Condicionar las fechas del reporte"
         Height          =   315
         Left            =   360
         TabIndex        =   5
         Top             =   240
         Width           =   4095
      End
      Begin VB.OptionButton OptTipo 
         Caption         =   "Detallado"
         Height          =   255
         Index           =   0
         Left            =   120
         TabIndex        =   8
         Top             =   1800
         Width           =   1095
      End
      Begin VB.OptionButton OptTipo 
         Caption         =   "Resumido"
         Height          =   255
         Index           =   1
         Left            =   1320
         TabIndex        =   9
         Top             =   1800
         Width           =   1215
      End
      Begin MSMask.MaskEdBox txtFCDESDE 
         Height          =   315
         Left            =   960
         TabIndex        =   6
         Top             =   600
         Width           =   1215
         _ExtentX        =   2143
         _ExtentY        =   556
         _Version        =   393216
         PromptChar      =   "_"
      End
      Begin MSMask.MaskEdBox txtFCHASTA 
         Height          =   315
         Left            =   3240
         TabIndex        =   7
         Top             =   600
         Width           =   1215
         _ExtentX        =   2143
         _ExtentY        =   556
         _Version        =   393216
         PromptChar      =   "_"
      End
      Begin Threed.SSCommand cmdIMPRIMIR 
         Height          =   735
         Index           =   2
         Left            =   3600
         TabIndex        =   10
         ToolTipText     =   "Imprime el informe de Kardex por uso "
         Top             =   1200
         Width           =   855
         _Version        =   65536
         _ExtentX        =   1508
         _ExtentY        =   1296
         _StockProps     =   78
         Caption         =   "&IMPRIMIR"
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   6
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         RoundedCorners  =   0   'False
         Picture         =   "FrmKardexUso.frx":058A
      End
      Begin VB.Label Label1 
         Alignment       =   1  'Right Justify
         Caption         =   "Hasta:"
         Height          =   255
         Left            =   2400
         TabIndex        =   13
         Top             =   600
         Width           =   735
      End
      Begin VB.Label lblDESDE 
         Alignment       =   1  'Right Justify
         Caption         =   "Desde:"
         Height          =   255
         Left            =   120
         TabIndex        =   12
         Top             =   600
         Width           =   735
      End
      Begin VB.Label Label2 
         Caption         =   "Tipo Informe:"
         Height          =   255
         Left            =   120
         TabIndex        =   11
         Top             =   1440
         Width           =   1095
      End
   End
   Begin VB.CheckBox chkUsos 
      Caption         =   "Todos los Usos"
      Height          =   375
      Left            =   480
      TabIndex        =   1
      Top             =   600
      Width           =   3495
   End
   Begin MSComctlLib.ProgressBar pgbLLEVO 
      Height          =   255
      Left            =   6000
      TabIndex        =   14
      Top             =   225
      Visible         =   0   'False
      Width           =   4455
      _ExtentX        =   7858
      _ExtentY        =   450
      _Version        =   393216
      Appearance      =   1
   End
   Begin MSComctlLib.ImageList imgBotones 
      Left            =   10680
      Top             =   3960
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   20
      ImageHeight     =   14
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   7
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FrmKardexUso.frx":0C54
            Key             =   ""
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FrmKardexUso.frx":0FA6
            Key             =   "ASG"
         EndProperty
         BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FrmKardexUso.frx":1308
            Key             =   "CAN"
         EndProperty
         BeginProperty ListImage4 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FrmKardexUso.frx":165A
            Key             =   "SAV"
         EndProperty
         BeginProperty ListImage5 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FrmKardexUso.frx":1974
            Key             =   "ADD"
         EndProperty
         BeginProperty ListImage6 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FrmKardexUso.frx":1C2E
            Key             =   "DEL"
         EndProperty
         BeginProperty ListImage7 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FrmKardexUso.frx":2070
            Key             =   "CHA"
         EndProperty
      EndProperty
   End
   Begin MSComctlLib.ListView lstBODEGAS 
      Height          =   2175
      Left            =   435
      TabIndex        =   4
      Top             =   4800
      Width           =   5655
      _ExtentX        =   9975
      _ExtentY        =   3836
      View            =   3
      LabelEdit       =   1
      MultiSelect     =   -1  'True
      LabelWrap       =   -1  'True
      HideSelection   =   -1  'True
      FullRowSelect   =   -1  'True
      GridLines       =   -1  'True
      _Version        =   393217
      ForeColor       =   -2147483640
      BackColor       =   -2147483643
      BorderStyle     =   1
      Appearance      =   1
      NumItems        =   0
   End
   Begin MSComctlLib.ListView lstUsos 
      Height          =   3375
      Left            =   435
      TabIndex        =   3
      Top             =   1080
      Width           =   10260
      _ExtentX        =   18098
      _ExtentY        =   5953
      View            =   3
      LabelEdit       =   1
      MultiSelect     =   -1  'True
      LabelWrap       =   -1  'True
      HideSelection   =   0   'False
      AllowReorder    =   -1  'True
      Checkboxes      =   -1  'True
      FullRowSelect   =   -1  'True
      GridLines       =   -1  'True
      _Version        =   393217
      ForeColor       =   -2147483640
      BackColor       =   -2147483643
      BorderStyle     =   1
      Appearance      =   1
      NumItems        =   2
      BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Text            =   "Nombre del Art�culo"
         Object.Width           =   7761
      EndProperty
      BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   1
         Text            =   "C�digo"
         Object.Width           =   3881
      EndProperty
   End
   Begin VB.Label txtTIPO 
      Alignment       =   1  'Right Justify
      Caption         =   "Tipo de Art�culos:"
      Height          =   330
      Left            =   120
      TabIndex        =   15
      Top             =   210
      Width           =   2415
   End
End
Attribute VB_Name = "FrmKardexUso"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Dim ArrUXB() As Variant
'Dim Articulo As UnArticulo  'DEPURACION DE CODIGO
Dim Dueno As New ElTercero
'Dim LaBodega As String, LaAccion As String, vTipo As String * 1
Dim LaAccion As String, vTipo As String * 1     'DEPURACION DE CODIGO
Dim CnTdr As Integer, NumEnCombo As Integer
Public OpcCod        As String   'Opci�n de seguridad

Private Sub cboTIPO_GotFocus()
    NumEnCombo = Me.cboTIPO.ListIndex
End Sub

Private Sub CboTipo_KeyPress(KeyAscii As Integer)
Call Cambiar_Enter(KeyAscii)
End Sub

Private Sub chkfcRANGO_Click()
    Me.txtFCDESDE.Enabled = False
    Me.txtFCHASTA.Enabled = False
    If Not CBool(Me.chkFCRANGO.Value) = True Then Exit Sub
    Me.txtFCDESDE.Enabled = True
    Me.txtFCHASTA.Enabled = True
End Sub

Private Sub chkFCRANGO_KeyPress(KeyAscii As Integer)
Call Cambiar_Enter(KeyAscii)
End Sub

Private Sub chkUsos_KeyPress(KeyAscii As Integer)
Call Cambiar_Enter(KeyAscii)
End Sub

Private Sub chkUsos_LostFocus()

   Dim Renglon As MSComctlLib.ListItem

   If Me.chkUsos.Value = 1 Then
   
      For Each Renglon In Me.lstUsos.ListItems
         Renglon.Selected = True
      Next
      
   Else
      For Each Renglon In Me.lstUsos.ListItems
         If Renglon.Selected Then Renglon.Selected = False
      Next
   End If
   
 
End Sub

Private Sub cmdIMPRIMIR_Click(Index As Integer)
    Dim Renglon As MSComctlLib.ListItem
'    Dim uso As MSComctlLib.ListItem     'DEPURACION DE CODIGO
    Dim strUsos As String, strBODEGAS As String
    Dim strTITRANGO As String, strTITFECHA As String
    Dim FecDesde As Date, FecHasta As Date
    Dim u, b As Double 'contador
    
    
    Dim SQL As String
    'Dim StSql As String 'HRR M4559 Nuevo Fin
    
    FecDesde = CDate(Me.txtFCDESDE.Text): FecHasta = CDate(Me.txtFCHASTA.Text)
    Call Limpiar_CrysListar
        
    If Me.lstUsos.ListItems.Count = 0 Then Exit Sub
        
    For Each Renglon In Me.lstUsos.ListItems
        If Renglon.Selected Then
           strUsos = strUsos & IIf(Len(strUsos) > 0, " OR ", "") & "(USOS.CD_CODI_USOS)='" & Renglon.ListSubItems("COAR").Text & "'"
           u = u + 1
        End If
    Next
    
    For Each Renglon In Me.lstBODEGAS.ListItems
       If Renglon.Selected Then
          strBODEGAS = strBODEGAS & IIf(Len(strBODEGAS) > 0, " OR ", "") & "(IN_BODEGA.TX_CODI_BODE)='" & Renglon.ListSubItems("COD").Text & "'"
          b = b + 1
       End If
    Next
    If Len(strUsos) > 0 Then strUsos = "(" & strUsos & ")"
    If Len(strBODEGAS) > 0 Then strBODEGAS = "(" & strBODEGAS & ")"
    If Len(strUsos) > 0 And Len(strBODEGAS) > 0 Then
        strUsos = strUsos & " AND " & strBODEGAS
    Else
        strUsos = strUsos & strBODEGAS
    End If
    
    strTITRANGO = "TODOS LOS COMPROBANTES"
    strTITFECHA = "A LA FECHA"
    
    If CBool(Me.chkFCRANGO.Value) Then
        FecDesde = CDate(Me.txtFCDESDE.Text): FecHasta = CDate(Me.txtFCHASTA.Text)
        FecDesde = DateAdd("d", -1, FecDesde): FecHasta = DateAdd("d", 1, FecHasta)
        strUsos = strUsos & IIf(Len(strUsos) > 0, " AND ", "")
        If OptTipo(0).Value Then
          strUsos = strUsos & " (IN_KARDEX.FE_FECH_KARD > " & FFechaCon(FecDesde) & ") AND" 'HRR M994
          strUsos = strUsos & " (IN_KARDEX.FE_FECH_KARD < " & FFechaCon(FecHasta) & ")" 'HRR M994
        Else
          strUsos = strUsos & " (IN_KARDEX.FE_FECH_KARD < " & FFechaCon(FecHasta) & ")" 'HRR M994
        End If
        strTITFECHA = "RANGO DE FECHAS: Del  " & Me.txtFCDESDE.Text & "  al  " & Me.txtFCHASTA.Text
    End If
    
    strUsos = strUsos & IIf(Len(strUsos) > 0, " AND ", "") & "(ARTICULO.TX_PARA_ARTI)='" & vTipo & "' AND (TX_AFEC_DOCU)='S'"
    
    MDI_Inventarios.Crys_Listar.Formulas(9) = "RAGFecha='" & strTITFECHA & Comi
    MDI_Inventarios.Crys_Listar.Formulas(10) = "ENTIDAD=" & Comi & Trim(Dueno.Nombre) & ", NIT:" & Trim(Dueno.Nit) & Comi
    
    'HRR M4559 Antes Inicio
    'HRR Issue851
    SQL = "SELECT  NU_AUTO_KARD, TX_CODI_BODE, TX_NOMB_BODE, NU_AUTO_ARTI_KARD, CD_USOS_ARTI, DE_DESC_USOS, CD_CODI_ARTI, "
    SQL = SQL & "NO_NOMB_ARTI, DE_DESC_ARTI, FE_FECH_KARD, TX_NOMB_COMP, NU_COMP_ENCA, NU_ENTRAD_KARD, NU_SALIDA_KARD,"
    ' DAHV T6796 - INICIO
    ' SQL = SQL & "NU_COSTO_KARD, NU_MULT_KARD, NU_DIVI_KARD, NU_ACTUNMR_KARD, NU_ACTUDNM_KARD, TX_PARA_ARTI, TX_AFEC_DOCU "
    SQL = SQL & "(CASE WHEN TX_IVADED_KARD = 'N' THEN CAST(NU_COSTO_KARD AS FLOAT) ELSE (CAST(NU_COSTO_KARD AS FLOAT) * 100)/(100+ CAST(NU_IMPU_KARD AS FLOAT)) END) AS NU_COSTO_KARD, "
    SQL = SQL & "NU_MULT_KARD, NU_DIVI_KARD, NU_ACTUNMR_KARD, NU_ACTUDNM_KARD, TX_PARA_ARTI, TX_AFEC_DOCU "
    'DAHV T6796 - FIN
    SQL = SQL & "INTO IN_KARDUSOS_TEMP "
    SQL = SQL & "FROM IN_KARDEX, ARTICULO, USOS, IN_BODEGA, IN_ENCABEZADO,"
    SQL = SQL & "IN_COMPROBANTE, IN_DOCUMENTO "
    SQL = SQL & "WHERE (NU_AUTO_ARTI_KARD=NU_AUTO_ARTI) AND "
    SQL = SQL & "(CD_USOS_ARTI=CD_CODI_USOS) AND (NU_AUTO_BODE_KARD=NU_AUTO_BODE) AND "
    SQL = SQL & "(NU_AUTO_ORGCABE_KARD=NU_AUTO_ENCA) AND (NU_AUTO_COMP_ENCA=NU_AUTO_COMP) AND "
    'SQL = SQL & "(NU_AUTO_MODCABE_KARD=NU_AUTO_ENCA) AND "
    SQL = SQL & "(NU_AUTO_DOCU_ENCA=NU_AUTO_DOCU) AND"

    SQL = SQL & "(NU_AUTO_DOCU_COMP=NU_AUTO_DOCU) " ' AND TX_ESTA_ENCA<>'A' AND (NU_AUTO_BODEORG_ENCA=NU_AUTO_BODE) " 'LINEA NUEVA
    'HRR Issues903 TX_ESTA_ENCA<>'A' valida que el documento no este anulado
    'Issue851
    'HRR M4559 Antes Fin
    
    'HRR M4559 Nuevo Inicio
'    SQL = "SELECT  NU_AUTO_KARD, TX_CODI_BODE, TX_NOMB_BODE, NU_AUTO_ARTI_KARD, CD_USOS_ARTI, DE_DESC_USOS, CD_CODI_ARTI, "
'    SQL = SQL & "NO_NOMB_ARTI, DE_DESC_ARTI, FE_FECH_KARD, TX_NOMB_COMP, NU_COMP_ENCA, NU_ENTRAD_KARD, NU_SALIDA_KARD,"
'    SQL = SQL & "NU_COSTO_KARD, NU_MULT_KARD, NU_DIVI_KARD, NU_ACTUNMR_KARD, NU_ACTUDNM_KARD, TX_PARA_ARTI, TX_AFEC_DOCU "
'    SQL = SQL & "INTO IN_KARDUSOS_TEMP "
'    StSql = "FROM IN_KARDEX, ARTICULO, USOS, IN_BODEGA, IN_ENCABEZADO,"
'    StSql = StSql & "IN_COMPROBANTE, IN_DOCUMENTO "
'    StSql = StSql & "WHERE (NU_AUTO_ARTI_KARD=NU_AUTO_ARTI) AND "
'    StSql = StSql & "(CD_USOS_ARTI=CD_CODI_USOS) AND (NU_AUTO_BODE_KARD=NU_AUTO_BODE) AND "
'    StSql = StSql & "(NU_AUTO_ORGCABE_KARD=NU_AUTO_ENCA) AND (NU_AUTO_COMP_ENCA=NU_AUTO_COMP) AND "
'    StSql = StSql & "(NU_AUTO_DOCU_ENCA=NU_AUTO_DOCU) AND"
'    StSql = StSql & "(NU_AUTO_DOCU_COMP=NU_AUTO_DOCU) "
'    SQL = SQL & StSql
    'HRR M4559 Fin
    
    If ExisteTABLA("IN_KARDUSOS_TEMP") Then Result = EliminarTabla("IN_KARDUSOS_TEMP")
    Debug.Print strUsos
'    StSql = StSql & " AND " & strUsos 'HRR M4559 Nuevo Fin
    strUsos = SQL & " AND " & strUsos
    'HRR M4559 Nuevo
    'StSql = " AND NU_AUTO_ARTI IN (SELECT T1.AUTOARTI FROM (SELECT MAX(NU_AUTO_KARD) AS AUTOKARD,NU_AUTO_ARTI AS AUTOARTI " & StSql
    'StSql = StSql & " GROUP BY NU_AUTO_ARTI ) AS T1, IN_KARDEX Where NU_AUTO_KARD = T1.AUTOKARD And NU_ACTUNMR_KARD <> 0 ) "
    'strUsos = strUsos & StSql
    'HRR M4559 fin
    Result = ExecSQL(strUsos)
    
    If Me.OptTipo(0).Value = True Then
       Call ListarD("InfKardexUso.rpt", "", crptToWindow, "KARDEX x USO", MDI_Inventarios, True)
    ElseIf Me.OptTipo(1).Value = True Then
       MDI_Inventarios.Crys_Listar.Formulas(11) = "FechDesde='" & Me.txtFCDESDE.Text & Comi 'HRR M1262
       Call ListarD("InfKardexUsoRes.rpt", "", crptToWindow, "KARDEX x USO", MDI_Inventarios, True) 'HRR M994
    End If
    MDI_Inventarios.Crys_Listar.Formulas(9) = ""
    MDI_Inventarios.Crys_Listar.Formulas(10) = ""
    MDI_Inventarios.Crys_Listar.Formulas(11) = ""
    'MDI_Inventarios.Crys_Listar.Formulas(12) = ""  'HRR M1262
    
  
End Sub

Private Sub Form_Load()
'    Call Main
    Call CenterForm(MDI_Inventarios, Me)
    Dueno.IniXNit (Dueno.Nit)
    Me.cboTIPO.Clear
    Me.cboTIPO.AddItem "Para la Venta"
    Me.cboTIPO.AddItem "Para El Consumo"
    NumEnCombo = Me.cboTIPO.ListIndex
    If Len(FormatDateTime(Date, vbShortDate)) = 8 Then
        Me.txtFCDESDE.Mask = "##/##/##"
        Me.txtFCHASTA.Mask = "##/##/##"
    Else
        Me.txtFCDESDE.Mask = "##/##/####"
        Me.txtFCHASTA.Mask = "##/##/####"
    End If
    Me.txtFCDESDE.Text = FormatDateTime(DateAdd("d", -(Day(Date) - 1), Date), vbShortDate)
    Me.txtFCHASTA.Text = FormatDateTime(DateAdd("d", -1, DateAdd("M", 1, DateAdd("d", -(Day(Date) - 1), Date))), vbShortDate)
    Me.chkFCRANGO.Value = False
    Me.txtFCDESDE.Enabled = False
    Me.txtFCHASTA.Enabled = False
    Me.lstBODEGAS.ListItems.Clear
    Me.lstBODEGAS.Checkboxes = False
    Me.lstBODEGAS.MultiSelect = True
    Me.lstBODEGAS.HideSelection = False
    Me.lstBODEGAS.ColumnHeaders.Clear
    Me.lstBODEGAS.ColumnHeaders.Add , "NOM", "Nombre de la Bodega", 0.66 * Me.lstBODEGAS.Width
    Me.lstBODEGAS.ColumnHeaders.Add , "COD", "C�digo", 0.34 * Me.lstBODEGAS.Width
    
    Me.lstUsos.ListItems.Clear
    Me.lstUsos.Checkboxes = False
    Me.lstUsos.MultiSelect = True
    Me.lstUsos.HideSelection = False
    Me.lstUsos.Width = 10500
    Me.lstUsos.ColumnHeaders.Clear
    Me.lstUsos.ColumnHeaders.Add , "NOAR", "Nombre del Uso", 0.4 * Me.lstUsos.Width
    Me.lstUsos.ColumnHeaders.Add , "COAR", "C�digo", 0.15 * Me.lstUsos.Width
    Me.OptTipo(0).Value = True
Fallo:
End Sub


Private Sub cboTIPO_LostFocus()
    If NumEnCombo = Me.cboTIPO.ListIndex Then Exit Sub
    Me.lstBODEGAS.ListItems.Clear
    Me.lstUsos.ListItems.Clear
    Me.pgbLLEVO.Value = 0
    Me.pgbLLEVO.Visible = True
    Select Case Me.cboTIPO.ListIndex
    Case Is = -1
        vTipo = "0"
    Case Is = 0
        vTipo = "V"
    Case Is = 1
        vTipo = "C"
    End Select
    Campos = "NU_AUTO_BODE, NU_AUTO_BODE_BODE, TX_CODI_BODE, TX_NOMB_BODE, " & _
        "CD_CODI_CECO_BODE, TX_VENTA_BODE"
    Desde = "IN_BODEGA"
    Condi = "TX_VENTA_BODE='" & vTipo & "' ORDER BY TX_CODI_BODE"
    ReDim ArrUXB(5, 0)
    Result = LoadMulData(Desde, Campos, Condi, ArrUXB())
    If Result = FAIL Then GoTo Fallo
    On Error GoTo SIGUI
    For CnTdr = 0 To UBound(ArrUXB, 2)
        Me.lstBODEGAS.ListItems.Add , "B" & ArrUXB(2, CnTdr), ArrUXB(3, CnTdr)
        Me.lstBODEGAS.ListItems("B" & ArrUXB(2, CnTdr)).ListSubItems.Add , "COD", ArrUXB(2, CnTdr)
SIGUI:
    Next
    On Error GoTo 0
    Campos = "CD_CODI_USOS, DE_DESC_USOS"
    Condi = "CD_CODI_USOS LIKE '%' ORDER BY CD_CODI_USOS"
    
    Desde = "USOS"
    ReDim ArrUXB(2, 0)
    Result = LoadMulData(Desde, Campos, Condi, ArrUXB())
    If Result = FAIL Then GoTo Fallo
    If Not Encontro Then GoTo NOENC
    Me.pgbLLEVO.Min = 0
    Me.pgbLLEVO.Value = 0
    Me.pgbLLEVO.Max = UBound(ArrUXB, 2) + 1
    
    For CnTdr = 0 To UBound(ArrUXB, 2)
        Me.pgbLLEVO.Value = CnTdr
        Me.lstUsos.ListItems.Add , "G" & ArrUXB(0, CnTdr), Trim(ArrUXB(1, CnTdr))
        Me.lstUsos.ListItems("G" & ArrUXB(0, CnTdr)).Tag = ArrUXB(0, CnTdr)
        
        Me.lstUsos.ListItems("G" & ArrUXB(0, CnTdr)).ListSubItems.Add , "COAR", Trim(ArrUXB(0, CnTdr))
    Next
NOENC:
Fallo:
    Me.pgbLLEVO.Visible = False
End Sub


Private Sub lstBODEGAS_KeyPress(KeyAscii As Integer)
Call Cambiar_Enter(KeyAscii)
End Sub

Private Sub lstUsos_ColumnClick(ByVal ColumnHeader As MSComctlLib.ColumnHeader)
    Me.lstUsos.SortKey = ColumnHeader.Index - 1
    Me.lstUsos.Sorted = True
End Sub


Private Sub lstUsos_KeyPress(KeyAscii As Integer)
Call Cambiar_Enter(KeyAscii)
End Sub


Private Sub OptTipo_KeyPress(Index As Integer, KeyAscii As Integer)
Call Cambiar_Enter(KeyAscii)
End Sub

Private Sub txtFCDESDE_KeyPress(KeyAscii As Integer)
Call Cambiar_Enter(KeyAscii)
End Sub

Private Sub txtfcDESDE_LostFocus()
    If IsDate(Me.txtFCDESDE.Text) Then
       Me.txtFCHASTA.Text = Me.txtFCDESDE.Text
    Else
       Me.txtFCDESDE.Text = FormatDateTime(DateAdd("d", -(Day(Date) - 1), Date), vbShortDate)
       Me.txtFCDESDE.SetFocus
    End If
End Sub

Private Sub txtFCHASTA_KeyPress(KeyAscii As Integer)
Call Cambiar_Enter(KeyAscii)
End Sub

Private Sub txtFCHASTA_LostFocus()
    If Me.txtFCDESDE.Text <> NUL$ Then
       If IsDate(Me.txtFCHASTA.Text) Then
          If DateDiff("d", CDate(Me.txtFCDESDE.Text), CDate(Me.txtFCHASTA)) < 0 Then
             Call Mensaje1("La fecha final debe ser mayor a la inicial.", 3)
             'Me.txtFCHASTA.Text = "//"
             Me.txtFCHASTA.SetFocus
          End If
       Else
          Me.txtFCHASTA.Text = FormatDateTime(DateAdd("d", -1, DateAdd("M", 1, DateAdd("d", -(Day(Date) - 1), Date))), vbShortDate)
          Me.txtFCHASTA.SetFocus
       End If
    Else
       Call Mensaje1("Digite la fecha desde la cual quiere generar el reporte.", 3)
       Me.txtFCDESDE.SetFocus
    End If
End Sub




