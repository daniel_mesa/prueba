VERSION 5.00
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "MSFLXGRD.OCX"
Object = "{0BA686C6-F7D3-101A-993E-0000C0EF6F5E}#1.0#0"; "THREED32.OCX"
Begin VB.Form FrmPresu 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Datos para afectar Presupuesto."
   ClientHeight    =   4725
   ClientLeft      =   1755
   ClientTop       =   2940
   ClientWidth     =   9120
   Icon            =   "FrmPresu.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   4725
   ScaleWidth      =   9120
   Begin Threed.SSCommand CmdSelec 
      Height          =   375
      Left            =   2160
      TabIndex        =   3
      ToolTipText     =   "Selecci�n de Dependencias"
      Top             =   480
      Width           =   615
      _Version        =   65536
      _ExtentX        =   1085
      _ExtentY        =   661
      _StockProps     =   78
      BevelWidth      =   0
      RoundedCorners  =   0   'False
      Outline         =   0   'False
      Picture         =   "FrmPresu.frx":058A
   End
   Begin VB.TextBox TxtDesc 
      BackColor       =   &H00D6FEFE&
      Height          =   285
      Left            =   2880
      MaxLength       =   18
      TabIndex        =   14
      Top             =   600
      Width           =   5895
   End
   Begin VB.TextBox Txtpres 
      BackColor       =   &H00D6FEFE&
      Height          =   285
      Left            =   1200
      MaxLength       =   18
      TabIndex        =   13
      Top             =   600
      Width           =   1095
   End
   Begin VB.ComboBox CmbCeco 
      BackColor       =   &H00D6FEFE&
      Height          =   315
      Left            =   1200
      Style           =   2  'Dropdown List
      TabIndex        =   11
      Top             =   120
      Width           =   3375
   End
   Begin VB.CommandButton Combot 
      Caption         =   "&CANCELAR"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   6
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   495
      Index           =   1
      Left            =   5160
      Picture         =   "FrmPresu.frx":0F3C
      Style           =   1  'Graphical
      TabIndex        =   5
      Top             =   4080
      Width           =   1335
   End
   Begin VB.CommandButton Combot 
      Caption         =   "&ACEPTAR"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   6
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   495
      Index           =   0
      Left            =   2400
      Picture         =   "FrmPresu.frx":127E
      Style           =   1  'Graphical
      TabIndex        =   4
      Top             =   4080
      Width           =   1335
   End
   Begin MSFlexGridLib.MSFlexGrid Grid 
      Height          =   2535
      Left            =   120
      TabIndex        =   2
      Top             =   1200
      Width           =   8895
      _ExtentX        =   15690
      _ExtentY        =   4471
      _Version        =   393216
      Cols            =   4
      FixedCols       =   0
      SelectionMode   =   1
      AllowUserResizing=   2
   End
   Begin Threed.SSCommand CmdSelOrd 
      Height          =   375
      Left            =   2760
      TabIndex        =   9
      ToolTipText     =   "Selecci�n de Dependencias"
      Top             =   2160
      Visible         =   0   'False
      Width           =   615
      _Version        =   65536
      _ExtentX        =   1085
      _ExtentY        =   661
      _StockProps     =   78
      BevelWidth      =   0
      RoundedCorners  =   0   'False
      Outline         =   0   'False
      Picture         =   "FrmPresu.frx":15C0
   End
   Begin VB.Label LblDocPpto 
      Caption         =   "&Documento "
      Height          =   255
      Index           =   2
      Left            =   120
      TabIndex        =   12
      Top             =   600
      Width           =   975
   End
   Begin VB.Label Cambia 
      Caption         =   "N"
      Height          =   255
      Left            =   240
      TabIndex        =   10
      Top             =   4320
      Visible         =   0   'False
      Width           =   735
   End
   Begin VB.Label Label1 
      Caption         =   "El total debe ser igual al valor del documento."
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   -1  'True
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000001&
      Height          =   255
      Left            =   120
      TabIndex        =   8
      Top             =   3720
      Visible         =   0   'False
      Width           =   3375
   End
   Begin VB.Label Labcxc 
      Alignment       =   1  'Right Justify
      Caption         =   "El valor del documento es "
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   -1  'True
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000001&
      Height          =   255
      Left            =   4080
      TabIndex        =   7
      Top             =   960
      Visible         =   0   'False
      Width           =   4935
   End
   Begin VB.Label LblTOT 
      Alignment       =   1  'Right Justify
      Caption         =   "Total: "
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   -1  'True
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000001&
      Height          =   255
      Left            =   4440
      TabIndex        =   6
      Top             =   3720
      Visible         =   0   'False
      Width           =   4575
   End
   Begin VB.Label LblCodDep 
      Height          =   255
      Index           =   1
      Left            =   120
      TabIndex        =   1
      Top             =   960
      Width           =   2655
   End
   Begin VB.Label LblCodDep 
      Caption         =   "&Dependencia : "
      Height          =   255
      Index           =   0
      Left            =   120
      TabIndex        =   0
      Top             =   120
      Width           =   1095
   End
End
Attribute VB_Name = "FrmPresu"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Dim Valrow As Double, ValReg As Double
Dim CodCeco() As Variant, Datos() As Variant
'Dim Depesel As String
Dim FechaP As String, Mens As String, TipoDcmto As String
Dim QueHacer As Integer '0-Nada / 1-cargar
Dim NoMostrar As Boolean
Private Frmllama    As VB.Form

Public Property Let Desde_Forma(vData As VB.Form)
   Set Frmllama = vData
End Property



Private Sub CmbCeco_KeyPress(KeyAscii As Integer)
    Call Cambiar_Enter(KeyAscii)
End Sub

Private Sub CmbCeco_LostFocus()
  If CmbCeco.ListCount > 0 And CmbCeco.ListIndex = -1 Then
     Call Mensaje1("Debe seleccionar una dependencia", 1)
     Exit Sub
  End If
  Depesel = CodCeco(CmbCeco.ListIndex)
End Sub

Private Sub CmdSelec_Click()
'Dim codigo As String       'DEPURACION DE CODIGO
  If ValReg = 0 Then Call Mensaje1("No existe saldo ", 3): Exit Sub
  Select Case TipoDcmto
    Case Is = ODCompra
        Campos = " NU_CONS_CERT, (VL_VALO_CERT - VL_COMP_CERT - VL_NOUS_CERT), FE_FECH_CERT,  FE_EXPI_CERT"
        Condicion = " (ID_ESTA_CERT='1') AND (" _
                    & "(VL_VALO_CERT-(VL_COMP_CERT + VL_NOUS_CERT))>=" & ValReg & ")" _
                    & " AND FE_FECH_CERT <= " & FFechaCon(FechaP) & " AND FE_EXPI_CERT >= " & FFechaCon(FechaP)
                   
                    
        If CmbCeco.ListIndex <> -1 Then
           Depesel = CodCeco(CmbCeco.ListIndex)
           Condicion = Condicion & " AND CD_DEPE_CERT=" & Comi & Depesel & Comi
        End If
        Desde = "CERTIFICADO"
        Txtpres = SelDocumento(Desde, campoOrd, Campos, "CDP DISPONIBLES", NUL$, CmbCeco.Text, Condicion)
        If Result = FAIL Then Call Mensaje1("Error cargando Datos�s", 1): Exit Sub
'    Case Is = Entrada
    Case Is = Entrada, DevolucionEntrada    'REOL M2290
        Campos = " NU_CONS_REGI, VL_VALO_REGI-(VL_COMP_REGI + VL_NOUS_REGI), FE_FECH_REGI, DE_DESC_REGI "
        Condicion = "(CD_TERC_REGI=" & Comi & Frmllama.txtDocum(5) & Comi & ") and (ID_ESTA_REGI='1') and (" _
                 & "(VL_VALO_REGI-(VL_COMP_REGI + VL_NOUS_REGI))>=" & CDbl(ValReg) & ")" _
                  & " AND FE_FECH_REGI <= " & FFechaCon(FechaP) & _
                   " AND FE_EXPI_REGI >= " & FFechaCon(FechaP) 'GAVL T4444
                  
        If CmbCeco.ListIndex <> -1 Then
           Depesel = CodCeco(CmbCeco.ListIndex)
           Condicion = Condicion & " AND CD_DEPE_REGI=" & Comi & Depesel & Comi
        End If
        Desde = "REGISTRO_PPTO"
        Txtpres = SelDocumento(Desde, campoOrd, Campos, "REGISTROS DISPONIBLES", C, CmbCeco.Text, Condicion)
        If Result = FAIL Then Call Mensaje1("Error cargando Registros", 1): Exit Sub
  End Select
  
  Txtpres_LostFocus
            
  'Result = LoadfGrid(Grid, Tabla, Campos, Condicion)
End Sub

Private Sub Combot_Click(Index As Integer)
Select Case Index
    Case 0: Call Salir
    Case 1: Unload Me
            Result = FAIL 'REOL M2387
End Select
End Sub

Private Sub Form_Load()
   
   Call Titulos_Grid
   Result = loadctrl("CENTRO_COSTO", "NO_NOMB_CECO", "CD_CODI_CECO", CmbCeco, CodCeco(), "CD_CODI_CECO <> 'ZZZZZZZZZZZ' ORDER BY NO_NOMB_CECO ")
   
   If QueHacer = 1 Then
     Me.Txtpres = Frmllama.TxtPpto(0)
     Txtpres_LostFocus
   ElseIf QueHacer = 2 Then
     Me.Txtpres = Frmllama.TxtPpto(0)
     Call VerDatos
   End If

End Sub

Private Sub Titulos_Grid()

  Grid.Rows = 2
  Grid.ColWidth(0) = 1500: Grid.ColWidth(1) = 3800: Grid.ColWidth(2) = 1700: Grid.ColWidth(3) = 1700
  Grid.Row = 0: Grid.Col = 0: Grid.CellAlignment = 4
  Grid.Text = "C�digo": Grid.Col = 1: Grid.CellAlignment = 4
  Grid.Text = "Descripci�n": Grid.Col = 2: Grid.CellAlignment = 4
  Grid.Text = "Valor ": Grid.Col = 3: Grid.CellAlignment = 4
  Select Case TipoDcmto
     Case Is = ODCompra
                Grid.Text = "Valor Orden Compra": Grid.Col = 2: Grid.CellAlignment = 4
     Case Is = Entrada
                Grid.Text = "Valor Entrada": Grid.Col = 2: Grid.CellAlignment = 4
     Case Is = CompraDElementos
                Grid.Text = "Valor Obligaci�n": Grid.Col = 2: Grid.CellAlignment = 4
  End Select
 
  'Grid.Col = 0: Grid.Row = 1
  'Sumval = Vlsaldo
  'LblTOT.Caption = "Total: " & Format(Sumval, "#,###,###,###,##0.##00")
  'Labcxc.Caption = "El valor del documento es " _
                 & Format(Vlsaldo, "#,###,###,###,##0.##00")
  'TxtDep = Depesel: TxtDep_LostFocus
  'If OrdeSel > 0 Then TxtOrdenador = OrdeSel:  TxtOrdenador_LostFocus
End Sub
Private Sub Salir()
Dim FIL As Long
Dim Bande As Boolean 'false si no encuentra el rubro del cdp en los articulos
'verifica que el cdp tenga los mismos rubros que los aticulos
'If ODCompra Then 'REOL M807
'--------------------------------------------------------
'DAHV M4217
    'Variable que detarminara el tama�o del arreglo
    'MatPpto
     Dim InCant As Integer
'DAHV M4217
'--------------------------------------------------------
If TipoDcmto = ODCompra Then  'REOL M807 - M2290
   If Grid.Rows - 1 >= UBound(PptoArti, 2) + 1 Then
      For J = 0 To UBound(PptoArti, 2)
        For I = 1 To Grid.Rows - 1
            If PptoArti(0, J) = Grid.TextMatrix(I, 0) Then
               Bande = True
               Exit For
            Else
               Bande = False
            End If
        Next
        If Bande = False Then Exit For
      Next
   End If
   If Bande = False Then Call Mensaje1("El CDP seleccionado no tiene los mismos rubros de los articulos", 3): Exit Sub
'ElseIf Entrada Then 'REOL M807
ElseIf TipoDcmto = Entrada Or TipoDcmto = DevolucionEntrada Then 'REOL M807 - M2290
   If Grid.Rows - 1 >= UBound(PptoArti, 2) + 1 Then
      For J = 0 To UBound(PptoArti, 2)
        For I = 1 To Grid.Rows - 1
            If PptoArti(0, J) = Grid.TextMatrix(I, 0) Then
               Bande = True
               Exit For
            Else
               Bande = False
            End If
        Next
        If Bande = False Then Exit For
      Next
   End If
   If Bande = False Then Call Mensaje1("El RP seleccionado no tiene los mismos rubros de los articulos", 3): Exit Sub
End If


'PJCA M3212
For FIL = 1 To Grid.Rows - 1
    If Grid.TextMatrix(FIL, 2) = "" Then Grid.TextMatrix(FIL, 2) = 0
    If Grid.TextMatrix(FIL, 3) = "" Then Grid.TextMatrix(FIL, 3) = 0
    'If Val(Grid.TextMatrix(FIL, 3)) > Val(Grid.TextMatrix(FIL, 2)) Then 'HRR M2968
    If Val(Grid.TextMatrix(FIL, 3)) > CDbl(Grid.TextMatrix(FIL, 2)) Then 'HRR M2968
        Call Mensaje1("Los valores del movimiento de Inventario son mayores a los valores del documento Presupuestal.", 3): Exit Sub
    End If
Next
DbTotal = 0
'PJCA M3212

'carga una matirz con los datos del grid
'----------------------------------------------
'DAHV M4217 - Inicio
  InCant = 0
  For FIL = 1 To Grid.Rows - 1
      If Trim(Grid.TextMatrix(FIL, 3)) <> "0" Then InCant = InCant + 1
  Next
'DAHV M4217 - Fin
'---------------------------------------------

'ReDim MatPpto(3, Grid.Rows - 1)
'----------------------------------------------
'DAHV M4217 - Inicio
ReDim MatPpto(3, InCant - 1)
InCant = 0
'DAHV M4217 - Fin
'---------------------------------------------

For FIL = 1 To Grid.Rows - 1
   If Trim(Grid.TextMatrix(FIL, 3)) <> "0" Then
        
'        MatPpto(0, FIL - 1) = Trim(Grid.TextMatrix(FIL, 0))
'        MatPpto(1, FIL - 1) = Trim(Grid.TextMatrix(FIL, 1))
'        MatPpto(2, FIL - 1) = Trim(Grid.TextMatrix(FIL, 2))
'        MatPpto(3, FIL - 1) = Trim(Grid.TextMatrix(FIL, 3))
                
        'dbtotal = dbtotal + MatPpto(3, FIL - 1) 'HRR M2968
        'dbtotal = dbtotal + MatPpto(2, FIL - 1) 'HRR M2968
        
        '----------------------------------------------
        'DAHV M4217 - Inicio
        MatPpto(0, InCant) = Trim(Grid.TextMatrix(FIL, 0))
        MatPpto(1, InCant) = Trim(Grid.TextMatrix(FIL, 1))
        MatPpto(2, InCant) = Trim(Grid.TextMatrix(FIL, 2))
        MatPpto(3, InCant) = Trim(Grid.TextMatrix(FIL, 3))
        DbTotal = DbTotal + MatPpto(2, InCant)
        InCant = InCant + 1
        'DAHV M4217 - Fin
        '---------------------------------------------
   End If
Next
Frmllama.TxtPpto(0) = Me.Txtpres
Frmllama.TxtPpto(1) = Depesel
'Frmllama.TxtPpto(3) = MatPpto(2, 0)
Frmllama.TxtPpto(3) = DbTotal       'PJCA M3212
Unload Me
End Sub

Sub Suma_valores()
Dim FIL As Long
Sumval = 0
For FIL = 1 To (Grid.Rows - 1)
  If Trim(Grid.TextMatrix(FIL, 3)) = NUL$ Then
    Valrow = 0
  Else
    Valrow = CDbl(Trim(Grid.TextMatrix(FIL, 3)))
  End If
  Sumval = Sumval + Valrow
Next
LblTOT.Caption = "Total: " & Format(Sumval, "#,###,###,###,##0.##00")
End Sub

Sub CargarDatos(valor As Double, Fech As String, Forma As String, Accion As Integer, Visible As Boolean)
  ValReg = valor
  FechaP = Fech
  TipoDcmto = Forma
  QueHacer = Accion     '0-Nada / 1 Cargar datos /2 Buscar
  NoMostrar = Visible
End Sub

Private Sub Form_Unload(Cancel As Integer)
    Set Frmllama = Nothing
End Sub

Private Sub Txtpres_LostFocus()
  If Not IsNumeric(Txtpres) Then Txtpres = 0: Exit Sub
  If Txtpres = 0 Or Not Txtpres.Enabled Then Exit Sub
  Select Case TipoDcmto
     Case Is = ODCompra
        If CargarCDP = 0 Then Call Mensaje1("Error cargando el CDP", 1): Exit Sub
'     Case Is = Entrada
'        If CargarRP = 0 Then Call Mensaje1("Error cargando el CDP", 1): Exit Sub
    'REOL M2290
     Case Is = Entrada, DevolucionEntrada
        If CargarRP = 0 Then Call Mensaje1("Error cargando el RP", 1): Exit Sub
    'REOL M2290
  End Select

End Sub

Private Function CargarCDP() As Integer
ReDim Datos(6)
CargarCDP = 1
Condicion = "NU_CONS_CERT=" & CDbl(Txtpres)
Result = LoadData("CERTIFICADO", "CD_DEPE_CERT, FE_FECH_CERT, FE_EXPI_CERT, DE_DESC_CERT, (VL_VALO_CERT - VL_COMP_CERT - VL_NOUS_CERT), ID_ESTA_CERT, CD_ORDE_CERT", Condicion, Datos())
If Result <> FAIL Then
   If Datos(0) = NUL$ Then
      Call Mensaje1("El CDP no se encuentra registrado", 1)
      Txtpres = 0: CargarCDP = 0
      Exit Function
   Else
      If CmbCeco.ListIndex = -1 Then
         CmbCeco.ListIndex = FindInArr(CodCeco, Datos(0))
         Depesel = CodCeco(CmbCeco.ListIndex)
      Else
         If CodCeco(CmbCeco.ListIndex) <> Datos(0) Then
            Call Mensaje1("No coincide la dependencia", 1)
            Txtpres = 0: CargarCDP = 0
            Exit Function
         End If
      End If
      Me.TxtDesc = Datos(3)
      If Datos(5) = "2" Then
         Call Mensaje1("El Datos se encuentra anulado", 1)
         Txtpres = 0: CargarCDP = 0: Exit Function
      End If
      If CDate(FechaP) < CDate(Datos(1)) Then
         Call Mensaje1("La fecha del CDP es posterior a la Orden de Compra: " & Datos(1), 2)
         Txtpres = 0: CargarCDP = 0: Exit Function
      End If
      If CDate(FechaP) > CDate(Datos(2)) Then
         Call Mensaje1("El CDP expir�: " & Datos(2), 2)
         Txtpres = 0: CargarCDP = 0: Exit Function
      End If
      If Datos(4) = NUL$ Then Datos(4) = 0
      If QueHacer = 0 Then
      Datos(4) = Round(Datos(4), 0) 'NYCM M2968
        If CDbl(Datos(4)) < (ValReg) Then
           Call Mensaje1("El CDP no tiene saldo disponible: " & Datos(4), 2)
           Txtpres = 0: CargarCDP = 0: Exit Function
        End If
      End If
      'JAUM T24956 Inicio se crea validacion para comprobar si el ordenador esta desactivado
      If fnDevDato("ORDENADOR", "TX_ESTA_ORDE", "CD_CODI_ORDE=" & OrdeSel, True) = "1" Then
         OrdeSel = Datos(6)
      End If
      'JAUM T24956 Fin
      'OrdeSel = Datos(6) JAUM T24956 comentario
      Depesel = Datos(0)
      Call Leer_Articulos_CDP
      If Result = FAIL Then CargarCDP = 0
   End If
End If
End Function

Private Sub Leer_Articulos_CDP()
   If QueHacer <> 2 Then
      Condicion = " NU_CERT_CEAR = " & CDbl(Txtpres) & " AND CD_ARTI_CEAR = CD_CODI_GAST" & _
                  " AND (VL_CERT_CEAR - VL_COMP_CEAR - VL_NOUS_CEAR) > 0"
      Campos = "CD_ARTI_CEAR, DE_DESC_GAST, (VL_CERT_CEAR - VL_COMP_CEAR - VL_NOUS_CEAR)," & ValReg
   Else
      Condicion = " NU_CERT_CEAR = " & CDbl(Txtpres) & " AND CD_ARTI_CEAR = CD_CODI_GAST"
      Campos = "CD_ARTI_CEAR, DE_DESC_GAST, VL_CERT_CEAR," & ValReg
   End If
   Result = LoadfGrid(Grid, "CDP_ARTICULO, GASTOS", Campos, Condicion)
    
    'PJCA M3212
   For I = 1 To Grid.Rows - 1 Step 1
      J = FindInArrM(PptoArti(), CStr(Grid.TextMatrix(I, 0)))
      If J <> -1 Then
         Grid.TextMatrix(I, 3) = PptoArti(3, J) 'OMOG T17510 SE QUITA EL COMENTARIO
         'INICIO AFMG T17969
         Condicion = Condicion & " AND NU_CERT_REGI= NU_CERT_CEAR"
         If QueHacer = 0 Then
            If Grid.TextMatrix(I, 2) < ValReg Then
               Call Mensaje1("El CDP no tiene saldo disponible: " & Grid.TextMatrix(I, 2), 2)
               Txtpres = 0: Grid.Rows = 1
               Result = FAIL
               Exit Sub
            End If
         End If
         'FIN AFMG T17969
           'SE ESTABLECE EN COMENTARIO YA QUE NO ERA NECESARIO PARA RESOLVER EL CASO
'           'OMOG T17510 INICIO
'           If Not boICxP Then
'              Grid.TextMatrix(i, 3) = PptoArti(3, J)
'           End If
      Else 'OMOG T19547 SE QUITA EL COMENTARIO
         Grid.TextMatrix(I, 3) = 0 'OMOG T19547 SE QUITA EL COMENTARIO
'           'OMOG T17510 FIN
      End If
   Next I
   'PJCA M3212
    
End Sub

Private Function CargarRP() As Integer

'ReDim Datos(7)
ReDim Datos(8) 'GAVL R2470 04/01/2011
CargarRP = 1
   Condicion = "NU_CONS_REGI=" & CLng(Txtpres)
   'Result = LoadData("REGISTRO_PPTO", "NU_CONS_REGI,DE_DESC_REGI, CD_DEPE_REGI, CD_ORDE_REGI, ID_ESTA_REGI, (VL_VALO_REGI-(VL_COMP_REGI+VL_NOUS_REGI)), CD_TERC_REGI, FE_FECH_REGI", Condicion, Datos())
   Result = LoadData("REGISTRO_PPTO", "NU_CONS_REGI,DE_DESC_REGI, CD_DEPE_REGI, CD_ORDE_REGI, ID_ESTA_REGI, (VL_VALO_REGI-(VL_COMP_REGI+VL_NOUS_REGI)), CD_TERC_REGI, FE_FECH_REGI, FE_EXPI_REGI", Condicion, Datos()) 'GAVL R2470 04/01/2011
   Msg = "El Registro"
   If Result <> FAIL Then
     If Encontro Then
       If Datos(4) = "2" Then
          MsgBox "El documento se encuentra anulado", vbInformation
          Txtpres = 0: TxtDesc = NUL$: CargarRP = 0
          Exit Function
       End If
       If Datos(5) = NUL$ Then Datos(5) = 0
       If QueHacer = 0 Then
         If Val(CDbl(Datos(5))) < Val(CDbl(ValReg)) Then
            MsgBox " El documento no tiene saldo disponible", vbInformation
            Txtpres = 0: TxtDesc = NUL$: CargarRP = 0
            Exit Function
         End If
       End If
       If Datos(6) <> Trim(Frmllama.txtDocum(5)) Then
          MsgBox "El documento no pertenece al tercero: " & Trim(Frmllama.txtDocum(5)), vbInformation
          Txtpres = 0: TxtDesc = NUL$: CargarRP = 0
          Exit Function
       End If
       Debug.Print CDate(Datos(7)) & "  " & CDate(FechaP)
       If CDate(Datos(7)) > CDate(FechaP) Then
          MsgBox "La fecha del documento es posterior a la Entrada: " & Trim(Datos(7)), vbInformation
          Txtpres = 0: TxtDesc = NUL$: CargarRP = 0
          Exit Function
       End If
       'INICIO GAVL R2470 04/01/2011
      'If CDate(Datos(8)) >= Format(Nowserver, "dd/mm/yyyy") Then
      'If CDate(FFechaCon(CStr(FechaServer)) >= Datos(8)) Then 'GAVL T4266
      
      'If DateDiff("d", CDate(FechaServer), CDate(Datos(8))) > 0 Then 'GAVL T4444
       If DateDiff("d", CDate(FechaP), CDate(Datos(8))) < 0 Then   'GAVL T4959
          MsgBox "La fecha del registro presupuestal ya caduc�: " & Trim(Datos(8)), vbInformation
          Txtpres = 0: TxtDesc = NUL$: CargarRP = 0
          Exit Function
       End If
        'FIN GAVL R2470
       Txtpres = CStr(Datos(0))
       TxtDesc = CStr(Datos(1))
       OrdeSel = Datos(3)
       Depesel = Datos(2)
       
       Call Leer_Articulos_Registro
       
'       If QueHacer <> 0 Then
          If CmbCeco.ListIndex = -1 Then
            'REOL939
            If FindInArr(CodCeco, Datos(2)) <> -1 Then
                CmbCeco.ListIndex = FindInArr(CodCeco, Datos(2))
                Depesel = CodCeco(CmbCeco.ListIndex)
            Else
                ReDim Datos(0)
                Result = LoadData("PARAMETROS_INVE", "CD_DEPE_APLI", "", Datos())
                CmbCeco.ListIndex = FindInArr(CodCeco, Datos(0))
                Depesel = CodCeco(CmbCeco.ListIndex)
            End If
          End If
'       End If
     Else
       MsgBox "El n�mero de documento no existe .", vbInformation
       Txtpres = 0: TxtDesc = NUL$: CargarRP = 0
     End If
   End If
End Function

Private Sub Leer_Articulos_Registro()
ReDim ArtiR(2, 0)  'articulos del CDP
ReDim MatPpto(3, 0)  'articulos
      
    If QueHacer <> 2 Then
        Condicion = " NU_REGI_REAR = " & CDbl(Txtpres) & " AND CD_ARTI_REAR = CD_CODI_GAST" & _
                " AND (VL_REGI_REAR - VL_COMP_REAR - VL_NOUS_REAR) > 0"
        Campos = "CD_ARTI_REAR, DE_DESC_GAST, (VL_REGI_REAR - VL_COMP_REAR - VL_NOUS_REAR)" & Coma & ValReg
    Else
        Campos = "CD_ARTI_REAR, DE_DESC_GAST, VL_REGI_REAR " & Coma & ValReg
        Condicion = " NU_REGI_REAR = " & CDbl(Txtpres) & " AND CD_ARTI_REAR = CD_CODI_GAST"
    End If
    Desde = "REG_ARTICULO, GASTOS"
    Result = LoadfGrid(Grid, Desde, Campos, Condicion)
    
    'PJCA M3212
    For I = 1 To Grid.Rows - 1 Step 1
        J = FindInArrM(PptoArti(), CStr(Grid.TextMatrix(I, 0)))
        If J <> -1 Then
            Grid.TextMatrix(I, 3) = PptoArti(3, J)
        Else
           Grid.TextMatrix(I, 3) = 0
        End If
    Next I
    'PJCA M3212
    
End Sub

Private Function VerDatos()
  Select Case TipoDcmto
     Case Is = ODCompra
        If CargarCDP = 0 Then Call Mensaje1("Error cargando el CDP", 1): Exit Function
     Case Is = Entrada
        If CargarRP = 0 Then Call Mensaje1("Error cargando el CDP", 1): Exit Function
  End Select
End Function
