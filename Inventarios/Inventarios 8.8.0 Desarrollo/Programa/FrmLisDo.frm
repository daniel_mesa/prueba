VERSION 5.00
Object = "{00025600-0000-0000-C000-000000000046}#5.2#0"; "CRYSTL32.OCX"
Object = "{C932BA88-4374-101B-A56C-00AA003668DC}#1.1#0"; "MSMASK32.OCX"
Object = "{0BA686C6-F7D3-101A-993E-0000C0EF6F5E}#1.0#0"; "THREED32.OCX"
Begin VB.Form FrmLisDo 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Listados de Documentos"
   ClientHeight    =   3615
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   3765
   Icon            =   "FrmLisDo.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   ScaleHeight     =   3615
   ScaleWidth      =   3765
   Begin VB.Frame FraOrden 
      Caption         =   "Ordenar por"
      Height          =   735
      Left            =   0
      TabIndex        =   9
      Top             =   1920
      Width           =   3735
      Begin VB.OptionButton OptOrden 
         Caption         =   "Numero/Factura"
         Height          =   375
         Index           =   2
         Left            =   2400
         TabIndex        =   19
         Top             =   240
         Width           =   975
      End
      Begin VB.OptionButton OptOrden 
         Caption         =   "Nit"
         Height          =   255
         Index           =   1
         Left            =   1440
         TabIndex        =   11
         Top             =   360
         Width           =   615
      End
      Begin VB.OptionButton OptOrden 
         Caption         =   "Fecha"
         Height          =   255
         Index           =   0
         Left            =   240
         TabIndex        =   10
         Top             =   360
         Value           =   -1  'True
         Width           =   855
      End
   End
   Begin VB.Frame FraEsta 
      Caption         =   "Mostrar"
      Height          =   735
      Left            =   0
      TabIndex        =   5
      Top             =   1320
      Width           =   3735
      Begin VB.OptionButton OptEstado 
         Caption         =   "Anulados"
         Height          =   255
         Index           =   1
         Left            =   1560
         TabIndex        =   7
         Top             =   360
         Width           =   1095
      End
      Begin VB.OptionButton OptEstado 
         Caption         =   "No Anulados"
         Height          =   255
         Index           =   0
         Left            =   120
         TabIndex        =   6
         Top             =   360
         Value           =   -1  'True
         Width           =   1335
      End
      Begin VB.OptionButton OptEstado 
         Caption         =   "Todos"
         Height          =   255
         Index           =   2
         Left            =   2760
         TabIndex        =   8
         Top             =   360
         Width           =   855
      End
   End
   Begin VB.Frame FraFecha 
      Caption         =   "Fecha    (dd/mm/aaaa)"
      Height          =   855
      Left            =   0
      TabIndex        =   2
      Top             =   600
      Width           =   3735
      Begin MSMask.MaskEdBox MskFecha 
         Height          =   375
         Index           =   0
         Left            =   720
         TabIndex        =   3
         Top             =   360
         Width           =   1095
         _ExtentX        =   1931
         _ExtentY        =   661
         _Version        =   393216
         MaxLength       =   10
         Mask            =   "##/##/####"
         PromptChar      =   "_"
      End
      Begin MSMask.MaskEdBox MskFecha 
         Height          =   375
         Index           =   1
         Left            =   2520
         TabIndex        =   4
         Top             =   360
         Width           =   1095
         _ExtentX        =   1931
         _ExtentY        =   661
         _Version        =   393216
         MaxLength       =   10
         Mask            =   "##/##/####"
         PromptChar      =   "_"
      End
      Begin VB.Label LblFeIni 
         Caption         =   "Inicial :"
         Height          =   255
         Left            =   120
         TabIndex        =   13
         Top             =   360
         Width           =   495
      End
      Begin VB.Label LblFeFinal 
         Caption         =   "Final :"
         Height          =   255
         Left            =   2040
         TabIndex        =   12
         Top             =   360
         Width           =   495
      End
   End
   Begin Crystal.CrystalReport Crys_Listar 
      Left            =   0
      Top             =   840
      _ExtentX        =   741
      _ExtentY        =   741
      _Version        =   348160
      WindowLeft      =   20
      WindowWidth     =   750
      WindowHeight    =   450
      WindowControlBox=   -1  'True
      WindowMaxButton =   -1  'True
      WindowMinButton =   -1  'True
      WindowControls  =   -1  'True
      PrintFileLinesPerPage=   60
      WindowShowCloseBtn=   -1  'True
      WindowShowSearchBtn=   -1  'True
      WindowShowPrintSetupBtn=   -1  'True
      WindowShowRefreshBtn=   -1  'True
   End
   Begin Threed.SSCommand SCmd_Options 
      Height          =   375
      Index           =   3
      Left            =   120
      TabIndex        =   14
      Top             =   3960
      Visible         =   0   'False
      Width           =   975
      _Version        =   65536
      _ExtentX        =   1720
      _ExtentY        =   661
      _StockProps     =   78
   End
   Begin Threed.SSCommand SCmd_Options 
      Height          =   375
      Index           =   4
      Left            =   1440
      TabIndex        =   15
      Top             =   3960
      Visible         =   0   'False
      Width           =   975
      _Version        =   65536
      _ExtentX        =   1720
      _ExtentY        =   661
      _StockProps     =   78
   End
   Begin Threed.SSCommand SCmd_Options 
      Height          =   735
      Index           =   1
      Left            =   1440
      TabIndex        =   16
      Top             =   2760
      Width           =   855
      _Version        =   65536
      _ExtentX        =   1508
      _ExtentY        =   1296
      _StockProps     =   78
      Caption         =   "&IMPRIMIR"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   6
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Picture         =   "FrmLisDo.frx":058A
   End
   Begin Threed.SSCommand SCmd_Options 
      Height          =   735
      Index           =   2
      Left            =   2520
      TabIndex        =   17
      Top             =   2760
      Width           =   855
      _Version        =   65536
      _ExtentX        =   1508
      _ExtentY        =   1296
      _StockProps     =   78
      Caption         =   "&SALIR"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   6
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Picture         =   "FrmLisDo.frx":0C54
   End
   Begin Threed.SSCommand SCmd_Options 
      Height          =   735
      Index           =   0
      Left            =   360
      TabIndex        =   18
      Top             =   2760
      Width           =   855
      _Version        =   65536
      _ExtentX        =   1508
      _ExtentY        =   1296
      _StockProps     =   78
      Caption         =   "&PANTALLA"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   6
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Picture         =   "FrmLisDo.frx":131E
   End
   Begin VB.Frame FraDocumento 
      Caption         =   "Documento"
      Height          =   735
      Left            =   0
      TabIndex        =   0
      Top             =   0
      Width           =   3735
      Begin VB.ComboBox Cmb_Documento 
         Height          =   315
         ItemData        =   "FrmLisDo.frx":2100
         Left            =   120
         List            =   "FrmLisDo.frx":2131
         TabIndex        =   1
         Top             =   240
         Width           =   3495
      End
   End
End
Attribute VB_Name = "FrmLisDo"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Public OpcCod        As String   'Opci�n de seguridad
Private Sub Form_Load()
    Dim ElClnt As New ElTercero
    ElClnt.IniXNit
 Call CenterForm(MDI_Inventarios, Me)
 Crys_Listar.Formulas(0) = "ENTIDAD='" & ElClnt.Nombre & Comi
 Crys_Listar.Formulas(1) = "NIT= 'NIT. " & ElClnt.Nit & Comi
 Crys_Listar.Formulas(2) = "HORA= 'Hora: " & Format(Now, "hh:mm") & Comi
 Crys_Listar.Formulas(3) = "USUARIO = 'Usuario:" & UserId & Comi
' Crys_Listar.Connect = conCrys
 MskFecha(0) = Hoy
 MskFecha(1) = Hoy
End Sub
Private Sub Cmb_Documento_KeyPress(KeyAscii As Integer)
  Call Cambiar_Enter(KeyAscii)
End Sub
Private Sub MskFecha_GotFocus(Index As Integer)
    MskFecha(Index).SelStart = 0
    MskFecha(Index).SelLength = Len(MskFecha(Index).Text)
End Sub
Private Sub MskFecha_KeyPress(Index As Integer, KeyAscii As Integer)
    Call ValKeyFecha(KeyAscii, MskFecha(Index))
    Call Cambiar_Enter(KeyAscii)
End Sub
Private Sub MskFecha_LostFocus(Index As Integer)
   Call ValFecha(MskFecha(Index), 0)
End Sub
Private Sub OptEstado_KeyPress(Index As Integer, KeyAscii As Integer)
  Call Cambiar_Enter(KeyAscii)
End Sub
Private Sub OptOrden_KeyPress(Index As Integer, KeyAscii As Integer)
  Call Cambiar_Enter(KeyAscii)
End Sub
Private Sub SCmd_Options_Click(Index As Integer)
    Screen.MousePointer = 11
    Select Case Index
        '''True = pantalla
        '''False = impresora
        Case 0: Call Seleccionar_Informe(True)
        Case 1: Call Seleccionar_Informe(False)
        Case 2: Unload Me
    End Select
    Screen.MousePointer = 0
End Sub
Private Sub Seleccionar_Informe(ByVal pantalla As Boolean)
Dim nomrep As String
On Error GoTo MensajeError
  
  Crys_Listar.SelectionFormula = ""
  Select Case Cmb_Documento.ListIndex
    Case -1
           Call Mensaje1("Debe seleccionar un documento", 3)
           Cmb_Documento.SetFocus
           Exit Sub
    Case 0
'           Call Leer_Permisos("03014", SCmd_Options(3), SCmd_Options(3), SCmd_Options(4))
           Crys_Listar.ReportFileName = DirTrab + "ldcoti.rpt"
           nomrep = "ldcoti.rpt"
           If OptOrden(0).Value = True Then
              Crys_Listar.SortFields(0) = "+{cotizacion.fe_fech_coti}"
           ElseIf OptOrden(1).Value = True Then
                  Crys_Listar.SortFields(0) = "+{cotizacion.cd_terc_coti}"
               Else
                  Crys_Listar.SortFields(0) = "+{cotizacion.CD_CODI_COTI}"
           End If
           If Optestado(0).Value = True Then
              Crys_Listar.SelectionFormula = "{cotizacion.id_esta_coti} <> " & "2"
           End If
           If Optestado(1).Value = True Then
              Crys_Listar.SelectionFormula = "{cotizacion.id_esta_coti} = " & "2"
           End If
           If Optestado(2).Value = True Then
              Crys_Listar.SelectionFormula = "{cotizacion.fe_fech_coti}>=date(" & fecha(MskFecha(0)) & ") "
              Crys_Listar.SelectionFormula = Crys_Listar.SelectionFormula & " AND {cotizacion.fe_fech_coti}<=date(" & fecha(MskFecha(1)) & ") "
           Else
              Crys_Listar.SelectionFormula = Crys_Listar.SelectionFormula & " AND {cotizacion.fe_fech_coti}>=date(" & fecha(MskFecha(0)) & ") "
              Crys_Listar.SelectionFormula = Crys_Listar.SelectionFormula & " AND {cotizacion.fe_fech_coti}<=date(" & fecha(MskFecha(1)) & ") "
           End If
    Case 1
'           Call Leer_Permisos("03015", SCmd_Options(3), SCmd_Options(3), SCmd_Options(4))
           Crys_Listar.ReportFileName = DirTrab + "ldorco.rpt"
           nomrep = "ldorco.rpt"
           If OptOrden(0).Value = True Then
              Crys_Listar.SortFields(0) = "+{orden_compra.fe_fech_orco}"
           ElseIf OptOrden(1).Value = True Then
                  Crys_Listar.SortFields(0) = "+{orden_compra.cd_terc_orco}"
               Else
                  Crys_Listar.SortFields(0) = "+{orden_compra.CD_CODI_ORCO}"
           End If
           If Optestado(0).Value = True Then
              Crys_Listar.SelectionFormula = "{orden_compra.id_esta_orco} <> " & "2"
           End If
           If Optestado(1).Value = True Then
              Crys_Listar.SelectionFormula = "{orden_compra.id_esta_orco} = " & "2"
           End If
           If Optestado(2).Value = True Then
              Crys_Listar.SelectionFormula = "{orden_compra.fe_fech_orco}>=date(" & fecha(MskFecha(0)) & ") "
              Crys_Listar.SelectionFormula = Crys_Listar.SelectionFormula & " AND {orden_compra.fe_fech_orco}<=date(" & fecha(MskFecha(1)) & ") "
           Else
              Crys_Listar.SelectionFormula = Crys_Listar.SelectionFormula & " AND {orden_compra.fe_fech_orco}>=date(" & fecha(MskFecha(0)) & ") "
              Crys_Listar.SelectionFormula = Crys_Listar.SelectionFormula & " AND {orden_compra.fe_fech_orco}<=date(" & fecha(MskFecha(1)) & ") "
           End If
    Case 2
'           Call Leer_Permisos("03011", SCmd_Options(3), SCmd_Options(3), SCmd_Options(4))
           Crys_Listar.ReportFileName = DirTrab + "ldentra.rpt"
           nomrep = "ldentra.rpt"
           If OptOrden(0).Value = True Then
              Crys_Listar.SortFields(0) = "+{entrada_almacen.fe_fech_entr}"
           ElseIf OptOrden(1).Value = True Then
                  Crys_Listar.SortFields(0) = "+{entrada_almacen.cd_terc_entr}"
               Else
                  Crys_Listar.SortFields(0) = "+{entrada_almacen.CD_CODI_ENTR}"
           End If
           If Optestado(0).Value = True Then
              Crys_Listar.SelectionFormula = "{entrada_almacen.id_esta_entr} <> " & "2"
           End If
           If Optestado(1).Value = True Then
              Crys_Listar.SelectionFormula = "{entrada_almacen.id_esta_entr} = " & "2"
           End If
           If Optestado(2).Value = True Then
              Crys_Listar.SelectionFormula = "{entrada_almacen.fe_fech_entr}>=date(" & fecha(MskFecha(0)) & ") "
              Crys_Listar.SelectionFormula = Crys_Listar.SelectionFormula & " AND {entrada_almacen.fe_fech_entr}<=date(" & fecha(MskFecha(1)) & ") "
           Else
              Crys_Listar.SelectionFormula = Crys_Listar.SelectionFormula & " AND {entrada_almacen.fe_fech_entr}>=date(" & fecha(MskFecha(0)) & ") "
              Crys_Listar.SelectionFormula = Crys_Listar.SelectionFormula & " AND {entrada_almacen.fe_fech_entr}<=date(" & fecha(MskFecha(1)) & ") "
           End If
    Case 3
'           Call Leer_Permisos("03016", SCmd_Options(3), SCmd_Options(3), SCmd_Options(4))
           Crys_Listar.ReportFileName = DirTrab + "ldcompra.rpt"
           nomrep = "ldcompra.rpt"
           If OptOrden(0).Value = True Then
              Crys_Listar.SortFields(0) = "+{compra.fe_fech_comp}"
           ElseIf OptOrden(1).Value = True Then
                  Crys_Listar.SortFields(0) = "+{compra.cd_terc_comp}"
               Else
                  Crys_Listar.SortFields(0) = "+{compra.CD_CODI_COMP}"
           End If
           If Optestado(0).Value = True Then
              Crys_Listar.SelectionFormula = "{compra.id_esta_comp} <> " & "2"
           End If
           If Optestado(1).Value = True Then
              Crys_Listar.SelectionFormula = "{compra.id_esta_comp} = " & "2"
           End If
           If Optestado(2).Value = True Then
              Crys_Listar.SelectionFormula = "{compra.fe_fech_comp}>=date(" & fecha(MskFecha(0)) & ") "
              Crys_Listar.SelectionFormula = Crys_Listar.SelectionFormula & " AND {compra.fe_fech_comp}<=date(" & fecha(MskFecha(1)) & ") "
           Else
              Crys_Listar.SelectionFormula = Crys_Listar.SelectionFormula & " AND {compra.fe_fech_comp}>=date(" & fecha(MskFecha(0)) & ") "
              Crys_Listar.SelectionFormula = Crys_Listar.SelectionFormula & " AND {compra.fe_fech_comp}<=date(" & fecha(MskFecha(1)) & ") "
           End If
    Case 4
'           Call Leer_Permisos("03017", SCmd_Options(3), SCmd_Options(3), SCmd_Options(4))
           Crys_Listar.ReportFileName = DirTrab + "ldventa.rpt"
           nomrep = "ldventa.rpt"
           If OptOrden(0).Value = True Then
              Crys_Listar.SortFields(0) = "+{venta.fe_fech_vent}"
           ElseIf OptOrden(1).Value = True Then
                  Crys_Listar.SortFields(0) = "+{venta.cd_terc_vent}"
               Else
                  Crys_Listar.SortFields(0) = "+{venta.CD_CODI_VENT}"
           End If
           If Optestado(0).Value = True Then
              Crys_Listar.SelectionFormula = "{venta.id_esta_vent} <> " & "2"
           End If
           If Optestado(1).Value = True Then
              Crys_Listar.SelectionFormula = "{venta.id_esta_vent} = " & "2"
           End If
           If Optestado(2).Value = True Then
              Crys_Listar.SelectionFormula = "{venta.fe_fech_vent}>=date(" & fecha(MskFecha(0)) & ") "
              Crys_Listar.SelectionFormula = Crys_Listar.SelectionFormula & " AND {venta.fe_fech_vent}<=date(" & fecha(MskFecha(1)) & ") "
           Else
              Crys_Listar.SelectionFormula = Crys_Listar.SelectionFormula & " AND {venta.fe_fech_vent}>=date(" & fecha(MskFecha(0)) & ") "
              Crys_Listar.SelectionFormula = Crys_Listar.SelectionFormula & " AND {venta.fe_fech_vent}<=date(" & fecha(MskFecha(1)) & ") "
           End If
           Crys_Listar.Formulas(6) = "MVALANUL ='N'"
    Case 5
'           Call Leer_Permisos("03021", SCmd_Options(3), SCmd_Options(3), SCmd_Options(4))
           Crys_Listar.ReportFileName = DirTrab + "lddevcli.rpt"
           nomrep = "lddevcli.rpt"
           If OptOrden(0).Value = True Then
              Crys_Listar.SortFields(0) = "+{devocli.fe_fech_devo}"
           ElseIf OptOrden(1).Value = True Then
                Crys_Listar.SortFields(0) = "+{devocli.fe_fech_devo}"
              Else
                Crys_Listar.SortFields(0) = "+{devocli.fe_fech_devo}"
           End If
           If Optestado(0).Value = True Then
              Crys_Listar.SelectionFormula = "{devocli.id_esta_devo} <> " & "2"
           End If
           If Optestado(1).Value = True Then
              Crys_Listar.SelectionFormula = "{devocli.id_esta_devo} = " & "2"
           End If
           If Optestado(2).Value = True Then
              Crys_Listar.SelectionFormula = "{devocli.fe_fech_devo}>=date(" & fecha(MskFecha(0)) & ") "
              Crys_Listar.SelectionFormula = Crys_Listar.SelectionFormula & " AND {devocli.fe_fech_devo}<=date(" & fecha(MskFecha(1)) & ") "
           Else
              Crys_Listar.SelectionFormula = Crys_Listar.SelectionFormula & " AND {devocli.fe_fech_devo}>=date(" & fecha(MskFecha(0)) & ") "
              Crys_Listar.SelectionFormula = Crys_Listar.SelectionFormula & " AND {devocli.fe_fech_devo}<=date(" & fecha(MskFecha(1)) & ") "
           End If
    Case 6
'           Call Leer_Permisos("03018", SCmd_Options(3), SCmd_Options(3), SCmd_Options(4))
           Crys_Listar.ReportFileName = DirTrab + "ldrequi.rpt"
           nomrep = "ldrequi.rpt"
           If OptOrden(0).Value = True Then
              Crys_Listar.SortFields(0) = "+{requisicion.fe_fech_requ}"
           ElseIf OptOrden(1).Value = True Then
                  Crys_Listar.SortFields(0) = "+{requisicion.cd_depe_requ}"
               Else
                  Crys_Listar.SortFields(0) = "+{requisicion.cd_codi_requ}"
           End If
           If Optestado(0).Value = True Then
              Crys_Listar.SelectionFormula = "{requisicion.id_esta_requ} <> " & "2"
           End If
           If Optestado(1).Value = True Then
              Crys_Listar.SelectionFormula = "{requisicion.id_esta_requ} = " & "2"
           End If
           If Optestado(2).Value = True Then
              Crys_Listar.SelectionFormula = "{requisicion.fe_fech_requ}>=date(" & fecha(MskFecha(0)) & ") "
              Crys_Listar.SelectionFormula = Crys_Listar.SelectionFormula & " AND {requisicion.fe_fech_requ}<=date(" & fecha(MskFecha(1)) & ") "
           Else
              Crys_Listar.SelectionFormula = Crys_Listar.SelectionFormula & " AND {requisicion.fe_fech_requ}>=date(" & fecha(MskFecha(0)) & ") "
              Crys_Listar.SelectionFormula = Crys_Listar.SelectionFormula & " AND {requisicion.fe_fech_requ}<=date(" & fecha(MskFecha(1)) & ") "
           End If
    Case 7
'           Call Leer_Permisos("03013", SCmd_Options(3), SCmd_Options(3), SCmd_Options(4))
           Crys_Listar.ReportFileName = DirTrab + "lddespa.rpt"
           nomrep = "lddespa.rpt"
           If OptOrden(0).Value = True Then
              Crys_Listar.SortFields(0) = "+{despacho.fe_fech_desp}"
           ElseIf OptOrden(1).Value = True Then
                  Crys_Listar.SortFields(0) = "+{despacho.cd_depe_desp}"
               Else
                  Crys_Listar.SortFields(0) = "+{despacho.cd_codi_desp}"
           End If
           If Optestado(0).Value = True Then
              Crys_Listar.SelectionFormula = "{despacho.id_esta_desp} <> " & "2"
           End If
           If Optestado(1).Value = True Then
              Crys_Listar.SelectionFormula = "{despacho.id_esta_desp} = " & "2"
           End If
           If Optestado(2).Value = True Then
              Crys_Listar.SelectionFormula = "{despacho.fe_fech_desp}>=date(" & fecha(MskFecha(0)) & ") "
              Crys_Listar.SelectionFormula = Crys_Listar.SelectionFormula & " AND {despacho.fe_fech_desp}<=date(" & fecha(MskFecha(1)) & ") "
           Else
              Crys_Listar.SelectionFormula = Crys_Listar.SelectionFormula & " AND {despacho.fe_fech_desp}>=date(" & fecha(MskFecha(0)) & ") "
              Crys_Listar.SelectionFormula = Crys_Listar.SelectionFormula & " AND {despacho.fe_fech_desp}<=date(" & fecha(MskFecha(1)) & ") "
           End If
    Case 8
'           Call Leer_Permisos("03007", SCmd_Options(3), SCmd_Options(3), SCmd_Options(4))
           Crys_Listar.ReportFileName = DirTrab + "ldsaldep.rpt"
           nomrep = "ldsaldep.rpt"
           If OptOrden(0).Value = True Then
              Crys_Listar.SortFields(0) = "+{baja_depe.fe_fech_bajd}"
           ElseIf OptOrden(1).Value = True Then
                  Crys_Listar.SortFields(0) = "+{baja_depe.cd_depe_bajd}"
               Else
                  Crys_Listar.SortFields(0) = "+{baja_depe.cd_codi_bajd}"
           End If
           If Optestado(0).Value = True Then
              Crys_Listar.SelectionFormula = "{baja_depe.id_esta_bajd} <> " & "2"
           End If
           If Optestado(1).Value = True Then
              Crys_Listar.SelectionFormula = "{baja_depe.id_esta_bajd} = " & "2"
           End If
           If Optestado(2).Value = True Then
              Crys_Listar.SelectionFormula = "{baja_depe.fe_fech_bajd}>=date(" & fecha(MskFecha(0)) & ") "
              Crys_Listar.SelectionFormula = Crys_Listar.SelectionFormula & " AND {baja_depe.fe_fech_bajd}<=date(" & fecha(MskFecha(1)) & ") "
           Else
              Crys_Listar.SelectionFormula = Crys_Listar.SelectionFormula & " AND {baja_depe.fe_fech_bajd}>=date(" & fecha(MskFecha(0)) & ") "
              Crys_Listar.SelectionFormula = Crys_Listar.SelectionFormula & " AND {baja_depe.fe_fech_bajd}<=date(" & fecha(MskFecha(1)) & ") "
           End If
    Case 9
'           Call Leer_Permisos("03019", SCmd_Options(3), SCmd_Options(3), SCmd_Options(4))
           Crys_Listar.ReportFileName = DirTrab + "lddevdep.rpt"
           nomrep = "lddevdep.rpt"
           If OptOrden(0).Value = True Then
              Crys_Listar.SortFields(0) = "+{devodep.fe_fech_devd}"
           ElseIf OptOrden(1).Value = True Then
                  Crys_Listar.SortFields(0) = "+{devodep.cd_depe_devd}"
               Else
                  Crys_Listar.SortFields(0) = "+{devodep.cd_codi_devd}"
           End If
           If Optestado(0).Value = True Then
              Crys_Listar.SelectionFormula = "{devodep.id_esta_devd} <> " & "2"
           End If
           If Optestado(1).Value = True Then
              Crys_Listar.SelectionFormula = "{devodep.id_esta_devd} = " & "2"
           End If
           If Optestado(2).Value = True Then
              Crys_Listar.SelectionFormula = "{devodep.fe_fech_devd}>=date(" & fecha(MskFecha(0)) & ") "
              Crys_Listar.SelectionFormula = Crys_Listar.SelectionFormula & " AND {devodep.fe_fech_devd}<=date(" & fecha(MskFecha(1)) & ") "
           Else
              Crys_Listar.SelectionFormula = Crys_Listar.SelectionFormula & " AND {devodep.fe_fech_devd}>=date(" & fecha(MskFecha(0)) & ") "
              Crys_Listar.SelectionFormula = Crys_Listar.SelectionFormula & " AND {devodep.fe_fech_devd}<=date(" & fecha(MskFecha(1)) & ") "
           End If
    Case 10
'           Call Leer_Permisos("03012", SCmd_Options(3), SCmd_Options(3), SCmd_Options(4))
           Crys_Listar.ReportFileName = DirTrab + "ldsalalm.rpt"
           nomrep = "ldsalalm.rpt"
           If OptOrden(0).Value = True Then
              Crys_Listar.SortFields(0) = "+{sali_alma.fe_fech_saal}"
           ElseIf OptOrden(1).Value = True Then
                  Crys_Listar.SortFields(0) = "+{sali_alma.cd_terc_saal}"
               Else
                  Crys_Listar.SortFields(0) = "+{sali_alma.cd_codi_saal}"
           End If
           If Optestado(0).Value = True Then
              Crys_Listar.SelectionFormula = "{sali_alma.id_esta_saal} <> " & "2"
           End If
           If Optestado(1).Value = True Then
              Crys_Listar.SelectionFormula = "{sali_alma.id_esta_saal} = " & "2"
           End If
           If Optestado(2).Value = True Then
              Crys_Listar.SelectionFormula = "{sali_alma.fe_fech_saal}>=date(" & fecha(MskFecha(0)) & ") "
              Crys_Listar.SelectionFormula = Crys_Listar.SelectionFormula & " AND {sali_alma.fe_fech_saal}<=date(" & fecha(MskFecha(1)) & ") "
           Else
              Crys_Listar.SelectionFormula = Crys_Listar.SelectionFormula & " AND {sali_alma.fe_fech_saal}>=date(" & fecha(MskFecha(0)) & ") "
              Crys_Listar.SelectionFormula = Crys_Listar.SelectionFormula & " AND {sali_alma.fe_fech_saal}<=date(" & fecha(MskFecha(1)) & ") "
           End If
    Case 11
'           Call Leer_Permisos("03020", SCmd_Options(3), SCmd_Options(3), SCmd_Options(4))
           Crys_Listar.ReportFileName = DirTrab + "ldtradep.rpt"
           nomrep = "ldtradep.rpt"
           If OptOrden(0).Value = True Then
              Crys_Listar.SortFields(0) = "+{trasdepe.fe_fech_trad}"
           ElseIf OptOrden(1).Value = True Then
                  Crys_Listar.SortFields(0) = "+{trasdepe.cd_desa_trad}"
               Else
                  Crys_Listar.SortFields(1) = "+{trasdepe.cd_CODI_trad}"
           End If
           If Optestado(0).Value = True Then
              Crys_Listar.SelectionFormula = "{trasdepe.id_esta_trad} <> " & "2"
           End If
           If Optestado(1).Value = True Then
              Crys_Listar.SelectionFormula = "{trasdepe.id_esta_trad} = " & "2"
           End If
           If Optestado(2).Value = True Then
              Crys_Listar.SelectionFormula = "{trasdepe.fe_fech_trad}>=date(" & fecha(MskFecha(0)) & ") "
              Crys_Listar.SelectionFormula = Crys_Listar.SelectionFormula & " AND {trasdepe.fe_fech_trad}<=date(" & fecha(MskFecha(1)) & ") "
           Else
              Crys_Listar.SelectionFormula = Crys_Listar.SelectionFormula & " AND {trasdepe.fe_fech_trad}>=date(" & fecha(MskFecha(0)) & ") "
              Crys_Listar.SelectionFormula = Crys_Listar.SelectionFormula & " AND {trasdepe.fe_fech_trad}<=date(" & fecha(MskFecha(1)) & ") "
           End If
           'listar las donaciones
    Case 12
'           Call Leer_Permisos("03011", SCmd_Options(3), SCmd_Options(3), SCmd_Options(4))
           Crys_Listar.ReportFileName = DirTrab + "ldona.rpt"
           nomrep = "ldona.rpt"
           If OptOrden(0).Value = True Then
              Crys_Listar.SortFields(0) = "+{donacion.fe_fech_dona}"
           ElseIf OptOrden(1).Value = True Then
                  Crys_Listar.SortFields(0) = "+{donacion.cd_terc_dona}"
               Else
                  Crys_Listar.SortFields(0) = "+{donacion.CD_CODI_DONA}"
           End If
           If Optestado(0).Value = True Then
              Crys_Listar.SelectionFormula = "{donacion.id_esta_dona} <> " & "2"
           End If
           If Optestado(1).Value = True Then
              Crys_Listar.SelectionFormula = "{donacion.id_esta_dona} = " & "2"
           End If
           If Optestado(2).Value = True Then
              Crys_Listar.SelectionFormula = "{donacion.fe_fech_dona}>=date(" & fecha(MskFecha(0)) & ") "
              Crys_Listar.SelectionFormula = Crys_Listar.SelectionFormula & " AND {donacion.fe_fech_dona}<=date(" & fecha(MskFecha(1)) & ") "
           Else
              Crys_Listar.SelectionFormula = Crys_Listar.SelectionFormula & " AND {donacion.fe_fech_dona}>=date(" & fecha(MskFecha(0)) & ") "
              Crys_Listar.SelectionFormula = Crys_Listar.SelectionFormula & " AND {donacion.fe_fech_dona}<=date(" & fecha(MskFecha(1)) & ") "
           End If
      Case 13
           'listar las devoluciones a dependencia
'           Call Leer_Permisos("03011", SCmd_Options(3), SCmd_Options(3), SCmd_Options(4))
           Crys_Listar.ReportFileName = DirTrab + "lddevolver.rpt"
           nomrep = "lddevolver.rpt"
           If OptOrden(0).Value = True Then
              Crys_Listar.SortFields(0) = "+{devolver.fe_fech_devd}"
           ElseIf OptOrden(1).Value = True Then
                  Crys_Listar.SortFields(0) = "+{devolver.cd_depe_devd}"
               Else
                  Crys_Listar.SortFields(0) = "+{devolver.cd_codi_devd}"
           End If
           If Optestado(0).Value = True Then
              Crys_Listar.SelectionFormula = "{devolver.id_esta_devd} <> " & "2"
           End If
           If Optestado(1).Value = True Then
              Crys_Listar.SelectionFormula = "{devolver.id_esta_devd} = " & "2"
           End If
           If Optestado(2).Value = True Then
              Crys_Listar.SelectionFormula = "{devolver.fe_fech_devd}>=date(" & fecha(MskFecha(0)) & ") "
              Crys_Listar.SelectionFormula = Crys_Listar.SelectionFormula & " AND {devolver.fe_fech_devd}<=date(" & fecha(MskFecha(1)) & ") "
           Else
              Crys_Listar.SelectionFormula = Crys_Listar.SelectionFormula & " AND {devolver.fe_fech_devd}>=date(" & fecha(MskFecha(0)) & ") "
              Crys_Listar.SelectionFormula = Crys_Listar.SelectionFormula & " AND {devolver.fe_fech_devd}<=date(" & fecha(MskFecha(1)) & ") "
           End If
           
      Case 14
           'devolucion a proveedores
           
'           Call Leer_Permisos("03011", SCmd_Options(3), SCmd_Options(3), SCmd_Options(4))
           Crys_Listar.ReportFileName = DirTrab + "ldeventra.rpt"
           nomrep = "ldeventra.rpt"
           If OptOrden(0).Value = True Then
              Crys_Listar.SortFields(0) = "+{devol_proveedor.fe_fech_devo}"
           ElseIf OptOrden(1).Value = True Then
                  Crys_Listar.SortFields(0) = "+{devol_proveedor.cd_terc_entr}"
               Else
                  Crys_Listar.SortFields(0) = "+{devol_proveedor.cd_codi_devo}"
           End If
           If Optestado(0).Value = True Then
              Crys_Listar.SelectionFormula = "{devol_proveedor.id_esta_devo} <> " & "2"
           End If
           If Optestado(1).Value = True Then
              Crys_Listar.SelectionFormula = "{devol_proveedor.id_esta_devo} = " & "2"
           End If
           If Optestado(2).Value = True Then
              Crys_Listar.SelectionFormula = "{devol_proveedor.fe_fech_devo}>=date(" & fecha(MskFecha(0)) & ") "
              Crys_Listar.SelectionFormula = Crys_Listar.SelectionFormula & " AND {devol_proveedor.fe_fech_devo}<=date(" & fecha(MskFecha(1)) & ") "
           Else
              Crys_Listar.SelectionFormula = Crys_Listar.SelectionFormula & " AND {devol_proveedor.fe_fech_devo}>=date(" & fecha(MskFecha(0)) & ") "
              Crys_Listar.SelectionFormula = Crys_Listar.SelectionFormula & " AND {devol_proveedor.fe_fech_devo}<=date(" & fecha(MskFecha(1)) & ") "
           End If
                    
           'FIN
  End Select
  If CDate(MskFecha(0)) > CDate(MskFecha(1)) Then
     Call Mensaje1("Fecha Inicial Mayor que Fecha Final", 3)
     Exit Sub
  End If
  If SCmd_Options(4).Enabled = False Then
     Call Mensaje1("Permiso Denegado", 3)
  Else
     If pantalla = True Then
        Crys_Listar.Destination = crptToWindow
     Else
        Crys_Listar.Destination = crptToPrinter
     End If
     Crys_Listar.WindowTitle = "LISTADO DE " & Cmb_Documento.List(Cmb_Documento.ListIndex)
     Crys_Listar.Formulas(4) = "FECHAI='" & Format(MskFecha(0), "dd/mm/yyyy") & Comi
     Crys_Listar.Formulas(5) = "FECHAF='" & Format(MskFecha(1), "dd/mm/yyyy") & Comi
     Crys_Listar.Action = 1
  End If
Exit Sub

MensajeError:
    Call Mensaje1(ERR.Number & ": " & ERR.Description & "  Nombre del Reporte : " & nomrep, 1)
End Sub

