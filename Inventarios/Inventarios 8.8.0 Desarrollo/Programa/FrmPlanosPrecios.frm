VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Object = "{0BA686C6-F7D3-101A-993E-0000C0EF6F5E}#1.0#0"; "THREED32.OCX"
Begin VB.Form FrmPlanosPrecios 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Plano Precios"
   ClientHeight    =   2445
   ClientLeft      =   4185
   ClientTop       =   2970
   ClientWidth     =   4770
   Icon            =   "FrmPlanosPrecios.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   ScaleHeight     =   2445
   ScaleWidth      =   4770
   Begin MSComctlLib.ProgressBar PrgPlano 
      Height          =   255
      Left            =   120
      TabIndex        =   2
      Top             =   2160
      Width           =   4575
      _ExtentX        =   8070
      _ExtentY        =   450
      _Version        =   393216
      BorderStyle     =   1
      Appearance      =   1
   End
   Begin VB.Frame Frame2 
      Height          =   1095
      Left            =   120
      TabIndex        =   3
      Top             =   720
      Width           =   4575
      Begin Threed.SSCommand SCmd_Options 
         Height          =   735
         Index           =   0
         Left            =   240
         TabIndex        =   4
         Top             =   240
         Width           =   855
         _Version        =   65536
         _ExtentX        =   1508
         _ExtentY        =   1296
         _StockProps     =   78
         Caption         =   "&DESCARGAR"
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   6
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Picture         =   "FrmPlanosPrecios.frx":058A
      End
      Begin Threed.SSCommand SCmd_Options 
         Height          =   735
         Index           =   1
         Left            =   1320
         TabIndex        =   5
         Top             =   240
         Width           =   855
         _Version        =   65536
         _ExtentX        =   1508
         _ExtentY        =   1296
         _StockProps     =   78
         Caption         =   "&CARGAR"
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   6
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Picture         =   "FrmPlanosPrecios.frx":0C54
      End
      Begin Threed.SSCommand SCmd_Options 
         Height          =   735
         Index           =   2
         Left            =   3480
         TabIndex        =   6
         Top             =   240
         Width           =   855
         _Version        =   65536
         _ExtentX        =   1508
         _ExtentY        =   1296
         _StockProps     =   78
         Caption         =   "&SALIR"
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   6
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Picture         =   "FrmPlanosPrecios.frx":131E
      End
      Begin Threed.SSCommand SCmd_Detener 
         Height          =   735
         Left            =   2400
         TabIndex        =   10
         Top             =   240
         Width           =   855
         _Version        =   65536
         _ExtentX        =   1508
         _ExtentY        =   1296
         _StockProps     =   78
         Caption         =   "CANCELAR"
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   6
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Picture         =   "FrmPlanosPrecios.frx":19E8
      End
   End
   Begin VB.ComboBox Cbo_ListaPrecios 
      Height          =   315
      Left            =   120
      TabIndex        =   0
      Top             =   360
      Width           =   4575
   End
   Begin VB.Frame Frame1 
      Caption         =   "Actualizar por"
      Height          =   555
      Left            =   1320
      TabIndex        =   7
      Top             =   1080
      Visible         =   0   'False
      Width           =   915
      Begin VB.OptionButton OptActualizar 
         Caption         =   "C�digo alterno, c�digo principal"
         Height          =   375
         Index           =   1
         Left            =   120
         TabIndex        =   9
         Top             =   540
         Width           =   1575
      End
      Begin VB.OptionButton OptActualizar 
         Caption         =   "C�digo principal"
         Height          =   255
         Index           =   0
         Left            =   120
         TabIndex        =   8
         Top             =   240
         Value           =   -1  'True
         Width           =   1575
      End
   End
   Begin VB.Label Lbl_Errores 
      Caption         =   "0"
      Height          =   255
      Left            =   3720
      TabIndex        =   14
      Top             =   1920
      Width           =   855
   End
   Begin VB.Label Lbl_Procesados 
      Caption         =   "0"
      Height          =   255
      Left            =   1080
      TabIndex        =   13
      Top             =   1920
      Width           =   1815
   End
   Begin VB.Label Label2 
      Caption         =   "Errores:"
      Height          =   255
      Left            =   3120
      TabIndex        =   12
      Top             =   1920
      Width           =   615
   End
   Begin VB.Label Label1 
      Caption         =   "Procesados:"
      Height          =   255
      Left            =   120
      TabIndex        =   11
      Top             =   1920
      Width           =   975
   End
   Begin VB.Label lbl 
      Caption         =   "Lista de Precios"
      Height          =   255
      Left            =   120
      TabIndex        =   1
      Top             =   120
      Width           =   1335
   End
End
Attribute VB_Name = "FrmPlanosPrecios"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Dim texto As String
Dim NumReg As Long
Dim BandErr, BandStop As Boolean
Dim ArrTarifa(), CodTarifaBase() As Variant
Dim Bandera As String
'Dim erroresCargue As Integer   'DEPURACION DE CODIGO

Private Sub Cbo_ListaPrecios_Change()
BandProcLote = 0
BandStop = False
End Sub

Private Sub Form_Load()
Call CenterForm(MDI_Inventarios, Me)
Result = LNC("IN_LISTAPRECIO", "NU_AUTO_LIPR", "TX_NOMB_LIPR", Cbo_ListaPrecios, NUL$)
'Result = loadctrl("TARIFA_BASE order by NO_NOMB_TARB", "NO_NOMB_TARB", "CD_CODI_TARB", CboTarifaBase, CodTarifaBase(), NUL$)
BandProcLote = 0
BandStop = False
End Sub

Private Sub SCmd_Detener_Click()
If BandProcLote Then
   If WarnMsg("�Esta seguro que desea detener el proceso?") Then
      BandStop = True
   End If
End If
End Sub

Private Sub SCmd_Options_Click(Index As Integer)
If BandProcLote = 1 Then Exit Sub
Select Case Index
   Case 0: Call DescargarTarifa
   Case 1: Call CargarTarifas
   Case 2: Unload Me
   Case 3: Bandera = 1
End Select
End Sub

Sub CargarTarifas()
'Dim limsup As Long     'DEPURACION DE CODIGO
'Dim liminf As Long     'DEPURACION DE CODIGO
'Dim Procesados As Integer      'DEPURACION DE CODIGO
Dim Errores
   On Error GoTo control
   PrgPlano.Value = 0
   If Cbo_ListaPrecios.ListIndex = -1 Then
      Call Mensaje1("Debe seleccionar una lista de precios", 3)
      Exit Sub
   End If
   If MDI_Inventarios.CMDialog1.InitDir = NUL$ Then MDI_Inventarios.CMDialog1.InitDir = DirDB
      MDI_Inventarios.CMDialog1.Filter = "(TODOS)|*"
      MDI_Inventarios.CMDialog1.Action = 1
      BandErr = False
      If MDI_Inventarios.CMDialog1.filename <> "" Then
         Result = DoDelete("ERRORES", NUL$)
         PrgPlano.Max = PrgPlano.Min + 1
         Open MDI_Inventarios.CMDialog1.filename For Input As #1
         Do While Not EOF(1)
            Line Input #1, texto
            PrgPlano.Max = PrgPlano.Max + 1
         Loop
         Close (1)
         Open MDI_Inventarios.CMDialog1.filename For Input As #1
         NumReg = 0
         BandProcLote = 1
         DoEvents
         Errores = 0
         BandStop = False
         Do While Not EOF(1)
            DoEvents
            If BandStop = True Then
               Exit Do
            End If
            PrgPlano.Value = PrgPlano.Value + 1
            Line Input #1, texto
            If Not ValidarTarifa Then
               Call IngresarTarifa
            Else
               Errores = Errores + 1
            End If
            On Error Resume Next
            Lbl_Procesados.Caption = PrgPlano.Value & " de: " & PrgPlano.Max
            Lbl_Procesados.Refresh
            Lbl_Errores.Caption = Errores
            Lbl_Errores.Refresh
         Loop
         Close (1)
         BandProcLote = 0
         ReDim Arr(0)
         Arr(0) = "PACIENTE.MDB"
         'If Errores > 0 Then Call ListarD("errores.rpt", NUL$, 0, "Errores en el archivo", MDI_Inventarios, False)
         'If Errores > 0 Then Call ListarD(DirRpt & "errores2.rpt", NUL$, 0, "Errores en el archivo", MDI_Inventarios, False) 'natalia M:223
         If Errores > 0 Then Call ListarD("errores2.rpt", NUL$, 0, "Errores en el archivo", MDI_Inventarios, False) 'natalia M:223
      End If
      PrgPlano.Value = 0
Exit Sub
control:
If ERR.Number <> 0 Then
   Call ConvertErr
   Close (1)
End If
Call MouseNorm
PrgPlano.Value = 0
End Sub

Sub DescargarTarifa()
Dim Linea As String
'Dim Procesados As Integer  'DEPURACION DE CODIGO
'Dim Errores As Integer         'DEPURACION DE CODIGO
Dim TOTAL As Integer

   Bandera = 0
   PrgPlano.Value = 0
   If Cbo_ListaPrecios.ListIndex = -1 Then
      Call Mensaje1("Debe seleccionar una lista de precios", 3)
      Exit Sub
   End If
   If MDI_Inventarios.CMDialog1.InitDir = NUL$ Then MDI_Inventarios.CMDialog1.InitDir = DirDB
   MDI_Inventarios.CMDialog1.Filter = "(TODOS)|*"
   MDI_Inventarios.CMDialog1.Action = 2
   BandErr = False
   If MDI_Inventarios.CMDialog1.filename <> "" Then
      On Error GoTo control
      Open MDI_Inventarios.CMDialog1.filename For Output As #1
      BandProcLote = 1
      Desde = "IN_R_LIST_ARTI,ARTICULO"
      'Campos = "CD_CODI_ARTI,FE_DESDE_RLIA,NU_PRECIO_RLIA,NU_AUTO_UNVE_RLIA"
      Campos = "CD_CODI_ARTI,FE_DESDE_RLIA,NU_PRECIO_RLIA,NU_AUTO_UNVE_RLIA,NO_NOMB_ARTI"
      Condi = "NU_AUTO_ARTI=NU_AUTO_ARTI_RLIA AND NU_AUTO_LIPR_RLIA=" & Cbo_ListaPrecios.ItemData(Cbo_ListaPrecios.ListIndex)
'      Desde = "R_SER_TARB"
'      Campos = "CD_CODI_SER_RST, NU_TILI_RST, VL_VALO_RST, CD_ALTE_RST"
'      Condicion = "CD_CODI_TARB_RST='" & CodTarifaBase(CboTarifaBase.ListIndex) & Comi
      ReDim Arr(0)
      Result = LoadData(Desde, "COUNT(CD_CODI_ARTI)", Condi, Arr)
      If Encontro Then TOTAL = Arr(0)
      If (DoSelect(Desde, Campos, Condi) <> FAIL) Then
         On Error GoTo control
         If (ResultQuery()) Then
            PrgPlano.Max = CLng(Arr(0))
            PrgPlano.Value = 0
            PrgPlano.Min = 0
            BandStop = False
            Do While (NextRowQuery())
               Debug.Print BandProcLote
               DoEvents
               If BandStop = True Then
                  Exit Do
               End If
'               Linea = CStr(PrgPlano.Value + 1) & String(6 - Len(CStr(PrgPlano.Value + 1)), " ")
'               Linea = Linea & DataQuery(1) & String(16 - Len(DataQuery(1)), " ")
'               Linea = Linea & Format(DataQuery(2), "dd/mm/yyyy") & " "
'               Linea = Linea & DataQuery(3) & String(20 - Len(DataQuery(3)), " ")
               Linea = CStr(PrgPlano.Value + 1)
               Linea = Linea & "," & DataQuery(1)
               Linea = Linea & "," & Format(DataQuery(2), "dd/mm/yyyy")
               Linea = Linea & "," & DataQuery(3)
               Linea = Linea & "," & DataQuery(4)
               Debug.Print Linea
               Print #1, Linea
               'If Bandera = 1 Then Close (1): Exit Sub
               Lbl_Procesados.Caption = PrgPlano.Value + 1 & " de " & TOTAL
               Lbl_Procesados.Refresh
               PrgPlano.Value = PrgPlano.Value + 1
            Loop
            Close (1)
            BandProcLote = 0
            PrgPlano.Value = 0
         End If
      End If
   End If
Exit Sub
control:
If ERR.Number <> 0 Then
   Call ConvertErr
   Close (1)
End If
Call MouseNorm
PrgPlano.Value = 0

End Sub

Function ValidarTarifa() As Boolean
Dim UnidadArti As Long
BandErr = False
Call CargarArr
'If Len(texto) = 53 Then
   ReDim Arr(1)
   Result = LoadData("ARTICULO", "NU_AUTO_ARTI,NU_AUTO_UNVE_ARTI", "CD_CODI_ARTI = '" & ArrTarifa(1) & Comi, Arr)
   If Not Encontro Then
      Call Error(CLng(NumReg), "Este c�digo de Art�culo no esta registrado en la base de datos")
   Else
      ArrTarifa(5) = Arr(0)
      UnidadArti = Val(Arr(1))
   End If
   If Not IsDate(ArrTarifa(2)) Then Call Error(CLng(NumReg), "La fecha debe ser una fecha v�lida")
   If Not IsNumeric(ArrTarifa(3)) Then Call Error(CLng(NumReg), "El valor debe ser num�rico")
   
   If IsNumeric(ArrTarifa(4)) Then
      If ArrTarifa(4) = 0 Then
         ArrTarifa(4) = UnidadArti
      Else
         ReDim Arr(0)
         Result = LoadData("IN_UNDVENTA", "NU_AUTO_UNVE", "NU_AUTO_UNVE=" & ArrTarifa(4), Arr)
         If Result <> FAIL And Encontro Then
            ArrTarifa(4) = CLng(Arr(0))
         Else
            Call Error(CLng(NumReg), "Esta Unidad de Medida no esta registrada en la base de datos")
         End If
      End If
   Else
      Call Error(CLng(NumReg), "El n�mero de la unidad de costo debe ser num�rico")
   End If
'Else
'   Call Error(CLng(NumReg), "El tama�o del registro es incorrecto( deben ser 53 caracteres)")
'End If
ValidarTarifa = BandErr
End Function

'               Linea = CStr(PrgPlano.Value + 1)
'               Linea = Linea & "," & DataQuery(1)
'               Linea = Linea & "," & Format(DataQuery(2), "dd/mm/yyyy")
'               Linea = Linea & "," & DataQuery(3)
Sub CargarArr()
'Dim DondEstaLaComa As Integer, ElMirador As Integer
Dim DondEstaLaComa As Integer       'DEPURACION DE CODIGO
Dim TextTemp As String
Dim i As Byte
ReDim ArrTarifa(5)

    NumReg = NumReg + 1
    TextTemp = texto
    
    ArrTarifa(1) = ""
    ArrTarifa(2) = ""
    ArrTarifa(3) = ""
    ArrTarifa(4) = ""
    
    DondEstaLaComa = InStr(1, TextTemp, ",", vbBinaryCompare)
    If Len(TextTemp) > DondEstaLaComa Then TextTemp = Right(TextTemp, Len(TextTemp) - DondEstaLaComa)
    DondEstaLaComa = InStr(1, TextTemp, ",", vbBinaryCompare)
    
    i = 1
    While Len(TextTemp) > DondEstaLaComa And DondEstaLaComa > 0 And i <= 3
        ArrTarifa(i) = Left(TextTemp, DondEstaLaComa - 1)
        TextTemp = Right(TextTemp, Len(TextTemp) - DondEstaLaComa)
        DondEstaLaComa = InStr(1, TextTemp, ",", vbBinaryCompare)
        i = i + 1
    Wend
    ArrTarifa(i) = TextTemp
    
'    If DondEstaLaComa > Len(texto) Then
'
'        While (Not ElMirador > Len(texto)) And (Not Mid(texto, ElMirador, 1) = ",")
'            ArrTarifa(1) = ArrTarifa(1) & Mid(texto, ElMirador, 1)
'            ElMirador = ElMirador + 1
'        Wend
'
'        ElMirador = ElMirador + 1
'        While (Not ElMirador > Len(texto)) And (Not Mid(texto, ElMirador, 1) = ",")
'            ArrTarifa(2) = ArrTarifa(2) & Mid(texto, ElMirador, 1)
'            ElMirador = ElMirador + 1
'        Wend
'        ElMirador = ElMirador + 1
'        If Len(texto) > ElMirador Then ArrTarifa(3) = Right(texto, Len(texto) - ElMirador + 1)
'    End If
    ArrTarifa(1) = Trim(ArrTarifa(1))
    ArrTarifa(2) = Trim(ArrTarifa(2))
    ArrTarifa(3) = Trim(ArrTarifa(3))
    ArrTarifa(4) = IIf(Trim(ArrTarifa(4)) = "", 0, Trim(ArrTarifa(4)))
End Sub

'Sub OLDCargarArr()
'ReDim ArrTarifa(5)
'   NumReg = NumReg + 1
'   ArrTarifa(1) = Trim(Mid(texto, 7, 16))  'Codigo del articulo
'   ArrTarifa(2) = Trim(Mid(texto, 23, 10)) 'fecha
'   ArrTarifa(3) = Trim(Mid(texto, 33, 20)) 'Valor
'End Sub

Sub Error(Linea As Long, Txt As String)
   '"EL tama�o del registro es diferente a 214"
   Valores = "NU_LINE_ERRO=" & Linea & Coma
   Valores = Valores & "DE_DESC_ERRO='" & Txt & Comi
   Result = DoInsertSQL("ERRORES", Valores)
   BandErr = True
End Sub

Sub IngresarTarifa()
   On Error GoTo control
   Valores = "NU_AUTO_LIPR_RLIA=" & Cbo_ListaPrecios.ItemData(Cbo_ListaPrecios.ListIndex) & Coma
   Valores = Valores & "NU_AUTO_ARTI_RLIA=" & ArrTarifa(5) & Coma
   Valores = Valores & "FE_DESDE_RLIA=" & FFechaIns(CDate(ArrTarifa(2))) & Coma
   Valores = Valores & "NU_PRECIO_RLIA=" & ArrTarifa(3) & Coma
   Valores = Valores & "NU_AUTO_UNVE_RLIA=" & ArrTarifa(4)
   
   
   
   Condi = "NU_AUTO_LIPR_RLIA=" & Cbo_ListaPrecios.ItemData(Cbo_ListaPrecios.ListIndex)
   Condi = Condi & " AND NU_AUTO_ARTI_RLIA=" & ArrTarifa(5)
   Condi = Condi & " AND FE_DESDE_RLIA=" & FFechaCon(CDate(ArrTarifa(2)))
   ReDim Arr(0)
   Result = LoadData("IN_R_LIST_ARTI", "NU_AUTO_ARTI_RLIA", Condi, Arr())
   If Result <> FAIL Then
      If Encontro Then
         Result = DoUpdate("IN_R_LIST_ARTI", Valores, Condi)
      Else
         Result = DoInsertSQL("IN_R_LIST_ARTI", Valores)
      End If
   End If
'   Valores = "CD_CODI_TARB_RST='" & CodTarifaBase(CboTarifaBase.ListIndex) & Comi & Coma
'   Valores = Valores & "CD_CODI_SER_RST='" & ArrTarifa(1) & Comi & Coma
'   Valores = Valores & "NU_TILI_RST=" & ArrTarifa(2) & Coma
'   Valores = Valores & "VL_VALO_RST=" & DecDouble(CStr(ArrTarifa(3))) & Coma
'   Valores = Valores & "CD_ALTE_RST='" & ArrTarifa(4) & Comi
   
'   ReDim Arr(0)
'   If OptActualizar(0) Then
'      Condicion = "CD_CODI_TARB_RST='" & CodTarifaBase(CboTarifaBase.ListIndex) & Comi
'      Condicion = Condicion & " AND CD_CODI_SER_RST = '" & ArrTarifa(1) & Comi
'
'      Result = LoadData("R_SER_TARB", "CD_CODI_SER_RST", Condicion, Arr)
'      If Encontro Then
'         Result = DoUpdate("R_SER_TARB", Valores, Condicion)
'      Else
'         Result = DoInsertSQL("R_SER_TARB", Valores)
'      End If
'   Else
'      If ArrTarifa(4) <> NUL$ Then
'         Condicion = "CD_CODI_TARB_RST='" & CodTarifaBase(CboTarifaBase.ListIndex) & Comi
'         Condicion = Condicion & " AND CD_ALTE_RST = '" & ArrTarifa(4) & Comi
'         ReDim Arr(0)
'         Result = LoadData("R_SER_TARB", "CD_CODI_SER_RST", Condicion, Arr)
'         If Encontro Then
'            Valores = "CD_CODI_TARB_RST='" & CodTarifaBase(CboTarifaBase.ListIndex) & Comi & Coma
'            Valores = Valores & "NU_TILI_RST=" & ArrTarifa(2) & Coma
'            Valores = Valores & "VL_VALO_RST=" & DecDouble(CStr(ArrTarifa(3))) & Coma
'            Valores = Valores & "CD_ALTE_RST='" & ArrTarifa(4) & Comi
'
'            Result = DoUpdate("R_SER_TARB", Valores, Condicion)
'         Else
'            Condicion = "CD_CODI_TARB_RST='" & CodTarifaBase(CboTarifaBase.ListIndex) & Comi
'            Condicion = Condicion & " AND CD_CODI_SER_RST = '" & ArrTarifa(1) & Comi
'
'            Result = LoadData("R_SER_TARB", "CD_CODI_SER_RST", Condicion, Arr)
'            If Encontro Then
'               Result = DoUpdate("R_SER_TARB", Valores, Condicion)
'            Else
'               Result = LoadData("SERVICIOS", "CD_CODI_SER", "CD_CODI_SER = '" & ArrTarifa(1) & Comi, Arr)
'               If Not Encontro Then
'                  Call Error(CLng(NumReg), "Este codigo de procedimiento no esta registrado en la base de datos")
'               Else
'                  Result = DoInsertSQL("R_SER_TARB", Valores)
'               End If
'            End If
'         End If
'      End If
'   End If
   If Result = FAIL Then
      Call Error(CLng(NumReg), ErrorProc)
   End If
Exit Sub
control:
Call Error(CLng(NumReg), ErrorProc)
End Sub
