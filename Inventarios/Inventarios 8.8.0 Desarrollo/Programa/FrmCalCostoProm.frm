VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Begin VB.Form FrmCalCostoProm 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Recalcular Costo Promedio"
   ClientHeight    =   6315
   ClientLeft      =   45
   ClientTop       =   435
   ClientWidth     =   10770
   Icon            =   "FrmCalCostoProm.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   6315
   ScaleWidth      =   10770
   Begin VB.CommandButton CmdBoton 
      Caption         =   "&SALIR"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   6
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   495
      Index           =   1
      Left            =   5760
      Picture         =   "FrmCalCostoProm.frx":058A
      Style           =   1  'Graphical
      TabIndex        =   5
      Top             =   5640
      Width           =   1335
   End
   Begin VB.CommandButton CmdBoton 
      Caption         =   "&CALCULAR"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   6
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   495
      Index           =   0
      Left            =   3360
      Picture         =   "FrmCalCostoProm.frx":08CC
      Style           =   1  'Graphical
      TabIndex        =   4
      Top             =   5640
      Width           =   1335
   End
   Begin VB.CheckBox ChkAllArti 
      Caption         =   "Todos los Art�culos"
      Height          =   255
      Left            =   240
      TabIndex        =   2
      Top             =   720
      Width           =   2055
   End
   Begin VB.ComboBox CmbTipoArti 
      Height          =   315
      ItemData        =   "FrmCalCostoProm.frx":0C3E
      Left            =   1680
      List            =   "FrmCalCostoProm.frx":0C40
      Style           =   2  'Dropdown List
      TabIndex        =   1
      Top             =   240
      Width           =   2775
   End
   Begin MSComctlLib.ListView LstVArti 
      Height          =   4455
      Left            =   120
      TabIndex        =   3
      Top             =   1080
      Width           =   10500
      _ExtentX        =   18521
      _ExtentY        =   7858
      View            =   3
      LabelEdit       =   1
      MultiSelect     =   -1  'True
      LabelWrap       =   -1  'True
      HideSelection   =   0   'False
      AllowReorder    =   -1  'True
      Checkboxes      =   -1  'True
      FullRowSelect   =   -1  'True
      GridLines       =   -1  'True
      _Version        =   393217
      ForeColor       =   -2147483640
      BackColor       =   -2147483643
      BorderStyle     =   1
      Appearance      =   1
      NumItems        =   2
      BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Text            =   "Nombre del Art�culo"
         Object.Width           =   7761
      EndProperty
      BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   1
         Text            =   "C�digo"
         Object.Width           =   3881
      EndProperty
   End
   Begin VB.Label LblTipoArti 
      Caption         =   "Tipos de Art�culos"
      Height          =   255
      Left            =   240
      TabIndex        =   0
      Top             =   240
      Width           =   1335
   End
End
Attribute VB_Name = "FrmCalCostoProm"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'---------------------------------------------------------------------------------------
' Module    : FrmCalCostoProm
' DateTime  : 18/02/2008 17:13
' Author    : hector_rodriguez
' Purpose   : recalcula el costo promedio de  los articulos
'---------------------------------------------------------------------------------------

Option Explicit

Dim InTipoArti As Integer
Dim BoProcCalc As Boolean


Private Sub ChkAllArti_KeyPress(KeyAscii As Integer)

   Call Cambiar_Enter(KeyAscii)
   
End Sub

Private Sub CmbTipoArti_GotFocus()

   InTipoArti = CmbTipoArti.ListIndex
   
End Sub

Private Sub CmbTipoArti_KeyPress(KeyAscii As Integer)

   Call Cambiar_Enter(KeyAscii)
   
End Sub

Private Sub CmbTipoArti_LostFocus()

   Call Buscar_Articulos
   ChkAllArti.Value = 0

End Sub

Private Sub CmdBoton_Click(Index As Integer)
   
   Select Case Index
      Case 0
         Calcular_Costo_Promedio
      Case 1
         Unload Me
      Case Else
   End Select
   
End Sub

Private Sub Form_Load()
   
   Call CenterForm(MDI_Inventarios, Me)
   CmbTipoArti.AddItem "Para la Venta"
   CmbTipoArti.AddItem "Para El Consumo"
   CmbTipoArti.ListIndex = 0
   InTipoArti = -1
   LstVArti.ListItems.Clear
   LstVArti.Checkboxes = False
   LstVArti.MultiSelect = True
   LstVArti.HideSelection = False
   LstVArti.Width = 10500
   LstVArti.ColumnHeaders.Clear
   LstVArti.ColumnHeaders.Add , "NOAR", "Nombre del art�culo", 0.4 * LstVArti.Width
   LstVArti.ColumnHeaders.Add , "COAR", "C�digo", 0.12 * LstVArti.Width
   LstVArti.ColumnHeaders.Add , "NOCO", "Unidad de conteo", 0.12 * LstVArti.Width
   LstVArti.ColumnHeaders.Add , "COPR", "Costo", 0.1 * LstVArti.Width
   LstVArti.ColumnHeaders("COPR").Alignment = lvwColumnRight
   LstVArti.ColumnHeaders.Add , "NOUS", "Uso", 0.12 * LstVArti.Width
   LstVArti.ColumnHeaders.Add , "NOGR", "Grupo", 0.12 * LstVArti.Width
   LstVArti.ColumnHeaders.Add , "AUCO", "AutoUNC", 0
   LstVArti.ColumnHeaders.Add , "COGR", "CodGRU", 0
   LstVArti.ColumnHeaders.Add , "COUS", "CodUSO", 0
   Call Buscar_Articulos
   InTipoArti = CmbTipoArti.ListIndex
End Sub

'---------------------------------------------------------------------------------------
' Procedure : Buscar_Articulos
' DateTime  : 18/02/2008 17:13
' Author    : hector_rodriguez
' Purpose   : busca todos los articulos dependiendo del tipo de articulo
'---------------------------------------------------------------------------------------
'
Private Sub Buscar_Articulos()
   
   Dim StTipo As String
   Dim ArArti() As Variant
   Dim InI As Integer
   
   If BoProcCalc Then Call Mensaje1("El proceso se esta ejecutando", 3)
   
   If InTipoArti = Me.CmbTipoArti.ListIndex Then Exit Sub
   
   LstVArti.ListItems.Clear
   
   Select Case Me.CmbTipoArti.ListIndex
      Case Is = 0
          StTipo = "V"
      Case Is = 1
          StTipo = "C"
      Case Else
         StTipo = "0"
   End Select
   
   Campos = "NU_AUTO_ARTI, CD_CODI_ARTI, NO_NOMB_ARTI, NU_AUTO_UNVE_ARTI, TX_PARA_ARTI, " & _
       "DE_DESC_GRUP, DE_DESC_USOS, TX_NOMB_UNVE, CD_GRUP_ARTI, CD_USOS_ARTI, " & _
       "NU_MULT_UNVE, NU_DIVI_UNVE, VL_COPR_ARTI"
   Desde = "ARTICULO, USOS, GRUP_ARTICULO, IN_UNDVENTA"
   Condicion = "CD_CODI_GRUP = CD_GRUP_ARTI AND CD_CODI_USOS = CD_USOS_ARTI"
   Condicion = Condicion & " AND NU_AUTO_UNVE = NU_AUTO_UNVE_ARTI"
   Condicion = Condicion & " AND TX_PARA_ARTI='" & StTipo & "' ORDER BY NO_NOMB_ARTI"
   ReDim ArArti(12, 0)
   On Error GoTo GoError
   Result = LoadMulData(Desde, Campos, Condicion, ArArti())
   If Result <> FAIL And Encontro Then
   
      Dim Articulo As New UnArticulo
      For InI = 0 To UBound(ArArti, 2)
          
          Articulo.AutoNumero = ArArti(0, InI)
          Articulo.Codigo = ArArti(1, InI)
          Articulo.Nombre = ArArti(2, InI)
          Articulo.AutoUDConteo = ArArti(3, InI)
          Articulo.NombreGrupo = ArArti(5, InI)
          Articulo.NombreUso = ArArti(6, InI)
          Articulo.NombreUndConteo = ArArti(7, InI)
          Articulo.ParaQueEs = InStr(1, "VC", ArArti(4, InI)) - 1
          Articulo.CodigoGrupo = ArArti(8, InI)
          Articulo.CodigoUso = ArArti(9, InI)
          If Not IsNumeric(ArArti(12, InI)) Then ArArti(12, InI) = "0"
          Articulo.CostoPromedio = ArArti(12, InI)
          
          Me.LstVArti.ListItems.Add , "A" & Articulo.AutoNumero, Trim(Articulo.Nombre)
          Me.LstVArti.ListItems("A" & Articulo.AutoNumero).Tag = CStr(Articulo.AutoNumero)
      'LstVArti, NOAR, COAR, NOCO, COPR, NOUS, NOGR, AUCO, COGR, COUS, MUCO, DICO
          Me.LstVArti.ListItems("A" & Articulo.AutoNumero).ListSubItems.Add , "COAR", Trim(Articulo.Codigo)
          Me.LstVArti.ListItems("A" & Articulo.AutoNumero).ListSubItems.Add , "NOCO", Trim(Articulo.NombreUndConteo)
          Me.LstVArti.ListItems("A" & Articulo.AutoNumero).ListSubItems.Add , "COPR", Format(Articulo.CostoPromedio, "###,###,###.##")
          Me.LstVArti.ListItems("A" & Articulo.AutoNumero).ListSubItems.Add , "NOUS", Trim(Articulo.NombreUso)
          Me.LstVArti.ListItems("A" & Articulo.AutoNumero).ListSubItems.Add , "NOGR", Trim(Articulo.NombreGrupo)
          Me.LstVArti.ListItems("A" & Articulo.AutoNumero).ListSubItems.Add , "AUCO", Trim(Articulo.AutoUDConteo)
          Me.LstVArti.ListItems("A" & Articulo.AutoNumero).ListSubItems.Add , "COGR", Trim(Articulo.CodigoGrupo)
          Me.LstVArti.ListItems("A" & Articulo.AutoNumero).ListSubItems.Add , "COUS", Trim(Articulo.CodigoUso)
      Next
      Set Articulo = Nothing
      
   End If
   
   Exit Sub
   
GoError:
   Call Mensaje1("Error cargando los art�culos", 3)
   Set Articulo = Nothing
   
End Sub

'---------------------------------------------------------------------------------------
' Procedure : Calcular_Costo_Promedio
' DateTime  : 18/02/2008 17:12
' Author    : hector_rodriguez
' Purpose   : Calcula el costo promedio de los articulos seleccionados o dependiendo del tipo de articulo
'---------------------------------------------------------------------------------------
'
Private Sub Calcular_Costo_Promedio()
   
   Dim LstItmsArti As MSComctlLib.ListItem
   Dim StTipo As String
   Dim StSentencia As String
   Dim ArCostPArti() As Variant
   Dim InI As Integer
   Dim InDoEvents
      
   If BoProcCalc Then Call Mensaje1("El proceso se esta ejecultando.", 3): Exit Sub
   If MotorBD = "SQL" Then If Not ExisteStoreProcedure("PA_COSTO_PROMEDIO") Then Call Mensaje1("No se encontro el procedimiento almacenado PA_COSTO_PROMEDIO.", 2): Exit Sub
   If MsgBox("Esta seguro que desea recalcular el costo promedio?", vbQuestion + vbYesNo) = vbNo Then Exit Sub
   
   Call MouseClock
   Select Case Me.CmbTipoArti.ListIndex
      Case Is = 0
          StTipo = "V"
      Case Is = 1
          StTipo = "C"
      Case Else
         StTipo = "0"
   End Select
   
   Result = DoDelete("ARTI_COSTOPROM_TMP", NUL$)
   If Result <> FAIL Then
      BoProcCalc = True
      InDoEvents = DoEvents()
      If Me.ChkAllArti.Value = 0 Then
         For Each LstItmsArti In Me.LstVArti.ListItems
            If LstItmsArti.Selected Then
               Result = DoInsertSQL("ARTI_COSTOPROM_TMP", "NU_AUTO_ARTI_ACPT=" & LstItmsArti.Tag)
            End If
            If Result = FAIL Then Call Mensaje1("Error al determinar el autonumerico del art�culo", 2): Call MouseNorm: BoProcCalc = False: Exit Sub
         Next
          StTipo = "N"
      End If
      On Error GoTo Error
      If MotorBD = "SQL" Then
         Dim Cmd As New adodb.Command
         Call CreaParametrosSP(Cmd, "TipoArti", adVarChar, StTipo, adParamInput, 1)
         Call CreaParametrosSP(Cmd, "Resultado", adInteger, 0, adParamOutput)
   
         Cmd.ActiveConnection = BD(BDCurCon)
         Cmd.CommandType = adCmdStoredProc
         Cmd.CommandText = "PA_COSTO_PROMEDIO"
         Cmd.Execute
         Result = Cmd.Parameters.Item("@Resultado").Value
   
         Set Cmd = Nothing
      ElseIf MotorBD = "ACCESS" Then
         ReDim ArCostPArti(1, 0)
         If Me.ChkAllArti.Value = 1 Then
'            Campos = "ELEMENTO, (IIF(CANTIDAD<>0 , ROUND((ABS(SALIDAS-ENTRADAS)/ABS(CANTIDAD)),2) "
'            Campos = Campos & Coma & " 0)) AS COSTOPROMEDIO"
'            StSentencia = " (SELECT NU_AUTO_ARTI_DETA AS ELEMENTO"
'            StSentencia = StSentencia & " ,SUM(IIF( NU_AUTO_DOCU_ENCA IN (4,8,13,19) , ROUND((NU_SALIDA_DETA * NU_COSTO_DETA),2) , 0))"
'            StSentencia = StSentencia & "  AS SALIDAS"
'            StSentencia = StSentencia & " ,SUM(IIF( NU_AUTO_DOCU_ENCA IN (3,9,14,22,24) , ROUND((NU_ENTRAD_DETA * NU_COSTO_DETA),2)"
'            StSentencia = StSentencia & " , 0 )) AS ENTRADAS"
'            StSentencia = StSentencia & " ,SUM(NU_SALIDA_DETA-NU_ENTRAD_DETA) AS CANTIDAD"
'            StSentencia = StSentencia & " FROM IN_DETALLE, IN_ENCABEZADO, ARTICULO"
'            StSentencia = StSentencia & " WHERE NU_AUTO_ORGCABE_DETA = NU_AUTO_ENCA AND NU_AUTO_DOCU_ENCA IN (3,4,8,9,13,14,19,22,24)"
'            StSentencia = StSentencia & " AND TX_PARA_ARTI = " & Comi & StTipo & Comi & " AND NU_AUTO_ARTI_DETA=NU_AUTO_ARTI GROUP BY NU_AUTO_ARTI_DETA) T1"

            Campos = "ELEMENTO, (IIF(CANTIDAD<>0 , ROUND((ABS(SALIDAS-ENTRADAS)/ABS(CANTIDAD)),2) "
            Campos = Campos & Coma & " 0)) AS COSTOPROMEDIO"
            StSentencia = " (SELECT NU_AUTO_ARTI_DETA AS ELEMENTO"
            StSentencia = StSentencia & " ,SUM(IIF( NU_AUTO_DOCU_ENCA IN (4,8,13,19) , ROUND((NU_SALIDA_DETA * NU_COSTO_DETA),2) , 0))"
            StSentencia = StSentencia & "  AS SALIDAS"
            StSentencia = StSentencia & " ,SUM(IIF( NU_AUTO_DOCU_ENCA IN (3,9,14,22,24) , ROUND((NU_ENTRAD_DETA * NU_COSTO_DETA),2)"
            StSentencia = StSentencia & " , 0 )) AS ENTRADAS"
            StSentencia = StSentencia & " ,SUM(NU_SALIDA_DETA-NU_ENTRAD_DETA) AS CANTIDAD"
            StSentencia = StSentencia & " FROM IN_DETALLE, IN_ENCABEZADO"
            StSentencia = StSentencia & " WHERE NU_AUTO_MODCABE_DETA=NU_AUTO_ORGCABE_DETA"
            StSentencia = StSentencia & " AND NU_AUTO_ORGCABE_DETA= NU_AUTO_ENCA AND NU_AUTO_DOCU_ENCA IN (3,4,8,9,13,14,19,22,24)"
            StSentencia = StSentencia & " AND NU_AUTO_ARTI_DETA IN (SELECT NU_AUTO_ARTI FROM ARTICULO WHERE TX_PARA_ARTI = " & Comi & StTipo & Comi & ") GROUP BY NU_AUTO_ARTI_DETA) T1"
            
         Else
            Campos = "ELEMENTO, (IIF(CANTIDAD<>0 , ROUND((ABS(SALIDAS-ENTRADAS)/ABS(CANTIDAD)),2) "
            Campos = Campos & Coma & " 0)) AS COSTOPROMEDIO"
            StSentencia = " (SELECT NU_AUTO_ARTI_DETA AS ELEMENTO"
            StSentencia = StSentencia & " ,SUM(IIF( NU_AUTO_DOCU_ENCA IN (4,8,13,19) , ROUND((NU_SALIDA_DETA * NU_COSTO_DETA),2) , 0))"
            StSentencia = StSentencia & "  AS SALIDAS"
            StSentencia = StSentencia & " ,SUM(IIF( NU_AUTO_DOCU_ENCA IN (3,9,14,22,24) , ROUND((NU_ENTRAD_DETA * NU_COSTO_DETA),2)"
            StSentencia = StSentencia & " , 0 )) AS ENTRADAS"
            StSentencia = StSentencia & " ,SUM(NU_SALIDA_DETA-NU_ENTRAD_DETA) AS CANTIDAD"
            StSentencia = StSentencia & " FROM ARTI_COSTOPROM_TMP,IN_DETALLE, IN_ENCABEZADO"
            'StSentencia = StSentencia & " WHERE NU_AUTO_ARTI_ACPT= NU_AUTO_ARTI_DETA AND NU_AUTO_ORGCABE_DETA = NU_AUTO_ENCA" 'HRR DEPURACION
            StSentencia = StSentencia & " WHERE NU_AUTO_ARTI_ACPT= NU_AUTO_ARTI_DETA AND NU_AUTO_MODCABE_DETA= NU_AUTO_ORGCABE_DETA" 'HRR DEPURACION
            StSentencia = StSentencia & " AND NU_AUTO_ORGCABE_DETA = NU_AUTO_ENCA"
            StSentencia = StSentencia & " AND NU_AUTO_DOCU_ENCA IN (3,4,8,9,13,14,19,22,24) "
            StSentencia = StSentencia & "  GROUP BY NU_AUTO_ARTI_DETA) T1"
            
         End If
         
         Result = LoadMulData(StSentencia, Campos, NUL$, ArCostPArti)
         If Result <> FAIL And Encontro Then
            If BeginTran(TranUpd & "-ARTICULO") <> FAIL Then
               For InI = 0 To UBound(ArCostPArti(), 2)
                  If ArCostPArti(1, InI) = NUL$ Then ArCostPArti(1, InI) = 0    'JACC DEPURACION NOV 2008
                  ArCostPArti(1, InI) = CDbl(Aplicacion.Formatear_Valor(ArCostPArti(1, InI)))         'JACC DEPURACION NOV 2008 'HRR M4537
                  Result = DoUpdate("ARTICULO", "VL_COPR_ARTI=" & ArCostPArti(1, InI), "NU_AUTO_ARTI=" & ArCostPArti(0, InI))
               Next
               If Result <> FAIL Then
                  If CommitTran() <> FAIL Then
                     
                  Else
                     RollBackTran
                  End If
               Else
                  RollBackTran
               End If
            End If
         End If
         
      End If
      If Result = FAIL Then
         Call Mensaje1("No se pudo recalcular el costo promedio", 2)
      Else
         Call Mensaje1("El proceso termino satisfactoriamente.", 3)
      End If
   Else
      Call Mensaje1("Error al borrar la tabla de art�culos seleccionados.", 2)
   End If
   BoProcCalc = False
   Call MouseNorm
   
 Exit Sub
Error:
    Call ConvertErr
    If MotorBD = "SQL" Then
      Set Cmd = Nothing
      Call Mensaje1("Error al ejecutar el procedimiento almacenado PA_COSTO_PROMEDIO", 2)
   End If
   BoProcCalc = False
   Call MouseNorm
    
 
End Sub

'HRR M3060
Private Sub LstVArti_ColumnClick(ByVal ColumnHeader As MSComctlLib.ColumnHeader)
   'If ColumnHeader.Index = 1 Or ColumnHeader.Index = 2 Then
      LstVArti.SortKey = ColumnHeader.Index - 1
      LstVArti.Sorted = True
   'End If
End Sub
'HRR M3060
