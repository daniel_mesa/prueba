VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Object = "{C932BA88-4374-101B-A56C-00AA003668DC}#1.1#0"; "MSMASK32.OCX"
Object = "{0BA686C6-F7D3-101A-993E-0000C0EF6F5E}#1.0#0"; "THREED32.OCX"
Begin VB.Form FrmMerNiif 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Valor del Mercado NIIF"
   ClientHeight    =   3510
   ClientLeft      =   45
   ClientTop       =   375
   ClientWidth     =   5760
   Icon            =   "FrmMerNiif.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   3510
   ScaleWidth      =   5760
   Begin VB.Frame Frame1 
      Height          =   975
      Left            =   120
      TabIndex        =   18
      Top             =   2040
      Width           =   4455
      Begin Threed.SSCommand SCmd_Options 
         Height          =   735
         Index           =   6
         Left            =   3480
         TabIndex        =   11
         ToolTipText     =   "Cargar Articulos con Valor NiiF"
         Top             =   120
         Width           =   855
         _Version        =   65536
         _ExtentX        =   1508
         _ExtentY        =   1296
         _StockProps     =   78
         Caption         =   "&CARGAR"
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   6
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Picture         =   "FrmMerNiif.frx":058A
      End
      Begin Threed.SSCommand SCmd_Options 
         Height          =   735
         Index           =   5
         Left            =   120
         TabIndex        =   9
         ToolTipText     =   "Descargar Articulos"
         Top             =   120
         Width           =   855
         _Version        =   65536
         _ExtentX        =   1508
         _ExtentY        =   1296
         _StockProps     =   78
         Caption         =   "&DESCARGA"
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   6
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Picture         =   "FrmMerNiif.frx":0C54
      End
   End
   Begin VB.Frame FrmBotones 
      Height          =   3375
      Left            =   4680
      TabIndex        =   16
      Top             =   0
      Width           =   975
      Begin Threed.SSCommand SCmd_Options 
         Height          =   615
         Index           =   0
         Left            =   120
         TabIndex        =   5
         ToolTipText     =   "Almacena la informacion registrada"
         Top             =   360
         Width           =   735
         _Version        =   65536
         _ExtentX        =   1296
         _ExtentY        =   1085
         _StockProps     =   78
         Caption         =   "&GUARDAR"
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   6
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Picture         =   "FrmMerNiif.frx":131E
      End
      Begin Threed.SSCommand SCmd_Options 
         Height          =   615
         Index           =   1
         Left            =   120
         TabIndex        =   6
         ToolTipText     =   "Elimina el valor del codigo ingresado"
         Top             =   1080
         Width           =   735
         _Version        =   65536
         _ExtentX        =   1296
         _ExtentY        =   1085
         _StockProps     =   78
         Caption         =   "&BORRAR"
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   6
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Picture         =   "FrmMerNiif.frx":19E8
      End
      Begin Threed.SSCommand SCmd_Options 
         Height          =   615
         Index           =   3
         Left            =   120
         TabIndex        =   7
         ToolTipText     =   "listar informacion"
         Top             =   1800
         Width           =   735
         _Version        =   65536
         _ExtentX        =   1296
         _ExtentY        =   1085
         _StockProps     =   78
         Caption         =   "&LISTAR"
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   6
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Picture         =   "FrmMerNiif.frx":20B2
      End
      Begin Threed.SSCommand SCmd_Options 
         Height          =   615
         Index           =   4
         Left            =   120
         TabIndex        =   8
         ToolTipText     =   "Cerrar"
         Top             =   2520
         Width           =   735
         _Version        =   65536
         _ExtentX        =   1296
         _ExtentY        =   1085
         _StockProps     =   78
         Caption         =   "&SALIR"
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   6
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Picture         =   "FrmMerNiif.frx":277C
      End
   End
   Begin VB.TextBox txtValMercado 
      Height          =   285
      Left            =   1800
      MaxLength       =   15
      TabIndex        =   4
      Top             =   1680
      Width           =   2415
   End
   Begin VB.TextBox txtNombre 
      Height          =   765
      Left            =   1800
      TabIndex        =   3
      Top             =   840
      Width           =   2415
   End
   Begin VB.TextBox txtCodigo 
      Height          =   285
      Left            =   1800
      MaxLength       =   20
      TabIndex        =   1
      Top             =   480
      Width           =   2415
   End
   Begin Threed.SSCommand CmdLimpia 
      Height          =   285
      Left            =   3720
      TabIndex        =   10
      ToolTipText     =   "Limpia Pantalla"
      Top             =   120
      Width           =   495
      _Version        =   65536
      _ExtentX        =   873
      _ExtentY        =   503
      _StockProps     =   78
      Picture         =   "FrmMerNiif.frx":2E46
   End
   Begin MSComctlLib.StatusBar StatusBar 
      Height          =   255
      Left            =   120
      TabIndex        =   17
      Top             =   3120
      Width           =   4455
      _ExtentX        =   7858
      _ExtentY        =   450
      _Version        =   393216
      BeginProperty Panels {8E3867A5-8586-11D1-B16A-00C0F0283628} 
         NumPanels       =   2
         BeginProperty Panel1 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            AutoSize        =   1
            Object.Width           =   6015
            MinWidth        =   2999
         EndProperty
         BeginProperty Panel2 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Object.Width           =   1766
            MinWidth        =   1766
            Object.ToolTipText     =   "Tiempo Transcurrido"
         EndProperty
      EndProperty
   End
   Begin MSMask.MaskEdBox txtFecha 
      BeginProperty DataFormat 
         Type            =   1
         Format          =   "dd/MM/yyyy"
         HaveTrueFalseNull=   0
         FirstDayOfWeek  =   0
         FirstWeekOfYear =   0
         LCID            =   9226
         SubFormatType   =   3
      EndProperty
      Height          =   300
      Left            =   1800
      TabIndex        =   0
      Top             =   120
      Width           =   1815
      _ExtentX        =   3201
      _ExtentY        =   529
      _Version        =   393216
      MaxLength       =   10
      Format          =   "dd/mm/yyyy"
      Mask            =   "##/##/####"
      PromptChar      =   "#"
   End
   Begin Threed.SSCommand SCmd_Options 
      Height          =   285
      Index           =   2
      Left            =   4200
      TabIndex        =   2
      ToolTipText     =   "Seleccionar un articulo"
      Top             =   480
      Width           =   375
      _Version        =   65536
      _ExtentX        =   661
      _ExtentY        =   503
      _StockProps     =   78
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   6
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BevelWidth      =   0
      Outline         =   0   'False
      Picture         =   "FrmMerNiif.frx":3198
   End
   Begin VB.Label Label4 
      Caption         =   "Valor mercado"
      Height          =   255
      Left            =   480
      TabIndex        =   15
      Top             =   1680
      Width           =   1335
   End
   Begin VB.Label Lblnombre 
      Caption         =   "Nombre"
      Height          =   255
      Left            =   480
      TabIndex        =   14
      Top             =   840
      Width           =   1335
   End
   Begin VB.Label Lblcodigo 
      Caption         =   "C�digo"
      Height          =   255
      Left            =   480
      TabIndex        =   13
      Top             =   480
      Width           =   1335
   End
   Begin VB.Label lblFecha 
      Caption         =   "Fecha"
      Height          =   255
      Left            =   480
      TabIndex        =   12
      Top             =   120
      Width           =   1335
   End
End
Attribute VB_Name = "FrmMerNiif"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit 'SKRV T24951
Dim FS_Err 'variables para el archivo error
Dim A_Err 'variables para el archivo error
Dim StMensaje As String 'SKRV T24953
Dim StFormato As String 'SKRV T26044
'---------------------------------------------------------------------------------------
' Procedure : Borrar
' DateTime  : 15/10/2014 14:51
' Author    : sinthia_rodriguez
' Purpose   : SKRV T24261/R22366
'---------------------------------------------------------------------------------------
'
Private Sub Borrar()

   If txtCodigo = NUL$ Then Exit Sub 'SKRV T24951
   
   ReDim Arr(0)
   
   Desde = "VALORMERINV"
   Campos = "CD_CODI_VAMEIN"
   'Condicion = "NU_ARTI_VAMEIN = " & txtCodigo 'SKRV T25007 COMENTARIO
   Condicion = "NU_ARTI_VAMEIN = '" & txtCodigo & "'" 'SKRV T25007
   
   'Arr(0) = fnDevDato(Desde, Campos, Condicion) 'SKRV T24951 COMENTARIO
   Arr(0) = fnDevDato(Desde, Campos, Condicion, True) 'SKRV T24951
   
   If Arr(0) <> "" Then
      If WarnMsg("Desea Borrar el registro?") Then
         Result = DoDelete(Desde, Condicion)
      'INICIO SKRV T25007
      Else
         Exit Sub
      'FIN SKRV T25007
      End If
   'INICIO SKRV T25007
   Else
      'JAUM T26318 INICIO SE DEJA LA SIGUIENTE LINEA EN COMENTARIO
      'Call Limpiar
      Call Mensaje1("No se pudo borrar la informaci�n de la base de datos, debido a que no existe !.", 1)
      'JAUM T26318 FIN
      Exit Sub
   'FIN SKRV T25007
   End If
   
   If Result <> FAIL Then Call _
      Mensaje1("El registro se ha eliminado exitosamente.", 3): Call Limpiar
End Sub



'---------------------------------------------------------------------------------------
' Procedure : Limpiar
' DateTime  : 15/10/2014 14:52
' Author    : sinthia_rodriguez
' Purpose   : SKRV T24261/R22366
'---------------------------------------------------------------------------------------
'
Private Sub Limpiar()
   txtFecha.Text = Format(Nowserver, "dd/mm/yyyy")
   txtCodigo.Text = NUL$
   txtCodigo.Enabled = True
   txtNombre.Text = NUL$
   txtValMercado.Text = NUL$
End Sub

'---------------------------------------------------------------------------------------
' Procedure : Descargar
' DateTime  : 15/10/2014 14:52
' Author    : sinthia_rodriguez
' Purpose   : SKRV T24261/R22366
'---------------------------------------------------------------------------------------
'
Private Sub Descargar()
   
   Dim FS
   Dim A
   
   Call Activar_Inactivar_Controles(False)
   
   Set FS = CreateObject("Scripting.FileSystemObject")

   If Dir("c:\cntsiste.cnt\" & "TXT_VALMER_NIIF" & ".txt", vbDirectory) <> "" _
      Then Call FS.DeleteFile("c:\cntsiste.cnt\" & "TXT_VALMER_NIIF" & ".txt", True)

   Set A = FS.CreateTextFile("c:\cntsiste.cnt\" & "TXT_VALMER_NIIF" & ".txt", _
      True)
   
   Call BarraInfo("Generando archivo ...")

   Campos = "CD_CODI_ARTI, NO_NOMB_ARTI"
   Desde = "ARTICULO"

   If (DoSelect(Desde, Campos, NUL$) <> FAIL) Then
      If (ResultQuery()) Then
         Call MouseClock
         Do While (NextRowQuery())
            A.WriteLine (DataQuery(1) & Coma & DataQuery(2) & "")
         Loop
         Call MouseNorm
      End If
    End If

   A.Close
   Call Activar_Inactivar_Controles(True)
   Call BarraInfo("Proceso Terminado..")
   'If Result <> FAIL Then Call Mensaje1("Archivo plano generado con �xito", 3) 'SKRV T24953 COMENTARIO
   If Result <> FAIL Then Call Mensaje1("Archivo plano generado con �xito en la carpeta c:\cntsiste.cnt\TXT_VALMER_NIIF.txt", 3) 'SKRV T24953
End Sub

'---------------------------------------------------------------------------------------
' Procedure : Cargar
' DateTime  : 15/10/2014 14:52
' Author    : sinthia_rodriguez
' Purpose   : SKRV T24261/R22366
'---------------------------------------------------------------------------------------
'
Private Sub Cargar()
   Dim texto As String
   Dim StCodArti As String
   Dim StValMerNiif As String
   Dim lin()
   ReDim lin(2)
   Dim BoErrores As Boolean
   
   Call Activar_Inactivar_Controles(False)
   
   Call BarraInfo("Cargando archivo ...")
   
   Call MouseClock
   
   Call Arch_Error
   
   If MDI_Inventarios.CMDialog1.InitDir = NUL$ Then _
      MDI_Inventarios.CMDialog1.InitDir = App.Path
      
   MDI_Inventarios.CMDialog1.filename = "" 'SKRV T24953
   MDI_Inventarios.CMDialog1.Filter = "(*.txt)|*.txt"
   MDI_Inventarios.CMDialog1.Action = 1
   
   BoErrores = False 'SKRV T24953
   
   If Dir$(MDI_Inventarios.CMDialog1.filename) = NUL$ Then Call Mensaje1("El archivo no existe!!!", 1): GoTo Salida 'SKRV T26749
   
   If MDI_Inventarios.CMDialog1.filename <> "" Then
      
'      If FileLen(MDI_Inventarios.CMDialog1.filename) = 0 Then Call _ SKRV T25015 COMENTARIO
'         Mensaje1("El archivo se encuentra vacio, cambie de archivo", 3): Exit Sub SKRV T25015 COMENTARIO
      If FileLen(MDI_Inventarios.CMDialog1.filename) = 0 Then Call Mensaje1("El archivo se encuentra vacio, cambie de archivo", 3): GoTo Salida 'SKRV T25015
      
      Open MDI_Inventarios.CMDialog1.filename For Input As #1
      
      If (BeginTran("TRANS_VALORMERINV") <> FAIL) Then
         Do While Not EOF(1)
            
            Line Input #1, texto
            If CuantosHay(texto, Coma) <> 1 Then Call _
               Mensaje1("La estructura del archivo no corresponde a la especificada", _
               3): GoTo Salida
            StCodArti = SacarUnoXPosicion(texto, Coma, 1)
            StValMerNiif = SacarUnoXPosicion(texto, Coma, 2)
            
            'If Not Val_Datos_Arch(StCodArti, StValMerNiif) Then GoTo Salida SKRV T24953 comentario
            
            If Val_Datos_Arch(StCodArti, StValMerNiif) Then 'SKRV T24953
            'SKRV T24953 se identa el texto sin ningun cambio
               lin(0) = StCodArti
               'lin(1) = Format(Nowserver, "dd/mm/yyyy hh:mm:ss")'SKRV T26044 COMENTARIO
               lin(1) = Format(Nowserver, StFormato) 'SKRV T26044
               lin(2) = StValMerNiif
               
               Result = Guardar(lin(), 2)
               
               'If Result = False Then GoTo Salida 'SKRV T25396 comentario
            'INICIO SKRV T24953
            Else
               Result = True 'SKRV T25396
               BoErrores = True
            End If
            'FIN SKRV T24953
         Loop
      End If
   'INICIO SKRV T24953
   Else
      A_Err.Close
      Call MouseNorm
      Call BarraInfo("") 'SKRV T24951
      Call Activar_Inactivar_Controles(True) 'SKRV T24951
      'JAUM T26342 INICIO
      If Dir("c:\cntsiste.cnt\" & "ERROR_VALMER_NIIF" & ".txt", vbDirectory) <> "" _
      Then Call FS_Err.DeleteFile("c:\cntsiste.cnt\" & "ERROR_VALMER_NIIF" & ".txt", True)
      'JAUM T26342 FIN
      Exit Sub
   'FIN SKRV T24953
   End If
      
   If (Result <> FAIL) Then
      If (CommitTran() = FAIL) Then
         Call RollBackTran
      End If
   Else
      Call RollBackTran
   End If
   
   Close (1)
   
   Desde = "ARTICULO LEFT JOIN VALORMERINV  on VALORMERINV.NU_ARTI_VAMEIN = ARTICULO.CD_CODI_ARTI"
   Campos = "COUNT(CD_CODI_ARTI)- COUNT(CD_CODI_VAMEIN) "
   
   'If CInt(fnDevDato(Desde, Campos, NUL$)) > 0 Then Call Mensaje1("Hacen falta " & CInt(fnDevDato(Desde, Campos, NUL$)) & " articulos.", 2)''SKRV T24953 COMENTARIO
   'SKRV T24951 INICIO
   'COMENTARIO INICIO
'   If CInt(fnDevDato(Desde, Campos, _
'      NUL$)) > 0 Then Call Mensaje1("Hacen falta " & CInt(fnDevDato(Desde, Campos, _
'      NUL$)) & " articulos." & IIf(BoErrores, vbCrLf & _
'      " Se encontraron errores en el archivo. (c:\cntsiste.cnt\ERROR_VALMER_NIIF.txt)", _
'      NUL$), 3) ''SKRV T24953
   'COMENTARIO FIN
   If CInt(fnDevDato(Desde, Campos, NUL$, True)) > 0 Then Call Mensaje1("Hacen falta " & CInt(fnDevDato(Desde, Campos, NUL$, True)) & " articulos." & IIf(BoErrores, vbCrLf & " Se encontraron errores en el archivo. (c:\cntsiste.cnt\ERROR_VALMER_NIIF.txt)", NUL$), 3) ''SKRV T24953
   'SKRV T24951  FIN
   'INICIO SKRV T24953
   If BoErrores Then
      If CInt(fnDevDato(Desde, Campos, NUL$, True)) = 0 Then Call Mensaje1(" Se encontraron errores en el archivo. (c:\cntsiste.cnt\ERROR_VALMER_NIIF.txt)", 3) 'SKRV T26582
      Call Activar_Inactivar_Controles(True) 'SKRV T25396
      A_Err.Close
      Call MouseNorm
      Call BarraInfo("") 'SKRV T26049
      Exit Sub
   End If
   'FIN SKRV T24953
   
   Call MouseNorm
   
   Call BarraInfo("Proceso Terminado..")
   'JAUM T26342 INICIO SE DEJA LA SIGUIENTE LINEA EN COMENTARIO
   'If Result <> FAIL Then Call Mensaje1("Archivo plano cargado con �xito", 3)
   If Result <> FAIL Then
      A_Err.Close
      If Dir("c:\cntsiste.cnt\" & "ERROR_VALMER_NIIF" & ".txt", vbDirectory) <> "" _
      Then Call FS_Err.DeleteFile("c:\cntsiste.cnt\" & "ERROR_VALMER_NIIF" & ".txt", True)
      Call Mensaje1("Archivo plano cargado con �xito", 3)
   End If
   'JAUM T26342 FIN
   
   Call Activar_Inactivar_Controles(True) 'SKRV T25015
   
   'A_Err.Close ' SE CIERRA EL ARCHIVO DE ERRORES JAUM T26342 SE DEJA LINEA EN COMENTARIO
   Call BarraInfo("") 'SKRV T26049
   Exit Sub
Salida:
    Close (1)
    'Call Mensaje1("Error con el Archivo", 1) SKRV T25015 COMENTARIO
    Call Activar_Inactivar_Controles(True) 'SKRV T25015
    Call BarraInfo("Error con el Archivo..")
    Call MouseNorm
    Call RollBackTran
    A_Err.Close ' SE CIERRA EL ARCHIVO DE ERRORES
    If Dir("c:\cntsiste.cnt\" & "ERROR_VALMER_NIIF" & ".txt", vbDirectory) <> "" Then Call FS_Err.DeleteFile("c:\cntsiste.cnt\" & "ERROR_VALMER_NIIF" & ".txt", True) 'SKRV T25396
End Sub







Private Sub CmdLimpia_Click()
   Call Limpiar 'SKRV T24261/R22366
End Sub

Private Sub Form_Load()
   'SKRV T24261/R22366 inicio
   Call CenterForm(MDI_Inventarios, Me)
   txtNombre.Enabled = False
   txtFecha.Text = Format(Nowserver, "dd/mm/yyyy")
   'SKRV T24261/R22366 fin
   'SKRV T26208 inicio
   Call Leer_Permisos(Me.Name, SCmd_Options(0), SCmd_Options(1), SCmd_Options(3), FrmMenu.StNomForm)
   SCmd_Options(6).Enabled = SCmd_Options(0).Enabled
   SCmd_Options(5).Enabled = SCmd_Options(3).Enabled
   'SKRV T26208 fin
   Call Get_FormatoFecha 'SKRV T26044
End Sub

Private Sub SCmd_Options_Click(Index As Integer)
   'SKRV T24261/R22366 INICIO
   Dim lin()
   ReDim lin(2)
   Select Case Index
      Case 0:
            lin(0) = txtCodigo
            lin(1) = Format(txtFecha, "dd/mm/yyyy hh:mm:ss")
            lin(2) = txtValMercado
            If Guardar(lin(), 1) = False Then Call Mensaje1("Error Almacenando la informacion.", 1)
      Case 1: Borrar
      Case 2: txtCodigo.Text = Seleccion("ARTICULO", "NO_NOMB_ARTI", _
         "CD_CODI_ARTI,NO_NOMB_ARTI", "ARTICULOS", NUL$)
              txtCodigo.Enabled = True
              txtCodigo.SetFocus
              txtCodigo_LostFocus 'DLCC T26435 para que al cargar el codigo cargue de una vez los dem�s datos
      Case 3: Call Listar("InfValMerNIIF.rpt", "ARTICULO", "CD_CODI_ARTI", "NO_NOMB_ARTI", "Art�culos", NUL$, NUL$, NUL$, NUL$, NUL$)
               Me.SetFocus
      Case 4: Unload Me
      Case 5: Call Descargar
      Case 6: Call Cargar
   End Select
   'SKRV T24261/R22366 FIN
End Sub

Private Sub SCmd_Options_KeyPress(Index As Integer, KeyAscii As Integer)
   Call Cambiar_Enter(KeyAscii) 'SKRV T24261/R22366
End Sub

Private Sub txtCodigo_KeyPress(KeyAscii As Integer)
   Call Cambiar_Enter(KeyAscii) 'SKRV T24261/R22366
End Sub

Private Sub txtCodigo_LostFocus()
   
   'txtNombre.Text = fnDevDato("ARTICULO", "NO_NOMB_ARTI", "CD_CODI_ARTI = '" & txtCodigo & "'") 'SKRV T24951 COMENTARIO
   txtNombre.Text = fnDevDato("ARTICULO", "NO_NOMB_ARTI", "CD_CODI_ARTI = '" & txtCodigo & "'", True) 'SKRV T24951
   
   'If TxtNombre.Text = NUL$ And TxtCodigo <> NUL$ Then Call Mensaje1("El Articulo no existe.", 1): Exit Sub 'SKRV T24953 SKRV T24999 COMENTARIO
   If txtNombre.Text = NUL$ And txtCodigo <> NUL$ Then Call Mensaje1("El Articulo no existe.", 1): txtCodigo.Text = NUL$: txtCodigo.SetFocus: Exit Sub    'SKRV T24999
   
   ReDim Arr(3)
   
   Desde = "VALORMERINV"
   Campos = "NU_ARTI_VAMEIN"
   Campos = Campos & ", FE_FECH_VAMEIN"
   Campos = Campos & ", NU_VALMER_VAMEIN"
   
   Condicion = "NU_ARTI_VAMEIN = '" & txtCodigo & "'"
   
   Result = LoadData(Desde, Campos, Condicion, Arr())
   'JAUM T26317 INICIO SE DEJA LA SIGUIENTE LINEA EN COMENTARIO
   'If Result <> FAIL And Encontro Then
   If Result <> FAIL Then
      If Encontro Then
   'JAUM T26317 FIN
         txtCodigo.Text = Arr(0)
         txtFecha.Text = Format(Arr(1), "dd/mm/yyyy")
         txtValMercado.Text = Arr(2)
   'JAUM T26317 INICIO
      Else
         txtValMercado.Text = ""
      End If
   'JAUM T26317 FIN
   End If
   If txtCodigo <> NUL$ Then
      txtCodigo.Enabled = False
   End If
End Sub



Private Sub txtFecha_KeyPress(KeyAscii As Integer)
   Call Cambiar_Enter(KeyAscii) 'SKRV T24261/R22366
   Call ValKeyFecha(KeyAscii, txtFecha) 'SKRV T24953
End Sub


'---------------------------------------------------------------------------------------
' Procedure : txtFecha_LostFocus
' DateTime  : 04/12/2014 16:39
' Author    : sinthia_rodriguez
' Purpose   : SKRV T24997
' Se llama a la funcion ValFecha para que ingrese una fecha correcta
'---------------------------------------------------------------------------------------
'
Private Sub txtFecha_LostFocus()
   Call ValFecha(txtFecha, 1) 'SKRV T24997
End Sub


Private Sub txtNombre_KeyPress(KeyAscii As Integer)
   Call Cambiar_Enter(KeyAscii) 'SKRV T24261/R22366
End Sub

'---------------------------------------------------------------------------------------
' Procedure : Activar_Inactivar_Controles
' DateTime  : 15/10/2014 15:00
' Author    : sinthia_rodriguez
' Purpose   : SKRV T24261/R22366
'---------------------------------------------------------------------------------------
'
Private Sub Activar_Inactivar_Controles(boEstado As Boolean)
   txtFecha.Enabled = boEstado
   txtCodigo.Enabled = boEstado
   txtValMercado.Enabled = boEstado
End Sub



'---------------------------------------------------------------------------------------
' Procedure : Valida_Datos
' DateTime  : 15/10/2014 15:04
' Author    : sinthia_rodriguez
' Purpose   : SKRV T24261/R22366   -true = exito
'---------------------------------------------------------------------------------------
'
Private Function Valida_Datos() As Boolean

   Dim bResult As Boolean
   Dim StMensaje As String
   Dim InCobjetos As Integer
   
   bResult = True
   
   If txtFecha.Text = NUL$ Then StMensaje = StMensaje & IIf(InCobjetos = 0, " Fecha", _
      ", Fecha"): InCobjetos = InCobjetos + 1: bResult = False
      
   'If txtCodigo.Text = NUL$ Then StMensaje = StMensaje & IIf(InCobjetos = 0, " Codigo", ", Codigo"): InCobjetos = InCobjetos + 1: bResult = False 'SKRV T24953 COMENTARIO
   If txtCodigo.Text = NUL$ Then StMensaje = StMensaje & IIf(InCobjetos = 0, " C�digo", ", C�digo"): InCobjetos = InCobjetos + 1: bResult = False 'SKRV T24953
      
   If txtNombre.Text = NUL$ Then StMensaje = StMensaje & IIf(InCobjetos = 0, " Nombre", _
      ", Nombre"): InCobjetos = InCobjetos + 1: bResult = False
      
   If txtValMercado.Text = NUL$ Then StMensaje = StMensaje & IIf(InCobjetos = 0, _
      " Valor mercado", ", Valor mercado"): InCobjetos = InCobjetos + 1: bResult = False
   
   If InCobjetos = 1 Then
      StMensaje = "El siguiente campo no ha sido diligenciado" & StMensaje
   Else
      StMensaje = "Los siguientes campos no han sido diligenciados" & StMensaje
   End If
   
   
   If bResult = False Then Call Mensaje1(StMensaje, 1)
   
   Valida_Datos = bResult

End Function
'---------------------------------------------------------------------------------------
' Procedure : BarraInfo
' DateTime  : 15/10/2014 16:41
' Author    : sinthia_rodriguez
' Purpose   : SKRV T24261/R22366
'---------------------------------------------------------------------------------------
'
Sub BarraInfo(StInfo As String)
    StatusBar.Panels(1).Text = StInfo
    DoEvents
End Sub


Private Sub txtValMercado_Change()
   'SKRV T24261/R22366 INICIO
   txtValMercado = ValDecimal(txtValMercado, 2)
   'SKRV T24261/R22366 FIN
End Sub

Private Sub txtValMercado_KeyPress(KeyAscii As Integer)
   Call Cambiar_Enter(KeyAscii) 'SKRV T24261/R22366
   'Call ValKeyNum(KeyAscii) 'SKRV T24261/R22366
End Sub



'---------------------------------------------------------------------------------------
' Procedure : Val_Datos_Arch
' DateTime  : 16/10/2014 14:33
' Author    : sinthia_rodriguez
' Purpose   : SKRV T24261/R22366
' Retorna True si los datos son correctos
' Valida si los el articulo existe y que el valor del mercado no supere el indicado
'---------------------------------------------------------------------------------------
'
Private Function Val_Datos_Arch(StCodArti As String, StValMerNiif As String) As Boolean
   
   Dim StTamVal() As String
   
   'Result = FAIL 'SKRV T26049 COMENTARIO
   Result = SUCCEED 'SKRV T26049
   
   ReDim Arr(0)
   'Arr(0) = fnDevDato("ARTICULO", "COUNT(CD_CODI_ARTI)", "CD_CODI_ARTI = '" & StCodArti & "'")'SKRV T24951 COMENTARIO
   Arr(0) = fnDevDato("ARTICULO", "COUNT(CD_CODI_ARTI)", "CD_CODI_ARTI = '" & StCodArti & "'", True) 'SKRV T24951
   'SKRV T26049 INICIO
   'If Arr(0) <> NUL$ Then
'      Result = SUCCEED
'   Else
   If Arr(0) = NUL$ Or Arr(0) = "0" Then
   'SKRV T26049 FIN
      A_Err.WriteLine ("El articulo " & StCodArti & " no existe.")
      Result = FAIL 'SKRV T26049
   End If
   
   If IsNumeric(StValMerNiif) Then
      'If Len(StValMerNiif) < 10 Then SKRV T24766 COMENTARIO
      If Len(StValMerNiif) < 16 Then 'SKRV T24766
         'Result = SUCCEED 'SKRV T26049 COMENTARIO
      Else
         Result = FAIL
         A_Err.WriteLine ("El valor del articulo " & StCodArti & " es superior al permitido.")
      End If
      'SKRV T26049 INICIO
      If CDbl(StValMerNiif) < 0 Then
         Result = FAIL
         A_Err.WriteLine ("El valor del articulo " & StCodArti & " es menor a 0.")
      End If
      'SKRV T26049 FIN
   Else
      Result = FAIL
      A_Err.WriteLine ("El valor del articulo " & StCodArti & " no es correcto.")
   End If
   
   StTamVal = Split(StValMerNiif, ".")
   If UBound(StTamVal) = 1 Then
      If Len(StTamVal(1)) > 2 Then
         Result = FAIL
         A_Err.WriteLine ("El valor del articulo " & StCodArti & " debe tener solo 2 decimales.")
      End If
   End If
   
   Val_Datos_Arch = Result
   
End Function

'---------------------------------------------------------------------------------------
' Procedure : Guardar
' DateTime  : 16/10/2014 14:54
' Author    : sinthia_rodriguez
' Purpose   : SKRV T24261/R22366
'           Datos(0) = Codigo
'           Datos(1) = Fecha
'           Datos(2) = ValMer
'           InOrgn = origen de la llamada (1:SCmd_Options_Click, 2:Cargar)
'---------------------------------------------------------------------------------------
'
Private Function Guardar(Datos(), InOrgn As Integer) As Boolean

   Dim bResult As Boolean
   ReDim Arr(0)

   On Error GoTo Guardar_Error
   
   If InOrgn = 1 Then
      'If Not Valida_Datos Then Exit Function 'SKRV T 24953 COMENTARIO
      If Not Valida_Datos Then Guardar = True: Exit Function  'SKRV T 24953
   End If
   
   Desde = "VALORMERINV"
   Campos = "CD_CODI_VAMEIN"
   Condicion = "NU_ARTI_VAMEIN = '" & Datos(0) & "'"
   
   'Arr(0) = fnDevDato(Desde, Campos, Condicion)'SKRV T24951 COMENTARIO
   Arr(0) = fnDevDato(Desde, Campos, Condicion, True) 'SKRV T24951
   
   If Arr(0) = NUL$ Then
      'Agrega
      Campos = "COUNT(CD_CODI_VAMEIN)"
      'Arr(0) = fnDevDato(Desde, Campos, NUL$) 'SKRV T24951 COMENTARIO
      Arr(0) = fnDevDato(Desde, Campos, NUL$, True) 'SKRV T24951
      
      If Arr(0) = NUL$ Then Arr(0) = 0
      
      Valores = "CD_CODI_VAMEIN = " & (CInt(Arr(0)) + 1)
      Valores = Valores & ", NU_ARTI_VAMEIN = '" & Datos(0) & "'"
      'Valores = Valores & ", FE_FECH_VAMEIN = '" & Datos(1) & "'" 'SKRV T26207 COMENTARIO
      Valores = Valores & ", FE_FECH_VAMEIN = '" & Format(Datos(1), StFormato) & "'" 'SKRV T26207
      Valores = Valores & ", NU_VALMER_VAMEIN = '" & Datos(2) & "'"
      Valores = Valores & ", NU_FALMA_VAMEIN = " & IIf(InOrgn = 1, 0, 1)

      bResult = DoInsertSQL(Desde, Valores)
      
   Else
      If InOrgn = 1 Then
         If Not WarnMsg("Desea Modificar la Informaci�n?") Then Guardar = False: Exit Function
      End If
      'Actualiza
      'Valores = "FE_FECH_VAMEIN = '" & Datos(1) & "'"'SKRV T26207 COMENTARIO
      Valores = "FE_FECH_VAMEIN = '" & Format(Datos(1), StFormato) & "'" 'SKRV T26207
      Valores = Valores & " , NU_VALMER_VAMEIN = '" & Datos(2) & "'"
      
      bResult = DoUpdate(Desde, Valores, Condicion)
   End If
   
   If bResult <> FAIL And InOrgn = 1 Then Call _
      Mensaje1("Los datos se almacenaron correctamente.", 3): Call Limpiar
      
   
   Guardar = bResult

   On Error GoTo 0
   Exit Function

Guardar_Error:
   If InOrgn = 2 Then A_Err.WriteLine ("Error " & ERR.Number & " (" & ERR.Description & ") in procedure Guardar of Formulario FrmMerNiif")

End Function

'---------------------------------------------------------------------------------------
' Procedure : Arch_Error
' DateTime  : 17/10/2014 08:32
' Author    : sinthia_rodriguez
' Purpose   : SKRV T24261/R22366
'---------------------------------------------------------------------------------------
'
Private Sub Arch_Error()
   
   Set FS_Err = CreateObject("Scripting.FileSystemObject")

   If Dir("c:\cntsiste.cnt\" & "ERROR_VALMER_NIIF" & ".txt", vbDirectory) <> "" _
      Then Call FS_Err.DeleteFile("c:\cntsiste.cnt\" & "ERROR_VALMER_NIIF" & ".txt", True)

   Set A_Err = FS_Err.CreateTextFile("c:\cntsiste.cnt\" & "ERROR_VALMER_NIIF" & ".txt", _
      True)
   
End Sub









'---------------------------------------------------------------------------------------
' Procedure : Get_FormatoFecha
' DateTime  : 21/01/2015 11:11
' Author    : sinthia_rodriguez
' Purpose   : SKRV T26044
' Retorna el formato de la fecha en el servidor
'---------------------------------------------------------------------------------------
'
Private Sub Get_FormatoFecha()

   Dim VaArr() As Variant
   'JAUM T26655 Inicio se deja bloque en comentario
'   Dim Ini As Integer
'   ReDim VaArr(2)
'   Desde = "master..syslanguages"
'   Campos = "substring(dateformat,1,1),substring(dateformat,2,1),substring(dateformat,3,1)"
'   Condicion = "langid = (select value from master..sysconfigures where comment = 'default language')"
'   Result = LoadData(Desde, Campos, Condicion, VaArr)
'
'   If Result <> FAIL And Encontro Then
'      For Ini = 0 To UBound(VaArr)
'         If VaArr(Ini) = "M" Or VaArr(Ini) = "m" Then
'            StFormato = StFormato & "mm/"
'         ElseIf VaArr(Ini) = "D" Or VaArr(Ini) = "d" Then
'            StFormato = StFormato & "dd/"
'         ElseIf VaArr(Ini) = "Y" Or VaArr(Ini) = "y" Then
'            StFormato = StFormato & "yyyy/"
'         End If
'      Next
'      StFormato = Mid(StFormato, 1, Len(StFormato) - 1)
'   End If
   ReDim VaArr(0)
   Result = LoadData("", "@@Language", "", VaArr())
   If UCase(VaArr(0)) = "ESPA�OL" Then
      StFormato = "DD/MM/YYYY"
   Else
      StFormato = "YYYY/MM/DD"
   End If
   'JAUM T26655 Fin
End Sub
