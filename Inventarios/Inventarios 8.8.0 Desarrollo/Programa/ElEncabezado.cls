VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "ElEncabezado"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit

Private pRc                 As Long 'Bodega de Principal
Private OPC                 As Long 'Bodega de Opcional
Private tpo                 As Long 'Tipo de documento, autonum�rico en IN_DOCUMENTO
Private trc                 As ElTercero 'Tercero
Private esi                 As Boolean 'Es un documento interno
Private rdp                 As Boolean 'Permite al doc romper la dependencia
Private lrp                 As Boolean 'La dependencia fue rota
Private afe                 As Boolean 'Afecta inventarios
Private dcm                 As Long 'documento del que depende
Private ueo                 As Long 'Autonum�rico del documento en IN_COMPROBANTE
Private Rev                 As Long 'Autonum�rico en IN_DOCUMENTO del que lo reversa
Private MoA                 As Long 'Autonum�rico del documento original en IN_ENCABEZADO
Private Cbt                 As Long 'Consecutivo del comprobante
Private fch                 As Date 'fecha del periodo
Private cha                 As Date 'fecha del documento
Private Vcm                 As Date 'fecha de vencimiento
Private Vgn                 As Date 'fecha de vencimiento
Private cdd                 As String 'C�digo del documento
Private nom                 As String 'Nombre del documento
Private obs                 As String 'Observaciones del documento
Private eSt                 As String * 1 'estado del doc (N/M/A)(0/1/2)
Private EsNuevo             As Boolean 'Verdadero si es un documento nuevo
Private vAutoUsuario        As Long 'Autonumerico del usuario
Private vNumeroConexion     As Long 'N�mero de conexi�n
Private vContable           As Long 'Consecutivo contable 'NumeroContable
Private vCondiComercial     As String
Private vCopias As Integer 'N�mero de copias
Private LnDocDep As Long 'HRR R1700 Documentos dependientes.

'HRR R1700
Public Property Let DocumentoDep(LnDocDepe As Long)
   LnDocDep = LnDocDepe
End Property

Public Property Get DocumentoDep() As Long
    DocumentoDep = LnDocDep
End Property
'HRR R1700

Public Property Let NumeroContable(num As Long)
    vContable = num
End Property

Public Property Get NumeroContable() As Long
    NumeroContable = vContable
End Property

Public Property Get NumeroCopias() As Integer
    NumeroCopias = vCopias
End Property

Public Property Let NumeroCopias(num As Integer) ''NumeroImpresiones
    vCopias = num
End Property

Public Property Let NumeroConexion(num As Long)
    vNumeroConexion = num
End Property

Public Property Get NumeroConexion() As Long
    NumeroConexion = vNumeroConexion
End Property

Public Property Let AutoDelUSUARIO(num As Long)
    vAutoUsuario = num
End Property

Public Property Get AutoDelUSUARIO() As Long
    AutoDelUSUARIO = vAutoUsuario
End Property

Public Property Get Observaciones() As String
    Observaciones = obs
End Property

Public Property Let Observaciones(Cue As String)
    obs = Cue
End Property

Public Property Get CondicionesComerciales() As String
    CondicionesComerciales = vCondiComercial
End Property

Public Property Let CondicionesComerciales(Cue As String)
    vCondiComercial = Cue
End Property

Public Property Get CodigoDocumento() As String
    CodigoDocumento = cdd
End Property

Public Property Let CodigoDocumento(Cue As String)
    cdd = Cue
End Property

Public Property Get NombreDocumento() As String
    NombreDocumento = nom
End Property

Public Property Get EstadoDocumento() As String
    EstadoDocumento = eSt
End Property

Public Property Let EstadoDocumento(Cue As String)
    eSt = Cue
End Property

Public Property Get EsInterno() As Boolean
    EsInterno = esi
End Property

Public Property Get AfectaInventario() As Boolean
    AfectaInventario = afe
End Property

Public Property Get RomDepende() As Boolean
    RomDepende = rdp
End Property

Public Property Let EsIndependiente(bol As Boolean)
    lrp = bol
End Property

Public Property Get EsIndependiente() As Boolean
    EsIndependiente = lrp
End Property

Public Property Get CualDepende() As Long
    CualDepende = dcm
End Property

Public Property Get AutoReversa() As Long
    AutoReversa = Rev
End Property

Public Property Get FechaCierre() As Date
    FechaCierre = fch
End Property

Public Property Get FechaDocumento() As Date
    FechaDocumento = cha
End Property

Public Property Let FechaDocumento(dta As Date)
    cha = dta
End Property

Public Property Get FechaVigencia() As Date
    FechaVigencia = Vgn
End Property

Public Property Let FechaVigencia(dta As Date)
    Vgn = dta
End Property

Public Property Get FechaVencimiento() As Date
    FechaVencimiento = Vcm
End Property

Public Property Let FechaVencimiento(dta As Date)
    Vcm = dta
End Property

Public Property Get Tercero() As ElTercero
    Set Tercero = trc
End Property

Public Property Get BodegaORIGEN() As Long
    BodegaORIGEN = pRc
End Property

Public Property Let BodegaORIGEN(cla As Long)
    pRc = cla
End Property

Public Property Get AutoDelDocCARGADO() As Long
    AutoDelDocCARGADO = MoA
End Property

Public Property Let AutoDelDocCARGADO(cla As Long)
    MoA = cla
End Property

Public Property Get ConsecComprobante() As Long
    ConsecComprobante = Cbt
End Property

Public Property Let ConsecComprobante(cla As Long)
    Cbt = cla
End Property

Public Property Get NumeroCOMPROBANTE() As Long
    NumeroCOMPROBANTE = ueo
End Property

Public Property Let NumeroCOMPROBANTE(cla As Long)
    ueo = cla
End Property

Public Property Let EsNuevoDocumento(lgi As Boolean)
    EsNuevo = lgi
End Property

Public Property Get EsNuevoDocumento() As Boolean
    EsNuevoDocumento = EsNuevo
End Property

Public Property Let BodegaDESTINO(cnt As Long)
    OPC = cnt
End Property

Public Property Get BodegaDESTINO() As Long
    BodegaDESTINO = OPC
End Property

Public Property Let TipDeDcmnt(cnt As Long)
    tpo = cnt
End Property

Public Property Get TipDeDcmnt() As Long
    TipDeDcmnt = tpo
End Property

Public Function CueDeDcmnts(cbx As ComboBox) As String
    Dim ArrTemp() As Variant, CnTdr As Integer
    Campos = "NU_AUTO_BODE_RBODU, NU_AUTO_DOCU_RBODU, TX_CODI_DOCU, TX_NOMB_DOCU, " & _
        "TX_AFEC_DOCU, TX_INTE_DOCU, NU_AUTO_DOCUDEPE_DOCU, NU_AUTO_DOCUREVE_DOCU, " & _
        "TX_TIPOVAL_DOCU, TX_INDE_DOCU"
    Desde = "IN_R_BODE_DOCU, IN_DOCUMENTO"
    Condi = "NU_AUTO_DOCU_RBODU = NU_AUTO_DOCU AND NU_AUTO_BODE_RBODU=" & pRc
    cbx.Clear
    ReDim ArrTemp(9, 0)
    Result = LoadMulData(Desde, Campos, Condi, ArrTemp())
    If Result = FAIL Then GoTo Fallo
    If Not Encontro Then GoTo NOENC
    For CnTdr = 0 To UBound(ArrTemp, 2)
        cbx.AddItem ArrTemp(3, CnTdr) 'RST("TX_NOMB_DOCU")
        cbx.ItemData(cbx.NewIndex) = ArrTemp(1, CnTdr) 'RST("NU_AUTO_DOCU_RBODU")
    Next
    Exit Function
NOENC:
Fallo:
End Function

Public Function CueDeDestinos(cbx As ComboBox) As String
    Dim ArrTemp() As Variant, CnTdr As Integer
    Campos = "NU_AUTO_DOCU_DORDS, NU_AUTO_BOORG_DORDS, NU_AUTO_BODST_DORDS, NU_AUTO_BODE, " & _
        "TX_CODI_BODE, TX_NOMB_BODE"
    Desde = "IN_BODEORGDST, IN_BODEGA"
    Condi = "NU_AUTO_BODST_DORDS = NU_AUTO_BODE " & _
        "AND NU_AUTO_DOCU_DORDS=" & tpo & " AND NU_AUTO_BOORG_DORDS=" & pRc
    cbx.Clear
    ReDim ArrTemp(5, 0)
    Result = LoadMulData(Desde, Campos, Condi, ArrTemp())
    If Result = FAIL Then GoTo Fallo
    If Not Encontro Then GoTo NOENC
    For CnTdr = 0 To UBound(ArrTemp, 2)
        cbx.AddItem ArrTemp(5, CnTdr) 'RST("TX_NOMB_BODE")
        cbx.ItemData(cbx.NewIndex) = ArrTemp(2, CnTdr) 'RST("NU_AUTO_BODST_DORDS")
    Next
    Exit Function
NOENC:
Fallo:
End Function

Private Sub Class_Initialize()
    Dim Dsd As String, CMP As String
    rdp = False
    lrp = True
    'fch = #6/30/2004# FE_CIER_APLI
    cha = FechaServer()
    Vcm = FechaServer()
    eSt = "N"
    Set trc = New ElTercero
    Dim ArrCLI() As Variant
    CMP = "FE_CIER_APLI"
    Dsd = "PARAMETROS_INVE"
    ReDim ArrCLI(0)
    Result = LoadData(Dsd, CMP, NUL$, ArrCLI())
    If Result = FAIL Then Exit Sub
    If Not Encontro Then Exit Sub
    fch = ArrCLI(0)
    fch = DateAdd("d", 1, fch)
    fch = DateAdd("M", 1, fch)
    fch = DateAdd("d", -1, fch)
End Sub

Public Sub Actualiza()
    Dim ArrTemp() As Variant
    Campos = "NU_AUTO_DOCU, TX_CODI_DOCU, TX_NOMB_DOCU, TX_AFEC_DOCU, TX_INTE_DOCU, " & _
        "NU_AUTO_DOCUDEPE_DOCU, NU_AUTO_DOCUREVE_DOCU, TX_TIPOVAL_DOCU, TX_INDE_DOCU"
    Desde = "IN_DOCUMENTO"
    Condi = "NU_AUTO_DOCU=" & tpo
    ReDim ArrTemp(8)
    Result = LoadData(Desde, Campos, Condi, ArrTemp())
    If Result = FAIL Then GoTo Fallo
    If Not Encontro Then GoTo NOENC
'    ueo = ArrTemp(0) 'RST("NU_AUTO_DOCU") revisar por que se hacia, revizado, era un error
    cdd = ArrTemp(1) 'RST("TX_CODI_DOCU")
    nom = ArrTemp(2) 'RST("TX_NOMB_DOCU")
    esi = IIf(UCase(ArrTemp(4)) = "S", True, False) 'RST("TX_INTE_DOCU")
    afe = IIf(UCase(ArrTemp(3)) = "S", True, False) 'RST("TX_AFEC_DOCU")
    Rev = ArrTemp(6) 'RST("NU_AUTO_DOCUDEPE_DOCU")
    dcm = ArrTemp(5) 'RST("NU_AUTO_DOCUREVE_DOCU")
    rdp = IIf(UCase(ArrTemp(8)) = "S", True, False) 'RST("TX_INDE_DOCU")
    Exit Sub
NOENC:
Fallo:
End Sub

Public Sub CueDeBodegas(cbx As ComboBox)
    Dim ArrTemp() As Variant, CnTdr As Integer
    Campos = "NU_AUTO_BODE,TX_NOMB_BODE"
    Desde = "IN_BODEGA"
    Condi = ""
    cbx.Clear
    ReDim ArrTemp(1, 0)
    Result = LoadMulData(Desde, Campos, Condi, ArrTemp())
    If Result = FAIL Then GoTo Fallo
    If Not Encontro Then GoTo NOENC
    For CnTdr = 0 To UBound(ArrTemp, 2)
        cbx.AddItem ArrTemp(1, CnTdr) 'RST("TX_NOMB_BODE")
        cbx.ItemData(cbx.NewIndex) = ArrTemp(0, CnTdr) 'RST("NU_AUTO_BODE")
    Next
    Exit Sub
NOENC:
Fallo:
End Sub

Public Sub ConsecutivoXBodega(cbx As ComboBox)
    Dim ArrTemp() As Variant, CnTdr As Integer
    If esi Then
        Campos = "DISTINCT NU_AUTO_COMP, TX_NOMB_COMP, TX_PRFJ_COMP"
        Desde = "IN_BODEORGDST, IN_COMPROBANTE"
        Condi = "NU_AUTO_COMP_DORDS = NU_AUTO_COMP " & _
            "AND NU_AUTO_DOCU_DORDS=" & tpo & " AND NU_AUTO_BOORG_DORDS=" & pRc
    Else
        Campos = "DISTINCT NU_AUTO_COMP, TX_NOMB_COMP, TX_PRFJ_COMP"
        Desde = "IN_COMPROBANTE"
        Condi = "NU_AUTO_DOCU_COMP=" & tpo
    End If
    ReDim ArrTemp(2, 0)
    Result = LoadMulData(Desde, Campos, Condi, ArrTemp())
    If Result = FAIL Then GoTo Fallo
    If Not Encontro Then GoTo NOENC
    For CnTdr = 0 To UBound(ArrTemp, 2)
        cbx.AddItem ArrTemp(1, CnTdr) 'RST("TX_NOMB_COMP")
        cbx.ItemData(cbx.NewIndex) = ArrTemp(0, CnTdr) 'RST("NU_AUTO_COMP")
    Next
    Exit Sub
NOENC:
Fallo:
End Sub

Public Sub NumeroImpresiones()
    Result = DoUpdate("IN_ENCABEZADO", "NU_COPI_ENCA=" & vCopias + 1, "NU_AUTO_ENCA=" & MoA)
End Sub

