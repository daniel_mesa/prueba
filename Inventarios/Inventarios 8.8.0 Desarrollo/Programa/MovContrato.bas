Attribute VB_Name = "MovContrato"
Option Explicit
Public Enum TipoRegistro
   Agregar = 1       'Agrega un registro a partir de uno existente
   Nuevo = 2         'Crea un registro
   ModDescuento = 3  'Crea un nuevo registro y modifica el valor del descuento de un registro existente
   ModOtrDes = 4     'Modifica el valor de otro descuento
   ModOtrRec = 5     'Modifica el valor de otro recargo
End Enum
Public rs   As ADODB.Recordset
Public No_Factura    As String
Public No_Documento  As Long
Public Logo          As String

Public Function Duplicar_DeclaSalud(ByRef AutoConAfilAnt As Long, ByRef AutoContAfilNue) As Integer
Dim arrTmp() As Variant
Dim Cad As Variant
Dim i As Integer

   ReDim arrTmp(7)
   Condicion = "NU_AUTO_COPN_DESA =" & AutoConAfilAnt
   Campos = "NU_AUTO_DESA,NU_AUTO_COPN_DESA,NU_AUTO_CIUD_DILI_DESA,FE_DILI_DESA,"
   Campos = Campos & "TX_REME_DESA,NU_AUTO_CIUD_AUDI_DESA,FE_AUDI_DESA,NU_AUTO_CONE_DESA"
   Result = LoadData("DECLARACION_SALUD", Campos, Condicion, arrTmp())
   If Result <> FAIL And Encontro Then
      Cad = arrTmp(0)   'A
      Valores = "NU_AUTO_COPN_DESA =" & AutoContAfilNue & Coma
      Valores = Valores & "NU_AUTO_CIUD_DILI_DESA=" & CLng(arrTmp(2)) & Coma
      Valores = Valores & "FE_DILI_DESA=" & Comi & arrTmp(3) & Comi & Coma
      Valores = Valores & "TX_REME_DESA=" & Comi & arrTmp(4) & Comi & Coma
      Valores = Valores & "NU_AUTO_CIUD_AUDI_DESA=" & CLng(arrTmp(5)) & Coma
      Valores = Valores & "FE_AUDI_DESA=" & Comi & arrTmp(6) & Comi & Coma
      Valores = Valores & "NU_AUTO_CONE_DESA=" & NumConex
      Result = DoInsertSQL("DECLARACION_SALUD", Valores)

      If Result <> FAIL Then
         ReDim arr(0)
         Condicion = "NU_AUTO_COPN_DESA =" & AutoContAfilNue
         Result = LoadData("DECLARACION_SALUD", "NU_AUTO_DESA", Condicion, arr())   'arr(0) N
         If Result <> FAIL And Encontro Then
            ReDim arrTmp(11, 0)
            Condicion = "NU_AUTO_DESA_DEDE =" & CLng(Cad) 'A
            Campos = "NU_AUTO_DEDE,NU_AUTO_DESA_DEDE,TX_PREG_DEDE,TX_RESP_DEDE,FE_FECH_DEDE,"
            Campos = Campos & "NU_AUTO_DIAG_DEDE,TX_VALO_DEDE,NU_UNID_DEDE,TX_TIPO_DEDE,"
            Campos = Campos & "TX_NOMB_MEDI_DEDE,TX_DIRE_MEDI_DEDE,TX_TELE_MEDI_DEDE"
            Result = LoadMulData("DECLARACION_DETALLE", Campos, Condicion, arrTmp())
            If Result <> FAIL And Encontro Then ' /************** CREA UN NUEVO REGISTRO DE DECLARACION PARA LA NUEVA DECLARACION DE SALUD
               For i = 0 To UBound(arrTmp, 2)
                  Valores = "NU_AUTO_DESA_DEDE = " & CLng(arr(0)) & Coma 'N
                  Valores = Valores & "TX_PREG_DEDE=" & Comi & arrTmp(2, i) & Comi & Coma
                  Valores = Valores & "TX_RESP_DEDE=" & Comi & arrTmp(3, i) & Comi & Coma
                  Valores = Valores & "FE_FECH_DEDE=" & Comi & arrTmp(4, i) & Comi & Coma
                  If arrTmp(5, i) = "" Then
                     Valores = Valores & "NU_AUTO_DIAG_DEDE= NULL" & Coma
                  Else
                     Valores = Valores & "NU_AUTO_DIAG_DEDE=" & CLng(arrTmp(5, i)) & Coma
                  End If
                  Valores = Valores & "TX_VALO_DEDE=" & Comi & arrTmp(6, i) & Comi & Coma
                  Valores = Valores & "NU_UNID_DEDE=" & arrTmp(7, i) & Coma
                  Valores = Valores & "TX_TIPO_DEDE=" & Comi & arrTmp(8, i) & Comi & Coma
                  Valores = Valores & "TX_NOMB_MEDI_DEDE=" & Comi & arrTmp(9, i) & Comi & Coma
                  Valores = Valores & "TX_DIRE_MEDI_DEDE=" & Comi & arrTmp(10, i) & Comi & Coma
                  Valores = Valores & "TX_TELE_MEDI_DEDE=" & Comi & arrTmp(11, i) & Comi
                  Result = DoInsertSQL("DECLARACION_DETALLE", Valores)
               Next
            End If
            If Result <> FAIL Then
               ' /************** CREA UN NUEVO REGISTRO DE EXCLUSION PARA LA NUEVA DECLARACION DE SALUD
               Condicion = "NU_AUTO_DESA_EXCL =" & CDbl(Cad) 'A
               ReDim arrTmp(3, 0)
               Campos = "NU_AUTO_DESA_EXCL,NU_AUTO_CIE_EXCL,TX_VALO_EXCL,TX_TIPO_EXCL"
               Result = LoadMulData("EXCLUSION", Campos, Condicion, arrTmp())
               If Result <> FAIL Then
                  For i = 0 To UBound(arrTmp, 2)
                     Valores = "NU_AUTO_DESA_EXCL=" & CLng(arr(0)) & Coma
                     Valores = Valores & "NU_AUTO_CIE_EXCL=" & CLng(arrTmp(1, i)) & Coma
                     Valores = Valores & "TX_VALO_EXCL=" & Comi & arrTmp(2, i) & Comi & Coma
                     Valores = Valores & "TX_TIPO_EXCL =" & Comi & arrTmp(3, i) & Comi
                     Result = DoInsertSQL("EXCLUSION", Valores)
                  Next
               End If
            End If
         End If
      End If
   End If
   
   Duplicar_DeclaSalud = Result
End Function
Public Sub Valor_Cuota_Contrato(ByVal AutCont As Long, ByVal AutNumCuota As Long, ByRef ArrValCuot() As Variant, Optional condaux As String, Optional Tipo As String, Optional Mensual As Boolean)
Dim ArrTemp() As Variant
Dim meses As Long
Dim dias As Long

ReDim ArrValCuot(3)
   
   If Tipo = Empty Then
      ReDim ArrTemp(5)
      If ArrTemp(0) = "" Then ArrTemp(0) = 0: If ArrTemp(1) = "" Then ArrTemp(1) = 0: If ArrTemp(2) = "" Then ArrTemp(2) = 0:  If ArrTemp(3) = "" Then ArrTemp(3) = 0
      Campos = "SUM(NU_BASE_COCU),SUM(NU_RECA_COCU+NU_OTRE_COCU),SUM(NU_DESC_COCU+NU_OTDE_COCU), SUM(NU_NETO_COCU),FE_INIC_CUOT,FE_VENC_CUOT"
   Else
      ReDim ArrTemp(7)
      ReDim ArrValCuot(5)
      If ArrTemp(0) = "" Then ArrTemp(0) = 0: If ArrTemp(1) = "" Then ArrTemp(1) = 0: If ArrTemp(2) = "" Then ArrTemp(2) = 0:  If ArrTemp(3) = "" Then ArrTemp(3) = 0: If ArrTemp(4) = "" Then ArrTemp(4) = 0: If ArrTemp(5) = "" Then ArrTemp(5) = 0
      Campos = "SUM(NU_BASE_COCU),SUM(NU_RECA_COCU),SUM(NU_DESC_COCU), SUM(NU_NETO_COCU),SUM(NU_OTRE_COCU),SUM(NU_OTDE_COCU),FE_INIC_CUOT,FE_VENC_CUOT"
   End If
   
   Desde = "CUOTA_CONTRATO,R_CONTRATO_AFILIADO,CONTRATO_DETALLE_CUOTA"
   condi = "NU_AUTO_CONT_CUOT=NU_AUTO_CONT_COPN"
   condi = condi & " AND NU_AUTO_CONT_CUOT=NU_AUTO_CONT_COCU AND NU_AUTO_PENA_COPN=NU_AUTO_PENA_COCU"
   condi = condi & " AND NU_AUTO_CONT_CUOT=" & AutCont & " AND NU_AUTO_CUOT=" & AutNumCuota
   condi = condi & " AND ((TX_ESTA_COPN='A' AND FE_INGR_COPN<=FE_VENC_CUOT)"
   condi = condi & " OR (TX_ESTA_COPN='E' AND FE_EXCL_COPN>=FE_VENC_CUOT))"
   'condi = condi & " AND FE_INIC_COCU<=FE_VENC_CUOT"
   'condi = condi & " AND FE_FINA_COCU>=FE_VENC_CUOT"
   'mod farm mar-25
   condi = condi & " AND ((FE_INIC_COCU<=FE_VENC_CUOT"
   condi = condi & " AND FE_FINA_COCU>=FE_VENC_CUOT)"
   condi = condi & " OR  (FE_INIC_COCU<=FE_VENC_CUOT AND FE_FINA_COCU<= FE_VENC_CUOT))"
   
   If condaux <> NUL$ Then condi = condi & " AND " & condaux
   condi = condi & " GROUP BY FE_INIC_CUOT,FE_VENC_CUOT"
   If Result <> FAIL Then Result = LoadData(Desde, Campos, condi, ArrTemp())
   If Result <> FAIL And Encontro Then
      If Tipo <> Empty Then
         meses = Cantidad_Meses(CDate(ArrTemp(6)), CDate(ArrTemp(7)))
      Else
         meses = Cantidad_Meses(CDate(ArrTemp(4)), CDate(ArrTemp(5)))
      End If
      If Mensual = True Then meses = 1
      ArrValCuot(0) = CDbl(ArrTemp(0)) * meses 'BASE
      ArrValCuot(1) = CDbl(ArrTemp(1)) * meses 'RECARGOS
      ArrValCuot(2) = CDbl(ArrTemp(2)) * meses 'DESCUENTOS
      ArrValCuot(3) = CDbl(ArrTemp(3)) * meses  'NETO
      If Tipo <> Empty Then
         ArrValCuot(4) = CDbl(ArrTemp(4)) * meses 'DESCUENTOS
         ArrValCuot(5) = CDbl(ArrTemp(5)) * meses  'NETO
      End If
   End If
End Sub


'Determina el valor del contrato, cuando aun este no ha sido cerrado
Public Function Valor_Temporal_Contrato(ByVal AutCont As Long, ByRef ArrValCuot() As Variant) As Integer
Dim ArrTemp() As Variant
ReDim ArrTemp(3)
ReDim ArrValCuot(3)
Dim meses As Integer

   ReDim arr(3)
   Condicion = "NU_AUTO_CONT=" & AutCont
   Result = LoadData("CONTRATO", "FE_INIC_CONT,FE_FINA_CONT,TX_ETAP_CONT,NU_AUTO_NEGO_CONT", Condicion, arr)
   If Result = FAIL Or Not Encontro Then Exit Function
   
   meses = Cantidad_Meses(CDate(arr(0)), CDate(arr(1)))
   Campos = "SUM(NU_BASE_COCU),SUM(NU_RECA_COCU+NU_OTRE_COCU),SUM(NU_DESC_COCU+NU_OTDE_COCU),SUM(NU_NETO_COCU)"
   Desde = "CONTRATO,R_CONTRATO_AFILIADO,CONTRATO_DETALLE_CUOTA"
   condi = "NU_AUTO_CONT=NU_AUTO_CONT_COPN"
   condi = condi & " AND NU_AUTO_CONT=NU_AUTO_CONT_COCU AND NU_AUTO_PENA_COPN=NU_AUTO_PENA_COCU"
   condi = condi & " AND NU_AUTO_CONT=" & AutCont
   condi = condi & " AND ((TX_ESTA_COPN='A')"
   condi = condi & " OR (TX_ESTA_COPN='E' ))"
   If Result <> FAIL Then Result = LoadData(Desde, Campos, condi, ArrTemp())
   If Result <> FAIL And Encontro Then
      If ArrTemp(0) = "" Then ArrTemp(0) = 0
      If ArrTemp(1) = "" Then ArrTemp(1) = 0
      If ArrTemp(2) = "" Then ArrTemp(2) = 0
      If ArrTemp(3) = "" Then ArrTemp(3) = 0
      ArrValCuot(0) = CDbl(ArrTemp(0)) * meses 'BASE
      ArrValCuot(1) = CDbl(ArrTemp(1)) * meses  'RECARGOS
      ArrValCuot(2) = CDbl(ArrTemp(2)) * meses 'DESCUENTOS
      ArrValCuot(3) = CDbl(ArrTemp(3)) * meses 'NETO
   End If
   Valor_Temporal_Contrato = Result
End Function

'Retorna la ultima cuota de una persona
Public Function Ultima_Cuota_Persona(ByVal Contrato As Long, ByVal afiliado As Long, ByRef ArrValAfiliado As Variant) As Integer
ReDim arr(6)
ReDim ArrValAfiliado(6)

   Campos = "NU_BASE_COCU,NU_RECA_COCU,NU_OTRE_COCU,NU_DESC_COCU,NU_OTDE_COCU,NU_NETO_COCU,FE_INIC_COCU"
   Desde = "CONTRATO_DETALLE_CUOTA"
   condi = "NU_AUTO_CONT_COCU=" & Contrato
   condi = condi & " AND NU_AUTO_PENA_COCU=" & afiliado
   condi = condi & " ORDER BY FE_FINA_COCU DESC"
   Result = LoadData(Desde, Campos, condi, arr)
   If Result <> FAIL Then
      If Encontro Then
         If arr(0) = "" Then arr(0) = 0
         If arr(1) = "" Then arr(1) = 0
         If arr(2) = "" Then arr(2) = 0
         If arr(3) = "" Then arr(3) = 0
         If arr(4) = "" Then arr(4) = 0
         If arr(5) = "" Then arr(5) = 0
         ArrValAfiliado(0) = CDbl(arr(0)) 'BASE
         ArrValAfiliado(1) = CDbl(arr(1)) 'RECARGOS
         ArrValAfiliado(2) = CDbl(arr(2)) 'DESCUENTOS
         ArrValAfiliado(3) = CDbl(arr(3)) 'NETO
         ArrValAfiliado(4) = CDbl(arr(4)) 'OTRO DESCUENTOS
         ArrValAfiliado(5) = CDbl(arr(5)) 'OTRO RECARGO
         ArrValAfiliado(6) = CDate(arr(6)) 'Fecha de Inicio
      Else
         Call Mensaje1("No se encontro el valor de la cuota del afiliado", 3)
         Result = FAIL
      End If
   End If
   Ultima_Cuota_Persona = Result
End Function

'Retorna el valor cuota de una persona en una fecha determinada
Public Function Valor_Cuota_Persona(ByVal Contrato As Long, ByVal afiliado As Long, ByRef ArrValAfiliado As Variant, ByVal Fecha As Date) As Integer
ReDim arr(3)
ReDim ArrValAfiliado(3)

   Campos = "NU_BASE_COCU,NU_RECA_COCU+NU_OTRE_COCU,NU_DESC_COCU+NU_OTDE_COCU,NU_NETO_COCU"
   Desde = "CONTRATO_DETALLE_CUOTA"
   condi = "NU_AUTO_CONT_COCU=" & Contrato
   condi = condi & " AND NU_AUTO_PENA_COCU=" & afiliado
   condi = condi & " AND ((FE_INIC_COCU<=" & FFechaCon(CDate(Fecha))
   condi = condi & " AND FE_FINA_COCU>=" & FFechaCon(CDate(Fecha)) & SParC
   condi = condi & " OR (FE_INIC_COCU>=" & FFechaCon(CDate(Fecha))
   condi = condi & " AND FE_FINA_COCU>=" & FFechaCon(CDate(Fecha)) & SParC & SParC
   condi = condi & " ORDER BY FE_FINA_COCU DESC"
   Result = LoadData(Desde, Campos, condi, arr)
   If Result <> FAIL Then
      If Encontro Then
         If arr(0) = "" Then arr(0) = 0
         If arr(1) = "" Then arr(1) = 0
         If arr(2) = "" Then arr(2) = 0
         If arr(3) = "" Then arr(3) = 0
         ArrValAfiliado(0) = CDbl(arr(0)) 'BASE
         ArrValAfiliado(1) = CDbl(arr(1)) 'RECARGOS
         ArrValAfiliado(2) = CDbl(arr(2)) 'DESCUENTOS
         ArrValAfiliado(3) = CDbl(arr(3)) 'NETO
      'Else
      '   Call Mensaje1("No se encontro el valor de la cuota del afiliado", 3)
      '   Result = FAIL
      End If
   End If
   Valor_Cuota_Persona = Result
End Function

'Tipo:  D, detallado
Public Function Valor_Contrato(ByVal AutCont As Long, ByRef arrvalcont() As Variant, Optional Tipo As String) As Integer
Dim arrcuotas()   As Variant
Dim ArrTemp()     As Variant
Dim i             As Integer
ReDim ArrTemp(1, 0)
ReDim arrcuotas(1, 0)

   If Tipo = Empty Then
      ReDim arrvalcont(3)
      arrvalcont(0) = 0: arrvalcont(1) = 0: arrvalcont(2) = 0: arrvalcont(3) = 0
   Else
      ReDim arrvalcont(5)
      arrvalcont(0) = 0: arrvalcont(1) = 0: arrvalcont(2) = 0: arrvalcont(3) = 0: arrvalcont(4) = 0: arrvalcont(5) = 0:
   End If
   
   Campos = "NU_AUTO_CUOT,TX_ESTA_CUOT"
   condi = "NU_AUTO_CONT_CUOT=" & AutCont & " ORDER BY NU_AUTO_CUOT"
   Result = LoadMulData("CUOTA_CONTRATO", Campos, condi, arrcuotas())
   If Result <> FAIL And Encontro Then
      For i = 0 To UBound(arrcuotas, 2)
         ReDim ArrTemp(0)
         Call Valor_Cuota_Contrato(AutCont, CLng(arrcuotas(0, i)), ArrTemp(), NUL$, Tipo)
         arrvalcont(0) = arrvalcont(0) + CDbl(ArrTemp(0)) 'BASE
         arrvalcont(1) = arrvalcont(1) + CDbl(ArrTemp(1)) 'RECARGOS
         arrvalcont(2) = arrvalcont(2) + CDbl(ArrTemp(2)) 'DESCUENTOS
         arrvalcont(3) = arrvalcont(3) + CDbl(ArrTemp(3)) 'NETO
         If Tipo <> Empty Then
            arrvalcont(4) = arrvalcont(4) + CDbl(ArrTemp(4)) 'otros recargos
            arrvalcont(5) = arrvalcont(5) + CDbl(ArrTemp(5)) 'otros descuentos
         End If
      Next
   End If
   Valor_Contrato = Result
End Function

'/************************************************************
'Rutina de facturacion
'/**************************************************************
'Parametros:
'        TipoDoc: Tipo de documento a Generar(Factura,Factura Masivos)
'        EstFactura: Estructura de los numeros de factura
'        Inicio : Fecha de inicio de la cuota a buscar
'        Final:   Fecha final de busqueda (inicio de la cuota)
'        arrFacturacion(), arreglo de 1 dimension, que contiene, le autonumerico de las cuotas a facturar
'        AutoSucural: Sucursal que se esta facturando
'        Cancel= variable externa, que controla cuando detener la facturacion
'        Interfase: 1, si esta activa la interfase, 0 si no
'        auxfactura:variable para retornar el n�mero de la factura generada
'        Imprimir: indica si la factura se debe imprimir(TRUE) o no(FALSE)
'        Enviar: 0 -Impresora, 1-Pantalla
'        Prg_Bar: Barra de progreso
'        Transaccion: True, si maneja la transaccion la funcion
Public Function Facturacion(ByVal TipoDoc As Documento, ByRef EstFactura As Numeracion, ByVal Inicio As Date, _
         ByVal final As Date, ByVal AutoSucursal As Long, ByRef arrfacturacion() As Variant, ByRef Cancel As Boolean, _
         ByVal Interfase As Long, ByRef auxfactura As Long, ByVal Imprimir As Boolean, ByVal Enviar As Integer, _
         ByVal Transaccion As Boolean, Optional Prg_Bar As ProgressBar) As Integer
Dim desdeF           As Long     'Numero inicial de la facturacion
Dim hastaF           As Long     'Numero final de la facutracion
Dim Nfactura          As String   'Numero de la factura
Dim AutNumTitular    As Long     'Autonumerico de la tabla tercero del titular
Dim k                As Integer
Dim h                As Long
Dim arrfac()         As Variant  'Arreglo con informacion de factura
Dim arrcuotas()      As Variant  'Arreglo con informacion sobre cuotas
Dim arrproducto()    As Variant  'Arreglo para productos
Dim Texto            As String   'Almacena temporalmente los nombres que se guardan en el arreglo de facturas
Dim meses            As Long
Dim arrTotND()       As Variant  'Arreglo para el total de las Notas D�bito
Dim arrTotNC()       As Variant  'Arreglo para el total de las Notas Cr�dito
Dim arrDetNDNC()     As Variant  'Arreglo para el detalle de las Notas D�bito y las notas cr�dito
Dim arrinfo()        As Variant
Dim i                As Long
Dim valorcuota       As Double
Dim arrVlCuota()     As Variant  'Arreglo para guardar los valores de la cuota(base,recargos,descuentos,neto)
Dim AutNumProducto   As Long
Dim TipoEmision      As String   'Tipo de fecha de emision
Dim fecha_emision    As String   'fecha de emision factura
ReDim arr(17)
ReDim arrfac(0)
ReDim arrcuotas(0)
ReDim arrproducto(1, 0)
ReDim arrinfo(3, 0)
ReDim arrVlCuota(3)
Dim arraux() As Variant
ReDim arraux(0, 0)
ReDim arrCom(0) As Comprobante

   Call MouseClock
   AutNumProducto = CargarParametro("06")

   If Transaccion = True Then
      If (BeginTran(STranIUp & "FACTURACION") <> FAIL) Then
      End If
   End If
   
   Facturacion = FAIL
   
   For h = 0 To UBound(arrfacturacion)
      'carga la informacion
      Desde = "CONTRATO,CUOTA_CONTRATO,NEGOCIO"
      Campos = "NU_AUTO_CONT,NU_AUTO_CUOT,FE_VENC_CUOT,NU_BASE_CUOT,NU_AUTO_TERC_CONT,TX_TIFA_NEGO"
      Campos = Campos & Coma & "NU_AUTO_NEGO_CONT,NU_AUTO_EMPR_CONT,FE_INIC_CONT,FE_FINA_CONT"
      Campos = Campos & Coma & "NU_AUTO_FOPA_CUOT,NU_AUTO_PEPA_CUOT,NU_AUTO_BANC_CUOT,TX_CUEN_CUOT"
      Campos = Campos & Coma & "NU_AUTO_TERC_CUOT,FE_INIC_CUOT,FE_VENC_CUOT,NU_NUME_CUOT"
      Condicion = "NU_AUTO_CONT=NU_AUTO_CONT_CUOT"
      Condicion = Condicion & " AND TX_ETAP_CONT=" & Comi & "C" & Comi  'Contrato Cerrado
      Condicion = Condicion & " AND TX_ESTA_CUOT=" & Comi & "S" & Comi  'Cuota sin facturar
      Condicion = Condicion & " AND " & SParA & " FE_INIC_CUOT>" & FFechaCon(DateAdd("d", -1, Inicio))
      Condicion = Condicion & " AND FE_INIC_CUOT<" & FFechaCon(DateAdd("d", 1, final)) & SParC  'Facturas fecha vencimiento menor o igual a hoy
      Condicion = Condicion & " AND NU_AUTO_NEGO_CONT=NU_AUTO_NEGO "
      Condicion = Condicion & " AND NU_AUTO_EMPR_CONT=" & AutoSucursal
      Condicion = Condicion & " AND NU_AUTO_CUOT=" & arrfacturacion(h)
      Condicion = Condicion & " ORDER BY NU_AUTO_CONT"
      Result = LoadData(Desde, Campos, Condicion, arr)
      If Result = FAIL Or Not Encontro Then Result = FAIL: Exit For
      
      If arrinfo(3, 0) <> NUL$ Then ReDim Preserve arrinfo(3, UBound(arrinfo, 2) + 1)
      
      DoEvents
      If Cancel = True Then Result = FAIL: Exit For
      
      meses = Cantidad_Meses(CStr(arr(15)), CStr(arr(16)))
      
      If TipoDoc = factura Then
         Result = Tipo_Fecha_Emision(CLng(arr(6)), TipoEmision)
         If Result = FAIL Then Exit For
      End If
      
      Call Valor_Cuota_Contrato(CLng(arr(0)), CLng(arr(1)), arrVlCuota())
      valorcuota = arrVlCuota(3)
      'Carga la sumatoria de la cuota
      
      'Buscar Notas D�bito de cuotas anteriores pendientes por cobrar
      ReDim arrTotND(0)
      Campos = "SUM(NU_VALDOC_MOCO)"
      condi = "NU_AUTO_CONT_MOCO=" & CDbl(arr(0))
      condi = condi & " AND TX_TIPDOC_MOCO='NDF' AND TX_ESTDOC_MOCO='P'"
      Result = LoadData("MOVIMIENTO_CONTRATO", Campos, condi, arrTotND())
      If Result = FAIL Then Result = FAIL: Exit For
      
      If arrTotND(0) = "" Then arrTotND(0) = 0
      'Buscar Notas Cr�bito de cuotas anteriores pendientes por cobrar
      ReDim arrTotNC(0)
      Campos = "SUM(NU_VALDOC_MOCO)"
      condi = "NU_AUTO_CONT_MOCO=" & CDbl(arr(0))
      condi = condi & " AND TX_TIPDOC_MOCO='NCF' AND TX_ESTDOC_MOCO='P'"
      Result = LoadData("MOVIMIENTO_CONTRATO", Campos, condi, arrTotNC())
      If Result = FAIL Then Result = FAIL: Exit For
      If arrTotNC(0) = "" Then arrTotNC(0) = 0
      
      If TipoDoc = 1 Then 'si es factura
         Nfactura = LeerNroFactura(EstFactura, auxfactura)
         If Nfactura = "" Then
            Call Mensaje1("No se encontro el consecutivo de facturacion", 3)
            Result = FAIL: Exit For
         End If
      ElseIf TipoDoc = FacturaMasivo Then
         Nfactura = CStr(LeerNroFacturaMasivo())
         If Nfactura = "-1" Then
            Call Mensaje1("No se encontro el consecutivo de facturacion para Masivos", 3)
            Result = FAIL: Exit For
         End If
      End If
                  
      If desdeF < CLng(Nfactura) And desdeF = 0 Then desdeF = CLng(Nfactura)
      If hastaF < CLng(Nfactura) Then hastaF = CLng(Nfactura)
                  
      DoEvents
      If Cancel = True Then Result = FAIL: Exit For
               
      'Actualiza el estado de la cuota a facturado
      '----------------------------------------------------------------------------
      Valores = "TX_ESTA_CUOT=" & Comi & "F" & Comi
      Condicion = "NU_AUTO_CUOT=" & CLng(arr(1))
      Result = DoUpdate("CUOTA_CONTRATO", Valores, Condicion)
      If Result = FAIL Or CountRecords = 0 Then Result = FAIL: Exit For
         
      'Graba en el tipo de movimiento
      '-----------------------------------------------------------------------------
      If TipoDoc = 1 Then 'si es factura
         Valores = "TX_TIPO_TIMO=" & Comi & "FA" & Comi & Coma
         Valores = Valores & "NU_AUTO_NUDO_TIMO=" & EstFactura.autonumerico & Coma
         Valores = Valores & "TX_FACT_TIMO=" & Comi & EstFactura.Prefijo & CStr(Nfactura) & Comi & Coma
      ElseIf TipoDoc = FacturaMasivo Then
         Valores = "TX_TIPO_TIMO=" & Comi & "FM" & Comi & Coma
         Valores = Valores & "NU_AUTO_NUDO_TIMO=NULL" & Coma
         Valores = Valores & "TX_FACT_TIMO=" & Comi & CStr(Nfactura) & Comi & Coma
      End If
      Valores = Valores & "TX_ESTA_TIMO=" & Comi & "S" & Comi & Coma
      
      If TipoEmision = "" Or TipoEmision = "F" Or val(arr(17)) = 1 Then
         fecha_emision = FechaServer
      Else
         fecha_emision = DateAdd("M", 1, FechaServer)
         fecha_emision = DateAdd("d", (Day(fecha_emision) - 1) * (-1), fecha_emision)
      End If
      Valores = Valores & "FE_GENE_TIMO=" & FFechaIns(FechaServer) & Coma
      Valores = Valores & "FE_EMIS_TIMO=" & FFechaIns(fecha_emision) & Coma
      'Valores = Valores & "FE_VENC_TIMO=" & FFechaIns(CDate(arr(2))) & Coma
      Valores = Valores & "FE_VENC_TIMO=" & FFechaIns(DateAdd("d", -1, CDate(arr(15)))) & Coma
      Valores = Valores & "NU_VALO_TIMO=" & valorcuota + CDbl(arrTotND(0) - CDbl(arrTotNC(0))) & Coma
      Valores = Valores & "NU_SALD_TIMO=" & valorcuota + CDbl(arrTotND(0) - CDbl(arrTotNC(0))) & Coma
      AutNumTitular = TitularGrupo(arr(0), 1) & Coma
      If AutNumTitular = 0 Then Result = FAIL: Exit For
               
      'Si la facturacion es al contratante, toma el de la cuota
      '--------------------------------------------------------
      Valores = Valores & "NU_AUTO_TERC_TIMO=" & arr(14) & Coma
      Valores = Valores & "NU_AUTO_NEGO_TIMO=" & arr(6) & Coma
      Valores = Valores & "NU_AUTO_SUCU_TIMO=" & arr(7) & Coma
      Texto = NUL$
      Valores = Valores & Consultar_Ejecutivo(CDbl(arr(0)), Texto)
      arrinfo(0, UBound(arrinfo, 2)) = Texto  'almancena el nombre del vendedor
      If NroGrupos(CDbl(arr(0))) > 1 Then
         Valores = Valores & "NU_AUTO_PLAN_TIMO=NULL" & Coma
         arrinfo(3, UBound(arrinfo, 2)) = "N"
      Else
         Texto = NUL$
         Valores = Valores & "NU_AUTO_PLAN_TIMO=" & Consultar_Plan(CDbl(arr(0)), Texto) & Coma
         arrinfo(1, UBound(arrinfo, 2)) = Texto 'Almacena el nombre del plan
         arrinfo(3, UBound(arrinfo, 2)) = "S"
      End If
               
      Valores = Valores & "FE_INVI_TIMO=" & FFechaIns(CDate(arr(8))) & Coma
      Valores = Valores & "FE_FIVI_TIMO=" & FFechaIns(CDate(arr(9))) & Coma
      Valores = Valores & "FE_INPE_TIMO=" & FFechaIns(CDate(arr(15))) & Coma
      Valores = Valores & "FE_FIPE_TIMO=" & FFechaIns(CDate(arr(16))) & Coma
      Valores = Valores & "NU_AUTO_FOPA_TIMO=" & arr(10) & Coma
      Valores = Valores & "NU_AUTO_PEPA_TIMO=" & arr(11) & Coma
               
      If arr(12) = NUL$ Then
         Valores = Valores & "NU_AUTO_BANC_TIMO=NULL" & Coma
      Else
         Texto = NUL$
         Valores = Valores & "NU_AUTO_BANC_TIMO=" & arr(12) & Coma
         Result = Consultar_Banco(CDbl(arr(12)), Texto)
         If Result = FAIL Then Exit For
         arrinfo(2, UBound(arrinfo, 2)) = Texto 'Almacena el nombre del banco
      End If
               
      Valores = Valores & "TX_CUEN_TIMO=" & Comi & arr(13) & Comi & Coma
      Valores = Valores & "NU_AUTO_CONT_TIMO=" & arr(0) & Coma
      Valores = Valores & "NU_AUTO_DIRE_TIMO=" & CargarDireccion(arr(14)) & Coma
      Valores = Valores & "TX_RUTI_TIMO=" & Comi & "N" & Comi & Coma
      Valores = Valores & "NU_AUTO_TIMO_TIMO=NULL" & Coma
      Valores = Valores & "NU_AUTO_CONE_TIMO=" & NumConex
      Result = DoInsertSQL("TIPO_MOVIMIENTO", Valores)
      If Result = FAIL Then Exit For
                              
      DoEvents
      If Cancel = True Then Result = FAIL: Exit For
         
      'Consulta el autonumerico asignado
      If TipoDoc = 1 Then 'si es factura
         Condicion = "NU_AUTO_NUDO_TIMO=" & EstFactura.autonumerico
         Condicion = Condicion & " AND TX_FACT_TIMO=" & Comi & EstFactura.Prefijo & CStr(Nfactura) & Comi
      ElseIf TipoDoc = FacturaMasivo Then
         Condicion = "TX_TIPO_TIMO='FM' AND TX_FACT_TIMO = " & Comi & CStr(Nfactura) & Comi
      End If
      Result = LoadData("TIPO_MOVIMIENTO", "NU_AUTO_TIMO", Condicion, arrfac)
      If Result = FAIL Or Not Encontro Then Result = FAIL: Exit For
      
      DoEvents
      If Cancel = True Then Result = FAIL: Exit For
         
      'Graba la relacion entre el tipo de movimiento y la cuota
      Valores = "NU_AUTO_CUOT_CUTI=" & arr(1) & Coma
      Valores = Valores & "NU_AUTO_TIMO_CUTI=" & arrfac(0) & Coma
      Valores = Valores & "NU_VALO_CUTI=" & valorcuota + CDbl(arrTotND(0) - CDbl(arrTotNC(0)))
      Result = DoInsertSQL("R_CUOTA_TIPO_MOVIMIENTO", Valores)
      If Result = FAIL Then Exit For
               
      DoEvents
      If Cancel = True Then Result = FAIL: Exit For
                     
      'Graba en el detalle del tipo de movimiento
      Valores = "NU_AUTO_TIMO_TMDE=" & arrfac(0) & Coma
      Valores = Valores & "NU_AUTO_PRAD_TMDE=" & AutNumProducto & Coma
      Valores = Valores & "NU_DEBI_TMDE=" & valorcuota & Coma
      Valores = Valores & "NU_CANT_TMDE=" & 1 & Coma
      Valores = Valores & "TX_INDI_TMDE=" & Comi & "S" & Comi & Coma
      Valores = Valores & "TX_TIPO_TMDE=" & Comi & "A" & Comi & Coma
      Result = DoInsertSQL("TIPO_MOVIMIENTO_DETALLE", Valores)
      If Result = FAIL Then Exit For
      
      If Result <> FAIL Then
         'Relaciona el movimiento con la factura
         Desde = "MOVIMIENTO_CONTRATO"
         Condicion = "NU_AUTO_CONT_MOCO=" & CDbl(arr(0))
         Condicion = Condicion & " AND TX_TIPDOC_MOCO IN('NDF','NCF') AND TX_ESTDOC_MOCO='P'"
         Result = LoadMulData("MOVIMIENTO_CONTRATO", "NU_NUME_MOCO", Condicion, arraux)
         If Result <> FAIL And Encontro Then
            For i = 0 To UBound(arraux, 2)
               Valores = "NU_NUME_MOCO_MCTM=" & CLng(arraux(0, i)) & Coma
               Valores = Valores & "NU_AUTO_TIMO_MCTM=" & CLng(arrfac(0))
               Result = DoInsertSQL("R_MOV_CON_TIPO_MOV", Valores)
               If Result = FAIL Then Exit For
            Next i
            If Result = FAIL Then Exit For
         End If
      End If
      
      'GRABAR El detalle de las Inclusiones y exclusiones
      ReDim arrDetNDNC(2, 0)
      Campos = "TX_TIPDOC_MOCO,COUNT(*),SUM(NU_VALDOC_MOCO)"
      Desde = "MOVIMIENTO_CONTRATO"
      condi = "NU_AUTO_CONT_MOCO=" & CDbl(arr(0))
      condi = condi & " AND TX_TIPDOC_MOCO IN('NDF','NCF') AND TX_ESTDOC_MOCO='P'"
      condi = condi & " GROUP BY TX_TIPDOC_MOCO"
      Result = LoadMulData("MOVIMIENTO_CONTRATO", Campos, condi, arrDetNDNC())
      If Result <> FAIL And Encontro Then
         For i = 0 To UBound(arrDetNDNC, 2)
            If arrDetNDNC(2, i) = "" Then arrDetNDNC(2, i) = 0
            Valores = "NU_AUTO_TIMO_TMDE=" & arrfac(0) & Coma
            Valores = Valores & "NU_AUTO_PRAD_TMDE=" & AutNumProducto & Coma
            Valores = Valores & "NU_CANT_TMDE=" & arrDetNDNC(1, i) & Coma
            Select Case arrDetNDNC(0, i)
               Case "NDF": Valores = Valores & "NU_DEBI_TMDE=" & arrDetNDNC(2, i) & Coma
                           Valores = Valores & "NU_CRED_TMDE=0" & Coma
                           Valores = Valores & "TX_INDI_TMDE='S'" & Coma
                           Valores = Valores & "TX_TIPO_TMDE='D'"
               Case "NCF": Valores = Valores & "NU_DEBI_TMDE=0" & Coma
                           Valores = Valores & "NU_CRED_TMDE=0" & arrDetNDNC(2, i) & Coma
                           Valores = Valores & "TX_INDI_TMDE='R'" & Coma
                           Valores = Valores & "TX_TIPO_TMDE='E'"
            End Select
            If Result <> FAIL Then Result = DoInsertSQL("TIPO_MOVIMIENTO_DETALLE", Valores)
            
         Next
      End If
      
      If Result <> FAIL Then
         'Cancela las ND y NC
         Desde = "MOVIMIENTO_CONTRATO"
         Condicion = "NU_AUTO_CONT_MOCO=" & CDbl(arr(0))
         Condicion = Condicion & " AND TX_TIPDOC_MOCO IN('NDF','NCF') AND TX_ESTDOC_MOCO='P'"
         Result = LoadMulData("MOVIMIENTO_CONTRATO", "NU_NUME_MOCO", Condicion, arraux)
         If Result <> FAIL And Encontro Then
            For i = 0 To UBound(arraux, 2)
               Valores = "TX_ESTDOC_MOCO='C'"
               Condicion = "NU_NUME_MOCO=" & CLng(arraux(0, i))
               Result = DoUpdate("MOVIMIENTO_CONTRATO", Valores, Condicion)
               If Result = FAIL Then Exit For
            Next i
            If Result = FAIL Then Exit For
         End If
      End If
      
      
      DoEvents
      If Cancel = True Then Result = FAIL: Exit For
               
      If Result <> FAIL Then Result = GrabarAfiliados(CLng(arrfac(0)), CLng(arr(0)), CDate(arr(15)), CDate(arr(16)))
      If Result = FAIL Then Exit For
               
      DoEvents
      If Cancel = True Then Result = FAIL: Exit For
               
      'Carga los productos parametrizados a los titulares
      Desde = "R_PLAN_PRODUCTO_ADICIONAL,R_PROADI_TITULAR,R_CONTRATO_AFILIADO,PERSONA_NATURAL"
      Campos = "NU_AUTO_PRAD_PLPR,SUM(NU_VALO_PATI)"
      Condicion = "NU_AUTO_PLPR=NU_AUTO_PLPR_PATI"
      Condicion = Condicion & " AND NU_AUTO_CONT_COPN=" & arr(0)
      Condicion = Condicion & " AND NU_AUTO_COPN_PATI=NU_AUTO_COPN"
      Condicion = Condicion & " AND NU_AUTO_PENA_COPN=NU_AUTO_PENA"
      Condicion = Condicion & " AND TX_ESTA_COPN=" & Comi & "A" & Comi
      Condicion = Condicion & " AND TX_TIPO_COPN=" & Comi & "T" & Comi
      Condicion = Condicion & " GROUP BY NU_AUTO_PRAD_PLPR"
      Result = LoadMulData(Desde, Campos, Condicion, arrproducto)
      If Result <> FAIL And Encontro Then
         For k = 0 To UBound(arrproducto, 2)
            DoEvents
            If Cancel = True Then Result = FAIL: Exit For
            'Graba en el detalle
            Valores = "NU_AUTO_TIMO_TMDE=" & CLng(arrfac(0)) & Coma
            Valores = Valores & "NU_AUTO_PRAD_TMDE=" & CLng(arrproducto(0, k)) & Coma
            Valores = Valores & "NU_DEBI_TMDE=" & (CDbl(arrproducto(1, k)) * meses) & Coma
            Valores = Valores & "NU_CANT_TMDE=" & 1 & Coma
            Valores = Valores & "TX_INDI_TMDE=" & Comi & "S" & Comi & Coma
            Valores = Valores & "TX_TIPO_TMDE=" & Comi & "P" & Comi
            Result = DoInsertSQL("TIPO_MOVIMIENTO_DETALLE", Valores)
            If Result = FAIL Then Exit For
            'Actualiza el valor de la factura
            Condicion = "NU_AUTO_TIMO=" & arrfac(0)
            Valores = "NU_VALO_TIMO=NU_VALO_TIMO + " & CDbl(arrproducto(1, k) * meses) & Coma
            Valores = Valores & "NU_SALD_TIMO=NU_SALD_TIMO + " & CDbl(arrproducto(1, k) * meses)
            Result = DoUpdate("TIPO_MOVIMIENTO", Valores, Condicion)
            If Result = FAIL Then Exit For
         Next k
      End If
               
      If TipoDoc = 1 And Interfase = 1 Then 'Si es factura
         condi = "NU_AUTO_TIMO=" & Comi & CLng(arrfac(0)) & Comi
         If Result <> FAIL Then Result = Apot_Fact(condi, "", arrCom)
      End If
            
      If Result = FAIL Then Exit For
      If IsMissing(Prg_Bar) Then
         Prg_Bar.Value = Prg_Bar.Value + 1
      End If
   Next h
   
   If Transaccion = True Then
      If (Result <> FAIL) Then
         If (CommitTran() <> FAIL) Then
            If Imprimir Then
               Call Imprimir_Facturas(desdeF, hastaF, EstFactura, arrinfo, Enviar)
               Call Mensaje1("Proceso de facturaci�n terminado", 3)
            End If
         Else
            Call RollBackTran
            Result = Eliminar_Comprobantes(arrCom)
            Result = FAIL
            Call Mensaje1("No se pudo realizar el proceso de facturaci�n", 3)
         End If
      Else
         Call RollBackTran
         Result = Eliminar_Comprobantes(arrCom)
         Result = FAIL
         If Cancel = True Then
            Call Mensaje1("El proceso de facturaci�n fue cancelado", 3)
         Else
            Call Mensaje1("No se pudo realizar el proceso de facturaci�n", 3)
         End If
      End If
   Else
      'Result = Eliminar_Comprobantes(arrCom)
      'Result = FAIL
   End If
   Facturacion = Result
   Call MouseNorm
End Function

'Busca el autonumerico en la tabla tercero del titular de un grupo
Private Function TitularGrupo(ByVal AutNumContrato, ByVal grupo As Long) As Long
   ReDim arr(0)
   Condicion = "NU_AUTO_TERC=NU_AUTO_TERC_PENA"
   Condicion = Condicion & " AND NU_AUTO_PENA=NU_AUTO_PENA_COPN"
   Condicion = Condicion & " AND NU_AUTO_CONT_COPN=" & AutNumContrato
   Condicion = Condicion & " AND NU_GRUP_COPN=" & grupo
   Condicion = Condicion & " AND TX_TIPO_COPN=" & Comi & "T" & Comi
   Result = LoadData("R_CONTRATO_AFILIADO,PERSONA_NATURAL,TERCERO", "NU_AUTO_TERC", Condicion, arr)
   If Result <> FAIL And Encontro Then
      TitularGrupo = CDbl(arr(0))
   Else
      TitularGrupo = FAIL
   End If
End Function
'Consulta cual es el vendedor o broker del contrato
'Parametros:   AutNumContrato: Autonumerico del contrato
Function Consultar_Ejecutivo(ByVal AutNumContrato, ByRef Nombre As String) As String
Dim cadena As String
   
   'Consulta  el vendedor
   ReDim arr(1)
   Campos = "NU_AUTO_VEND_COVE,TX_DESC_TERC"
   Condicion = "NU_AUTO_CONT_COVE=" & AutNumContrato
   Condicion = Condicion & " AND NU_AUTO_VEND_COVE=NU_AUTO_VEND"
   Condicion = Condicion & " AND NU_AUTO_TERC_VEND=NU_AUTO_TERC"
   Result = LoadData("R_CONTRATO_VENDEDOR,VENDEDOR,TERCERO", Campos, Condicion, arr)
   If Result = FAIL Then Consultar_Ejecutivo = NUL$: Exit Function
   
   If Encontro Then
      cadena = "NU_AUTO_VEND_TIMO=" & CDbl(arr(0)) & Coma
      cadena = cadena & "NU_AUTO_BROK_TIMO=NULL" & Coma
      Nombre = Restaurar_Comas_Comillas(CStr(arr(1)))
      Consultar_Ejecutivo = cadena: Exit Function
   End If
   
   Campos = "NU_AUTO_BROK_COBR,TX_DESC_TERC"
   Condicion = "NU_AUTO_CONT_COBR=" & AutNumContrato
   Condicion = Condicion & " AND NU_AUTO_BROK_COBR=NU_AUTO_BROK"
   Condicion = Condicion & " AND NU_AUTO_TERC_BROK=NU_AUTO_TERC"
   Result = LoadData("R_CONTRATO_BROKER,BROKER,TERCERO", Campos, Condicion, arr)
   If Result = FAIL Then Consultar_Ejecutivo = NUL$: Exit Function
   
   If Encontro Then
      cadena = "NU_AUTO_VEND_TIMO=NULL" & Coma
      cadena = cadena & "NU_AUTO_BROK_TIMO=" & CDbl(arr(0)) & Coma
      Nombre = Restaurar_Comas_Comillas(CStr(arr(1)))
      Consultar_Ejecutivo = cadena: Exit Function
   Else
      cadena = "NU_AUTO_VEND_TIMO=NULL" & Coma
      cadena = cadena & "NU_AUTO_BROK_TIMO=NULL" & Coma
      Consultar_Ejecutivo = cadena
   End If
   
End Function

'Consulta la cantidad de grupo que tiene el contrato
'Parametros AutNumContrato 'autonumerico del contrato
Private Function NroGrupos(ByVal AutNumContrato As Long) As Integer
   ReDim arr(0)
   Condicion = "NU_AUTO_CONT_COPN=" & AutNumContrato
   Condicion = Condicion & " AND TX_ESTA_COPN=" & Comi & "A" & Comi & SParC
   Campos = "COUNT(NU_GRUP_COPN)"
   Desde = " (SELECT DISTINCT(NU_GRUP_COPN) FROM "
   Desde = Desde & "R_CONTRATO_AFILIADO"
   Result = LoadData(Desde, Campos, Condicion, arr)
   If Result <> FAIL And Encontro Then
      NroGrupos = CDbl(arr(0))
   Else
      NroGrupos = 0
   End If
End Function

'Carga el autonumerico del plan
Function Consultar_Plan(ByVal AutNumContrato, ByRef Nombre As String) As Long
   ReDim arr(1)
   Campos = "NU_AUTO_PLAN_COPN,TX_DESC_PLAN"
   Condicion = "NU_AUTO_CONT_COPN=" & AutNumContrato
   Condicion = Condicion & " AND NU_AUTO_PLAN_COPN=NU_AUTO_PLAN"
   Result = LoadData("R_CONTRATO_AFILIADO,PLAN", Campos, Condicion, arr)
   If Result = FAIL Then Exit Function
   If Encontro Then
      Consultar_Plan = CDbl(arr(0))
      Nombre = Restaurar_Comas_Comillas(CStr(arr(1)))
   Else
      Consultar_Plan = 0
   End If
End Function

Private Function Consultar_Banco(ByVal AutNumBanco, ByRef Texto As String) As Integer
   ReDim arr(0)
   Campos = "TX_DESC_BANC"
   Condicion = "NU_AUTO_BANC=" & AutNumBanco
   Result = LoadData("BANCO", Campos, Condicion, arr)
   If Result = FAIL Or Not Encontro Then Consultar_Banco = FAIL: Exit Function
   Texto = Restaurar_Comas_Comillas(CStr(arr(0)))
   Consultar_Banco = Result
End Function

Private Function CargarDireccion(ByVal AutNumContratante As Long) As Long
ReDim arr(0)
   Campos = "NU_AUTO_DIRE"
   Condicion = "NU_AUTO_TERC_DIRE=" & AutNumContratante
   Condicion = Condicion & " AND (TX_TIPO_DIRE='3' OR TX_TIPO_DIRE='6')"   'Direccion de un afiliado o un titular juridico
   Condicion = Condicion & " AND TX_CLAS_DIRE=" & Comi & "2" & Comi        'Direccion de cobro
   Condicion = Condicion & " AND TX_ESTA_DIRE='A'"                         'activa
   Result = LoadData("DIRECCION", Campos, Condicion, arr)
   If Result <> FAIL And Encontro Then
      CargarDireccion = CLng(arr(0))
   Else
      CargarDireccion = 0
   End If
End Function

'Esta funcion graba los afiliados que pertenecen al contrato
'y que estan activos en el momento de la facturacion
Public Function GrabarAfiliados(ByVal AutNumFactura As Long, ByVal AutNumContrato As Long, ByVal FeInicCuot As String, ByVal FeFinaCuot As Date) As Integer
Dim llave As String
Dim meses As Integer
Dim rst As New ADODB.Recordset
   
   On Error GoTo ErrorHandler
   'meses = Abs(DateDiff("m", FeInicCuot, FeFinaCuot))
   'If meses = 0 Then meses = 1
   meses = Cantidad_Meses(CDate(FeInicCuot), CDate(FeFinaCuot))
   
   Campos = "TX_TIPO_COPN,TX_NUID_TERC,NU_AUTO_COPN,NU_NETO_COCU,NU_AUTO_PARE_COPN,NU_AUTO_PLAN_COPN"
   Desde = "R_CONTRATO_AFILIADO,PERSONA_NATURAL,TERCERO,CONTRATO_DETALLE_CUOTA"
   condi = "NU_AUTO_PENA_COPN=NU_AUTO_PENA AND NU_AUTO_TERC_PENA=NU_AUTO_TERC"
   condi = condi & " AND NU_AUTO_CONT_COPN=NU_AUTO_CONT_COCU AND NU_AUTO_PENA_COPN=NU_AUTO_PENA_COCU"
   condi = condi & " AND NU_AUTO_CONT_COPN=" & AutNumContrato
   condi = condi & " AND ((TX_ESTA_COPN='A' AND FE_INGR_COPN<=" & FFechaCon(FeFinaCuot) & ")"
   condi = condi & " OR (TX_ESTA_COPN='E' AND FE_EXCL_COPN>=" & FFechaCon(FeFinaCuot) & "))"
   condi = condi & " AND FE_INIC_COCU<=" & FFechaCon(FeFinaCuot)
   condi = condi & " AND FE_FINA_COCU>=" & FFechaCon(FeFinaCuot)
   condi = condi & " ORDER BY NU_GRUP_COPN, TX_TIPO_COPN DESC"
   Debug.Print "SELECT " & Campos & " FROM " & Desde & " WHERE " & condi
   rst.Open "SELECT " & Campos & " FROM " & Desde & " WHERE " & condi, BD(BDCurCon), adOpenStatic
   rst.MoveFirst
      
   While Not rst.EOF
      If rst.Fields(0) = "T" Then llave = rst.Fields(1)
      Valores = "NU_AUTO_TIMO_TMAF=" & AutNumFactura & Coma
      Valores = Valores & "NU_AUTO_COPN_TMAF=" & rst.Fields(2) & Coma
      Valores = Valores & "TX_LLAV_TMAF=" & Comi & llave & Comi & Coma
      Valores = Valores & "NU_VALO_CUOT_TMAF=" & rst.Fields(3) * meses & Coma
      Valores = Valores & "NU_AUTO_PARE_TMAF=" & rst.Fields(4) & Coma
      Valores = Valores & "NU_AUTO_PLAN_TMAF=" & rst.Fields(5) & Coma
      Valores = Valores & "TX_TIPO_TMAF=" & Comi & rst.Fields(0) & Comi
      Result = DoInsertSQL("TIPO_MOVIMIENTO_AFILIADO", Valores)
      If Result = FAIL Then GrabarAfiliados = FAIL: Exit Function
      rst.MoveNext
   Wend
   
   GrabarAfiliados = Result
   Exit Function
ErrorHandler:
   Call ConvertErr
   Result = FAIL
   Exit Function
End Function

'Esta funcion imprime las facturas generadas
'Parametros:   IniFac: Factura Inicial
'              FinFac: Factura Final
Public Sub Imprimir_Facturas(ByVal IniFac As Long, ByVal FinFac As Long, ByRef EstFactura As Numeracion, ByRef arrinfo As Variant, Enviar As Integer)
   Dim Conexion     As String
   Dim cadena       As String
   Dim i            As Long
   Dim j            As Byte
   Dim h            As Long
   Dim Frm          As Form
   Dim t            As Date
   Dim Formula      As String
   Dim CondA        As String
   Dim CondB        As String
   Dim Ejec         As String
   Dim Broker       As String
   Dim Plan         As String
   Dim Banco        As String
   Dim valor        As Double
   Dim Resp         As String
   Dim Imp_No       As Integer
   Dim arrTmp()     As Variant
   Dim Arr1()       As Variant
   
      
   With MDI_Medicina.Crys_Listar
      On Error GoTo Control
      Call Limpiar_CrysListar
      Conexion = .Connect
      .WindowTitle = "Facturaci�n"
      .Destination = IIf(Enviar = 0, crptToPrinter, crptToWindow)
      .ReportFileName = App.Path & "\" & "Factura.rpt"
      h = 0
      For i = IniFac To FinFac
         cadena = Crear_NumeroFactura(EstFactura, i)
         'Verifica si el contrato al que pertenece la factura tiene mas de un
         'nucleo
         If Busca_Nucleos_Contrato(cadena, EstFactura.autonumerico) Then ' Tiene un solo nucleo
            If Factura_rs(cadena, EstFactura.autonumerico) = True Then
               '''''''''Set Frm = Forms.Add("FrmRptFactura")
               
               ReDim arr(0)
               Condicion = "TX_FACT_TIMO = '" & cadena & Comi
               Condicion = Condicion & " AND NU_AUTO_NUDO_TIMO = " & EstFactura.autonumerico
               Result = LoadData("TIPO_MOVIMIENTO", "NU_IMPR_TIMO", Condicion, arr)
               Imp_No = arr(0)
               
               'FACTURA
               'Carga el nombre del ejecutivo
               Ejec = NUL$
               If Not IsNull(rs("NU_AUTO_CONT")) And rs("NU_AUTO_CONT") > 0 Then
                  Resp = Consultar_Ejecutivo_Fact(0, CLng(rs("NU_AUTO_CONT")), Ejec)
               End If
               
               'Carga el nombre del broker
               Broker = NUL$
               If Not IsNull(rs("NU_AUTO_BROK_TIMO")) Then
                  Result = Consultar_Ejecutivo_Fact(1, CLng(rs("NU_AUTO_BROK_TIMO")), Broker)
               End If
   
               'Carga el plan
               Plan = NUL$
               If Not IsNull(rs("NU_AUTO_CONT")) And rs("NU_AUTO_CONT") > 0 Then
                  Result = Consultar_Plan(rs("NU_AUTO_CONT"), Plan)
                  If Result = FAIL Then Plan = ""
               End If
   
               'Carga el nombre del banco
               Banco = NUL$
               If Not IsNull(rs("NU_AUTO_BANC_TIMO")) And rs("NU_AUTO_BANC_TIMO") > 0 Then
                  Result = Consultar_Banco(rs("NU_AUTO_BANC_TIMO"), Banco)
                  If Result = FAIL Then Banco = ""
               End If
      
               'Carga el valor
               If rs("NU_DEBI_TMDE") = 0 Then
                  valor = rs("NU_CRED_TMDE")
               Else
                  valor = rs("NU_DEBI_TMDE")
               End If
                            
               .Formulas(0) = "Grupo = {TIPO_MOVIMIENTO.NU_AUTO_TIMO}"
               
               'Verifica si la factura ha sido impresa por primera vez
               If Imp_No = 0 Then
                  .Formulas(1) = "Copia = 0"
               Else
                  .Formulas(1) = "Copia = 1"
               End If
               .Formulas(2) = "Logo = " & Logo
               .SelectionFormula = "{TIPO_MOVIMIENTO.NU_AUTO_TIMO} = " & rs("NU_AUTO_TIMO")
               .ParameterFields(0) = "Division;" & (rs("TX_DESC_NEGO")) & ";True"
               .ParameterFields(1) = "Sucursal;" & (rs("TX_DESC_EMPR")) & ";True"
               .ParameterFields(2) = "Ejecutivo;" & Ejec & ";True"
               .ParameterFields(3) = "Plan;" & Plan & ";True"
               .ParameterFields(4) = "fech_Max_Pag;" & Format$((rs("FE_VENC_TIMO")), "dd/mm/yyyy") & ";True"
               .ParameterFields(5) = "Vig_Desde;" & Format$((rs("FE_INVI_TIMO")), "dd/mm/yyyy") & ";True"
               .ParameterFields(6) = "Vig_Hasta;" & Format$((rs("FE_FIVI_TIMO")), "dd/mm/yyyy") & ";True"
               .ParameterFields(7) = "Per_Cobert_Desde;" & Format$((rs("FE_INPE_TIMO")), "dd/mm/yyyy") & ";True"
               .ParameterFields(8) = "Per_Cobert_Hasta;" & Format$((rs("FE_FIPE_TIMO")), "dd/mm/yyyy") & ";True"
               .ParameterFields(9) = "Cliente;" & (rs("TX_NUID_TERC")) & (rs("TX_VERI_TERC")) & ";True"
               .ParameterFields(10) = "Atencion;" & (rs("TX_DESC_TERC")) & ";True"
               .ParameterFields(11) = "Dir;" & (rs("TX_DESC_DIRE")) & ";True"
               If Not IsNull(rs("TX_TEL1_DIRE")) Then
                 .ParameterFields(12) = "Tel;" & (rs("TX_TEL1_DIRE")) & ";True"
               Else
                  .ParameterFields(12) = "Tel;" & (NUL$) & ";True"
               End If
               .ParameterFields(13) = "Forma_Pg;" & (rs("TX_DESC_FOPA")) & ";True"
               .ParameterFields(14) = "Periodo_Pg;" & (rs("TX_DESC_PEPA")) & ";True"
               If Not IsNull(rs("TX_CUEN_TIMO")) Then
                  .ParameterFields(15) = "Cta;" & (rs("TX_CUEN_TIMO")) & ";True"
               Else
                  .ParameterFields(15) = "Cta;" & (NUL$) & ";True"
               End If
               .ParameterFields(16) = "Control;" & (rs("TX_FACT_TIMO")) & ";True"
               .ParameterFields(17) = "Registro;" & CDbl((rs("NU_NUME_CONT"))) & ";True"
               .ParameterFields(18) = "Contrato;" & (rs("TX_REGI_CONT")) & ";True"
               .ParameterFields(19) = "Cuota;" & CDbl((rs("NU_NUME_CUOT"))) & ";True"
               .ParameterFields(20) = "Banco;" & (Banco) & ";True"
               .ParameterFields(21) = "Broker;" & " - " & Broker & ";True"
               .CopiesToPrinter = 3
               .Action = 1
            End If
         Else 'Tiene m�s de un nucleo
            Dim Contrato       As Long
            Dim automovimiento As Long
            
            'Consulta el autonumerico del contrato
            ReDim arr(1)
            Condicion = "Tipo_Movimiento.TX_FACT_TIMO = " & Comi & cadena & Comi
            Condicion = Condicion & " AND TIPO_MOVIMIENTO.NU_AUTO_NUDO_TIMO = " & EstFactura.autonumerico
            Result = LoadData("TIPO_MOVIMIENTO", "NU_AUTO_CONT_TIMO,NU_AUTO_TIMO", Condicion, arr)
            If Result = FAIL Or Not Encontro Then
               Call Mensaje1("Factura no encontrada", 3)
               Call MouseNorm
               Exit Sub
            Else
               Contrato = CLng(arr(0))
               automovimiento = CLng(arr(1))
            End If
         
            ReDim arr(0)
            Condicion = "TX_FACT_TIMO = '" & cadena & Comi
            Condicion = Condicion & " AND NU_AUTO_NUDO_TIMO = " & EstFactura.autonumerico
            Result = LoadData("TIPO_MOVIMIENTO", "NU_IMPR_TIMO", Condicion, arr)
            Imp_No = arr(0)
            
            'Carga las facturas que fueron impresas
            ReDim Preserve arrTmp(1, j)
            arrTmp(0, j) = cadena
            arrTmp(1, j) = EstFactura.autonumerico
            j = j + 1
            
            .ReportFileName = App.Path & "\" & "Factura_Corp.rpt"
            'Verifica si la factura ha sido impresa por primera vez
            If Imp_No = 0 Then
               .Formulas(1) = "Copia = 0"
            Else
               .Formulas(1) = "Copia = 1"
            End If
            .Formulas(2) = "Logo = " & Logo
            .SelectionFormula = "{TIPO_MOVIMIENTO.TX_FACT_TIMO} = " & Comi & cadena & Comi & _
                                " AND {TIPO_MOVIMIENTO.NU_AUTO_NUDO_TIMO} = " & EstFactura.autonumerico & _
                                " AND {TIPO_MOVIMIENTO.NU_AUTO_TIMO} = " & automovimiento & _
                                " AND {TIPO_MOVIMIENTO.NU_AUTO_CONT_TIMO} = " & Contrato & _
                                " AND {CONTRATO.NU_AUTO_CONT} = " & Contrato
            .SelectionFormula = .SelectionFormula & " AND {R_CONTRATO_AFILIADO.NU_AUTO_CONT_COPN} = " & Contrato
            .SelectionFormula = .SelectionFormula & " AND {CUOTA_CONTRATO.NU_AUTO_CONT_CUOT} = " & Contrato

            Debug.Print .SelectionFormula
            .CopiesToPrinter = 3
            .Action = 1
         End If
      Next i
   End With
   
   'Actualiza el contador de impresiones de la factura
   Valores = "NU_IMPR_TIMO = " & Imp_No + 1
   Condicion = "Tipo_Movimiento.TX_FACT_TIMO = " & Comi & cadena & Comi
   Condicion = Condicion & " AND TIPO_MOVIMIENTO.NU_AUTO_NUDO_TIMO = " & EstFactura.autonumerico
   Result = DoUpdate("TIPO_MOVIMIENTO", Valores, Condicion)

   'Imprime los anexos de las facturas
   ReDim arr(1)
   For i = 0 To UBound(arrTmp, 2)
      Campos = "CUOTA_CONTRATO.NU_NUME_CUOT, TIPO_MOVIMIENTO.NU_AUTO_TIMO"
      Desde = "TIPO_MOVIMIENTO, R_CUOTA_TIPO_MOVIMIENTO, CUOTA_CONTRATO"
      Condicion = "TIPO_MOVIMIENTO.NU_AUTO_TIMO = R_CUOTA_TIPO_MOVIMIENTO.NU_AUTO_TIMO_CUTI"
      Condicion = Condicion & " AND R_CUOTA_TIPO_MOVIMIENTO.NU_AUTO_CUOT_CUTI =  CUOTA_CONTRATO.NU_AUTO_CUOT"
      Condicion = Condicion & " AND TIPO_MOVIMIENTO.TX_FACT_TIMO = '" & arrTmp(0, i) & Comi
      Condicion = Condicion & " AND TIPO_MOVIMIENTO.NU_AUTO_NUDO_TIMO = " & arrTmp(1, i)
      Result = LoadData(Desde, Campos, Condicion, arr)
      If Result <> FAIL And Encontro Then
         If arr(0) = 1 Then
            If CondA = NUL$ Then
               CondA = arr(1)
            Else
               CondA = CondA & Coma & arr(1)
            End If
         ElseIf arr(0) > 1 Then
            ReDim Arr1(0)
            Result = LoadData("R_MOV_CON_TIPO_MOV", "COUNT(*)", "NU_AUTO_TIMO_MCTM = " & arr(1), Arr1)
            If Result <> FAIL And Encontro Then
               If CondB = NUL$ Then
                  CondB = arr(1)
               Else
                  CondB = CondB & Coma & arr(1)
               End If
            End If
         End If
      End If
   Next i
   
   Call Mensaje1("A continuaci�n se enviara la impresi�n de los anexos de las facturas generadas.", 3)
   
   Resp = MsgBox("Para confirmar la impresi�n de los anexos, presione Aceptar de lo contrario Cancelar", vbInformation + vbOKCancel)
   If Resp = vbOK Then
      'Anexo afiliado para la factura de la cuota 1
      If CondA <> NUL$ Then
         Formula = "{TIPO_MOVIMIENTO.NU_AUTO_TIMO} IN [" & CondA & "]"
         Call Imprimir_Reporte("Anexo - Factura - Cuota 1", App.Path & "\Tit_Dep_Cta_1.rpt", Formula, False, 1, False)
      End If
      
      If CondB <> NUL$ Then
         Formula = "{TIPO_MOVIMIENTO.NU_AUTO_TIMO} IN [" & CondB & "]"
         Call Imprimir_Reporte("Anexo - Factura - Cuota 2 en Adelante", App.Path & "\Tip_Dep_Mayor_1_Cta.rpt", Formula, False, 1, False)
      End If
   End If
   
   Exit Sub
Control:
   Call Mensaje1(err.Number & ": " & err.Description, 1)

End Sub
'No_factura=numero del documento que se quiere imprimir
'no_documento: autonumerico del tipo de numeracion de documentos
Public Function Factura_rs(ByVal No_Factura As String, ByVal No_Documento As Long) As Boolean
   Dim ssql As String
   Dim Contrato As Long
   Dim automovimiento As Long
   
   'Consulta el autonumerico del contrato
   ReDim arr(1)
   Condicion = "Tipo_Movimiento.TX_FACT_TIMO = " & Comi & No_Factura & Comi
   Condicion = Condicion & " AND TIPO_MOVIMIENTO.NU_AUTO_NUDO_TIMO = " & No_Documento
   Result = LoadData("TIPO_MOVIMIENTO", "NU_AUTO_CONT_TIMO,NU_AUTO_TIMO", Condicion, arr)
   If Result = FAIL Or Not Encontro Then
      Call Mensaje1("Factura no encontrada", 3)
      Call MouseNorm
      Factura_rs = False
      Exit Function
   Else
      Contrato = CLng(arr(0))
      automovimiento = CLng(arr(1))
   End If
   
   
   FrmRptFactura.t = Time
   ssql = ""
   
   'FACTURA
   ssql = "SELECT TIPO_MOVIMIENTO.NU_AUTO_TIMO, "
   ssql = ssql + "TIPO_MOVIMIENTO.FE_VENC_TIMO, "
   ssql = ssql + "TIPO_MOVIMIENTO.FE_INVI_TIMO, "
   ssql = ssql + "TIPO_MOVIMIENTO.FE_FIVI_TIMO, "
   ssql = ssql + "TIPO_MOVIMIENTO.FE_INPE_TIMO, "
   ssql = ssql + "TIPO_MOVIMIENTO.FE_FIPE_TIMO, "
   ssql = ssql + "TIPO_MOVIMIENTO.TX_CUEN_TIMO, "
   ssql = ssql + "TIPO_MOVIMIENTO.TX_FACT_TIMO, "
   ssql = ssql + "TIPO_MOVIMIENTO.NU_AUTO_VEND_TIMO, "
   ssql = ssql + "TIPO_MOVIMIENTO.NU_AUTO_BROK_TIMO, "
   ssql = ssql + "TIPO_MOVIMIENTO.NU_AUTO_PLAN_TIMO, "
   ssql = ssql + "TIPO_MOVIMIENTO.NU_AUTO_BANC_TIMO, "
   ssql = ssql + "TIPO_MOVIMIENTO.NU_AUTO_CONT_TIMO, "
   ssql = ssql + "EMPRESA.TX_DESC_EMPR, "
   ssql = ssql + "NEGOCIO.TX_DESC_NEGO, "
   ssql = ssql + "TERCERO.TX_NUID_TERC, "
   ssql = ssql + "TERCERO.TX_VERI_TERC, "
   ssql = ssql + "TERCERO.TX_DESC_TERC, "
   ssql = ssql + "FORMA_PAGO.TX_DESC_FOPA, "
   ssql = ssql + "PERIODO_PAGO.TX_DESC_PEPA, "
   ssql = ssql + "DIRECCION.TX_DESC_DIRE, "
   ssql = ssql + "DIRECCION.TX_TEL1_DIRE, "
   ssql = ssql + "TIPO_MOVIMIENTO_DETALLE.NU_DEBI_TMDE, "
   ssql = ssql + "TIPO_MOVIMIENTO_DETALLE.NU_CRED_TMDE, "
   ssql = ssql + "TIPO_MOVIMIENTO_DETALLE.NU_CANT_TMDE, "
   ssql = ssql + "TIPO_MOVIMIENTO_DETALLE.TX_TIPO_TMDE, "
   ssql = ssql + "TIPO_IDENTIFICACION.NU_DIVE_TIID, "
   ssql = ssql + "TIPO_IDENTIFICACION.NU_SUCU_TIID, "
   ssql = ssql + "PRODUCTO_ADICIONAL.TX_DESC_PRAD, "
   ssql = ssql + "CUOTA_CONTRATO.NU_NUME_CUOT, "
   ssql = ssql + "CONTRATO.NU_NUME_CONT, "
   ssql = ssql + "CONTRATO.TX_REGI_CONT, "
   ssql = ssql + "CONTRATO.NU_AUTO_CONT "
   
   ssql = ssql + "FROM TIPO_MOVIMIENTO, "
   ssql = ssql + "EMPRESA, "
   ssql = ssql + "NEGOCIO, "
   ssql = ssql + "TERCERO, "
   ssql = ssql + "FORMA_PAGO, "
   ssql = ssql + "PERIODO_PAGO, "
   ssql = ssql + "DIRECCION, "
   ssql = ssql + "TIPO_MOVIMIENTO_DETALLE, "
   ssql = ssql + "TIPO_IDENTIFICACION, "
   ssql = ssql + "PRODUCTO_ADICIONAL, "
   ssql = ssql + "R_CUOTA_TIPO_MOVIMIENTO, "
   ssql = ssql + "CUOTA_CONTRATO, "
   ssql = ssql + "CONTRATO "

   ssql = ssql + "WHERE TIPO_MOVIMIENTO.NU_AUTO_SUCU_TIMO = + EMPRESA.NU_AUTO_EMPR "
   ssql = ssql + "AND TIPO_MOVIMIENTO.NU_AUTO_NEGO_TIMO = + NEGOCIO.NU_AUTO_NEGO "
   ssql = ssql + "AND TIPO_MOVIMIENTO.NU_AUTO_TERC_TIMO = + TERCERO.NU_AUTO_TERC "
   ssql = ssql + "AND TIPO_MOVIMIENTO.NU_AUTO_FOPA_TIMO = + FORMA_PAGO.NU_AUTO_FOPA "
   ssql = ssql + "AND TIPO_MOVIMIENTO.NU_AUTO_PEPA_TIMO = + PERIODO_PAGO.NU_AUTO_PEPA "
   ssql = ssql + "AND TIPO_MOVIMIENTO.NU_AUTO_DIRE_TIMO = + DIRECCION.NU_AUTO_DIRE "
   ssql = ssql + "AND TIPO_MOVIMIENTO.NU_AUTO_TIMO = + TIPO_MOVIMIENTO_DETALLE.NU_AUTO_TIMO_TMDE "
   ssql = ssql + "AND TERCERO.NU_AUTO_TIID_TERC = + TIPO_IDENTIFICACION.NU_AUTO_TIID "
   ssql = ssql + "AND TIPO_MOVIMIENTO_DETALLE.NU_AUTO_PRAD_TMDE = + PRODUCTO_ADICIONAL.NU_AUTO_PRAD "
   ssql = ssql + "AND TIPO_MOVIMIENTO_DETALLE.NU_AUTO_TIMO_TMDE = + R_CUOTA_TIPO_MOVIMIENTO.NU_AUTO_TIMO_CUTI "
   ssql = ssql + "AND R_CUOTA_TIPO_MOVIMIENTO.NU_AUTO_CUOT_CUTI = + CUOTA_CONTRATO.NU_AUTO_CUOT "
   ssql = ssql + "AND CUOTA_CONTRATO.NU_AUTO_CONT_CUOT = + CONTRATO.NU_AUTO_CONT "
   ssql = ssql + "AND TIPO_MOVIMIENTO.TX_FACT_TIMO = " & Comi & No_Factura & Comi
   ssql = ssql + " AND TIPO_MOVIMIENTO.NU_AUTO_NUDO_TIMO = " & No_Documento
   ssql = ssql + " AND CONTRATO.NU_AUTO_CONT= " & Contrato
   ssql = ssql + " AND TIPO_MOVIMIENTO.NU_AUTO_CONT_TIMO= " & Contrato
   ssql = ssql + " AND CUOTA_CONTRATO.NU_AUTO_CONT_CUOT= " & Contrato
   ssql = ssql + " AND TIPO_MOVIMIENTO.NU_AUTO_TIMO= " & automovimiento
   
   Debug.Print ssql
   Set rs = BD(BDCurCon).Execute(ssql)
   If rs.EOF Then
      Call Mensaje1("Factura no encontrada", 3)
      Call MouseNorm
      Exit Function
      Factura_rs = False
   Else
      Factura_rs = True
   End If
End Function

'Esta funcion arma el numero de factura
'Parametros: Estructura Longitud de la secuencia
'            NvaSecuencia: Nuevo numero de secuencia
Public Function Crear_NumeroFactura(ByRef Estructura As Numeracion, ByVal NvaSecuencia As Long) As String
Dim cadena As String
   cadena = Len(Estructura.NumeroActual) - Len(Estructura.Prefijo) - Len(CStr(NvaSecuencia))
   cadena = String(CLng(cadena), "0")
   cadena = Estructura.Prefijo & cadena & NvaSecuencia
   Crear_NumeroFactura = cadena
End Function

'Carga el numero para generar una factura
Function LeerNroFactura(ByRef EstFactura As Numeracion, ByRef auxfactura As Long) As String
Dim Numero As Long
Dim cadena As String
   
   If EstFactura.Generacion = "A" Then
      
      DoEvents
      ReDim arr(0)
      cadena = ""
      
      'consulta el numero actual
      Numero = 0
      Condicion = "NU_AUTO_NUDO=" & EstFactura.autonumerico
      Result = DoUpdate("NUMERACION_DOCUMENTO", "NU_CONS_NUDO=NU_CONS_NUDO+1", Condicion)
      If Result = FAIL Or CountRecords = 0 Then Exit Function
      
      Result = LoadData("NUMERACION_DOCUMENTO", "NU_CONS_NUDO", Condicion, arr)
      If Result = FAIL Or Not Encontro Then Exit Function
      Numero = CLng(arr(0))
         
      'consulta el valor del detalle
      Condicion = "NU_AUTO_NUDO_NUDE=" & EstFactura.autonumerico
      Condicion = Condicion & " AND NU_DIVI_NUDE=3"   'consecutivo
      Result = LoadData("NUMERACION_DOCUMENTO_DETA", "TX_VALR_NUDE", Condicion, arr)
      If Result = FAIL Or Not Encontro Then Exit Function
      
      'actualiza el detalle
      cadena = Len(arr(0)) - Len(CStr(Numero))
      cadena = String(CLng(cadena), "0")
      cadena = cadena & CStr(Numero)
      Condicion = "NU_AUTO_NUDO_NUDE=" & EstFactura.autonumerico
      Condicion = Condicion & " AND NU_DIVI_NUDE=3"   'consecutivo
      Valores = "TX_VALR_NUDE=" & Comi & cadena & Comi
      Result = DoUpdate("NUMERACION_DOCUMENTO_DETA", Valores, Condicion)
      If Result = FAIL Then Exit Function
      
      LeerNroFactura = cadena
   Else
      'verifica que el numero no exista
      Encontro = True
      Do While Encontro = True
         Numero = val(auxfactura)
         cadena = Len(EstFactura.SecuenciaActual) - Len(CStr(Numero))
         cadena = String(CLng(cadena), "0")
         cadena = cadena & CStr(Numero)
         Condicion = "NU_AUTO_NUDO_TIMO=" & EstFactura.autonumerico
         Condicion = Condicion & " AND TX_FACT_TIMO=" & Comi & EstFactura.Prefijo & CStr(cadena) & Comi
         Campos = "NU_AUTO_TIMO"
         ReDim arr(0)
         Result = LoadData("TIPO_MOVIMIENTO", Campos, Condicion, arr)
         If Not Encontro Then
            auxfactura = auxfactura + 1
         Else
            auxfactura = auxfactura + 1
         End If
      Loop
      
      'acutaliza el consecutivo
      Condicion = "NU_AUTO_NUDO=" & EstFactura.autonumerico
      Valores = "NU_CONS_NUDO=" & Numero
      Result = DoUpdate("NUMERACION_DOCUMENTO", Valores, Condicion)
      If Result = FAIL Then Exit Function
      
      'actualiza el detalle
      cadena = Len(EstFactura.SecuenciaActual) - Len(CStr(Numero))
      cadena = String(CLng(cadena), "0")
      cadena = cadena & CStr(Numero)
      Condicion = "NU_AUTO_NUDO_NUDE=" & EstFactura.autonumerico
      Condicion = Condicion & " AND NU_DIVI_NUDE=3"   'consecutivo
      Valores = "TX_VALR_NUDE=" & Comi & cadena & Comi
      Result = DoUpdate("NUMERACION_DOCUMENTO_DETA", Valores, Condicion)
      If Result = FAIL Then Exit Function
      
      LeerNroFactura = cadena
   
   End If

End Function

'Actualiza un registro de la tabla - CONTRATO_DETALLE_CUOTA
'Auto_Contrato => Autonumerico contrato
'Auto_Pers_Nat => Autonumerico persona natural
'Val           => Valor
'Tipo_Opr      => Tipo de modificaci�n (D - Campo Valor de los descuentos de un afiliado)
'                                      (B - Campo Valor base de la cuota sin recargos, ni descuentos)
'                                      (R - Campo Valor de los recargos de un afiliado)
Public Function Actualiza_Valor_Persona(ByVal Contrato As Long, ByVal PersonaNatural As Long, ByVal Fh_Inicio As Date, ByVal valor As Double, ByVal Operacion As String) As Integer
   Desde = "CONTRATO_DETALLE_CUOTA"
   Condicion = " NU_AUTO_CONT_COCU = " & Contrato
   Condicion = Condicion & "AND NU_AUTO_PENA_COCU = " & PersonaNatural
   If Operacion = "D" Then
      Valores = "NU_DESC_COCU = " & valor & Coma
      Valores = Valores & "NU_NETO_COCU=NU_BASE_COCU + NU_RECA_COCU + NU_OTRE_COCU - NU_OTDE_COCU - " & valor
   ElseIf Operacion = "B" Then
      Valores = "NU_BASE_COCU = " & valor & Coma
      Valores = Valores & "NU_NETO_COCU=" & valor & " + NU_RECA_COCU - NU_DESC_COCU + NU_OTRE_COCU - NU_OTDE_COCU "
   ElseIf Operacion = "R" Then
      Valores = "NU_RECA_COCU = " & valor & Coma
      Valores = Valores & "NU_NETO_COCU=" & "NU_BASE_COCU - NU_DESC_COCU + NU_OTRE_COCU - NU_OTDE_COCU + " & valor
   ElseIf Operacion = "RA" Then
      Valores = "NU_OTRE_COCU = " & valor & Coma
      Valores = Valores & "NU_NETO_COCU=" & "NU_BASE_COCU - NU_DESC_COCU + NU_RECA_COCU - NU_OTDE_COCU + " & valor
   ElseIf Operacion = "DA" Then
      Valores = "NU_OTDE_COCU = " & valor & Coma
      Valores = Valores & "NU_NETO_COCU=" & "NU_BASE_COCU - NU_DESC_COCU + NU_RECA_COCU + NU_OTRE_COCU - " & valor
   End If
   Result = DoUpdate(Desde, Valores, Condicion)
   If CountRecords = 0 Then Result = FAIL
   Actualiza_Valor_Persona = Result
End Function


'Borra un registro de la tabla - CONTRATO_DETALLE_CUOTA
'Auto_Contrato => Autonumerico contrato
'Auto_Pers_Nat => Autonumerico persona natural
Public Function Elimina_Contrato_Detalle_Cuota(ByVal Auto_Contrato As Long, ByVal Auto_Pers_Nat As Long) As Integer
   Desde = "CONTRATO_DETALLE_CUOTA"
   Condicion = " NU_AUTO_CONT_COCU=" & Auto_Contrato
   Condicion = Condicion & "AND NU_AUTO_PENA_COCU = " & Auto_Pers_Nat
   Result = DoDelete(Desde, Condicion)
   Debug.Print CountRecords
   Elimina_Contrato_Detalle_Cuota = Result
   
End Function

'contrato         => Autonumerico contrato
'Persona_Natural  => Autonumerico persona natural
'fh_cambio        => Fecha de Inicio del valor de la cuota de un afiliado
'Val_base         => Valor base de la cuota sin recargos, ni descuentos
'Val_Recargo      => Valor de los descuentos de un afiliado
'Val_Descto       => Valor neto
'Operacion        => Tipo de modificaci�n (A - Agrega un nuevo registro a partir de uno existente
'                                            - Cambia la fecha fin con base a la fecha que ingresa menos un dia)
'                                         (N - Inserta un nuevo registro)

'                                         (A - Ampliacion - Cambian Fecha final)
'                                         (ME-Movimiento exclusion

Public Function Registro_Valores_Persona(ByVal Contrato As Long, ByVal Persona_Natural As Long, ByVal fh_cambio As Date, ByVal Val_base As Double, ByVal Val_Recargo As Double, ByVal Val_Descto As Double, ByVal Operacion As TipoRegistro, ByVal Val_OtrDes As Double, ByVal Val_OtrRec As Double) As Integer
   'Declaracion de variables
   Dim arr()      As Variant
   Dim Val_Neto   As Double
   Dim Fh_up      As String
   Dim Fh_Hasta   As Date
   Dim update     As Boolean
   
   '*****************************
   update = False
   If Operacion = Nuevo Then
      ReDim arr(1)
      Condicion = "NU_AUTO_CONT = " & Contrato
      Result = LoadData("CONTRATO", "FE_FINA_CONT,TX_ETAP_CONT", Condicion, arr)
      If Result = FAIL Or Not Encontro Then Result = FAIL: Registro_Valores_Persona = Result: Exit Function

      If arr(1) = "A" Then 'abierto then
         Condicion = "NU_AUTO_CONT_COCU=" & Contrato
         Condicion = Condicion & " AND NU_AUTO_PENA_COCU=" & Persona_Natural
         Result = DoDelete("CONTRATO_DETALLE_CUOTA", Condicion)
         If Result = FAIL Then Registro_Valores_Persona = Result: Exit Function
      'Else
         'nuevo, aplica para inclusiones
      '   If Result <> FAIL Then Result = Calculo_Parcial(Contrato, Persona_Natural, Val_base, Val_Recargo, Val_Descto, fh_cambio)
      End If

      'Crea nuevo registro
      If Result <> FAIL Then
         If CDate(fh_cambio) > CDate(arr(0)) Then fh_cambio = CDate(arr(0))
         Fh_Hasta = CDate(arr(0))
      End If
   Else
      'Asignaci�n de valor a variables de trabajo
      ReDim arr(7)
      Campos = "NU_AUTO_COCU,FE_INIC_COCU, FE_FINA_COCU, NU_BASE_COCU" & Coma
      Campos = Campos & " NU_RECA_COCU, NU_DESC_COCU,NU_OTRE_COCU,NU_OTDE_COCU"
      Condicion = " NU_AUTO_CONT_COCU = " & Contrato
      Condicion = Condicion & " AND NU_AUTO_PENA_COCU = " & Persona_Natural
      Condicion = Condicion & " AND FE_INIC_COCU<=" & FFechaCon(fh_cambio)
      Condicion = Condicion & " AND FE_FINA_COCU>=" & FFechaCon(fh_cambio)
      Result = LoadData("CONTRATO_DETALLE_CUOTA", Campos, Condicion, arr)
      If Result = FAIL Or Not Encontro Then Result = FAIL: Registro_Valores_Persona = Result: Exit Function
   
      If Operacion = Agregar Then
         'Actualiza el registro
         Fh_up = IIf(CDate(fh_cambio) - 1 < CDate(arr(1)), CDate(arr(1)), CDate(fh_cambio) - 1)
         'borra los registros que tenga
         If Fh_up = fh_cambio Then
            Condicion = "NU_AUTO_COCU=" & CLng(arr(0))
            Result = DoDelete("CONTRATO_DETALLE_CUOTA", Condicion)
            'mod prorrateo FEB 26
            If Val_OtrRec = 0 And CDbl(arr(6)) <> 0 Then Val_OtrRec = CDbl(arr(6))
            If Val_OtrDes = 0 And CDbl(arr(7)) <> 0 Then Val_OtrDes = CDbl(arr(7))
            
         Else
            Condicion = "NU_AUTO_COCU=" & CLng(arr(0))
            Valores = "FE_FINA_COCU = " & FFechaIns(Fh_up)
            Result = DoUpdate("CONTRATO_DETALLE_CUOTA", Valores, Condicion)
            If Result = FAIL Then Registro_Valores_Persona = Result: Exit Function
         End If
         Fh_Hasta = CDate(arr(2))
         
      ElseIf Operacion = ModDescuento Then
         If CDate(arr(1)) = fh_cambio Then
            Fh_Hasta = CDate(arr(2))
            Val_base = CDbl(arr(3))
            Val_Recargo = CDbl(arr(4))
            Val_OtrRec = CDbl(arr(6))
            If Val_OtrDes = 0 Then Val_OtrDes = CDbl(arr(7))
            update = True
         Else
            Fh_Hasta = CDate(arr(2))
            Val_base = CDbl(arr(3))
            Val_Recargo = CDbl(arr(4))
            'Actualiza el registro
            Fh_up = DateAdd("d", -1, fh_cambio)
            Condicion = "NU_AUTO_COCU=" & CLng(arr(0))
            Valores = "FE_FINA_COCU = " & FFechaIns(Fh_up) & Coma
            Valores = Valores & "NU_BASE_COCU = " & Val_base & Coma
            Valores = Valores & "NU_RECA_COCU = " & Val_Recargo
            Result = DoUpdate("CONTRATO_DETALLE_CUOTA", Valores, Condicion)
            If Result = FAIL Or CountRecords = 0 Then Result = FAIL: Registro_Valores_Persona = Result: Exit Function
         End If
      ElseIf Operacion = ModOtrDes Then
         Val_base = CDbl(arr(3))
         Val_Recargo = CDbl(arr(4))
         Val_Descto = CDbl(arr(5))
         Val_OtrRec = CDbl(arr(6))
         fh_cambio = CDate(arr(1))
         Fh_Hasta = CDate(arr(2))
         update = True
      ElseIf Operacion = ModOtrRec Then
         Val_base = CDbl(arr(3))
         Val_Recargo = CDbl(arr(4))
         Val_Descto = CDbl(arr(5))
         Val_OtrDes = CDbl(arr(7))
         fh_cambio = CDate(arr(1))
         Fh_Hasta = CDate(arr(2))
         update = True
      End If
   End If
   
   Val_Neto = Val_base - Val_Descto + Val_Recargo - Val_OtrDes + Val_OtrRec
   Valores = "NU_AUTO_CONT_COCU = " & Contrato & Coma
   Valores = Valores & "NU_AUTO_PENA_COCU = " & Persona_Natural & Coma
   Valores = Valores & "FE_INIC_COCU = " & FFechaIns(fh_cambio) & Coma
   Valores = Valores & "FE_FINA_COCU = " & FFechaIns(Fh_Hasta) & Coma
   Valores = Valores & "NU_BASE_COCU = " & Val_base & Coma
   Valores = Valores & "NU_RECA_COCU = " & Val_Recargo & Coma
   Valores = Valores & "NU_DESC_COCU = " & Val_Descto & Coma
   Valores = Valores & "NU_NETO_COCU = " & Val_Neto & Coma
   Valores = Valores & "NU_OTRE_COCU = " & Val_OtrRec & Coma
   Valores = Valores & "NU_OTDE_COCU = " & Val_OtrDes
   If Result <> FAIL And update = False Then
      Result = DoInsertSQL("CONTRATO_DETALLE_CUOTA", Valores)
   Else
      Condicion = "NU_AUTO_COCU=" & CLng(arr(0))
      Result = DoUpdate("CONTRATO_DETALLE_CUOTA", Valores, Condicion)
   End If
   Registro_Valores_Persona = Result

End Function
Public Function Busca_Nucleos_Contrato(ByVal No_Factura As String, ByVal No_Documento As Long) As Boolean
   Dim arrNucleos() As Variant
   Dim i As Integer
   ReDim arrNucleos(i)
   
   Campos = "R_CONTRATO_AFILIADO.NU_GRUP_COPN"
   Desde = "TIPO_MOVIMIENTO, CONTRATO, R_CONTRATO_AFILIADO "
   Condicion = "CONTRATO.NU_AUTO_CONT = TIPO_MOVIMIENTO.NU_AUTO_CONT_TIMO "
   Condicion = Condicion & "AND CONTRATO.NU_AUTO_CONT = R_CONTRATO_AFILIADO.NU_AUTO_CONT_COPN "
   Condicion = Condicion & "AND TIPO_MOVIMIENTO.TX_FACT_TIMO = " & Comi & No_Factura & Comi
   Condicion = Condicion & " AND TIPO_MOVIMIENTO.NU_AUTO_NUDO_TIMO = " & No_Documento
   Condicion = Condicion & " GROUP BY R_CONTRATO_AFILIADO.NU_GRUP_COPN "
   Result = LoadMulData(Desde, Campos, Condicion, arrNucleos)
   If Result = FAIL Then
      Busca_Nucleos_Contrato = True
      Exit Function
   Else
      If UBound(arrNucleos, 2) = 0 Then
         Busca_Nucleos_Contrato = True
      Else
         Busca_Nucleos_Contrato = False
      End If
   End If
End Function

'Calcula el valor de una persona en un numero de dias
Private Function Calculo_Parcial(ByVal Contrato As Long, ByVal Persona_Natural As Long, ByVal ValorBase As Double, ByVal ValorRecargo As Double, ByVal ValorDescuento As Double, ByRef fh_cambio As Date) As Integer
Dim dias    As Long     'Cantidad de dias de diferencia entre dos fechas
Dim nroadc  As Long     'Numero de meses de 31 dias entre dos fechas
Dim fecaux  As Date     'Fecha auxiliar para calculos
ReDim arr(1)
   
   Calculo_Parcial = FAIL
   
   Campos = "FE_INIC_CUOT,FE_VENC_CUOT"
   Condicion = "NU_AUTO_CONT_CUOT=" & Contrato
   Condicion = Condicion & " AND FE_INIC_CUOT<=" & FFechaCon(fh_cambio)
   Condicion = Condicion & " AND FE_VENC_CUOT>=" & FFechaCon(fh_cambio)
   Result = LoadData("CUOTA_CONTRATO", Campos, Condicion, arr)
   If Result = FAIL Or Not Encontro Then Exit Function
   
   If CDate(arr(0)) <> CDate(fh_cambio) Then
      fecaux = fh_cambio
      dias = Abs(DateDiff("d", fh_cambio, CDate(arr(1)))) + 1
      Do While fecaux <= CDate(arr(1))
         fecaux = DateAdd("d", 1, fecaux)
         If Day(fecaux) = 31 Then
            'Si la fecha de inclusion
            If Not (Day(CDate(fh_cambio)) = 31 And Day(fecaux) = 31) Then nroadc = nroadc + 1
         End If
      Loop
      dias = dias - nroadc
      Valores = "NU_AUTO_CONT_COCU = " & Contrato & Coma
      Valores = Valores & "NU_AUTO_PENA_COCU = " & Persona_Natural & Coma
      Valores = Valores & "FE_INIC_COCU = " & FFechaIns(fh_cambio) & Coma
      Valores = Valores & "FE_FINA_COCU = " & FFechaIns(CDate(arr(1))) & Coma
      Valores = Valores & "NU_BASE_COCU = " & ValorBase & Coma
      Valores = Valores & "NU_RECA_COCU = " & ValorRecargo & Coma
      Valores = Valores & "NU_DESC_COCU = " & ValorDescuento & Coma
      Valores = Valores & "NU_NETO_COCU = " & (ValorBase - ValorDescuento + ValorRecargo)
      Result = DoInsertSQL("CONTRATO_DETALLE_CUOTA", Valores)
      fh_cambio = DateAdd("d", CDate(arr(1)), 1)
   End If
   
   Calculo_Parcial = Result
   
End Function

'Esta funcion calcula el valor de la tarifa por nucleo
'Parametros AutoContrato:  Autonumerico del contrato
'           Grupo:         Numero del nucleo del contrato
'           Tipo:          0-Toma solos los afiliados que estan grabados en la tabla
'                          1-Toma los afiliados que estan en la tabla junto con los que estan en el arreglo de movimientos
'           Agregar        True, si esta agregando un afiliado, false si esta quitandolo
'           CargarTabla    True, carga los datos de la tabla

Public Function Tarifa_X_Nucleo(ByVal AutoContrato As Long, ByVal grupo As Long, ByVal Tipo As Byte, ByVal autonegocio As Long, arreglo() As Movimientos, ByVal AutoTarifaNucleo As Long, ByVal Agregar As Boolean, ByRef Ope As TipoRegistro) As Integer
Dim ParAut     As Double    'Autonumerico parentesco hijo
Dim ParEda     As Double    'Edad maxima para parentesco hijo
Dim ParNeg     As Double    'Autonumerico del negocio de grupo etareo
Dim Edad       As Long      'Edad de la persona
Dim Prima      As Double    'Valor de la prima
Dim Base       As Double    'Valor base del plan
Dim suma       As Double    'Sumatoria de los factores x grupo etareo de los integrantes del nucleo
Dim aux        As Double    'Variable auxiliar para hacer calculos
Dim prtnuc     As Double    'porcentaje de la tabla tipo nucleo
Dim acumu      As Double    'acumulado del valor de las cuotas
Dim i          As Integer
Dim arrnue()   As Variant
Dim arrprt()   As Variant   'arreglo de porcentajes segun grupo etareo
ReDim arrnue(0)
ReDim arr(8, 0)

   'Inicializa variables
   Tarifa_X_Nucleo = FAIL
   suma = 0
   prtnuc = 0
   acumu = 0
   
   'Carga las personas de un contrato y un nucleo
   ParAut = CargarParametro("03")
   ParEda = CargarParametro("04")
   ParNeg = CargarParametro("11")
   
   If ParAut = -1 Or ParEda = -1 Then Call Mensaje1("No se encontro el parentesco de los parametros generales", 3): Exit Function
   If ParNeg = -1 Then Call Mensaje1("No se encontro el negocio de los parametros generales", 3): Exit Function
      
   Desde = "R_CONTRATO_AFILIADO,PERSONA_NATURAL"
   Campos = "NU_AUTO_COPN,NU_AUTO_PLAN_COPN,FE_INGR_COPN,FE_NACI_PENA,TX_GENE_PENA,TX_TIPO_COPN,NU_AUTO_PARE_COPN,'',NU_AUTO_PENA_COPN"
   Condicion = "NU_AUTO_PENA_COPN=NU_AUTO_PENA"
   Condicion = Condicion & " AND NU_AUTO_CONT_COPN=" & AutoContrato
   Condicion = Condicion & " AND NU_GRUP_COPN=" & grupo
   Condicion = Condicion & " AND TX_ESTA_COPN=" & Comi & "A" & Comi
   Condicion = Condicion & " ORDER BY TX_TIPO_COPN DESC, NU_AUTO_COPN"
   Result = LoadMulData(Desde, Campos, Condicion, arr)
   If (Result = FAIL Or Not Encontro) And Agregar = True And Tipo = 0 Then Exit Function
   If (Result = FAIL) Then Exit Function
   If (Not Encontro) And Agregar = False And Tipo = 0 Then Tarifa_X_Nucleo = SUCCEED: Exit Function

   'If (Not Encontro) And Agregar = False And Tipo = 1 Then Tarifa_X_Nucleo = SUCCEED: Exit Function
   
   If Tipo = 1 Then  'Agrega los datos que estan en el arreglo de movimientos
      For i = 0 To UBound(arreglo, 1)
         If arreglo(i).grupo = grupo Then 'Si son solo del mismo grupo
            If Not (arr(0, 0)) = NUL$ Then
               ReDim Preserve arr(8, UBound(arr, 2) + 1)
            End If
            arr(0, UBound(arr, 2)) = arreglo(i).Contrato
            arr(1, UBound(arr, 2)) = arreglo(i).Plan
            arr(2, UBound(arr, 2)) = arreglo(i).FechaIngreso
            arr(3, UBound(arr, 2)) = arreglo(i).FechaNacimiento
            arr(4, UBound(arr, 2)) = arreglo(i).Genero
            arr(5, UBound(arr, 2)) = arreglo(i).Tipo
            arr(6, UBound(arr, 2)) = arreglo(i).Parentesco
            arr(7, UBound(arr, 2)) = i                            'posicion del arreglo de movimientos
            arr(8, UBound(arr, 2)) = arreglo(i).Persona
         End If
      Next i
   End If
      
   ReDim arrprt(0, UBound(arr, 2))
   
   For i = 0 To UBound(arr, 2)
      'Busca la edad del titular
      If arr(3, i) = "" Then Exit For
      Edad = Abs(DateDiff("YYYY", CDate(arr(3, i)), CDate(arr(2, i))))
      If ParAut = CLng(arr(6, i)) And Edad <= ParEda Then
         Campos = "NU_FAHI_FAGE"
      Else
         If arr(4, i) = "F" Then Campos = "NU_FAFE_FAGE"
         If arr(4, i) = "M" Then Campos = "NU_FAMA_FAGE"
      End If
   
      'Busca una tipo de grupo etareo vigente-y que contenga la edad del afiliado
      Condicion = "NU_AUTO_NEGO_PEGE=" & ParNeg
      Condicion = Condicion & " AND FE_INIC_PEGE<=" & FFechaCon(CDate(arr(2, i)))
      Condicion = Condicion & " AND FE_FINA_PEGE>=" & FFechaCon(CDate(arr(2, i)))
      Condicion = Condicion & " AND (NU_EDIN_FAGE<=" & Edad
      Condicion = Condicion & " AND  NU_EDFI_FAGE>=" & Edad & SParC
      Condicion = Condicion & " AND NU_AUTO_PEGE=NU_AUTO_PEGE_FAGE"
      Desde = "PERIODO_GRUPO_ETAREO,FACTOR_GRUPO_ETAREO"
      Result = LoadData(Desde, Campos, Condicion, arrnue)
      If Result = FAIL Or Not Encontro Then Call Mensaje1("No se encontro el grupo etareo", 3): Exit Function
      
      arrprt(0, i) = CDbl(arrnue(0))
      suma = suma + CDbl(arrnue(0))
      
      'Si el afiliado es el titular busca el valor segun tabla de tipo de nucleo
      If arr(5, i) = "T" Then
         Desde = "PERIODO_TIPO_NUCLEO,FACTOR_TIPO_NUCLEO"
         Campos = "NU_FACT_FATN"
         Condicion = "NU_AUTO_PETN=NU_AUTO_PETN_FATN"
         Condicion = Condicion & " AND NU_AUTO_NEGO_PETN=" & autonegocio
         Condicion = Condicion & " AND (NU_EDIN_FATN<=" & Edad
         Condicion = Condicion & " AND  NU_EDFI_FATN>=" & Edad & SParC
         Condicion = Condicion & " AND (FE_INIC_PETN<=" & FFechaCon(CDate(arr(2, i)))
         Condicion = Condicion & " AND  FE_FINA_PETN>=" & FFechaCon(CDate(arr(2, i))) & SParC
         Condicion = Condicion & " AND NU_AUTO_TINU_FATN=" & AutoTarifaNucleo
         Result = LoadData(Desde, Campos, Condicion, arrnue)
         If Result = FAIL Or Not Encontro Then Call Mensaje1("No se encontro el grupo etareo", 3): Exit Function
         prtnuc = CDbl(arrnue(0))
         
         ReDim arrplan(0)
         Condicion = "NU_AUTO_PLAN=" & CLng(arr(1, i))
         Result = LoadData("PLAN", "NU_BASE_PLAN", Condicion, arrplan)
         If Result = FAIL Or Not Encontro Then Call Mensaje1("No se encontro el valor base del plan ", 3): Exit Function
         Base = CDbl(arrplan(0))
      
      End If
   Next i
   
   For i = 0 To UBound(arr, 2)
      If suma <> 0 Then
         aux = FormatNumber((CDbl(arrprt(0, i)) / suma), 4)
      Else
         aux = 0
      End If
      Prima = ((Base * aux) * prtnuc)
      Prima = FormatNumber(Prima, 2)
      acumu = acumu + Prima
      If acumu > (prtnuc * Base) Then
         Prima = Prima - FormatNumber((acumu - (prtnuc * Base)), 2)
         acumu = (prtnuc * Base)
      ElseIf acumu < (prtnuc * Base) And i = UBound(arr, 2) Then
         Prima = Prima + FormatNumber(((prtnuc * Base) - acumu), 2)
         acumu = (prtnuc * Base)
      End If
      
      If Tipo = 0 Then 'Guarda en la tabla
         Result = Registro_Valores_Persona(AutoContrato, CLng(arr(8, i)), CDate(arr(2, i)), Prima, 0, 0, Ope, 0, 0)
         If Result = FAIL Then Exit Function
      Else
         If arr(7, i) <> NUL$ Then
            arreglo(arr(7, i)).Cuota = Prima
         End If
      End If
   Next i
   Tarifa_X_Nucleo = Result
End Function


'Retorna el valor por persona de la nota correspondiente, tanto de lo
'correspondiente a facturado y a no facturado
'FechaInicio: Fecha de Inicio a partir de la que comienza el calculo
'Contrato:     Autonumerico del contrato
'Retorna el valor de lo facturado y sin facturar por persona y por cuota....persona-sin facturar-facturado,numero cuota

Public Function Retornar_Valores_Persona(ByVal Contrato As Long, ByVal FechaInicio As Date, ByVal CondAfiliados As String, ByRef facturado As Single, ByRef nofacturado As Single, ByRef arrpersonas() As Variant, ByVal Agrupado As Boolean)
Dim i                As Integer
Dim j                As Integer
Dim k                As Integer
Dim dias             As Integer
Dim val_sinfacturar  As Single
Dim val_facturado    As Single
Dim ArrValAfiliado() As Variant
Dim auxdias          As Long
Dim auxvalor         As Single
            
   ReDim arrpersonas(5, 0)
   
   If CondAfiliados = "" Then
      Call Mensaje1("La condicion de seleccion de afiliados no puede ser vacia", 3)
      Exit Function
   End If

   ReDim arr(4, 0)
   Condicion = "NU_AUTO_CONT_CUOT=" & Contrato
   'Condicion = Condicion & " AND FE_FINI_CUOT<=" & FFechaCon(FechaInicio)
   Condicion = Condicion & " AND FE_VENC_CUOT>=" & FFechaCon(FechaInicio)
   Campos = "NU_AUTO_CUOT,NU_NUME_CUOT,FE_INIC_CUOT,FE_VENC_CUOT,TX_ESTA_CUOT"
   Result = LoadMulData("CUOTA_CONTRATO", Campos, Condicion, arr)
   If Result = FAIL Or Not Encontro Then Exit Function
   
   If Agrupado = False Then
      'Usado para muerte de titular
      For i = 0 To UBound(arr, 2)
         DoEvents
         dias = DateDiff("d", CDate(arr(2, i)), CDate(arr(3, i))) + 1
         dias = dias / 30

         'Trae el valor de la cuota de la persona
         ReDim arraux(0, 0)
         Campos = "NU_AUTO_PENA_COPN"
         Condicion = CondAfiliados
         Result = LoadMulData("R_CONTRATO_AFILIADO", Campos, Condicion, arraux)
         If Result = FAIL Then Exit Function

         If Encontro Then
            
            For j = 0 To UBound(arraux, 2)
               If arrpersonas(0, 0) <> NUL$ Then
                  ReDim Preserve arrpersonas(5, UBound(arrpersonas, 2) + 1)
               End If
               
               DoEvents

               Result = Valor_Cuota_Persona(Contrato, arraux(0, j), ArrValAfiliado, CDate(arr(3, i)))
               If Result = FAIL Then Exit Function

               If FechaInicio >= CDate(arr(2, i)) And FechaInicio <= CDate(arr(3, i)) Then
                  auxdias = DateDiff("d", FechaInicio, CDate(arr(3, i))) + 1 - Dias_Adicionales(FechaInicio, arr(3, i))
                  If DateDiff("M", FechaInicio, CDate(arr(3, i))) = 1 Then
                     If auxdias > 30 Then auxdias = 30
                  End If
                  
                  auxvalor = Redondeo_Aritmetico(((ArrValAfiliado(3)) / 30) * auxdias, 2)
               Else
                  auxdias = DateDiff("d", CDate(arr(2, i)), CDate(arr(3, i))) + 1 - Dias_Adicionales(CDate(arr(2, i)), arr(3, i))
                  If DateDiff("d", CDate(arr(2, i)), CDate(arr(3, i))) = 1 Then
                     If auxdias > 30 Then auxdias = 30
                  End If
                  
                  auxvalor = Redondeo_Aritmetico(((ArrValAfiliado(3)) / 30) * auxdias, 2)
               End If

               If arr(4, i) = "S" Then
                  arrpersonas(0, UBound(arrpersonas, 2)) = arraux(0, j)          'autonumerico persona
                  arrpersonas(2, UBound(arrpersonas, 2)) = auxvalor              'valor sin facturar
                  arrpersonas(3, UBound(arrpersonas, 2)) = 0                     'valor no facturado
                  val_sinfacturar = val_sinfacturar + auxvalor
               ElseIf arr(4, i) = "F" Then
                  arrpersonas(0, UBound(arrpersonas, 2)) = arraux(0, j)          'autonumerico persona
                  arrpersonas(2, UBound(arrpersonas, 2)) = 0                     'valor sin factura
                  arrpersonas(3, UBound(arrpersonas, 2)) = auxvalor              'valor facturado
                  val_facturado = val_facturado + auxvalor
               End If

               arrpersonas(1, UBound(arrpersonas, 2)) = arr(1, i)                'numero cuota
            Next j
         End If
      Next i
   Else
      'usado para desafilaciones
      'Devuelve los valores por persona si es facturado
      'devuelve los valores por cuota si no es facturado
      'ReDim Preserve arrpersonas(5, UBound(arr, 2))
      For i = 0 To UBound(arr, 2)
      
         DoEvents
         dias = DateDiff("d", CDate(arr(2, i)), CDate(arr(3, i))) + 1
         dias = (dias / 30)
         
         Call Valor_Cuota_Contrato(Contrato, CLng(arr(0, i)), ArrValAfiliado)
         
         If FechaInicio >= CDate(arr(2, i)) And FechaInicio <= CDate(arr(3, i)) Then
            auxdias = DateDiff("d", FechaInicio, CDate(arr(3, i))) + 1 - Dias_Adicionales(FechaInicio, arr(3, i))
            auxvalor = ((ArrValAfiliado(3)) / (dias * 30)) * auxdias
         Else
            auxdias = DateDiff("d", CDate(arr(2, i)), CDate(arr(3, i))) + 1 - Dias_Adicionales(CDate(arr(2, i)), arr(3, i))
            auxvalor = ((ArrValAfiliado(3)) / (dias * 30)) * auxdias
         End If
               
         If arr(4, i) = "S" Then
            If arrpersonas(0, 0) <> NUL$ Then
               ReDim Preserve arrpersonas(5, UBound(arrpersonas, 2) + 1)
            End If
            arrpersonas(0, UBound(arrpersonas, 2)) = arr(0, i)             'Autonumerico cuota
            arrpersonas(1, UBound(arrpersonas, 2)) = arr(1, i)             'Numero de la cuota
            arrpersonas(2, UBound(arrpersonas, 2)) = auxvalor              'valor sin facturar
            arrpersonas(3, UBound(arrpersonas, 2)) = 0                     'valor facturado
            'val_sinfacturar = val_sinfacturar + auxvalor
            val_sinfacturar = Format(val_sinfacturar, "#,##0.00") + CDbl(CStr(Format(auxvalor, "#,##0.00")))
         ElseIf arr(4, i) = "F" Then
            'Trae el valor de la cuota de la persona
            ReDim arraux(0, 0)
            Campos = "NU_AUTO_PENA_COPN"
            Condicion = CondAfiliados
            Result = LoadMulData("R_CONTRATO_AFILIADO", Campos, Condicion, arraux)
            If Result = FAIL Then Exit Function

            If Encontro Then
               'recorre las personas encontradas
               For j = 0 To UBound(arraux, 2)
                  DoEvents
                  If arrpersonas(0, 0) <> NUL$ Then
                     ReDim Preserve arrpersonas(5, UBound(arrpersonas, 2) + 1)
                  End If
   
                  Result = Valor_Cuota_Persona(Contrato, arraux(0, j), ArrValAfiliado, CDate(arr(3, i)))
                  If Result = FAIL Then Exit Function
   
                  If FechaInicio >= CDate(arr(2, i)) And FechaInicio <= CDate(arr(3, i)) Then
                     auxdias = DateDiff("d", FechaInicio, CDate(arr(3, i))) + 1 - Dias_Adicionales(FechaInicio, arr(3, i))
                     auxvalor = Redondeo_Aritmetico(((ArrValAfiliado(3)) / 30) * auxdias, 2)
                  Else
                     auxdias = DateDiff("d", CDate(arr(2, i)), CDate(arr(3, i))) + 1 - Dias_Adicionales(CDate(arr(2, i)), arr(3, i))
                     auxvalor = Redondeo_Aritmetico(((ArrValAfiliado(3)) / 30) * auxdias, 2)
                  End If
   
                  If arr(4, i) = "S" Then
                     arrpersonas(0, UBound(arrpersonas, 2)) = arraux(0, j)          'Autonumerico persona natural
                     arrpersonas(2, UBound(arrpersonas, 2)) = CStr(Format(auxvalor, "#,##0.00"))              'Valor no facturado a la persona
                     arrpersonas(3, UBound(arrpersonas, 2)) = 0                     'valor facturado a la persona
                     val_sinfacturar = Format(val_sinfacturar, "#,##0.00") + CDbl(CStr(Format(auxvalor, "#,##0.00")))
                  ElseIf arr(4, i) = "F" Then
                     arrpersonas(0, UBound(arrpersonas, 2)) = arraux(0, j)          'Autonumerico persona natural
                     arrpersonas(2, UBound(arrpersonas, 2)) = 0                     'valor no facturado
                     arrpersonas(3, UBound(arrpersonas, 2)) = CStr(Format(auxvalor, "#,##0.00"))          'valor facutrado,
                     val_facturado = Format(val_facturado, "#,##0.00") + CDbl(CStr(Format(auxvalor, "#,##0.00")))
                  End If
   
                  arrpersonas(1, UBound(arrpersonas, 2)) = arr(1, i)                 'Numero de la cuota
               Next j
            End If
         End If
      Next i
   End If
   facturado = val_facturado
   nofacturado = val_sinfacturar
   Retornar_Valores_Persona = Result
End Function

'Devuelve cuantos dias adicionales de 31 hay entre dos fechas
Public Function Dias_Adicionales(ByVal FechaInicio As Date, ByVal FechaFinal As Date) As Long
Dim dias As Long
   Do While FechaInicio <= FechaFinal
      If Day(FechaInicio) = 31 Then dias = dias + 1
     
      If Month(FechaInicio) = 2 And ((Day(FechaInicio) = 28) And Month(CDate(FechaInicio) + 1) = 3) Then
         dias = dias - 2
      ElseIf Month(FechaInicio) = 2 And ((Day(FechaInicio) = 29) And Month(CDate(FechaInicio) + 1) = 3) Then
         dias = dias - 1
      End If
     FechaInicio = CDate(FechaInicio) + 1
   Loop
   Dias_Adicionales = dias
End Function

'*****************************************************************************************
'Carga las fechas de aniversario de un contrato, o la fecha maxima y minima del movimiento
'Parametros:   Contrato= Autonumerico del contrato
'              Cbo= combo donde se cargan las fechas aniversario
'              Dtp= picker para contratos prorrateados
'              Tipo=Tipo de movimiento desde donde se llama la funcion
'              Prorrateo= True, si es prorrateado o no
'Proceso:      Carga las fechas de aniversario del contrato, y el numero de cuota al que pertenece la fecha
'               o la fecha minima y maxima que corresponda
'*****************************************************************************************'


Public Function Determinar_Fechas_Movimiento(ByVal Contrato As Long, ByRef Cbo As VB.ComboBox, ByRef Dtp As DTPicker, ByVal Tipo As TipoMovimiento, ByVal Prorrateo As Boolean) As Integer
ReDim arr(1)
Dim meses      As Integer
Dim Fecha      As String
Dim Fechaini   As String
Dim atras      As Long     'Numero de dias atras
Dim i          As Integer
Dim fechaMin   As Date
Dim fechaMax   As Date
Dim finvig     As Date


   Cbo.Clear
   Condicion = "NU_AUTO_CONT=" & Contrato
   Result = LoadData("CONTRATO", "FE_INIC_CONT,FE_FINA_CONT", Condicion, arr)
   If Result <> FAIL And Encontro Then
      Fecha = DateAdd("d", -1, CDate(arr(0)))
      meses = DateDiff("m", Fecha, CDate(arr(1)))
      Fecha = CDate(arr(0))
      Fechaini = Fecha
      fechaMin = CDate(arr(0))
      fechaMax = CDate(arr(1))
      finvig = CDate(arr(1))
      
      If Tipo = Desafiliacion Then
         atras = CargarParametro("72")
      End If
      
      If Prorrateo = False Then
         Cbo.Visible = True
         Cbo.Enabled = True
         Dtp.Visible = False
         For i = 0 To meses
           'busca el numero de la cuota
           ReDim arr(0)
            Fecha = DateAdd("m", i, Fechaini)
            Condicion = "NU_AUTO_CONT_CUOT=" & Contrato
            Condicion = Condicion & " AND FE_INIC_CUOT<=" & FFechaCon(Fecha)
            Condicion = Condicion & " AND FE_VENC_CUOT>=" & FFechaCon(Fecha)
            Result = LoadData("CUOTA_CONTRATO", "NU_NUME_CUOT", Condicion, arr)
            If Result = FAIL Or Not Encontro Then
               If Tipo = Desafiliacion And Fecha > finvig Then
                  Fecha = DateAdd("m", i, Fechaini)
                  'Carga la ultima cuota pero coloca la fecha
                  Condicion = "NU_AUTO_CONT_CUOT=" & Contrato
                  Condicion = Condicion & " AND FE_INIC_CUOT<=" & FFechaCon(finvig)
                  Condicion = Condicion & " AND FE_VENC_CUOT>=" & FFechaCon(finvig)
                  Result = LoadData("CUOTA_CONTRATO", "NU_NUME_CUOT", Condicion, arr)
                  If Result <> FAIL And Encontro Then
                     Cbo.AddItem Format(Fecha, "yyyy/mm/dd")
                     Cbo.ItemData(Cbo.NewIndex) = CLng(arr(0))
                     Determinar_Fechas_Movimiento = True
                     Exit Function
                  End If
               End If
               Result = FAIL: Exit For
            End If
            
            If atras <> 0 Then
               If CDate(Fecha) >= DateAdd("d", atras * (-1), FechaServer) Then
                  Cbo.AddItem Format(Fecha, "yyyy/mm/dd")
                  Cbo.ItemData(Cbo.NewIndex) = CLng(arr(0))
               End If
            Else
               Cbo.AddItem Format(Fecha, "yyyy/mm/dd")
               Cbo.ItemData(Cbo.NewIndex) = CLng(arr(0))
            End If
         
         Next
         If Cbo.ListCount > 0 Then Cbo.ListIndex = 0
      ElseIf Prorrateo = True Then
         Cbo.Visible = False
         Cbo.Enabled = False
         Dtp.Visible = True
         Dtp.Enabled = True
         If Tipo = Desafiliacion And atras <> 0 Then
            fechaMin = DateAdd("d", atras, FechaServer)
            fechaMax = DateAdd("d", 1, fechaMax)
            Call Dtp_Rango_Fechas(Dtp, fechaMin, fechaMax)
            Dtp.MinDate = fechaMin
         ElseIf Tipo = Exclusiondependiente Or Tipo = ExclusionTitular Then
            Dtp.MinDate = fechaMin
            Dtp.Value = Dtp.MinDate
         Else
            Dtp.SetFocus
            Dtp.MinDate = fechaMin
            Dtp.Value = Dtp.MinDate
         End If
      End If
   End If
   Determinar_Fechas_Movimiento = True
End Function

Public Function Numero_Cuota(ByVal Contrato As Long, ByVal Fecha As Date, ByRef Cuota As String, ByRef Inicio As String, ByRef final As String) As Integer
   ReDim arr(2)
   Condicion = "NU_AUTO_CONT_CUOT=" & Contrato
   Condicion = Condicion & " AND FE_INIC_CUOT<=" & FFechaCon(Fecha)
   Condicion = Condicion & " AND FE_VENC_CUOT>=" & FFechaCon(Fecha)
   Result = LoadData("CUOTA_CONTRATO", "NU_NUME_CUOT,FE_INIC_CUOT,FE_VENC_CUOT", Condicion, arr)
   If Result = FAIL Then Exit Function
   If Encontro Then
      Cuota = val(arr(0))
      Inicio = Format(CDate(arr(1)), "yyyy/mm/dd")
      final = Format(CDate(arr(2)), "yyyy/mm/dd")
   End If
   Numero_Cuota = Result
End Function

'Esta funcion retorna el tipo de fecha de emision para la facturacion
Public Function Tipo_Fecha_Emision(ByVal Negocio As Long, ByRef TipoEmision As String) As Integer
   Tipo_Fecha_Emision = FAIL
   
   ReDim arr(0)
   Condicion = "NU_AUTO_NEGO=" & Negocio
   Result = LoadData("NEGOCIO", "TX_EMIS_NEGO", Condicion, arr)
   If Result = FAIL Or Not Encontro Then Exit Function
   TipoEmision = arr(0)
   
   Tipo_Fecha_Emision = Result
End Function

'Consulta cual es el vendedor o broker del contrato
'Parametros:   AutNumContrato: Autonumerico del contrato, Tipo=0 Vendedor, 1: Broker
Private Function Consultar_Ejecutivo_Fact(ByVal Tipo As Byte, ByVal AutNumContrato As Long, ByRef Nombre As String) As Integer
Dim cadena As String
   
   Nombre = NUL$
   
   'Consulta  el vendedor
   ReDim arr(0)
   If Tipo = 0 Then
      Campos = "TX_CODI_VEND"
      Condicion = "NU_AUTO_CONT_COVE=" & AutNumContrato
      Condicion = Condicion & " AND NU_AUTO_VEND_COVE=NU_AUTO_VEND"
      Result = LoadData("R_CONTRATO_VENDEDOR, VENDEDOR", Campos, Condicion, arr)
      If Result = FAIL Then Consultar_Ejecutivo_Fact = NUL$: Exit Function
      
      If Encontro Then
         cadena = "NU_AUTO_VEND_TIMO=" & CDbl(arr(0)) & Coma
         cadena = cadena & "NU_AUTO_BROK_TIMO=NULL" & Coma
         Nombre = Restaurar_Comas_Comillas(CStr(arr(0)))
         Consultar_Ejecutivo_Fact = Result: Exit Function
      End If
   End If
   
   If Tipo = 1 Then
      Campos = "TX_DESC_TERC"
      Condicion = "NU_AUTO_CONT_COBR=" & AutNumContrato
      Condicion = Condicion & " AND NU_AUTO_BROK_COBR=NU_AUTO_BROK"
      Condicion = Condicion & " AND NU_AUTO_TERC_BROK=NU_AUTO_TERC"
      Result = LoadData("R_CONTRATO_BROKER,BROKER,TERCERO", Campos, Condicion, arr)
      If Result = FAIL Then Consultar_Ejecutivo_Fact = NUL$: Exit Function
      
      If Encontro Then
         cadena = "NU_AUTO_VEND_TIMO=NULL" & Coma
         cadena = cadena & "NU_AUTO_BROK_TIMO=" & CDbl(arr(0)) & Coma
         Nombre = Restaurar_Comas_Comillas(CStr(arr(1)))
         Consultar_Ejecutivo_Fact = Result: Exit Function
      Else
         cadena = "NU_AUTO_VEND_TIMO=NULL" & Coma
         cadena = cadena & "NU_AUTO_BROK_TIMO=NULL" & Coma
         Consultar_Ejecutivo_Fact = Result
      End If
   End If
   
End Function

