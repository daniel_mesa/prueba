VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Object = "{C932BA88-4374-101B-A56C-00AA003668DC}#1.1#0"; "MSMASK32.OCX"
Begin VB.Form frmPrmBARRAXARTICULO 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "C�digos de Barra X Art�culo"
   ClientHeight    =   7095
   ClientLeft      =   45
   ClientTop       =   435
   ClientWidth     =   11475
   Icon            =   "frmPrmBARRAXARTICULO.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   7095
   ScaleWidth      =   11475
   Begin MSComctlLib.Toolbar tlbARTICULOS 
      Height          =   600
      Left            =   2160
      TabIndex        =   10
      Top             =   2640
      Width           =   11475
      _ExtentX        =   20241
      _ExtentY        =   1058
      ButtonWidth     =   1667
      ButtonHeight    =   953
      Appearance      =   1
      ImageList       =   "imgBotones"
      _Version        =   393216
      BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
         NumButtons      =   1
         BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "&Importar"
            Key             =   "IMP"
            ImageIndex      =   8
         EndProperty
      EndProperty
      Begin MSComctlLib.ProgressBar pgbLLEVO 
         Height          =   255
         Left            =   4080
         TabIndex        =   12
         Top             =   170
         Visible         =   0   'False
         Width           =   4455
         _ExtentX        =   7858
         _ExtentY        =   450
         _Version        =   393216
         Appearance      =   1
      End
      Begin VB.ComboBox cboTIPO 
         Height          =   315
         Left            =   1080
         Style           =   2  'Dropdown List
         TabIndex        =   11
         Top             =   150
         Width           =   2655
      End
   End
   Begin MSComctlLib.Toolbar tlbCDBARRAS 
      Height          =   600
      Left            =   2160
      TabIndex        =   9
      Top             =   1560
      Visible         =   0   'False
      Width           =   11475
      _ExtentX        =   20241
      _ExtentY        =   1058
      ButtonWidth     =   1799
      ButtonHeight    =   953
      Appearance      =   1
      ImageList       =   "imgBotones"
      _Version        =   393216
      BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
         NumButtons      =   5
         BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "&Adicionar"
            Key             =   "ADD"
            ImageIndex      =   2
         EndProperty
         BeginProperty Button2 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "&Borrar"
            Key             =   "DEL"
            ImageIndex      =   6
         EndProperty
         BeginProperty Button3 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "&Cambiar"
            Key             =   "CHA"
            ImageIndex      =   7
         EndProperty
         BeginProperty Button4 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Enabled         =   0   'False
            Caption         =   "Ca&ncelar"
            Key             =   "CAN"
            ImageIndex      =   3
         EndProperty
         BeginProperty Button5 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Enabled         =   0   'False
            Caption         =   "&Guardar"
            Key             =   "SAV"
            ImageIndex      =   4
         EndProperty
      EndProperty
   End
   Begin VB.Frame frmVALORES 
      Caption         =   "Informaci�n"
      Height          =   2535
      Left            =   6240
      TabIndex        =   2
      Top             =   4320
      Width           =   4695
      Begin MSMask.MaskEdBox txtCDBARRA 
         Height          =   300
         Left            =   1680
         TabIndex        =   4
         Top             =   360
         Width           =   1575
         _ExtentX        =   2778
         _ExtentY        =   529
         _Version        =   393216
         ClipMode        =   1
         PromptInclude   =   0   'False
         MaxLength       =   13
         Mask            =   "9999999999999"
         PromptChar      =   "_"
      End
      Begin MSMask.MaskEdBox txtCIAL 
         Height          =   300
         Left            =   1680
         TabIndex        =   6
         Top             =   720
         Width           =   2775
         _ExtentX        =   4895
         _ExtentY        =   529
         _Version        =   393216
         ClipMode        =   1
         PromptInclude   =   0   'False
         MaxLength       =   20
         PromptChar      =   "_"
      End
      Begin MSMask.MaskEdBox txtREGI 
         Height          =   300
         Left            =   1680
         TabIndex        =   8
         Top             =   1080
         Width           =   2775
         _ExtentX        =   4895
         _ExtentY        =   529
         _Version        =   393216
         ClipMode        =   1
         PromptInclude   =   0   'False
         MaxLength       =   20
         PromptChar      =   "_"
      End
      Begin MSMask.MaskEdBox txtLABO 
         Height          =   300
         Left            =   1680
         TabIndex        =   13
         Top             =   1440
         Width           =   2775
         _ExtentX        =   4895
         _ExtentY        =   529
         _Version        =   393216
         ClipMode        =   1
         PromptInclude   =   0   'False
         MaxLength       =   20
         PromptChar      =   "_"
      End
      Begin VB.Label Label4 
         Alignment       =   1  'Right Justify
         Caption         =   "Laboratorio:"
         Height          =   255
         Left            =   240
         TabIndex        =   14
         Top             =   1485
         Width           =   1335
      End
      Begin VB.Label Label3 
         Alignment       =   1  'Right Justify
         Caption         =   "Registro:"
         Height          =   255
         Left            =   240
         TabIndex        =   7
         Top             =   1125
         Width           =   1335
      End
      Begin VB.Label Label2 
         Alignment       =   1  'Right Justify
         Caption         =   "Nombre Comercial:"
         Height          =   255
         Left            =   240
         TabIndex        =   5
         Top             =   765
         Width           =   1335
      End
      Begin VB.Label Label1 
         Alignment       =   1  'Right Justify
         Caption         =   "C�digo de Barra:"
         Height          =   255
         Left            =   240
         TabIndex        =   3
         Top             =   405
         Width           =   1335
      End
   End
   Begin MSComctlLib.ImageList imgBotones 
      Left            =   10920
      Top             =   3960
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   20
      ImageHeight     =   14
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   9
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmPrmBARRAXARTICULO.frx":058A
            Key             =   ""
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmPrmBARRAXARTICULO.frx":08DC
            Key             =   "ASG"
         EndProperty
         BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmPrmBARRAXARTICULO.frx":0C2E
            Key             =   "CAN"
         EndProperty
         BeginProperty ListImage4 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmPrmBARRAXARTICULO.frx":0F80
            Key             =   "SAV"
         EndProperty
         BeginProperty ListImage5 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmPrmBARRAXARTICULO.frx":12D2
            Key             =   "ADD"
         EndProperty
         BeginProperty ListImage6 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmPrmBARRAXARTICULO.frx":158C
            Key             =   "DEL"
         EndProperty
         BeginProperty ListImage7 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmPrmBARRAXARTICULO.frx":19CE
            Key             =   "CHA"
         EndProperty
         BeginProperty ListImage8 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmPrmBARRAXARTICULO.frx":1CF0
            Key             =   "IMP"
         EndProperty
         BeginProperty ListImage9 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmPrmBARRAXARTICULO.frx":2042
            Key             =   "EXPO"
         EndProperty
      EndProperty
   End
   Begin MSComctlLib.ListView lstARTICULOS 
      Height          =   3135
      Left            =   480
      TabIndex        =   0
      Top             =   960
      Width           =   10500
      _ExtentX        =   18521
      _ExtentY        =   5530
      View            =   3
      LabelEdit       =   1
      LabelWrap       =   -1  'True
      HideSelection   =   0   'False
      AllowReorder    =   -1  'True
      FullRowSelect   =   -1  'True
      GridLines       =   -1  'True
      _Version        =   393217
      ForeColor       =   -2147483640
      BackColor       =   -2147483643
      BorderStyle     =   1
      Appearance      =   1
      NumItems        =   2
      BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Text            =   "Nombre del Art�culo"
         Object.Width           =   7761
      EndProperty
      BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   1
         Text            =   "C�digo"
         Object.Width           =   3881
      EndProperty
   End
   Begin MSComctlLib.ListView lstCODIGOS 
      Height          =   2535
      Left            =   480
      TabIndex        =   1
      Top             =   4320
      Width           =   5460
      _ExtentX        =   9631
      _ExtentY        =   4471
      View            =   3
      LabelEdit       =   1
      LabelWrap       =   -1  'True
      HideSelection   =   0   'False
      AllowReorder    =   -1  'True
      FullRowSelect   =   -1  'True
      GridLines       =   -1  'True
      _Version        =   393217
      ForeColor       =   -2147483640
      BackColor       =   -2147483643
      BorderStyle     =   1
      Appearance      =   1
      NumItems        =   2
      BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Text            =   "Nombre del Art�culo"
         Object.Width           =   7761
      EndProperty
      BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   1
         Text            =   "C�digo"
         Object.Width           =   3881
      EndProperty
   End
End
Attribute VB_Name = "frmPrmBARRAXARTICULO"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Dim ArrUXB() As Variant
Dim Articulo As UnArticulo
Dim Dueno As New ElTercero
'Dim LaBodega As String, LaAccion As String     'DEPURACION DE CODIGO
Dim NumEnCombo As Integer
Private vclave As String, vActivar As Boolean
Private vContar As Integer
Private vArticulo As String, vCodigo As String
Private vAccion As String, vAviso As String
Public OpcCod        As String   'Opci�n de seguridad

Private Sub cboTIPO_GotFocus()
    If vActivar Then vActivar = False: Exit Sub
    NumEnCombo = Me.cboTIPO.ListIndex
End Sub

Private Sub Form_Activate()
'    Me.cboTIPO.SetFocus
If Me.cboTIPO.Enabled = True And Me.cboTIPO.Visible Then Me.cboTIPO.SetFocus    'NMSR M3642
End Sub

Private Sub Form_Load()
    Call CenterForm(MDI_Inventarios, Me)
    Dueno.IniXNit (Dueno.Nit)
    Me.tlbCDBARRAS.ImageList = Me.imgBotones
    Me.tlbCDBARRAS.Buttons.Item("SAV").Image = "SAV"
    Me.tlbCDBARRAS.Buttons.Item("CHA").Image = "CHA"
    Me.tlbCDBARRAS.Buttons.Item("DEL").Image = "DEL"
    Me.tlbCDBARRAS.Buttons.Item("CAN").Image = "CAN"
    Me.tlbCDBARRAS.Buttons.Item("ADD").Image = "ADD"
    
    Me.txtCDBARRA.MaxLength = 13
    Me.txtCDBARRA.Mask = String(Me.txtCDBARRA.MaxLength, "9")
    Me.txtCIAL.MaxLength = 20
    Me.txtCIAL.Mask = ">" & String(Me.txtCIAL.MaxLength, "a")
    Me.txtREGI.MaxLength = 20
    Me.txtREGI.Mask = ">" & String(Me.txtREGI.MaxLength, "a")
    Me.txtLABO.MaxLength = 20
    Me.txtLABO.Mask = ">" & String(Me.txtLABO.MaxLength, "a")
    Me.tlbARTICULOS.Align = vbAlignTop
    Me.tlbARTICULOS.Visible = True
    Me.tlbCDBARRAS.Align = vbAlignTop
    Me.tlbCDBARRAS.Visible = False
    Me.frmVALORES.Enabled = False
    Me.cboTIPO.Clear
    Me.cboTIPO.AddItem "Para la Venta"
    Me.cboTIPO.AddItem "Para El Consumo"
    NumEnCombo = -1
    Me.cboTIPO.ListIndex = 0
    vActivar = True
    
    Me.lstARTICULOS.ListItems.Clear
    Me.lstARTICULOS.Checkboxes = False
    Me.lstARTICULOS.HideSelection = False
    Me.lstARTICULOS.Width = 10500
    Me.lstCODIGOS.ColumnHeaders.Clear
    Me.lstCODIGOS.ColumnHeaders.Add , "COD", "C�digo", 0.25 * Me.lstCODIGOS.Width
    Me.lstCODIGOS.ColumnHeaders.Add , "CIA", "Nom C/ial", 0.4 * Me.lstCODIGOS.Width
    Me.lstCODIGOS.ColumnHeaders.Add , "REG", "Registro", 0.3 * Me.lstCODIGOS.Width
    Me.lstCODIGOS.ColumnHeaders.Add , "LAB", "Laboratorio", 0.3 * Me.lstCODIGOS.Width
    Me.lstARTICULOS.ColumnHeaders.Clear
    Me.lstARTICULOS.ColumnHeaders.Add , "NOAR", "Nombre del art�culo", 0.4 * Me.lstARTICULOS.Width
    Me.lstARTICULOS.ColumnHeaders.Add , "COAR", "C�digo", 0.12 * Me.lstARTICULOS.Width
    Me.lstARTICULOS.ColumnHeaders.Add , "NOCO", "Unidad de conteo", 0.12 * Me.lstARTICULOS.Width
    Me.lstARTICULOS.ColumnHeaders.Add , "COPR", "Costo", 0.1 * Me.lstARTICULOS.Width
    Me.lstARTICULOS.ColumnHeaders("COPR").Alignment = lvwColumnRight
    Me.lstARTICULOS.ColumnHeaders.Add , "NOUS", "Uso", 0.12 * Me.lstARTICULOS.Width
    Me.lstARTICULOS.ColumnHeaders.Add , "NOGR", "Grupo", 0.12 * Me.lstARTICULOS.Width
    Me.lstARTICULOS.ColumnHeaders.Add , "AUCO", "AutoUNC", 0
    Me.lstARTICULOS.ColumnHeaders.Add , "COGR", "CodGRU", 0
    Me.lstARTICULOS.ColumnHeaders.Add , "COUS", "CodUSO", 0
End Sub

Private Sub cboTIPO_LostFocus()
    If NumEnCombo = Me.cboTIPO.ListIndex Then Exit Sub
    Dim vTipo As String * 1
    Me.lstARTICULOS.ListItems.Clear
    Me.lstCODIGOS.ListItems.Clear
    Call BorrarTextos
    Me.pgbLLEVO.Value = 0
    Me.pgbLLEVO.Visible = True
    Select Case Me.cboTIPO.ListIndex
    Case Is = -1
        vTipo = "0"
    Case Is = 0
        vTipo = "V"
    Case Is = 1
        vTipo = "C"
    End Select
    
    Campos = "NU_AUTO_ARTI, CD_CODI_ARTI, NO_NOMB_ARTI, NU_AUTO_UNVE_ARTI, TX_PARA_ARTI, " & _
        "DE_DESC_GRUP, DE_DESC_USOS, TX_NOMB_UNVE, CD_GRUP_ARTI, CD_USOS_ARTI, " & _
        "NU_MULT_UNVE, NU_DIVI_UNVE, VL_COPR_ARTI"
    Desde = "ARTICULO, USOS, GRUP_ARTICULO, IN_UNDVENTA"
    Condi = "CD_CODI_GRUP = CD_GRUP_ARTI AND CD_CODI_USOS = CD_USOS_ARTI"
    Condi = Condi & " AND NU_AUTO_UNVE = NU_AUTO_UNVE_ARTI"
    Condi = Condi & " AND TX_PARA_ARTI='" & vTipo & "' ORDER BY NO_NOMB_ARTI"
    ReDim ArrUXB(12, 0)
    Result = LoadMulData(Desde, Campos, Condi, ArrUXB())
    If Result = FAIL Then GoTo FALLO
    If Not Encontro Then GoTo NOENC
    Me.pgbLLEVO.Min = 0
    Me.pgbLLEVO.Value = 0
    Me.pgbLLEVO.Max = UBound(ArrUXB, 2) + 1
    Dim Articulo As New UnArticulo
    For vContar = 0 To UBound(ArrUXB, 2)
        Me.pgbLLEVO.Value = vContar
        Articulo.AutoNumero = CLng(ArrUXB(0, vContar))
        Articulo.Codigo = ArrUXB(1, vContar)
        Articulo.Nombre = ArrUXB(2, vContar)
        Articulo.AutoUDConteo = CLng(ArrUXB(3, vContar))
        Articulo.NombreGrupo = ArrUXB(5, vContar)
        Articulo.NombreUso = ArrUXB(6, vContar)
        Articulo.NombreUndConteo = ArrUXB(7, vContar)
        Articulo.ParaQueEs = InStr(1, "VC", ArrUXB(4, vContar)) - 1
        Articulo.CodigoGrupo = ArrUXB(8, vContar)
        Articulo.CodigoUso = ArrUXB(9, vContar)
        If Not IsNumeric(ArrUXB(10, vContar)) Then ArrUXB(10, vContar) = "1"
        Articulo.Multiplicador = CInt(ArrUXB(10, vContar))
        If Not IsNumeric(ArrUXB(11, vContar)) Then ArrUXB(11, vContar) = "1"
        Articulo.Divisor = CInt(ArrUXB(11, vContar))
        If Not IsNumeric(ArrUXB(12, vContar)) Then ArrUXB(12, vContar) = "0"
        Articulo.CostoPromedio = CSng(ArrUXB(12, vContar))
        On Error GoTo NLADD
        vclave = "A" & Articulo.AutoNumero
        Me.lstARTICULOS.ListItems.Add , vclave, Trim(Articulo.Nombre)
        Me.lstARTICULOS.ListItems.Item(vclave).Tag = CStr(Articulo.AutoNumero)
'lstArticulos, NOAR, COAR, NOCO, COPR, NOUS, NOGR, AUCO, COGR, COUS, MUCO, DICO
        Me.lstARTICULOS.ListItems(vclave).ListSubItems.Add , "COAR", Trim(Articulo.Codigo)
        Me.lstARTICULOS.ListItems(vclave).ListSubItems.Add , "NOCO", Trim(Articulo.NombreUndConteo)
        Me.lstARTICULOS.ListItems(vclave).ListSubItems.Add , "COPR", Format(Articulo.CostoPromedio, "#,##0.00")
        Me.lstARTICULOS.ListItems(vclave).ListSubItems.Add , "NOUS", Trim(Articulo.NombreUso)
        Me.lstARTICULOS.ListItems(vclave).ListSubItems.Add , "NOGR", Trim(Articulo.NombreGrupo)
        Me.lstARTICULOS.ListItems(vclave).ListSubItems.Add , "AUCO", Trim(Articulo.AutoUDConteo)
        Me.lstARTICULOS.ListItems(vclave).ListSubItems.Add , "COGR", Trim(Articulo.CodigoGrupo)
        Me.lstARTICULOS.ListItems(vclave).ListSubItems.Add , "COUS", Trim(Articulo.CodigoUso)
NLADD:
        On Error GoTo 0
    Next
    Set Articulo = Nothing
NOENC:
FALLO:
    Me.pgbLLEVO.Visible = False
End Sub

Private Sub lstArticulos_ColumnClick(ByVal ColumnHeader As MSComctlLib.ColumnHeader)
    Me.lstARTICULOS.SortKey = ColumnHeader.Index - 1
    Me.lstARTICULOS.Sorted = True
End Sub

Private Sub lstARTICULOS_GotFocus()
    Me.tlbARTICULOS.Visible = True
    Me.tlbCDBARRAS.Visible = False
End Sub

Private Sub lstARTICULOS_ItemClick(ByVal Item As MSComctlLib.ListItem)
    Dim vCodigoBarra As New CdgBarra
    Me.lstCODIGOS.ListItems.Clear
    Call BorrarTextos
''IN_CODIGOBAR(NU_AUTO_ARTI_COBA,TX_COBA_COBA,TX_REGI_COBA,TX_CIAL_COBA,TX_LABO_COBA)
    Desde = "IN_CODIGOBAR"
    Campos = "NU_AUTO_ARTI_COBA,TX_COBA_COBA,TX_REGI_COBA,TX_CIAL_COBA,TX_LABO_COBA"
    Condi = "NU_AUTO_ARTI_COBA=" & Item.Tag
    ReDim ArrUXB(4, 0)
    Result = LoadMulData(Desde, Campos, Condi, ArrUXB())
    If Result = FAIL Then Exit Sub
    If Not Encontro Then Exit Sub
    Set Me.lstCODIGOS.SelectedItem = Nothing
    For vContar = 0 To UBound(ArrUXB, 2)
        vclave = "R" & ArrUXB(1, vContar)
        vCodigoBarra.AutoArticulo = CLng(ArrUXB(0, vContar))
        vCodigoBarra.Codigo = ArrUXB(1, vContar)
        vCodigoBarra.Registro = ArrUXB(2, vContar)
        vCodigoBarra.Comercial = ArrUXB(3, vContar)
        vCodigoBarra.Laboratorio = ArrUXB(4, vContar)
'AUT, COD, REG, CIA
        Me.lstCODIGOS.ListItems.Add , vclave, vCodigoBarra.Codigo
        Me.lstCODIGOS.ListItems.Item(vclave).Tag = vCodigoBarra.Codigo
        Me.lstCODIGOS.ListItems.Item(vclave).ListSubItems.Add , "CIA", vCodigoBarra.Comercial
        Me.lstCODIGOS.ListItems.Item(vclave).ListSubItems.Add , "REG", vCodigoBarra.Registro
        Me.lstCODIGOS.ListItems.Item(vclave).ListSubItems.Add , "LAB", vCodigoBarra.Laboratorio
        If vContar = 0 Then Set Me.lstCODIGOS.SelectedItem = Me.lstCODIGOS.ListItems.Item(vclave)
    Next
    If Not Me.lstCODIGOS.SelectedItem Is Nothing Then Call lstCODIGOS_ItemClick(Me.lstCODIGOS.SelectedItem)
End Sub

Private Sub lstARTICULOS_KeyUp(KeyCode As Integer, Shift As Integer)
    If vArticulo = Me.lstARTICULOS.SelectedItem.Tag Then Exit Sub
    vArticulo = Me.lstARTICULOS.SelectedItem.Tag
    Call lstARTICULOS_ItemClick(Me.lstARTICULOS.SelectedItem)
End Sub

Private Sub lstCODIGOS_GotFocus()
    Me.tlbARTICULOS.Visible = False
    Me.tlbCDBARRAS.Visible = True
End Sub

Private Sub lstCODIGOS_ItemClick(ByVal Item As MSComctlLib.ListItem)
    Me.txtCDBARRA.Text = Item.Tag
    Me.txtCIAL.Text = Item.ListSubItems.Item("CIA").Text
    Me.txtREGI.Text = Item.ListSubItems.Item("REG").Text
    Me.txtLABO.Text = Item.ListSubItems.Item("LAB").Text
End Sub

Private Sub lstCODIGOS_KeyPress(KeyAscii As Integer)
    If Me.lstCODIGOS.SelectedItem.Text = "" Then
        Me.lstCODIGOS.SelectedItem.Tag = NUL$
    End If
End Sub

Private Sub lstCODIGOS_KeyUp(KeyCode As Integer, Shift As Integer)

'LJSA M3963----------------
If Me.lstCODIGOS.ListItems.Count = 0 Then
    Exit Sub
End If
'LJSA M3963-------------

If vCodigo = Me.lstCODIGOS.SelectedItem.Tag Then Exit Sub
vCodigo = Me.lstCODIGOS.SelectedItem.Tag
Call lstCODIGOS_ItemClick(Me.lstCODIGOS.SelectedItem)

End Sub

Private Sub tlbARTICULOS_ButtonClick(ByVal Button As MSComctlLib.Button)
    Select Case Button.Key
    Case Is = "IMP"
        FrmIMPOCDBA.Show vbModal
    End Select
End Sub

Private Sub tlbCDBARRAS_ButtonClick(ByVal Button As MSComctlLib.Button)
    Dim vUno As MSComctlLib.Button
    Me.txtCDBARRA.Enabled = True
    Me.lstARTICULOS.Enabled = False
    Me.lstCODIGOS.Enabled = False
    Me.frmVALORES.Enabled = False
    For Each vUno In Me.tlbCDBARRAS.Buttons
        vUno.Enabled = False
    Next
    Select Case Button.Key
    Case Is = "ADD"
        vAccion = "ADD"
        Me.tlbCDBARRAS.Buttons.Item("CAN").Enabled = True
        Me.tlbCDBARRAS.Buttons.Item("SAV").Enabled = True
        Me.frmVALORES.Enabled = True
        Me.txtCDBARRA.SetFocus
    Case Is = "DEL"
        If Me.lstCODIGOS.ListItems.Count > 0 Then
            If MsgBox("Procedo a BORRAR el C�digo:" & Me.lstCODIGOS.SelectedItem.Tag, vbYesNo) = vbYes Then
                Condi = "NU_AUTO_ARTI_COBA=" & Me.lstARTICULOS.SelectedItem.Tag
                Condi = Condi & " AND TX_COBA_COBA='" & Me.lstCODIGOS.SelectedItem.Tag & "'"
                Result = DoDelete("IN_CODIGOBAR", Condi)
                vArticulo = ""
                Call lstARTICULOS_KeyUp(0, 0)
            End If
        End If
        For Each vUno In Me.tlbCDBARRAS.Buttons
            vUno.Enabled = True
        Next
        Me.tlbCDBARRAS.Buttons.Item("CAN").Enabled = False
        Me.tlbCDBARRAS.Buttons.Item("SAV").Enabled = False
        Me.lstCODIGOS.Enabled = True
        Me.lstARTICULOS.Enabled = True
    Case Is = "CHA"
        vAccion = "CHA"
        Me.tlbCDBARRAS.Buttons.Item("CAN").Enabled = True
        Me.tlbCDBARRAS.Buttons.Item("SAV").Enabled = True
        Me.frmVALORES.Enabled = True
        Me.txtCDBARRA.Enabled = False
        Me.txtCIAL.SetFocus
    Case Is = "CAN"
        For Each vUno In Me.tlbCDBARRAS.Buttons
            vUno.Enabled = True
        Next
        Me.tlbCDBARRAS.Buttons.Item("CAN").Enabled = False
        Me.tlbCDBARRAS.Buttons.Item("SAV").Enabled = False
        Me.lstCODIGOS.Enabled = True
        Me.lstARTICULOS.Enabled = True
    Case Is = "SAV"
        If MalaInformacion(vAviso) Then
            Call MsgBox("Incorrecta informaci�n en:" & vAviso, vbOKOnly)
        Else
            Select Case vAccion
            Case Is = "ADD"
''IN_CODIGOBAR(NU_AUTO_ARTI_COBA,TX_COBA_COBA,TX_REGI_COBA,TX_CIAL_COBA,TX_LABO_COBA) 'lstCODIGOS
                
'LJSA M3964-----------------------------
                ReDim Arr(1)
                Condicion = "TX_COBA_COBA = '" & Me.txtCDBARRA.Text & "'"
                Campos = "TX_COBA_COBA"
                Result = LoadData("IN_CODIGOBAR", Campos, Condicion, Arr)
                
                If Result <> FAIL Then
                    If Encontro = True Then
                        Call Mensaje1("El c�digo de barras ya se encuentra registrado.", 2)
                        For Each vUno In Me.tlbCDBARRAS.Buttons
                            vUno.Enabled = True
                        Next
                        Me.tlbCDBARRAS.Buttons.Item("CAN").Enabled = False
                        Me.tlbCDBARRAS.Buttons.Item("SAV").Enabled = False
                        Me.lstCODIGOS.Enabled = True
                        Me.lstARTICULOS.Enabled = True
                        Exit Sub
                    End If
                End If
'LJSA M3964---------------------------

                Valores = "NU_AUTO_ARTI_COBA=" & Me.lstARTICULOS.SelectedItem.Tag
                Valores = Valores & ",TX_COBA_COBA='" & Me.txtCDBARRA.Text & "'"
                Valores = Valores & ",TX_REGI_COBA='" & Me.txtREGI.Text & "'"
                Valores = Valores & ",TX_CIAL_COBA='" & Me.txtCIAL.Text & "'"
                Valores = Valores & ",TX_LABO_COBA='" & Me.txtLABO.Text & "'"
                Result = DoInsertSQL("IN_CODIGOBAR", Valores)
                vArticulo = ""
                Call lstARTICULOS_KeyUp(0, 0)
            Case Is = "CHA"
                Condi = "NU_AUTO_ARTI_COBA=" & Me.lstARTICULOS.SelectedItem.Tag
                Condi = Condi & " AND TX_COBA_COBA='" & Me.txtCDBARRA.Text & "'"
                Valores = "TX_REGI_COBA='" & Me.txtREGI.Text & "'"
                Valores = Valores & ",TX_CIAL_COBA='" & Me.txtCIAL.Text & "'"
                Valores = Valores & ",TX_LABO_COBA='" & Me.txtLABO.Text & "'"
                Result = DoUpdate("IN_CODIGOBAR", Valores, Condi)
                vArticulo = ""
                Call lstARTICULOS_KeyUp(0, 0)
            End Select
        End If
        For Each vUno In Me.tlbCDBARRAS.Buttons
            vUno.Enabled = True
        Next
        Me.tlbCDBARRAS.Buttons.Item("CAN").Enabled = False
        Me.tlbCDBARRAS.Buttons.Item("SAV").Enabled = False
        Me.lstCODIGOS.Enabled = True
        Me.lstARTICULOS.Enabled = True
    End Select
End Sub

Private Function MalaInformacion(ByRef Cual As String) As Boolean
    Cual = ""
    MalaInformacion = False
    If Len(Trim(Me.txtCDBARRA.Text)) = 0 Then Cual = Cual & IIf(Len(Cual) = 0, "", ", ") & "C�digo de Barra"
    If Len(Trim(Me.txtCIAL.Text)) = 0 Then Cual = Cual & IIf(Len(Cual) = 0, "", ", ") & "Nombre Comercial"
    If Len(Trim(Me.txtREGI.Text)) = 0 Then Cual = Cual & IIf(Len(Cual) = 0, "", ", ") & "Registro Sanit�rio"
    If Len(Trim(Me.txtLABO.Text)) = 0 Then Cual = Cual & IIf(Len(Cual) = 0, "", ", ") & "Laboratorio"
    If Len(Cual) > 0 Then MalaInformacion = True
End Function

Private Sub BorrarTextos()
    Me.txtCDBARRA.Text = ""
    Me.txtCIAL.Text = ""
    Me.txtREGI.Text = ""
    Me.txtLABO.Text = ""
End Sub
