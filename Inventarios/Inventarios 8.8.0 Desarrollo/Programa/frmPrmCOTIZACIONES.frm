VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Object = "{C932BA88-4374-101B-A56C-00AA003668DC}#1.1#0"; "MSMASK32.OCX"
Begin VB.Form frmPrmCOTIZACIONES 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Precios de Compra X Proveedor"
   ClientHeight    =   7095
   ClientLeft      =   45
   ClientTop       =   435
   ClientWidth     =   11475
   Icon            =   "frmPrmCOTIZACIONES.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   7095
   ScaleWidth      =   11475
   Begin MSComctlLib.Toolbar tlbTERCEROS 
      Height          =   630
      Left            =   2040
      TabIndex        =   12
      Top             =   5040
      Width           =   11475
      _ExtentX        =   20241
      _ExtentY        =   1111
      ButtonWidth     =   1667
      ButtonHeight    =   1005
      Appearance      =   1
      _Version        =   393216
      BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
         NumButtons      =   1
         BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "&Importar"
            Key             =   "IMP"
         EndProperty
      EndProperty
      Begin MSComctlLib.ProgressBar pgbLLEVO 
         Height          =   255
         Left            =   4080
         TabIndex        =   13
         Top             =   170
         Visible         =   0   'False
         Width           =   4455
         _ExtentX        =   7858
         _ExtentY        =   450
         _Version        =   393216
         Appearance      =   1
      End
   End
   Begin MSComctlLib.Toolbar tlbCDBARRAS 
      Height          =   630
      Left            =   2040
      TabIndex        =   11
      Top             =   4080
      Visible         =   0   'False
      Width           =   11475
      _ExtentX        =   20241
      _ExtentY        =   1111
      ButtonWidth     =   1799
      ButtonHeight    =   1005
      Appearance      =   1
      _Version        =   393216
      BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
         NumButtons      =   5
         BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "&Adicionar"
            Key             =   "ADD"
         EndProperty
         BeginProperty Button2 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "&Borrar"
            Key             =   "DEL"
         EndProperty
         BeginProperty Button3 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "&Cambiar"
            Key             =   "CHA"
         EndProperty
         BeginProperty Button4 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Enabled         =   0   'False
            Caption         =   "Ca&ncelar"
            Key             =   "CAN"
         EndProperty
         BeginProperty Button5 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Enabled         =   0   'False
            Caption         =   "&Guardar"
            Key             =   "SAV"
         EndProperty
      EndProperty
   End
   Begin VB.Frame frmVALORES 
      Caption         =   "Informaci�n"
      Height          =   2535
      Left            =   6360
      TabIndex        =   2
      Top             =   840
      Width           =   4695
      Begin MSMask.MaskEdBox txtCDBARRA 
         Height          =   300
         Left            =   1680
         TabIndex        =   5
         Top             =   360
         Width           =   1575
         _ExtentX        =   2778
         _ExtentY        =   529
         _Version        =   393216
         ClipMode        =   1
         PromptInclude   =   0   'False
         MaxLength       =   13
         Mask            =   "9999999999999"
         PromptChar      =   "_"
      End
      Begin MSMask.MaskEdBox txtMULT 
         Height          =   300
         Left            =   1680
         TabIndex        =   6
         Top             =   960
         Width           =   855
         _ExtentX        =   1508
         _ExtentY        =   529
         _Version        =   393216
         ClipMode        =   1
         PromptInclude   =   0   'False
         MaxLength       =   20
         PromptChar      =   "_"
      End
      Begin MSMask.MaskEdBox txtCOST 
         Height          =   300
         Left            =   1680
         TabIndex        =   8
         Top             =   1560
         Width           =   1575
         _ExtentX        =   2778
         _ExtentY        =   529
         _Version        =   393216
         ClipMode        =   1
         PromptInclude   =   0   'False
         MaxLength       =   20
         PromptChar      =   "_"
      End
      Begin MSMask.MaskEdBox txtDIVI 
         Height          =   300
         Left            =   3600
         TabIndex        =   7
         Top             =   960
         Width           =   855
         _ExtentX        =   1508
         _ExtentY        =   529
         _Version        =   393216
         ClipMode        =   1
         PromptInclude   =   0   'False
         MaxLength       =   20
         PromptChar      =   "_"
      End
      Begin MSMask.MaskEdBox txtIMPU 
         Height          =   300
         Left            =   1680
         TabIndex        =   9
         Top             =   2040
         Width           =   1095
         _ExtentX        =   1931
         _ExtentY        =   529
         _Version        =   393216
         ClipMode        =   1
         PromptInclude   =   0   'False
         MaxLength       =   20
         PromptChar      =   "_"
      End
      Begin VB.Label Label5 
         Alignment       =   1  'Right Justify
         Caption         =   "Divide:"
         Height          =   255
         Left            =   2520
         TabIndex        =   15
         Top             =   1005
         Width           =   975
      End
      Begin VB.Label Label4 
         Alignment       =   1  'Right Justify
         Caption         =   "Impuesto:"
         Height          =   255
         Left            =   240
         TabIndex        =   14
         Top             =   2085
         Width           =   1335
      End
      Begin VB.Label Label3 
         Alignment       =   1  'Right Justify
         Caption         =   "Costo:"
         Height          =   255
         Left            =   240
         TabIndex        =   10
         Top             =   1605
         Width           =   1335
      End
      Begin VB.Label Label2 
         Alignment       =   1  'Right Justify
         Caption         =   "Multiplica:"
         Height          =   255
         Left            =   480
         TabIndex        =   4
         Top             =   1005
         Width           =   1095
      End
      Begin VB.Label Label1 
         Alignment       =   1  'Right Justify
         Caption         =   "C�digo de Barra:"
         Height          =   255
         Left            =   240
         TabIndex        =   3
         Top             =   405
         Width           =   1335
      End
   End
   Begin MSComctlLib.ImageList imgBotones 
      Left            =   10920
      Top             =   3600
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   20
      ImageHeight     =   14
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   7
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmPrmCOTIZACIONES.frx":058A
            Key             =   ""
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmPrmCOTIZACIONES.frx":0844
            Key             =   "ASG"
         EndProperty
         BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmPrmCOTIZACIONES.frx":0B96
            Key             =   "CAN"
         EndProperty
         BeginProperty ListImage4 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmPrmCOTIZACIONES.frx":0EE8
            Key             =   "SAV"
         EndProperty
         BeginProperty ListImage5 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmPrmCOTIZACIONES.frx":1202
            Key             =   "ADD"
         EndProperty
         BeginProperty ListImage6 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmPrmCOTIZACIONES.frx":14BC
            Key             =   "DEL"
         EndProperty
         BeginProperty ListImage7 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmPrmCOTIZACIONES.frx":18FE
            Key             =   "CHA"
         EndProperty
      EndProperty
   End
   Begin MSComctlLib.ListView lstTERCEROS 
      Height          =   2535
      Left            =   480
      TabIndex        =   0
      Top             =   840
      Width           =   5460
      _ExtentX        =   9631
      _ExtentY        =   4471
      View            =   3
      LabelEdit       =   1
      LabelWrap       =   -1  'True
      HideSelection   =   0   'False
      AllowReorder    =   -1  'True
      FullRowSelect   =   -1  'True
      GridLines       =   -1  'True
      _Version        =   393217
      ForeColor       =   -2147483640
      BackColor       =   -2147483643
      BorderStyle     =   1
      Appearance      =   1
      NumItems        =   2
      BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Text            =   "Nombre del Art�culo"
         Object.Width           =   7761
      EndProperty
      BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   1
         Text            =   "C�digo"
         Object.Width           =   3881
      EndProperty
   End
   Begin MSComctlLib.ListView lstCODIGOS 
      Height          =   3255
      Left            =   480
      TabIndex        =   1
      Top             =   3600
      Width           =   10620
      _ExtentX        =   18733
      _ExtentY        =   5741
      View            =   3
      LabelEdit       =   1
      LabelWrap       =   -1  'True
      HideSelection   =   0   'False
      AllowReorder    =   -1  'True
      FullRowSelect   =   -1  'True
      GridLines       =   -1  'True
      _Version        =   393217
      ForeColor       =   -2147483640
      BackColor       =   -2147483643
      BorderStyle     =   1
      Appearance      =   1
      NumItems        =   2
      BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Text            =   "Nombre del Art�culo"
         Object.Width           =   7761
      EndProperty
      BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   1
         Text            =   "C�digo"
         Object.Width           =   3881
      EndProperty
   End
End
Attribute VB_Name = "frmPrmCOTIZACIONES"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Dim ArrUXB() As Variant
Dim Dueno As New ElTercero
'Dim LaBodega As String, LaAccion As String
'Dim NumEnCombo As Integer
Private vclave As String
Private vContar As Integer
Private vTercero As String, vCodigo As String
Private vAccion As String, vAviso As String
Public OpcCod        As String   'Opci�n de seguridad

Private Sub Form_Load()
    Call CenterForm(MDI_Inventarios, Me)
    Dueno.IniXNit (Dueno.Nit)
    Me.tlbCDBARRAS.ImageList = Me.imgBotones
    Me.tlbCDBARRAS.Buttons.Item("SAV").Image = "SAV"
    Me.tlbCDBARRAS.Buttons.Item("CHA").Image = "CHA"
    Me.tlbCDBARRAS.Buttons.Item("DEL").Image = "DEL"
    Me.tlbCDBARRAS.Buttons.Item("CAN").Image = "CAN"
    Me.txtCDBARRA.MaxLength = 13
'    Me.txtCDBARRA.Mask = String(Me.txtCDBARRA.MaxLength, "9")
    Me.txtMULT.MaxLength = 3
    Me.txtDIVI.MaxLength = 3
    Me.txtCOST.MaxLength = 12
    Me.txtIMPU.MaxLength = 5
    Me.txtMULT.Mask = ""
    Me.txtDIVI.Mask = ""
    Me.txtCOST.Mask = ""
    Me.txtIMPU.Mask = ""
    Me.txtMULT.Format = "0;(0);0;0"
    Me.txtDIVI.Format = "0;(0);0;0"
    Me.txtCOST.Format = "0.00;(0.00);0.00;0.00"
    Me.txtIMPU.Format = "0.00;(0.00);0.00;0.00"
    Me.tlbTERCEROS.Align = vbAlignTop
    Me.tlbTERCEROS.Visible = True
    Me.tlbCDBARRAS.Align = vbAlignTop
    Me.tlbCDBARRAS.Visible = False
    Me.frmVALORES.Enabled = False
    
    Me.lstTERCEROS.ListItems.Clear
'    Me.lstTERCEROS.Checkboxes = False
'    Me.lstTERCEROS.HideSelection = False
'    Me.lstTERCEROS.Width = 10500
    Me.lstCODIGOS.ColumnHeaders.Clear
    Me.lstCODIGOS.ColumnHeaders.Add , "COD", "C�digo", 0.15 * Me.lstCODIGOS.Width
    Me.lstCODIGOS.ColumnHeaders.Add , "GEN", "Nom Gen�rico", 0.3 * Me.lstCODIGOS.Width
    Me.lstCODIGOS.ColumnHeaders.Add , "CIA", "Nom C/ial", 0.15 * Me.lstCODIGOS.Width
    Me.lstCODIGOS.ColumnHeaders.Add , "MUL", "Multiplica", 0 * Me.lstCODIGOS.Width
    Me.lstCODIGOS.ColumnHeaders.Add , "DIV", "Divide", 0 * Me.lstCODIGOS.Width
    Me.lstCODIGOS.ColumnHeaders.Add , "COS", "Precio", 0.15 * Me.lstCODIGOS.Width
    Me.lstCODIGOS.ColumnHeaders.Add , "IMP", "Impuesto", 0.1 * Me.lstCODIGOS.Width
    
    Me.lstTERCEROS.ColumnHeaders.Clear
    Me.lstTERCEROS.ColumnHeaders.Add , "NIT", "Nit", 0.2 * Me.lstTERCEROS.Width
    Me.lstTERCEROS.ColumnHeaders.Add , "NOM", "Nombre", 0.5 * Me.lstTERCEROS.Width
''COD,GEN,CIA,MUL,DIV,COS,IMP
''IN_R_CODIGOBAR_TERCERO(TX_COBA_COBA_RCBTE,CD_CODI_TERC_RCBTE,NU_MULT_RCBTE,NU_DIVI_RCBTE,NU_COST_RCBTE,NU_IMPU_RCBTE)
''TERCERO(CD_CODI_TERC,NO_NOMB_TERC)
    Campos = "CD_CODI_TERC,NO_NOMB_TERC"
    Desde = "TERCERO ORDER BY NO_NOMB_TERC"
    Condi = ""
    ReDim ArrUXB(1, 0)
    Result = LoadMulData(Desde, Campos, Condi, ArrUXB())
    If Result = FAIL Then GoTo FALLO
    If Not Encontro Then GoTo NOENC
    Me.pgbLLEVO.Min = 0
    Me.pgbLLEVO.Value = 0
    Me.pgbLLEVO.Max = UBound(ArrUXB, 2) + 1
    
    For vContar = 0 To UBound(ArrUXB, 2)
        Me.pgbLLEVO.Value = vContar
        On Error GoTo NLADD
'        vclave = "T" & ArrUXB(0, vContar)
        vclave = "T" & CStr(vContar)
        Me.lstTERCEROS.ListItems.Add , vclave, ArrUXB(0, vContar)
        Me.lstTERCEROS.ListItems.Item(vclave).Tag = ArrUXB(0, vContar)
        Me.lstTERCEROS.ListItems(vclave).ListSubItems.Add , "NOM", ArrUXB(1, vContar)
NLADD:
        On Error GoTo 0
    Next
NOENC:
FALLO:
    Me.pgbLLEVO.Visible = False
End Sub

Private Sub lstTERCEROS_ColumnClick(ByVal ColumnHeader As MSComctlLib.ColumnHeader)
    Me.lstTERCEROS.SortKey = ColumnHeader.Index - 1
    Me.lstTERCEROS.Sorted = True
End Sub

Private Sub lstTERCEROS_GotFocus()
    Me.tlbTERCEROS.Visible = True
    Me.tlbCDBARRAS.Visible = False
End Sub

Private Sub lstTERCEROS_ItemClick(ByVal Item As MSComctlLib.ListItem)
'    Dim vCodigoBarra As New CdgBarra       'DEPURACION DE CODIGO
    Me.lstCODIGOS.ListItems.Clear
    Call BorrarTextos
''COD,GEN,CIA,COS,IMP
''IN_R_CODIGOBAR_TERCERO(TX_COBA_COBA_RCBTE,CD_CODI_TERC_RCBTE,NU_MULT_RCBTE,NU_DIVI_RCBTE,NU_COST_RCBTE,NU_IMPU_RCBTE)
''TERCERO(CD_CODI_TERC,NO_NOMB_TERC)
    Desde = "IN_R_CODIGOBAR_TERCERO,IN_CODIGOBAR,ARTICULO"
    Campos = "TX_COBA_COBA_RCBTE,CD_CODI_TERC_RCBTE,NU_MULT_RCBTE,NU_DIVI_RCBTE,NU_COST_RCBTE,NU_IMPU_RCBTE," & _
        "NU_AUTO_ARTI_COBA,TX_COBA_COBA,TX_REGI_COBA,TX_CIAL_COBA,NO_NOMB_ARTI,TX_LABO_COBA"
    Condi = "TX_COBA_COBA=TX_COBA_COBA_RCBTE"
    Condi = Condi & " AND NU_AUTO_ARTI=NU_AUTO_ARTI_COBA"
    Condi = Condi & " AND CD_CODI_TERC_RCBTE='" & Item.Tag & "'"
    ReDim ArrUXB(11, 0)
    Result = LoadMulData(Desde, Campos, Condi, ArrUXB())
    If Result = FAIL Then Exit Sub
    If Not Encontro Then Exit Sub
    Set Me.lstCODIGOS.SelectedItem = Nothing
    For vContar = 0 To UBound(ArrUXB, 2)
        vclave = "R" & ArrUXB(0, vContar)
''COD,GEN,CIA,MUL,DIV,COS,IMP
        Me.lstCODIGOS.ListItems.Add , vclave, ArrUXB(0, vContar)
        Me.lstCODIGOS.ListItems.Item(vclave).Tag = ArrUXB(0, vContar)
        Me.lstCODIGOS.ListItems.Item(vclave).ListSubItems.Add , "GEN", ArrUXB(10, vContar)
        Me.lstCODIGOS.ListItems.Item(vclave).ListSubItems.Add , "CIA", ArrUXB(9, vContar)
        Me.lstCODIGOS.ListItems.Item(vclave).ListSubItems.Add , "MUL", Format(ArrUXB(2, vContar), "#,##0.00")
        Me.lstCODIGOS.ListItems.Item(vclave).ListSubItems.Add , "DIV", Format(ArrUXB(3, vContar), "#,##0.00")
        Me.lstCODIGOS.ListItems.Item(vclave).ListSubItems.Add , "COS", Format(ArrUXB(4, vContar), "#,##0.00")
        Me.lstCODIGOS.ListItems.Item(vclave).ListSubItems.Add , "IMP", Format(ArrUXB(5, vContar), "#0.00")
        If vContar = 0 Then Set Me.lstCODIGOS.SelectedItem = Me.lstCODIGOS.ListItems.Item(vclave)
    Next
    If Not Me.lstCODIGOS.SelectedItem Is Nothing Then Call lstCODIGOS_ItemClick(Me.lstCODIGOS.SelectedItem)
End Sub

Private Sub lstTERCEROS_KeyUp(KeyCode As Integer, Shift As Integer)
    If vTercero = Me.lstTERCEROS.SelectedItem.Tag Then Exit Sub
    vTercero = Me.lstTERCEROS.SelectedItem.Tag
    Call lstTERCEROS_ItemClick(Me.lstTERCEROS.SelectedItem)
End Sub

Private Sub lstCODIGOS_GotFocus()
    Me.tlbTERCEROS.Visible = False
    Me.tlbCDBARRAS.Visible = True
End Sub

Private Sub lstCODIGOS_ItemClick(ByVal Item As MSComctlLib.ListItem)
    Me.txtCDBARRA.Text = Item.Tag
    Call BorrarTextos
''COD,GEN,CIA,MUL,DIV,COS,IMP
    Me.txtMULT.Text = Item.ListSubItems.Item("MUL").Text
    Me.txtDIVI.Text = Item.ListSubItems.Item("DIV").Text
    Me.txtCOST.Text = Item.ListSubItems.Item("COS").Text
    Me.txtIMPU.Text = Item.ListSubItems.Item("IMP").Text
End Sub

Private Sub lstCODIGOS_KeyUp(KeyCode As Integer, Shift As Integer)
    If vCodigo = Me.lstCODIGOS.SelectedItem.Tag Then Exit Sub
    vCodigo = Me.lstCODIGOS.SelectedItem.Tag
    Call lstCODIGOS_ItemClick(Me.lstCODIGOS.SelectedItem)
End Sub

Private Sub tlbTERCEROS_ButtonClick(ByVal Button As MSComctlLib.Button)
    Select Case Button.Key
    Case Is = "IMP"
        FrmIMPOCOTIZA.Show vbModal
    End Select
End Sub

Private Sub tlbCDBARRAS_ButtonClick(ByVal Button As MSComctlLib.Button)
    Dim vUno As MSComctlLib.Button
    Me.txtCDBARRA.Enabled = True
    Me.lstTERCEROS.Enabled = False
    Me.lstCODIGOS.Enabled = False
    Me.frmVALORES.Enabled = False
    For Each vUno In Me.tlbCDBARRAS.Buttons
        vUno.Enabled = False
    Next
    Select Case Button.Key
    Case Is = "ADD"
        vAccion = "ADD"
        Me.tlbCDBARRAS.Buttons.Item("CAN").Enabled = True
        Me.tlbCDBARRAS.Buttons.Item("SAV").Enabled = True
        Me.frmVALORES.Enabled = True
'        Me.txtMULT.Format = ""
'        Me.txtDIVI.Format = ""
'        Me.txtCOST.Format = ""
'        Me.txtIMPU.Format = ""
'        Me.txtMULT.Mask = "###"
'        Me.txtDIVI.Mask = "###"
'        Me.txtCOST.Mask = "#########.##"
'        Me.txtIMPU.Mask = "##.##"
        Me.txtCDBARRA.SetFocus
    Case Is = "DEL"
        If Me.lstCODIGOS.ListItems.Count > 0 Then
            If MsgBox("Procedo a BORRAR el C�digo:" & Me.lstCODIGOS.SelectedItem.Tag, vbYesNo) = vbYes Then
                Condi = "NU_AUTO_ARTI_COBA=" & Me.lstTERCEROS.SelectedItem.Tag
                Condi = Condi & " AND TX_COBA_COBA='" & Me.lstCODIGOS.SelectedItem.Tag & "'"
                Result = DoDelete("IN_CODIGOBAR", Condi)
                vTercero = ""
                Call lstTERCEROS_KeyUp(0, 0)
            End If
        End If
        For Each vUno In Me.tlbCDBARRAS.Buttons
            vUno.Enabled = True
        Next
        Me.tlbCDBARRAS.Buttons.Item("CAN").Enabled = False
        Me.tlbCDBARRAS.Buttons.Item("SAV").Enabled = False
        Me.lstCODIGOS.Enabled = True
        Me.lstTERCEROS.Enabled = True
    Case Is = "CHA"
        vAccion = "CHA"
        Me.tlbCDBARRAS.Buttons.Item("CAN").Enabled = True
        Me.tlbCDBARRAS.Buttons.Item("SAV").Enabled = True
        Me.frmVALORES.Enabled = True
        Me.txtCDBARRA.Enabled = False
        Me.txtMULT.MaxLength = 3
        Me.txtDIVI.MaxLength = 3
        Me.txtCOST.MaxLength = 12
        Me.txtIMPU.MaxLength = 5
        Me.txtMULT.Text = Right(String(Me.txtMULT.MaxLength, " ") & Me.txtMULT.FormattedText, Me.txtMULT.MaxLength)
        Me.txtDIVI.Text = Right(String(Me.txtDIVI.MaxLength, " ") & Me.txtDIVI.FormattedText, Me.txtDIVI.MaxLength)
        Me.txtCOST.Text = Right(String(Me.txtCOST.MaxLength, " ") & Me.txtCOST.FormattedText, Me.txtCOST.MaxLength)
        Me.txtIMPU.Text = Right(String(Me.txtIMPU.MaxLength, " ") & Me.txtIMPU.FormattedText, Me.txtIMPU.MaxLength)
        Me.txtMULT.Format = ""
        Me.txtDIVI.Format = ""
        Me.txtCOST.Format = ""
        Me.txtIMPU.Format = ""
        Me.txtMULT.Mask = "###"
        Me.txtDIVI.Mask = "###"
        Me.txtCOST.Mask = "#########\.##"
        Me.txtIMPU.Mask = "##\.##"
        Me.txtMULT.SetFocus
    Case Is = "CAN"
        For Each vUno In Me.tlbCDBARRAS.Buttons
            vUno.Enabled = True
        Next
        Me.tlbCDBARRAS.Buttons.Item("CAN").Enabled = False
        Me.tlbCDBARRAS.Buttons.Item("SAV").Enabled = False
        Me.lstCODIGOS.Enabled = True
        Me.lstTERCEROS.Enabled = True
    Case Is = "SAV"
        If MalaInformacion(vAviso) Then
            Call MsgBox("Incorrecta informaci�n en:" & vAviso, vbOKOnly)
        Else
            Select Case vAccion
            Case Is = "ADD"
''IN_R_CODIGOBAR_TERCERO(TX_COBA_COBA_RCBTE,CD_CODI_TERC_RCBTE,NU_MULT_RCBTE,NU_DIVI_RCBTE,NU_COST_RCBTE,NU_IMPU_RCBTE)
                Valores = "CD_CODI_TERC_RCBTE=" & Me.lstTERCEROS.SelectedItem.Tag
                Valores = Valores & ",TX_COBA_COBA_RCBTE='" & Me.txtCDBARRA.Text & "'"
                Valores = Valores & ",NU_MULT_RCBTE=" & Me.txtMULT.Text
                Valores = Valores & ",NU_DIVI_RCBTE=" & Me.txtDIVI.Text
                Valores = Valores & ",NU_COST_RCBTE=" & Me.txtCOST.Text
                Valores = Valores & ",NU_IMPU_RCBTE=" & Me.txtIMPU.Text
                Result = DoInsertSQL("IN_R_CODIGOBAR_TERCERO", Valores)
                vTercero = ""
                Call lstTERCEROS_KeyUp(0, 0)
            Case Is = "CHA"
                Condi = "CD_CODI_TERC_RCBTE=" & Me.lstTERCEROS.SelectedItem.Tag
                Condi = Condi & " AND TX_COBA_COBA_RCBTE='" & Me.txtCDBARRA.Text & "'"
                Valores = "NU_MULT_RCBTE=" & Me.txtMULT.Text
                Valores = Valores & ",NU_DIVI_RCBTE=" & Me.txtDIVI.Text
                Valores = Valores & ",NU_COST_RCBTE=" & Me.txtCOST.Text
                Valores = Valores & ",NU_IMPU_RCBTE=" & Me.txtIMPU.Text
                Result = DoUpdate("IN_R_CODIGOBAR_TERCERO", Valores, Condi)
                vTercero = ""
                Call lstTERCEROS_KeyUp(0, 0)
            End Select
        End If
        For Each vUno In Me.tlbCDBARRAS.Buttons
            vUno.Enabled = True
        Next
        Me.tlbCDBARRAS.Buttons.Item("CAN").Enabled = False
        Me.tlbCDBARRAS.Buttons.Item("SAV").Enabled = False
        Me.lstCODIGOS.Enabled = True
        Me.lstTERCEROS.Enabled = True
    End Select
End Sub

Private Function MalaInformacion(ByRef Cual As String) As Boolean
    Cual = ""
    MalaInformacion = False
    If Len(Trim(Me.txtCDBARRA.Text)) = 0 Then Cual = Cual & IIf(Len(Cual) = 0, "", ", ") & "C�digo de Barra"
    If Len(Trim(Me.txtMULT.Text)) = 0 Then Cual = Cual & IIf(Len(Cual) = 0, "", ", ") & "Factor Multiplica"
    If Len(Trim(Me.txtDIVI.Text)) = 0 Then Cual = Cual & IIf(Len(Cual) = 0, "", ", ") & "Factor Divide"
    If Len(Trim(Me.txtCOST.Text)) = 0 Then Cual = Cual & IIf(Len(Cual) = 0, "", ", ") & "Precio de Compra"
    If Len(Trim(Me.txtIMPU.Text)) = 0 Then Cual = Cual & IIf(Len(Cual) = 0, "", ", ") & "Impuesto"
    If Len(Cual) > 0 Then MalaInformacion = True
End Function

Private Sub BorrarTextos()
    Me.txtMULT.Mask = ""
    Me.txtDIVI.Mask = ""
    Me.txtCOST.Mask = ""
    Me.txtIMPU.Mask = ""
    Me.txtMULT.Text = ""
    Me.txtDIVI.Text = ""
    Me.txtCOST.Text = ""
    Me.txtIMPU.Text = ""
    Me.txtMULT.Format = "0;(0);0;0"
    Me.txtDIVI.Format = "0;(0);0;0"
    Me.txtCOST.Format = "0.00;(0.00);0.00;0.00"
    Me.txtIMPU.Format = "0.00;(0.00);0.00;0.00"
End Sub
