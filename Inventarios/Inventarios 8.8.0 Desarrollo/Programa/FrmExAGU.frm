VERSION 5.00
Object = "{00025600-0000-0000-C000-000000000046}#5.2#0"; "CRYSTL32.OCX"
Object = "{C932BA88-4374-101B-A56C-00AA003668DC}#1.1#0"; "MSMASK32.OCX"
Object = "{0BA686C6-F7D3-101A-993E-0000C0EF6F5E}#1.0#0"; "THREED32.OCX"
Begin VB.Form FrmExAGU 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Informe de Existencias en Almacen por Grupo y Usos"
   ClientHeight    =   3180
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   5235
   Icon            =   "FrmExAGU.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   ScaleHeight     =   3180
   ScaleWidth      =   5235
   Begin VB.TextBox TxtRango 
      Height          =   285
      Index           =   0
      Left            =   1440
      MaxLength       =   9
      TabIndex        =   3
      Text            =   "0"
      Top             =   600
      Width           =   1215
   End
   Begin VB.TextBox TxtRango 
      Height          =   285
      Index           =   1
      Left            =   1440
      MaxLength       =   9
      TabIndex        =   5
      Text            =   "ZZZZZZZZZ"
      Top             =   1080
      Width           =   1215
   End
   Begin VB.TextBox TxtRango 
      Height          =   285
      Index           =   3
      Left            =   1440
      MaxLength       =   16
      TabIndex        =   9
      Text            =   "ZZZZ"
      Top             =   2040
      Width           =   615
   End
   Begin VB.TextBox TxtRango 
      Height          =   285
      Index           =   2
      Left            =   1440
      MaxLength       =   4
      TabIndex        =   7
      Text            =   "0"
      Top             =   1560
      Width           =   615
   End
   Begin Threed.SSCommand CmdSelec 
      Height          =   375
      Index           =   1
      Left            =   2760
      TabIndex        =   13
      ToolTipText     =   "Selecci�n de Grupo"
      Top             =   960
      Width           =   615
      _Version        =   65536
      _ExtentX        =   1085
      _ExtentY        =   661
      _StockProps     =   78
      BevelWidth      =   0
      RoundedCorners  =   0   'False
      Outline         =   0   'False
      Picture         =   "FrmExAGU.frx":058A
   End
   Begin Threed.SSCommand CmdSelec 
      Height          =   375
      Index           =   0
      Left            =   2760
      TabIndex        =   14
      ToolTipText     =   "Selecci�n de Grupo"
      Top             =   480
      Width           =   615
      _Version        =   65536
      _ExtentX        =   1085
      _ExtentY        =   661
      _StockProps     =   78
      BevelWidth      =   0
      RoundedCorners  =   0   'False
      Outline         =   0   'False
      Picture         =   "FrmExAGU.frx":0F3C
   End
   Begin Threed.SSCommand SCmd_Options 
      Height          =   735
      Index           =   1
      Left            =   4080
      TabIndex        =   11
      Top             =   960
      Width           =   855
      _Version        =   65536
      _ExtentX        =   1508
      _ExtentY        =   1296
      _StockProps     =   78
      Caption         =   "&IMPRIMIR"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   6
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Picture         =   "FrmExAGU.frx":18EE
   End
   Begin Threed.SSCommand SCmd_Options 
      Height          =   735
      Index           =   2
      Left            =   4080
      TabIndex        =   12
      Top             =   1800
      Width           =   855
      _Version        =   65536
      _ExtentX        =   1508
      _ExtentY        =   1296
      _StockProps     =   78
      Caption         =   "&SALIR"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   6
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Picture         =   "FrmExAGU.frx":1FB8
   End
   Begin Threed.SSCommand SCmd_Options 
      Height          =   735
      Index           =   0
      Left            =   4080
      TabIndex        =   10
      Top             =   120
      Width           =   855
      _Version        =   65536
      _ExtentX        =   1508
      _ExtentY        =   1296
      _StockProps     =   78
      Caption         =   "&PANTALLA"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   6
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Picture         =   "FrmExAGU.frx":2682
   End
   Begin MSMask.MaskEdBox MskFecha 
      Height          =   285
      Index           =   0
      Left            =   1440
      TabIndex        =   1
      Top             =   120
      Width           =   1095
      _ExtentX        =   1931
      _ExtentY        =   503
      _Version        =   393216
      MaxLength       =   10
      Mask            =   "##/##/####"
      PromptChar      =   "_"
   End
   Begin Crystal.CrystalReport Crys_Listar 
      Left            =   4680
      Top             =   120
      _ExtentX        =   741
      _ExtentY        =   741
      _Version        =   348160
      WindowLeft      =   20
      WindowWidth     =   750
      WindowHeight    =   450
      WindowControlBox=   -1  'True
      WindowMaxButton =   -1  'True
      WindowMinButton =   -1  'True
      WindowControls  =   -1  'True
      PrintFileLinesPerPage=   60
      WindowShowCloseBtn=   -1  'True
      WindowShowSearchBtn=   -1  'True
      WindowShowPrintSetupBtn=   -1  'True
      WindowShowRefreshBtn=   -1  'True
   End
   Begin Threed.SSCommand CmdSelec 
      Height          =   375
      Index           =   2
      Left            =   2160
      TabIndex        =   15
      ToolTipText     =   "Selecci�n de Usos"
      Top             =   1440
      Width           =   615
      _Version        =   65536
      _ExtentX        =   1085
      _ExtentY        =   661
      _StockProps     =   78
      BevelWidth      =   0
      RoundedCorners  =   0   'False
      Outline         =   0   'False
      Picture         =   "FrmExAGU.frx":3464
   End
   Begin Threed.SSCommand CmdSelec 
      Height          =   375
      Index           =   3
      Left            =   2160
      TabIndex        =   16
      ToolTipText     =   "Selecci�n de Usos"
      Top             =   1920
      Width           =   615
      _Version        =   65536
      _ExtentX        =   1085
      _ExtentY        =   661
      _StockProps     =   78
      BevelWidth      =   0
      RoundedCorners  =   0   'False
      Outline         =   0   'False
      Picture         =   "FrmExAGU.frx":3E16
   End
   Begin VB.Label Label1 
      Caption         =   "Grupo Inicial"
      Height          =   375
      Left            =   360
      TabIndex        =   2
      Top             =   600
      Width           =   1095
   End
   Begin VB.Label Label2 
      Caption         =   "Grupo Final"
      Height          =   375
      Left            =   360
      TabIndex        =   4
      Top             =   1080
      Width           =   975
   End
   Begin VB.Label LblFeIni 
      Caption         =   "Fecha:"
      Height          =   255
      Left            =   360
      TabIndex        =   0
      Top             =   120
      Width           =   495
   End
   Begin VB.Label LblInicio 
      Caption         =   "Uso Inicial : "
      Height          =   435
      Left            =   360
      TabIndex        =   6
      Top             =   1560
      Width           =   1020
   End
   Begin VB.Label LblHasta 
      Caption         =   "Uso Final : "
      Height          =   195
      Left            =   360
      TabIndex        =   8
      Top             =   2040
      Width           =   945
   End
   Begin VB.Label LblReg 
      Height          =   375
      Left            =   240
      TabIndex        =   17
      Top             =   2640
      Width           =   4815
   End
End
Attribute VB_Name = "FrmExAGU"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Dim Rstdatos As ADODB.Recordset
Dim CantE As Integer
Dim SaldE As Double
Dim CantS As Integer
Dim SaldS As Double
Dim Imprime As Boolean
Dim Dependencia As String
Public OpcCod        As String   'Opci�n de seguridad

'DEPURACION DE CODIGO
'Private Sub ChkConsolida_KeyPress(KeyAscii As Integer)
'    Call Cambiar_Enter(KeyAscii)
'End Sub

Private Sub CmdSelec_Click(Index As Integer)
    Codigo = NUL$
    Select Case Index
        Case 0, 1: Codigo = Seleccion("GRUP_ARTICULO", "DE_DESC_GRUP", "CD_CODI_GRUP,DE_DESC_GRUP", " GRUPO DE ARTICULOS", NUL$)
        Case 2, 3: Codigo = Seleccion("USOS", "DE_DESC_USOS", "CD_CODI_USOS,DE_DESC_USOS", " USO DE ARTICULOS", NUL$)
    End Select
    If Codigo <> NUL$ Then TxtRango(Index) = Codigo
    TxtRango(Index).SetFocus
End Sub
Private Sub Form_Activate()
    If Not SCmd_Options(0).Enabled Then
       Unload Me
       Call Mensaje1("Acceso denegado. Consulte a su administrador", 2)
    End If
End Sub
Private Sub Form_Load()
    Dim ElClnt As New ElTercero
    ElClnt.IniXNit
    Dim Arr(0)
    Result = LoadData("OPCIONES_IN", "CD_CODI_OPCI", "CD_CODI_OPCI='03026'", Arr())
    If Arr(0) = NUL$ Then
       Result = DoInsert("OPCIONES_IN", "'03026','EXISTENCIAS EN ALMACEN POR GRUPO Y USO'")
    End If
'    Call Leer_Permisos("03026", SCmd_Options(1), SCmd_Options(1), SCmd_Options(0))
    Call CenterForm(MDI_Inventarios, Me)
    SCmd_Options(1).Enabled = SCmd_Options(0).Enabled
    Crys_Listar.Formulas(0) = "ENTIDAD='" & ElClnt.Nombre & Comi
    Crys_Listar.Formulas(1) = "NIT= 'NIT. " & ElClnt.Nit & Comi
    Crys_Listar.Formulas(2) = "HORA= 'Hora: " & Format(Now, "hh:mm") & Comi
    Crys_Listar.Formulas(3) = "USUARIO = 'Usuario:" & UserId & Comi
'    Crys_Listar.Connect = conCrys
    MskFecha(0) = Hoy
End Sub

Private Sub MskFecha_GotFocus(Index As Integer)
    MskFecha(Index).SelStart = 0
    MskFecha(Index).SelLength = Len(MskFecha(Index).Text)
End Sub
Private Sub MskFecha_KeyPress(Index As Integer, KeyAscii As Integer)
    Call ValKeyFecha(KeyAscii, MskFecha(Index))
    Call Cambiar_Enter(KeyAscii)
End Sub
Private Sub MskFecha_LostFocus(Index As Integer)
   Call ValFecha(MskFecha(Index), 0)
End Sub
Private Sub SCmd_Options_Click(Index As Integer)
    Select Case Index
        Case 0: Crys_Listar.Destination = crptToWindow
                Call Crea_Temporales
        Case 1: Crys_Listar.Destination = crptToPrinter
                Call Crea_Temporales
        Case 2: Unload Me
    End Select
End Sub
Private Sub TxtRango_GotFocus(Index As Integer)
    TxtRango(Index).SelStart = 0
    TxtRango(Index).SelLength = Len(TxtRango(Index).Text)
End Sub
Private Sub TxtRango_KeyPress(Index As Integer, KeyAscii As Integer)
'    Dim i As Integer       'DEPURACION DE CODIGO
    Call ValKeyAlfaNum(KeyAscii)
    Call Cambiar_Enter(KeyAscii)
End Sub
Private Sub TxtRango_KeyDown(Index As Integer, KeyCode As Integer, Shift As Integer)
 Select Case Index
    Case 0
           If KeyCode = 40 Then
              TxtRango(1).SetFocus
           End If
    Case 1, 2
           If KeyCode = 38 Then
              TxtRango(Index - 1).SetFocus
           End If
           If KeyCode = 40 Then
              TxtRango(Index + 1).SetFocus
           End If
    Case 3
           If KeyCode = 38 Then
              TxtRango(Index - 1).SetFocus
           End If
           If KeyCode = 40 Then
              MskFecha(0).SetFocus
           End If
 End Select
End Sub
''Crea temporal para informaci�n del k�rdex de dependencias
Private Sub Crea_Temporales()
    
  Result = DoDelete("INFOINVE", NUL$)
  Imprime = False
  Call Buscar_Saldo_Inicial
  Condicion = "CT_SAIN_INFO=0 AND CT_ENTR_INFO= 0 AND CT_SALI_INFO=0"
  Result = DoDelete("INFOINVE", Condicion)
  If Imprime And Result <> FAIL Then Call Imprimir
End Sub
Private Sub Buscar_Saldo_Inicial()
Dim Cantidad As Double
Dim Costo As Double
Dim CostoK As Double
Dim CantiK As Double
ReDim mat(1, 0)
Dim Arr(7)
Dim Fuc As String ' FECHA ULTIMO CIERRE

    Fuc = "01/" & Format(MskFecha(0), "mm") & "/" & Format(MskFecha(0), "yyyy")
    Fuc = DateAdd("d", -1, Fuc)
    
    Condicion = "CD_GRUP_ARTI >= '" & TxtRango(0) & Comi
    Condicion = Condicion & "And CD_GRUP_ARTI <= '" & TxtRango(1) & Comi
    Condicion = Condicion & "And CD_USOS_ARTI >= '" & TxtRango(2) & Comi
    Condicion = Condicion & "And CD_USOS_ARTI <= '" & TxtRango(3) & Comi
    Condicion = Condicion & " and ID_ESTA_KARD <> " & "2"
    Condicion = Condicion & " and FE_FECH_KARD <= " & fecha(MskFecha(0))
    Condicion = Condicion & " and CD_ARTI_KARD = CD_CODI_ARTI"
    Condicion = Condicion & " ORDER BY CD_ARTI_KARD"
    
    Set Rstdatos = New ADODB.Recordset
    Msglin "Leyendo Saldos Iniciales"
    Dependencia = "ZZZZZZZZZZZ"
          
    Call SelectRST("KARDEX_ARTI, ARTICULO", "DISTINCT CD_ARTI_KARD", Condicion, Rstdatos)
    If Not Rstdatos.EOF Then
       Rstdatos.MoveFirst
       Imprime = True
       LblReg = NUL$
       Do While Not Rstdatos.EOF
          LblReg = "Leyendo: " & Rstdatos.Fields(0)
          DoEvents
          Cantidad = Saldo_Inicial(Rstdatos.Fields(0), Fuc, Costo)
          CantiK = Saldo_Kardex(Rstdatos.Fields(0), MskFecha(0), CostoK)
          
          Cantidad = Cantidad + CantiK
          Costo = Costo + CostoK
          
          Condicion = " CD_DEPE_INFO =" & Comi & Dependencia & Comi
          Condicion = Condicion & " AND CD_ARTI_INFO = " & Comi & Rstdatos.Fields(0) & Comi
          
          If Cantidad = 0 Then Costo = 0
          
          Result = LoadData("INFOINVE", Asterisco, Condicion, Arr())
            If Arr(0) = NUL$ Then
               Valores = "CD_DEPE_INFO=" & Comi & Dependencia & Comi & Coma
               Valores = Valores & " CD_ARTI_INFO=" & Comi & Rstdatos.Fields(0) & Comi & Coma
               Valores = Valores & " CT_SAIN_INFO=" & Cantidad & Coma
               Valores = Valores & " VL_SAIN_INFO = " & Costo & Coma
               Valores = Valores & " CT_ENTR_INFO=0, VL_ENTR_INFO=0,"
               Valores = Valores & " CT_SALI_INFO=0, VL_SALI_INFO=0"
               Result = DoInsertSQL("INFOINVE", Valores)
            Else
               Valores = " CT_SAIN_INFO = CT_SAIN_INFO + " & Cantidad & Coma
               Valores = Valores & " VL_SAIN_INFO = VL_SAIN_INFO + " & Costo
               
               Result = DoUpdate("INFOINVE", Valores, Condicion)
            End If
            Rstdatos.MoveNext
       Loop
    End If
    Rstdatos.Close
    Msglin "": LblReg = NUL$
    
    If Result <> FAIL Then Call Recorrer_Kardex
End Sub
Private Sub Recorrer_Kardex()
ReDim Arr(7)

  Condicion = "CD_GRUP_ARTI >= '" & TxtRango(0) & Comi
  Condicion = Condicion & " And CD_GRUP_ARTI <= '" & TxtRango(1) & Comi
  Condicion = Condicion & " And CD_USOS_ARTI >= '" & TxtRango(2) & Comi
  Condicion = Condicion & " And CD_USOS_ARTI <= '" & TxtRango(3) & Comi
  Condicion = Condicion & " And ID_ESTA_KARD <> " & "2"
  Condicion = Condicion & " And FE_FECH_KARD = " & fecha(MskFecha(0))
  Condicion = Condicion & " and CD_ORDO_KARD <> " & "2"
  Condicion = Condicion & " and CD_ORDO_KARD <> " & "4"
  Condicion = Condicion & " and CD_ORDO_KARD <> " & "5"
  Condicion = Condicion & " and CD_ARTI_KARD = CD_CODI_ARTI"
  Condicion = Condicion & " GROUP BY CD_ARTI_KARD, CD_ORDO_KARD"
  
  Set Rstdatos = New ADODB.Recordset
  ReDim mat(4, 0)
  Msglin " Leyendo Kardex"
  Call SelectRST("KARDEX_ARTI, ARTICULO", "CD_ARTI_KARD, CD_ARTI_KARD, CD_ORDO_KARD, SUM (CT_CANT_KARD), SUM (VL_VALO_KARD)", Condicion, Rstdatos)
  If (Result <> False) Then
    If Not Rstdatos.EOF Then
       Rstdatos.MoveFirst
       LblReg = NUL$
       Imprime = True
       Do While Not Rstdatos.EOF
             LblReg = "Leyendo Kardex: " & Rstdatos.Fields(0)
             DoEvents
             
             Condicion = " CD_DEPE_INFO =" & Comi & Dependencia & Comi
             Condicion = Condicion & " AND CD_ARTI_INFO = " & Comi & Rstdatos.Fields(1) & Comi
             Result = LoadData("INFOINVE", Asterisco, Condicion, Arr())
             If (Result = False) Then Exit Do
             CantE = 0: CantS = 0
             SaldE = 0: SaldS = 0
             
             If CDbl(Rstdatos.Fields(2)) = 1 Or CDbl(Rstdatos.Fields(2)) = 3 Then
               CantE = CDbl(Rstdatos.Fields(3))
               SaldE = CDbl(Rstdatos.Fields(4))
             End If
             If CDbl(Rstdatos.Fields(2)) = 6 Or CDbl(Rstdatos.Fields(2)) = 7 Then
               CantS = CDbl(Rstdatos.Fields(3))
               SaldS = CDbl(Rstdatos.Fields(4))
             End If
             If Arr(0) = NUL$ Then
                Valores = "CD_DEPE_INFO=" & Comi & Dependencia & Comi & Coma
                Valores = Valores & " CD_ARTI_INFO=" & Comi & Rstdatos.Fields(1) & Comi & Coma
                Valores = Valores & " CT_SAIN_INFO=0, VL_SAIN_INFO = 0,"
                Valores = Valores & " CT_ENTR_INFO=" & CantE & Coma
                Valores = Valores & " VL_ENTR_INFO=" & SaldE & Coma
                Valores = Valores & " CT_SALI_INFO=" & CantS & Coma
                Valores = Valores & " VL_SALI_INFO=" & SaldS
                Result = DoInsertSQL("INFOINVE", Valores)
             Else
                Valores = " CT_ENTR_INFO = CT_ENTR_INFO + " & CantE & Coma
                Valores = Valores & " VL_ENTR_INFO = VL_ENTR_INFO + " & SaldE & Coma
                Valores = Valores & " CT_SALI_INFO = CT_SALI_INFO + " & CantS & Coma
                Valores = Valores & " VL_SALI_INFO = VL_SALI_INFO + " & SaldS
                
                Result = DoUpdate("INFOINVE", Valores, Condicion)
             End If
             If Result = FAIL Then Exit Do
             Rstdatos.MoveNext
             DoEvents
       Loop
    End If
    Rstdatos.Close
  End If
  Msglin "": LblReg = NUL$
End Sub

Private Sub Imprimir()
  On Error GoTo control
  If TxtRango(0) > TxtRango(1) Or TxtRango(1) = "" Then
     Call Mensaje1("Revise rango de grupo de art�culos", 3)
     Exit Sub
  End If
  If TxtRango(2) > TxtRango(3) Or TxtRango(3) = "" Then
     Call Mensaje1("Revise rango de usos art�culos", 3)
     Exit Sub
  End If
  With Crys_Listar
        Debug.Print .Connect
        .ReportFileName = App.Path & "\lexalgru.rpt"
        .Formulas(4) = "FECHA= '" & Format(MskFecha(0), "MMMM DD ") & " de " & Format(MskFecha(0), "YYYY") & Comi
        .Action = 1
    End With
Exit Sub
control:
Call Mensaje1(ERR.Number & ": " & ERR.Description & "  Nombre del Reporte : lexalgru.rpt", 1)
End Sub

