VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Object = "{C932BA88-4374-101B-A56C-00AA003668DC}#1.1#0"; "MSMASK32.OCX"
Begin VB.Form FrmINFLA 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "A/dmon Inventario FISICO"
   ClientHeight    =   7095
   ClientLeft      =   45
   ClientTop       =   435
   ClientWidth     =   11475
   Icon            =   "FrmINFLA.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   7095
   ScaleWidth      =   11475
   Begin MSComctlLib.ProgressBar pgbLLEVO 
      Height          =   320
      Left            =   2760
      TabIndex        =   5
      Top             =   750
      Visible         =   0   'False
      Width           =   5895
      _ExtentX        =   10398
      _ExtentY        =   556
      _Version        =   393216
      Appearance      =   1
   End
   Begin VB.Frame frmCONSUMO 
      Height          =   855
      Left            =   480
      TabIndex        =   1
      Top             =   5880
      Visible         =   0   'False
      Width           =   10575
      Begin MSMask.MaskEdBox txtPORCE 
         Height          =   300
         Left            =   5040
         TabIndex        =   4
         Top             =   360
         Width           =   975
         _ExtentX        =   1720
         _ExtentY        =   529
         _Version        =   393216
         AllowPrompt     =   -1  'True
         MaxLength       =   5
         PromptChar      =   "0"
      End
      Begin VB.CommandButton cmdCALCULAR 
         Caption         =   "&CALCULAR"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   6
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   495
         Left            =   8880
         Picture         =   "FrmINFLA.frx":058A
         Style           =   1  'Graphical
         TabIndex        =   6
         Top             =   240
         Width           =   975
      End
      Begin VB.Label Label4 
         Alignment       =   1  'Right Justify
         Caption         =   "% de Ajuste X Inflaci�n (PAAG):"
         Height          =   375
         Left            =   720
         TabIndex        =   2
         Top             =   360
         Width           =   4155
      End
   End
   Begin MSComctlLib.ImageList imgBotones 
      Left            =   10800
      Top             =   4200
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   20
      ImageHeight     =   14
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   9
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FrmINFLA.frx":08FC
            Key             =   ""
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FrmINFLA.frx":0FC6
            Key             =   "ASG"
         EndProperty
         BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FrmINFLA.frx":2018
            Key             =   "CAN"
         EndProperty
         BeginProperty ListImage4 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FrmINFLA.frx":25AA
            Key             =   "SAV"
         EndProperty
         BeginProperty ListImage5 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FrmINFLA.frx":28FC
            Key             =   "ADD"
         EndProperty
         BeginProperty ListImage6 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FrmINFLA.frx":354E
            Key             =   "DEL"
         EndProperty
         BeginProperty ListImage7 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FrmINFLA.frx":3C18
            Key             =   "CHA"
         EndProperty
         BeginProperty ListImage8 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FrmINFLA.frx":429A
            Key             =   "PRN"
            Object.Tag             =   "PRN"
         EndProperty
         BeginProperty ListImage9 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FrmINFLA.frx":4EEE
            Key             =   "GNR"
            Object.Tag             =   "GNR"
         EndProperty
      EndProperty
   End
   Begin MSComctlLib.ListView lstArticulos 
      Height          =   5580
      Left            =   480
      TabIndex        =   3
      Top             =   1200
      Width           =   10425
      _ExtentX        =   18389
      _ExtentY        =   9843
      View            =   3
      LabelEdit       =   1
      MultiSelect     =   -1  'True
      LabelWrap       =   -1  'True
      HideSelection   =   0   'False
      AllowReorder    =   -1  'True
      FullRowSelect   =   -1  'True
      GridLines       =   -1  'True
      _Version        =   393217
      ForeColor       =   -2147483640
      BackColor       =   -2147483643
      BorderStyle     =   1
      Appearance      =   1
      NumItems        =   2
      BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Text            =   "Nombre del Art�culo"
         Object.Width           =   7761
      EndProperty
      BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   1
         Text            =   "C�digo"
         Object.Width           =   3881
      EndProperty
   End
   Begin MSComctlLib.Toolbar tlbACCIONES 
      Align           =   1  'Align Top
      Height          =   630
      Left            =   0
      TabIndex        =   0
      Top             =   0
      Width           =   11475
      _ExtentX        =   20241
      _ExtentY        =   1111
      ButtonWidth     =   1535
      ButtonHeight    =   953
      AllowCustomize  =   0   'False
      Appearance      =   1
      _Version        =   393216
      BorderStyle     =   1
   End
End
Attribute VB_Name = "FrmINFLA"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Dim ArrUXB() As Variant
'Dim Articulo As UnArticulo     'DEPURACION DE CODIGO
'Dim CualBodega As New LaBodega     'DEPURACION DE CODIGO
'Dim LaBodega As Long, LaAccion As String       'DEPURACION DE CODIGO
'Private vSeCargaron As Boolean     'DEPURACION DE CODIGO
Public OpcCod As String   'Opci�n de seguridad
'NU_AUTO_ARTI, CD_CODI_ARTI, NO_NOMB_ARTI, NU_AUTO_UNVE_ARTI, TX_VENCE_ARTI, TX_PARA_ARTI,
'TX_ENTRA_ARTI, CD_GRUP_ARTI, CD_USOS_ARTI, VL_ULCO_ARTI, VL_COPR_ARTI, CT_EXIS_ARTI, DE_OBSE_ARTI, ID_TIPO_ARTIID_TIPO_ARTI, CD_RIPS_ARTI, NU_INDPYP_ARTI

Private Sub cmdCALCULAR_Click()
    Dim Uno As MSComctlLib.ListItem
'    Dim CnTdr As Integer, CostoPromedio As Single, PorInfla As Single
    Dim CostoPromedio As Single, PorInfla As Single     'DEPURACION DE CODIGO
    Me.frmCONSUMO.Visible = False
'    Me.tlbACCIONES.Buttons("GENERAR").Enabled = True
    Me.pgbLLEVO.Min = 0
    Me.pgbLLEVO.Max = Me.lstArticulos.ListItems.Count + 1
    Me.pgbLLEVO.Value = 0
    Me.pgbLLEVO.Visible = True
    PorInfla = CSng(Me.txtPORCE.Text)
    If Not PorInfla > 0 Then Exit Sub
'NOAR,COAR,UNDA,CSTO,INFL,GRUP,USOS
    For Each Uno In Me.lstArticulos.ListItems
        Me.pgbLLEVO.Value = Me.pgbLLEVO.Value + 1
        CostoPromedio = CSng(Uno.ListSubItems("CSTO").Text)
        If CostoPromedio > 0 Then
            CostoPromedio = CostoPromedio * (100 + PorInfla) / 100
            Uno.ListSubItems("INFL").Text = Format(CostoPromedio * (100 + PorInfla) / 100, "#########.#0")
        Else
            Uno.ListSubItems("INFL").Text = Format(0, "#########.#0")
        End If
    Next
    Me.pgbLLEVO.Visible = False
    Me.lstArticulos.Height = 5700
    Me.tlbACCIONES.Buttons("GUARDAR").Enabled = True
End Sub

Private Sub Form_Load()
   '    Dim CnTdr As Integer, Punto As MSComctlLib.Node
   Dim CnTdr As Integer    'DEPURACION DE CODIGO
   Dim AutoNumero As Long
   Call CenterForm(MDI_Inventarios, Me)
   Me.tlbACCIONES.ImageList = Me.imgBotones
   Me.txtPORCE.Mask = "99.99"
   Me.tlbACCIONES.Buttons.Clear
   'LIMPIAR,GENERAR,GUARDAR
    
   Me.tlbACCIONES.Buttons.Add , "LIMPIAR", "&Limpiar"
   Me.tlbACCIONES.Buttons("LIMPIAR").Image = "DEL"
   Me.tlbACCIONES.Buttons("LIMPIAR").Style = tbrDefault
   
   Me.tlbACCIONES.Buttons.Add , "GENERAR", "&Generar"
   Me.tlbACCIONES.Buttons("GENERAR").Image = "GNR"
   Me.tlbACCIONES.Buttons("GENERAR").Style = tbrDefault
   
   Me.tlbACCIONES.Buttons.Add , "GUARDAR", "G&uardar"
   Me.tlbACCIONES.Buttons("GUARDAR").Image = "SAV"
   Me.tlbACCIONES.Buttons("GUARDAR").Style = tbrDefault
   Me.tlbACCIONES.Buttons("GUARDAR").Enabled = False
   
   Call PrepararLista
   
   Campos = "NU_AUTO_ARTI, CD_CODI_ARTI, NO_NOMB_ARTI, NU_AUTO_UNVE_ARTI, TX_PARA_ARTI, " & _
   "DE_DESC_GRUP, DE_DESC_USOS, TX_NOMB_UNVE, CD_GRUP_ARTI, CD_USOS_ARTI, " & _
   "NU_MULT_UNVE, NU_DIVI_UNVE, VL_COPR_ARTI, VL_COPR_ARTI"
   Desde = "ARTICULO, USOS, GRUP_ARTICULO, IN_UNDVENTA"
   Condi = "CD_CODI_GRUP = CD_GRUP_ARTI AND CD_CODI_USOS = CD_USOS_ARTI"
   Condi = Condi & " AND NU_AUTO_UNVE = NU_AUTO_UNVE_ARTI"
   Condi = Condi & " ORDER BY NO_NOMB_ARTI"
   ReDim ArrUXB(13, 0)
   'Result = LoadMulData(Desde, Campos, Condi, ArrUXB()) 'JAUM T32017 Se deja linea en comentario
   Result = LoadMulData(Desde, "TOP (32767) " & Campos, Condi, ArrUXB()) 'JAUM T32017
   If Result = FAIL Then Exit Sub
   If Not Encontro Then Exit Sub
   Me.pgbLLEVO.Min = 0
   Me.pgbLLEVO.Value = 0
   Me.pgbLLEVO.Max = UBound(ArrUXB, 2) + 1

   For CnTdr = 0 To UBound(ArrUXB, 2)
      Me.pgbLLEVO.Value = CnTdr
      AutoNumero = CLng(ArrUXB(0, CnTdr))
      Me.lstArticulos.ListItems.Add , "A" & AutoNumero, Trim(CStr(ArrUXB(2, CnTdr)))
      Me.lstArticulos.ListItems("A" & AutoNumero).Tag = CStr(AutoNumero)
      'NOAR,COAR,UNDA,CSTO,INFL,GRUP,USOS
      If Len(ArrUXB(13, CnTdr)) = 0 Then ArrUXB(13, CnTdr) = "0"
      Me.lstArticulos.ListItems("A" & AutoNumero).ListSubItems.Add , "COAR", Trim(ArrUXB(1, CnTdr))
      Me.lstArticulos.ListItems("A" & AutoNumero).ListSubItems.Add , "UNDA", Trim(ArrUXB(7, CnTdr))
      Me.lstArticulos.ListItems("A" & AutoNumero).ListSubItems.Add , "CSTO", Trim(Format(CSng(ArrUXB(13, CnTdr)), "#########.#0"))
      Me.lstArticulos.ListItems("A" & AutoNumero).ListSubItems.Add , "INFL", "0.00"
      Me.lstArticulos.ListItems("A" & AutoNumero).ListSubItems.Add , "GRUP", Trim(ArrUXB(5, CnTdr))
      Me.lstArticulos.ListItems("A" & AutoNumero).ListSubItems.Add , "USOS", Trim(ArrUXB(6, CnTdr))
      Me.lstArticulos.ListItems("A" & AutoNumero).ListSubItems.Add , "SLDO", "0"
      Me.lstArticulos.ListItems("A" & AutoNumero).ListSubItems.Add , "CDGR", Trim(ArrUXB(8, CnTdr))
      Me.lstArticulos.ListItems("A" & AutoNumero).ListSubItems.Add , "CDUS", Trim(ArrUXB(9, CnTdr))
   Next

End Sub

Private Sub lstArticulos_ColumnClick(ByVal ColumnHeader As MSComctlLib.ColumnHeader)
    Me.lstArticulos.SortKey = ColumnHeader.Index - 1
    Me.lstArticulos.Sorted = True
End Sub

Private Sub tlbACCIONES_ButtonClick(ByVal Boton As MSComctlLib.Button)
    Dim Uno As MSComctlLib.ListItem
    Dim ValorAjuste As Double, PorInfla As Single
    Dim vArticulo As New ElArticulo
    Dim CllGrupos As New Collection
    Dim vGrupo As ElGrupo, GrupoComodin As ElGrupo
    Dim CuanEncontro As Integer, Comprobante As String
    Select Case Boton.Key
'LIMPIAR,GENERAR,GUARDAR
    Case Is = "LIMPIAR"
        Me.tlbACCIONES.Buttons("GUARDAR").Enabled = False
        Me.frmCONSUMO.Visible = False
        For Each Uno In Me.lstArticulos.ListItems
            Uno.ListSubItems("INFL").Text = Format(0, "#########.#0")
        Next
        Me.lstArticulos.Height = 5700
    Case Is = "GENERAR"
        If Me.frmCONSUMO.Visible Then
            Me.lstArticulos.Height = 5700
            Me.frmCONSUMO.Visible = False
        Else
            Me.lstArticulos.Height = 4700
            Me.frmCONSUMO.Visible = True
        End If
    Case Is = "GUARDAR"
        
'R_GRUP_DEPE (CD_GRUP_RGD, DE_DESC_GRUP, CD_AJDB_RGD, CD_AJCR_RGD)
        Campos = "CD_CODI_GRUP, DE_DESC_GRUP, CD_AJDB_GRUP, CD_AJCR_GRUP"
        Desde = "GRUP_ARTICULO"
        Condi = ""
'        Campos = "DISTINCT CD_GRUP_RGD, DE_DESC_GRUP, CD_AJDB_RGD, CD_AJCR_RGD"
'        Desde = "R_GRUP_DEPE, GRUP_ARTICULO"
'        Condi = "CD_GRUP_RGD=CD_CODI_GRUP"
        ReDim ArrUXB(3, 0)
        Result = LoadMulData(Desde, Campos, Condi, ArrUXB())
        If Result = FAIL Then Exit Sub
        If Not Encontro Then Exit Sub
        For CuanEncontro = 0 To UBound(ArrUXB, 2)
            Set vGrupo = New ElGrupo
            vGrupo.Codigo = ArrUXB(0, CuanEncontro)
            vGrupo.Nombre = ArrUXB(1, CuanEncontro)
            vGrupo.CtaDebito = ArrUXB(2, CuanEncontro)
            vGrupo.CtaCredito = ArrUXB(3, CuanEncontro)
            On Error Resume Next
            CllGrupos.Add vGrupo, "G" & ArrUXB(0, CuanEncontro)
            On Error GoTo 0
            Set vGrupo = Nothing
            Set vGrupo = CllGrupos.Item("G" & ArrUXB(0, CuanEncontro))
            If Len(vGrupo.CtaDebito) = 0 Then vGrupo.CtaDebito = ArrUXB(2, CuanEncontro)
            If Len(vGrupo.CtaCredito) = 0 Then vGrupo.CtaCredito = ArrUXB(3, CuanEncontro)
            Set vGrupo = Nothing
        Next
        Set vGrupo = New ElGrupo
        vGrupo.Codigo = "SINCUENTAS"
        vGrupo.Nombre = "SIN CUENTAS"
        CllGrupos.Add vGrupo, "Z00"
        Me.pgbLLEVO.Visible = True
        Me.pgbLLEVO.Min = 0
        Me.pgbLLEVO.Value = 0
        Me.pgbLLEVO.Max = Me.lstArticulos.ListItems.Count + 1
        PorInfla = CSng(Me.txtPORCE.Text)
        If Not PorInfla > 0 Then Exit Sub
        Set GrupoComodin = CllGrupos.Item("Z00")
        For Each Uno In Me.lstArticulos.ListItems
            Set vGrupo = Nothing
            Me.pgbLLEVO.Value = Me.pgbLLEVO.Value + 1
            vArticulo.IniciaXNumero (CLng(Uno.Tag))
            Set vGrupo = CllGrupos.Item("G" & Uno.ListSubItems("CDGR"))
            vArticulo.INTCllSaldosXBodega
            ValorAjuste = PorInfla * vArticulo.CostoPromedio * vArticulo.EnExistencia / 100
            Uno.ListSubItems("SLDO").Text = Format(vArticulo.EnExistencia, "#########.#0")
            If vGrupo Is Nothing Then
                GrupoComodin.AcumulaDebito = GrupoComodin.AcumulaDebito + ValorAjuste
                GrupoComodin.AcumulaCredito = GrupoComodin.AcumulaCredito + ValorAjuste
            Else
                If Len(vGrupo.CtaDebito) = 0 Then
                    GrupoComodin.AcumulaDebito = GrupoComodin.AcumulaDebito + ValorAjuste
                Else
                    vGrupo.AcumulaDebito = vGrupo.AcumulaDebito + ValorAjuste
                End If
                If Len(vGrupo.CtaCredito) = 0 Then
                    GrupoComodin.AcumulaCredito = GrupoComodin.AcumulaCredito + ValorAjuste
                Else
                    vGrupo.AcumulaCredito = vGrupo.AcumulaCredito + ValorAjuste
                End If
            End If
        Next
        Me.pgbLLEVO.Visible = False
        'ReDim CtasConta(3, 0)
        ReDim CtasConta(5, 0) 'LDCR T10396
        For Each vGrupo In CllGrupos
            If vGrupo.AcumulaDebito > 0 Then
                CuanEncontro = UBound(CtasConta(), 2)
                'ReDim Preserve CtasConta(3, CuanEncontro + 1)
                ReDim Preserve CtasConta(5, CuanEncontro + 1) 'LDCR PNC T10191
                CtasConta(0, CuanEncontro) = vGrupo.CtaDebito
                CtasConta(1, CuanEncontro) = Format(vGrupo.AcumulaDebito, "#########.#0")
                CtasConta(2, CuanEncontro) = 0
                CtasConta(3, CuanEncontro) = "D"
            End If
            If vGrupo.AcumulaCredito > 0 Then
                CuanEncontro = UBound(CtasConta(), 2)
                'ReDim Preserve CtasConta(3, CuanEncontro + 1)
                ReDim Preserve CtasConta(5, CuanEncontro + 1) 'LDCR PNC T10191
                CtasConta(0, CuanEncontro) = vGrupo.CtaCredito
                CtasConta(1, CuanEncontro) = Format(vGrupo.AcumulaCredito, "#########.#0")
                CtasConta(2, CuanEncontro) = 0
                CtasConta(3, CuanEncontro) = "C"
            End If
            Debug.Print vGrupo.Codigo, vGrupo.Nombre, vGrupo.AcumulaDebito, vGrupo.AcumulaCredito
        Next
        If Aplicacion.Interfaz_Contabilidad Then
            primera_entrada = True
'            Call Cuentas_Contables_Movi(0, CtasConta(), NUL$, NUL$)
            Call Cuentas_Contables_Movi(CtasConta(), NUL$, NUL$)    'DEPURACION DE CODIGO
            FrmCuentas.DesdeOtroFormulario = True
            continuar = False
            muestra_cuenta "S", Me
            FrmCuentas.DesdeOtroFormulario = False
        Else
            continuar = (MsgBox("Esta a punto de cambiar el costo de todos los art�culos, CANCELO LA ORDEN", vbYesNo) = vbNo)
        End If
        If Aplicacion.Interfaz_Contabilidad Then
            Comprobante = Buscar_Comprobante_Concepto(AjusteXInflacion)
            'SKRV T24951 INICIO
            'Call Guardar_Movimiento_Contable("INFL", FechaServer, Comprobante, Buscar_Numero_Comprobante(Comprobante), "AJUSTE POR INFLACION INVENTARIOS", NUL$) SE ESTABLECE EN COMENTARIO
            Call Guardar_Movimiento_Contable("INFL", FechaServer, Comprobante, Buscar_Numero_Comprobante(Comprobante), "AJUSTE POR INFLACION INVENTARIOS", NUL$, , , fnDevDato("MODU_MOV_NIIF", "NU_CODMO_MODU", "TX_DESCM_MODU = 'Ajustes por Inflaci�n'", True), Buscar_Numero_Comprobante(Comprobante))
            'SKRV T24951 FIN
            Me.pgbLLEVO.Visible = True
            Me.pgbLLEVO.Min = 0
            Me.pgbLLEVO.Value = 0
            Me.pgbLLEVO.Max = Me.lstArticulos.ListItems.Count + 1
            For Each Uno In Me.lstArticulos.ListItems
                Me.pgbLLEVO.Value = Me.pgbLLEVO.Value + 1
                Campos = "VL_COPR_ARTI=" & Uno.ListSubItems("INFL").Text
                Condi = "NU_AUTO_ARTI=" & CLng(Uno.Tag)
                Result = DoUpdate("ARTICULO", Campos, Condi)
            Next
            Me.pgbLLEVO.Visible = False
            Call MsgBox("El ajuste ha finalizado", vbInformation)
            Unload Me
        End If
    End Select
End Sub

Private Sub Form_KeyPress(KeyAscii As Integer)
    If KeyAscii = vbKeyReturn Then KeyAscii = vbKeyTab
End Sub

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyReturn Then KeyCode = vbKeyTab
End Sub

Private Sub PrepararLista()
    Me.lstArticulos.ListItems.Clear
    Me.lstArticulos.Width = 10500
    Me.lstArticulos.ColumnHeaders.Clear
'NOAR,COAR,UNDA,CSTO,INFL,GRUP,USOS
    Me.lstArticulos.ColumnHeaders.Add , "NOAR", "Nombre del art�culo", 0.4 * Me.lstArticulos.Width
    Me.lstArticulos.ColumnHeaders.Add , "COAR", "C�digo", 0.15 * Me.lstArticulos.Width
    Me.lstArticulos.ColumnHeaders.Add , "UNDA", "Unidad", 0.15 * Me.lstArticulos.Width
    Me.lstArticulos.ColumnHeaders.Add , "CSTO", "Costo Pro", 0.1 * Me.lstArticulos.Width
    Me.lstArticulos.ColumnHeaders("CSTO").Alignment = lvwColumnRight
    Me.lstArticulos.ColumnHeaders.Add , "INFL", "Costo Aju", 0.1 * Me.lstArticulos.Width
    Me.lstArticulos.ColumnHeaders("INFL").Alignment = lvwColumnRight
    Me.lstArticulos.ColumnHeaders.Add , "GRUP", "Grupo", 0.15 * Me.lstArticulos.Width
    Me.lstArticulos.ColumnHeaders.Add , "USOS", "Usos", 0.15 * Me.lstArticulos.Width
    Me.lstArticulos.ColumnHeaders.Add , "SLDO", "Saldo", 0
    Me.lstArticulos.ColumnHeaders("SLDO").Alignment = lvwColumnRight
    Me.lstArticulos.ColumnHeaders.Add , "CDGR", "Grupo", 0
    Me.lstArticulos.ColumnHeaders.Add , "CDUS", "Usos", 0
End Sub

