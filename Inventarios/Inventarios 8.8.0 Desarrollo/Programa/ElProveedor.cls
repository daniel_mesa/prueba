VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "ElProveedor"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Private vCodigo As String
Private vTercero As String
'Private vUnidad As Long
Private vMultiplica As Integer 'multiplicador
Private vDivide As Integer 'divisor
Private vCosto As Single
Private vIVACompra As Single
'vCodigo,vTercero,vMultiplica,vDivide,vCosto,vIVACompra
'IN_R_CODIGOBAR_TERCERO(TX_COBA_COBA_RCBTE,CD_CODI_TERC_RCBTE,NU_MULT_RCBTE,NU_DIVI_RCBTE,NU_COST_RCBTE,NU_IMPU_RCBTE)

Public Property Let CodigoDeBarra(Cue As String)
    vCodigo = Cue
End Property

Public Property Get CodigoDeBarra() As String
    CodigoDeBarra = vCodigo
End Property

Public Property Let CodigoTercero(Cue As String)
    vTercero = Cue
End Property

Public Property Get CodigoTercero() As String
    CodigoTercero = vTercero
End Property

Public Property Let Multiplicador(num As Integer)
    If Not num > 0 Then Exit Property
    vMultiplica = num
End Property

Public Property Get Multiplicador() As Integer
    Multiplicador = vMultiplica
End Property

Public Property Let Divisor(num As Integer)
    If Not num > 0 Then Exit Property
    vDivide = num
End Property

Public Property Get Divisor() As Integer
    Divisor = vDivide
End Property

Public Property Let Costo(num As Single)
    vCosto = num
End Property

Public Property Get Costo() As Single
    Costo = vCosto
End Property

Public Property Let Impuesto(num As Single)
    vIVACompra = num
End Property

Public Property Get Impuesto() As Single
    Impuesto = vIVACompra
End Property

Private Sub Class_Initialize()
    vMultiplica = 1
    vDivide = 1
End Sub
