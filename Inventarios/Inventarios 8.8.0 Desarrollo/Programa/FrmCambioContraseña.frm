VERSION 5.00
Object = "{0D452EE1-E08F-101A-852E-02608C4D0BB4}#2.0#0"; "FM20.DLL"
Begin VB.Form FrmCambioContraseña 
   BorderStyle     =   4  'Fixed ToolWindow
   Caption         =   "   Cambio de Contraseña"
   ClientHeight    =   2985
   ClientLeft      =   45
   ClientTop       =   315
   ClientWidth     =   4845
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   2985
   ScaleWidth      =   4845
   ShowInTaskbar   =   0   'False
   StartUpPosition =   1  'CenterOwner
   Begin VB.Frame FrmCambioCon 
      Height          =   2775
      Left            =   120
      TabIndex        =   8
      Top             =   120
      Width           =   4575
      Begin VB.TextBox TxtPswdAnt 
         Height          =   285
         IMEMode         =   3  'DISABLE
         Left            =   2160
         MaxLength       =   15
         PasswordChar    =   "*"
         TabIndex        =   1
         Top             =   720
         Width           =   2175
      End
      Begin VB.TextBox TxtPswdNuevo 
         Height          =   285
         IMEMode         =   3  'DISABLE
         Left            =   2160
         MaxLength       =   15
         PasswordChar    =   "*"
         TabIndex        =   2
         Top             =   1080
         Width           =   2175
      End
      Begin VB.TextBox TxtPswdConfir 
         Height          =   285
         IMEMode         =   3  'DISABLE
         Left            =   2160
         MaxLength       =   15
         PasswordChar    =   "*"
         TabIndex        =   3
         Top             =   1440
         Width           =   2175
      End
      Begin VB.TextBox TxtNombUsua 
         Height          =   285
         Left            =   2160
         MaxLength       =   30
         TabIndex        =   0
         Top             =   360
         Width           =   2175
      End
      Begin VB.CommandButton CmdOptions 
         Caption         =   "ACEPTAR"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   6
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   495
         Index           =   0
         Left            =   480
         Style           =   1  'Graphical
         TabIndex        =   6
         Top             =   2760
         Visible         =   0   'False
         Width           =   1215
      End
      Begin VB.CommandButton CmdOptions 
         Caption         =   "CANCELAR"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   6
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   495
         Index           =   1
         Left            =   1080
         Style           =   1  'Graphical
         TabIndex        =   7
         Top             =   2760
         Visible         =   0   'False
         Width           =   1215
      End
      Begin MSForms.CommandButton cancelar 
         Height          =   615
         Left            =   3120
         TabIndex        =   5
         Top             =   1920
         Width           =   1215
         Caption         =   "CANCELAR"
         Size            =   "2143;1085"
         Picture         =   "FrmCambioContraseña.frx":0000
         FontName        =   "Tahoma"
         FontEffects     =   1073741825
         FontHeight      =   120
         FontCharSet     =   0
         FontPitchAndFamily=   2
         ParagraphAlign  =   3
         FontWeight      =   700
      End
      Begin MSForms.CommandButton aceptar 
         Height          =   615
         Left            =   1800
         TabIndex        =   4
         Top             =   1920
         Width           =   1215
         Caption         =   "ACEPTAR"
         Size            =   "2143;1085"
         Picture         =   "FrmCambioContraseña.frx":0352
         FontName        =   "Tahoma"
         FontEffects     =   1073741825
         FontHeight      =   120
         FontCharSet     =   0
         FontPitchAndFamily=   2
         ParagraphAlign  =   3
         FontWeight      =   700
      End
      Begin VB.Label Label1 
         Caption         =   "Nombre de usuario:"
         Height          =   255
         Left            =   360
         TabIndex        =   12
         Top             =   360
         Width           =   1455
      End
      Begin VB.Label Label2 
         Caption         =   "Contraseña anterior:"
         Height          =   255
         Left            =   360
         TabIndex        =   11
         Top             =   720
         Width           =   1455
      End
      Begin VB.Label Label3 
         Caption         =   "Nueva contraseña:"
         Height          =   255
         Left            =   360
         TabIndex        =   10
         Top             =   1080
         Width           =   1455
      End
      Begin VB.Label Label4 
         Caption         =   "Confirmar contraseña:"
         Height          =   255
         Left            =   360
         TabIndex        =   9
         Top             =   1440
         Width           =   1575
      End
   End
End
Attribute VB_Name = "FrmCambioContraseña"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'APGR T3925 PNC - INICIO
'Se deja en comentario porque y
'se crean dos botones nuevos para acepatar y cancelar
Private Sub aceptar_Click()
               If ValidarInfo() = 0 Then Exit Sub
               If CambiarConstraseña = 1 Then
                  Call Mensaje1("La contraseña ha sido cambiada con exito.", 3)
               Else
                  Call Mensaje1("Falló el cambio de contraseña.", 1)
               End If
               Unload Me
End Sub
Private Sub cancelar_Click()
Unload Me
End Sub
'Private Sub CmdOptions_Click(Index As Integer)
'    Select Case Index
'        Case 0:
'               If ValidarInfo() = 0 Then Exit Sub
'               If CambiarConstraseña = 1 Then
'                  Call Mensaje1("La contraseña ha sido cambiada con exito.", 3)
'               Else
'                  Call Mensaje1("Falló el cambio de contraseña.", 1)
'               End If
'               Unload Me
'        Case 1: Unload Me
'    End Select
'End Sub
'APGR T3925 PNC - FIN
Function CambiarConstraseña() As Integer
    
    CambiarConstraseña = 0
    
    UserId = TxtNombUsua.Text
    UserPass = TxtPswdAnt.Text
    If ConexionBD = 0 Then
       Call Mensaje1("Error con la conexión al servidor. Revise el nombre del servidor, el nombre de usuario y la contraseña.", 1)
       Exit Function
    Else
      Select Case MotorBD
         Case "ACCESS":
            DBEngine.RegisterDatabase BDDSNODBCName, "Microsoft Access Driver (*.mdb)", True, "DEFAULTDIR=" & DirDB & vbCr & "DBQ=" & BDNameStr & ".mdb"
         Case "SQL"
            DBEngine.RegisterDatabase BDDSNODBCName, "SQL Server", True, "DATABASE=" & BDNameStr & vbCr & "SERVER=" & ServerBD
      End Select
    End If
    

    Valores = "TX_PASS_USUA=" & Comi & ConvertCadenaHexa(Encriptar(UCase(Trim(TxtPswdNuevo.Text)), UCase(Trim(TxtNombUsua.Text)))) & Comi
    Condi = "TX_IDEN_USUA=" & Comi & UCase(TxtNombUsua.Text) & Comi
    Result = DoUpdate("USUARIO", Valores, Condi)

    
    If Result <> FAIL Then CambiarConstraseña = 1
    
    Call LogOff
    
End Function

'DEPURACION DE CODIGO
'Sub CargarForma(NombUsuario As String)
'    TxtNombUsua.Text = UserId
'End Sub

Function ConexionBD() As Integer
'Dim Conn As Integer
   
   ConexionBD = 0
   
   Call LogOff
   If Not CrearConexSeg > 0 Then Exit Function
   
   If ConexionSeguridad Then
'      Conn = NewConn(1)
      
      Result = SetBDTime(240)
        
      ReDim Arr(0)
      Result = LoadData("PARAMETROS_INVE", "NU_NVERDB_PINV", NUL$, Arr)

      If Result <> FAIL Then
         If ModEjecucion = "EXE" And Arr(0) <> (App.Major & "." & App.Minor & "." & App.Revision) Then
            Call Mensaje1("La Versión de la Base de Datos no coincide con la de la aplicación " & Chr(13) & Chr(13) & "Comuniquese con el administrador del sistema", 1)
         Else
            'Call FormatosFecha
            Call FormatosFecha      'REOL M2222
            'If ConexionUser <> FAIL Then
            ConexionBD = 1
         End If
      End If
   End If
   
End Function

Function ValidarInfo() As Integer
    
    ValidarInfo = 0

    If Trim(TxtNombUsua.Text) = NUL$ Then Call Mensaje1("Debe ingresar el nombre de usuario.", 3): Exit Function
    If Trim(TxtPswdAnt.Text) = NUL$ Then Call Mensaje1("Debe ingresar la contraseña anterior.", 3): Exit Function
    If Trim(TxtPswdNuevo.Text) = NUL$ Then Call Mensaje1("Debe ingresar la nueva contraseña.", 3): Exit Function
    If Trim(TxtPswdConfir.Text) = NUL$ Then Call Mensaje1("Debe confirmar la nueva contraseña.", 3): Exit Function
    If Trim(TxtPswdNuevo.Text) <> Trim(TxtPswdConfir.Text) Then Call Mensaje1("La contraseña nueva y su confirmación no coinciden.", 3): Exit Function
    
    ValidarInfo = 1
End Function

Private Sub Form_Load()

End Sub

Private Sub TxtPswdAnt_KeyPress(KeyAscii As Integer)
    Call Cambiar_Enter(KeyAscii)
End Sub

Private Sub TxtPswdConfir_KeyPress(KeyAscii As Integer)
    Call Cambiar_Enter(KeyAscii)
End Sub

Private Sub TxtPswdNuevo_KeyPress(KeyAscii As Integer)
    Call Cambiar_Enter(KeyAscii)
End Sub
