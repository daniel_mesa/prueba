VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "MSFLXGRD.OCX"
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Object = "{C932BA88-4374-101B-A56C-00AA003668DC}#1.1#0"; "MSMASK32.OCX"
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Begin VB.Form FrmPROYCOMPRA 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Proyecto de Compra"
   ClientHeight    =   6435
   ClientLeft      =   45
   ClientTop       =   435
   ClientWidth     =   9735
   Icon            =   "FrmPROYCOMPRA.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   6435
   ScaleMode       =   0  'User
   ScaleWidth      =   9735
   Begin MSComctlLib.Toolbar tlbACCIONES 
      Height          =   630
      Index           =   4
      Left            =   2280
      TabIndex        =   17
      Top             =   3600
      Width           =   9675
      _ExtentX        =   17066
      _ExtentY        =   1111
      ButtonWidth     =   1323
      ButtonHeight    =   1005
      AllowCustomize  =   0   'False
      Appearance      =   1
      _Version        =   393216
      BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
         NumButtons      =   1
         BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Enabled         =   0   'False
            Caption         =   "Exportar"
            Key             =   "EXP"
            Object.ToolTipText     =   "Generar archivo plano"
         EndProperty
      EndProperty
      Begin VB.TextBox txtCUALOP 
         Height          =   375
         Left            =   1080
         TabIndex        =   20
         Text            =   "0"
         Top             =   120
         Width           =   720
      End
      Begin MSComCtl2.UpDown updCUALOP 
         Height          =   375
         Left            =   1800
         TabIndex        =   19
         Top             =   120
         Width           =   255
         _ExtentX        =   450
         _ExtentY        =   661
         _Version        =   393216
         BuddyControl    =   "txtCUALOP"
         BuddyDispid     =   196609
         OrigLeft        =   1800
         OrigTop         =   120
         OrigRight       =   2055
         OrigBottom      =   495
         SyncBuddy       =   -1  'True
         BuddyProperty   =   65547
         Enabled         =   -1  'True
      End
   End
   Begin MSComctlLib.Toolbar tlbACCIONES 
      Height          =   630
      Index           =   3
      Left            =   2280
      TabIndex        =   12
      Top             =   2760
      Width           =   9675
      _ExtentX        =   17066
      _ExtentY        =   1111
      ButtonWidth     =   1323
      ButtonHeight    =   1005
      AllowCustomize  =   0   'False
      Appearance      =   1
      _Version        =   393216
      BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
         NumButtons      =   1
         BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Enabled         =   0   'False
            Caption         =   "Exportar"
            Key             =   "EXP"
            Object.ToolTipText     =   "Generar archivo plano"
         EndProperty
      EndProperty
      Begin VB.ComboBox cmbTERCEROS 
         Height          =   315
         ItemData        =   "FrmPROYCOMPRA.frx":058A
         Left            =   840
         List            =   "FrmPROYCOMPRA.frx":0594
         Style           =   2  'Dropdown List
         TabIndex        =   13
         Top             =   120
         Width           =   3375
      End
   End
   Begin MSComctlLib.Toolbar tlbACCIONES 
      Height          =   630
      Index           =   2
      Left            =   2280
      TabIndex        =   2
      Top             =   1920
      Width           =   9675
      _ExtentX        =   17066
      _ExtentY        =   1111
      ButtonWidth     =   1323
      ButtonHeight    =   1005
      AllowCustomize  =   0   'False
      Appearance      =   1
      _Version        =   393216
      BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
         NumButtons      =   4
         BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "Guardar"
            Key             =   "SAV"
            Object.ToolTipText     =   "Guardar el Proyecto"
         EndProperty
         BeginProperty Button2 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "Abrir"
            Key             =   "LOA"
            Object.ToolTipText     =   "Abrir un Proyecto"
         EndProperty
         BeginProperty Button3 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Style           =   4
            Object.Width           =   1800
         EndProperty
         BeginProperty Button4 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "Calcular"
            Key             =   "CAL"
            Object.ToolTipText     =   "Procesar la Informaci�n"
         EndProperty
      EndProperty
      Begin MSMask.MaskEdBox txtNUPROY 
         Height          =   375
         Left            =   2190
         TabIndex        =   16
         ToolTipText     =   "# de Proyecto"
         Top             =   120
         Width           =   915
         _ExtentX        =   1614
         _ExtentY        =   661
         _Version        =   393216
         ClipMode        =   1
         PromptInclude   =   0   'False
         MaxLength       =   6
         Mask            =   "999999"
         PromptChar      =   "_"
      End
      Begin VB.TextBox Text3 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         BackColor       =   &H80000000&
         BorderStyle     =   0  'None
         Enabled         =   0   'False
         Height          =   255
         Left            =   1440
         TabIndex        =   15
         Text            =   "# de Proy:"
         Top             =   200
         Width           =   735
      End
   End
   Begin MSComctlLib.Toolbar tlbACCIONES 
      Height          =   630
      Index           =   0
      Left            =   2280
      TabIndex        =   1
      Top             =   240
      Width           =   9675
      _ExtentX        =   17066
      _ExtentY        =   1111
      ButtonWidth     =   1296
      ButtonHeight    =   1005
      AllowCustomize  =   0   'False
      Appearance      =   1
      _Version        =   393216
      BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
         NumButtons      =   2
         BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "Agregar"
            Key             =   "ADD"
            Object.ToolTipText     =   "Agregar un Art�culo"
         EndProperty
         BeginProperty Button2 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "Borrar"
            Key             =   "DEL"
            Object.ToolTipText     =   "Borrar un Art�culo"
         EndProperty
      EndProperty
      Begin VB.ComboBox cmbTIPO 
         Height          =   315
         ItemData        =   "FrmPROYCOMPRA.frx":05A8
         Left            =   1440
         List            =   "FrmPROYCOMPRA.frx":05B2
         TabIndex        =   3
         Top             =   120
         Width           =   2175
      End
   End
   Begin MSComctlLib.Toolbar tlbACCIONES 
      Height          =   630
      Index           =   1
      Left            =   2280
      TabIndex        =   0
      Top             =   1080
      Width           =   9675
      _ExtentX        =   17066
      _ExtentY        =   1111
      ButtonWidth     =   1376
      ButtonHeight    =   1005
      AllowCustomize  =   0   'False
      Appearance      =   1
      _Version        =   393216
      BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
         NumButtons      =   4
         BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "Agregar"
            Key             =   "ADD"
            Object.ToolTipText     =   "Agregar un Proveedor"
         EndProperty
         BeginProperty Button2 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "Borrar"
            Key             =   "DEL"
            Object.ToolTipText     =   "Borrar un Proveedor"
         EndProperty
         BeginProperty Button3 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Style           =   4
            Object.Width           =   3000
         EndProperty
         BeginProperty Button4 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "Cambiar"
            Key             =   "CHA"
         EndProperty
      EndProperty
      Begin VB.TextBox Text2 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         BackColor       =   &H80000000&
         BorderStyle     =   0  'None
         Enabled         =   0   'False
         Height          =   255
         Left            =   2910
         TabIndex        =   11
         Text            =   "% Precio"
         Top             =   200
         Width           =   735
      End
      Begin VB.TextBox Text1 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         BackColor       =   &H80000000&
         BorderStyle     =   0  'None
         Enabled         =   0   'False
         Height          =   255
         Left            =   1440
         TabIndex        =   10
         Text            =   "% Calif."
         Top             =   200
         Width           =   735
      End
      Begin MSMask.MaskEdBox txtPRECIO 
         Height          =   375
         Left            =   3660
         TabIndex        =   5
         ToolTipText     =   "% Peso Precio"
         Top             =   120
         Width           =   615
         _ExtentX        =   1085
         _ExtentY        =   661
         _Version        =   393216
         ClipMode        =   1
         PromptInclude   =   0   'False
         MaxLength       =   3
         Mask            =   "999"
         PromptChar      =   "_"
      End
      Begin MSMask.MaskEdBox txtCALIFI 
         Height          =   375
         Left            =   2190
         TabIndex        =   4
         ToolTipText     =   "% Peso Calificaci�n"
         Top             =   120
         Width           =   615
         _ExtentX        =   1085
         _ExtentY        =   661
         _Version        =   393216
         ClipMode        =   1
         PromptInclude   =   0   'False
         MaxLength       =   3
         Mask            =   "999"
         PromptChar      =   "_"
      End
   End
   Begin MSComctlLib.ImageList imgBotones 
      Left            =   720
      Top             =   120
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   20
      ImageHeight     =   14
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   15
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FrmPROYCOMPRA.frx":05C6
            Key             =   ""
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FrmPROYCOMPRA.frx":0918
            Key             =   "ASG"
         EndProperty
         BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FrmPROYCOMPRA.frx":0BD2
            Key             =   "CAN"
         EndProperty
         BeginProperty ListImage4 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FrmPROYCOMPRA.frx":0F24
            Key             =   "SAV"
         EndProperty
         BeginProperty ListImage5 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FrmPROYCOMPRA.frx":123E
            Key             =   "ADD"
         EndProperty
         BeginProperty ListImage6 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FrmPROYCOMPRA.frx":14F8
            Key             =   "DEL"
         EndProperty
         BeginProperty ListImage7 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FrmPROYCOMPRA.frx":193A
            Key             =   "CHA"
         EndProperty
         BeginProperty ListImage8 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FrmPROYCOMPRA.frx":1C5C
            Key             =   "PRN"
            Object.Tag             =   "PRN"
         EndProperty
         BeginProperty ListImage9 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FrmPROYCOMPRA.frx":209E
            Key             =   "GNR"
            Object.Tag             =   "GNR"
         EndProperty
         BeginProperty ListImage10 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FrmPROYCOMPRA.frx":2420
            Key             =   "ATC"
         EndProperty
         BeginProperty ListImage11 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FrmPROYCOMPRA.frx":2772
            Key             =   "CRG"
            Object.Tag             =   "CRG"
         EndProperty
         BeginProperty ListImage12 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FrmPROYCOMPRA.frx":2A8C
            Key             =   "CMP"
         EndProperty
         BeginProperty ListImage13 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FrmPROYCOMPRA.frx":2ECE
            Key             =   "GRD"
         EndProperty
         BeginProperty ListImage14 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FrmPROYCOMPRA.frx":3220
            Key             =   "ABR"
         EndProperty
         BeginProperty ListImage15 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FrmPROYCOMPRA.frx":3572
            Key             =   "EXP"
         EndProperty
      EndProperty
   End
   Begin TabDlg.SSTab ssSEPARA 
      Height          =   5295
      Left            =   360
      TabIndex        =   6
      Top             =   840
      Width           =   9015
      _ExtentX        =   15901
      _ExtentY        =   9340
      _Version        =   393216
      Tabs            =   5
      Tab             =   4
      TabsPerRow      =   5
      TabHeight       =   520
      TabCaption(0)   =   "Que Comprar"
      TabPicture(0)   =   "FrmPROYCOMPRA.frx":388C
      Tab(0).ControlEnabled=   0   'False
      Tab(0).Control(0)=   "lstARTICULO"
      Tab(0).Control(0).Enabled=   0   'False
      Tab(0).ControlCount=   1
      TabCaption(1)   =   "A Qui�n"
      TabPicture(1)   =   "FrmPROYCOMPRA.frx":38A8
      Tab(1).ControlEnabled=   0   'False
      Tab(1).Control(0)=   "lstTERCERO"
      Tab(1).Control(0).Enabled=   0   'False
      Tab(1).ControlCount=   1
      TabCaption(2)   =   "Proyecto"
      TabPicture(2)   =   "FrmPROYCOMPRA.frx":38C4
      Tab(2).ControlEnabled=   0   'False
      Tab(2).Control(0)=   "grdPROYECTO"
      Tab(2).Control(0).Enabled=   0   'False
      Tab(2).ControlCount=   1
      TabCaption(3)   =   "Como Va"
      TabPicture(3)   =   "FrmPROYCOMPRA.frx":38E0
      Tab(3).ControlEnabled=   0   'False
      Tab(3).Control(0)=   "lstORDEN"
      Tab(3).Control(0).Enabled=   0   'False
      Tab(3).ControlCount=   1
      TabCaption(4)   =   "Opciones"
      TabPicture(4)   =   "FrmPROYCOMPRA.frx":38FC
      Tab(4).ControlEnabled=   -1  'True
      Tab(4).Control(0)=   "lstOPCION"
      Tab(4).Control(0).Enabled=   0   'False
      Tab(4).ControlCount=   1
      Begin MSFlexGridLib.MSFlexGrid grdPROYECTO 
         Height          =   4455
         Left            =   -74640
         TabIndex        =   7
         Top             =   620
         Width           =   8295
         _ExtentX        =   14631
         _ExtentY        =   7858
         _Version        =   393216
         AllowUserResizing=   1
      End
      Begin MSComctlLib.ListView lstARTICULO 
         Height          =   4305
         Left            =   -74760
         TabIndex        =   8
         Top             =   740
         Width           =   8550
         _ExtentX        =   15081
         _ExtentY        =   7594
         View            =   3
         LabelEdit       =   1
         MultiSelect     =   -1  'True
         LabelWrap       =   -1  'True
         HideSelection   =   0   'False
         AllowReorder    =   -1  'True
         FullRowSelect   =   -1  'True
         GridLines       =   -1  'True
         _Version        =   393217
         ForeColor       =   -2147483640
         BackColor       =   -2147483643
         BorderStyle     =   1
         Appearance      =   1
         NumItems        =   0
      End
      Begin MSComctlLib.ListView lstTERCERO 
         Height          =   4305
         Left            =   -74760
         TabIndex        =   9
         Top             =   740
         Width           =   8550
         _ExtentX        =   15081
         _ExtentY        =   7594
         View            =   3
         LabelEdit       =   1
         MultiSelect     =   -1  'True
         LabelWrap       =   -1  'True
         HideSelection   =   0   'False
         AllowReorder    =   -1  'True
         FullRowSelect   =   -1  'True
         GridLines       =   -1  'True
         _Version        =   393217
         ForeColor       =   -2147483640
         BackColor       =   -2147483643
         BorderStyle     =   1
         Appearance      =   1
         NumItems        =   0
      End
      Begin MSComctlLib.ListView lstORDEN 
         Height          =   4305
         Left            =   -74760
         TabIndex        =   14
         Top             =   740
         Width           =   8550
         _ExtentX        =   15081
         _ExtentY        =   7594
         View            =   3
         LabelEdit       =   1
         MultiSelect     =   -1  'True
         LabelWrap       =   -1  'True
         HideSelection   =   0   'False
         AllowReorder    =   -1  'True
         FullRowSelect   =   -1  'True
         GridLines       =   -1  'True
         _Version        =   393217
         ForeColor       =   -2147483640
         BackColor       =   -2147483643
         BorderStyle     =   1
         Appearance      =   1
         NumItems        =   0
      End
      Begin MSComctlLib.ListView lstOPCION 
         Height          =   4305
         Left            =   240
         TabIndex        =   18
         Top             =   720
         Width           =   8550
         _ExtentX        =   15081
         _ExtentY        =   7594
         View            =   3
         LabelEdit       =   1
         MultiSelect     =   -1  'True
         LabelWrap       =   -1  'True
         HideSelection   =   0   'False
         AllowReorder    =   -1  'True
         FullRowSelect   =   -1  'True
         GridLines       =   -1  'True
         _Version        =   393217
         ForeColor       =   -2147483640
         BackColor       =   -2147483643
         BorderStyle     =   1
         Appearance      =   1
         NumItems        =   0
      End
   End
   Begin MSComctlLib.ListView lstCDBXPROVE 
      Height          =   4305
      Left            =   120
      TabIndex        =   21
      Top             =   1440
      Visible         =   0   'False
      Width           =   8550
      _ExtentX        =   15081
      _ExtentY        =   7594
      View            =   3
      LabelEdit       =   1
      MultiSelect     =   -1  'True
      LabelWrap       =   -1  'True
      HideSelection   =   0   'False
      FullRowSelect   =   -1  'True
      GridLines       =   -1  'True
      _Version        =   393217
      ForeColor       =   -2147483640
      BackColor       =   -2147483643
      BorderStyle     =   1
      Appearance      =   1
      NumItems        =   0
   End
End
Attribute VB_Name = "FrmPROYCOMPRA"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Public OpcCod        As String   'Opci�n de seguridad
Private ArrPYC() As Variant
Private vTipoArticulo As String * 1
'Private vTipoBodega As String * 1      'DEPURACION DE CODIGO
Private vTercero As String
Private vArticulo As String
Private vColAutoArticulo As Byte
Private vColCodigoArticulo As Byte
Private vColNombreArticulo As Byte
Private vColCDBarra As Byte
Private vColNombreComercial As Byte
Private vColNitTercero As Byte
Private vColNombreTercero As Byte
Private vColCosto As Byte
Private vColImpuesto As Byte
Private vColCalifica As Byte
Private vColOpcion As Byte
Private vColOrden As Byte
Private CllCDBXArt As Collection
Private UnArticulo As New ElArticulo
Private UnTercero As New ElTercero
Private UnCDBarra As New CdgBarra
Private vContar As Integer, vclave As String

Private Sub cmbTERCEROS_LostFocus()
'    Dim vUno As MSComctlLib.ListItem, vDos As MSComctlLib.ListItem
    Dim vContar As Integer, vclave As String
    If Me.cmbTERCEROS.ListIndex < 0 Then Exit Sub
    Me.lstORDEN.ListItems.Clear
    With Me.grdPROYECTO
    For vContar = 1 To Me.grdPROYECTO.Rows - 1
        If .TextMatrix(vContar, vColNombreTercero) = Me.cmbTERCEROS.List(Me.cmbTERCEROS.ListIndex) Then
'            vclave = "A" & .TextMatrix(vContar, vColCodigoArticulo)
            vclave = "A" & vContar
            Me.lstORDEN.ListItems.Add , vclave, .TextMatrix(vContar, vColCodigoArticulo)
            Me.lstORDEN.ListItems(vclave).ListSubItems.Add , "NOM", .TextMatrix(vContar, vColNombreArticulo)
            Me.lstORDEN.ListItems(vclave).ListSubItems.Add , "BAR", .TextMatrix(vContar, vColCDBarra)
            Me.lstORDEN.ListItems(vclave).ListSubItems.Add , "CIA", .TextMatrix(vContar, vColNombreComercial)
            Me.lstORDEN.ListItems(vclave).ListSubItems.Add , "OPC", Format(CInt(.TextMatrix(vContar, vColOpcion)), "#,##0")
            Me.lstORDEN.ListItems(vclave).ListSubItems.Add , "COS", Format(CSng(.TextMatrix(vContar, vColCosto)), "#,##0.00")
            Me.lstORDEN.ListItems(vclave).ListSubItems.Add , "IMP", Format(CSng(.TextMatrix(vContar, vColImpuesto)), "#0.00")
        End If 'COD, NOM, BAR,CIA,OPC, COS, SUB lstORDEN
    Next
    End With
End Sub

Private Sub Form_Load()
    Dim vContar As Integer
    Call CenterForm(MDI_Inventarios, Me)
    Me.grdPROYECTO.Rows = 2
    Me.grdPROYECTO.FixedRows = 1
'Auto, Codigo, Descripci�n, Nit, Nombre, Costo, Califica, Opci�n, Orden
'vColAutoArticulo=0:vColCodigoArticulo=1:vColNombreArticulo=2:vColCDBarra=3:vColNombreComercial=4:vColNitTercero=5
'vColNombreTercero=6:vColCosto=7:vColCalifica=8:vColOpcion=9:vColImpuesto=10:vColOrden=11
    Call GrdFDef(grdPROYECTO, 0, 0, "Auto,+0," & _
        "Codigo,0," & _
        "Descripci�n,2300," & _
        "CD Barra,1500," & _
        "Nom C/ial,1800," & _
        "Nit,0," & _
        "Nombre,2800," & _
        "Costo,+1200," & _
        "Califica,+900," & _
        "Opci�n,+900," & _
        "Impuesto,+0," & _
        "Orden,+0")
    
    vColAutoArticulo = 0: vColCodigoArticulo = 1: vColNombreArticulo = 2: vColCDBarra = 3: vColNombreComercial = 4
    vColNitTercero = 5: vColNombreTercero = 6: vColCosto = 7: vColCalifica = 8: vColOpcion = 9: vColImpuesto = 10: vColOrden = 11
    
    Me.grdPROYECTO.FixedCols = 3
    Me.grdPROYECTO.MergeCells = flexMergeRestrictColumns
    Me.grdPROYECTO.MergeCol(0) = True
    Me.grdPROYECTO.MergeCol(1) = True
    Me.grdPROYECTO.MergeCol(2) = True
    Me.grdPROYECTO.MergeCol(3) = True
    Me.cmbTIPO.ListIndex = 0
    Me.txtNUPROY.Text = "0"

'COD, NOM, BAR,CIA,OPC, COS, SUB lstORDEN
    Me.lstORDEN.ColumnHeaders.Clear
    Me.lstORDEN.ColumnHeaders.Add , "COD", "C�digo", 0.12 * Me.lstORDEN.Width
    Me.lstORDEN.ColumnHeaders.Add , "NOM", "Nombre", 0.3 * Me.lstORDEN.Width
    Me.lstORDEN.ColumnHeaders.Add , "BAR", "CDBarra", 0.12 * Me.lstOPCION.Width
    Me.lstORDEN.ColumnHeaders.Add , "CIA", "Nom C/ial", 0.18 * Me.lstOPCION.Width
    Me.lstORDEN.ColumnHeaders.Add , "OPC", "Opci�n", 0.12 * Me.lstORDEN.Width
    Me.lstORDEN.ColumnHeaders.Add , "COS", "Cos U/dad", 0.12 * Me.lstORDEN.Width
    Me.lstORDEN.ColumnHeaders.Add , "IMP", "Impuesto", 0.1 * Me.lstORDEN.Width
    Me.lstORDEN.ColumnHeaders.Item("OPC").Alignment = lvwColumnRight
    Me.lstORDEN.ColumnHeaders.Item("COS").Alignment = lvwColumnRight
    Me.lstORDEN.ColumnHeaders.Item("IMP").Alignment = lvwColumnRight

'COD,NOM,BAR,CIA,PRO,OPC,COS,IMP lstOPCION
    Me.lstOPCION.ColumnHeaders.Clear
    Me.lstOPCION.ColumnHeaders.Add , "COD", "C�digo", 0.12 * Me.lstOPCION.Width
    Me.lstOPCION.ColumnHeaders.Add , "NOM", "Nombre", 0.25 * Me.lstOPCION.Width
    Me.lstOPCION.ColumnHeaders.Add , "BAR", "CDBarra", 0.18 * Me.lstOPCION.Width
    Me.lstOPCION.ColumnHeaders.Add , "CIA", "Nom C/ial", 0.18 * Me.lstOPCION.Width
    Me.lstOPCION.ColumnHeaders.Add , "PRO", "Proveedor", 0.25 * Me.lstOPCION.Width
    Me.lstOPCION.ColumnHeaders.Add , "OPC", "Opci�n", 0
    Me.lstOPCION.ColumnHeaders.Add , "COS", "Cos U/dad", 0.12 * Me.lstOPCION.Width
    Me.lstOPCION.ColumnHeaders.Add , "IMP", "Impuesto", 0.1 * Me.lstOPCION.Width
    Me.lstOPCION.ColumnHeaders.Item("OPC").Alignment = lvwColumnRight
    Me.lstOPCION.ColumnHeaders.Item("COS").Alignment = lvwColumnRight
    Me.lstOPCION.ColumnHeaders.Item("IMP").Alignment = lvwColumnRight
    
    vTipoArticulo = IIf(Me.cmbTIPO.ListIndex = 0, "V", "C")
'COD, BAR, NOM, CIA, UND, AUT lstARTICULO
''IN_CODIGOBAR(NU_AUTO_ARTI_COBA,TX_COBA_COBA,TX_REGI_COBA,TX_CIAL_COBA,TX_LABO_COBA)
    Me.lstARTICULO.ColumnHeaders.Clear
    Me.lstARTICULO.ColumnHeaders.Add , "COD", "C�digo", 0.12 * Me.lstARTICULO.Width
    Me.lstARTICULO.ColumnHeaders.Add , "BAR", "CD Barra", 0.2 * Me.lstARTICULO.Width
    Me.lstARTICULO.ColumnHeaders.Add , "NOM", "Descripci�n", 0.3 * Me.lstARTICULO.Width
    Me.lstARTICULO.ColumnHeaders.Add , "CIA", "Nom C/ial", 0.25 * Me.lstARTICULO.Width
    Me.lstARTICULO.ColumnHeaders.Add , "UND", "Unidad", 0.25 * Me.lstARTICULO.Width
    Me.lstARTICULO.ColumnHeaders.Add , "AUT", "Auto", 0 * Me.lstARTICULO.Width

    Me.lstTERCERO.ColumnHeaders.Clear
'NIT, NOM, CLF, PRC, PUN, COS, IMP lstTERCERO
    Me.lstTERCERO.ColumnHeaders.Add , "NIT", "NIT", 0.18 * Me.lstTERCERO.Width
    Me.lstTERCERO.ColumnHeaders.Add , "NOM", "Nombre", 0.5 * Me.lstTERCERO.Width
    Me.lstTERCERO.ColumnHeaders.Add , "CLF", "% Califc", 0.12 * Me.lstTERCERO.Width
    Me.lstTERCERO.ColumnHeaders.Add , "PRC", "% Precio", 0.12 * Me.lstTERCERO.Width
    Me.lstTERCERO.ColumnHeaders.Add , "PUN", "Puntaje", 0
    Me.lstTERCERO.ColumnHeaders.Add , "COS", "Costo", 0
    Me.lstTERCERO.ColumnHeaders.Add , "IMP", "Impuesto", 0
    Me.lstTERCERO.ColumnHeaders.Item("CLF").Alignment = lvwColumnRight
    Me.lstTERCERO.ColumnHeaders.Item("PRC").Alignment = lvwColumnRight
    Me.lstTERCERO.ColumnHeaders.Item("PUN").Alignment = lvwColumnRight
    Me.lstTERCERO.ColumnHeaders.Item("COS").Alignment = lvwColumnRight
    Me.lstTERCERO.ColumnHeaders.Item("IMP").Alignment = lvwColumnRight
    
    Me.lstCDBXPROVE.ColumnHeaders.Clear
'NIT, NOM, BAR, CLF, PRC, PUN, COS, IMP lstCDBXPROVE
    Me.lstCDBXPROVE.ColumnHeaders.Add , "NIT", "NIT", 0.18 * Me.lstCDBXPROVE.Width
    Me.lstCDBXPROVE.ColumnHeaders.Add , "NOM", "Nombre", 0.18 * Me.lstCDBXPROVE.Width
    Me.lstCDBXPROVE.ColumnHeaders.Add , "BAR", "CDBarra", 0.2 * Me.lstCDBXPROVE.Width
    Me.lstCDBXPROVE.ColumnHeaders.Add , "CLF", "Califica", 0.12 * Me.lstCDBXPROVE.Width
    Me.lstCDBXPROVE.ColumnHeaders.Add , "PRC", "Porcent", 0.12 * Me.lstCDBXPROVE.Width
    Me.lstCDBXPROVE.ColumnHeaders.Add , "PUN", "Puntaje", 0.12 * Me.lstCDBXPROVE.Width
    Me.lstCDBXPROVE.ColumnHeaders.Add , "COS", "Costo", 0.08 * Me.lstCDBXPROVE.Width
    Me.lstCDBXPROVE.ColumnHeaders.Add , "IMP", "Impuesto", 0.08 * Me.lstCDBXPROVE.Width
    
    For vContar = 1 To Me.tlbACCIONES.Count
        Me.tlbACCIONES.Item(vContar - 1).Align = vbAlignTop 'vBarraHerramienta
        Me.tlbACCIONES.Item(vContar - 1).Visible = False
    Next
    Me.tlbACCIONES.Item(0).ImageList = Me.imgBotones
    Me.tlbACCIONES.Item(0).Buttons.Item("ADD").Image = "ATC"
    Me.tlbACCIONES.Item(0).Buttons.Item("DEL").Image = "DEL"
    Me.tlbACCIONES.Item(1).ImageList = Me.imgBotones
    Me.tlbACCIONES.Item(1).Buttons.Item("ADD").Image = "ATC"
    Me.tlbACCIONES.Item(1).Buttons.Item("DEL").Image = "DEL"
    Me.tlbACCIONES.Item(1).Buttons.Item("CHA").Image = "CHA"
    Me.tlbACCIONES.Item(2).ImageList = Me.imgBotones
    Me.tlbACCIONES.Item(2).Buttons.Item("LOA").Image = "ABR"
    Me.tlbACCIONES.Item(2).Buttons.Item("SAV").Image = "GRD"
    Me.tlbACCIONES.Item(2).Buttons.Item("CAL").Image = "GNR"
    Me.tlbACCIONES.Item(3).ImageList = Me.imgBotones
    Me.tlbACCIONES.Item(3).Buttons.Item("EXP").Image = "EXP"
    Me.tlbACCIONES.Item(0).Visible = True
    Me.ssSEPARA.Tab = 0
    Me.txtCALIFI.Text = "0"
    Me.txtPRECIO.Text = "100"
End Sub

Private Sub grdPROYECTO_EnterCell()
    With Me.grdPROYECTO
        If Not .Col = vColOpcion Then Exit Sub
        If Len(.TextMatrix(.Row, vColOpcion)) = 0 Then .TextMatrix(.Row, vColOpcion) = "0"
    End With
End Sub

Private Sub grdPROYECTO_KeyPress(KeyAscii As Integer)
    If Me.grdPROYECTO.Col = vColOpcion Then
        Call Escribir_Fgrid(Me.grdPROYECTO, vColOpcion, KeyAscii, 6, 3, False, Me)
    End If
End Sub

Private Sub lstOPCION_ColumnClick(ByVal ColumnHeader As MSComctlLib.ColumnHeader)
    Me.lstOPCION.SortKey = ColumnHeader.Index - 1
    Me.lstOPCION.Sorted = True
End Sub

Private Sub lstORDEN_ColumnClick(ByVal ColumnHeader As MSComctlLib.ColumnHeader)
    Me.lstORDEN.SortKey = ColumnHeader.Index - 1
    Me.lstORDEN.Sorted = True
End Sub

Private Sub lstTERCERO_KeyUp(KeyCode As Integer, Shift As Integer)
    If vArticulo = Me.lstTERCERO.SelectedItem.Key Then Exit Sub
    vArticulo = Me.lstTERCERO.SelectedItem.Key
    Me.txtCALIFI.Text = Me.lstTERCERO.SelectedItem.ListSubItems("CLF").Text
    Me.txtPRECIO.Text = Me.lstTERCERO.SelectedItem.ListSubItems("PRC").Text
End Sub

Private Sub ssSEPARA_Click(PreviousTab As Integer)
'    Dim vUno As MSComctlLib.ListItem, vDos As MSComctlLib.ListItem
    Dim vUno As MSComctlLib.ListItem        'DEPURACION DE CODIGO
    If Me.ssSEPARA.Tab = PreviousTab Then Exit Sub
    Me.tlbACCIONES.Item(PreviousTab).Visible = False
    Me.tlbACCIONES.Item(Me.ssSEPARA.Tab).Visible = True
    If Me.ssSEPARA.Tab = 3 Then
        Me.cmbTERCEROS.Clear
        Me.lstORDEN.ListItems.Clear
        For Each vUno In Me.lstTERCERO.ListItems
            Me.cmbTERCEROS.AddItem vUno.ListSubItems("NOM").Text
'            Me.cmbTERCEROS.ItemData(Me.cmbTERCEROS.NewIndex) = vUno.ListSubItems("AUT").Text
        Next
    End If
End Sub

Private Sub tlbACCIONES_ButtonClick(Index As Integer, ByVal Button As MSComctlLib.Button)
    Dim vAsignado As Long
    Dim vCuantos As Integer, vCuerda As String, vFila As Integer
    Dim vCual As MSComctlLib.ListView, ArrPRYC() As Variant
    Dim vUno As MSComctlLib.ListItem, vDos As MSComctlLib.ListItem
    Dim vSetListaPrecio As New ADODB.Recordset, vSetCriterios As New ADODB.Recordset
    Dim vCuerdaArticulos As String, vCuerdaTerceros As String
    Dim vCuerdaSQLArticulos As String, vCuerdaSQLCriterios As String
    Dim vCosto As Single, vPeso As Single, vNorma As Integer, vOpcion As Integer
    Dim vCalifica As Single, vTamano As Integer, vIVACompra As Single
    Dim vRecorre As Integer
'    Dim vProveedor As New ElTercero
    Select Case Button.Key
    Case Is = "ADD"
        Select Case Index
        Case Is = 0 'from ARTICULO, TC_IMPUESTOS where CD_IMPU_ARTI_TCIM=CD_CODI_IMPU
            FrmSeleccion3.Lbl_Tabla = "ARTICULO, TC_IMPUESTOS, IN_UNDVENTA"
''          FrmSeleccion3.Lbl_Campos = "NU_AUTO_ARTI, NO_NOMB_ARTI, CD_CODI_ARTI, VL_COPR_ARTI, TX_NOMB_UNVE, DE_CTRA_ARTI,'',''"
'            FrmSeleccion3.Lbl_Campos = "'', NO_NOMB_ARTI, CD_CODI_ARTI, VL_COPR_ARTI, TX_NOMB_UNVE, DE_CTRA_ARTI, NU_AUTO_ARTI, ''"
            FrmSeleccion3.Lbl_Campos = "'', NO_NOMB_ARTI, CD_CODI_ARTI, VL_COPR_ARTI, TX_NOMB_UNVE, DE_CTRA_ARTI, NU_AUTO_ARTI, '', PR_PORC_IMPU, VL_COPR_ARTI, PR_PORC_IMPU"
            FrmSeleccion3.Lbl_Titulo = "Selecci�n de Art�culos"
            FrmSeleccion3.Caption = "Selecci�n de Art�culos"
            FrmSeleccion3.Lbl_Condicion = "CD_IMPU_ARTI_TCIM=CD_CODI_IMPU AND NU_AUTO_UNVE_ARTI=NU_AUTO_UNVE AND TX_PARA_ARTI=" & Comi & vTipoArticulo & Comi
            FrmSeleccion3.TipoDeBodega = vTipoArticulo
            FrmSeleccion3.EsOrdenDeCompra = False
            FrmSeleccion3.Show 1
            vContar = 1
            vclave = ""
            Do Until Mselec2(vContar, 3) = ""
                vclave = vclave & IIf(Len(vclave) = 0, "", ",") & Mselec2(vContar, 1)
                vContar = vContar + 1
            Loop
            vclave = "(" & vclave & ")"
            Desde = "IN_CODIGOBAR,ARTICULO,IN_UNDVENTA"
            Campos = "NU_AUTO_ARTI, CD_CODI_ARTI, NO_NOMB_ARTI, TX_NOMB_UNVE, TX_COBA_COBA, TX_CIAL_COBA"
            Condi = "NU_AUTO_UNVE_ARTI=NU_AUTO_UNVE"
            Condi = Condi & " AND NU_AUTO_ARTI_COBA=NU_AUTO_ARTI"
            Condi = Condi & " AND NU_AUTO_ARTI IN " & vclave
            ReDim ArrPYC(5, 0)
            Result = LoadMulData(Desde, Campos, Condi, ArrPYC())
            If Result = FAIL Then Exit Sub
            If Not Encontro Then Exit Sub
            On Error GoTo Articulo
'COD, BAR, NOM, CIA, UND, AUT lstARTICULO
''IN_CODIGOBAR(NU_AUTO_ARTI_COBA,TX_COBA_COBA,TX_REGI_COBA,TX_CIAL_COBA,TX_LABO_COBA)
            For vContar = 0 To UBound(ArrPYC, 2)
                vclave = "B" & vContar
                Set vUno = Me.lstARTICULO.FindItem(ArrPYC(4, vContar), lvwTag)
                If vUno Is Nothing Then
                    Me.lstARTICULO.ListItems.Add , vclave, ArrPYC(1, vContar)
                    Me.lstARTICULO.ListItems.Item(vclave).Tag = ArrPYC(4, vContar)
                    Me.lstARTICULO.ListItems.Item(vclave).ListSubItems.Add , "BAR", ArrPYC(4, vContar)
                    Me.lstARTICULO.ListItems.Item(vclave).ListSubItems.Add , "NOM", ArrPYC(2, vContar)
                    Me.lstARTICULO.ListItems.Item(vclave).ListSubItems.Add , "CIA", ArrPYC(5, vContar)
                    Me.lstARTICULO.ListItems.Item(vclave).ListSubItems.Add , "UND", ArrPYC(3, vContar)
                    Me.lstARTICULO.ListItems.Item(vclave).ListSubItems.Add , "AUT", ArrPYC(0, vContar)
                End If
Articulo:
            Next
            Me.lstARTICULO.SortKey = 1
            Me.lstARTICULO.SortOrder = lvwAscending
            Me.lstARTICULO.Sorted = True
            On Error GoTo 0
        Case Is = 1
            Call NormaCalificaPrecio
            FrmSeleccion4.Lbl_Tabla = "TERCERO"
            FrmSeleccion4.Lbl_Campos = "CD_CODI_TERC,NO_NOMB_TERC,CD_CODI_TERC,'',''"
            FrmSeleccion4.Lbl_Titulo = "Selecci�n de Proveedores"
            FrmSeleccion4.Caption = "Selecci�n de Proveedores"
            FrmSeleccion4.Lbl_Condicion = ""
            FrmSeleccion4.Show 1
            vFila = Me.lstTERCERO.ListItems.Count + 1
            vContar = 1
            On Error GoTo Tercero
            Do Until Mselec2(vContar, 1) = ""
'                vClave = "T" & Mselec2(vContar, 1)
                vclave = "T" & CStr(vFila)
                Set vUno = Me.lstTERCERO.FindItem(Mselec2(vContar, 1), lvwTag)
                If vUno Is Nothing Then
                    Me.lstTERCERO.ListItems.Add , vclave, Mselec2(vContar, 1)
                    Me.lstTERCERO.ListItems.Item(vclave).Tag = Mselec2(vContar, 1)
                    Me.lstTERCERO.ListItems.Item(vclave).ListSubItems.Add , "NOM", Mselec2(vContar, 2)
                    Me.lstTERCERO.ListItems.Item(vclave).ListSubItems.Add , "CLF", Format(CInt(Me.txtCALIFI.Text), "##0")
                    Me.lstTERCERO.ListItems.Item(vclave).ListSubItems.Add , "PRC", Format(CInt(Me.txtPRECIO.Text), "##0")
                    Me.lstTERCERO.ListItems.Item(vclave).ListSubItems.Add , "PUN", Format(vContar, "#########0")
                    Me.lstTERCERO.ListItems.Item(vclave).ListSubItems.Add , "COS", Format(0, "#########0.00")
                    Me.lstTERCERO.ListItems.Item(vclave).ListSubItems.Add , "IMP", Format(0, "#0.00")
                End If
                vContar = vContar + 1
                vFila = vFila + 1
Tercero:
            Loop
            Me.lstTERCERO.SortKey = 1
            Me.lstTERCERO.SortOrder = lvwAscending
            Me.lstTERCERO.Sorted = True
            On Error GoTo 0
        End Select
    Case Is = "DEL"
        If Me.ssSEPARA.Tab = 0 Then
            Set vCual = Me.lstARTICULO
        ElseIf Me.ssSEPARA.Tab = 1 Then
            Set vCual = Me.lstTERCERO
        End If
        For Each vUno In vCual.ListItems
            If vUno.Selected Then vCuerda = vCuerda & IIf(Len(vCuerda) = 0, "", Coma) & vUno.Key
        Next
        If Len(vCuerda) = 0 Then Exit Sub
        vCuantos = CuantosHay(vCuerda, Coma)
        For vContar = 1 To vCuantos + 1
            Call vCual.ListItems.Remove(SacarUnoXPosicion(vCuerda, Coma, vContar))
        Next
    Case Is = "CHA"
        For Each vUno In Me.lstTERCERO.ListItems
            If vUno.Selected Then
                Me.lstTERCERO.ListItems.Item(vUno.Key).ListSubItems.Item("CLF").Text = Format(CInt(Me.txtCALIFI.Text), "##0")
                Me.lstTERCERO.ListItems.Item(vUno.Key).ListSubItems.Item("PRC").Text = Format(CInt(Me.txtPRECIO.Text), "##0")
            End If
        Next
    Case Is = "CAL"
        vTamano = Len(UnTercero.Nit)
        vclave = ""
        Me.lstARTICULO.SortKey = 5
        Me.lstARTICULO.Sorted = True
        For Each vUno In Me.lstARTICULO.ListItems
            If Not vclave = vUno.ListSubItems.Item("AUT").Text Then
                vclave = vUno.ListSubItems.Item("AUT").Text
                vCuerdaArticulos = vCuerdaArticulos & IIf(Len(vCuerdaArticulos) = 0, "", Coma) & vclave
            End If
        Next
'        vCuerdaArticulos = "(" & vCuerdaArticulos & ")"
        
        For Each vUno In Me.lstTERCERO.ListItems
            vCuerdaTerceros = vCuerdaTerceros & IIf(Len(vCuerdaTerceros) = 0, "", Coma) & "'" & vUno.Tag & "'"
        Next
        vCuerdaTerceros = "(" & vCuerdaTerceros & ")"
        vCuerdaSQLCriterios = "SELECT NU_AUTO_CRIT, TX_DESC_CRIT, CD_CODI_TERC_RCRTE," & _
            " NU_AUTO_CRIT_RCRTE, NU_CALI_RCRTE, NU_PESO_RCRTE" & _
            " FROM IN_R_CRITERIO_TERCERO, IN_CRITERIO" & _
            " WHERE NU_AUTO_CRIT=NU_AUTO_CRIT_RCRTE" & _
            " AND CD_CODI_TERC_RCRTE IN " & vCuerdaTerceros

''IN_CODIGOBAR(NU_AUTO_ARTI_COBA,TX_COBA_COBA,TX_REGI_COBA,TX_CIAL_COBA,TX_LABO_COBA)
''IN_R_CODIGOBAR_TERCERO(TX_COBA_COBA_RCBTE,CD_CODI_TERC_RCBTE,NU_MULT_RCBTE,NU_DIVI_RCBTE,NU_COST_RCBTE,NU_IMPU_RCBTE)
        vCuerdaSQLArticulos = "SELECT NU_AUTO_ARTI, CD_CODI_ARTI, NO_NOMB_ARTI, TX_COBA_COBA, TX_CIAL_COBA, CD_CODI_TERC_RCBTE, NU_IMPU_RCBTE, NU_COST_RCBTE*NU_MULT_RCBTE/NU_DIVI_RCBTE AS CostoUnidad" & _
            " FROM IN_CODIGOBAR, IN_R_CODIGOBAR_TERCERO, ARTICULO" & _
            " WHERE NU_AUTO_ARTI_COBA=NU_AUTO_ARTI" & _
            " AND TX_COBA_COBA_RCBTE=TX_COBA_COBA" & _
            " AND NU_AUTO_ARTI IN (" & vCuerdaArticulos & ")" & _
            " AND CD_CODI_TERC_RCBTE IN " & vCuerdaTerceros & _
            " ORDER BY NO_NOMB_ARTI"
        Debug.Print vCuerdaSQLArticulos
        vSetCriterios.Open vCuerdaSQLCriterios, BD(BDCurCon), adOpenDynamic, adLockReadOnly
        vSetListaPrecio.Open vCuerdaSQLArticulos, BD(BDCurCon), adOpenDynamic, adLockReadOnly
'Auto, Codigo, Descripci�n, Nit, Nombre, Costo, Califica, Opci�n, Orden
        Me.grdPROYECTO.Rows = 1
        For vRecorre = 0 To CuantosHay(vCuerdaArticulos, ",")
            UnArticulo.IniciaXNumero CLng(SacarUnoXPosicion(vCuerdaArticulos, ",", vRecorre + 1))
            Me.lstCDBXPROVE.ListItems.Clear
            Me.lstCDBXPROVE.Sorted = False
            UnArticulo.INTCllCodigosDBarra
            Set CllCDBXArt = Nothing
            Set CllCDBXArt = UnArticulo.GetCllDeCDBarra
'NIT, NOM, CLF, PRC, PUN, COS, IMP lstTERCERO
'NIT, NOM, BAR, CLF, PRC, PUN, COS, IMP lstCDBXPROVE
            For Each vDos In Me.lstTERCERO.ListItems
                For Each UnCDBarra In CllCDBXArt
                    vclave = "TC" & CStr(Me.lstCDBXPROVE.ListItems.Count + 1)
                    Me.lstCDBXPROVE.ListItems.Add , vclave, vDos.Text
                    Me.lstCDBXPROVE.ListItems.Item(vclave).Tag = vDos.Text & UnCDBarra.Codigo
                    Me.lstCDBXPROVE.ListItems.Item(vclave).ListSubItems.Add , "NOM", vDos.ListSubItems("NOM").Text
                    Me.lstCDBXPROVE.ListItems.Item(vclave).ListSubItems.Add , "BAR", UnCDBarra.Codigo
                    Me.lstCDBXPROVE.ListItems.Item(vclave).ListSubItems.Add , "CLF", vDos.ListSubItems("CLF").Text
                    Me.lstCDBXPROVE.ListItems.Item(vclave).ListSubItems.Add , "PRC", vDos.ListSubItems("PRC").Text
                    Me.lstCDBXPROVE.ListItems.Item(vclave).ListSubItems.Add , "PUN", "0"
                    Me.lstCDBXPROVE.ListItems.Item(vclave).ListSubItems.Add , "COS", "0"
                    Me.lstCDBXPROVE.ListItems.Item(vclave).ListSubItems.Add , "IMP", "0"
                Next
            Next
            For Each vDos In Me.lstCDBXPROVE.ListItems
                vDos.ListSubItems.Item("PUN").Text = Format(0, "#########0")
                vTercero = Left(vDos.Text & String(vTamano, " "), vTamano)
                vSetCriterios.Filter = "CD_CODI_TERC_RCRTE='" & vTercero & "'"
                vPeso = 0: vNorma = 0
                While Not vSetCriterios.EOF
                    If vSetCriterios.Fields("NU_PESO_RCRTE").Value > 0 Then
                        vNorma = vNorma + vSetCriterios.Fields("NU_PESO_RCRTE").Value
                        vPeso = vPeso + vSetCriterios.Fields("NU_CALI_RCRTE").Value * vSetCriterios.Fields("NU_PESO_RCRTE").Value
                    End If
                    vSetCriterios.MoveNext
                Wend
                If vNorma > 0 Then vPeso = vPeso / vNorma
                If Not vPeso > 0 Then vPeso = 1
                vSetListaPrecio.Filter = "TX_COBA_COBA='" & vDos.ListSubItems.Item("BAR").Text & "' AND CD_CODI_TERC_RCBTE='" & vTercero & "'"
                vIVACompra = 0: vCosto = 0
                If Not vSetListaPrecio.EOF Then vIVACompra = vSetListaPrecio.Fields("NU_IMPU_RCBTE").Value
                vDos.ListSubItems.Item("IMP").Text = Format(vIVACompra, "#0.00")
                If Not vSetListaPrecio.EOF Then vCosto = vSetListaPrecio.Fields("CostoUnidad").Value
                vCalifica = 0
'                vCalifica = (CInt(Me.lstTERCERO.ListItems.Item(vDos.Key).ListSubItems("PRC").Text) + (vPeso * CInt(Me.lstTERCERO.ListItems.Item(vDos.Key).ListSubItems("CLF").Text) / 10))
                vCalifica = (CInt(vDos.ListSubItems("PRC").Text) + (vPeso * CInt(vDos.ListSubItems("CLF").Text) / 10))
                vDos.ListSubItems.Item("COS").Text = Format(vCosto, "#########0.00")
                If vCalifica > 0 Then
                    vDos.ListSubItems.Item("PUN").Text = Format(100 * vCosto / vCalifica, "#########0")
                Else
                    vDos.ListSubItems.Item("PUN").Text = Format(0, "#########0")
                End If
            Next
            Me.lstCDBXPROVE.SortKey = 5
            Me.lstCDBXPROVE.SortOrder = lvwAscending
            Me.lstCDBXPROVE.Sorted = True
            For Each vDos In Me.lstCDBXPROVE.ListItems
                If CLng(vDos.ListSubItems("PUN").Text) > 0 Then
                    With Me.grdPROYECTO
'Auto, Codigo, Descripci�n, CDBarra, Nit, Nombre, Costo, Califica, Opci�n, Orden, Impuesto
'vColAutoArticulo=0:vColCodigoArticulo=1:vColNombreArticulo=2:vColCDBarra=3:vColNombreComercial=4:vColNitTercero=5
'vColNombreTercero=6:vColCosto=7:vColCalifica=8:vColOpcion=9:vColImpuesto=10:vColOrden=11
                    vFila = .Rows
                    .Rows = .Rows + 1
                    For Each vUno In Me.lstARTICULO.ListItems
                        If vUno.ListSubItems.Item("AUT").Text = CStr(UnArticulo.Numero) And vUno.ListSubItems.Item("BAR").Text = vDos.ListSubItems.Item("BAR").Text Then Exit For
                    Next
                    .TextMatrix(vFila, vColAutoArticulo) = vUno.ListSubItems("AUT").Text
                    .TextMatrix(vFila, vColCodigoArticulo) = vUno.Text
                    .TextMatrix(vFila, vColNombreArticulo) = vUno.ListSubItems("NOM").Text
                    .TextMatrix(vFila, vColCDBarra) = vUno.ListSubItems("BAR").Text
                    .TextMatrix(vFila, vColNombreComercial) = vUno.ListSubItems("CIA").Text
                    .TextMatrix(vFila, vColNitTercero) = vDos.Text
                    .TextMatrix(vFila, vColNombreTercero) = vDos.ListSubItems("NOM").Text
                    .TextMatrix(vFila, vColCosto) = Format(vDos.ListSubItems("COS").Text, "#,##0.00")
                    .TextMatrix(vFila, vColCalifica) = Format(vDos.ListSubItems("PUN").Text, "#,##0")
                    .TextMatrix(vFila, vColImpuesto) = vDos.ListSubItems("IMP").Text
                    .TextMatrix(vFila, vColOrden) = CStr(.Rows)
                    End With
                End If
            Next
            For Each vDos In Me.lstCDBXPROVE.ListItems
                If CLng(vDos.ListSubItems("PUN").Text) = 0 Then
                    With Me.grdPROYECTO
                    vFila = .Rows
                    .Rows = .Rows + 1
                    For Each vUno In Me.lstARTICULO.ListItems
                        If vUno.ListSubItems.Item("AUT").Text = CStr(UnArticulo.Numero) And vUno.ListSubItems.Item("BAR").Text = vDos.ListSubItems.Item("BAR").Text Then Exit For
                    Next
                    .TextMatrix(vFila, vColAutoArticulo) = vUno.ListSubItems("AUT").Text
                    .TextMatrix(vFila, vColCodigoArticulo) = vUno.Text
                    .TextMatrix(vFila, vColNombreArticulo) = vUno.ListSubItems("NOM").Text
                    .TextMatrix(vFila, vColCDBarra) = vUno.ListSubItems("BAR").Text
                    .TextMatrix(vFila, vColNombreComercial) = vUno.ListSubItems("CIA").Text
                    .TextMatrix(vFila, vColNitTercero) = vDos.Text
                    .TextMatrix(vFila, vColNombreTercero) = vDos.ListSubItems("NOM").Text
                    .TextMatrix(vFila, vColCosto) = Format(vDos.ListSubItems("COS").Text, "#,##0.00")
                    .TextMatrix(vFila, vColCalifica) = Format(vDos.ListSubItems("PUN").Text, "#,##0")
                    .TextMatrix(vFila, vColImpuesto) = vDos.ListSubItems("IMP").Text
                    .TextMatrix(vFila, vColOrden) = CStr(.Rows)
                    End With
                End If
            Next
        Next
        vArticulo = ""
        With Me.grdPROYECTO
        For vContar = 1 To .Rows - 1
            If Not vArticulo = .TextMatrix(vContar, vColCodigoArticulo) Then vOpcion = 0: vArticulo = .TextMatrix(vContar, vColCodigoArticulo)
            If vArticulo = .TextMatrix(vContar, vColCodigoArticulo) Then vOpcion = vOpcion + 1
            .TextMatrix(vContar, vColOpcion) = CStr(vOpcion)
        Next
        End With
        vSetListaPrecio.Close
        vSetCriterios.Close
        Me.lstTERCERO.SortKey = 1
        Me.lstTERCERO.SortOrder = lvwAscending
        Me.grdPROYECTO.Refresh
    Case Is = "EXP"
        Call DescargaOrden
    Case Is = "SAV"
'IN_PROYETOCOMPRA (NU_AUTO_PRYC,FE_CREA_PRYC,TX_DESC_PRYC)
'Auto, Codigo, Descripci�n, Nit, Nombre, Costo, Califica, Opci�n, Orden
'IN_ARTICULOSELECCION (NU_AUTO_PROY_ARSE,NU_AUTO_ARTI_ARSE,CD_CODI_TERC_ARSE,NU_CANT_ARSE,NU_COST_ARSE,NU_CALI_ARSE,NU_ORDE_ARSE)
'COD, NOM, CAN, COS, SUB lstORDEN
'COD, NOM, UND, AUT lstARTICULO
'NIT, NOM, CLF, PRC, PUN lstTERCERO
'IN_PROVEEDORPROYECTO (NU_AUTO_PROY_PROPY,CD_CODI_TERC_PROPY,NU_CALI_PROPY,NU_PORC_PROPY)
        If Not Me.grdPROYECTO.Rows > 1 Then Exit Sub
        Valores = ""
        Valores = Valores & "FE_CREA_PRYC='" & FechaServer & "'"
        Valores = Valores & ", TX_DESC_PRYC='" & Cambiar_Comas_Comillas("Proyecto de verificaci�n") & "'"
        Result = DoInsertSQL("IN_PROYETOCOMPRA", Valores)
        ReDim ArrPRYC(0)
        Result = LoadData("IN_PROYETOCOMPRA", "MAX(NU_AUTO_PRYC)", "", ArrPRYC())
        If Len(ArrPRYC(0)) = 0 Then ArrPRYC(0) = "0"
        vAsignado = CLng(ArrPRYC(0))
        Me.txtNUPROY.Text = CStr(ArrPRYC(0))
        For Each vUno In Me.lstTERCERO.ListItems
            Valores = "NU_AUTO_PROY_PROPY=" & vAsignado
            Valores = Valores & ", CD_CODI_TERC_PROPY='" & vUno.Text & "'"
            Valores = Valores & ", NU_CALI_PROPY=" & vUno.ListSubItems.Item("CLF").Text
            Valores = Valores & ", NU_PORC_PROPY=" & vUno.ListSubItems.Item("PRC").Text
            Debug.Print Valores
            Result = DoInsertSQL("IN_PROVEEDORPROYECTO", Valores)
        Next
        With Me.grdPROYECTO
''IN_CDBARSELE(NU_AUTO_PROY_CDBSE,TX_COBA_COBA_CDBSE,CD_CODI_TERC_CDBSE,NU_OPCI_CDBSE,NU_COST_CDBSE,NU_CALI_CDBSE,NU_ORDE_CDBSE,NU_IMPU_CDBSE)
        For vFila = 1 To .Rows - 1
            Valores = "NU_AUTO_PROY_CDBSE=" & vAsignado
            Valores = Valores & ", TX_COBA_COBA_CDBSE='" & .TextMatrix(vFila, vColCDBarra) & "'"
            Valores = Valores & ", CD_CODI_TERC_CDBSE='" & .TextMatrix(vFila, vColNitTercero) & "'"
            Valores = Valores & ", NU_OPCI_CDBSE=" & CLng(.TextMatrix(vFila, vColOpcion))
            Valores = Valores & ", NU_COST_CDBSE=" & CSng(.TextMatrix(vFila, vColCosto))
            Valores = Valores & ", NU_IMPU_CDBSE=" & CSng(.TextMatrix(vFila, vColImpuesto))
            Valores = Valores & ", NU_CALI_CDBSE=" & CLng(.TextMatrix(vFila, vColCalifica))
            Valores = Valores & ", NU_ORDE_CDBSE=" & CInt(.TextMatrix(vFila, vColOrden))
            Debug.Print Valores
            Result = DoInsertSQL("IN_CDBARSELE", Valores)
        Next
        End With
    Case Is = "LOA"
        Me.lstARTICULO.ListItems.Clear
        Me.lstTERCERO.ListItems.Clear
        If Len(Me.txtNUPROY.Text) = 0 Then Me.txtNUPROY.Text = "0"
        vAsignado = CLng(Me.txtNUPROY.Text)
'        Campos = "NU_AUTO_PROY_ARSE,NU_AUTO_ARTI_ARSE,CD_CODI_TERC_ARSE,NU_CANT_ARSE,NU_COST_ARSE,NU_CALI_ARSE,NU_ORDE_ARSE"
''IN_CODIGOBAR(NU_AUTO_ARTI_COBA,TX_COBA_COBA,TX_REGI_COBA,TX_CIAL_COBA,TX_LABO_COBA)
''IN_CDBARSELE(NU_AUTO_PROY_CDBSE,TX_COBA_COBA_CDBSE,CD_CODI_TERC_CDBSE,NU_OPCI_CDBSE,NU_COST_CDBSE,NU_CALI_CDBSE,NU_ORDE_CDBSE,NU_IMPU_CDBSE)
        Campos = "NU_AUTO_PROY_CDBSE,TX_COBA_COBA_CDBSE,CD_CODI_TERC_CDBSE,NU_OPCI_CDBSE,NU_COST_CDBSE,NU_CALI_CDBSE,NU_ORDE_CDBSE,NU_IMPU_CDBSE"
        Campos = Campos & ",NU_AUTO_ARTI_COBA,TX_COBA_COBA,TX_REGI_COBA,TX_CIAL_COBA,TX_LABO_COBA"
        Condi = "TX_COBA_COBA_CDBSE=TX_COBA_COBA"
        Condi = Condi & " AND NU_AUTO_PROY_CDBSE=" & vAsignado
        Condi = Condi & " ORDER BY NU_ORDE_CDBSE"
        ReDim ArrPRYC(12, 0)
        Result = LoadMulData("IN_CDBARSELE,IN_CODIGOBAR", Campos, Condi, ArrPRYC())
        Me.grdPROYECTO.Rows = 1
        If Result = FAIL Then Exit Sub
        If Encontro Then
            vAsignado = 0
            With Me.grdPROYECTO
            For vFila = 0 To UBound(ArrPRYC, 2)
                .Rows = .Rows + 1
                .Row = .Rows - 1
                If Len(ArrPRYC(3, vFila)) = 0 Then ArrPRYC(3, vFila) = "0"
                If Len(ArrPRYC(4, vFila)) = 0 Then ArrPRYC(4, vFila) = "0"
                If Len(ArrPRYC(5, vFila)) = 0 Then ArrPRYC(5, vFila) = "0"
                If Len(ArrPRYC(6, vFila)) = 0 Then ArrPRYC(6, vFila) = "0"
                If Len(ArrPRYC(7, vFila)) = 0 Then ArrPRYC(7, vFila) = "0"
                If Not vAsignado = CLng(ArrPRYC(8, vFila)) Then
                    vAsignado = CLng(ArrPRYC(8, vFila))
                    UnArticulo.IniciaXNumero vAsignado
                End If
                UnTercero.IniXNit CStr(ArrPRYC(2, vFila))
                Set vUno = Me.lstARTICULO.FindItem(CStr(ArrPRYC(1, vFila)), lvwTag)
                If vUno Is Nothing Then
                    UnCDBarra.IniciaXCodigo CStr(ArrPRYC(1, vFila))
                    vclave = "B" & vFila
                    Me.lstARTICULO.ListItems.Add , vclave, Trim(UnArticulo.Codigo)
                    Me.lstARTICULO.ListItems.Item(vclave).Tag = Trim(UnCDBarra.Codigo)
                    Me.lstARTICULO.ListItems.Item(vclave).ListSubItems.Add , "BAR", Trim(UnCDBarra.Codigo)
                    Me.lstARTICULO.ListItems.Item(vclave).ListSubItems.Add , "NOM", Trim(UnArticulo.Nombre)
                    Me.lstARTICULO.ListItems.Item(vclave).ListSubItems.Add , "CIA", Trim(UnCDBarra.Comercial)
                    Me.lstARTICULO.ListItems.Item(vclave).ListSubItems.Add , "UND", Trim(UnArticulo.UnidadConteo)
                    Me.lstARTICULO.ListItems.Item(vclave).ListSubItems.Add , "AUT", Trim(UnArticulo.Numero)
                End If
'vColAutoArticulo=0:vColCodigoArticulo=1:vNombreArticulo=2:vColCDBarra=3
'vColNombreComercial=4:vNitTercero=5:vColNombreTercero=6:vColCosto=7:vColCalifica=8
'vColOpcion=9:vColImpuesto=10:vColOrden=11
                .TextMatrix(.Row, vColAutoArticulo) = CStr(vAsignado)
                .TextMatrix(.Row, vColCodigoArticulo) = Trim(UnArticulo.Codigo)
                .TextMatrix(.Row, vColNombreArticulo) = Trim(UnArticulo.Nombre)
                .TextMatrix(.Row, vColCDBarra) = ArrPRYC(1, vFila)
                .TextMatrix(.Row, vColNombreComercial) = ArrPRYC(11, vFila)
                .TextMatrix(.Row, vColNitTercero) = ArrPRYC(2, vFila)
                .TextMatrix(.Row, vColNombreTercero) = Trim(UnTercero.Nombre)
                .TextMatrix(.Row, vColCosto) = Format(CSng(ArrPRYC(4, vFila)), "#,###.00")
                .TextMatrix(.Row, vColCalifica) = Format(CLng(ArrPRYC(5, vFila)), "#,##0")
                .TextMatrix(.Row, vColImpuesto) = Format(CSng(ArrPRYC(7, vFila)), "#0.00")
                .TextMatrix(.Row, vColOrden) = Format(CInt(ArrPRYC(6, vFila)), "#####")
                .TextMatrix(.Row, vColOpcion) = ArrPRYC(3, vFila)
            Next
            End With
        End If
'IN_PROVEEDORPROYECTO (NU_AUTO_PROY_PROPY,CD_CODI_TERC_PROPY,NU_CALI_PROPY,NU_PORC_PROPY)
        vAsignado = CLng(Me.txtNUPROY.Text)
        Campos = "NU_AUTO_PROY_PROPY,CD_CODI_TERC_PROPY,NU_CALI_PROPY,NU_PORC_PROPY"
        Condi = "NU_AUTO_PROY_PROPY=" & vAsignado
        ReDim ArrPRYC(3, 0)
        Result = LoadMulData("IN_PROVEEDORPROYECTO", Campos, Condi, ArrPRYC())
        If Result = FAIL Then Exit Sub
        If Encontro Then
            For vFila = 0 To UBound(ArrPRYC, 2)
'                vClave = "T" & CStr(ArrPRYC(1, vFila))
                vclave = "T" & CStr(vFila)
                UnTercero.IniXNit CStr(ArrPRYC(1, vFila))
                If Len(ArrPRYC(2, vFila)) = 0 Then ArrPRYC(2, vFila) = "0"
                If Len(ArrPRYC(3, vFila)) = 0 Then ArrPRYC(3, vFila) = "0"
                Set vUno = Me.lstTERCERO.FindItem(UnTercero.Nit, lvwTag)
                If vUno Is Nothing Then
'NIT, NOM, CLF, PRC, PUN lstTERCERO
                    Me.lstTERCERO.ListItems.Add , vclave, CStr(ArrPRYC(1, vFila))
                    Me.lstTERCERO.ListItems.Item(vclave).Tag = CStr(ArrPRYC(1, vFila))
                    Me.lstTERCERO.ListItems.Item(vclave).ListSubItems.Add , "NOM", Trim(UnTercero.Nombre)
                    Me.lstTERCERO.ListItems.Item(vclave).ListSubItems.Add , "CLF", Format(CInt(ArrPRYC(2, vFila)), "##0")
                    Me.lstTERCERO.ListItems.Item(vclave).ListSubItems.Add , "PRC", Format(CInt(ArrPRYC(3, vFila)), "##0")
                    Me.lstTERCERO.ListItems.Item(vclave).ListSubItems.Add , "PUN", "0"
                End If
            Next
        End If
        Me.grdPROYECTO.Refresh
    End Select
End Sub

Private Sub lstARTICULO_ColumnClick(ByVal ColumnHeader As MSComctlLib.ColumnHeader)
    Me.lstARTICULO.SortKey = ColumnHeader.Index - 1
    Me.lstARTICULO.Sorted = True
End Sub

Private Sub lstTERCERO_ColumnClick(ByVal ColumnHeader As MSComctlLib.ColumnHeader)
    Me.lstTERCERO.SortKey = ColumnHeader.Index - 1
    Me.lstTERCERO.Sorted = True
End Sub

Private Sub txtCALIFI_LostFocus()
    If Len(Me.txtCALIFI.Text) = 0 Then Me.txtCALIFI.Text = "0"
    If CLng(Me.txtCALIFI.Text) < 0 Or CLng(Me.txtCALIFI.Text) > 100 Then Me.txtCALIFI.SetFocus
End Sub

Private Sub txtCUALOP_LostFocus()
    Call updCUALOP_LostFocus
End Sub

Private Sub txtPRECIO_LostFocus()
    If Len(Me.txtPRECIO.Text) = 0 Then Me.txtPRECIO.Text = "0"
    If CLng(Me.txtPRECIO.Text) < 0 Or CLng(Me.txtPRECIO.Text) > 100 Then Me.txtPRECIO.SetFocus
End Sub

Private Sub NormaCalificaPrecio()
    If Len(Me.txtCALIFI.Text) = 0 Then Me.txtCALIFI.Text = "0"
    If Len(Me.txtPRECIO.Text) = 0 Then Me.txtPRECIO.Text = "0"
    If CInt(Me.txtCALIFI.Text) = 0 And CInt(Me.txtPRECIO.Text) = 0 Then
        Me.txtPRECIO.Text = "100"
    ElseIf CInt(Me.txtCALIFI.Text) > 0 And CInt(Me.txtPRECIO.Text) = 0 Then
        Me.txtPRECIO.Text = CStr(100 - CInt(Me.txtCALIFI.Text))
    ElseIf CInt(Me.txtCALIFI.Text) = 0 And CInt(Me.txtPRECIO.Text) > 0 Then
        Me.txtCALIFI.Text = CStr(100 - CInt(Me.txtPRECIO.Text))
    ElseIf CInt(Me.txtCALIFI.Text) > 0 And CInt(Me.txtPRECIO.Text) > 0 Then
        Me.txtCALIFI.Text = CStr(100 * CInt(Me.txtCALIFI.Text) / (CInt(Me.txtCALIFI.Text) + CInt(Me.txtPRECIO.Text)))
        Me.txtPRECIO.Text = CStr(100 * CInt(Me.txtPRECIO.Text) / (CInt(Me.txtCALIFI.Text) + CInt(Me.txtPRECIO.Text)))
    End If
End Sub

Private Sub DescargaOrden()
'    Dim Linea As String, Estoy As String, Faltan As String, elOrden As String
    Dim Linea As String     'DEPURACION DE CODIGO
'    Dim Procesados As Integer, ColEnGrilla As Integer, ColActual As Integer
'    Dim Procesados As Integer, ColEnGrilla As Integer, ColActual As Integer
    Dim vUno As MSComctlLib.ListItem
'    Dim Errores As Integer     'DEPURACION DE CODIGO
    Dim TOTAL As Integer
    Bandera = 0
'    PrgPlano.Value = 0
    If MDI_Inventarios.CMDialog1.InitDir = NUL$ Then MDI_Inventarios.CMDialog1.InitDir = App.Path
    MDI_Inventarios.CMDialog1.Filter = "(TODOS)|*"
    MDI_Inventarios.CMDialog1.Action = 2
'    BandErr = False
    If MDI_Inventarios.CMDialog1.filename <> "" Then
        On Error GoTo control
        Open MDI_Inventarios.CMDialog1.filename For Output As #1
        BandProcLote = 1
'        PrgPlano.Max = Me.grdPROYECTO.Rows
        TOTAL = Me.grdPROYECTO.Rows - 1
'        PrgPlano.Value = 0
'        PrgPlano.Min = 0
'        BandStop = False
        
        For Each vUno In Me.lstORDEN.ListItems
            If Len(vUno.ListSubItems.Item("OPC").Text) = 0 Then vUno.ListSubItems.Item("OPC").Text = "0"
            If CInt(Replace(vUno.ListSubItems.Item("OPC").Text, ",", "")) > 0 Then
                Linea = "" 'COD, NOM, CAN, COS, SUB
                Linea = Linea & vUno.Text
                Linea = Linea & "," & Replace(vUno.ListSubItems.Item("OPC").Text, ",", "")
                Linea = Linea & "," & Replace(vUno.ListSubItems.Item("COS").Text, ",", "")
                Debug.Print Linea
                Print #1, Linea
            End If
'            Lbl_Procesados.Caption = PrgPlano.Value + 1 & " de " & TOTAL
'            Lbl_Procesados.Refresh
'            PrgPlano.Value = PrgPlano.Value + 1
OtrReg:
        Next
        
        Close (1)
        BandProcLote = 0
'        PrgPlano.Value = 0
    End If
    Exit Sub
control:
    If ERR.Number <> 0 Then
       Call ConvertErr
       Close (1)
    End If
    Call MouseNorm
'    PrgPlano.Value = 0
End Sub

Private Sub updCUALOP_LostFocus()
    Me.lstOPCION.ListItems.Clear
    With Me.grdPROYECTO
    For vContar = 1 To .Rows - 1
'        If .TextMatrix(vContar, vColOpcion) = Me.updCUALOP.Value Then
        If .TextMatrix(vContar, vColOpcion) = Me.txtCUALOP.Text Then
            vclave = "O" & CStr(vContar)
'vColAutoArticulo=0:vColCodigoArticulo=1:vColNombreArticulo=2:vColCDBarra=3:vColNombreComercial=4:vColNitTercero=5
'vColNombreTercero=6:vColCosto=7:vColCalifica=8:vColOpcion=9:vColImpuesto=10:vColOrden=11
            Me.lstOPCION.ListItems.Add , vclave, .TextMatrix(vContar, vColCodigoArticulo)
            Me.lstOPCION.ListItems.Item(vclave).ListSubItems.Add , "NOM", .TextMatrix(vContar, vColNombreArticulo)
            Me.lstOPCION.ListItems.Item(vclave).ListSubItems.Add , "BAR", .TextMatrix(vContar, vColCDBarra)
            Me.lstOPCION.ListItems.Item(vclave).ListSubItems.Add , "CIA", .TextMatrix(vContar, vColNombreComercial)
            Me.lstOPCION.ListItems.Item(vclave).ListSubItems.Add , "PRO", .TextMatrix(vContar, vColNombreTercero)
            Me.lstOPCION.ListItems.Item(vclave).ListSubItems.Add , "OPC", .TextMatrix(vContar, vColOpcion)
            Me.lstOPCION.ListItems.Item(vclave).ListSubItems.Add , "COS", .TextMatrix(vContar, vColCosto)
            Me.lstOPCION.ListItems.Item(vclave).ListSubItems.Add , "IMP", .TextMatrix(vContar, vColImpuesto)
        End If
    Next
    End With
End Sub
