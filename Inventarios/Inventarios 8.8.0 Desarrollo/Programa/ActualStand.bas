Attribute VB_Name = "ActualStand"
Option Explicit
Dim VersionAct As Double
Dim CambiarResol As Boolean
Global Desactualizado As Boolean
Global flagNoMsg As Boolean
Dim SQL As String

'---------------------------------------------------------------------------------------
' Procedure : Actualizar_1_0
' DateTime  : 08/02/2016 12:22
' Author    : juan_urrego
' Purpose   : T32800-R31099 Metodo que crea o actualiza las tablas y la nueva versi�n de la BD
'---------------------------------------------------------------------------------------
'
Sub Actualizar_8_1()
   'JAUM T33493-R32409 Inicio
   Dim VrArr() As Variant 'Almacena informaci�n consultada
   Dim InI, InJ As Integer 'Contador
   Dim StOrdenador As String 'consulta si existe un ordenador con codigo cero
   Dim StSql As String 'JAUM T34597 Almacena consulta sql
   Result = SUCCEED
   'JAUM T33493-R32409 Fin
   'JAUM T32800-R31099 Inicio
   If Not ExisteCAMPO("ENTIDAD", "NU_MOVNIIF_ENTI") Then
      If Result <> FAIL Then Result = CrearCampo("ENTIDAD", "NU_MOVNIIF_ENTI", "1", "1", 0)
      'Result = DoUpdate("ENTIDAD", "NU_MOVNIIF_ENTI = 1", NUL$) JAUM T33276 Se deja linea en comentario
      If Result <> FAIL Then Result = DoUpdate("ENTIDAD", "NU_MOVNIIF_ENTI = 1", NUL$) 'JAUM T33276
   End If
   'JAUM T33493-R32409 Inicio
   ReDim VrArr(0)
   Campos = "TYP.name AS TIPO"
   Desde = "dbo.SYSCOLUMNS COL INNER JOIN dbo.SYSOBJECTS OBJ ON OBJ.id = COL.id"
   Desde = Desde & " INNER JOIN dbo.SYSTYPES TYP ON TYP.xusertype = COL.xtype"
   Desde = Desde & " LEFT JOIN dbo.SYSFOREIGNKEYS FK ON FK.fkey = COL.colid AND FK.fkeyid=OBJ.id"
   Desde = Desde & " LEFT JOIN dbo.SYSOBJECTS OBJ2 ON OBJ2.id = FK.rkeyid"
   Desde = Desde & " LEFT JOIN dbo.SYSCOLUMNS COL2 ON COL2.colid = FK.rkey AND COL2.id = OBJ2.id"
   Condicion = "OBJ.name = 'ORDENADOR' AND (OBJ.xtype='U' OR OBJ.xtype='V')"
   Condicion = Condicion & " AND COL.name = 'CD_CODI_ORDE' ORDER BY COL.colid"
   If Result <> FAIL Then Result = LoadData(Desde, Campos, Condicion, VrArr)
   If Result <> FAIL And Encontro Then
      If VrArr(0) = "int" Then
         ReDim VrArr(10, 0)
         Campos = "FK_TABLA=FK.TABLE_NAME,PK_TABLA=PK.TABLE_NAME, PK_COL=PT.COLUMN_NAME, "
         Campos = Campos & "DROP_IND=CASE WHEN PT4.IND IS NOT NULL THEN 'DROP INDEX ' + PT4.IND + ' ON ' + FK.TABLE_NAME END, "
         Campos = Campos & "DROP_EST=CASE WHEN PT3.EST IS NOT NULL THEN 'DROP STATISTICS ' + FK.TABLE_NAME  + '.' + PT3.EST END, "
         Campos = Campos & "DROP_CT=CASE WHEN PT2.CT IS NOT NULL THEN 'ALTER TABLE ' + FK.TABLE_NAME  + ' DROP CONSTRAINT ' + PT2.CT END, "
         Campos = Campos & "ALTER_ORD= 'UPDATE ' + FK.TABLE_NAME  + ' SET ' + CU.COLUMN_NAME + ' = NULL WHERE ' + CU.COLUMN_NAME + ' = 0', "
         Campos = Campos & "DROP_REL=CASE WHEN C.CONSTRAINT_NAME IS NOT NULL THEN 'ALTER TABLE ' + FK.TABLE_NAME  + ' DROP CONSTRAINT ' + C.CONSTRAINT_NAME END, "
         Campos = Campos & "ALT_CAMP='ALTER TABLE ' + FK.TABLE_NAME + ' ALTER COLUMN ' + CU.COLUMN_NAME + ' VARCHAR(20)', "
         Campos = Campos & "CREA_IND=CASE WHEN PT4.IND IS NOT NULL THEN 'CREATE INDEX ' + PT4.IND  + ' ON ' + FK.TABLE_NAME + ' (' + CU.COLUMN_NAME + ')' END, "
         Campos = Campos & "CREA_REL=CASE WHEN C.CONSTRAINT_NAME IS NOT NULL THEN 'ALTER TABLE ' + FK.TABLE_NAME  + ' ADD CONSTRAINT FK_' + FK.TABLE_NAME + PK.TABLE_NAME + ' FOREIGN KEY (' + CU.COLUMN_NAME + ') REFERENCES ORDENADOR (CD_CODI_ORDE)'  END "
         Desde = "INFORMATION_SCHEMA.REFERENTIAL_CONSTRAINTS C "
         Desde = Desde & "INNER JOIN INFORMATION_SCHEMA.TABLE_CONSTRAINTS FK ON C.CONSTRAINT_NAME = FK.CONSTRAINT_NAME "
         Desde = Desde & "INNER JOIN INFORMATION_SCHEMA.TABLE_CONSTRAINTS PK ON C.UNIQUE_CONSTRAINT_NAME = PK.CONSTRAINT_NAME "
         Desde = Desde & "INNER JOIN INFORMATION_SCHEMA.KEY_COLUMN_USAGE CU ON C.CONSTRAINT_NAME = CU.CONSTRAINT_NAME "
         Desde = Desde & "INNER JOIN (SELECT TC.TABLE_NAME, CU.COLUMN_NAME "
         Desde = Desde & "FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS TC "
         Desde = Desde & "INNER JOIN INFORMATION_SCHEMA.KEY_COLUMN_USAGE CU ON TC.CONSTRAINT_NAME = CU.CONSTRAINT_NAME "
         Condicion = "TC.CONSTRAINT_TYPE = 'PRIMARY KEY') PT ON PT.TABLE_NAME = PK.TABLE_NAME "
         Condicion = Condicion & "LEFT JOIN (SELECT COL=CL.name,CT=CS.name "
         Condicion = Condicion & "FROM sys.all_columns CL INNER JOIN sys.tables TB ON CL.object_id = TB.object_id "
         Condicion = Condicion & "INNER JOIN sys.schemas SC ON TB.schema_id = SC.schema_id "
         Condicion = Condicion & "INNER JOIN sys.default_constraints CS ON CL.default_object_id = CS.object_id) PT2 ON CU.COLUMN_NAME=PT2.COL "
         Condicion = Condicion & "LEFT JOIN (SELECT COL=SS.name,EST=COL_NAME(SC.object_id, SC.column_id) "
         Condicion = Condicion & "FROM sys.stats SS INNER JOIN sys.stats_columns SC ON SS.stats_id=SC.stats_id "
         Condicion = Condicion & "AND SS.object_id=SC.object_id WHERE SS.name like '_dta%') PT3 ON CU.COLUMN_NAME=PT3.COL "
         Condicion = Condicion & "LEFT JOIN (SELECT COL=SC.name,IND=SI.name FROM sys.columns SC "
         Condicion = Condicion & "INNER JOIN sys.indexes SI ON SC.object_id=SI.object_id "
         Condicion = Condicion & "INNER JOIN sys.index_columns SIC ON SC.object_id=SIC.object_id "
         Condicion = Condicion & "AND SC.column_id=SIC.column_id AND SI.index_id=SIC.index_id) PT4 ON CU.COLUMN_NAME=PT4.COL "
         Condicion = Condicion & "WHERE PK.TABLE_NAME = 'ORDENADOR' "
         Condicion = Condicion & "UNION SELECT FK_TABLA=CU.TABLE_NAME, PK_TABLA='ORDENADOR', PK_COL='CD_CODI_ORDE', "
         Condicion = Condicion & "DROP_IND=CASE WHEN PT4.IND IS NOT NULL THEN 'DROP INDEX ' + PT4.IND + ' ON ' + CU.TABLE_NAME END, "
         Condicion = Condicion & "DROP_EST=CASE WHEN PT3.EST IS NOT NULL THEN 'DROP STATISTICS ' + CU.TABLE_NAME  + '.' + PT3.EST END, "
         Condicion = Condicion & "DROP_CT=CASE WHEN PT2.CT IS NOT NULL THEN 'ALTER TABLE ' + CU.TABLE_NAME  + ' DROP CONSTRAINT ' + PT2.CT END, "
         Condicion = Condicion & "ALTER_ORD = 'UPDATE ' + CU.TABLE_NAME  + ' SET ' + CU.COLUMN_NAME + ' = NULL WHERE ' + CU.COLUMN_NAME + ' = 0', "
         Condicion = Condicion & "DROP_REL = '', ALT_CAMP='ALTER TABLE ' + CU.TABLE_NAME + ' ALTER COLUMN ' + CU.COLUMN_NAME + ' VARCHAR(20)', "
         Condicion = Condicion & "CASE WHEN PT4.IND IS NOT NULL THEN 'CREATE INDEX ' + PT4.IND  + ' ON ' + CU.TABLE_NAME END + ' (' + CU.COLUMN_NAME + ')',"
         Condicion = Condicion & "CREA_REL='ALTER TABLE ' + CU.TABLE_NAME  + ' ADD CONSTRAINT FK_' + CU.TABLE_NAME + 'ORDENADOR FOREIGN KEY (' + CU.COLUMN_NAME + ') REFERENCES ORDENADOR (CD_CODI_ORDE)' "
         Condicion = Condicion & "FROM INFORMATION_SCHEMA.COLUMNS CU LEFT JOIN (SELECT COL=CL.name,CT=CS.name "
         Condicion = Condicion & "FROM sys.all_columns CL INNER JOIN sys.tables TB ON CL.object_id = TB.object_id "
         Condicion = Condicion & "INNER JOIN sys.schemas SC ON TB.schema_id = SC.schema_id "
         Condicion = Condicion & "INNER JOIN sys.default_constraints CS ON CL.default_object_id = CS.object_id) PT2 ON CU.COLUMN_NAME=PT2.COL "
         Condicion = Condicion & "LEFT JOIN (SELECT COL=SS.name,EST=COL_NAME(SC.object_id, SC.column_id) "
         Condicion = Condicion & "FROM sys.stats SS INNER JOIN sys.stats_columns SC ON SS.stats_id=SC.stats_id "
         Condicion = Condicion & "AND SS.object_id=SC.object_id WHERE SS.name like '_dta%') PT3 ON CU.COLUMN_NAME=PT3.COL "
         Condicion = Condicion & "LEFT JOIN (SELECT COL=SC.name,IND=SI.name FROM sys.columns SC "
         Condicion = Condicion & "INNER JOIN sys.indexes SI ON SC.object_id=SI.object_id "
         Condicion = Condicion & "INNER JOIN sys.index_columns SIC ON SC.object_id=SIC.object_id "
         Condicion = Condicion & "AND SC.column_id=SIC.column_id "
         Condicion = Condicion & "AND SI.index_id=SIC.index_id) PT4 ON CU.COLUMN_NAME=PT4.COL "
         Condicion = Condicion & "WHERE (COLUMN_NAME LIKE '%ORDE%' OR COLUMN_NAME LIKE 'CD_COOR_CTRL') "
         Condicion = Condicion & "AND (DATA_TYPE = 'int' OR DATA_TYPE = 'float') "
         Condicion = Condicion & "AND TABLE_NAME IN ('DOC_ANULADOS', 'GIROING','ORDENADOR', 'PARAMETROS_CO', "
         Condicion = Condicion & "'PARAMETROS_CXC', 'PARAMETROS_CXP', 'PARAMETROS_INVE','PARAMETROS_NOMI', "
         Condicion = Condicion & "'REGISTRO_VF', 'SOLICITA_VF', 'TC_CONTROL') ORDER BY 1"
         Result = LoadMulData(Desde, Campos, Condicion, VrArr)
         If Result <> FAIL And Encontro Then
            For InJ = 3 To 10
               If InJ = 6 Then
                  StOrdenador = fnDevDato("ORDENADOR", "CD_CODI_ORDE", "CD_CODI_ORDE = 0", True)
                  If StOrdenador <> NUL$ Then GoTo Siguiente
               End If
               If InJ = 8 And Result <> FAIL Then
                  'JAUM T33919 Inicio Se deja linea en cometario
                  'Valores = Vrarr(5, 12)
                  For InI = 0 To UBound(VrArr, 2)
                     If VrArr(0, InI) = "ORDENADOR" Then
                        Valores = VrArr(5, InI)
                        Exit For
                     End If
                  Next
                  'JAUM T33919 Fin
                  Result = ExecSQLCommand(Valores)
                  Valores = "ALTER TABLE ORDENADOR DROP CONSTRAINT PKORDENADOR"
                  If Result <> FAIL Then Result = ExecSQLCommand(Valores)
                  Valores = "ALTER TABLE ORDENADOR ALTER COLUMN CD_CODI_ORDE VARCHAR(20) NOT NULL"
                  If Result <> FAIL Then Result = ExecSQLCommand(Valores)
                  StOrdenador = fnDevDato("PARAMETROS_NOMI", "CD_COOR_CTRL", NUL$, True)
                  If StOrdenador <> NUL$ Then StOrdenador = Format(StOrdenador, "##0") 'JAUM T34375
               End If
               If InJ = 10 And Result <> FAIL Then
                  Valores = "ALTER TABLE ORDENADOR ADD CONSTRAINT PKORDENADOR PRIMARY KEY (CD_CODI_ORDE)"
                  Result = ExecSQLCommand(Valores)
                  Valores = "CD_COOR_CTRL = '" & StOrdenador & "'"
                  If StOrdenador <> NUL$ And Result <> FAIL Then Result = DoUpdate("PARAMETROS_NOMI", Valores, NUL$)
               End If
               For InI = 0 To UBound(VrArr, 2)
                  'If InI = 12 Then InI = InI + 1 JAUM T33919 Se deja linea en cometario
                  If VrArr(0, InI) = "ORDENADOR" Then InI = InI + 1 'JAUM T33919
                  If VrArr(InJ, InI) <> NUL$ Then
                     If Result <> FAIL Then Result = ExecSQLCommand(VrArr(InJ, InI))
                  End If
               Next
Siguiente:
            Next
         End If
      End If
   End If
   'JAUM T33493-R32409 Fin
   'JAUM T32800-R31099 Fin
   'JAUM T34597 Inicio
   ReDim VrArr(0)
   Campos = "TYP.name AS TIPO"
   Desde = "dbo.SYSCOLUMNS COL INNER JOIN dbo.SYSOBJECTS OBJ ON OBJ.id = COL.id"
   Desde = Desde & " INNER JOIN dbo.SYSTYPES TYP ON TYP.xusertype = COL.xtype"
   Desde = Desde & " LEFT JOIN dbo.SYSFOREIGNKEYS FK ON FK.fkey = COL.colid AND FK.fkeyid=OBJ.id"
   Desde = Desde & " LEFT JOIN dbo.SYSOBJECTS OBJ2 ON OBJ2.id = FK.rkeyid"
   Desde = Desde & " LEFT JOIN dbo.SYSCOLUMNS COL2 ON COL2.colid = FK.rkey AND COL2.id = OBJ2.id"
   Condicion = "OBJ.name = 'MOVIMIENTOS' AND (OBJ.xtype='U' OR OBJ.xtype='V')"
   Condicion = Condicion & " AND COL.name = 'NU_MOV_MOVI' ORDER BY COL.colid"
   If Result <> FAIL Then Result = LoadData(Desde, Campos, Condicion, VrArr)
   If Result <> FAIL And Encontro Then
      If VrArr(0) = "varchar" Then
         Condicion = "ISNUMERIC(NU_MOV_MOVI) = 0 OR LEN(NU_MOV_MOVI) > 10"
         Result = DoUpdate("MOVIMIENTOS", "NU_MOV_MOVI = NU_CONS_MOVI", Condicion)
         StSql = "ALTER TABLE MOVIMIENTOS ALTER COLUMN NU_MOV_MOVI NUMERIC(10,0) NULL"
         If Result <> FAIL Then Result = ExecSQLCommand(StSql)
         StSql = "ALTER TABLE MOVIMIENTOS ADD CONSTRAINT DF__MOVIMIENT__NU__MO DEFAULT 0 FOR NU_MOV_MOVI"
         If Result <> FAIL Then Result = ExecSQLCommand(StSql)
         Condicion = "ISNUMERIC(NU_MOV_MVNF) = 0 OR LEN(NU_MOV_MVNF) > 10"
         If Result <> FAIL Then Result = DoUpdate("MOVIMIENTOS_NIIF", "NU_MOV_MVNF = NU_CONS_MVNF", Condicion)
         StSql = "ALTER TABLE MOVIMIENTOS_NIIF ALTER COLUMN NU_MOV_MVNF NUMERIC(10,0) NULL"
         If Result <> FAIL Then Result = ExecSQLCommand(StSql)
         StSql = "ALTER TABLE MOVIMIENTOS_NIIF ADD CONSTRAINT DF__MOVIMIENT__NU__MOV DEFAULT 0 FOR NU_MOV_MVNF"
         If Result <> FAIL Then Result = ExecSQLCommand(StSql)
      End If
   End If
   'JAUM T34597 Fin
   'Result = SUCCEED JAUM T33276 Se deja linea en comentario
   If Result <> FAIL Then Result = DoUpdate("PARAMETROS_INVE", "NU_NVERDB_PINV='8.1.0'", NUL$)
End Sub

'---------------------------------------------------------------------------------------
' Procedure : Actualizar_8_2
' DateTime  : 29/09/2016 15:23
' Author    : jairo_parra
' Purpose   : JLPB T36875-R35965 'Metodo que crea o actualiza las tablas y la nueva versi�n de la BD
'---------------------------------------------------------------------------------------
'
Sub Actualizar_8_2()
   Dim InNumarchivo As Integer 'JLPB T37570 'Contiene el resultado de la apertura de un documento de texto
   Dim StScript As String 'JLPB T37570 'Contiene el script a ejecutar
   Result = SUCCEED
   If Result <> FAIL Then
      Desde = "UNIDAD_MEDIDA"
      If Not ExisteTABLA(Desde) Then
         If Result <> FAIL Then Result = CrearTabla(Desde, "TX_CODIGO_UMED,10,3,0")
         If Result <> FAIL Then Result = CrearCampo(Desde, "TX_DESC_UMED", "10", 50, 1)
         If Result <> FAIL Then Result = CrearIndice(Desde, "PK_" & Desde, "TX_CODIGO_UMED", True)
      End If
      Desde = "ARTICULO"
      If ExisteTABLA(Desde) And Result <> FAIL Then
         'JLPB T37126 INICIO Se deja en comentario las siguientes lineas
         'If Result <> FAIL Then Result = CrearCampo(Desde, "TX_CODIGO_UMED_ARTI", "10", 3, 1)
         'If Result <> FAIL Then Result = CrearRelacion("UNIDAD_MEDIDA_ARTICULO", "TX_CODIGO_UMED_ARTI", "TX_CODIGO_UMED", Desde, "UNIDAD_MEDIDA")
         If Not ExisteCAMPO(Desde, "TX_CODIGO_UMED_ARTI") And Result <> FAIL Then Result = CrearCampo(Desde, "TX_CODIGO_UMED_ARTI", "10", 3, 1)
         If BuscarRelacion("UNIDAD_MEDIDA", Desde, "TX_CODIGO_UMED_ARTI") <> "UNIDAD_MEDIDA_ARTICULO" And Result <> FAIL Then Result = CrearRelacion("UNIDAD_MEDIDA_ARTICULO", "TX_CODIGO_UMED_ARTI", "TX_CODIGO_UMED", Desde, "UNIDAD_MEDIDA")
         'JLPB T37126 FIN
      End If
      If Result <> FAIL Then 'JLPB T37126
         Result = fnAgregarOpcion("UNIDAD DE MEDIDA", "FrmUMedida", "02", "FRM")
      End If 'JLPB T37126
   End If
   'JLPB T37570 INICIO
   If Dir$(App.Path & "\PA_COSTO_PROMEDIO.txt") <> "" And Result <> FAIL Then
      InNumarchivo = FreeFile
      Open App.Path & "\PA_COSTO_PROMEDIO.txt" For Input As InNumarchivo
      StScript = Input(LOF(InNumarchivo), #InNumarchivo)
      Close InNumarchivo
      If ExisteStoreProcedure("PA_COSTO_PROMEDIO") Then
         Call ExecSQLCommand("DROP PROCEDURE PA_COSTO_PROMEDIO")
      End If
      If Result <> FAIL Then Result = ExecSQL(StScript)
      If Result = FAIL Then Call Mensaje1("El procedimiento" & " " & "PA_COSTO_PROMEDIO" & " no se creo correctamente", 1)
   Else
      Call Mensaje1("El archivo" & " " & "PA_COSTO_PROMEDIO.txt" & " no existe.", 1)
   End If
   'JLPB T37570 FIN
   If Result <> FAIL Then Result = DoUpdate("PARAMETROS_INVE", "NU_NVERDB_PINV='8.2.0'", NUL$)
End Sub
'---------------------------------------------------------------------------------------
' Procedure : Actualizar_8_3
' DateTime  : 08/02/2017 12:10
' Author    : miguel_avendano
' Purpose   : MFAB T38552 'Metodo que crea o actualiza las tablas y la nueva versi�n de la BD
'---------------------------------------------------------------------------------------
'
Sub Actualizar_8_3()
   Dim StSql As String 'Contiene el script a ejecutar
   Result = SUCCEED
   StSql = "ALTER TABLE PERFIL ALTER COLUMN NU_AUTO_EMPR_PERF INT  NULL"
   If Result <> FAIL Then Result = ExecSQLCommand(StSql)
   If Result <> FAIL Then Result = DoUpdate("PARAMETROS_INVE", "NU_NVERDB_PINV='8.3.0'", NUL$)
End Sub

'---------------------------------------------------------------------------------------
' Procedure : Actualizar_8_4
' DateTime  : 20/10/2017 16:44
' Author    : jairo_parra
' Purpose   : JLPB T42237 'Metodo que crea o actualiza las tablas y la nueva versi�n de la BD
'---------------------------------------------------------------------------------------
'
Sub Actualizar_8_4()
   Result = SUCCEED
   If Result <> FAIL Then Result = DoUpdate("PARAMETROS_INVE", "NU_NVERDB_PINV='8.4.0'", NUL$)
End Sub

'---------------------------------------------------------------------------------------
' Procedure : Actualizar_8_5
' DateTime  : 01/03/2018 11:14
' Author    : jairo_parra
' Purpose   : JLPB T41459-R37519 'Metodo que crea o actualiza las tablas y la nueva versi�n de la BD
'---------------------------------------------------------------------------------------
'
Sub Actualizar_8_5()
   Result = SUCCEED
   If Result <> FAIL Then
      If Not ExisteTABLA("IN_AUDMOVCONT") Then
         If Result <> FAIL Then Result = CrearTabla("IN_AUDMOVCONT", "NU_AUTENCA_AUDT,4,0,0")
         If Result <> FAIL Then Result = CrearCampo("IN_AUDMOVCONT", "TX_COMP_AUDT", "10", 4, 0)
         If Result <> FAIL Then Result = CrearCampo("IN_AUDMOVCONT", "NU_CONMOV_AUDT", "4", 0, 0)
         If Result <> FAIL Then Result = CrearCampo("IN_AUDMOVCONT", "TX_CUENTA_AUDT", "10", 15, 0)
         If Result <> FAIL Then Result = CrearCampo("IN_AUDMOVCONT", "TX_TERC_AUDT", "10", 20, 0)
         If Result <> FAIL Then Result = CrearCampo("IN_AUDMOVCONT", "TX_CC_AUDT", "10", 11, 0)
         If Result <> FAIL Then Result = CrearCampo("IN_AUDMOVCONT", "TX_NATU_AUDT", "10", 1, 0)
         If Result <> FAIL Then Result = CrearCampo("IN_AUDMOVCONT", "NU_VALOR_AUDT", "13", 20.2, 0)
         If Result <> FAIL Then Result = CrearCampo("IN_AUDMOVCONT", "NU_VALORB_AUDT", "13", 20.2, 0)
         If Result <> FAIL Then Result = CrearCampo("IN_AUDMOVCONT", "NU_VALORIVA_AUDT", "13", 20.2, 0)
         If Result <> FAIL Then Result = CrearCampo("IN_AUDMOVCONT", "NU_AUTUSU_AUDT", "4", 0, 0)
         If Result <> FAIL Then Result = CrearCampo("IN_AUDMOVCONT", "NU_IMPT_AUDT", "3", 0, 0)
         If Result <> FAIL Then Result = CrearCampo("IN_AUDMOVCONT", "NU_ORIALM_AUDT", "1", 0, 0)
         If Result <> FAIL Then Result = CrearIndice("IN_AUDMOVCONT", "PK_IN_AUDMOVCONT", "NU_AUTENCA_AUDT,NU_IMPT_AUDT,NU_ORIALM_AUDT", True)
      End If
   End If
   If Result <> FAIL Then Result = fnAgregarOpcion("AUDITORIA MOVIMIENTOS", "FrmAudtMov", "01", "FRM")
   If Result <> FAIL Then Result = fnAgregarOpcion("FACTURAS DE VENTA", "FrmFacVent", "04", "FRM") 'DRMG T43070-R41726 se agrega la nueva opcion de facturas de ventas dentro de informes
   If Result <> FAIL Then Result = DoUpdate("PARAMETROS_INVE", "NU_NVERDB_PINV='8.5.0'", NUL$)
End Sub
'---------------------------------------------------------------------------------------
' Procedure : Actualizar_8_6_0
' DateTime  : 17/05/2018 09:55
' Author    : daniel_mesa
' Purpose   : DRMG T44728-41288 'Metodo que crea o actualiza las tablas y la nueva versi�n de la BD
'---------------------------------------------------------------------------------------
'
Sub Actualizar_8_6_0()
   Dim StScript As String 'DRMG T45162-R41288 Almacena los registros que se insertaran en la tabla
   Dim InArchivo As Integer 'DRMG T45162-R41288 Valor que indica si se puede leer o no un archivo
   Result = SUCCEED
   
   'DRMG T44413-R44232 INICIO
   If ExisteTABLA("ENTIDAD") Then
      If Not ExisteCAMPO("ENTIDAD", "NU_NIIF_ENTI") Then
         If Result <> FAIL Then
            Result = CrearCampo("ENTIDAD", "NU_NIIF_ENTI", 1, 0, 1)
            If Result <> FAIL Then Result = DoUpdate("ENTIDAD", "NU_NIIF_ENTI = 0", NUL$)
         End If
      End If
   End If
   'DRMG T44413-R44232 FIN
   
   If Result <> FAIL Then
      If Not ExisteTABLA("FACTURA_ELECTRONICA") Then
         Result = CrearTabla("FACTURA_ELECTRONICA", "NU_MODULO_FAEL, 4, 0, 1")
         If Result <> FAIL Then Result = CrearCampo("FACTURA_ELECTRONICA", "NU_FACINI_FAEL", "13", 10, 1)
         If Result <> FAIL Then Result = ExecSQL("ALTER TABLE FACTURA_ELECTRONICA ALTER COLUMN NU_FACINI_FAEL NUMERIC(10,0)")
         If Result <> FAIL Then Result = CrearCampo("FACTURA_ELECTRONICA", "NU_FACFIN_FAEL", "13", 10, 1)
         If Result <> FAIL Then Result = ExecSQL("ALTER TABLE FACTURA_ELECTRONICA ALTER COLUMN NU_FACFIN_FAEL NUMERIC(10,0)")
         If Result <> FAIL Then Result = CrearCampo("FACTURA_ELECTRONICA", "TX_PREFI_FAEL", "10", 4, 1)
         If Result <> FAIL Then Result = CrearCampo("FACTURA_ELECTRONICA", "TX_AUTORI_FAEL", "10", 30, 1)
         If Result <> FAIL Then Result = CrearCampo("FACTURA_ELECTRONICA", "TX_CTEC_FAEL", "10", 50, 1)
         If Result <> FAIL Then Result = CrearCampo("FACTURA_ELECTRONICA", "FE_AUTORI_FAEL", "8", 0, 1)
         If Result <> FAIL Then Result = CrearCampo("FACTURA_ELECTRONICA", "FE_FINAUT_FAEL", "8", 0, 1)
         If Result <> FAIL Then Result = CrearCampo("FACTURA_ELECTRONICA", "NU_CONFAC_FAEL", "13", 14, 1)
         If Result <> FAIL Then Result = ExecSQL("ALTER TABLE FACTURA_ELECTRONICA ALTER COLUMN NU_CONFAC_FAEL NUMERIC(14,0)")
         If Result <> FAIL Then Result = CrearCampo("FACTURA_ELECTRONICA", "IM_LOGO_FAEL", "11", 0, 1)
         If Result <> FAIL Then Result = CrearCampo("FACTURA_ELECTRONICA", "TX_ENCA_FAEL", "10", 255, 1)
         If Result <> FAIL Then Result = CrearCampo("FACTURA_ELECTRONICA", "TX_PIEUNO_FAEL", "10", 1000, 1)
         If Result <> FAIL Then Result = CrearCampo("FACTURA_ELECTRONICA", "TX_PIEDOS_FAEL", "10", 1000, 1)
         If Result <> FAIL Then Result = CrearCampo("FACTURA_ELECTRONICA", "TX_URL_FAEL", "10", 1000, 1)
         If Result <> FAIL Then Result = CrearCampo("FACTURA_ELECTRONICA", "NU_ESTADO_FAEL", "2", 0, 1)
         If Result <> FAIL Then Result = CrearCampo("FACTURA_ELECTRONICA", "TX_URLCER_FAEL", "10", 1000, 1)
         If Result <> FAIL Then Result = CrearCampo("FACTURA_ELECTRONICA", "TX_PSSWRD_FAEL", "10", 100, 1)
         If Result <> FAIL Then Result = CrearCampo("FACTURA_ELECTRONICA", "TX_IDSOFT_FAEL", "10", 100, 1)
         If Result <> FAIL Then Result = CrearCampo("FACTURA_ELECTRONICA", "TX_PIN_FAEL", "10", 100, 1)
         If Result <> FAIL Then Result = CrearCampo("FACTURA_ELECTRONICA", "TX_URLENV_FAEL", "10", 1000, 1)
         If Result <> FAIL Then Result = CrearCampo("FACTURA_ELECTRONICA", "TX_URLCON_FAEL", "10", 1000, 1)
      End If
   End If
   
   'DRMG T45150 INICIO
   If Result <> FAIL Then
      If ExisteTABLA("FACTURA_ELECTRONICA") Then
         If Not ExisteCAMPO("FACTURA_ELECTRONICA", "TX_SFPSW_FAEL") Then
            Result = CrearCampo("FACTURA_ELECTRONICA", "TX_SFPSW_FAEL", "10", 100, 1)
         End If
      End If
   End If
   'DRMG T45150 FIN
   
   'DRMG T45218 INICIO
   If Result <> FAIL Then
      If Not ExisteTABLA("AUTO_FAEL") Then
         Result = CrearTabla("AUTO_FAEL", "NU_MODULO_AUFA, 4, 0, 1")
         If Result <> FAIL Then Result = CrearCampo("AUTO_FAEL", "NU_FACINI_AUFA", "13", 10, 1)
         If Result <> FAIL Then Result = ExecSQLCommand("ALTER TABLE AUTO_FAEL ALTER COLUMN NU_FACINI_AUFA NUMERIC(10,0)")
         If Result <> FAIL Then Result = CrearCampo("AUTO_FAEL", "NU_FACFIN_AUFA", "13", 10, 1)
         If Result <> FAIL Then Result = ExecSQLCommand("ALTER TABLE AUTO_FAEL ALTER COLUMN NU_FACFIN_AUFA NUMERIC(10,0)")
         If Result <> FAIL Then Result = CrearCampo("AUTO_FAEL", "TX_PREFI_AUFA", "10", 4, 1)
         If Result <> FAIL Then Result = CrearCampo("AUTO_FAEL", "TX_AUTORI_AUFA", "10", 30, 1)
         If Result <> FAIL Then Result = CrearCampo("AUTO_FAEL", "TX_CTEC_AUFA", "10", 50, 1)
         If Result <> FAIL Then Result = CrearCampo("AUTO_FAEL", "FE_AUTORI_AUFA", "8", 0, 1)
         If Result <> FAIL Then Result = CrearCampo("AUTO_FAEL", "FE_FINAUT_AUFA", "8", 0, 1)
         If Result <> FAIL Then Result = CrearCampo("AUTO_FAEL", "NU_CONFAC_AUFA", "13", 14, 1)
         If Result <> FAIL Then Result = ExecSQLCommand("ALTER TABLE AUTO_FAEL ALTER COLUMN NU_CONFAC_AUFA NUMERIC(14,0)")
         If Result <> FAIL Then Result = CrearCampo("AUTO_FAEL", "IM_LOGO_AUFA", "11", 0, 1)
         If Result <> FAIL Then Result = CrearCampo("AUTO_FAEL", "TX_ENCA_AUFA", "10", 255, 1)
         If Result <> FAIL Then Result = CrearCampo("AUTO_FAEL", "TX_PIEUNO_AUFA", "10", 1000, 1)
         If Result <> FAIL Then Result = CrearCampo("AUTO_FAEL", "TX_PIEDOS_AUFA", "10", 1000, 1)
         If Result <> FAIL Then Result = CrearCampo("AUTO_FAEL", "TX_URL_AUFA", "10", 1000, 1)
         If Result <> FAIL Then Result = CrearCampo("AUTO_FAEL", "TX_URLCER_AUFA", "10", 1000, 1)
         If Result <> FAIL Then Result = CrearCampo("AUTO_FAEL", "TX_PSSWRD_AUFA", "10", 100, 1)
         If Result <> FAIL Then Result = CrearCampo("AUTO_FAEL", "TX_IDSOFT_AUFA", "10", 100, 1)
         If Result <> FAIL Then Result = CrearCampo("AUTO_FAEL", "TX_PIN_AUFA", "10", 100, 1)
         If Result <> FAIL Then Result = CrearCampo("AUTO_FAEL", "TX_URLENV_AUFA", "10", 1000, 1)
         If Result <> FAIL Then Result = CrearCampo("AUTO_FAEL", "TX_URLCON_AUFA", "10", 1000, 1)
         If Result <> FAIL Then Result = CrearCampo("AUTO_FAEL", "TX_SFPSW_AUFA", "10", 100, 1)
      End If
   End If
   'DRMG T45218 FIN
   
   If Result <> FAIL Then
      If Not ExisteTABLA("R_FAVE_FAEL") Then
         Result = ExecSQL("CREATE TABLE R_FAVE_FAEL (NU_AUTO_FAFA BIGINT IDENTITY(1,1) NOT NULL)")
         If Result <> FAIL Then Result = CrearCampo("R_FAVE_FAEL", "NU_CNFAVE_FAFA", "13", 14, 0)
         If Result <> FAIL Then Result = CrearCampo("R_FAVE_FAEL", "NU_DOCU_FAFA", "4", 0, 0)
         If Result <> FAIL Then Result = CrearCampo("R_FAVE_FAEL", "NU_COMP_FAFA", "4", 0, 0)
         If Result <> FAIL Then Result = CrearCampo("R_FAVE_FAEL", "NU_NUCOMP_FAFA", "4", 0, 0)
         If Result <> FAIL Then Result = CrearCampo("R_FAVE_FAEL", "TX_PREFI_FAFA", "10", 4, 0)
         If Result <> FAIL Then Result = CrearCampo("R_FAVE_FAEL", "TX_QR_FAFA", "10", 300, 1)
         If Result <> FAIL Then Result = CrearCampo("R_FAVE_FAEL", "TX_CUFE_FAFA", "10", 200, 1)
         'If Result <> FAIL Then Result = CrearCampo("R_FAVE_FAEL", "TX_HORA_FAFA", "10", 5, 0) 'DRMG T45162-R41288
         If Result <> FAIL Then Result = CrearCampo("R_FAVE_FAEL", "TX_HORA_FAFA", "10", 8, 0) 'DRMG T45162-R41288
         Result = ExecSQL("ALTER TABLE R_FAVE_FAEL ADD CONSTRAINT FK_NU_COMP_FAFA FOREIGN KEY (NU_COMP_FAFA)REFERENCES IN_COMPROBANTE (NU_AUTO_COMP)")
      End If
   End If
   
   If Result <> FAIL Then
      If Not ExisteTABLA("TM_FACTURA_PLANO") Then
         Result = ExecSQL("CREATE TABLE TM_FACTURA_PLANO (ID BIGINT IDENTITY(1,1) CONSTRAINT [PK_TM_FACTURA_PLANO]" & _
            " PRIMARY KEY CLUSTERED (ID ASC)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) )")
         If Result <> FAIL Then Result = CrearCampo("TM_FACTURA_PLANO", "ID_TERCERO", "10", 20, 0)
         If Result <> FAIL Then Result = CrearCampo("TM_FACTURA_PLANO", "ID_CONVENIO", "4", 0, 0)
         If Result <> FAIL Then Result = CrearCampo("TM_FACTURA_PLANO", "ID_IPS", "10", 16, 0)
         If Result <> FAIL Then Result = CrearCampo("TM_FACTURA_PLANO", "FECHA", "8", 0, 0)
         If Result <> FAIL Then Result = CrearCampo("TM_FACTURA_PLANO", "RUTA_ARCHIVO", "10", 100, 1)
         If Result <> FAIL Then Result = CrearCampo("TM_FACTURA_PLANO", "NOMBRE_ARCHIVO", "10", 50, 1)
         If Result <> FAIL Then Result = CrearCampo("TM_FACTURA_PLANO", "USUARIO", "10", 30, 1)
         If Result <> FAIL Then Result = CrearCampo("TM_FACTURA_PLANO", "IP_ORIGEN", "10", 70, 1)
         If Result <> FAIL Then Result = CrearCampo("TM_FACTURA_PLANO", "DOCUMENTO", "10", 200, 1)
         'If Result <> FAIL Then Result = CrearCampo("TM_FACTURA_PLANO", "OBSERVACION", "10", 100, 1) 'DRMG T45162-R41288 SE DEJA EN COMENTARIO
         If Result <> FAIL Then Result = CrearCampo("TM_FACTURA_PLANO", "OBSERVACION", "10", 8000, 1) 'DRMG T45162-R41288
         If Result <> FAIL Then Result = CrearCampo("TM_FACTURA_PLANO", "MODULO", "4", 0, 1)
      End If
   End If
   
   'DRMG T45162-R41288 INICIO
   If Result <> FAIL Then
      If ExisteTABLA("TM_FACTURA_PLANO") Then
         If Not ExisteCAMPO("TM_FACTURA_PLANO", "TIPO") Then
            If Result <> FAIL Then Result = CrearCampo("TM_FACTURA_PLANO", "TIPO", "10", 1, 1)
         End If
         If Not ExisteCAMPO("TM_FACTURA_PLANO", "ESTADO_DIAN") Then
            Result = CrearCampo("TM_FACTURA_PLANO", "ESTADO_DIAN", "2", 0, 1)
         End If
         If Result <> FAIL And Not ExisteCAMPO("TM_FACTURA_PLANO", "OBSERVACION_ESTADO") Then
            Result = CrearCampo("TM_FACTURA_PLANO", "OBSERVACION_ESTADO", "10", 8000, 1)
         End If
      End If
   End If
   'DRMG T45612
   
   
   If Result <> FAIL Then
      If Not ExisteTABLA("DATOS_EMAIL_FAC") Then
         Result = CrearTabla("DATOS_EMAIL_FAC", "TX_SMPT_DEF, 10, 255, 0")
         If Result <> FAIL Then Result = CrearCampo("DATOS_EMAIL_FAC", "TX_PUERTO_DEF", "10", 5, 0)
         If Result <> FAIL Then Result = CrearCampo("DATOS_EMAIL_FAC", "TX_CORREO_DEF", "10", 50, 0)
         If Result <> FAIL Then Result = CrearCampo("DATOS_EMAIL_FAC", "TX_CLAVE_DEF", "10", 30, 0)
         'If Result <> FAIL Then Result = CrearCampo("DATOS_EMAIL_FAC", "TX_ASUNTO_DEF", "10", 30, 0) 'DRMG T45220 SE DEJA EN COMENTARIO
         If Result <> FAIL Then Result = CrearCampo("DATOS_EMAIL_FAC", "TX_ASUNTO_DEF", "10", 60, 0) 'DRMG T45220
         If Result <> FAIL Then Result = CrearCampo("DATOS_EMAIL_FAC", "TX_DETA_DEF", "10", 500, 1)
         If Result <> FAIL Then Result = CrearCampo("DATOS_EMAIL_FAC", "TX_MODULO_DEF", "4", 0, 0)
      End If
   End If
   
   'DRMG T45220 INICIO
   If ExisteTABLA("DATOS_EMAIL_FAC") Then
      'SE AGREGA EL CAMPO PARA EL PROTOCOLO DE SEGUIRIDAD SSL
      If Not ExisteCAMPO("DATOS_EMAIL_FAC", "NU_SSL_DEF") Then
         If Result <> FAIL Then Result = CrearCampo("DATOS_EMAIL_FAC", "NU_SSL_DEF", "1", 0, 1)
      End If
   End If
   'DRMG T45220 FIN
   
   If Result <> FAIL Then
      If Not ExisteTABLA("ENVIO_FAEL") Then
         'DRMG T45136 INICIO SE DEJA EN COMENTARIO LAS SIGUIENTES 7 LINEAS
'         Result = CrearTabla("ENVIO_FAEL", "NU_NUMFAC_FAEL, 4, 0, 0")
'         If Result <> FAIL Then Result = CrearCampo("ENVIO_FAEL", "NU_ESTA_FAEL", "1", 0, 1)
'         If Result <> FAIL Then Result = CrearCampo("ENVIO_FAEL", "TX_OBEMAIL_FAEL", "10", 200, 1)
'         If Result <> FAIL Then Result = CrearCampo("ENVIO_FAEL", "NU_ESTXML_FAEL", "1", 0, 1)
'         'If Result <> FAIL Then Result = CrearCampo("ENVIO_FAEL", "TX_OBXML_FAEL", "10", 200, 1) 'DRMG T45162-R41288 SE DEJA EN COMENTARIO
'         If Result <> FAIL Then Result = CrearCampo("ENVIO_FAEL", "TX_OBXML_FAEL", "10", 8000, 1) 'DRMG T45162-R41288
'         If Result <> FAIL Then Result = CrearCampo("ENVIO_FAEL", "NU_MODULO_FAEL", "4", 0, 0)
         Result = CrearTabla("ENVIO_FAEL", "NU_NUMFAC_ENFA, 4, 0, 0")
         If Result <> FAIL Then Result = CrearCampo("ENVIO_FAEL", "NU_ESTA_ENFA", "1", 0, 1)
         If Result <> FAIL Then Result = CrearCampo("ENVIO_FAEL", "TX_OBEMAIL_ENFA", "10", 200, 1)
         If Result <> FAIL Then Result = CrearCampo("ENVIO_FAEL", "NU_ESTXML_ENFA", "1", 0, 1)
         If Result <> FAIL Then Result = CrearCampo("ENVIO_FAEL", "TX_OBXML_ENFA", "10", 8000, 1)
         If Result <> FAIL Then Result = CrearCampo("ENVIO_FAEL", "NU_MODULO_ENFA", "4", 0, 0)
         'DRMG T45136 FIN
      End If
   End If
   
   If Result <> FAIL Then
      If Not ExisteTABLA("IMPUESTO_FAVE") Then
         Result = CrearTabla("IMPUESTO_FAVE", "NU_CONFA_IMFA, 13, 14, 0")
         If Result <> FAIL Then Result = CrearCampo("IMPUESTO_FAVE", "TX_IMPU_IMFA", "10", 3, 0)
         If Result <> FAIL Then Result = CrearCampo("IMPUESTO_FAVE", "TX_TIPO_IMFA", "10", 1, 1)
         If Result <> FAIL Then Result = CrearCampo("IMPUESTO_FAVE", "NU_PORCEN_IMFA", "7", 0, 0)
         If Result <> FAIL Then Result = CrearCampo("IMPUESTO_FAVE", "NU_VALOR_IMFA", "7", 0, 0)
         If Result <> FAIL Then Result = CrearCampo("IMPUESTO_FAVE", "NU_MODULO_IMFA", "4", 0, 0)
      End If
   End If
   
   If Result <> FAIL Then
      If Not ExisteTABLA("DESCUENTO_FAVE") Then
         'Result = CrearTabla("DESCUENTO_FAVE", "NU_CONFA_FAVE, 13, 14, 0") 'DRMG T45162-R41288 SE DEJA EN COMENTARIO
         Result = CrearTabla("DESCUENTO_FAVE", "NU_CONFA_DEFA, 13, 14, 0") 'DRMG T45162-R41288
         If Result <> FAIL Then Result = CrearCampo("DESCUENTO_FAVE", "TX_CODDES_DEFA", "10", 3, 0)
         If Result <> FAIL Then Result = CrearCampo("DESCUENTO_FAVE", "NU_PORCEN_DEFA", "7", 0, 0)
         If Result <> FAIL Then Result = CrearCampo("DESCUENTO_FAVE", "NU_VALOR_DEFA", "7", 0, 0)
         If Result <> FAIL Then Result = CrearCampo("DESCUENTO_FAVE", "NU_MODULO_DEFA", "4", 0, 0)
      End If
   End If
   
   'DRMG T45221-R41288 INICIO
   If Result <> FAIL Then
      If ExisteTABLA("DESCUENTO_FAVE") Then
         If Not ExisteCAMPO("DESCUENTO_FAVE", "NU_BASE_DEFA") Then
            Result = CrearCampo("DESCUENTO_FAVE", "NU_BASE_DEFA", "7", 0, 1)
         End If
      End If
   End If
   
   If Result <> FAIL Then
      If ExisteTABLA("IMPUESTO_FAVE") Then
         If Not ExisteCAMPO("IMPUESTO_FAVE", "NU_BASE_IMFA") Then
            Result = CrearCampo("IMPUESTO_FAVE", "NU_BASE_IMFA", "7", 0, 1)
         End If
      End If
   End If
   'DRMG T45221-R41288 FIN
   
   'DRMG T45162-R41288 INICIO
   If Result <> FAIL Then
      If Not ExisteTABLA("ENVIO_NOTA") Then
         Result = CrearTabla("ENVIO_NOTA", "NU_NUMNOT_ENNO, 4, 0, 0")
         If Result <> FAIL Then Result = CrearCampo("ENVIO_NOTA", "NU_NOTA_ENNO", "4", 0, 1)
         If Result <> FAIL Then Result = CrearCampo("ENVIO_NOTA", "NU_ESTA_ENNO", "1", 0, 1)
         If Result <> FAIL Then Result = CrearCampo("ENVIO_NOTA", "TX_OBEMAIL_ENNO", "10", 200, 1)
         If Result <> FAIL Then Result = CrearCampo("ENVIO_NOTA", "NU_ESTXML_ENNO", "1", 0, 1)
         If Result <> FAIL Then Result = CrearCampo("ENVIO_NOTA", "TX_OBXML_ENNO", "10", 8000, 1)
         If Result <> FAIL Then Result = CrearCampo("ENVIO_NOTA", "NU_MODULO_ENNO", "4", 0, 0)
      End If
   End If
   
   If Result <> FAIL Then
      If Not ExisteTABLA("CONCEPTO_FAVE") Then
         Result = CrearTabla("CONCEPTO_FAVE", "NU_TIPNOT_COFA, 4, 0, 0")
         If Result <> FAIL Then Result = CrearCampo("CONCEPTO_FAVE", "NU_CODCON_COFA", "4", 0, 0)
         If Result <> FAIL Then Result = CrearCampo("CONCEPTO_FAVE", "TX_NOMCON_COFA", "10", 50, 1)
         If Result <> FAIL Then Result = ExecSQLCommand("ALTER TABLE CONCEPTO_FAVE ADD PRIMARY KEY(NU_TIPNOT_COFA,NU_CODCON_COFA)")
         If Result <> FAIL Then Result = ExecSQLCommand("INSERT INTO CONCEPTO_FAVE VALUES(1,1,'Devoluci�n de parte de los bienes')")
         If Result <> FAIL Then Result = ExecSQLCommand("INSERT INTO CONCEPTO_FAVE VALUES(1,2,'Anulaci�n de factura electr�nica')")
         If Result <> FAIL Then Result = ExecSQLCommand("INSERT INTO CONCEPTO_FAVE VALUES(1,3,'Rebaja total aplicada')")
         If Result <> FAIL Then Result = ExecSQLCommand("INSERT INTO CONCEPTO_FAVE VALUES(1,4,'Descuento total aplicado')")
         If Result <> FAIL Then Result = ExecSQLCommand("INSERT INTO CONCEPTO_FAVE VALUES(1,5,'Rescisi�n: nulidad por falta de requisitos')")
         If Result <> FAIL Then Result = ExecSQLCommand("INSERT INTO CONCEPTO_FAVE VALUES(1,6,'Otros')")
         If Result <> FAIL Then Result = ExecSQLCommand("INSERT INTO CONCEPTO_FAVE VALUES(0,1,'Intereses')")
         If Result <> FAIL Then Result = ExecSQLCommand("INSERT INTO CONCEPTO_FAVE VALUES(0,2,'Gastos por cobrar')")
         If Result <> FAIL Then Result = ExecSQLCommand("INSERT INTO CONCEPTO_FAVE VALUES(0,3,'Cambio del valor')")
      End If
   End If
   'DEGP T43448 INICIO
   If Result <> FAIL Then
      If Not ExisteTABLA("R_DESCFV_ENDE") Then
         Result = CrearTabla("R_DESCFV_ENDE", "NU_AUENCA_ENDE,4,4,0")
         If Result <> FAIL Then Result = CrearCampo("R_DESCFV_ENDE", "NU_AUTO_ENDE", "4", 4, 0, True)
         If Result <> FAIL Then Result = CrearCampo("R_DESCFV_ENDE", "NU_AUDETA_ENDE", "4", 4, 0)
         If Result <> FAIL Then Result = CrearCampo("R_DESCFV_ENDE", "NU_DESC_ENDE", "6", 4, 0)
      End If
   End If
   'DEGP T43448 FIN
   If Result <> FAIL Then
      If Dir$(App.Path & "\SP PA_FACTURA_ELECTRONICA_INVENTARIOS.txt") <> "" Then
         InArchivo = FreeFile
         Open App.Path & "\SP PA_FACTURA_ELECTRONICA_INVENTARIOS.txt" For Input As InArchivo
         StScript = Input(LOF(InArchivo), #InArchivo)
         Close InArchivo
         'DRMG T45431 INICIO SE DEJA EN COMENTARIO LAS DOS PRIMERAS LINEAS
         'If ExisteStoreProcedure("QRY_FACTURACION_ELECTRONICA_INVE") Then
            'Result = ExecSQLCommand("DROP PROCEDURE QRY_FACTURACION_ELECTRONICA_INVE")
         If ExisteStoreProcedure("PA_FACTURA_ELECTRONICA_INVENTARIOS") Then
            Result = ExecSQLCommand("DROP PROCEDURE PA_FACTURA_ELECTRONICA_INVENTARIOS")
         'DRMG T45431 FIN
         End If
         If Result <> FAIL Then Result = ExecSQL(StScript)
         'If Result = FAIL Then Call Mensaje1("El procedimiento QRY_FACTURACION_ELECTRONICA_INVE no se creo correctamente", 1) 'DRMG T45431 SE DEJA EN COMENTARIO
         If Result = FAIL Then Call Mensaje1("El procedimiento PA_FACTURA_ELECTRONICA_INVENTARIOS no se creo correctamente", 1) 'DRMG T45431
      Else
         Call Mensaje1("El archivo SP PA_FACTURA_ELECTRONICA_INVENTARIOS.txt no existe", 1)
         Result = FAIL
      End If
   End If
   'DRMG T45162-R41288 FIN
   
   If Result <> FAIL Then Result = DoUpdate("PARAMETROS_INVE", "NU_NVERDB_PINV='8.6.0'", NUL$)
   
End Sub

'---------------------------------------------------------------------------------------
' Procedure : Actualizar_8_7_0
' DateTime  : 24/02/2019 19:06
' Author    : gloria_camargo
' Purpose   : GSCD T45902-R42283 Actualizaci�n de la BD
'---------------------------------------------------------------------------------------
'
Sub Actualizar_8_7_0()

   Result = SUCCEED
   If ExisteTABLA("ARTICULO") Then
      If Not ExisteCAMPO("ARTICULO", "NU_CONDI_ARTI") Then
         If Result <> FAIL Then
            Result = CrearCampo("ARTICULO", "NU_CONDI_ARTI", 1, 0, 0)
         End If
      End If
   End If
   'DRMG T45889-R43750 INICIO
   If ExisteTABLA("IN_KARDEX") Then
      If Not ExisteCAMPO("IN_KARDEX", "NU_COSPRO_KARD") Then
         If Result <> FAIL Then
            'Result = CrearCampo("IN_KARDEX", "NU_COSPRO_KARD", 4, 0, 0) 'DRMG T46118 se deja en comentario
            Result = CrearCampo("IN_KARDEX", "NU_COSPRO_KARD", 7, 0, 1) 'DRMG T46118
         End If
         
         'DRMG T46118 INICIO SE DEJA EN COMENTARIO EL SIGUIENTE BLOQUE
         'If Result <> FAIL Then
            'Campos = "NU_COSPRO_KARD=VL_COPR_ARTI FROM IN_KARDEX INNER JOIN ARTICULO ON NU_AUTO_ARTI_KARD = NU_AUTO_ARTI"
            'Result = DoUpdate("IN_KARDEX", Campos, NUL$)
         'End If
         'DRMG T46118 FIN
         
      End If
   End If
   'DRMG T45889-R43750 FIN
   If Result <> FAIL Then Result = DoUpdate("PARAMETROS_INVE", "NU_NVERDB_PINV='8.7.0'", NUL$)
End Sub
Function fnEliminarVista(stNombre As String) As Integer '|.DR.|
'Elimina una vista/consulta.
    Dim StSql As String, lgError As Long
    Dim BoOpenBD As Boolean 'HRR M2655
    Dim StNameCon As String 'HRR M2655

    BoOpenBD = False 'HRR M2655
    StSql = "DROP VIEW " & stNombre
    
On Error Resume Next
    If MotorBD = "ACCESS" Then
       'BDDAO.QueryDefs.Delete stNombre
       'HRR M2655
         ERR.Number = 0
        StNameCon = BDDAO(BDCur).Name 'Se realiza para determinar si ya existe una conexion abierta
        If ERR.Number <> 0 Then
            ERR.Number = 0
            Set BDDAO(BDCurCon) = OpenDatabase(DirDB & NombBD & ".mdb", False, False, ";pwd=" & cGClave)
            BoOpenBD = True
        End If
        BDDAO(BDCurCon).QueryDefs.Delete stNombre
        BDDAO(BDCurCon).TableDefs.Refresh
        If BoOpenBD Then
            BDDAO(BDCurCon).Close
            Set BDDAO(BDCurCon) = Nothing
        End If
        'HRR M2655
    Else
        BD(BDCur).Execute StSql
    End If
        lgError = ERR.Number
On Error GoTo 0

    fnEliminarVista = IIf(lgError = 0, SUCCEED, FAIL)
    
End Function

Function fnCrearVista(stNombre As String, stSQLCmd As String, stDesc As String) As Integer '|.DR.|
'Crea una vista/consulta.
'El par�metro stDesc, agrega una descripci�n a la vista, no se puso opcional pues es mejor agregar siempre una descripci�n.
' stDesc es ignorado para Access.

    Dim StSql As String, vError As Long, cError As String
    Dim qdfNueva As DAO.QueryDef
    Dim BoOpenBD As Boolean 'HRR M2655
    Dim StNameCon As String 'HRR M2655

    BoOpenBD = False 'HRR M2655
    StSql = "CREATE VIEW " & stNombre & " AS " & stSQLCmd
    
    On Error Resume Next
    If MotorBD = "ACCESS" Then
        Set qdfNueva = New DAO.QueryDef
        qdfNueva.Name = stNombre
        qdfNueva.SQL = stSQLCmd
        'BDDAO.QueryDefs.Append qdfNueva
        'HRR M2655
        ERR.Number = 0
        StNameCon = BDDAO(BDCur).Name 'Se realiza para determinar si ya existe una conexion abierta
        If ERR.Number <> 0 Then
            ERR.Number = 0
            Set BDDAO(BDCurCon) = OpenDatabase(DirDB & NombBD & ".mdb", False, False, ";pwd=" & cGClave)
            BoOpenBD = True
        End If
        BDDAO(BDCurCon).QueryDefs.Append qdfNueva
        BDDAO(BDCurCon).TableDefs.Refresh
        If BoOpenBD Then
            BDDAO(BDCurCon).Close
            Set BDDAO(BDCurCon) = Nothing
        End If
        'HRR M2655
        
    Else
        
        BD(BDCur).Execute StSql & vbCrLf & "/* " & stDesc & " */"
    End If
        vError = ERR.Number
        cError = ERR.Description
On Error GoTo 0
    
    If vError <> 0 Then MsgBox cError, 16, "Error creando vista/consulta."
    
    fnCrearVista = IIf(vError = 0, SUCCEED, FAIL)
    
End Function

Function fnExisteVista(stNombre As String) As Boolean '|.DR.|
'Devuelve "verdadero" si la vista/consulta ya existe.
'    Dim stTest As String, lgError As Long       'DEPURACION DE CODIGO
    Dim lgError As Long
    Dim BoOpenBD As Boolean 'HRR M2655
    Dim StCadena As String 'HRR M2655
   
   BoOpenBD = False 'HRR M2655
On Error Resume Next
    If MotorBD = "ACCESS" Then
        'stTest = BDDAO.QueryDefs(stNombre).Name
        'HRR M2655
        ERR.Number = 0
        StCadena = BDDAO(BDCur).Name 'Se realiza para determinar si ya existe una conexion abierta
        If ERR.Number <> 0 Then
            ERR.Number = 0
            Set BDDAO(BDCurCon) = OpenDatabase(DirDB & NombBD & ".mdb", False, False, ";pwd=" & cGClave)
            BoOpenBD = True
        End If
        If ERR.Number = 0 Then
           StCadena = BDDAO(BDCurCon).QueryDefs(stNombre).Name 'si existe no se genera error
        End If
        
        If BoOpenBD Then
            BDDAO(BDCurCon).Close
            Set BDDAO(BDCurCon) = Nothing
        End If
        'HRR M2655
    Else
        BD(BDCur).Execute "Select Top 1 * From " & stNombre
    End If
        lgError = ERR.Number
On Error GoTo 0

    fnExisteVista = (lgError = 0)

End Function
Sub ActualizarBDSQL()
Dim i As Double
Dim J, PosPunto, Revision As Byte
Dim Inicio As Double
Dim Version As String
ReDim Arr(0)
Call MouseClock
Valores = "Realice un Backup de Su base de datos Antes de Actualizar"
Valores = Valores & Chr(13) & Chr(13) & "Desea Continuar?"
If WarnMsg(Valores) Then
   Result = LoadData("PARAMETROS_INVE", "NU_NVERDB_PINV", "", Arr) 'N�mero de versi�n de la la base de datos
   If Encontro Then
      If MotorBD = "ACCESS" Then Set BDDAO(BDCurCon) = Workspaces(0).OpenDatabase(DirDB & NombBD & ".mdb", False, False, ";pwd=TRIACON")
      PosPunto = InStr(1, Arr(0), ".")
      If PosPunto > 0 Then
         VersionAct = Mid(Arr(0), 1, PosPunto + 1)
         Inicio = VersionAct
         PosPunto = InStr(PosPunto + 1, Arr(0), ".")
         If PosPunto > 0 Then
            Revision = Right(Arr(0), 1)
            Revision = Revision + 1
         End If
      Else
         VersionAct = Arr(0)
         Inicio = Val(Arr(0))
      End If
      'If Inicio > 5 Then Inicio = Inicio + 0.1   'edgar
      CambiarResol = False
      For i = Inicio To 12 Step 0.1 'Versi�n actual
         If CambiarResol = False Then
            If Round(i, 1) >= 5 Then
               For J = Revision To 9
                  Version = CStr(Round(i, 1)) & "." & J
                  Result = ActualizaVersion(Version)
                  Debug.Print Version
               Next
            Else
               Debug.Print CStr(Round(i, 1))
               Result = ActualizaVersion(CStr(Round(i, 1)))
            End If
         End If
         Revision = 0
      Next
      Beep
      If MotorBD = "ACCESS" Then BDDAO(BDCurCon).Close: Set BDDAO(BDCurCon) = Nothing
      Call Mensaje1("La actualizaci�n ha finalizado.", 3)
      MDI_Inventarios.MnuACTUALIZA.Visible = False 'HRR M3032
   End If
End If
Call MouseNorm
End Sub

Function ActualizaVersion(OPC As String) As Integer
'End Function
'
'Sub AdicionesALaBase()
    Dim vContar As Integer
    Dim ArrINTRFC() As Variant, vNumero As Long
'    Dim CMP As String, Dsd As String, Cdc As String    'DEPURACION DE CODIGO
'    Dim ArrImp() As Variant, vCon As Integer, vCue As String   'DEPURACION DE CODIGO
'No.  ACCESS              SQL         ORACLE          CAPACIDAD
'1    BIT(SI/NO)          BIT         NUMBER(1)         0 o 1
'2    BYTE                TINYINT     NUMBER(3)     Entero 0 a 255
'3    SMALLINT(ENTERO)    SMALLINT    NUMBER(4)     Entero -32.768 a 32.767
'4    INT(ENTERO LARGO)   INT         NUMBER(9)     Entero -2.147.483.648 a 2.147.483.647
'6    REAL(SIMPLE)        REAL                      -3,40E + 38 a 3,40E+38
'7    FLOAT(DOBLE)        FLOAT       NUMBER(15,2)  -1,79E + 308 a 1,79E +308
'8    DATETIME(FECHA/HORA)DATETIME    DATE
'10   TEXT(TEXTO)         VARCHAR     VARCHAR2      Requiere tama�o
'12   MEMO                TEXT        LONG

    ReDim ArrINTRFC(0)
    Select Case OPC
    Case "5.1.0":
        Result = CrearCampo("ARTICULO", "NU_UTIL_ARTI", 6, 4, 1, False)
        Result = CrearCampo("ARTICULO", "NU_SUMO_ARTI", 6, 4, 1, False)
        Result = CrearCampo("GRUP_ARTICULO", "CD_AJDB_GRUP", 10, 14, 1, False)
        Result = CrearCampo("GRUP_ARTICULO", "CD_AJCR_GRUP", 10, 14, 1, False)
    
    'R_GRUP_DEPE (CD_AJDB_RGD, CD_AJCR_RGD)
    '    Result = CrearCampo("R_GRUP_DEPE", "CD_AJDB_RGD", 10, 14, 1, False)
    '    Result = CrearCampo("R_GRUP_DEPE", "CD_AJCR_RGD", 10, 14, 1, False)
        Result = CrearCampo("PARAMETROS_INVE", "ID_UTILID_PINV", 10, 1, 1, False)
        Result = CrearCampo("PARAMETROS_INVE", "CD_DONA_APLI", 10, 14, 1, False)
        Result = CrearCampo("R_GRUP_DEPE", "CD_AJDB_RGD", 10, 14, 1, False)
        Result = CrearCampo("R_GRUP_DEPE", "CD_AJCR_RGD", 10, 14, 1, False)
        
        Result = CrearCampo("IN_ENCABEZADO", "FE_VIGE_ENCA", 8, 8, 1, False)
        Result = CrearCampo("IN_ENCABEZADO", "TX_CXPE_ENCA", 10, 1, 1, False)
'NU_COMP_CONTA_ENCA
        Result = CrearCampo("IN_ENCABEZADO", "NU_COMP_CONTA_ENCA", 4, 4, 1, False)

        Result = CrearTabla("IN_CRITERIO", "TX_DESC_CRIT, 10, 30, 1")
        Result = CrearCampo("IN_CRITERIO", "NU_AUTO_CRIT", 4, 4, 1, True)
        Result = CrearCampo("IN_CRITERIO", "TX_CODI_CRIT", 10, 10, 1, False)
        Result = 0: vContar = 5
        While vContar > 0 And Result = 0
        Result = CrearIndice("IN_CRITERIO", "PK_IN_CRITERIO", "NU_AUTO_CRIT", True)
            vContar = vContar - 1
        Wend
        Result = 0: vContar = 5
        While vContar > 0 And Result = 0
        Result = CrearIndice("IN_CRITERIO", "LL_IN_CRITERIO", "TX_CODI_CRIT", False, True)
            vContar = vContar - 1
        Wend
        Result = CrearTabla("IN_R_CRITERIO_TERCERO", "NU_AUTO_CRIT_RCRTE, 4, 4, 0")
        Result = CrearCampo("IN_R_CRITERIO_TERCERO", "CD_CODI_TERC_RCRTE", 10, 11, 0, False)
        Result = CrearCampo("IN_R_CRITERIO_TERCERO", "NU_CALI_RCRTE", 7, 8, 0, False)
        Result = CrearCampo("IN_R_CRITERIO_TERCERO", "NU_PESO_RCRTE", 7, 8, 0, False)
        Result = 0: vContar = 5
        While vContar > 0 And Result = 0
            Result = CrearIndice("IN_R_CRITERIO_TERCERO", "PK_IN_R_CRITERIO_TERCERO", "NU_AUTO_CRIT_RCRTE, CD_CODI_TERC_RCRTE", True)
            vContar = vContar - 1
        Wend
        Result = 0: vContar = 5
        While vContar > 0 And Result = 0
        Result = CrearRelacion("IN_R_CRITERIO_TERCEROCRITERIO", "NU_AUTO_CRIT_RCRTE", "NU_AUTO_CRIT", "IN_R_CRITERIO_TERCERO", "IN_CRITERIO")
            vContar = vContar - 1
        Wend
        Result = 0: vContar = 5
        While vContar > 0 And Result = 0
        Result = CrearRelacion("IN_R_CRITERIO_TERCEROTERCERO", "CD_CODI_TERC_RCRTE", "CD_CODI_TERC", "IN_R_CRITERIO_TERCERO", "TERCERO")
            vContar = vContar - 1
        Wend
    'IN_CONDICOMERCIAL(NU_AUTO_ENCA_CDCL,ME_CDCL_CDCL)
        Result = CrearTabla("IN_CONDICOMERCIAL", "NU_AUTO_ENCA_CDCL, 4, 4, 0")
        Result = CrearCampo("IN_CONDICOMERCIAL", "ME_CDCL_CDCL", 12, 4000, 1)
        Result = 0: vContar = 5
        While vContar > 0 And Result = 0
            Result = CrearIndice("IN_CONDICOMERCIAL", "LL_IN_CONDICOMERCIAL", "NU_AUTO_ENCA_CDCL", False, True)
            vContar = vContar - 1
        Wend
        Result = 0: vContar = 5
        While vContar > 0 And Result = 0
        Result = CrearRelacion("IN_CONDICOMERCIALIN_ENCABEZADO", "NU_AUTO_ENCA_CDCL", "NU_AUTO_ENCA", "IN_CONDICOMERCIAL", "IN_ENCABEZADO")
            vContar = vContar - 1
        Wend
    'IN_PROYETOCOMPRA (NU_AUTO_PRYC,FE_CREA_PRYC,TX_DESC_PRYC)
        Result = CrearTabla("IN_PROYETOCOMPRA", "FE_CREA_PRYC, 8, 8, 0")
        Result = CrearCampo("IN_PROYETOCOMPRA", "NU_AUTO_PRYC", 4, 4, 1, True)
        Result = CrearCampo("IN_PROYETOCOMPRA", "TX_DESC_PRYC", 10, 250, 1, False)
        Result = 0: vContar = 5
        While vContar > 0 And Result = 0
            Result = CrearIndice("IN_PROYETOCOMPRA", "PK_IN_PROYETOCOMPRA", "NU_AUTO_PRYC", True)
            vContar = vContar - 1
        Wend
    'IN_PROVEEDORPROYECTO (NU_AUTO_PROY_PROPY,CD_CODI_TERC_PROPY,NU_CALI_PROPY,NU_PORC_PROPY)
        Result = CrearTabla("IN_PROVEEDORPROYECTO", "NU_AUTO_PROY_PROPY, 4, 4, 0")
        Result = CrearCampo("IN_PROVEEDORPROYECTO", "CD_CODI_TERC_PROPY", 10, 11, 0, False)
        Result = CrearCampo("IN_PROVEEDORPROYECTO", "NU_CALI_PROPY", 3, 2, 0, False)
        Result = CrearCampo("IN_PROVEEDORPROYECTO", "NU_PORC_PROPY", 3, 2, 0, False)
        Result = 0: vContar = 5
        While vContar > 0 And Result = 0
            Result = CrearRelacion("IN_PROYETOCOMPRAIN_PROVEEDORPROYECTO", "NU_AUTO_PROY_PROPY", "NU_AUTO_PRYC", "IN_PROVEEDORPROYECTO", "IN_PROYETOCOMPRA")
            vContar = vContar - 1
        Wend
        Result = 0: vContar = 5
        While vContar > 0 And Result = 0
            Result = CrearRelacion("IN_PROVEEDORPROYECTOTERCERO", "CD_CODI_TERC_PROPY", "CD_CODI_TERC", "IN_PROVEEDORPROYECTO", "TERCERO")
            vContar = vContar - 1
        Wend
    'IN_ARTICULOSELECCION (NU_AUTO_PROY_ARSE,NU_AUTO_ARTI_ARSE,CD_CODI_TERC_ARSE,NU_CANT_ARSE,NU_COST_ARSE,NU_CALI_ARSE,NU_ORDE_ARSE)
        Result = CrearTabla("IN_ARTICULOSELECCION", "NU_AUTO_PROY_ARSE, 4, 4, 0")
        Result = CrearCampo("IN_ARTICULOSELECCION", "NU_AUTO_ARTI_ARSE", 4, 4, 0, False)
        Result = CrearCampo("IN_ARTICULOSELECCION", "CD_CODI_TERC_ARSE", 10, 11, 0, False)
        Result = CrearCampo("IN_ARTICULOSELECCION", "NU_CANT_ARSE", 3, 2, 1, False)
        Result = CrearCampo("IN_ARTICULOSELECCION", "NU_COST_ARSE", 7, 8, 1, False)
        Result = CrearCampo("IN_ARTICULOSELECCION", "NU_CALI_ARSE", 3, 2, 1, False)
        Result = CrearCampo("IN_ARTICULOSELECCION", "NU_ORDE_ARSE", 3, 2, 0, False)
        Result = 0: vContar = 5
        While vContar > 0 And Result = 0
            Result = CrearRelacion("IN_PROYETOCOMPRAARTICULO", "NU_AUTO_ARTI_ARSE", "NU_AUTO_ARTI", "IN_ARTICULOSELECCION", "ARTICULO")
            vContar = vContar - 1
        Wend
        Result = 0: vContar = 5
        While vContar > 0 And Result = 0
            Result = CrearRelacion("IN_PROYETOCOMPRAIN_ARTICULOSELECCION", "NU_AUTO_PROY_ARSE", "NU_AUTO_PRYC", "IN_ARTICULOSELECCION", "IN_PROYETOCOMPRA")
            vContar = vContar - 1
        Wend
        Result = 0: vContar = 5
        While vContar > 0 And Result = 0
            Result = CrearRelacion("IN_ARTICULOSELECCIONTERCERO", "CD_CODI_TERC_ARSE", "CD_CODI_TERC", "IN_ARTICULOSELECCION", "TERCERO")
            vContar = vContar - 1
        Wend
    'NU_MOVI_CONC    CD_COMP_CONC    CD_IVA_CONC CD_RIVA_CONC    CD_ICA_CONC CD_RETE_CONC
        
        If ExisteTABLA("CONCEPTOT") Then BD(BDCurCon).Execute ("DELETE FROM CONCEPTOT")
        Result = 0: vContar = 5
        While vContar > 0 And Result = 0
            Result = CrearRelacion("CONCEPTOTTC_COMPROBANTE", "CD_COMP_CONC", "CD_CODI_COMP", "CONCEPTOT", "TC_COMPROBANTE")
            vContar = vContar - 1
        Wend
        'ENTR,DVEN,APDN,SALI,VENT,BAJA

        Result = LoadData("TC_COMPROBANTE", "CD_CODI_COMP", "CD_CODI_COMP='ENA'", ArrINTRFC())
        If Result <> FAIL Then If Not Encontro Then Result = DoInsertSQL("TC_COMPROBANTE", "CD_CODI_COMP='ENA', NO_NOMB_COMP='ENTRADA DE ALMACEN', NU_CONS_COMP=0, ID_SIIF_COMP='N', ID_TRAN_COMP='N'")
        Result = LoadData("MOV_CONTABLE", "MAX(NU_CONS_MOVC)", "CD_CONC_MOVC='ENTR'", ArrINTRFC())
        If Result <> FAIL Then If Encontro And Len(ArrINTRFC(0)) > 0 Then vNumero = CLng(ArrINTRFC(0)): Result = DoUpdate("TC_COMPROBANTE", "NU_CONS_COMP=" & vNumero, "CD_CODI_COMP='ENA'")
        
        Result = LoadData("TC_COMPROBANTE", "CD_CODI_COMP", "CD_CODI_COMP='SLD'", ArrINTRFC())
        If Result <> FAIL Then If Not Encontro Then Result = DoInsertSQL("TC_COMPROBANTE", "CD_CODI_COMP='SLD', NO_NOMB_COMP='SALIDA DE BODEGA', NU_CONS_COMP=0, ID_SIIF_COMP='N', ID_TRAN_COMP='N'")
        Result = LoadData("MOV_CONTABLE", "MAX(NU_CONS_MOVC)", "CD_CONC_MOVC='SALI'", ArrINTRFC())
        If Result <> FAIL Then If Encontro And Len(ArrINTRFC(0)) > 0 Then vNumero = CLng(ArrINTRFC(0)): Result = DoUpdate("TC_COMPROBANTE", "NU_CONS_COMP=" & vNumero, "CD_CODI_COMP='SLD'")
        
        Result = LoadData("TC_COMPROBANTE", "CD_CODI_COMP", "CD_CODI_COMP='VTI'", ArrINTRFC())
        If Result <> FAIL Then If Not Encontro Then Result = DoInsertSQL("TC_COMPROBANTE", "CD_CODI_COMP='VTI', NO_NOMB_COMP='VENTA DE MERCANCIA', NU_CONS_COMP=0, ID_SIIF_COMP='N', ID_TRAN_COMP='N'")
        Result = LoadData("MOV_CONTABLE", "MAX(NU_CONS_MOVC)", "CD_CONC_MOVC='VENT'", ArrINTRFC())
        If Result <> FAIL Then If Encontro And Len(ArrINTRFC(0)) > 0 Then vNumero = CLng(ArrINTRFC(0)): Result = DoUpdate("TC_COMPROBANTE", "NU_CONS_COMP=" & vNumero, "CD_CODI_COMP='VTI'")
        
        Result = LoadData("TC_COMPROBANTE", "CD_CODI_COMP", "CD_CODI_COMP='BJP'", ArrINTRFC())
        If Result <> FAIL Then If Not Encontro Then Result = DoInsertSQL("TC_COMPROBANTE", "CD_CODI_COMP='BJP', NO_NOMB_COMP='BAJA DE PACIENTES', NU_CONS_COMP=0, ID_SIIF_COMP='N', ID_TRAN_COMP='N'")
        Result = LoadData("MOV_CONTABLE", "MAX(NU_CONS_MOVC)", "CD_CONC_MOVC='BAJA'", ArrINTRFC())
        If Result <> FAIL Then If Encontro And Len(ArrINTRFC(0)) > 0 Then vNumero = CLng(ArrINTRFC(0)): Result = DoUpdate("TC_COMPROBANTE", "NU_CONS_COMP=" & vNumero, "CD_CODI_COMP='BJP'")
        
        Result = LoadData("TC_COMPROBANTE", "CD_CODI_COMP", "CD_CODI_COMP='DDC'", ArrINTRFC())
        If Result <> FAIL Then If Not Encontro Then Result = DoInsertSQL("TC_COMPROBANTE", "CD_CODI_COMP='DDC', NO_NOMB_COMP='DEVOLUCION DE CLIENTE', NU_CONS_COMP=0, ID_SIIF_COMP='N', ID_TRAN_COMP='N'")
        
        Result = LoadData("TC_COMPROBANTE", "CD_CODI_COMP", "CD_CODI_COMP='DAP'", ArrINTRFC())
        If Result <> FAIL Then If Not Encontro Then Result = DoInsertSQL("TC_COMPROBANTE", "CD_CODI_COMP='DAP', NO_NOMB_COMP='DEVOLUCION A PROVEEDOR', NU_CONS_COMP=0, ID_SIIF_COMP='N', ID_TRAN_COMP='N'")
        Result = LoadData("MOV_CONTABLE", "MAX(NU_CONS_MOVC)", "CD_CONC_MOVC='DVEN'", ArrINTRFC())
        If Result <> FAIL Then If Encontro And Len(ArrINTRFC(0)) > 0 Then vNumero = CLng(ArrINTRFC(0)): Result = DoUpdate("TC_COMPROBANTE", "NU_CONS_COMP=" & vNumero, "CD_CODI_COMP='DAP'")
        
        Result = LoadData("TC_COMPROBANTE", "CD_CODI_COMP", "CD_CODI_COMP='APD'", ArrINTRFC())
        If Result <> FAIL Then If Not Encontro Then Result = DoInsertSQL("TC_COMPROBANTE", "CD_CODI_COMP='APD', NO_NOMB_COMP='APROVECHAMIENTOS/DONACIONES', NU_CONS_COMP=0, ID_SIIF_COMP='N', ID_TRAN_COMP='N'")
        Result = LoadData("MOV_CONTABLE", "MAX(NU_CONS_MOVC)", "CD_CONC_MOVC='APDN'", ArrINTRFC())
        If Result <> FAIL Then If Encontro And Len(ArrINTRFC(0)) > 0 Then vNumero = CLng(ArrINTRFC(0)): Result = DoUpdate("TC_COMPROBANTE", "NU_CONS_COMP=" & vNumero, "CD_CODI_COMP='APD'")
        
        Result = LoadData("TC_COMPROBANTE", "CD_CODI_COMP", "CD_CODI_COMP='AJI'", ArrINTRFC())
        If Result <> FAIL Then If Not Encontro Then Result = DoInsertSQL("TC_COMPROBANTE", "CD_CODI_COMP='AJI', NO_NOMB_COMP='AJUSTES X INFLACION', NU_CONS_COMP=0, ID_SIIF_COMP='N', ID_TRAN_COMP='N'")
'PARAMETROS_CXP
        Result = LoadData("C_X_P", "MAX(CD_NUME_CXP)", "", ArrINTRFC())
        If Result <> FAIL Then If Encontro And Len(ArrINTRFC(0)) > 0 Then vNumero = CLng(ArrINTRFC(0)): Result = DoUpdate("PARAMETROS_CXP", "NU_CXPA_APLI=" & vNumero, "")
'PARAMETROS_CXC
        Result = LoadData("C_X_C", "MAX(CD_NUME_CXC)", "", ArrINTRFC())
        If Result <> FAIL Then If Encontro And Len(ArrINTRFC(0)) > 0 Then vNumero = CLng(ArrINTRFC(0)): Result = DoUpdate("PARAMETROS_CXC", "NU_CXCO_APLI=" & vNumero, "")

'        BD(BDCurCon).Execute ("INSERT INTO TC_COMPROBANTE (CD_CODI_COMP,NO_NOMB_COMP,NU_CONS_COMP,ID_SIIF_COMP,ID_TRAN_COMP) VALUES ('ENA', 'ENTRADA DE ALMACEN', 0, 'N', 'N')")
'        BD(BDCurCon).Execute ("INSERT INTO TC_COMPROBANTE (CD_CODI_COMP,NO_NOMB_COMP,NU_CONS_COMP,ID_SIIF_COMP,ID_TRAN_COMP) VALUES ('SLD', 'SALIDA DE BODEGA', 0, 'N', 'N')")
'        BD(BDCurCon).Execute ("INSERT INTO TC_COMPROBANTE (CD_CODI_COMP,NO_NOMB_COMP,NU_CONS_COMP,ID_SIIF_COMP,ID_TRAN_COMP) VALUES ('VTI', 'VENTA DE MERCANCIA', 0, 'N', 'N')")
'        BD(BDCurCon).Execute ("INSERT INTO TC_COMPROBANTE (CD_CODI_COMP,NO_NOMB_COMP,NU_CONS_COMP,ID_SIIF_COMP,ID_TRAN_COMP) VALUES ('BJP', 'BAJA DE PACIENTES', 0, 'N', 'N')")
'        BD(BDCurCon).Execute ("INSERT INTO TC_COMPROBANTE (CD_CODI_COMP,NO_NOMB_COMP,NU_CONS_COMP,ID_SIIF_COMP,ID_TRAN_COMP) VALUES ('DDC', 'DEVOLUCION DE CLIENTE', 0, 'N', 'N')")
'        BD(BDCurCon).Execute ("INSERT INTO TC_COMPROBANTE (CD_CODI_COMP,NO_NOMB_COMP,NU_CONS_COMP,ID_SIIF_COMP,ID_TRAN_COMP) VALUES ('DAP', 'DEVOLUCION A PROVEEDOR', 0, 'N', 'N')")
'        BD(BDCurCon).Execute ("INSERT INTO TC_COMPROBANTE (CD_CODI_COMP,NO_NOMB_COMP,NU_CONS_COMP,ID_SIIF_COMP,ID_TRAN_COMP) VALUES ('APD', 'APROVECHAMIENTOS/DONACIONES', 0, 'N', 'N')")
'        BD(BDCurCon).Execute ("INSERT INTO TC_COMPROBANTE (CD_CODI_COMP,NO_NOMB_COMP,NU_CONS_COMP,ID_SIIF_COMP,ID_TRAN_COMP) VALUES ('AJI', 'AJUSTES X INFLACION', 0, 'N', 'N')")
        
        Result = LoadData("CONCEPTOT", "CD_COMP_CONC", "NU_MOVI_CONC=3 AND CD_COMP_CONC='ENA'", ArrINTRFC())
        If Result <> FAIL Then If Not Encontro Then Result = DoInsertSQL("CONCEPTOT", "NU_MOVI_CONC=3,CD_COMP_CONC='ENA'")
        Result = LoadData("CONCEPTOT", "CD_COMP_CONC", "NU_MOVI_CONC=19 AND CD_COMP_CONC='SLD'", ArrINTRFC())
        If Result <> FAIL Then If Not Encontro Then Result = DoInsertSQL("CONCEPTOT", "NU_MOVI_CONC=19,CD_COMP_CONC='SLD'")
        Result = LoadData("CONCEPTOT", "CD_COMP_CONC", "NU_MOVI_CONC=11 AND CD_COMP_CONC='VTI'", ArrINTRFC())
        If Result <> FAIL Then If Not Encontro Then Result = DoInsertSQL("CONCEPTOT", "NU_MOVI_CONC=11,CD_COMP_CONC='VTI'")
        Result = LoadData("CONCEPTOT", "CD_COMP_CONC", "NU_MOVI_CONC=8 AND CD_COMP_CONC='BJP'", ArrINTRFC())
        If Result <> FAIL Then If Not Encontro Then Result = DoInsertSQL("CONCEPTOT", "NU_MOVI_CONC=8,CD_COMP_CONC='BJP'")
        Result = LoadData("CONCEPTOT", "CD_COMP_CONC", "NU_MOVI_CONC=12 AND CD_COMP_CONC='DDC'", ArrINTRFC())
        If Result <> FAIL Then If Not Encontro Then Result = DoInsertSQL("CONCEPTOT", "NU_MOVI_CONC=12,CD_COMP_CONC='DDC'")
        Result = LoadData("CONCEPTOT", "CD_COMP_CONC", "NU_MOVI_CONC=4 AND CD_COMP_CONC='DAP'", ArrINTRFC())
        If Result <> FAIL Then If Not Encontro Then Result = DoInsertSQL("CONCEPTOT", "NU_MOVI_CONC=4,CD_COMP_CONC='DAP'")
        Result = LoadData("CONCEPTOT", "CD_COMP_CONC", "NU_MOVI_CONC=26 AND CD_COMP_CONC='APD'", ArrINTRFC())
        If Result <> FAIL Then If Not Encontro Then Result = DoInsertSQL("CONCEPTOT", "NU_MOVI_CONC=26,CD_COMP_CONC='APD'")
        Result = LoadData("CONCEPTOT", "CD_COMP_CONC", "NU_MOVI_CONC=31 AND CD_COMP_CONC='AJI'", ArrINTRFC())
        If Result <> FAIL Then If Not Encontro Then Result = DoInsertSQL("CONCEPTOT", "NU_MOVI_CONC=31,CD_COMP_CONC='AJI'")

'        BD(BDCurCon).Execute ("INSERT INTO CONCEPTOT (NU_MOVI_CONC,CD_COMP_CONC) VALUES (3,'ENA')")
'        BD(BDCurCon).Execute ("INSERT INTO CONCEPTOT (NU_MOVI_CONC,CD_COMP_CONC) VALUES (19,'SLD')")
'        BD(BDCurCon).Execute ("INSERT INTO CONCEPTOT (NU_MOVI_CONC,CD_COMP_CONC) VALUES (11,'VTI')")
'        BD(BDCurCon).Execute ("INSERT INTO CONCEPTOT (NU_MOVI_CONC,CD_COMP_CONC) VALUES (8,'BJP')")
'        BD(BDCurCon).Execute ("INSERT INTO CONCEPTOT (NU_MOVI_CONC,CD_COMP_CONC) VALUES (12,'DDC')")
'        BD(BDCurCon).Execute ("INSERT INTO CONCEPTOT (NU_MOVI_CONC,CD_COMP_CONC) VALUES (4,'DAP')")
'        BD(BDCurCon).Execute ("INSERT INTO CONCEPTOT (NU_MOVI_CONC,CD_COMP_CONC) VALUES (26,'APD')")
'        BD(BDCurCon).Execute ("INSERT INTO CONCEPTOT (NU_MOVI_CONC,CD_COMP_CONC) VALUES (31,'AJI')")
        
                        'ENA, ENTRADA DE ALMACEN, 3
                        'SLD, SALIDA DE BODEGA, 19
                        'VTI, VENTA DE MERCANCIA, 11
                        'BJP, BAJA DE PACIENTES, 8
                        'DDC, DEVOLUCION DE CLIENTE, 12
                        'DAP, DEVOLUCION A PROVEEDOR, 4
                        'APD, APROVECHAMIENTOS/DONACIONES, 26
                        'AJI, AJUSTES X INFLACION, 31
        
        Result = LoadData("IN_DOCUMENTO", "NU_AUTO_DOCU", "NU_AUTO_DOCU=26", ArrINTRFC())
        If Result <> FAIL Then If Not Encontro Then Result = DoInsertSQL("IN_DOCUMENTO", "NU_AUTO_DOCU='26',TX_CODI_DOCU='APRDON',TX_NOMB_DOCU='ENTRADA APROVECHA/DONACION',TX_AFEC_DOCU='S',TX_INTE_DOCU='N',NU_AUTO_DOCUDEPE_DOCU='2',NU_AUTO_DOCUREVE_DOCU='27',TX_TIPOVAL_DOCU='C',TX_INDE_DOCU='S'")
        Result = LoadData("IN_DOCUMENTO", "NU_AUTO_DOCU", "NU_AUTO_DOCU=27", ArrINTRFC())
        If Result <> FAIL Then If Not Encontro Then Result = DoInsertSQL("IN_DOCUMENTO", "NU_AUTO_DOCU='27',TX_CODI_DOCU='ANLAPR',TX_NOMB_DOCU='ANULACION ENTRADA APROVECHA/DONACION',TX_AFEC_DOCU='S',TX_INTE_DOCU='N',NU_AUTO_DOCUDEPE_DOCU='26',NU_AUTO_DOCUREVE_DOCU='0',TX_TIPOVAL_DOCU='C',TX_INDE_DOCU='N'")
        Result = LoadData("IN_DOCUMENTO", "NU_AUTO_DOCU", "NU_AUTO_DOCU=28", ArrINTRFC())
        If Result <> FAIL Then If Not Encontro Then Result = DoInsertSQL("IN_DOCUMENTO", "NU_AUTO_DOCU='28',TX_CODI_DOCU='DEVAPR',TX_NOMB_DOCU='DEVOLUCION ENTRADA APROVECHA/DONACION',TX_AFEC_DOCU='S',TX_INTE_DOCU='N',NU_AUTO_DOCUDEPE_DOCU='26',NU_AUTO_DOCUREVE_DOCU='0',TX_TIPOVAL_DOCU='C',TX_INDE_DOCU='N'")
        Result = LoadData("IN_DOCUMENTO", "NU_AUTO_DOCU", "NU_AUTO_DOCU=29", ArrINTRFC())
        If Result <> FAIL Then If Not Encontro Then Result = DoInsertSQL("IN_DOCUMENTO", "NU_AUTO_DOCU='29',TX_CODI_DOCU='LISTAS',TX_NOMB_DOCU='COTIZACION/LISTA DE PRECIOS',TX_AFEC_DOCU='N',TX_INTE_DOCU='N',NU_AUTO_DOCUDEPE_DOCU='0',NU_AUTO_DOCUREVE_DOCU='30',TX_TIPOVAL_DOCU='C',TX_INDE_DOCU='S'")
        Result = LoadData("IN_DOCUMENTO", "NU_AUTO_DOCU", "NU_AUTO_DOCU=30", ArrINTRFC())
        If Result <> FAIL Then If Not Encontro Then Result = DoInsertSQL("IN_DOCUMENTO", "NU_AUTO_DOCU='30',TX_CODI_DOCU='ANLSOL',TX_NOMB_DOCU='ANULACION COTIZACION/LISTA DE PRECIOS',TX_AFEC_DOCU='N',TX_INTE_DOCU='N',NU_AUTO_DOCUDEPE_DOCU='1',NU_AUTO_DOCUREVE_DOCU='0',TX_TIPOVAL_DOCU='C',TX_INDE_DOCU='N'")
        
'        BD(BDCurCon).Execute ("INSERT INTO IN_DOCUMENTO (NU_AUTO_DOCU, TX_CODI_DOCU, TX_NOMB_DOCU, TX_AFEC_DOCU, TX_INTE_DOCU, NU_AUTO_DOCUDEPE_DOCU, NU_AUTO_DOCUREVE_DOCU,TX_TIPOVAL_DOCU, TX_INDE_DOCU) VALUES ('26','APRDON','ENTRADA APROVECHA/DONACION','S','N','2','27','C','S')")
'        BD(BDCurCon).Execute ("INSERT INTO IN_DOCUMENTO (NU_AUTO_DOCU, TX_CODI_DOCU, TX_NOMB_DOCU, TX_AFEC_DOCU, TX_INTE_DOCU, NU_AUTO_DOCUDEPE_DOCU, NU_AUTO_DOCUREVE_DOCU,TX_TIPOVAL_DOCU, TX_INDE_DOCU) VALUES ('27','ANLAPR','ANULACION ENTRADA APROVECHA/DONACION','S','N','26','0','C','N')")
'        BD(BDCurCon).Execute ("INSERT INTO IN_DOCUMENTO (NU_AUTO_DOCU, TX_CODI_DOCU, TX_NOMB_DOCU, TX_AFEC_DOCU, TX_INTE_DOCU, NU_AUTO_DOCUDEPE_DOCU, NU_AUTO_DOCUREVE_DOCU,TX_TIPOVAL_DOCU, TX_INDE_DOCU) VALUES ('28','DEVAPR','DEVOLUCION ENTRADA APROVECHA/DONACION','S','N','26','0','C','N')")
'        BD(BDCurCon).Execute ("INSERT INTO IN_DOCUMENTO (NU_AUTO_DOCU, TX_CODI_DOCU, TX_NOMB_DOCU, TX_AFEC_DOCU, TX_INTE_DOCU, NU_AUTO_DOCUDEPE_DOCU, NU_AUTO_DOCUREVE_DOCU,TX_TIPOVAL_DOCU, TX_INDE_DOCU) VALUES ('29','LISTAS','COTIZACION/LISTA DE PRECIOS','N','N','0','30','C','S')")
'        BD(BDCurCon).Execute ("INSERT INTO IN_DOCUMENTO (NU_AUTO_DOCU, TX_CODI_DOCU, TX_NOMB_DOCU, TX_AFEC_DOCU, TX_INTE_DOCU, NU_AUTO_DOCUDEPE_DOCU, NU_AUTO_DOCUREVE_DOCU,TX_TIPOVAL_DOCU, TX_INDE_DOCU) VALUES ('30','ANLSOL','ANULACION COTIZACION/LISTA DE PRECIOS','N','N','1','0','C','N')")
        
        Result = LoadData("FORMULARIO", "TX_DESC_FORM", "TX_DESC_FORM='COMPRA' AND TX_FORM_FORM='FrmFASECOMPRA' AND TX_TIPO_FORM='FRM'", ArrINTRFC())
        If Result <> FAIL Then If Not Encontro Then Result = DoInsertSQL("FORMULARIO", "TX_DESC_FORM='COMPRA',TX_FORM_FORM='FrmFASECOMPRA',TX_TIPO_FORM='FRM'")
        
        Result = LoadData("FORMULARIO", "TX_DESC_FORM", "TX_FORM_FORM='FrmGENERAL' AND TX_TIPO_FORM='26'", ArrINTRFC())
        If Result <> FAIL Then If Not Encontro Then Result = DoInsertSQL("FORMULARIO", "TX_DESC_FORM='APROVECHAMIENTO/DONACION',TX_FORM_FORM='FrmGENERAL',TX_TIPO_FORM='26'")
        
        Result = LoadData("FORMULARIO", "TX_DESC_FORM", "TX_FORM_FORM='FrmFISICO' AND TX_TIPO_FORM='FRM'", ArrINTRFC())
        If Result <> FAIL Then If Not Encontro Then Result = DoInsertSQL("FORMULARIO", "TX_DESC_FORM='INVENTARIO FISICO',TX_FORM_FORM='FrmFISICO',TX_TIPO_FORM='FRM'")
        
        Result = LoadData("FORMULARIO", "TX_DESC_FORM", "TX_FORM_FORM='FrmINFLA' AND TX_TIPO_FORM='FRM'", ArrINTRFC())
        If Result <> FAIL Then If Not Encontro Then Result = DoInsertSQL("FORMULARIO", "TX_DESC_FORM='AJUSTES X INFLACION',TX_FORM_FORM='FrmINFLA',TX_TIPO_FORM='FRM'")
        
        Result = LoadData("FORMULARIO", "TX_DESC_FORM", "TX_FORM_FORM='FrmOTREPORTE' AND TX_TIPO_FORM='FRM'", ArrINTRFC())
        If Result <> FAIL Then If Not Encontro Then Result = DoInsertSQL("FORMULARIO", "TX_DESC_FORM='OTROS INFORMES',TX_FORM_FORM='FrmOTREPORTE',TX_TIPO_FORM='FRM'")
        Result = LoadData("FORMULARIO", "TX_DESC_FORM", "TX_FORM_FORM='FrmGENERAL' AND TX_TIPO_FORM='29'", ArrINTRFC())
        If Result <> FAIL Then If Not Encontro Then Result = DoInsertSQL("FORMULARIO", "TX_DESC_FORM='COTIZACION/LISTA PRECIOS PROVEEDOR',TX_FORM_FORM='FrmGENERAL',TX_TIPO_FORM='29'")
        
        Result = LoadData("FORMULARIO", "TX_DESC_FORM", "TX_FORM_FORM='FrmCriterios' AND TX_TIPO_FORM='FRM'", ArrINTRFC())
        If Result <> FAIL Then If Not Encontro Then Result = DoInsertSQL("FORMULARIO", "TX_DESC_FORM='CRITERIOS DE COMPRA',TX_FORM_FORM='FrmCriterios',TX_TIPO_FORM='FRM'")

'    'FrmCriteXProve
        Result = LoadData("FORMULARIO", "TX_DESC_FORM", "TX_FORM_FORM='FrmCriteXProve' AND TX_TIPO_FORM='FRM'", ArrINTRFC())
        If Result <> FAIL Then If Not Encontro Then Result = DoInsertSQL("FORMULARIO", "TX_DESC_FORM='CRITERIOS X PROVEEDOR',TX_FORM_FORM='FrmCriteXProve',TX_TIPO_FORM='FRM'")
'    'FrmPROYCOMPRA
        Result = LoadData("FORMULARIO", "TX_DESC_FORM", "TX_FORM_FORM='FrmPROYCOMPRA' AND TX_TIPO_FORM='FRM'", ArrINTRFC())
        If Result <> FAIL Then If Not Encontro Then Result = DoInsertSQL("FORMULARIO", "TX_DESC_FORM='PROYECTO DE COMPRA',TX_FORM_FORM='FrmPROYCOMPRA',TX_TIPO_FORM='FRM'")
        
        Result = LoadData("FORMULARIO", "TX_DESC_FORM", "TX_FORM_FORM='FrmCONSUMO' AND TX_TIPO_FORM='FRM'", ArrINTRFC())
        If Result <> FAIL Then If Not Encontro Then Result = DoInsertSQL("FORMULARIO", "TX_DESC_FORM='CONSUMO PROMEDIO',TX_FORM_FORM='FrmCONSUMO',TX_TIPO_FORM='FRM'")
        
'        BD(BDCurCon).Execute ("INSERT INTO FORMULARIO (TX_DESC_FORM,TX_FORM_FORM,TX_TIPO_FORM) VALUES ('COMPRA','FrmFASECOMPRA','FRM')")
'        BD(BDCurCon).Execute ("INSERT INTO FORMULARIO (TX_DESC_FORM,TX_FORM_FORM,TX_TIPO_FORM) VALUES ('APROVECHAMIENTO/DONACION','FrmGENERAL',26)")
'        BD(BDCurCon).Execute ("INSERT INTO FORMULARIO (TX_DESC_FORM,TX_FORM_FORM,TX_TIPO_FORM) VALUES ('INVENTARIO FISICO','FrmFISICO','FRM')")
'        BD(BDCurCon).Execute ("INSERT INTO FORMULARIO (TX_DESC_FORM,TX_FORM_FORM,TX_TIPO_FORM) VALUES ('AJUSTES X INFLACION','FrmINFLA','FRM')")
'        BD(BDCurCon).Execute ("INSERT INTO FORMULARIO (TX_DESC_FORM,TX_FORM_FORM,TX_TIPO_FORM) VALUES ('OTROS INFORMES','FrmOTREPORTE','FRM')")
'        BD(BDCurCon).Execute ("INSERT INTO FORMULARIO (TX_DESC_FORM,TX_FORM_FORM,TX_TIPO_FORM) VALUES ('COTIZACION/LISTA PRECIOS PROVEEDOR','FrmGENERAL',29)")
'        BD(BDCurCon).Execute ("INSERT INTO FORMULARIO (TX_DESC_FORM,TX_FORM_FORM,TX_TIPO_FORM) VALUES ('CRITERIOS DE COMPRA','FrmCriterios','FRM')")
'    'FrmCriteXProve
'        BD(BDCurCon).Execute ("INSERT INTO FORMULARIO (TX_DESC_FORM,TX_FORM_FORM,TX_TIPO_FORM) VALUES ('CRITERIOS X PROVEEDOR','FrmCriteXProve','FRM')")
'    'FrmPROYCOMPRA
'        BD(BDCurCon).Execute ("INSERT INTO FORMULARIO (TX_DESC_FORM,TX_FORM_FORM,TX_TIPO_FORM) VALUES ('PROYECTO DE COMPRA','FrmPROYCOMPRA','FRM')")
        
    '    BD(BDCurCon).Execute ("INSERT INTO OPCION (TX_CODI_OPCI,TX_DESC_OPCI,TX_CODPADR_OPCI,NU_AUTO_FORM_OPCI) VALUES ('0325','APROVECHAMIENTO/DONACION','03','50')")
    '    BD(BDCurCon).Execute ("INSERT INTO OPCION (TX_CODI_OPCI,TX_DESC_OPCI,TX_CODPADR_OPCI,NU_AUTO_FORM_OPCI) VALUES ('06','INVENTARIO FISICO','CNT','51')")
    '    BD(BDCurCon).Execute ("INSERT INTO OPCION (TX_CODI_OPCI,TX_DESC_OPCI,TX_CODPADR_OPCI,NU_AUTO_FORM_OPCI) VALUES ('0503','AJUSTES X INFLACION','05','52')")
    '    BD(BDCurCon).Execute ("INSERT INTO OPCION (TX_CODI_OPCI,TX_DESC_OPCI,TX_CODPADR_OPCI,NU_AUTO_FORM_OPCI) VALUES ('0410','OTROS INFORMES','04','53')")
    '    BD(BDCurCon).Execute ("INSERT INTO OPCION (TX_CODI_OPCI,TX_DESC_OPCI,TX_CODPADR_OPCI,NU_AUTO_FORM_OPCI) VALUES ('0302','COTIZACION/LISTA PRECIOS','03','54')")
    '    BD(BDCurCon).Execute ("INSERT INTO OPCION (TX_CODI_OPCI,TX_DESC_OPCI,TX_CODPADR_OPCI,NU_AUTO_FORM_OPCI) VALUES ('0242','CRITERIOS DE COMPRA','02','55')")
    '    BD(BDCurCon).Execute ("INSERT INTO OPCION (TX_CODI_OPCI,TX_DESC_OPCI,TX_CODPADR_OPCI,NU_AUTO_FORM_OPCI) VALUES ('0245','CRITERIOS X PROVEEDOR','02','56')")
    '    BD(BDCurCon).Execute ("INSERT INTO OPCION (TX_CODI_OPCI,TX_DESC_OPCI,TX_CODPADR_OPCI,NU_AUTO_FORM_OPCI) VALUES ('0506','PROYECTO DE COMPRA','05','57')")
    '
    '    BD(BDCurCon).Execute ("INSERT INTO PERMISO (NU_AUTO_PERF_PERM,NU_AUTO_OPCI_PERM,TX_CREA_PERM,TX_ANUL_PERM,TX_CONS_PERM,TX_IMPR_PERM,TX_OTRO_PERM,TX_OTR1_PERM) VALUES ('1','51','S','S','S','S','N','N')")
    '    BD(BDCurCon).Execute ("INSERT INTO PERMISO (NU_AUTO_PERF_PERM,NU_AUTO_OPCI_PERM,TX_CREA_PERM,TX_ANUL_PERM,TX_CONS_PERM,TX_IMPR_PERM,TX_OTRO_PERM,TX_OTR1_PERM) VALUES ('1','52','S','S','S','S','N','N')")
    '    BD(BDCurCon).Execute ("INSERT INTO PERMISO (NU_AUTO_PERF_PERM,NU_AUTO_OPCI_PERM,TX_CREA_PERM,TX_ANUL_PERM,TX_CONS_PERM,TX_IMPR_PERM,TX_OTRO_PERM,TX_OTR1_PERM) VALUES ('1','53','S','S','S','S','N','N')")
    '    BD(BDCurCon).Execute ("INSERT INTO PERMISO (NU_AUTO_PERF_PERM,NU_AUTO_OPCI_PERM,TX_CREA_PERM,TX_ANUL_PERM,TX_CONS_PERM,TX_IMPR_PERM,TX_OTRO_PERM,TX_OTR1_PERM) VALUES ('1','54','S','S','S','S','N','N')")
    '    BD(BDCurCon).Execute ("INSERT INTO PERMISO (NU_AUTO_PERF_PERM,NU_AUTO_OPCI_PERM,TX_CREA_PERM,TX_ANUL_PERM,TX_CONS_PERM,TX_IMPR_PERM,TX_OTRO_PERM,TX_OTR1_PERM) VALUES ('1','55','S','S','S','S','N','N')")
    '    BD(BDCurCon).Execute ("INSERT INTO PERMISO (NU_AUTO_PERF_PERM,NU_AUTO_OPCI_PERM,TX_CREA_PERM,TX_ANUL_PERM,TX_CONS_PERM,TX_IMPR_PERM,TX_OTRO_PERM,TX_OTR1_PERM) VALUES ('1','56','S','S','S','S','N','N')")
    '    BD(BDCurCon).Execute ("INSERT INTO PERMISO (NU_AUTO_PERF_PERM,NU_AUTO_OPCI_PERM,TX_CREA_PERM,TX_ANUL_PERM,TX_CONS_PERM,TX_IMPR_PERM,TX_OTRO_PERM,TX_OTR1_PERM) VALUES ('1','57','S','S','S','S','N','N')")
    '    BD(BDCurCon).Execute ("INSERT INTO PERMISO (NU_AUTO_PERF_PERM,NU_AUTO_OPCI_PERM,TX_CREA_PERM,TX_ANUL_PERM,TX_CONS_PERM,TX_IMPR_PERM,TX_OTRO_PERM,TX_OTR1_PERM) VALUES ('1','58','S','S','S','S','N','N')")
    '    BD(BDCurCon).Execute ("INSERT INTO PERMISO (NU_AUTO_PERF_PERM,NU_AUTO_OPCI_PERM,TX_CREA_PERM,TX_ANUL_PERM,TX_CONS_PERM,TX_IMPR_PERM,TX_OTRO_PERM,TX_OTR1_PERM) VALUES ('1','59','S','S','S','S','N','N')")
    '    BD(BDCurCon).Execute ("INSERT INTO PERMISO (NU_AUTO_PERF_PERM,NU_AUTO_OPCI_PERM,TX_CREA_PERM,TX_ANUL_PERM,TX_CONS_PERM,TX_IMPR_PERM,TX_OTRO_PERM,TX_OTR1_PERM) VALUES ('1','60','S','S','S','S','N','N')")
    
'    [NU_AUTO_ARTI_COBA] [int] NOT NULL ,
'    [TX_COBA_COBA] [varchar] (13) NOT NULL ,
'    [TX_REGI_COBA] [varchar] (20) NULL ,
'    [TX_CIAL_COBA] [varchar] (20) NULL ,
        Result = DoUpdate("PARAMETROS_INVE", "NU_NVERDB_PINV='5.1'", NUL$)
    
    Case "5.2.0":
'    BD(BDCurCon).Execute ("INSERT INTO FORMULARIO (TX_DESC_FORM,TX_FORM_FORM,TX_TIPO_FORM) VALUES ('CODIGOS DE BARRA','frmPrmCDBARRA','FRM')")
        Result = LoadData("FORMULARIO", "TX_DESC_FORM", "TX_FORM_FORM='frmPrmCDBARRA' AND TX_TIPO_FORM='FRM'", ArrINTRFC())
        If Result <> FAIL Then
            If Encontro Then
                Result = DoUpdate("FORMULARIO", "TX_FORM_FORM='frmPrmREGISTRO'", "TX_FORM_FORM='frmPrmCDBARRA' AND TX_TIPO_FORM='FRM'")
            Else
                Result = DoInsertSQL("FORMULARIO", "TX_DESC_FORM='CODIGOS DE BARRA',TX_FORM_FORM='frmPrmREGISTRO',TX_TIPO_FORM='FRM'")
            End If
        End If
        Result = LoadData("FORMULARIO", "TX_DESC_FORM", "TX_FORM_FORM='frmPrmREGISTRO' AND TX_TIPO_FORM='FRM'", ArrINTRFC())
        If Result <> FAIL Then
            If Encontro Then
                Result = DoUpdate("FORMULARIO", "TX_FORM_FORM='frmPrmBARRAXARTICULO'", "TX_FORM_FORM='frmPrmREGISTRO' AND TX_TIPO_FORM='FRM'")
            Else
                Result = DoInsertSQL("FORMULARIO", "TX_DESC_FORM='CODIGOS DE BARRA',TX_FORM_FORM='frmPrmBARRAXARTICULO',TX_TIPO_FORM='FRM'")
            End If
        End If
        Result = LoadData("FORMULARIO", "TX_DESC_FORM", "TX_FORM_FORM='FrmGENERAL' AND TX_TIPO_FORM='29'", ArrINTRFC())
        If Result <> FAIL Then
            If Encontro Then
                Result = DoUpdate("FORMULARIO", "TX_FORM_FORM='frmPrmCOTIZACIONES',TX_TIPO_FORM='FRM'", "TX_FORM_FORM='FrmGENERAL' AND TX_TIPO_FORM='29'")
            Else
                Result = DoInsertSQL("FORMULARIO", "TX_DESC_FORM='CODIGOS DE BARRA',TX_FORM_FORM='frmPrmBARRAXARTICULO',TX_TIPO_FORM='FRM'")
            End If
        End If
'    BD(BDCurCon).Execute ("INSERT INTO FORMULARIO (TX_DESC_FORM,TX_FORM_FORM,TX_TIPO_FORM) VALUES ('KARDEX','frmPrmREPOKAR','FRM')")
''    COTIZACION/LISTA PRECIOS PROVEEDOR  47  FrmGENERAL  29
''frmPrmCOTIZACIONES,

''IN_CODIGOBAR(NU_AUTO_ARTI_COBA,TX_COBA_COBA,TX_REGI_COBA,TX_CIAL_COBA,TX_LABO_COBA)
        Result = DoInsertSQL("FORMULARIO", "TX_DESC_FORM='KARDEX X GRUPO',TX_FORM_FORM='frmPrmREPOKARCNP',TX_TIPO_FORM='FRM'")
        Result = DoInsertSQL("FORMULARIO", "TX_DESC_FORM='ENTRADAS Y SALIDAS X GRUPO',TX_FORM_FORM='frmPrmREPOENTSAL',TX_TIPO_FORM='FRM'")
        Result = DoInsertSQL("FORMULARIO", "TX_DESC_FORM='RENTAS Y GASTOS',TX_FORM_FORM='frmPrmRENYGAS',TX_TIPO_FORM='FRM'")
        
        If ExisteTABLA("IN_R_COBA_TERC") Then Call EliminarTabla("IN_R_COBA_TERC")
        If ExisteTABLA("IN_R_CODIGOBAR_TERCERO") Then Call EliminarTabla("IN_R_CODIGOBAR_TERCERO")
        If ExisteTABLA("IN_CODIGOBAR") Then Call EliminarTabla("IN_CODIGOBAR")
        Result = CrearTabla("IN_CODIGOBAR", "NU_AUTO_ARTI_COBA, 4, 4, 0")
         Result = CrearCampo("IN_CODIGOBAR", "TX_COBA_COBA", 10, 13, 0, False)
        Result = CrearCampo("IN_CODIGOBAR", "TX_REGI_COBA", 10, 20, 1, False)
        Result = CrearCampo("IN_CODIGOBAR", "TX_CIAL_COBA", 10, 20, 1, False)
        Result = CrearCampo("IN_CODIGOBAR", "TX_LABO_COBA", 10, 20, 1, False)
        Result = 0: vContar = 5
        While vContar > 0 And Result = 0
            Result = CrearIndice("IN_CODIGOBAR", "PK_IN_CODIGOBAR", "TX_COBA_COBA", True)
            vContar = vContar - 1
        Wend
        Result = 0: vContar = 5
        While vContar > 0 And Result = 0
            Result = CrearRelacion("IN_CODIGOBARARTICULO", "NU_AUTO_ARTI_COBA", "NU_AUTO_ARTI", "IN_CODIGOBAR", "ARTICULO")
            vContar = vContar - 1
        Wend
''Result = CrearRelacion("IN_R_PERFIL_BODEGAIN_BODEGA", "NU_AUTO_BODE_RPEB", "NU_AUTO_BODE", "IN_R_PERFIL_BODEGA", "IN_BODEGA")
''IN_R_CODIGOBAR_TERCERO(TX_COBA_COBA_RCBTE,CD_CODI_TERC_RCBTE,NU_MULT_RCBTE,NU_DIVI_RCBTE,NU_COST_RCBTE,NU_IMPU_RCBTE)
        Result = CrearTabla("IN_R_CODIGOBAR_TERCERO", "TX_COBA_COBA_RCBTE, 10, 13, 0")
        Result = CrearCampo("IN_R_CODIGOBAR_TERCERO", "CD_CODI_TERC_RCBTE", 10, 20, 0, False)
        Result = CrearCampo("IN_R_CODIGOBAR_TERCERO", "NU_MULT_RCBTE", 3, 2, 0, False)
        Result = CrearCampo("IN_R_CODIGOBAR_TERCERO", "NU_DIVI_RCBTE", 3, 2, 0, False)
        Result = CrearCampo("IN_R_CODIGOBAR_TERCERO", "NU_COST_RCBTE", 6, 4, 1, False)
        Result = CrearCampo("IN_R_CODIGOBAR_TERCERO", "NU_IMPU_RCBTE", 6, 4, 1, False)
        Result = 0: vContar = 5
        While vContar > 0 And Result = 0
            Result = CrearIndice("IN_R_CODIGOBAR_TERCERO", "PK_IN_R_CODIGOBAR_TERCERO", "TX_COBA_COBA_RCBTE, CD_CODI_TERC_RCBTE", True)
            vContar = vContar - 1
        Wend
        Result = 0: vContar = 5
        While vContar > 0 And Result = 0
            Result = CrearRelacion("IN_R_CODIGOBAR_TERCEROIN_CODIGOBAR", "TX_COBA_COBA_RCBTE", "TX_COBA_COBA", "IN_R_CODIGOBAR_TERCERO", "IN_CODIGOBAR")
            vContar = vContar - 1
        Wend
        Result = 0: vContar = 5
        While vContar > 0 And Result = 0
            Result = CrearRelacion("IN_R_CODIGOBAR_TERCEROTERCERO", "CD_CODI_TERC_RCBTE", "CD_CODI_TERC", "IN_R_CODIGOBAR_TERCERO", "TERCERO")
            vContar = vContar - 1
        Wend
'IN_ARTICULOSELECCION
        If ExisteTABLA("IN_ARTICULOSELECCION") Then Call EliminarTabla("IN_ARTICULOSELECCION")
        If ExisteTABLA("IN_CDBARSELE") Then Call EliminarTabla("IN_CDBARSELE")
''IN_CDBARSELE(NU_AUTO_PROY_CDBSE,TX_COBA_COBA_CDBSE,CD_CODI_TERC_CDBSE,NU_OPCI_CDBSE,NU_COST_CDBSE,NU_CALI_CDBSE,NU_ORDE_CDBSE,NU_IMPU_CDBSE)
        Result = CrearTabla("IN_CDBARSELE", "NU_AUTO_PROY_CDBSE, 4, 4, 0")
        Result = CrearCampo("IN_CDBARSELE", "TX_COBA_COBA_CDBSE", 10, 13, 0, False)
        Result = CrearCampo("IN_CDBARSELE", "CD_CODI_TERC_CDBSE", 10, 20, 0, False)
        Result = CrearCampo("IN_CDBARSELE", "NU_OPCI_CDBSE", 3, 2, 0, False)
        Result = CrearCampo("IN_CDBARSELE", "NU_COST_CDBSE", 6, 4, 0, False)
        Result = CrearCampo("IN_CDBARSELE", "NU_CALI_CDBSE", 6, 4, 0, False)
        Result = CrearCampo("IN_CDBARSELE", "NU_ORDE_CDBSE", 3, 2, 0, False)
        Result = CrearCampo("IN_CDBARSELE", "NU_IMPU_CDBSE", 6, 4, 0, False)
        Result = 0: vContar = 5
        While vContar > 0 And Result = 0
            Result = CrearRelacion("IN_CDBARSELEIN_CODIGOBAR", "TX_COBA_COBA_CDBSE", "TX_COBA_COBA", "IN_CDBARSELE", "IN_CODIGOBAR")
            vContar = vContar - 1
        Wend
        Result = 0: vContar = 5
        While vContar > 0 And Result = 0
            Result = CrearRelacion("IN_PROYETOCOMPRAIN_CDBARSELE", "NU_AUTO_PROY_CDBSE", "NU_AUTO_PRYC", "IN_CDBARSELE", "IN_PROYETOCOMPRA")
            vContar = vContar - 1
        Wend
        Result = 0: vContar = 5
        While vContar > 0 And Result = 0
            Result = CrearRelacion("IN_CDBARSELETERCERO", "CD_CODI_TERC_CDBSE", "CD_CODI_TERC", "IN_CDBARSELE", "TERCERO")
            vContar = vContar - 1
        Wend
'CD_CODI_TERC
        Result = CrearCampo("IN_KARDEX", "CD_CODI_ARTI_KARD", 10, 16, 1, False)
        Result = CrearCampo("IN_KARDEX", "TX_LOTE_KARD", 10, 16, 1, False)
        Result = DoUpdate("PARAMETROS_INVE", "NU_NVERDB_PINV='5.2.0'", NUL$)
    
      Case "5.3.0": Call Actualizar_5_3_0
      Case "5.4.0": Call Actualizar_5_4_0
      Case "5.4.1": Call Actualizar_5_4_1
      Case "5.5.0": Call Actualizar_5_5_0
      Case "5.6.0": Call Actualizar_5_6_0
      Case "5.7.0": Call Actualizar_5_7_0
      Case "5.8.0": Call Actualizar_5_8_0
      Case "5.9.0": Call Actualizar_5_9_0
      Case "6.0": Call Actualizar_6_0
      Case "6.1": Call Actualizar_6_1
      Case "6.2.0", "6.2": Call Actualizar_6_2
      Case "6.3.0", "6.3": Call Actualizar_6_3
      Case "6.4.0", "6.4": Call Actualizar_6_4
      Case "6.5.0", "6.5": Call Actualizar_6_5
      Case "6.6.0", "6.6": Call Actualizar_6_6
      Case "6.7.0", "6.7": Call Actualizar_6_7 'JACC R2396
      Case "6.8.0", "6.8": Call Actualizar_6_8 'GAPM R2427 ACTUZALIZACION DE VERSION BD
      Case "6.9.0", "6.9": Call Actualizar_6_9
      Case "7.0.0", "7.0": Call Actualizar_7_0 'GAVL R3201 23/02/2011
      Case "7.1.0", "7.1": Call Actualizar_7_1 'LDCR T5865
      Case "7.2.0", "7.2": Call Actualizar_7_2 'JAGS T7783
      Case "7.3.0", "7.3": Call Actualizar_7_3 'JAGS T9979
      Case "7.4.0", "7.4": Call Actualizar_7_4 'APGR APERTURA DE VERSION
      Case "7.5.0", "7.5": Call Actualizar_7_5 'JJRG 15524
      Case "7.6.0", "7.6": Call Actualizar_7_6 'SKRV T18846
      Case "7.7.0", "7.7": Call Actualizar_7_7 'AFMG T21136
      Case "7.8.0", "7.8": Call Actualizar_7_8 'SKRV T24261/R22366
      Case "7.9.0", "7.8": Call Actualizar_7_9 'JLPB T27307
      Case "8.0.0", "8.0": Call Actualizar_8_0 'JAUM T28370-R22535
      Case "8.1.0", "8.1": Call Actualizar_8_1 'JAUM T32800-R31099
      Case "8.2.0", "8.1": Call Actualizar_8_2 'JLPB T36875-R35965
      Case "8.3.0", "8.3": Call Actualizar_8_3 'MFAB T38552
      Case "8.4.0", "8.4": Call Actualizar_8_4 'JLPB T42237
      Case "8.5.0", "8.5": Call Actualizar_8_5 'JLPB T41459-R37519
      'Case "8.5.1", "8.5": Call Actualizar_8_6_0 'DRMG T44728-R41288 'DRMG T45218 SE DEJA EN COMENTARIO
      Case "8.6.0", "8.6": Call Actualizar_8_6_0 'DRMG T45218
      Case "8.7.0", "8.7": Call Actualizar_8_7_0 'GSCD T45902-R42283
   End Select
'End Sub
End Function

Sub Actualizar_5_3_0()
'Dim MulArr() As Variant    'DEPURACION DE CODIGO
'Dim i As Long                 'DEPURACION DE CODIGO
Dim Cmp As String
Dim Dsd As String
Dim Cdc As String
Dim ArrImp() As Variant
Dim vCon As Integer
Dim vCue As String
'Dim Cmd As String            'DEPURACION DE CODIGO
'Dim Clave As String          'DEPURACION DE CODIGO
'Dim ArrUsua() As Variant   'DEPURACION DE CODIGO

    ''    FACTURA DE VENTA   29  FrmGENERAL  11
    ''    12  DEVFAC  DEVOLUCION FACTURA DE VENTA S   N   11  0   V   N
    ''SELECT CD_CODI_IMPU, DE_NOMB_IMPU, PR_PORC_IMPU FROM TC_IMPUESTOS WHERE ID_TIPO_IMPU='O'
    'TC_IMPUESTOS, CD_CODI_IMPU, DE_NOMB_IMPU, PR_PORC_IMPU, VL_TOPE_IMPU, ID_SUMA_IMPU, ID_TIPO_IMPU

    Result = CrearCampo("ARTICULO", "CD_IMPU_ARTI_TCIM", 10, 3, 1, False)
    Result = CrearRelacion("ARTICULOTC_IMPUESTOS", "CD_IMPU_ARTI_TCIM", "CD_CODI_IMPU", "ARTICULO", "TC_IMPUESTOS")
    
    Result = EliminarRelacion("FK_IN_PARTIPARTI_IN_PARTICULAR", "IN_PARTIPARTI")
    Result = DoDelete("IN_PARTICULAR", "")
    'Result = CrearCampo("IN_PARTICULAR", "TX_NOMB_PART", 10, 50, 1, False)
    'Result = DoInsertSQL("IN_PARTICULAR", "NU_AUTO_PART='1',TX_CODI_PART='PORDE',TX_NOMB_PART='PORCENTAJE DE DESCUENTO',NU_AUTO_DOCU_PART='3'")
    'Result = DoInsertSQL("IN_PARTICULAR", "NU_AUTO_PART='2',TX_CODI_PART='RETFU',TX_NOMB_PART='PORCENTAJE DE RETENCION EN LA FUENTE',NU_AUTO_DOCU_PART='3'")
    'Result = DoInsertSQL("IN_PARTICULAR", "NU_AUTO_PART='3',TX_CODI_PART='RETIV',TX_NOMB_PART='PORCENTAJE DE RETENCION DE IVA',NU_AUTO_DOCU_PART='3'")
    'Result = DoInsertSQL("IN_PARTICULAR", "NU_AUTO_PART='4',TX_CODI_PART='RETIC',TX_NOMB_PART='PORCENTAJE DE RETENCION DE ICA',NU_AUTO_DOCU_PART='3'")
    'Result = DoInsertSQL("IN_PARTICULAR", "NU_AUTO_PART='5',TX_CODI_PART='FLETE',TX_NOMB_PART='COSTO DE LOS FLETES',NU_AUTO_DOCU_PART='3'")
    'Result = DoInsertSQL("IN_PARTICULAR", "NU_AUTO_PART='6',TX_CODI_PART='DESAD',TX_NOMB_PART='PORCENTAJE DE DESCUENTO ADICIONAL',NU_AUTO_DOCU_PART='3'")
    'Result = DoInsertSQL("IN_PARTICULAR", "NU_AUTO_PART='7',TX_CODI_PART='PROPA',TX_NOMB_PART='PAGA FLETES PROVEDOR/PARTICULAR',NU_AUTO_DOCU_PART='3'")
    'Result = DoInsertSQL("IN_PARTICULAR", "NU_AUTO_PART='8',TX_CODI_PART='FACTU',TX_NOMB_PART='NUMERO DE FACTURA',NU_AUTO_DOCU_PART='3'")
    
    Result = CrearCampo("IN_PARTIPARTI", "TX_CODI_PART_PRPR", 10, 3, 1, False)
    Result = CrearCampo("IN_PARTIPARTI", "TX_CUAL_PRPR", 10, 1, 1, False)
    Result = DoUpdate("IN_PARTIPARTI", "TX_CODI_PART_PRPR=NU_AUTO_PART_PRPR, TX_CUAL_PRPR='A'", "")
    Result = EliminarCampo("IN_PARTIPARTI", "NU_AUTO_PART_PRPR")
    
    Result = CrearCampo("FORMA_PAGO", "CD_CUEN_FOPA", 10, 14, 1, False)
    '''        Result = CrearCampo("FORMA_PAGO", "NU_PART_FOPA", 3, 2, 1, False)
    '''        Result = CrearCampo("DESCUENTO", "NU_PART_DESC", 3, 2, 1, False)
    '''        Result = CrearCampo("TC_IMPUESTOS", "NU_PART_IMPU", 3, 2, 1, False)
    Result = DoInsertSQL("FORMA_PAGO", "NU_NUME_FOPA='3', DE_DESC_FOPA='Credito', CD_CUEN_FOPA='CRDT'")

    'SELECT NU_NUME_FOPA, DE_DESC_FOPA, CD_CUEN_FOPA, NU_PART_FOPA FROM FORMA_PAGO
    'SELECT CD_CODI_DESC, DE_NOMB_DESC, PR_PORC_DESC, VL_TOPE_DESC, CD_CUEN_DESC, VL_VALO_DESC, NU_PART_DESC FROM DESCUENTO
    'SELECT DISTINCT PR_IMPU_ARTI FROM ARTICULO WHERE PR_IMPU_ARTI>0
    '        Result = DoInsertSQL("TC_IMPUESTOS", "CD_CODI_IMPU='XNT', DE_NOMB_IMPU='EXENTO DE IVA', PR_PORC_IMPU='0', VL_TOPE_IMPU='0', ID_SUMA_IMPU='', ID_TIPO_IMPU='I'")
    '        Result = DoUpdate("ARTICULO", "CD_IMPU_ARTI_TCIM='XNT'", "PR_IMPU_ARTI=0")
    ''////////////
    Cmp = "Distinct PR_IMPU_ARTI"
    Dsd = "ARTICULO"
    Cdc = ""
    ReDim ArrImp(1, 0)
    Result = LoadMulData(Dsd, Cmp, Cdc, ArrImp())
    If Not Result = FAIL Then
        If Encontro Then
            For vCon = 0 To UBound(ArrImp, 2)
                vCue = "V" & Right(String(2, "0") & vCon, 2)
                If ArrImp(0, vCon) <> NUL$ Then     'PedroJ
                    Result = DoInsertSQL("TC_IMPUESTOS", "CD_CODI_IMPU='" & vCue & "', DE_NOMB_IMPU='IVA DEL " & FormatNumber(ArrImp(0, vCon), 2) & "', PR_PORC_IMPU='" & FormatNumber(ArrImp(0, vCon), 2) & "', VL_TOPE_IMPU='0', ID_SUMA_IMPU='', ID_TIPO_IMPU='I'")
                    Result = DoUpdate("ARTICULO", "CD_IMPU_ARTI_TCIM='" & vCue & "'", "PR_IMPU_ARTI=" & FormatNumber(ArrImp(0, vCon), 2))
                End If
            Next
        End If
    End If
    ''////////////
    Result = DoInsertSQL("FORMULARIO", "TX_DESC_FORM='FACTURA DE VENTA',TX_FORM_FORM='FrmGENERAL',TX_TIPO_FORM='11'")
    ''    DEVOLUCION FACTURA DE VENTA FrmGENERAL  12
    Result = DoInsertSQL("FORMULARIO", "TX_DESC_FORM='DEVOLUCION FACTURA DE VENTA',TX_FORM_FORM='FrmGENERAL',TX_TIPO_FORM='12'")
    Result = DoInsertSQL("FORMULARIO", "TX_DESC_FORM='DESCUENTOS',TX_FORM_FORM='FrmDescuentos',TX_TIPO_FORM='FRM'")
    Result = DoInsertSQL("FORMULARIO", "TX_DESC_FORM='IMPUESTOS Y OTROS CARGOS',TX_FORM_FORM='FrmImpuestos',TX_TIPO_FORM='FRM'")
    Result = DoInsertSQL("FORMULARIO", "TX_DESC_FORM='FORMAS DE PAGO',TX_FORM_FORM='FrmFormaPago',TX_TIPO_FORM='FRM'")
    
    ''FrmImpuestos, FrmFormaPago
    ''CD_DEPE_APLI
    Result = CrearCampo("PARAMETROS_INVE", "CD_DEPE_APLI", 10, 3, 1, False)
    Result = CrearCampo("IN_ENCABEZADO", "NU_COPI_ENCA", 3, 1, 1, False)
    
    Result = DoUpdate("PARAMETROS_INVE", "NU_NVERDB_PINV='5.3.0'", NUL$)
    
End Sub

Sub Actualizar_5_4_0()
Dim i As Long
Dim Clave As String
Dim ArrUsua() As Variant
'Dim Cmd As String, Estado  'DEPURACION DE CODIGO
Dim Cmd As String
Dim ArrArt() As Variant 'Dayan
'Dim UniVen As Long 'Dayan  'DEPURACION DE CODIGO

    '/Dayan, Def 4981
    ReDim ArrArt(0, 0)
    ReDim Arr(0)
    Condicion = "NU_AUTO_UNVE_ARTI IS NULL"
    Result = LoadMulData("ARTICULO", "NU_AUTO_ARTI", Condicion, ArrArt)
    If Result <> FAIL And Encontro Then
        Valores = "TX_CODI_UNVE=" & Comi & "CNT" & Comi & Coma
        Valores = Valores & "TX_NOMB_UNVE=" & Comi & "CNT" & Comi & Coma
        Valores = Valores & "NU_MULT_UNVE=1" & Coma
        Valores = Valores & "NU_DIVI_UNVE=1"
        Result = DoInsertSQL("IN_UNDVENTA", Valores)
        If Result <> FAIL Then
            Condicion = "TX_CODI_UNVE=" & Comi & "CNT" & Comi
            Condicion = "TX_NOMB_UNVE=" & Comi & "CNT" & Comi
            Result = LoadData("IN_UNDVENTA", "NU_AUTO_UNVE", Condicion, Arr)
            If Result <> FAIL And Encontro Then
                For i = 0 To UBound(ArrArt, 2)
                    Condicion = " NU_AUTO_ARTI=" & ArrArt(0, i)
                    Campos = "NU_AUTO_UNVE_ARTI=" & CLng(Arr(0))
                    Result = DoUpdate("ARTICULO", Campos, Condicion)
                Next i
            End If
        End If
    End If '\Dayan

    Result = CrearCampo("IN_R_LIST_ARTI", "NU_AUTO_UNVE_RLIA", 4, 9, 1)
    Result = CrearRelacion("INRLISTARTI_INUNDVENTA", "NU_AUTO_UNVE_RLIA", "NU_AUTO_UNVE", "IN_R_LIST_ARTI", "IN_UNDVENTA")
    
    If MotorBD = "SQL" Then
        Cmd = "UPDATE IN_R_LIST_ARTI SET NU_AUTO_UNVE_RLIA = NU_AUTO_UNVE_ARTI FROM IN_R_LIST_ARTI,ARTICULO WHERE NU_AUTO_ARTI = NU_AUTO_ARTI_RLIA"
        Result = ExecSQL(Cmd)
    Else
        ReDim MulArr(1, 0)
        Result = LoadMulData("IN_R_LIST_ARTI,ARTICULO", "NU_AUTO_ARTI,NU_AUTO_UNVE_ARTI", "NU_AUTO_ARTI = NU_AUTO_ARTI_RLIA", MulArr())
        If Result <> FAIL And Encontro Then
           For i = 0 To UBound(MulArr(), 2)
               If Result <> FAIL Then Result = DoUpdate("IN_R_LIST_ARTI", "NU_AUTO_UNVE_RLIA=" & CLng(MulArr(1, i)), "NU_AUTO_ARTI_RLIA=" & CLng(MulArr(0, i)))
           Next
        End If
    End If

'''Encriptar contrase�as actuales
    Result = CrearCampo("USUARIO", "TX_PASS_USUA_TEMP", 10, 15, 1)
    
    If Result <> FAIL Then Result = DoUpdate("USUARIO", "TX_PASS_USUA_TEMP = TX_PASS_USUA", NUL$)
    If Result <> FAIL Then Result = EliminarCampo("USUARIO", "TX_PASS_USUA")
    If Result <> FAIL Then Result = CrearCampo("USUARIO", "TX_PASS_USUA", 10, 30, 1)
    
    ReDim ArrUsua(1, 0)
    If Result <> FAIL Then Result = LoadMulData("USUARIO", "TX_IDEN_USUA,TX_PASS_USUA_TEMP", NUL$, ArrUsua())
    'Call Mensaje1("Al buscar-> Result=" & Result, 1)
    If Result <> FAIL And Encontro Then
       For i = 0 To UBound(ArrUsua(), 2)
           DoEvents
           Clave = NUL$
           'CA: Quitar los espacios y enviar en may�scula la contrase�a leida DIRECTAMENTE de la  BD S�LO esta vez
           Clave = Encriptar(UCase(Trim(ArrUsua(1, i))), UCase(Trim(ArrUsua(0, i))))
           'Call Mensaje1("Clave=" & Clave & "  Result=" & Result, 1)
           Valores = "TX_PASS_USUA='" & ConvertCadenaHexa(Clave) & Comi
           If Result <> FAIL Then Result = DoUpdate("USUARIO", Valores, "TX_IDEN_USUA=" & Comi & ArrUsua(0, i) & Comi)
           'If Result <> FAIL Then Call Mensaje1("Actualizo", 1)
       Next
    End If
    If Result <> FAIL Then Result = EliminarCampo("USUARIO", "TX_PASS_USUA_TEMP")
''Fin encriptar

    Result = DoUpdate("PARAMETROS_INVE", "NU_NVERDB_PINV='5.4.0'", NUL$)

End Sub

Sub Actualizar_5_4_1()
Dim SQL As String
Dim i As Double

    ' Campo adicional email tercero
    If Not ExisteCAMPO("TERCERO", "TX_MAIL_TERC") Then
        Result = CrearCampo("TERCERO", "TX_MAIL_TERC", 10, 100, 1)       'descripcion
    End If
    
    'Tabla para guaradar la relacion entre el documento de inventario y ppto
    Result = CrearTabla("IN_R_ENCA_PPTO", "NU_AUTO_ENCA_ENPP,4,9,0")    'Auntonum del In_encabezado
    Result = CrearCampo("IN_R_ENCA_PPTO", "NU_CONS_PTAL_ENPP", 4, 9, 0) 'Consecutivo del doc pptal
    Result = CrearCampo("IN_R_ENCA_PPTO", "NU_TIPO_PTAL_ENPP", 4, 9, 0) 'Tipo Doc pptal 1=CDP/ 2=RP/ 3=OBLIG
    Result = CrearCampo("IN_R_ENCA_PPTO", "NU_AUTO_ENPP", 4, 9, 0, True)
    
    Result = CrearIndice("IN_R_ENCA_PPTO", "PK_IN_R_ENCA_PPTO", "NU_AUTO_ENPP", True)
    Result = CrearRelacion("R_ENCABEZADO_PPTO", "NU_AUTO_ENCA_ENPP", "NU_AUTO_ENCA", "IN_R_ENCA_PPTO", "IN_ENCABEZADO")
    
    'Campo para relacionar el # de CxP de la compra
    Result = CrearCampo("IN_ENCABEZADO", "NU_CXP_ENCA", 10, 5, 1)
    
    'Si estos campos no se crearon el la 5.3.0 los crea
    If Not ExisteCAMPO("IN_ENCABEZADO", "NU_COPI_ENCA") Then Result = CrearCampo("IN_ENCABEZADO", "NU_COPI_ENCA", 3, 1, 1, False)
    If Not ExisteCAMPO("PARAMETROS_INVE", "CD_DEPE_APLI") Then
        Result = CrearCampo("PARAMETROS_INVE", "CD_DEPE_APLI", 10, 11, 1, False)
    Else
        'Aumentar el tama�o del campo CD_DEPE_APLI
        If Result <> FAIL Then Result = EliminarCampo("PARAMETROS_INVE", "CD_DEPE_APLI")
        If Result <> FAIL Then Result = CrearCampo("PARAMETROS_INVE", "CD_DEPE_APLI", 10, 11, 1, False)
    End If
    
    'Campo para guardar la dependencia de CxP con la que se hace la Nota Debito
    Result = CrearCampo("PARAMETROS_INVE", "CD_DEPE_CXP_APLI", 10, 11, 1)
    
    'La fecha de IN_ENCABEZADO sea igual en IN_DETALLE
    If (BeginTran(STranIUp & "UPD_FECHAS_DETALLE") <> FAIL) Then
      If MotorBD <> "ACCESS" Then
        SQL = "UPDATE IN_DETALLE SET FE_FECH_DETA=FE_CREA_ENCA "
        SQL = SQL & "FROM IN_ENCABEZADO, IN_DETALLE WHERE NU_AUTO_ORGCABE_DETA=NU_AUTO_ENCA "
        Result = ExecSQL(SQL)
      Else
        ReDim Arr(1, 0)
        Result = LoadMulData("IN_ENCABEZADO", "NU_AUTO_ENCA,FE_CREA_ENCA", NUL$, Arr)
        If Result <> FAIL And Encontro Then
           For i = 0 To UBound(Arr, 2)
              Result = DoUpdate("IN_DETALLE", "FE_FECH_DETA=" & FFechaIns(CDate(Arr(1, i))), "NU_AUTO_ORGCABE_DETA=" & Arr(0, i))
              If Result = FAIL Then Exit For
           Next i
        End If
      End If
    End If
    If (Result <> FAIL) Then
       If (CommitTran() = FAIL) Then
          Call RollBackTran
          Call Mensaje1("No se registraron los cambios de la transacci�n 'UPD_FECHAS_DETALLE'.", 1)
       End If
    Else
       Call RollBackTran
       Call Mensaje1("No se registraron los cambios de la transacci�n 'UPD_FECHAS_DETALLE'.", 1)
    End If
    
    
    'Comprobante contable para la Devoluci�n baja de consumo
    Result = DoInsertSQL("TC_COMPROBANTE", "CD_CODI_COMP='DBJ', NO_NOMB_COMP='DEVOLUCION BAJA DE CONSUMO', NU_CONS_COMP=0, ID_SIIF_COMP='N', ID_TRAN_COMP='N'")
    Result = DoInsertSQL("CONCEPTOT", "NU_MOVI_CONC=9,CD_COMP_CONC='DBJ'")
    
    Result = DoUpdate("PARAMETROS_INVE", "NU_NVERDB_PINV='5.4.1'", NUL$)

End Sub

Sub Actualizar_5_5_0()
Dim SQL As String
'Dim I As Double
    
'    If EntiLic <> "CENTRO POLICLINICO DEL OLAYA S.A." Then       'Si no es CPO que haga el cambio
'        'La fecha de IN_ENCABEZADO sea igual en IN_DETALLE
'        If (BeginTran(STranIUp & "UPD_FECHAS_DETALLE") <> FAIL) Then
'          If MotorBD <> "ACCESS" Then
'            SQL = "UPDATE IN_DETALLE SET FE_FECH_DETA=FE_CREA_ENCA "
'            SQL = SQL & "FROM IN_ENCABEZADO, IN_DETALLE WHERE NU_AUTO_ORGCABE_DETA=NU_AUTO_ENCA "
'            SQL = SQL & "AND FE_CREA_ENCA>" & FFechaCon("01/01/2006")
'            Result = ExecSQL(SQL)
'            If Result <> FAIL Then  'La fecha de IN_ENCABEZADO sea igual en IN_KARDEX
'                SQL = "UPDATE IN_KARDEX SET FE_FECH_KARD=FE_CREA_ENCA "
'                SQL = SQL & "FROM IN_ENCABEZADO, IN_KARDEX WHERE NU_AUTO_ORGCABE_KARD=NU_AUTO_ENCA "
'                SQL = SQL & "AND FE_CREA_ENCA>" & FFechaCon("01/01/2006")
'                Result = ExecSQL(SQL)
'            End If
'          Else
'            ReDim Arr(1, 0)
'            Result = LoadMulData("IN_ENCABEZADO", "NU_AUTO_ENCA,FE_CREA_ENCA", NUL$, Arr)
'            If Result <> FAIL And Encontro Then
'               For I = 0 To UBound(Arr, 2)
'                  'actualizar IN_DETALLE
'                  Result = DoUpdate("IN_DETALLE", "FE_FECH_DETA=" & FFechaIns(CDate(Arr(1, I))), "NU_AUTO_ORGCABE_DETA=" & Arr(0, I))
'                  If Result = FAIL Then Exit For
'                  'actualizar IN_KARDEX
'                  Result = DoUpdate("IN_KARDEX", "FE_FECH_KARD=" & FFechaIns(CDate(Arr(1, I))), "NU_AUTO_ORGCABE_KARD=" & Arr(0, I))
'                  If Result = FAIL Then Exit For
'               Next I
'            End If
'          End If
'        End If
'        If (Result <> FAIL) Then
'           If (CommitTran() = FAIL) Then
'              Call RollBackTran
'              Call Mensaje1("No se registraron los cambios de la transacci�n 'UPD_FECHAS_DETALLE'.", 1)
'           End If
'        Else
'           Call RollBackTran
'           Call Mensaje1("No se registraron los cambios de la transacci�n 'UPD_FECHAS_DETALLE'.", 1)
'        End If
'    End If

    SQL = "ALTER TABLE " & "IN_ENCABEZADO" & " ALTER COLUMN " & "NU_CXP_ENCA " & "VARCHAR " & "(10)"
    Result = ExecSQLCommand(SQL)

    'Campo para relacionar el # de CxP de la compra
    'If (BeginTran(STranIUp & "CXP_ENCA") <> FAIL) Then
    '    Result = CrearCampo("IN_ENCABEZADO", "NU_CXP1_ENCA", 10, 5, 1)
    '    Result = DoUpdate("IN_ENCABEZADO", "NU_CXP1_ENCA=NU_CXP_ENCA", NUL$)
    '    Result = EliminarCampo("IN_ENCABEZADO", "NU_CXP_ENCA")
    '    Result = CrearCampo("IN_ENCABEZADO", "NU_CXP_ENCA", 10, 10, 1)
    '    Result = DoUpdate("IN_ENCABEZADO", "NU_CXP_ENCA=NU_CXP1_ENCA", NUL$)
    '    Result = EliminarCampo("IN_ENCABEZADO", "NU_CXP1_ENCA")
    'End If
    'If (Result <> FAIL) Then
    '   If (CommitTran() = FAIL) Then
    '      Call RollBackTran
    '      Call Mensaje1("No se registraron los cambios de la transacci�n 'CXP_ENCA'.", 1)
    '   End If
    'Else
    '   Call RollBackTran
    '   Call Mensaje1("No se registraron los cambios de la transacci�n 'CXP_ENCA'.", 1)
    'End If
    
    If Result <> FAIL Then Result = DoUpdate("PARAMETROS_INVE", "NU_NVERDB_PINV='5.5.0'", NUL$)
End Sub

Sub Actualizar_5_6_0()
Dim SQL As String

  'Req 1337
    If Not ExisteTABLA("TIPO_EMPRESA") Then
        If MotorBD = "ACCESS" Then
            SQL = "CREATE TABLE TIPO_EMPRESA(CD_CODI_TIEMP VARCHAR(5) NOT NULL)"
            Result = ExecSQL(SQL)
        Else
            Result = CrearTabla("TIPO_EMPRESA", "CD_CODI_TIEMP,10,5,0")
        End If
        Result = CrearCampo("TIPO_EMPRESA", "DE_DESC_TIEMP", 10, 60, 0)
        Result = CrearIndice("TIPO_EMPRESA", "PK_TIPOEMPRESA", "CD_CODI_TIEMP", True)
    
        Valores = "CD_CODI_TIEMP='01',DE_DESC_TIEMP='NINGUNA'"
        Result = DoInsertSQL("TIPO_EMPRESA", Valores)
    End If
    
    Result = CrearCampo("R_GRUP_DEPE", "CD_CODI_TIEMP_RGD", 10, 5, 1)
    Result = DoUpdate("R_GRUP_DEPE", "CD_CODI_TIEMP_RGD='01'", NUL$)     'Se actualizan los datos actuales con '01'
    SQL = "ALTER TABLE R_GRUP_DEPE ALTER COLUMN CD_CODI_TIEMP_RGD VARCHAR (5) NOT NULL" 'se vuelve not null el campo
    Result = ExecSQL(SQL)
    
    Result = CrearRelacion("R_GRUP_DEPE_SERTIPOEMPRESA", "CD_CODI_TIEMP_RGD", "CD_CODI_TIEMP", "R_GRUP_DEPE", "TIPO_EMPRESA")
    
    Result = EliminarRelacion(BuscarIndice("R_GRUP_DEPE", "CD_GRUP_RGD,CD_DEPE_RGD"), "R_GRUP_DEPE")
    Result = CrearIndice("R_GRUP_DEPE", "PKR_GRUP_DEPE", "CD_GRUP_RGD,CD_DEPE_RGD,CD_CODI_TIEMP_RGD", True)
    
   'Req 1207
    Result = CrearCampo("ARTICULO", "CD_CODI_GRUF_ARTI", 10, 5, 1)
    Result = DoUpdate("ARTICULO", "CD_CODI_GRUF_ARTI='01'", NUL$)
    
    SQL = "ALTER TABLE ARTICULO ALTER COLUMN CD_CODI_GRUF_ARTI VARCHAR (5) NOT NULL"
    Result = ExecSQL(SQL)
    
    If Not ExisteTABLA("GRUPO_FACTURAS") Then
        If MotorBD = "ACCESS" Then
          SQL = "CREATE TABLE GRUPO_FACTURAS(CD_CODI_GRUF VARCHAR(5) NOT NULL)"
          Result = ExecSQL(SQL)
        Else
          Result = CrearTabla("GRUPO_FACTURAS", "CD_CODI_GRUF,10,5,0")
        End If
        Result = CrearCampo("GRUPO_FACTURAS", "DE_DESC_GRUF", 10, 100, 0)
        Result = CrearIndice("GRUPO_FACTURAS", "PK_GRUPOFACTURA", "CD_CODI_GRUF", True)
        
        Valores = "CD_CODI_GRUF='01',DE_DESC_GRUF='NINGUNO'"
        Result = DoInsertSQL("GRUPO_FACTURAS", Valores)
        
        Valores = "CD_CODI_GRUF='02',DE_DESC_GRUF='MEDICAMENTOS'"
        Result = DoInsertSQL("GRUPO_FACTURAS", Valores)
        
        Valores = "CD_CODI_GRUF='03',DE_DESC_GRUF='INSUMOS'"
        Result = DoInsertSQL("GRUPO_FACTURAS", Valores)
    
        Result = CrearRelacion("R_GRUPOFACTARTICULO", "CD_CODI_GRUF_ARTI", "CD_CODI_GRUF", "ARTICULO", "GRUPO_FACTURAS")
    End If
 
    'Req 1112-983-1035
    Valores = "TX_DESC_FORM='PERSONALIZACION DE LA EMPRESA',TX_FORM_FORM='FrmEmpresa',TX_TIPO_FORM='FRM'"
    Result = DoInsertSQL("FORMULARIO", Valores)
    If Result <> FAIL Then
        ReDim Arr(0)
        Result = LoadData("FORMULARIO", "NU_AUTO_FORM", "TX_FORM_FORM='FrmEmpresa'", Arr)
        If Result <> FAIL And Encontro Then
            Valores = "TX_CODI_OPCI='0504', TX_DESC_OPCI='PERSONALIZACION DE LA EMPRESA',"
            Valores = Valores & "TX_CODPADR_OPCI='05',NU_AUTO_FORM_OPCI=" & Arr(0)
            Result = DoInsertSQL("OPCION", Valores)
            If Result <> FAIL Then Call CrearOpcion("0504")
        End If
    End If
    
'Manejo de Imp y Desc de la ENTRADA
    'Result = CrearTabla("IN_R_ENCA_IMDE", "NU_AUTO_ENCA_ENID,4,9,0")        'Autonum de la entrada
    If MotorBD = "ACCESS" Then
        SQL = "CREATE TABLE IN_R_ENCA_IMDE(PRUEBA VARCHAR(10) NULL) "
        Result = ExecSQL(SQL)
        Result = CrearCampo("IN_R_ENCA_IMDE", "NU_AUTO_ENCA_ENID", 4, 9, 0)     'Autonum de la entrada
    Else
        Result = CrearTabla("IN_R_ENCA_IMDE", "NU_AUTO_ENCA_ENID,4,9,0")        'Autonum de la entrada
    End If
    
    Result = CrearCampo("IN_R_ENCA_IMDE", "NU_AUTO_ENID", 4, 9, 0, True)
    Result = CrearCampo("IN_R_ENCA_IMDE", "TX_NOMB_ENID", 10, 30, 0)        'Nombre del imp/desc
    Result = CrearCampo("IN_R_ENCA_IMDE", "NU_POSI_ENID", 3, 2, 0)          'Posicion del registro
    Result = CrearCampo("IN_R_ENCA_IMDE", "NU_VALO_ENID", 7, 8, 0)           'valor
    Result = CrearCampo("IN_R_ENCA_IMDE", "TX_TIPO_ENID", 10, 1, 0)          'Tipo S Suma/R Resta
    Result = CrearCampo("IN_R_ENCA_IMDE", "CD_CODI_IMPU_ENID", 10, 3, 1)    'codigo del IMPUESTO
    Result = CrearCampo("IN_R_ENCA_IMDE", "CD_CODI_DESC_ENID", 10, 3, 1)    'codigo del DESCUENTO
    Result = CrearCampo("IN_R_ENCA_IMDE", "NU_PRAPLI_ENID", 7, 8, 0)         '%
    If MotorBD = "ACCESS" Then Result = EliminarCampo("IN_R_ENCA_IMDE", "PRUEBA")
    Result = CrearIndice("IN_R_ENCA_IMDE", "PK_IN_R_ENTR_IMDE", "NU_AUTO_ENID", True)
    
    If Result <> FAIL Then Result = DoUpdate("PARAMETROS_INVE", "NU_NVERDB_PINV='5.6.0'", NUL$)
End Sub
Sub Actualizar_5_7_0()
Dim CtasGrupo() As Variant, bodegas() As Variant
Dim i As Double, J As Double
Dim SQL As String

'HRR Req.1386
    Valores = "TX_DESC_FORM='KARDEX POR USO',TX_FORM_FORM='FrmKardexUso',TX_TIPO_FORM='FRM'"
    Result = DoInsertSQL("FORMULARIO", Valores)
    If Result <> FAIL Then
       ReDim Arr(0)
       Result = LoadData("FORMULARIO", "NU_AUTO_FORM", "TX_FORM_FORM='FrmKardexUso'", Arr)
       If Result <> FAIL And Encontro Then
          Valores = "TX_CODPADR_OPCI='04',NU_AUTO_FORM_OPCI=" & Arr(0) & ","
          ReDim Arr(0)
          Result = LoadData("OPCION", "max(TX_CODI_OPCI)", "TX_CODI_OPCI LIKE '04%'", Arr)
          If Result <> FAIL Then
             If Arr(0) = NUL$ Then Arr(0) = "0400"
             Valores = Valores & "TX_CODI_OPCI='0" & CInt(Arr(0)) + 1 & "', TX_DESC_OPCI='KARDEX POR USO',"
             Result = DoInsertSQL("OPCION", Valores)
             If Result <> FAIL Then Call CrearOpcion("0" & CStr(CInt(Arr(0)) + 1))
          End If
       End If
    End If
    'HRR Req. 1309
    Valores = "TX_DESC_FORM='UNIFICAR TERCEROS',TX_FORM_FORM='FrmUnificaTerc',TX_TIPO_FORM='FRM'"
    Result = DoInsertSQL("FORMULARIO", Valores)
    If Result <> FAIL Then
       ReDim Arr(0)
       Result = LoadData("FORMULARIO", "NU_AUTO_FORM", "TX_FORM_FORM='FrmUnificaTerc'", Arr)
       If Result <> FAIL And Encontro Then
          Valores = "TX_CODPADR_OPCI='05',NU_AUTO_FORM_OPCI=" & Arr(0) & ","
          ReDim Arr(0)
          Result = LoadData("OPCION", "max(TX_CODI_OPCI)", "TX_CODI_OPCI LIKE '05%'", Arr)
          If Result <> FAIL Then
             If Arr(0) = NUL$ Then Arr(0) = "0500"
             Valores = Valores & "TX_CODI_OPCI='0" & CInt(Arr(0)) + 1 & "', TX_DESC_OPCI='UNIFICAR TERCEROS',"
             Result = DoInsertSQL("OPCION", Valores)
             If Result <> FAIL Then Call CrearOpcion("0" & CStr(CInt(Arr(0)) + 1))
          End If
       End If
    End If
    
    'tabla que lleva el registro de la persona que realiza la unificacion de terceros
    If Not ExisteTABLA("REG_UNIFICACION") Then
      Valores = "FE_FECHA_REUN, 8, 10, 0"  'fecha
      If MotorBD = "ACCESS" Then
        SQL = "CREATE TABLE REG_UNIFICACION (FE_FECHA_REUN DATETIME NOT NULL)"
        Result = ExecSQL(SQL)
      Else
        Result = CrearTabla("REG_UNIFICACION", Valores)
      End If
      Result = CrearCampo("REG_UNIFICACION", "TX_NUMCON_REUN", 10, 15, 0) 'numero de conexion o usuario
      Result = CrearCampo("REG_UNIFICACION", "TX_OLDTER_REUN", 10, 15, 0) 'nit antes del tercero
      Result = CrearCampo("REG_UNIFICACION", "TX_NEWTER_REUN", 10, 15, 0) 'nit nuevo del tercero
      Result = CrearCampo("REG_UNIFICACION", "NU_AUTO_REUN", 4, 4, 0, True) 'autonumerico
    End If
 
 'Req 1552 CTAS CONTABLES DE GRUPO X BODEGA
    If MotorBD = "ACCESS" Then
        SQL = "CREATE TABLE IN_R_GRUP_BODE (NU_AUTO_BODE_RGB INT NOT NULL)"
        Result = ExecSQL(SQL)
        
        SQL = "ALTER TABLE IN_R_GRUP_BODE ADD CD_CODI_GRUP_RGB TEXT(9) not null"
        Result = ExecSQL(SQL)
    Else
        Result = CrearTabla("IN_R_GRUP_BODE", "NU_AUTO_BODE_RGB,4,9,0")
        Result = CrearCampo("IN_R_GRUP_BODE", "CD_CODI_GRUP_RGB", 10, 9, 0)
    End If
    
    Result = CrearCampo("IN_R_GRUP_BODE", "CD_ENTR_RGB", 10, 15, 1)
    Result = CrearCampo("IN_R_GRUP_BODE", "CD_SALI_RGB", 10, 15, 1)
    Result = CrearCampo("IN_R_GRUP_BODE", "CD_INGR_RGB", 10, 15, 1)
    Result = CrearCampo("IN_R_GRUP_BODE", "CD_GAST_RGB", 10, 15, 1)
    Result = CrearCampo("IN_R_GRUP_BODE", "CD_COST_RGB", 10, 15, 1)
    Result = CrearCampo("IN_R_GRUP_BODE", "CD_AJDB_RGB", 10, 15, 1)
    Result = CrearCampo("IN_R_GRUP_BODE", "CD_AJCR_RGB", 10, 15, 1)
    Result = CrearIndice("IN_R_GRUP_BODE", "PK_RGRUP_BODEGA", "CD_CODI_GRUP_RGB,NU_AUTO_BODE_RGB", True)
    
    If Result <> FAIL Then Result = CrearRelacion("INBODEGA_RGRUP_BODE", "NU_AUTO_BODE_RGB", "NU_AUTO_BODE", "IN_R_GRUP_BODE", "IN_BODEGA")
    If Result <> FAIL Then Result = CrearRelacion("GRUPARTICULO_RGRUP_BODE", "CD_CODI_GRUP_RGB", "CD_CODI_GRUP", "IN_R_GRUP_BODE", "GRUP_ARTICULO")
    
    If Result <> FAIL Then
        ReDim CtasGrupo(10): ReDim bodegas(0, 0)
        Campos = "CD_CODI_GRUP,CD_ENTR_GRUP,CD_SALI_GRUP,CD_INGR_GRUP,CD_GAST_GRUP,CD_COST_GRUP,CD_AGAS_GRUP,CD_AING_GRUP,CD_AJDB_GRUP,CD_AJCR_GRUP"
        Result = LoadMulData("GRUP_ARTICULO", Campos, NUL$, CtasGrupo) 'Busca la parametrizaci�n existente
        If Result <> FAIL And Encontro Then
           If Result <> FAIL Then
             Result = LoadMulData("IN_BODEGA ORDER BY NU_AUTO_BODE", "NU_AUTO_BODE", NUL$, bodegas)  'Busca las bodegas
             If Result <> FAIL And Encontro Then
                For i = 0 To UBound(CtasGrupo, 2)
                  For J = 0 To UBound(bodegas, 2)
                     Valores = "NU_AUTO_BODE_RGB=" & bodegas(0, J) & Coma
                     Valores = Valores & "CD_CODI_GRUP_RGB=" & Comi & CtasGrupo(0, i) & Comi & Coma
                     Valores = Valores & "CD_ENTR_RGB=" & Comi & CtasGrupo(1, i) & Comi & Coma
                     Valores = Valores & "CD_SALI_RGB=" & Comi & CtasGrupo(2, i) & Comi & Coma
                     Valores = Valores & "CD_INGR_RGB=" & Comi & CtasGrupo(3, i) & Comi & Coma
                     Valores = Valores & "CD_GAST_RGB=" & Comi & CtasGrupo(4, i) & Comi & Coma
                     Valores = Valores & "CD_COST_RGB=" & Comi & CtasGrupo(5, i) & Comi & Coma
                     'Valores = Valores & "CD_AGAS_RGB=" & Comi & CtasGrupo(6, I) & Comi & Coma
                     'Valores = Valores & "CD_AING_RGB=" & Comi & CtasGrupo(7, I) & Comi & Coma
                     Valores = Valores & "CD_AJDB_RGB=" & Comi & CtasGrupo(8, i) & Comi & Coma
                     Valores = Valores & "CD_AJCR_RGB=" & Comi & CtasGrupo(9, i) & Comi
                     Result = DoInsertSQL("IN_R_GRUP_BODE", Valores)
                     If Result = FAIL Then Exit For
                  Next J
                  If Result = FAIL Then Exit For
                Next i
             End If
           End If
        End If
    End If
    If Result <> FAIL Then Result = EliminarCampo("GRUP_ARTICULO", "CD_ENTR_GRUP")
    If Result <> FAIL Then Result = EliminarCampo("GRUP_ARTICULO", "CD_SALI_GRUP")
    If Result <> FAIL Then Result = EliminarCampo("GRUP_ARTICULO", "CD_INGR_GRUP")
    If Result <> FAIL Then Result = EliminarCampo("GRUP_ARTICULO", "CD_GAST_GRUP")
    If Result <> FAIL Then Result = EliminarCampo("GRUP_ARTICULO", "CD_COST_GRUP")
    If Result <> FAIL Then Result = EliminarCampo("GRUP_ARTICULO", "CD_AJDB_GRUP")
    If Result <> FAIL Then Result = EliminarCampo("GRUP_ARTICULO", "CD_AJCR_GRUP")
    'Result = EliminarIndice("GRUP_ARTICULO", BuscarIndice("GRUP_ARTICULO", "CD_AGAS_GRUP"))
    'If Result <> FAIL Then Result = EliminarCampo("GRUP_ARTICULO", "CD_AGAS_GRUP")
    'Result = EliminarIndice("GRUP_ARTICULO", BuscarIndice("GRUP_ARTICULO", "CD_AING_GRUP"))
    'If Result <> FAIL Then Result = EliminarCampo("GRUP_ARTICULO", "CD_AING_GRUP")
    
    'Comprobante contable para el Traslado TRS
    'Result = DoInsertSQL("TC_COMPROBANTE", "CD_CODI_COMP='TRS', NO_NOMB_COMP='TRASLADO ENTRE BODEGAS', NU_CONS_COMP=0, ID_SIIF_COMP='N', ID_TRAN_COMP='N'")
    Campos = "CD_CODI_COMP='TRS', NO_NOMB_COMP='TRASLADO ENTRE BODEGAS', NU_CONS_COMP=0, ID_SIIF_COMP='N'"      'PJCA M1606
    If ExisteCAMPO("TC_COMPROBANTE", "ID_TRAN_COMP") Then Campos = Campos & ", ID_TRAN_COMP='N'"                'PJCA M1606
    Result = DoInsertSQL("TC_COMPROBANTE", Campos)                                                              'PJCA M1606
    Result = DoInsertSQL("CONCEPTOT", "NU_MOVI_CONC=10,CD_COMP_CONC='TRS'")
    
    'Comprobante contable para el Despacho DSP
    'Result = DoInsertSQL("TC_COMPROBANTE", "CD_CODI_COMP='DSP', NO_NOMB_COMP='DESPACHO DE INVENTARIO', NU_CONS_COMP=0, ID_SIIF_COMP='N', ID_TRAN_COMP='N'")
    Campos = "CD_CODI_COMP='DSP', NO_NOMB_COMP='DESPACHO DE INVENTARIO', NU_CONS_COMP=0, ID_SIIF_COMP='N'"      'PJCA M1606
    If ExisteCAMPO("TC_COMPROBANTE", "ID_TRAN_COMP") Then Campos = Campos & ", ID_TRAN_COMP='N'"                'PJCA M1606
    Result = DoInsertSQL("TC_COMPROBANTE", Campos)                                                              'PJCA M1606
    Result = DoInsertSQL("CONCEPTOT", "NU_MOVI_CONC=6,CD_COMP_CONC='DSP'")
    
    'Comprobante contable para la Devoluci�n de Despacho DDP
    'Result = DoInsertSQL("TC_COMPROBANTE", "CD_CODI_COMP='DDP', NO_NOMB_COMP='DEVOLUCION DE DESPACHO', NU_CONS_COMP=0, ID_SIIF_COMP='N', ID_TRAN_COMP='N'")
    Campos = "CD_CODI_COMP='DDP', NO_NOMB_COMP='DEVOLUCION DE DESPACHO', NU_CONS_COMP=0, ID_SIIF_COMP='N'"      'PJCA M1606
    If ExisteCAMPO("TC_COMPROBANTE", "ID_TRAN_COMP") Then Campos = Campos & ", ID_TRAN_COMP='N'"                'PJCA M1606
    Result = DoInsertSQL("TC_COMPROBANTE", Campos)                                                              'PJCA M1606
    Result = DoInsertSQL("CONCEPTOT", "NU_MOVI_CONC=17,CD_COMP_CONC='DDP'")
   
'Req 1552
    
    'HRR Req1553
    Result = CrearCampo("PARAMETROS_INVE", "ID_ENTDEP_PINV", 10, 1, 1, False) 'indica si se puede modificar los valores de una entrada de almacen de una orden de compra
    'N: No, S: Si
    'Req1553
    
 If Result <> FAIL Then Result = DoUpdate("PARAMETROS_INVE", "NU_NVERDB_PINV='5.7.0'", NUL$)
 
End Sub

Sub Actualizar_5_8_0()
     
    Dim Arr(0)
    
    '00 |.DR.|, R:1633
    If ExisteTABLA("IN_BODEGA") Then
       If Not ExisteCAMPO("IN_BODEGA", "TX_CONTA_BODE") Then
          Result = CrearCampo("IN_BODEGA", "TX_CONTA_BODE", 10, 1, 1)
          If Result <> FAIL Then Result = DoUpdate("IN_BODEGA", "TX_CONTA_BODE='S'", NUL$)
       End If
    End If
    '00 Fin.
    
    '01 |.DR.|, R:979-1295-1632

'Result = ExecSQLCommand("INSERT INTO IN_DOCUMENTO (NU_AUTO_DOCU, TX_CODI_DOCU, TX_NOMB_DOCU, TX_AFEC_DOCU, TX_INTE_DOCU, NU_AUTO_DOCUDEPE_DOCU, NU_AUTO_DOCUREVE_DOCU,TX_TIPOVAL_DOCU, TX_INDE_DOCU) VALUES ('31','COMPRA','COMPRA (ENTRADA DE MERCANCIA)','N','S','3','0','C','N')")

If Not ExisteCAMPO("IN_R_GRUP_BODE", "CD_TRAN_RGB") Then Result = CrearCampo("IN_R_GRUP_BODE", "CD_TRAN_RGB", 10, 15, 1)
If Not ExisteCAMPO("PARAMETROS_INVE", "CD_CXPT_APLI") Then Result = CrearCampo("PARAMETROS_INVE", "CD_CXPT_APLI", 10, 15, 1)

If Not ExisteTABLA("IN_COMPRA") Then
    If MotorBD <> "ACCESS" Then
        Result = CrearTabla("IN_COMPRA", "FE_FECH_COMP, 8, 0, 0")
        Result = CrearCampo("IN_COMPRA", "FE_VENC_COMP", 8, 0, 0)
        Result = CrearCampo("IN_COMPRA", "NU_AUTO_COMP", 4, 0, 0, True)
        Result = CrearCampo("IN_COMPRA", "CD_CODI_TERC_COMP", 10, 20, 0)
        Result = CrearCampo("IN_COMPRA", "TX_FAPR_COMP", 10, 10, 0)
        Result = CrearCampo("IN_COMPRA", "VL_BRUT_COMP", 7, 0, 0)
        Result = CrearCampo("IN_COMPRA", "VL_DESC_COMP", 7, 0, 0)
        Result = CrearCampo("IN_COMPRA", "VL_IMPU_COMP", 7, 0, 0)
        Result = CrearCampo("IN_COMPRA", "VL_RETE_COMP", 7, 0, 0)
        Result = CrearCampo("IN_COMPRA", "VL_FLET_COMP", 7, 0, 0)
        Result = CrearCampo("IN_COMPRA", "VL_NETO_COMP", 7, 0, 0)
        Result = CrearCampo("IN_COMPRA", "TX_ESTA_COMP", 2, 0, 0)
        Result = CrearCampo("IN_COMPRA", "NU_CXP_COMP", 7, 0, 1)
        Result = CrearCampo("IN_COMPRA", "NU_CONS_MOVI_COMP", 4, 0, 1)
        Result = CrearCampo("IN_COMPRA", "TX_OBSE_COMP", 12, 0, 1)
        Result = CrearIndice("IN_COMPRA", "PK_IN_COMPRA", "NU_AUTO_COMP", True)
    Else
        Result = ExecSQLCommand("CREATE TABLE IN_COMPRA ( FE_FECH_COMP DATETIME NOT NULL , FE_VENC_COMP DATETIME NOT NULL , NU_AUTO_COMP COUNTER NOT NULL , CD_CODI_TERC_COMP TEXT (20)  NOT NULL , TX_FAPR_COMP TEXT (10)  NOT NULL , VL_BRUT_COMP FLOAT NOT NULL , VL_DESC_COMP FLOAT NOT NULL , VL_IMPU_COMP FLOAT NOT NULL , VL_RETE_COMP FLOAT NOT NULL , VL_FLET_COMP FLOAT NOT NULL , VL_NETO_COMP FLOAT NOT NULL , TX_ESTA_COMP BYTE NOT NULL , NU_CXP_COMP FLOAT NULL , NU_CONS_MOVI_COMP INT NULL,TX_OBSE_COMP MEMO  NULL )")
        Result = ExecSQLCommand("ALTER TABLE IN_COMPRA ADD CONSTRAINT PK_IN_COMPRA PRIMARY KEY  (  NU_AUTO_COMP )")
    End If
End If

If Not ExisteTABLA("IN_R_COMP_ENCA") Then
    If MotorBD <> "ACCESS" Then
        Result = CrearTabla("IN_R_COMP_ENCA", "NU_AUTO_COMP_COEN, 4, 0, 0")
        Result = CrearCampo("IN_R_COMP_ENCA", "NU_AUTO_ENCA_COEN", 4, 0, 0)
        Result = CrearCampo("IN_R_COMP_ENCA", "NU_AUTO_COEN", 4, 0, 0, True)
        Result = CrearCampo("IN_R_COMP_ENCA", "VL_COST_COEN", 7, 0, 0)
        Result = CrearIndice("IN_R_COMP_ENCA", "PK_IN_R_COMP_ENCA", "NU_AUTO_COEN", True)
    Else
        Result = ExecSQLCommand("CREATE TABLE IN_R_COMP_ENCA ( NU_AUTO_COMP_COEN INT NOT NULL , NU_AUTO_ENCA_COEN INT NOT NULL , NU_AUTO_COEN COUNTER NOT NULL, VL_COST_COEN FLOAT NOT NULL )")
        Result = ExecSQLCommand("ALTER TABLE IN_R_COMP_ENCA ADD CONSTRAINT PK_IN_R_COMP_ENCA PRIMARY KEY  (  NU_AUTO_COEN )")
    End If
End If

If Not ExisteTABLA("IN_R_COMP_IMDE") Then
    If MotorBD <> "ACCESS" Then
        Result = CrearTabla("IN_R_COMP_IMDE", "NU_AUTO_COMP_CCOID, 4, 0, 0")
        Result = CrearCampo("IN_R_COMP_IMDE", "NU_AUTO_CCOID", 4, 0, 0, True)
        Result = CrearCampo("IN_R_COMP_IMDE", "TX_NOMB_CCOID", 10, 30, 0)
        Result = CrearCampo("IN_R_COMP_IMDE", "NU_POSI_CCOID", 10, 1, 0)
        Result = CrearCampo("IN_R_COMP_IMDE", "NU_VALO_CCOID", 7, 0, 1)
        Result = CrearCampo("IN_R_COMP_IMDE", "TX_TIPO_CCOID", 10, 1, 0)
        Result = CrearCampo("IN_R_COMP_IMDE", "CD_CODI_IMPU_CCOID", 10, 3, 1)
        Result = CrearCampo("IN_R_COMP_IMDE", "CD_CODI_DESC_CCOID", 10, 3, 1)
        Result = CrearCampo("IN_R_COMP_IMDE", "NU_PRAPL_CCOID", 6, 0, 1)
        Result = CrearIndice("IN_R_COMP_IMDE", "PK_IN_R_COMP_IMDE", "NU_AUTO_CCOID", True)
    Else
        Result = ExecSQLCommand("CREATE TABLE IN_R_COMP_IMDE ( NU_AUTO_COMP_CCOID INT NOT NULL , NU_AUTO_CCOID COUNTER NOT NULL , TX_NOMB_CCOID TEXT (30)  NOT NULL , NU_POSI_CCOID TEXT (1)  NOT NULL , NU_VALO_CCOID FLOAT NOT NULL , TX_TIPO_CCOID TEXT (1)  NOT NULL , CD_CODI_IMPU_CCOID TEXT (3)  NULL , CD_CODI_DESC_CCOID TEXT (3)  NULL , NU_PRAPL_CCOID REAL NOT NULL )")
        Result = ExecSQLCommand("ALTER TABLE IN_R_COMP_IMDE ADD CONSTRAINT PK_IN_R_COMP_IMDE PRIMARY KEY  CLUSTERED  (  NU_AUTO_CCOID )")
    End If
End If
    
    
    'Result = DoInsertSQL("TC_COMPROBANTE", "CD_CODI_COMP='CPI', NO_NOMB_COMP='COMPRA INVENTARIOS', NU_CONS_COMP=0, ID_SIIF_COMP='N', ID_TRAN_COMP='N'")
    Campos = "CD_CODI_COMP='CPI', NO_NOMB_COMP='COMPRA INVENTARIOS', NU_CONS_COMP=0, ID_SIIF_COMP='N'"
    If ExisteCAMPO("TC_COMPROBANTE", "ID_TRAN_COMP") Then Campos = Campos & ", ID_TRAN_COMP='N'"
    Result = DoInsertSQL("TC_COMPROBANTE", Campos)
    Result = DoInsertSQL("CONCEPTOT", "NU_MOVI_CONC=25,CD_COMP_CONC='CPI'")
    '01 Fin.
    
    'HRR R1249
    If ExisteTABLA("PARAMETROS_INVE") Then
       If Not ExisteCAMPO("PARAMETROS_INVE", "NU_NUMESTA_PINV") Then
          Result = CrearCampo("PARAMETROS_INVE", "NU_NUMESTA_PINV", 10, 10, 1)
          If Result <> FAIL Then Result = DoUpdate("PARAMETROS_INVE", "NU_NUMESTA_PINV = '0'", NUL$)
          If Result <> FAIL Then Result = ExecSQLCommand("ALTER TABLE PARAMETROS_INVE ALTER COLUMN NU_NUMESTA_PINV VARCHAR (10) NOT NULL")
          If Result <> FAIL Then
             Valores = "NU_NUMESTA_PINV=" & Comi & ConvertCadenaHexa(Encriptar(UCase(Trim("0")), UCase(Trim(IdEntidad)))) & Comi
             Result = DoUpdate("PARAMETROS_INVE", Valores, NUL$)
          End If
       End If
       If Not ExisteCAMPO("PARAMETROS_INVE", "FE_LICE_PINV") Then
          Result = CrearCampo("PARAMETROS_INVE", "FE_LICE_PINV", 8, 0, 1)
       End If
    End If
    'R1249
    
    'GMS R1636
    If ExisteTABLA("ARTICULO") Then
       If Not ExisteCAMPO("ARTICULO", "TX_ESTA_ARTI") Then
          Result = CrearCampo("ARTICULO", "TX_ESTA_ARTI", 10, 1, 1)
          If Result <> FAIL Then Result = DoUpdate("ARTICULO", "TX_ESTA_ARTI = '0'", NUL$)
          If Result <> FAIL Then Result = ExecSQLCommand("ALTER TABLE ARTICULO ALTER COLUMN TX_ESTA_ARTI  VARCHAR ( 1 ) NOT NULL")
       End If
    End If
    ' SMDL R1393
    If (BeginTran(STranIUp & "Actualizacion 5.8.0") <> FAIL) Then
        Valores = "TX_DESC_FORM='INICIALIZAR MOVIMIENTO',TX_FORM_FORM='FrmClave',TX_TIPO_FORM='FRM'"
        Result = DoInsertSQL("FORMULARIO", Valores)
        If Result <> FAIL Then
           'ReDim Arr(0)
           Result = LoadData("FORMULARIO", "NU_AUTO_FORM", "TX_FORM_FORM='FrmClave'", Arr)
           If Result <> FAIL And Encontro Then
              Valores = "TX_CODPADR_OPCI='05',NU_AUTO_FORM_OPCI=" & Arr(0) & ","
            '  ReDim Arr(0)
              Result = LoadData("OPCION", "max(TX_CODI_OPCI)", "TX_CODI_OPCI LIKE '05%'", Arr)
              If Result <> FAIL Then
                 If Arr(0) = NUL$ Then Arr(0) = "0500"
                 Valores = Valores & "TX_CODI_OPCI='0" & CInt(Arr(0)) + 1 & "', TX_DESC_OPCI='INICIALIZAR MOVIMIENTO',"
                 Result = DoInsertSQL("OPCION", Valores)
                 If Result <> FAIL Then Call CrearOpcion("0" & CStr(CInt(Arr(0)) + 1))
              End If
           End If
        End If
    End If
    If (Result <> FAIL) Then
       If (CommitTran() = FAIL) Then
          Call RollBackTran
          Call Mensaje1("No se registraron los cambios de la transacci�n Req 1393", 1)
       End If
    Else
       Call RollBackTran
       Call Mensaje1("No se registraron los cambios de la transacci�n Req 1393", 1)
    End If
    'smdl req.1393
    
    'SMDL M1451
    If ExisteTABLA("IN_AUTORIZACION") Then
       Result = ExecSQLCommand("ALTER TABLE IN_AUTORIZACION ALTER COLUMN NU_AUTO_USUA_AUTO INT NULL")
    End If
    'SMDL M1451

    
    If Result <> FAIL Then Result = DoUpdate("PARAMETROS_INVE", "NU_NVERDB_PINV='5.8.0'", NUL$)

End Sub

Sub Actualizar_5_9_0()

    '00 |.DR.|, R:1400
        fnAgregarOpcion "LOGIN DE ACCESO", "frmBDLogin", "01", "FRM"
    '00 Fin.
    
    'NMSR R1690
    If Not ExisteCAMPO("ARTICULO", "TX_INVIMA_ARTI") Then Result = CrearCampo("ARTICULO", "TX_INVIMA_ARTI ", 10, 50, 1)
    'NMSR R1690
    
    If Result <> FAIL Then Result = DoUpdate("PARAMETROS_INVE", "NU_NVERDB_PINV='5.9.0'", NUL$)

End Sub

Sub Actualizar_6_0()
   ReDim Arr(0)
   Dim StSql As String 'NMSR R1754-1839
   
   Result = 1
   'HRR R1717
   If Not ExisteCAMPO("TERCERO", "TX_PNOM_TERC") Then
      Result = CrearCampo("TERCERO", "TX_PNOM_TERC", 10, 20, 1)
   End If
   If Not ExisteCAMPO("TERCERO", "TX_SNOM_TERC") Then
      Result = CrearCampo("TERCERO", "TX_SNOM_TERC", 10, 20, 1)
   End If
   If Not ExisteCAMPO("TERCERO", "TX_PAPE_TERC") Then
      Result = CrearCampo("TERCERO", "TX_PAPE_TERC", 10, 20, 1)
   End If
   If Not ExisteCAMPO("TERCERO", "TX_SAPE_TERC") Then
      Result = CrearCampo("TERCERO", "TX_SAPE_TERC", 10, 20, 1)
   End If
   'HRR R1717
   'NMSR R1754-1839
    Valores = "TX_DESC_FORM='CIRC 01 CNM',TX_FORM_FORM='FrmPlanCirc0102',TX_TIPO_FORM='FRM'"
    Result = DoInsertSQL("FORMULARIO", Valores)
    If Result <> FAIL Then
       ReDim Arr(0)
       Result = LoadData("FORMULARIO", "NU_AUTO_FORM", "TX_FORM_FORM='FrmPlanCirc0102'", Arr)
       If Result <> FAIL And Encontro Then
          Valores = "TX_CODPADR_OPCI='04',NU_AUTO_FORM_OPCI=" & Arr(0) & ","
          ReDim Arr(0)
          Result = LoadData("OPCION", "max(TX_CODI_OPCI)", "TX_CODI_OPCI LIKE '04" & CaractLike & "'", Arr)
          If Result <> FAIL Then
             If Arr(0) = NUL$ Then Arr(0) = "0400"
             Valores = Valores & "TX_CODI_OPCI='0" & CInt(Arr(0)) + 1 & "', TX_DESC_OPCI='PLANO CIRC 01-02 CNM',"
             Result = DoInsertSQL("OPCION", Valores)
             If Result <> FAIL Then Call CrearOpcion("0" & CStr(CInt(Arr(0)) + 1))
          End If
       End If
    End If
    StSql = "SELECT FE_FECH_COMP,CD_CODI_ARTI"
    StSql = StSql & ",MIN(NU_COSTO_DETA) AS MINIMO, MAX(NU_COSTO_DETA) AS MAXIMO"
    StSql = StSql & ", SUM(NU_ENTRAD_DETA * NU_COSTO_DETA) AS COMP_NETA"
    StSql = StSql & ",SUM(((NU_ENTRAD_DETA - NU_SALIDA_DETA)*NU_MULT_DETA)/NU_DIVI_DETA) AS UNID_COMPR"
    StSql = StSql & ",TX_FAPR_COMP"
    StSql = StSql & " FROM IN_COMPRA, IN_R_COMP_ENCA, IN_ENCABEZADO, IN_DETALLE, ARTICULO"
    StSql = StSql & " WHERE NU_AUTO_COMP_COEN = NU_AUTO_COMP"
    StSql = StSql & " AND NU_AUTO_ENCA_COEN = NU_AUTO_ENCA"
    StSql = StSql & " AND NU_AUTO_ENCA=NU_AUTO_ORGCABE_DETA"
    StSql = StSql & " AND NU_AUTO_ENCA=NU_AUTO_MODCABE_DETA"
    StSql = StSql & " AND NU_AUTO_ARTI=NU_AUTO_ARTI_DETA"
    StSql = StSql & " AND NU_AUTO_DOCU_ENCA=3"
    StSql = StSql & " AND TX_ESTA_COMP=1"
    StSql = StSql & " GROUP BY CD_CODI_ARTI,FE_FECH_COMP,TX_FAPR_COMP"
    
    If Not fnExisteVista("VW_PLANOCIR01") Then Result = fnCrearVista("VW_PLANOCIR01", StSql, "Registro Detalle para la Circular 001-002 CNM")
    
    'NYCM R1235
    Valores = "TX_DESC_FORM='Entradas sin Compra',TX_FORM_FORM='FrmEntsincompra',TX_TIPO_FORM='FRM'"
    Result = DoInsertSQL("FORMULARIO", Valores)
    If Result <> FAIL Then
       ReDim Arr(0)
       Result = LoadData("FORMULARIO", "NU_AUTO_FORM", "TX_FORM_FORM='FrmEntsincompra'", Arr)
       If Result <> FAIL And Encontro Then
          Valores = "TX_CODPADR_OPCI='04',NU_AUTO_FORM_OPCI=" & Arr(0) & ","
          ReDim Arr(0)
          Result = LoadData("OPCION", "max(TX_CODI_OPCI)", "TX_CODI_OPCI LIKE '04%'", Arr)
          If Result <> FAIL Then
             If Arr(0) = NUL$ Then Arr(0) = "0400"
             Valores = Valores & "TX_CODI_OPCI='0" & CInt(Arr(0)) + 1 & "', TX_DESC_OPCI='ENTRADAS SIN COMPRA',"
             Result = DoInsertSQL("OPCION", Valores)
             If Result <> FAIL Then Call CrearOpcion("0" & CStr(CInt(Arr(0)) + 1))
       End If
    End If
        
    End If
   'NMSR R1754-1839
   
   '----------------------------------------------
   'GMS 2007-10-18. M�dulo de Costos
   '----------------------------------------------
   'Creaci�n del campo TX_ROPA_ARTI en la tabla
   'ARTICULO.
   '----------------------------------------------
   If ExisteTABLA("ARTICULO") Then
      If Not ExisteCAMPO("ARTICULO", "TX_ROPA_ARTI") Then
         Result = CrearCampo("ARTICULO", "TX_ROPA_ARTI", 10, 1, 1)
         If Result <> FAIL Then Result = DoUpdate("ARTICULO", "TX_ROPA_ARTI='0'", NUL$)
         If Result <> FAIL Then Result = ExecSQLCommand("ALTER TABLE ARTICULO ALTER COLUMN TX_ROPA_ARTI VARCHAR (1) NOT NULL")
         If MotorBD = "SQL" Then
            If Result <> FAIL Then Result = ExecSQLCommand("ALTER TABLE ARTICULO ADD CONSTRAINT DF_ARTICULO_TX_ROPA_ARTI DEFAULT (0) FOR TX_ROPA_ARTI")
         End If
      End If
   End If
   '----------------------------------------------
   
   'HRR M2264
   If Not ExisteTABLA("IN_R_ENCA_ENCA") Then
      If MotorBD = "SQL" Then 'HRR M2508
         Result = CrearTabla("IN_R_ENCA_ENCA", "NU_ENCCMP_RENEN,4,0,0") 'Autonumerico del encabezado del comprobante
         If Result <> FAIL Then Result = CrearCampo("IN_R_ENCA_ENCA", "NU_ENCDEP_RENEN", 4, 0, 0) 'Autonumerico del encabezado del cual depende
         If Result <> FAIL Then Result = CrearCampo("IN_R_ENCA_ENCA", "NU_AUTO_RENEN", 4, 0, 0, True) 'Autonumerico
         If Result <> FAIL Then Result = CrearIndice("IN_R_ENCA_ENCA", "PK_IN_R_ENCA_ENCA", "NU_AUTO_RENEN", True, True)
         If Result <> FAIL Then Result = CrearIndice("IN_R_ENCA_ENCA", "LL_IN_R_ENCA_ENCA", "NU_ENCCMP_RENEN", False, False)
      'HRR M2508
      ElseIf MotorBD = "ACCESS" Then
         Result = ExecSQLCommand("CREATE TABLE IN_R_ENCA_ENCA")
         If Result <> FAIL Then Result = CrearCampo("IN_R_ENCA_ENCA", "NU_ENCCMP_RENEN", 4, 0, 0)
         If Result <> FAIL Then Result = CrearCampo("IN_R_ENCA_ENCA", "NU_ENCDEP_RENEN", 4, 0, 0) 'Autonumerico del encabezado del cual depende
         If Result <> FAIL Then Result = CrearCampo("IN_R_ENCA_ENCA", "NU_AUTO_RENEN", 4, 0, 0, True) 'Autonumerico
         If Result <> FAIL Then Result = ExecSQLCommand("ALTER TABLE IN_R_ENCA_ENCA ADD CONSTRAINT PK_IN_R_ENCA_ENCA PRIMARY KEY  CLUSTERED  (  NU_AUTO_RENEN )")
         If Result <> FAIL Then Result = ExecSQLCommand("CREATE INDEX LL_IN_R_ENCA_ENCA ON IN_R_ENCA_ENCA (NU_ENCCMP_RENEN)")
      End If
      'HRR M2508
   End If
   'HRR M2264
   
   'HRR R1853
   If Not ExisteCAMPO("PARAMETROS_INVE", "TX_IMPCOMP_PINV") Then
      If Result <> FAIL Then Result = CrearCampo("PARAMETROS_INVE", "TX_IMPCOMP_PINV", 10, 1, 1)
   End If
     
   If Not fnExisteVista("VW_ENTRSINCOMP") Then
      
       StSql = "SELECT NU_AUTO_ENCA,NU_COMP_ENCA,CD_CODI_TERC_ENCA"
       StSql = StSql & " FROM IN_ENCABEZADO,IN_DOCUMENTO"
       StSql = StSql & "  WHERE NU_AUTO_DOCU_ENCA=NU_AUTO_DOCU "
       StSql = StSql & " AND NU_AUTO_ENCA NOT IN (SELECT NU_AUTO_ENCA_COEN FROM IN_R_COMP_ENCA)"
       StSql = StSql & " AND NU_AUTO_DOCU= 3 "
       StSql = StSql & " AND TX_ESTA_ENCA<>'A' "
       
       If Result <> FAIL Then Result = fnCrearVista("VW_ENTRSINCOMP", StSql, "Entradas sin compra")
    End If
   'HRR R1853
    
   'HRR R1873
   If Not ExisteCAMPO("IN_COMPRA", "FE_FACT_COMP") Then
      If Result <> FAIL Then Result = CrearCampo("IN_COMPRA", "FE_FACT_COMP", 8, 0, 1)
      If Result <> FAIL Then Result = DoUpdate("IN_COMPRA", "FE_FACT_COMP=FE_FECH_COMP", NUL$)
      If Result <> FAIL Then Result = ExecSQLCommand("ALTER TABLE IN_COMPRA ALTER COLUMN FE_FACT_COMP DATETIME NOT NULL")
   End If
   If Not ExisteCAMPO("PARAMETROS_INVE", "TX_MESOPEN_APLI") Then
      If Result <> FAIL Then Result = CrearCampo("PARAMETROS_INVE", "TX_MESOPEN_APLI", 10, 1, 1)
   End If
   'HRR R1873
   
   If Result <> FAIL Then Result = DoUpdate("PARAMETROS_INVE", "NU_NVERDB_PINV='6.0.0'", NUL$)
   
End Sub


Sub Actualizar_6_1()
Dim InI As Integer 'NMSR M3031
Dim ArComp() As Variant 'NMSR M3031

    fnAgregarOpcion "BACKUP BASE DE DATOS", NUL$, "01", NUL$
'    Result = DoUpdate("TC_COMPROBANTE", "ID_TRAN_COMP='S' ", "CD_CODI_COMP IN ('ENA','SLD','VTI','BJP','DDC','DAP','APD','AJI','TRS','DSP','DDP','CPI','DBJ')")    'NMSR R1956-1969-1939
    'NMSR M3031
    If ExisteCAMPO("TC_COMPROBANTE", "ID_TRAN_COMP") Then
        Condicion = "CD_CODI_COMP='ENA'"
        Condicion = Condicion & " OR CD_CODI_COMP='SLD'"
        Condicion = Condicion & " OR CD_CODI_COMP='VTI'"
        Condicion = Condicion & " OR CD_CODI_COMP='BJP'"
        Condicion = Condicion & " OR CD_CODI_COMP='DDC'"
        Condicion = Condicion & " OR CD_CODI_COMP='DAP'"
        Condicion = Condicion & " OR CD_CODI_COMP='APD'"
        Condicion = Condicion & " OR CD_CODI_COMP='AJI'"
        Condicion = Condicion & " OR CD_CODI_COMP='TRS'"
        Condicion = Condicion & " OR CD_CODI_COMP='DSP'"
        Condicion = Condicion & " OR CD_CODI_COMP='DDP'"
        Condicion = Condicion & " OR CD_CODI_COMP='CPI'"
        Condicion = Condicion & " OR CD_CODI_COMP='DBJ'"
        
        ReDim ArComp(0, 0)
        Result = LoadMulData("TC_COMPROBANTE", "CD_CODI_COMP", Condicion, ArComp)
        If Result <> FAIL And Encontro Then
            For InI = 0 To UBound(ArComp, 2)
                Result = DoUpdate("TC_COMPROBANTE", "ID_TRAN_COMP='S' ", "CD_CODI_COMP=" & Comi & ArComp(0, InI) & Comi)
            Next
        End If
    End If
    'NMSR M3031
    
    'HRR R1946
      ReDim Arr(0)
      Result = LoadData("FORMULARIO", "TX_DESC_FORM", "TX_FORM_FORM='FrmCalCostoProm' AND TX_TIPO_FORM='FRM'", Arr)
      If Result <> FAIL And Not Encontro Then
         Result = DoInsertSQL("FORMULARIO", "TX_DESC_FORM='RECALCULAR COSTO PROMEDIO',TX_FORM_FORM='FrmCalCostoProm',TX_TIPO_FORM='FRM'")
         If Result <> FAIL Then
            
            Result = LoadData("FORMULARIO", "NU_AUTO_FORM", "TX_FORM_FORM='FrmCalCostoProm'", Arr)
            If Result <> FAIL And Encontro Then
               Valores = "TX_CODPADR_OPCI='05',NU_AUTO_FORM_OPCI=" & Arr(0) & ","
               Result = LoadData("OPCION", "max(TX_CODI_OPCI)", "TX_CODI_OPCI LIKE '05" & CaractLike & "'", Arr)
               If Result <> FAIL And Encontro Then
                  If Arr(0) = NUL$ Then Arr(0) = "0500"
                  Valores = Valores & "TX_CODI_OPCI='0" & CInt(Arr(0)) + 1 & "', TX_DESC_OPCI='RECALCULAR COSTO PROMEDIO'," 'HRR M2939
                  Result = DoInsertSQL("OPCION", Valores)
                  If Result <> FAIL Then Call CrearOpcion("0" & CStr(CInt(Arr(0)) + 1))
               End If
            End If
         End If
      End If
   
   If Result <> FAIL Then
      If Not ExisteTABLA("ARTI_COSTOPROM_TMP") Then
         Result = CrearTabla("ARTI_COSTOPROM_TMP", "NU_AUTO_ARTI_ACPT,4,0,0")
         If Result <> FAIL Then
            If MotorBD = "SQL" Then
               Result = CrearIndice("ARTI_COSTOPROM_TMP", "PK_ARTI_COSTOPROM_TMP", "NU_AUTO_ARTI_ACPT", True, True)
            ElseIf MotorBD = "ACCESS" Then
               BDDAO(BDCurCon).TableDefs.Refresh
               BDDAO(BDCurCon).Execute "ALTER TABLE ARTI_COSTOPROM_TMP ADD CONSTRAINT PK_ARTI_COSTOPROM_TMP PRIMARY KEY (NU_AUTO_ARTI_ACPT)"
            End If
         End If
         
      End If
   End If
    'HRR R1946
    
    'HRR R1872
      ReDim Arr(0)
      Result = LoadData("FORMULARIO", "TX_DESC_FORM", "TX_FORM_FORM='FrmOCPendiente' AND TX_TIPO_FORM='FRM'", Arr)
      If Result <> FAIL And Not Encontro Then
         Result = DoInsertSQL("FORMULARIO", "TX_DESC_FORM='ORDENES COMPRA PENDIENTES',TX_FORM_FORM='FrmOCPendiente',TX_TIPO_FORM='FRM'")
         If Result <> FAIL Then
               
            Result = LoadData("FORMULARIO", "NU_AUTO_FORM", "TX_FORM_FORM='FrmOCPendiente'", Arr)
            If Result <> FAIL And Encontro Then
               Valores = "TX_CODPADR_OPCI='04',NU_AUTO_FORM_OPCI=" & Arr(0) & ","
               Result = LoadData("OPCION", "max(TX_CODI_OPCI)", "TX_CODI_OPCI LIKE '04" & CaractLike & "'", Arr)
               If Result <> FAIL And Encontro Then
                  If Arr(0) = NUL$ Then Arr(0) = "0400"
                  Valores = Valores & "TX_CODI_OPCI='0" & CInt(Arr(0)) + 1 & "', TX_DESC_OPCI='ORDENES COMPRA PENDIENTES'," 'HRR M2939
                  Result = DoInsertSQL("OPCION", Valores)
                  If Result <> FAIL Then Call CrearOpcion("0" & CStr(CInt(Arr(0)) + 1))
               End If
            End If
               
         End If
      
      End If

    'HRR R1872
    
   'HRR R1905
   ReDim Arr(0)
   Result = LoadData("FORMULARIO", "TX_DESC_FORM", "TX_FORM_FORM='FrmInfCompra' AND TX_TIPO_FORM='FRM'", Arr)
   If Result <> FAIL And Not Encontro Then
      Result = DoInsertSQL("FORMULARIO", "TX_DESC_FORM='COMPRAS',TX_FORM_FORM='FrmInfCompra',TX_TIPO_FORM='FRM'")
      If Result <> FAIL Then
            
         Result = LoadData("FORMULARIO", "NU_AUTO_FORM", "TX_FORM_FORM='FrmInfCompra'", Arr)
         If Result <> FAIL And Encontro Then
            Valores = "TX_CODPADR_OPCI='04',NU_AUTO_FORM_OPCI=" & Arr(0) & ","
            Result = LoadData("OPCION", "max(TX_CODI_OPCI)", "TX_CODI_OPCI LIKE '04" & CaractLike & "'", Arr)
            If Result <> FAIL And Encontro Then
               If Arr(0) = NUL$ Then Arr(0) = "0400"
               Valores = Valores & "TX_CODI_OPCI='0" & CInt(Arr(0)) + 1 & "', TX_DESC_OPCI='COMPRAS'," 'HRR M2939
               Result = DoInsertSQL("OPCION", Valores)
               If Result <> FAIL Then Call CrearOpcion("0" & CStr(CInt(Arr(0)) + 1))
            End If
         End If
            
      End If
   
   End If
   'HRR R1905
   
   'NYCM M2940
   'Creaci�n del campo NU_AUTO_ENCAREAL_ENID en la tabla IN_R_ENCA_IMDE
   '----------------------------------------------
   If ExisteTABLA("IN_R_ENCA_IMDE") Then
      If Not ExisteCAMPO("IN_R_ENCA_IMDE", "NU_AUTO_ENCAREAL_ENID") Then
         Result = CrearCampo("IN_R_ENCA_IMDE", "NU_AUTO_ENCAREAL_ENID", 4, 0, 1) 'Tipo int (4)
         If Result <> FAIL Then Result = DoUpdate("IN_R_ENCA_IMDE", "NU_AUTO_ENCAREAL_ENID=0", NUL$)
         If Result <> FAIL Then Result = ExecSQLCommand("ALTER TABLE IN_R_ENCA_IMDE ALTER COLUMN NU_AUTO_ENCAREAL_ENID INT NOT NULL")
      End If
      If MotorBD <> "ACCESS" Then
        'Actualizaci�n del campo NU_AUTO_ENCAREAL_ENID en la tabla IN_R_ENCA_IMDE cuando
        'existe una solo comprobante entrada de mercancia com impuestos o descuentos
        SQL = "UPDATE IN_R_ENCA_IMDE SET NU_AUTO_ENCAREAL_ENID=IN_ENCABEZADO.NU_AUTO_ENCA"
        SQL = SQL & " FROM (SELECT NU_COMP_ENCA AS NUMEROCOMP FROM IN_ENCABEZADO WHERE NU_AUTO_DOCU_ENCA=3"
        SQL = SQL & " GROUP BY NU_COMP_ENCA HAVING COUNT(*)=1) AS T1,IN_ENCABEZADO "
        SQL = SQL & " WHERE NU_AUTO_ENCA_ENID=IN_ENCABEZADO.NU_COMP_ENCA "
        SQL = SQL & " AND IN_ENCABEZADO.NU_AUTO_DOCU_ENCA=3 "
        SQL = SQL & " AND IN_ENCABEZADO.NU_COMP_ENCA = T1.NUMEROCOMP "
        Result = ExecSQL(SQL)
          
        'Actualizaci�n del campo NU_AUTO_ENCAREAL_ENID en la tabla IN_R_ENCA_IMDE cuando
        'existen entradas de mercancia para los dos comprobantes (Almacen y Farmacia) y NU_POSI_ENID=1
        
        SQL = "UPDATE IN_R_ENCA_IMDE SET NU_AUTO_ENCAREAL_ENID=T2.AUTO_ENCA FROM "
        SQL = SQL & " (SELECT MIN(NU_AUTO_ENCA) AS AUTO_ENCA, NU_COMP_ENCA AS COMPROBANTE FROM IN_ENCABEZADO, "
        SQL = SQL & " (SELECT NU_AUTO_ENCA_ENID AS NUMEROCOMP  FROM IN_R_ENCA_IMDE GROUP BY NU_AUTO_ENCA_ENID"
        SQL = SQL & " HAVING COUNT(DISTINCT NU_POSI_ENID)=2) AS T1 "
        SQL = SQL & " WHERE IN_ENCABEZADO.NU_AUTO_DOCU_ENCA=3 AND IN_ENCABEZADO.NU_COMP_ENCA = T1.NUMEROCOMP "
        SQL = SQL & " GROUP BY NU_COMP_ENCA) AS T2 "
        SQL = SQL & " WHERE NU_AUTO_ENCA_ENID=T2.COMPROBANTE AND NU_POSI_ENID=1"
        
        Result = ExecSQL(SQL)
        
        'Actualizaci�n del campo NU_AUTO_ENCAREAL_ENID en la tabla IN_R_ENCA_IMDE cuando
        'existen entradas de mercancia para los dos comprobantes (Almacen y Farmacia) y NU_POSI_ENID=2
        
        SQL = "UPDATE IN_R_ENCA_IMDE SET NU_AUTO_ENCAREAL_ENID=T2.AUTO_ENCA FROM "
        SQL = SQL & " (SELECT MAX(NU_AUTO_ENCA) AS AUTO_ENCA, NU_COMP_ENCA AS COMPROBANTE FROM IN_ENCABEZADO, "
        SQL = SQL & " (SELECT NU_AUTO_ENCA_ENID AS NUMEROCOMP  FROM IN_R_ENCA_IMDE GROUP BY NU_AUTO_ENCA_ENID"
        SQL = SQL & " HAVING COUNT(DISTINCT NU_POSI_ENID)=2) AS T1 "
        SQL = SQL & " WHERE IN_ENCABEZADO.NU_AUTO_DOCU_ENCA=3 AND IN_ENCABEZADO.NU_COMP_ENCA = T1.NUMEROCOMP "
        SQL = SQL & " GROUP BY NU_COMP_ENCA) AS T2 "
        SQL = SQL & " WHERE NU_AUTO_ENCA_ENID=T2.COMPROBANTE AND NU_POSI_ENID=2"
        
        Result = ExecSQL(SQL)
      Else
        'Actualizaci�n del campo NU_AUTO_ENCAREAL_ENID en la tabla IN_R_ENCA_IMDE cuando
        'existe una solo comprobante entrada de mercancia com impuestos o descuentos
        SQL = "UPDATE IN_R_ENCA_IMDE, IN_ENCABEZADO"
        SQL = SQL & " SET IN_R_ENCA_IMDE.NU_AUTO_ENCAREAL_ENID=NU_AUTO_ENCA "
        SQL = SQL & " WHERE IN_R_ENCA_IMDE.NU_AUTO_ENCA_ENID=NU_COMP_ENCA "
        SQL = SQL & " AND NU_POSI_ENID=1 AND NU_COMP_ENCA IN "
        SQL = SQL & " (SELECT NU_COMP_ENCA FROM IN_ENCABEZADO "
        SQL = SQL & " WHERE NU_AUTO_DOCU_ENCA=3 GROUP BY NU_COMP_ENCA "
        SQL = SQL & " HAVING COUNT(*)=1)"
        Result = ExecSQL(SQL)
        
        'Actualizaci�n del campo NU_AUTO_ENCAREAL_ENID en la tabla IN_R_ENCA_IMDE cuando
        'existen entradas de mercancia para los dos comprobantes (Almacen y Farmacia) y NU_POSI_ENID=1
        
        SQL = " UPDATE IN_R_ENCA_IMDE, IN_ENCABEZADO"
        SQL = SQL & " SET NU_AUTO_ENCAREAL_ENID=IN_ENCABEZADO.NU_AUTO_ENCA"
        SQL = SQL & " WHERE IN_R_ENCA_IMDE.NU_AUTO_ENCA_ENID=NU_COMP_ENCA AND NU_POSI_ENID=1"
        SQL = SQL & " AND NU_AUTO_ENCA IN"
        SQL = SQL & " (SELECT MIN(NU_AUTO_ENCA)  FROM IN_ENCABEZADO,"
        SQL = SQL & " (SELECT COMPROBANTE  FROM"
        SQL = SQL & " (SELECT DISTINCT NU_POSI_ENID AS POSI, NU_AUTO_ENCA_ENID AS COMPROBANTE FROM IN_R_ENCA_IMDE) AS T1"
        SQL = SQL & " WHERE POSI=1) AS T2"
        SQL = SQL & " WHERE IN_ENCABEZADO.NU_AUTO_DOCU_ENCA=3 AND IN_ENCABEZADO.NU_COMP_ENCA = T2.COMPROBANTE "
        SQL = SQL & " GROUP BY NU_COMP_ENCA)"

        Result = ExecSQL(SQL)
        
        'Actualizaci�n del campo NU_AUTO_ENCAREAL_ENID en la tabla IN_R_ENCA_IMDE cuando
        'existen entradas de mercancia para los dos comprobantes (Almacen y Farmacia) y NU_POSI_ENID=2
        
        SQL = " UPDATE IN_R_ENCA_IMDE, IN_ENCABEZADO"
        SQL = SQL & " SET NU_AUTO_ENCAREAL_ENID=IN_ENCABEZADO.NU_AUTO_ENCA"
        SQL = SQL & " WHERE IN_R_ENCA_IMDE.NU_AUTO_ENCA_ENID=NU_COMP_ENCA AND NU_POSI_ENID=2"
        SQL = SQL & " AND NU_AUTO_ENCA IN"
        SQL = SQL & " (SELECT MAX(NU_AUTO_ENCA)  FROM IN_ENCABEZADO,"
        SQL = SQL & " (SELECT COMPROBANTE  FROM"
        SQL = SQL & " (SELECT DISTINCT NU_POSI_ENID AS POSI, NU_AUTO_ENCA_ENID AS COMPROBANTE FROM IN_R_ENCA_IMDE) AS T1"
        SQL = SQL & " WHERE POSI=2) AS T2"
        SQL = SQL & " WHERE IN_ENCABEZADO.NU_AUTO_DOCU_ENCA=3 AND IN_ENCABEZADO.NU_COMP_ENCA = T2.COMPROBANTE "
        SQL = SQL & " GROUP BY NU_COMP_ENCA)"

        Result = ExecSQL(SQL)
    
      End If
   End If
   '----------------------------------------------
    If Result <> FAIL Then Result = DoUpdate("PARAMETROS_INVE", "NU_NVERDB_PINV='6.1.0'", NUL$)
End Sub

Sub Actualizar_6_2()
Dim StSql As String 'AASV M3279


'DAHV R1182 INICIO
    If Not ExisteTABLA("IN_EXISTENCIA_MINIMA") Then
            Result = CrearTabla("IN_EXISTENCIA_MINIMA", "TX_CODI_ARTI_EXMI, 10, 16, 0")
            Result = CrearCampo("IN_EXISTENCIA_MINIMA", "TX_NOMB_ARTI_EXMI", 10, 60, 0)
            Result = CrearCampo("IN_EXISTENCIA_MINIMA", "TX_CODI_BODE_EXMI", 10, 10, 0)
            Result = CrearCampo("IN_EXISTENCIA_MINIMA", "TX_NOMB_BODE_EXMI", 10, 50, 0)
            Result = CrearCampo("IN_EXISTENCIA_MINIMA", "NU_STMIN_EXMI", 4, 0, 0)
            Result = CrearCampo("IN_EXISTENCIA_MINIMA", "NU_STMAX_EXMI", 4, 0, 0)
            'Result = CrearCampo("IN_EXISTENCIA_MINIMA", "NU_EXISTE_EXMI", 4, 0, 0) 'HRR Nov2008
            Result = CrearCampo("IN_EXISTENCIA_MINIMA", "NU_EXISTE_EXMI", 13, 0, 0) 'HRR Nov2008
            Result = CrearCampo("IN_EXISTENCIA_MINIMA", "NU_AUTO_USUA_EXMI ", 4, 0, 0)
            Result = CrearCampo("IN_EXISTENCIA_MINIMA", "NU_PORC_EXMI ", 4, 0, 0)
            Result = CrearCampo("IN_EXISTENCIA_MINIMA", "NU_PEDIDO_EXMI ", 4, 0, 0)
            Result = CrearCampo("IN_EXISTENCIA_MINIMA", "NU_STREPO_EXMI ", 4, 0, 0)
            
    End If
    
    'fnAgregarOpcion "MINIMO DE EXISTENCIAS", "FrmExistenciaMinima", "04", "FRM"
    fnAgregarOpcion "GENERADOR DE PEDIDOS", "FrmExistenciaMinima", "04", "FRM" 'DAHV R1182 OE

'DAHV R1182 FIN



'JACC R1310-2034
    If ExisteTABLA("ORDENADOR") Then
        If Not ExisteCAMPO("ORDENADOR", "TX_ESTA_ORDE") Then
            Result = CrearCampo("ORDENADOR", "TX_ESTA_ORDE", 10, 1, 1)
            Valores = "TX_ESTA_ORDE = 0"
            If Result <> FAIL Then Result = DoUpdate("ORDENADOR", Valores, NUL$)
        End If
    End If
'JACC R1310-2034
'AASV R1883
    If ExisteTABLA("PARAMETROS_INVE") Then
       If Not ExisteCAMPO("PARAMETROS_INVE", "TX_IMPDOC_PINV") Then
          Result = CrearCampo("PARAMETROS_INVE", "TX_IMPDOC_PINV", 10, 1, 1)
          Valores = "TX_IMPDOC_PINV ='N'"
          If Result <> FAIL Then Result = DoUpdate("PARAMETROS_INVE", Valores, NUL$)
       End If
    End If
'AASV R1883
'AASV R Auditoria
ReDim Arr(0)
   Result = LoadData("FORMULARIO", "TX_DESC_FORM", "TX_FORM_FORM='FrmAuditoria' AND TX_TIPO_FORM='FRM'", Arr)
   If Result <> FAIL And Not Encontro Then
      'Result = DoInsertSQL("FORMULARIO", "TX_DESC_FORM='FORMULARIO DE AUDITORIA',TX_FORM_FORM='FrmAuditoria',TX_TIPO_FORM='FRM'")
      Result = DoInsertSQL("FORMULARIO", "TX_DESC_FORM='AUDITORIA',TX_FORM_FORM='FrmAuditoria',TX_TIPO_FORM='FRM'") 'AASV M2663
      If Result <> FAIL Then
            
         Result = LoadData("FORMULARIO", "NU_AUTO_FORM", "TX_FORM_FORM='FrmAuditoria'", Arr)
         If Result <> FAIL And Encontro Then
            Valores = "TX_CODPADR_OPCI='01',NU_AUTO_FORM_OPCI=" & Arr(0) & ","
            Result = LoadData("OPCION", "max(TX_CODI_OPCI)", "TX_CODI_OPCI LIKE '01" & CaractLike & "'", Arr)
            If Result <> FAIL And Encontro Then
               If Arr(0) = NUL$ Then Arr(0) = "0100"
               'Valores = Valores & "TX_CODI_OPCI='0" & CInt(Arr(0)) + 1 & "', TX_DESC_OPCI='FORMULARIO DE AUDITORIA',"
               Valores = Valores & "TX_CODI_OPCI='0" & CInt(Arr(0)) + 1 & "', TX_DESC_OPCI='AUDITORIA'," 'AASV M2663
               Result = DoInsertSQL("OPCION", Valores)
               If Result <> FAIL Then Call CrearOpcion("0" & CStr(CInt(Arr(0)) + 1))
            End If
         End If
            
      End If
   
   End If
'AASV R Auditoria

 'AASV RLICENCIA
          If Not ExisteCAMPO("PARAMETROS_INVE", "NU_NUMELIC_PINV") Then
              Result = CrearCampo("PARAMETROS_INVE", "NU_NUMELIC_PINV", 7, 0, 1)
              If Result <> FAIL Then Result = DoUpdate("PARAMETROS_INVE", "NU_NUMELIC_PINV = '0'", NUL$)
          End If
'AASV RLICENCIA
'AASV M3279

    Result = fnEliminarVista("VW_PLANOCIR01")
    If Not ExisteCAMPO("ARTICULO", "NU_CODCUM_ARTI") Then
        Result = CrearCampo("ARTICULO", "NU_CODCUM_ARTI", 10, 18, 1)
    End If
         
    If Result <> FAIL Then
        StSql = "SELECT FE_FECH_COMP,CD_CODI_ARTI"
        StSql = StSql & ",MIN(NU_COSTO_DETA) AS MINIMO, MAX(NU_COSTO_DETA) AS MAXIMO"
        StSql = StSql & ", SUM(NU_ENTRAD_DETA * NU_COSTO_DETA) AS COMP_NETA"
        StSql = StSql & ",SUM(((NU_ENTRAD_DETA - NU_SALIDA_DETA)*NU_MULT_DETA)/NU_DIVI_DETA) AS UNID_COMPR"
        StSql = StSql & ",TX_FAPR_COMP,CD_RIPS_ARTI,NU_CODCUM_ARTI"
        StSql = StSql & " FROM IN_COMPRA, IN_R_COMP_ENCA, IN_ENCABEZADO, IN_DETALLE, ARTICULO"
        StSql = StSql & " WHERE NU_AUTO_COMP_COEN = NU_AUTO_COMP"
        StSql = StSql & " AND NU_AUTO_ENCA_COEN = NU_AUTO_ENCA"
        StSql = StSql & " AND NU_AUTO_ENCA=NU_AUTO_ORGCABE_DETA"
        StSql = StSql & " AND NU_AUTO_ENCA=NU_AUTO_MODCABE_DETA"
        StSql = StSql & " AND NU_AUTO_ARTI=NU_AUTO_ARTI_DETA"
        StSql = StSql & " AND NU_AUTO_DOCU_ENCA=3"
        StSql = StSql & " AND TX_ESTA_COMP=1"
        StSql = StSql & " GROUP BY CD_CODI_ARTI,FE_FECH_COMP,TX_FAPR_COMP,CD_RIPS_ARTI,NU_CODCUM_ARTI"
        
        If Not fnExisteVista("VW_PLANOCIR01") Then Result = fnCrearVista("VW_PLANOCIR01", StSql, "Registro Detalle para la Circular 001-002 CNM")
    End If
'AASV M3279




If Result <> FAIL Then Result = DoUpdate("PARAMETROS_INVE", "NU_NVERDB_PINV='6.2.0'", NUL$)

End Sub
Sub Actualizar_6_3()
    Dim Base As DAO.Database 'AASV R2171
    
    '-------------------------------------------
    'GMS R2074
    If Not ExisteTABLA("IN_AJUSTECOSTOPROM") Then
        Result = CrearTabla("IN_AJUSTECOSTOPROM", "FE_INICIA_ACPR,8,0,0")
        Result = CrearCampo("IN_AJUSTECOSTOPROM", "FE_FINAL_ACPR", 8, 0, 0)
        Result = CrearCampo("IN_AJUSTECOSTOPROM", "FE_RUNPRO_ACPR", 8, 0, 0)
        Result = CrearCampo("IN_AJUSTECOSTOPROM", "NU_CONE_ACPR", 4, 0, 0)
        Result = CrearCampo("IN_AJUSTECOSTOPROM", "TX_OBSER_ACPR", 10, 150, 1)
        'Result = CrearCampo("IN_AJUSTECOSTOPROM", "TX_BODE_ACPR", 10, 4000, 1) 'GMS Rev. Tecnica M4031
        'Result = CrearCampo("IN_AJUSTECOSTOPROM", "TX_BODE_ACPR", 10, 7999, 1) 'GMS Rev. Tecnica M4031
        '-----------------------------------------------------------------------
        'GMS M4265
        If MotorBD = "SQL" Then
            Result = ExecSQLCommand("ALTER TABLE IN_AJUSTECOSTOPROM ADD TX_BODE_ACPR VARCHAR(7999) NULL")
        Else
            Result = ExecSQLCommand("ALTER TABLE IN_AJUSTECOSTOPROM ADD COLUMN TX_BODE_ACPR MEMO NULL")
        End If
        '-----------------------------------------------------------------------
        Result = CrearCampo("IN_AJUSTECOSTOPROM", "NU_AUTO_ACPR", 4, 0, 0, True)
        Result = CrearCampo("IN_AJUSTECOSTOPROM", "NU_SELCONDI_ACPR", 4, 0, 0) 'GMS 13-11-2008 Obs. Entrega. Valores: 0 - Menores, 1 - Mayores, 2 - Todos
        '-----------------------------------------------------------------------
        'GMS Rev. Tenica M4031
        If MotorBD = "SQL" Then
            'Result = ExecSQLCommand("ALTER TABLE IN_AJUSTECOSTOPROM ADD TX_BODE_ACPR VARCHAR(8000) NOT NULL")
            'Result = ExecSQLCommand("ALTER TABLE IN_AJUSTECOSTOPROM ALTER COLUMN TX_BODE_ACPR VARCHAR(4000) NOT NULL")
            Result = ExecSQLCommand("ALTER TABLE IN_AJUSTECOSTOPROM ALTER COLUMN TX_BODE_ACPR VARCHAR(7999) NOT NULL")
        ElseIf MotorBD = "ACCESS" Then
            'Result = ExecSQLCommand("ALTER TABLE IN_AJUSTECOSTOPROM ADD TX_BODE_ACPR TEXT(8000) NOT NULL")
            'Result = ExecSQLCommand("ALTER TABLE IN_AJUSTECOSTOPROM ALTER COLUMN TX_BODE_ACPR TEXT(4000) NOT NULL")
            'Result = ExecSQLCommand("ALTER TABLE IN_AJUSTECOSTOPROM ALTER COLUMN TX_BODE_ACPR TEXT(7999) NOT NULL")
            Result = ExecSQLCommand("ALTER TABLE IN_AJUSTECOSTOPROM ALTER COLUMN TX_BODE_ACPR MEMO NOT NULL") 'GMS M4265
        End If
        'Result = CrearIndice("IN_AJUSTECOSTOPROM", "PK_IN_AJUSTECOSTOPROM", "NU_AUTO_ACPR", True)
        If Result <> FAIL Then Result = CrearIndice("IN_AJUSTECOSTOPROM", "PK_IN_AJUSTECOSTOPROM", "NU_AUTO_ACPR", True)
        'Result = CrearRelacion("IN_AJUSTECOSTOPROM_CONEXION_INV", "NU_CONE_ACPR", "NU_AUTO_CINV", "IN_AJUSTECOSTOPROM", "CONEXION_INV")
        If Result <> FAIL Then Result = CrearRelacion("IN_AJUSTECOSTOPROM_CONEXION", "NU_CONE_ACPR", "NU_AUTO_CONE", "IN_AJUSTECOSTOPROM", "CONEXION")
        '-----------------------------------------------------------------------
    End If
    If Not ExisteTABLA("IN_DETA_AJUSTECOSTO") Then
        Result = CrearTabla("IN_DETA_AJUSTECOSTO", "NU_AUTO_ACPR_DACP,4,0,0")
        Result = CrearCampo("IN_DETA_AJUSTECOSTO", "NU_AUTO_DACP", 4, 0, 0, True)
        Result = CrearCampo("IN_DETA_AJUSTECOSTO", "NU_AUTO_ARTI_DACP", 4, 0, 0)
        Result = CrearCampo("IN_DETA_AJUSTECOSTO", "NU_COSTPR_DACP", 7, 0, 0)
        Result = CrearCampo("IN_DETA_AJUSTECOSTO", "NU_PORCEN_DACP", 4, 0, 1)
        '-----------------------------------------------------------------------
        'GMS M4031. Se hace comentario sobre estas lineas, para eliminar la creaci�n de este campo.
        'Se realizar� la creaci�n de una nueva tabla para el detalle de los documentos modificados.
        '-----------------------------------------------------------------------
        'Result = CrearCampo("IN_DETA_AJUSTECOSTO", "TX_DCMNTS_DACP", 10, 4000, 1) 'GMS Rev. Tecnica M4031
        '-----------------------------------------------------------------------
        'GMS Rev. Tenica M4031
        'If MotorBD = "SQL" Then
        '    'Result = ExecSQLCommand("ALTER TABLE IN_DETA_AJUSTECOSTO ADD TX_DCMNTS_DACP VARCHAR(8000) NOT NULL")
        '    Result = ExecSQLCommand("ALTER TABLE IN_DETA_AJUSTECOSTO ALTER COLUMN TX_DCMNTS_DACP VARCHAR(4000) NOT NULL")
        'ElseIf MotorBD = "ACCESS" Then
        '    'Result = ExecSQLCommand("ALTER TABLE IN_DETA_AJUSTECOSTO ADD TX_DCMNTS_DACP TEXT(8000) NOT NULL")
        '    Result = ExecSQLCommand("ALTER TABLE IN_DETA_AJUSTECOSTO ALTER COLUMN TX_DCMNTS_DACP TEXT(4000) NOT NULL")
        'End If
        '-----------------------------------------------------------------------
        
        'Result = CrearIndice("IN_DETA_AJUSTECOSTO", "PK_IN_DETA_AJUSTECOSTO", "NU_AUTO_DACP", True)
        If Result <> FAIL Then Result = CrearIndice("IN_DETA_AJUSTECOSTO", "PK_IN_DETA_AJUSTECOSTO", "NU_AUTO_DACP", True)
        'Result = CrearRelacion("IN_DETA_AJUSTECOSTO_IN_AJUSTECOSTOPROM", "NU_AUTO_ACPR_DACP", "NU_AUTO_ACPR", "IN_DETA_AJUSTECOSTO", "IN_AJUSTECOSTOPROM")
        'If Result <> FAIL Then Result = CrearRelacion("IN_DETA_AJUSTECOSTO_IN_AJUSTECOSTOPROM", "NU_AUTO_ACPR_DACP", "NU_AUTO_ACPR", "IN_DETA_AJUSTECOSTO", "IN_AJUSTECOSTOPROM")
        If Result <> FAIL Then Result = CrearRelacion("IN_DETA_AJUSTECOSTO_IN_AJUSTE", "NU_AUTO_ACPR_DACP", "NU_AUTO_ACPR", "IN_DETA_AJUSTECOSTO", "IN_AJUSTECOSTOPROM") 'GMS R4031
        '-----------------------------------------------------------------------
    End If
    'GMS M4031
    If Not ExisteTABLA("IN_DETA_DOCUCOSTO") Then
        Result = CrearTabla("IN_DETA_DOCUCOSTO", "NU_AUTO_DACP_DCPR,4,0,0")
        Result = CrearCampo("IN_DETA_DOCUCOSTO", "NU_AUTO_ENCA_DCPR", 4, 0, 0)
        Result = CrearCampo("IN_DETA_DOCUCOSTO", "NU_AUTO_DCPR", 4, 0, 0, True)
        If Result <> FAIL Then Result = CrearIndice("IN_DETA_DOCUCOSTO", "PK_IN_DETA_DOCUCOSTO", "NU_AUTO_DCPR", True)
        'If Result <> FAIL Then Result = CrearRelacion("IN_DETA_DOCUCOSTO_IN_DETA_AJUSTECOSTO", "NU_AUTO_DACP_DCPR", "NU_AUTO_DACP", "IN_DETA_DOCUCOSTO", "IN_DETA_AJUSTECOSTO")
        If Result <> FAIL Then Result = CrearRelacion("IN_DETA_DOCUCOSTO_IN_DETA_AJU", "NU_AUTO_DACP_DCPR", "NU_AUTO_DACP", "IN_DETA_DOCUCOSTO", "IN_DETA_AJUSTECOSTO") 'GMS M4031
    End If
        
    'Call fnAgregarOpcion("AJUSTAR COSTO PROMEDIO", "FrmAjusteCostoProm", "05", "FRM")
    Call fnAgregarOpcion("AJUSTAR COSTO HISTORICO", "FrmAjusteCostoProm", "05", "FRM") 'GMS 17-11-2008 Obs. Entrega
    '-------------------------------------------
    'AASV R2171
    If ExisteTABLA("MOVIMIENTOS") Then
        If Not ExisteCAMPO("MOVIMIENTOS", "TX_USER_MOVI") Then
            Result = CrearCampo("MOVIMIENTOS", "TX_USER_MOVI", 10, 50, 1)
        End If
        If (Dir("C:\CNTSISTE.CNT\TEMPORAL\TEMPORAC.MDB") <> "") Then 'AASV M4206
            Set Base = OpenDatabase("C:\CNTSISTE.CNT\TEMPORAL\TEMPORAC.MDB")
            If Not ExisteCampoTemporAccess(Base, "MOVIMIENTOS", "TX_USER_MOVI") Then
                   Base.Execute ("ALTER TABLE MOVIMIENTOS ADD TX_USER_MOVI TEXT(50)NULL")
            End If
            Base.Close
        End If 'AASV M4206
    End If
    '-------------------------------------------
    
    'LJSA R2164---
    If Not ExisteTABLA("PARAMETROS") Then
        Result = CrearTabla("PARAMETROS", "TX_CODPAR_PRMT, 10, 10, 0")
        If Result <> FAIL Then Result = CrearCampo("PARAMETROS", "TX_DESPAR_CPPTO", 10, 50, 0, False)
        If Result <> FAIL Then Result = CrearCampo("PARAMETROS", "TX_VALPAR_CPPTO", 10, 15, 0, False)
        If Result <> FAIL Then Result = CrearIndice("PARAMETROS", "PK_PARAMETROS", "TX_CODPAR_PRMT", True)
        If Result <> FAIL Then Result = DoInsertSQL("PARAMETROS", "TX_CODPAR_PRMT='CIERRANUAL',TX_DESPAR_CPPTO='Comprobante cierre anual', TX_VALPAR_CPPTO='NULL'")   'Fecha cierre anual valor
        If Result <> FAIL Then Result = DoInsertSQL("PARAMETROS", "TX_CODPAR_PRMT='SALDPPTO',TX_DESPAR_CPPTO='Comprobante Traslado de saldos', TX_VALPAR_CPPTO='0'")   'Fecha cierre anual valor
    End If
    
    'Marcaci�n traslado de movimientos en CXC. 1 : SI ; 0 : NO
    If ExisteTABLA("PARAMETROS_CXC") Then
        If Not ExisteCAMPO("PARAMETROS_CXC", "NU_TRMV_APLI") Then
            Result = CrearCampo("PARAMETROS_CXC", "NU_TRMV_APLI", "2", "1", 1, False)
        End If
    End If
    
    'Marcaci�n traslado de movimientos en CXP. 1 : SI ; 0 : NO
    If ExisteTABLA("PARAMETROS_CXP") Then
        If Not ExisteCAMPO("PARAMETROS_CXP", "NU_TRMV_APLI") Then
            Result = CrearCampo("PARAMETROS_CXP", "NU_TRMV_APLI", "2", "1", 1, False)
        End If
    End If
    'LJSA R2164---
    ''JACC M4204
    If ExisteTABLA("ENTIDAD") Then
        If Not ExisteCAMPO("ENTIDAD", "TX_INDICATIVO_ENTI") Then Result = CrearCampo("ENTIDAD", "TX_INDICATIVO_ENTI", 10, 3, 1)
    End If
    'JACC M4204
    
    If Result <> FAIL Then Result = DoUpdate("PARAMETROS_INVE", "NU_NVERDB_PINV='6.3.0'", NUL$)
End Sub
Sub Actualizar_6_4()

   'AASV R2241 INICIO
   If ExisteTABLA("IN_COMPRA") Then
      If Not ExisteCAMPO("IN_COMPRA", "NU_CONE_COMP") Then Result = CrearCampo("IN_COMPRA", "NU_CONE_COMP", "4", 0, 1)
   End If
   'AASV M4858 Inicio
   If ExisteTABLA("IN_ENCABEZADO") Then
      If Not ExisteCAMPO("IN_ENCABEZADO", "TX_NOUSAN_ENCA") Then Result = CrearCampo("IN_ENCABEZADO", "TX_NOUSAN_ENCA", "10", 50, 1)
      If Not ExisteCAMPO("IN_ENCABEZADO", "FE_FEANUL_ENCA") Then Result = CrearCampo("IN_ENCABEZADO", "FE_FEANUL_ENCA", "8", 0, 1)
   End If
   'AASV M4858 Fin
   'AASV R2241 FIN
   'AASV OPTIMIZACION 05/02/2009 INICIO
   If ExisteTABLA("IN_KARDEXLOTE") Then
      If ExisteCAMPO("IN_KARDEXLOTE", "NU_AUTO_KLOT") Then
         If BuscarIndice("IN_KARDEXLOTE", "NU_AUTO_KLOT") = NUL$ Then
            Result = CrearIndice("IN_KARDEXLOTE", "PK_IN_KARDEXLOTE", "NU_AUTO_KLOT", True)
         End If
      End If
   End If
   'AASV OPTIMIZACION 05/02/2009 FIN

   'If Result <> FAIL Then Result = DoUpdate("PARAMETROS_INVE", "NU_NVERDB_PINV='6.4.0'", NUL$)
   Result = DoUpdate("PARAMETROS_INVE", "NU_NVERDB_PINV='6.4.0'", NUL$) 'AASV R2241 Depuracion


End Sub

Sub Actualizar_6_5()

   'fnAgregarOpcion "INFORME DIARIO DE FACTURACION", "FrmInfDiarioFact", "04", "FRM" 'JACC R2250
   ''JACC M5304
   ReDim ArrFor(0)
   Result = LoadData("OPCION", "NU_AUTO_FORM_OPCI", "NU_AUTO_FORM_OPCI = 63", ArrFor)
   If Result <> FAIL And Encontro Then fnAgregarOpcion "INFORME DIARIO DE FACTURACION", "FrmInfDiarioFact", "04", "FRM"
   'JACC M5304
   Result = DoUpdate("PARAMETROS_INVE", "NU_NVERDB_PINV='6.5.0'", NUL$) 'AASV R2241 Depuracion
End Sub

Sub Actualizar_6_6()
   If ExisteTABLA("IN_EXISTENCIA_MINIMA") Then 'inicio CARV M4160
       'If Not ExisteCAMPO("IN_EXISTENCIA_MINIMA", "TX_NOMB_ARTI_EXMI") Then
       If ExisteCAMPO("IN_EXISTENCIA_MINIMA", "TX_NOMB_ARTI_EXMI") Then
            'If Result <> FAIL Then Result = ExecSQLCommand("ALTER TABLE IN_EXISTENCIA_MINIMA ALTER COLUMN TX_NOMB_ARTI_EXMI  varchar(120)")
            Result = ExecSQLCommand("ALTER TABLE IN_EXISTENCIA_MINIMA ALTER COLUMN TX_NOMB_ARTI_EXMI  varchar(120)")
       End If
   End If 'fin CARV M4160
   'inicio CARV R2320
   If ExisteTABLA("ARTICULO") Then
      'If Not ExisteCAMPO("ARTICULO", "NU_MEDCTRL_ARTI ") Then Result = CrearCampo("ARTICULO", "NU_MEDCTRL_ARTI ", "1", 1, 1)
      If Not ExisteCAMPO("ARTICULO", "NU_MEDCTRL_ARTI") Then Result = CrearCampo("ARTICULO", "NU_MEDCTRL_ARTI", "1", 1, 1) 'CARV M5775
      If ExisteCAMPO("ARTICULO", "NU_MEDCTRL_ARTI") Then Result = DoUpdate("ARTICULO", "NU_MEDCTRL_ARTI= 0", NUL$)
   End If
   'fin CARV R2320
   ''JACC R2345
   If ExisteTABLA("TC_IMPUESTOS") Then
      If Not ExisteCAMPO("TC_IMPUESTOS", "TX_CODI_DESC_IMPU") Then
         Result = CrearCampo("TC_IMPUESTOS", "TX_CODI_DESC_IMPU", "10", 3, 1, False)
      End If
   End If
   'JACC R2345
   
   ''JACC R2307 Almacenar la descripci�n de la sesion
   If Not ExisteTABLA("SESION_APLICACION") Then
      If Result <> FAIL Then Result = CrearTabla("SESION_APLICACION", "TX_NBD_SAPL, 10, 50, 0")
      If Result <> FAIL Then Result = CrearCampo("SESION_APLICACION", "TX_NPC_SAPL", "10", 50, 0, False)
      If Result <> FAIL Then Result = CrearCampo("SESION_APLICACION", "NU_APLI_SAPL", "2", 3, 0, False)
      If Result <> FAIL Then Result = CrearCampo("SESION_APLICACION", "TX_PROC_SAPL", "10", 20, 0, False)
      If Result <> FAIL Then Result = CrearCampo("SESION_APLICACION", "NU_ESTA_SAPL", "2", 3, 0, False)
      If Result <> FAIL Then Result = CrearCampo("SESION_APLICACION", "NU_AUTO_SAPL", "4", 0, 0, True)
   End If
   'JACC R2307
   ''JACC R2221
    If Result <> FAIL Then ExecSQLCommand ("ALTER TABLE IN_COMPRA ALTER COLUMN TX_FAPR_COMP   " & NombreCampo(10) & "(" & 14 & ") ")
    ' DAHV R2221 - INICIO
    ' SE COLOCA EN COMENTARIO PARA MANTENER INDEPENDENCIA DE LOS MODULOS
    ' If Result <> FAIL Then Result = ExecSQLCommand("ALTER TABLE COMPRA_ACTIVO ALTER COLUMN TX_FAPR_COAC   " & NombreCampo(10) & "(" & 14 & ") ")
    ' If Result <> FAIL Then Result = ExecSQLCommand("ALTER TABLE C_X_P ALTER COLUMN NU_FAPR_CXP  " & NombreCampo(10) & "(" & 14 & ") ")
    ' DAHV R2221 - FIN
    If Result <> FAIL Then Result = ExecSQLCommand("ALTER TABLE IN_PARTIPARTI ALTER COLUMN NU_VALO_PRPR " & NombreCampo(10) & "(" & 20 & ") ")
    'JACC R2221
   
   Result = DoUpdate("PARAMETROS_INVE", "NU_NVERDB_PINV='6.6.0'", NUL$) '
End Sub

Sub Actualizar_6_7()
      fnAgregarOpcion "REQUISICIONES PENDIENTES", "FrmRDPendientes", "04", "FRM" 'JACC R2396
      'DAHV R2395 - INICIO
      fnAgregarOpcion "DIFERENCIA AJUSTE AL PESO", "FrmAjustePeso", "04", "FRM" 'JACC R2396
      'DAHV R2395 - FIN
      Result = DoUpdate("PARAMETROS_INVE", "NU_NVERDB_PINV='6.7.0'", NUL$)
End Sub

'---------------------------------------------------------------------------------------
' Procedure : Actualizar_6_8
' DateTime  : 04/05/2010 11:20
' Author    : gonzalo_panqueva
' Purpose   : actualizacion de version de la BD
'---------------------------------------------------------------------------------------
'
Sub Actualizar_6_8()
      'GAPM R2367 INICIO
      'CREO EL NUEVO CAMPO PARA LA TABLA ARTICULOS
        If ExisteTABLA("ARTICULO") Then
            If Not ExisteCAMPO("ARTICULO", "NU_MINSNP_ART ") Then Result = CrearCampo("ARTICULO", "NU_MINSNP_ART", "1", 0, 1)
            If ExisteCAMPO("ARTICULO", "NU_MINSNP_ART ") Then Result = DoUpdate("ARTICULO", "NU_MINSNP_ART= 0", NUL$)
        End If
      'GAPM R2367 FIN
      'GAPM R2427 INICIO
      'SE ACTUALIZA LA VERSION
      Result = DoUpdate("PARAMETROS_INVE", "NU_NVERDB_PINV='6.8.0'", NUL$)
      'GAPM R2427 FIN
End Sub
Sub Actualizar_6_9()
      Result = DoUpdate("PARAMETROS_INVE", "NU_NVERDB_PINV='6.9.0'", NUL$)
End Sub
'---------------------------------------------------------------------------------------
' Procedure : Actualizar_7_0
' DateTime  : 23/02/2011 16:00
' Author    : gabriel_vallejo
' Purpose   : ACTUALIZACION DE BASE DE DATOS
'---------------------------------------------------------------------------------------
' GAVL R3201 / T5101 25/02/2011
Sub Actualizar_7_0()
Dim ArrINTRFC() As Variant


       ReDim ArrINTRFC(0)
'SE AGREGAN OPCIONES AL MENU FALTA EL FORMULARIO
       fnAgregarOpcion "PRINCIPIO ACTIVO", "FrmConfGeneral", "02", "FRM"
       fnAgregarOpcionConti "CLASIFICACI�N DE RIESGO", "FrmConfGeneral", "02", "FRM"
       fnAgregarOpcionConti "PRESENTACI�N COMERCIAL", "FrmConfGeneral", "02", "FRM"
       fnAgregarOpcion "EXISTENCIA POR TIPO", "FrmExistxDisp", "04", "FRM"
       
       
       '***************
       ReDim Arr(0)
       Result = LoadData("FORMULARIO", "NU_AUTO_FORM", "TX_FORM_FORM='FrmConfGeneral'", Arr)
       If Result <> FAIL And Encontro Then
          Valores = "TX_CODPADR_OPCI='02',NU_AUTO_FORM_OPCI=" & Arr(0) & ","
          ReDim Arr(0)
          Result = LoadData("OPCION", "max(TX_CODI_OPCI)", "TX_CODI_OPCI LIKE '02%'", Arr)
          If Result <> FAIL Then
             If Arr(0) = NUL$ Then Arr(0) = "0200"
             Valores = Valores & "TX_CODI_OPCI='0" & CInt(Arr(0)) + 1 & "', TX_DESC_OPCI='PRINCIPIO ACTIVO',"
             Result = DoInsertSQL("OPCION", Valores)
             If Result <> FAIL Then Call CrearOpcion("0" & CStr(CInt(Arr(0)) + 1))
          End If
       End If
    '***************************
  If Not ExisteTABLA("PRINCIPIO_ACTIVO") Then
       Result = CrearTabla("PRINCIPIO_ACTIVO", "TX_CODIGO_PRAC,10,3,1")
       If Result <> FAIL Then Result = CrearCampo("PRINCIPIO_ACTIVO", "TX_DESC_PRAC", "10", 255, 1)
       If Result <> FAIL Then Result = CrearIndice("PRINCIPIO_ACTIVO", "PK_PRINCIPIO_ACTIVO", "TX_CODIGO_PRAC", True)
  End If
  
  If Result <> FAIL Then
     If Not ExisteTABLA("CLASIFICACION_RIESGO") Then
       Result = CrearTabla("CLASIFICACION_RIESGO", "TX_CODIGO_CLRI,10,3,1")
       If Result <> FAIL Then Result = CrearCampo("CLASIFICACION_RIESGO", "TX_DESC_CLRI", "10", 255, 1)
       If Result <> FAIL Then Result = CrearIndice("CLASIFICACION_RIESGO", "PK_CLASIFICACION_RIESGO", "TX_CODIGO_CLRI", True)
     End If
  End If
  
  If Result <> FAIL Then
   If Not ExisteTABLA("PRESENTACION_COMERCIAL") Then
       Result = CrearTabla("PRESENTACION_COMERCIAL", "TX_CODI_PRCO,10,3,1")
       If Result <> FAIL Then Result = CrearCampo("PRESENTACION_COMERCIAL", "TX_DESC_PRCO", "10", 255, 1)
       If Result <> FAIL Then Result = CrearIndice("PRESENTACION_COMERCIAL", "PK_PRESENTACION_COMERCIAL", "TX_CODI_PRCO", True)
     End If
  End If
  
  If Result <> FAIL Then
    If Not ExisteCAMPO("ARTICULO", "NU_DISP_ARTI") Then
        Result = CrearCampo("ARTICULO", "NU_DISP_ARTI", "1", 0, 1)
        If Result <> FAIL Then Result = DoUpdate("ARTICULO", "NU_DISP_ARTI= 0", NUL$)
        If Result <> FAIL Then Result = CrearCampo("ARTICULO", "NU_VIUT_ARTI", "3", 0, 1)
        If Result <> FAIL Then Result = CrearCampo("ARTICULO", "TX_CODCLAS_ARTI", "10", 3, 1)
        If Result <> FAIL Then Result = CrearCampo("ARTICULO", "TX_CODPRI_ARTI", "10", 3, 1)
        If Result <> FAIL Then Result = CrearCampo("ARTICULO", "TX_PRCO_ARTI", "10", 3, 1)
     End If
  End If
      
      'SE ACTUALIZA LA VERSION
      If Result <> FAIL Then
        Result = DoUpdate("PARAMETROS_INVE", "NU_NVERDB_PINV='7.0.0'", NUL$)
      End If
End Sub
'---------------------------------------------------------------------------------------
' Procedure : Actualizar_7_1
' DateTime  : 25/04/2011 16:00
' Author    : Luis David Colmenares
' Purpose   : ACTUALIZACION DE BASE DE DATOS
'---------------------------------------------------------------------------------------
'INICIO LDCR T5865/R5153
Sub Actualizar_7_1()

Dim StSql As String
If Not fnExisteVista("VW_PLANOCIR02") Then

  'INICIO LDCR T5987
'        StSql = "SELECT FE_FECH_DETA as FECHA ,CD_CODI_ARTI"  'LDCR T5987 SE COLOCA EN COMENTARIOS
'        StSql = StSql & ",MIN(NU_COSTO_DETA) AS MINIMO, MAX(NU_COSTO_DETA) AS MAXIMO"
'        StSql = StSql & ", SUM(NU_SALIDA_DETA * NU_COSTO_DETA) AS COMP_NETA"
'        StSql = StSql & ",SUM(((NU_SALIDA_DETA - NU_ENTRAD_DETA )*NU_MULT_DETA)/NU_DIVI_DETA) AS UNID_COMPR"
'        StSql = StSql & ",NU_COMP_ENCA as CODIGO,CD_RIPS_ARTI,NU_CODCUM_ARTI"
'        StSql = StSql & " FROM IN_COMPRA, IN_R_COMP_ENCA, IN_ENCABEZADO, IN_DETALLE, ARTICULO"
'        StSql = StSql & " WHERE "
'        StSql = StSql & " NU_AUTO_ENCA=NU_AUTO_ORGCABE_DETA"
'        StSql = StSql & " AND NU_AUTO_ENCA=NU_AUTO_MODCABE_DETA"
'        StSql = StSql & " AND NU_AUTO_ARTI=NU_AUTO_ARTI_DETA"
'        StSql = StSql & " AND NU_AUTO_DOCU_ENCA=11"
'        StSql = StSql & " AND TX_ESTA_ENCA='N'"
'        StSql = StSql & " AND CD_CODI_ARTI  not in (select CD_CODI_ARTI  from ARTICULO "
'        StSql = StSql & " WHERE  ID_TIPO_ARTI = 2 or ID_TIPO_ARTI = 3 or ID_TIPO_ARTI = 4)"
'        StSql = StSql & " GROUP BY CD_CODI_ARTI,FE_FECH_DETA,NU_COMP_ENCA,CD_RIPS_ARTI,NU_CODCUM_ARTI"
'        'INICIO  LDCR T5871
'        StSql = StSql & " Union"
'        StSql = StSql & " SELECT FE_FECH_MOVI as FECHA,CD_CODI_ARTI,MIN(VL_UNID_FORM) AS MINIMO, MAX(VL_UNID_FORM) AS MAXIMO,"
'        StSql = StSql & " sum(CT_CANT_FORM *VL_UNID_FORM)AS COMP_NETA,"
'        StSql = StSql & " CT_CANT_FORM As UNID_COMPR, NU_NUME_FACO As Codigo,"
'        StSql = StSql & " CD_RIPS_ARTI,NU_CODCUM_ARTI"
'        StSql = StSql & " From FACTURAS_CONTADO, MOVI_CARGOS, Formulas, Articulo"
'        StSql = StSql & " WHERE "
'        StSql = StSql & " NU_NUME_FACO = NU_NUME_FACO_MOVI"
'        StSql = StSql & " AND NU_TIPO_MOVI = 1"
'        StSql = StSql & " AND NU_ESTA_FACO = 1"
'        StSql = StSql & " AND NU_NUME_MOVI =  NU_NUME_MOVI_FORM"
'        StSql = StSql & " AND CD_CODI_ARTI=CD_CODI_ELE_FORM"
'        StSql = StSql & " AND CD_CODI_ARTI  not in (select CD_CODI_ARTI  from ARTICULO "
'        StSql = StSql & " WHERE  ID_TIPO_ARTI = 2 or ID_TIPO_ARTI = 3 or ID_TIPO_ARTI = 4)"
'        StSql = StSql & " GROUP BY CD_CODI_ARTI,FE_FECH_MOVI,CD_RIPS_ARTI,NU_CODCUM_ARTI,NU_NUME_FACO,CT_CANT_FORM"
'        'FIN LDCR T5871

  StSql = "SELECT"
  StSql = StSql & " FE_CREA_ENCA as FECHA ,CD_CODI_ARTI,MIN(NU_VALOR_DETA) AS MINIMO, MAX(NU_VALOR_DETA) AS MAXIMO,"
  StSql = StSql & " SUM(NU_SALIDA_DETA * NU_VALOR_DETA) AS COMP_NETA,"
  StSql = StSql & " SUM(((NU_SALIDA_DETA - NU_ENTRAD_DETA )*NU_MULT_DETA)/NU_DIVI_DETA) AS UNID_COMPR,"
  StSql = StSql & " NU_COMP_ENCA As Codigo, CD_RIPS_ARTI, NU_CODCUM_ARTI"
  StSql = StSql & " From IN_ENCABEZADO, IN_DETALLE, Articulo"
  StSql = StSql & " WHERE NU_AUTO_DOCU_ENCA=11 AND"
  StSql = StSql & " TX_ESTA_ENCA='N'  AND"
  StSql = StSql & " NU_AUTO_ENCA=NU_AUTO_ORGCABE_DETA AND"
  StSql = StSql & " NU_AUTO_ENCA=NU_AUTO_MODCABE_DETA AND"
  StSql = StSql & " NU_AUTO_ARTI=NU_AUTO_ARTI_DETA AND"
  StSql = StSql & " ID_TIPO_ARTI IN (0,1)"
  StSql = StSql & " GROUP BY CD_CODI_ARTI,FE_CREA_ENCA,NU_COMP_ENCA,CD_RIPS_ARTI,NU_CODCUM_ARTI"
  StSql = StSql & " Union"
  StSql = StSql & " SELECT FE_FECH_FACO as FECHA,CD_CODI_ARTI,MIN(VL_UNID_FORM) AS MINIMO, MAX(VL_UNID_FORM) AS MAXIMO,"
  StSql = StSql & " SUM(CT_CANT_FORM *VL_UNID_FORM)AS COMP_NETA, CT_CANT_FORM As UNID_COMPR, NU_NUME_FACO As CODIGO,"
  StSql = StSql & " CD_RIPS_ARTI , NU_CODCUM_ARTI"
  StSql = StSql & " From FACTURAS_CONTADO, MOVI_CARGOS, Formulas, Articulo"
  StSql = StSql & " WHERE NU_NUME_FACO = NU_NUME_FACO_MOVI AND"
  StSql = StSql & " NU_ESTA_FACO = 1 AND"
  StSql = StSql & " NU_NUME_MOVI =  NU_NUME_MOVI_FORM AND"
  StSql = StSql & " CD_CODI_ARTI=CD_CODI_ELE_FORM  AND"
  StSql = StSql & " ID_TIPO_ARTI IN (0,1) AND"
  StSql = StSql & " NU_NUME_FACO <> 0"
  StSql = StSql & " GROUP BY CD_CODI_ARTI,FE_FECH_FACO,CD_RIPS_ARTI,NU_CODCUM_ARTI,NU_NUME_FACO,CT_CANT_FORM"
  'FIN LDCR T5987
        Result = fnCrearVista("VW_PLANOCIR02", StSql, "Genera archivo plano de  venta de medicamentos seg�n la estructura de la circular 001 de 2007 y 001 de 2010")
    End If
    
'INICIO LDCR T6099
  'INICIO LDCR T6024
 ' Result = ExisteTABLA("IN_COMPRA")
  
 'INICIO LDCR T6081
 'If Result <> FAIL Then
 If ExisteTABLA("IN_COMPRA") Then
 'FIN LDCR T6099
    ReDim Arr(0)
    Campos = "NAME"
    Desde = "SYSOBJECTS"
    Condicion = " NAME LIKE '%TX_FA%'"
    Condicion = Condicion & " AND PARENT_OBJ IN (SELECT ID FROM SYSOBJECTS WHERE NAME = 'IN_COMPRA' AND XTYPE = 'U') "
    Condicion = Condicion & " AND XTYPE = 'D'"

    Result = LoadData(Desde, Campos, Condicion, Arr())

    If Result <> FAIL Then
       If Encontro Then
            Result = ExecSQLCommand("ALTER TABLE IN_COMPRA DROP CONSTRAINT [" & Arr(0) & "]")
       End If
    End If
 End If
 'FIN LDCR T6081
  If Result <> FAIL Then ExecSQLCommand ("ALTER TABLE IN_COMPRA ALTER COLUMN TX_FAPR_COMP   " & NombreCampo(10) & "(" & 20 & ") ")
  'FIN LDCR T6024
     
  'DAHV T6105 - INICIO
  If Result <> FAIL Then
        If ExisteTABLA("PARAMETROS_IMPUESTOS") Then
                If Not ExisteCAMPO("PARAMETROS_IMPUESTOS", "TX_IVADEDAR_PAIM") Then Result = CrearCampo("PARAMETROS_IMPUESTOS", "TX_IVADEDAR_PAIM", 10, 1, 1)
                If Result <> FAIL Then
                        Result = DoUpdate("PARAMETROS_IMPUESTOS", "TX_IVADEDAR_PAIM='N'", NUL$)
                End If
        End If
  End If
  
  If Result <> FAIL Then
        If ExisteTABLA("ARTICULO") Then
                If Not ExisteCAMPO("ARTICULO", "TX_IVADED_ARTI") Then Result = CrearCampo("ARTICULO", "TX_IVADED_ARTI", 10, 1, 1)
                If Result <> FAIL Then
                        Result = DoUpdate("ARTICULO", "TX_IVADED_ARTI='0'", NUL$)
                End If
        End If
  End If
  
  If Result <> FAIL Then
        If ExisteTABLA("IN_KARDEX") Then
                If Not ExisteCAMPO("IN_KARDEX", "TX_IVADED_KARD") Then Result = CrearCampo("IN_KARDEX", "TX_IVADED_KARD", 10, 1, 1)
                If Result <> FAIL Then
                        Result = DoUpdate("IN_KARDEX", "TX_IVADED_KARD='N'", NUL$)
                End If
        End If
  End If
  
  'DAHV T6105 - INICIO
     
     
     If Result <> FAIL Then
        Result = DoUpdate("PARAMETROS_INVE", "NU_NVERDB_PINV='7.1.0'", NUL$)
      End If
End Sub 'FIN LDCR T5865

'Numdia= Lun 1, Mar 2, Mie 3, Jue 4, Vie 5, Sab 6, Dom 7
Sub FechasDeUnDiaEnElA�o(ByVal NumDia As Byte, Arr() As Variant)
Dim DiaIni, N As Integer
Dim Fechaini As Date
Fechaini = CDate("01/01/" & Year(FechaServer))
DiaIni = Weekday(Fechaini)
If NumDia = 7 Then NumDia = 1 Else NumDia = NumDia + 1
If NumDia <> DiaIni Then
   While NumDia <> DiaIni
      Fechaini = DateAdd("d", 1, Fechaini)
      DiaIni = Weekday(Fechaini)
   Wend
End If
ReDim Arr(UBound(Arr, 1), 0)
N = 0
While Year(Fechaini) = Year(FechaServer)
   ReDim Preserve Arr(UBound(Arr, 1), N)
   Arr(0, N) = IIf(NumDia <> 1, CStr(NumDia - 1), "7")
   Arr(1, N) = Fechaini
   Fechaini = DateAdd("d", 7, Fechaini)
   N = N + 1
Wend
End Sub

Function Abrir_Archivo(ByVal Nomarch As String) As Boolean
On Error GoTo ERR
Open DirDB & "\" & Nomarch For Input As #1
Abrir_Archivo = True
Exit Function
ERR:
   Call ConvertErr
   Abrir_Archivo = False
End Function

'//////////////////////////////////
Function CrearTabla(Tbl As String, ByVal Def As String) As Integer
'Dim Result, i, J, TipCamp, TamCamp, NullCamp As Integer 'DEPURACION DE CODIGO
Dim J, TipCamp, TamCamp, NullCamp As Integer
Dim SQL, NombCamp, Campos As String
On Error GoTo control
While (Len(Def) > 0)
  J = InStr(Def, Coma)
  NombCamp = Left(Def, J - 1)
  Def = Right(Def, Len(Def) - J)
  J = InStr(Def, Coma)
  TipCamp = Val(Left(Def, J - 1))
  Def = Right(Def, Len(Def) - J)
  J = InStr(Def, Coma)
  TamCamp = Val(Left(Def, J - 1))
  Def = Right(Def, Len(Def) - J)
  J = InStr(Def, Coma)
  If (J = 0) Then J = Len(Def)
  NullCamp = Val(Left(Def, J - 1))
  Def = Right(Def, Len(Def) - J)
  Campos = NombCamp & ESP & NombreCampo(TipCamp)
  If TipCamp = 10 Then Campos = Campos & " (" & TamCamp & ")"
  If MotorBD = "ORACLE" Then
      If (TipCamp >= 1 And TipCamp <= 4) Or TipCamp = 12 Then
         Campos = Campos & " (" & TamCamp & ")"
      End If
      If TipCamp = 7 Then
         Campos = Campos & " (" & TamCamp & Coma & "2)"
      End If
  End If
  If MotorBD = "SQL" Then
      If TipCamp = 13 Then
         Campos = Campos & " (" & TamCamp & " )"
      End If
  End If
  Campos = Campos & ESP & IIf(NullCamp = 1, "NULL", "NOT NULL") & Coma
Wend
SQL = "CREATE TABLE " & Tbl & " (" & Left(Campos, Len(Campos) - 1) & ")"
Debug.Print SQL
'HRR M3678
'If MotorBD = "ACCESS" Then
'   BDDAO(BDCurCon).Execute SQL, dbFailOnError
'Else
   BD(BDCurCon).Execute SQL
'End If
CrearTabla = SUCCEED
Exit Function

control:
If ERR.Number <> 3010 Then
   Call ConvertErr
   CrearTabla = FAIL
End If
End Function

Function EliminarTabla(ByVal Tbl As String) As Integer
Dim SQL As String
SQL = "DROP TABLE " & Tbl
Result = ExecSQLCommand(SQL)
EliminarTabla = Result
End Function
'Nombre del campo, tipo de campo, tama�o,
'condicion que indica si acepta un nulo (0-No, 1-Si)
'Function CrearCampo(Tbl As String, Nomb As String, tipo As String, tam As Integer, CondNull As Byte, Optional AutoNum As Boolean, Optional defecto As Boolean) As Integer 'JLPB T41459-R37519 Se deja en comentario
Function CrearCampo(Tbl As String, Nomb As String, tipo As String, tam As Double, CondNull As Byte, Optional AutoNum As Boolean, Optional defecto As Boolean) As Integer 'JLPB T41459-R37519
Dim SQL As String

'If MotorBD = "ACCESS" Then
'   Dim Tabla As DAO.TableDef
'   Dim campo As DAO.Field
'   On Error GoTo ErrAcc
'   Set campo = New DAO.Field
'   campo.Name = Nomb
'   campo.Type = tipo
'   campo.Size = tam
'   If CondNull = 0 Then
'      campo.Required = True
'   Else
'      campo.Required = False
'      If tipo = 10 Then campo.AllowZeroLength = True
'   End If
'   If AutoNum Then
'      campo.Attributes = 16
'   End If
'   BDDAO(BDCurCon).TableDefs.Refresh
'   BDDAO(BDCurCon).TableDefs(Tbl).Fields.Append campo
'   BDDAO(BDCurCon).TableDefs.Refresh
'   CrearCampo = SUCCEED
'Else
   SQL = "ALTER TABLE " & Tbl & " ADD "
   'SQL = SQL & Nomb & ESP & NombreCampo(tipo) & ESP 'HRR M2662
   'HRR M2662
   If MotorBD = "ACCESS" And AutoNum Then
      SQL = SQL & Nomb & ESP & "COUNTER" & ESP
   Else
      SQL = SQL & Nomb & ESP & NombreCampo(tipo) & ESP
   End If
   'HRR M2662
   
   If MotorBD = "POSTGRES" Then
      If tipo = 10 Then SQL = SQL & "(" & tam & ") "
      Debug.Print SQL
   End If
   If MotorBD = "ORACLE" Then
      If tipo = 10 Or tipo = 12 Then SQL = SQL & "(" & tam & ") "
      If tipo >= 1 And tipo <= 4 Then
         SQL = SQL & "(" & tam & ") "
      End If
      If tipo = 7 Then
         SQL = SQL & "(" & tam & Coma & "2" & ")"
      End If
      If defecto = True Or AutoNum = True Then 'realizado para poner por defecto o a los autonumericos de ORACLE
         SQL = SQL & "DEFAULT 0 "
      End If
   End If
   If MotorBD = "SQL" Then
      If tipo = 10 Then SQL = SQL & "(" & tam & ") "
      If tipo = 13 Then SQL = SQL & "(" & Replace(CStr(tam), ".", ",") & ") " 'JLPB T41459-R37519
      If AutoNum Then
         SQL = SQL & " IDENTITY (1,1)"
      Else
         SQL = SQL & IIf(CondNull, "null", "DEFAULT 0")
      End If
   Else
      If MotorBD <> "POSTGRES" Then
         If tipo = 10 Then SQL = SQL & "(" & tam & ") " 'HRR M3678
         SQL = SQL & IIf(CondNull, "null", "not null")
      End If
   End If
   Result = ExecSQLCommand(SQL)
   If MotorBD = "SQL" And CondNull = 0 And Not AutoNum Then
      If ExisteTABLA(Tbl) Then 'JAUM T29614
         Result = DoUpdate(Tbl, Nomb & " = 0", NUL$) 'JAUM T29614
         SQL = "ALTER TABLE " & Tbl & " ALTER COLUMN " & Nomb & ESP & NombreCampo(tipo) & ESP
         If tipo = 10 Then SQL = SQL & "(" & tam & ") "
         If tipo = 13 Then SQL = SQL & "(" & Replace(CStr(tam), ".", ",") & ") " 'JLPB T41459-R37519
         SQL = SQL & " NOT NULL"
         Result = ExecSQLCommand(SQL)
      End If 'JAUM T29614
   End If
   If MotorBD = "POSTGRES" And CondNull = 0 Then
      SQL = "ALTER TABLE " & Tbl & " ADD CONSTRAINT " & Nomb & " CHECK(NOT " & Nomb & " IS NULL)"
      Result = ExecSQLCommand(SQL)
   End If

   'Creacion de Secuencias y Triger cuando sea autonumerico

   If AutoNum = True Then
      If MotorBD = "ORACLE" Then
         SQL = "create sequence SQ_" & Tbl & " start with 1 increment by 1"
         If Result <> FAIL Then Result = ExecSQLCommand(SQL)
         SQL = "create or replace trigger TR_" & Tbl
         SQL = SQL & " after insert on " & Tbl
         SQL = SQL & " update " & Tbl & " set " & Nomb & "=" & "SQ_" & Tbl & ".nextval where " & Nomb & "=0"
         If Result <> FAIL Then Result = ExecSQLCommand(SQL)
      End If

      If MotorBD = "POSTGRES" Then
         SQL = "create sequence SQ_" & Tbl & " start 1"
         If Result <> FAIL Then Result = ExecSQLCommand(SQL)
         SQL = "alter table " & Tbl & " alter column " & Nomb & " set default nextval('SQ_" & Tbl & "')"
         If Result <> FAIL Then Result = ExecSQLCommand(SQL)
      End If

   End If

   CrearCampo = Result

'End If
Exit Function
ErrAcc:
   Call ConvertErr
   CrearCampo = FAIL
End Function

Function EliminarCampo(Tbl As String, Nomb As String) As Integer
Dim SQL As String
SQL = "ALTER TABLE " & Tbl & " DROP COLUMN " & Nomb
Result = ExecSQLCommand(SQL)
EliminarCampo = Result
End Function
'Tabla, Nombre del indice, Campos separados por comas, Si es llave primaria
Function CrearIndice(Tbl As String, NombIndx As String, Campos As String, Pk As Boolean, Optional Unique As Boolean) As Integer
Dim SQL As String
If Pk = True Then
   If MotorBD = "POSTGRES" Then
      SQL = "CREATE UNIQUE INDEX " & NombIndx & " ON " & Tbl & "(" & Campos & ") "
   Else
      SQL = "ALTER TABLE " & Tbl & " ADD CONSTRAINT " & NombIndx & " PRIMARY KEY(" & Campos & ") "
   End If
Else
   If Unique = True Then
      SQL = "CREATE UNIQUE INDEX " & NombIndx & " ON " & Tbl & "(" & Campos & ") "
   Else
      SQL = "CREATE INDEX " & NombIndx & " ON " & Tbl & "(" & Campos & ") "
   End If
End If
Result = ExecSQLCommand(SQL)
CrearIndice = Result
End Function

Function EliminarIndice(Tbl As String, NombIndx As String)
Dim SQL As String
If MotorBD = "ACCESS" Then
   SQL = SQL & NombIndx & " ON " & Tbl
Else
   SQL = "ALTER TABLE " & Tbl & " DROP INDEX " & NombIndx
End If
If MotorBD = "SQL" Then SQL = "DROP INDEX " & Tbl & "." & NombIndx
Result = ExecSQLCommand(SQL)
EliminarIndice = Result
End Function

Function CrearRelacion(NombRel As String, CampFor As String, CampPri As String, Tbl As String, TblExt As String)
Dim SQL As String
'Dim REL As Relation     'DEPURACION DE CODIGO
'Dim CMP As Field        'DEPURACION DE CODIGO
On Error GoTo ERR
SQL = "ALTER TABLE " & Tbl & " ADD CONSTRAINT " & NombRel & " FOREIGN KEY ("
SQL = SQL & CampFor & ") REFERENCES " & TblExt & "(" & CampPri & ")"
Result = ExecSQLCommand(SQL)
If MotorBD = "ACCESS" Then
   'Set REL = New Relation
   'Set CMP = New Field
   'REL.Name = NombRel
   'REL.Table = TblExt
   'CMP.Name = CampPri
   'CMP.ForeignName = CampFor
   'REL.Fields.Append CMP
   'REL.ForeignTable = Tbl
   'REL.Attributes = dbRelationUpdateCascade
   'BDDAO(1).Relations.Append REL
   'BDDAO(1).Relations.Refresh

'   Debug.Print BDDAO(1).Relations(NombRel).Table
'   Debug.Print BDDAO(1).Relations(NombRel).ForeignTable
'   Debug.Print BDDAO(1).Relations(NombRel).Fields.Count
'   Debug.Print BDDAO(1).Relations(NombRel).Fields(0).Name
'   Debug.Print BDDAO(1).Relations(NombRel).Fields(0).ForeignName
End If
'dbRelationUpdateCascade
CrearRelacion = Result
Exit Function
ERR:
   Call ConvertErr
End Function

Function EliminarRelacion(NombRel As String, Tbl As String)
Dim SQL As String
On Error GoTo ERR
If NombRel <> "" Then
'   If MotorBD = "ACCESS" Then
'      BDDAO(1).Relations.Delete (NombRel)
'   Else
      SQL = "ALTER TABLE " & Tbl & " DROP CONSTRAINT " & NombRel
      Result = ExecSQLCommand(SQL)
      EliminarRelacion = Result
'   End If
End If
EliminarRelacion = Result
Exit Function
ERR:
   Call ConvertErr
End Function

Function NombreCampo(ByVal TipCamp As Integer) As String
If MotorBD = "ACCESS" Then
  Select Case TipCamp
    Case 1: NombreCampo = "BIT"
    Case 2: NombreCampo = "BYTE"
    Case 3: NombreCampo = "SMALLINT"
    Case 4: NombreCampo = "INT"
    Case 6: NombreCampo = "REAL"
    Case 7: NombreCampo = "FLOAT"
    Case 8: NombreCampo = "DATETIME"
    Case 10: NombreCampo = "TEXT"
    Case 12: NombreCampo = "MEMO"
    Case 13: NombreCampo = "INT"
  End Select
End If
If MotorBD = "SQL" Then
  Select Case TipCamp
    Case 1: NombreCampo = "BIT"
    Case 2: NombreCampo = "TINYINT"
    Case 3: NombreCampo = "SMALLINT"
    Case 4: NombreCampo = "INT"
    Case 6: NombreCampo = "REAL"
    Case 7: NombreCampo = "FLOAT"
    Case 8: NombreCampo = "DATETIME"
    Case 10: NombreCampo = "VARCHAR"
     Case 11: NombreCampo = "IMAGE" 'DRMG T44728-R41288
    Case 12: NombreCampo = "TEXT"
    Case 13: NombreCampo = "NUMERIC"
  End Select
End If
'No.  ACCESS              SQL         ORACLE          CAPACIDAD
'1    BIT(SI/NO)          BIT         NUMBER(1)         0 o 1
'2    BYTE                TINYINT     NUMBER(3)     Entero 0 a 255
'3    SMALLINT(ENTERO)    SMALLINT    NUMBER(4)     Entero -32.768 a 32.767
'4    INT(ENTERO LARGO)   INT         NUMBER(9)     Entero -2.147.483.648 a 2.147.483.647
'6    REAL(SIMPLE)        REAL                      -3,40E + 38 a 3,40E+38
'7    FLOAT(DOBLE)        FLOAT       NUMBER(15,2)  -1,79E + 308 a 1,79E +308
'8    DATETIME(FECHA/HORA)DATETIME    DATE
'10   TEXT(TEXTO)         VARCHAR     VARCHAR2      Requiere tama�o
'12   MEMO                TEXT        LONG
If MotorBD = "ORACLE" Then
   Select Case TipCamp
    Case 1: NombreCampo = "NUMBER"
    Case 2: NombreCampo = "NUMBER"
    Case 3: NombreCampo = "NUMBER"
    Case 4: NombreCampo = "NUMBER"
    Case 6: NombreCampo = ""
    Case 7: NombreCampo = "NUMBER"
    Case 8: NombreCampo = "DATE"
    Case 10: NombreCampo = "VARCHAR2"
    Case 12: NombreCampo = "VARCHAR2"
    Case 13: NombreCampo = "NUMERIC"
  End Select
End If
If MotorBD = "POSTGRES" Then 'POSTGRESS
   Select Case TipCamp
    Case 1: NombreCampo = "BIT"
    Case 2: NombreCampo = "NUMERIC(3,0)"
    Case 3: NombreCampo = "SMALLINT"
    Case 4: NombreCampo = "INTEGER"
    Case 6: NombreCampo = "REAL"
    Case 7: NombreCampo = "DOUBLE PRECISION"
    Case 8: NombreCampo = "TIMESTAMP"
    Case 10: NombreCampo = "VARCHAR"
    Case 12: NombreCampo = "TEXT"
'    Case 20: NombreCampo = "INTEGER"
   End Select
End If
End Function

Sub RelacionesLimpio()
    If MotorBD = "ACCESS" Then Set BDDAO(BDCurCon) = Workspaces(0).OpenDatabase(DirDB & NombBD & ".mdb", False, False, ";pwd=TRIACON")
    Dim Creacion As String, Eliminacion As String
    Dim EnProceso As String, LasQueFaltan As String
    Dim ArrPR() As String
    Dim Existen As Integer, control As Integer, Parame As Integer
    Creacion = CreaListaRelaDeTabla("ARTICULO", True)
    Eliminacion = ListaRelaDeTabla("ARTICULO", True)
    Existen = CuantosHay(Eliminacion, vbCr)
    LasQueFaltan = Eliminacion
    While Len(LasQueFaltan) > 0
        EnProceso = ParteIzquierda(LasQueFaltan, vbCr)
        Parame = CuantosHay(EnProceso, "|")
        Call CreaLosParametros(EnProceso, "|", ArrPR())
        If UBound(ArrPR) = 1 Then
            Debug.Print "Result = EliminarRelacion(" & ArrPR(0) & ", " & ArrPR(1) & ")"
            Result = EliminarRelacion(ArrPR(0), ArrPR(1))
        End If
        LasQueFaltan = ParteDerecha(LasQueFaltan, vbCr)
    Wend
'    Stop
    Call EliminaPrimaria("ARTICULO")
    Result = CrearCampo("ARTICULO", "NU_AUTO_ARTI", 4, 4, 1, True)
    Result = CrearCampo("ARTICULO", "NU_AUTO_UNVE_ARTI", 4, 4, 1, False)
    Result = CrearCampo("ARTICULO", "TX_VENCE_ARTI", 10, 1, 1, False)
    Result = CrearCampo("ARTICULO", "TX_PARA_ARTI", 10, 1, 1, False)
    Result = CrearCampo("ARTICULO", "TX_ENTRA_ARTI", 10, 1, 1, False)
    Result = CrearCampo("ARTICULO", "NU_AUTO_FOFA_ARTI", 4, 4, 1, False)
    Result = CrearCampo("ARTICULO", "NU_UTIL_ARTI", 7, 8, 1, False)
'    Call CreaPrimaria("ARTICULO", "PK_ARTICULO", "NU_AUTO_ARTI")
    Result = CrearIndice("ARTICULO", "PK_ARTICULO", "NU_AUTO_ARTI", True)
    Result = CrearIndice("ARTICULO", "LL_CD_CODI_ARTI", "CD_CODI_ARTI", False, True)
    Existen = CuantosHay(Creacion, vbCr)
    LasQueFaltan = Creacion
    While Len(LasQueFaltan) > 0
        EnProceso = ParteIzquierda(LasQueFaltan, vbCr)
        Parame = CuantosHay(EnProceso, "|")
        Call CreaLosParametros(EnProceso, "|", ArrPR())
        If UBound(ArrPR) = 4 Then
            Debug.Print "Result = CrearRelacion(" & ArrPR(0) & ", " & ArrPR(1) & ", " & ArrPR(2) & ", " & ArrPR(3) & ", " & ArrPR(4) & ")"
            Result = CrearRelacion(ArrPR(0), ArrPR(1), ArrPR(2), ArrPR(3), ArrPR(4))
        End If
        LasQueFaltan = ParteDerecha(LasQueFaltan, vbCr)
    Wend
'    Stop
    Call ModeloNuevoInventarios
    If MotorBD = "ACCESS" Then BDDAO(BDCurCon).Close: Set BDDAO(BDCurCon) = Nothing
End Sub

Sub CreaLosParametros(Cuerda As String, Cara As String, Arr() As String)
    Dim XProcesar As String
    Dim ind As Integer, Hay As Integer
    Hay = CuantosHay(Cuerda, Cara)
    XProcesar = Cuerda
    ReDim Arr(Hay)
    For ind = 0 To Hay - 1
        Arr(ind) = ParteIzquierda(XProcesar, Cara)
        XProcesar = ParteDerecha(XProcesar, Cara)
    Next
    Arr(Hay) = XProcesar
End Sub

Function ListaRelaDeTabla(Cual As String, Principal As Boolean) As String
    If MotorBD = "ACCESS" Then
        Dim rlt As Relation
        For Each rlt In BDDAO(BDCurCon).Relations
            If rlt.Table = Cual And Principal Then
    '            Debug.Print rlt.Name
                If Len(ListaRelaDeTabla) > 0 Then ListaRelaDeTabla = ListaRelaDeTabla & vbCr
                ListaRelaDeTabla = ListaRelaDeTabla & rlt.Name & "|" & rlt.ForeignTable
            End If
            If rlt.ForeignTable = Cual And Not Principal Then
    '            Debug.Print rlt.Name
                If Len(ListaRelaDeTabla) > 0 Then ListaRelaDeTabla = ListaRelaDeTabla & vbCr
                ListaRelaDeTabla = ListaRelaDeTabla & rlt.Name & "|" & rlt.Table
            End If
        Next
    Else
        Dim Cata As New adox.Catalog, Tabl As adox.Table, Sobr As adox.Table, Indc As adox.Index
        Dim Rela As adox.Key, Cion As adox.Key, Clmn As adox.Column, Prpd As adox.Property
        Dim Cuer As String, Otra As String
        Cata.ActiveConnection = BD(BDCurCon)
        For Each Tabl In Cata.Tables
            If Not Tabl.Name = Cual Then
                For Each Rela In Tabl.Keys
                    If Rela.RelatedTable = Cual Then
'                        ActiveWindow.ActiveSheet.Cells(Fila, Colu).Activate
'                        ActiveCell.Value = "ALTER TABLE [" & Tabl.Name & "] DROP CONSTRAINT " & Rela.Name
                        If Len(ListaRelaDeTabla) > 0 Then ListaRelaDeTabla = ListaRelaDeTabla & vbCr
                        ListaRelaDeTabla = ListaRelaDeTabla & Rela.Name & "|" & Tabl.Name
                    End If
                Next
            End If
        Next
    End If
    ListaRelaDeTabla = ListaRelaDeTabla & vbCr
End Function

Function CreaListaRelaDeTabla(Cual As String, Principal As Boolean) As String
    If MotorBD = "ACCESS" Then
        Dim rlt As Relation
        For Each rlt In BDDAO(BDCurCon).Relations
            Debug.Print rlt.Table, rlt.ForeignTable
            If rlt.Table = Cual And Principal Then
                If Len(CreaListaRelaDeTabla) > 0 Then CreaListaRelaDeTabla = CreaListaRelaDeTabla & vbCr
                CreaListaRelaDeTabla = CreaListaRelaDeTabla & rlt.Table & rlt.ForeignTable & "|" & CuerdaCampos(rlt.Fields, False) & "|" & CuerdaCampos(rlt.Fields, True) & "|" & rlt.ForeignTable & "|" & rlt.Table
            End If
            If rlt.ForeignTable = Cual And Not Principal Then
                If Len(CreaListaRelaDeTabla) > 0 Then CreaListaRelaDeTabla = CreaListaRelaDeTabla & vbCr
                CreaListaRelaDeTabla = CreaListaRelaDeTabla & rlt.Table & rlt.ForeignTable & "|" & CuerdaCampos(rlt.Fields, False) & "|" & CuerdaCampos(rlt.Fields, True) & "|" & rlt.ForeignTable & "|" & rlt.Table
            End If
        Next
    Else
        Dim Cata As New adox.Catalog, Tabl As adox.Table, Sobr As adox.Table, Indc As adox.Index
        Dim Rela As adox.Key, Cion As adox.Key, Clmn As adox.Column, Prpd As adox.Property
        Dim Cuer As String, Otra As String
        Cata.ActiveConnection = BD(BDCurCon)
        For Each Tabl In Cata.Tables
            If Not Tabl.Name = Cual Then
                For Each Rela In Tabl.Keys
                    If Rela.RelatedTable = Cual Then
'                        ActiveWindow.ActiveSheet.Cells(Fila, Colu).Activate
                        Cuer = "": Otra = ""
                        For Each Clmn In Rela.Columns
                            Cuer = Cuer & IIf(Len(Cuer) > 0, ",", "") & Clmn.Name
                            Otra = Otra & IIf(Len(Otra) > 0, ",", "") & Clmn.RelatedColumn
                        Next
'                        Cuer = Cuer & ")"
'                        Otra = Otra & ")"
'                        ActiveCell.Value = "ALTER TABLE [" & Tabl.Name & "] ADD CONSTRAINT " & Rela.Name & " FOREIGN KEY (" & Cuer & " REFERENCES [" & Rela.RelatedTable & "] (" & Otra
'Result = CrearRelacion("IN_ENCABEZADOIN_BODEGA", "NU_AUTO_BODEORG_ENCA", "NU_AUTO_BODE", "IN_ENCABEZADO", "IN_BODEGA")
                        If Len(CreaListaRelaDeTabla) > 0 Then CreaListaRelaDeTabla = CreaListaRelaDeTabla & vbCr
                        CreaListaRelaDeTabla = CreaListaRelaDeTabla & Rela.Name & "|" & Cuer & "|" & Otra & "|" & Tabl.Name & "|" & Rela.RelatedTable
                    End If
                Next
            End If
        Next
    End If
    CreaListaRelaDeTabla = CreaListaRelaDeTabla & vbCr
End Function

Function CuerdaCampos(CllCampos As DAO.Fields, Principal As Boolean) As String
    Dim UnCampo As DAO.Field
    For Each UnCampo In CllCampos
        If Len(CuerdaCampos) > 0 Then CuerdaCampos = CuerdaCampos & ","
        If Principal Then
            CuerdaCampos = CuerdaCampos & UnCampo.Name
        Else
            CuerdaCampos = CuerdaCampos & UnCampo.ForeignName
        End If
    Next
End Function
'
'Function CuantosHay(ByVal Desde As Integer, ByRef Cuerda As String, Cara As String) As Integer
'    Dim PoSi As Integer
'    PoSi = InStr(Desde, Cuerda, Cara, vbTextCompare)
'    If PoSi > 0 And PoSi < Len(Cuerda) Then CuantosHay = 1 + CuantosHay(PoSi + 1, Cuerda, Cara)
'End Function
'
Function BuscaPrimaria(Tabla As String) As String
    Dim ind As DAO.Index
    Dim Fld As DAO.Field
    Dim Tbl As DAO.TableDef
    Dim Cmp As Integer
    For Each Tbl In BDDAO(BDCurCon).TableDefs
        If Tbl.Name = Tabla Then
            For Each ind In Tbl.Indexes
                If ind.Primary Then
                    BuscaPrimaria = Tabla & "|"
                    For Each Fld In ind.Fields
                        If Cmp > 0 Then BuscaPrimaria = BuscaPrimaria & ","
                        BuscaPrimaria = BuscaPrimaria & Fld.Name
                        Cmp = Cmp + 1
                    Next
                    Exit For
                End If
            Next
            Exit For
        End If
    Next
End Function

Function EliminaPrimaria(Tabla As String) As String
    If MotorBD = "ACCESS" Then
        Dim ind As DAO.Index
        Dim Fld As DAO.Field
        Dim Tbl As DAO.TableDef
        Dim Cmp As Integer
        For Each Tbl In BDDAO(BDCurCon).TableDefs
            If Tbl.Name = Tabla Then
                For Each ind In Tbl.Indexes
                    If ind.Primary Then
                        Tbl.Indexes.Delete ind.Name
                        Exit For
                    End If
                Next
                Exit For
            End If
        Next
    Else
        Dim Cata As New adox.Catalog, Tabl As adox.Table, Indc As adox.Index
        Cata.ActiveConnection = BD(BDCurCon)
        Set Tabl = Cata.Tables(Tabla)
        If Tabl Is Nothing Then Exit Function
        For Each Indc In Tabl.Indexes
            If Indc.PrimaryKey Then
                Tabl.Indexes.Delete Indc.Name
                Exit For
            End If
        Next
    End If
End Function

Sub CreaPrimaria(Tabla As String, Nombre As String, Campos As String)
    If MotorBD = "ACCESS" Then
        Dim ind As DAO.Index
        Dim Fld As DAO.Field
        Dim Tbl As DAO.TableDef
        Dim ArrCPS() As String
        Dim Hay As Integer, Cmp As Integer
        Set Tbl = BDDAO(BDCurCon).TableDefs(Tabla)
        If Tbl Is Nothing Then Exit Sub
        With Tbl
            Call EliminaPrimaria(Tabla)
            Set ind = .CreateIndex(Nombre)
            ind.Name = Nombre
            ind.Primary = True
            Hay = CuantosHay(Campos, ",")
            With ind
                Call CreaLosParametros(Campos, ",", ArrCPS())
                For Cmp = 0 To Hay
                    .Fields.Append .CreateField(ArrCPS(Cmp))
                Next
            End With
            .Indexes.Append ind
        End With
    Else
        Call EliminaPrimaria(Tabla)
        Result = CrearIndice(Tabla, Nombre, Campos, True)
    End If
End Sub

Sub ModeloNuevoInventarios()
    
'BD(0).Execute ("CREATE TABLE [ARTICULO] (" & _
'"   [CD_CODI_ARTI] [varchar] (16) NOT NULL ,[NU_AUTO_ARTI] [int] IDENTITY (1, 1) NOT NULL ,[NO_NOMB_ARTI] [varchar] (50) NOT NULL ," & _
'"   [NU_AUTO_UNVE_ARTI] [int] NOT NULL ,[DE_DESC_ARTI] [varchar] (50) NULL ,[TX_VENCE_ARTI] [varchar] (1) NULL ,[TX_PARA_ARTI] [varchar] (1) NOT NULL ," & _
'"   [TX_ENTRA_ARTI] [varchar] (1) NOT NULL ,[CD_GRUP_ARTI] [varchar] (9) NOT NULL ,[CD_USOS_ARTI] [varchar] (4) NOT NULL ," & _
'"   [DE_UNME_ARTI] [varchar] (15) NULL ,[CT_CNVR_ARTI] [real] NULL ,[DE_UNCO_ARTI] [varchar] (15) NULL ,[DE_CTRA_ARTI] [varchar] (15) NULL ," & _
'"   [CT_MAXI_ARTI] [real] NULL ,[CT_MINI_ARTI] [real] NULL ,[FE_VENC_ARTI] [datetime] NULL ,[ID_GRAV_ARTI] [varchar] (1) NOT NULL ," & _
'"   [PR_IMPU_ARTI] [float] NULL ,[VL_ULCO_ARTI] [float] NULL ,[VL_COPR_ARTI] [float] NULL ,[CT_EXIS_ARTI] [real] NULL ," & _
'"   [DE_OBSE_ARTI] [varchar] (70) NULL ,[ID_TIPO_ARTI] [tinyint] NULL ,[DE_FOFA_ARTI] [varchar] (20) NULL ,[DE_UBIC_ARTI] [varchar] (60) NULL ," & _
'"   [CD_RIPS_ARTI] [varchar] (16) NULL ,[NU_INDPYP_ARTI] [tinyint] NULL ,[NU_AUTO_FOFA_ARTI] [int] NOT NULL )")

'CD_CODI_ARTI,NO_NOMB_ARTI,DE_DESC_ARTI,CD_GRUP_ARTI,CD_USOS_ARTI,DE_UNME_ARTI,CT_CNVR_ARTI,
'DE_UNCO_ARTI,DE_CTRA_ARTI,CT_MAXI_ARTI,CT_MINI_ARTI,FE_VENC_ARTI,ID_GRAV_ARTI,PR_IMPU_ARTI,VL_ULCO_ARTI,
'VL_COPR_ARTI , CT_EXIS_ARTI, DE_OBSE_ARTI, ID_TIPO_ARTI, DE_UBIC_ARTI, CD_RIPS_ARTI, NU_INDPYP_ARTI
    
    Result = CrearTabla("CONEXION", "NU_AUTO_USUA_CONE, 4, 4, 0")
    Result = CrearCampo("CONEXION", "NU_AUTO_CONE", 4, 4, 1, True)
    Result = CrearCampo("CONEXION", "FE_FECH_CONE", 8, 8, 1, False)
    Result = CrearCampo("CONEXION", "TX_COMP_CONE", 10, 70, 1, False)
    Result = CrearCampo("CONEXION", "FE_DESC_CONE", 8, 8, 1, False)
    Result = CrearTabla("FORMULARIO", "TX_DESC_FORM, 10, 50, 0")
    Result = CrearCampo("FORMULARIO", "NU_AUTO_FORM", 4, 4, 1, True)
    Result = CrearCampo("FORMULARIO", "TX_FORM_FORM", 10, 25, 1, False)
    Result = CrearCampo("FORMULARIO", "TX_TIPO_FORM", 10, 3, 1, False)
    Result = CrearTabla("IN_AUTORIZACION", "NU_AUTO_COMP_AUTO, 4, 4, 0")
    Result = CrearCampo("IN_AUTORIZACION", "NU_AUTO_AUTO", 4, 4, 1, True)
    Result = CrearCampo("IN_AUTORIZACION", "NU_AUTO_USUA_AUTO", 4, 4, 1, False)
    Result = CrearCampo("IN_AUTORIZACION", "NU_CONE_AUTO", 4, 4, 1, False)
    Result = CrearCampo("IN_AUTORIZACION", "TX_OBSE_AUTO", 12, 0, 1, False)
    Result = CrearTabla("IN_BODEGA", "TX_CODI_BODE, 10, 10, 0")
    Result = CrearCampo("IN_BODEGA", "NU_AUTO_BODE", 4, 4, 1, True)
    Result = CrearCampo("IN_BODEGA", "NU_AUTO_BODE_BODE", 4, 4, 1, False)
    Result = CrearCampo("IN_BODEGA", "TX_NOMB_BODE", 10, 50, 0, False)
    Result = CrearCampo("IN_BODEGA", "CD_CODI_CECO_BODE", 3, 2, 0, False)
    Result = CrearCampo("IN_BODEGA", "TX_VENTA_BODE", 10, 1, 0, False)
    Result = CrearTabla("IN_CODIGOBAR", "NU_AUTO_ARTI_COBA, 4, 4, 0")
    Result = CrearCampo("IN_CODIGOBAR", "NU_AUTO_COBA", 4, 4, 1, True)
    Result = CrearCampo("IN_CODIGOBAR", "TX_COBA_COBA", 10, 13, 0, False)
    Result = CrearCampo("IN_CODIGOBAR", "CD_CODI_TERC_COBA", 10, 11, 0, False)
    Result = CrearTabla("IN_COMPROBANTE", "TX_CODI_COMP, 10, 10, 0")
    Result = CrearCampo("IN_COMPROBANTE", "NU_AUTO_COMP", 4, 4, 1, True)
    Result = CrearCampo("IN_COMPROBANTE", "TX_NOMB_COMP", 10, 50, 0, False)
    Result = CrearCampo("IN_COMPROBANTE", "TX_PRFJ_COMP", 10, 2, 0, False)
    Result = CrearCampo("IN_COMPROBANTE", "NU_AUTO_DOCU_COMP", 4, 4, 0, False)
    Result = CrearCampo("IN_COMPROBANTE", "NU_COMP_COMP", 4, 4, 0, False)
    Result = CrearCampo("IN_COMPROBANTE", "NU_AUTO_ANUL_COMP", 4, 4, 1, False)
    Result = CrearCampo("IN_COMPROBANTE", "TX_REQU_COMP", 10, 1, 0, False)
    Result = CrearCampo("IN_COMPROBANTE", "TX_ROMP_COMP", 10, 1, 1, False)
    Result = CrearTabla("IN_DETALLE", "FE_PERI_DETA, 8, 8, 0")
    Result = CrearCampo("IN_DETALLE", "NU_AUTO_DETA", 4, 4, 1, True)
    Result = CrearCampo("IN_DETALLE", "FE_FECH_DETA", 8, 8, 0, False)
    Result = CrearCampo("IN_DETALLE", "NU_AUTO_ORGCABE_DETA", 4, 4, 0, False)
    Result = CrearCampo("IN_DETALLE", "NU_AUTO_MODCABE_DETA", 4, 4, 0, False)
    Result = CrearCampo("IN_DETALLE", "NU_AUTO_BODE_DETA", 4, 4, 0, False)
    Result = CrearCampo("IN_DETALLE", "NU_AUTO_ARTI_DETA", 4, 4, 0, False)
    Result = CrearCampo("IN_DETALLE", "NU_ENTRAD_DETA", 4, 4, 0, False)
    Result = CrearCampo("IN_DETALLE", "NU_SALIDA_DETA", 4, 4, 0, False)
    Result = CrearCampo("IN_DETALLE", "NU_MULT_DETA", 3, 2, 0, False)
    Result = CrearCampo("IN_DETALLE", "NU_DIVI_DETA", 3, 2, 0, False)
    Result = CrearCampo("IN_DETALLE", "NU_COSTO_DETA", 7, 8, 0, False)
    Result = CrearCampo("IN_DETALLE", "NU_IMPU_DETA", 6, 4, 1, False)
    Result = CrearCampo("IN_DETALLE", "NU_VALOR_DETA", 7, 8, 0, False)
    Result = CrearCampo("IN_DETALLE", "NU_AUTO_UNVE_DETA", 4, 4, 1, False)
    Result = CrearTabla("IN_DOCUMENTO", "TX_CODI_DOCU, 10, 6, 0")
    Result = CrearCampo("IN_DOCUMENTO", "NU_AUTO_DOCU", 4, 4, 1, False)
    Result = CrearCampo("IN_DOCUMENTO", "TX_NOMB_DOCU", 10, 50, 0, False)
    Result = CrearCampo("IN_DOCUMENTO", "TX_AFEC_DOCU", 10, 1, 0, False)
    Result = CrearCampo("IN_DOCUMENTO", "TX_INTE_DOCU", 10, 1, 0, False)
    Result = CrearCampo("IN_DOCUMENTO", "NU_AUTO_DOCUDEPE_DOCU", 4, 4, 1, False)
    Result = CrearCampo("IN_DOCUMENTO", "NU_AUTO_DOCUREVE_DOCU", 4, 4, 1, False)
    Result = CrearCampo("IN_DOCUMENTO", "TX_TIPOVAL_DOCU", 10, 1, 0, False)
    Result = CrearCampo("IN_DOCUMENTO", "TX_INDE_DOCU", 10, 1, 1, False)
    Result = CrearTabla("IN_ENCABEZADO", "NU_AUTO_COMP_ENCA, 4, 4, 0")
    Result = CrearCampo("IN_ENCABEZADO", "NU_AUTO_ENCA", 4, 4, 1, True)
    Result = CrearCampo("IN_ENCABEZADO", "NU_AUTO_DOCU_ENCA", 4, 4, 1, False)
    Result = CrearCampo("IN_ENCABEZADO", "NU_COMP_ENCA", 4, 4, 0, False)
    Result = CrearCampo("IN_ENCABEZADO", "NU_AUTO_BODEORG_ENCA", 4, 4, 0, False)
    Result = CrearCampo("IN_ENCABEZADO", "NU_AUTO_BODEDST_ENCA", 4, 4, 1, False)
    Result = CrearCampo("IN_ENCABEZADO", "FE_CREA_ENCA", 8, 8, 0, False)
    Result = CrearCampo("IN_ENCABEZADO", "FE_CIER_ENCA", 8, 8, 0, False)
    Result = CrearCampo("IN_ENCABEZADO", "CD_CODI_TERC_ENCA", 10, 11, 1, False)
    Result = CrearCampo("IN_ENCABEZADO", "TX_OBSE_ENCA", 12, 0, 1, False)
    Result = CrearCampo("IN_ENCABEZADO", "TX_ESTA_ENCA", 10, 1, 1, False)
    Result = CrearCampo("IN_ENCABEZADO", "NU_CONE_ENCA", 4, 4, 1, False)
    Result = CrearCampo("IN_ENCABEZADO", "NU_AUTO_USUA_ENCA", 4, 4, 1, False)
    Result = CrearCampo("IN_ENCABEZADO", "NU_AUTO_AUTO_ENCA", 4, 4, 1, False)
'    Result = CrearTabla("IN_EXISTENCIA", "NU_AUTO_BODE_EXIS, 4, 4, 0")
'    Result = CrearCampo("IN_EXISTENCIA", "NU_AUTO_ARTI_EXIS", 4, 4, 0, False)
'    Result = CrearCampo("IN_EXISTENCIA", "FE_PERI_EXIS", 8, 8, 1, False)
'    Result = CrearCampo("IN_EXISTENCIA", "NU_SLINNMR_EXIS", 4, 4, 1, False)
'    Result = CrearCampo("IN_EXISTENCIA", "NU_SLINDNM_EXIS", 4, 4, 1, False)
'    Result = CrearIndice("IN_EXISTENCIA", "FK_NU_AUTO_ARTI_EXIS", "NU_AUTO_ARTI_EXIS", False, False)
''    Result = CrearIndice("IN_EXISTENCIA", "FK_NU_AUTO_ARTI_EXIS", "NU_AUTO_ARTI_EXIS", False, False)
'    Result = CrearIndice("IN_EXISTENCIA", "FK_TX_PERI_EXIS", "FE_PERI_EXIS", False, False)
'    Result = CrearIndice("IN_EXISTENCIA", "PK_IN_EXISTENCIA", "NU_AUTO_BODE_EXIS, NU_AUTO_ARTI_EXIS", True)
    Result = CrearTabla("IN_FORMAFARMACEUTICA", "TX_CODI_FOFA, 10, 10, 0")
    Result = CrearCampo("IN_FORMAFARMACEUTICA", "NU_AUTO_FOFA", 4, 4, 1, True)
    Result = CrearCampo("IN_FORMAFARMACEUTICA", "TX_NOMB_FOFA", 10, 50, 1, False)
    Result = CrearTabla("IN_KARDEX", "FE_PERI_KARD, 8, 8, 0")
    Result = CrearCampo("IN_KARDEX", "NU_AUTO_KARD", 4, 4, 1, True)
    Result = CrearCampo("IN_KARDEX", "FE_FECH_KARD", 8, 8, 0, False)
    Result = CrearCampo("IN_KARDEX", "NU_AUTO_ORGCABE_KARD", 4, 4, 0, False)
    Result = CrearCampo("IN_KARDEX", "NU_AUTO_MODCABE_KARD", 4, 4, 0, False)
    Result = CrearCampo("IN_KARDEX", "NU_AUTO_BODE_KARD", 4, 4, 0, False)
    Result = CrearCampo("IN_KARDEX", "NU_AUTO_ARTI_KARD", 4, 4, 0, False)
    Result = CrearCampo("IN_KARDEX", "NU_ENTRAD_KARD", 4, 4, 1, False)
    Result = CrearCampo("IN_KARDEX", "NU_SALIDA_KARD", 4, 4, 1, False)
    Result = CrearCampo("IN_KARDEX", "NU_MULT_KARD", 3, 2, 1, False)
    Result = CrearCampo("IN_KARDEX", "NU_DIVI_KARD", 3, 2, 1, False)
    Result = CrearCampo("IN_KARDEX", "NU_ENTRNMR_KARD", 4, 4, 1, False)
    Result = CrearCampo("IN_KARDEX", "NU_ENTRDNM_KARD", 4, 4, 1, False)
    Result = CrearCampo("IN_KARDEX", "NU_SALINMR_KARD", 4, 4, 1, False)
    Result = CrearCampo("IN_KARDEX", "NU_SALIDNM_KARD", 4, 4, 1, False)
    Result = CrearCampo("IN_KARDEX", "NU_ACTUNMR_KARD", 4, 4, 1, False)
    Result = CrearCampo("IN_KARDEX", "NU_ACTUDNM_KARD", 4, 4, 1, False)
    Result = CrearCampo("IN_KARDEX", "NU_COSTO_KARD", 7, 8, 1, False)
    Result = CrearCampo("IN_KARDEX", "NU_IMPU_KARD", 6, 4, 1, False)
    Result = CrearCampo("IN_KARDEX", "NU_VALOR_KARD", 7, 8, 1, False)
    Result = CrearCampo("IN_KARDEX", "NU_APUN_KARD", 4, 4, 1, False)
    Result = CrearCampo("IN_KARDEX", "NU_AUTO_UNVE_KARD", 4, 4, 0, False)
    Result = CrearTabla("IN_KARDEXLOTE", "NU_AUTO_BODE_KLOT, 4, 4, 0")
    Result = CrearCampo("IN_KARDEXLOTE", "NU_AUTO_KLOT", 4, 4, 1, True)
    Result = CrearCampo("IN_KARDEXLOTE", "NU_AUTO_LOTE_KLOT", 4, 4, 0, False)
    Result = CrearCampo("IN_KARDEXLOTE", "NU_AUTO_KARD_KLOT", 4, 4, 0, False)
    Result = CrearCampo("IN_KARDEXLOTE", "NU_ENTRAD_KLOT", 4, 4, 1, False)
    Result = CrearCampo("IN_KARDEXLOTE", "NU_SALIDA_KLOT", 4, 4, 1, False)
    Result = CrearCampo("IN_KARDEXLOTE", "NU_MULT_KLOT", 4, 4, 1, False)
    Result = CrearCampo("IN_KARDEXLOTE", "NU_DIVI_KLOT", 4, 4, 1, False)
    Result = CrearCampo("IN_KARDEXLOTE", "NU_ACTUNMR_KLOT", 4, 4, 1, False)
    Result = CrearCampo("IN_KARDEXLOTE", "NU_ACTUDNM_KLOT", 4, 4, 1, False)
    Result = CrearCampo("IN_KARDEXLOTE", "NU_APUN_KLOT", 4, 4, 1, False)
    Result = CrearTabla("IN_LIMITES_BODE_ARTI", "NU_AUTO_BODE_LIBA, 4, 4, 0")
    Result = CrearCampo("IN_LIMITES_BODE_ARTI", "NU_AUTO_ARTI_LIBA", 4, 4, 0, False)
    Result = CrearCampo("IN_LIMITES_BODE_ARTI", "NU_STMAX_LIBA", 4, 4, 1, False)
    Result = CrearCampo("IN_LIMITES_BODE_ARTI", "NU_STMIN_LIBA", 4, 4, 1, False)
    Result = CrearCampo("IN_LIMITES_BODE_ARTI", "NU_REPOS_LIBA", 4, 4, 1, False)
    Result = CrearCampo("IN_LIMITES_BODE_ARTI", "NU_MXDES_LIBA", 4, 4, 1, False)
    Result = CrearTabla("IN_LISTAPRECIO", "TX_CODI_LIPR, 10, 10, 0")
    Result = CrearCampo("IN_LISTAPRECIO", "NU_AUTO_LIPR", 4, 4, 1, True)
    Result = CrearCampo("IN_LISTAPRECIO", "TX_NOMB_LIPR", 10, 50, 0, False)
    Result = CrearTabla("IN_LOTE", "NU_AUTO_ARTI_LOTE, 4, 4, 0")
    Result = CrearCampo("IN_LOTE", "NU_AUTO_LOTE", 4, 4, 1, True)
    Result = CrearCampo("IN_LOTE", "FE_FECHA_LOTE", 8, 8, 1, False)
    Result = CrearCampo("IN_LOTE", "TX_TIPO_LOTE", 10, 1, 1, False)
    Result = CrearTabla("IN_R_BODE_ARTI_UNDVENTA", "NU_AUTO_BODE_RBAU, 4, 4, 0")
    Result = CrearCampo("IN_R_BODE_ARTI_UNDVENTA", "NU_AUTO_ARTI_RBAU", 4, 4, 0, False)
    Result = CrearCampo("IN_R_BODE_ARTI_UNDVENTA", "NU_AUTO_UNVE_RBAU", 4, 4, 0, False)
    Result = CrearTabla("IN_R_BODE_COMP", "NU_AUTO_BODE_RBOCO, 4, 4, 0")
    Result = CrearCampo("IN_R_BODE_COMP", "NU_AUTO_COMP_RBOCO", 4, 4, 0, False)
    Result = CrearTabla("IN_R_BODE_DOCU", "NU_AUTO_BODE_RBODU, 4, 4, 0")
    Result = CrearCampo("IN_R_BODE_DOCU", "NU_AUTO_DOCU_RBODU", 4, 4, 0, False)
    Result = CrearTabla("IN_R_CENTRO_COSTOBODEGA", "CD_CODI_CECO_RCCBO, 10, 11, 0")
    Result = CrearCampo("IN_R_CENTRO_COSTOBODEGA", "NU_AUTO_BODE_RCCBO", 4, 4, 0, False)
    Result = CrearTabla("IN_R_LIST_ARTI", "NU_AUTO_LIPR_RLIA, 4, 4, 0")
    Result = CrearCampo("IN_R_LIST_ARTI", "NU_AUTO_ARTI_RLIA", 4, 4, 0, False)
    Result = CrearCampo("IN_R_LIST_ARTI", "FE_DESDE_RLIA", 8, 8, 0, False)
    Result = CrearCampo("IN_R_LIST_ARTI", "NU_PRECIO_RLIA", 7, 8, 0, False)
    Result = CrearTabla("IN_R_PERFIL_BODEGA", "NU_AUTO_BODE_RPEB, 4, 4, 0")
    Result = CrearCampo("IN_R_PERFIL_BODEGA", "NU_AUTO_PERF_RPEB", 4, 4, 0, False)
    Result = CrearTabla("IN_UNDVENTA", "TX_CODI_UNVE, 10, 10, 0")
    Result = CrearCampo("IN_UNDVENTA", "NU_AUTO_UNVE", 4, 4, 1, True)
    Result = CrearCampo("IN_UNDVENTA", "TX_NOMB_UNVE", 10, 50, 0, False)
    Result = CrearCampo("IN_UNDVENTA", "NU_MULT_UNVE", 3, 2, 0, False)
    Result = CrearCampo("IN_UNDVENTA", "NU_DIVI_UNVE", 3, 2, 0, False)
    Result = CrearCampo("IN_UNDVENTA", "VALOR", 4, 4, 1, False)
    Result = CrearTabla("OPCION", "TX_CODI_OPCI, 10, 12, 0")
    Result = CrearCampo("OPCION", "NU_AUTO_OPCI", 4, 4, 1, True)
    Result = CrearCampo("OPCION", "TX_DESC_OPCI", 10, 50, 1, False)
    Result = CrearCampo("OPCION", "TX_CODPADR_OPCI", 10, 12, 1, False)
    Result = CrearCampo("OPCION", "NU_AUTO_FORM_OPCI", 4, 4, 1, False)
    Result = CrearTabla("PERFIL", "NU_AUTO_EMPR_PERF, 4, 4, 1")
    Result = CrearCampo("PERFIL", "NU_AUTO_PERF", 4, 4, 1, True)
    Result = CrearCampo("PERFIL", "TX_CODI_PERF", 10, 10, 0, False)
    Result = CrearCampo("PERFIL", "TX_DESC_PERF", 10, 50, 1, False)
    Result = CrearTabla("PERMISO", "NU_AUTO_PERF_PERM, 4, 4, 0")
    Result = CrearCampo("PERMISO", "NU_AUTO_OPCI_PERM", 4, 4, 0, False)
    Result = CrearCampo("PERMISO", "TX_CREA_PERM", 10, 1, 1, False)
    Result = CrearCampo("PERMISO", "TX_ANUL_PERM", 10, 1, 1, False)
    Result = CrearCampo("PERMISO", "TX_CONS_PERM", 10, 1, 1, False)
    Result = CrearCampo("PERMISO", "TX_IMPR_PERM", 10, 1, 1, False)
    Result = CrearCampo("PERMISO", "TX_OTRO_PERM", 10, 1, 1, False)
    Result = CrearCampo("PERMISO", "TX_OTR1_PERM", 10, 1, 1, False)
    Result = CrearTabla("USUARIO", "TX_IDEN_USUA, 10, 30, 0")
    Result = CrearCampo("USUARIO", "NU_AUTO_USUA", 4, 4, 1, True)
    Result = CrearCampo("USUARIO", "TX_PASS_USUA", 10, 15, 1, False)
    Result = CrearCampo("USUARIO", "TX_DESC_USUA", 10, 50, 1, False)
    Result = CrearCampo("USUARIO", "NU_AUTO_PERF_USUA", 4, 4, 1, False)
'5_1_0
    Result = CrearCampo("ARTICULO", "NU_UTIL_ARTI", 7, 8, 1, False)
'R_GRUP_DEPE (CD_AJDB_RGD, CD_AJCR_RGD)
'    Result = CrearCampo("R_GRUP_DEPE", "CD_AJDB_RGD", 10, 14, 1, False)
'    Result = CrearCampo("R_GRUP_DEPE", "CD_AJCR_RGD", 10, 14, 1, False)
    Result = CrearCampo("R_GRUP_DEPE", "CD_AJDB_RGD", 10, 14, 1, False)
    Result = CrearCampo("R_GRUP_DEPE", "CD_AJCR_RGD", 10, 14, 1, False)
    
    Result = CrearCampo("GRUP_ARTICULO", "CD_AJDB_GRUP", 10, 14, 1, False)
    Result = CrearCampo("GRUP_ARTICULO", "CD_AJCR_GRUP", 10, 14, 1, False)
    
    Result = CrearCampo("IN_ENCABEZADO", "FE_VIGE_ENCA", 8, 8, 0, False)
    
    Result = CrearTabla("IN_CRITERIO", "TX_DESC_CRIT, 10, 30, 1")
    Result = CrearCampo("IN_CRITERIO", "NU_AUTO_CRIT", 4, 4, 1, True)
    Result = CrearCampo("IN_CRITERIO", "TX_CODI_CRIT", 10, 10, 1, False)
    Result = CrearIndice("IN_CRITERIO", "PK_IN_CRITERIO", "NU_AUTO_CRIT", True)
    Result = CrearIndice("IN_CRITERIO", "LL_IN_CRITERIO", "TX_CODI_CRIT", False, True)
    Result = CrearTabla("IN_R_CRITERIO_TERCERO", "NU_AUTO_CRIT_RCRTE, 4, 4, 0")
    Result = CrearCampo("IN_R_CRITERIO_TERCERO", "CD_CODI_TERC_RCRTE", 10, 11, 0, False)
    Result = CrearCampo("IN_R_CRITERIO_TERCERO", "NU_CALI_RCRTE", 7, 8, 0, False)
    Result = CrearCampo("IN_R_CRITERIO_TERCERO", "NU_PESO_RCRTE", 7, 8, 0, False)
    Result = CrearRelacion("IN_R_CRITERIO_TERCEROCRITERIO", "NU_AUTO_CRIT_RCRTE", "NU_AUTO_CRIT", "IN_R_CRITERIO_TERCERO", "IN_CRITERIO")
    Result = CrearRelacion("IN_R_CRITERIO_TERCEROTERCERO", "CD_CODI_TERC_RCRTE", "CD_CODI_TERC", "IN_R_CRITERIO_TERCERO", "TERCERO")
    
    BD(BDCurCon).Execute ("DROP TABLE [PARAMETROS_INVE]")
    
    Result = CrearTabla("PARAMETROS_INVE", "FE_CIER_APLI, 8, 8, 0")
    Result = CrearCampo("PARAMETROS_INVE", "ID_CONT_APLI", 10, 1, 1, False)
    Result = CrearCampo("PARAMETROS_INVE", "ID_PRES_APLI", 10, 1, 1, False)
    Result = CrearCampo("PARAMETROS_INVE", "ID_PRED_APLI", 10, 1, 1, False)
    Result = CrearCampo("PARAMETROS_INVE", "ID_INCO_APLI", 10, 1, 1, False)
    Result = CrearCampo("PARAMETROS_INVE", "ID_SEPU_APLI", 10, 1, 1, False)
    Result = CrearCampo("PARAMETROS_INVE", "ID_PACI_APLI", 10, 1, 1, False)
    Result = CrearCampo("PARAMETROS_INVE", "ID_NOMI_APLI", 10, 1, 1, False)
    Result = CrearCampo("PARAMETROS_INVE", "ID_ACTI_APLI", 10, 1, 1, False)
    Result = CrearCampo("PARAMETROS_INVE", "ID_CXC_APLI", 10, 1, 1, False)
    Result = CrearCampo("PARAMETROS_INVE", "ID_CXP_APLI", 10, 1, 1, False)
    Result = CrearCampo("PARAMETROS_INVE", "ID_DEUS_APLI", 10, 1, 1, False)
    Result = CrearCampo("PARAMETROS_INVE", "CD_CXC_APLI", 10, 14, 1, False)
    Result = CrearCampo("PARAMETROS_INVE", "CD_CXP_APLI", 10, 14, 1, False)
    Result = CrearCampo("PARAMETROS_INVE", "CD_CAJA_APLI", 10, 14, 1, False)
    Result = CrearCampo("PARAMETROS_INVE", "CD_DECO_APLI", 10, 14, 1, False)
    Result = CrearCampo("PARAMETROS_INVE", "CD_DEVE_APLI", 10, 14, 1, False)
    Result = CrearCampo("PARAMETROS_INVE", "CD_DEAD_APLI", 10, 14, 1, False)
    Result = CrearCampo("PARAMETROS_INVE", "CD_FLET_APLI", 10, 14, 1, False)
    Result = CrearCampo("PARAMETROS_INVE", "CD_CONT_APLI", 10, 3, 1, False)
    Result = CrearCampo("PARAMETROS_INVE", "CD_ORDE_APLI", 4, 4, 1, False)
    Result = CrearCampo("PARAMETROS_INVE", "CD_ARFL_APLI", 10, 20, 1, False)
    Result = CrearCampo("PARAMETROS_INVE", "CD_FLIN_APLI", 10, 20, 1, False)
    Result = CrearCampo("PARAMETROS_INVE", "NU_NVERDB_PINV", 10, 13, 1, False)
    Result = CrearCampo("PARAMETROS_INVE", "ID_REST_PINV", 10, 1, 1, False)
    Result = CrearCampo("PARAMETROS_INVE", "ID_NOAU_PINV", 10, 1, 1, False)
    Result = CrearCampo("PARAMETROS_INVE", "ID_DECVAL_PINV", 10, 1, 1, False)
    Result = CrearCampo("PARAMETROS_INVE", "NU_DECVAL_PINV", 4, 4, 1, False)
    Result = CrearCampo("PARAMETROS_INVE", "ID_DECCANT_PINV", 10, 1, 1, False)
    Result = CrearCampo("PARAMETROS_INVE", "NU_DECCANT_PINV", 4, 4, 1, False)
    Result = CrearCampo("PARAMETROS_INVE", "ID_DECPORC_PINV", 10, 1, 1, False)
    Result = CrearCampo("PARAMETROS_INVE", "NU_DECPORC_PINV", 4, 4, 1, False)
    Result = CrearCampo("PARAMETROS_INVE", "ID_AUTORCO_PINV", 10, 1, 1, False)
'5_1_0
    Result = CrearCampo("PARAMETROS_INVE", "ID_UTILID_PINV", 10, 1, 1, False)
    Result = CrearCampo("PARAMETROS_INVE", "CD_DONA_APLI", 10, 14, 1, False)
    
    Result = CrearIndice("IN_LISTAPRECIO", "LL_IN_LISTAPRECIO", "TX_CODI_LIPR", False, True)
    Result = CrearIndice("FORMULARIO", "PK_FORMULARIO", "NU_AUTO_FORM", True)
    Result = CrearIndice("IN_AUTORIZACION", "PK_IN_AUTORIZACION", "NU_AUTO_AUTO", True)
    Result = CrearIndice("IN_BODEGA", "FK_CD_CODI_CECO_BODE", "CD_CODI_CECO_BODE", False, False)
    Result = CrearIndice("IN_BODEGA", "FK_NU_AUTO_BODE_BODE", "NU_AUTO_BODE_BODE", False, False)
    Result = CrearIndice("IN_BODEGA", "LL_IN_BODEGA", "TX_CODI_BODE", False, True)
    Result = CrearIndice("IN_BODEGA", "PK_INBODEGA", "NU_AUTO_BODE", True)
    Result = CrearIndice("IN_CODIGOBAR", "FK_CD_CODI_TERC_COBA", "CD_CODI_TERC_COBA", False, False)
    Result = CrearIndice("IN_CODIGOBAR", "FK_NU_AUTO_ARTI_COBA", "NU_AUTO_ARTI_COBA", False, False)
    Result = CrearIndice("IN_CODIGOBAR", "LL_IN_CODIGOBAR", "TX_COBA_COBA", False, True)
    Result = CrearIndice("IN_CODIGOBAR", "PK_IN_CODIGOBAR", "NU_AUTO_COBA", True)
    Result = CrearIndice("IN_COMPROBANTE", "FK_NU_AUTO_DOCU_COMP", "NU_AUTO_DOCU_COMP", False, False)
    Result = CrearIndice("IN_COMPROBANTE", "LL_IN_CONSECUTIVO", "TX_CODI_COMP", False, True)
    Result = CrearIndice("IN_COMPROBANTE", "PK_IN_CONSECUTIVO", "NU_AUTO_COMP", True)
    Result = CrearIndice("IN_DETALLE", "FK_NU_AUTO_ARTI_MOVI", "NU_AUTO_ARTI_DETA", False, False)
    Result = CrearIndice("IN_DETALLE", "FK_NU_AUTO_ORGCABE_DETA", "NU_AUTO_ORGCABE_DETA", False, False)
    Result = CrearIndice("IN_DETALLE", "FK_NU_AUTO_MODCABE_DETA", "NU_AUTO_MODCABE_DETA", False, False)
    Result = CrearIndice("IN_DETALLE", "FK_NU_AUTO_ARTI_DETA", "NU_AUTO_ARTI_DETA", False, False)
    Result = CrearIndice("IN_DETALLE", "PK_IN_DETALLE", "NU_AUTO_DETA", True)
    Result = CrearIndice("IN_DOCUMENTO", "PK_IN_DOCUMENTO", "NU_AUTO_DOCU", True)
    Result = CrearIndice("IN_ENCABEZADO", "FK_CD_CODI_TERC_ENCA", "CD_CODI_TERC_ENCA", False, False)
    Result = CrearIndice("IN_ENCABEZADO", "FK_NU_AUTO_COMP_ENCA", "NU_AUTO_COMP_ENCA", False, False)
    Result = CrearIndice("IN_ENCABEZADO", "PK_IN_ENCABEZADO", "NU_AUTO_ENCA", True)
    Result = CrearIndice("IN_FORMAFARMACEUTICA", "PK_NU_AUTO_FOFA", "NU_AUTO_FOFA", True)
    Result = CrearIndice("IN_FORMAFARMACEUTICA", "LL_IN_FORMAFARMACEUTICA", "TX_CODI_FOFA", False, True)
    Result = CrearIndice("IN_KARDEX", "FK_NU_AUTO_ARTI_MOVI", "NU_AUTO_ARTI_KARD", False, False)
    Result = CrearIndice("IN_KARDEX", "FK_NU_AUTO_ORGCABE_KARD", "NU_AUTO_ORGCABE_KARD", False, False)
    Result = CrearIndice("IN_KARDEX", "FK_NU_AUTO_MODCABE_KARD", "NU_AUTO_MODCABE_KARD", False, False)
    Result = CrearIndice("IN_KARDEX", "PK_IN_KARDEX", "NU_AUTO_KARD", True)
    Result = CrearIndice("IN_KARDEXLOTE", "PK_IN_KARDEXLOTE", "NU_AUTO_KLOT", True)
    Result = CrearIndice("IN_LIMITES_BODE_ARTI", "PK_IN_LIMITES_BODE_ARTI", "NU_AUTO_BODE_LIBA, NU_AUTO_ARTI_LIBA", True)
    Result = CrearIndice("IN_LISTAPRECIO", "PK_IN_LISTAPRECIO", "NU_AUTO_LIPR", True)
    Result = CrearIndice("IN_LOTE", "PK_IN_LOTE", "NU_AUTO_LOTE", True)
    Result = CrearIndice("IN_R_BODE_ARTI_UNDVENTA", "PK_IN_R_BODE_ARTI_UNDVENTA", "NU_AUTO_BODE_RBAU, NU_AUTO_ARTI_RBAU", True)
    Result = CrearIndice("IN_R_LIST_ARTI", "PK_IN_R_LIST_ARTI", "NU_AUTO_LIPR_RLIA, NU_AUTO_ARTI_RLIA, FE_DESDE_RLIA", True)
    Result = CrearIndice("IN_R_PERFIL_BODEGA", "PK_R_PERFIL_BODEGA", "NU_AUTO_BODE_RPEB, NU_AUTO_PERF_RPEB", True)
    Result = CrearIndice("IN_UNDVENTA", "PK_IN_UNDVENTA", "NU_AUTO_UNVE", True)
    Result = CrearIndice("IN_UNDVENTA", "LL_IN_UNDVENTA", "TX_CODI_UNVE", False, True)
    Result = CrearIndice("OPCION", "PK_OPCION", "NU_AUTO_OPCI", True)
    Result = CrearIndice("PERFIL", "PK_PERFIL", "NU_AUTO_PERF", True)
    Result = CrearIndice("PERMISO", "PK_PERMISO", "NU_AUTO_PERF_PERM, NU_AUTO_OPCI_PERM", True)
    Result = CrearIndice("USUARIO", "PK_USUARIOS", "NU_AUTO_USUA", True)
    
    Result = CrearRelacion("IN_R_PERFIL_BODEGAIN_BODEGA", "NU_AUTO_BODE_RPEB", "NU_AUTO_BODE", "IN_R_PERFIL_BODEGA", "IN_BODEGA")
    Result = CrearRelacion("IN_DETALLEIN_ENCABEZADO", "NU_AUTO_ORGCABE_DETA", "NU_AUTO_ENCA", "IN_DETALLE", "IN_ENCABEZADO")
    Result = CrearRelacion("IN_KARDEXIN_ENCABEZADO", "NU_AUTO_MODCABE_KARD", "NU_AUTO_ENCA", "IN_KARDEX", "IN_ENCABEZADO")
    Result = CrearRelacion("IN_ENCABEZADOIN_BODEGA", "NU_AUTO_BODEORG_ENCA", "NU_AUTO_BODE", "IN_ENCABEZADO", "IN_BODEGA")
    Result = CrearRelacion("IN_DETALLEIN_BODEGA", "NU_AUTO_BODE_DETA", "NU_AUTO_BODE", "IN_DETALLE", "IN_BODEGA")
    Result = CrearRelacion("IN_R_BODE_DOCUIN_DOCUMENTO", "NU_AUTO_DOCU_RBODU", "NU_AUTO_DOCU", "IN_R_BODE_DOCU", "IN_DOCUMENTO")
    Result = CrearRelacion("IN_KARDEXIN_ENCABEZADO_1", "NU_AUTO_ORGCABE_KARD", "NU_AUTO_ENCA", "IN_KARDEX", "IN_ENCABEZADO")
    Result = CrearRelacion("IN_R_BODE_ARTI_UNDVENTAIN_BODEGA", "NU_AUTO_BODE_RBAU", "NU_AUTO_BODE", "IN_R_BODE_ARTI_UNDVENTA", "IN_BODEGA")
    Result = CrearRelacion("IN_LIMITES_BODE_ARTIIN_BODEGA", "NU_AUTO_BODE_LIBA", "NU_AUTO_BODE", "IN_LIMITES_BODE_ARTI", "IN_BODEGA")
    Result = CrearRelacion("IN_R_BODE_DOCUIN_BODEGA", "NU_AUTO_BODE_RBODU", "NU_AUTO_BODE", "IN_R_BODE_DOCU", "IN_BODEGA")
    Result = CrearRelacion("IN_R_LIST_ARTIIN_LISTAPRECIO", "NU_AUTO_LIPR_RLIA", "NU_AUTO_LIPR", "IN_R_LIST_ARTI", "IN_LISTAPRECIO")
    Result = CrearRelacion("IN_R_BODE_ARTI_UNDVENTAIN_UNDVENTA", "NU_AUTO_UNVE_RBAU", "NU_AUTO_UNVE", "IN_R_BODE_ARTI_UNDVENTA", "IN_UNDVENTA")
    Result = CrearRelacion("IN_KARDEXLOTEIN_KARDEX", "NU_AUTO_KARD_KLOT", "NU_AUTO_KARD", "IN_KARDEXLOTE", "IN_KARDEX")
    Result = CrearRelacion("IN_KARDEXLOTEIN_LOTE", "NU_AUTO_LOTE_KLOT", "NU_AUTO_LOTE", "IN_KARDEXLOTE", "IN_LOTE")
    Result = CrearRelacion("IN_KARDEXIN_BODEGA", "NU_AUTO_BODE_KARD", "NU_AUTO_BODE", "IN_KARDEX", "IN_BODEGA")
    Result = CrearRelacion("IN_DETALLEIN_UNDVENTA", "NU_AUTO_UNVE_DETA", "NU_AUTO_UNVE", "IN_DETALLE", "IN_UNDVENTA")
    Result = CrearRelacion("IN_COMPROBANTEIN_DOCUMENTO", "NU_AUTO_DOCU_COMP", "NU_AUTO_DOCU", "IN_COMPROBANTE", "IN_DOCUMENTO")
    Result = CrearRelacion("IN_ENCABEZADOIN_COMPROBANTE", "NU_AUTO_COMP_ENCA", "NU_AUTO_COMP", "IN_ENCABEZADO", "IN_COMPROBANTE")
    Result = CrearRelacion("IN_DETALLEIN_ENCABEZADO_1", "NU_AUTO_MODCABE_DETA", "NU_AUTO_ENCA", "IN_DETALLE", "IN_ENCABEZADO")
    Result = CrearRelacion("CONEXIONUSUARIO", "NU_AUTO_USUA_CONE", "NU_AUTO_USUA", "CONEXION", "USUARIO")
    Result = CrearRelacion("IN_KARDEXLOTEIN_BODEGA", "NU_AUTO_BODE_KLOT", "NU_AUTO_BODE", "IN_KARDEXLOTE", "IN_BODEGA")
    Result = CrearRelacion("IN_CODIGOBARARTICULO", "NU_AUTO_ARTI_COBA", "NU_AUTO_ARTI", "IN_CODIGOBAR", "ARTICULO")
    Result = CrearRelacion("IN_DETALLEARTICULO", "NU_AUTO_ARTI_DETA", "NU_AUTO_ARTI", "IN_DETALLE", "ARTICULO")
    Result = CrearRelacion("IN_KARDEXARTICULO", "NU_AUTO_ARTI_KARD", "NU_AUTO_ARTI", "IN_KARDEX", "ARTICULO")
    Result = CrearRelacion("IN_LIMITES_BODE_ARTIARTICULO", "NU_AUTO_ARTI_LIBA", "NU_AUTO_ARTI", "IN_LIMITES_BODE_ARTI", "ARTICULO")
    Result = CrearRelacion("IN_LOTEARTICULO", "NU_AUTO_ARTI_LOTE", "NU_AUTO_ARTI", "IN_LOTE", "ARTICULO")
    Result = CrearRelacion("IN_R_BODE_ARTI_UNDVENTAARTICULO", "NU_AUTO_ARTI_RBAU", "NU_AUTO_ARTI", "IN_R_BODE_ARTI_UNDVENTA", "ARTICULO")
    Result = CrearRelacion("IN_R_LIST_ARTIARTICULO", "NU_AUTO_ARTI_RLIA", "NU_AUTO_ARTI", "IN_R_LIST_ARTI", "ARTICULO")
    Result = CrearRelacion("IN_ENCABEZADOIN_AUTORIZACION", "NU_AUTO_AUTO_ENCA", "NU_AUTO_AUTO", "IN_ENCABEZADO", "IN_AUTORIZACION")
    Result = CrearRelacion("IN_R_BODE_COMPIN_BODEGA", "NU_AUTO_BODE_RBOCO", "NU_AUTO_BODE", "IN_R_BODE_COMP", "IN_BODEGA")
    Result = CrearRelacion("IN_R_BODE_COMPIN_COMPROBANTE", "NU_AUTO_COMP_RBOCO", "NU_AUTO_COMP", "IN_R_BODE_COMP", "IN_COMPROBANTE")
    Result = CrearRelacion("ARTICULOIN_FORMAFARMACEUTICA", "NU_AUTO_FOFA_ARTI", "NU_AUTO_FOFA", "ARTICULO", "IN_FORMAFARMACEUTICA")
    Result = CrearRelacion("ARTICULOIN_UNDVENTA", "NU_AUTO_UNVE_ARTI", "NU_AUTO_UNVE", "ARTICULO", "IN_UNDVENTA")
    Result = CrearRelacion("IN_R_PERFIL_BODEGAPERFIL", "NU_AUTO_PERF_RPEB", "NU_AUTO_PERF", "IN_R_PERFIL_BODEGA", "PERFIL")
    Result = CrearRelacion("USUARIOPERFIL", "NU_AUTO_PERF_USUA", "NU_AUTO_PERF", "USUARIO", "PERFIL")
    Result = CrearRelacion("IN_ENCABEZADOUSUARIO", "NU_AUTO_USUA_ENCA", "NU_AUTO_USUA", "IN_ENCABEZADO", "USUARIO")

    Result = CrearIndice("IN_FORMAFARMACEUTICA", "LL_IN_FORMAFARMACEUTICA", "TX_CODI_FOFA", False, True)
    Result = CrearIndice("IN_UNDVENTA", "LL_IN_UNDVENTA", "TX_CODI_UNVE", False, True)
    Result = CrearIndice("PERFIL", "LL_PERFIL", "TX_CODI_PERF", False, True)
    Result = CrearIndice("USUARIO", "LL_USUARIOS", "TX_IDEN_USUA", False, True)

'//////////////////
    Result = CrearIndice("CONEXION", "FK_NU_AUTO_USUA_CONE", "NU_AUTO_USUA_CONE", False, False)
    Result = CrearIndice("CONEXION", "PK_CONEXIONES", "NU_AUTO_CONE", True)
'//////////////////

    BD(BDCurCon).Execute ("INSERT INTO IN_DOCUMENTO (NU_AUTO_DOCU, TX_CODI_DOCU, TX_NOMB_DOCU, TX_AFEC_DOCU, TX_INTE_DOCU, NU_AUTO_DOCUDEPE_DOCU, NU_AUTO_DOCUREVE_DOCU,TX_TIPOVAL_DOCU, TX_INDE_DOCU) VALUES ('1','COTIZA','SOLICITUD COTIZACION','N','N','0','20','C','S')")
    BD(BDCurCon).Execute ("INSERT INTO IN_DOCUMENTO (NU_AUTO_DOCU, TX_CODI_DOCU, TX_NOMB_DOCU, TX_AFEC_DOCU, TX_INTE_DOCU, NU_AUTO_DOCUDEPE_DOCU, NU_AUTO_DOCUREVE_DOCU,TX_TIPOVAL_DOCU, TX_INDE_DOCU) VALUES ('2','ORDEN','ORDEN COMPRA','N','N','1','18','C','S')")
    BD(BDCurCon).Execute ("INSERT INTO IN_DOCUMENTO (NU_AUTO_DOCU, TX_CODI_DOCU, TX_NOMB_DOCU, TX_AFEC_DOCU, TX_INTE_DOCU, NU_AUTO_DOCUDEPE_DOCU, NU_AUTO_DOCUREVE_DOCU,TX_TIPOVAL_DOCU, TX_INDE_DOCU) VALUES ('3','ENTRA','ENTRADA POR COMPRA','S','N','2','13','C','S')")
    BD(BDCurCon).Execute ("INSERT INTO IN_DOCUMENTO (NU_AUTO_DOCU, TX_CODI_DOCU, TX_NOMB_DOCU, TX_AFEC_DOCU, TX_INTE_DOCU, NU_AUTO_DOCUDEPE_DOCU, NU_AUTO_DOCUREVE_DOCU,TX_TIPOVAL_DOCU, TX_INDE_DOCU) VALUES ('4','DEVENT','DEVOLUCION ENTRADA','S','N','3','0','C','N')")
    BD(BDCurCon).Execute ("INSERT INTO IN_DOCUMENTO (NU_AUTO_DOCU, TX_CODI_DOCU, TX_NOMB_DOCU, TX_AFEC_DOCU, TX_INTE_DOCU, NU_AUTO_DOCUDEPE_DOCU, NU_AUTO_DOCUREVE_DOCU,TX_TIPOVAL_DOCU, TX_INDE_DOCU) VALUES ('5','REQUE','REQUISICION DE MERCANCIA','N','S','0','21','C','N')")
    BD(BDCurCon).Execute ("INSERT INTO IN_DOCUMENTO (NU_AUTO_DOCU, TX_CODI_DOCU, TX_NOMB_DOCU, TX_AFEC_DOCU, TX_INTE_DOCU, NU_AUTO_DOCUDEPE_DOCU, NU_AUTO_DOCUREVE_DOCU,TX_TIPOVAL_DOCU, TX_INDE_DOCU) VALUES ('6','DESPA','DESPACHO MERCANCIA','S','S','5','16','C','S')")
    BD(BDCurCon).Execute ("INSERT INTO IN_DOCUMENTO (NU_AUTO_DOCU, TX_CODI_DOCU, TX_NOMB_DOCU, TX_AFEC_DOCU, TX_INTE_DOCU, NU_AUTO_DOCUDEPE_DOCU, NU_AUTO_DOCUREVE_DOCU,TX_TIPOVAL_DOCU, TX_INDE_DOCU) VALUES ('8','BAJA','BAJA CONSUMO','S','N','0','14','C','N')")
    BD(BDCurCon).Execute ("INSERT INTO IN_DOCUMENTO (NU_AUTO_DOCU, TX_CODI_DOCU, TX_NOMB_DOCU, TX_AFEC_DOCU, TX_INTE_DOCU, NU_AUTO_DOCUDEPE_DOCU, NU_AUTO_DOCUREVE_DOCU,TX_TIPOVAL_DOCU, TX_INDE_DOCU) VALUES ('9','DEVBJ','DEVOLUCION BAJA CONSUMO','S','N','8','0','C','N')")
    BD(BDCurCon).Execute ("INSERT INTO IN_DOCUMENTO (NU_AUTO_DOCU, TX_CODI_DOCU, TX_NOMB_DOCU, TX_AFEC_DOCU, TX_INTE_DOCU, NU_AUTO_DOCUDEPE_DOCU, NU_AUTO_DOCUREVE_DOCU,TX_TIPOVAL_DOCU, TX_INDE_DOCU) VALUES ('10','TRASL','TRASLADO','S','S','0','23','C','N')")
    BD(BDCurCon).Execute ("INSERT INTO IN_DOCUMENTO (NU_AUTO_DOCU, TX_CODI_DOCU, TX_NOMB_DOCU, TX_AFEC_DOCU, TX_INTE_DOCU, NU_AUTO_DOCUDEPE_DOCU, NU_AUTO_DOCUREVE_DOCU,TX_TIPOVAL_DOCU, TX_INDE_DOCU) VALUES ('11','FACTR','FACTURA DE VENTA','S','N','8','15','V','S')")
    BD(BDCurCon).Execute ("INSERT INTO IN_DOCUMENTO (NU_AUTO_DOCU, TX_CODI_DOCU, TX_NOMB_DOCU, TX_AFEC_DOCU, TX_INTE_DOCU, NU_AUTO_DOCUDEPE_DOCU, NU_AUTO_DOCUREVE_DOCU,TX_TIPOVAL_DOCU, TX_INDE_DOCU) VALUES ('12','DEVFAC','DEVOLUCION FACTURA','S','N','11','0','V','N')")
    BD(BDCurCon).Execute ("INSERT INTO IN_DOCUMENTO (NU_AUTO_DOCU, TX_CODI_DOCU, TX_NOMB_DOCU, TX_AFEC_DOCU, TX_INTE_DOCU, NU_AUTO_DOCUDEPE_DOCU, NU_AUTO_DOCUREVE_DOCU,TX_TIPOVAL_DOCU, TX_INDE_DOCU) VALUES ('13','ANLENT','ANULACION ENTRADA','S','N','3','0','C','N')")
    BD(BDCurCon).Execute ("INSERT INTO IN_DOCUMENTO (NU_AUTO_DOCU, TX_CODI_DOCU, TX_NOMB_DOCU, TX_AFEC_DOCU, TX_INTE_DOCU, NU_AUTO_DOCUDEPE_DOCU, NU_AUTO_DOCUREVE_DOCU,TX_TIPOVAL_DOCU, TX_INDE_DOCU) VALUES ('14','ANLBJ','ANULACION BAJA','S','N','8','0','C','N')")
    BD(BDCurCon).Execute ("INSERT INTO IN_DOCUMENTO (NU_AUTO_DOCU, TX_CODI_DOCU, TX_NOMB_DOCU, TX_AFEC_DOCU, TX_INTE_DOCU, NU_AUTO_DOCUDEPE_DOCU, NU_AUTO_DOCUREVE_DOCU,TX_TIPOVAL_DOCU, TX_INDE_DOCU) VALUES ('15','ANLFAC','ANULACION FACTURA','S','N','11','0','V','N')")
    BD(BDCurCon).Execute ("INSERT INTO IN_DOCUMENTO (NU_AUTO_DOCU, TX_CODI_DOCU, TX_NOMB_DOCU, TX_AFEC_DOCU, TX_INTE_DOCU, NU_AUTO_DOCUDEPE_DOCU, NU_AUTO_DOCUREVE_DOCU,TX_TIPOVAL_DOCU, TX_INDE_DOCU) VALUES ('16','ANLDES','ANULACION DESPACHO','S','S','6','0','C','N')")
    BD(BDCurCon).Execute ("INSERT INTO IN_DOCUMENTO (NU_AUTO_DOCU, TX_CODI_DOCU, TX_NOMB_DOCU, TX_AFEC_DOCU, TX_INTE_DOCU, NU_AUTO_DOCUDEPE_DOCU, NU_AUTO_DOCUREVE_DOCU,TX_TIPOVAL_DOCU, TX_INDE_DOCU) VALUES ('17','DEVDES','DEVOLUCION DESPACHO','S','S','6','0','C','N')")
    BD(BDCurCon).Execute ("INSERT INTO IN_DOCUMENTO (NU_AUTO_DOCU, TX_CODI_DOCU, TX_NOMB_DOCU, TX_AFEC_DOCU, TX_INTE_DOCU, NU_AUTO_DOCUDEPE_DOCU, NU_AUTO_DOCUREVE_DOCU,TX_TIPOVAL_DOCU, TX_INDE_DOCU) VALUES ('18','ANLORD','ANULACION ORDEN DE COMPRA','N','N','2','0','C','N')")
    BD(BDCurCon).Execute ("INSERT INTO IN_DOCUMENTO (NU_AUTO_DOCU, TX_CODI_DOCU, TX_NOMB_DOCU, TX_AFEC_DOCU, TX_INTE_DOCU, NU_AUTO_DOCUDEPE_DOCU, NU_AUTO_DOCUREVE_DOCU,TX_TIPOVAL_DOCU, TX_INDE_DOCU) VALUES ('19','SALIDA','SALIDA DE MERCANCIA','S','N','0','22','C','N')")
    BD(BDCurCon).Execute ("INSERT INTO IN_DOCUMENTO (NU_AUTO_DOCU, TX_CODI_DOCU, TX_NOMB_DOCU, TX_AFEC_DOCU, TX_INTE_DOCU, NU_AUTO_DOCUDEPE_DOCU, NU_AUTO_DOCUREVE_DOCU,TX_TIPOVAL_DOCU, TX_INDE_DOCU) VALUES ('20','ANLSOL','ANULACION SOLICITUD COTIZACION','N','N','1','0','C','N')")
    BD(BDCurCon).Execute ("INSERT INTO IN_DOCUMENTO (NU_AUTO_DOCU, TX_CODI_DOCU, TX_NOMB_DOCU, TX_AFEC_DOCU, TX_INTE_DOCU, NU_AUTO_DOCUDEPE_DOCU, NU_AUTO_DOCUREVE_DOCU,TX_TIPOVAL_DOCU, TX_INDE_DOCU) VALUES ('21','ANLREQ','ANULACION REQUISICION','N','S','5','0','C','N')")
    BD(BDCurCon).Execute ("INSERT INTO IN_DOCUMENTO (NU_AUTO_DOCU, TX_CODI_DOCU, TX_NOMB_DOCU, TX_AFEC_DOCU, TX_INTE_DOCU, NU_AUTO_DOCUDEPE_DOCU, NU_AUTO_DOCUREVE_DOCU,TX_TIPOVAL_DOCU, TX_INDE_DOCU) VALUES ('22','ANLSAL','ANULACION SALIDA','S','N','19','0','C','N')")
    BD(BDCurCon).Execute ("INSERT INTO IN_DOCUMENTO (NU_AUTO_DOCU, TX_CODI_DOCU, TX_NOMB_DOCU, TX_AFEC_DOCU, TX_INTE_DOCU, NU_AUTO_DOCUDEPE_DOCU, NU_AUTO_DOCUREVE_DOCU,TX_TIPOVAL_DOCU, TX_INDE_DOCU) VALUES ('23','ANLTRA','ANULACION TRASLADO','S','S','10','0','C','N')")
    BD(BDCurCon).Execute ("INSERT INTO IN_DOCUMENTO (NU_AUTO_DOCU, TX_CODI_DOCU, TX_NOMB_DOCU, TX_AFEC_DOCU, TX_INTE_DOCU, NU_AUTO_DOCUDEPE_DOCU, NU_AUTO_DOCUREVE_DOCU,TX_TIPOVAL_DOCU, TX_INDE_DOCU) VALUES ('24','DEVSAL','DEVOLUCION SALIDA','S','N','19','0','C','N')")
'5_1_0
    BD(BDCurCon).Execute ("INSERT INTO IN_DOCUMENTO (NU_AUTO_DOCU, TX_CODI_DOCU, TX_NOMB_DOCU, TX_AFEC_DOCU, TX_INTE_DOCU, NU_AUTO_DOCUDEPE_DOCU, NU_AUTO_DOCUREVE_DOCU,TX_TIPOVAL_DOCU, TX_INDE_DOCU) VALUES ('26','APRDON','ENTRADA APROVECHA/DONACION','S','N','2','27','C','S')")
    BD(BDCurCon).Execute ("INSERT INTO IN_DOCUMENTO (NU_AUTO_DOCU, TX_CODI_DOCU, TX_NOMB_DOCU, TX_AFEC_DOCU, TX_INTE_DOCU, NU_AUTO_DOCUDEPE_DOCU, NU_AUTO_DOCUREVE_DOCU,TX_TIPOVAL_DOCU, TX_INDE_DOCU) VALUES ('27','ANLAPR','ANULACION ENTRADA APROVECHA/DONACION','S','N','26','0','C','N')")
    BD(BDCurCon).Execute ("INSERT INTO IN_DOCUMENTO (NU_AUTO_DOCU, TX_CODI_DOCU, TX_NOMB_DOCU, TX_AFEC_DOCU, TX_INTE_DOCU, NU_AUTO_DOCUDEPE_DOCU, NU_AUTO_DOCUREVE_DOCU,TX_TIPOVAL_DOCU, TX_INDE_DOCU) VALUES ('28','DEVAPR','DEVOLUCION ENTRADA APROVECHA/DONACION','S','N','26','0','C','N')")
    BD(BDCurCon).Execute ("INSERT INTO IN_DOCUMENTO (NU_AUTO_DOCU, TX_CODI_DOCU, TX_NOMB_DOCU, TX_AFEC_DOCU, TX_INTE_DOCU, NU_AUTO_DOCUDEPE_DOCU, NU_AUTO_DOCUREVE_DOCU,TX_TIPOVAL_DOCU, TX_INDE_DOCU) VALUES ('29','LISTAS','COTIZACION/LISTA DE PRECIOS','N','N','0','30','C','S')")
    BD(BDCurCon).Execute ("INSERT INTO IN_DOCUMENTO (NU_AUTO_DOCU, TX_CODI_DOCU, TX_NOMB_DOCU, TX_AFEC_DOCU, TX_INTE_DOCU, NU_AUTO_DOCUDEPE_DOCU, NU_AUTO_DOCUREVE_DOCU,TX_TIPOVAL_DOCU, TX_INDE_DOCU) VALUES ('30','ANLSOL','ANULACION COTIZACION/LISTA DE PRECIOS','N','N','1','0','C','N')")
    
    BD(BDCurCon).Execute ("INSERT INTO FORMULARIO (TX_DESC_FORM,TX_FORM_FORM,TX_TIPO_FORM) VALUES ('SEGURIDAD','','')")
    BD(BDCurCon).Execute ("INSERT INTO FORMULARIO (TX_DESC_FORM,TX_FORM_FORM,TX_TIPO_FORM) VALUES ('OPCIONES','FrmOpciones','FRM')")
    BD(BDCurCon).Execute ("INSERT INTO FORMULARIO (TX_DESC_FORM,TX_FORM_FORM,TX_TIPO_FORM) VALUES ('PERFILES','FrmPerfiles','FRM')")
    BD(BDCurCon).Execute ("INSERT INTO FORMULARIO (TX_DESC_FORM,TX_FORM_FORM,TX_TIPO_FORM) VALUES ('PERMISOS DE PERFIL','FrmPermisos','FRM')")
    BD(BDCurCon).Execute ("INSERT INTO FORMULARIO (TX_DESC_FORM,TX_FORM_FORM,TX_TIPO_FORM) VALUES ('USUARIOS','FrmUsuario','FRM')")
    BD(BDCurCon).Execute ("INSERT INTO FORMULARIO (TX_DESC_FORM,TX_FORM_FORM,TX_TIPO_FORM) VALUES ('GRUPOS DE ARTICULO','FrmGrupo','FRM')")
    BD(BDCurCon).Execute ("INSERT INTO FORMULARIO (TX_DESC_FORM,TX_FORM_FORM,TX_TIPO_FORM) VALUES ('SOLICITUD DE COTIZACION','FrmGENERAL','1')")
    BD(BDCurCon).Execute ("INSERT INTO FORMULARIO (TX_DESC_FORM,TX_FORM_FORM,TX_TIPO_FORM) VALUES ('ORDEN DE COMPRA','FrmGENERAL','2')")
    BD(BDCurCon).Execute ("INSERT INTO FORMULARIO (TX_DESC_FORM,TX_FORM_FORM,TX_TIPO_FORM) VALUES ('ENTRADA DE ALMACEN','FrmGENERAL','3')")
    BD(BDCurCon).Execute ("INSERT INTO FORMULARIO (TX_DESC_FORM,TX_FORM_FORM,TX_TIPO_FORM) VALUES ('EXISTENCIA','frmPrmREPOSAL','FRM')")
    BD(BDCurCon).Execute ("INSERT INTO FORMULARIO (TX_DESC_FORM,TX_FORM_FORM,TX_TIPO_FORM) VALUES ('KARDEX','frmPrmREPOKAR','FRM')")
    BD(BDCurCon).Execute ("INSERT INTO FORMULARIO (TX_DESC_FORM,TX_FORM_FORM,TX_TIPO_FORM) VALUES ('PARAMETROS DE LA APLICACI�N','FrmParam','FRM')")
    BD(BDCurCon).Execute ("INSERT INTO FORMULARIO (TX_DESC_FORM,TX_FORM_FORM,TX_TIPO_FORM) VALUES ('PERFILES POR BODEGA','FrmPerfXBod','FRM')")
    BD(BDCurCon).Execute ("INSERT INTO FORMULARIO (TX_DESC_FORM,TX_FORM_FORM,TX_TIPO_FORM) VALUES ('COMPROBANTES POR BODEGA','FrmComXBod','FRM')")
    BD(BDCurCon).Execute ("INSERT INTO FORMULARIO (TX_DESC_FORM,TX_FORM_FORM,TX_TIPO_FORM) VALUES ('LISTAS DE PRECIOS','FrmLisPre','FRM')")
    BD(BDCurCon).Execute ("INSERT INTO FORMULARIO (TX_DESC_FORM,TX_FORM_FORM,TX_TIPO_FORM) VALUES ('DESPACHO','FrmGENERAL','6')")
    BD(BDCurCon).Execute ("INSERT INTO FORMULARIO (TX_DESC_FORM,TX_FORM_FORM,TX_TIPO_FORM) VALUES ('REQUISICION','FrmGENERAL','5')")
    BD(BDCurCon).Execute ("INSERT INTO FORMULARIO (TX_DESC_FORM,TX_FORM_FORM,TX_TIPO_FORM) VALUES ('ARTICULO','FrmArtic','FRM')")
    BD(BDCurCon).Execute ("INSERT INTO FORMULARIO (TX_DESC_FORM,TX_FORM_FORM,TX_TIPO_FORM) VALUES ('USOS','FrmUsos','FRM')")
    BD(BDCurCon).Execute ("INSERT INTO FORMULARIO (TX_DESC_FORM,TX_FORM_FORM,TX_TIPO_FORM) VALUES ('ARTICULOS POR LISTA DE PRECIOS','FrmArticXLisPre','FRM')")
    BD(BDCurCon).Execute ("INSERT INTO FORMULARIO (TX_DESC_FORM,TX_FORM_FORM,TX_TIPO_FORM) VALUES ('BODEGAS','frmPrmBODEGA','FRM')")
    BD(BDCurCon).Execute ("INSERT INTO FORMULARIO (TX_DESC_FORM,TX_FORM_FORM,TX_TIPO_FORM) VALUES ('COMPROBANTE','frmPrmCOMPROBA','FRM')")
    BD(BDCurCon).Execute ("INSERT INTO FORMULARIO (TX_DESC_FORM,TX_FORM_FORM,TX_TIPO_FORM) VALUES ('UNIDADES','frmPrmUNIDADES','FRM')")
    BD(BDCurCon).Execute ("INSERT INTO FORMULARIO (TX_DESC_FORM,TX_FORM_FORM,TX_TIPO_FORM) VALUES ('UNIDADES X BODEGA','frmPrmUNDXBOD','FRM')")
    BD(BDCurCon).Execute ("INSERT INTO FORMULARIO (TX_DESC_FORM,TX_FORM_FORM,TX_TIPO_FORM) VALUES ('CODIGOS DE BARRA','frmPrmCDBARRA','FRM')")
    BD(BDCurCon).Execute ("INSERT INTO FORMULARIO (TX_DESC_FORM,TX_FORM_FORM,TX_TIPO_FORM) VALUES ('BAJA X CONSUMO','FrmGENERAL','8')")
    BD(BDCurCon).Execute ("INSERT INTO FORMULARIO (TX_DESC_FORM,TX_FORM_FORM,TX_TIPO_FORM) VALUES ('MAXIMOS, MINIMOS, REPOSICION','frmPrmMAXMIN','FRM')")
    BD(BDCurCon).Execute ("INSERT INTO FORMULARIO (TX_DESC_FORM,TX_FORM_FORM,TX_TIPO_FORM) VALUES ('DEVOLUCION ENTRADA','FrmGENERAL','4')")
    BD(BDCurCon).Execute ("INSERT INTO FORMULARIO (TX_DESC_FORM,TX_FORM_FORM,TX_TIPO_FORM) VALUES ('DEVOLUCION BAJA X CONSUMO','FrmGENERAL','9')")
    BD(BDCurCon).Execute ("INSERT INTO FORMULARIO (TX_DESC_FORM,TX_FORM_FORM,TX_TIPO_FORM) VALUES ('DEVOLUCION DESPACHO','FrmGENERAL','17')")
    BD(BDCurCon).Execute ("INSERT INTO FORMULARIO (TX_DESC_FORM,TX_FORM_FORM,TX_TIPO_FORM) VALUES ('ANULACION DESPACHO','FrmGENERAL','16')")
    BD(BDCurCon).Execute ("INSERT INTO FORMULARIO (TX_DESC_FORM,TX_FORM_FORM,TX_TIPO_FORM) VALUES ('TRASLADOS ENTRE BODEGAS','FrmGENERAL','10')")
    BD(BDCurCon).Execute ("INSERT INTO FORMULARIO (TX_DESC_FORM,TX_FORM_FORM,TX_TIPO_FORM) VALUES ('SALIDA DE MERCANCIA','FrmGENERAL','19')")
    BD(BDCurCon).Execute ("INSERT INTO FORMULARIO (TX_DESC_FORM,TX_FORM_FORM,TX_TIPO_FORM) VALUES ('AUTORIZACIONES','FrmAUTORIZA','FRM')")
    BD(BDCurCon).Execute ("INSERT INTO FORMULARIO (TX_DESC_FORM,TX_FORM_FORM,TX_TIPO_FORM) VALUES ('FORMA FARMACEUTICA','FrmFormaFarmaceutica','FRM')")
    BD(BDCurCon).Execute ("INSERT INTO FORMULARIO (TX_DESC_FORM,TX_FORM_FORM,TX_TIPO_FORM) VALUES ('DOCUMENTOS','frmPrmREPODOC','FRM')")
    BD(BDCurCon).Execute ("INSERT INTO FORMULARIO (TX_DESC_FORM,TX_FORM_FORM,TX_TIPO_FORM) VALUES ('ARTICULOS X DOCUMENTO','frmPrmREPOART','FRM')")
    BD(BDCurCon).Execute ("INSERT INTO FORMULARIO (TX_DESC_FORM,TX_FORM_FORM,TX_TIPO_FORM) VALUES ('ARTICULOS','frmPrmREPOLST','FRM')")
    BD(BDCurCon).Execute ("INSERT INTO FORMULARIO (TX_DESC_FORM,TX_FORM_FORM,TX_TIPO_FORM) VALUES ('TERCEROS','FrmTerceros','FRM')")
    BD(BDCurCon).Execute ("INSERT INTO FORMULARIO (TX_DESC_FORM,TX_FORM_FORM,TX_TIPO_FORM) VALUES ('ANULACION SOLICITUD COTIZACION','FrmGENERAL',20)")
    BD(BDCurCon).Execute ("INSERT INTO FORMULARIO (TX_DESC_FORM,TX_FORM_FORM,TX_TIPO_FORM) VALUES ('ANULACION ORDEN DE COMPRA','FrmGENERAL',18)")
    BD(BDCurCon).Execute ("INSERT INTO FORMULARIO (TX_DESC_FORM,TX_FORM_FORM,TX_TIPO_FORM) VALUES ('ANULACION ENTRADA','FrmGENERAL',13)")
    BD(BDCurCon).Execute ("INSERT INTO FORMULARIO (TX_DESC_FORM,TX_FORM_FORM,TX_TIPO_FORM) VALUES ('ANULACION REQUISICION','FrmGENERAL',21)")
    BD(BDCurCon).Execute ("INSERT INTO FORMULARIO (TX_DESC_FORM,TX_FORM_FORM,TX_TIPO_FORM) VALUES ('FACTURA DE VENTA','FrmGENERAL',11)")
    BD(BDCurCon).Execute ("INSERT INTO FORMULARIO (TX_DESC_FORM,TX_FORM_FORM,TX_TIPO_FORM) VALUES ('ANULACION TRASLADO','FrmGENERAL',23)")
    BD(BDCurCon).Execute ("INSERT INTO FORMULARIO (TX_DESC_FORM,TX_FORM_FORM,TX_TIPO_FORM) VALUES ('ANULACION BAJA','FrmGENERAL',14)")
    BD(BDCurCon).Execute ("INSERT INTO FORMULARIO (TX_DESC_FORM,TX_FORM_FORM,TX_TIPO_FORM) VALUES ('ANULACION SALIDA','FrmGENERAL',22)")
    BD(BDCurCon).Execute ("INSERT INTO FORMULARIO (TX_DESC_FORM,TX_FORM_FORM,TX_TIPO_FORM) VALUES ('ANULACION FACTURA','FrmGENERAL',15)")
'5_1_0
    BD(BDCurCon).Execute ("INSERT INTO FORMULARIO (TX_DESC_FORM,TX_FORM_FORM,TX_TIPO_FORM) VALUES ('COMPRA','FrmFASECOMPRA','FRM')")
    BD(BDCurCon).Execute ("INSERT INTO FORMULARIO (TX_DESC_FORM,TX_FORM_FORM,TX_TIPO_FORM) VALUES ('APROVECHAMIENTO/DONACION','FrmGENERAL',26)")
    BD(BDCurCon).Execute ("INSERT INTO FORMULARIO (TX_DESC_FORM,TX_FORM_FORM,TX_TIPO_FORM) VALUES ('INVENTARIO FISICO','FrmFISICO','FRM')")
    BD(BDCurCon).Execute ("INSERT INTO FORMULARIO (TX_DESC_FORM,TX_FORM_FORM,TX_TIPO_FORM) VALUES ('AJUSTES X INFLACION','FrmINFLA','FRM')")
    BD(BDCurCon).Execute ("INSERT INTO FORMULARIO (TX_DESC_FORM,TX_FORM_FORM,TX_TIPO_FORM) VALUES ('OTROS INFORMES','FrmOTREPORTE','FRM')")
    BD(BDCurCon).Execute ("INSERT INTO FORMULARIO (TX_DESC_FORM,TX_FORM_FORM,TX_TIPO_FORM) VALUES ('COTIZACION/LISTA PRECIOS PROVEEDOR','FrmGENERAL',29)")
    BD(BDCurCon).Execute ("INSERT INTO FORMULARIO (TX_DESC_FORM,TX_FORM_FORM,TX_TIPO_FORM) VALUES ('CRITERIOS DE COMPRA','FrmCriterios','FRM')")
'FrmOTREPORTE

    BD(BDCurCon).Execute ("INSERT INTO PARAMETROS_INVE (" & _
"FE_CIER_APLI, ID_CONT_APLI, ID_PRES_APLI, ID_PRED_APLI, ID_INCO_APLI, ID_SEPU_APLI, ID_PACI_APLI, ID_NOMI_APLI, ID_ACTI_APLI," & _
"ID_CXC_APLI, ID_CXP_APLI, ID_DEUS_APLI, CD_CXC_APLI, CD_CXP_APLI, CD_CAJA_APLI, CD_DECO_APLI, CD_DEVE_APLI, CD_DEAD_APLI," & _
"CD_FLET_APLI, CD_CONT_APLI, CD_ORDE_APLI, CD_ARFL_APLI, CD_FLIN_APLI, NU_NVERDB_PINV, ID_REST_PINV, ID_NOAU_PINV, ID_DECVAL_PINV," & _
"NU_DECVAL_PINV, ID_DECCANT_PINV, NU_DECCANT_PINV, ID_DECPORC_PINV, NU_DECPORC_PINV, ID_AUTORCO_PINV, ID_UTILID_PINV" & _
    ") VALUES ('31/12/2004','N','N','N','N','N','N','N','N','N','N','S','','','','','','','','','0','','','','','N','S','2','S','2','S','4','N','N')")
    
    BD(BDCurCon).Execute ("INSERT INTO PERFIL (NU_AUTO_EMPR_PERF,TX_CODI_PERF,TX_DESC_PERF) VALUES ('1','01','ADMINISTRADOR')")
    
    BD(BDCurCon).Execute ("INSERT INTO USUARIO (TX_IDEN_USUA,TX_PASS_USUA,TX_DESC_USUA,NU_AUTO_PERF_USUA) VALUES ('ADMINISTRADOR','ADMIN','CNT','1')")
    
    BD(BDCurCon).Execute ("INSERT INTO OPCION (TX_CODI_OPCI,TX_DESC_OPCI,TX_CODPADR_OPCI,NU_AUTO_FORM_OPCI) VALUES ('01','SEGURIDAD','CNT','1')")
    BD(BDCurCon).Execute ("INSERT INTO OPCION (TX_CODI_OPCI,TX_DESC_OPCI,TX_CODPADR_OPCI) VALUES ('02','BASE DE DATOS','CNT')")
    BD(BDCurCon).Execute ("INSERT INTO OPCION (TX_CODI_OPCI,TX_DESC_OPCI,TX_CODPADR_OPCI) VALUES ('03','MOVIMIENTOS','CNT')")
    BD(BDCurCon).Execute ("INSERT INTO OPCION (TX_CODI_OPCI,TX_DESC_OPCI,TX_CODPADR_OPCI) VALUES ('04','INFORMES','CNT')")
    BD(BDCurCon).Execute ("INSERT INTO OPCION (TX_CODI_OPCI,TX_DESC_OPCI,TX_CODPADR_OPCI) VALUES ('05','PROCESOS ESPECIALES','CNT')")
    BD(BDCurCon).Execute ("INSERT INTO OPCION (TX_CODI_OPCI,TX_DESC_OPCI,TX_CODPADR_OPCI,NU_AUTO_FORM_OPCI) VALUES ('0101','OPCIONES','01','2')")
    BD(BDCurCon).Execute ("INSERT INTO OPCION (TX_CODI_OPCI,TX_DESC_OPCI,TX_CODPADR_OPCI,NU_AUTO_FORM_OPCI) VALUES ('0102','PERFILES','01','3')")
    BD(BDCurCon).Execute ("INSERT INTO OPCION (TX_CODI_OPCI,TX_DESC_OPCI,TX_CODPADR_OPCI,NU_AUTO_FORM_OPCI) VALUES ('0103','PERMISOS DE PERFIL','01','4')")
    BD(BDCurCon).Execute ("INSERT INTO OPCION (TX_CODI_OPCI,TX_DESC_OPCI,TX_CODPADR_OPCI,NU_AUTO_FORM_OPCI) VALUES ('0104','USUARIOS','01','5')")
    BD(BDCurCon).Execute ("INSERT INTO OPCION (TX_CODI_OPCI,TX_DESC_OPCI,TX_CODPADR_OPCI,NU_AUTO_FORM_OPCI) VALUES ('0105','PERFILES POR BODEGA','01','13')")
    BD(BDCurCon).Execute ("INSERT INTO OPCION (TX_CODI_OPCI,TX_DESC_OPCI,TX_CODPADR_OPCI,NU_AUTO_FORM_OPCI) VALUES ('0203','GRUPOS DE ARTICULOS','02','6')")
    BD(BDCurCon).Execute ("INSERT INTO OPCION (TX_CODI_OPCI,TX_DESC_OPCI,TX_CODPADR_OPCI,NU_AUTO_FORM_OPCI) VALUES ('0205','FORMA FARMACEUTICA','02','35')")
    BD(BDCurCon).Execute ("INSERT INTO OPCION (TX_CODI_OPCI,TX_DESC_OPCI,TX_CODPADR_OPCI,NU_AUTO_FORM_OPCI) VALUES ('0206','USOS','02','19')")
    BD(BDCurCon).Execute ("INSERT INTO OPCION (TX_CODI_OPCI,TX_DESC_OPCI,TX_CODPADR_OPCI,NU_AUTO_FORM_OPCI) VALUES ('0209','TERCEROS','02','39')")
    BD(BDCurCon).Execute ("INSERT INTO OPCION (TX_CODI_OPCI,TX_DESC_OPCI,TX_CODPADR_OPCI,NU_AUTO_FORM_OPCI) VALUES ('0212','LISTAS DE PRECIOS','02','15')")
    BD(BDCurCon).Execute ("INSERT INTO OPCION (TX_CODI_OPCI,TX_DESC_OPCI,TX_CODPADR_OPCI,NU_AUTO_FORM_OPCI) VALUES ('0215','ARTICULOS','02','18')")
    BD(BDCurCon).Execute ("INSERT INTO OPCION (TX_CODI_OPCI,TX_DESC_OPCI,TX_CODPADR_OPCI,NU_AUTO_FORM_OPCI) VALUES ('0218','BODEGAS','02','21')")
    BD(BDCurCon).Execute ("INSERT INTO OPCION (TX_CODI_OPCI,TX_DESC_OPCI,TX_CODPADR_OPCI,NU_AUTO_FORM_OPCI) VALUES ('0221','COMPROBANTES','02','22')")
    BD(BDCurCon).Execute ("INSERT INTO OPCION (TX_CODI_OPCI,TX_DESC_OPCI,TX_CODPADR_OPCI,NU_AUTO_FORM_OPCI) VALUES ('0224','COMPROBANTES X BODEGA','02','14')")
    BD(BDCurCon).Execute ("INSERT INTO OPCION (TX_CODI_OPCI,TX_DESC_OPCI,TX_CODPADR_OPCI,NU_AUTO_FORM_OPCI) VALUES ('0227','ARTICULOS POR LISTA DE PRECIOS','02','20')")
    BD(BDCurCon).Execute ("INSERT INTO OPCION (TX_CODI_OPCI,TX_DESC_OPCI,TX_CODPADR_OPCI,NU_AUTO_FORM_OPCI) VALUES ('0230','UNIDADES','02','23')")
    BD(BDCurCon).Execute ("INSERT INTO OPCION (TX_CODI_OPCI,TX_DESC_OPCI,TX_CODPADR_OPCI,NU_AUTO_FORM_OPCI) VALUES ('0233','UNIDADES X BODEGA','02','24')")
    BD(BDCurCon).Execute ("INSERT INTO OPCION (TX_CODI_OPCI,TX_DESC_OPCI,TX_CODPADR_OPCI,NU_AUTO_FORM_OPCI) VALUES ('0236','CODIGOS DE BARRA','02','25')")
    BD(BDCurCon).Execute ("INSERT INTO OPCION (TX_CODI_OPCI,TX_DESC_OPCI,TX_CODPADR_OPCI,NU_AUTO_FORM_OPCI) VALUES ('0239','MAXIMOS* MINIMOS* REPOSICION','02','27')")
    BD(BDCurCon).Execute ("INSERT INTO OPCION (TX_CODI_OPCI,TX_DESC_OPCI,TX_CODPADR_OPCI,NU_AUTO_FORM_OPCI) VALUES ('0301','SOLICITUD DE COTIZACION','03','7')")
    BD(BDCurCon).Execute ("INSERT INTO OPCION (TX_CODI_OPCI,TX_DESC_OPCI,TX_CODPADR_OPCI,NU_AUTO_FORM_OPCI) VALUES ('0303','ORDEN DE COMPRA','03','8')")
    BD(BDCurCon).Execute ("INSERT INTO OPCION (TX_CODI_OPCI,TX_DESC_OPCI,TX_CODPADR_OPCI,NU_AUTO_FORM_OPCI) VALUES ('0304','ENTRADA DE MERCANCIA','03','9')")
    BD(BDCurCon).Execute ("INSERT INTO OPCION (TX_CODI_OPCI,TX_DESC_OPCI,TX_CODPADR_OPCI,NU_AUTO_FORM_OPCI) VALUES ('0305','DEVOLUCION ENTRADA','03','28')")
    BD(BDCurCon).Execute ("INSERT INTO OPCION (TX_CODI_OPCI,TX_DESC_OPCI,TX_CODPADR_OPCI,NU_AUTO_FORM_OPCI) VALUES ('0310','REQUISICI�N','03','17')")
    BD(BDCurCon).Execute ("INSERT INTO OPCION (TX_CODI_OPCI,TX_DESC_OPCI,TX_CODPADR_OPCI,NU_AUTO_FORM_OPCI) VALUES ('0311','DESPACHOS','03','16')")
    BD(BDCurCon).Execute ("INSERT INTO OPCION (TX_CODI_OPCI,TX_DESC_OPCI,TX_CODPADR_OPCI,NU_AUTO_FORM_OPCI) VALUES ('0312','DEVOLUCION DESPACHO','03','30')")
    BD(BDCurCon).Execute ("INSERT INTO OPCION (TX_CODI_OPCI,TX_DESC_OPCI,TX_CODPADR_OPCI,NU_AUTO_FORM_OPCI) VALUES ('0313','BAJA X CONSUMO','03','26')")
    BD(BDCurCon).Execute ("INSERT INTO OPCION (TX_CODI_OPCI,TX_DESC_OPCI,TX_CODPADR_OPCI,NU_AUTO_FORM_OPCI) VALUES ('0314','DEVOLUCION BAJA X CONSUMO','03','29')")
    BD(BDCurCon).Execute ("INSERT INTO OPCION (TX_CODI_OPCI,TX_DESC_OPCI,TX_CODPADR_OPCI,NU_AUTO_FORM_OPCI) VALUES ('0316','TRASLADOS ENTRE BODEGAS','03','32')")
    BD(BDCurCon).Execute ("INSERT INTO OPCION (TX_CODI_OPCI,TX_DESC_OPCI,TX_CODPADR_OPCI,NU_AUTO_FORM_OPCI) VALUES ('0320','SALIDA DE MERCANCIA','03','33')")
    BD(BDCurCon).Execute ("INSERT INTO OPCION (TX_CODI_OPCI,TX_DESC_OPCI,TX_CODPADR_OPCI,NU_AUTO_FORM_OPCI) VALUES ('0401','KARDEX','04','11')")
    BD(BDCurCon).Execute ("INSERT INTO OPCION (TX_CODI_OPCI,TX_DESC_OPCI,TX_CODPADR_OPCI,NU_AUTO_FORM_OPCI) VALUES ('0402','EXISTENCIA','04','10')")
    BD(BDCurCon).Execute ("INSERT INTO OPCION (TX_CODI_OPCI,TX_DESC_OPCI,TX_CODPADR_OPCI,NU_AUTO_FORM_OPCI) VALUES ('0403','DOCUMENTOS','04','36')")
    BD(BDCurCon).Execute ("INSERT INTO OPCION (TX_CODI_OPCI,TX_DESC_OPCI,TX_CODPADR_OPCI,NU_AUTO_FORM_OPCI) VALUES ('0405','ARTICULOS X DOCUMENTO','04','37')")
    BD(BDCurCon).Execute ("INSERT INTO OPCION (TX_CODI_OPCI,TX_DESC_OPCI,TX_CODPADR_OPCI,NU_AUTO_FORM_OPCI) VALUES ('0407','ARTICULOS','04','38')")
    BD(BDCurCon).Execute ("INSERT INTO OPCION (TX_CODI_OPCI,TX_DESC_OPCI,TX_CODPADR_OPCI,NU_AUTO_FORM_OPCI) VALUES ('0501','PARAMETROS DE LA APLICACION','05','12')")
    BD(BDCurCon).Execute ("INSERT INTO OPCION (TX_CODI_OPCI,TX_DESC_OPCI,TX_CODPADR_OPCI,NU_AUTO_FORM_OPCI) VALUES ('0502','AUTORIZACIONES','05','34')")
    BD(BDCurCon).Execute ("INSERT INTO OPCION (TX_CODI_OPCI,TX_DESC_OPCI,TX_CODPADR_OPCI) VALUES ('0330','ANULACIONES','03')")
    BD(BDCurCon).Execute ("INSERT INTO OPCION (TX_CODI_OPCI,TX_DESC_OPCI,TX_CODPADR_OPCI,NU_AUTO_FORM_OPCI) VALUES ('033003','ANULACION SOLICITUD COTIZACION','0330','40')")
    BD(BDCurCon).Execute ("INSERT INTO OPCION (TX_CODI_OPCI,TX_DESC_OPCI,TX_CODPADR_OPCI,NU_AUTO_FORM_OPCI) VALUES ('033006','ANULACION ORDEN DE COMPRA','0330','41')")
    BD(BDCurCon).Execute ("INSERT INTO OPCION (TX_CODI_OPCI,TX_DESC_OPCI,TX_CODPADR_OPCI,NU_AUTO_FORM_OPCI) VALUES ('033009','ANULACION ENTRADA','0330','42')")
    BD(BDCurCon).Execute ("INSERT INTO OPCION (TX_CODI_OPCI,TX_DESC_OPCI,TX_CODPADR_OPCI,NU_AUTO_FORM_OPCI) VALUES ('033012','ANULACION REQUISICION','0330','43')")
    BD(BDCurCon).Execute ("INSERT INTO OPCION (TX_CODI_OPCI,TX_DESC_OPCI,TX_CODPADR_OPCI,NU_AUTO_FORM_OPCI) VALUES ('033015','ANULACION DESPACHO','0330','44')")
    BD(BDCurCon).Execute ("INSERT INTO OPCION (TX_CODI_OPCI,TX_DESC_OPCI,TX_CODPADR_OPCI,NU_AUTO_FORM_OPCI) VALUES ('033018','ANULACION TRASLADO','0330','45')")
    BD(BDCurCon).Execute ("INSERT INTO OPCION (TX_CODI_OPCI,TX_DESC_OPCI,TX_CODPADR_OPCI,NU_AUTO_FORM_OPCI) VALUES ('033021','ANULACION BAJA','0330','46')")
    BD(BDCurCon).Execute ("INSERT INTO OPCION (TX_CODI_OPCI,TX_DESC_OPCI,TX_CODPADR_OPCI,NU_AUTO_FORM_OPCI) VALUES ('033024','ANULACION SALIDA','0330','47')")
    BD(BDCurCon).Execute ("INSERT INTO OPCION (TX_CODI_OPCI,TX_DESC_OPCI,TX_CODPADR_OPCI,NU_AUTO_FORM_OPCI) VALUES ('033027','ANULACION FACTURA','0330','48')")
    BD(BDCurCon).Execute ("INSERT INTO OPCION (TX_CODI_OPCI,TX_DESC_OPCI,TX_CODPADR_OPCI,NU_AUTO_FORM_OPCI) VALUES ('0335','COMPRA','03','49')")
'5_1_0
'    0325    54  APROVECHAMIENTO/DONACION    03  50
    BD(BDCurCon).Execute ("INSERT INTO OPCION (TX_CODI_OPCI,TX_DESC_OPCI,TX_CODPADR_OPCI,NU_AUTO_FORM_OPCI) VALUES ('0325','APROVECHAMIENTO/DONACION','03','50')")
    BD(BDCurCon).Execute ("INSERT INTO OPCION (TX_CODI_OPCI,TX_DESC_OPCI,TX_CODPADR_OPCI,NU_AUTO_FORM_OPCI) VALUES ('06','INVENTARIO FISICO','CNT','51')")
    BD(BDCurCon).Execute ("INSERT INTO OPCION (TX_CODI_OPCI,TX_DESC_OPCI,TX_CODPADR_OPCI,NU_AUTO_FORM_OPCI) VALUES ('0503','AJUSTES X INFLACION','05','52')")
    BD(BDCurCon).Execute ("INSERT INTO OPCION (TX_CODI_OPCI,TX_DESC_OPCI,TX_CODPADR_OPCI,NU_AUTO_FORM_OPCI) VALUES ('0410','OTROS INFORMES','04','53')")
    BD(BDCurCon).Execute ("INSERT INTO OPCION (TX_CODI_OPCI,TX_DESC_OPCI,TX_CODPADR_OPCI,NU_AUTO_FORM_OPCI) VALUES ('0302','COTIZACION/LISTA PRECIOS','03','54')")
    BD(BDCurCon).Execute ("INSERT INTO OPCION (TX_CODI_OPCI,TX_DESC_OPCI,TX_CODPADR_OPCI,NU_AUTO_FORM_OPCI) VALUES ('0242','CRITERIOS DE COMPRA','02','55')")
    
    BD(BDCurCon).Execute ("INSERT INTO PERMISO (NU_AUTO_PERF_PERM,NU_AUTO_OPCI_PERM,TX_CREA_PERM,TX_ANUL_PERM,TX_CONS_PERM,TX_IMPR_PERM,TX_OTRO_PERM,TX_OTR1_PERM) VALUES ('1','1','S','S','S','S','N','N')")
    BD(BDCurCon).Execute ("INSERT INTO PERMISO (NU_AUTO_PERF_PERM,NU_AUTO_OPCI_PERM,TX_CREA_PERM,TX_ANUL_PERM,TX_CONS_PERM,TX_IMPR_PERM,TX_OTRO_PERM,TX_OTR1_PERM) VALUES ('1','2','S','S','S','S','N','N')")
    BD(BDCurCon).Execute ("INSERT INTO PERMISO (NU_AUTO_PERF_PERM,NU_AUTO_OPCI_PERM,TX_CREA_PERM,TX_ANUL_PERM,TX_CONS_PERM,TX_IMPR_PERM,TX_OTRO_PERM,TX_OTR1_PERM) VALUES ('1','3','S','S','S','S','N','N')")
    BD(BDCurCon).Execute ("INSERT INTO PERMISO (NU_AUTO_PERF_PERM,NU_AUTO_OPCI_PERM,TX_CREA_PERM,TX_ANUL_PERM,TX_CONS_PERM,TX_IMPR_PERM,TX_OTRO_PERM,TX_OTR1_PERM) VALUES ('1','4','S','S','S','S','N','N')")
    BD(BDCurCon).Execute ("INSERT INTO PERMISO (NU_AUTO_PERF_PERM,NU_AUTO_OPCI_PERM,TX_CREA_PERM,TX_ANUL_PERM,TX_CONS_PERM,TX_IMPR_PERM,TX_OTRO_PERM,TX_OTR1_PERM) VALUES ('1','5','S','S','S','S','N','N')")
    BD(BDCurCon).Execute ("INSERT INTO PERMISO (NU_AUTO_PERF_PERM,NU_AUTO_OPCI_PERM,TX_CREA_PERM,TX_ANUL_PERM,TX_CONS_PERM,TX_IMPR_PERM,TX_OTRO_PERM,TX_OTR1_PERM) VALUES ('1','6','S','S','S','S','N','N')")
    BD(BDCurCon).Execute ("INSERT INTO PERMISO (NU_AUTO_PERF_PERM,NU_AUTO_OPCI_PERM,TX_CREA_PERM,TX_ANUL_PERM,TX_CONS_PERM,TX_IMPR_PERM,TX_OTRO_PERM,TX_OTR1_PERM) VALUES ('1','7','S','S','S','S','N','N')")
    BD(BDCurCon).Execute ("INSERT INTO PERMISO (NU_AUTO_PERF_PERM,NU_AUTO_OPCI_PERM,TX_CREA_PERM,TX_ANUL_PERM,TX_CONS_PERM,TX_IMPR_PERM,TX_OTRO_PERM,TX_OTR1_PERM) VALUES ('1','8','S','S','S','S','N','N')")
    BD(BDCurCon).Execute ("INSERT INTO PERMISO (NU_AUTO_PERF_PERM,NU_AUTO_OPCI_PERM,TX_CREA_PERM,TX_ANUL_PERM,TX_CONS_PERM,TX_IMPR_PERM,TX_OTRO_PERM,TX_OTR1_PERM) VALUES ('1','9','S','S','S','S','N','N')")
    BD(BDCurCon).Execute ("INSERT INTO PERMISO (NU_AUTO_PERF_PERM,NU_AUTO_OPCI_PERM,TX_CREA_PERM,TX_ANUL_PERM,TX_CONS_PERM,TX_IMPR_PERM,TX_OTRO_PERM,TX_OTR1_PERM) VALUES ('1','10','S','S','S','S','N','N')")
    BD(BDCurCon).Execute ("INSERT INTO PERMISO (NU_AUTO_PERF_PERM,NU_AUTO_OPCI_PERM,TX_CREA_PERM,TX_ANUL_PERM,TX_CONS_PERM,TX_IMPR_PERM,TX_OTRO_PERM,TX_OTR1_PERM) VALUES ('1','11','S','S','S','S','N','N')")
    BD(BDCurCon).Execute ("INSERT INTO PERMISO (NU_AUTO_PERF_PERM,NU_AUTO_OPCI_PERM,TX_CREA_PERM,TX_ANUL_PERM,TX_CONS_PERM,TX_IMPR_PERM,TX_OTRO_PERM,TX_OTR1_PERM) VALUES ('1','12','S','S','S','S','N','N')")
    BD(BDCurCon).Execute ("INSERT INTO PERMISO (NU_AUTO_PERF_PERM,NU_AUTO_OPCI_PERM,TX_CREA_PERM,TX_ANUL_PERM,TX_CONS_PERM,TX_IMPR_PERM,TX_OTRO_PERM,TX_OTR1_PERM) VALUES ('1','13','S','S','S','S','N','N')")
    BD(BDCurCon).Execute ("INSERT INTO PERMISO (NU_AUTO_PERF_PERM,NU_AUTO_OPCI_PERM,TX_CREA_PERM,TX_ANUL_PERM,TX_CONS_PERM,TX_IMPR_PERM,TX_OTRO_PERM,TX_OTR1_PERM) VALUES ('1','14','S','S','S','S','N','N')")
    BD(BDCurCon).Execute ("INSERT INTO PERMISO (NU_AUTO_PERF_PERM,NU_AUTO_OPCI_PERM,TX_CREA_PERM,TX_ANUL_PERM,TX_CONS_PERM,TX_IMPR_PERM,TX_OTRO_PERM,TX_OTR1_PERM) VALUES ('1','15','S','S','S','S','N','N')")
    BD(BDCurCon).Execute ("INSERT INTO PERMISO (NU_AUTO_PERF_PERM,NU_AUTO_OPCI_PERM,TX_CREA_PERM,TX_ANUL_PERM,TX_CONS_PERM,TX_IMPR_PERM,TX_OTRO_PERM,TX_OTR1_PERM) VALUES ('1','16','S','S','S','S','N','N')")
    BD(BDCurCon).Execute ("INSERT INTO PERMISO (NU_AUTO_PERF_PERM,NU_AUTO_OPCI_PERM,TX_CREA_PERM,TX_ANUL_PERM,TX_CONS_PERM,TX_IMPR_PERM,TX_OTRO_PERM,TX_OTR1_PERM) VALUES ('1','17','S','S','S','S','N','N')")
    BD(BDCurCon).Execute ("INSERT INTO PERMISO (NU_AUTO_PERF_PERM,NU_AUTO_OPCI_PERM,TX_CREA_PERM,TX_ANUL_PERM,TX_CONS_PERM,TX_IMPR_PERM,TX_OTRO_PERM,TX_OTR1_PERM) VALUES ('1','18','S','S','S','S','N','N')")
    BD(BDCurCon).Execute ("INSERT INTO PERMISO (NU_AUTO_PERF_PERM,NU_AUTO_OPCI_PERM,TX_CREA_PERM,TX_ANUL_PERM,TX_CONS_PERM,TX_IMPR_PERM,TX_OTRO_PERM,TX_OTR1_PERM) VALUES ('1','19','S','S','S','S','N','N')")
    BD(BDCurCon).Execute ("INSERT INTO PERMISO (NU_AUTO_PERF_PERM,NU_AUTO_OPCI_PERM,TX_CREA_PERM,TX_ANUL_PERM,TX_CONS_PERM,TX_IMPR_PERM,TX_OTRO_PERM,TX_OTR1_PERM) VALUES ('1','20','S','S','S','S','N','N')")
    BD(BDCurCon).Execute ("INSERT INTO PERMISO (NU_AUTO_PERF_PERM,NU_AUTO_OPCI_PERM,TX_CREA_PERM,TX_ANUL_PERM,TX_CONS_PERM,TX_IMPR_PERM,TX_OTRO_PERM,TX_OTR1_PERM) VALUES ('1','21','S','S','S','S','N','N')")
    BD(BDCurCon).Execute ("INSERT INTO PERMISO (NU_AUTO_PERF_PERM,NU_AUTO_OPCI_PERM,TX_CREA_PERM,TX_ANUL_PERM,TX_CONS_PERM,TX_IMPR_PERM,TX_OTRO_PERM,TX_OTR1_PERM) VALUES ('1','22','S','S','S','S','N','N')")
    BD(BDCurCon).Execute ("INSERT INTO PERMISO (NU_AUTO_PERF_PERM,NU_AUTO_OPCI_PERM,TX_CREA_PERM,TX_ANUL_PERM,TX_CONS_PERM,TX_IMPR_PERM,TX_OTRO_PERM,TX_OTR1_PERM) VALUES ('1','23','S','S','S','S','N','N')")
    BD(BDCurCon).Execute ("INSERT INTO PERMISO (NU_AUTO_PERF_PERM,NU_AUTO_OPCI_PERM,TX_CREA_PERM,TX_ANUL_PERM,TX_CONS_PERM,TX_IMPR_PERM,TX_OTRO_PERM,TX_OTR1_PERM) VALUES ('1','24','S','S','S','S','N','N')")
    BD(BDCurCon).Execute ("INSERT INTO PERMISO (NU_AUTO_PERF_PERM,NU_AUTO_OPCI_PERM,TX_CREA_PERM,TX_ANUL_PERM,TX_CONS_PERM,TX_IMPR_PERM,TX_OTRO_PERM,TX_OTR1_PERM) VALUES ('1','25','S','S','S','S','N','N')")
    BD(BDCurCon).Execute ("INSERT INTO PERMISO (NU_AUTO_PERF_PERM,NU_AUTO_OPCI_PERM,TX_CREA_PERM,TX_ANUL_PERM,TX_CONS_PERM,TX_IMPR_PERM,TX_OTRO_PERM,TX_OTR1_PERM) VALUES ('1','26','S','S','S','S','N','N')")
    BD(BDCurCon).Execute ("INSERT INTO PERMISO (NU_AUTO_PERF_PERM,NU_AUTO_OPCI_PERM,TX_CREA_PERM,TX_ANUL_PERM,TX_CONS_PERM,TX_IMPR_PERM,TX_OTRO_PERM,TX_OTR1_PERM) VALUES ('1','27','S','S','S','S','N','N')")
    BD(BDCurCon).Execute ("INSERT INTO PERMISO (NU_AUTO_PERF_PERM,NU_AUTO_OPCI_PERM,TX_CREA_PERM,TX_ANUL_PERM,TX_CONS_PERM,TX_IMPR_PERM,TX_OTRO_PERM,TX_OTR1_PERM) VALUES ('1','28','S','S','S','S','N','N')")
    BD(BDCurCon).Execute ("INSERT INTO PERMISO (NU_AUTO_PERF_PERM,NU_AUTO_OPCI_PERM,TX_CREA_PERM,TX_ANUL_PERM,TX_CONS_PERM,TX_IMPR_PERM,TX_OTRO_PERM,TX_OTR1_PERM) VALUES ('1','29','S','S','S','S','N','N')")
    BD(BDCurCon).Execute ("INSERT INTO PERMISO (NU_AUTO_PERF_PERM,NU_AUTO_OPCI_PERM,TX_CREA_PERM,TX_ANUL_PERM,TX_CONS_PERM,TX_IMPR_PERM,TX_OTRO_PERM,TX_OTR1_PERM) VALUES ('1','30','S','S','S','S','N','N')")
    BD(BDCurCon).Execute ("INSERT INTO PERMISO (NU_AUTO_PERF_PERM,NU_AUTO_OPCI_PERM,TX_CREA_PERM,TX_ANUL_PERM,TX_CONS_PERM,TX_IMPR_PERM,TX_OTRO_PERM,TX_OTR1_PERM) VALUES ('1','31','S','S','S','S','N','N')")
    BD(BDCurCon).Execute ("INSERT INTO PERMISO (NU_AUTO_PERF_PERM,NU_AUTO_OPCI_PERM,TX_CREA_PERM,TX_ANUL_PERM,TX_CONS_PERM,TX_IMPR_PERM,TX_OTRO_PERM,TX_OTR1_PERM) VALUES ('1','32','S','S','S','S','N','N')")
    BD(BDCurCon).Execute ("INSERT INTO PERMISO (NU_AUTO_PERF_PERM,NU_AUTO_OPCI_PERM,TX_CREA_PERM,TX_ANUL_PERM,TX_CONS_PERM,TX_IMPR_PERM,TX_OTRO_PERM,TX_OTR1_PERM) VALUES ('1','33','S','S','S','S','N','N')")
    BD(BDCurCon).Execute ("INSERT INTO PERMISO (NU_AUTO_PERF_PERM,NU_AUTO_OPCI_PERM,TX_CREA_PERM,TX_ANUL_PERM,TX_CONS_PERM,TX_IMPR_PERM,TX_OTRO_PERM,TX_OTR1_PERM) VALUES ('1','34','S','S','S','S','N','N')")
    BD(BDCurCon).Execute ("INSERT INTO PERMISO (NU_AUTO_PERF_PERM,NU_AUTO_OPCI_PERM,TX_CREA_PERM,TX_ANUL_PERM,TX_CONS_PERM,TX_IMPR_PERM,TX_OTRO_PERM,TX_OTR1_PERM) VALUES ('1','35','S','S','S','S','N','N')")
    BD(BDCurCon).Execute ("INSERT INTO PERMISO (NU_AUTO_PERF_PERM,NU_AUTO_OPCI_PERM,TX_CREA_PERM,TX_ANUL_PERM,TX_CONS_PERM,TX_IMPR_PERM,TX_OTRO_PERM,TX_OTR1_PERM) VALUES ('1','36','S','S','S','S','N','N')")
    BD(BDCurCon).Execute ("INSERT INTO PERMISO (NU_AUTO_PERF_PERM,NU_AUTO_OPCI_PERM,TX_CREA_PERM,TX_ANUL_PERM,TX_CONS_PERM,TX_IMPR_PERM,TX_OTRO_PERM,TX_OTR1_PERM) VALUES ('1','37','S','S','S','S','N','N')")
    BD(BDCurCon).Execute ("INSERT INTO PERMISO (NU_AUTO_PERF_PERM,NU_AUTO_OPCI_PERM,TX_CREA_PERM,TX_ANUL_PERM,TX_CONS_PERM,TX_IMPR_PERM,TX_OTRO_PERM,TX_OTR1_PERM) VALUES ('1','38','S','S','S','S','N','N')")
    BD(BDCurCon).Execute ("INSERT INTO PERMISO (NU_AUTO_PERF_PERM,NU_AUTO_OPCI_PERM,TX_CREA_PERM,TX_ANUL_PERM,TX_CONS_PERM,TX_IMPR_PERM,TX_OTRO_PERM,TX_OTR1_PERM) VALUES ('1','39','S','S','S','S','N','N')")
    BD(BDCurCon).Execute ("INSERT INTO PERMISO (NU_AUTO_PERF_PERM,NU_AUTO_OPCI_PERM,TX_CREA_PERM,TX_ANUL_PERM,TX_CONS_PERM,TX_IMPR_PERM,TX_OTRO_PERM,TX_OTR1_PERM) VALUES ('1','40','S','S','S','S','N','N')")
    BD(BDCurCon).Execute ("INSERT INTO PERMISO (NU_AUTO_PERF_PERM,NU_AUTO_OPCI_PERM,TX_CREA_PERM,TX_ANUL_PERM,TX_CONS_PERM,TX_IMPR_PERM,TX_OTRO_PERM,TX_OTR1_PERM) VALUES ('1','41','S','S','S','S','N','N')")
    BD(BDCurCon).Execute ("INSERT INTO PERMISO (NU_AUTO_PERF_PERM,NU_AUTO_OPCI_PERM,TX_CREA_PERM,TX_ANUL_PERM,TX_CONS_PERM,TX_IMPR_PERM,TX_OTRO_PERM,TX_OTR1_PERM) VALUES ('1','42','S','S','S','S','N','N')")
    BD(BDCurCon).Execute ("INSERT INTO PERMISO (NU_AUTO_PERF_PERM,NU_AUTO_OPCI_PERM,TX_CREA_PERM,TX_ANUL_PERM,TX_CONS_PERM,TX_IMPR_PERM,TX_OTRO_PERM,TX_OTR1_PERM) VALUES ('1','43','S','S','S','S','N','N')")
    BD(BDCurCon).Execute ("INSERT INTO PERMISO (NU_AUTO_PERF_PERM,NU_AUTO_OPCI_PERM,TX_CREA_PERM,TX_ANUL_PERM,TX_CONS_PERM,TX_IMPR_PERM,TX_OTRO_PERM,TX_OTR1_PERM) VALUES ('1','44','S','S','S','S','N','N')")
    BD(BDCurCon).Execute ("INSERT INTO PERMISO (NU_AUTO_PERF_PERM,NU_AUTO_OPCI_PERM,TX_CREA_PERM,TX_ANUL_PERM,TX_CONS_PERM,TX_IMPR_PERM,TX_OTRO_PERM,TX_OTR1_PERM) VALUES ('1','45','S','S','S','S','N','N')")
    BD(BDCurCon).Execute ("INSERT INTO PERMISO (NU_AUTO_PERF_PERM,NU_AUTO_OPCI_PERM,TX_CREA_PERM,TX_ANUL_PERM,TX_CONS_PERM,TX_IMPR_PERM,TX_OTRO_PERM,TX_OTR1_PERM) VALUES ('1','46','S','S','S','S','N','N')")
    BD(BDCurCon).Execute ("INSERT INTO PERMISO (NU_AUTO_PERF_PERM,NU_AUTO_OPCI_PERM,TX_CREA_PERM,TX_ANUL_PERM,TX_CONS_PERM,TX_IMPR_PERM,TX_OTRO_PERM,TX_OTR1_PERM) VALUES ('1','47','S','S','S','S','N','N')")
    BD(BDCurCon).Execute ("INSERT INTO PERMISO (NU_AUTO_PERF_PERM,NU_AUTO_OPCI_PERM,TX_CREA_PERM,TX_ANUL_PERM,TX_CONS_PERM,TX_IMPR_PERM,TX_OTRO_PERM,TX_OTR1_PERM) VALUES ('1','48','S','S','S','S','N','N')")
    BD(BDCurCon).Execute ("INSERT INTO PERMISO (NU_AUTO_PERF_PERM,NU_AUTO_OPCI_PERM,TX_CREA_PERM,TX_ANUL_PERM,TX_CONS_PERM,TX_IMPR_PERM,TX_OTRO_PERM,TX_OTR1_PERM) VALUES ('1','49','S','S','S','S','N','N')")
    BD(BDCurCon).Execute ("INSERT INTO PERMISO (NU_AUTO_PERF_PERM,NU_AUTO_OPCI_PERM,TX_CREA_PERM,TX_ANUL_PERM,TX_CONS_PERM,TX_IMPR_PERM,TX_OTRO_PERM,TX_OTR1_PERM) VALUES ('1','50','S','S','S','S','N','N')")
'5_1_0
    BD(BDCurCon).Execute ("INSERT INTO PERMISO (NU_AUTO_PERF_PERM,NU_AUTO_OPCI_PERM,TX_CREA_PERM,TX_ANUL_PERM,TX_CONS_PERM,TX_IMPR_PERM,TX_OTRO_PERM,TX_OTR1_PERM) VALUES ('1','51','S','S','S','S','N','N')")
    BD(BDCurCon).Execute ("INSERT INTO PERMISO (NU_AUTO_PERF_PERM,NU_AUTO_OPCI_PERM,TX_CREA_PERM,TX_ANUL_PERM,TX_CONS_PERM,TX_IMPR_PERM,TX_OTRO_PERM,TX_OTR1_PERM) VALUES ('1','52','S','S','S','S','N','N')")
    BD(BDCurCon).Execute ("INSERT INTO PERMISO (NU_AUTO_PERF_PERM,NU_AUTO_OPCI_PERM,TX_CREA_PERM,TX_ANUL_PERM,TX_CONS_PERM,TX_IMPR_PERM,TX_OTRO_PERM,TX_OTR1_PERM) VALUES ('1','53','S','S','S','S','N','N')")
    BD(BDCurCon).Execute ("INSERT INTO PERMISO (NU_AUTO_PERF_PERM,NU_AUTO_OPCI_PERM,TX_CREA_PERM,TX_ANUL_PERM,TX_CONS_PERM,TX_IMPR_PERM,TX_OTRO_PERM,TX_OTR1_PERM) VALUES ('1','54','S','S','S','S','N','N')")
    BD(BDCurCon).Execute ("INSERT INTO PERMISO (NU_AUTO_PERF_PERM,NU_AUTO_OPCI_PERM,TX_CREA_PERM,TX_ANUL_PERM,TX_CONS_PERM,TX_IMPR_PERM,TX_OTRO_PERM,TX_OTR1_PERM) VALUES ('1','55','S','S','S','S','N','N')")
    BD(BDCurCon).Execute ("INSERT INTO PERMISO (NU_AUTO_PERF_PERM,NU_AUTO_OPCI_PERM,TX_CREA_PERM,TX_ANUL_PERM,TX_CONS_PERM,TX_IMPR_PERM,TX_OTRO_PERM,TX_OTR1_PERM) VALUES ('1','56','S','S','S','S','N','N')")
    BD(BDCurCon).Execute ("INSERT INTO PERMISO (NU_AUTO_PERF_PERM,NU_AUTO_OPCI_PERM,TX_CREA_PERM,TX_ANUL_PERM,TX_CONS_PERM,TX_IMPR_PERM,TX_OTRO_PERM,TX_OTR1_PERM) VALUES ('1','57','S','S','S','S','N','N')")
    BD(BDCurCon).Execute ("INSERT INTO PERMISO (NU_AUTO_PERF_PERM,NU_AUTO_OPCI_PERM,TX_CREA_PERM,TX_ANUL_PERM,TX_CONS_PERM,TX_IMPR_PERM,TX_OTRO_PERM,TX_OTR1_PERM) VALUES ('1','58','S','S','S','S','N','N')")

End Sub

Public Function ExisteTABLA(Cual As String) As Boolean
    ExisteTABLA = False
    Dim Cata As New adox.Catalog, Tabl As adox.Table
    Cata.ActiveConnection = BD(BDCurCon)
    For Each Tabl In Cata.Tables
        If Tabl.Name = Cual Then ExisteTABLA = True: Exit For
    Next
    Set Cata = Nothing
End Function

Sub BaseDatosCasa()
    Result = CrearCampo("ARTICULO", "NU_UTIL_ARTI", 7, 8, 1, False)
    Result = CrearCampo("GRUP_ARTICULO", "CD_AJDB_GRUP", 10, 14, 1, False)
    Result = CrearCampo("GRUP_ARTICULO", "CD_AJCR_GRUP", 10, 14, 1, False)

'R_GRUP_DEPE (CD_AJDB_RGD, CD_AJCR_RGD)
'    Result = CrearCampo("R_GRUP_DEPE", "CD_AJDB_RGD", 10, 14, 1, False)
'    Result = CrearCampo("R_GRUP_DEPE", "CD_AJCR_RGD", 10, 14, 1, False)
    Result = CrearCampo("PARAMETROS_INVE", "ID_UTILID_PINV", 10, 1, 1, False)
    Result = CrearCampo("PARAMETROS_INVE", "CD_DONA_APLI", 10, 14, 1, False)
    Result = CrearCampo("R_GRUP_DEPE", "CD_AJDB_RGD", 10, 14, 1, False)
    Result = CrearCampo("R_GRUP_DEPE", "CD_AJCR_RGD", 10, 14, 1, False)
    
    Result = CrearCampo("IN_ENCABEZADO", "FE_VIGE_ENCA", 8, 8, 1, False)
    
    Result = CrearTabla("IN_CRITERIO", "TX_DESC_CRIT, 10, 30, 1")
    Result = CrearCampo("IN_CRITERIO", "NU_AUTO_CRIT", 4, 4, 1, True)
    Result = CrearCampo("IN_CRITERIO", "TX_CODI_CRIT", 10, 10, 1, False)
    Result = CrearIndice("IN_CRITERIO", "PK_IN_CRITERIO", "NU_AUTO_CRIT", True)
    Result = CrearIndice("IN_CRITERIO", "LL_IN_CRITERIO", "TX_CODI_CRIT", False, True)
    Result = CrearTabla("IN_R_CRITERIO_TERCERO", "NU_AUTO_CRIT_RCRTE, 4, 4, 0")
    Result = CrearCampo("IN_R_CRITERIO_TERCERO", "CD_CODI_TERC_RCRTE", 10, 11, 0, False)
    Result = CrearCampo("IN_R_CRITERIO_TERCERO", "NU_CALI_RCRTE", 7, 8, 0, False)
    Result = CrearCampo("IN_R_CRITERIO_TERCERO", "NU_PESO_RCRTE", 7, 8, 0, False)
    Result = CrearIndice("IN_R_CRITERIO_TERCERO", "PK_IN_R_CRITERIO_TERCERO", "NU_AUTO_CRIT_RCRTE, CD_CODI_TERC_RCRTE", True)
    Result = CrearRelacion("IN_R_CRITERIO_TERCEROCRITERIO", "NU_AUTO_CRIT_RCRTE", "NU_AUTO_CRIT", "IN_R_CRITERIO_TERCERO", "IN_CRITERIO")
    Result = CrearRelacion("IN_R_CRITERIO_TERCEROTERCERO", "CD_CODI_TERC_RCRTE", "CD_CODI_TERC", "IN_R_CRITERIO_TERCERO", "TERCERO")
    
''    BD(BDCurCon).Execute ("INSERT INTO IN_DOCUMENTO (NU_AUTO_DOCU, TX_CODI_DOCU, TX_NOMB_DOCU, TX_AFEC_DOCU, TX_INTE_DOCU, NU_AUTO_DOCUDEPE_DOCU, NU_AUTO_DOCUREVE_DOCU,TX_TIPOVAL_DOCU, TX_INDE_DOCU) VALUES ('26','APRDON','ENTRADA APROVECHA/DONACION','S','N','2','27','C','S')")
''    BD(BDCurCon).Execute ("INSERT INTO IN_DOCUMENTO (NU_AUTO_DOCU, TX_CODI_DOCU, TX_NOMB_DOCU, TX_AFEC_DOCU, TX_INTE_DOCU, NU_AUTO_DOCUDEPE_DOCU, NU_AUTO_DOCUREVE_DOCU,TX_TIPOVAL_DOCU, TX_INDE_DOCU) VALUES ('27','ANLAPR','ANULACION ENTRADA APROVECHA/DONACION','S','N','26','0','C','N')")
''    BD(BDCurCon).Execute ("INSERT INTO IN_DOCUMENTO (NU_AUTO_DOCU, TX_CODI_DOCU, TX_NOMB_DOCU, TX_AFEC_DOCU, TX_INTE_DOCU, NU_AUTO_DOCUDEPE_DOCU, NU_AUTO_DOCUREVE_DOCU,TX_TIPOVAL_DOCU, TX_INDE_DOCU) VALUES ('28','DEVAPR','DEVOLUCION ENTRADA APROVECHA/DONACION','S','N','26','0','C','N')")
    BD(BDCurCon).Execute ("INSERT INTO IN_DOCUMENTO (NU_AUTO_DOCU, TX_CODI_DOCU, TX_NOMB_DOCU, TX_AFEC_DOCU, TX_INTE_DOCU, NU_AUTO_DOCUDEPE_DOCU, NU_AUTO_DOCUREVE_DOCU,TX_TIPOVAL_DOCU, TX_INDE_DOCU) VALUES ('29','LISTAS','COTIZACION/LISTA DE PRECIOS','N','N','0','30','C','S')")
    BD(BDCurCon).Execute ("INSERT INTO IN_DOCUMENTO (NU_AUTO_DOCU, TX_CODI_DOCU, TX_NOMB_DOCU, TX_AFEC_DOCU, TX_INTE_DOCU, NU_AUTO_DOCUDEPE_DOCU, NU_AUTO_DOCUREVE_DOCU,TX_TIPOVAL_DOCU, TX_INDE_DOCU) VALUES ('30','ANLSOL','ANULACION COTIZACION/LISTA DE PRECIOS','N','N','1','0','C','N')")
    
'''    BD(BDCurCon).Execute ("INSERT INTO FORMULARIO (TX_DESC_FORM,TX_FORM_FORM,TX_TIPO_FORM) VALUES ('COMPRA','FrmFASECOMPRA','FRM')")
''    BD(BDCurCon).Execute ("INSERT INTO FORMULARIO (TX_DESC_FORM,TX_FORM_FORM,TX_TIPO_FORM) VALUES ('APROVECHAMIENTO/DONACION','FrmGENERAL',26)")
''    BD(BDCurCon).Execute ("INSERT INTO FORMULARIO (TX_DESC_FORM,TX_FORM_FORM,TX_TIPO_FORM) VALUES ('INVENTARIO FISICO','FrmFISICO','FRM')")
''    BD(BDCurCon).Execute ("INSERT INTO FORMULARIO (TX_DESC_FORM,TX_FORM_FORM,TX_TIPO_FORM) VALUES ('AJUSTES X INFLACION','FrmINFLA','FRM')")
''    BD(BDCurCon).Execute ("INSERT INTO FORMULARIO (TX_DESC_FORM,TX_FORM_FORM,TX_TIPO_FORM) VALUES ('OTROS INFORMES','FrmOTREPORTE','FRM')")
    BD(BDCurCon).Execute ("INSERT INTO FORMULARIO (TX_DESC_FORM,TX_FORM_FORM,TX_TIPO_FORM) VALUES ('COTIZACION/LISTA PRECIOS PROVEEDOR','FrmGENERAL',29)")
    BD(BDCurCon).Execute ("INSERT INTO FORMULARIO (TX_DESC_FORM,TX_FORM_FORM,TX_TIPO_FORM) VALUES ('CRITERIOS DE COMPRA','FrmCriterios','FRM')")
    BD(BDCurCon).Execute ("INSERT INTO FORMULARIO (TX_DESC_FORM,TX_FORM_FORM,TX_TIPO_FORM) VALUES ('CRITERIOS X PROVEEDOR','FrmCriteXProve','FRM')")
    BD(BDCurCon).Execute ("INSERT INTO FORMULARIO (TX_DESC_FORM,TX_FORM_FORM,TX_TIPO_FORM) VALUES ('PROYECTO DE COMPRA','FrmPROYCOMPRA','FRM')")
'''FrmCriteXProve
'''FrmPROYCOMPRA
''    BD(BDCurCon).Execute ("INSERT INTO OPCION (TX_CODI_OPCI,TX_DESC_OPCI,TX_CODPADR_OPCI,NU_AUTO_FORM_OPCI) VALUES ('0325','APROVECHAMIENTO/DONACION','03','50')")
''    BD(BDCurCon).Execute ("INSERT INTO OPCION (TX_CODI_OPCI,TX_DESC_OPCI,TX_CODPADR_OPCI,NU_AUTO_FORM_OPCI) VALUES ('06','INVENTARIO FISICO','CNT','51')")
''    BD(BDCurCon).Execute ("INSERT INTO OPCION (TX_CODI_OPCI,TX_DESC_OPCI,TX_CODPADR_OPCI,NU_AUTO_FORM_OPCI) VALUES ('0503','AJUSTES X INFLACION','05','52')")
''    BD(BDCurCon).Execute ("INSERT INTO OPCION (TX_CODI_OPCI,TX_DESC_OPCI,TX_CODPADR_OPCI,NU_AUTO_FORM_OPCI) VALUES ('0410','OTROS INFORMES','04','53')")
    BD(BDCurCon).Execute ("INSERT INTO OPCION (TX_CODI_OPCI,TX_DESC_OPCI,TX_CODPADR_OPCI,NU_AUTO_FORM_OPCI) VALUES ('302','COTIZACION/LISTA PRECIOS','03','54')")
    BD(BDCurCon).Execute ("INSERT INTO OPCION (TX_CODI_OPCI,TX_DESC_OPCI,TX_CODPADR_OPCI,NU_AUTO_FORM_OPCI) VALUES ('242','CRITERIOS DE COMPRA','02','55')")
    BD(BDCurCon).Execute ("INSERT INTO OPCION (TX_CODI_OPCI,TX_DESC_OPCI,TX_CODPADR_OPCI,NU_AUTO_FORM_OPCI) VALUES ('245','CRITERIOS X PROVEEDOR','02','56')")
    BD(BDCurCon).Execute ("INSERT INTO OPCION (TX_CODI_OPCI,TX_DESC_OPCI,TX_CODPADR_OPCI,NU_AUTO_FORM_OPCI) VALUES ('506','PROYECTO DE COMPRA','05','57')")

    BD(BDCurCon).Execute ("INSERT INTO PERMISO (NU_AUTO_PERF_PERM,NU_AUTO_OPCI_PERM,TX_CREA_PERM,TX_ANUL_PERM,TX_CONS_PERM,TX_IMPR_PERM,TX_OTRO_PERM,TX_OTR1_PERM) VALUES ('1','49','S','S','S','S','N','N')")
    BD(BDCurCon).Execute ("INSERT INTO PERMISO (NU_AUTO_PERF_PERM,NU_AUTO_OPCI_PERM,TX_CREA_PERM,TX_ANUL_PERM,TX_CONS_PERM,TX_IMPR_PERM,TX_OTRO_PERM,TX_OTR1_PERM) VALUES ('1','50','S','S','S','S','N','N')")
    BD(BDCurCon).Execute ("INSERT INTO PERMISO (NU_AUTO_PERF_PERM,NU_AUTO_OPCI_PERM,TX_CREA_PERM,TX_ANUL_PERM,TX_CONS_PERM,TX_IMPR_PERM,TX_OTRO_PERM,TX_OTR1_PERM) VALUES ('1','51','S','S','S','S','N','N')")
    BD(BDCurCon).Execute ("INSERT INTO PERMISO (NU_AUTO_PERF_PERM,NU_AUTO_OPCI_PERM,TX_CREA_PERM,TX_ANUL_PERM,TX_CONS_PERM,TX_IMPR_PERM,TX_OTRO_PERM,TX_OTR1_PERM) VALUES ('1','52','S','S','S','S','N','N')")
''    BD(BDCurCon).Execute ("INSERT INTO PERMISO (NU_AUTO_PERF_PERM,NU_AUTO_OPCI_PERM,TX_CREA_PERM,TX_ANUL_PERM,TX_CONS_PERM,TX_IMPR_PERM,TX_OTRO_PERM,TX_OTR1_PERM) VALUES ('1','53','S','S','S','S','N','N')")
''    BD(BDCurCon).Execute ("INSERT INTO PERMISO (NU_AUTO_PERF_PERM,NU_AUTO_OPCI_PERM,TX_CREA_PERM,TX_ANUL_PERM,TX_CONS_PERM,TX_IMPR_PERM,TX_OTRO_PERM,TX_OTR1_PERM) VALUES ('1','54','S','S','S','S','N','N')")
''    BD(BDCurCon).Execute ("INSERT INTO PERMISO (NU_AUTO_PERF_PERM,NU_AUTO_OPCI_PERM,TX_CREA_PERM,TX_ANUL_PERM,TX_CONS_PERM,TX_IMPR_PERM,TX_OTRO_PERM,TX_OTR1_PERM) VALUES ('1','55','S','S','S','S','N','N')")
''    BD(BDCurCon).Execute ("INSERT INTO PERMISO (NU_AUTO_PERF_PERM,NU_AUTO_OPCI_PERM,TX_CREA_PERM,TX_ANUL_PERM,TX_CONS_PERM,TX_IMPR_PERM,TX_OTRO_PERM,TX_OTR1_PERM) VALUES ('1','56','S','S','S','S','N','N')")
''    BD(BDCurCon).Execute ("INSERT INTO PERMISO (NU_AUTO_PERF_PERM,NU_AUTO_OPCI_PERM,TX_CREA_PERM,TX_ANUL_PERM,TX_CONS_PERM,TX_IMPR_PERM,TX_OTRO_PERM,TX_OTR1_PERM) VALUES ('1','57','S','S','S','S','N','N')")
''    BD(BDCurCon).Execute ("INSERT INTO PERMISO (NU_AUTO_PERF_PERM,NU_AUTO_OPCI_PERM,TX_CREA_PERM,TX_ANUL_PERM,TX_CONS_PERM,TX_IMPR_PERM,TX_OTRO_PERM,TX_OTR1_PERM) VALUES ('1','58','S','S','S','S','N','N')")
''    BD(BDCurCon).Execute ("INSERT INTO PERMISO (NU_AUTO_PERF_PERM,NU_AUTO_OPCI_PERM,TX_CREA_PERM,TX_ANUL_PERM,TX_CONS_PERM,TX_IMPR_PERM,TX_OTRO_PERM,TX_OTR1_PERM) VALUES ('1','59','S','S','S','S','N','N')")
''    BD(BDCurCon).Execute ("INSERT INTO PERMISO (NU_AUTO_PERF_PERM,NU_AUTO_OPCI_PERM,TX_CREA_PERM,TX_ANUL_PERM,TX_CONS_PERM,TX_IMPR_PERM,TX_OTRO_PERM,TX_OTR1_PERM) VALUES ('1','60','S','S','S','S','N','N')")

End Sub

Sub DecimalesListar()
    Dim DirRpt As String
    Dim m_Proj As CRAXDRT.Application
    Dim m_Report As CRAXDRT.Report
    Dim Seccion As CRAXDRT.Section
    Dim campo As CRAXDRT.FieldObject
    Dim compo As Object, objeto As Object
'    DirRpt = "C:\FACE SAM\Mis documentos\FuentesCNT\Inventarios\Inventarios 5.0.0\PROGRAMA5_2_0\infDCODC_COS.rpt"
'    DirRpt = "C:\FACE SAM\Mis documentos\FuentesCNT\Inventarios\Inventarios 5.0.0\PROGRAMA5_2_0\terceros.rpt"
    DirRpt = "C:\FACE SAM\Mis documentos\FuentesCNT\Inventarios\Inventarios 5.2.0\958.rpt"
'    Set m_Proj = New CRAXDRT.Application
    Set m_Proj = New CRAXDRT.Application
    Set m_Report = m_Proj.OpenReport(DirRpt)
'    Set m_Report = m_Proj.OpenReport(DirRpt & "terceros.rpt")
'    Set rs = New ADODB.Recordset
'    rs.Open "SELECT " & Campos & " FROM " & Tabla, BD(0)
'    m_Report.Database.SetDataSource rs, 3, 1
'    m_Report.FormulaFields(1).Text = Comi & Entidad & Comi
'    m_Report.FormulaFields(2).Text = Comi & Me.Caption & Comi
    For Each Seccion In m_Report.Sections
        For Each compo In Seccion.ReportObjects
            If compo.Kind = crFieldObject Then
                Set campo = compo
                Set objeto = campo.Field
                If objeto.Kind = crDatabaseField Then Debug.Print "Campo:" & compo.Name, compo.DecimalPlaces
                If objeto.Kind = crFormulaField And objeto.ValueType = crNumberField Then Debug.Print "Formula:" & objeto.FormulaFieldName, compo.DecimalPlaces, objeto.Text
            End If
        Next
    Next
End Sub

'CA: Retorna el nombre del �ndice que contiene todas las columnas por las que se est� preguntando
Function BuscarIndice(Table As String, Columns As String) As String
Dim i As Integer
Dim J As Integer
Dim cat As New adox.Catalog
Dim Tbl As New adox.Table
Dim Index As adox.Index
Dim TBLDAO As DAO.TableDef
Dim IndexDAO As DAO.Index
Dim cont As Integer
Dim Pos As Integer

Dim ListColumns() As String

   cont = 0
   
   On Error GoTo control
   
   Do
     ReDim Preserve ListColumns(cont)
     
     Pos = InStr(Columns, Coma)
     If Pos = 0 Then
        ListColumns(cont) = Columns
        Columns = NUL$
     Else
        ListColumns(cont) = Left(Columns, Pos - 1)
        Columns = Right(Columns, Len(Columns) - Pos)
     End If
     cont = cont + 1
   Loop While Len(Columns) > 0


   If MotorBD = "ACCESS" Then
      Set TBLDAO = BDDAO(BDCurCon).TableDefs(Table)
      For Each IndexDAO In TBLDAO.Indexes
         cont = 0
         For J = 0 To UBound(ListColumns, 1)
            If InStr(1, UCase(IndexDAO.Fields), UCase(ListColumns(J))) Then
               cont = cont + 1
               If cont = UBound(ListColumns, 1) + 1 Then   'Si estan todas las columnas, ese es el �ndice
                  BuscarIndice = IndexDAO.Name
                  Exit Function
               End If
            End If
         Next
      Next
   Else
      'Set cat.ActiveConnection = BD(1)
      Set cat.ActiveConnection = BD(BDCurCon)
      Set Tbl = cat.Tables(Table)
        
      For Each Index In Tbl.Indexes
         cont = 0
         For i = 0 To Index.Columns.Count - 1
            For J = 0 To UBound(ListColumns, 1)
               If UCase(Index.Columns(i).Name) = UCase(ListColumns(J)) Then
                  cont = cont + 1
                  If cont = UBound(ListColumns, 1) + 1 Then   'Si estan todas las columnas, ese es el �ndice
                     BuscarIndice = Index.Name
                     Exit Function
                  Else
                     Exit For
                  End If
               End If
            Next
         Next
      Next
   End If
   BuscarIndice = NUL$
   Exit Function
   
control:
   BuscarIndice = NUL$
   Call ConvertErr
End Function

' AGD
Public Function ExisteCAMPO(Tabla As String, campo As String) As Boolean
Dim cat As New adox.Catalog
'Dim TBLDAO As DAO.TableDef     'DEPURACION DE CODIGO
'Dim FLDDAO As DAO.Field           'DEPURACION DE CODIGO
Dim FLDDA As adox.Column
Dim Tbl As New adox.Table
    
'    If MotorBD = "ACCESS" Then
'      For Each TBLDAO In BDDAO.TableDefs
'        If UCase(TBLDAO.Name) = UCase(Tabla) Then
'            For Each FLDDAO In TBLDAO.Fields
'                If UCase(FLDDAO.Name) = UCase(campo) Then ExisteCAMPO = True: Exit Function
'            Next
'        End If
'      Next
'    Else
          Set cat.ActiveConnection = BD(BDCur)
          Set Tbl = cat.Tables(Tabla)
          
        For Each FLDDA In Tbl.Columns
            If UCase(FLDDA.Name) = UCase(campo) Then ExisteCAMPO = True: Exit Function
        Next
'    End If
End Function
'AGD

Function BuscarRelacion(TablePrimary As String, TableSecundary As String, Foreingkey As String) As String
Dim i As Integer
Dim J As Integer
'Dim Llaves As adox.Keys        'DEPURACION DE CODIGO
Dim cat As New adox.Catalog
Dim Tbl As New adox.Table
'Dim TBLDAO As DAO.TableDef  'DEPURACION DE CODIGO
Dim cont As Integer
Dim Relac As adox.Key
Dim ListColumns() As String
Dim Pos As Integer
    
cont = 0

   On Error GoTo control
   
   Do
     ReDim Preserve ListColumns(cont)
     
     Pos = InStr(Foreingkey, Coma)
     If Pos = 0 Then
        ListColumns(cont) = Foreingkey
        Foreingkey = NUL$
     Else
        ListColumns(cont) = Left(Foreingkey, Pos - 1)
        Foreingkey = Right(Foreingkey, Len(Foreingkey) - Pos)
     End If
     cont = cont + 1
   Loop While Len(Foreingkey) > 0
   

   If MotorBD = "ACCESS" Then
      For i = 0 To BDDAO(1).Relations.Count - 1
         If BDDAO(1).Relations(i).Table = TablePrimary And BDDAO(1).Relations(i).ForeignTable = TableSecundary Then
            BuscarRelacion = BDDAO(1).Relations(i).Name
            Exit Function
         End If
      Next
   ElseIf MotorBD = "SQL" Then
      'Debug.Print BD(BDCurCon).Connect
      Set cat.ActiveConnection = BD(BDCurCon)
      'Debug.Print cat.Tables.Count
      Set Tbl = cat.Tables(TableSecundary)
      'Debug.Print Tbl.Name
      For Each Relac In Tbl.Keys
         If Relac.Type = adKeyForeign Then
            cont = 0
            For i = 0 To Relac.Columns.Count - 1
               For J = 0 To UBound(ListColumns, 1)
                  If UCase(Relac.Columns(i).Name) = UCase(ListColumns(J)) Then
                     cont = cont + 1
                     If cont = UBound(ListColumns, 1) + 1 Then   'Si estan todas las columnas, ese es el �ndice
                        BuscarRelacion = Relac.Name
                        Exit Function
                     Else
                        Exit For
                     End If
                  End If
               Next
            Next
         End If
      Next
   End If
   
control:
   BuscarRelacion = NUL$
End Function

'Crea una tabla temporal de la tabla Tbl.
'Con los parametros: TblTemporal (Nombre de la nueva tabla), OptEliminar (True=Eliminar, False=No eliminar).
Function CrearTablaTemporal(Tbl As String, TblTemporal As String, OptEliminar As Boolean) As Integer
Dim SQL As String

   SQL = "SELECT * INTO " & TblTemporal & " FROM " & Tbl
   Result = ExecSQLCommand(SQL)
   
   If OptEliminar = True And Result <> FAIL Then Result = EliminarTabla(Tbl)
   
   CrearTablaTemporal = Result
   
End Function
Function fnAgregarOpcion(cTitulo As String, cForma As String, cPadre As String, cTipoForma As String) As Boolean '|.DR.|
    
    
    'HRR Depuracion Nov2008
    ReDim Arr(0)
    Result = LoadData("FORMULARIO", "TX_FORM_FORM", "TX_FORM_FORM=" & Comi & cForma & Comi & " AND TX_TIPO_FORM=" & Comi & cTipoForma & Comi, Arr)
    If Result <> FAIL And Encontro Then fnAgregarOpcion = True: Exit Function
    'HRR Depuracion Nov2008
    
    
    If (BeginTran(STranIUp & "UPD_AGREGAR_OPCION") <> FAIL) Then
    'If (BeginTran(STranIUp & "UPD_FECHAS_DETALLE") <> FAIL) Then
        Dim vAuto As Long
        Valores = "TX_DESC_FORM='" & cTitulo & "',TX_FORM_FORM='" & cForma & "',TX_TIPO_FORM='" & cTipoForma & "'"
        Result = DoInsertSQL("FORMULARIO", Valores)
        If Result <> FAIL Then
            ReDim Arr(0)
            '-----------------------------------------------------------------------------------------
            'GMS 2008-01-30.
            'Si cForma = NUL$ , busca por el nombre de la opcion
            'Result = LoadData("FORMULARIO", "NU_AUTO_FORM", "TX_FORM_FORM='FrmBDLogin'", Arr)
            Result = LoadData("FORMULARIO", "NU_AUTO_FORM", IIf(cForma = NUL$, "TX_DESC_FORM=" & Comi & cTitulo & Comi, "TX_FORM_FORM=" & Comi & cForma & Comi), Arr)
            '-----------------------------------------------------------------------------------------
            If Result = FAIL Then GoTo Fall�
            vAuto = Val(Arr(0))
            Result = LoadData("OPCION", "Max(TX_CODI_OPCI)", "TX_CODI_OPCI Like '" & cPadre & "%'", Arr)
            Arr(0) = Format(Arr(0) + 1, String(Len(Arr(0)), "0"))
            If Result <> FAIL And Encontro Then
                Valores = "TX_CODI_OPCI='" & Arr(0) & "', TX_DESC_OPCI='" & cTitulo & "',"
                Valores = Valores & "TX_CODPADR_OPCI='" & cPadre & "',NU_AUTO_FORM_OPCI=" & vAuto
                Result = DoInsertSQL("OPCION", Valores)
                If Result <> FAIL Then Call CrearOpcion(Arr(0))
            Else
                Result = FAIL
                GoTo Fall�
            End If
        End If
    End If

Fall�:

    If (Result <> FAIL) Then
       If (CommitTran() = FAIL) Then
          Call RollBackTran
            fnAgregarOpcion = False
       Else
            fnAgregarOpcion = True
       End If
       
    Else
       Call RollBackTran
        fnAgregarOpcion = False
    End If

End Function


'---------------------------------------------------------------------------------------
' Procedure : ExisteCampoTemporAccess
' DateTime  : 10/10/2008 11:36
' Author    : albert_silva
' Purpose   : AASV R2171 Verifica si existe una tabla en una base de datos de Access
'---------------------------------------------------------------------------------------
'
Public Function ExisteCampoTemporAccess(Temporal As DAO.Database, Tabla As String, campo As String) As Boolean
Dim TBLDAO As DAO.TableDef
Dim FLDDAO As DAO.Field
   campo = Trim(campo)
   For Each TBLDAO In Temporal.TableDefs
      If UCase(TBLDAO.Name) = UCase(Tabla) Then
          For Each FLDDAO In TBLDAO.Fields
              If UCase(FLDDAO.Name) = UCase(campo) Then ExisteCampoTemporAccess = True: Exit Function
          Next
      End If
   Next
End Function

'---------------------------------------------------------------------------------------
' Procedure : fnAgregarOpcionConti
' DateTime  : 25/02/2011 14:54
' Author    : gabriel_vallejo
' Purpose   :
'---------------------------------------------------------------------------------------
'
Function fnAgregarOpcionConti(cTitulo As String, cForma As String, cPadre As String, cTipoForma As String) As Boolean '|.DR.|
    
    

    ReDim Arr(0)
    Result = LoadData("FORMULARIO", "TX_FORM_FORM", "TX_FORM_FORM=" & Comi & cForma & Comi & " AND TX_DESC_FORM=" & Comi & cTitulo & Comi, Arr)
    If Result <> FAIL And Encontro Then fnAgregarOpcionConti = True: Exit Function

    
    
    If (BeginTran(STranIUp & "UPD_AGREGAR_OPCION") <> FAIL) Then
        Dim vAuto As Long
        Valores = "TX_DESC_FORM='" & cTitulo & "',TX_FORM_FORM='" & cForma & "',TX_TIPO_FORM='" & cTipoForma & "'"
        Result = DoInsertSQL("FORMULARIO", Valores)
        If Result <> FAIL Then
            ReDim Arr(0)
            '-----------------------------------------------------------------------------------------
            'Result = LoadData("FORMULARIO", "NU_AUTO_FORM", IIf(cForma = NUL$, "TX_DESC_FORM=" & Comi & cTitulo & Comi, "TX_FORM_FORM=" & Comi & cForma & Comi), Arr)
            Result = LoadData("FORMULARIO", "NU_AUTO_FORM", "TX_DESC_FORM=" & Comi & cTitulo & Comi, Arr) 'GAVL T5266
            '-----------------------------------------------------------------------------------------
            If Result = FAIL Then GoTo Fall�
            vAuto = Val(Arr(0))
            Result = LoadData("OPCION", "Max(TX_CODI_OPCI)", "TX_CODI_OPCI Like '" & cPadre & "%'", Arr)
            Arr(0) = Format(Arr(0) + 1, String(Len(Arr(0)), "0"))
            If Result <> FAIL And Encontro Then
                Valores = "TX_CODI_OPCI='" & Arr(0) & "', TX_DESC_OPCI='" & cTitulo & "',"
                Valores = Valores & "TX_CODPADR_OPCI='" & cPadre & "',NU_AUTO_FORM_OPCI=" & vAuto
                Result = DoInsertSQL("OPCION", Valores)
                If Result <> FAIL Then Call CrearOpcion(Arr(0))
            Else
                Result = FAIL
                GoTo Fall�
            End If
        End If
    End If

Fall�:

    If (Result <> FAIL) Then
       If (CommitTran() = FAIL) Then
          Call RollBackTran
            fnAgregarOpcionConti = False
       Else
            fnAgregarOpcionConti = True
       End If
       
    Else
       Call RollBackTran
        fnAgregarOpcionConti = False
    End If

End Function

'---------------------------------------------------------------------------------------
' Procedure : Actualizar_7_2
' DateTime  : 30/04/2011 14:30
' Author    : Juan Alejandro Garcia
' Purpose   : ACTUALIZACION DE BASE DE DATOS
'---------------------------------------------------------------------------------------

Sub Actualizar_7_2()

 
'INICIO JAGS T7783 /R7019
Result = SUCCEED

If Result <> FAIL Then
    If ExisteTABLA("AUDITOR_IN") Then
    
        If Result <> FAIL Then
                If Not ExisteCAMPO("AUDITOR_IN", "AudVerExe") Then Result = CrearCampo("AUDITOR_IN", "AudVerExe", 10, 50, 1, False)
        End If
        
        If Result <> FAIL Then
                If Not ExisteCAMPO("AUDITOR_IN", "AudFecExe") Then Result = CrearCampo("AUDITOR_IN", "AudFecExe", 8, 0, 1, False)
        End If
        
    End If
End If

'JAGS R7123
If Result <> FAIL Then
    If ExisteTABLA("USUARIO") Then
        If Not ExisteCAMPO("USUARIO", "NU_ESTA_USUA") Then Result = CrearCampo("USUARIO", "NU_ESTA_USUA", 2, 0, 1, False)
        If Result <> FAIL Then Result = DoUpdate("USUARIO", "NU_ESTA_USUA = '0'", NUL$)
   End If
End If
'JAGS R7123

If Result <> FAIL Then
        Result = DoUpdate("PARAMETROS_INVE", "NU_NVERDB_PINV='7.2.0'", NUL$)
      End If
 
'FIN JAGS T7783 /R7019
End Sub

'---------------------------------------------------------------------------------------
' Procedure : Actualizar_7_3
' DateTime  : 17/02/2012 10:25
' Author    : Juan Alejandro Garcia
' Purpose   : ACTUALIZACION DE BASE DE DATOS
'---------------------------------------------------------------------------------------

Sub Actualizar_7_3()
 
'INICIO JAGS T9979
Result = SUCCEED
'ReDim Arr(0)
ReDim Arr(2) 'T9979 PNC


If Result <> FAIL Then
    'Result = LoadData("PARAMETROS_INVE", "NU_DECVAL_PINV", NUL$, Arr)
    Result = LoadData("PARAMETROS_INVE", "NU_DECVAL_PINV,NU_DECPORC_PINV,NU_DECCANT_PINV", NUL$, Arr) 'T9979 PNC
    If Encontro Then
        If Arr(0) > 2 Then
             Result = DoUpdate("PARAMETROS_INVE", "NU_DECVAL_PINV=2", NUL$)
        End If
        'T9979 PNC INICIO
        If Arr(1) > 2 Then
             Result = DoUpdate("PARAMETROS_INVE", "NU_DECPORC_PINV=2", NUL$)
        End If
        If Arr(2) > 2 Then
             Result = DoUpdate("PARAMETROS_INVE", "NU_DECCANT_PINV=2", NUL$)
        End If
    End If
    'T9979 PNC FIN
End If


If Result <> FAIL Then
        Result = DoUpdate("PARAMETROS_INVE", "NU_NVERDB_PINV='7.3.0'", NUL$)
      End If
 
'FIN JAGS T9979
End Sub


'---------------------------------------------------------------------------------------
' Procedure : Actualizar_7_4
' DateTime  : 17/09/2012
' Author    : Angie Galicia
' Purpose   : ACTUALIZACION DE BASE DE DATOS
'---------------------------------------------------------------------------------------

Sub Actualizar_7_4()
Result = SUCCEED

If Result <> FAIL Then Result = DoUpdate("PARAMETROS_INVE", "NU_NVERDB_PINV='7.4.0'", NUL$)

End Sub


'---------------------------------------------------------------------------------------
' Procedure : Actualizar_7_5
' DateTime  : 01/03/2013
' Author    : John Rodriguez
' Purpose   : ACTUALIZACION DE BASE DE DATOS
'---------------------------------------------------------------------------------------

Sub Actualizar_7_5()

'If ExisteTABLA("IN_ENCABEZADO") Then
'   Result = ExecSQLCommand("ALTER TABLE IN_ENCABEZADO ALTER COLUMN TX_OBSE_ENCA NVARCHAR (400) NULL")
'End If

Result = SUCCEED

If Result <> FAIL Then Result = DoUpdate("PARAMETROS_INVE", "NU_NVERDB_PINV='7.5.0'", NUL$)

End Sub

'---------------------------------------------------------------------------------------
' Procedure : Actualizar_7_6
' DateTime  : 10/10/2013
' Author    : SINTHIA KATHERINE RODRIGUEZ VALENTIN
' Purpose   : ACTUALIZACION DE BASE DE DATOS T18846
'---------------------------------------------------------------------------------------

Sub Actualizar_7_6()

Result = SUCCEED

If Result <> FAIL Then Result = DoUpdate("PARAMETROS_INVE", "NU_NVERDB_PINV='7.6.0'", NUL$)

End Sub



'---------------------------------------------------------------------------------------
' Procedure : Actualizar_7_7
' DateTime  : 04/3/2014 03:50 p.m.
' Author    : Andres Manrique
' Purpose   : AFMG T21136 ACTUALIZACION DE BASE DE DATOS
'---------------------------------------------------------------------------------------

Sub Actualizar_7_7()
   Result = SUCCEED
   'JLPB T21283 INICIO
   If Result <> FAIL Then
      If ExisteTABLA("USUARIO") Then
         If ExisteCAMPO("USUARIO", "NU_ESTA_USUA") Then Result = DoUpdate("USUARIO", "NU_ESTA_USUA = 0", "NU_ESTA_USUA IS NULL")
      End If
   End If
   'JLPB T21283 FIN
   If Result <> FAIL Then Result = DoUpdate("PARAMETROS_INVE", "NU_NVERDB_PINV='7.7.0'", NUL$)
End Sub
'---------------------------------------------------------------------------------------
' Procedure : Actualizar_7_9
' DateTime  : 13/04/2015 14:38
' Author    : jairo_parra
' Purpose   : JLPB T7307
'---------------------------------------------------------------------------------------
'
Sub Actualizar_7_9()
   Result = SUCCEED
   If Result <> FAIL Then Result = DoUpdate("PARAMETROS_INVE", "NU_NVERDB_PINV='7.9.0'", NUL$)
End Sub

'---------------------------------------------------------------------------------------
' Procedure : Actualizar_8_0
' DateTime  : 19/06/2015 17:08
' Author    : juan_urrego
' Purpose   : JAUM T28370-R22535
'---------------------------------------------------------------------------------------
'
'Sub actualizar_8_0() JAUM T28834 Se deja linea en comentario
Sub Actualizar_8_0() 'JAUM T28834
   'JLPB T29905/R28918 INICIO
   Dim InNumarchivo As Integer 'Contiene el resultado de la apertura de un documento de texto
   Dim StScript As String 'Contiene el script a ejecutar
   Result = SUCCEED
   'JLPB T29905/R28918 FIN
   'JAUM T286444-R27752 Inicio
   If ExisteTABLA("TC_IMPUESTOS") Then
      If Not ExisteCAMPO("TC_IMPUESTOS", "TX_RETEFU_IMPU") Then Result = CrearCampo("TC_IMPUESTOS", "TX_RETEFU_IMPU", 1, 1, 0, False)
   End If
   'JAUM T286444-R27752 Fin
   'JLPB T29905/R28918 INICIO
   If ExisteTABLA("CUENTAS") And Result <> FAIL Then
      'JLPB T30764 INICIO Se deja en comentario las siguientes lineas
      'If Not ExisteCAMPO("CUENTAS", "TX_PUCNIIF_CUEN") And Result <> FAIL Then Result = CrearCampo("CUENTAS", "TX_PUCNIIF_CUEN", "10", 1, 0)
      'If Not ExisteCAMPO("CUENTAS", "TX_NOMNF_CUEN") And Result <> FAIL Then Result = CrearCampo("CUENTAS", "TX_NOMNF_CUEN", "10", 100, 1)
      'If Result <> FAIL Then
      '   Valores = "TX_PUCNIIF_CUEN='2'" & Coma
      '   Valores = Valores & "TX_NOMNF_CUEN=NO_NOMB_CUEN"
      '   Result = DoUpdate("CUENTAS", Valores, NUL$)
      'End If
      If Not ExisteCAMPO("CUENTAS", "TX_PUCNIIF_CUEN") And Result <> FAIL Then
         Result = CrearCampo("CUENTAS", "TX_PUCNIIF_CUEN", "10", 1, 0)
         If Result <> FAIL Then
            Valores = "TX_PUCNIIF_CUEN='2'"
            Result = DoUpdate("CUENTAS", Valores, NUL$)
         End If
      End If
      If Not ExisteCAMPO("CUENTAS", "TX_NOMNF_CUEN") And Result <> FAIL Then
         Result = CrearCampo("CUENTAS", "TX_NOMNF_CUEN", "10", 100, 1)
         If Result <> FAIL Then
            Valores = "TX_NOMNF_CUEN=NO_NOMB_CUEN"
            Result = DoUpdate("CUENTAS", Valores, NUL$)
         End If
      End If
      'JLPB T30764 FIN
   End If
   If Not ExisteTABLA("HOM_PCNF") And Result <> FAIL Then Result = CrearTabla("HOM_PCNF", "TX_CNIIF_PCNF, 10, 15, 0")
   If ExisteTABLA("HOM_PCNF") And Result <> FAIL Then
      If Not ExisteCAMPO("HOM_PCNF", "TX_CPUNI_PCNF") And Result <> FAIL Then Result = CrearCampo("HOM_PCNF", "TX_CPUNI_PCNF", "10", 15, 0)
      If Not ExisteCAMPO("HOM_PCNF", "FE_FECHM_PCNF") And Result <> FAIL Then Result = CrearCampo("HOM_PCNF", "FE_FECHM_PCNF", "8", 10, 0)
      If Not ExisteCAMPO("HOM_PCNF", "NU_ID_PCNF") And Result <> FAIL Then Result = CrearCampo("HOM_PCNF", "NU_ID_PCNF", "4", 1, 0, True)
      If BuscarIndice("HOM_PCNF", "TX_CPUNI_PCNF") <> "PK_TX_CPUNI_PCNF" And Result <> FAIL Then Result = CrearIndice("HOM_PCNF", "PK_TX_CPUNI_PCNF", "TX_CPUNI_PCNF", True)
      If BuscarRelacion("CUENTAS", "HOM_PCNF", "TX_CNIIF_PCNF") <> "HOM_PCNF_CUENTAS1" And Result <> FAIL Then Result = CrearRelacion("HOM_PCNF_CUENTAS1", "TX_CNIIF_PCNF", "CD_CODI_CUEN", "HOM_PCNF", "CUENTAS")
      If BuscarRelacion("CUENTAS", "HOM_PCNF", "TX_CPUNI_PCNF") <> "HOM_PCNF_CUENTAS2" And Result <> FAIL Then Result = CrearRelacion("HOM_PCNF_CUENTAS2", "TX_CPUNI_PCNF", "CD_CODI_CUEN", "HOM_PCNF", "CUENTAS")
   End If
      
   If Dir$(App.Path & "\SP_Crear_Movimiento_Contable_NIIF.txt") <> "" And Result <> FAIL Then
      InNumarchivo = FreeFile
      Open App.Path & "\SP_Crear_Movimiento_Contable_NIIF.txt" For Input As InNumarchivo
      StScript = Input(LOF(InNumarchivo), #InNumarchivo)
      Close InNumarchivo
      If ExisteStoreProcedure("PA_Crear_Movimiento_Contable_niif") Then
         Call ExecSQLCommand("DROP PROCEDURE PA_Crear_Movimiento_Contable_niif")
      End If
      If Result <> FAIL Then Result = ExecSQLCommand(StScript)
      If Result = FAIL Then Call Mensaje1("El procedimiento" & " " & "PA_Crear_Movimiento_Contable_niif" & " no se creo correctamente", 1)
   Else
      Call Mensaje1("El archivo" & " " & "PA_Crear_Movimiento_Contable_niif.txt" & " no existe.", 1)
   End If
   
   If Dir$(App.Path & "\SP Crear Movimiento Contable.txt") <> "" And Result <> FAIL Then
      InNumarchivo = FreeFile
      Open App.Path & "\SP Crear Movimiento Contable.txt" For Input As InNumarchivo
      StScript = Input(LOF(InNumarchivo), #InNumarchivo)
      Close InNumarchivo
      If ExisteStoreProcedure("PA_Crear_Movimiento_Contable") Then
         Call ExecSQLCommand("DROP PROCEDURE PA_Crear_Movimiento_Contable")
      End If
      If Result <> FAIL Then Result = ExecSQLCommand(StScript)
      If Result = FAIL Then Call Mensaje1("El procedimiento" & " " & "PA_Crear_Movimiento_Contable" & " no se creo correctamente", 1)
   Else
      Call Mensaje1("El archivo" & " " & "SP Crear Movimiento Contable.txt" & " no existe.", 1)
   End If
   'JLPB T29905/R28918 FIN
   'JAUM T29815 Inicio
   If Not ExisteTABLA("IN_VAL_IMDE") Then
      If Result <> FAIL Then Result = CrearTabla("IN_VAL_IMDE", "NU_AUTO_COMP_IMDE,4,1,0")
      If Result <> FAIL Then Result = CrearCampo("IN_VAL_IMDE", "NU_AUTO_ENCA_IMDE", 4, 1, 0)
      If Result <> FAIL Then Result = CrearCampo("IN_VAL_IMDE", "NU_VALOR_IMDE", 7, 1, 0)
      If Result <> FAIL Then Result = CrearCampo("IN_VAL_IMDE", "NU_PPTO_IMDE", 1, 1, 0)
      If Result <> FAIL Then Result = CrearIndice("IN_VAL_IMDE", "PK_IN_VAL_IMDE", "NU_AUTO_COMP_IMDE, NU_AUTO_ENCA_IMDE", True)
      If Result <> FAIL Then Result = CrearRelacion("R_IN_VAL_IMDEIN_COMPRA", "NU_AUTO_COMP_IMDE", "NU_AUTO_COMP", "IN_VAL_IMDE", "IN_COMPRA")
      If Result <> FAIL Then Result = CrearRelacion("R_IN_VAL_IMDEIN_ENCABEZADO", "NU_AUTO_ENCA_IMDE", "NU_AUTO_ENCA", "IN_VAL_IMDE", "IN_ENCABEZADO")
   End If
   'JAUM T29815 Fin
   Result = SUCCEED
   If Result <> FAIL Then Result = DoUpdate("PARAMETROS_INVE", "NU_NVERDB_PINV='8.0.0'", NUL$)
End Sub
'---------------------------------------------------------------------------------------
' Procedure : Actualizar_7_8
' DateTime  : 14/10/2014 14:17
' Author    : sinthia_rodriguez
' Purpose   : T24261/R22366
'---------------------------------------------------------------------------------------
'
Sub Actualizar_7_8()
   'JLPB T26466 INICIO
   Dim StMod() As String 'ALMACENA LOS CODIGOS DE LOS MODULOS Y SU DESCRIPCI�N
   Dim StTexto As String  'ALMACENA LOS CODIGOS DE LOS MODULOS Y SU DESCRIPCI�N
   Dim InI As Integer 'VARIABLE UTILIZADA COMO CONTADOR
   'JLPB T26466 FIN
   Result = SUCCEED
   
   If ExisteTABLA("PARAM_NIIF") Then
      If Not ExisteCAMPO("PARAM_NIIF", "NU_COSTONIIF_PANI") Then Result = CrearCampo("PARAM_NIIF", "NU_COSTONIIF_PANI", 1, 50, 1, False)
   Else
      Result = CrearTabla("PARAM_NIIF", "NU_CODMOD_PANI, 2, 4, 0")
      If Result <> FAIL Then Result = CrearCampo("PARAM_NIIF", "NU_COSTONIIF_PANI", 1, 50, 1, False)
      If Result <> FAIL Then Result = CrearIndice("PARAM_NIIF", "PK_NU_CODMOD_PANI", "NU_CODMOD_PANI", True)
      
      Valores = "NU_CODMOD_PANI = 1"
      If Result <> FAIL Then Result = DoInsertSQL("PARAM_NIIF", Valores)
      
      Valores = "NU_CODMOD_PANI = 2"
      If Result <> FAIL Then Result = DoInsertSQL("PARAM_NIIF", Valores)
      
      Valores = "NU_CODMOD_PANI = 3"
      If Result <> FAIL Then Result = DoInsertSQL("PARAM_NIIF", Valores)
      
      Valores = "NU_CODMOD_PANI = 4"
      If Result <> FAIL Then Result = DoInsertSQL("PARAM_NIIF", Valores)
      
      Valores = "NU_CODMOD_PANI = 5"
      If Result <> FAIL Then Result = DoInsertSQL("PARAM_NIIF", Valores)
      
      Valores = "NU_CODMOD_PANI = 6"
      If Result <> FAIL Then Result = DoInsertSQL("PARAM_NIIF", Valores)
      
      Valores = "NU_CODMOD_PANI = 7"
      If Result <> FAIL Then Result = DoInsertSQL("PARAM_NIIF", Valores)
      
      Valores = "NU_CODMOD_PANI = 8"
      If Result <> FAIL Then Result = DoInsertSQL("PARAM_NIIF", Valores)
      
      Valores = "NU_CODMOD_PANI = 9"
      If Result <> FAIL Then Result = DoInsertSQL("PARAM_NIIF", Valores)
      
   End If
   
   If Result <> FAIL Then Result = DoUpdate("PARAM_NIIF", "NU_COSTONIIF_PANI=0", "NU_CODMOD_PANI = 6") 'SKRV T24766
   
   If Not ExisteTABLA("VALORMERINV") Then

      Result = CrearTabla("VALORMERINV", "CD_CODI_VAMEIN, 4, 50, 0")
      If Result <> FAIL Then Result = CrearCampo("VALORMERINV", "NU_ARTI_VAMEIN", 10, 16, 0, False)

      If Result <> FAIL Then Result = CrearIndice("VALORMERINV", "PK_CD_CODI_VAMEIN", "CD_CODI_VAMEIN, NU_ARTI_VAMEIN", True)
'      SKRV T24951 COMENTARIO /*
'      If Result <> FAIL Then Result = CrearCampo("VALORMERINV", "FE_FECH_VAMEIN", 8, 50, 1, False)
'      If Result <> FAIL Then Result = CrearCampo("VALORMERINV", "NU_VALMER_VAMEIN", 7, 50, 1, False)
'      If Result <> FAIL Then Result = CrearCampo("VALORMERINV", "NU_FALMA_VAMEIN", 2, 50, 1, False)
'      */
      'INICIO SKRV T24951
      If Result <> FAIL Then Result = CrearCampo("VALORMERINV", "FE_FECH_VAMEIN", 8, 50, 0, False)
      If Result <> FAIL Then Result = CrearCampo("VALORMERINV", "NU_VALMER_VAMEIN", 7, 50, 0, False)
      If Result <> FAIL Then Result = CrearCampo("VALORMERINV", "NU_FALMA_VAMEIN", 1, 50, 0, False)
      'FIN SKRV T24951
   End If
   
   'MODU_MOV_NIIF
   If Not ExisteTABLA("MODU_MOV_NIIF") Then
      Result = CrearTabla("MODU_MOV_NIIF", "NU_CODMO_MODU , 10, 5, 0")
      If Result <> FAIL Then Result = CrearCampo("MODU_MOV_NIIF ", "TX_DESCM_MODU", 10, 250, 1, False)
      If Result <> FAIL Then Result = CrearIndice("MODU_MOV_NIIF", "PK_NU_CODMO_MODU", "NU_CODMO_MODU", True)
   'INICIO SKRV T24951
   'End If COMENTARIO
   'JLPB T26466 INICIO EL SIGUIENTE BLOQUE SE DEJA EN COMENTARIO
'      'insert
'      Valores = " INSERT INTO MODU_MOV_NIIF"
'      Valores = Valores & "( NU_CODMO_MODU,TX_DESCM_MODU "
'      Valores = Valores & ") VALUES "
'      'campos
'      Valores = Valores & " ('01','Activos Fijos'),"
'      Valores = Valores & " ('02','Cartera'),"
'      Valores = Valores & " ('03','Contabilidad'),"
'      Valores = Valores & " ('04','Costos'),"
'      Valores = Valores & " ('05','Cuentas x pagar'),"
'      Valores = Valores & " ('06','Inventarios'),"
'      Valores = Valores & " ('07','Nomina'),"
'      Valores = Valores & " ('08','Pacientes'),"
'      Valores = Valores & " ('09','Presupuesto'),"
'
'      Valores = Valores & " ('0601','Entrada de Almac�n'),"
'      Valores = Valores & " ('0602','Salidas de Dependencia'),"
'      Valores = Valores & " ('0603','Venta de Inventario'),"
'      Valores = Valores & " ('0604','Devoluci�n de Clientes'),"
'      Valores = Valores & " ('0605','Devoluci�n de dependencias'),"
'      Valores = Valores & " ('0606','Traslados entre Bodegas'),"
'      Valores = Valores & " ('0607','Despacho de Inventario'),"
'      Valores = Valores & " ('0608','Devoluci�n de Despacho'),"
'      Valores = Valores & " ('0609','Compra Inventarios')"
'      'SKRV T24951 INICIO
'      Valores = Valores & ", ('0610','Salidas o Bajas de Almac�n'),"
'      Valores = Valores & " ('0611','Devoluci�n a Proveedor'),"
'      Valores = Valores & " ('0612','Aprovechamientos/Donaciones'),"
'      Valores = Valores & " ('0613','Ajustes por Inflaci�n'),"
'      Valores = Valores & " ('0614','Devoluci�n Baja de Consumo')"
'      'SKRV T24951 FIN
'      If Result <> FAIL Then Result = ExecSQLCommand(Valores)
'
'   Else
'      If CInt(fnDevDato("MODU_MOV_NIIF", "COUNT(NU_CODMO_MODU)", "NU_CODMO_MODU like '06%'")) = 1 Then 'debe ser igual a uno porque (06, 'Inventarios') ya debe existir
'
'         Valores = " INSERT INTO MODU_MOV_NIIF"
'         Valores = Valores & "( NU_CODMO_MODU,TX_DESCM_MODU "
'         Valores = Valores & ") VALUES "
'
'         Valores = Valores & " ('0601','Entrada de Almac�n'),"
'         Valores = Valores & " ('0602','Salidas de Dependencia'),"
'         Valores = Valores & " ('0603','Venta de Inventario'),"
'         Valores = Valores & " ('0604','Devoluci�n de Clientes'),"
'         Valores = Valores & " ('0605','Devoluci�n de dependencias'),"
'         Valores = Valores & " ('0606','Traslados entre Bodegas'),"
'         Valores = Valores & " ('0607','Despacho de Inventario'),"
'         Valores = Valores & " ('0608','Devoluci�n de Despacho'),"
'         Valores = Valores & " ('0609','Compra Inventarios')"
'         'SKRV T24951 INICIO
'         Valores = Valores & ", ('0610','Salidas o Bajas de Almac�n'),"
'         Valores = Valores & " ('0611','Devoluci�n a Proveedor'),"
'         Valores = Valores & " ('0612','Aprovechamientos/Donaciones'),"
'         Valores = Valores & " ('0613','Ajustes por Inflaci�n'),"
'         Valores = Valores & " ('0614','Devoluci�n Baja de Consumo')"
'         'SKRV T24951 FIN
'         If Result <> FAIL Then Result = ExecSQLCommand(Valores)
'      End If
   'JLPB T26466 FIN
   End If
   'FIN SKRV T24951
   'JLPB T26466 INICIO
   StTexto = "01.Activos Fijos,02.Cartera,03.Contabilidad,04.Costos,05.Cuentas x Pagar,06.Inventarios,07.Nomina,08.Pacientes,09.Presupuesto,"
   StTexto = StTexto & "0601.Entrada de Almac�n,0602.Salidas de Dependencia,0603.Venta de Inventario,0604.Devoluci�n de Clientes,0605.Devoluci�n de dependencias,"
   StTexto = StTexto & "0606.Traslados entre Bodegas,0607.Despacho de Inventario,0608.Devoluci�n de Despacho,0609.Compra Inventarios,0610.Salidas o Bajas de Almac�n,"
   StTexto = StTexto & "0611.Devoluci�n a Proveedor,0612.Aprovechamientos/Donaciones,0613.Ajustes por Inflaci�n,0614.Devoluci�n Baja de Consumo"
   StMod = Split(StTexto, ",")
   If Result <> FAIL Then
      For InI = 0 To UBound(StMod)
         Valores = Comi & Mid(StMod(InI), 1, InStr(StMod(InI), ".") - 1) & Comi & Coma
         Valores = Valores & Comi & Mid(StMod(InI), InStr(StMod(InI), ".") + 1, Len(StMod(InI))) & Comi
         If fnDevDato("MODU_MOV_NIIF", "COUNT(*)", "NU_CODMO_MODU=" & Comi & Mid(StMod(InI), 1, InStr(StMod(InI), ".") - 1) & Comi, True) = 0 Then
            Result = DoInsert("MODU_MOV_NIIF", Valores)
         End If
      Next
   End If
   'JLPB T26466 FIN
   
   Valores = "TX_DESC_FORM='Valor del Mercado NIIF',TX_FORM_FORM='FrmMerNiif',TX_TIPO_FORM='FRM'"
   Result = DoInsertSQL("FORMULARIO", Valores)
   If Result <> FAIL Then
      ReDim Arr(0)
      Result = LoadData("FORMULARIO", "NU_AUTO_FORM", "TX_FORM_FORM='FrmMerNiif'", Arr)
      If Result <> FAIL And Encontro Then
         Valores = "TX_CODPADR_OPCI='05',NU_AUTO_FORM_OPCI=" & Arr(0) & ","
         ReDim Arr(0)
         Result = LoadData("OPCION", "max(TX_CODI_OPCI)", "TX_CODI_OPCI LIKE '05%'", Arr)
         If Result <> FAIL Then
            If Arr(0) = NUL$ Then Arr(0) = "0500"
            Valores = Valores & "TX_CODI_OPCI='0" & CInt(Arr(0)) + 1 & "', TX_DESC_OPCI='VALOR DEL MERCADO NIIF',"
            Result = DoInsertSQL("OPCION", Valores)
            If Result <> FAIL Then Call CrearOpcion("0" & CStr(CInt(Arr(0)) + 1))
         End If
      End If
   End If
   
   If Result <> FAIL Then Result = DoUpdate("PARAMETROS_INVE", "NU_NVERDB_PINV='7.8.0'", NUL$)
End Sub

'---------------------------------------------------------------------------------------
' Procedure : AdjuntarImagen
' DateTime  : 04/07/2018 11:00
' Author    : daniel_mesa
' Purpose   : DRMG T44173-R41288 GUARDA UNA IMAGEN EN LA BD
'---------------------------------------------------------------------------------------
'
Function AdjuntarImagen(TableName As String, CampoImag As String, Imagen As Image, Condi As String, Optional BoSaveImg As Boolean = False) As Integer
   Dim Cmd As String 'ALMACENA SENTENCIAS SQL

   AdjuntarImagen = 0
   If MotorBD <> "SQL" And (Not BoSaveImg) Then Exit Function
   If Imagen.Picture = 0 Then Call Mensaje1("Se requiere una imagen.", 3): Exit Function
    
   On Error GoTo ExecError
   
   Set BDSSIMG = New ADODB.Recordset

   CampoImag = Trim(CampoImag)
   If (Len(CampoImag) > 0) Then
      Cmd = "SELECT " & CampoImag & " FROM " & TableName & IIf(Len(Condi) > 0, " WHERE " & Condi, NUL$)
      Debug.Print Cmd
      If MotorBD = "SQL" Or BoSaveImg Then
         BDSSIMG.Open Cmd, BD(BDCurCon), adOpenDynamic, adLockOptimistic

         If BDSSIMG.BOF <> True And BDSSIMG.EOF <> True Then  'Si no esta vacio
            If GuardarImagADO(BDSSIMG(CampoImag), Imagen) = 1 Then
               BDSSIMG.Update
               BDSSIMG.Close
               Debug.Print Cmd
               AdjuntarImagen = 1
            End If
         End If
      End If
   End If
   
   Exit Function
   
ExecError:
   Call ConvertErr
   FrmCmd.TxtCmd.Text = Cmd
   Exit Function
   Resume
End Function


