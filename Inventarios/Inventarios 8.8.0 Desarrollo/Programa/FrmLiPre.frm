VERSION 5.00
Object = "{00025600-0000-0000-C000-000000000046}#5.2#0"; "Crystl32.OCX"
Object = "{0BA686C6-F7D3-101A-993E-0000C0EF6F5E}#1.0#0"; "THREED32.OCX"
Begin VB.Form FrmLisPrecio 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Listado de Precio de los Art�culos"
   ClientHeight    =   3060
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   3540
   Icon            =   "FrmLiPre.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   ScaleHeight     =   3060
   ScaleWidth      =   3540
   Begin VB.ComboBox CboTarifa 
      Height          =   315
      Left            =   840
      TabIndex        =   8
      Text            =   "Combo1"
      Top             =   1680
      Width           =   2655
   End
   Begin VB.Frame FraArticulos 
      Caption         =   "Rango de Art�culos"
      Height          =   1335
      Left            =   120
      TabIndex        =   0
      Top             =   120
      Width           =   3375
      Begin VB.TextBox TxtRango 
         Height          =   285
         Index           =   1
         Left            =   840
         MaxLength       =   16
         TabIndex        =   2
         Text            =   "ZZZZZZZZZZZZZZZZ"
         Top             =   840
         Width           =   1815
      End
      Begin VB.TextBox TxtRango 
         Height          =   285
         Index           =   0
         Left            =   840
         MaxLength       =   16
         TabIndex        =   1
         Text            =   "0"
         Top             =   360
         Width           =   1815
      End
      Begin Threed.SSCommand CmdSelec 
         Height          =   375
         Index           =   1
         Left            =   2640
         TabIndex        =   3
         ToolTipText     =   "Selecci�n de Art�culos"
         Top             =   840
         Width           =   615
         _Version        =   65536
         _ExtentX        =   1085
         _ExtentY        =   661
         _StockProps     =   78
         BevelWidth      =   0
         RoundedCorners  =   0   'False
         Outline         =   0   'False
         Picture         =   "FrmLiPre.frx":0442
      End
      Begin Threed.SSCommand CmdSelec 
         Height          =   375
         Index           =   0
         Left            =   2640
         TabIndex        =   4
         ToolTipText     =   "Selecci�n de Art�culos"
         Top             =   360
         Width           =   615
         _Version        =   65536
         _ExtentX        =   1085
         _ExtentY        =   661
         _StockProps     =   78
         BevelWidth      =   0
         RoundedCorners  =   0   'False
         Outline         =   0   'False
         Picture         =   "FrmLiPre.frx":0894
      End
      Begin VB.Label LblHasta 
         AutoSize        =   -1  'True
         Caption         =   "Final : "
         Height          =   195
         Left            =   240
         TabIndex        =   6
         Top             =   840
         Width           =   465
      End
      Begin VB.Label LblInicio 
         AutoSize        =   -1  'True
         Caption         =   "Inicial : "
         Height          =   195
         Left            =   240
         TabIndex        =   5
         Top             =   360
         Width           =   540
      End
   End
   Begin Crystal.CrystalReport Crys_Listar 
      Left            =   0
      Top             =   0
      _ExtentX        =   741
      _ExtentY        =   741
      _Version        =   348160
      WindowLeft      =   20
      WindowWidth     =   750
      WindowHeight    =   450
      WindowTitle     =   "Consolidado de Existencias"
      WindowControlBox=   -1  'True
      WindowMaxButton =   -1  'True
      WindowMinButton =   -1  'True
      WindowControls  =   -1  'True
      PrintFileLinesPerPage=   60
      WindowShowCloseBtn=   -1  'True
      WindowShowSearchBtn=   -1  'True
      WindowShowPrintSetupBtn=   -1  'True
      WindowShowRefreshBtn=   -1  'True
   End
   Begin Threed.SSCommand SCmd_Options 
      Height          =   735
      Index           =   1
      Left            =   1320
      TabIndex        =   9
      Top             =   2160
      Width           =   855
      _Version        =   65536
      _ExtentX        =   1508
      _ExtentY        =   1296
      _StockProps     =   78
      Caption         =   "&Imprimir"
      Picture         =   "FrmLiPre.frx":0CE6
   End
   Begin Threed.SSCommand SCmd_Options 
      Height          =   735
      Index           =   2
      Left            =   2400
      TabIndex        =   10
      Top             =   2160
      Width           =   855
      _Version        =   65536
      _ExtentX        =   1508
      _ExtentY        =   1296
      _StockProps     =   78
      Caption         =   "&Salir"
      Picture         =   "FrmLiPre.frx":1228
   End
   Begin Threed.SSCommand SCmd_Options 
      Height          =   735
      Index           =   0
      Left            =   240
      TabIndex        =   11
      Top             =   2160
      Width           =   855
      _Version        =   65536
      _ExtentX        =   1508
      _ExtentY        =   1296
      _StockProps     =   78
      Caption         =   "&Pantalla"
      Picture         =   "FrmLiPre.frx":1936
   End
   Begin Threed.SSCommand SCmd_Options 
      Height          =   375
      Index           =   3
      Left            =   0
      TabIndex        =   12
      Top             =   0
      Visible         =   0   'False
      Width           =   975
      _Version        =   65536
      _ExtentX        =   1720
      _ExtentY        =   661
      _StockProps     =   78
   End
   Begin Threed.SSCommand SCmd_Options 
      Height          =   375
      Index           =   4
      Left            =   1080
      TabIndex        =   13
      Top             =   0
      Visible         =   0   'False
      Width           =   975
      _Version        =   65536
      _ExtentX        =   1720
      _ExtentY        =   661
      _StockProps     =   78
   End
   Begin VB.Label Label1 
      Caption         =   "&Tarifa:"
      Height          =   255
      Left            =   120
      TabIndex        =   7
      Top             =   1680
      Width           =   735
   End
End
Attribute VB_Name = "FrmLisPrecio"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Dim CodTari()

Private Sub CboTarifa_KeyPress(KeyAscii As Integer)
   Call Cambiar_Enter(KeyAscii)
End Sub

Private Sub Form_Load()
 Call CenterForm(MDI_Inventarios, Me)
 Call Leer_Permisos("03024", SCmd_Options(3), SCmd_Options(3), SCmd_Options(4))
 Crys_Listar.Formulas(0) = "ENTIDAD='" & Entidad & Comi
 Crys_Listar.Formulas(2) = "NIT= 'NIT. " & Nit & Comi
 Crys_Listar.Formulas(1) = "HORA= 'Hora: " & Format(Now, "hh:mm") & Comi
 Crys_Listar.Formulas(3) = "USUARIO = 'Usuario:" & UserId & Comi
 Crys_Listar.Connect = ReportConnect
 Result = loadctrl("TARIFAS", "DE_DESC_TARI", "CD_CODI_TARI", Me.CboTarifa, CodTari(), NUL$)
 CboTarifa.AddItem "TODOS"
 CboTarifa.ListIndex = 0
End Sub
Private Sub TxtRango_GotFocus(Index As Integer)
    TxtRango(Index).SelStart = 0
    TxtRango(Index).SelLength = Len(TxtRango(Index).Text)
End Sub
Private Sub TxtRango_KeyPress(Index As Integer, KeyAscii As Integer)
    Call ValKeyAlfaNum(KeyAscii)
    If KeyAscii = 13 Then
     If Index = 0 Then
        TxtRango(1).SetFocus
     Else
        SCmd_Options(0).SetFocus
     End If
    End If
End Sub
Private Sub TxtRango_KeyDown(Index As Integer, KeyCode As Integer, Shift As Integer)
 Select Case Index
    Case 0
         If KeyCode = 40 Then
            TxtRango(1).SetFocus
         End If
    Case 1
         If KeyCode = 38 Then
            TxtRango(0).SetFocus
         End If
         If KeyCode = 40 Then
            SCmd_Options(0).SetFocus
         End If
 End Select
End Sub
Private Sub CmdSelec_Click(Index As Integer)
    Codigo = NUL$
    Codigo = Seleccion("ARTICULO", "NO_NOMB_ARTI", "CD_CODI_ARTI,NO_NOMB_ARTI", "ARTICULOS", NUL$)
    If Codigo <> NUL$ Then TxtRango(Index) = Codigo
    TxtRango(Index).SetFocus
End Sub
Private Sub SCmd_Options_Click(Index As Integer)
    Screen.MousePointer = 11
    Select Case Index
        '''True = pantalla
        '''False = impresora
        Case 0: Call Informe(True)
        Case 1: Call Informe(False)
        Case 2: Unload Me
    End Select
    Screen.MousePointer = 0
End Sub
Private Sub Informe(ByVal pantalla As Boolean)
  If TxtRango(0) > TxtRango(1) Or TxtRango(1) = "" Then
     Call Mensaje1("Revise rango de art�culos", 3)
     Exit Sub
  End If
  Crys_Listar.ReportFileName = DirTrab + "PRECARTI.RPT"
  Crys_Listar.SortFields(0) = "+{R_TARI_ARTI.CD_ARTI_TAAR}"
  Crys_Listar.SelectionFormula = "{R_TARI_ARTI.CD_ARTI_TAAR} in '" & TxtRango(0) & "' to '" & TxtRango(1) & Comi
  If CboTarifa.ListIndex = -1 Then Exit Sub
  If CboTarifa.Text <> "TODOS" Then
    Crys_Listar.SelectionFormula = Crys_Listar.SelectionFormula & _
                        " and {R_TARI_ARTI.CD_TARI_TAAR} = '" & CodTari(CboTarifa.ListIndex) & Comi
  End If
  Debug.Print Crys_Listar.SelectionFormula
  Crys_Listar.Connect = ReportConnect
  If SCmd_Options(4).Enabled = False Then
     Call Mensaje1("Permiso Denegado (Opc.03024)", 3)
  Else
     If pantalla = True Then
        Crys_Listar.Destination = crptToWindow
     Else
        Crys_Listar.Destination = crptToPrinter
     End If
     Crys_Listar.Action = 1
  End If
End Sub

