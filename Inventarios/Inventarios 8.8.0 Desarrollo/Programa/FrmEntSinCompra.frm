VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Object = "{C932BA88-4374-101B-A56C-00AA003668DC}#1.1#0"; "MSMASK32.OCX"
Object = "{0BA686C6-F7D3-101A-993E-0000C0EF6F5E}#1.0#0"; "THREED32.OCX"
Begin VB.Form FrmEntSinCompra 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Entradas sin Compra"
   ClientHeight    =   7605
   ClientLeft      =   45
   ClientTop       =   435
   ClientWidth     =   8445
   Icon            =   "FrmEntSinCompra.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   7605
   ScaleWidth      =   8445
   Begin VB.Frame Frame1 
      Height          =   1065
      Left            =   180
      TabIndex        =   7
      Top             =   6300
      Width           =   4695
      Begin VB.CheckBox Chkfecrango 
         Caption         =   "Condicionar las Fechas del Reporte"
         Height          =   375
         Left            =   120
         TabIndex        =   3
         Top             =   120
         Width           =   3015
      End
      Begin MSMask.MaskEdBox txtFCHASTA 
         Height          =   315
         Left            =   3210
         TabIndex        =   5
         Top             =   540
         Width           =   1215
         _ExtentX        =   2143
         _ExtentY        =   556
         _Version        =   393216
         PromptChar      =   "_"
      End
      Begin MSMask.MaskEdBox txtFCDESDE 
         Height          =   315
         Left            =   990
         TabIndex        =   4
         Top             =   540
         Width           =   1215
         _ExtentX        =   2143
         _ExtentY        =   556
         _Version        =   393216
         PromptChar      =   "_"
      End
      Begin VB.Label Label6 
         Alignment       =   1  'Right Justify
         Caption         =   "Hasta:"
         Height          =   255
         Left            =   2430
         TabIndex        =   9
         Top             =   600
         Width           =   750
      End
      Begin VB.Label Label7 
         Alignment       =   1  'Right Justify
         Caption         =   "Desde:"
         Height          =   255
         Left            =   150
         TabIndex        =   8
         Top             =   600
         Width           =   750
      End
   End
   Begin VB.CheckBox chkTERCEROS 
      Caption         =   "Todos los Terceros"
      Height          =   375
      Left            =   120
      TabIndex        =   0
      Top             =   180
      Width           =   3015
   End
   Begin MSComctlLib.ListView LstBODEGAS 
      Height          =   2535
      Left            =   150
      TabIndex        =   2
      Top             =   3570
      Width           =   7875
      _ExtentX        =   13891
      _ExtentY        =   4471
      View            =   3
      LabelEdit       =   1
      MultiSelect     =   -1  'True
      LabelWrap       =   -1  'True
      HideSelection   =   0   'False
      FullRowSelect   =   -1  'True
      GridLines       =   -1  'True
      _Version        =   393217
      ForeColor       =   -2147483640
      BackColor       =   -2147483643
      BorderStyle     =   1
      Appearance      =   1
      NumItems        =   0
   End
   Begin MSComctlLib.ListView LstTERCEROS 
      Height          =   2655
      Left            =   150
      TabIndex        =   1
      Top             =   690
      Width           =   7875
      _ExtentX        =   13891
      _ExtentY        =   4683
      View            =   3
      LabelEdit       =   1
      MultiSelect     =   -1  'True
      LabelWrap       =   -1  'True
      HideSelection   =   0   'False
      AllowReorder    =   -1  'True
      FullRowSelect   =   -1  'True
      GridLines       =   -1  'True
      _Version        =   393217
      ForeColor       =   -2147483640
      BackColor       =   -2147483643
      BorderStyle     =   1
      Appearance      =   1
      NumItems        =   2
      BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Text            =   "Nombre del Art�culo"
         Object.Width           =   7761
      EndProperty
      BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   1
         Text            =   "C�digo"
         Object.Width           =   3881
      EndProperty
   End
   Begin Threed.SSCommand CmdImprimir 
      Height          =   765
      Index           =   1
      Left            =   7200
      TabIndex        =   6
      Top             =   6570
      Width           =   855
      _Version        =   65536
      _ExtentX        =   1508
      _ExtentY        =   1349
      _StockProps     =   78
      Caption         =   "&IMPRIMIR"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   6
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      RoundedCorners  =   0   'False
      Picture         =   "FrmEntSinCompra.frx":058A
   End
   Begin MSComctlLib.ProgressBar pgbLLEVO 
      Height          =   255
      Left            =   3780
      TabIndex        =   10
      Top             =   6180
      Visible         =   0   'False
      Width           =   4455
      _ExtentX        =   7858
      _ExtentY        =   450
      _Version        =   393216
      Appearance      =   1
      Max             =   1
   End
End
Attribute VB_Name = "FrmEntSinCompra"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit 'NYCM M2432

Dim Dueno As New ElTercero
Dim Tercero As ElTercero, Docume As ElEncabezado
Public OpcCod As String   'Opci�n de seguridad

Private Sub Chkfecrango_Click() 'NYCM R1235
  txtFCDESDE.Enabled = True
  txtFCHASTA.Enabled = True
  txtFCHASTA = Hoy
End Sub

Private Sub Chkfecrango_KeyPress(KeyAscii As Integer)
Call Cambiar_Enter(KeyAscii) 'NYCM M2432
End Sub

Private Sub chkTERCEROS_KeyPress(KeyAscii As Integer)
Call Cambiar_Enter(KeyAscii) 'NYCM M2432
End Sub

Private Sub CmdImprimir_Click(Index As Integer) 'NYCM R1235
    Dim SQL As String
    Call Limpiar_CrysListar
    Dim Renglon As MSComctlLib.ListItem, txtDR As String * 1
    Dim strTERCEROS As String, strBODEGAS As String
    Dim strTITRANGO As String, strTITFECHA As String
    Dim consulta As String
    Dim consultaB As String
    
    MDI_Inventarios.Crys_Listar.Formulas(0) = "ENTIDAD='" & Trim(Dueno.Nombre) & Comi
    MDI_Inventarios.Crys_Listar.Formulas(1) = "NIT='" & Trim(Dueno.Nit) & "'"
    MDI_Inventarios.Crys_Listar.Formulas(2) = "USUARIO='" & UserId & Comi
               
           
    SQL = "SELECT DISTINCT NU_AUTO_ENCA,NU_COMP_ENCA,FE_CREA_ENCA,TX_NOMB_BODE,CD_CODI_TERC,NO_NOMB_TERC "
    SQL = SQL & "INTO IN_ENCABEZADO_TEMP "
    'SQL = SQL & "FROM IN_ENCABEZADO,IN_BODEGA,TERCERO,IN_R_COMP_ENCA, IN_COMPROBANTE,IN_DOCUMENTO "
    'SQL = SQL & "FROM IN_ENCABEZADO,IN_BODEGA,TERCERO,IN_COMPROBANTE,IN_DOCUMENTO "
    'SQL = SQL & "FROM IN_ENCABEZADO,IN_BODEGA,TERCERO,IN_COMPROBANTE,IN_DOCUMENTO,IN_COMPRA "
    SQL = SQL & "FROM IN_ENCABEZADO,IN_BODEGA,TERCERO,IN_COMPROBANTE,IN_DOCUMENTO" 'NMSR M3610
    SQL = SQL & " WHERE NU_AUTO_DOCU_ENCA = NU_AUTO_DOCU "
    SQL = SQL & " AND IN_ENCABEZADO.NU_AUTO_BODEORG_ENCA=IN_BODEGA.NU_AUTO_BODE "
    SQL = SQL & " AND IN_ENCABEZADO.CD_CODI_TERC_ENCA=TERCERO.CD_CODI_TERC "
    'SQL = SQL & "AND IN_ENCABEZADO.NU_AUTO_ENCA NOT IN (SELECT NU_AUTO_ENCA_COEN FROM IN_R_COMP_ENCA)
    SQL = SQL & "AND IN_ENCABEZADO.NU_AUTO_ENCA NOT IN (SELECT NU_AUTO_ENCA_COEN FROM IN_R_COMP_ENCA,IN_COMPRA WHERE NU_AUTO_COMP=NU_AUTO_COMP_COEN AND TX_ESTA_COMP<>2) " 'NYCM M2619
    SQL = SQL & "AND NU_AUTO_DOCU= 3 "
    SQL = SQL & "AND TX_ESTA_ENCA<>'A' "
    
   If Me.chkTERCEROS.Value = 0 Then
        For Each Renglon In Me.LstTERCEROS.ListItems
            If Renglon.Selected Then strTERCEROS = Renglon.Text
        Next
        SQL = SQL & "AND CD_CODI_TERC='" & strTERCEROS & Comi
   End If
    
    'NYCM M2619------------------
    For Each Renglon In Me.lstBODEGAS.ListItems
        If Renglon.Selected Then consultaB = consultaB & IIf(Len(consultaB) > 0, " OR ", "") & " IN_BODEGA.TX_CODI_BODE='" & Renglon.ListSubItems("COD").Text & "'"
        If Renglon.Selected Then strBODEGAS = strBODEGAS & IIf(Len(strBODEGAS) > 0, " OR ", "") & "{IN_BODEGA.TX_CODI_BODE}='" & Renglon.ListSubItems("COD").Text & "'"
    Next

     If Len(consultaB) > 0 Then consultaB = consultaB
     If Len(consulta) > 0 And Len(consultaB) > 0 Then
        consulta = "(" & consulta & ")"
        consulta = consulta & " AND " & consultaB
     Else
        consulta = consulta & " AND " & consultaB
     End If
     If Len(strTERCEROS) > 0 Then strTERCEROS = "(" & strTERCEROS & ")"
     If Len(strBODEGAS) > 0 Then strBODEGAS = "(" & strBODEGAS & ")"
     If Len(strTERCEROS) > 0 And Len(strBODEGAS) > 0 Then
        strTERCEROS = strTERCEROS & " AND " & strBODEGAS
    Else
       strTERCEROS = strTERCEROS & " AND " & strBODEGAS
    End If
    '/--------------------------
    
    If Me.Chkfecrango.Value Then
    SQL = SQL & "AND IN_ENCABEZADO.FE_CREA_ENCA>= " & FFechaCon(txtFCDESDE.Text)
    SQL = SQL & "AND IN_ENCABEZADO.FE_CREA_ENCA<= " & FFechaCon(txtFCHASTA.Text)
    End If
                
    If ExisteTABLA("IN_ENCABEZADO_TEMP") Then Result = EliminarTabla("IN_ENCABEZADO_TEMP")
      consulta = SQL & consulta & "ORDER BY TERCERO.NO_NOMB_TERC ASC "
      Result = ExecSQL(consulta)
            
      Call ListarD("InfEntrasinComp.rpt", "", crptToWindow, "ENTRADAS SIN COMPRA", MDI_Inventarios, True)
    MDI_Inventarios.Crys_Listar.Formulas(0) = ""
    MDI_Inventarios.Crys_Listar.Formulas(1) = ""
    MDI_Inventarios.Crys_Listar.Formulas(2) = ""
End Sub

Private Sub Form_Load() 'NYCM R1235
 Call CenterForm(MDI_Inventarios, Me)
Dueno.IniXNit (Dueno.Nit)
    If Len(FormatDateTime(Date, vbShortDate)) = 8 Then
        Me.txtFCDESDE.Mask = "##/##/##"
        Me.txtFCHASTA.Mask = "##/##/##"
    Else
        Me.txtFCDESDE.Mask = "##/##/####"
        Me.txtFCHASTA.Mask = "##/##/####"
    End If
    Me.txtFCDESDE.Text = FormatDateTime(DateAdd("d", -(Day(Date) - 1), Date), vbShortDate)
    Me.txtFCHASTA.Text = FormatDateTime(DateAdd("d", -1, DateAdd("M", 1, DateAdd("d", -(Day(Date) - 1), Date))), vbShortDate)
    Me.txtFCDESDE.Enabled = False
    Me.txtFCHASTA.Enabled = False
   

    Me.lstBODEGAS.ListItems.Clear
    Me.lstBODEGAS.Checkboxes = False
    Me.lstBODEGAS.MultiSelect = True
    Me.lstBODEGAS.HideSelection = False
    Me.lstBODEGAS.ColumnHeaders.Clear
    Me.lstBODEGAS.ColumnHeaders.Add , "NOM", "Nombre de la Bodega", 0.66 * Me.lstBODEGAS.Width
    Me.lstBODEGAS.ColumnHeaders.Add , "COD", "C�digo", 0.34 * Me.lstBODEGAS.Width
    
    Me.LstTERCEROS.ListItems.Clear
    Me.LstTERCEROS.Checkboxes = False
    Me.LstTERCEROS.MultiSelect = True
    Me.LstTERCEROS.HideSelection = False
    Me.LstTERCEROS.ColumnHeaders.Clear
'lstTerceros, NOAR, COAR, NOCO, NOUS, NOGR, AUCO, COGR, COUS
    Me.LstTERCEROS.ColumnHeaders.Add , "COTE", "NIT", 0.3 * Me.LstTERCEROS.Width
    Me.LstTERCEROS.ColumnHeaders.Add , "NOTE", "Nombre", 0.6 * Me.LstTERCEROS.Width
'FALLO:
 cargalistview
End Sub



Private Sub LstBODEGAS_KeyPress(KeyAscii As Integer)
Call Cambiar_Enter(KeyAscii)
End Sub

'NYCM R1235
Private Sub lstTERCEROS_ColumnClick(ByVal ColumnHeader As MSComctlLib.ColumnHeader)
Me.LstTERCEROS.SortKey = ColumnHeader.Index - 1
Me.LstTERCEROS.Sorted = True
End Sub
'NYCM R1235
'---------------------------------------------------------------------------------------
' Procedure : cargalistview
' DateTime  : 17/10/2007 08:29
' Author    : nelly_canon
' Purpose   : Carga los Listview LstBodegas y LstTerceros
'---------------------------------------------------------------------------------------
'
Private Sub cargalistview()
    Dim tipo As String * 1
    Dim CnTdr As Double
    Me.lstBODEGAS.ListItems.Clear
    Me.LstTERCEROS.ListItems.Clear
    Me.pgbLLEVO.Value = 0
    Me.pgbLLEVO.Visible = True
    Campos = "DISTINCT NU_AUTO_BODE, NU_AUTO_BODE_BODE, TX_CODI_BODE, TX_NOMB_BODE, " & _
        "CD_CODI_CECO_BODE, TX_VENTA_BODE"
    Desde = "IN_R_BODE_COMP,IN_BODEGA,IN_COMPROBANTE"
    Condi = "NU_AUTO_BODE = NU_AUTO_BODE_RBOCO" & _
            " AND NU_AUTO_COMP_RBOCO=NU_AUTO_COMP" & _
            " AND NU_AUTO_DOCU_COMP= 3  ORDER BY TX_NOMB_BODE"
    
    ReDim ArrUXB(5, 0)
    Result = LoadMulData(Desde, Campos, Condi, ArrUXB())
    If Result = FAIL Then GoTo FALLO
    On Error GoTo SIGUI
    For CnTdr = 0 To UBound(ArrUXB, 2)
        Me.lstBODEGAS.ListItems.Add , "B" & ArrUXB(2, CnTdr), ArrUXB(3, CnTdr)
        Me.lstBODEGAS.ListItems("B" & ArrUXB(2, CnTdr)).ListSubItems.Add , "COD", ArrUXB(2, CnTdr)
SIGUI:
    Next
    On Error GoTo 0
    
    'Campos = "CD_CODI_TERC, NO_NOMB_TERC"
    Campos = "DISTINCT CD_CODI_TERC, NO_NOMB_TERC" 'NYCM M2654
    Desde = "TERCERO, IN_ENCABEZADO, IN_DOCUMENTO"
    Condi = "NU_AUTO_DOCU_ENCA = NU_AUTO_DOCU" & _
        " AND CD_CODI_TERC_ENCA = CD_CODI_TERC" & _
        " AND TX_ESTA_ENCA<>'A'" & _
        " AND NU_AUTO_DOCU=3 ORDER BY NO_NOMB_TERC"
        
    ReDim ArrUXB(2, 0)
    Result = LoadMulData(Desde, Campos, Condi, ArrUXB())
    If Result = FAIL Then GoTo FALLO
    If Not Encontro Then GoTo NOENC
    Me.pgbLLEVO.Min = 0
    Me.pgbLLEVO.Value = 0
    Me.pgbLLEVO.Max = UBound(ArrUXB, 2) + 1
    Dim Tercero As New ElTercero
    For CnTdr = 0 To UBound(ArrUXB, 2)
        Me.pgbLLEVO.Value = CnTdr
        Tercero.Nit = ArrUXB(0, CnTdr)
        Tercero.Nombre = ArrUXB(1, CnTdr)
        Me.LstTERCEROS.ListItems.Add , "T" & CnTdr, Trim(Tercero.Nit)
        Me.LstTERCEROS.ListItems("T" & CnTdr).ListSubItems.Add , "NOTE", Trim(Tercero.Nombre)
    Next
    Set Tercero = Nothing
NOENC:
FALLO:
    Me.pgbLLEVO.Visible = False
    
End Sub

Private Sub LstTERCEROS_KeyPress(KeyAscii As Integer)
Call Cambiar_Enter(KeyAscii) 'NYCM M2432
End Sub

Private Sub txtFCDESDE_KeyPress(KeyAscii As Integer)
Call Cambiar_Enter(KeyAscii) 'NYCM M2432
End Sub

Private Sub txtFCDESDE_LostFocus()
If IsDate(Me.txtFCDESDE.Text) Then Exit Sub
    Me.txtFCDESDE.Text = FormatDateTime(DateAdd("d", -(Day(Date) - 1), Date), vbShortDate)
    Me.txtFCDESDE.SetFocus
End Sub

Private Sub txtFCHASTA_KeyPress(KeyAscii As Integer)
Call Cambiar_Enter(KeyAscii) 'NYCM M2432
End Sub

Private Sub txtFCHASTA_LostFocus()
 If IsDate(txtFCHASTA.Text) Then
       If DateDiff("d", CDate(txtFCDESDE.Text), CDate(txtFCHASTA)) < 0 Then
         Call Mensaje1("La fecha final debe ser mayor a la inicial.", 3)
         txtFCHASTA = Hoy
         txtFCHASTA.SetFocus
       End If
    Else
       txtFCHASTA.Text = FormatDateTime(DateAdd("d", -1, DateAdd("M", 1, DateAdd("d", -(Day(Date) - 1), Date))), vbShortDate)
       txtFCHASTA.SetFocus
    End If
End Sub
