VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "InfXBodega"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit
    
Private bdg As Long 'Consecutivo de la bodega
Private cdi As String * 10 'C�digo de la bodega
Private nmb As String * 50 'Nombre de la bodega
Private vSaldo As New UnFraccionario
Private vEntradas As New UnFraccionario
Private vSalidas As New UnFraccionario
'Private SldNmr As Long 'Numerador del saldo de la bodega
'Private SldDnm As Long 'Denominador del saldo de la bodega
Private Max As Long 'Maximo stock x bodega
Private Min As Long 'Minimoxstock x bodega
Private Rep As Long 'Cantidad de reposici�n
Private Mxd As Long 'Maximo a despachar
'Private TotEntraNmr As Long, TotEntraDnm As Long
'Private TotSalidaNmr As Long, TotSalidaDnm As Long

Public Property Let Bodega(cnt As Long)
    bdg = cnt
End Property

Public Property Get Bodega() As Long
    Bodega = bdg
End Property

Public Property Let Codigo(rda As String)
    cdi = rda
End Property

Public Property Get Codigo() As String
    Codigo = cdi
End Property

Public Property Let Nombre(rda As String)
    nmb = rda
End Property

Public Property Get Nombre() As String
    Nombre = nmb
End Property

Public Property Get Saldo() As UnFraccionario
    Set Saldo = vSaldo
End Property

Public Property Set Saldo(cnt As UnFraccionario)
    Set vSaldo = cnt
End Property

Public Property Get Entradas() As UnFraccionario
    Set Entradas = vEntradas
End Property

Public Property Set Entradas(cnt As UnFraccionario)
    Set vEntradas = cnt
End Property

Public Property Get Salidas() As UnFraccionario
    Set Salidas = vSalidas
End Property

Public Property Set Salidas(cnt As UnFraccionario)
    Set vSalidas = cnt
End Property

Public Property Let Maximo(cnt As Long)
    Max = cnt
End Property

Public Property Get Maximo() As Long
    Maximo = Max
End Property

Public Property Let Minimo(cnt As Long)
    Min = cnt
End Property

Public Property Get Minimo() As Long
    Minimo = Min
End Property

Public Property Let Reposicion(cnt As Long)
    Rep = cnt
End Property

Public Property Get Reposicion() As Long
    Reposicion = Rep
End Property

Public Property Let MaxDespachar(cnt As Long)
    Mxd = cnt
End Property

Public Property Get MaxDespachar() As Long
    MaxDespachar = Mxd
End Property
