VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Object = "{0BA686C6-F7D3-101A-993E-0000C0EF6F5E}#1.0#0"; "THREED32.OCX"
Begin VB.Form FrmFISICOIMPO 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Archivos Planos"
   ClientHeight    =   2445
   ClientLeft      =   4185
   ClientTop       =   2970
   ClientWidth     =   4770
   Icon            =   "FrmFISICOIMPO.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   ScaleHeight     =   2445
   ScaleWidth      =   4770
   Begin MSComctlLib.ProgressBar PrgPlano 
      Height          =   255
      Left            =   120
      TabIndex        =   0
      Top             =   2040
      Width           =   4575
      _ExtentX        =   8070
      _ExtentY        =   450
      _Version        =   393216
      BorderStyle     =   1
      Appearance      =   1
   End
   Begin VB.Frame Frame2 
      Height          =   1095
      Left            =   120
      TabIndex        =   1
      Top             =   360
      Width           =   4575
      Begin Threed.SSCommand SCmd_Options 
         Height          =   735
         Index           =   0
         Left            =   240
         TabIndex        =   2
         Top             =   240
         Width           =   870
         _Version        =   65536
         _ExtentX        =   1535
         _ExtentY        =   1296
         _StockProps     =   78
         Caption         =   "&DESCARGAR"
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   6
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Picture         =   "FrmFISICOIMPO.frx":058A
      End
      Begin Threed.SSCommand SCmd_Options 
         Height          =   735
         Index           =   1
         Left            =   1320
         TabIndex        =   3
         Top             =   240
         Width           =   855
         _Version        =   65536
         _ExtentX        =   1508
         _ExtentY        =   1296
         _StockProps     =   78
         Caption         =   "&CARGAR"
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   6
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Picture         =   "FrmFISICOIMPO.frx":0C54
      End
      Begin Threed.SSCommand SCmd_Options 
         Height          =   735
         Index           =   3
         Left            =   3480
         TabIndex        =   4
         Top             =   240
         Width           =   855
         _Version        =   65536
         _ExtentX        =   1508
         _ExtentY        =   1296
         _StockProps     =   78
         Caption         =   "&SALIR"
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   6
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Picture         =   "FrmFISICOIMPO.frx":131E
      End
      Begin Threed.SSCommand SCmd_Detener 
         Height          =   735
         Left            =   2400
         TabIndex        =   5
         Top             =   240
         Width           =   855
         _Version        =   65536
         _ExtentX        =   1508
         _ExtentY        =   1296
         _StockProps     =   78
         Caption         =   "CANCELAR"
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   6
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Picture         =   "FrmFISICOIMPO.frx":19E8
      End
   End
   Begin VB.Label Lbl_Errores 
      Caption         =   "0"
      Height          =   255
      Left            =   3720
      TabIndex        =   9
      Top             =   1680
      Width           =   855
   End
   Begin VB.Label Lbl_Procesados 
      Caption         =   "0"
      Height          =   255
      Left            =   1080
      TabIndex        =   8
      Top             =   1680
      Width           =   1815
   End
   Begin VB.Label Label2 
      Caption         =   "Errores:"
      Height          =   255
      Left            =   3120
      TabIndex        =   7
      Top             =   1680
      Width           =   615
   End
   Begin VB.Label Label1 
      Caption         =   "Procesados:"
      Height          =   255
      Left            =   120
      TabIndex        =   6
      Top             =   1680
      Width           =   975
   End
End
Attribute VB_Name = "FrmFISICOIMPO"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Dim texto As String
Dim NumReg As Long
Dim BandErr, BandStop As Boolean
'Dim ArrTarifa(), CodTarifaBase() As Variant
'Dim erroresCargue As Integer       'DEPURACION DE CODIGO
Dim LarReg As Integer
Dim Articulo As New ElArticulo
Private vLista As MSComctlLib.ListView
Private vBodega As Long
'Private PosEnGri As String         'DEPURACION DE CODIGO
Private vAccionExportar As Boolean
Private vTipoSobrantes As Boolean

Private Sub Form_Load()
    SCmd_Options(0).Enabled = False
    SCmd_Options(1).Enabled = True
    If vAccionExportar Then
        SCmd_Options(0).Enabled = True
        SCmd_Options(1).Enabled = False
    End If
    Call CenterForm(MDI_Inventarios, Me)
    BandProcLote = 0
    BandStop = False
End Sub

Private Sub SCmd_Detener_Click()
If BandProcLote Then
   If MsgBox("�Esta seguro que desea detener el proceso?", vbExclamation) = vbYes Then
      BandStop = True
   End If
End If
End Sub

Private Sub SCmd_Options_Click(Index As Integer)
If BandProcLote = 1 Then Exit Sub
Select Case Index
   Case 0: Call DescargarArticulos
   Case 1: Call CargarArticulos
   Case 3: Unload Me
End Select
End Sub

Sub CargarArticulos()
    Dim TotaLeidos As Long
'    Dim TotalLinea As Double       'DEPURACION DE CODIGO
'    Dim Procesados As Integer, vClave As String
    Dim vClave As String        'DEPURACION DE CODIGO
'    Dim Linea As String, Estoy As String, Faltan As String
    Dim Estoy As String, Faltan As String            'DEPURACON DE CODIGO
    Dim Errores
    On Error GoTo control
    PrgPlano.Value = 0
    If MDI_Inventarios.CMDialog1.InitDir = NUL$ Then MDI_Inventarios.CMDialog1.InitDir = DirDB
    MDI_Inventarios.CMDialog1.Filter = "(TODOS)|*"
    MDI_Inventarios.CMDialog1.Action = 1
    BandErr = False
    If MDI_Inventarios.CMDialog1.filename <> "" Then
        PrgPlano.Max = FileLen(MDI_Inventarios.CMDialog1.filename)
        Open MDI_Inventarios.CMDialog1.filename For Input As #1
        Open App.Path & "\ErrorDeCarga.txt" For Output As #2
        NumReg = 1
        BandProcLote = 1
        PrgPlano.Value = PrgPlano.Min
        DoEvents
        Errores = 0
        BandStop = False
        LarReg = 0: TotaLeidos = 0
        Do While Not EOF(1)
            DoEvents
            If BandStop = True Then Exit Do
            Line Input #1, texto
            Faltan = texto
            TotaLeidos = TotaLeidos + Len(Faltan)
            LarReg = TotaLeidos / NumReg
            If PrgPlano.Value + LarReg < PrgPlano.Max Then PrgPlano.Value = PrgPlano.Value + LarReg
            Estoy = ParteIzquierda(Faltan, ",")
            If Len(Estoy) = 0 Then Estoy = Faltan
            If ValidarArticulo(Estoy) Then
                Faltan = ParteDerecha(Faltan, ",")
                If Len(Faltan) = 0 Then GoTo EsUNERR
                If CLng(Faltan) <= 0 Then GoTo EsUNERR
                vClave = "A" & Articulo.Numero
                vLista.ListItems.Add , vClave, Trim(Articulo.Nombre)
                vLista.ListItems("A" & Articulo.Numero).Tag = CStr(Articulo.Numero)
        'lstArticulos, NOAR, COAR, NOCO, NOUS, NOGR, AUCO, COGR, COUS, MUCO, DICO
                vLista.ListItems(vClave).ListSubItems.Add , "COAR", Trim(Articulo.Codigo)
                vLista.ListItems(vClave).ListSubItems.Add , "CANT", CStr(CLng(Faltan))
                vLista.ListItems(vClave).ListSubItems.Add , "UNDD", Format(Articulo.CostoPromedio, "#########.#0")
                vLista.ListItems(vClave).ListSubItems.Add , "CSTO", FormatNumber(CLng(Faltan) * Articulo.CostoPromedio, 0, vbFalse, vbTrue)
                vLista.ListItems(vClave).ListSubItems.Add , "FALT", "0"
                vLista.ListItems(vClave).ListSubItems.Add , "SOBR", "0"
                vLista.ListItems(vClave).ListSubItems.Add , "VRFC", "0"
'Codigo,NombreUndConteo,NombreUso,NombreGrupo
            Else
EsUNERR:
                Print #2, "Linea:" & NumReg & ", C�digo:" & Estoy
                Errores = Errores + 1
            End If
            Lbl_Procesados.Caption = NumReg & " de " & CLng(PrgPlano.Max / IIf(LarReg > 0, LarReg, 20))
            Lbl_Procesados.Refresh
            Lbl_Errores.Caption = Errores
            Lbl_Errores.Refresh
OtrReg:
            NumReg = NumReg + 1
        Loop
        Close (1): Close (2)
        BandProcLote = 0
        Lbl_Procesados.Caption = NumReg & " de " & NumReg
        Lbl_Procesados.Refresh
        If Errores > 0 Then MsgBox "Se encontro errores, el resumen esta en el archivo:" & App.Path & "\ErrorDeCarga.txt", vbCritical
    End If
    PrgPlano.Value = 0
    Exit Sub
control:
    If ERR.Number <> 0 Then
       Call ConvertErr
       Close (1)
    End If
    Call MouseNorm
    PrgPlano.Value = 0
End Sub

Sub DescargarArticulos()
    Dim Linea As String, CosUnidad As Single
'    Dim Estoy As String, Faltan As String           'DEPURACON DE CODIGO
    Dim Uno As MSComctlLib.ListItem, NroUnidades As Long
'    Dim Errores As Integer, TOTAL As Integer
    Dim TOTAL As Integer         'DEPURACON DE CODIGO
'    Dim ColActual As Byte, Procesados As Integer        'DEPURACON DE CODIGO
    Bandera = 0
    PrgPlano.Value = 0
    If MDI_Inventarios.CMDialog1.InitDir = NUL$ Then MDI_Inventarios.CMDialog1.InitDir = DirDB
    MDI_Inventarios.CMDialog1.Filter = "(TODOS)|*"
    MDI_Inventarios.CMDialog1.Action = 2
    BandErr = False
    If MDI_Inventarios.CMDialog1.filename <> "" Then
        On Error GoTo control
        Open MDI_Inventarios.CMDialog1.filename For Output As #1
        BandProcLote = 1
        PrgPlano.Max = vLista.ListItems.Count + 1
        TOTAL = vLista.ListItems.Count
        PrgPlano.Value = 0
        PrgPlano.Min = 0
        BandStop = False
        'Me.PosicionesEnGrilla = "1,7"
        For Each Uno In vLista.ListItems
            Linea = Uno.ListSubItems("COAR").Text
            CosUnidad = 0: NroUnidades = 0
            If vTipoSobrantes Then
                NroUnidades = CLng(Uno.ListSubItems("SOBR").Text)
                If Not NroUnidades > 0 Then GoTo OtrReg
            Else
                NroUnidades = CLng(Uno.ListSubItems("FALT").Text)
                If Not NroUnidades > 0 Then GoTo OtrReg
            End If
'            CosUnidad = CLng(Uno.ListSubItems("CSTO").Text) / NroUnidades
            CosUnidad = CSng(Uno.ListSubItems("UNDD").Text)
            Linea = Linea & "," & Format(NroUnidades, "###########0")
            Linea = Linea & "," & Format(CosUnidad, "############.#0")
            Debug.Print Linea
            Print #1, Linea
            Lbl_Procesados.Caption = PrgPlano.Value + 1 & " de " & TOTAL
            Lbl_Procesados.Refresh
            PrgPlano.Value = PrgPlano.Value + 1
OtrReg:
        Next
        Close (1)
        BandProcLote = 0
        PrgPlano.Value = 0
    End If
    Exit Sub
control:
    If ERR.Number <> 0 Then
       Call ConvertErr
       Close (1)
    End If
    Call MouseNorm
    PrgPlano.Value = 0
End Sub

Private Function ValidarArticulo(Codigo As String) As Boolean
    ValidarArticulo = True
    If Articulo.IniciaXCodigo(Codigo) = Encontroinfo Then Exit Function
    If Articulo.IniciaXBarra(Codigo) = Encontroinfo Then Exit Function
    ValidarArticulo = False
End Function

Public Property Set ListaArticulos(LaLista As MSComctlLib.ListView)
    Set vLista = LaLista
End Property

Public Property Let CualBodega(num As Long)
    vBodega = num
End Property

Public Property Let AccionExportar(bol As Boolean)
    vAccionExportar = bol
End Property
'vTipoSobrantes

Public Property Let TipoSobrantes(bol As Boolean)
    vTipoSobrantes = bol
End Property

