VERSION 5.00
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "MSFLXGRD.OCX"
Object = "{0BA686C6-F7D3-101A-993E-0000C0EF6F5E}#1.0#0"; "THREED32.OCX"
Begin VB.Form FrmSelecNivel 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Selecci�n"
   ClientHeight    =   4950
   ClientLeft      =   2595
   ClientTop       =   2445
   ClientWidth     =   9150
   ClipControls    =   0   'False
   Icon            =   "FrmSelNi.frx":0000
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   4950
   ScaleWidth      =   9150
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin VB.Frame Frame1 
      Height          =   4935
      Left            =   0
      TabIndex        =   8
      Top             =   0
      Width           =   9135
      Begin VB.TextBox TxtHasta 
         Height          =   285
         Left            =   5160
         TabIndex        =   3
         Text            =   "Z"
         Top             =   600
         Visible         =   0   'False
         Width           =   2175
      End
      Begin VB.TextBox Txt_Seleccion 
         Height          =   285
         Left            =   5160
         TabIndex        =   1
         Top             =   240
         Width           =   2175
      End
      Begin VB.CheckBox ChNivel 
         Caption         =   "&Ultimo Nivel"
         Height          =   255
         Left            =   2160
         TabIndex        =   17
         Top             =   600
         Value           =   1  'Checked
         Visible         =   0   'False
         Width           =   1215
      End
      Begin VB.OptionButton OptCriterio 
         Caption         =   "Nombre"
         Height          =   255
         Index           =   0
         Left            =   1800
         TabIndex        =   15
         Top             =   240
         Value           =   -1  'True
         Width           =   1095
      End
      Begin VB.OptionButton OptCriterio 
         Caption         =   "C�digo"
         Height          =   255
         Index           =   1
         Left            =   2880
         TabIndex        =   16
         Top             =   240
         Width           =   855
      End
      Begin VB.CommandButton Cmd_Cancelar 
         Caption         =   "&CANCELAR"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   6
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   435
         Left            =   4920
         MouseIcon       =   "FrmSelNi.frx":058A
         Picture         =   "FrmSelNi.frx":0894
         Style           =   1  'Graphical
         TabIndex        =   7
         Top             =   4320
         Width           =   1335
      End
      Begin VB.CommandButton Cmd_Seleccion 
         Caption         =   "&SELECCIONAR"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   6
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   435
         Left            =   2880
         Picture         =   "FrmSelNi.frx":0BD6
         Style           =   1  'Graphical
         TabIndex        =   6
         Top             =   4320
         Width           =   1335
      End
      Begin Threed.SSPanel SSPanel2 
         Height          =   3255
         Left            =   15
         TabIndex        =   9
         Top             =   960
         Width           =   8985
         _Version        =   65536
         _ExtentX        =   15849
         _ExtentY        =   5741
         _StockProps     =   15
         BackColor       =   12632256
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Begin MSFlexGridLib.MSFlexGrid Grd_Selecciones 
            Height          =   2745
            Left            =   120
            TabIndex        =   5
            Top             =   480
            Width           =   8775
            _ExtentX        =   15478
            _ExtentY        =   4842
            _Version        =   393216
            Rows            =   1
            FixedRows       =   0
            FixedCols       =   0
            BackColorBkg    =   16777215
            Redraw          =   -1  'True
            AllowBigSelection=   -1  'True
            SelectionMode   =   1
            MouseIcon       =   "FrmSelNi.frx":1128
         End
         Begin VB.Label Lbl_Condicion 
            BackColor       =   &H80000009&
            BorderStyle     =   1  'Fixed Single
            Caption         =   "Lbl_Condicion"
            Height          =   255
            Left            =   6120
            TabIndex        =   14
            Top             =   120
            Visible         =   0   'False
            Width           =   975
         End
         Begin VB.Image Image1 
            Height          =   615
            Left            =   3000
            Top             =   1200
            Width           =   1095
         End
         Begin VB.Label Lbl_Tabla 
            BackColor       =   &H80000009&
            BorderStyle     =   1  'Fixed Single
            Caption         =   "Lbl_Tabla"
            Height          =   255
            Left            =   1320
            TabIndex        =   12
            Top             =   120
            Visible         =   0   'False
            Width           =   855
         End
         Begin VB.Label Lbl_Campos 
            BackColor       =   &H80000009&
            BorderStyle     =   1  'Fixed Single
            Caption         =   "Lbl_Campos"
            Height          =   255
            Left            =   7080
            TabIndex        =   13
            Top             =   120
            Visible         =   0   'False
            Width           =   975
         End
         Begin VB.Label Lbl_OrderBY 
            BackColor       =   &H80000009&
            BorderStyle     =   1  'Fixed Single
            Caption         =   "Lbl_OrderBY"
            Height          =   255
            Left            =   120
            TabIndex        =   11
            Top             =   120
            Visible         =   0   'False
            Width           =   975
         End
         Begin VB.Label Lbl_Titulo 
            Alignment       =   2  'Center
            Caption         =   "Lbl_Titulo"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   12
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Left            =   120
            TabIndex        =   10
            Top             =   120
            Width           =   8475
         End
      End
      Begin VB.Label Label1 
         AutoSize        =   -1  'True
         Caption         =   "Hasta:"
         Height          =   195
         Index           =   1
         Left            =   4440
         TabIndex        =   2
         Top             =   600
         Visible         =   0   'False
         Width           =   465
      End
      Begin VB.Label Label1 
         AutoSize        =   -1  'True
         Caption         =   "Desde:"
         Height          =   195
         Index           =   0
         Left            =   4440
         TabIndex        =   0
         Top             =   240
         Width           =   510
      End
      Begin VB.Label Label3 
         Caption         =   "Cri&terios de Selecci�n"
         Height          =   255
         Left            =   120
         TabIndex        =   4
         Top             =   240
         Width           =   1935
      End
   End
End
Attribute VB_Name = "FrmSelecNivel"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Dim BAND As Integer
'Dim SQL2 As String     'DEPURACION DE CODIGO

Private Sub Cmd_Cancelar_Click()
    Codigo = NUL$
    If BAND = 1 Then ' cargando el grid
      CancelTrans = 1
    End If
    If BAND = 0 Then
      Unload Me
      CancelTrans = 0
    End If
End Sub
Private Sub Cmd_Seleccion_Click()
Grd_Selecciones.Col = 0
Codigo = Grd_Selecciones.Text
Grd_Selecciones.Col = 1
Valores = Grd_Selecciones.Text
Unload Me
End Sub

Private Sub ChNivel_KeyPress(KeyAscii As Integer)
    Call Cambiar_Enter(KeyAscii)
End Sub

Private Sub Form_Unload(Cancel As Integer)
   If BAND = 1 Then
      CancelTrans = 1
      Cancel = True
   Else
      CancelTrans = 0
   End If
End Sub
Private Sub Grd_selecciones_DblClick()
If BAND = 0 Then Call Cmd_Seleccion_Click
End Sub
Private Sub Grd_selecciones_KeyPress(KeyAscii As Integer)
If BAND = 0 Then
   If KeyAscii = 13 Then Call Cmd_Seleccion_Click
End If
End Sub
Private Sub Form_Load()
    Grd_Selecciones.ColWidth(0) = 2000
    Grd_Selecciones.ColWidth(1) = 10000
    Grd_Selecciones.ColAlignment(0) = 0
    CancelTrans = 0
End Sub

Private Sub OptCriterio_Click(Index As Integer)
Label1(1).Visible = OptCriterio(1).Value
txtHASTA.Visible = OptCriterio(1).Value
If OptCriterio(1).Value = True Then Txt_Seleccion = "0"
End Sub
Private Sub OptCriterio_KeyPress(Index As Integer, KeyAscii As Integer)
Call Cambiar_Enter(KeyAscii)
End Sub
Private Sub OptCriterio_LostFocus(Index As Integer)
    Label1(1).Visible = OptCriterio(1).Value
    txtHASTA.Visible = OptCriterio(1).Value
    If OptCriterio(1).Value = True Then Txt_Seleccion = "0"
End Sub
Private Sub Txt_Seleccion_GotFocus()
Txt_Seleccion.SelStart = 0
Txt_Seleccion.SelLength = Len(Txt_Seleccion.Text)
Label1(1).Visible = OptCriterio(1).Value
txtHASTA.Visible = OptCriterio(1).Value
End Sub
Private Sub Txt_Seleccion_KeyPress(KeyAscii As Integer)
Call Cambiar_Enter(KeyAscii)
End Sub
Private Sub Txt_Seleccion_LostFocus()
Dim Pos As Integer
Dim tipo As Byte

Condicion = NUL$
Select Case Lbl_Tabla
    Case "INGRESOS": tipo = 1
                     Condicion = "DE_DESC_INGR"
    Case "GASTOS": tipo = 0
                   Condicion = "DE_DESC_GAST"
    Case "CUENTAS": tipo = 2
                    Condicion = "NO_NOMB_CUEN"
End Select
If OptCriterio(1).Value = True Then txtHASTA.SetFocus: Exit Sub
DoEvents
If Txt_Seleccion = "*" Or Txt_Seleccion = "%" Then Txt_Seleccion = CaractLike
Grd_Selecciones.Clear
Grd_Selecciones.Rows = 0

Pos = InStr(Lbl_Campos, ",")
If Pos >= 1 Then
   Codigo = Mid(Lbl_Campos, 1, Pos - 1)
Else
   Codigo = ""
End If

If OptCriterio(0).Value = True Then
   Condicion = Condicion & " Like '" & Txt_Seleccion & CaractLike & Comi & IIf(Lbl_Condicion = NUL$, NUL$, " AND " & Lbl_Condicion) & " ORDER BY " & Lbl_OrderBY
Else
   Condicion = Codigo & " Like '" & Txt_Seleccion & CaractLike & Comi & IIf(Lbl_Condicion = NUL$, NUL$, " AND " & Lbl_Condicion) & " ORDER BY " & Codigo
End If

If Txt_Seleccion.Text <> "" Then
    Call desabilitar
    If ChNivel.Value = 0 Then
       Result = LoadfGrid(Grd_Selecciones, Lbl_Tabla, Lbl_Campos, Condicion)
    Else
       Result = LoadfGrid_Nivel(Grd_Selecciones, Lbl_Tabla, Lbl_Campos, Condicion, tipo)
    End If
    Call habilitar
    Cmd_Seleccion.Enabled = True
    If Not Encontro And Grd_Selecciones.TextMatrix(0, 0) = NUL$ Then
      Call Mensaje1("Ningun elemento encontrado", 3)
    End If
    Grd_Selecciones.Col = 0
       Grd_Selecciones.SetFocus
    If Result = FAIL Then
        Call Mensaje1("Error al cargar la tabla " & Lbl_Tabla, 1)
        Unload Me
    End If
    
End If
CancelTrans = 0
End Sub
Sub desabilitar()
    BAND = 1
    Cmd_Seleccion.Enabled = False
    Txt_Seleccion.Enabled = False
    'SSPanel2.Enabled = False
End Sub
Sub habilitar()
    BAND = 0
    Cmd_Seleccion.Enabled = True
    Txt_Seleccion.Enabled = True
    SSPanel2.Enabled = True
End Sub
Private Sub TxtHasta_GotFocus()
    txtHASTA.SelStart = 0
    txtHASTA.SelLength = Len(txtHASTA)
End Sub
Private Sub TxtHasta_KeyPress(KeyAscii As Integer)
    Call Cambiar_Enter(KeyAscii)
End Sub
Private Sub TxtHasta_LostFocus()
Dim Pos, A As Integer
    If OptCriterio(1).Value = True Then
        If Txt_Seleccion > txtHASTA Then txtHASTA = Txt_Seleccion

        DoEvents
        If txtHASTA = "*" Then txtHASTA = "%"
        Grd_Selecciones.Clear
        Grd_Selecciones.Rows = 0
        
        Pos = InStr(Lbl_Campos, ",")
        Codigo = Mid(Lbl_Campos, 1, Pos - 1)
        
        Condicion = Codigo & " >= '" & Txt_Seleccion & Comi & " AND " & Codigo & " <= '" & txtHASTA & "' " & IIf(Lbl_Condicion = NUL$, NUL$, " AND " & Lbl_Condicion) & " ORDER BY " & Codigo
        Debug.Print Condicion
        If Txt_Seleccion.Text <> "" Then
            Call desabilitar
            If ChNivel.Value = 0 Then
               Result = LoadfGrid(Grd_Selecciones, Lbl_Tabla, Lbl_Campos, Condicion)
            Else
                If Lbl_Tabla = "CUENTAS" Then
                    A = 2
                ElseIf Lbl_Tabla = "GASTOS" Then
                    A = 0
                ElseIf Lbl_Tabla = "INGRESOS" Then
                    A = 1
                End If
               Result = LoadfGrid_Nivel(Grd_Selecciones, Lbl_Tabla, Lbl_Campos, Condicion, A)
            End If
            Call habilitar
            Cmd_Seleccion.Enabled = True
            If Not Encontro And Grd_Selecciones.TextMatrix(0, 0) = NUL$ Then
              Call Mensaje1("Ningun elemento encontrado", 3)
            End If
            Grd_Selecciones.Col = 0
            Grd_Selecciones.SetFocus
            If Result = FAIL Then
                Call Mensaje1("Error al cargar la tabla " & Lbl_Tabla, 1)
                Unload Me
            End If
            
        End If
        CancelTrans = 0
    End If
End Sub



