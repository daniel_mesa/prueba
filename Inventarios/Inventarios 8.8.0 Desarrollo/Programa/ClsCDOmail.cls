VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "ClsCDOmail"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
'---------------------------------------------------------------------------------------
' Module    : clsCDOmail
' DateTime  : 24/10/2018 09:50
' Author    : Daniel_mesa
' Purpose   : DRMG T45220 clase para envio de correo electronico CDO
'---------------------------------------------------------------------------------------

Option Explicit

' para la conexi�n a internet
Private Declare Function InternetGetConnectedState Lib "wininet.dll" (ByRef lpdwFlags As Long, ByVal dwReserved As Long) As Long
Private Const INTERNET_CONNECTION_MODEM_BUSY As Long = &H8
Private Const INTERNET_RAS_INSTALLED As Long = &H10
Private Const INTERNET_CONNECTION_OFFLINE As Long = &H20
Private Const INTERNET_CONNECTION_CONFIGURED As Long = &H40

' variables locales
Private StServidor As String 'almacena el servidor
Private StPara As String 'almnacena el correo a quien va dirigido
Private StDe As String 'almacena el correo del remitente
Private StAsunto As String 'almacena el asunto
Private StMensaje As String 'almacena el detalle del correo
Private StAdjunto As String 'almacena las rutas de los correos ajuntos
Private StPuerto As Variant 'almacena el puerto
Private StUsuario As String 'almmacena el usuario o correo de la cuenta que se va a enviar
Private StPassword As String 'almcana la contrase�a de correo
Private BoUseAuntentificacion As Boolean 'true si requiere autenticacion el correo
Private BoSSL As Boolean 'true si manega protocolo de seguridad SSL

Public StMsj As String 'Almacena el mensaje del resultado del envio del correo

'---------------------------------------------------------------------------------------
' Procedure : Enviar_Backup
' DateTime  : 24/10/2018 09:50
' Author    : daniel_mesa
' Purpose   : DRMG T45220 define los detalles del envio y envia el correo electronico
'---------------------------------------------------------------------------------------
'
Function Enviar_Backup() As Boolean
       
   Dim oCDO As Object ' Variable de objeto Cdo.Message
   Dim StArr() As String 'almacena las rutas de los archivoa ajuntos
   Dim Ini As Integer 'contador usado para ciclo for
   StMsj = NUL$
   
   StArr = Split(StAdjunto, ";")
   ' chequea si hay conexi�n
   If InternetGetConnectedState(0&, 0&) = False Then
      StMsj = "No se puede enviar el correo. Verificar la conexi�n a internet si est� disponible"
      Exit Function
   End If
    
   ' chequea que el puerto sea un n�mero, o que no est� vac�o
   If Not IsNumeric(puerto) Then
      StMsj = "No se ha indicado el puerto del servidor"
      Exit Function
   End If
    
   ' Crea un Nuevo objeto CDO.Message
   Set oCDO = CreateObject("CDO.Message")
    
   ' Indica el servidor Smtp para poder enviar el Mail ( puede ser el nombre del servidor o su direcci�n IP )
   oCDO.Configuration.Fields( _
   "http://schemas.microsoft.com/cdo/configuration/smtpserver") = StServidor
    
   oCDO.Configuration.Fields( _
   "http://schemas.microsoft.com/cdo/configuration/sendusing") = 2
    
   ' Puerto. Por defecto se usa el puerto 25,  en el caso de Gmail se usa el puerto 465
    
   oCDO.Configuration.Fields.Item _
   ("http://schemas.microsoft.com/cdo/configuration/smtpserverport") = StPuerto

    
   ' Indica el tipo de autentificaci�n con el servidor de correo El valor 0 no requiere autentificarse, el valor 1 es con autentificaci�n
   oCDO.Configuration.Fields.Item("http://schemas.microsoft.com/cdo/" & _
   "configuration/smtpauthenticate") = Abs(BoUseAuntentificacion)
    
   ' Tiempo m�ximo de espera en segundos para la conexi�n
   oCDO.Configuration.Fields.Item("http://schemas.microsoft.com/cdo/configuration/smtpconnectiontimeout") = 10

   ' Configura las opciones para el login en el SMTP
   If BoUseAuntentificacion Then

      ' Id de usuario del servidor Smtp ( en el caso de gmail,debe ser la direcci�n de correro mas el @gmail.com )
      oCDO.Configuration.Fields.Item("http://schemas.microsoft.com/cdo/configuration/sendusername") = StUsuario

      ' Password de la cuenta
      oCDO.Configuration.Fields.Item("http://schemas.microsoft.com/cdo/configuration/sendpassword") = StPassword

      ' Indica si se usa SSL para el env�o. En el caso de Gmail requiere que est� en True
      oCDO.Configuration.Fields.Item("http://schemas.microsoft.com/cdo/configuration/smtpusessl") = BoSSL
    
   End If
    
   ' Estructura del mail
   '''''''''''''''''''''''''''''''''''''''''''''''
    
   ' Direcci�n del Destinatario
   
   oCDO.To = StPara
    
   ' Direcci�n del remitente
   oCDO.From = StDe
    
   ' Asunto del mensaje
   oCDO.Subject = StAsunto
    
   ' Cuerpo del mensaje
   oCDO.TextBody = StMensaje
    
   'Ruta del archivo adjunto
   For Ini = 0 To UBound(StArr)
      If StArr(Ini) <> NUL$ Then
         If Len(Dir(StArr(Ini))) = 0 Then
            ' ..error
            StMsj = "No se ha encontrado el archivo en la siguiente ruta: " & StArr(Ini)
            Exit Function
         Else
            ' ..lo agrega
             oCDO.AddAttachment (StArr(Ini))
         End If
      End If
   Next
    
   ' Actualiza los datos antes de enviar
   oCDO.Configuration.Fields.Update
    
   On Error Resume Next
   Screen.MousePointer = vbHourglass
   ' Env�a el email
   oCDO.Send
   Screen.MousePointer = 0
   ' .. si no hubo error
   If ERR.Number = 0 Then
      Enviar_Backup = True
      StMsj = "Envio de correo de forma Exitosa"
   ElseIf ERR.Number = -2147220973 Then
      StMsj = "Posible error : nombre del Servidor incorrecto o n�mero de puerto incorrecto. Error:" & ERR.Number
   ElseIf ERR.Number = -2147220975 Then
      StMsj = "Posible error : error en la el nombre de usuario, o en el password. Error:" & ERR.Number
   Else
      StMsj = ERR.Description & ERR.Number
   End If

   ' Descarga la referencia
   If Not oCDO Is Nothing Then
      Set oCDO = Nothing
   End If
    
   ERR.Clear
    
   Screen.MousePointer = vbNormal
End Function

' propiedades
'''''''''''''''''''''
'---------------------------------------------------------------------------------------
' Procedure : servidor
' DateTime  : 24/10/2018 09:55
' Author    : daniel_mesa
' Purpose   : DRMG T45220 OBTIENE EL VALOR DEL SERVIDOR
'---------------------------------------------------------------------------------------
'
Property Get servidor() As String
    servidor = StServidor
End Property
'---------------------------------------------------------------------------------------
' Procedure : servidor
' DateTime  : 24/10/2018 09:56
' Author    : daniel_mesa
' Purpose   : DRMG T45220 RESIVE EL VALOR DEL SERVIDOR
'---------------------------------------------------------------------------------------
'
Property Let servidor(stvalue As String)
   StServidor = stvalue
End Property


'---------------------------------------------------------------------------------------
' Procedure : para
' DateTime  : 24/10/2018 09:57
' Author    : daniel_mesa
' Purpose   : DRMG T45220 OBTIENE EL DE QUIEN VA DIRIGUIDO
'---------------------------------------------------------------------------------------
'
Property Get para() As String
   para = StPara
End Property
'---------------------------------------------------------------------------------------
' Procedure : para
' DateTime  : 24/10/2018 09:57
' Author    : daniel_mesa
' Purpose   : DRMG T45220 RECIBE EL CORRE DE QUIEN VA DIRIGUIDO
'---------------------------------------------------------------------------------------
'
Property Let para(stvalue As String)
   StPara = stvalue
End Property


'---------------------------------------------------------------------------------------
' Procedure : de
' DateTime  : 24/10/2018 09:58
' Author    : daniel_mesa
' Purpose   : DRMG T45220 OBTIENE EL  CORREO DEL REMITENTE
'---------------------------------------------------------------------------------------
'
Property Get de() As String
   de = StDe
End Property
'---------------------------------------------------------------------------------------
' Procedure : de
' DateTime  : 24/10/2018 09:58
' Author    : daniel_mesa
' Purpose   : DRMG T45220 RECIBE EL CORREO DEL REMITENTE
'---------------------------------------------------------------------------------------
'
Property Let de(value As String)
   StDe = value
End Property


'---------------------------------------------------------------------------------------
' Procedure : Asunto
' DateTime  : 24/10/2018 09:59
' Author    : daniel_mesa
' Purpose   : DRMG T45220 OBTIENE EL ASUNTO
'---------------------------------------------------------------------------------------
'
Property Get Asunto() As String
   Asunto = StAsunto
End Property
'---------------------------------------------------------------------------------------
' Procedure : Asunto
' DateTime  : 24/10/2018 10:00
' Author    : daniel_mesa
' Purpose   : DRMG T45220 RECIBE EL ASUNTO
'---------------------------------------------------------------------------------------
'
Property Let Asunto(stvalue As String)
   StAsunto = stvalue
End Property


'---------------------------------------------------------------------------------------
' Procedure : Mensaje
' DateTime  : 24/10/2018 10:00
' Author    : daniel_mesa
' Purpose   : DRMG T45220 OBTIENE EL DETTALLE
'---------------------------------------------------------------------------------------
'
Property Get Mensaje() As String
   Mensaje = StMensaje
End Property
'---------------------------------------------------------------------------------------
' Procedure : Mensaje
' DateTime  : 24/10/2018 10:00
' Author    : daniel_mesa
' Purpose   : DRMG T45220 RTECIBE EL DETALLE
'---------------------------------------------------------------------------------------
'
Property Let Mensaje(value As String)
   StMensaje = value
End Property


'---------------------------------------------------------------------------------------
' Procedure : Adjunto
' DateTime  : 24/10/2018 10:00
' Author    : daniel_mesa
' Purpose   : DRMG T45220 OBTIENE LA RUTA DE LOS ARCHIVOS AJUNTO
'---------------------------------------------------------------------------------------
'
Property Get Adjunto() As String
   Adjunto = StAdjunto
End Property
'---------------------------------------------------------------------------------------
' Procedure : Adjunto
' DateTime  : 24/10/2018 10:01
' Author    : daniel_mesa
' Purpose   : DRMG T45220 RECIBE LAS RUTAS DE LOS ARCIVOS AJUNTO
'---------------------------------------------------------------------------------------
'
Property Let Adjunto(stvalue As String)
   StAdjunto = stvalue
End Property


'---------------------------------------------------------------------------------------
' Procedure : puerto
' DateTime  : 24/10/2018 10:01
' Author    : daniel_mesa
' Purpose   : DRMG T45220 OBTIENE EL PUERTO
'---------------------------------------------------------------------------------------
'
Property Get puerto() As Variant
   puerto = StPuerto
End Property
'---------------------------------------------------------------------------------------
' Procedure : puerto
' DateTime  : 24/10/2018 10:02
' Author    : daniel_mesa
' Purpose   : DRMG T45220 RECIBE EL PUERTO
'---------------------------------------------------------------------------------------
'
Property Let puerto(stvalue As Variant)
   StPuerto = stvalue
End Property


'---------------------------------------------------------------------------------------
' Procedure : Usuario
' DateTime  : 24/10/2018 10:02
' Author    : daniel_mesa
' Purpose   : DRMG T45220 OBTIENE EL USUARIO
'---------------------------------------------------------------------------------------
'
Property Get Usuario() As String
   Usuario = StUsuario
End Property
'---------------------------------------------------------------------------------------
' Procedure : Usuario
' DateTime  : 24/10/2018 10:02
' Author    : daniel_mesa
' Purpose   : DRMG T45220 RECIBE EL USUARIO
'---------------------------------------------------------------------------------------
'
Property Let Usuario(stvalue As String)
   StUsuario = stvalue
End Property


'---------------------------------------------------------------------------------------
' Procedure : PassWord
' DateTime  : 24/10/2018 10:03
' Author    : daniel_mesa
' Purpose   : DRMG T45220 OBTIENE LA CONTRASE�A
'---------------------------------------------------------------------------------------
'
Property Get PassWord() As String
   PassWord = StPassword
End Property
'---------------------------------------------------------------------------------------
' Procedure : PassWord
' DateTime  : 24/10/2018 10:03
' Author    : daniel_mesa
' Purpose   : DRMG T45220 RECIBE LA CONTRASE�A
'---------------------------------------------------------------------------------------
'
Property Let PassWord(stvalue As String)
   StPassword = stvalue
End Property


'---------------------------------------------------------------------------------------
' Procedure : UseAuntentificacion
' DateTime  : 24/10/2018 10:12
' Author    : daniel_mesa
' Purpose   : DRMG T45220 obtiene el valor si requiere autenticacion
'---------------------------------------------------------------------------------------
'
Property Get UseAuntentificacion() As Boolean
   UseAuntentificacion = BoUseAuntentificacion
End Property
'---------------------------------------------------------------------------------------
' Procedure : UseAuntentificacion
' DateTime  : 24/10/2018 10:13
' Author    : daniel_mesa
' Purpose   : DRMG T45220 recibe el valor si requiere autenticacion
'---------------------------------------------------------------------------------------
'
Property Let UseAuntentificacion(BoValue As Boolean)
   BoUseAuntentificacion = BoValue
End Property


'---------------------------------------------------------------------------------------
' Procedure : ssl
' DateTime  : 24/10/2018 10:13
' Author    : daniel_mesa
' Purpose   : DRMG T45220 obtiene el valor si requiere protocolo SSL
'---------------------------------------------------------------------------------------
'
Property Get ssl() As Boolean
   ssl = BoSSL
End Property
'---------------------------------------------------------------------------------------
' Procedure : ssl
' DateTime  : 24/10/2018 10:13
' Author    : daniel_mesa
' Purpose   : DRMG T45220 recibe el valor si requiere protocolo SSL
'---------------------------------------------------------------------------------------
'
Property Let ssl(BoValue As Boolean)
   BoSSL = BoValue
End Property





