VERSION 5.00
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "MSFLXGRD.OCX"
Begin VB.Form frmOtrosDescuentos 
   Caption         =   "Forma de Pago"
   ClientHeight    =   4515
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   8550
   LinkTopic       =   "Form1"
   ScaleHeight     =   4515
   ScaleWidth      =   8550
   Begin VB.Frame frmOTROSDESCUENTOS 
      Height          =   4335
      Left            =   120
      TabIndex        =   0
      Top             =   0
      Visible         =   0   'False
      Width           =   8295
      Begin VB.Frame Frame1 
         Height          =   2055
         Left            =   4560
         TabIndex        =   6
         Top             =   1320
         Width           =   3615
         Begin VB.TextBox txtValorDescuento 
            Alignment       =   1  'Right Justify
            Height          =   375
            Left            =   1680
            TabIndex        =   8
            Text            =   "$ 0"
            Top             =   1200
            Width           =   1815
         End
         Begin VB.ComboBox cboOtrosDescuentos 
            Height          =   315
            Left            =   1200
            TabIndex        =   7
            Top             =   360
            Width           =   2295
         End
         Begin VB.Label Label2 
            Alignment       =   1  'Right Justify
            Caption         =   "Valor:"
            Height          =   375
            Left            =   240
            TabIndex        =   10
            Top             =   1200
            Width           =   1095
         End
         Begin VB.Label Label3 
            Alignment       =   1  'Right Justify
            Caption         =   "Descu:"
            Height          =   375
            Left            =   120
            TabIndex        =   9
            Top             =   360
            Width           =   855
         End
      End
      Begin VB.Frame Frame2 
         Height          =   855
         Left            =   1680
         TabIndex        =   3
         Top             =   360
         Width           =   4935
         Begin VB.TextBox txtValorOtrosDescuentos 
            Alignment       =   1  'Right Justify
            Enabled         =   0   'False
            Height          =   375
            Left            =   2640
            TabIndex        =   4
            Text            =   "$ 0"
            Top             =   360
            Width           =   1935
         End
         Begin VB.Label Label1 
            Alignment       =   1  'Right Justify
            Caption         =   "Valor a Pagar:"
            Height          =   375
            Left            =   960
            TabIndex        =   5
            Top             =   360
            Width           =   1455
         End
      End
      Begin VB.CommandButton cmdOtrosCancelar 
         Caption         =   "&Cancelar"
         Height          =   495
         Left            =   2400
         TabIndex        =   2
         Top             =   3600
         Width           =   1695
      End
      Begin VB.CommandButton cmdOtrosDescuentos 
         Caption         =   "&Otros Cargos"
         Default         =   -1  'True
         Enabled         =   0   'False
         Height          =   495
         Left            =   4680
         TabIndex        =   1
         Top             =   3600
         Width           =   1695
      End
      Begin MSFlexGridLib.MSFlexGrid grdOtrosDescuentos 
         Height          =   1935
         Left            =   120
         TabIndex        =   11
         Top             =   1440
         Width           =   4095
         _ExtentX        =   7223
         _ExtentY        =   3413
         _Version        =   393216
         Cols            =   3
         FixedCols       =   0
         ForeColorSel    =   -2147483635
         FocusRect       =   2
         MergeCells      =   1
      End
   End
End
Attribute VB_Name = "frmOtrosDescuentos"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private vTotalOtrosDescuentos As Single
Private vSeDescuenta As Boolean
Dim itmOtrosDescuentos() As Variant

Private Sub cmdOtrosCancelar_Click()
    Me.Hide
End Sub

Private Sub cmdOtrosCargos_Click()
'    If Not CSng(Me.txtValorOtrosCargos.Text) = SumaOtrosCargos() Then
'        Me.cmdOtrosCargos.Enabled = False
'        Exit Sub
'    End If
    vSeCarga = True
    Me.Hide
End Sub

Private Sub otroscargosForm_Load()
''SELECT CD_CODI_IMPU, DE_NOMB_IMPU FROM TC_IMPUESTOS WHERE ID_TIPO_IMPU='O'
    vSeCarga = False
'    Me.txtValorOtrosCargos = FormatCurrency(vTotalOtrosCargos, 2)
'    Me.txtValorCargo = FormatCurrency(vTotalOtrosCargos, 2)
    Me.grdOtrosCargos.Rows = 1
    Result = loadctrl("TC_IMPUESTOS", "DE_NOMB_IMPU", "CD_CODI_IMPU", Me.cboOtrosCargos, itmOtrosCargos, "ID_TIPO_IMPU='O'")
    Call GrdFDef(Me.grdOtrosCargos, 0, 0, "C�digo,0,Nombre,2500,Valor,+1200")
'    Call InsertaOtroCargo("0", vTotalOtrosCargos)
End Sub

Public Property Get SeCarga() As Boolean
    SeCarga = vSeCarga
End Property

Public Property Let TotalOtrosCargos(Num As Single)
    vTotalOtrosCargos = Num
End Property

Private Sub InsertaOtroCargo(Optional vTipo As String = "", Optional vValor As Single = 0)
    Dim vFil As Integer
    If Len(vTipo) > 0 Then Me.cboOtrosCargos.ListIndex = FindInArr(itmOtrosCargos, vTipo)
    If Not Me.cboOtrosCargos.ListIndex > -1 Then Exit Sub
    If Not CSng(Me.txtValorCargo.Text) > 0 Then
        For vFil = 1 To Me.grdOtrosCargos.Rows - 1
            If Me.grdOtrosCargos.TextMatrix(vFil, 0) = itmOtrosCargos(Me.cboOtrosCargos.ListIndex) Then
                Me.grdOtrosCargos.RemoveItem vFil
            End If
        Next
        Exit Sub
    Else
        For vFil = 1 To Me.grdOtrosCargos.Rows - 1
            If Me.grdOtrosCargos.TextMatrix(vFil, 0) = itmOtrosCargos(Me.cboOtrosCargos.ListIndex) Then
                Me.grdOtrosCargos.TextMatrix(vFil, 1) = Me.cboOtrosCargos.List(Me.cboOtrosCargos.ListIndex)
                Me.grdOtrosCargos.TextMatrix(vFil, 2) = FormatCurrency(CSng(Me.txtValorCargo.Text), 2)
                Exit Sub
            End If
        Next
    End If
    Me.grdOtrosCargos.Rows = Me.grdOtrosCargos.Rows + 1
    Me.grdOtrosCargos.Row = Me.grdOtrosCargos.Rows - 1
    Me.grdOtrosCargos.TextMatrix(Me.grdOtrosCargos.Row, 0) = itmOtrosCargos(Me.cboOtrosCargos.ListIndex)
    Me.grdOtrosCargos.TextMatrix(Me.grdOtrosCargos.Row, 1) = Me.cboOtrosCargos.List(Me.cboOtrosCargos.ListIndex)
    Me.grdOtrosCargos.TextMatrix(Me.grdOtrosCargos.Row, 2) = FormatCurrency(CSng(Me.txtValorCargo.Text), 2)
End Sub

Private Function SumaOtrosCargos() As Single
    For vFil = 1 To Me.grdOtrosCargos.Rows - 1
        SumaOtrosCargos = SumaOtrosCargos + CSng(Me.grdOtrosCargos.TextMatrix(vFil, 2))
    Next
End Function

Private Sub txtValorCargo_GotFocus()
'    Me.txtValorCargo.Text = FormatCurrency(Me.txtValorOtrosCargos - SumaOtrosCargos(), 2)
    Me.txtValorCargo.Text = FormatCurrency(0, 2)
End Sub

Private Sub txtValorCargo_KeyPress(KeyAscii As Integer)
    Call Cambiar_Enter(KeyAscii)
End Sub

Private Sub txtValorCargo_LostFocus()
    Me.txtValorCargo.Text = FormatCurrency(IIf(Len(Me.txtValorCargo.Text) > 0, CSng(Me.txtValorCargo.Text), 0), 2)
    Call InsertaOtroCargo
End Sub

