VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Begin VB.Form FrmOpciones 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Opciones"
   ClientHeight    =   6225
   ClientLeft      =   1575
   ClientTop       =   1515
   ClientWidth     =   7710
   HelpContextID   =   23
   Icon            =   "FrmOpciones.frx":0000
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   PaletteMode     =   1  'UseZOrder
   ScaleHeight     =   6225
   ScaleWidth      =   7710
   Tag             =   "10000201"
   Begin VB.Frame Frame2 
      Height          =   3615
      Left            =   120
      TabIndex        =   10
      Top             =   2400
      Width           =   7455
      Begin MSComctlLib.ListView Lstw 
         Height          =   3255
         Left            =   120
         TabIndex        =   12
         Top             =   240
         Width           =   7215
         _ExtentX        =   12726
         _ExtentY        =   5741
         View            =   3
         LabelEdit       =   1
         MultiSelect     =   -1  'True
         LabelWrap       =   -1  'True
         HideSelection   =   -1  'True
         AllowReorder    =   -1  'True
         Checkboxes      =   -1  'True
         FullRowSelect   =   -1  'True
         _Version        =   393217
         ForeColor       =   -2147483640
         BackColor       =   -2147483643
         BorderStyle     =   1
         Appearance      =   1
         NumItems        =   1
         BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Text            =   "Modulos "
            Object.Width           =   12347
         EndProperty
      End
   End
   Begin VB.Frame Frame1 
      Height          =   1815
      Left            =   120
      TabIndex        =   0
      Top             =   480
      Width           =   7455
      Begin VB.CheckBox Chk_Raiz 
         Caption         =   "Pertenece a la raiz"
         Height          =   255
         Left            =   120
         TabIndex        =   15
         Top             =   1440
         Width           =   2775
      End
      Begin VB.CommandButton cmd_SelOpc 
         DisabledPicture =   "FrmOpciones.frx":058A
         Height          =   300
         Index           =   0
         Left            =   2760
         Picture         =   "FrmOpciones.frx":0AD4
         Style           =   1  'Graphical
         TabIndex        =   14
         Top             =   240
         UseMaskColor    =   -1  'True
         Width           =   495
      End
      Begin VB.CommandButton cmd_SelOpc 
         DisabledPicture =   "FrmOpciones.frx":0F06
         Height          =   300
         Index           =   1
         Left            =   3000
         Picture         =   "FrmOpciones.frx":1450
         Style           =   1  'Graphical
         TabIndex        =   13
         Top             =   960
         UseMaskColor    =   -1  'True
         Width           =   495
      End
      Begin VB.CommandButton Cmd_Arbol 
         Caption         =   "Arbol"
         Height          =   375
         Left            =   6240
         TabIndex        =   11
         Top             =   160
         Width           =   1095
      End
      Begin VB.TextBox Txt_Codigo 
         Height          =   285
         Left            =   840
         MaxLength       =   12
         TabIndex        =   2
         Top             =   240
         Width           =   1815
      End
      Begin VB.TextBox Txt_Desc 
         Height          =   285
         Left            =   840
         MaxLength       =   50
         TabIndex        =   4
         Top             =   600
         Width           =   6495
      End
      Begin VB.TextBox Txt_CodPadre 
         Height          =   285
         Left            =   1560
         MaxLength       =   12
         TabIndex        =   6
         Top             =   960
         Width           =   1335
      End
      Begin VB.TextBox Txt_DescPadre 
         Height          =   285
         Left            =   4200
         Locked          =   -1  'True
         MaxLength       =   50
         TabIndex        =   8
         Top             =   960
         Width           =   3135
      End
      Begin VB.Label Lbl_Codigo 
         AutoSize        =   -1  'True
         Caption         =   "&C�digo:"
         Height          =   195
         Index           =   0
         Left            =   120
         TabIndex        =   1
         Top             =   240
         Width           =   540
      End
      Begin VB.Label Lbl_Nombre 
         AutoSize        =   -1  'True
         Caption         =   "&Nombre"
         Height          =   195
         Index           =   1
         Left            =   120
         TabIndex        =   3
         Top             =   600
         Width           =   555
      End
      Begin VB.Label Label1 
         Caption         =   "Codigo Nodo Padre"
         Height          =   255
         Left            =   120
         TabIndex        =   5
         Top             =   960
         Width           =   1575
      End
      Begin VB.Label Label2 
         Caption         =   "Nombre"
         Height          =   255
         Left            =   3600
         TabIndex        =   7
         Top             =   960
         Width           =   615
      End
   End
   Begin MSComctlLib.Toolbar Tlb_Opciones 
      Align           =   1  'Align Top
      Height          =   420
      Left            =   0
      TabIndex        =   9
      Top             =   0
      Width           =   7710
      _ExtentX        =   13600
      _ExtentY        =   741
      ButtonWidth     =   609
      ButtonHeight    =   582
      Appearance      =   1
      _Version        =   393216
      BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
         NumButtons      =   7
         BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Style           =   3
         EndProperty
         BeginProperty Button2 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "G"
            Object.ToolTipText     =   "Guardar"
         EndProperty
         BeginProperty Button3 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "B"
            Object.ToolTipText     =   "Borrar"
         EndProperty
         BeginProperty Button4 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "I"
            Object.ToolTipText     =   "Imprimir"
         EndProperty
         BeginProperty Button5 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Style           =   3
         EndProperty
         BeginProperty Button6 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Enabled         =   0   'False
            Key             =   "C"
            Object.ToolTipText     =   "Cambiar C�digo"
         EndProperty
         BeginProperty Button7 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "A"
            Object.ToolTipText     =   "Ayuda"
         EndProperty
      EndProperty
   End
End
Attribute VB_Name = "FrmOpciones"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Dim Encontro_Opcion As Integer
Dim Tabla           As String
'Dim TreeOpc         As TreeView        'DEPURACION DE CODIGO
Dim Mensaje         As String
Dim arrEventos()    As Variant
Public OpcCod       As String   'Opci�n de seguridad
Public OpcReg       As String   'Opci�n a registrar
Dim OtroCampo       As String
Dim camCodigo       As Boolean
Dim Nuevo           As String

Private Sub Chk_Raiz_Click()
   If Chk_Raiz.Value = 1 Then
      Txt_CodPadre.Text = "CNT"
      Txt_DescPadre.Text = "CNT"
      Txt_CodPadre.Enabled = False
      Txt_DescPadre.Enabled = False
   Else
      Txt_CodPadre.Enabled = True
      Txt_DescPadre.Enabled = True
   End If
End Sub

Private Sub Cmd_Arbol_Click()
   Frmarbol.Show
End Sub

Private Sub cmd_SelOpc_Click(Index As Integer)
   Dim Arrsel() As Variant
   
   ReDim Arrsel(3, 1)
   Arrsel(0, 0) = "TX_CODI_OPCI": Arrsel(1, 0) = "Codigo":       Arrsel(2, 0) = "1200": Arrsel(3, 0) = "T"
   Arrsel(0, 1) = "TX_DESC_OPCI": Arrsel(1, 1) = "Descripcion":  Arrsel(2, 1) = "4000": Arrsel(3, 1) = "T"

   Select Case Index
      Case 0 'Busca opciones registradas
         Condicion = NUL$
         Codigo = Seleccion_opcion("OPCION", Condicion, Arrsel, 0, False, Me.Caption)
         If Codigo = NUL$ Then
            Codigo = 0
         Else
            Txt_Codigo = Codigo
            Txt_Codigo_LostFocus
         End If
      Case 1 'Asigna c�digo padre a la opci�n
         Condicion = NUL$
         Condicion = "TX_CODI_OPCI <> " & Comi & Txt_Codigo.Text & Comi
         'Pendiente validar condicion para motro oracle
         Condicion = Condicion & " AND NU_AUTO_FORM_OPCI is null "
         Codigo = Seleccion_opcion("OPCION", Condicion, Arrsel, 0, False, Me.Caption)
         If Codigo = NUL$ Then
            Codigo = 0
         Else
            Txt_CodPadre.Text = Codigo
            Txt_CodPadre_LostFocus
            If Lstw.Enabled = True Then Lstw.SetFocus
         End If
   End Select
End Sub

'DEPURACION DE CODIGO
'Private Sub Command1_Click()
'   Call Grabar
'End Sub

Private Sub Form_Activate()
   FrmMenu.CodOpc = OpcCod
End Sub

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
   Select Case KeyCode
      Case vbKeyF1:
      Case vbKeyF2: Call Grabar
      Case vbKeyF3: Call Borrar
      Case vbKeyF5:
      Case vbKeyF6:
      Case vbKeyF7:
      Case vbKeyF8:
      Case vbKeyF10:
      Case vbKeyF12: Unload Me
   End Select
End Sub

Private Sub Form_Load()
   Mensaje = "Opci�n"
   Tabla = "OPCION"
   Call CenterForm(MDI_Inventarios, Me)
   Call LoadIconos(Tlb_Opciones)
   Call EventosControles(Me, arrEventos)
   Call Estado_Botones_Forma(Tlb_Opciones, OpcCod)
   'Desde = "FORMULARIO ORDER BY TX_DESC_FORM"
   Desde = "FORMULARIO"     'LJSA Depuraci�n de Noviembre 2008
   Campos = "TX_DESC_FORM "
   OtroCampo = "NU_AUTO_FORM "
   Condicion = NUL$
   'Condicion = " NU_AUTO_FORM NOT IN (SELECT NU_AUTO_FORM_OPCI FROM OPCION WHERE NU_AUTO_FORM = NU_AUTO_FORM_OPCI)"
   'LJSA Depuraci�n de Noviembre 2008----------------
   Condicion = "NU_AUTO_FORM NOT IN (SELECT NU_AUTO_FORM FROM FORMULARIO WHERE "
   Condicion = Condicion & "TX_DESC_FORM = 'COTIZACION/LISTA PRECIOS PROVEEDOR' "
   Condicion = Condicion & "Or TX_DESC_FORM = 'CRITERIOS DE COMPRA' "
   Condicion = Condicion & "Or TX_DESC_FORM = 'CRITERIOS X PROVEEDOR' "
   Condicion = Condicion & "Or TX_DESC_FORM = 'PROYECTO DE COMPRA') "
   Condicion = Condicion & "Order By TX_DESC_FORM"
   'LJSA Depuraci�n de Noviembre 2008----------------
   Call LoadListView(Lstw, Desde, Campos, OtroCampo, Condicion)
   Call MouseNorm
End Sub

Private Sub Form_Unload(Cancel As Integer)
   Call DestruirControles(arrEventos)
End Sub

Private Sub Lstw_ItemCheck(ByVal Item As MSComctlLib.ListItem)
   Dim i As Integer
   OpcReg = ""
   For i = 1 To Lstw.ListItems.Count
      Lstw.ListItems(i).Checked = False
   Next i
   Item.Checked = True
   OpcReg = Mid(Item.Key, 2, Len(Item.Key))
End Sub

Private Sub Tlb_Opciones_ButtonClick(ByVal Button As MSComctlLib.Button)
   Select Case Button.Key
      Case "G": Encontro = Encontro_Opcion
                Call Grabar
      Case "B": Call Borrar
      Case "C": Call cambiar
      Case "I": 'DAHV M0003306
                
                ReDim Arr(1)
                Result = LoadData("ENTIDAD", "CD_NIT_ENTI, NO_NOMB_ENTI", NUL$, Arr)
                If Result <> FAIL Then
                    Entidad = Arr(1)
                    Nit = Arr(0)
                End If
                Call Listar1("InfOpciones.rpt", NUL$, NUL$, 0, "LISTADO DE OPCIONES", MDI_Inventarios)
      
   End Select
End Sub

Private Sub cambiar()
   Dim TipoCodi As String
   
   TipoCodi = "T"
   If Tlb_Opciones.Buttons("C").Enabled = False Then Exit Sub
   Nuevo = Cambiar_Codigo(Txt_Codigo.MaxLength, TipoCodi)
   If Nuevo <> NUL$ Then
      camCodigo = True
      Call Grabar
   End If
End Sub

Public Sub Grabar()
   Dim Arr(0)  As Variant
   Dim OpcionC As Variant
   
   DoEvents
   
   Call MouseClock
   Msglin "Ingresando informaci�n del " & Mensaje
      
   If Txt_Codigo = NUL$ Then
      Call Mensaje1("Se debe especifica el codigo de la " & Mensaje, 3)
      Txt_Codigo.SetFocus
      Call MouseNorm: Exit Sub
   End If
   If Txt_Desc = NUL$ Then
      Call Mensaje1("Se debe especificar el nombre de la " & Mensaje, 3)
      Txt_Desc.SetFocus
      Call MouseNorm: Exit Sub
   End If
   If Txt_CodPadre = NUL$ Then
      Call Mensaje1("Se debe especificar el nodo padre de la " & Mensaje, 3)
      Txt_CodPadre.SetFocus
      Call MouseNorm: Exit Sub
   End If
'   If OpcReg = "" And Txt_CodPadre <> "CNT" Then
'   If OpcReg = "" Then
'      Call Mensaje1("Se debe especificar la que desea registrar", 3)
'      Lstw.SetFocus
'      Call MouseNorm: Exit Sub
'   End If
   Debug.Print OpcReg
   If (BeginTran(STranIUp & Tabla) <> FAIL) Then
      If camCodigo = True Then
         Valores = "TX_CODI_OPCI=" & Comi & Nuevo & Comi & Coma
      Else
         Valores = "TX_CODI_OPCI=" & Comi & Txt_Codigo & Comi & Coma
      End If
       Valores = Valores & "TX_DESC_OPCI=" & Comi & Cambiar_Comas_Comillas(Txt_Desc) & Comi & Coma
       Valores = Valores & "TX_CODPADR_OPCI=" & Comi & Txt_CodPadre & Comi & Coma
      If Txt_CodPadre <> "CNT" And Val(OpcReg) <> 0 Then
         Valores = Valores & "NU_AUTO_FORM_OPCI = " & CInt(OpcReg)
      Else
         'Verifica si tiene hijos asociados
         Dim ArrPadre() As Variant
         ReDim ArrPadre(0)
         Campos = "COUNT(*)"
         Desde = "OPCION"
         Condicion = "TX_CODPADR_OPCI = " & Comi & Trim$(Txt_Codigo.Text) & Comi
         Result = LoadData(Desde, Campos, Condicion, ArrPadre)
         If Result = FAIL Then
            Debug.Print "No encontro resgistros"
         End If
         
         If Arr(0) > 0 Then
            Call Mensaje1("No puede asociar la pantalla, esta modulo tienes varias opciones relacionadas. ", 3)
            Call MouseNorm
            Exit Sub
         Else
            Valores = Valores & "NU_AUTO_FORM_OPCI = " & IIf(CInt(OpcReg) = 0, "Null", CInt(OpcReg))
         End If
      End If
       If (Not Encontro_Opcion) Then
         If Not WarnIns() Then
            Result = FAIL
         Else
            Result = DoInsertSQL(Tabla, Valores)
            OpcionC = Txt_Codigo
            Call CrearOpcion(OpcionC)
         End If
       Else
         If WarnMsg("Desea Modificar la Informaci�n de la " & Mensaje) Then
            Condicion = "TX_CODI_OPCI=" & Comi & Txt_Codigo.Text & Comi
            Result = DoUpdate(Tabla, Valores, Condicion)
         End If
       End If
   End If
   
   If (Result <> FAIL) Then
      If (CommitTran() <> FAIL) Then
          Call Limpiar
      Else
         Call RollBackTran
      End If
   Else
      Call RollBackTran
   End If
   Call MouseNorm
   Msglin NUL$
End Sub

Private Sub Borrar()
   ReDim Arr(0) As Variant
   
   DoEvents
   
   Call MouseClock
   
   'Verifica si esta la opci�n a eliminar tiene hijos
   Campos = "COUNT(OPCION.NU_AUTO_OPCI) AS Hijos"
   Desde = "OPCION"
   Condicion = "TX_CODPADR_OPCI = " & Comi & Txt_Codigo & Comi
   Result = LoadData(Desde, Campos, Condicion, Arr())
   If Arr(0) > 0 Then
      Call Mensaje1("No se pudo borrar la opcion " & Me.Txt_Desc & " de la base de datos, debido a que tiene modulos relacionados !!!...", 1)
      Call MouseNorm
      Exit Sub
   End If
   
   Msglin "Borrando un " & Mensaje
   If (Txt_Codigo = NUL$) Then
       Call Mensaje1("Ingrese el c�digo del " & Mensaje, 1)
       Call MouseNorm: Msglin NUL$: Exit Sub
   End If
   
   If (Encontro_Opcion <> False) Then
      'Busca el autonumerico de la opci�n
      Campos = "NU_AUTO_OPCI"
      Desde = "OPCION"
      Condicion = "TX_CODI_OPCI = " & Comi & Txt_Codigo & Comi
      Result = LoadData(Desde, Campos, Condicion, Arr())
      
      If (Not WarnDel()) Then Call MouseNorm: Msglin NUL$: Exit Sub
      Condicion = "TX_CODI_OPCI=" & Comi & Txt_Codigo & Comi

         If (BeginTran(STranDel & Tabla) <> FAIL) Then
            'Borra los permisos de la opci�n
            Result = DoDelete("PERMISO", "NU_AUTO_OPCI_PERM = " & Arr(0))
            If Result <> FAIL Then Result = DoDelete(Tabla, Condicion)
'            If (Result <> FAIL) Then Result = Auditor(TABLA, TranDel, LastCmd)
         End If
   Else
      Call Mensaje1("No se pudo borrar el " & Mensaje & " de la base de datos, debido a que no existe !!!...", 1)
      Call MouseNorm: Msglin NUL$
   End If
   If (Result <> FAIL) Then
       If (CommitTran() <> FAIL) Then
          Limpiar
       Else
          Call RollBackTran
       End If
   Else
       Call RollBackTran
   End If
   Call MouseNorm
   Msglin NUL$
End Sub

Private Sub Limpiar()
'   Dim I As Integer        'DEPURACION DE CODIGO
   
   Call MouseClock
   Msglin "Limpiando la ventana"
   
   Txt_Codigo = ""
   Txt_Desc = ""
   Txt_CodPadre = ""
   Txt_DescPadre = ""
   Chk_Raiz.Value = False
   Desde = "FORMULARIO"
   Campos = "TX_DESC_FORM "
   OtroCampo = "NU_AUTO_FORM "
   
   'LJSA Depuraci�n de Noviembre 2008----------------
   Condicion = "NU_AUTO_FORM NOT IN (SELECT NU_AUTO_FORM FROM FORMULARIO WHERE "
   Condicion = Condicion & "TX_DESC_FORM = 'COTIZACION/LISTA PRECIOS PROVEEDOR' "
   Condicion = Condicion & "Or TX_DESC_FORM = 'CRITERIOS DE COMPRA' "
   Condicion = Condicion & "Or TX_DESC_FORM = 'CRITERIOS X PROVEEDOR' "
   Condicion = Condicion & "Or TX_DESC_FORM = 'PROYECTO DE COMPRA') "
   'LJSA Depuraci�n de Noviembre 2008----------------
   
   'Condicion = " TX_FORM_FORM <> '' ORDER BY TX_DESC_FORM"
   Condicion = Condicion & " And TX_FORM_FORM <> '' ORDER BY TX_DESC_FORM"
   'Condicion = " NU_AUTO_FORM NOT IN (SELECT NU_AUTO_FORM_OPCI FROM OPCION WHERE NU_AUTO_FORM = NU_AUTO_FORM_OPCI)"
   
   Call LoadListView(Lstw, Desde, Campos, OtroCampo, Condicion)
   Nuevo = ""
   camCodigo = False
   Tlb_Opciones.Buttons("C").Enabled = False
   Txt_Codigo.SetFocus
   Call MouseNorm
   Msglin NUL$

End Sub

Private Sub Txt_Codigo_GotFocus()
   Txt_Codigo.Tag = Txt_Codigo.Text
   Msglin "Digite el c�digo de la " & Mensaje
End Sub

Private Sub Txt_Codigo_KeyPress(KeyAscii As Integer)
   Call ValKeyAlfaNum(KeyAscii)
End Sub

Private Sub Txt_Codigo_LostFocus()
ReDim Arr(2)
   Condicion = NUL$
   Chk_Raiz.Value = False
   'Condicion = " NU_AUTO_FORM NOT IN (SELECT NU_AUTO_FORM_OPCI FROM OPCION WHERE NU_AUTO_FORM = NU_AUTO_FORM_OPCI)"
   
   'LJSA Depuraci�n de Noviembre 2008----------------
   Condicion = "NU_AUTO_FORM NOT IN (SELECT NU_AUTO_FORM FROM FORMULARIO WHERE "
   Condicion = Condicion & "TX_DESC_FORM = 'COTIZACION/LISTA PRECIOS PROVEEDOR' "
   Condicion = Condicion & "Or TX_DESC_FORM = 'CRITERIOS DE COMPRA' "
   Condicion = Condicion & "Or TX_DESC_FORM = 'CRITERIOS X PROVEEDOR' "
   Condicion = Condicion & "Or TX_DESC_FORM = 'PROYECTO DE COMPRA') "
   Condicion = Condicion & "Order By TX_DESC_FORM"
   'LJSA Depuraci�n de Noviembre 2008----------------
   
   'Call LoadListView(Lstw, "FORMULARIO ORDER BY TX_DESC_FORM", "TX_DESC_FORM", "NU_AUTO_FORM", Condicion)
   Call LoadListView(Lstw, "FORMULARIO", "TX_DESC_FORM", "NU_AUTO_FORM", Condicion) 'LJSA Depuraci�n de c�digo Noviembre 2008
   If Txt_Codigo <> NUL$ Then
      Condicion = "TX_CODI_OPCI=" & Comi & Txt_Codigo.Text & Comi
      Campos = "TX_DESC_OPCI, TX_CODPADR_OPCI,NU_AUTO_FORM_OPCI"
      Result = LoadData(Tabla, Campos, Condicion, Arr())
      If (Result <> FAIL) And Encontro Then
         Txt_Desc.Text = Restaurar_Comas_Comillas(CStr(Arr(0)))
         Txt_CodPadre.Text = Arr(1)
         If Arr(1) = "CNT" Then
            Chk_Raiz.Value = 1
         End If
         OpcReg = Val(Arr(2))
         Encontro_Opcion = True
         Tlb_Opciones.Buttons("C").Enabled = True
      Else
         Txt_Desc.Text = NUL$
         Txt_CodPadre.Text = NUL$
         Txt_DescPadre.Text = NUL$
         OpcReg = 0
         Encontro_Opcion = False
         Tlb_Opciones.Buttons("C").Enabled = False
         Chk_Raiz.Value = False
''''         Call Limpiar
      End If
      
      'verifica si tiene hijos, si los tiene no puede seleccionar nada de la lista
      'o si tiene una forma asigndada
      ReDim Arr(0)
      Condicion = "TX_CODPADR_OPCI=" & Comi & Txt_Codigo.Text & Comi
      Result = LoadData(Tabla, "COUNT(TX_CODI_OPCI)", Condicion, Arr)
      If Result = FAIL Then Exit Sub
      Lstw.Enabled = IIf(Val(Arr(0)) = 0, True, False)
      If Lstw.Enabled = True Then
         'If val(OpcReg) > 0 Then Lstw.Enabled = False
      End If
      'Busca el padre
''''''      If OpcReg <> NUL$ Then
''''''         ReDim arr(0)
''''''         Condicion = "NU_AUTO_FORM=" & val(OpcReg)
''''''         Result = LoadData("FORMULARIO", "TX_DESC_FORM", Condicion, arr)
''''''         If Result <> FAIL And Encontro Then
''''''            Lstw.ListItems.Add , "L" & val(OpcReg), Cambiar_Comas_Comillas(CStr(arr(0)))
''''''            Lstw.ListItems.Item(Lstw.ListItems.Count).Checked = True
''''''         End If
''''''      End If
      
      ReDim Arr(1)
      Condicion = "TX_CODI_OPCI=" & Comi & Txt_CodPadre.Text & Comi
      Result = LoadData(Tabla, "TX_DESC_OPCI,''", Condicion, Arr())
      If Result = FAIL Or Not Encontro Then Exit Sub
      Txt_DescPadre = Restaurar_Comas_Comillas(CStr(Arr(0)))
      
   End If
End Sub

Private Sub Txt_CodPadre_KeyPress(KeyAscii As Integer)
   Call ValKeyAlfaNum(KeyAscii)
End Sub

Private Sub Txt_CodPadre_LostFocus()
   ReDim Arr(0)
   Condicion = "TX_CODI_OPCI=" & Comi & Txt_CodPadre.Text & Comi
   'Pendiente validar condicion para motro oracle
   'Condicion = Condicion & " AND NU_AUTO_FORM_OPCI IS NULL"
   Result = LoadData(Tabla, "TX_DESC_OPCI", Condicion, Arr())
   If (Result <> False And Encontro) Then
       Txt_DescPadre = Restaurar_Comas_Comillas(CStr(Arr(0)))
   Else
      Txt_CodPadre.Text = NUL$
      Txt_DescPadre.Text = NUL$
   End If
End Sub

Private Sub Txt_Desc_KeyPress(KeyAscii As Integer)
   Call ValKeyAlfaNum(KeyAscii)
End Sub

Private Sub Txt_DescPadre_KeyPress(KeyAscii As Integer)
   Call ValKeyAlfaNum(KeyAscii)
End Sub

Function Seleccion_opcion(ByVal Tbla As String, ByVal Condi As String, Campos() As Variant, ByVal NroCampo As Integer, ByVal Multiple As Boolean, ByVal Titulo As String) As Variant
   Dim i   As Integer
   Dim ind As Byte
   
   With FrmSelgen
      .Sel_Cam = Campos
      .Sel_Tab = Tbla
      .Sel_Con = Condi
      .Sel_Nro = NroCampo
      .Sel_Mul = Multiple
      .TituloSel = Titulo
      'Verifica que tenga datos,y que esten completos
      For i = 0 To UBound(Campos, 2)
         If Campos(0, i) = NUL$ Then Call Mensaje1("Falta nombre del campo n�mero " & i, 3): ind = 1: Exit For
         If Campos(1, i) = NUL$ Then Call Mensaje1("Falta descripci�n del campo n�mero " & i, 3): ind = 1: Exit For
         If Not (IsNumeric(Campos(2, i))) Or Campos(2, i) = NUL$ Then
            Call Mensaje1("Falta ancho de la columna n�mero " & i, 3): ind = 1: Exit For
         End If
      Next i
      If NroCampo > UBound(Campos, 2) + 1 Then Call Mensaje1("N�mero de columna no valido", 3): ind = 1
   End With
   
   If ind = 1 Then
      Seleccion_opcion = NUL$
   Else
      FrmSelgen.Show 1
      If Multiple = False Then
         Seleccion_opcion = Codigo
      Else
         Seleccion_opcion = Sel_Registros
      End If
   End If
End Function

