VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Object = "{C932BA88-4374-101B-A56C-00AA003668DC}#1.1#0"; "MSMASK32.OCX"
Object = "{0BA686C6-F7D3-101A-993E-0000C0EF6F5E}#1.0#0"; "THREED32.OCX"
Begin VB.Form FrmExistxDisp 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Form1"
   ClientHeight    =   7260
   ClientLeft      =   45
   ClientTop       =   375
   ClientWidth     =   10860
   Icon            =   "FrmExistxDisp.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   7260
   ScaleWidth      =   10860
   Begin VB.ComboBox cboTIPO 
      Height          =   315
      ItemData        =   "FrmExistxDisp.frx":058A
      Left            =   3000
      List            =   "FrmExistxDisp.frx":058C
      TabIndex        =   0
      Text            =   "Combo1"
      Top             =   120
      Width           =   2655
   End
   Begin VB.Frame fraRANGO 
      Height          =   2175
      Left            =   5880
      TabIndex        =   4
      Top             =   4920
      Width           =   4815
      Begin VB.CheckBox chkFCRANGO 
         Caption         =   "Condicionar las fechas del reporte"
         Height          =   315
         Left            =   360
         TabIndex        =   5
         Top             =   240
         Width           =   4095
      End
      Begin VB.Frame Frame1 
         Height          =   615
         Left            =   240
         TabIndex        =   8
         Top             =   1080
         Width           =   3225
         Begin VB.OptionButton opcCON 
            Caption         =   "Costeado"
            Height          =   315
            Left            =   240
            TabIndex        =   9
            Top             =   200
            Value           =   -1  'True
            Width           =   1300
         End
         Begin VB.OptionButton opcSIN 
            Caption         =   "Sin Costear"
            Height          =   315
            Left            =   1800
            TabIndex        =   10
            Top             =   200
            Width           =   1300
         End
      End
      Begin VB.CheckBox ChkExistencia 
         Caption         =   "Mostrar articulo con existencia cero"
         Height          =   255
         Left            =   360
         TabIndex        =   11
         Top             =   1800
         Width           =   3015
      End
      Begin MSMask.MaskEdBox txtFCHASTA 
         Height          =   315
         Left            =   3240
         TabIndex        =   7
         Top             =   600
         Width           =   1215
         _ExtentX        =   2143
         _ExtentY        =   556
         _Version        =   393216
         PromptChar      =   "_"
      End
      Begin Threed.SSCommand cmdIMPRIMIR 
         Height          =   735
         Index           =   2
         Left            =   3600
         TabIndex        =   12
         Top             =   1200
         Width           =   855
         _Version        =   65536
         _ExtentX        =   1508
         _ExtentY        =   1296
         _StockProps     =   78
         Caption         =   "&IMPRIMIR"
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   6
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         RoundedCorners  =   0   'False
         Picture         =   "FrmExistxDisp.frx":058E
      End
      Begin MSMask.MaskEdBox txtFCDESDE 
         Height          =   315
         Left            =   960
         TabIndex        =   6
         Top             =   600
         Visible         =   0   'False
         Width           =   1215
         _ExtentX        =   2143
         _ExtentY        =   556
         _Version        =   393216
         PromptChar      =   "_"
      End
      Begin VB.Label Label1 
         Alignment       =   1  'Right Justify
         Caption         =   "Hasta:"
         Height          =   255
         Left            =   2400
         TabIndex        =   14
         Top             =   600
         Width           =   735
      End
      Begin VB.Label lblDESDE 
         Alignment       =   1  'Right Justify
         Caption         =   "Desde:"
         Height          =   255
         Left            =   120
         TabIndex        =   13
         Top             =   600
         Visible         =   0   'False
         Width           =   735
      End
   End
   Begin VB.CheckBox chkARTICULOS 
      Caption         =   "Todos Los Art�culos"
      Height          =   375
      Left            =   480
      TabIndex        =   1
      Top             =   720
      Width           =   2895
   End
   Begin MSComctlLib.ProgressBar pgbLLEVO 
      Height          =   255
      Left            =   6240
      TabIndex        =   15
      Top             =   120
      Visible         =   0   'False
      Width           =   4455
      _ExtentX        =   7858
      _ExtentY        =   450
      _Version        =   393216
      Appearance      =   1
      Max             =   1
   End
   Begin MSComctlLib.ListView lstBODEGAS 
      Height          =   2055
      Left            =   120
      TabIndex        =   3
      Top             =   4920
      Width           =   5655
      _ExtentX        =   9975
      _ExtentY        =   3625
      View            =   3
      LabelEdit       =   1
      MultiSelect     =   -1  'True
      LabelWrap       =   -1  'True
      HideSelection   =   -1  'True
      FullRowSelect   =   -1  'True
      GridLines       =   -1  'True
      _Version        =   393217
      ForeColor       =   -2147483640
      BackColor       =   -2147483643
      BorderStyle     =   1
      Appearance      =   1
      NumItems        =   0
   End
   Begin MSComctlLib.ListView lstARTICULOS 
      Height          =   3375
      Left            =   120
      TabIndex        =   2
      Top             =   1200
      Width           =   10620
      _ExtentX        =   18733
      _ExtentY        =   5953
      View            =   3
      LabelEdit       =   1
      MultiSelect     =   -1  'True
      LabelWrap       =   -1  'True
      HideSelection   =   0   'False
      AllowReorder    =   -1  'True
      Checkboxes      =   -1  'True
      FullRowSelect   =   -1  'True
      GridLines       =   -1  'True
      _Version        =   393217
      ForeColor       =   -2147483640
      BackColor       =   -2147483643
      BorderStyle     =   1
      Appearance      =   1
      NumItems        =   2
      BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Text            =   "Nombre del Art�culo"
         Object.Width           =   7761
      EndProperty
      BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   1
         Text            =   "C�digo"
         Object.Width           =   3881
      EndProperty
   End
   Begin VB.Label txtTIPO 
      Alignment       =   1  'Right Justify
      Caption         =   "Tipo de Art�culos:"
      Height          =   315
      Left            =   240
      TabIndex        =   16
      Top             =   120
      Width           =   2415
   End
End
Attribute VB_Name = "FrmExistxDisp"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'---------------------------------------------------------------------------------------
' Module    : FrmExistxDisp
' DateTime  : 25/02/2011 13:47
' Author    : gabriel_vallejo
' Purpose   : FORMULARIO PARA LA IMPRESION DE LOS TIPO DE EXISTENCIA FRETE ARTICULO
'---------------------------------------------------------------------------------------
' GAVL R3201 / T5101 25/02/2011

Option Explicit
Dim ArrUXB() As Variant
Dim Articulo As UnArticulo
Dim Dueno As New ElTercero
Dim LaAccion As String
'JAUM T29577 Inicio se deja linea en comentario
'Dim CnTdr As Integer, NumEnCombo As Integer
Dim CnTdr As Double 'Contador
Dim NumEnCombo As Integer
'JAUM T29577 Fin
Public OpcCod        As String   'Opci�n de seguridad


Private Sub cboTIPO_GotFocus()
    SendKeys "{F4}", True 'GAVL T5270
    NumEnCombo = Me.cboTIPO.ListIndex
End Sub

Private Sub chkfcRANGO_Click()
    Me.txtFCDESDE.Enabled = False
    Me.txtFCHASTA.Enabled = False
    If Not CBool(Me.chkFCRANGO.Value) = True Then Exit Sub
    Me.txtFCDESDE.Enabled = True
    Me.txtFCHASTA.Enabled = True
End Sub

Private Sub CmdImprimir_Click(Index As Integer)
    Dim RstSALD As ADODB.Recordset
    Set RstSALD = New ADODB.Recordset
    Dim Renglon As MSComctlLib.ListItem
    Dim NroRegs As Long
    Dim strARTICULOS As String, strBODEGAS As String
    Dim strTITRANGO As String, strTITFECHA As String, strFECORTE As String
    Dim stCmd As String
    Dim StSql As String
    Dim InDec As Integer
    Dim StCondic As String 'GAVL T5119
       
    If Me.cboTIPO.ListIndex < 0 Then Exit Sub
    
    Call Limpiar_CrysListar
    
   
    If Aplicacion.VerDecimales_Cantidades(InDec) Then
    Else
       InDec = 0
    End If

    
    If Me.chkARTICULOS.Value = 0 Then
        For Each Renglon In Me.lstARTICULOS.ListItems
            If Renglon.Selected Then strARTICULOS = strARTICULOS & IIf(Len(strARTICULOS) > 0, " OR ", "") & "NU_AUTO_ARTI_KARD=" & Renglon.Tag
        Next
    End If
    For Each Renglon In Me.lstBODEGAS.ListItems

        If Renglon.Selected Then strBODEGAS = strBODEGAS & IIf(Len(strBODEGAS) > 0, " OR ", "") & "NU_AUTO_BODE_KARD=" & Renglon.Tag
    Next
    If Len(strARTICULOS) > 0 Then strARTICULOS = "(" & strARTICULOS & ")"
    If Len(strBODEGAS) > 0 Then strBODEGAS = "(" & strBODEGAS & ")"
    If Len(strARTICULOS) > 0 And Len(strBODEGAS) > 0 Then
        strARTICULOS = strARTICULOS & " AND " & strBODEGAS
    Else
        strARTICULOS = strARTICULOS & strBODEGAS
    End If

    strTITRANGO = "TODOS LOS COMPROBANTES"
    strTITFECHA = "A LA FECHA"
    If CBool(Me.chkFCRANGO.Value) Then

        strFECORTE = "(FE_FECH_KARD <= " & FFechaCon(CDate(Me.txtFCHASTA.Text)) & ")"
        strTITFECHA = "Fecha de Corte: " & Me.txtFCHASTA.Text
    End If
    MDI_Inventarios.Crys_Listar.Formulas(8) = "RAGFecha='" & strTITFECHA & Comi

    strFECORTE = strFECORTE & IIf(Len(strFECORTE) = 0, "", " AND ") & "(NU_AUTO_KARD>=" & PriRegKARD & ")"

    If Len(strARTICULOS) > 0 Then
        strARTICULOS = strARTICULOS & " AND " & strFECORTE
    Else
        strARTICULOS = strARTICULOS & strFECORTE
    End If
    Debug.Print strARTICULOS
    
    If ExisteTABLA("IN_SALDOREPORT") Then Result = EliminarTabla("IN_SALDOREPORT")
    
    'INICIO GAVL T5119
      If Me.cboTIPO.ListIndex = 0 Then
        StCondic = " AND (ID_TIPO_ARTI =0 OR ID_TIPO_ARTI=1)  AND NU_DISP_ARTI = 0"
      Else
        StCondic = " AND NU_DISP_ARTI = 1"
      End If
    
    'FIN GAVL T5119
    
       
    StSql = "SELECT TX_NOMB_BODE, CD_CODI_ARTI, NO_NOMB_ARTI, TX_NOMB_UNVE,SUM(ENTRADAS) AS ENTRADAS, 0 AS SALIDAS, SUM(COSTO_PROMEDIO) AS COSTO_PROMEDIO"
    StSql = StSql & ", TX_CODCLAS_ARTI as CODCLAS, TX_CODPRI_ARTI AS CODPRINC, TX_PRCO_ARTI AS CODPRESCOR"
    StSql = StSql & "  INTO IN_SALDOREPORT FROM ("
    StSql = StSql & " SELECT TX_NOMB_BODE, CD_CODI_ARTI, NO_NOMB_ARTI, TX_NOMB_UNVE, 0 AS ENTRADAS, 0 AS SALIDAS,"
    StSql = StSql & "SUM(CAST((NU_ENTRAD_KARD+(NU_SALIDA_KARD*-1)) AS FLOAT)*CAST(NU_MULT_KARD AS FLOAT)"
    StSql = StSql & " /CAST(NU_DIVI_KARD AS FLOAT)*CAST(NU_COSTO_KARD AS FLOAT)) AS COSTO_PROMEDIO, TX_CODCLAS_ARTI, TX_CODPRI_ARTI, TX_PRCO_ARTI"
    StSql = StSql & " FROM IN_KARDEX, ARTICULO, IN_BODEGA, IN_UNDVENTA "
    StSql = StSql & " WHERE NU_AUTO_ARTI_KARD=NU_AUTO_ARTI"
    StSql = StSql & " AND NU_AUTO_BODE_KARD=NU_AUTO_BODE"
    StSql = StSql & " AND NU_AUTO_UNVE_ARTI=NU_AUTO_UNVE"
    StSql = StSql & StCondic 'GAVL T5119
    StSql = StSql & "  AND " & strARTICULOS
    StSql = StSql & " GROUP BY TX_NOMB_BODE, CD_CODI_ARTI, NO_NOMB_ARTI, TX_NOMB_UNVE, TX_CODCLAS_ARTI, TX_CODPRI_ARTI, TX_PRCO_ARTI"
    StSql = StSql & " UNION "
    
    
    StSql = StSql & " SELECT TX_NOMB_BODE, CD_CODI_ARTI, NO_NOMB_ARTI, TX_NOMB_UNVE, "
    StSql = StSql & " CASE NU_ACTUDNM_KARD WHEN 0 THEN 0 ELSE ROUND(CAST(NU_ACTUNMR_KARD AS FLOAT)/CAST(NU_ACTUDNM_KARD AS FLOAT)," & InDec & " ) END AS ENTRADAS,"
    StSql = StSql & " 0 AS SALIDAS, 0 AS COSTO_PROMEDIO, TX_CODCLAS_ARTI, TX_CODPRI_ARTI, TX_PRCO_ARTI"
    
    
    StSql = StSql & " FROM IN_KARDEX, ARTICULO, IN_BODEGA, IN_UNDVENTA"
    StSql = StSql & " WHERE NU_AUTO_KARD IN (SELECT MAX(NU_AUTO_KARD)"
    'StSql = StSql & " FROM IN_KARDEX " & IIf(Len(strARTICULOS) > 0, "WHERE ", "") & strARTICULOS 'EACT T20150
    StSql = StSql & " FROM IN_KARDEX, ARTICULO " & IIf(Len(strARTICULOS) > 0, "WHERE ", "") & strARTICULOS
    StSql = StSql & " AND (NU_AUTO_ARTI_KARD=NU_AUTO_ARTI)" 'EACT T20150
    StSql = StSql & " GROUP BY NU_AUTO_BODE_KARD, NU_AUTO_ARTI_KARD, TX_CODCLAS_ARTI, TX_CODPRI_ARTI, TX_PRCO_ARTI)"
    StSql = StSql & " AND NU_AUTO_ARTI_KARD=NU_AUTO_ARTI"
    StSql = StSql & " AND NU_AUTO_BODE_KARD=NU_AUTO_BODE"
    StSql = StSql & " AND NU_AUTO_UNVE_ARTI=NU_AUTO_UNVE"
    StSql = StSql & StCondic 'GAVL T5119
    If CBool(Me.ChkExistencia.Value) = False Then StSql = StSql & " AND NU_ACTUNMR_KARD<>0"
    StSql = StSql & " ) AS T1"
    StSql = StSql & " GROUP BY TX_NOMB_BODE, CD_CODI_ARTI, NO_NOMB_ARTI, TX_NOMB_UNVE,  TX_CODCLAS_ARTI, TX_CODPRI_ARTI,TX_PRCO_ARTI "
    If CBool(Me.ChkExistencia.Value) = False Then StSql = StSql & " HAVING SUM(ENTRADAS)<>0"
    strARTICULOS = StSql
    
    Debug.Print strARTICULOS
    BD(BDCurCon).Execute strARTICULOS, NroRegs

 
    MDI_Inventarios.Crys_Listar.Formulas(10) = "ENTIDAD=" & Comi & Trim(Dueno.Nombre) & ", NIT:" & Trim(Dueno.Nit) & Comi
    If Me.cboTIPO.ListIndex = 0 Then
        'Call ListarD("InfExistxPrin" & IIf(Me.opcCON, "CON", "SIN") & ".rpt", "", crptToWindow, "SALDOS DE INVENTARIO ", MDI_Inventarios, True)
        Call ListarD("InfExistxPrin" & IIf(Me.opcCON, "CON", "SIN") & ".rpt", "", crptToWindow, "SALDOS DE INVENTARIO POR TIPO " & IIf(Me.opcCON, "CON ", "SIN ") & "COSTO", MDI_Inventarios, True)  'GAVL T5119
     Else
        'Call ListarD("InfExistxCla" & IIf(Me.opcCON, "CON", "SIN") & ".rpt", "", crptToWindow, "SALDOS DE INVENTARIO ", MDI_Inventarios, True)
         Call ListarD("InfExistxCla" & IIf(Me.opcCON, "CON", "SIN") & ".rpt", "", crptToWindow, "SALDOS DE INVENTARIO POR TIPO " & IIf(Me.opcCON, "CON ", "SIN ") & "COSTO", MDI_Inventarios, True)  'GAVL T5119
    '-------------------------------------------------------
    End If
    
    MDI_Inventarios.Crys_Listar.Formulas(10) = ""
End Sub

Private Sub Form_Load()

    Call CenterForm(MDI_Inventarios, Me)
    Me.cboTIPO.Clear
    Dueno.IniXNit (Dueno.Nit)
    Me.cboTIPO.AddItem "MEDICAMENTOS"
    Me.cboTIPO.AddItem "DISPOSITIVOS"
    NumEnCombo = Me.cboTIPO.ListIndex
    If Len(FormatDateTime(Date, vbShortDate)) = 8 Then
        Me.txtFCDESDE.Mask = "##/##/##"
        Me.txtFCHASTA.Mask = "##/##/##"
    Else
        Me.txtFCDESDE.Mask = "##/##/####"
        Me.txtFCHASTA.Mask = "##/##/####"
    End If
    Me.txtFCDESDE.Text = FormatDateTime(DateAdd("d", -(Day(Date) - 1), Date), vbShortDate)
    Me.txtFCHASTA.Text = FormatDateTime(DateAdd("d", -1, DateAdd("M", 1, DateAdd("d", -(Day(Date) - 1), Date))), vbShortDate)
    Me.chkFCRANGO.Value = False
    Me.txtFCDESDE.Enabled = False
    Me.txtFCHASTA.Enabled = False
    Me.lstBODEGAS.ListItems.Clear
    Me.lstBODEGAS.Checkboxes = False
    Me.lstBODEGAS.MultiSelect = True
    Me.lstBODEGAS.HideSelection = False
    Me.lstBODEGAS.ColumnHeaders.Clear
    Me.lstBODEGAS.ColumnHeaders.Add , "NOM", "Nombre de la Bodega", 0.66 * Me.lstBODEGAS.Width
    Me.lstBODEGAS.ColumnHeaders.Add , "COD", "C�digo", 0.34 * Me.lstBODEGAS.Width
    
    Me.lstARTICULOS.ListItems.Clear
    Me.lstARTICULOS.Checkboxes = False
    Me.lstARTICULOS.MultiSelect = True
    Me.lstARTICULOS.HideSelection = False
    Me.lstARTICULOS.Width = 10500
    Me.lstARTICULOS.ColumnHeaders.Clear
    Me.lstARTICULOS.ColumnHeaders.Add , "NOAR", "Nombre del art�culo", 0.4 * Me.lstARTICULOS.Width
    Me.lstARTICULOS.ColumnHeaders.Add , "COAR", "C�digo", 0.15 * Me.lstARTICULOS.Width
    Me.lstARTICULOS.ColumnHeaders.Add , "NOCO", "Unidad de conteo", 0.15 * Me.lstARTICULOS.Width
    Me.lstARTICULOS.ColumnHeaders.Add , "NOUS", "Uso", 0.15 * Me.lstARTICULOS.Width
    Me.lstARTICULOS.ColumnHeaders.Add , "NOGR", "Grupo", 0.15 * Me.lstARTICULOS.Width
    Me.lstARTICULOS.ColumnHeaders.Add , "AUCO", "AutoUNC", 0
    Me.lstARTICULOS.ColumnHeaders.Add , "COGR", "CodGRU", 0
    Me.lstARTICULOS.ColumnHeaders.Add , "COUS", "CodUSO", 0
    
FALLO:
End Sub

Private Sub cboTIPO_LostFocus()
    If NumEnCombo = Me.cboTIPO.ListIndex Then Exit Sub
    Dim tipo As String * 1
    Me.lstBODEGAS.ListItems.Clear
    Me.lstARTICULOS.ListItems.Clear
    Me.pgbLLEVO.Value = 0
    Me.pgbLLEVO.Visible = True
    
    Campos = "NU_AUTO_BODE, NU_AUTO_BODE_BODE, TX_CODI_BODE, TX_NOMB_BODE, " & _
             "CD_CODI_CECO_BODE, TX_VENTA_BODE"
    Desde = "IN_BODEGA"
  
    ReDim ArrUXB(5, 0)
    Result = LoadMulData(Desde, Campos, NUL$, ArrUXB())
    If Result = FAIL Then GoTo FALLO
    On Error GoTo SIGUI
    For CnTdr = 0 To UBound(ArrUXB, 2)
        Me.lstBODEGAS.ListItems.Add , "B" & ArrUXB(2, CnTdr), ArrUXB(3, CnTdr)
        Me.lstBODEGAS.ListItems("B" & ArrUXB(2, CnTdr)).ListSubItems.Add , "COD", ArrUXB(2, CnTdr)
        Me.lstBODEGAS.ListItems("B" & ArrUXB(2, CnTdr)).Tag = ArrUXB(0, CnTdr)
SIGUI:
    Next
    On Error GoTo 0
'---------------------------------------------------
    If Me.cboTIPO.ListIndex = 0 Then
       Campos = "DISTINCT NU_AUTO_ARTI, CD_CODI_ARTI, NO_NOMB_ARTI, NU_AUTO_UNVE_ARTI, TX_PARA_ARTI,"
       Campos = Campos & "DE_DESC_GRUP, DE_DESC_USOS, TX_NOMB_UNVE, CD_GRUP_ARTI, CD_USOS_ARTI, NU_MULT_UNVE, NU_DIVI_UNVE"
       'Desde = "ARTICULO,PRINCIPIO_ACTIVO, GRUP_ARTICULO, USOS"
       Desde = "ARTICULO, USOS, GRUP_ARTICULO, IN_UNDVENTA, IN_DETALLE" 'GAVL T5117
       'Condicion = "TX_CODIGO_PRAC = TX_CODPRI_ARTI  AND CD_CODI_GRUP = CD_GRUP_ARTI"
       Condicion = "  CD_CODI_GRUP = CD_GRUP_ARTI AND CD_CODI_USOS = CD_USOS_ARTI"
       Condicion = Condicion & " AND NU_AUTO_UNVE = NU_AUTO_UNVE_ARTI"
       Condicion = Condicion & " AND NU_AUTO_ARTI = NU_AUTO_ARTI_DETA"
       Condicion = Condicion & " AND (ID_TIPO_ARTI =0 or ID_TIPO_ARTI=1)  AND NU_DISP_ARTI = 0"
       Condicion = Condicion & " ORDER BY NO_NOMB_ARTI"
     Else
'        Campos = "DISTINCT NU_AUTO_ARTI, CD_CODI_ARTI, NO_NOMB_ARTI, NU_AUTO_UNVE_ARTI, TX_PARA_ARTI,"
'       Campos = Campos & "DE_DESC_GRUP, DE_DESC_USOS, CD_GRUP_ARTI , CD_USOS_ARTI"
'       Desde = "ARTICULO,CLASIFICACION_RIESGO , GRUP_ARTICULO, USOS"
'       Condicion = "TX_CODIGO_CLRI  = TX_CODCLAS_ARTI    AND CD_CODI_GRUP = CD_GRUP_ARTI And CD_CODI_USOS = CD_USOS_ARTI"
'       Condicion = Condicion & " ORDER BY NO_NOMB_ARTI"
        'INICIO GAVL T5117
       Campos = "DISTINCT NU_AUTO_ARTI, CD_CODI_ARTI, NO_NOMB_ARTI, NU_AUTO_UNVE_ARTI, TX_PARA_ARTI,"
       Campos = Campos & "DE_DESC_GRUP, DE_DESC_USOS, TX_NOMB_UNVE, CD_GRUP_ARTI, CD_USOS_ARTI, NU_MULT_UNVE, NU_DIVI_UNVE"
       Desde = "ARTICULO, USOS, GRUP_ARTICULO, IN_UNDVENTA, IN_DETALLE"
       Condicion = "CD_CODI_GRUP = CD_GRUP_ARTI AND CD_CODI_USOS = CD_USOS_ARTI"
       Condicion = Condicion & " AND NU_AUTO_UNVE = NU_AUTO_UNVE_ARTI"
       Condicion = Condicion & " AND NU_AUTO_ARTI = NU_AUTO_ARTI_DETA"
       Condicion = Condicion & " AND NU_DISP_ARTI = 1"
       Condicion = Condicion & " ORDER BY NO_NOMB_ARTI"
        'FIN GAVL T5117
    '-------------------------------------------------------
    End If
    ReDim ArrUXB(9, 0)
    Result = LoadMulData(Desde, Campos, Condicion, ArrUXB())
    If Result = FAIL Then GoTo FALLO
    If Not Encontro Then GoTo NOENC
    Me.pgbLLEVO.Min = 0
    Me.pgbLLEVO.Value = 0
    Me.pgbLLEVO.Max = UBound(ArrUXB, 2) + 1
    Dim Articulo As New UnArticulo
    For CnTdr = 0 To UBound(ArrUXB, 2)
        Me.pgbLLEVO.Value = CnTdr
        Articulo.AutoNumero = ArrUXB(0, CnTdr)
        Articulo.Codigo = ArrUXB(1, CnTdr)
        Articulo.Nombre = ArrUXB(2, CnTdr)
        Articulo.AutoUDConteo = ArrUXB(3, CnTdr)
        Articulo.NombreGrupo = ArrUXB(5, CnTdr)
        Articulo.NombreUso = ArrUXB(6, CnTdr)
        Articulo.NombreUndConteo = ArrUXB(7, CnTdr)
        Articulo.ParaQueEs = InStr(1, "VC", ArrUXB(4, CnTdr)) - 1
        Articulo.CodigoGrupo = ArrUXB(8, CnTdr)
        Articulo.CodigoUso = ArrUXB(9, CnTdr)
        Me.lstARTICULOS.ListItems.Add , "A" & Articulo.AutoNumero, Trim(Articulo.Nombre)
        Me.lstARTICULOS.ListItems("A" & Articulo.AutoNumero).Tag = CStr(Articulo.AutoNumero)

        Me.lstARTICULOS.ListItems("A" & Articulo.AutoNumero).ListSubItems.Add , "COAR", Trim(Articulo.Codigo)
        Me.lstARTICULOS.ListItems("A" & Articulo.AutoNumero).ListSubItems.Add , "NOCO", Trim(Articulo.NombreUndConteo)
        Me.lstARTICULOS.ListItems("A" & Articulo.AutoNumero).ListSubItems.Add , "NOUS", Trim(Articulo.NombreUso)
        Me.lstARTICULOS.ListItems("A" & Articulo.AutoNumero).ListSubItems.Add , "NOGR", Trim(Articulo.NombreGrupo)
        Me.lstARTICULOS.ListItems("A" & Articulo.AutoNumero).ListSubItems.Add , "AUCO", Trim(Articulo.AutoUDConteo)
        Me.lstARTICULOS.ListItems("A" & Articulo.AutoNumero).ListSubItems.Add , "COGR", Trim(Articulo.CodigoGrupo)
        Me.lstARTICULOS.ListItems("A" & Articulo.AutoNumero).ListSubItems.Add , "COUS", Trim(Articulo.CodigoUso)
    Next
    Set Articulo = Nothing
NOENC:
FALLO:
    Me.pgbLLEVO.Visible = False
End Sub

Private Sub lstARTICULOS_ColumnClick(ByVal ColumnHeader As MSComctlLib.ColumnHeader)
    Me.lstARTICULOS.SortKey = ColumnHeader.Index - 1
    Me.lstARTICULOS.Sorted = True
End Sub



Private Sub txtFCDESDE_LostFocus()
    If IsDate(Me.txtFCDESDE.Text) Then Exit Sub
    Me.txtFCDESDE.Text = FormatDateTime(DateAdd("d", -(Day(Date) - 1), Date), vbShortDate)
    Me.txtFCDESDE.SetFocus
End Sub

Private Sub txtFCHASTA_LostFocus()
    If IsDate(Me.txtFCHASTA.Text) Then Exit Sub
    Me.txtFCHASTA.Text = FormatDateTime(DateAdd("d", -1, DateAdd("M", 1, DateAdd("d", -(Day(Date) - 1), Date))), vbShortDate)
    Me.txtFCHASTA.SetFocus
End Sub

Private Sub Form_KeyPress(KeyAscii As Integer)
    If KeyAscii = vbKeyReturn Then KeyAscii = vbKeyTab
End Sub




