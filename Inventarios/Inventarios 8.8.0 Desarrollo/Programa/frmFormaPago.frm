VERSION 5.00
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "MSFLXGRD.OCX"
Begin VB.Form frmFormaPago 
   Caption         =   "Forma de Pago"
   ClientHeight    =   4515
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   8550
   LinkTopic       =   "Form1"
   ScaleHeight     =   4515
   ScaleWidth      =   8550
   Begin VB.Frame frmFORMAPAGO 
      Height          =   4335
      Left            =   120
      TabIndex        =   0
      Top             =   0
      Width           =   8295
      Begin VB.Frame Frame1 
         Height          =   2055
         Left            =   4560
         TabIndex        =   6
         Top             =   1320
         Width           =   3615
         Begin VB.TextBox txtValorRenglon 
            Alignment       =   1  'Right Justify
            Height          =   375
            Left            =   1680
            TabIndex        =   8
            Text            =   "$ 0"
            Top             =   1200
            Width           =   1815
         End
         Begin VB.ComboBox cboFormas 
            Height          =   315
            Left            =   1200
            TabIndex        =   7
            Top             =   360
            Width           =   2295
         End
         Begin VB.Label Label2 
            Alignment       =   1  'Right Justify
            Caption         =   "Valor Parcial:"
            Height          =   375
            Left            =   240
            TabIndex        =   10
            Top             =   1200
            Width           =   1095
         End
         Begin VB.Label Label3 
            Alignment       =   1  'Right Justify
            Caption         =   "Forma:"
            Height          =   375
            Left            =   120
            TabIndex        =   9
            Top             =   360
            Width           =   855
         End
      End
      Begin VB.Frame Frame2 
         Height          =   855
         Left            =   1680
         TabIndex        =   3
         Top             =   240
         Width           =   4935
         Begin VB.TextBox txtValorFactura 
            Alignment       =   1  'Right Justify
            Enabled         =   0   'False
            Height          =   375
            Left            =   2640
            TabIndex        =   4
            Text            =   "$ 0"
            Top             =   360
            Width           =   1935
         End
         Begin VB.Label Label1 
            Alignment       =   1  'Right Justify
            Caption         =   "Valor a Pagar:"
            Height          =   375
            Left            =   960
            TabIndex        =   5
            Top             =   360
            Width           =   1455
         End
      End
      Begin VB.CommandButton cmdCancelar 
         Caption         =   "&Cancelar"
         Height          =   495
         Left            =   2400
         TabIndex        =   2
         Top             =   3600
         Width           =   1695
      End
      Begin VB.CommandButton cmdFacturar 
         Caption         =   "&Facturar"
         Default         =   -1  'True
         Enabled         =   0   'False
         Height          =   495
         Left            =   4680
         TabIndex        =   1
         Top             =   3600
         Width           =   1695
      End
      Begin MSFlexGridLib.MSFlexGrid grdFormaPago 
         Height          =   1935
         Left            =   120
         TabIndex        =   11
         Top             =   1440
         Width           =   4095
         _ExtentX        =   7223
         _ExtentY        =   3413
         _Version        =   393216
         Cols            =   3
         FixedCols       =   0
         ForeColorSel    =   -2147483635
         FocusRect       =   2
         MergeCells      =   1
      End
   End
End
Attribute VB_Name = "frmFormaPago"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private vTotalFactura As Single
Private vSeFactura As Boolean
Dim itmFormas() As Variant

Private Sub cmdCancelar_Click()
    Me.Hide
End Sub

Private Sub cmdFacturar_Click()
    If Not CSng(Me.txtValorFactura.Text) = SumaFormas() Then
        Me.cmdFacturar.Enabled = False
        Exit Sub
    End If
    vSeFactura = True
    Me.Hide
End Sub

Private Sub cmdFacturar_LostFocus()
    Me.cmdFacturar.Enabled = False
End Sub

Private Sub Form_Load()
''SELECT NU_NUME_FOPA, DE_DESC_FOPA FROM FORMA_PAGO
    vSeFactura = False
    Me.txtValorFactura = FormatCurrency(vTotalFactura, 2)
    Me.txtValorRenglon = FormatCurrency(vTotalFactura, 2)
    Me.grdFormaPago.Rows = 1
    Result = loadctrl("FORMA_PAGO", "DE_DESC_FOPA", "NU_NUME_FOPA", Me.cboFormas, itmFormas, "")
    Call GrdFDef(Me.grdFormaPago, 0, 0, "C�digo,0,Nombre,2500,Valor,+1200")
    Call InsertaForma("0", vTotalFactura)
'                "C�digo,0,Nombre,2500,Valor,+1200" & _
'                "Nombre,2500," & _
'                "Valor,+1200"
End Sub

Public Property Get SeFactura() As Boolean
    SeFactura = vSeFactura
End Property

Public Property Let TotalFactura(Num As Single)
    vTotalFactura = Num
End Property

Private Sub InsertaForma(Optional vTipo As String = "", Optional vValor As Single = 0)
    If Len(vTipo) > 0 Then Me.cboFormas.ListIndex = FindInArr(itmFormas, vTipo)
    If Not Me.cboFormas.ListIndex > -1 Then Exit Sub
    If Not CSng(Me.txtValorRenglon.Text) > 0 Then Exit Sub
    Dim vFil As Integer
    For vFil = 1 To Me.grdFormaPago.Rows - 1
        If Me.grdFormaPago.TextMatrix(vFil, 0) = itmFormas(Me.cboFormas.ListIndex) Then
            Me.grdFormaPago.TextMatrix(vFil, 1) = Me.cboFormas.List(Me.cboFormas.ListIndex)
            Me.grdFormaPago.TextMatrix(vFil, 2) = FormatCurrency(CSng(Me.txtValorRenglon.Text), 2)
            Exit Sub
        End If
    Next
    Me.grdFormaPago.Rows = Me.grdFormaPago.Rows + 1
    Me.grdFormaPago.Row = Me.grdFormaPago.Rows - 1
    Me.grdFormaPago.TextMatrix(Me.grdFormaPago.Row, 0) = itmFormas(Me.cboFormas.ListIndex)
    Me.grdFormaPago.TextMatrix(Me.grdFormaPago.Row, 1) = Me.cboFormas.List(Me.cboFormas.ListIndex)
    Me.grdFormaPago.TextMatrix(Me.grdFormaPago.Row, 2) = FormatCurrency(CSng(Me.txtValorRenglon.Text), 2)
End Sub

Private Function SumaFormas() As Single
    For vFil = 1 To Me.grdFormaPago.Rows - 1
        SumaFormas = SumaFormas + CSng(Me.grdFormaPago.TextMatrix(vFil, 2))
    Next
End Function

Private Sub txtValorRenglon_GotFocus()
    Me.txtValorRenglon.Text = FormatCurrency(Me.txtValorFactura - SumaFormas(), 2)
End Sub

Private Sub txtValorRenglon_KeyPress(KeyAscii As Integer)
    Call Cambiar_Enter(KeyAscii)
End Sub

Private Sub txtValorRenglon_LostFocus()
    Me.txtValorRenglon.Text = FormatCurrency(IIf(Len(Me.txtValorRenglon.Text) > 0, CSng(Me.txtValorRenglon.Text), 0), 2)
    Call InsertaForma
End Sub


