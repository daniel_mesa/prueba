VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Object = "{C932BA88-4374-101B-A56C-00AA003668DC}#1.1#0"; "MSMASK32.OCX"
Begin VB.Form FrmCriteXProve 
   Caption         =   "Criterios X Proveedor"
   ClientHeight    =   7065
   ClientLeft      =   60
   ClientTop       =   450
   ClientWidth     =   11445
   Icon            =   "FrmCriteXProve.frx":0000
   LinkTopic       =   "Form1"
   ScaleHeight     =   7065
   ScaleWidth      =   11445
   StartUpPosition =   3  'Windows Default
   Begin VB.Frame Frame1 
      Height          =   2055
      Left            =   7320
      TabIndex        =   1
      Top             =   4680
      Width           =   3615
      Begin MSMask.MaskEdBox txtCALIFICA 
         Height          =   375
         Left            =   2160
         TabIndex        =   3
         ToolTipText     =   "Rango (0, 10)"
         Top             =   480
         Width           =   1215
         _ExtentX        =   2143
         _ExtentY        =   661
         _Version        =   393216
         ClipMode        =   1
         PromptInclude   =   0   'False
         PromptChar      =   "_"
      End
      Begin MSMask.MaskEdBox txtPORCENTA 
         Height          =   375
         Left            =   2160
         TabIndex        =   4
         ToolTipText     =   "Rango (0, 100)"
         Top             =   1200
         Width           =   1215
         _ExtentX        =   2143
         _ExtentY        =   661
         _Version        =   393216
         PromptInclude   =   0   'False
         PromptChar      =   "_"
      End
      Begin VB.Label Label2 
         Alignment       =   1  'Right Justify
         Caption         =   "Peso porcentual:"
         Height          =   375
         Left            =   600
         TabIndex        =   6
         Top             =   1320
         Width           =   1500
      End
      Begin VB.Label Label1 
         Alignment       =   1  'Right Justify
         Caption         =   "Calificaci�n (1-10):"
         Height          =   375
         Left            =   600
         TabIndex        =   5
         Top             =   600
         Width           =   1500
      End
   End
   Begin MSComctlLib.ListView lstTERCEROS 
      Height          =   3375
      Left            =   480
      TabIndex        =   0
      Top             =   1080
      Width           =   10455
      _ExtentX        =   18441
      _ExtentY        =   5953
      View            =   3
      LabelEdit       =   1
      MultiSelect     =   -1  'True
      LabelWrap       =   -1  'True
      HideSelection   =   0   'False
      AllowReorder    =   -1  'True
      FullRowSelect   =   -1  'True
      GridLines       =   -1  'True
      _Version        =   393217
      ForeColor       =   -2147483640
      BackColor       =   -2147483643
      BorderStyle     =   1
      Appearance      =   1
      NumItems        =   2
      BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Text            =   "Nombre del Art�culo"
         Object.Width           =   7761
      EndProperty
      BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   1
         Text            =   "C�digo"
         Object.Width           =   3881
      EndProperty
   End
   Begin MSComctlLib.ListView lstCRITERIO 
      Height          =   2055
      Left            =   480
      TabIndex        =   2
      Top             =   4680
      Width           =   6615
      _ExtentX        =   11668
      _ExtentY        =   3625
      View            =   3
      LabelEdit       =   1
      MultiSelect     =   -1  'True
      LabelWrap       =   -1  'True
      HideSelection   =   0   'False
      AllowReorder    =   -1  'True
      Checkboxes      =   -1  'True
      FullRowSelect   =   -1  'True
      GridLines       =   -1  'True
      _Version        =   393217
      ForeColor       =   -2147483640
      BackColor       =   -2147483643
      BorderStyle     =   1
      Appearance      =   1
      NumItems        =   2
      BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Text            =   "Nombre del Art�culo"
         Object.Width           =   7761
      EndProperty
      BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   1
         Text            =   "C�digo"
         Object.Width           =   3881
      EndProperty
   End
End
Attribute VB_Name = "FrmCriteXProve"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Public OpcCod        As String   'Opci�n de seguridad
Private vKeyTercero  As String

Private Sub Form_Load()
    Dim vClave As String, vContar As Long
    Dim ArrPRV() As Variant, arrCRT() As Variant
    Call CenterForm(MDI_Inventarios, Me)
    Me.txtCALIFICA.Mask = "99"
    Me.txtPORCENTA.Mask = "999"
    Me.lstCRITERIO.ListItems.Clear
    Me.lstCRITERIO.Checkboxes = True
    Me.lstCRITERIO.ColumnHeaders.Clear
    Me.lstCRITERIO.ColumnHeaders.Add , "COD", "C�digo", 0.25 * Me.lstCRITERIO.Width
    Me.lstCRITERIO.ColumnHeaders.Add , "NOM", "Nombre", 0.5 * Me.lstCRITERIO.Width
    Me.lstCRITERIO.ColumnHeaders.Add , "CLF", "Calificaci�n", 0.1 * Me.lstCRITERIO.Width
    Me.lstCRITERIO.ColumnHeaders.Item("CLF").Alignment = lvwColumnRight
    Me.lstCRITERIO.ColumnHeaders.Add , "PRC", "Peso %", 0.1 * Me.lstCRITERIO.Width
    Me.lstCRITERIO.ColumnHeaders.Item("PRC").Alignment = lvwColumnRight
    Campos = "NU_AUTO_CRIT, TX_CODI_CRIT, TX_DESC_CRIT"
    Desde = "IN_CRITERIO"
    Condi = ""
    ReDim arrCRT(2, 0)
    Result = LoadMulData(Desde, Campos, Condi, arrCRT())
    If Result = FAIL Then Exit Sub
    If Encontro Then
        For vContar = 0 To UBound(arrCRT, 2)
            vClave = "C" & arrCRT(0, vContar)
            Me.lstCRITERIO.ListItems.Add , vClave, arrCRT(1, vContar)
            Me.lstCRITERIO.ListItems.Item(vClave).Tag = arrCRT(0, vContar)
            Me.lstCRITERIO.ListItems.Item(vClave).ListSubItems.Add , "NON", arrCRT(2, vContar)
            Me.lstCRITERIO.ListItems.Item(vClave).ListSubItems.Add , "CLF", "0"
            Me.lstCRITERIO.ListItems.Item(vClave).ListSubItems.Add , "PRC", "0"
        Next
    End If
    Me.lstTERCEROS.ListItems.Clear
    Me.lstTERCEROS.ColumnHeaders.Clear
    Me.lstTERCEROS.ColumnHeaders.Add , "NIT", "Nit", 0.2 * Me.lstTERCEROS.Width
    Me.lstTERCEROS.ColumnHeaders.Add , "NOM", "Nombre", 0.6 * Me.lstTERCEROS.Width
    Campos = "DISTINCT CD_CODI_TERC, NO_NOMB_TERC"
    Desde = "IN_ENCABEZADO, TERCERO"
    Condi = "CD_CODI_TERC_ENCA=CD_CODI_TERC ORDER BY NO_NOMB_TERC"
'    Condi = Condi & " AND NU_AUTO_DOCU_ENCA=" & ListaPreciosCotiza
    ReDim ArrPRV(1, 0)
    Result = LoadMulData(Desde, Campos, Condi, ArrPRV())
    If Result = FAIL Then Exit Sub
    If Encontro Then
        For vContar = 0 To UBound(ArrPRV, 2)
            vClave = "T" & ArrPRV(0, vContar)
            Me.lstTERCEROS.ListItems.Add , vClave, ArrPRV(0, vContar)
            Me.lstTERCEROS.ListItems.Item(vClave).Tag = ArrPRV(0, vContar)
            Me.lstTERCEROS.ListItems.Item(vClave).ListSubItems.Add , "NOM", ArrPRV(1, vContar)
            Me.lstTERCEROS.SelectedItem.Selected = False
        Next
        Call lstTERCEROS_KeyUp(0, 0)
    End If
End Sub

Private Sub lstTERCEROS_SeDesplazo(ByVal Item As MSComctlLib.ListItem)
    Dim vClave As String, vContar As Long
    Dim vUno As MSComctlLib.ListItem
    Dim arrCRT() As Variant
    Me.txtCALIFICA.Text = Format(0, "#0")
    Me.txtPORCENTA.Text = Format(0, "##0")
    For Each vUno In Me.lstCRITERIO.ListItems
        vUno.ListSubItems.Item("CLF").Text = "0"
        vUno.ListSubItems.Item("PRC").Text = "0"
        vUno.Checked = False
    Next
    Campos = "NU_AUTO_CRIT_RCRTE, CD_CODI_TERC_RCRTE, NU_CALI_RCRTE, NU_PESO_RCRTE"
    Desde = "IN_R_CRITERIO_TERCERO, IN_CRITERIO"
    Condi = "NU_AUTO_CRIT=NU_AUTO_CRIT_RCRTE"
    Condi = "CD_CODI_TERC_RCRTE=" & Item.Tag
    ReDim arrCRT(3, 0)
'NU_AUTO_CRIT_RCRTE, CD_CODI_TERC_RCRTE, NU_CALI_RCRTE, NU_PESO_RCRTE
'NU_AUTO_CRIT, TX_CODI_CRIT, TX_DESC_CRIT
    Result = LoadMulData(Desde, Campos, Condi, arrCRT())
    If Encontro Then
        For vContar = 0 To UBound(arrCRT, 2)
            vClave = "C" & arrCRT(0, vContar)
            Me.lstCRITERIO.ListItems.Item(vClave).ListSubItems("CLF").Text = Format(CInt(arrCRT(2, vContar)), "#0")
            Me.lstCRITERIO.ListItems.Item(vClave).ListSubItems("PRC").Text = Format(CInt(arrCRT(3, vContar)), "##0")
            Me.txtCALIFICA.Text = Format(CInt(arrCRT(2, vContar)), "#0")
            Me.txtPORCENTA.Text = Format(CInt(arrCRT(3, vContar)), "##0")
            Me.lstCRITERIO.ListItems.Item(vClave).Checked = True
        Next
    End If
End Sub

Private Sub lstCRITERIO_ItemCheck(ByVal Item As MSComctlLib.ListItem)
    Dim vProvedores As String
    Dim vUno As MSComctlLib.ListItem
    If Len(Me.txtCALIFICA.Text) = 0 Then Me.txtCALIFICA.Text = "0"
    If Len(Me.txtPORCENTA.Text) = 0 Then Me.txtPORCENTA.Text = "0"
    If Item.Checked Then
        If (CInt(Me.txtCALIFICA.Text) > 0 And CInt(Me.txtCALIFICA.Text) <= 10) And (CInt(Me.txtPORCENTA.Text) > 0 And CInt(Me.txtPORCENTA.Text) <= 100) Then
            For Each vUno In Me.lstTERCEROS.ListItems
                If vUno.Selected Then
                    Desde = "IN_R_CRITERIO_TERCERO"
                    Condi = "NU_AUTO_CRIT_RCRTE=" & Item.Tag
                    Condi = Condi & ", CD_CODI_TERC_RCRTE=" & vUno.Tag
                    Condi = Condi & ", NU_CALI_RCRTE=" & CInt(Me.txtCALIFICA.Text)
                    Condi = Condi & ", NU_PESO_RCRTE=" & CInt(Me.txtPORCENTA.Text)
                    Result = DoInsertSQL(Desde, Condi)
                End If
            Next
            vKeyTercero = ""
            Call lstTERCEROS_KeyUp(0, 0)
        Else
            Item.Checked = False
        End If
    Else
        For Each vUno In Me.lstTERCEROS.ListItems
            If vUno.Selected Then vProvedores = vProvedores & IIf(Len(vProvedores) = 0, "", Coma) & "'" & vUno.Tag & "'"
        Next
        If Len(vProvedores) > 0 Then
            vProvedores = "(" & vProvedores & ")"
            Desde = "IN_R_CRITERIO_TERCERO"
            Condi = "NU_AUTO_CRIT_RCRTE=" & Item.Tag
            Condi = Condi & " AND CD_CODI_TERC_RCRTE IN " & vProvedores
            Result = DoDelete(Desde, Condi)
        End If
        vKeyTercero = ""
        Call lstTERCEROS_KeyUp(0, 0)
    End If
End Sub

Private Sub lstTERCEROS_ItemClick(ByVal Item As MSComctlLib.ListItem)
    Call lstTERCEROS_SeDesplazo(Item)
End Sub

Private Sub lstTERCEROS_KeyUp(KeyCode As Integer, Shift As Integer)
    If Trim(Me.lstTERCEROS.SelectedItem.Key) = vKeyTercero Then Exit Sub
    vKeyTercero = Trim(Me.lstTERCEROS.SelectedItem.Key)
    Call lstTERCEROS_SeDesplazo(Me.lstTERCEROS.SelectedItem)
End Sub

Private Sub txtCALIFICA_LostFocus()
    If Len(Me.txtCALIFICA.Text) = 0 Then Me.txtCALIFICA.Text = "0"
    If CInt(Me.txtCALIFICA.Text) < 0 Or CInt(Me.txtCALIFICA.Text) > 10 Then Me.txtCALIFICA.SetFocus
End Sub

Private Sub txtPORCENTA_LostFocus()
    If Len(Me.txtPORCENTA.Text) = 0 Then Me.txtPORCENTA.Text = "0"
    If CInt(Me.txtPORCENTA.Text) < 0 Or CInt(Me.txtPORCENTA.Text) > 100 Then Me.txtPORCENTA.SetFocus
End Sub
