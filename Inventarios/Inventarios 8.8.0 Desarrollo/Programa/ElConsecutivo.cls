VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "ElConsecutivo"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit

Private AuCn As Long 'Autonum�rico
Private Codi As String * 10 'C�digo
Private Nomb As String * 50 'Nombre
Private Pref As String * 2 'Prefijo
Private AuDo As Long 'Autonum�rico en IN_DOCUMENTO
Private NAct As Long 'N�mero actual
Private aNla As Long 'Autonum�rico que lo anula en IN_COMPROBANTE
Private Requ As Boolean 'Requiere autorizacion
Private Romp As Boolean 'Permite romper dependecia
Private VarAutoConsecutivo As Long

Public Property Let AutoNumero(num As Long)
    AuCn = num
End Property

Public Property Get AutoNumero() As Long
    AutoNumero = AuCn
End Property

Public Property Let RequereAutorizacion(lgc As Boolean)
    Requ = lgc
End Property

Public Property Get RequereAutorizacion() As Boolean
    RequereAutorizacion = Requ
End Property

Public Property Let RompeDependencia(lgc As Boolean)
    Romp = lgc
End Property

Public Property Get RompeDependencia() As Boolean
    RompeDependencia = Romp
End Property

Public Property Let Codigo(Cue As String)
    Codi = Cue
End Property

Public Property Get Codigo() As String
    Codigo = Codi
End Property

Public Property Let Nombre(Cue As String)
    Nomb = Cue
End Property

Public Property Get Nombre() As String
    Nombre = Nomb
End Property

Public Property Let Prefijo(Cue As String)
    Pref = Cue
End Property

Public Property Get Prefijo() As String
    Prefijo = Pref
End Property

Public Property Let AutoDocu(num As Long)
    AuDo = num
End Property

Public Property Get AutoDocu() As Long
    AutoDocu = AuDo
End Property

Public Property Let NumActual(num As Long)
    NAct = num
End Property

Public Property Get NumActual() As Long
    NumActual = NAct
End Property

Public Property Let AutoAnula(num As Long)
    aNla = num
End Property

Public Property Get AutoAnula() As Long
    AutoAnula = aNla
End Property

Private Sub Class_Initialize()
    Requ = False
End Sub

Public Property Let AutoConsecutivo(auto As Long)
VarAutoConsecutivo = auto
End Property

Public Property Get AutoConsecutivo() As Long
AutoConsecutivo = VarAutoConsecutivo
End Property

'Public Property Get NumeroActual() As Long
'    Dim ArrCMP() As Variant
'    Dim Mpos As String, Desd As String, Dici As String
'    Mpos = "NU_COMP_COMP"
'    Desd = "IN_COMPROBANTE"
'    Dici = "NU_AUTO_COMP=" & VarAutoConsecutivo
'    ReDim ArrCMP(0)
'    Result = LoadData(Desd, Mpos, Dici, ArrCMP())
'    If Result <> FAIL And Encontro Then
'       VarNumeroActual = ArrCMP(0)
'    Else
'       Call Mensaje1("No se pudo obtener el n�mero actual del consecutivo", 3)
'    End If
'    NumeroActual = VarNumeroActual
'End Property

Public Function Actualiza(ByVal num As Long) As ResultConsulta
    Dim ArrCMP() As Variant
    Dim Mpos As String, Desd As String, Dici As String
    Mpos = "NU_AUTO_COMP, TX_CODI_COMP, TX_NOMB_COMP, TX_PRFJ_COMP, NU_AUTO_DOCU_COMP, " & _
        "NU_COMP_COMP, NU_AUTO_ANUL_COMP, TX_REQU_COMP, TX_ROMP_COMP"
    Desd = "IN_COMPROBANTE"
    Dici = "NU_AUTO_COMP=" & num
    ReDim ArrCMP(8)
    Result = LoadData(Desd, Mpos, Dici, ArrCMP())
    If Result = FAIL Then Actualiza = Errorinfo: Exit Function
    If Not encontro Then Actualiza = NoEncontroinfo: Exit Function
    AuCn = ArrCMP(0)
    Codi = ArrCMP(1)
    Nomb = ArrCMP(2)
    Pref = ArrCMP(3)
    AuDo = ArrCMP(4)
    NAct = ArrCMP(5)
    If ArrCMP(6) = NUL$ Then
        ArrCMP(6) = 0
    Else
        ArrCMP(6) = CLng(ArrCMP(6))
    End If
    Requ = IIf(ArrCMP(7) = "S", True, False)
    Romp = IIf(ArrCMP(8) = "S", True, False)
End Function

