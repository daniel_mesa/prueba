VERSION 5.00
Object = "{00025600-0000-0000-C000-000000000046}#5.2#0"; "Crystl32.OCX"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.1#0"; "Mscomctl.ocx"
Object = "{C932BA88-4374-101B-A56C-00AA003668DC}#1.1#0"; "MSMASK32.OCX"
Object = "{0BA686C6-F7D3-101A-993E-0000C0EF6F5E}#1.0#0"; "THREED32.OCX"
Begin VB.Form FrmFacVent 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Facturas Ventas"
   ClientHeight    =   7710
   ClientLeft      =   45
   ClientTop       =   435
   ClientWidth     =   7560
   Icon            =   "FrmFacVent.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   7710
   ScaleMode       =   0  'User
   ScaleWidth      =   7713.036
   Begin VB.Frame FrmFechas 
      Height          =   1215
      Left            =   240
      TabIndex        =   4
      Top             =   6240
      Width           =   4575
      Begin VB.CheckBox ChkFechas 
         Caption         =   "Condicionar las fechas del reporte"
         Height          =   315
         Left            =   120
         TabIndex        =   5
         Top             =   120
         Width           =   4095
      End
      Begin MSMask.MaskEdBox MskFechFin 
         Height          =   315
         Left            =   3120
         TabIndex        =   9
         Top             =   600
         Width           =   1215
         _ExtentX        =   2143
         _ExtentY        =   556
         _Version        =   393216
         Enabled         =   0   'False
         MaxLength       =   10
         Format          =   "dd/mm/yyyy"
         Mask            =   "##/##/####"
         PromptChar      =   "_"
      End
      Begin MSMask.MaskEdBox MskFechIni 
         Height          =   315
         Left            =   1080
         TabIndex        =   7
         Top             =   600
         Width           =   1215
         _ExtentX        =   2143
         _ExtentY        =   556
         _Version        =   393216
         Enabled         =   0   'False
         MaxLength       =   10
         Format          =   "dd/mm/yyyy"
         Mask            =   "##/##/####"
         PromptChar      =   "_"
      End
      Begin VB.Label LblDesde 
         Alignment       =   1  'Right Justify
         Caption         =   "Desde:"
         Height          =   255
         Left            =   360
         TabIndex        =   6
         Top             =   600
         Width           =   615
      End
      Begin VB.Label LblHasta 
         Alignment       =   1  'Right Justify
         Caption         =   "Hasta:"
         Height          =   255
         Left            =   2400
         TabIndex        =   8
         Top             =   600
         Width           =   615
      End
   End
   Begin VB.CheckBox ChkTercero 
      Caption         =   "Todos los Terceros"
      Height          =   315
      Left            =   120
      TabIndex        =   0
      Top             =   120
      Width           =   3015
   End
   Begin VB.CheckBox ChckBodegas 
      Caption         =   "Todas la Bodegas"
      Height          =   375
      Left            =   120
      TabIndex        =   2
      Top             =   3240
      Width           =   2295
   End
   Begin MSComctlLib.ListView LstVBodega 
      Height          =   2535
      Left            =   120
      TabIndex        =   3
      Top             =   3600
      Width           =   7215
      _ExtentX        =   12726
      _ExtentY        =   4471
      View            =   3
      LabelEdit       =   1
      MultiSelect     =   -1  'True
      LabelWrap       =   -1  'True
      HideSelection   =   0   'False
      Checkboxes      =   -1  'True
      FullRowSelect   =   -1  'True
      GridLines       =   -1  'True
      _Version        =   393217
      ForeColor       =   -2147483640
      BackColor       =   -2147483643
      BorderStyle     =   1
      Appearance      =   1
      NumItems        =   0
   End
   Begin MSComctlLib.ListView LstVTercero 
      Height          =   2655
      Left            =   120
      TabIndex        =   1
      Top             =   480
      Width           =   7215
      _ExtentX        =   12726
      _ExtentY        =   4683
      View            =   3
      LabelEdit       =   1
      MultiSelect     =   -1  'True
      LabelWrap       =   -1  'True
      HideSelection   =   0   'False
      AllowReorder    =   -1  'True
      Checkboxes      =   -1  'True
      FullRowSelect   =   -1  'True
      GridLines       =   -1  'True
      _Version        =   393217
      ForeColor       =   -2147483640
      BackColor       =   -2147483643
      BorderStyle     =   1
      Appearance      =   1
      NumItems        =   2
      BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Text            =   "Nit."
         Object.Width           =   2647
      EndProperty
      BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   1
         Text            =   "Tercero"
         Object.Width           =   7409
      EndProperty
   End
   Begin Threed.SSCommand SCmdOpcion 
      Height          =   735
      Left            =   6360
      TabIndex        =   10
      ToolTipText     =   "Imprimir"
      Top             =   6480
      Width           =   855
      _Version        =   65536
      _ExtentX        =   1508
      _ExtentY        =   1296
      _StockProps     =   78
      Caption         =   "&IMPRIMIR"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   6
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      RoundedCorners  =   0   'False
      Picture         =   "FrmFacVent.frx":058A
   End
   Begin Crystal.CrystalReport Crys_Listar 
      Left            =   7080
      Top             =   7200
      _ExtentX        =   741
      _ExtentY        =   741
      _Version        =   348160
      WindowLeft      =   20
      WindowWidth     =   750
      WindowHeight    =   450
      WindowTitle     =   "Facturas Ventas"
      WindowControlBox=   -1  'True
      WindowMaxButton =   -1  'True
      WindowMinButton =   -1  'True
      WindowControls  =   -1  'True
      PrintFileLinesPerPage=   60
      WindowShowGroupTree=   -1  'True
      WindowShowCloseBtn=   -1  'True
      WindowShowSearchBtn=   -1  'True
      WindowShowPrintSetupBtn=   -1  'True
      WindowShowRefreshBtn=   -1  'True
   End
End
Attribute VB_Name = "FrmFacVent"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'---------------------------------------------------------------------------------------
' Module    : FrmFacVent
' DateTime  : 02/04/2018 08:36
' Author    : daniel_mesa
' Purpose   : DRMG T43070-R41726 Visualiza facturas de venta detalladas, filtrada por tercero, bodega y fecha, agrupadas por consecutivo de comprobante
'---------------------------------------------------------------------------------------
Option Explicit 'DRMG T43415
'---------------------------------------------------------------------------------------
' Procedure : Imrimir
' DateTime  : 14/03/2018 10:46
' Author    : daniel_mesa
' Purpose   : DRMG T43070-R41726 Imprime el informe de factura de venta
'---------------------------------------------------------------------------------------
'
Private Sub Imprimir()
   Dim ElClnt As New ElTercero
   ElClnt.IniXNit
    
   On Error GoTo Error_Listar
   Crys_Listar.Destination = crptToWindow
   ReportConnect = "ODBC;DSN=" + BDDSNODBCName + ";"
   ReportConnect = ReportConnect & ";uid=" + UserIdBD + ";pwd=" + UserPwdBD
   ReportConnect = ReportConnect & ";server=" + ServName
   With Crys_Listar
      .ReportFileName = App.Path & "\InfFactVent.RPT"
      Crys_Listar.Connect = ReportConnect
      .Formulas(0) = "ENTI='" & Entidad & Comi
      .Formulas(1) = "NIT='NIT." & Nit & "-" & DigitoNIT(CStr(Nit)) & Comi
      .Formulas(3) = "USUARIO = '" & UserId & Comi
      .Formulas(2) = "FECHA= 'Del: " & MskFechIni.Text & " Al: " & MskFechFin.Text & Comi
      .Action = 1
    End With
Exit Sub
Error_Listar:
   If ERR.Number <> 0 And ERR.Number <> 20 And ERR.Number <> 20000 Then Call Mensaje1(ERR.Number & ESP$ & ERR.Description & "  Nombre del Reporte : InfFactVent.RPT", 1)
End Sub

'---------------------------------------------------------------------------------------
' Procedure : ChckBodegas_Click
' DateTime  : 14/03/2018 09:27
' Author    : daniel_mesa
' Purpose   : DRMG T43070-R41726  selecciona todas las bodegas
'---------------------------------------------------------------------------------------
'
Private Sub ChckBodegas_Click()
   Dim IntCont As Integer 'DRMG T43415 cuenta la cantidad de bodegas seleccionadas
   If ChckBodegas.value = 1 Then
      For IntCont = 1 To LstVBodega.ListItems.Count
         LstVBodega.ListItems(IntCont).Checked = True
      Next
   Else
      For IntCont = 1 To LstVBodega.ListItems.Count
         LstVBodega.ListItems(IntCont).Checked = False
      Next
   End If
End Sub

'---------------------------------------------------------------------------------------
' Procedure : ChckBodegas_KeyPress
' DateTime  : 14/03/2018 09:27
' Author    : daniel_mesa
' Purpose   : DRMG T43070-R41726  tabulador
'---------------------------------------------------------------------------------------
'
Private Sub ChckBodegas_KeyPress(KeyAscii As Integer)
   Call Cambiar_Enter(KeyAscii)
End Sub


Private Sub ChkFechas_Click()
   Call Un_Enable_Fechas
End Sub

'---------------------------------------------------------------------------------------
' Procedure : Un_Enable_Fechas
' DateTime  : 15/03/2018 08:32
' Author    : jairo_parra
' Purpose   : DRMG T43070-R41726  Habilita o deshavilita los campos de fecha
'---------------------------------------------------------------------------------------
'
Private Sub Un_Enable_Fechas()
   
   If ChkFechas.value = Unchecked Then
      MskFechIni.Enabled = False
      MskFechFin.Enabled = False
   Else
      MskFechIni.Enabled = True
      MskFechFin.Enabled = True
   End If
   
End Sub

'---------------------------------------------------------------------------------------
' Procedure : ChkFechas_KeyPress
' DateTime  : 16/03/2018 11:47
' Author    : daniel_mesa
' Purpose   : DRMG T43070-R41726 hace que el enter funcione como tap
'---------------------------------------------------------------------------------------
'
Private Sub ChkFechas_KeyPress(KeyAscii As Integer)
   Call Cambiar_Enter(KeyAscii)
End Sub

'---------------------------------------------------------------------------------------
' Procedure : ChkTercero_Click
' DateTime  : 14/03/2018 09:24
' Author    : daniel_mesa
' Purpose   : DRMG T43070-R41726  Seleciona todos los terceros
'---------------------------------------------------------------------------------------
'
Private Sub ChkTercero_Click()
   Dim IntCont As Integer 'DRMG T43415 cuenta la cantidad de terceros seleccionados
   If ChkTercero.value = 1 Then
      For IntCont = 1 To LstVTercero.ListItems.Count
         LstVTercero.ListItems(IntCont).Checked = True
      Next
   Else
      For IntCont = 1 To LstVTercero.ListItems.Count
         LstVTercero.ListItems(IntCont).Checked = False
      Next
   End If
End Sub

'---------------------------------------------------------------------------------------
' Procedure : ChkTercero_KeyPress
' DateTime  : 14/03/2018 09:25
' Author    : daniel_mesa
' Purpose   : DRMG T43070-R41726  hace que el enter funcione como tabulador
'---------------------------------------------------------------------------------------
'
Private Sub ChkTercero_KeyPress(KeyAscii As Integer)
   Call Cambiar_Enter(KeyAscii)
End Sub


'---------------------------------------------------------------------------------------
' Procedure : Form_Load
' DateTime  : 07/03/2018 10:06
' Author    : daniel_mesa
' Purpose   : DRMG T43070-R41726 trae las bodegas y terceros que tengan relacionado una o mas facturas de venta
'---------------------------------------------------------------------------------------
'
Private Sub Form_Load()

   Dim ArTerc() As Variant 'Almacena los terceros
   Dim ArBodega() As Variant 'Almacena las bogegas
   Dim Ini As Integer 'DRMG T43415 contador que sirve para ir agregando la cantidad de terceros y bodegas dentro de cada grid
   
   Call CenterForm(MDI_Inventarios, Me)
   Call Leer_Permiso(Me.Name, SCmdOpcion, "I")
   Me.LstVBodega.ListItems.Clear
   Me.LstVBodega.MultiSelect = True
   Me.LstVBodega.HideSelection = False
   Me.LstVBodega.ColumnHeaders.Clear
   Me.LstVBodega.ColumnHeaders.Add , "NOM", "Nombre de la Bodega", 0.66 * Me.LstVBodega.Width
   Me.LstVBodega.ColumnHeaders.Add , "COD", "C�digo", 0.34 * Me.LstVBodega.Width
   
   Me.LstVTercero.ListItems.Clear
   Me.LstVTercero.MultiSelect = True
   Me.LstVTercero.HideSelection = False
   Me.LstVTercero.ColumnHeaders.Clear
   Me.LstVTercero.ColumnHeaders.Add , "COTE", "NIT", 0.3 * Me.LstVTercero.Width
   Me.LstVTercero.ColumnHeaders.Add , "NOTE", "Nombre", 0.6 * Me.LstVTercero.Width
   
   ReDim ArTerc(1, 0)
   Campos = "DISTINCT CD_CODI_TERC, NO_NOMB_TERC"
   Desde = "TERCERO, IN_ENCABEZADO, IN_DOCUMENTO"
   Condicion = "NU_AUTO_DOCU_ENCA = NU_AUTO_DOCU AND CD_CODI_TERC_ENCA = CD_CODI_TERC"
   Condicion = Condicion & " AND NU_AUTO_DOCU=11 ORDER BY NO_NOMB_TERC"
   Result = LoadMulData(Desde, Campos, Condicion, ArTerc)
   
   For Ini = 0 To UBound(ArTerc, 2)
       Me.LstVTercero.ListItems.Add , "T" & Trim(ArTerc(0, Ini)), Trim(ArTerc(0, Ini))
       Me.LstVTercero.ListItems("T" & Trim(ArTerc(0, Ini))).ListSubItems.Add , "NAME", Trim(ArTerc(1, Ini))
   Next

   ReDim ArBodega(2, 0)
   Desde = "IN_BODEGA,IN_R_BODE_COMP,IN_COMPROBANTE"
   Campos = "DISTINCT NU_AUTO_BODE,TX_CODI_BODE, TX_NOMB_BODE"
   Condicion = "NU_AUTO_BODE=NU_AUTO_BODE_RBOCO AND NU_AUTO_COMP_RBOCO=NU_AUTO_COMP"
   Condicion = Condicion & " AND NU_AUTO_DOCU_COMP=11"
   Condicion = Condicion & " ORDER BY TX_NOMB_BODE" 'DRMG T43417 se agrega order by para organizar las bodegas alfabeticamente
   Result = LoadMulData(Desde, Campos, Condicion, ArBodega())
   For Ini = 0 To UBound(ArBodega, 2)
      Me.LstVBodega.ListItems.Add , "B" & ArBodega(0, Ini), ArBodega(2, Ini)
      Me.LstVBodega.ListItems("B" & ArBodega(0, Ini)).ListSubItems.Add , "COD", ArBodega(1, Ini)
   Next
   
   MskFechIni.Text = Format(Date, "dd/mm/yyyy")
   MskFechFin.Text = MskFechIni.Text
    
End Sub

'---------------------------------------------------------------------------------------
' Procedure : LstVBodega_KeyPress
' DateTime  : 16/03/2018 11:51
' Author    : daniel_mesa
' Purpose   : DRMG T43070-R41726 Hace que el enter funcione como tabulador
'---------------------------------------------------------------------------------------
'
Private Sub LstVBodega_KeyPress(KeyAscii As Integer)
   Call Cambiar_Enter(KeyAscii)
End Sub

'---------------------------------------------------------------------------------------
' Procedure : LstVTercero_KeyPress
' DateTime  : 16/03/2018 11:50
' Author    : daniel_mesa
' Purpose   : DRMG T43070-R41726 Hace que el enter funcione como tabulador
'---------------------------------------------------------------------------------------
'
Private Sub LstVTercero_KeyPress(KeyAscii As Integer)
   Call Cambiar_Enter(KeyAscii)
End Sub

'---------------------------------------------------------------------------------------
' Procedure : MskFechFin_KeyPress
' DateTime  : 16/03/2018 11:49
' Author    : daniel_mesa
' Purpose   : DRMG T43070-R41726 Hace que el enter funcione como tabulador
'---------------------------------------------------------------------------------------
'
Private Sub MskFechFin_KeyPress(KeyAscii As Integer)
   Call Cambiar_Enter(KeyAscii)
End Sub

'---------------------------------------------------------------------------------------
' Procedure : MskFechFin_LostFocus
' DateTime  : 15/03/2018 08:41
' Author    : daniel_mesa
' Purpose   : DRMG T43070-R41726  validacion de que la fecha final debe ser menor a la incial
'---------------------------------------------------------------------------------------
'
Private Sub MskFechFin_LostFocus()
    If ChkFechas.value = Checked Then
      If ValFecha(MskFechFin, 1) <> FAIL Then
         'DRMG T43435 INICIO SE DEJA EN COMENTARIO LAS SIGUIENTES LINEAS
         'If DateDiff("d", CDate(MskFechFin.Text), CDate(MskFechIni.Text)) > 0 Then
            'Call Mensaje1("La fecha hasta no puede ser menor a la fecha desde", 3)
            'MskFechFin.Text = MskFechIni.Text
         'End If
         Call ValiFeInMenorFeFin
         'DRMG T43435 FIN
      End If
   End If
End Sub

'---------------------------------------------------------------------------------------
' Procedure : MskFechIni_KeyPress
' DateTime  : 16/03/2018 11:49
' Author    : daniel_mesa
' Purpose   : DRMG T43070-R41726 Hace que el enter funcione como tabulador
'---------------------------------------------------------------------------------------
'
Private Sub MskFechIni_KeyPress(KeyAscii As Integer)
   Call Cambiar_Enter(KeyAscii)
End Sub

'---------------------------------------------------------------------------------------
' Procedure : MskFechIni_LostFocus
' DateTime  : 15/03/2018 08:42
' Author    : daniel_mesa
' Purpose   : DRMG T43070-R41726  validacion de la fecha inicial debe ser menor a la fecha final
'---------------------------------------------------------------------------------------
'
Private Sub MskFechIni_LostFocus()
   If ChkFechas.value = Checked Then
      If ValFecha(MskFechIni, 1) <> FAIL Then
         'DRMG T43435 INICIO SE DEJA EN COMENTARIO LAS SIGUIENTES LINEAS
         'If DateDiff("d", CDate(MskFechFin.Text), CDate(MskFechIni.Text)) > 0 Then
            'Call Mensaje1("La fecha desde no puede ser mayor a la fecha hasta", 3)
            'MskFechIni.Text = MskFechFin.Text
         'End If
         Call ValiFeInMenorFeFin
         'DRMG T43435 FIN
      End If
   End If
End Sub

'---------------------------------------------------------------------------------------
' Procedure : SCmdOpcion_Click
' DateTime  : 15/03/2018 08:43
' Author    : daniel_mesa
' Purpose   : DRMG T43070-R41726  llama la funcion Facturas_de_Venta
'---------------------------------------------------------------------------------------
'
Private Sub SCmdOpcion_Click()
Call Facturas_de_Venta
End Sub

'---------------------------------------------------------------------------------------
' Procedure : Facturas_de_Venta
' DateTime  : 07/03/2018 10:09
' Author    : daniel_mesa
' Purpose   : DRMG T43070-R41726  Trae y guarda las facturas de venta agrupado por bodegas y terceros, ademas los guarda en una tabla temporal
'---------------------------------------------------------------------------------------
'
Private Sub Facturas_de_Venta()
   Dim SPCmd As New ADODB.Command
   'DRMG T43415 INICIO Se deja en comentario las sigientes variables que no se usan
   'Dim StFechaIni As String
   'Dim StFechaFin As String
   'Dim StBodegaSel As String
   'Dim InI As Integer
   'Dim StFecha As String
   Dim StBodega As String 'Amacena las bodegas que fueron seleccionados
   Dim IntCont As Integer 'cuenta la cantidad de bodegas o terceros seleccionados
   Dim StTercero As String 'almacena los terceros seleccionados
   Dim StSql As String 'almacena la cadena SQL a ejecutar
   'DRMG T43415 FIN
   Dim VrTer() As Variant 'DRMG T43417 almacena los resultados de la tabla FACTVENT
   'DRMG T43549 INICIO
   Dim InValrDec As Integer 'Var para obtener el Num. de Decimales para Valores
   Dim InCantDec As Integer 'Var para obtener el Num. de Decimales para Cantidades
   Dim InPorcDec As Integer 'Var para obtener el Num. de Decimales para Porcentajes
   
   InValrDec = IIf(Aplicacion.VerDecimales_Valores(InValrDec), InValrDec, 0)
   InCantDec = IIf(Aplicacion.VerDecimales_Cantidades(InCantDec), InCantDec, 0)
   InPorcDec = IIf(Aplicacion.VerDecimales_Porcentajes(InPorcDec), InPorcDec, 0)
   'DRMG T43549 FIN
   
   On Error GoTo GoErr
   For IntCont = 1 To LstVTercero.ListItems.Count
      If LstVTercero.ListItems(IntCont).Checked Then
         If StTercero <> NUL$ Then StTercero = StTercero & Coma
         StTercero = StTercero & Comi & LstVTercero.ListItems(IntCont).Text & Comi
      End If
   Next

   If StTercero = NUL$ Then
      Call Mensaje1("Debe seleccionar los terceros ", 2)
      Exit Sub
   End If
   
   For IntCont = 1 To LstVBodega.ListItems.Count
      If LstVBodega.ListItems(IntCont).Checked Then
         If StBodega <> NUL$ Then StBodega = StBodega & ","
         StBodega = StBodega & Mid(LstVBodega.ListItems(IntCont).Key, 2)
      End If
   Next
   
   If StBodega = NUL$ Then
      Call Mensaje1("Debe seleccionar como m�nimo una bodega ", 2)
      Exit Sub
   End If
   
   If ExisteTABLA("FACTVENT") Then Result = EliminarTabla("FACTVENT")
   
   If Result <> FAIL Then
      StSql = "SELECT "
      StSql = StSql & "IN_BODEGA.TX_CODI_BODE AS TX_CODI_TEMP, "
      StSql = StSql & "IN_BODEGA.TX_NOMB_BODE AS TX_NOMB_TEMP, "
      StSql = StSql & "IN_ENCABEZADO.NU_COMP_ENCA AS NU_COMP_TEMP, "
      StSql = StSql & "IN_ENCABEZADO.NU_AUTO_DOCU_ENCA AS NU_AUTO_DOCU_TEMP, "
      StSql = StSql & "IN_ENCABEZADO.NU_AUTO_BODEORG_ENCA AS NU_AUTO_BODEORG_TEMP, "
      StSql = StSql & "IN_ENCABEZADO.FE_CREA_ENCA AS FE_CREA_TEMP, IN_ENCABEZADO.FE_CIER_ENCA AS FE_CIER_TEMP, "
      StSql = StSql & "IN_ENCABEZADO.TX_ESTA_ENCA AS TX_ESTA_TEMP, "
      StSql = StSql & "TERCERO.CD_CODI_TERC AS CD_CODI_TEMP, "
      StSql = StSql & "TERCERO.NO_NOMB_TERC AS NO_NOMB_TEMP, "
      StSql = StSql & "SUM(T1.BRUTO) AS BRUTO, "
      StSql = StSql & "SUM(T1.SIN_IVA) AS SIN_iva, "
      StSql = StSql & "SUM(T1.SU_IVA) AS IVA, "
      'DRMG T43549 INICIO SE ENVIA LA CANTIDAD DE DECIMALES PARA IMPRIMIR
      'Y SE DEJA LAS DOS PRIMERAS LINEAS EN COMENTARIO
      'StSql = StSql & "SUM(ISNULL(CAST(T1.BRUTO *(T2.DESCUENTO/100)AS FLOAT),0)) AS DESCUENTO, "
      'StSql = StSql & "SUM(T1.BRUTO+T1.SU_IVA-(ISNULL(CAST(T1.BRUTO *(T2.DESCUENTO/100)AS FLOAT),0))) AS VL_NETO "
      'DRMG T44031 INICIO SE DEJA EN COMENTARIO LAS SIGIENTES 2 LINEAS
      'StSql = StSql & "SUM(ISNULL(CAST(ROUND(T1.BRUTO *(T2.DESCUENTO/100)," & 2 & ",1)AS FLOAT),0)) AS DESCUENTO, "
      'StSql = StSql & "SUM(T1.BRUTO+T1.SU_IVA-(ISNULL(CAST(ROUND(T1.BRUTO *(T2.DESCUENTO/100)," & 2 & ",1)AS FLOAT),0))) AS VL_NETO "
      StSql = StSql & "(CASE WHEN ISNULL(T2.DESCUENTO,0)=0 THEN ISNULL(T3.DESCUENTO,0) ELSE T2.DESCUENTO END) AS  DESCUENTO, "
      StSql = StSql & "SUM(T1.BRUTO+T1.SU_IVA) - (CASE WHEN ISNULL(T2.DESCUENTO,0)=0 THEN ISNULL(T3.DESCUENTO,0) ELSE T2.DESCUENTO END) AS VL_NETO "
      'DRMG T44031 FIN
      StSql = StSql & Coma & Comi & InValrDec & InCantDec & InPorcDec & Comi & " AS DECIMAL "
      'DRMG T43549 FIN
      StSql = StSql & "INTO FACTVENT "
      StSql = StSql & "FROM "
      StSql = StSql & "IN_BODEGA INNER JOIN IN_ENCABEZADO ON NU_AUTO_BODE =NU_AUTO_BODEORG_ENCA "
      StSql = StSql & "INNER JOIN TERCERO ON CD_CODI_TERC=CD_CODI_TERC_ENCA "
      'DRMG T43549 INICIO
      'se deja las siguientes CUATRO linieas en comentario
      'StSql = StSql & "INNER JOIN IN_DOCUMENTO ON IN_ENCABEZADO.NU_AUTO_DOCU_ENCA = IN_DOCUMENTO.NU_AUTO_DOCU "
      'StSql = StSql & "INNER JOIN (SELECT NU_AUTO_ORGCABE_DETA, Round((100*(NU_VALOR_DETA*NU_SALIDA_DETA)/(NU_IMPU_DETA+100)),2,0)AS BRUTO, "
      'StSql = StSql & "(NU_VALOR_DETA*NU_SALIDA_DETA) AS SIN_IVA, "
      'StSql = StSql & "Round(((100*NU_VALOR_DETA/(NU_IMPU_DETA+100))*(NU_IMPU_DETA/100)),2,0) AS SU_IVA
      StSql = StSql & "INNER JOIN IN_COMPROBANTE ON NU_AUTO_COMP = NU_AUTO_COMP_ENCA "
      StSql = StSql & "INNER JOIN IN_DOCUMENTO ON NU_AUTO_DOCU = NU_AUTO_DOCU_COMP "
      StSql = StSql & "INNER JOIN (SELECT NU_AUTO_ORGCABE_DETA, ROUND(((100*(NU_VALOR_DETA))/(NU_IMPU_DETA+100))," & 2 & ",1)*NU_SALIDA_DETA AS BRUTO, "
      StSql = StSql & " ROUND(NU_VALOR_DETA*NU_SALIDA_DETA," & 2 & ",1) AS SIN_IVA, "
      StSql = StSql & "ROUND((ROUND(100*(NU_VALOR_DETA*NU_SALIDA_DETA)/(NU_IMPU_DETA+100)," & 2 & ",1))*(NU_IMPU_DETA/100)," & 2 & ",1) AS SU_IVA "
      'DRMG T43549 FIN
      StSql = StSql & "FROM IN_DETALLE ,IN_ENCABEZADO "
      StSql = StSql & "WHERE IN_DETALLE.NU_AUTO_ORGCABE_DETA=IN_ENCABEZADO.NU_AUTO_ENCA) "
      StSql = StSql & "AS T1 ON T1.NU_AUTO_ORGCABE_DETA=IN_ENCABEZADO.NU_AUTO_ENCA "
      'DRMG T44031 INICIO SE DEJA EN CIOMENTARIO LAS SIGUIENTES 3 LINEAS
      'StSql = StSql & "LEFT JOIN (Select IN_ENCABEZADO.NU_AUTO_ENCA, ISNULL(CAST(NU_VALO_PRPR AS FLOAT),0) AS DESCUENTO "
      'StSql = StSql & "FROM IN_PARTIPARTI, IN_ENCABEZADO WHERE TX_CUAL_PRPR IS NULL "
      'StSql = StSql & "AND NU_AUTO_ENCA_PRPR=IN_ENCABEZADO.NU_AUTO_ENCA) "
      StSql = StSql & "LEFT JOIN (SELECT NU_AUTO_ENCA, SUM(ROUND((((100*(NU_VALOR_DETA))/(NU_IMPU_DETA+100))*NU_SALIDA_DETA)*(PORCENTAJE/100),2,1)) AS DESCUENTO "
      StSql = StSql & "FROM IN_DETALLE "
      StSql = StSql & "INNER JOIN (SELECT NU_AUTO_ENCA, ISNULL(SUM(CAST(NU_VALO_PRPR AS FLOAT)),0) AS PORCENTAJE "
      StSql = StSql & "FROM IN_ENCABEZADO "
      StSql = StSql & "INNER JOIN IN_PARTIPARTI ON TX_CUAL_PRPR IS NULL AND NU_AUTO_ENCA_PRPR=IN_ENCABEZADO.NU_AUTO_ENCA GROUP BY NU_AUTO_ENCA) "
      StSql = StSql & "AS D1 ON NU_AUTO_MODCABE_DETA=NU_AUTO_ENCA GROUP BY NU_AUTO_ENCA) "
      'DRMG T44031 FIN
      StSql = StSql & "AS T2 ON T2.NU_AUTO_ENCA =IN_ENCABEZADO.NU_AUTO_ENCA "
      'DRMG T44031 INICIO
      StSql = StSql & "LEFT JOIN (SELECT NU_AUTO_ENCA,SUM(ROUND((((100*(NU_VALOR_DETA))/(NU_IMPU_DETA+100))*NU_SALIDA_DETA)*(NU_DESC_ENDE/100),2,1)) AS DESCUENTO "
      StSql = StSql & "FROM R_DESCFV_ENDE "
      StSql = StSql & "INNER JOIN IN_ENCABEZADO ON NU_AUENCA_ENDE=NU_AUTO_ENCA "
      StSql = StSql & "INNER JOIN IN_DETALLE ON NU_AUTO_MODCABE_DETA=NU_AUTO_ENCA AND NU_AUDETA_ENDE =NU_AUTO_DETA GROUP BY NU_AUTO_ENCA) "
      StSql = StSql & "AS T3 ON T3.NU_AUTO_ENCA =IN_ENCABEZADO.NU_AUTO_ENCA "
      'DRMG T44031 FIN
      StSql = StSql & "WHERE "
      StSql = StSql & "IN_DOCUMENTO.NU_AUTO_DOCU = 11 "
      If Me.ChkFechas.value Then
         'DRMG T43548 INICIO Se deja en comentario LAS SIGUIENTES DOS LINEAS
         'StSql = StSql & "AND IN_ENCABEZADO.FE_CREA_ENCA>= " & FFechaCon(MskFechIni.Text)
         'StSql = StSql & "AND IN_ENCABEZADO.FE_CREA_ENCA<= " & FFechaCon(MskFechFin.Text)
         StSql = StSql & "AND CONVERT(DATE ,IN_ENCABEZADO.FE_CREA_ENCA)>= " & FFechaCon(MskFechIni.Text)
         StSql = StSql & "AND CONVERT(DATE ,IN_ENCABEZADO.FE_CREA_ENCA)<= " & FFechaCon(MskFechFin.Text)
         'DRMG T43548 FIN
      End If
      StSql = StSql & " AND NU_AUTO_BODEORG_ENCA IN ( " & StBodega & ")"
      StSql = StSql & " AND CD_CODI_TERC_ENCA IN (" & StTercero & ")"
      StSql = StSql & " GROUP BY "
      StSql = StSql & "TX_CODI_BODE,TX_NOMB_BODE,NU_COMP_ENCA, NU_AUTO_DOCU_ENCA, "
      StSql = StSql & "NU_AUTO_BODEORG_ENCA, FE_CREA_ENCA,FE_CIER_ENCA,TX_ESTA_ENCA, "
      StSql = StSql & "CD_CODI_TERC,NO_NOMB_TERC "
      StSql = StSql & Coma & "T3.DESCUENTO,T2.DESCUENTO " 'DRMG T44031
      StSql = StSql & "ORDER BY "
      StSql = StSql & "NU_COMP_ENCA"
      Result = ExecSQLCommand(StSql)
   End If
   
   'DRMG T43417 INICIO SE CREA VAIDACION PARA SAVER SI EXISTEN DATOS A MOSTRAR PARA IMPRIMIR
   'ADICIONALMENTE SE DEJA EN COMENTARIO LA SIGUIENTE LINEA
   'If Result <> FAIL Then Call Imprimir
   If Result <> FAIL Then
      ReDim VrTer(0)
      Result = LoadData("FACTVENT", "TX_CODI_TEMP", NUL$, VrTer())
      If Result <> FAIL Then
         If Encontro = False Then
            Call Mensaje1("No se han encontrado datos relacionados", 3)
         Else
            Call Imprimir
         End If
      End If
   End If
   'DRMG T43417 FIN
   If ExisteTABLA("FACTVENT") Then Result = EliminarTabla("FACTVENT")
   Call MouseNorm
   Exit Sub
   
GoErr:
    Call ConvertErr
    If Not SPCmd Is Nothing Then Set SPCmd = Nothing
    Call MouseNorm
    
End Sub

'---------------------------------------------------------------------------------------
' Procedure : ValiFeInMenorFeFin
' DateTime  : 03/04/2018 15:27
' Author    : daniel_mesa
' Purpose   : DRMG T43435 valida que la fecha inicial no sea mayor que la final
'---------------------------------------------------------------------------------------
'
Sub ValiFeInMenorFeFin()
   If DateDiff("yyyy", MskFechIni, MskFechFin) < 0 Then Call Mensaje1("La fecha inicial no puede ser mayor a la fecha final", 3): MskFechFin = Format(FechaServer, "dd/mm/yyyy"): MskFechIni.SetFocus: Exit Sub
   If DateDiff("m", MskFechIni, MskFechFin) < 0 And DateDiff("yyyy", MskFechIni, MskFechFin) <= 0 Then Call Mensaje1("La fecha inicial no puede ser mayor a la fecha final", 3): MskFechFin = Format(FechaServer, "dd/mm/yyyy"): MskFechIni.SetFocus: Exit Sub
   If DateDiff("d", MskFechIni, MskFechFin) < 0 Then Call Mensaje1("La fecha inicial no puede ser mayor a la fecha final", 3): MskFechFin = Format(FechaServer, "dd/mm/yyyy"): MskFechIni.SetFocus
End Sub
