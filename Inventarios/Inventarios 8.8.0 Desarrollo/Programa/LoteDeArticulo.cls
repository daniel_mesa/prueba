VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "LoteDeArticulo"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit

Private lte As Long 'Autonum�rico del lote en IN_LOTE
Private bdg As Long 'Autonum�rico de la bodega en IN_BODEGA
Private num As Long 'Autonum�rico del art�culo en ARTICULO
Private art As String * 16 'C�digo del art�culo
Private nmb As String * 50 'Nombre del art�culo
Private fch As Date 'Fecha de vencimiento/entrada del lote
Private acu As Long 'Saldo actual en la unidad de venta saldo=(act*mlt/dvs)
Private mlt As Long 'Multiplicador de la unidad de venta
Private dvs As Long 'Divisor de la unidad de venta
'lte, bdg, num, art, nmb, fch, acu, mlt, dvs

Public Property Let Bodega(cnt As Long)
    bdg = cnt
End Property

Public Property Get Bodega() As Long
    Bodega = bdg
End Property

Public Property Let NumeroLote(cnt As Long)
    lte = cnt
End Property

Public Property Get NumeroLote() As Long
    NumeroLote = lte
End Property

Public Property Let FechaLote(cal As Date)
    fch = cal
End Property

Public Property Get FechaLote() As Date
    FechaLote = fch
End Property

Public Property Let NumeroArticulo(cnt As Long)
    num = cnt
End Property

Public Property Get NumeroArticulo() As Long
    NumeroArticulo = num
End Property

Public Property Let ActualXBodega(cnt As Long)
    acu = cnt
End Property

Public Property Get ActualXBodega() As Long
    ActualXBodega = acu
End Property

Public Property Let CodigoArticulo(Cue As String)
    art = Cue
End Property

Public Property Get CodigoArticulo() As String
    CodigoArticulo = art
End Property

Public Property Get NombreArticulo() As String
    NombreArticulo = nmb
End Property

Public Property Let NombreArticulo(Cue As String)
    nmb = Cue
End Property

Public Property Let Multiplicador(cnt As Long)
    If cnt > 1 Then mlt = cnt
End Property

Public Property Get Multiplicador() As Long
    Multiplicador = mlt
End Property

Public Property Let Divisor(cnt As Long)
    If cnt > 1 Then dvs = cnt
End Property

Public Property Get Divisor() As Long
    Divisor = dvs
End Property

Private Sub Class_Initialize()
    fch = Date
    mlt = 1
    dvs = 1
End Sub

Public Sub INIXNumero()
'lte, bdg, num, art, nmb, fch, acu, mlt, dvs
    Dim ArrTemp() As Variant
    Campos = "NU_AUTO_ARTI, CD_CODI_ARTI, NO_NOMB_ARTI, NU_AUTO_UNVE, TX_CODI_UNVE, " & _
        "TX_NOMB_UNVE, NU_MULT_UNVE, NU_DIVI_UNVE"
    Desde = "ARTICULO, IN_UNDVENTA"
    condi = "ARTICULO.NU_AUTO_UNVE_ARTI=IN_UNDVENTA.NU_AUTO_UNVE AND " & _
        "ARTICULO.NU_AUTO_ARTI=" & num
    ReDim ArrTemp(7)
    Result = LoadData(Desde, Campos, condi, ArrTemp())
    If Result = FAIL Then GoTo FALLO
    If Not encontro Then GoTo NOENC
    art = ArrTemp(1) 'CD_CODI_ARTI
    nmb = ArrTemp(2) 'NO_NOMB_ARTI
    mlt = ArrTemp(6) 'NU_MULT_UNVE
    dvs = ArrTemp(7) 'NU_DIVI_UNVE
FALLO:
NOENC:
End Sub

'    Dim ArrTemp() As Variant, CnTdr As Integer
'    Campos = ""
'    Desde = ""
'    Condi = ""
'    ReDim ArrTemp(1, 0)
'    Result = LoadMulData(Desde, Campos, Condi, ArrTemp())
'    If Result <> FAIL Then
'        If Encontro Then
'            For CnTdr = 0 To UBound(ArrTemp, 2)
'                cbx.ItemData(cbx.NewIndex) = ArrTemp(0, CnTdr) 'RST("NU_AUTO_BODE")
'            Next
'        End If
'    End If

'    Dim ArrTemp() As Variant
'    Campos = ""
'    Desde = ""
'    Condi = ""
'    ReDim ArrTemp(0)
'    Result = LoadData(Desde, Campos, Condi, ArrTemp())
'    If Result <> FAIL Then
'        If Encontro Then
'           cdi = ArrTemp(4) 'TX_CODI_UNVE
'        End If
'    End If

