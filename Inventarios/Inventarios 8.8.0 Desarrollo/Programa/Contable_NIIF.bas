Attribute VB_Name = "Contable_NIIF"
'---------------------------------------------------------------------------------------
' Module    : Contable_NIIF
' DateTime  : 09/01/2015 11:46
' Author    : jairo_parra
' Purpose   : JLPB T25784-R25328
'---------------------------------------------------------------------------------------

Option Explicit

Public xArr13() As Plano013

Public Type Plano013
   Archivo As String
   Cliente As String
   NombreCliente As String
   TipoDato As String
   Periodo As String
   SaldoAnterior As Double
   Saldo As Double
   Saldo60 As Double
   Saldo90 As Double
   Saldo180 As Double
   Saldo360 As Double
   SaldoM360 As Double
End Type


'VALIDA QUE NO EXISTAN NIVELES SUPERIORES; TRUE, FALSO EN LAS CUENTAS CONTABLES
'---------------------------------------------------------------------------------------
' Procedure : Valida_Nivel_Cuentas_NIIF
' DateTime  : 09/01/2015 11:46
' Author    : jairo_parra
' Purpose   : JLPB T25784-R25328
'---------------------------------------------------------------------------------------
'
Function Valida_Nivel_Cuentas_NIIF(Cuenta As String) As Boolean
   ReDim Arr(0, 0)
   If Len(Cuenta) >= 6 Then
      Condicion = "CD_CODI_CUEN LIKE '" & Cambiar_Comas_Comillas(Cuenta) & "%'"
      Result = LoadMulData("CUENTAS", "CD_CODI_CUEN", Condicion, Arr())
      If Result <> FAIL Then
         If UBound(Arr(), 2) > 0 Then
            Valida_Nivel_Cuentas_NIIF = True
         Else
            Valida_Nivel_Cuentas_NIIF = False
         End If
      Else
         Valida_Nivel_Cuentas_NIIF = True
      End If
   Else
      Valida_Nivel_Cuentas_NIIF = True
   End If
End Function


'---------------------------------------------------------------------------------------
' Procedure : Crear_Movimiento_Contable_NIIF
' DateTime  : 09/01/2015 11:46
' Author    : jairo_parra
' Purpose   : JLPB T25784-R25328
'---------------------------------------------------------------------------------------
'
Function Crear_Movimiento_Contable_NIIF(Datos() As Variant) As Boolean
   Dim ArrCta() As Variant
   Dim StNombreUsuario As String
   ReDim Arr(0)
   
   If Result <> FAIL Then
      Result = LoadData("USUARIO", "TX_DESC_USUA", "TX_IDEN_USUA = '" & UserId & Comi, Arr)
      If Encontro Then StNombreUsuario = Arr(0)
   End If
   
   If MotorBD <> "SQL" Then
      ReDim aux(0)
      ReDim ArrCta(0)
      Result = LoadData("CUENTAS", "CD_CODI_CUEN", "CD_CODI_CUEN=" & Comi & Datos(0) & Comi, ArrCta)
      If Result <> FAIL And Not Encontro Then
         Result = FAIL
         Crear_Movimiento_Contable_NIIF = FAIL
         Call Mensaje1("La cuenta: " & Datos(0) & " no existe en la tabla Cuentas", 1)
         Exit Function
      End If
      Campos = "CD_CENT_MVNF = " & Comi & CentroC_Default & Comi
      Campos = Campos & Coma & "CD_CUEN_MVNF = " & Comi & Datos(0) & Comi
      Campos = Campos & Coma & "CD_TERC_MVNF = " & Comi & Datos(1) & Comi
      Campos = Campos & Coma & "FE_FECH_MVNF = " & Comi & Format(CDate(CStr(Datos(2))), FormatF) & Comi
      Campos = Campos & Coma & "NU_COMP_MVNF = " & Comi & Datos(3) & Comi
      Campos = Campos & Coma & "NU_CONS_MVNF = " & CLng(Datos(4))
      Campos = Campos & Coma & "NU_IMPU_MVNF = " & CInt(Datos(5))
      Campos = Campos & Coma & "DE_DESC_MVNF = " & Comi & Mid(Cambiar_Comas_Comillas(CStr(Datos(6))), 1, 249) & Comi
      Campos = Campos & Coma & "VL_VALO_MVNF = " & CDbl(Datos(7))
      Campos = Campos & Coma & "ID_NATU_MVNF = " & Comi & Datos(8) & Comi
      Campos = Campos & Coma & "CD_CECO_MVNF = " & Comi & Datos(9) & Comi
      Campos = Campos & Coma & "NU_CHEQ_MVNF = " & Comi & Cambiar_Comas_Comillas(CStr(Datos(10))) & Comi
      Campos = Campos & Coma & "VL_REAL_MVNF = " & CDbl(Datos(11))
      Campos = Campos & Coma & "NU_MES_MVNF = " & CByte(Datos(12))
      Campos = Campos & Coma & "ID_GETR_MVNF = " & Comi & "X" & Comi
      Campos = Campos & Coma & "TX_USER_MVNF = " & Comi & Cambiar_Comas_Comillas(StNombreUsuario) & Comi
    
      Condicion = "NU_COMP_MVNF = " & Comi & Datos(3) & Comi
      Condicion = Condicion & " AND NU_CONS_MVNF=" & Datos(4)
      Condicion = Condicion & " AND NU_IMPU_MVNF=" & Datos(5)
      Result = LoadData("MOVIMIENTOS_NIIF", "CD_CUEN_MVNF", Condicion, aux())
      If Result <> FAIL Then
         If Datos(5) > 1 Then aux(0) = ""
            If aux(0) = NUL$ Then
            Campos = Campos & Coma & "NU_TIPMOV_MVNF = " & Comi & Datos(17) & Comi
            Campos = Campos & Coma & "NU_MOV_MVNF = " & CLng(Datos(18))
            Result = DoInsertSQL("MOVIMIENTOS_NIIF", Campos)
            If Result <> FAIL Then
                Result = Auditor("MOVIMIENTOS_NIIF", TranIns, LastCmd)
                If (Result <> FAIL) Then Call Saldos_NIIF(CStr(Datos(0)), CByte(Datos(12)), CDbl(Datos(7)), CStr(Datos(8)))
                'CUENTA - COMPROBANTE
                Campos = Mid(Datos(0), 1, 4) 'NIVEL 3
                If (Result <> FAIL) Then Call Cuenta_Comprobante_NIIF(Campos, CStr(Datos(3)), CByte(Datos(12)), CDbl(Datos(7)), CStr(Datos(8)))
                Campos = Mid(Datos(0), 1, 6) 'NIVEL 4
                If (Result <> FAIL) Then Call Cuenta_Comprobante_NIIF(Campos, CStr(Datos(3)), CByte(Datos(12)), CDbl(Datos(7)), CStr(Datos(8)))
                'CUENTA - TERCERO
                If (Result <> FAIL) Then If Datos(1) <> NUL$ Then Call Cuenta_Tercero_NIIF(CStr(Datos(0)), CStr(Datos(1)), CByte(Datos(12)), CDbl(Datos(7)), CStr(Datos(8)), CDbl(Datos(11)))
                'CUENTA - CENTRO DE COSTO
                If (Result <> FAIL) Then If Datos(9) <> NUL$ Then Call Cuenta_CentroCosto_NIIF(CStr(Datos(0)), CStr(Datos(9)), CByte(Datos(12)), CDbl(Datos(7)), CStr(Datos(8)))
                'CUENTA - CENTRO DE COSTO - TERCERO
                If (Result <> FAIL) Then
                   If Datos(1) <> NUL$ And Datos(9) <> NUL$ Then Call Cuenta_CentroTercero_NIIF(CStr(Datos(0)), CStr(Datos(9)), CStr(Datos(1)), CByte(Datos(12)), CDbl(Datos(7)), CStr(Datos(8)), CDbl(Datos(11)))
                End If
            End If
            Crear_Movimiento_Contable_NIIF = Result
         Else
            If Datos(5) = 1 Then Call Mensaje1("El movimiento contable NIIF para el comprobante """ & Datos(3) & " " & Datos(4) & " " & Datos(5) & """; Ya se encuentra registrado en la Contabilidad.", 2)
            Crear_Movimiento_Contable_NIIF = FAIL
         End If
      End If
   ElseIf MotorBD = "SQL" Then
      Dim Cmd As New ADODB.Command
      Call CreaParametrosSP(Cmd, "CD_CENT_MVNF", adVarChar, "ZZZZZZZZZZZ", adParamInput, 11)
      Call CreaParametrosSP(Cmd, "CD_CUEN_MVNF", adVarChar, Datos(0), adParamInput, 15)
      Call CreaParametrosSP(Cmd, "CD_TERC_MVNF", adVarChar, Datos(1), adParamInput, 20)
      Call CreaParametrosSP(Cmd, "FE_FECH_MVNF", adDate, Format(CDate(CStr(Datos(2))), FormatF), adParamInput, 19)
      Call CreaParametrosSP(Cmd, "NU_COMP_MVNF", adVarChar, Datos(3), adParamInput, 3)
      Call CreaParametrosSP(Cmd, "NU_CONS_MVNF", adNumeric, Datos(4), adParamInput, 20, 11)
      Call CreaParametrosSP(Cmd, "NU_IMPU_MVNF", adNumeric, Datos(5), adParamInput, 20, 5)
      Call CreaParametrosSP(Cmd, "DE_DESC_MVNF", adVarChar, Cambiar_Comas_Comillas(CStr(Datos(6))), adParamInput, 250)
      Call CreaParametrosSP(Cmd, "VL_VALO_MVNF", adNumeric, Datos(7), adParamInput, 20, 28, 2)
      Call CreaParametrosSP(Cmd, "ID_NATU_MVNF", adVarChar, Datos(8), adParamInput, 1)
      Call CreaParametrosSP(Cmd, "CD_CECO_MVNF", adVarChar, Datos(9), adParamInput, 11)
      Call CreaParametrosSP(Cmd, "NU_CHEQ_MVNF", adVarChar, Datos(10), adParamInput, 10)
      Call CreaParametrosSP(Cmd, "VL_REAL_MVNF", adNumeric, Datos(11), adParamInput, 20, 28, 2)
      Call CreaParametrosSP(Cmd, "NU_MES_MVNF", adNumeric, Datos(12), adParamInput, 2, 3)
      Call CreaParametrosSP(Cmd, "ID_GETR_MVNF", adVarChar, "X", adParamInput, 1)
      Call CreaParametrosSP(Cmd, "TX_USER_MVNF", adVarChar, StNombreUsuario, adParamInput, 50)
      Call CreaParametrosSP(Cmd, "TIPO_IVA", adInteger, Datos(14), adParamInput)
      Call CreaParametrosSP(Cmd, "VL_VALIVA", adNumeric, Datos(15), adParamInput, 20, 28, 2)
      Call CreaParametrosSP(Cmd, "VL_VALBIVA", adNumeric, Datos(16), adParamInput, 20, 28, 2)
      Call CreaParametrosSP(Cmd, "Resultado", adInteger, 0, adParamOutput)
      Call CreaParametrosSP(Cmd, "TIPO_EMPRE", adInteger, IIf(BoTipoEmpre = True, 1, 0), adParamInput)
      Call CreaParametrosSP(Cmd, "NU_TIPMOV_MVNF", adVarChar, Datos(17), adParamInput, 10)
      Call CreaParametrosSP(Cmd, "NU_MOV_MVNF", adVarChar, Datos(18), adParamInput, 11)
      
      Cmd.ActiveConnection = BD(BDCurCon)
      Cmd.CommandType = adCmdStoredProc
      Cmd.CommandText = "PA_Crear_Movimiento_Contable_niif"
      On Error GoTo Error
      Cmd.Execute
      Result = Cmd.Parameters.Item("@Resultado").Value

      Set Cmd = Nothing
      Crear_Movimiento_Contable_NIIF = Result
      Exit Function
      
Error:
      Call ConvertErr
      FrmCmd.TxtCmd.Text = "Para ver la ultima Sentencia Remitase al Servidor : Visor de Sucesos De Windows"
      Set Cmd = Nothing
      Result = FAIL
      Crear_Movimiento_Contable_NIIF = FAIL
   End If
End Function


'Busca los niveles de una cuenta para actualizar los saldos en cada una de ellas
'---------------------------------------------------------------------------------------
' Procedure : Saldos_NIIF
' DateTime  : 09/01/2015 11:46
' Author    : jairo_parra
' Purpose   : JLPB T25784-R25328
'---------------------------------------------------------------------------------------
'
Sub Saldos_NIIF(ByVal Cuenta As String, Mes As Byte, Valor As Double, Naturaleza As String)
   Dim I As Byte
   Dim ArrCta() As Variant

   I = Nivel_Cuenta_NIIF(Cuenta)
   Do While I > 0 And Cuenta <> NUL$
      If BoTipoEmpre Then
         Select Case I
            Case 1: Cuenta = Mid(Cuenta, 1, 1)
            Case 2: Cuenta = Mid(Cuenta, 1, 2)
            Case 3: Cuenta = Mid(Cuenta, 1, 4)
            Case 4: Cuenta = Mid(Cuenta, 1, 6)
            Case 5: Cuenta = Mid(Cuenta, 1, 8)
            Case 6: Cuenta = Mid(Cuenta, 1, 10)
            Case 7: Cuenta = Mid(Cuenta, 1, 12)
         End Select
      Else
         Select Case I
            Case 1: Cuenta = Mid(Cuenta, 1, 1)
            Case 2: Cuenta = Mid(Cuenta, 1, 2)
            Case 3: Cuenta = Mid(Cuenta, 1, 4)
            Case 4: Cuenta = Mid(Cuenta, 1, 6)
            Case 5: Cuenta = Mid(Cuenta, 1, 9)
            Case 6: Cuenta = Mid(Cuenta, 1, 12)
            Case 7: Cuenta = Mid(Cuenta, 1, 15)
         End Select
      End If
      ReDim ArrCta(0)
      Result = LoadData("CUENTAS", "CD_CODI_CUEN", "CD_CODI_CUEN=" & Comi & Cuenta & Comi, ArrCta)
      If Result <> FAIL And Not Encontro Then
         Result = FAIL
         Call Mensaje1("La cuenta: " & Cuenta & " no existe en la tabla Cuentas", 1)
         Exit Sub
      End If
      Call Actualiza_Cuentas_NIIF(Cuenta, Mes, Valor, Naturaleza)
      I = I - 1
   Loop
End Sub


'Actualiza los saldos en los diferentes niveles de la cuenta.
'---------------------------------------------------------------------------------------
' Procedure : Actualiza_Cuentas_NIIF
' DateTime  : 09/01/2015 11:46
' Author    : jairo_parra
' Purpose   : JLPB T25784-R25328
'---------------------------------------------------------------------------------------
'
Sub Actualiza_Cuentas_NIIF(ByVal Cuenta As String, Mes As Byte, Valor As Double, Naturaleza As String)
   Dim Arr(0)
   
   If Valor <> 0 Then Valor = Format(Valor, "####0.#0")
   Condicion = "CD_CUEN_SANF = '" & Cuenta & Comi & " AND NU_MES_SANF = " & Mes
   Result = LoadData("SALDOS_NIIF", "CD_CUEN_SANF", Condicion, Arr())
   If Arr(0) = NUL$ Then
      Campos = "CD_CUEN_SANF = " & Comi & Cuenta & Comi
      Campos = Campos & Coma & "NU_MES_SANF = " & Mes
      Campos = Campos & Coma & "VL_SAIN_SANF = " & SCero
      Campos = Campos & Coma & "VL_CRED_SANF = " & IIf(Naturaleza = "C", Valor, 0)
      Campos = Campos & Coma & "VL_DEBI_SANF = " & IIf(Naturaleza = "D", Valor, 0)
      Result = DoInsertSQL("SALDOS_NIIF", Campos)
   Else
      If Naturaleza = "D" Then
         Campos = "VL_DEBI_SANF = VL_DEBI_SANF + " & Valor
      Else
         Campos = "VL_CRED_SANF = VL_CRED_SANF + " & Valor
      End If
      Result = DoUpdate("SALDOS_NIIF", Campos, Condicion)
   End If
End Sub


'Actualiza el valor debito, credito de la cuenta de un comprobante
'---------------------------------------------------------------------------------------
' Procedure : Cuenta_Comprobante_NIIF
' DateTime  : 09/01/2015 11:46
' Author    : jairo_parra
' Purpose   : JLPB T25784-R25328
'---------------------------------------------------------------------------------------
'
Sub Cuenta_Comprobante_NIIF(Cuenta As String, comprobante As String, Mes As Byte, Valor As Double, Naturaleza As String)
   Dim Arr(0)
   Dim ArrCta() As Variant
   
   If Valor <> 0 Then Valor = Format(Valor, "####0.#0")
   ReDim ArrCta(0)
   Result = LoadData("CUENTAS", "CD_CODI_CUEN", "CD_CODI_CUEN=" & Comi & Cuenta & Comi, ArrCta)
   If Result <> FAIL And Not Encontro Then
      Result = FAIL
      Call Mensaje1("La cuenta: " & Cuenta & " no existe en la tabla Cuentas", 1)
      Exit Sub
   End If
   ReDim ArrCta(0)
   Result = LoadData("TC_COMPROBANTE", "CD_CODI_COMP", "CD_CODI_COMP=" & Comi & comprobante & Comi, ArrCta)
   If Result <> FAIL And Not Encontro Then
      Result = FAIL
      Call Mensaje1("El Comprobante: " & comprobante & " no existe en la tabla TC_COMPROBANTE", 1)
      Exit Sub
   End If
   Condicion = "CD_CUEN_RCNF = '" & Cuenta & Comi & " AND CD_COMP_RCNF = '" & comprobante & Comi & " AND NU_MES_RCNF = " & Mes
   Result = LoadData("CUENTA_COMPROB_NIIF", "CD_CUEN_RCNF", Condicion, Arr())
   If Arr(0) = NUL$ Then
      Campos = "CD_CUEN_RCNF = " & Comi & Cuenta & Comi
      Campos = Campos & Coma & "CD_COMP_RCNF = " & Comi & comprobante & Comi
      Campos = Campos & Coma & "NU_MES_RCNF = " & Mes
      Campos = Campos & Coma & "VL_DEBI_RCNF = " & IIf(Naturaleza = "D", Valor, 0)
      Campos = Campos & Coma & "VL_CRED_RCNF = " & IIf(Naturaleza = "C", Valor, 0)
      Result = DoInsertSQL("CUENTA_COMPROB_NIIF", Campos)
   Else
      If Naturaleza = "D" Then
         Campos = "VL_DEBI_RCNF = VL_DEBI_RCNF + " & Valor
      Else
         Campos = "VL_CRED_RCNF = VL_CRED_RCNF + " & Valor
      End If
      Result = DoUpdate("CUENTA_COMPROB_NIIF", Campos, Condicion)
   End If
End Sub


'Actualiza el valor debito, credito, base  de la cuenta de un tercero
'---------------------------------------------------------------------------------------
' Procedure : Cuenta_Tercero_NIIF
' DateTime  : 09/01/2015 11:47
' Author    : jairo_parra
' Purpose   : JLPB T25784-R25328
'---------------------------------------------------------------------------------------
'
Sub Cuenta_Tercero_NIIF(Cuenta As String, Tercero As String, Mes As Byte, Valor As Double, Naturaleza As String, ByVal Base As Double)
   Dim Arr(0)
   Dim ArrCta() As Variant
   
   If Valor <> 0 Then Valor = Format(Valor, "####0.#0")
   If Base <> 0 Then Base = Format(Base, "####0.#0")
   ReDim ArrCta(0)
   Result = LoadData("CUENTAS", "CD_CODI_CUEN", "CD_CODI_CUEN=" & Comi & Cuenta & Comi, ArrCta)
   If Result <> FAIL And Not Encontro Then
      Result = FAIL
      Call Mensaje1("La cuenta: " & Cuenta & " no existe en la tabla Cuentas", 1)
      Exit Sub
   End If
   ReDim ArrCta(0)
   Result = LoadData("TERCERO", "CD_CODI_TERC", "CD_CODI_TERC=" & Comi & Tercero & Comi, ArrCta)
   If Result <> FAIL And Not Encontro Then
      Result = FAIL
      Call Mensaje1("El Tercero: " & Cuenta & " no existe en la tabla TERCERO", 1)
      Exit Sub
   End If
   Condicion = "CD_CUEN_RTNF = '" & Cuenta & Comi & " AND CD_TERC_RTNF = '" & Tercero & Comi & " AND NU_MES_RTNF = " & Mes
   Result = LoadData("CUENTA_TERCERO_NIIF", "CD_CUEN_RTNF", Condicion, Arr())
   If Arr(0) = NUL$ Then
      Campos = "CD_CUEN_RTNF = " & Comi & Cuenta & Comi & Coma
      Campos = Campos & "CD_TERC_RTNF = " & Comi & Tercero & Comi & Coma
      Campos = Campos & "NU_MES_RTNF = " & Mes & Coma
      Campos = Campos & "VL_DEBI_RTNF = " & IIf(Naturaleza = "D", Valor, 0) & Coma
      Campos = Campos & "VL_CRED_RTNF = " & IIf(Naturaleza = "C", Valor, 0) & Coma
      Campos = Campos & "VL_BADB_RTNF = " & IIf(Naturaleza = "D", Base, 0) & Coma
      Campos = Campos & "VL_BACR_RTNF = " & IIf(Naturaleza = "C", Base, 0) & Coma
      Campos = Campos & "VL_CREDIVA_RTNF = 0, "
      Campos = Campos & "VL_DEBIIVA_RTNF = 0, "
      Campos = Campos & "VL_CREDNIVA_RTNF = 0, "
      Campos = Campos & "VL_DEBINIVA_RTNF = 0, "
      Campos = Campos & "VL_CREDBASE_RTNF = 0, "
      Campos = Campos & "VL_DEBIBASE_RTNF = 0, "
      Campos = Campos & "VL_CREDNBASE_RTNF = 0, "
      Campos = Campos & "VL_DEBINBASE_RTNF = 0"
      Result = DoInsertSQL("CUENTA_TERCERO_NIIF", Campos)
   Else
      If Naturaleza = "D" Then
         Campos = "VL_DEBI_RTNF = VL_DEBI_RTNF + " & Valor & Coma
         Campos = Campos & "VL_BADB_RTNF = VL_BADB_RTNF + " & Base
      Else
         Campos = "VL_CRED_RTNF = VL_CRED_RTNF + " & Valor & Coma
         Campos = Campos & "VL_BACR_RTNF = VL_BACR_RTNF + " & Base
      End If
      Result = DoUpdate("CUENTA_TERCERO_NIIF", Campos, Condicion)
   End If
End Sub


'Actualiza el valor debito, credito  de la cuenta de un centro de costo en un determinado mes
'---------------------------------------------------------------------------------------
' Procedure : Cuenta_CentroCosto_NIIF
' DateTime  : 09/01/2015 11:47
' Author    : jairo_parra
' Purpose   : JLPB T25784-R25328
'---------------------------------------------------------------------------------------
'
Sub Cuenta_CentroCosto_NIIF(Cuenta As String, Centro As String, Mes As Byte, Valor As Double, Naturaleza As String)
   Dim Arr(0)
   Dim ArrCta() As Variant
   
   If Valor <> 0 Then Valor = Format(Valor, "####0.#0")
   ReDim ArrCta(0)
   Result = LoadData("CUENTAS", "CD_CODI_CUEN", "CD_CODI_CUEN=" & Comi & Cuenta & Comi, ArrCta)
   If Result <> FAIL And Not Encontro Then
      Result = FAIL
      Call Mensaje1("La cuenta: " & Cuenta & " no existe en la tabla Cuentas", 1)
      Exit Sub
   End If
   ReDim ArrCta(0)
   Result = LoadData("CENTRO_COSTO", "CD_CODI_CECO", "CD_CODI_CECO=" & Comi & Centro & Comi, ArrCta)
   If Result <> FAIL And Not Encontro Then
      Result = FAIL
      Call Mensaje1("El Centro de Costos: " & Cuenta & " no existe en la tabla CENTRO_COSTO", 1)
      Exit Sub
   End If
   Condicion = "CD_CUEN_RCNF = '" & Cuenta & Comi & " AND CD_CECO_RCNF = '" & Centro & Comi & " AND NU_MES_RCNF = " & Mes
   Result = LoadData("CUENTA_CECO_NIIF", "CD_CUEN_RCNF", Condicion, Arr())
   If Arr(0) = NUL$ Then
      Campos = "CD_CUEN_RCNF = " & Comi & Cuenta & Comi & Coma
      Campos = Campos & "CD_CECO_RCNF = " & Comi & Centro & Comi & Coma
      Campos = Campos & "NU_MES_RCNF = " & Mes & Coma
      Campos = Campos & "VL_SAIN_RCNF = " & SCero & Coma
      Campos = Campos & "VL_CRED_RCNF = " & IIf(Naturaleza = "C", Valor, 0) & Coma
      Campos = Campos & "VL_DEBI_RCNF = " & IIf(Naturaleza = "D", Valor, 0)
      Result = DoInsertSQL("CUENTA_CECO_NIIF", Campos)
   Else
      If Naturaleza = "D" Then
         Campos = "VL_DEBI_RCNF = VL_DEBI_RCNF + " & Valor
      Else
         Campos = "VL_CRED_RCNF = VL_CRED_RCNF + " & Valor
      End If
      Result = DoUpdate("CUENTA_CECO_NIIF", Campos, Condicion)
   End If
End Sub


'actualiza el valor credito, debito, base de la cuenta de un tercero y un centro de costo en un determinado mes
'---------------------------------------------------------------------------------------
' Procedure : Cuenta_CentroTercero_NIIF
' DateTime  : 09/01/2015 11:47
' Author    : jairo_parra
' Purpose   : JLPB T25784-R25328
'---------------------------------------------------------------------------------------
'
Sub Cuenta_CentroTercero_NIIF(Cuenta As String, Centro As String, Tercero As String, Mes As Byte, Valor As Double, Naturaleza As String, ByVal Base As Double)
   Dim Arr(0)
   Dim ArrCta() As Variant
   
   If Valor <> 0 Then Valor = Format(Valor, "####0.#0")
   If Base <> 0 Then Base = Format(Base, "####0.#0")
   ReDim ArrCta(0)
   Result = LoadData("CUENTAS", "CD_CODI_CUEN", "CD_CODI_CUEN=" & Comi & Cuenta & Comi, ArrCta)
   If Result <> FAIL And Not Encontro Then
      Result = FAIL
      Call Mensaje1("La cuenta: " & Cuenta & " no existe en la tabla Cuentas", 1)
      Exit Sub
   End If
   ReDim ArrCta(0)
   Result = LoadData("CENTRO_COSTO", "CD_CODI_CECO", "CD_CODI_CECO=" & Comi & Centro & Comi, ArrCta)
   If Result <> FAIL And Not Encontro Then
      Result = FAIL
      Call Mensaje1("El Centro de Costos: " & Cuenta & " no existe en la tabla CENTRO_COSTO", 1)
      Exit Sub
   End If
   ReDim ArrCta(0)
   Result = LoadData("TERCERO", "CD_CODI_TERC", "CD_CODI_TERC=" & Comi & Tercero & Comi, ArrCta)
   If Result <> FAIL And Not Encontro Then
      Result = FAIL
      Call Mensaje1("El Tercero: " & Cuenta & " no existe en la tabla TERCERO", 1)
      Exit Sub
   End If
   Condicion = " CD_CUEN_RTNF = '" & Cuenta & Comi & " AND CD_CECO_RTNF = '" & Centro & Comi
   Condicion = Condicion & " AND CD_TERC_RTNF = '" & Tercero & Comi & " AND NU_MES_RTNF = " & Mes
   Result = LoadData("CUENTA_CETE_NIIF", "CD_CUEN_RTNF", Condicion, Arr())
   If Arr(0) = NUL$ Then
      Campos = "CD_CUEN_RTNF = " & Comi & Cuenta & Comi & Coma
      Campos = Campos & "CD_CECO_RTNF = " & Comi & Centro & Comi & Coma
      Campos = Campos & "CD_TERC_RTNF = " & Comi & Tercero & Comi & Coma
      Campos = Campos & "NU_MES_RTNF = " & Mes & Coma
      Campos = Campos & "VL_DEBI_RTNF = " & IIf(Naturaleza = "D", Valor, 0) & Coma
      Campos = Campos & "VL_CRED_RTNF = " & IIf(Naturaleza = "C", Valor, 0) & Coma
      Campos = Campos & "VL_BADB_RTNF = " & IIf(Naturaleza = "D", Base, 0) & Coma
      Campos = Campos & "VL_BACR_RTNF = " & IIf(Naturaleza = "C", Base, 0)
      Result = DoInsertSQL("CUENTA_CETE_NIIF", Campos)
   Else
      If Naturaleza = "D" Then
         Campos = "VL_DEBI_RTNF = VL_DEBI_RTNF + " & Valor & Coma
         Campos = Campos & "VL_BADB_RTNF = VL_BADB_RTNF + " & Base
      Else
         Campos = "VL_CRED_RTNF = VL_CRED_RTNF + " & Valor & Coma
         Campos = Campos & "VL_BACR_RTNF = VL_BACR_RTNF + " & Base
      End If
      Result = DoUpdate("CUENTA_CETE_NIIF", Campos, Condicion)
   End If
End Sub


'funciones para cargar los saldos iniciales
'Busca los niveles de una cuenta para actualizar el valor saldo de cada una de ellas
'---------------------------------------------------------------------------------------
' Procedure : Saldo_Cuenta_NIIF
' DateTime  : 09/01/2015 11:47
' Author    : jairo_parra
' Purpose   : JLPB T25784-R25328
'---------------------------------------------------------------------------------------
'
Function Saldo_Cuenta_NIIF(ByVal Cuenta As String, Valor As Double) As Integer
   Dim I As Byte

   I = Nivel_Cuenta_NIIF(Cuenta)
   Do While I > 0 And Cuenta <> NUL$
      If BoTipoEmpre Then
         Select Case I
            Case 1: Cuenta = Mid(Cuenta, 1, 1)
            Case 2: Cuenta = Mid(Cuenta, 1, 2)
            Case 3: Cuenta = Mid(Cuenta, 1, 4)
            Case 4: Cuenta = Mid(Cuenta, 1, 6)
            Case 5: Cuenta = Mid(Cuenta, 1, 8)
            Case 6: Cuenta = Mid(Cuenta, 1, 10)
            Case 7: Cuenta = Mid(Cuenta, 1, 12)
         End Select
      Else
         Select Case I
            Case 1: Cuenta = Mid(Cuenta, 1, 1)
            Case 2: Cuenta = Mid(Cuenta, 1, 2)
            Case 3: Cuenta = Mid(Cuenta, 1, 4)
            Case 4: Cuenta = Mid(Cuenta, 1, 6)
            Case 5: Cuenta = Mid(Cuenta, 1, 9)
            Case 6: Cuenta = Mid(Cuenta, 1, 12)
            Case 7: Cuenta = Mid(Cuenta, 1, 15)
         End Select
      End If
      Result = Actualiza_Saldo_Cuenta_NIIF(Cuenta, Valor)
      If Result = FAIL Then Exit Do
      I = I - 1
   Loop
   Saldo_Cuenta_NIIF = Result
End Function


'Actualiza el valor de saldo de una cuenta.
'---------------------------------------------------------------------------------------
' Procedure : Actualiza_Saldo_Cuenta_NIIF
' DateTime  : 09/01/2015 11:47
' Author    : jairo_parra
' Purpose   : JLPB T25784-R25328
'---------------------------------------------------------------------------------------
'
Function Actualiza_Saldo_Cuenta_NIIF(ByVal Cuenta As String, Valor As Double) As Integer
   Condicion = "CD_CODI_CUEN = '" & Cuenta & Comi
   Campos = "VL_SNIIF_CUEN = VL_SNIIF_CUEN + " & Valor
   Result = DoUpdate("CUENTAS", Campos, Condicion)
   Actualiza_Saldo_Cuenta_NIIF = Result
End Function

'Nivel de Cuentas
Function Nivel_Cuenta_NIIF(Cuenta As String) As Byte
   If BoTipoEmpre Then
      Select Case Len(Cuenta)
         Case 1: Nivel_Cuenta_NIIF = 1
         Case 2: Nivel_Cuenta_NIIF = 2
         Case 3 To 4: Nivel_Cuenta_NIIF = 3
         Case 5 To 6: Nivel_Cuenta_NIIF = 4
         Case 7 To 8: Nivel_Cuenta_NIIF = 5
         Case 9 To 10: Nivel_Cuenta_NIIF = 6
         Case 11 To 12: Nivel_Cuenta_NIIF = 7
      End Select
   Else
      Select Case Len(Cuenta)
         Case 1: Nivel_Cuenta_NIIF = 1
         Case 2: Nivel_Cuenta_NIIF = 2
         Case 3 To 4: Nivel_Cuenta_NIIF = 3
         Case 5 To 6: Nivel_Cuenta_NIIF = 4
         Case 7 To 9: Nivel_Cuenta_NIIF = 5
         Case 10 To 12: Nivel_Cuenta_NIIF = 6
         Case 13 To 15: Nivel_Cuenta_NIIF = 7
      End Select
   End If
End Function


'Elimina el comprobante contable de la cxp
'comprobante: Numero de comprobante contable del concepto de la cxp
'numero: Numero de cxp
'---------------------------------------------------------------------------------------
' Procedure : Eliminar_Comprobante_NIIF
' DateTime  : 09/01/2015 11:47
' Author    : jairo_parra
' Purpose   : JLPB T25784-R25328
'---------------------------------------------------------------------------------------
'
Function Eliminar_Comprobante_NIIF(comprobante As String, Numero As Long) As Integer
   Dim I As Long
   ReDim Datos(14)
   Dim Rstsel As ADODB.Recordset
   
   Msglin "Anulando Comprobante"
   Condicion = " NU_COMP_MVNF='" & comprobante & Comi
   Condicion = Condicion & " AND NU_CONS_MVNF = " & Numero
   Condicion = Condicion & " ORDER BY NU_IMPU_MOVI "
   Set Rstsel = New ADODB.Recordset
   Campos = " CD_CENT_MVNF,CD_CUEN_MVNF,CD_TERC_MVNF,FE_FECH_MVNF,"
   Campos = Campos & "NU_COMP_MVNF,NU_CONS_MVNF,NU_IMPU_MVNF,DE_DESC_MVNF,"
   Campos = Campos & "VL_VALO_MVNF,ID_NATU_MVNF,CD_CECO_MVNF,NU_CHEQ_MVNF,"
   Campos = Campos & "VL_REAL_MVNF,NU_MES_MVNF,NU_TIPMOV_MVNF,NU_MOV_MVNF"
   Call SelectRST("MOVIMIENTOS_NIIF", Campos, Condicion, Rstsel)
   If Result <> FAIL Then
      With Rstsel
         If Not .EOF Then
            .MoveLast
            I = .Fields(6)
            .MoveFirst
            Do While Not .EOF
               Datos(0) = .Fields(1): Datos(1) = .Fields(2)
               Datos(2) = .Fields(3): Datos(3) = .Fields(4)
               Datos(4) = .Fields(5): Datos(5) = I + 1
               Datos(6) = "ANULACION DOCUMENTO TESORERIA"
               Datos(7) = .Fields(8)
               Datos(8) = IIf(.Fields(9) = "D", "C", "D")
               Datos(9) = .Fields(10)
               Datos(10) = .Fields(11): Datos(11) = .Fields(12)
               Datos(12) = Format(.Fields(3), "mm")
               Datos(13) = .Fields(13)
               Datos(14) = .Fields(14)
               .MoveNext
               I = I + 1
               Call Crear_Movimiento_Contable_NIIF(Datos())
               If Result = FAIL Then Exit Do
            Loop
         End If
      End With
   End If
   Eliminar_Comprobante_NIIF = Result
   Msglin ""
End Function


'---------------------------------------------------------------------------------------
' Procedure : Eliminar_Comprobante_New_NIIF
' DateTime  : 09/01/2015 11:47
' Author    : jairo_parra
' Purpose   : JLPB T25784-R25328
'---------------------------------------------------------------------------------------
'
'Function Eliminar_Comprobante_New_NIIF(comprobante As String, Numero As Long, Optional StCompro As String) As Integer 'JLPB T26186 SE DEJA EN COMENTARIO
Function Eliminar_Comprobante_New_NIIF(comprobante As String, Numero As Long, Optional StCompro As String, Optional StCodMod As String = NUL$, Optional StNumMov As String = NUL$) As Integer 'JLPB T26186
   Dim I As Long
   Dim RegCont(18)
   ReDim Datos(18, 0)
   
   Msglin "Eliminando Comprobante"
   Condicion = " NU_COMP_MVNF='" & comprobante & Comi
   Condicion = Condicion & " AND NU_CONS_MVNF = " & Numero
   Condicion = Condicion & " ORDER BY NU_IMPU_MVNF "
   Campos = " CD_CENT_MVNF,CD_CUEN_MVNF,CD_TERC_MVNF,FE_FECH_MVNF,"
   Campos = Campos & "NU_COMP_MVNF,NU_CONS_MVNF,NU_IMPU_MVNF,DE_DESC_MVNF,"
   Campos = Campos & "VL_VALO_MVNF,ID_NATU_MVNF,CD_CECO_MVNF,NU_CHEQ_MVNF,"
   Campos = Campos & "VL_REAL_MVNF,NU_MES_MVNF,TX_IVADED_MVNF,TX_IVA_MVNF,"
   Campos = Campos & "VL_REAL_MVNF,NU_TIPMOV_MVNF,NU_MOV_MVNF"
   Result = LoadMulData("MOVIMIENTOS_NIIF", Campos, Condicion, Datos())
   If Result <> FAIL Then
      If Encontro Then
         For I = 0 To UBound(Datos(), 2)
            'ACUMULAR SALDOS
            If Datos(8, I) = NUL$ Then Datos(8, I) = 0
            If Datos(12, I) = NUL$ Then Datos(12, I) = 0
            Call Saldos_NIIF(CStr(Datos(1, I)), CByte(Datos(13, I)), CDbl(Datos(8, I)) * -1, CStr(Datos(9, I)))
            'CUENTA - COMPROBANTE
            Campos = Mid(Datos(1, I), 1, 4) 'NIVEL 3
            If (Result <> FAIL) Then Call Cuenta_Comprobante_NIIF(Campos, CStr(Datos(4, I)), CByte(Datos(13, I)), CDbl(Datos(8, I)) * -1, CStr(Datos(9, I)))
            Campos = Mid(Datos(1, I), 1, 6) 'NIVEL 4
            If (Result <> FAIL) Then Call Cuenta_Comprobante_NIIF(Campos, CStr(Datos(4, I)), CByte(Datos(13, I)), CDbl(Datos(8, I)) * -1, CStr(Datos(9, I)))
            'CUENTA - TERCERO
            If (Result <> FAIL) Then If Datos(2, I) <> NUL$ Then Call Cuenta_Tercero_NIIF(CStr(Datos(1, I)), CStr(Datos(2, I)), CByte(Datos(13, I)), CDbl(Datos(8, I)) * -1, CStr(Datos(9, I)), CDbl(Datos(12, I)) * -1)
            'CUENTA - CENTRO DE COSTO
            If (Result <> FAIL) Then If Datos(10, I) <> NUL$ Then Call Cuenta_CentroCosto_NIIF(CStr(Datos(1, I)), CStr(Datos(10, I)), CByte(Datos(13, I)), CDbl(Datos(8, I)) * -1, CStr(Datos(9, I)))
            'CUENTA - CENTRO DE COSTO - TERCERO
            If (Result <> FAIL) Then
               If Datos(2, I) <> NUL$ And Datos(10, I) <> NUL$ Then Call Cuenta_CentroTercero_NIIF(CStr(Datos(1, I)), CStr(Datos(10, I)), CStr(Datos(2, I)), CByte(Datos(13, I)), CDbl(Datos(8, I)) * -1, CStr(Datos(9, I)), CDbl(Datos(12, I)) * -1)
            End If
            If Result = FAIL Then Exit For
         Next
         If Result <> FAIL Then
            Condicion = " NU_COMP_MVNF='" & comprobante & Comi
            Condicion = Condicion & " AND NU_CONS_MVNF = " & Numero
            Result = DoDelete("MOVIMIENTOS_NIIF", Condicion)
            For I = 1 To 18
               If I <= 13 Then
                  RegCont(I - 1) = Datos(I, 0)
               ElseIf I >= 17 Then
                  RegCont(I) = Datos(I, 0)
               End If
            Next
            'JLPB T26186 INICIO
            If StCodMod <> NUL$ Then RegCont(17) = StCodMod
            If StNumMov <> NUL$ Then RegCont(18) = StNumMov
            'JLPB T26186 FIN
            RegCont(14) = "2"
            RegCont(15) = "0"
            RegCont(16) = "0"
            RegCont(6) = "COMPROBANTE ANULADO"
            RegCont(7) = 0
            If Result <> FAIL Then Result = Crear_Movimiento_Contable_NIIF(RegCont())
            'Actualizar tabla MOV_CONTABLE_NIIF
            If StCompro = NUL$ Then GoTo Siguiente
            Condicion = "CD_CONC_MCNF = " & Comi & StCompro & Comi
            Condicion = Condicion & " AND NU_CONS_MCNF = " & Numero
            If Result <> FAIL Then Result = DoDelete("MOV_CONTABLE_NIIF", Condicion)
            Valores = "CD_CONC_MCNF = " & Comi & StCompro & Comi & Coma
            Valores = Valores & "NU_CONS_MCNF = " & Numero & Coma
            Valores = Valores & "NU_LINE_MCNF = 1" & Coma
            Valores = Valores & "CD_CUEN_MCNF = " & Comi & RegCont(0) & Comi & Coma
            Valores = Valores & "VL_VALO_MCNF = " & ConDoble(RegCont(7)) & Coma
            Valores = Valores & "ID_NATU_MCNF = " & Comi & RegCont(8) & Comi & Coma
            Valores = Valores & "CD_TERC_MCNF = " & Comi & RegCont(1) & Comi & Coma
            Valores = Valores & "CD_CECO_MCNF = " & Comi & RegCont(9) & Comi & Coma
            Valores = Valores & "VL_BASE_MCNF = 0"
            If Result <> FAIL Then Result = DoInsertSQL("MOV_CONTABLE_NIIF", Valores)
Siguiente:
         End If
      End If
   End If
   Eliminar_Comprobante_New_NIIF = Result
   Msglin ""
End Function



