VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Begin VB.Form Frmarbol 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Lista Jerarquica de Opciones"
   ClientHeight    =   5400
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   5835
   Icon            =   "Frmarbol.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   5400
   ScaleWidth      =   5835
   Begin VB.Frame Frame2 
      Height          =   735
      Left            =   120
      TabIndex        =   1
      Top             =   4560
      Width           =   5535
      Begin VB.CommandButton CmdArbol 
         Caption         =   "&ACEPTAR"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   6
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   495
         Left            =   2040
         Picture         =   "Frmarbol.frx":058A
         Style           =   1  'Graphical
         TabIndex        =   2
         Top             =   160
         Width           =   1215
      End
      Begin MSComctlLib.ImageList image_arbol 
         Index           =   1
         Left            =   4440
         Top             =   120
         _ExtentX        =   1005
         _ExtentY        =   1005
         BackColor       =   -2147483643
         ImageWidth      =   16
         ImageHeight     =   16
         MaskColor       =   12632256
         _Version        =   393216
         BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
            NumListImages   =   5
            BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
               Picture         =   "Frmarbol.frx":089C
               Key             =   ""
            EndProperty
            BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
               Picture         =   "Frmarbol.frx":09AE
               Key             =   ""
            EndProperty
            BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
               Picture         =   "Frmarbol.frx":0AC0
               Key             =   ""
            EndProperty
            BeginProperty ListImage4 {2C247F27-8591-11D1-B16A-00C0F0283628} 
               Picture         =   "Frmarbol.frx":0BD2
               Key             =   ""
            EndProperty
            BeginProperty ListImage5 {2C247F27-8591-11D1-B16A-00C0F0283628} 
               Picture         =   "Frmarbol.frx":0CE4
               Key             =   ""
            EndProperty
         EndProperty
      End
   End
   Begin VB.Frame Frame1 
      Caption         =   "Arbol de Opciones"
      Height          =   4455
      Left            =   120
      TabIndex        =   0
      Top             =   120
      Width           =   5535
      Begin MSComctlLib.TreeView TreeOpc 
         Height          =   4095
         Left            =   120
         TabIndex        =   3
         Top             =   240
         Width           =   5295
         _ExtentX        =   9340
         _ExtentY        =   7223
         _Version        =   393217
         Indentation     =   706
         Style           =   7
         ImageList       =   "image_arbol(1)"
         Appearance      =   1
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
   End
End
Attribute VB_Name = "Frmarbol"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub CmdArbol_Click()
    Unload Me
End Sub

Private Sub Form_Load()
    Call CenterForm(MDI_Inventarios, Me)
    Campos = "TX_CODI_OPCI,TX_DESC_OPCI,TX_CODPADR_OPCI"
    'Call Carga_Arbol("OPCION", Campos, "TX_CODI_OPCI<>'0' ORDER BY TX_CODI_OPCI", TreeOpc)
    Call Carga_Arbol("OPCION", Campos, "TX_CODPADR_OPCI='CNT' ORDER BY TX_CODI_OPCI ", TreeOpc) 'AASV M4119 SOLO OPCIONES PADRE
End Sub

'carga arbol para el Treeview control desde la tabla opciones
Function Carga_Arbol(ByVal TableName As String, ByVal Whatsel As String, Condi As String, TreeOpc As TreeView)
    Dim nodoArbol As Node
   ' Dim Origen As String AASV M4119 DEPURACION DE CODIGO
    Dim I As Integer
'    Dim J As Integer       'DEPURACION DE CODIGO
'    Dim N As Integer       'DEPURACION DE CODIGO
'    Dim l As Integer       'DEPURACION DE CODIGO
    Dim ind As Integer
    Dim S, CodiOpc, NombOpc, PadrOpc As String
    ind = 0
    If (DoSelect(TableName, Whatsel, Condi) <> FAIL) Then
        If (ResultQuery()) Then
         Call MouseClock
         Do While (NextRowQuery())
            'On Error Resume Next
            If CancelTrans = 1 Then Exit Do
            DoEvents
            S = NUL$
            I = I + 1
            'Se ha llegado al m�ximo soportado por Visual Basic para un grid?
            
            'creaci�n nodo raiz principal nodo padre
            If ind = 0 Then
                Set nodoArbol = TreeOpc.Nodes.Add(, , "CNT", UCase(App.Title), 1, 1)
                TreeOpc.Nodes(TreeOpc.Nodes.Count).EnsureVisible
                ind = 1
            End If
            CodiOpc = DataQuery(1)
            NombOpc = DataQuery(2)
            PadrOpc = Comi & DataQuery(3) & Comi
           
            'creaci�n nodo segundario nodo hijo 2 nivel
            If PadrOpc = "'CNT'" Then
                Set nodoArbol = TreeOpc.Nodes.Add("CNT", 4, Comi & CodiOpc & Comi, NombOpc, 2, 3)
                TreeOpc.Nodes(TreeOpc.Nodes.Count).EnsureVisible
                TreeOpc.Style = tvwTreelinesPlusMinusPictureText     ' Estilo 7.
                TreeOpc.LineStyle = tvwRootLines  ' Estilo 1
            'creacion nodo terciario hacia adelante,  nodos nietos o nivel 3 hacia adelante
            'Else 'AASV M4119 SE COLOCA EN COMENTARIO SE CAMBIA POR LA FUNCION Cargar_Nodo_Nieto
                   'Origen = PadrOpc
                   'Set nodoArbol = TreeOpc.Nodes.Add(Origen, 4, Comi & CodiOpc & Comi, NombOpc, 4, 5)
                   ''TreeOpc.Nodes(TreeOpc.Nodes.Count).EnsureVisible
                   'TreeOpc.Style = tvwTreelinesPlusMinusPictureText     ' Estilo 7.
                   'TreeOpc.LineStyle = tvwRootLines  ' Estilo 1
            End If
         Loop
         'AASV M4119
         If ind = 1 Then
            Condi = " TX_CODI_OPCI<>'0' AND TX_CODPADR_OPCI<>'CNT'"
            Condi = Condi & " ORDER BY TX_CODI_OPCI"
            Call Cargar_Nodo_Nieto(TableName, Whatsel, Condi, TreeOpc)
         End If
         'AASV M4119
         Call MouseNorm
         On Error Resume Next
        End If
      Result = SUCCEED
   Else
      Result = FAIL
   End If
End Function
'---------------------------------------------------------------------------------------
' Procedure : Cargar_Nodo_Nieto
' DateTime  : 02/12/2008 10:32
' Author    : albert_silva
' Purpose   : M4119 CARGA LOS NODOS NIETO
'---------------------------------------------------------------------------------------
'
Function Cargar_Nodo_Nieto(ByVal TableName As String, ByVal Whatsel As String, Condi As String, TreeOpc As TreeView) As Boolean
Dim InPos As Integer
Dim CodiOpc As String
Dim NombOpc As String
Dim PadrOpc As String
Dim nodoArbol As Node

ReDim Arr(2, 0)
   
   Result = LoadMulData(TableName, Whatsel, Condi, Arr)
   If Result <> FAIL And Encontro Then
      For InPos = 0 To UBound(Arr, 2)
         CodiOpc = Arr(0, InPos)
         NombOpc = Arr(1, InPos)
         PadrOpc = Comi & Arr(2, InPos) & Comi
         Set nodoArbol = TreeOpc.Nodes.Add(PadrOpc, 4, Comi & CodiOpc & Comi, NombOpc, 4, 5)
         TreeOpc.Style = tvwTreelinesPlusMinusPictureText     ' Estilo 7.
         TreeOpc.LineStyle = tvwRootLines  ' Estilo 1
      Next
   End If
End Function
