VERSION 5.00
Begin VB.Form FrmAcceso 
   BorderStyle     =   4  'Fixed ToolWindow
   Caption         =   "Confirmación de acceso"
   ClientHeight    =   1380
   ClientLeft      =   45
   ClientTop       =   285
   ClientWidth     =   3375
   Icon            =   "FrmAcceso.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   1380
   ScaleWidth      =   3375
   ShowInTaskbar   =   0   'False
   StartUpPosition =   1  'CenterOwner
   Begin VB.CommandButton Cmd_Cancelar 
      Caption         =   "CANCELAR"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   6
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   495
      Left            =   1800
      Picture         =   "FrmAcceso.frx":000C
      Style           =   1  'Graphical
      TabIndex        =   3
      Top             =   720
      Width           =   1215
   End
   Begin VB.CommandButton Cmd_Aceptar 
      Caption         =   "ACEPTAR"
      Default         =   -1  'True
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   6
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   495
      Left            =   240
      Picture         =   "FrmAcceso.frx":034E
      Style           =   1  'Graphical
      TabIndex        =   2
      Top             =   720
      Width           =   1215
   End
   Begin VB.TextBox TxtClave 
      Height          =   285
      IMEMode         =   3  'DISABLE
      Left            =   720
      PasswordChar    =   "*"
      TabIndex        =   1
      Top             =   240
      Width           =   2535
   End
   Begin VB.Label Ca 
      Caption         =   "Clave"
      Height          =   255
      Left            =   120
      TabIndex        =   0
      Top             =   240
      Width           =   495
   End
End
Attribute VB_Name = "FrmAcceso"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub Cmd_Aceptar_Click()
If TxtClave <> NUL$ Then
   Codigo = TxtClave
   Unload Me
Else
   Call Mensaje1("La clave esta errada", 1)
End If
End Sub

Private Sub Cmd_Cancelar_Click()
Unload Me: Codigo = NUL$
End Sub
