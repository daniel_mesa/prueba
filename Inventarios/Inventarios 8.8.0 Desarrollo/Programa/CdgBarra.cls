VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CdgBarra"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit
    
'Private num As Long 'Autonum�rico del c�digo de barra
Private vCodigo As String * 13 'C�digo de barra
Private vAutoArti As Long 'Autonum�rico del art�culo
Private vRegistro As String 'Registro sanitario
Private vCial As String 'Nombre comercial
Private vLabora As String 'Nombre laboratorio
'Public Property Let Numero(cnt As Long)
'    num = cnt
'End Property
'
'Public Property Get Numero() As Long
'    Numero = num
'End Property
''IN_CODIGOBAR(NU_AUTO_ARTI_COBA,TX_COBA_COBA,TX_REGI_COBA,TX_CIAL_COBA,TX_CIAL_COBA,TX_LABO_COBA)

Public Property Let Codigo(rda As String)
    vCodigo = rda
End Property

Public Property Get Codigo() As String
    Codigo = vCodigo
End Property

Public Property Let AutoArticulo(num As Long)
    vAutoArti = num
End Property

Public Property Get AutoArticulo() As Long
    AutoArticulo = vAutoArti
End Property

Public Property Let Registro(Cue As String)
    vRegistro = Cue
End Property

Public Property Get Registro() As String
    Registro = vRegistro
End Property

Public Property Let Comercial(Cue As String)
    vCial = Cue
End Property

Public Property Get Comercial() As String
    Comercial = vCial
End Property

Public Property Let Laboratorio(Cue As String)
    vLabora = Cue
End Property

Public Property Get Laboratorio() As String
    Laboratorio = vLabora
End Property

Public Function IniciaXCodigo(Cue As String) As ResultConsulta
    Dim ArrCBD() As Variant
    Dim Dnd As String, Cmp As String, Cdc As String
    vCodigo = Cue
'IN_CODIGOBAR(NU_AUTO_ARTI_COBA,TX_COBA_COBA,TX_REGI_COBA,TX_CIAL_COBA,TX_LABO_COBA)
    Dnd = "IN_CODIGOBAR"
    Cmp = "NU_AUTO_ARTI_COBA,TX_COBA_COBA,TX_REGI_COBA,TX_CIAL_COBA,TX_LABO_COBA"
    Cdc = "TX_COBA_COBA=" & Comi & Cue & Comi
    ReDim ArrCBD(4)
    Result = LoadData(Dnd, Cmp, Cdc, ArrCBD())
    If Result = FAIL Then GoTo FALLO
    If Not encontro Then GoTo NOENC
    vCodigo = CStr(ArrCBD(1))
    vAutoArti = CStr(ArrCBD(0))
    vRegistro = CStr(ArrCBD(2))
    vCial = CStr(ArrCBD(3))
    vLabora = CStr(ArrCBD(4))
    IniciaXCodigo = Encontroinfo
    Exit Function
FALLO:
   IniciaXCodigo = Errorinfo
   Exit Function
NOENC:
    IniciaXCodigo = NoEncontroinfo
   Exit Function
End Function

