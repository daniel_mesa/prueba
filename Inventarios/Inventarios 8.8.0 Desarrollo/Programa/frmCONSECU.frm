VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "Mscomctl.ocx"
Begin VB.Form frmCONSECU 
   Caption         =   "Consecutivos"
   ClientHeight    =   5940
   ClientLeft      =   60
   ClientTop       =   450
   ClientWidth     =   10725
   LinkTopic       =   "Form1"
   ScaleHeight     =   5940
   ScaleWidth      =   10725
   StartUpPosition =   3  'Windows Default
   Begin MSComctlLib.Toolbar tlbACCIONES 
      Align           =   1  'Align Top
      Height          =   630
      Left            =   0
      TabIndex        =   12
      Top             =   0
      Width           =   10725
      _ExtentX        =   18918
      _ExtentY        =   1111
      ButtonWidth     =   1535
      ButtonHeight    =   953
      Appearance      =   1
      _Version        =   393216
      BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
         NumButtons      =   4
         BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Enabled         =   0   'False
            Caption         =   "&Adicionar"
            Key             =   "ADD"
            Object.ToolTipText     =   "Adicionar"
         EndProperty
         BeginProperty Button2 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Enabled         =   0   'False
            Caption         =   "&Borrar"
            Key             =   "DEL"
         EndProperty
         BeginProperty Button3 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Enabled         =   0   'False
            Caption         =   "&Modificar"
            Key             =   "CHA"
         EndProperty
         BeginProperty Button4 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Enabled         =   0   'False
            Caption         =   "&Cancelar"
            Key             =   "CAN"
         EndProperty
      EndProperty
      BorderStyle     =   1
   End
   Begin VB.TextBox txtNumActual 
      Height          =   315
      Left            =   6960
      TabIndex        =   5
      Text            =   "txtNumActual"
      Top             =   4680
      Width           =   1215
   End
   Begin VB.TextBox txtPrefijo 
      Height          =   315
      Left            =   6960
      TabIndex        =   4
      Text            =   "txtPrefijo"
      Top             =   4080
      Width           =   855
   End
   Begin VB.ComboBox cboDocum 
      Height          =   315
      Left            =   6960
      TabIndex        =   3
      Text            =   "Documentos"
      Top             =   3480
      Width           =   3255
   End
   Begin VB.TextBox txtCodigo 
      Height          =   315
      Left            =   6960
      TabIndex        =   2
      Text            =   "txtCodigo"
      Top             =   2880
      Width           =   1935
   End
   Begin VB.TextBox txtNombre 
      Height          =   855
      Left            =   6960
      TabIndex        =   1
      Text            =   "txtNombre"
      Top             =   1680
      Width           =   3255
   End
   Begin MSComctlLib.TreeView arbConsec 
      Height          =   4335
      Left            =   480
      TabIndex        =   0
      Top             =   1200
      Width           =   4455
      _ExtentX        =   7858
      _ExtentY        =   7646
      _Version        =   393217
      Indentation     =   529
      LabelEdit       =   1
      LineStyle       =   1
      Style           =   7
      Appearance      =   1
      Enabled         =   0   'False
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin VB.Frame Frame1 
      Caption         =   "Informaci�on del Consecutivo"
      Height          =   4335
      Left            =   5280
      TabIndex        =   6
      Top             =   1200
      Width           =   5175
      Begin VB.Label Label2 
         Alignment       =   1  'Right Justify
         Caption         =   "C�digo:"
         Height          =   255
         Left            =   120
         TabIndex        =   11
         Top             =   1680
         Width           =   1500
      End
      Begin VB.Label Label3 
         Alignment       =   1  'Right Justify
         Caption         =   "Tipo de Documento:"
         Height          =   255
         Left            =   120
         TabIndex        =   10
         Top             =   2280
         Width           =   1500
      End
      Begin VB.Label Label4 
         Alignment       =   1  'Right Justify
         Caption         =   "Prefijo del Consec.:"
         Height          =   255
         Left            =   120
         TabIndex        =   9
         Top             =   2880
         Width           =   1500
      End
      Begin VB.Label Label5 
         Alignment       =   1  'Right Justify
         Caption         =   "N�mero Actual:"
         Height          =   255
         Left            =   120
         TabIndex        =   8
         Top             =   3480
         Width           =   1500
      End
      Begin VB.Label Label1 
         Alignment       =   1  'Right Justify
         Caption         =   "Nombre:"
         Height          =   255
         Left            =   120
         TabIndex        =   7
         Top             =   480
         Width           =   1500
      End
   End
End
Attribute VB_Name = "frmCONSECU"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Dim CllDeConsec As New Collection
Public OpcCod        As String   'Opci�n de seguridad

Private Sub arbConsec_Collapse(ByVal Punto As MSComctlLib.Node)
    If Punto.Key = "CNSC" Then
        Me.txtCodigo.Text = ""
        Me.txtNombre.Text = ""
        Me.txtNumActual.Text = CStr(0)
        Me.txtPrefijo.Text = ""
        Me.cboDocum.ListIndex = -1
        If Me.tlbACCIONES.Buttons("DEL").Enabled Then Me.tlbACCIONES.Buttons("DEL").Enabled = False
        If Me.tlbACCIONES.Buttons("CHA").Enabled Then Me.tlbACCIONES.Buttons("CHA").Enabled = False
        If Me.tlbACCIONES.Buttons("CAN").Enabled Then Me.tlbACCIONES.Buttons("CAN").Enabled = False
        If Me.txtCodigo.Enabled Then Me.txtCodigo.Enabled = False
        If Me.txtNombre.Enabled Then Me.txtNombre.Enabled = False
        If Me.txtNumActual.Enabled Then Me.txtNumActual.Enabled = False
        If Me.txtPrefijo.Enabled Then Me.txtPrefijo.Enabled = False
        If Me.cboDocum.Enabled Then Me.cboDocum.Enabled = False
    End If
End Sub

Private Sub arbConsec_NodeClick(ByVal Punto As MSComctlLib.Node)
    Dim UnConsec As ElConsec, NuMr As Integer, UnNodo As Node
    For Each UnNodo In Me.arbConsec.Nodes
        If UnNodo.Bold Then UnNodo.Bold = False
    Next
    Me.arbConsec.SelectedItem.Bold = True
    If Me.tlbACCIONES.Buttons("DEL").Enabled Then Me.tlbACCIONES.Buttons("DEL").Enabled = False
    If Me.tlbACCIONES.Buttons("CHA").Enabled Then Me.tlbACCIONES.Buttons("CHA").Enabled = False
    If Me.tlbACCIONES.Buttons("CAN").Enabled Then Me.tlbACCIONES.Buttons("CAN").Enabled = False
    If Punto.Key = "CNSC" Then
        Me.txtCodigo.Text = ""
        Me.txtNombre.Text = ""
        Me.txtNumActual.Text = CStr(0)
        Me.txtPrefijo.Text = ""
        Me.cboDocum.ListIndex = -1
        If Not Me.tlbACCIONES.Buttons("ADD").Enabled Then Me.tlbACCIONES.Buttons("ADD").Enabled = True
        If Me.tlbACCIONES.Buttons("DEL").Enabled Then Me.tlbACCIONES.Buttons("DEL").Enabled = False
        If Me.tlbACCIONES.Buttons("CHA").Enabled Then Me.tlbACCIONES.Buttons("CHA").Enabled = False
        If Me.tlbACCIONES.Buttons("CAN").Enabled Then Me.tlbACCIONES.Buttons("CAN").Enabled = False
        If Me.txtCodigo.Enabled Then Me.txtCodigo.Enabled = False
        If Me.txtNombre.Enabled Then Me.txtNombre.Enabled = False
        If Me.txtNumActual.Enabled Then Me.txtNumActual.Enabled = False
        If Me.txtPrefijo.Enabled Then Me.txtPrefijo.Enabled = False
        If Me.cboDocum.Enabled Then Me.cboDocum.Enabled = False
        Exit Sub
    End If
    If Me.tlbACCIONES.Buttons("ADD").Enabled Then Me.tlbACCIONES.Buttons("ADD").Enabled = False
    If Not Me.tlbACCIONES.Buttons("DEL").Enabled Then Me.tlbACCIONES.Buttons("DEL").Enabled = True
    If Not Me.tlbACCIONES.Buttons("CHA").Enabled Then Me.tlbACCIONES.Buttons("CHA").Enabled = True
    If Not Me.txtCodigo.Enabled Then Me.txtCodigo.Enabled = True
    If Not Me.txtNombre.Enabled Then Me.txtNombre.Enabled = True
    If Not Me.txtNumActual.Enabled Then Me.txtNumActual.Enabled = True
    If Not Me.txtPrefijo.Enabled Then Me.txtPrefijo.Enabled = True
    If Not Me.cboDocum.Enabled Then Me.cboDocum.Enabled = True
    If Me.txtCodigo.Enabled Then Me.txtCodigo.Enabled = False
    If Me.txtNombre.Enabled Then Me.txtNombre.Enabled = False
    If Me.txtNumActual.Enabled Then Me.txtNumActual.Enabled = False
    If Me.txtPrefijo.Enabled Then Me.txtPrefijo.Enabled = False
    If Me.cboDocum.Enabled Then Me.cboDocum.Enabled = False
    Set UnConsec = CllDeConsec.Item(Punto.Key)
    Me.txtCodigo = Trim(UnConsec.Codigo)
    Me.txtNombre = Trim(UnConsec.Nombre)
    Me.txtNumActual = CStr(UnConsec.NumActual)
    Me.txtPrefijo = Trim(UnConsec.Prefijo)
    Me.cboDocum.ListIndex = -1
    For NuMr = 1 To Me.cboDocum.ListCount
        If Me.cboDocum.ItemData(NuMr - 1) = UnConsec.AutoDocu Then
            Me.cboDocum.ListIndex = NuMr - 1
            Exit For
        End If
    Next
End Sub

Private Sub btnAdd_Click()
    Dim UnConsec As New ElConsec, Aviso As String
    Dim ArrCSC() As Variant
    If Me.arbConsec.Enabled Then Me.arbConsec.Enabled = False
    If Me.tlbACCIONES.Buttons("ADD").Caption = "&Adicionar" Then
'    If Me.btnAdd.Caption = "&Adicionar" Then
        Me.txtCodigo.Text = ""
        Me.txtNombre.Text = ""
        Me.txtNumActual.Text = CStr(0)
        Me.txtPrefijo.Text = ""
        Me.cboDocum.ListIndex = -1
        Me.cboDocum.Text = "Seleccione uno"
        Me.txtCodigo.Enabled = True
        Me.txtNombre.Enabled = True
        Me.txtNumActual.Enabled = True
        Me.txtPrefijo.Enabled = True
        Me.cboDocum.Enabled = True
        Me.tlbACCIONES.Buttons("ADD").Caption = "&Guardar"
        If Not Me.tlbACCIONES.Buttons("CAN").Enabled Then Me.tlbACCIONES.Buttons("CAN").Enabled = True
        Me.txtNombre.SetFocus
    Else
        Me.txtCodigo.Enabled = False
        Me.txtNombre.Enabled = False
        Me.txtNumActual.Enabled = False
        Me.txtPrefijo.Enabled = False
        Me.cboDocum.Enabled = False
        If MalaInformacion(Aviso) Then
            Call MsgBox("Informaci�n incompleta en el(los) siguiente(s) campos:" & Aviso, vbExclamation)
        Else
            Valores = "TX_CODI_CNSC='" & Me.txtCodigo.Text & _
                "', TX_NOMB_CNSC='" & Me.txtNombre.Text & _
                "', TX_PRFJ_CNSC='" & Me.txtPrefijo.Text & _
                "', NU_CNSC_CNSC=" & CLng(Me.txtNumActual.Text) & _
                ", NU_AUTO_DOCU_CNSC=" & Me.cboDocum.ItemData(Me.cboDocum.ListIndex)
            Result = DoInsertSQL("IN_CONSECUTIVO", Valores)
            If Result = FAIL Then GoTo FALLO
            Campos = "NU_AUTO_CNSC, TX_CODI_CNSC, TX_NOMB_CNSC, " & _
                "TX_PRFJ_CNSC, NU_AUTO_DOCU_CNSC, NU_CNSC_CNSC"
            Desde = "IN_CONSECUTIVO"
            Condi = "TX_CODI_CNSC='" & Me.txtCodigo.Text & "'"
            ReDim ArrCSC(5)
            Result = LoadData(Desde, Campos, Condi, ArrCSC())
            If Result = FAIL Then GoTo FALLO
            If Not Encontro Then GoTo NOENC
            UnConsec.AutoDocu = ArrCSC(4)
            UnConsec.AutoCnsc = ArrCSC(0)
            UnConsec.Codigo = ArrCSC(1)
            UnConsec.Nombre = ArrCSC(2)
            UnConsec.NumActual = ArrCSC(5)
            UnConsec.Prefijo = ArrCSC(3)
            CllDeConsec.Add UnConsec, "S" & ArrCSC(0)
            Me.arbConsec.Nodes.Add "CNSC", tvwChild, "S" & UnConsec.AutoCnsc, Trim(UnConsec.Nombre)
            Me.txtCodigo.Text = Trim(UnConsec.Codigo)
            Me.txtNombre.Text = Trim(UnConsec.Nombre)
            Me.txtPrefijo.Text = Trim(UnConsec.Prefijo)
            Me.txtNumActual.Text = CStr(UnConsec.NumActual)
            Set Me.arbConsec.SelectedItem = Me.arbConsec.Nodes("S" & UnConsec.AutoCnsc)
            Set UnConsec = Nothing
            Me.arbConsec.SelectedItem.Bold = True
        End If
        Me.tlbACCIONES.Buttons("ADD").Caption = "&Adicionar"
        Me.tlbACCIONES.Buttons("CAN").Enabled = False
        Me.arbConsec.Enabled = True
    End If
NOENC:
    Exit Sub
FALLO:
End Sub

Private Sub btnCambiar_Click()
    Dim UnConsec As New ElConsec, Aviso As String
    If Me.arbConsec.Enabled Then Me.arbConsec.Enabled = False
    If Me.tlbACCIONES.Buttons("CHA").Caption = "&Modificar" Then
        If Me.tlbACCIONES.Buttons("DEL").Enabled Then Me.tlbACCIONES.Buttons("DEL").Enabled = False
        If Not Me.txtCodigo.Enabled Then Me.txtCodigo.Enabled = True
        If Not Me.txtNombre.Enabled Then Me.txtNombre.Enabled = True
        If Not Me.txtNumActual.Enabled Then Me.txtNumActual.Enabled = True
        If Not Me.txtPrefijo.Enabled Then Me.txtPrefijo.Enabled = True
        If Not Me.cboDocum.Enabled Then Me.cboDocum.Enabled = True
        Me.tlbACCIONES.Buttons("CHA").Caption = "&Guardar"
        If Not Me.tlbACCIONES.Buttons("CAN").Enabled Then Me.tlbACCIONES.Buttons("CAN").Enabled = True
        Me.txtNombre.SetFocus
    Else
        If Not Me.tlbACCIONES.Buttons("DEL").Enabled Then Me.tlbACCIONES.Buttons("DEL").Enabled = True
        Me.txtCodigo.Enabled = False
        Me.txtNombre.Enabled = False
        Me.txtNumActual.Enabled = False
        Me.txtPrefijo.Enabled = False
        Me.cboDocum.Enabled = False
        If MalaInformacion(Aviso) Then
            Call MsgBox("Informaci�n incompleta en el(los) siguiente(s) campos:" & Aviso, vbExclamation)
        Else
            Set UnConsec = CllDeConsec.Item(Me.arbConsec.SelectedItem.Key)
            Valores = "TX_CODI_CNSC='" & Me.txtCodigo.Text & _
                "', TX_NOMB_CNSC='" & Me.txtNombre.Text & _
                "', TX_PRFJ_CNSC='" & Me.txtPrefijo.Text & _
                "', NU_AUTO_DOCU_CNSC=" & Me.cboDocum.ItemData(Me.cboDocum.ListIndex) & _
                ", NU_CNSC_CNSC=" & CLng(Me.txtNumActual.Text)
            Condi = "NU_AUTO_CNSC=" & CLng(Right(Me.arbConsec.SelectedItem.Key, Len(Me.arbConsec.SelectedItem.Key) - 1))
            Result = DoUpdate("IN_CONSECUTIVO", Valores, Condi)
            If Result = FAIL Then GoTo FALLO
            UnConsec.Codigo = Me.txtCodigo.Text
            UnConsec.Nombre = Me.txtNombre.Text
            UnConsec.Prefijo = Me.txtPrefijo.Text
        End If
        Me.tlbACCIONES.Buttons("CHA").Caption = "&Modificar"
        Me.tlbACCIONES.Buttons("CAN").Enabled = False
        Me.arbConsec.Enabled = True
    End If
FALLO:
End Sub

Private Sub btnCancelar_Click()
    If Me.arbConsec.SelectedItem.Key = "CNSC" Then
        If Me.tlbACCIONES.Buttons("ADD").Caption = "&Guardar" Then Me.tlbACCIONES.Buttons("ADD").Caption = "&Adicionar"
Else
        If Me.tlbACCIONES.Buttons("CHA").Caption = "&Guardar" Then Me.tlbACCIONES.Buttons("CHA").Caption = "&Modificar"
        If Not Me.tlbACCIONES.Buttons("DEL").Enabled Then Me.tlbACCIONES.Buttons("DEL").Enabled = True
        Me.tlbACCIONES.Buttons("CHA").Enabled = False
    End If
    Call arbConsec_NodeClick(Me.arbConsec.SelectedItem)
    Me.arbConsec.Enabled = True
End Sub

Private Sub Form_Load()
    Dim CnTdr As Integer, Punto As MSComctlLib.Node
    Dim ArrCSC() As Variant
'    Call main
    Call CenterForm(MDI_Inventarios, Me)
    Me.txtCodigo.Text = ""
    Me.txtNombre.Text = ""
    Me.txtNumActual.Text = CStr(0)
    Me.txtPrefijo.Text = ""
    Me.cboDocum.ListIndex = -1
    Me.txtCodigo.Enabled = False
    Me.txtNombre.Enabled = False
    Me.txtNumActual.Enabled = False
    Me.txtPrefijo.Enabled = False
    Me.cboDocum.Enabled = False
    Campos = "NU_AUTO_DOCU, TX_CODI_DOCU, TX_NOMB_DOCU, TX_AFEC_DOCU, " & _
        "TX_INTE_DOCU, NU_AUTO_DOCUDEPE_DOCU, NU_AUTO_DOCUREVE_DOCU, " & _
        "TX_TIPOVAL_DOCU, TX_INDEP_DOCU"
    Desde = "IN_DOCUMENTO"
    Condi = ""
    ReDim ArrCSC(8, 0)
    Result = LoadMulData(Desde, Campos, Condi, ArrCSC())
    If Result = FAIL Then GoTo FALLO
    For CnTdr = 0 To UBound(ArrCSC, 2)
        Me.cboDocum.AddItem ArrCSC(2, CnTdr)
        Me.cboDocum.ItemData(Me.cboDocum.NewIndex) = ArrCSC(0, CnTdr)
    Next
    
    Campos = "NU_AUTO_CNSC, TX_CODI_CNSC, TX_NOMB_CNSC, " & _
        "TX_PRFJ_CNSC, NU_AUTO_DOCU_CNSC, NU_CNSC_CNSC"
    Desde = "IN_CONSECUTIVO"
    Condi = ""
    ReDim ArrCSC(5, 0)
    Result = LoadMulData(Desde, Campos, Condi, ArrCSC())
    If Result = FAIL Then GoTo FALLO
    Me.arbConsec.Nodes.Add , , "CNSC", "CONSECUTIVOS"
    If Not Me.tlbACCIONES.Buttons("ADD").Enabled Then Me.tlbACCIONES.Buttons("ADD").Enabled = True
    Set Me.arbConsec.SelectedItem = Me.arbConsec.Nodes("CNSC")
    If Encontro Then
        Call BorrarColeccion(CllDeConsec)
        For CnTdr = 0 To UBound(ArrCSC, 2)
            Dim UnConsec As New ElConsec
            UnConsec.AutoDocu = ArrCSC(4, CnTdr)
            UnConsec.AutoCnsc = ArrCSC(0, CnTdr)
            UnConsec.Codigo = ArrCSC(1, CnTdr)
            UnConsec.Nombre = ArrCSC(2, CnTdr)
            UnConsec.NumActual = ArrCSC(5, CnTdr)
            UnConsec.Prefijo = ArrCSC(3, CnTdr)
            CllDeConsec.Add UnConsec, "S" & ArrCSC(0, CnTdr)
            Me.arbConsec.Nodes.Add "CNSC", tvwChild, "S" & UnConsec.AutoCnsc, Trim(UnConsec.Nombre)
            Set UnConsec = Nothing
        Next
    End If
FALLO:
End Sub

Private Sub tlbACCIONES_ButtonClick(ByVal Boton As MSComctlLib.Button)
    Select Case Boton.Key
    Case Is = "CHA"
        Call btnCambiar_Click
    Case Is = "ADD"
        Call btnAdd_Click
    Case Is = "DEL"
    Case Is = "CAN"
        Call btnCancelar_Click
    End Select
End Sub

'Private Sub Form_Unload(Cancel As Integer)
'    BD(BDCur).Close
'End Sub

Private Sub txtCodigo_KeyPress(Tecla As Integer)
    Call txtNombre_KeyPress(Tecla)
End Sub

Private Sub txtNombre_KeyPress(Tecla As Integer)
    Dim Cara As String * 1
    Cara = Chr(Tecla)
    If Cara = vbBack Then Exit Sub
    If Asc(Cara) > 96 And Asc(Tecla) < 113 Then Tecla = Asc(UCase(Chr(Tecla)))
    If InStr(" 01234567890ABCDEFGHIJKLMNOPQRSTUVWXYZ", Chr(Tecla)) = 0 Then Tecla = 0
End Sub

Private Sub txtNumActual_KeyPress(Tecla As Integer)
    Call txtNombre_KeyPress(Tecla)
End Sub

Private Sub txtPrefijo_KeyPress(Tecla As Integer)
    Call txtNombre_KeyPress(Tecla)
End Sub

Private Function MalaInformacion(ByRef Cual As String) As Boolean
    Cual = ""
    MalaInformacion = False
    If Len(Trim(Me.txtNombre)) = 0 Then Cual = Cual & IIf(Len(Cual) = 0, "", ", ") & "Nombre"
    If Len(Trim(Me.txtCodigo)) = 0 Then Cual = Cual & IIf(Len(Cual) = 0, "", ", ") & "C�digo"
    If Len(Trim(Me.txtPrefijo)) = 0 Then Cual = Cual & IIf(Len(Cual) = 0, "", ", ") & "Prefijo"
    If Not IsNumeric(Trim(Me.txtNumActual)) Then Cual = Cual & IIf(Len(Cual) = 0, "", ", ") & "N�mero actual"
    If Len(Cual) > 0 Then MalaInformacion = True
End Function
