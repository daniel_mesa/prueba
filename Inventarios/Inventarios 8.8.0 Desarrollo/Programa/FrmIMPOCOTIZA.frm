VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Object = "{0BA686C6-F7D3-101A-993E-0000C0EF6F5E}#1.0#0"; "THREED32.OCX"
Begin VB.Form FrmIMPOCOTIZA 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Importar Cotizaciones/Listas de Precio"
   ClientHeight    =   2370
   ClientLeft      =   4185
   ClientTop       =   2970
   ClientWidth     =   5820
   Icon            =   "FrmIMPOCOTIZA.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   ScaleHeight     =   2370
   ScaleWidth      =   5820
   Begin MSComctlLib.ProgressBar PrgPlano 
      Height          =   255
      Left            =   840
      TabIndex        =   0
      Top             =   600
      Width           =   4095
      _ExtentX        =   7223
      _ExtentY        =   450
      _Version        =   393216
      BorderStyle     =   1
      Appearance      =   1
   End
   Begin VB.Frame Frame2 
      Height          =   1095
      Left            =   600
      TabIndex        =   1
      Top             =   1080
      Width           =   4575
      Begin Threed.SSCommand SCmd_Options 
         Height          =   735
         Index           =   1
         Left            =   480
         TabIndex        =   2
         Top             =   240
         Width           =   855
         _Version        =   65536
         _ExtentX        =   1508
         _ExtentY        =   1296
         _StockProps     =   78
         Caption         =   "&CARGAR"
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   6
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Picture         =   "FrmIMPOCOTIZA.frx":058A
      End
      Begin Threed.SSCommand SCmd_Options 
         Height          =   735
         Index           =   3
         Left            =   3360
         TabIndex        =   3
         Top             =   240
         Width           =   855
         _Version        =   65536
         _ExtentX        =   1508
         _ExtentY        =   1296
         _StockProps     =   78
         Caption         =   "&SALIR"
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   6
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Picture         =   "FrmIMPOCOTIZA.frx":0C54
      End
      Begin Threed.SSCommand SCmd_Detener 
         Height          =   735
         Left            =   1920
         TabIndex        =   4
         Top             =   240
         Width           =   855
         _Version        =   65536
         _ExtentX        =   1508
         _ExtentY        =   1296
         _StockProps     =   78
         Caption         =   "CANCELAR"
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   6
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Picture         =   "FrmIMPOCOTIZA.frx":131E
      End
   End
   Begin VB.Label Lbl_Errores 
      Caption         =   "0"
      Height          =   255
      Left            =   3960
      TabIndex        =   8
      Top             =   240
      Width           =   855
   End
   Begin VB.Label Lbl_Procesados 
      Caption         =   "0"
      Height          =   255
      Left            =   1920
      TabIndex        =   7
      Top             =   240
      Width           =   855
   End
   Begin VB.Label Label2 
      Caption         =   "Errores:"
      Height          =   255
      Left            =   3360
      TabIndex        =   6
      Top             =   240
      Width           =   615
   End
   Begin VB.Label Label1 
      Caption         =   "Procesados:"
      Height          =   255
      Left            =   960
      TabIndex        =   5
      Top             =   240
      Width           =   975
   End
End
Attribute VB_Name = "FrmIMPOCOTIZA"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Dim texto As String
Dim NumReg As Long
Dim BandErr, BandStop As Boolean
'Dim ArrTarifa(), CodTarifaBase() As Variant        'DEPURACION DE CODIGO
'Dim erroresCargue As Integer       'DEPURACION DE CODIGO
Dim LarReg As Integer
Dim vTercero                        As New ElTercero
Dim vCodigoDeBarra                  As New CdgBarra
'Private PosEnGri                    As String      'DEPURACION DE CODIGO
Private vCuentaComas As Integer

Private Sub Form_Load()
    Call CenterForm(MDI_Inventarios, Me)
    BandProcLote = 0
    BandStop = False
    vCuentaComas = 5
End Sub

Private Sub SCmd_Detener_Click()
If BandProcLote Then
   If MsgBox("�Esta seguro que desea detener el proceso?", vbExclamation) = vbYes Then
      BandStop = True
   End If
End If
End Sub

Private Sub SCmd_Options_Click(Index As Integer)
If BandProcLote = 1 Then Exit Sub
Select Case Index
   Case 1: Call CargarTarifas
   Case 3: Unload Me
End Select
End Sub

Private Sub CargarTarifas()
    Dim TotaLeidos As Long
'    Dim TotalLinea As Double       'DEPURACION DE CODIGO
'    Dim Procesados As Integer      'DEPURACION DE CODIGO
'    Dim UnItem As MSComctlLib.ListItem     'DEPURACION DE CODIGO
'    Dim Linea As String, Estoy As String, Faltan As String     'DEPURACION DE CODIGO
    Dim Estoy As String, Faltan As String
    Dim Errores
    On Error GoTo control
    PrgPlano.Value = 0
    'If MDI_Inventarios.CMDialog1.InitDir = NUL$ Then MDI_Inventarios.CMDialog1.InitDir = DirDB
    If MDI_Inventarios.CMDialog1.InitDir = NUL$ Then MDI_Inventarios.CMDialog1.InitDir = App.Path
    MDI_Inventarios.CMDialog1.Filter = "(TODOS)|*"
    MDI_Inventarios.CMDialog1.Action = 1
    BandErr = False
    If MDI_Inventarios.CMDialog1.filename <> "" Then
        PrgPlano.Max = FileLen(MDI_Inventarios.CMDialog1.filename)
        Open MDI_Inventarios.CMDialog1.filename For Input As #1
        Open App.Path & "\ErrorDeCarga.txt" For Output As #2
        NumReg = 1
        BandProcLote = 1
        PrgPlano.Value = PrgPlano.Min
        DoEvents
        Errores = 0
        BandStop = False
        LarReg = 0: TotaLeidos = 0
        Do While Not EOF(1)
            DoEvents
            If BandStop = True Then Exit Do
            Line Input #1, texto
            TotaLeidos = TotaLeidos + Len(Faltan)
            LarReg = TotaLeidos / NumReg
            If PrgPlano.Value + LarReg < PrgPlano.Max Then PrgPlano.Value = PrgPlano.Value + LarReg
            If CuantosHay(texto, Coma) <> vCuentaComas Then GoTo OtrReg
            If ValidarTercero(Trim(SacarUnoXPosicion(texto, Coma, 1))) And vCodigoDeBarra.IniciaXCodigo(Trim(SacarUnoXPosicion(texto, Coma, 2))) Then
''IN_R_CODIGOBAR_TERCERO(TX_COBA_COBA_RCBTE,CD_CODI_TERC_RCBTE,NU_MULT_RCBTE,NU_DIVI_RCBTE,NU_COST_RCBTE,NU_IMPU_RCBTE)
                If Len(Trim(SacarUnoXPosicion(texto, Coma, 2))) > 0 And _
                    Len(Trim(SacarUnoXPosicion(texto, Coma, 3))) > 0 And _
                    Len(Trim(SacarUnoXPosicion(texto, Coma, 4))) > 0 And _
                    Len(Trim(SacarUnoXPosicion(texto, Coma, 5))) > 0 And _
                    Len(Trim(SacarUnoXPosicion(texto, Coma, 6))) > 0 Then
                    Valores = "CD_CODI_TERC_RCBTE='" & Trim(SacarUnoXPosicion(texto, Coma, 1)) & "'"
                    Valores = Valores & ",TX_COBA_COBA_RCBTE='" & Trim(SacarUnoXPosicion(texto, Coma, 2)) & "'"
                    Valores = Valores & ",NU_MULT_RCBTE=" & Trim(SacarUnoXPosicion(texto, Coma, 3))
                    Valores = Valores & ",NU_DIVI_RCBTE=" & Trim(SacarUnoXPosicion(texto, Coma, 4))
                    Valores = Valores & ",NU_COST_RCBTE=" & Trim(SacarUnoXPosicion(texto, Coma, 5))
                    Valores = Valores & ",NU_IMPU_RCBTE=" & Trim(SacarUnoXPosicion(texto, Coma, 6))
                    Result = DoInsertSQL("IN_R_CODIGOBAR_TERCERO", Valores)
                End If
            Else
                Print #2, "Linea:" & NumReg & ", C�digo:" & Estoy
                Errores = Errores + 1
            End If
            Lbl_Procesados.Caption = NumReg & " de " & CLng(PrgPlano.Max / IIf(LarReg > 0, LarReg, 20))
            Lbl_Procesados.Refresh
            Lbl_Errores.Caption = Errores
            Lbl_Errores.Refresh
OtrReg:
            NumReg = NumReg + 1
        Loop
        Close (1): Close (2)
        BandProcLote = 0
        Lbl_Procesados.Caption = NumReg & " de " & NumReg
        Lbl_Procesados.Refresh
        If Errores > 0 Then MsgBox "Se encontro errores, el resumen esta en el archivo:" & App.Path & "\ErrorDeCarga.txt", vbCritical
    End If
    PrgPlano.Value = 0
    Exit Sub
control:
    If ERR.Number <> 0 Then
       Call ConvertErr
       Close (1)
    End If
    Call MouseNorm
    PrgPlano.Value = 0
End Sub

Function ValidarTercero(Codigo As String) As Boolean
    ValidarTercero = True
    If vTercero.IniXNit(Codigo) = Encontroinfo Then Exit Function
    ValidarTercero = False
End Function

Function ValidarCodigoBarra(Codigo As String) As Boolean
    ValidarCodigoBarra = True
    If vCodigoDeBarra.IniciaXCodigo(Codigo) = Encontroinfo Then Exit Function
    ValidarCodigoBarra = False
End Function

