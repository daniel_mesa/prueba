VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "mscomctl.ocx"
Object = "{C932BA88-4374-101B-A56C-00AA003668DC}#1.1#0"; "MSMASK32.OCX"
Begin VB.Form frmPrmREGISTRO 
   BorderStyle     =   1  'Fixed Single
   Caption         =   " "
   ClientHeight    =   7935
   ClientLeft      =   45
   ClientTop       =   435
   ClientWidth     =   12405
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   7935
   ScaleWidth      =   12405
   Begin MSComctlLib.Toolbar tlbTERCEROS 
      Height          =   600
      Left            =   2640
      TabIndex        =   14
      Top             =   2880
      Width           =   12405
      _ExtentX        =   21881
      _ExtentY        =   1058
      ButtonWidth     =   1535
      ButtonHeight    =   953
      Appearance      =   1
      _Version        =   393216
      BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
         NumButtons      =   5
         BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "&Adicionar"
            Key             =   "ADD"
         EndProperty
         BeginProperty Button2 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "&Borrar"
            Key             =   "DEL"
         EndProperty
         BeginProperty Button3 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "&Guardar"
            Key             =   "SAV"
         EndProperty
         BeginProperty Button4 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "&Cancelar"
            Key             =   "CAN"
         EndProperty
         BeginProperty Button5 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Enabled         =   0   'False
            Caption         =   "Proveedor"
            Style           =   1
         EndProperty
      EndProperty
   End
   Begin MSComctlLib.Toolbar tlbBARRAS 
      Height          =   600
      Left            =   2640
      TabIndex        =   13
      Top             =   2040
      Width           =   12405
      _ExtentX        =   21881
      _ExtentY        =   1058
      ButtonWidth     =   1535
      ButtonHeight    =   953
      Appearance      =   1
      _Version        =   393216
      BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
         NumButtons      =   5
         BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "&Adicionar"
            Key             =   "ADD"
         EndProperty
         BeginProperty Button2 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "&Borrar"
            Key             =   "DEL"
         EndProperty
         BeginProperty Button3 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "&Guardar"
            Key             =   "SAV"
         EndProperty
         BeginProperty Button4 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "&Cancelar"
            Key             =   "CAN"
         EndProperty
         BeginProperty Button5 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Enabled         =   0   'False
            Caption         =   "Codigos"
            Key             =   "CUA"
            Style           =   1
         EndProperty
      EndProperty
   End
   Begin MSComctlLib.Toolbar tlbARTICULOS 
      Height          =   600
      Left            =   2640
      TabIndex        =   12
      Top             =   1200
      Width           =   12405
      _ExtentX        =   21881
      _ExtentY        =   1058
      ButtonWidth     =   1508
      ButtonHeight    =   953
      Appearance      =   1
      _Version        =   393216
      BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
         NumButtons      =   1
         BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "&Modificar"
            Key             =   "CHA"
         EndProperty
      EndProperty
   End
   Begin MSComctlLib.ListView lstArticulos 
      Height          =   4695
      Left            =   240
      TabIndex        =   8
      Top             =   840
      Width           =   6855
      _ExtentX        =   12091
      _ExtentY        =   8281
      View            =   3
      LabelEdit       =   1
      MultiSelect     =   -1  'True
      LabelWrap       =   -1  'True
      HideSelection   =   0   'False
      AllowReorder    =   -1  'True
      FullRowSelect   =   -1  'True
      GridLines       =   -1  'True
      _Version        =   393217
      ForeColor       =   -2147483640
      BackColor       =   -2147483643
      BorderStyle     =   1
      Appearance      =   1
      NumItems        =   0
   End
   Begin MSComctlLib.ListView lstPROVEEDORES 
      Height          =   4695
      Left            =   240
      TabIndex        =   22
      Top             =   840
      Width           =   6855
      _ExtentX        =   12091
      _ExtentY        =   8281
      View            =   3
      LabelEdit       =   1
      MultiSelect     =   -1  'True
      LabelWrap       =   -1  'True
      HideSelection   =   -1  'True
      FullRowSelect   =   -1  'True
      GridLines       =   -1  'True
      _Version        =   393217
      ForeColor       =   -2147483640
      BackColor       =   -2147483643
      BorderStyle     =   1
      Appearance      =   1
      NumItems        =   0
   End
   Begin VB.Frame frmOFERTA 
      Height          =   2055
      Left            =   9120
      TabIndex        =   23
      Top             =   5760
      Width           =   3135
      Begin VB.ComboBox cboPRESENTA 
         Height          =   315
         Left            =   240
         Style           =   2  'Dropdown List
         TabIndex        =   28
         Top             =   1440
         Width           =   2655
      End
      Begin MSMask.MaskEdBox txtCOSTO 
         Height          =   285
         Left            =   1320
         TabIndex        =   24
         Top             =   240
         Width           =   1455
         _ExtentX        =   2566
         _ExtentY        =   503
         _Version        =   393216
         PromptInclude   =   0   'False
         MaxLength       =   9
         Format          =   "#,##0"
         Mask            =   "999999999"
         PromptChar      =   "_"
      End
      Begin MSMask.MaskEdBox txtIMPUES 
         Height          =   285
         Left            =   1320
         TabIndex        =   26
         Top             =   720
         Width           =   855
         _ExtentX        =   1508
         _ExtentY        =   503
         _Version        =   393216
         PromptInclude   =   0   'False
         MaxLength       =   5
         Format          =   "##0.00"
         Mask            =   "99.99"
         PromptChar      =   "_"
      End
      Begin VB.Label Label10 
         Caption         =   "Presentaci�n"
         Height          =   255
         Left            =   240
         TabIndex        =   29
         Top             =   1200
         Width           =   1095
      End
      Begin VB.Label Label9 
         Alignment       =   1  'Right Justify
         Caption         =   "Impuesto"
         Height          =   255
         Left            =   120
         TabIndex        =   27
         Top             =   780
         Width           =   1100
      End
      Begin VB.Label Label8 
         Alignment       =   1  'Right Justify
         Caption         =   "Costo con Impuesto"
         Height          =   400
         Left            =   120
         TabIndex        =   25
         Top             =   210
         Width           =   1100
      End
   End
   Begin VB.Frame frmCAMBIOS 
      Height          =   2055
      Left            =   5280
      TabIndex        =   15
      Top             =   5760
      Width           =   3735
      Begin MSMask.MaskEdBox txtCIAL 
         Height          =   285
         Left            =   1080
         TabIndex        =   20
         Top             =   1020
         Width           =   2535
         _ExtentX        =   4471
         _ExtentY        =   503
         _Version        =   393216
         PromptInclude   =   0   'False
         MaxLength       =   20
         PromptChar      =   "_"
      End
      Begin MSMask.MaskEdBox txtREGI 
         Height          =   285
         Left            =   1080
         TabIndex        =   18
         Top             =   660
         Width           =   1455
         _ExtentX        =   2566
         _ExtentY        =   503
         _Version        =   393216
         PromptInclude   =   0   'False
         MaxLength       =   20
         PromptChar      =   "_"
      End
      Begin MSMask.MaskEdBox txtCDBarra 
         Height          =   285
         Left            =   1080
         TabIndex        =   16
         Top             =   300
         Width           =   1455
         _ExtentX        =   2566
         _ExtentY        =   503
         _Version        =   393216
         PromptInclude   =   0   'False
         MaxLength       =   13
         PromptChar      =   "_"
      End
      Begin VB.Label Label5 
         Alignment       =   1  'Right Justify
         Caption         =   "CD Barra:"
         Height          =   255
         Left            =   120
         TabIndex        =   21
         Top             =   360
         Width           =   750
      End
      Begin VB.Label Label6 
         Alignment       =   1  'Right Justify
         Caption         =   "Registro:"
         Height          =   255
         Left            =   120
         TabIndex        =   19
         Top             =   720
         Width           =   750
      End
      Begin VB.Label Label7 
         Alignment       =   1  'Right Justify
         Caption         =   "Nombre Comercial:"
         Height          =   495
         Left            =   120
         TabIndex        =   17
         Top             =   980
         Width           =   750
      End
   End
   Begin VB.Frame frmINFORMA 
      Caption         =   "Informaci�n:"
      Height          =   2055
      Left            =   240
      TabIndex        =   0
      Top             =   5760
      Width           =   4935
      Begin VB.TextBox txtNOMB 
         Height          =   600
         Left            =   840
         TabIndex        =   9
         Text            =   "txtNOMB"
         Top             =   240
         Width           =   3975
      End
      Begin VB.TextBox txtIDEN 
         Height          =   300
         Left            =   840
         TabIndex        =   7
         Text            =   "txtIDEN"
         Top             =   1080
         Width           =   1695
      End
      Begin VB.TextBox txtGRUP 
         Height          =   300
         Left            =   840
         TabIndex        =   6
         Text            =   "txtGRUP"
         Top             =   1560
         Width           =   1695
      End
      Begin VB.TextBox txtUSOS 
         Height          =   300
         Left            =   3120
         TabIndex        =   5
         Text            =   "txtUSOS"
         Top             =   1560
         Width           =   1695
      End
      Begin VB.Label Label4 
         Alignment       =   1  'Right Justify
         Caption         =   "Usos:"
         Height          =   255
         Left            =   2400
         TabIndex        =   4
         Top             =   1560
         Width           =   705
      End
      Begin VB.Label Label2 
         Alignment       =   1  'Right Justify
         Caption         =   "Nombre:"
         Height          =   255
         Left            =   120
         TabIndex        =   3
         Top             =   240
         Width           =   705
      End
      Begin VB.Label Label1 
         Alignment       =   1  'Right Justify
         Caption         =   "C�digo:"
         Height          =   255
         Left            =   120
         TabIndex        =   2
         Top             =   1080
         Width           =   705
      End
      Begin VB.Label Label3 
         Alignment       =   1  'Right Justify
         Caption         =   "Grupo:"
         Height          =   255
         Left            =   120
         TabIndex        =   1
         Top             =   1560
         Width           =   705
      End
   End
   Begin MSComctlLib.ListView lstTerceros 
      Height          =   2295
      Left            =   7320
      TabIndex        =   10
      Top             =   3240
      Width           =   4815
      _ExtentX        =   8493
      _ExtentY        =   4048
      View            =   3
      LabelEdit       =   1
      MultiSelect     =   -1  'True
      LabelWrap       =   -1  'True
      HideSelection   =   0   'False
      AllowReorder    =   -1  'True
      FullRowSelect   =   -1  'True
      GridLines       =   -1  'True
      _Version        =   393217
      ForeColor       =   -2147483640
      BackColor       =   -2147483643
      BorderStyle     =   1
      Appearance      =   1
      NumItems        =   0
   End
   Begin MSComctlLib.ListView lstBarras 
      Height          =   2175
      Left            =   7320
      TabIndex        =   11
      Top             =   840
      Width           =   4815
      _ExtentX        =   8493
      _ExtentY        =   3836
      View            =   3
      LabelEdit       =   1
      LabelWrap       =   -1  'True
      HideSelection   =   0   'False
      FullRowSelect   =   -1  'True
      GridLines       =   -1  'True
      _Version        =   393217
      ForeColor       =   -2147483640
      BackColor       =   -2147483643
      BorderStyle     =   1
      Appearance      =   1
      NumItems        =   0
   End
End
Attribute VB_Name = "frmPrmREGISTRO"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Dim ArrBRR() As Variant
Dim Articulo As UnArticulo
Dim erda As String
Private vClaveArticuloActual As String
Public OpcCod        As String   'Opci�n de seguridad
Private vContar As Integer
Private vClave As String
Private vExiste As Boolean
Private vUno As MSComctlLib.ListItem, vDos As MSComctlLib.ListItem
'NU_AUTO_UNVE, TX_CODI_UNVE, TX_NOMB_UNVE, NU_MULT_UNVE, NU_DIVI_UNVE

Private Sub btnAdd_Click()
    If MalaInformacion(erda) Then
        Call MsgBox("Incompleta informaci�n en:" & erda, vbInformation)
    Else
'IN_CODIGOBAR(NU_AUTO_ARTI_COBA, TX_COBA_COBA, CD_CODI_TERC_COBA)

'IN_CODIGOBAR(NU_AUTO_ARTI_COBA,TX_COBA_COBA,TX_REGI_COBA,TX_CIAL_COBA)
'IN_R_CODIGOBAR_TERCERO(TX_COBA_COBA_RCBTE,CD_CODI_TERC_RCBTE,NU_MULT_RCBTE,NU_DIVI_RCBTE,NU_COST_RCBTE,NU_IMPU_RCBTE)
        vExiste = False
        ReDim ArrBRR(0)
        Result = LoadData("IN_CODIGOBAR", "TX_COBA_COBA", "TX_COBA_COBA='" & Trim(Me.txtCDBarra.Text) & "'", ArrBRR())
        If Result = FAIL Then Exit Sub
        If Encontro Then
            Valores = "TX_REGI_COBA='" & Trim(Me.txtREGI.Text) & "'"
            Valores = Valores & ", TX_CIAL_COBA='" & Trim(Me.txtCIAL.Text) & "'"
            Result = DoUpdate("IN_CODIGOBAR", Valores, "TX_COBA_COBA='" & Trim(Me.txtCDBarra.Text) & "'")
        Else
            Valores = "TX_COBA_COBA='" & Trim(Me.txtCDBarra.Text) & "'"
            Valores = Valores & ", NU_AUTO_ARTI_COBA=" & Me.lstArticulos.SelectedItem.Tag
            Valores = Valores & ", TX_REGI_COBA='" & Trim(Me.txtREGI.Text) & "'"
            Valores = Valores & ", TX_CIAL_COBA='" & Trim(Me.txtCIAL.Text) & "'"
            Result = DoInsertSQL("IN_CODIGOBAR", Valores)
        End If
        For Each vUno In Me.lstTerceros.ListItems
            If vUno.Selected Then
                Valores = "TX_COBA_COBA_RCBTE='" & Trim(Me.txtCDBarra.Text) & _
                    "', CD_CODI_TERC_RCBTE='" & vUno.Tag & "'"
                Result = DoInsertSQL("IN_R_COBA_TERC", Valores)
            End If
        Next
        Call lstArticulos_Click
    End If
End Sub

Private Sub btnCha_Click()
    If False Then
    Else
        If MalaInformacion(erda) Then
            Call MsgBox("Incompleta informaci�n en:" & erda, vbInformation)
        Else
'SELECT IN_CODIGOBAR.NU_AUTO_ARTI_COBA, IN_CODIGOBAR.TX_COBA_COBA, IN_CODIGOBAR.CD_CODI_TERC_COBA
'FROM IN_CODIGOBAR;
'IN_CODIGOBAR(NU_AUTO_ARTI_COBA,TX_COBA_COBA,TX_REGI_COBA,TX_CIAL_COBA)
'            Valores = "TX_COBA_COBA='" & Me.txtCDBarra.Text & _
'                "', CD_CODI_TERC_COBA='" & Me.lstTerceros.SelectedItem.Tag & "'"
'            Condi = "NU_AUTO_COBA=" & Me.lstBarras.SelectedItem.Tag
            Valores = "TX_REGI_COBA='" & Trim(Me.txtREGI.Text) & "'" & _
                ", TX_CIAL_COBA='" & Trim(Me.txtCIAL.Text) & "'"
            Condi = "TX_COBA_COBA=" & Me.lstBarras.SelectedItem.Tag
            Result = DoUpdate("IN_CODIGOBAR", Valores, Condi)
        End If
    End If
End Sub

Private Sub Form_Load()
    Dim Articulo As New UnArticulo
    Call CenterForm(MDI_Inventarios, Me)
    Me.tlbBARRAS.Align = vbAlignTop
    Me.tlbTERCEROS.Align = vbAlignTop
    Me.tlbARTICULOS.Align = vbAlignTop
    Me.tlbBARRAS.Buttons.Item("SAV").Enabled = False
    Me.tlbBARRAS.Buttons.Item("CAN").Enabled = False
    Me.tlbTERCEROS.Buttons.Item("SAV").Enabled = False
    Me.tlbTERCEROS.Buttons.Item("CAN").Enabled = False
    Me.tlbBARRAS.Visible = False
    Me.tlbTERCEROS.Visible = False
    Me.tlbARTICULOS.Visible = False
    Me.txtIDEN.Text = ""
    Me.txtNOMB.Text = ""
    Me.txtGRUP.Text = ""
    Me.txtUSOS.Text = ""
    Me.txtCDBarra.Text = ""
    Me.txtREGI.Text = ""
    Me.txtCIAL.Text = ""
    Me.txtCDBarra.Mask = String(Me.txtCDBarra.MaxLength, "9")
    Me.txtREGI.Mask = String(Me.txtREGI.MaxLength, "a")
    Me.txtCIAL.Mask = String(Me.txtCIAL.MaxLength, "a")
    Me.txtCOSTO.Mask = String(Me.txtCOSTO.MaxLength, "9")
    Me.txtCOSTO.Format = "#,##0;#,##0;#,##0;#,##0"
    Me.frmINFORMA.Enabled = False
    Me.frmCAMBIOS.Enabled = False
'    Me.txtCDBarra.Enabled = False
'    Me.txtIDEN.Enabled = False
'    Me.txtNOMB.Enabled = False
'    Me.txtGRUP.Enabled = False
'    Me.txtUSOS.Enabled = False
'    Me.txtREGI.Enabled = False
'    Me.txtCIAL.Enabled = False
    Me.lstPROVEEDORES.ListItems.Clear
    Me.lstPROVEEDORES.Visible = False
    Me.lstPROVEEDORES.ColumnHeaders.Add , "NIT", "Nit", 0.25 * Me.lstPROVEEDORES.Width
    Me.lstPROVEEDORES.ColumnHeaders.Add , "NOM", "Proveedor", 0.75 * Me.lstPROVEEDORES.Width
'/////////
    Campos = "CD_CODI_TERC, NO_NOMB_TERC"
    Desde = "TERCERO"
    Condi = ""
    If Len(Condi) > 0 Then Condi = "CD_CODI_TERC NOT IN (" & Condi & ")"
    If Len(Condi) > 0 Then Condi = Condi & " ORDER BY NO_NOMB_TERC"
    If Len(Condi) = 0 Then Desde = Desde & " ORDER BY NO_NOMB_TERC"
    ReDim ArrUXB(1, 0)
    Result = LoadMulData(Desde, Campos, Condi, ArrUXB())
    If Result = FAIL Then Exit Sub
    If Not Encontro Then GoTo NOENC
    For vContar = 0 To UBound(ArrUXB, 2)
        vClave = "P" & ArrUXB(0, vContar)
        Me.lstPROVEEDORES.ListItems.Add , vClave, ArrUXB(0, vContar)
        Me.lstPROVEEDORES.ListItems.Item(vClave).Tag = ArrUXB(0, vContar)
        Me.lstPROVEEDORES.ListItems(vClave).ListSubItems.Add , "NOM", ArrUXB(1, vContar)
    Next
'/////////
    Me.lstArticulos.Visible = True
    Me.lstBarras.ListItems.Clear
    Me.lstBarras.ColumnHeaders.Clear
    Me.lstBarras.ColumnHeaders.Add , "COD", "CD Barras", 0.25 * Me.lstBarras.Width
    Me.lstBarras.ColumnHeaders.Add , "REG", "Registro", 0.25 * Me.lstBarras.Width
    Me.lstBarras.ColumnHeaders.Add , "COM", "Comercial", 0.5 * Me.lstBarras.Width
    Me.lstTerceros.ListItems.Clear
    Me.lstTerceros.ColumnHeaders.Clear
    Me.lstTerceros.AllowColumnReorder = True
    Me.lstTerceros.MultiSelect = True
'lstArticulos, NOAR, COAR, NOCO, NOUS, NOGR, AUCO, COGR, COUS
    Me.lstTerceros.ColumnHeaders.Add , "NIT", "Nit", 0.25 * Me.lstTerceros.Width
    Me.lstTerceros.ColumnHeaders.Add , "NOM", "Proveedor", 0.75 * Me.lstTerceros.Width

'NU_AUTO_ARTI, CD_CODI_ARTI, NO_NOMB_ARTI, NU_AUTO_UNVE_ARTI, TX_PARA_ARTI, DE_DESC_GRUP, DE_DESC_USOS, TX_NOMB_UNVE
'FROM ARTICULO, USOS, GRUP_ARTICULO, IN_UNDVENTA

    Me.cboPRESENTA.Clear
    Desde = "IN_UNDVENTA"
    Campos = "NU_AUTO_UNVE, TX_NOMB_UNVE"
    Condi = "NU_MULT_UNVE=1 AND NU_DIVI_UNVE=1 ORDER BY TX_NOMB_UNVE"
    ReDim ArrUXB(1, 0)
    Result = LoadMulData(Desde, Campos, Condi, ArrUXB())
    If Result <> FAIL And Encontro Then
        For vContar = 0 To UBound(ArrUXB, 2)
            Me.cboPRESENTA.AddItem ArrUXB(1, vContar)
            Me.cboPRESENTA.ItemData(Me.cboPRESENTA.NewIndex) = CLng(ArrUXB(0, vContar))
        Next
    End If
    Campos = "NU_AUTO_ARTI, CD_CODI_ARTI, NO_NOMB_ARTI, NU_AUTO_UNVE_ARTI, TX_PARA_ARTI, " & _
        "DE_DESC_GRUP, DE_DESC_USOS, TX_NOMB_UNVE, CD_GRUP_ARTI, CD_USOS_ARTI, " & _
        "NU_MULT_UNVE, NU_DIVI_UNVE"
    Desde = "ARTICULO, USOS, GRUP_ARTICULO, IN_UNDVENTA"
    Condi = "CD_CODI_GRUP = CD_GRUP_ARTI AND CD_CODI_USOS = CD_USOS_ARTI AND " & _
        "NU_AUTO_UNVE = NU_AUTO_UNVE_ARTI ORDER BY NO_NOMB_ARTI"
    ReDim ArrUXB(11, 0)
    Result = LoadMulData(Desde, Campos, Condi, ArrUXB())
    If Result = FAIL Then GoTo FALLO
    If Not Encontro Then GoTo NOENC
    Me.lstArticulos.ListItems.Clear
    Me.lstArticulos.ColumnHeaders.Clear
'lstArticulos, NOAR, COAR, NOCO, NOUS, NOGR, AUCO, COGR, COUS
    Me.lstArticulos.ColumnHeaders.Add , "NOAR", "Nombre del art�culo", 0.4 * Me.lstArticulos.Width
    Me.lstArticulos.ColumnHeaders.Add , "COAR", "C�digo", 0.2 * Me.lstArticulos.Width
    Me.lstArticulos.ColumnHeaders.Add , "NOCO", "Unidad de conteo", 0.2 * Me.lstArticulos.Width
    Me.lstArticulos.ColumnHeaders.Add , "NOUS", "Uso", 0.1 * Me.lstArticulos.Width
    Me.lstArticulos.ColumnHeaders.Add , "NOGR", "Grupo", 0.1 * Me.lstArticulos.Width
    Me.lstArticulos.ColumnHeaders.Add , "AUCO", "AutoUNC", 0
    Me.lstArticulos.ColumnHeaders.Add , "COGR", "CodGRU", 0
    Me.lstArticulos.ColumnHeaders.Add , "COUS", "CodUSO", 0
    Me.lstArticulos.MultiSelect = False
    Me.lstArticulos.AllowColumnReorder = True
    For vContar = 0 To UBound(ArrUXB, 2)
        Articulo.AutoNumero = ArrUXB(0, vContar)
        Articulo.Codigo = ArrUXB(1, vContar)
        Articulo.Nombre = ArrUXB(2, vContar)
        Articulo.AutoUDConteo = ArrUXB(3, vContar)
        Articulo.NombreGrupo = ArrUXB(5, vContar)
        Articulo.NombreUso = ArrUXB(6, vContar)
        Articulo.NombreUndConteo = ArrUXB(7, vContar)
        Articulo.ParaQueEs = InStr(1, "VC", ArrUXB(4, vContar)) - 1
        Articulo.CodigoGrupo = ArrUXB(8, vContar)
        Articulo.CodigoUso = ArrUXB(9, vContar)
        vClave = "A" & Articulo.AutoNumero
        Me.lstArticulos.ListItems.Add , vClave, Trim(Articulo.Nombre)
        Me.lstArticulos.ListItems("A" & Articulo.AutoNumero).Tag = CStr(Articulo.AutoNumero)
'lstArticulos, NOAR, COAR, NOCO, NOUS, NOGR, AUCO, COGR, COUS, MUCO, DICO
        Me.lstArticulos.ListItems(vClave).ListSubItems.Add , "COAR", Trim(Articulo.Codigo)
        Me.lstArticulos.ListItems(vClave).ListSubItems.Add , "NOCO", Trim(Articulo.NombreUndConteo)
        Me.lstArticulos.ListItems(vClave).ListSubItems.Add , "NOUS", Trim(Articulo.NombreUso)
        Me.lstArticulos.ListItems(vClave).ListSubItems.Add , "NOGR", Trim(Articulo.NombreGrupo)
        Me.lstArticulos.ListItems(vClave).ListSubItems.Add , "AUCO", Trim(Articulo.AutoUDConteo)
        Me.lstArticulos.ListItems(vClave).ListSubItems.Add , "COGR", Trim(Articulo.CodigoGrupo)
        Me.lstArticulos.ListItems(vClave).ListSubItems.Add , "COUS", Trim(Articulo.CodigoUso)
        If vContar = 0 Then Set Me.lstArticulos.SelectedItem = Me.lstArticulos.ListItems(vClave)
    Next
    Call lstArticulos_Click
    Set Articulo = Nothing
NOENC:
FALLO:
End Sub

Private Sub lstArticulos_GotFocus()
    Me.tlbBARRAS.Visible = False
    Me.tlbTERCEROS.Visible = False
    Me.tlbARTICULOS.Visible = True
End Sub

Private Sub lstArticulos_KeyUp(KeyCode As Integer, Shift As Integer)
    If vClaveArticuloActual = Me.lstArticulos.SelectedItem.Key Then Exit Sub
    vClaveArticuloActual = Me.lstArticulos.SelectedItem.Key
    Call lstArticulos_Click
End Sub

Private Sub lstBarras_Click()
    Me.lstTerceros.ListItems.Clear
    If Not Me.lstBarras.ListItems.Count > 0 Then Exit Sub
'IN_R_COBA_TERC(TX_COBA_COBA_RCBTE,CD_CODI_TERC_RCBTE)
'SELECT CD_CODI_TERC, NO_NOMB_TERC
'FROM TERCERO;
    Campos = "CD_CODI_TERC, NO_NOMB_TERC"
    Desde = "IN_R_COBA_TERC, TERCERO"
    Condi = "CD_CODI_TERC_RCBTE=CD_CODI_TERC"
    Condi = Condi & " AND TX_COBA_COBA_RCBTE=" & Me.lstBarras.SelectedItem.Text
    ReDim ArrBRR(1, 0)
    Result = LoadMulData(Desde, Campos, Condi, ArrBRR())
    If Result = FAIL Then Exit Sub
    If Not Encontro Then Exit Sub
    For vContar = 0 To UBound(ArrBRR, 2)
        vClave = "T" & ArrBRR(0, vContar)
        Me.lstTerceros.ListItems.Add , vClave, ArrBRR(0, vContar)
        Me.lstTerceros.ListItems.Item(vClave).ListSubItems.Add , "NOM", ArrBRR(1, vContar)
    Next
End Sub

Private Sub lstBarras_ColumnClick(ByVal ColumnHeader As MSComctlLib.ColumnHeader)
    Me.lstArticulos.SortKey = ColumnHeader.Index - 1
    Me.lstArticulos.Sorted = True
End Sub

Private Sub lstBarras_DblClick()
    If Me.lstBarras.ListItems.Count = 0 Then Exit Sub
End Sub

Private Sub lstArticulos_Click()
    Dim NuMr As Integer, vClave As String
    Me.txtNOMB.Text = Me.lstArticulos.SelectedItem.Text
    Me.txtIDEN.Text = Me.lstArticulos.SelectedItem.ListSubItems("COAR").Text
    Me.txtGRUP.Text = Me.lstArticulos.SelectedItem.ListSubItems("NOGR").Text
    Me.txtUSOS.Text = Me.lstArticulos.SelectedItem.ListSubItems("NOUS").Text
    Me.txtCDBarra.Text = ""
    Me.txtCIAL.Text = ""
    Me.txtREGI.Text = ""
    Me.lstBarras.ListItems.Clear
    Me.lstTerceros.ListItems.Clear
'    Me.lstBarras.AddItem "C�digo" & vbTab & "Nit" & vbTab & "Nombre", 0
'SELECT NU_AUTO_COBA, TX_COBA_COBA, CD_CODI_TERC, NO_NOMB_TERC
'FROM TERCERO, IN_CODIGOBAR ON TERCERO.CD_CODI_TERC = IN_CODIGOBAR.CD_CODI_TERC_COBA;
'IN_CODIGOBAR(NU_AUTO_ARTI_COBA, TX_COBA_COBA, TX_REGI_COBA, TX_CIAL_COBA)

    Campos = "NU_AUTO_ARTI_COBA, TX_COBA_COBA, TX_REGI_COBA, TX_CIAL_COBA"
    Desde = "IN_CODIGOBAR"
    Condi = "NU_AUTO_ARTI_COBA=" & Me.lstArticulos.SelectedItem.Tag
    ReDim ArrBRR(4, 0)
    Result = LoadMulData(Desde, Campos, Condi, ArrBRR())
    If Result = FAIL Then GoTo FALLO
    If Not Encontro Then GoTo NOENC
    For NuMr = 0 To UBound(ArrBRR, 2)
        vClave = "C" & ArrBRR(1, NuMr)
        Me.lstBarras.ListItems.Add , vClave, ArrBRR(1, NuMr)
        Me.lstBarras.ListItems(vClave).Tag = ArrBRR(0, NuMr)
        Me.lstBarras.ListItems(vClave).ListSubItems.Add , "REG", ArrBRR(2, NuMr)
        Me.lstBarras.ListItems(vClave).ListSubItems.Add , "COM", ArrBRR(3, NuMr)
    Next
    Call lstBarras_Click
FALLO:
NOENC:
End Sub

Private Function MalaInformacion(ByRef Cual As String) As Boolean
    Cual = ""
    MalaInformacion = False
    If Len(Trim(Me.txtCDBarra.Text)) = 0 Then Cual = Cual & IIf(Len(Cual) = 0, "", ", ") & "CD Barras"
    If Len(Trim(Me.txtCIAL.Text)) = 0 Then Cual = Cual & IIf(Len(Cual) = 0, "", ", ") & "Nombre C/cial"
    If Len(Trim(Me.txtREGI.Text)) = 0 Then Cual = Cual & IIf(Len(Cual) = 0, "", ", ") & "Registro Sanitario"
    If Len(Cual) > 0 Then MalaInformacion = True
End Function

Private Sub lstArticulos_ColumnClick(ByVal ColumnHeader As MSComctlLib.ColumnHeader)
    Me.lstArticulos.SortKey = ColumnHeader.Index - 1
    Me.lstArticulos.Sorted = True
End Sub

Private Sub lstBarras_GotFocus()
    Me.tlbTERCEROS.Visible = False
    Me.tlbARTICULOS.Visible = False
    Me.tlbBARRAS.Visible = True
End Sub

Private Sub lstPROVEEDORES_ColumnClick(ByVal ColumnHeader As MSComctlLib.ColumnHeader)
    Me.lstPROVEEDORES.SortKey = ColumnHeader.Index - 1
    Me.lstPROVEEDORES.Sorted = True
End Sub

Private Sub lstTerceros_GotFocus()
    Me.tlbBARRAS.Buttons.Item("ADD").Enabled = True
    Me.tlbBARRAS.Buttons.Item("DEL").Enabled = True
    Me.tlbBARRAS.Buttons.Item("SAV").Enabled = False
    Me.tlbBARRAS.Buttons.Item("CAN").Enabled = False
    Me.tlbTERCEROS.Buttons.Item("ADD").Enabled = True
    Me.tlbTERCEROS.Buttons.Item("DEL").Enabled = True
    Me.tlbTERCEROS.Buttons.Item("SAV").Enabled = False
    Me.tlbTERCEROS.Buttons.Item("CAN").Enabled = False
    Me.tlbARTICULOS.Buttons.Item("CHA").Enabled = True
    Me.tlbARTICULOS.Visible = False
    Me.tlbBARRAS.Visible = False
    Me.tlbTERCEROS.Visible = True
End Sub

Private Sub tlbBARRAS_ButtonClick(ByVal Button As MSComctlLib.Button)
    Me.tlbBARRAS.Buttons.Item("ADD").Enabled = False
    Me.tlbBARRAS.Buttons.Item("DEL").Enabled = False
    Me.tlbBARRAS.Buttons.Item("SAV").Enabled = False
    Me.tlbBARRAS.Buttons.Item("CAN").Enabled = False
    Select Case Button.Key
    Case Is = "ADD"
        If Not Me.lstArticulos.ListItems.Count > 0 Then Exit Sub
        Me.frmCAMBIOS.Enabled = True
        Me.lstTerceros.Enabled = False
        Me.lstArticulos.Enabled = False
        Me.lstBarras.Enabled = False
        Me.tlbBARRAS.Buttons.Item("SAV").Enabled = True
        Me.tlbBARRAS.Buttons.Item("CAN").Enabled = True
        Me.txtCDBarra.SetFocus
'        Call lstArticulos_Click
    Case Is = "SAV"
        Me.tlbBARRAS.Buttons.Item("ADD").Enabled = True
        Me.tlbBARRAS.Buttons.Item("DEL").Enabled = True
        Me.lstArticulos.Enabled = True
        Me.lstBarras.Enabled = True
        Me.lstTerceros.Enabled = True
        Me.frmCAMBIOS.Enabled = False
        If MalaInformacion(vClave) Then
        Else
'IN_CODIGOBAR(NU_AUTO_ARTI_COBA, TX_COBA_COBA, TX_REGI_COBA, TX_CIAL_COBA)
'            Condi = "NU_AUTO_ARTI_COBA=" & Me.lstArticulos.SelectedItem.Tag
            Condi = "TX_COBA_COBA='" & Trim(Me.txtCDBarra.Text) & "'"
            ReDim ArrBRR(0)
            Result = LoadMulData("IN_CODIGOBAR", "TX_COBA_COBA", Condi, ArrBRR())
            If Result = FAIL Then Exit Sub
            If Encontro Then Exit Sub
            Valores = "NU_AUTO_ARTI_COBA=" & Me.lstArticulos.SelectedItem.Tag
            Valores = Valores & ",TX_COBA_COBA='" & Trim(Me.txtCDBarra.Text) & "'"
            Valores = Valores & ",TX_REGI_COBA='" & Trim(Me.txtREGI.Text) & "'"
            Valores = Valores & ",TX_CIAL_COBA='" & Trim(Me.txtCIAL.Text) & "'"
            Result = DoInsertSQL("IN_CODIGOBAR", Valores)
        End If
    Case Is = "CAN"
        Me.tlbBARRAS.Buttons.Item("ADD").Enabled = True
        Me.tlbBARRAS.Buttons.Item("DEL").Enabled = True
        Me.lstArticulos.Enabled = True
        Me.lstBarras.Enabled = True
        Me.lstTerceros.Enabled = True
        Me.frmCAMBIOS.Enabled = False
    Case Is = "DEL"
        For Each vUno In Me.lstBarras.ListItems
            If vUno.Selected Then
                Condi = "TX_COBA_COBA_RCBTE='" & Trim(Me.lstBarras.SelectedItem.Text) & "'"
                Result = DoDelete("IN_R_COBA_TERC", Condi)
                Condi = "TX_COBA_COBA_RCBTE='" & Trim(Me.lstBarras.SelectedItem.Text) & "'"
                Result = DoDelete("IN_R_COBA_TERC", Condi)
'IN_CODIGOBAR(NU_AUTO_ARTI_COBA, TX_COBA_COBA, TX_REGI_COBA, TX_CIAL_COBA)
                Condi = "NU_AUTO_ARTI_COBA=" & Me.lstArticulos.SelectedItem.Tag
                Condi = Condi & " AND TX_COBA_COBA='" & Trim(vUno.Text) & "'"
                Result = DoDelete("IN_CODIGOBAR", Condi)
            End If
        Next
        Call lstArticulos_Click
    End Select
End Sub

Private Sub tlbTERCEROS_ButtonClick(ByVal Button As MSComctlLib.Button)
    Dim vCuerda As String
    Me.tlbTERCEROS.Buttons.Item("ADD").Enabled = False
    Me.tlbTERCEROS.Buttons.Item("DEL").Enabled = False
    Me.tlbTERCEROS.Buttons.Item("SAV").Enabled = False
    Me.tlbTERCEROS.Buttons.Item("CAN").Enabled = False
    Select Case Button.Key
    Case Is = "ADD"
        Me.tlbTERCEROS.Buttons.Item("SAV").Enabled = True
        Me.tlbTERCEROS.Buttons.Item("CAN").Enabled = True
        If Not Me.lstBarras.ListItems.Count > 0 Then Exit Sub
'SELECT CD_CODI_TERC, NO_NOMB_TERC
'FROM TERCERO;
'        Me.lstPROVEEDORES.ListItems.Clear
'        Campos = "CD_CODI_TERC, NO_NOMB_TERC"
'        Desde = "TERCERO"
'        Condi = ""
'        For Each vUno In Me.lstTerceros.ListItems
'            Condi = Condi & IIf(Len(Condi) = 0, "", ",") & "'" & Trim(vUno.Text) & "'"
'        Next
'        If Len(Condi) > 0 Then Condi = "CD_CODI_TERC NOT IN (" & Condi & ")"
'        If Len(Condi) > 0 Then Condi = Condi & " ORDER BY NO_NOMB_TERC"
'        If Len(Condi) = 0 Then Desde = Desde & " ORDER BY NO_NOMB_TERC"
'        ReDim ArrUXB(1, 0)
'        Result = LoadMulData(Desde, Campos, Condi, ArrUXB())
'        If Result = FAIL Then Exit Sub
'        If Not Encontro Then GoTo NOENC
'        For vContar = 0 To UBound(ArrUXB, 2)
'            vClave = "P" & ArrUXB(0, vContar)
'            Me.lstPROVEEDORES.ListItems.Add , vClave, ArrUXB(0, vContar)
'            Me.lstPROVEEDORES.ListItems.Item(vClave).Tag = ArrUXB(0, vContar)
'            Me.lstPROVEEDORES.ListItems(vClave).ListSubItems.Add , "NOM", ArrUXB(1, vContar)
'        Next
        Me.lstArticulos.Visible = False
        Me.lstBarras.Enabled = False
        Me.lstTerceros.Enabled = False
        Me.lstPROVEEDORES.Visible = True
'        Call lstArticulos_Click
    Case Is = "CAN"
'        If Not Me.lstTerceros.ListItems.Count > 0 Then Exit Sub
        Me.tlbTERCEROS.Buttons.Item("ADD").Enabled = True
        Me.tlbTERCEROS.Buttons.Item("DEL").Enabled = True
        Me.lstArticulos.Visible = True
        Me.lstBarras.Enabled = True
        Me.lstTerceros.Enabled = True
        Me.lstPROVEEDORES.Visible = False
    Case Is = "SAV"
        vCuerda = ""
        For Each vUno In Me.lstTerceros.ListItems
            vCuerda = vCuerda & IIf(Len(Condi) = 0, "", "/") & Trim(vUno.Text)
        Next
        For Each vDos In Me.lstBarras.ListItems
            If vDos.Selected Then Exit For
        Next
        If Not vDos.Selected Then Exit Sub
        Me.tlbTERCEROS.Buttons.Item("ADD").Enabled = True
        Me.tlbTERCEROS.Buttons.Item("DEL").Enabled = True
        For Each vUno In Me.lstPROVEEDORES.ListItems
            If vUno.Selected Then
                If Not InStr(1, vCuerda, Trim(vUno.Text), vbBinaryCompare) > 0 Then
'IN_R_CODIGOBAR_TERCERO(TX_COBA_COBA_RCBTE,CD_CODI_TERC_RCBTE,NU_MULT_RCBTE,NU_DIVI_RCBTE,NU_COST_RCBTE,NU_IMPU_RCBTE)
                    Valores = "TX_COBA_COBA_RCBTE='" & vDos.Text & "'"
                    Valores = Valores & ", CD_CODI_TERC_RCBTE='" & Trim(vUno.Text) & "'"
                    Valores = Valores & ", NU_MULT_RCBTE=" & Me.txtMULT.Text
                    Valores = Valores & ", NU_DIVI_RCBTE=" & Me.txtDIVI.Text
                    Valores = Valores & ", NU_COST_RCBTE=" & Me.txtCOSTO.Text
                    Valores = Valores & ", NU_IMPU_RCBTE=" & Me.txtIMPUES.Text
                    Result = DoInsertSQL("IN_R_COBA_TERC", Valores)
                End If
            End If
        Next
        Me.lstArticulos.Visible = True
        Me.lstBarras.Enabled = True
        Me.lstTerceros.Enabled = True
        Me.lstPROVEEDORES.Visible = False
        Call lstArticulos_Click
    Case Is = "DEL"
        For Each vUno In Me.lstTerceros.ListItems
            If vUno.Selected Then
                If MsgBox("Elimino:" & vUno.Text, vbYesNo) = vbYes Then
                    Condi = "TX_COBA_COBA_RCBTE='" & Trim(Me.lstBarras.SelectedItem.Text) & "'"
                    Condi = Condi & " AND CD_CODI_TERC_RCBTE='" & Trim(vUno.Text) & "'"
                    Result = DoDelete("IN_R_COBA_TERC", Condi)
                End If
            End If
        Next
        Me.tlbTERCEROS.Buttons.Item("ADD").Enabled = True
        Me.tlbTERCEROS.Buttons.Item("DEL").Enabled = True
        Call lstArticulos_Click
    End Select
NOENC:
End Sub

