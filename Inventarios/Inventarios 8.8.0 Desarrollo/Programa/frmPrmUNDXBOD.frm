VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "Mscomctl.ocx"
Begin VB.Form frmPrmUNDXBOD 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Unidades X Bodega"
   ClientHeight    =   7950
   ClientLeft      =   45
   ClientTop       =   435
   ClientWidth     =   11550
   Icon            =   "frmPrmUNDXBOD.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   7950
   ScaleWidth      =   11550
   Begin MSComctlLib.ListView lstUnidades 
      Height          =   1815
      Left            =   480
      TabIndex        =   3
      Top             =   5640
      Visible         =   0   'False
      Width           =   10335
      _ExtentX        =   18230
      _ExtentY        =   3201
      View            =   3
      LabelEdit       =   1
      MultiSelect     =   -1  'True
      LabelWrap       =   -1  'True
      HideSelection   =   -1  'True
      FullRowSelect   =   -1  'True
      GridLines       =   -1  'True
      _Version        =   393217
      ForeColor       =   -2147483640
      BackColor       =   -2147483643
      BorderStyle     =   1
      Appearance      =   1
      NumItems        =   0
   End
   Begin MSComctlLib.ListView lstSeleccion 
      Height          =   3735
      Left            =   7320
      TabIndex        =   2
      Top             =   1560
      Visible         =   0   'False
      Width           =   3495
      _ExtentX        =   6165
      _ExtentY        =   6588
      View            =   3
      LabelEdit       =   1
      MultiSelect     =   -1  'True
      LabelWrap       =   -1  'True
      HideSelection   =   -1  'True
      FullRowSelect   =   -1  'True
      GridLines       =   -1  'True
      _Version        =   393217
      ForeColor       =   -2147483640
      BackColor       =   -2147483643
      BorderStyle     =   1
      Appearance      =   1
      NumItems        =   0
   End
   Begin MSComctlLib.ListView lstArticulos 
      Height          =   5895
      Left            =   480
      TabIndex        =   1
      Top             =   1560
      Width           =   6420
      _ExtentX        =   11324
      _ExtentY        =   10398
      View            =   3
      LabelEdit       =   1
      LabelWrap       =   -1  'True
      HideSelection   =   -1  'True
      AllowReorder    =   -1  'True
      FullRowSelect   =   -1  'True
      GridLines       =   -1  'True
      _Version        =   393217
      ForeColor       =   -2147483640
      BackColor       =   -2147483643
      BorderStyle     =   1
      Appearance      =   1
      NumItems        =   2
      BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Text            =   "Nombre del Art�culo"
         Object.Width           =   7761
      EndProperty
      BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   1
         Text            =   "C�digo"
         Object.Width           =   3881
      EndProperty
   End
   Begin MSComctlLib.Toolbar tlbACCIONES 
      Align           =   1  'Align Top
      Height          =   630
      Left            =   0
      TabIndex        =   0
      Top             =   0
      Width           =   11550
      _ExtentX        =   20373
      _ExtentY        =   1111
      ButtonWidth     =   1535
      ButtonHeight    =   953
      AllowCustomize  =   0   'False
      _Version        =   393216
      BorderStyle     =   1
   End
   Begin MSComctlLib.ImageList imgBotones 
      Left            =   720
      Top             =   7080
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   20
      ImageHeight     =   14
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   8
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmPrmUNDXBOD.frx":058A
            Key             =   ""
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmPrmUNDXBOD.frx":08DC
            Key             =   "ASG"
         EndProperty
         BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmPrmUNDXBOD.frx":0C2E
            Key             =   "CAN"
         EndProperty
         BeginProperty ListImage4 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmPrmUNDXBOD.frx":0F80
            Key             =   "SAV"
         EndProperty
         BeginProperty ListImage5 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmPrmUNDXBOD.frx":12D2
            Key             =   "ADD"
         EndProperty
         BeginProperty ListImage6 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmPrmUNDXBOD.frx":1624
            Key             =   "DEL"
         EndProperty
         BeginProperty ListImage7 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmPrmUNDXBOD.frx":1A66
            Key             =   "CHA"
         EndProperty
         BeginProperty ListImage8 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmPrmUNDXBOD.frx":20E8
            Key             =   "DELETE"
         EndProperty
      EndProperty
   End
   Begin VB.Label lblInforma 
      Caption         =   "Informaci�n"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   480
      TabIndex        =   4
      Top             =   1095
      Width           =   10335
   End
End
Attribute VB_Name = "frmPrmUNDXBOD"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Dim ArrUXB() As Variant
Dim Articulo As UnArticulo
Dim LaBodega As String, LaAccion As String
Public OpcCod        As String   'Opci�n de seguridad
'NU_AUTO_ARTI, CD_CODI_ARTI, NO_NOMB_ARTI, NU_AUTO_UNVE_ARTI, TX_VENCE_ARTI, TX_PARA_ARTI,
'TX_ENTRA_ARTI, CD_GRUP_ARTI, CD_USOS_ARTI, VL_ULCO_ARTI, VL_COPR_ARTI, CT_EXIS_ARTI, DE_OBSE_ARTI, ID_TIPO_ARTIID_TIPO_ARTI, CD_RIPS_ARTI, NU_INDPYP_ARTI
Dim ArrDelete() As Variant

Private Sub Form_Load()
   'Dim CnTdr As Integer, Punto As MSComctlLib.Node
   'Dim CnTdr As Integer        'DEPURACION DE CODIGO 'JLPB T29135 SE DEJA EN COMENTARIO
   Dim CnTdr As Long 'JLPB T29135 'Utilizada para realizar el recorrido en el for de los articulos a listar
   'Call Main
   Call CenterForm(MDI_Inventarios, Me)
   Me.lblInforma.Caption = ""
   Me.lblInforma.Visible = False
   Me.tlbACCIONES.ImageList = Me.imgBotones
   Me.tlbACCIONES.Buttons.Clear
   Me.tlbACCIONES.Buttons.Add , "ASIGNAR", "&Asignar"
   Me.tlbACCIONES.Buttons("ASIGNAR").Image = "ASG"
   Me.tlbACCIONES.Buttons("ASIGNAR").Style = tbrDropdown
   Me.tlbACCIONES.Buttons.Add , "CAMBIAR", "&Cambiar"
   Me.tlbACCIONES.Buttons("CAMBIAR").Style = tbrDropdown
   Me.tlbACCIONES.Buttons("CAMBIAR").Image = "CHA"
   Me.tlbACCIONES.Buttons.Add , "GUARDAR", "&Guardar"
   Me.tlbACCIONES.Buttons("GUARDAR").Image = "SAV"
   Me.tlbACCIONES.Buttons("GUARDAR").Style = tbrDefault
   Me.tlbACCIONES.Buttons.Add , "CANCELAR", "&Cancelar"
   Me.tlbACCIONES.Buttons("CANCELAR").Image = "CAN"
   Me.tlbACCIONES.Buttons("CANCELAR").Style = tbrDefault
   Me.tlbACCIONES.Buttons("GUARDAR").Enabled = False
   Me.tlbACCIONES.Buttons("CANCELAR").Enabled = False
   ' DAHV R289(2461) - INICIO
   Me.tlbACCIONES.Buttons.Add , "ELIMINAR", "&Eliminar"
   Me.tlbACCIONES.Buttons("ELIMINAR").Image = "DELETE"
   Me.tlbACCIONES.Buttons("ELIMINAR").Enabled = False
   ' DAHV R289(2461) - FIN
   'NU_AUTO_BODE, NU_AUTO_BODE_BODE, TX_CODI_BODE, TX_NOMB_BODE, CD_CODI_CECO_BODE, TX_VENTA_BODE
   'FROM IN_BODEGA;
   Campos = "NU_AUTO_BODE, NU_AUTO_BODE_BODE, TX_CODI_BODE, TX_NOMB_BODE, " & _
   "CD_CODI_CECO_BODE, TX_VENTA_BODE"
   Desde = "IN_BODEGA ORDER BY TX_NOMB_BODE"
   Condi = ""
   ReDim ArrUXB(5, 0)
   Result = LoadMulData(Desde, Campos, Condi, ArrUXB())
   If Result = FAIL Then GoTo FALLO
   On Error GoTo SIGUI
   For CnTdr = 0 To UBound(ArrUXB, 2)
      Me.tlbACCIONES.Buttons("ASIGNAR").ButtonMenus.Add , "B" & ArrUXB(0, CnTdr), ArrUXB(3, CnTdr)
      Me.tlbACCIONES.Buttons("ASIGNAR").ButtonMenus("B" & ArrUXB(0, CnTdr)).Tag = CStr(ArrUXB(0, CnTdr))
      Me.tlbACCIONES.Buttons("CAMBIAR").ButtonMenus.Add , "B" & ArrUXB(0, CnTdr), ArrUXB(3, CnTdr)
      Me.tlbACCIONES.Buttons("CAMBIAR").ButtonMenus("B" & ArrUXB(0, CnTdr)).Tag = CStr(ArrUXB(0, CnTdr))
SIGUI:
   Next
   On Error GoTo SIGUI
   'NU_AUTO_UNVE, TX_CODI_UNVE, TX_NOMB_UNVE, NU_MULT_UNVE, NU_DIVI_UNVE
   'FROM IN_UNDVENTA;
    
   Campos = "NU_AUTO_UNVE, TX_CODI_UNVE, TX_NOMB_UNVE, NU_MULT_UNVE, NU_DIVI_UNVE"
   Desde = "IN_UNDVENTA"
   Condi = "NU_MULT_UNVE<>NU_DIVI_UNVE ORDER BY TX_NOMB_UNVE"
   ReDim ArrUXB(4, 0)
   Result = LoadMulData(Desde, Campos, Condi, ArrUXB())
   If Result = FAIL Then GoTo FALLO
   Me.lstUnidades.ListItems.Clear
   Me.lstUnidades.Width = 10500
   Me.lstUnidades.MultiSelect = False
   'lstUnidades, NOUN, AUUN, COUN, MULT, DIVI
   Me.lstUnidades.ColumnHeaders.Clear
   Me.lstUnidades.ColumnHeaders.Add , "NOUN", "Unidades disponibles", 0.4 * Me.lstUnidades.Width
   Me.lstUnidades.ColumnHeaders.Add , "AUUN", "AuUND", 0
   Me.lstUnidades.ColumnHeaders.Add , "COUN", "Unidad de distr.", 0.2 * Me.lstUnidades.Width
   Me.lstUnidades.ColumnHeaders.Add , "MULT", "Mul.", 0.1 * Me.lstUnidades.Width
   Me.lstUnidades.ColumnHeaders.Add , "DIVI", "Div.", 0.1 * Me.lstUnidades.Width
   For CnTdr = 0 To UBound(ArrUXB, 2)
      Me.lstUnidades.ListItems.Add , "U" & ArrUXB(0, CnTdr), ArrUXB(2, CnTdr)
      Me.lstUnidades.ListItems("U" & ArrUXB(0, CnTdr)).ListSubItems.Add , "AUUN", ArrUXB(0, CnTdr)
      Me.lstUnidades.ListItems("U" & ArrUXB(0, CnTdr)).ListSubItems.Add , "COUN", ArrUXB(1, CnTdr)
      Me.lstUnidades.ListItems("U" & ArrUXB(0, CnTdr)).ListSubItems.Add , "MULT", ArrUXB(3, CnTdr)
      Me.lstUnidades.ListItems("U" & ArrUXB(0, CnTdr)).ListSubItems.Add , "DIVI", ArrUXB(4, CnTdr)
   Next
    
   Campos = "NU_AUTO_ARTI, CD_CODI_ARTI, NO_NOMB_ARTI, NU_AUTO_UNVE_ARTI, TX_PARA_ARTI, " & _
   "DE_DESC_GRUP, DE_DESC_USOS, TX_NOMB_UNVE, CD_GRUP_ARTI, CD_USOS_ARTI, " & _
   "NU_MULT_UNVE, NU_DIVI_UNVE"
    
   'NU_AUTO_ARTI, CD_CODI_ARTI, NO_NOMB_ARTI, NU_AUTO_UNVE_ARTI, TX_PARA_ARTI, DE_DESC_GRUP, DE_DESC_USOS, TX_NOMB_UNVE
   'FROM ARTICULO, USOS, GRUP_ARTICULO, IN_UNDVENTA

   Desde = "ARTICULO, USOS, GRUP_ARTICULO, IN_UNDVENTA"
   Condi = "CD_CODI_GRUP = CD_GRUP_ARTI AND CD_CODI_USOS = CD_USOS_ARTI AND " & _
   "NU_AUTO_UNVE = NU_AUTO_UNVE_ARTI"
   ReDim ArrUXB(11, 0)
   Result = LoadMulData(Desde, Campos, Condi, ArrUXB())
   If Result = FAIL Then GoTo FALLO
   Me.tlbACCIONES.Buttons("ASIGNAR").Enabled = True
   Me.tlbACCIONES.Buttons("CAMBIAR").Enabled = True
   If Not Encontro Then GoTo NOENC
   Me.lstArticulos.ListItems.Clear
   Me.lstArticulos.Width = 10500
   Me.lstArticulos.ColumnHeaders.Clear
   'lstArticulos, NOAR, COAR, NOCO, NOUS, NOGR, AUCO, COGR, COUS
   Me.lstArticulos.ColumnHeaders.Add , "NOAR", "Nombre del art�culo", 0.4 * Me.lstArticulos.Width
   Me.lstArticulos.ColumnHeaders.Add , "COAR", "C�digo", 0.15 * Me.lstArticulos.Width
   Me.lstArticulos.ColumnHeaders.Add , "NOCO", "Unidad de conteo", 0.15 * Me.lstArticulos.Width
   Me.lstArticulos.ColumnHeaders.Add , "NOUS", "Uso", 0.15 * Me.lstArticulos.Width
   Me.lstArticulos.ColumnHeaders.Add , "NOGR", "Grupo", 0.15 * Me.lstArticulos.Width
   Me.lstArticulos.ColumnHeaders.Add , "AUCO", "AutoUNC", 0
   Me.lstArticulos.ColumnHeaders.Add , "COGR", "CodGRU", 0
   Me.lstArticulos.ColumnHeaders.Add , "COUS", "CodUSO", 0
   Me.lstArticulos.MultiSelect = True
   Dim Articulo As New UnArticulo
   For CnTdr = 0 To UBound(ArrUXB, 2)
      Articulo.AutoNumero = ArrUXB(0, CnTdr)
      Articulo.Codigo = ArrUXB(1, CnTdr)
      Articulo.Nombre = ArrUXB(2, CnTdr)
      Articulo.AutoUDConteo = ArrUXB(3, CnTdr)
      Articulo.NombreGrupo = ArrUXB(5, CnTdr)
      Articulo.NombreUso = ArrUXB(6, CnTdr)
      Articulo.NombreUndConteo = ArrUXB(7, CnTdr)
      Articulo.ParaQueEs = InStr(1, "VC", ArrUXB(4, CnTdr)) - 1
      Articulo.CodigoGrupo = ArrUXB(8, CnTdr)
      Articulo.CodigoUso = ArrUXB(9, CnTdr)
      Me.lstArticulos.ListItems.Add , "A" & Articulo.AutoNumero, Trim(Articulo.Nombre)
      Me.lstArticulos.ListItems("A" & Articulo.AutoNumero).Tag = CStr(Articulo.AutoNumero)
      'lstArticulos, NOAR, COAR, NOCO, NOUS, NOGR, AUCO, COGR, COUS, MUCO, DICO
      Me.lstArticulos.ListItems("A" & Articulo.AutoNumero).ListSubItems.Add , "COAR", Trim(Articulo.Codigo)
      Me.lstArticulos.ListItems("A" & Articulo.AutoNumero).ListSubItems.Add , "NOCO", Trim(Articulo.NombreUndConteo)
      Me.lstArticulos.ListItems("A" & Articulo.AutoNumero).ListSubItems.Add , "NOUS", Trim(Articulo.NombreUso)
      Me.lstArticulos.ListItems("A" & Articulo.AutoNumero).ListSubItems.Add , "NOGR", Trim(Articulo.NombreGrupo)
      Me.lstArticulos.ListItems("A" & Articulo.AutoNumero).ListSubItems.Add , "AUCO", Trim(Articulo.AutoUDConteo)
      Me.lstArticulos.ListItems("A" & Articulo.AutoNumero).ListSubItems.Add , "COGR", Trim(Articulo.CodigoGrupo)
      Me.lstArticulos.ListItems("A" & Articulo.AutoNumero).ListSubItems.Add , "COUS", Trim(Articulo.CodigoUso)
   Next
   Set Articulo = Nothing
NOENC:
FALLO:
End Sub

Private Sub lstARTICULOS_ColumnClick(ByVal ColumnHeader As MSComctlLib.ColumnHeader)
    Me.lstArticulos.SortKey = ColumnHeader.Index - 1
    Me.lstArticulos.Sorted = True
End Sub

Private Sub lstUnidades_ColumnClick(ByVal ColumnHeader As MSComctlLib.ColumnHeader)
    Me.lstUnidades.SortKey = ColumnHeader.Index - 1
    Me.lstUnidades.Sorted = True
End Sub

Private Sub lstUnidades_DblClick()
    Dim Uno As MSComctlLib.ListItem
    If Me.lstUnidades.SelectedItem.Text = "" Then Exit Sub 'AASV M3364 'NMSR M3578

        For Each Uno In Me.lstSeleccion.ListItems
            If Uno.Selected Then
    'lstUnidades, NOUN, AUUN, COUN, MULT, DIVI
    'lstArticulos, NOAR, COAR, NOCO, NOUS, NOGR, AUCO, COGR, COUS
    'lstSeleccion, NOAR, AUCO, NOCO, AUDI, NODI
                Uno.ListSubItems("AUDI").Text = Me.lstUnidades.SelectedItem.ListSubItems("AUUN").Text
                Uno.ListSubItems("FACT").Text = Format(CInt(Me.lstUnidades.SelectedItem.ListSubItems("MULT").Text) / CInt(Me.lstUnidades.SelectedItem.ListSubItems("DIVI").Text), "000.000")
                Uno.ListSubItems("NODI").Text = Me.lstUnidades.SelectedItem.Text
            End If
        Next
     
End Sub

Private Sub tlbACCIONES_ButtonClick(ByVal Boton As MSComctlLib.Button)
    Dim Uno As MSComctlLib.ListItem
    'DAHV R289 - INICIO
    Dim DbSaldo As Double
    Dim Incont As Integer
    'DAHV R289 - INICIO
    
    Me.lblInforma.Visible = False
    Select Case Boton.Key
    Case Is = "GUARDAR"
        Me.tlbACCIONES.Buttons("ASIGNAR").Enabled = True
        Me.tlbACCIONES.Buttons("CAMBIAR").Enabled = True
        Me.tlbACCIONES.Buttons("GUARDAR").Enabled = False
        Me.tlbACCIONES.Buttons("CANCELAR").Enabled = False
        'DAHV R289 - INICIO
        Me.tlbACCIONES.Buttons("ELIMINAR").Enabled = False
        'DAHV R289 - FIN
        Select Case LaAccion
        Case Is = "CAMBIAR"
            For Each Uno In Me.lstSeleccion.ListItems
                If Len(Uno.ListSubItems("AUDI").Text) > 0 Then
'NU_AUTO_BODE_RBAU, NU_AUTO_ARTI_RBAU, NU_AUTO_UNVE_RBAU
'IN_R_BODE_ARTI_UNDVENTA;
                    Valores = "NU_AUTO_UNVE_RBAU=" & Uno.ListSubItems("AUDI").Text
                    Condi = "NU_AUTO_BODE_RBAU=" & LaBodega & _
                        " AND NU_AUTO_ARTI_RBAU=" & Uno.Tag
                    Result = DoUpdate("IN_R_BODE_ARTI_UNDVENTA", Valores, Condi) 'LDCR 5187
                End If
            Next
        Case Is = "ASIGNAR"
            For Each Uno In Me.lstSeleccion.ListItems
                If Len(Uno.ListSubItems("AUDI").Text) > 0 Then
'NU_AUTO_BODE_RBAU, NU_AUTO_ARTI_RBAU, NU_AUTO_UNVE_RBAU
'IN_R_BODE_ARTI_UNDVENTA;
                    Valores = "NU_AUTO_UNVE_RBAU=" & Uno.ListSubItems("AUDI").Text & _
                        ", NU_AUTO_BODE_RBAU=" & LaBodega & _
                        ", NU_AUTO_ARTI_RBAU=" & Uno.Tag
                    Result = DoInsertSQL("IN_R_BODE_ARTI_UNDVENTA", Valores)
                End If
            Next
        'DAHV R289 - INICIO
        Case Is = "ELIMINAR"
            For Incont = 0 To UBound(ArrDelete(), 2)
                Condicion = "NU_AUTO_UNVE_RBAU=" & ArrDelete(1, Incont)
                Condicion = Condicion & " AND  NU_AUTO_BODE_RBAU=" & LaBodega
                Condicion = Condicion & " AND  NU_AUTO_ARTI_RBAU=" & ArrDelete(0, Incont)
                Result = DoDelete("IN_R_BODE_ARTI_UNDVENTA", Condicion)
                If Result <> FAIL Then Call Auditor("IN_R_BODE_ARTI_UNDVENTA", TranDel, LastCmd)
            Next Incont
        'DAHV R289 - FIN
        End Select
        Me.lstSeleccion.Visible = False
        Me.lstUnidades.Visible = False
        Me.lstArticulos.Visible = True
    Case Is = "CANCELAR"
        Me.tlbACCIONES.Buttons("ASIGNAR").Enabled = True
        Me.tlbACCIONES.Buttons("CAMBIAR").Enabled = True
        Me.tlbACCIONES.Buttons("GUARDAR").Enabled = False
        Me.tlbACCIONES.Buttons("CANCELAR").Enabled = False
        'DAHV R289 - INICIO
        Me.tlbACCIONES.Buttons("ELIMINAR").Enabled = False
        'DAHV R289 - FIN
        Me.lstSeleccion.Visible = False
        Me.lstSeleccion.ListItems.Clear     'PedroJ
        Me.lstUnidades.Visible = False
        Me.lstArticulos.Visible = True
        
    Case Is = "ELIMINAR"
        
        ' DAHV T2938 - INICIO
        ' Se incluye mensaje de confirmaci�n
        If MsgBox("�Desea quitar la(s) unidad(es) de distribuci�n?", vbYesNo + vbQuestion) = vbYes Then
                    ReDim ArrDelete(1, 0)
                    Incont = 0
                    LaAccion = "ELIMINAR"
                    
                    For Each Uno In Me.lstSeleccion.ListItems
                        If Uno.Selected Then
                            DbSaldo = FnSaldoArti(CDbl(Uno.Tag), CDbl(LaBodega))
                            If DbSaldo = 0 Then
                                    DbSaldo = FnSaldoReq(CDbl(Uno.Tag), CDbl(LaBodega))
                                    If DbSaldo = 0 Then
                                            DbSaldo = FnSaldodesp(CDbl(Uno.Tag), CDbl(LaBodega))
                                            If DbSaldo = 0 Then
                                                    ReDim Preserve ArrDelete(1, Incont)
                                                    ArrDelete(0, Incont) = Uno.Tag
                                                    ArrDelete(1, Incont) = Uno.ListSubItems("AUDI").Text
                                                    
                                                    Uno.ListSubItems("AUDI").Text = ""
                                                    Uno.ListSubItems("FACT").Text = ""
                                                    Uno.ListSubItems("NODI").Text = ""
                                                    
                                                    Incont = Incont + 1
                                            Else
                                                    Call Mensaje1("No se puede eliminar la unidad porque el art�culo tiene despachos pendientes en la bodega seleccionada", 1)
                                                    Exit Sub
                                            End If
                                     Else
                                            Call Mensaje1("No se puede eliminar la unidad porque el art�culo tiene requisiciones pendientes en la bodega seleccionada", 1)
                                            Exit Sub
                                     End If
                             Else
                                    Call Mensaje1("No se puede eliminar la unidad porque el art�culo tiene existencias en la bodega seleccionada", 1)
                                    Exit Sub
                             End If
                        End If
                        
                    Next
           End If
    End Select
End Sub

'Private Sub Form_Unload(Cancel As Integer)
'    BD(BDCur).Close
'End Sub

'DEPURACION DE CODIGO
'Private Sub txtIDEN_KeyPress(Tecla As Integer)
'    Call txtNOMB_KeyPress(Tecla)
'End Sub

'DEPURACION DE CODIGO
'Private Sub txtNOMB_KeyPress(Tecla As Integer)
'    Dim Cara As String * 1
'    Cara = Chr(Tecla)
'    If Cara = vbBack Then Exit Sub
'    If Asc(Cara) > 96 And Asc(Tecla) < 113 Then Tecla = Asc(UCase(Chr(Tecla)))
'    If InStr(" 01234567890ABCDEFGHIJKLMNOPQRSTUVWXYZ", Chr(Tecla)) = 0 Then Tecla = 0
'End Sub

'DEPURACION DE CODIGO
'Private Function MalaInformacion(ByRef Cual As String) As Boolean
'    Cual = "Versi�n de prueba"
''    MalaInformacion = False
''    If Len(Trim(Me.txtNOMB)) = 0 Then Cual = Cual & IIf(Len(Cual) = 0, "", ", ") & "Nombre"
''    If Len(Trim(Me.txtIDEN)) = 0 Then Cual = Cual & IIf(Len(Cual) = 0, "", ", ") & "C�digo"
''    If IsNumeric(Me.txtMULT.Text) Then
''        If CInt(Me.txtMULT.Text) < 1 Then Cual = Cual & IIf(Len(Cual) = 0, "", ", ") & "Multiplica"
''    Else
''        Cual = Cual & IIf(Len(Cual) = 0, "", ", ") & "Multiplica"
''    End If
''    If IsNumeric(Me.txtDIVI.Text) Then
''        If CInt(Me.txtDIVI.Text) < 1 Then Cual = Cual & IIf(Len(Cual) = 0, "", ", ") & "Divide"
''    Else
''        Cual = Cual & IIf(Len(Cual) = 0, "", ", ") & "Divide"
''    End If
'    If Len(Cual) > 0 Then MalaInformacion = True
'End Function

Private Sub tlbACCIONES_ButtonMenuClick(ByVal ButtonMenu As MSComctlLib.ButtonMenu)
    Dim CnTr As Integer
    Dim Uno As MSComctlLib.ListItem
    Dim ClaSelect As String, ClaBorrar As String, AutBuscar As String
    '   DAHV T2937 - INICIO
        Dim StPermiso As String
        
        Campos = "TX_ANUL_PERM"
        Desde = "PERFIL,PERMISO, OPCION"
        Condicion = "OPCION.NU_AUTO_OPCI  = PERMISO.NU_AUTO_OPCI_PERM"
        Condicion = Condicion & " AND PERFIL.NU_AUTO_PERF = PERMISO.NU_AUTO_PERF_PERM"
        Condicion = Condicion & " AND OPCION.TX_DESC_OPCI = 'UNIDADES X BODEGA'"
        Condicion = Condicion & " AND NU_AUTO_PERF = " & AutonumPerfil
        
        StPermiso = fnDevDato(Desde, Campos, Condicion)
    
    ' DAHV T2937 - FIN
    
    
    LaBodega = ButtonMenu.Tag
    LaAccion = ButtonMenu.Parent.Key
    Select Case LaAccion
    Case Is = "ASIGNAR"
        Me.lblInforma.Caption = "Asignando unidades en la bodega: " & ButtonMenu.Text
        Me.lblInforma.Visible = True
        Me.tlbACCIONES.Buttons("ASIGNAR").Enabled = False
        Me.tlbACCIONES.Buttons("CAMBIAR").Enabled = False
        Me.tlbACCIONES.Buttons("GUARDAR").Enabled = True
        Me.tlbACCIONES.Buttons("CANCELAR").Enabled = True
        'DAHV R289 - INICIO
        Me.tlbACCIONES.Buttons("ELIMINAR").Enabled = False
        'DAHV R289 - FIN
        
        Me.lstArticulos.Visible = False
        Me.lstSeleccion.Top = Me.lstArticulos.Top
        Me.lstSeleccion.Left = Me.lstArticulos.Left
        Me.lstSeleccion.Width = Me.lstArticulos.Width
        Me.lstUnidades.Left = Me.lstArticulos.Left
        Me.lstUnidades.Width = Me.lstArticulos.Width
'NU_AUTO_BODE_RBAU, NU_AUTO_ARTI_RBAU, NU_AUTO_UNVE_RBAU
'IN_R_BODE_ARTI_UNDVENTA;
        Campos = "NU_AUTO_BODE_RBAU, NU_AUTO_ARTI_RBAU, NU_AUTO_UNVE_RBAU"
        Desde = "IN_R_BODE_ARTI_UNDVENTA"
        Condi = "NU_AUTO_BODE_RBAU=" & ButtonMenu.Tag
        ReDim ArrUXB(2, 0)
        Result = LoadMulData(Desde, Campos, Condi, ArrUXB())
        If Result = FAIL Then GoTo FALLO
        For CnTr = 0 To UBound(ArrUXB, 2)
            AutBuscar = AutBuscar & IIf(Len(AutBuscar) = 0, "0", ",0") & ArrUXB(1, CnTr)
        Next
FALLO:
        For Each Uno In Me.lstSeleccion.ListItems
            If Me.lstArticulos.ListItems(Uno.Key).Selected Then
                ClaSelect = ClaSelect & IIf(Len(ClaSelect) = 0, "0", ",0") & Uno.Tag
                Me.lstArticulos.ListItems(Uno.Key).Selected = False
            Else
                ClaBorrar = ClaBorrar & IIf(Len(ClaBorrar) = 0, "0", ",0") & Uno.Tag
            End If
        Next
'lstArticulos, NOAR, COAR, NOCO, NOUS, NOGR, AUCO, COGR, COUS
'lstSeleccion, NOAR, AUCO, NOCO, AUDI, NODI
        Me.lstSeleccion.ColumnHeaders.Clear
        Me.lstSeleccion.ColumnHeaders.Add , "NOAR", "Art�culos para esa bodega", 0.4 * Me.lstSeleccion.Width
        Me.lstSeleccion.ColumnHeaders.Add , "AUCO", "AutoCON", 0
        Me.lstSeleccion.ColumnHeaders.Add , "NOCO", "Und de Conteo", 0.2 * Me.lstSeleccion.Width
        Me.lstSeleccion.ColumnHeaders.Add , "AUDI", "AutoDIS", 0
        Me.lstSeleccion.ColumnHeaders.Add , "FACT", "Factor", 0.1 * Me.lstSeleccion.Width
        Me.lstSeleccion.ColumnHeaders.Add , "NODI", "Und de Distr.", 0.2 * Me.lstSeleccion.Width
        
        Dim Ardclear() As String 'JACC M5568
        Dim BoEncontro As Boolean 'JACC M5568
        Dim InI As Integer 'JACC M5568
        
        Ardclear = Split(ClaBorrar, ",") 'JACC M5568
        
        For Each Uno In Me.lstArticulos.ListItems
            'If InStr(1, ClaBorrar, "0" & Uno.Tag) Then

'            'JACC M5568 SE COLOCA EN COMMENTARIO
'            If ClaBorrar = "0" & Uno.Tag Then 'AASV M5568 Se modifica ya que se estaba verificando que una cadena este contenida dentro de otra no que sea iguales
'                Me.lstSeleccion.ListItems.Remove Uno.Key
'            Else
'                If InStr(1, AutBuscar, "0" & Uno.Tag) Then
'                    On Error Resume Next
'                    Me.lstSeleccion.ListItems.Remove Uno.Key
'                    On Error GoTo 0
'                    Uno.Selected = False
'                End If
'            End If
           'JACC M5568 FIN COMENTARIO
            
            ''JACC M5568
            BoEncontro = False
            For InI = 0 To UBound(Ardclear)
                  If "0" & Uno.Tag = Ardclear(InI) And Ardclear(InI) <> "0" Then Me.lstSeleccion.ListItems.Remove Uno.Key: BoEncontro = True: Exit For
            Next
            If BoEncontro = False Then
                For InI = 0 To UBound(ArrUXB, 2)
                  If Uno.Tag = ArrUXB(1, InI) Then
                     On Error Resume Next
                     Me.lstSeleccion.ListItems.Remove Uno.Key
                     On Error GoTo 0
                     Uno.Selected = False
                  End If
                Next
            End If
            'JACC M5568
            
            If Uno.Selected Then
                Me.lstSeleccion.ListItems.Add , "A" & Uno.Tag, Uno.Text
                Me.lstSeleccion.ListItems("A" & Uno.Tag).Tag = Uno.Tag
                Me.lstSeleccion.ListItems("A" & Uno.Tag).ListSubItems.Add , "AUCO", Uno.ListSubItems("AUCO").Text
                Me.lstSeleccion.ListItems("A" & Uno.Tag).ListSubItems.Add , "NOCO", Uno.ListSubItems("NOCO").Text
                Me.lstSeleccion.ListItems("A" & Uno.Tag).ListSubItems.Add , "AUDI", ""
                Me.lstSeleccion.ListItems("A" & Uno.Tag).ListSubItems.Add , "FACT", ""
                Me.lstSeleccion.ListItems("A" & Uno.Tag).ListSubItems.Add , "NODI", ""
            End If
            If InStr(1, ClaSelect, "0" & Uno.Tag) Then Uno.Selected = True
            If InStr(1, AutBuscar, "0" & Uno.Tag) Then Uno.Selected = True
        Next
        Me.lstSeleccion.Visible = True
        Me.lstUnidades.Visible = True
    Case Is = "CAMBIAR"
        Me.lblInforma.Caption = "Cambiando unidades en la bodega: " & ButtonMenu.Text
        Me.lblInforma.Visible = True
        Me.tlbACCIONES.Buttons("ASIGNAR").Enabled = False
        Me.tlbACCIONES.Buttons("CAMBIAR").Enabled = False
        Me.tlbACCIONES.Buttons("GUARDAR").Enabled = True
        Me.tlbACCIONES.Buttons("CANCELAR").Enabled = True
        '   DAHV T2937 - INICIO - Se incluye condicional de permiso
        'DAHV R289 - INICIO
        If StPermiso = "S" Then
            Me.tlbACCIONES.Buttons("ELIMINAR").Enabled = True
        End If
        'DAHV R289 - FIN
        Me.lstArticulos.Visible = False
        Me.lstSeleccion.Top = Me.lstArticulos.Top
        Me.lstSeleccion.Left = Me.lstArticulos.Left
        Me.lstSeleccion.Width = Me.lstArticulos.Width
        Me.lstUnidades.Left = Me.lstArticulos.Left
        Me.lstUnidades.Width = Me.lstArticulos.Width

'SELECT IN_R_BODE_ARTI_UNDVENTA.NU_AUTO_BODE_RBAU, IN_R_BODE_ARTI_UNDVENTA.NU_AUTO_ARTI_RBAU, IN_R_BODE_ARTI_UNDVENTA.NU_AUTO_UNVE_RBAU, IN_UNDVENTA.TX_CODI_UNVE, IN_UNDVENTA.TX_NOMB_UNVE, IN_UNDVENTA.NU_MULT_UNVE, IN_UNDVENTA.NU_DIVI_UNVE, ARTICULO.CD_CODI_ARTI, ARTICULO.NO_NOMB_ARTI
'FROM ARTICULO IN_UNDVENTA IN_R_BODE_ARTI_UNDVENTA ON IN_UNDVENTA.NU_AUTO_UNVE = IN_R_BODE_ARTI_UNDVENTA.NU_AUTO_UNVE_RBAU) ON ARTICULO.NU_AUTO_ARTI = IN_R_BODE_ARTI_UNDVENTA.NU_AUTO_ARTI_RBAU
'WHERE (((IN_R_BODE_ARTI_UNDVENTA.NU_AUTO_BODE_RBAU)=10));

        Campos = "NU_AUTO_BODE_RBAU, NU_AUTO_ARTI_RBAU, NU_AUTO_UNVE_RBAU, " & _
            "DISTRI.TX_CODI_UNVE AS DISCODUNVE, DISTRI.TX_NOMB_UNVE AS DISNOMUNVE, " & _
            "CONTEO.NU_MULT_UNVE AS CONMUL, CONTEO.NU_DIVI_UNVE AS CONDIV, CD_CODI_ARTI, " & _
            "NO_NOMB_ARTI, NU_AUTO_UNVE_ARTI, CONTEO.TX_CODI_UNVE AS CONCODUNVE, " & _
            "CONTEO.TX_NOMB_UNVE AS CONNOMUNVE, DISTRI.NU_MULT_UNVE AS DISMUL, " & _
            "DISTRI.NU_DIVI_UNVE AS DISDIV"
        Desde = "ARTICULO, IN_UNDVENTA AS CONTEO, IN_R_BODE_ARTI_UNDVENTA, IN_UNDVENTA AS DISTRI"
        Condi = "(DISTRI.NU_AUTO_UNVE=NU_AUTO_UNVE_RBAU) AND (CONTEO.NU_AUTO_UNVE=NU_AUTO_UNVE_ARTI) AND (NU_AUTO_ARTI=NU_AUTO_ARTI_RBAU) AND NU_AUTO_BODE_RBAU=" & ButtonMenu.Tag
        ReDim ArrUXB(13, 0)
        Result = LoadMulData(Desde, Campos, Condi, ArrUXB())
        If Result = FAIL Then GoTo FALLO
'lstArticulos, NOAR, COAR, NOCO, NOUS, NOGR, AUCO, COGR, COUS
'lstSeleccion, NOAR, AUCO, NOCO, AUDI, NODI
        Me.lstSeleccion.ListItems.Clear
        Me.lstSeleccion.ColumnHeaders.Clear
        Me.lstSeleccion.ColumnHeaders.Add , "NOAR", "Nombre del art�culo", 0.4 * Me.lstSeleccion.Width
        Me.lstSeleccion.ColumnHeaders.Add , "AUCO", "AutoCON", 0
        Me.lstSeleccion.ColumnHeaders.Add , "NOCO", "Und de Conteo", 0.2 * Me.lstSeleccion.Width
        Me.lstSeleccion.ColumnHeaders.Add , "AUDI", "AutoDIS", 0
        Me.lstSeleccion.ColumnHeaders.Add , "FACT", "Factor", 0.1 * Me.lstSeleccion.Width
        Me.lstSeleccion.ColumnHeaders.Add , "NODI", "Und de Distr.", 0.2 * Me.lstSeleccion.Width
        If Not Encontro Then GoTo NOENC
        For CnTr = 0 To UBound(ArrUXB, 2)
            Me.lstSeleccion.ListItems.Add , "A" & ArrUXB(1, CnTr), ArrUXB(8, CnTr)
            Me.lstSeleccion.ListItems("A" & ArrUXB(1, CnTr)).Tag = ArrUXB(1, CnTr)
            Me.lstSeleccion.ListItems("A" & ArrUXB(1, CnTr)).ListSubItems.Add , "AUCO", ArrUXB(9, CnTr)
            Me.lstSeleccion.ListItems("A" & ArrUXB(1, CnTr)).ListSubItems.Add , "NOCO", ArrUXB(11, CnTr)
            Me.lstSeleccion.ListItems("A" & ArrUXB(1, CnTr)).ListSubItems.Add , "AUDI", ArrUXB(2, CnTr)
            Me.lstSeleccion.ListItems("A" & ArrUXB(1, CnTr)).ListSubItems.Add , "FACT", Format(ArrUXB(12, CnTr) / ArrUXB(13, CnTr), "000.000")
            Me.lstSeleccion.ListItems("A" & ArrUXB(1, CnTr)).ListSubItems.Add , "NODI", ArrUXB(4, CnTr)
        Next
NOENC:
        Me.lstSeleccion.Visible = True
        Me.lstUnidades.Visible = True
    End Select
End Sub

Private Sub Form_KeyPress(KeyAscii As Integer)
    If KeyAscii = vbKeyReturn Then KeyAscii = vbKeyTab
End Sub

'---------------------------------------------------------------------------------------
' Procedure : FnSaldoArti
' DateTime  : 26/10/2010 17:03
' Author       : diego_hernandez
' Purpose     : DAHV R289(2461)
'---------------------------------------------------------------------------------------
'
Function FnSaldoArti(DbAutoArti As Double, DbAutoBode As Double) As Double

    Dim stResult As String
    
    'Campos = "TX_NOMB_BODE, CD_CODI_ARTI, NO_NOMB_ARTI, TX_NOMB_UNVE,  "
    'Campos = Campos & " CASE NU_ACTUDNM_KARD WHEN 0 THEN 0 ELSE ROUND(CAST(NU_ACTUNMR_KARD AS FLOAT)/CAST(NU_ACTUDNM_KARD AS FLOAT),0 ) END AS ENTRADAS"
    Campos = " CASE NU_ACTUDNM_KARD WHEN 0 THEN 0 ELSE ROUND(CAST(NU_ACTUNMR_KARD AS FLOAT)/CAST(NU_ACTUDNM_KARD AS FLOAT),0 ) END AS ENTRADAS"
    Desde = "IN_KARDEX, ARTICULO, IN_BODEGA, IN_UNDVENTA"
    Condicion = "NU_AUTO_KARD IN"
    Condicion = Condicion & " (SELECT MAX(NU_AUTO_KARD) FROM IN_KARDEX"
    Condicion = Condicion & " WHERE  (NU_AUTO_ARTI_KARD = " & DbAutoArti & " )"
    Condicion = Condicion & " AND (NU_AUTO_BODE_KARD= " & DbAutoBode & " )"
    Condicion = Condicion & " AND (NU_AUTO_KARD>=0)"
    Condicion = Condicion & " GROUP BY NU_AUTO_BODE_KARD, NU_AUTO_ARTI_KARD)"
    Condicion = Condicion & " AND NU_AUTO_ARTI_KARD=NU_AUTO_ARTI AND NU_AUTO_BODE_KARD=NU_AUTO_BODE AND NU_AUTO_UNVE_ARTI=NU_AUTO_UNVE AND NU_ACTUNMR_KARD<>0"
    
    stResult = fnDevDato(Desde, Campos, Condicion)
    
    If stResult = NUL$ Then stResult = "0"
    
    FnSaldoArti = CDbl(stResult)
    
End Function

'---------------------------------------------------------------------------------------
' Procedure : FnSaldoReq
' DateTime  : 26/10/2010 17:03
' Author       : diego_hernandez
' Purpose     : DAHV R289(2461) - Presentar el saldo de requsiciones del articulo seleccionado
'                     Requisiciones vs despacho
'---------------------------------------------------------------------------------------
'
'
Function FnSaldoReq(DbAutoArti As Double, DbAutoBode As Double) As Double

    Dim stResult As String
    
    Campos = "  (CONSULTA_TEMP.TOREQUISICION - CONSULTA_TEMP.TODEPACHO ) AS SALDO"
    Desde = " ARTICULO,"
    Desde = Desde & " (SELECT NU_ORDEN_OEPT,NU_ARTI_OEPT,SUM(CANTDEPACHO) TODEPACHO,SUM(CANTREQUISICION) TOREQUISICION,CD_CODI_TERC_ENCA"
    Desde = Desde & " FROM (SELECT NU_AUTO_MODCABE_DETA AS NU_ORDEN_OEPT,NU_AUTO_ARTI_DETA AS NU_ARTI_OEPT,"
    Desde = Desde & " SUM(NU_ENTRAD_DETA)AS CANTDEPACHO,0 AS CANTREQUISICION,CD_CODI_TERC_ENCA"
    Desde = Desde & " From IN_DETALLE, IN_ENCABEZADO"
    Desde = Desde & " WHERE NU_AUTO_ORGCABE_DETA IN  (SELECT NU_AUTO_ENCA"
    Desde = Desde & " From IN_ENCABEZADO"
    Desde = Desde & " Where NU_AUTO_DOCU_ENCA = 6"
    Desde = Desde & " AND TX_ESTA_ENCA<>'A')"
    Desde = Desde & " AND NU_AUTO_ORGCABE_DETA<>NU_AUTO_ENCA"
    Desde = Desde & " AND NU_AUTO_MODCABE_DETA=NU_AUTO_ENCA"
    Desde = Desde & " AND NU_AUTO_DOCU_ENCA=5"
    Desde = Desde & " AND NU_AUTO_BODEORG_ENCA IN ( " & DbAutoBode & ")"
    Desde = Desde & " AND TX_ESTA_ENCA='N'"
    Desde = Desde & " GROUP BY NU_AUTO_MODCABE_DETA,NU_AUTO_ARTI_DETA,NU_ENTRAD_DETA, NU_COSTO_DETA,CD_CODI_TERC_ENCA"
    Desde = Desde & " Union"
    Desde = Desde & " SELECT NU_AUTO_MODCABE_DETA AS NU_ORDEN_OEPT,NU_AUTO_ARTI_DETA AS NU_ARTI_OEPT,"
    Desde = Desde & " (-1)*SUM(NU_ENTRAD_DETA) AS CANTDEPACHO,0 AS CANTREQUISICION,  CD_CODI_TERC_ENCA"
    Desde = Desde & " From IN_DETALLE, IN_ENCABEZADO"
    Desde = Desde & " WHERE NU_AUTO_ORGCABE_DETA IN (SELECT NU_AUTO_ENCA"
    Desde = Desde & " From IN_ENCABEZADO"
    Desde = Desde & " Where NU_AUTO_DOCU_ENCA = 17"
    Desde = Desde & " AND NU_AUTO_BODEORG_ENCA IN ( " & DbAutoBode & ")"
    Desde = Desde & " AND TX_ESTA_ENCA='N')"
    Desde = Desde & " AND NU_AUTO_ORGCABE_DETA<>NU_AUTO_ENCA"
    Desde = Desde & " AND NU_AUTO_MODCABE_DETA=NU_AUTO_ENCA"
    Desde = Desde & " AND NU_AUTO_DOCU_ENCA=5"
    Desde = Desde & " AND NU_AUTO_BODEORG_ENCA IN ( " & DbAutoBode & ")"
    Desde = Desde & " AND TX_ESTA_ENCA='N'"
    Desde = Desde & " GROUP BY NU_AUTO_MODCABE_DETA,NU_AUTO_ARTI_DETA,NU_ENTRAD_DETA, NU_COSTO_DETA,CD_CODI_TERC_ENCA"
    Desde = Desde & " Union"
    Desde = Desde & " SELECT NU_AUTO_ENCA AS NU_ORDEN_OEPT, NU_AUTO_ARTI_DETA AS NU_ARTI_OEPT, 0 AS CANTDEPACHO,NU_SALIDA_DETA AS CANTREQUISICION,CD_CODI_TERC_ENCA"
    Desde = Desde & " From IN_DETALLE, IN_ENCABEZADO"
    Desde = Desde & " Where NU_AUTO_MODCABE_DETA = NU_AUTO_ENCA"
    Desde = Desde & " AND NU_AUTO_ORGCABE_DETA=NU_AUTO_ENCA"
    Desde = Desde & " AND NU_AUTO_DOCU_ENCA=5"
    Desde = Desde & " AND NU_AUTO_BODEORG_ENCA IN ( " & DbAutoBode & ")"
    Desde = Desde & " AND TX_ESTA_ENCA='N'"
    Desde = Desde & " GROUP BY NU_AUTO_ENCA,NU_AUTO_ARTI_DETA ,NU_SALIDA_DETA,CD_CODI_TERC_ENCA ) AS T2"
    Desde = Desde & " GROUP BY NU_ORDEN_OEPT,NU_ARTI_OEPT,CD_CODI_TERC_ENCA"
    Desde = Desde & " Having SUM(CANTDEPACHO) <> SUM(CANTREQUISICION) And SUM(CANTDEPACHO) < SUM(CANTREQUISICION)"
    Desde = Desde & " ) AS CONSULTA_TEMP"
    Condicion = " CONSULTA_TEMP.NU_ARTI_OEPT = Articulo.NU_AUTO_ARTI"
    Condicion = Condicion & " AND ARTICULO.NU_AUTO_ARTI = " & DbAutoArti
    
    
        
    stResult = fnDevDato(Desde, Campos, Condicion)
    
    If stResult = NUL$ Then stResult = "0"
    
    FnSaldoReq = CDbl(stResult)
    
End Function


'---------------------------------------------------------------------------------------
' Procedure : FnSaldoReq
' DateTime  : 26/10/2010 17:03
' Author       : diego_hernandez
' Purpose     : DAHV R289(2461) - Presentar el saldo de requsiciones del articulo seleccionado
'                     Requisiciones vs despacho
'---------------------------------------------------------------------------------------
'
'
Function FnSaldodesp(DbAutoArti As Double, DbAutoBode As Double) As Double

    Dim stResult As String
    
    Campos = "  (CONSULTA_TEMP.TOREQUISICION - CONSULTA_TEMP.TODEPACHO ) AS SALDO"
    Desde = " ARTICULO,"
    Desde = Desde & " (SELECT NU_ORDEN_OEPT,NU_ARTI_OEPT,SUM(CANTDEPACHO) TODEPACHO,SUM(CANTREQUISICION) TOREQUISICION,CD_CODI_TERC_ENCA"
    Desde = Desde & " FROM (SELECT NU_AUTO_MODCABE_DETA AS NU_ORDEN_OEPT,NU_AUTO_ARTI_DETA AS NU_ARTI_OEPT,"
    Desde = Desde & " SUM(NU_ENTRAD_DETA)AS CANTDEPACHO,0 AS CANTREQUISICION,CD_CODI_TERC_ENCA"
    Desde = Desde & " From IN_DETALLE, IN_ENCABEZADO"
    Desde = Desde & " WHERE NU_AUTO_ORGCABE_DETA IN  (SELECT NU_AUTO_ENCA"
    Desde = Desde & " From IN_ENCABEZADO"
    Desde = Desde & " Where NU_AUTO_DOCU_ENCA = 8"
    Desde = Desde & " AND TX_ESTA_ENCA<>'A')"
    Desde = Desde & " AND NU_AUTO_ORGCABE_DETA<>NU_AUTO_ENCA"
    Desde = Desde & " AND NU_AUTO_MODCABE_DETA=NU_AUTO_ENCA"
    Desde = Desde & " AND NU_AUTO_DOCU_ENCA=6"
    Desde = Desde & " AND NU_AUTO_BODEORG_ENCA IN ( " & DbAutoBode & ")"
    Desde = Desde & " AND TX_ESTA_ENCA='N'"
    Desde = Desde & " GROUP BY NU_AUTO_MODCABE_DETA,NU_AUTO_ARTI_DETA,NU_ENTRAD_DETA, NU_COSTO_DETA,CD_CODI_TERC_ENCA"
    Desde = Desde & " Union"
    Desde = Desde & " SELECT NU_AUTO_MODCABE_DETA AS NU_ORDEN_OEPT,NU_AUTO_ARTI_DETA AS NU_ARTI_OEPT,"
    Desde = Desde & " (-1)*SUM(NU_ENTRAD_DETA) AS CANTDEPACHO,0 AS CANTREQUISICION,  CD_CODI_TERC_ENCA"
    Desde = Desde & " From IN_DETALLE, IN_ENCABEZADO"
    Desde = Desde & " WHERE NU_AUTO_ORGCABE_DETA IN (SELECT NU_AUTO_ENCA"
    Desde = Desde & " From IN_ENCABEZADO"
    Desde = Desde & " Where NU_AUTO_DOCU_ENCA = 9"
    Desde = Desde & " AND NU_AUTO_BODEORG_ENCA IN ( " & DbAutoBode & ")"
    Desde = Desde & " AND TX_ESTA_ENCA='N')"
    Desde = Desde & " AND NU_AUTO_ORGCABE_DETA<>NU_AUTO_ENCA"
    Desde = Desde & " AND NU_AUTO_MODCABE_DETA=NU_AUTO_ENCA"
    Desde = Desde & " AND NU_AUTO_DOCU_ENCA=6"
    Desde = Desde & " AND NU_AUTO_BODEORG_ENCA IN ( " & DbAutoBode & ")"
    Desde = Desde & " AND TX_ESTA_ENCA='N'"
    Desde = Desde & " GROUP BY NU_AUTO_MODCABE_DETA,NU_AUTO_ARTI_DETA,NU_ENTRAD_DETA, NU_COSTO_DETA,CD_CODI_TERC_ENCA"
    Desde = Desde & " Union"
    Desde = Desde & " SELECT NU_AUTO_ENCA AS NU_ORDEN_OEPT, NU_AUTO_ARTI_DETA AS NU_ARTI_OEPT, 0 AS CANTDEPACHO,NU_SALIDA_DETA AS CANTREQUISICION,CD_CODI_TERC_ENCA"
    Desde = Desde & " From IN_DETALLE, IN_ENCABEZADO"
    Desde = Desde & " Where NU_AUTO_MODCABE_DETA = NU_AUTO_ENCA"
    Desde = Desde & " AND NU_AUTO_ORGCABE_DETA=NU_AUTO_ENCA"
    Desde = Desde & " AND NU_AUTO_DOCU_ENCA=6"
    Desde = Desde & " AND NU_AUTO_BODEORG_ENCA IN ( " & DbAutoBode & ")"
    Desde = Desde & " AND TX_ESTA_ENCA='N'"
    Desde = Desde & " GROUP BY NU_AUTO_ENCA,NU_AUTO_ARTI_DETA ,NU_SALIDA_DETA,CD_CODI_TERC_ENCA ) AS T2"
    Desde = Desde & " GROUP BY NU_ORDEN_OEPT,NU_ARTI_OEPT,CD_CODI_TERC_ENCA"
    Desde = Desde & " Having SUM(CANTDEPACHO) <> SUM(CANTREQUISICION) And SUM(CANTDEPACHO) < SUM(CANTREQUISICION)"
    Desde = Desde & " ) AS CONSULTA_TEMP"
    Condicion = " CONSULTA_TEMP.NU_ARTI_OEPT = Articulo.NU_AUTO_ARTI"
    Condicion = Condicion & " AND ARTICULO.NU_AUTO_ARTI = " & DbAutoArti
    
    
        
    stResult = fnDevDato(Desde, Campos, Condicion)
    
    If stResult = NUL$ Then stResult = "0"
    
    FnSaldodesp = CDbl(stResult)
    
End Function


