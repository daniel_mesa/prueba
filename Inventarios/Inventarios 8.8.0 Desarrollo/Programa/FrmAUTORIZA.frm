VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Object = "{C932BA88-4374-101B-A56C-00AA003668DC}#1.1#0"; "MSMASK32.OCX"
Begin VB.Form FrmAUTORIZA 
   BorderStyle     =   1  'Fixed Single
   ClientHeight    =   7095
   ClientLeft      =   45
   ClientTop       =   435
   ClientWidth     =   11475
   Icon            =   "FrmAUTORIZA.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   7095
   ScaleWidth      =   11475
   Begin VB.Frame fraRANGO 
      Height          =   590
      Left            =   1440
      TabIndex        =   3
      Top             =   720
      Width           =   8655
      Begin VB.CheckBox chkRANGO 
         Caption         =   "Condicionar las fechas a mostrar"
         Height          =   315
         Left            =   240
         TabIndex        =   4
         Top             =   160
         Width           =   3135
      End
      Begin MSMask.MaskEdBox txtDESDE 
         Height          =   315
         Left            =   4320
         TabIndex        =   5
         Top             =   160
         Width           =   1215
         _ExtentX        =   2143
         _ExtentY        =   556
         _Version        =   393216
         PromptChar      =   "_"
      End
      Begin MSMask.MaskEdBox txtHASTA 
         Height          =   315
         Left            =   6600
         TabIndex        =   6
         Top             =   160
         Width           =   1215
         _ExtentX        =   2143
         _ExtentY        =   556
         _Version        =   393216
         PromptChar      =   "_"
      End
      Begin VB.Label Label1 
         Alignment       =   1  'Right Justify
         Caption         =   "Hasta:"
         Height          =   255
         Left            =   5760
         TabIndex        =   8
         Top             =   160
         Width           =   735
      End
      Begin VB.Label lblDESDE 
         Alignment       =   1  'Right Justify
         Caption         =   "Desde:"
         Height          =   255
         Left            =   3480
         TabIndex        =   7
         Top             =   160
         Width           =   735
      End
   End
   Begin MSComctlLib.ImageList imgBotones 
      Left            =   10920
      Top             =   3960
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   20
      ImageHeight     =   14
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   10
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FrmAUTORIZA.frx":058A
            Key             =   ""
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FrmAUTORIZA.frx":0C54
            Key             =   "ASG"
         EndProperty
         BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FrmAUTORIZA.frx":1CA6
            Key             =   "CAN"
         EndProperty
         BeginProperty ListImage4 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FrmAUTORIZA.frx":2238
            Key             =   "SAV"
         EndProperty
         BeginProperty ListImage5 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FrmAUTORIZA.frx":2902
            Key             =   "ADD"
         EndProperty
         BeginProperty ListImage6 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FrmAUTORIZA.frx":3554
            Key             =   "DEL"
         EndProperty
         BeginProperty ListImage7 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FrmAUTORIZA.frx":3C1E
            Key             =   "CHA"
         EndProperty
         BeginProperty ListImage8 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FrmAUTORIZA.frx":42A0
            Key             =   "TOD"
         EndProperty
         BeginProperty ListImage9 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FrmAUTORIZA.frx":496A
            Key             =   "CON"
         EndProperty
         BeginProperty ListImage10 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FrmAUTORIZA.frx":5034
            Key             =   "SIN"
         EndProperty
      EndProperty
   End
   Begin MSComctlLib.ListView lstCOMPRO 
      Height          =   2295
      Left            =   480
      TabIndex        =   0
      Top             =   1440
      Width           =   10500
      _ExtentX        =   18521
      _ExtentY        =   4048
      View            =   3
      LabelEdit       =   1
      LabelWrap       =   -1  'True
      HideSelection   =   0   'False
      AllowReorder    =   -1  'True
      Checkboxes      =   -1  'True
      FullRowSelect   =   -1  'True
      GridLines       =   -1  'True
      _Version        =   393217
      ForeColor       =   -2147483640
      BackColor       =   -2147483643
      BorderStyle     =   1
      Appearance      =   1
      NumItems        =   3
      BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Text            =   "Comprobante"
         Object.Width           =   3528
      EndProperty
      BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   1
         Text            =   "NIT"
         Object.Width           =   2540
      EndProperty
      BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   2
         Text            =   "NOMBRE"
         Object.Width           =   7056
      EndProperty
   End
   Begin MSComctlLib.Toolbar tlbACCIONES 
      Height          =   630
      Left            =   0
      TabIndex        =   1
      Top             =   0
      Width           =   11475
      _ExtentX        =   20241
      _ExtentY        =   1111
      ButtonWidth     =   1667
      ButtonHeight    =   953
      AllowCustomize  =   0   'False
      Wrappable       =   0   'False
      Appearance      =   1
      ImageList       =   "imgBotones"
      _Version        =   393216
      BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
         NumButtons      =   6
         BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "&Todos"
            Key             =   "TOD"
            Object.ToolTipText     =   "Ver todos los autorizados"
            ImageIndex      =   8
         EndProperty
         BeginProperty Button2 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "&Con"
            Key             =   "CON"
            Object.ToolTipText     =   "Ver los autorizados"
            ImageIndex      =   9
         EndProperty
         BeginProperty Button3 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "&Sin"
            Key             =   "SIN"
            Object.ToolTipText     =   "Los sin aurorizar"
            ImageIndex      =   10
         EndProperty
         BeginProperty Button4 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Style           =   4
            Object.Width           =   3000
         EndProperty
         BeginProperty Button5 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "&Guardar"
            Key             =   "SAV"
            Object.ToolTipText     =   "Gaurdar los cambios"
            ImageIndex      =   4
         EndProperty
         BeginProperty Button6 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "&Cancelar"
            Key             =   "CAN"
            Object.ToolTipText     =   "Cancela los cambios"
            ImageIndex      =   3
         EndProperty
      EndProperty
      BorderStyle     =   1
      OLEDropMode     =   1
      Begin VB.ComboBox cboCOMPRO 
         Height          =   315
         Left            =   2640
         TabIndex        =   2
         Text            =   "cboCOMPRO"
         Top             =   180
         Width           =   2775
      End
   End
   Begin MSComctlLib.ListView lstARTICULO 
      Height          =   3015
      Left            =   120
      TabIndex        =   9
      Top             =   3960
      Width           =   5820
      _ExtentX        =   10266
      _ExtentY        =   5318
      View            =   3
      LabelEdit       =   1
      LabelWrap       =   -1  'True
      HideSelection   =   0   'False
      AllowReorder    =   -1  'True
      FullRowSelect   =   -1  'True
      GridLines       =   -1  'True
      _Version        =   393217
      ForeColor       =   -2147483640
      BackColor       =   -2147483643
      BorderStyle     =   1
      Appearance      =   1
      NumItems        =   3
      BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Text            =   "Comprobante"
         Object.Width           =   3528
      EndProperty
      BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   1
         Text            =   "NIT"
         Object.Width           =   2540
      EndProperty
      BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   2
         Text            =   "NOMBRE"
         Object.Width           =   7056
      EndProperty
   End
   Begin MSComctlLib.ListView lstSALXBOD 
      Height          =   3015
      Left            =   6120
      TabIndex        =   10
      Top             =   3960
      Width           =   5175
      _ExtentX        =   9128
      _ExtentY        =   5318
      View            =   3
      LabelEdit       =   1
      LabelWrap       =   -1  'True
      HideSelection   =   0   'False
      Checkboxes      =   -1  'True
      FullRowSelect   =   -1  'True
      GridLines       =   -1  'True
      _Version        =   393217
      ForeColor       =   -2147483640
      BackColor       =   -2147483643
      BorderStyle     =   1
      Appearance      =   1
      NumItems        =   0
   End
End
Attribute VB_Name = "FrmAUTORIZA"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Public OpcCod        As String   'Opci�n de seguridad
Private vDocumento As New ElDocumento
Private vBodegas As New Collection
'Private vFilaCompro As Integer     'DEPURACION DE CODIGO
'Private vFilaArticulo As Integer       'DEPURACION DE CODIGO
Private vKeyComprobante As String
Private vKeyArticulo As String
Private vKeySaldosXBodega As String
Private vSeCargoVentana As Boolean
Private vArticulo As ElArticulo, VlrDocumento As Double
Private ArrUXB() As Variant, CnTdr As Integer, numCOMPRO


Public Property Get ClaveComprobante() As String
    ClaveComprobante = vKeyComprobante
End Property

Public Property Get ClaveArticulo() As String
    ClaveArticulo = vKeyArticulo
End Property

Public Property Get ClaveSaldosXBodega() As String
    ClaveSaldosXBodega = vKeySaldosXBodega
End Property

Public Property Get ValorDocumento() As Double
    ValorDocumento = VlrDocumento
End Property

Private Sub chkRANGO_Click()
    Me.txtDESDE.Enabled = False
    Me.txtHASTA.Enabled = False
    If Not CBool(Me.chkRANGO.Value) = True Then Exit Sub
    Me.txtDESDE.Enabled = True
    Me.txtHASTA.Enabled = True
End Sub

Private Sub cboCOMPRO_LostFocus()
    If numCOMPRO = Me.cboCOMPRO.ListIndex Then Exit Sub
    Me.tlbACCIONES.Buttons("TOD").Enabled = False
    Me.tlbACCIONES.Buttons("CON").Enabled = False
    Me.tlbACCIONES.Buttons("SIN").Enabled = False
    Me.tlbACCIONES.Buttons("SAV").Enabled = False
    Me.tlbACCIONES.Buttons("CAN").Enabled = False
    If Not Me.cboCOMPRO.ListIndex > -1 Then Exit Sub
    Me.tlbACCIONES.Buttons("TOD").Enabled = True
    Me.tlbACCIONES.Buttons("CON").Enabled = True
    Me.tlbACCIONES.Buttons("SIN").Enabled = True
    vSeCargoVentana = True
End Sub

Private Sub chkRANGO_GotFocus()
    numCOMPRO = Me.cboCOMPRO.ListIndex
End Sub

Private Sub Form_Load()
    Call CenterForm(MDI_Inventarios, Me)
    If Len(FormatDateTime(Date, vbShortDate)) = 8 Then
        Me.txtDESDE.Mask = "##/##/##"
        Me.txtHASTA.Mask = "##/##/##"
    Else
        Me.txtDESDE.Mask = "##/##/####"
        Me.txtHASTA.Mask = "##/##/####"
    End If
    Me.lstARTICULO.ColumnHeaders.Clear
    Me.lstARTICULO.Width = 1.5 * Me.lstARTICULO.Width
    Me.lstARTICULO.ColumnHeaders.Add , "COD", "C�digo", 0.15 * Me.lstARTICULO.Width
    Me.lstARTICULO.ColumnHeaders.Add , "NOM", "Nombre", 0.4 * Me.lstARTICULO.Width
    Me.lstARTICULO.ColumnHeaders.Add , "CAN", "C/dad", 0.13 * Me.lstARTICULO.Width
    Me.lstARTICULO.ColumnHeaders("CAN").Alignment = lvwColumnRight
    Me.lstARTICULO.ColumnHeaders.Add , "COS", "Costo", 0.11 * Me.lstARTICULO.Width
    Me.lstARTICULO.ColumnHeaders("COS").Alignment = lvwColumnRight
    Me.lstARTICULO.ColumnHeaders.Add , "TOT", "Total", 0.13 * Me.lstARTICULO.Width
    Me.lstARTICULO.ColumnHeaders("TOT").Alignment = lvwColumnRight
    Me.lstARTICULO.Width = Me.lstARTICULO.Width / 1.5
    Me.lstCOMPRO.ListItems.Clear
    Me.lstCOMPRO.ColumnHeaders.Clear
    Me.lstCOMPRO.ColumnHeaders.Add , "COM", "Comprobante", 0.12 * Me.lstCOMPRO.Width
    Me.lstCOMPRO.ColumnHeaders.Add , "FEC", "Fecha", 0.1 * Me.lstCOMPRO.Width
    Me.lstCOMPRO.ColumnHeaders.Add , "NIT", "Nit", 0.12 * Me.lstCOMPRO.Width
    Me.lstCOMPRO.ColumnHeaders.Add , "NOM", "Nombre", 0.4 * Me.lstCOMPRO.Width
    Me.lstCOMPRO.ColumnHeaders.Add , "AUE", "AutoEnca", 0 * Me.lstCOMPRO.Width
    Me.lstCOMPRO.ColumnHeaders.Add , "TOT", "Total", 0.16 * Me.lstCOMPRO.Width
    Me.lstCOMPRO.ColumnHeaders("TOT").Alignment = lvwColumnRight
    Me.tlbACCIONES.Buttons("TOD").Enabled = False
    Me.tlbACCIONES.Buttons("CON").Enabled = False
    Me.tlbACCIONES.Buttons("SIN").Enabled = False
    Me.tlbACCIONES.Buttons("SAV").Enabled = False
    Me.tlbACCIONES.Buttons("CAN").Enabled = False
    Me.txtDESDE.Text = FormatDateTime(DateAdd("d", -(Day(Date) - 1), Date), vbShortDate)
    Me.txtHASTA.Text = FormatDateTime(DateAdd("d", -1, DateAdd("M", 1, DateAdd("d", -(Day(Date) - 1), Date))), vbShortDate)
    Me.chkRANGO.Value = False
    Me.txtDESDE.Enabled = False
    Me.txtHASTA.Enabled = False
    
    Me.lstSALXBOD.ListItems.Clear
    Me.lstSALXBOD.ColumnHeaders.Clear
    Me.lstSALXBOD.ColumnHeaders.Add , "COD", "C�digo", 0.2 * Me.lstSALXBOD.Width
    Me.lstSALXBOD.ColumnHeaders.Add , "NOM", "Nombre", 0.5 * Me.lstSALXBOD.Width
    Me.lstSALXBOD.ColumnHeaders.Add , "SLD", "Saldo", 0.16 * Me.lstSALXBOD.Width
    Me.lstSALXBOD.ColumnHeaders("SLD").Alignment = lvwColumnRight
    Me.lstSALXBOD.ColumnHeaders.Add , "MIN", "M�nimo", 0.16 * Me.lstSALXBOD.Width
    Me.lstSALXBOD.ColumnHeaders("MIN").Alignment = lvwColumnRight
    Me.lstSALXBOD.ColumnHeaders.Add , "REP", "Reposi", 0.16 * Me.lstSALXBOD.Width
    Me.lstSALXBOD.ColumnHeaders("REP").Alignment = lvwColumnRight
    Me.lstSALXBOD.ColumnHeaders.Add , "MAX", "M�ximo", 0.16 * Me.lstSALXBOD.Width
    Me.lstSALXBOD.ColumnHeaders("MAX").Alignment = lvwColumnRight
    Me.lstSALXBOD.ColumnHeaders.Add , "DES", "MxDesp", 0.16 * Me.lstSALXBOD.Width
    Me.lstSALXBOD.ColumnHeaders("DES").Alignment = lvwColumnRight
    Me.lstSALXBOD.ColumnHeaders.Add , "ENT", "Entradas", 0.16 * Me.lstSALXBOD.Width
    Me.lstSALXBOD.ColumnHeaders("ENT").Alignment = lvwColumnRight
    Me.lstSALXBOD.ColumnHeaders.Add , "SAL", "Salidas", 0.16 * Me.lstSALXBOD.Width
    Me.lstSALXBOD.ColumnHeaders("SAL").Alignment = lvwColumnRight
    
    Campos = "IN_COMPROBANTE.NU_AUTO_COMP, IN_COMPROBANTE.TX_CODI_COMP, IN_COMPROBANTE.TX_NOMB_COMP, IN_COMPROBANTE.TX_PRFJ_COMP, IN_COMPROBANTE.NU_AUTO_DOCU_COMP, IN_COMPROBANTE.NU_COMP_COMP, IN_COMPROBANTE.NU_AUTO_ANUL_COMP, IN_COMPROBANTE.TX_REQU_COMP, IN_COMPROBANTE.TX_ROMP_COMP"
    Desde = "IN_COMPROBANTE AS IN_COMPROBANTE_1, IN_DOCUMENTO, IN_COMPROBANTE"
    Condi = "IN_COMPROBANTE_1.TX_REQU_COMP='S'" & _
        " AND IN_DOCUMENTO.NU_AUTO_DOCUDEPE_DOCU = IN_COMPROBANTE.NU_AUTO_DOCU_COMP" & _
        " AND IN_COMPROBANTE_1.NU_AUTO_DOCU_COMP = IN_DOCUMENTO.NU_AUTO_DOCU"
    ReDim ArrUXB(8, 0)
    Result = LoadMulData(Desde, Campos, Condi, ArrUXB())
    If Result = FAIL Then GoTo FALLO
    If Not Encontro Then GoTo NOENC
    For CnTdr = 0 To UBound(ArrUXB, 2)
        Me.cboCOMPRO.AddItem ArrUXB(2, CnTdr)
        Me.cboCOMPRO.ItemData(Me.cboCOMPRO.NewIndex) = ArrUXB(0, CnTdr)
    Next
FALLO:
NOENC:
    Me.cboCOMPRO.ListIndex = -1
    Me.cboCOMPRO.Text = "Seleccione uno"
    numCOMPRO = Me.cboCOMPRO.ListIndex
End Sub

'SeDesplazo
Private Sub lstARTICULO_ItemClick(ByVal Item As MSComctlLib.ListItem)
    Call lstARTICULO_KeyUp(0, 0)
End Sub

Private Sub lstARTICULO_SeDesplazo(ByVal Item As MSComctlLib.ListItem)
    vArticulo.IniciaXCodigo (Trim(Me.lstARTICULO.SelectedItem.Text))
    Call INTCllBodegas(vBodegas, vArticulo.Para)
    Call InfoArticuloXBodega(Me.lstSALXBOD, vArticulo, vBodegas, Me.lstSALXBOD.Visible)
End Sub

Private Sub lstARTICULO_KeyUp(KeyCode As Integer, Shift As Integer)
    If Not vSeCargoVentana Then Exit Sub
    If Trim(Me.lstARTICULO.SelectedItem.Key) = vKeyArticulo Then Exit Sub
    vKeyArticulo = Trim(Me.lstARTICULO.SelectedItem.Key)
'    Call lstARTICULO_ItemClick(Me.lstARTICULO.SelectedItem)
    Call lstARTICULO_SeDesplazo(Me.lstARTICULO.SelectedItem)
End Sub

Private Sub lstCOMPRO_ItemClick(ByVal Item As MSComctlLib.ListItem)
    Call lstCOMPRO_KeyUp(0, 0)
    'DAHV M3938 Se invoca este procedimiento en esta secci�n para que se calculen los datos apartir de lo que se seleccione
    'por parte del usuario en la lista de los comprobantes - Inicio
     If Me.lstCOMPRO.ListItems.Count > 0 Then
        Call lstCOMPRO_SeDesplazo(Me.lstCOMPRO.SelectedItem)
     End If
     'DAHV M3938 Fin
End Sub

Private Sub lstCOMPRO_SeDesplazo(ByVal Item As MSComctlLib.ListItem)
    Dim Clave As String, nUme As Integer
    VlrDocumento = 0
    Me.lstARTICULO.ListItems.Clear
'    vDocumento.CargaDocumentoActualXAutoNumerico (CLng(Me.lstCOMPRO.SelectedItem.SubItems(4)))
    vDocumento.CargaDocumentoActualXAutoNumerico CLng(Me.lstCOMPRO.SelectedItem.Tag)
    For nUme = 1 To vDocumento.NroDeArticulos
        Set vArticulo = vDocumento.GETArticuloXIndice(nUme)
        Clave = "A" & Trim(vArticulo.Codigo) & Right(String(5, "0") & nUme, 5)
        Me.lstARTICULO.ListItems.Add , Clave, vArticulo.Codigo
        Me.lstARTICULO.ListItems(Clave).ListSubItems.Add , "NOM", vArticulo.Nombre
        Me.lstARTICULO.ListItems(Clave).ListSubItems.Add , "CAN", FormatNumber(vArticulo.Cantidad * vArticulo.Multi / vArticulo.Divide, 3, vbFalse, vbTrue)
        Me.lstARTICULO.ListItems(Clave).ListSubItems.Add , "COS", FormatCurrency(vArticulo.CostoPromedio, 0, vbUseDefault, vbTrue, vbUseDefault)
        Me.lstARTICULO.ListItems(Clave).ListSubItems.Add , "TOT", FormatCurrency(vArticulo.Cantidad * vArticulo.CostoPromedio, 0, vbUseDefault, vbTrue, vbUseDefault)
        VlrDocumento = VlrDocumento + vArticulo.CostoPromedio * vArticulo.Cantidad * vArticulo.Multi / vArticulo.Divide
    Next
    Me.lstCOMPRO.ListItems(Item.Key).ListSubItems("TOT").Text = FormatCurrency(Me.ValorDocumento, 0, vbUseDefault, vbTrue, vbUseDefault)
    Me.lstSALXBOD.ListItems.Clear
    If Me.lstARTICULO.ListItems.Count > 0 Then
        Call lstARTICULO_SeDesplazo(Me.lstARTICULO.SelectedItem)
    End If
End Sub

Private Sub lstCOMPRO_KeyUp(KeyCode As Integer, Shift As Integer)
    If Not vSeCargoVentana Then Exit Sub
    If Trim(Me.lstCOMPRO.SelectedItem.Key) = vKeyComprobante Then Exit Sub
    vKeyComprobante = Trim(Me.lstCOMPRO.SelectedItem.Key)
'    Call lstCOMPRO_ItemClick(Me.lstCOMPRO.SelectedItem)
    Call lstCOMPRO_SeDesplazo(Me.lstCOMPRO.SelectedItem)
End Sub

Private Sub tlbACCIONES_ButtonClick(ByVal Button As MSComctlLib.Button)
    If Not Me.cboCOMPRO.ListIndex > -1 Then Exit Sub
    Dim CondFECHAS As String, flgExiste As Boolean
    Dim lstAUTO As String, ClvCompro As String
    Dim cllBORRA As New Collection
    Dim Tercero As New ElTercero, unElemento As MSComctlLib.ListItem
    
    Dim Incont As Integer 'DAHV M3938
    
    Select Case Button.Key
    Case "TOD", "CON", "SIN"
        Me.lstCOMPRO.ListItems.Clear
'        If CBool(Me.chkRANGO.Value) Then CondFECHAS = " AND (FE_CREA_ENCA>=" & ComiF & _
'            FormatDateTime(CDate(Me.txtDESDE.Text), vbShortDate) & ComiF & " AND FE_CREA_ENCA<=" & ComiF & _
'            FormatDateTime(CDate(Me.txtHASTA.Text), vbShortDate) & ComiF & ")"
        '****JACC M3252
        If CBool(Me.chkRANGO.Value) Then CondFECHAS = " AND (FE_CREA_ENCA>=" & ComiF & _
            Format(FormatDateTime(CDate(Me.txtDESDE.Text), vbShortDate), FormatF) & ComiF & " AND FE_CREA_ENCA<=" & ComiF & _
            Format(FormatDateTime(CDate(Me.txtHASTA.Text), vbShortDate), FormatF) & ComiF & ")"
        '**JACC M3252
        Me.fraRANGO.Enabled = False
        Me.cboCOMPRO.Enabled = False
        Me.tlbACCIONES.Buttons("TOD").Enabled = False
        Me.tlbACCIONES.Buttons("CON").Enabled = False
        Me.tlbACCIONES.Buttons("SIN").Enabled = False
        Me.tlbACCIONES.Buttons("SAV").Enabled = True
        Me.tlbACCIONES.Buttons("CAN").Enabled = True
        vSeCargoVentana = False 'smdl m1477
        
        Result = 0: Encontro = 0
        Campos = "0, NU_AUTO_ENCA, NU_AUTO_COMP_ENCA, NU_COMP_ENCA, " & _
            "CD_CODI_TERC_ENCA, FE_CREA_ENCA, TX_NOMB_COMP, TX_PRFJ_COMP"
        Desde = "IN_ENCABEZADO, IN_COMPROBANTE"
        'JAUM T32017 Inicio
        ReDim ArrUXB(0)
        Condi = "NU_AUTO_COMP = NU_AUTO_COMP_ENCA "
        Condi = Condi & "AND NU_AUTO_COMP_ENCA = " & Me.cboCOMPRO.ItemData(Me.cboCOMPRO.ListIndex) & CondFECHAS
        Result = LoadData(Desde, "COUNT(NU_AUTO_ENCA)", Condi, ArrUXB)
        If ArrUXB(0) > "32766" Then
           Call Mensaje1("El total de registros solicitados excede la cantidad permitida (32767), por favor seleccione " & _
           "un filtro de fechas, o un rango de fechas m�s corto", 3)
        End If
        'JAUM T32017 Fin
        Condi = "NU_AUTO_COMP = NU_AUTO_COMP_ENCA" & _
            " AND NU_AUTO_COMP_ENCA=" & Me.cboCOMPRO.ItemData(Me.cboCOMPRO.ListIndex) & CondFECHAS & _
            " ORDER BY NU_COMP_ENCA"
        ReDim ArrUXB(7, 0)
        'JAUM T32017 Inicio Se deja linea en comentario
        'Result = LoadMulData(Desde, Campos, Condi, ArrUXB())
        Condicion = "SELECT TOP (32767) NU_AUTO_ENCA FROM " & Desde & " WHERE " & Condi
        Result = LoadMulData(Desde, "TOP (32767) " & Campos, Condi, ArrUXB())
        'JAUM T32017 Fin
        If Result = FAIL Then Exit Sub
        If Not Encontro Then Exit Sub
        Me.lstCOMPRO.ListItems.Clear
        
        For CnTdr = 0 To UBound(ArrUXB, 2)
'            lstAUTO = lstAUTO & IIf(Len(lstAUTO) > 0, ",", "(") & ArrUXB(1, CnTdr)
            ClvCompro = "C" & ArrUXB(1, CnTdr)
            Debug.Print ClvCompro
            Tercero.IniXNit (ArrUXB(4, CnTdr))
            Me.lstCOMPRO.ListItems.Add , ClvCompro, ArrUXB(7, CnTdr) & Right(String(7, "0") & ArrUXB(3, CnTdr), 7)
            Me.lstCOMPRO.ListItems(ClvCompro).Selected = True
            Me.lstCOMPRO.ListItems(ClvCompro).Tag = ArrUXB(1, CnTdr)
            Me.lstCOMPRO.ListItems(ClvCompro).ListSubItems.Add , "FEC", FormatDateTime(ArrUXB(5, CnTdr), vbShortDate)
            Me.lstCOMPRO.ListItems(ClvCompro).ListSubItems.Add , "NIT", Tercero.Nit
            Me.lstCOMPRO.ListItems(ClvCompro).ListSubItems.Add , "NOM", Tercero.Nombre
            Me.lstCOMPRO.ListItems(ClvCompro).ListSubItems.Add , "AUE", CStr(CLng(ArrUXB(1, CnTdr)))
'            lstCOMPRO_SeDesplazo Me.lstCOMPRO.SelectedItem
'            Me.lstCOMPRO.ListItems(ClvCompro).ListSubItems.Add , "TOT", FormatCurrency(Me.ValorDocumento, 0, vbUseDefault, vbTrue, vbUseDefault)
            Me.lstCOMPRO.ListItems(ClvCompro).ListSubItems.Add , "TOT", FormatCurrency(0, 0, vbUseDefault, vbTrue, vbUseDefault)
        Next
        If Len(lstAUTO) > 0 Then lstAUTO = lstAUTO & ")"
        
'        Campos = "NU_AUTO_AUTO,NU_AUTO_COMP_AUTO,NU_AUTO_ENCA_AUTO,NU_AUTO_USUA_AUTO,NU_CONE_AUTO,TX_OBSE_AUTO"
'        Desde = "IN_AUTORIZACION"
'        Condi = "NU_AUTO_ENCA_AUTO IN " & lstAUTO
        Condi = ""
        Campos = "NU_AUTO_AUTO,NU_AUTO_COMP_AUTO,NU_AUTO_ENCA_AUTO,NU_AUTO_USUA_AUTO,NU_CONE_AUTO,TX_OBSE_AUTO"
        Desde = "IN_AUTORIZACION, IN_ENCABEZADO, IN_COMPROBANTE"
        Condi = Condi & "NU_AUTO_COMP = NU_AUTO_COMP_ENCA"
        Condi = Condi & " AND NU_AUTO_ENCA_AUTO = NU_AUTO_ENCA"
        Condi = Condi & " AND NU_AUTO_COMP_ENCA=" & Me.cboCOMPRO.ItemData(Me.cboCOMPRO.ListIndex) & CondFECHAS

        ReDim ArrUXB(5, 0)
        Result = LoadMulData(Desde, Campos, Condi, ArrUXB())
        If Result = FAIL Then Exit Sub
        
        For Each unElemento In Me.lstCOMPRO.ListItems
            Select Case Button.Key
            Case "TOD"
                flgExiste = False
                For CnTdr = 0 To UBound(ArrUXB, 2)
                    If IsNumeric(ArrUXB(2, CnTdr)) Then If CLng(unElemento.Tag) = CLng(ArrUXB(2, CnTdr)) Then flgExiste = True: Exit For
                Next
                If flgExiste Then unElemento.Checked = True
            Case "CON"
                flgExiste = False
                For CnTdr = 0 To UBound(ArrUXB, 2)
                    If IsNumeric(ArrUXB(2, CnTdr)) Then If CLng(unElemento.Tag) = CLng(ArrUXB(2, CnTdr)) Then flgExiste = True: Exit For
                Next
                If flgExiste Then
                    unElemento.Checked = True
                Else
                    cllBORRA.Add unElemento.Key
                End If
            Case "SIN"
                flgExiste = False
                For CnTdr = 0 To UBound(ArrUXB, 2)
                    If IsNumeric(ArrUXB(2, CnTdr)) Then If CLng(unElemento.Tag) = CLng(ArrUXB(2, CnTdr)) Then flgExiste = True: Exit For
                Next
                If Not flgExiste Then
                    unElemento.Checked = False
                Else
                    cllBORRA.Add unElemento.Key
                End If
            End Select
        Next
        For CnTdr = 1 To cllBORRA.Count
            Me.lstCOMPRO.ListItems.Remove cllBORRA.Item(CnTdr)
        Next
    Case Else
        Me.fraRANGO.Enabled = True
        Me.cboCOMPRO.Enabled = True
        Me.tlbACCIONES.Buttons("TOD").Enabled = True
        Me.tlbACCIONES.Buttons("CON").Enabled = True
        Me.tlbACCIONES.Buttons("SIN").Enabled = True
        Me.tlbACCIONES.Buttons("SAV").Enabled = False
        Me.tlbACCIONES.Buttons("CAN").Enabled = False
        If Button.Key = "SAV" Then
            'JAUM T32017 Inicio se deja bloque en comentario
            'For Each unElemento In Me.lstCOMPRO.ListItems
                'lstAUTO = lstAUTO & IIf(Len(lstAUTO) > 0, ",", "(") & unElemento.Tag
            'Next
            'If Len(lstAUTO) > 0 Then
                'lstAUTO = lstAUTO & ")"
            'JAUM T32017 Fin
                Campos = "NU_AUTO_AUTO,NU_AUTO_COMP_AUTO,NU_AUTO_ENCA_AUTO,NU_AUTO_USUA_AUTO,NU_CONE_AUTO,TX_OBSE_AUTO"
                Desde = "IN_AUTORIZACION"
                'Condi = "NU_AUTO_ENCA_AUTO IN " & lstAUTO 'JAUM T32017 Se deja linea en comentario
                Condi = "NU_AUTO_ENCA_AUTO IN (" & Condicion & ")" 'JAUM T32017
                ReDim ArrUXB(5, 0)
                Result = LoadMulData(Desde, Campos, Condi, ArrUXB())
                If Result = FAIL Then Exit Sub
                For Each unElemento In Me.lstCOMPRO.ListItems
                    flgExiste = False
                    For CnTdr = 0 To UBound(ArrUXB, 2)
                        If IsNumeric(ArrUXB(2, CnTdr)) Then If CLng(unElemento.Tag) = CLng(ArrUXB(2, CnTdr)) Then flgExiste = True: Exit For
                    Next
                    If flgExiste And unElemento.Checked Then
                    ElseIf Not flgExiste And unElemento.Checked Then
                        Campos = "NU_AUTO_COMP_AUTO=" & Me.cboCOMPRO.ItemData(Me.cboCOMPRO.ListIndex)
                        Campos = Campos & ",NU_AUTO_ENCA_AUTO=" & unElemento.Tag
                        Encontro = DoInsertSQL("IN_AUTORIZACION", Campos)
                    ElseIf flgExiste And Not unElemento.Checked Then
                        Condi = "NU_AUTO_ENCA_AUTO=" & unElemento.Tag
                        Encontro = DoDelete("IN_AUTORIZACION", Condi)
                    ElseIf Not flgExiste And Not unElemento.Checked Then
                    End If
                Next
            'End If 'JAUM T32017 Se deja linea en comentario
        End If
        Me.lstCOMPRO.ListItems.Clear
        Me.lstARTICULO.ListItems.Clear
        Me.lstSALXBOD.ListItems.Clear
    End Select
    'If Me.lstCOMPRO.ListItems.Count > 0 Then Call lstCOMPRO_SeDesplazo(Me.lstCOMPRO.SelectedItem)
    
    '===================================================================================================
    'DAHV M3938 'DAHV Solo es estaba mostrando el valor total del �ltimo item
    'debido a que era el que quedaba seleccionado - Inicio
    
'    InCont = 0
'    While InCont < Me.lstCOMPRO.ListItems.Count
'      Me.lstCOMPRO.ListItems(InCont + 1).Selected = True
'      Call lstCOMPRO_SeDesplazo(Me.lstCOMPRO.SelectedItem)
'      InCont = InCont + 1
'      Me.lstARTICULO.ListItems.Clear
'      Me.lstSALXBOD.ListItems.Clear
'    Wend
    
    'DAHV M3938 Fin
    '===================================================================================================
End Sub

Private Sub TxtDesde_KeyPress(KeyAscii As Integer)
    Call Cambiar_Enter(KeyAscii) 'LJSA M3966
End Sub

Private Sub TxtDesde_LostFocus()
    Call ValFecha(txtDESDE, 1)  'LJSA M3966
End Sub


Private Sub TxtHasta_KeyPress(KeyAscii As Integer)
    Call Cambiar_Enter(KeyAscii)    'LJSA M3966
End Sub

Private Sub TxtHasta_LostFocus()
    'LJSA M3966 ------------------
    Call ValFecha(txtHASTA, 1)
    If Evaluar_Fechas(txtDESDE.Text, txtHASTA.Text) = False Then
        Call Mensaje1("La fecha inicial no puede ser mayor a la fecha final.", 2)
    End If
    'LJSA M3966 ------------------
End Sub


